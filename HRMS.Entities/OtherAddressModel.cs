﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{

    public class OtherAddressModel
    {
        public int AddressInfoId { get; set; }

        public int EmployeeID { get; set; }

        [Display(Name = "Address Type", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int AddressType { get; set; }

        [Display(Name = "Apt No:", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string ApartmentNo { get; set; }

        [Display(Name = "Address Name:", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string Address { get; set; }

        [Display(Name = "Address Line 1:", GroupName = "Logger")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2:", GroupName = "Logger")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Street:", GroupName = "Logger")]
        public String Street { get; set; }

        [Display(Name = "P.O.BOX:", GroupName = "Logger")]
        public string PoBox { get; set; }
        [Display(Name = "Post Code:", GroupName = "Logger")]
        public string PostCode { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Country:", GroupName = "Logger")]
        public int CountryID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "State:", GroupName = "Logger")]
        public int StateID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "City:", GroupName = "Logger")]
        public int CityID { get; set; }

        [Display(Name = "Area:", GroupName = "Logger")]
        public int? AreaId { get; set; }

        public string CountryName { get; set; }

        public string StateName { get; set; }

        public string CityName { get; set; }

        public string AreaName { get; set; }
        public string AddressTypeName { get; set; }
    }
}
