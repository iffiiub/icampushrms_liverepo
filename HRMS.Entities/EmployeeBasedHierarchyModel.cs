﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeBasedHierarchyModel
    {
        public EmployeeBasedHierarchyModel()
        {
            WorkFlowGroupModelList = new List<WorkFlowGroupModel>();
            EmployeeHierarchyModelList = new List<EmployeeHierarchyModel>();
        }
        public IList<WorkFlowGroupModel> WorkFlowGroupModelList { get; set; }
        public IList<EmployeeHierarchyModel> EmployeeHierarchyModelList { get; set; }
    }
    public class EmployeeHierarchyModel
    {
        public int EBHierarchyID { get; set; }
        public int EmployeeID { get; set; }

        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public int ManagerID { get; set; }
        public string ManagerAlternativeID { get; set; }
        public string ManagerName { get; set; }
        //*** Naresh 2020-03-19 CompanyName displayed
        public string CompanyName { get; set; }
        public int GroupID { get; set; }
    }

    public class WorkFlowGroupModel
    {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool SchoolBasedGroup { get; set; }
    }
}
