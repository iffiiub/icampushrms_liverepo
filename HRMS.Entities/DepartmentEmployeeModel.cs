﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class DepartmentEmployeeModel :BaseModel
    {
        public int DepartmentID { get; set; }
        public int EmployeeID { get; set; }
        public bool IsActive { get; set; }
        public bool IsLeader { get; set; }

        
    }
}
