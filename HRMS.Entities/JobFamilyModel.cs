﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class JobFamilyModel : BaseModel
    {
        public int JobFamilyID { set; get; }
        public string JobFamilyName { set; get; }
        public string JobFamilyNameFrench { set; get; }
        public string JobFamilyDescription { set; get; }
    }
}
