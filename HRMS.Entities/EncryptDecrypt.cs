﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
   public class EncryptDecrypt
    {
        public string decryptPassword(string password)
        {
            byte[] HashKey = SecureIt.GetHashKey(Properties.Settings.Default["salt1"].ToString());
            string Decrypted = SecureIt.Decrypt(HashKey, password);
            return Decrypted;
        }

        public string EncryptPassword(string password)
        {
            byte[] HashKey = SecureIt.GetHashKey(Properties.Settings.Default["salt1"].ToString());
            string encrypted = SecureIt.Encrypt(HashKey, password);
            return encrypted;
        }
    }
}
