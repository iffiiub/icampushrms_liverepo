﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class ExpenseReportDetails : BaseModel
    {
        public int ExpenseReportDetailsId { set; get; }
        public int ExpenseReportId { set; get; }
        public string Category { set; get; }
        public string Description { set; get; }
        public int? Units { set; get; }
        public decimal? CostPerUnit { set; get; }
        public decimal? Tax { set; get; }
        public string Receipts { set; get; }
        public decimal? Total { set; get; }
        public int BillTo { set; get; }
        public Guid ExpenseReportTempId { set; get; }
        public Guid ExpenseReportDetailsTempId { set; get; }
    }
}
