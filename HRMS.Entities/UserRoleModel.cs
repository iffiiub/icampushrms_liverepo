﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class UserRoleModel
    {
        [Display(Name = "Roles")]
        public int UserRoleId { set; get; }
        [Display(Name = "User role name", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string UserRoleName { set; get; }
        [Display(Name = "Permissions", GroupName = "Logger")]
        public string Permissions { set; get; }
        [Display(Name = "Read only pages", GroupName = "Logger")]
        public string ViewPagesSubCategoryIDs { set; get; }
        public string AddPagesSubCategoryIDs { set; get; }
        public string UpdatePagesSubCategoryIDs { set; get; }
        public string DeletePagesSubCategoryIDs { set; get; }
        public string OtherPermissionIds { set; get; }
        [Display(Name = "Job Category", GroupName = "Logger")]
        public Int16? EmployeeJobCategoryID { set; get; }
        public string EmployeeJobCategoryName { set; get; }
    }


    public class PermissionModel
    {
        public int UserRoleId { set; get; }
        public string UserRoleName { set; get; }
        public string ModuleName { set; get; }
        public bool IsMainNavigationPermission { get; set; }
        public bool IsViewOnlyPermission { set; get; }
        public bool IsAddOnlyPermission { set; get; }
        public bool IsUpdateOnlyPermission { set; get; }
        public bool IsDeleteOnlyPermission { set; get; }
        
    }
}
