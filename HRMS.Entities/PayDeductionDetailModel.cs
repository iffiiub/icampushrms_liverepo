﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayDeductionDetailModel
    {
       
        public int PayDeductionDetailID { get; set; }

        public int PayDeductionID { get; set; }

        public string DeductionDate { get; set; }

        public double Amount { get; set; }

        public string Comments { get; set; }

        public bool IsActive { get; set; }
        public Boolean IsProcessed { get; set; }
        public bool IsDeleted { get; set; }
        public double TotalAmount { get; set; }
        public double AmountLeft { get; set; }
        public int EmployeeId { get; set; }
        public bool IsWaived { get; set; }
    }

    public class PayDeductionAmountsSummation
    {
        public int PayDeductionID { get; set; }
        
        public double Amount { get; set; }

        public double InstallmentTotalAmount { get; set; }
    }
}
