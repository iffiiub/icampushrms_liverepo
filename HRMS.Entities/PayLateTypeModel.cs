﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayLateTypeModel
    {
        public int PayLateTypeID { get; set; }

        public string PayLateTypeName_1 { get; set; }

        public string PayLateTypeName_2 { get; set; }

        public string PayLateTypeName_3 { get; set; }

    }
}
