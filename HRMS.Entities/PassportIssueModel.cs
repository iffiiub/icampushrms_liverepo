﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PassportIssueModel
    {
        public int PassportIssueId { get; set; }

        public string PassportIssueName { get; set; }
    }
}
