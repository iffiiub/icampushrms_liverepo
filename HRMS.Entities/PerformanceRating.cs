﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PerformanceRatingModel
    {
        public int RateIndex { get; set; }
        public int EmployeeId { get; set; }
        public int PerformanceRatingID { get; set; }
        public string EmployeeAltId { get; set; }
        public string EmployeeName { get; set; }
        public decimal Rate { get; set; }
        public int AcademicYearId { get; set; }
        public string AcademicYear { get; set; }
        public string Note { get; set; }
    }
}
