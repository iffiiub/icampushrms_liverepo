﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeePersonalIdentityInfoModel
    {
        public int PersonalInfoId { get; set; }

        public int EmployeeID { get; set; }
        public int DocPassportId { get; set; }
        public int DocVisaId { get; set; }

        [Display(Name = "Passport Number:", GroupName = "Logger")]
        public string PassportNumber { get; set; }

        //[Display(Name = "Passport Issue By")]
        //public int PassportIssueBy { get; set; }

        [Display(Name = "Passport Issue Date:", GroupName = "Logger")]
         
        public string PassportIssueDate { get; set; }

        [Display(Name = "Passport Expiry Date:", GroupName = "Logger")]
       
        public string PassportExpiryDate { get; set; }

        [Display(Name = "Insurance Number")]
        public string InsuranceNumber { get; set; }

        [Display(Name = "Visa Number:", GroupName = "Logger")]
        public string VisaNumber { get; set; }

        [Display(Name = "Visa Issue Date:", GroupName = "Logger")]
        
        public string VisaIssueDate { get; set; }

        [Display(Name = "Visa Expiration Date:", GroupName = "Logger")]
         
        public string VisaExpirationDate { get; set; }

        

        [Display(Name = "Passport Issue Name")]
        public string PassportIssueName { get; set; }

        [Display(Name = "Passport Exp Rem Name")]
        public string PassportExpRemName { get; set; }
    }
}
