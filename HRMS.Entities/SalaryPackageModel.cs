﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
   public class SalaryPackageModel
    {
        public int SalaryPackagesID { get; set; }

        public string SalaryPackgeName { get; set; }
            
        public bool? IsEnabled { get; set; }

        public List<SalaryPackageAllowances> lstPackAllowances { get; set; }
 
    }

    public class SalaryPackageAllowances
    {
        public int id { get; set; }

        public decimal AmountInPercent { get; set; }

        public int SalaryPackagesID { get; set; }

        public string SalaryPackageName { get; set; }
        public short SalaryAllowanceId { get; set; }

        public string PaySalaryAllowanceName { get; set; }

        public decimal ActualAmount { get; set; }

        public bool isSelected { get; set; }

    }

   
}
