﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class UserGroupModel : BaseModel
    {
        public int UserGroupId { set; get; }
        [Display(Name="Group Name", GroupName = "Logger")]
        public string UserGroupName { set; get; }
        public string Description { set; get; }
        [Display(Name = "Roles", GroupName = "Logger")]
        public string RoleIds { set; get; }
        [Display(Name = "Roles", GroupName = "Logger")]
        public string RoleNames { set; get; }
        [Display(Name = "Employees")]
        public string EmployeeIds { set; get; }
        [Display(Name = "Employees")]
        public string EmployeeNames { set; get; }

        public bool isSelectedAll { set; get; }

        public int EmployeeID { set; get; }

        [Display(Name = "Account Status:", GroupName = "Logger")]
        public int Status { get; set; }

        private List<string> _selectedValues;
        private List<CheckListBoxItem> _items;

        public List<CheckListBoxItem> Items
        {
            get { return _items ?? (_items = new List<CheckListBoxItem>()); }
            set { _items = value; }
        }

        // the name of this list should be the same as of the CheckBoxes otherwise you will not get any result after post
        public List<string> SelectedValues
        {
            get { return _selectedValues ?? (_selectedValues = new List<string>()); }
            set { _selectedValues = value; }
        }

        [Display(Name = "Termination Reason")]
        [Required(ErrorMessage = "Please enter Leave Reason")]
        public int LeftReasonID { set; get; }

        [Display(Name = "Termination Date", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter Leave Date")]
        public string LeftDate { get; set; }
    
        [Required(ErrorMessage = "Please select employee list access")]
        public int UserListAccessID { get; set; }
        [Required(ErrorMessage = "Please select company type")]
        public int CompanyTypeID { get; set; }
        public string ListAccessName { get; set; }
        public string CompanyTypeName { get; set; }
       // [Required(ErrorMessage = "Please select a company")]
        public string CompanyIds { get; set; }
        public string CompanyNames { get; set; }
        public bool IsEditable { get; set; }      

    }

    public class UserGroupListAccess
    {
        public int UserListAccessID { get; set; }
        public string ListAccessName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }                                           
    }
}
