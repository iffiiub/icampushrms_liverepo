﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HRMS.Entities
{
    public class SchoolInformation
    {
        public int SchoolID { get; set; }
        public string SchoolName_1 { get; set; }
        public string SchoolName_2 { get; set; }
        public string SchoolName_3 { get; set; }
        public string SchoolShortName_1 { get; set; }
        public string SchoolShortName_2 { get; set; }
        public string SchoolShortName_3 { get; set; }
        public string Slogan_1 { get; set; }
        public string Slogan_2 { get; set; }
        public string Slogan_3 { get; set; }
        public string SchoolSystem_1 { get; set; }
        public string SchoolSystem_2 { get; set; }
        public string SchoolSystem_3 { get; set; }
        public string CampusName_1 { get; set; }
        public string CampusName_2 { get; set; }
        public string CampusName_3 { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string RegistrationNUmber { get; set; }
        public string MinistryName_1 { get; set; }
        public string MinistryName_2 { get; set; }
        public string MinistryName_3 { get; set; }
        public string SchoolLargeLogoPath { get; set; }
        public string SchoolReportLogoPath { get; set; }
        public string SchoolSmallLogoPath { get; set; }
        public DateTime EstablishDate { get; set; }
        public int iConnectAccountNumber { get; set; }
        public bool IsActive { get; set; }
        public string SchoolCode { get; set; }
        public bool isAVGRounding { get; set; }
        public bool HasIBProgram { get; set; }
        public decimal TotalGraduationCredit { get; set; }
        public bool IsAttendancePerCoursePeriod { get; set; }
        public bool HasMidTermGB { get; set; }
        public int HighestAbsenteeism { get; set; }
        public bool HasCLGExportModule { get; set; }
        public bool HasICDL { get; set; }
        public string ICDLCode { get; set; }
        public string ICDLID { get; set; }
        public string isGradingBookPerTeacher { get; set; }
        public string SchoolKeyID { get; set; }
        public bool IsEncryptedPassword { get; set; }
        public string EyeSightLimit { get; set; }
        public int RoundingDigits { get; set; }
        public bool IsPwdVerificationEnabled { get; set; }
        public bool isEncryptor { get; set; }
        public string StudentPicField { get; set; }
        public string GBShowPassPortName { get; set; }
        public string GBOrderBy { get; set; }
        public string MOESchoolID { get; set; }
        public string MarkVerificationRPTOrderBy { get; set; }
        public bool UseAlternativeTeacherIdForImage { get; set; }
        public string FingerPrintSyncType { get; set; }
        public string MOECouncillor { get; set; }
        public string Logo { get; set; }

        /*Contact details*/
        public string Address { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public string City_2 { get; set; }
        public string AreaName_1 { get; set; }
        public string AreaName_2 { get; set; }
        public string Country { get; set; }
        public string Country_2 { get; set; }
        public string POBox { get; set; }
        public string POBox_2 { get; set; }
        public string Phone { get; set; }
        public string Phone_2 { get; set; }
        public string Fax { get; set; }
        public string Fax_2 { get; set; }
        public string EmailId { get; set; }
        public string Website { get; set; }  
        
        public string TodaysDateTime { get; set; }
        public string ReportLogo { get; set; }
        public string  AcNextYear { get; set; }

    }

    public class GeneralAccSetting
    {
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string FilsName { get; set; }
    }
}
