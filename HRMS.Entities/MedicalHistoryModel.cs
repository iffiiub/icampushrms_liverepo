﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class MedicalHistoryModel : BaseModel
    {
        public int EmployeeHealthHistoryID { set; get; }
        
        public string EmployeeHealthHistoryDate { set; get; }
          
        public string Description { set; get; }
          public string Action { set; get; }
         public string Note { set; get; }
       
        public int EmployeeID { set; get; }
         
        public int aTntTranMode { set; get; }
 



    }
}
