﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class ShiftGRoupModel : BaseModel
    {
        public int GroupNameID { get; set; }


        [Required(ErrorMessage = "Group Name Required")]
        public string GroupNameEn { get; set; }

       
        public string GroupNameAr { get; set; }
         
        public string GroupNameFr { get; set; }
    
        [Required(ErrorMessage = "Select Group Head")]
        public int Head { get; set; }
        
        public int Assistant1 { get; set; }
        public int Assistant2 { get; set; }
        public int Assistant3 { get; set; }
        public int Section { get; set; }

         [Required(ErrorMessage = "Select Shift")]
        public int Shift { get; set; }


        public String Assistant1Name { get; set; }
        public String Assistant2Name { get; set; }
        public String Assistant3Name { get; set; }

        public String SectionName { get; set; }
        public String ShiftName { get; set; }
        public String HeadName { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }


      
        //public IEnumerable<SelectListItem> departmentList { get; set; }

    }
}
