﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayAbsentTypeModel
    {
        public int PayAbsentTypeID { get; set; }

        public string PayAbsentTypeName_1 { get; set; }

        public string PayAbsentTypeName_2 { get; set; }

        public string PayAbsentTypeName_3 { get; set; }

        public bool ISDeductable { get; set; }
        public int AbsentPaidTypeId { get; set; }
        public bool AnnualLeave { get; set; }

    }
}
