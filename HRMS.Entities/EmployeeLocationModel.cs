﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeLocationModel
    {
        public List<EmployeeDetailsModel> employeeInLocation { get; set; }
        public List<EmployeeDetailsModel> employeeNotInLocation { get; set; }
        public int LocationID { get; set; }
        public EmployeeLocationModel()
        {
            this.employeeInLocation = new List<EmployeeDetailsModel>();
            this.employeeNotInLocation = new List<EmployeeDetailsModel>();
        }

      
    }
}
