﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class LeaveReason
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "LeaveReason (EN)", GroupName = "Logger")]
        public string LeaveReason_1 { get; set; }

        [Display(Name = "LeaveReason (AR)", GroupName = "Logger")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LeaveReason_2 { get; set; }

        [Display(Name = "LeaveReason (FR)", GroupName = "Logger")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LeaveReason_3 { get; set; }
    }
}
