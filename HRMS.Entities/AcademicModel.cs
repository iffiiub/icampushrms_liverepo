﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class AcademicModel : BaseModel
    {
        public int AcademicID { set; get; }
        public int EmployeeID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int HRQualificationID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int HRDegreeID { set; get; }
        public string StartDate { set; get; }
        public string EndDate { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int HRInstituteID { set; get; }
        public bool MinistryApproved { set; get; }
        public string NameAsInDegree { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int CountryID { set; get; }
        public string HRQualificationName_1 { set; get; }
        public string HRDegreeName_1 { set; get; }
        public string HRInstituteName_1 { set; get; }
        public string CountryName_1 { set; get; }
       
        public bool Completed { set; get; }
        public bool Relevant { get; set; }
    }

    public class CertificateModel : BaseModel
    {
        public int CertificateID { set; get; }
        public int EmployeeID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
     
        public int HRDegreeID { set; get; }
        public string CertificationDate { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int HRInstituteID { set; get; }
        public bool MinistryApproved { set; get; }
        public string NameAsInDegree { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int CountryID { set; get; }
        public string CertificationName { set; get; }
        public string HRDegreeName_1 { set; get; }
        public string HRInstituteName_1 { set; get; }
        public string CountryName_1 { set; get; }

    }
}
