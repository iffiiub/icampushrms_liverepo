﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class LeaveBalanceManagerModel
    {
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }        
        public string EmployeeName { get; set; }
        public string DepartmentName_1 { get; set; }        
        public DateTime? HireDate { get; set; }
        public decimal LapsingDays { get; set; }
        public DateTime? LapseDate { get; set; }
        public decimal? ExtendedLapsingDays { get; set; }
        public DateTime? ExtendedLapseDate { get; set; }
        public int VacationTypeID { get; set; }
        public string VacationTypeName { get; set; }
        public float TotalDays { get; set; }
        public float TotalLeaveDays { get; set; }
        public decimal BalanceLeaveDays { get; set; }
        public bool IsAccumulatedLeave { get; set; }
        public bool IsActive { get; set; }
        public int VTCategoryID { get; set; }
        public string CompanyName { get; set; }
        public int AnnualVacationTypeID { get; set; }
    }
    public class LeaveBalanceManagerViewModel
    {
        public List<LeaveBalanceManagerModel> LeaveBalanceManagerModelList { get; set; }
        public List<VacationTypeModel> VacationTypeModellList { get; set; }
    }

    public class AccumulativeAllVacations
    {
        public int AccumulativeAllVacationsID { get; set; }
        public int EmployeeID { get; set; }
        public int VacationTypeID { get; set; } 
        public DateTime Date { get; set; }
        public float Days { get; set; }
        public string VacationDates { get; set; }      
        public bool IsDeleted { get; set; }
        public bool IsManual { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public float? LapsingDays { get; set; }
        public float? ExtendedDays { get; set; }
        public DateTime? ExtendedDate { get; set; }
        public DateTime? LapsingDate { get; set; }

    }
    public class LeaveAccrualReportModel
    {
        //public int AccumulativeAllVacationsID { get; set; }
        public string CompanyName { get; set; }        
        //public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string EmailID { get; set; }
        public string AccrualDate { get; set; }
        public string AccrualDays { get; set; }
        public string TotalAccrualDaysBefore { get; set; }
        public string TotalAccrualDaysAfter { get; set; }
        public string ModifiedByName { get; set; }       
        public string ModifiedOn { get; set; }
        public string BalanceUpdated { get; set; }

    }
}
