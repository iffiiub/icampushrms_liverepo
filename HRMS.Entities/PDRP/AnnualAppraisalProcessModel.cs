﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.PDRP
{
    public class AnnualAppraisalProcessModel
    {
        public AnnualAppraisalProcessModel()
        {
            MidYearAppraisalFormModel = new MidYearAppraisalForm();
            BusinessTargetMidYearAppraisalList = new List<BusinessTargetsMidYearAppraisal>();
            MidYearAppraisalDetailsModel = new MidYearAppraisalDetails();
        }

        public int GoalSettingId { get; set; }
        public int FullFormMode { get; set; }
        public int ID { get; set; }
        public int AnnualAppraisalProcessId { get; set; }
        public int RequestId { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string ProcessName { get; set; }
        public int PerformanceGroupId { get; set; }
        public string PerformanceGroupName { get; set; }
        public bool IsGoalSubmitted { get; set; }
        public int YearId { get; set; }
        public string Year { get; set; }
        public string EvaluationStartDate { get; set; }
        //*** Naresh 2020-02-27 To display intermidiate status
        public string MidEvaluationStartDate { get; set; }
        public string FullEvaluationStartDate { get; set; }
        public string EvaluationDueDate { get; set; }
        public int ReqStatusId { get; set; }
        public int RequesterEmployeeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsAddMode { get; set; }
        public int CompanyId { get; set; }
        public bool IsSubmitForLM { get; set; }
        public bool IsSignOffForm { get; set; }
        public bool IsMidYearAppraisalSubmitted { get; set; }
        public bool IsFullYearAppraisalSubmitted { get; set; }
        public int PeriodId { get; set; }
        //public int TotalScore { get; set; }
        //public int ProfessionalTotalScore { get; set; }
        //public int BehavioralTotalScore { get; set; }
        //public int OverallScore { get; set; }
        public string OverallScoreFormula { get; set; }

        //*** Naresh 2020-03-11 Save comments during save
        public string Comments { get; set; }
        public MidYearAppraisalForm MidYearAppraisalFormModel { get; set; }
        public IEnumerable<BusinessTargetsMidYearAppraisal> BusinessTargetMidYearAppraisalList { get; set; }
        public MidYearAppraisalDetails MidYearAppraisalDetailsModel { get; set; }
        public FullYearAppraisalDetails FullYearAppraisalDetails { get; set; }
        public List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightages { get; set; }
        public List<ProfessionalCompetencies> ProfessionalCompetencies { get; set; }
        public List<BehavioralCompetencies> BehavioralCompetencies { get; set; }
    }
    public class AnnualAppraisalReverceData
    {
        //public AnnualAppraisalProcessModel()
        //{
        //    MidYearAppraisalFormModel = new MidYearAppraisalForm();
        //    BusinessTargetMidYearAppraisalList = new List<BusinessTargetsMidYearAppraisal>();
        //    MidYearAppraisalDetailsModel = new MidYearAppraisalDetails();
        //}
        public int Cnt { get; set; }
        public int FormID { get; set; }
        public int FormInstanceID { get; set; }
        public string IsReversed { get; set; }
        public string MidIsReversed { get; set; }
        public string FullIsReversed { get; set; }

        //public int GoalSettingId { get; set; }
        //public int FullFormMode { get; set; }
        //public int ID { get; set; }
        //public int AnnualAppraisalProcessId { get; set; }
        public int RequestId { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        //public string ProcessName { get; set; }
        public int PerformanceGroupId { get; set; }
        //public string PerformanceGroupName { get; set; }
        //public bool IsGoalSubmitted { get; set; }
        public int YearId { get; set; }
        public string Year { get; set; }
        //public string Year { get; set; }
        //public string EvaluationStartDate { get; set; }
        ////*** Naresh 2020-02-27 To display intermidiate status
        //public string MidEvaluationStartDate { get; set; }
        //public string FullEvaluationStartDate { get; set; }
        //public string EvaluationDueDate { get; set; }
        public int ReqStatusId { get; set; }
        //public int RequesterEmployeeId { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public bool IsAddMode { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int ProcessInitializeBy { get; set; }
        public string ProcessInitializeByName { get; set; }
        public int DepartmentID  { get; set; }
        public string  DepartmentName_1{ get; set; }
        //public bool IsSubmitForLM { get; set; }
        //public bool IsSignOffForm { get; set; }
        //public bool IsMidYearAppraisalSubmitted { get; set; }
        //public bool IsFullYearAppraisalSubmitted { get; set; }
        public int PeriodId { get; set; }
        ////public int TotalScore { get; set; }
        ////public int ProfessionalTotalScore { get; set; }
        ////public int BehavioralTotalScore { get; set; }
        ////public int OverallScore { get; set; }
        //public string OverallScoreFormula { get; set; }

        ////*** Naresh 2020-03-11 Save comments during save
        //public string Comments { get; set; }
        //public MidYearAppraisalForm MidYearAppraisalFormModel { get; set; }
        //public IEnumerable<BusinessTargetsMidYearAppraisal> BusinessTargetMidYearAppraisalList { get; set; }
        //public MidYearAppraisalDetails MidYearAppraisalDetailsModel { get; set; }
        //public FullYearAppraisalDetails FullYearAppraisalDetails { get; set; }
        //public List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightages { get; set; }
        //public List<ProfessionalCompetencies> ProfessionalCompetencies { get; set; }
        //public List<BehavioralCompetencies> BehavioralCompetencies { get; set; }
    }

    public class MidYearAppraisalForm
    {
        public int MidYearAppraisalFormId { get; set; }
        public int ID { get; set; }
        public int PerformanceGroupId { get; set; }
        public int EmployeeID { get; set; }
        public int ApproverEmployeeID { get; set; }
        public int YearId { get; set; }
        public string BusinessTargetsComments { get; set; }
        public string CoreCompetenciesComments { get; set; }
        public int CompanyId { get; set; }
        public int ReqStatusID { get; set; }
        public int RequesterEmployeeId { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
        public bool IsSignOffForm { get; set; }
        public string JsonString { get; set; }
        public string MidYearAppraisalDetails { get; set; }
        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }
        public string JsonString1 { get; set; }
        public string JsonString2 { get; set; }
        public string JobGrade { get; set; }
        public string FormState { get; set; }

        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }


        public Decimal BusinessTargetsTota { get; set; }
        public Decimal ProfessionalCompetenciesAVG { get; set; }
        public Decimal BehavioralCompetenciesAVG { get; set; }
        public Decimal CompetenciesAVG { get; set; }
        public Decimal OverallAppraisalScore { get; set; }
        public string OverallScoreFormula { get; set; }
        
        public string FormSubmit { get; set; }

    }

    public class BusinessTargetsMidYearAppraisal
    {
        
        public int BusinessTargetNo { get; set; }

        public int BusinessTargetId { get; set; }
        public string BusinessTargetDetails { get; set; }
        public string Accomplishments { get; set; }
        public int Weight { get; set; }
        public int? Rating { get; set; }
        public string Score { get; set; }

        public String Formula { get; set; }
        public int ID { get; set; }
        public int PerformanceGroupId { get; set; }
    }

    public class MidYearAppraisalDetails
    {
        public int MidYearAppraisalDetailId { get; set; }
        public int ID { get; set; }
        public int PerformanceGroupId { get; set; }
        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }
    }
    public class FullYearAppraisalDetails
    {
        public int FullYearAppraisalDetailId { get; set; }
        public int ID { get; set; }
        public int PerformanceGroupId { get; set; }
        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
    }


    public class MidYearEvaluationReportModel
    {
        public string BUName { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeOracleNumber { get; set; }
        public string IsGoalSettingInitialize { get; set; }
        public string IsGoalSettingCompleted { get; set; }
        public string IsMidYrEvaluationInitialize { get; set; }
        public string RequestID { get; set; }
        public string Year { get; set; }
        public string StartDate { get; set; }
        public string DueDate { get; set; }
        public string MidYearSignOffDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
    }

    public class FullYearEvaluationReportModel
    {
        public string BUName { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeOracleNumber { get; set; }
        public string IsGoalSettingInitialize { get; set; }
        public string IsGoalSettingCompleted { get; set; }
        public string IsMidYrEvaluationInitialize { get; set; }
        public string IsMidYrEvaluationCompleted { get; set; }
        public string IsFullYrEvaluationInitialize { get; set; }
        public string RequestID { get; set; }
        public string Year { get; set; }
        public string StartDate { get; set; }
        public string DueDate { get; set; }
        public string FullYearSignOffDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
    }

    //public class AppraisalNonTeachDetailsRatingScaleWeightages
    //{
    //    public int RatingScaleID { get; set; }

    //    public int RatingScaleNumber { get; set; }

    //}
}
