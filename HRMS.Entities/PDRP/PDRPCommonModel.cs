﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace HRMS.Entities.PDRP
{
    public class PDRPCommonModel
    {

    }

    public class PDRPTeachersModel : Employee
    {
        public int DropInsCount { get; set; }
        public string VisitDate { get; set; }
        public string VisitTime { get; set; }
        public string PreObservationDate { get; set; } //*** Evaluation Due Date also
        public int KPICount { get; set; }
        public int Observation { get; set; }
    }

    public class PDRPFormulas
    {
        public int FormulasId { get; set; }
        public string Formulas { get; set; }
        public string FormulasType { get; set; }
        public int IsActive { get; set; }
    }
    public class PDRPFormulasTotalScore
    {
        public decimal Total { get; set; }
        public decimal TotalScore { get; set; }
        public string Formula { get; set; }
        public List<decimal> ItemTotal { get; set; }
    }
    //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
    public class PDRPA1UserAndCurrentUserDetails
    {
        public int A1User { get; set; }
        public int A1UserGroupID { get; set; }

        public int CurUser { get; set; }
        public int CurUserGroupID { get; set; }
        public bool EmployeeBasedGorup { get; set; }
        public int TaskCount { get; set; }
    }

}
