﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.Common;
using System.Web.Mvc;
using HRMS.XMLResources;
namespace HRMS.Entities.Forms
{
    public class KPIsListModel
    {
        [Display(Name = "KPIYear", ResourceType = typeof(KPI_Resources))]
        public string KPIYear { get; set; }
        //[Display(Name = "Total Weightage")]
        [Display(Name = "TotalWeightage", ResourceType = typeof(KPI_Resources))]
        public int TotalWeightage { get; set; }

        public List<KPIsModel> KPIsList { get; set; }
        //[PDRPLocalizedDisplayName("PDRP_KPI.KPIYears", false)]
        //[LocalizedDynamicValidators(KeyName = "KPIYears")]
        //[PDRPXMLResources("KPI_Resources", "KPIYears")]
        [Display(Name = "KPIYears", ResourceType = typeof(KPI_Resources))]
        //[Required(ErrorMessageResourceType = typeof(KPI_Resources),
        // ErrorMessageResourceName = "KPIYearsRequired")]
        //[StringLength(50, ErrorMessageResourceType = typeof(KPI_Resources),
        //         ErrorMessageResourceName = "KPIYearsLong")]
        public List<KPIYears> KPIYears { get; set; }

      

        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public List<PDRPFormula> PDRPFormulasList { get; set; }
        public bool IsAnnualAppraisalStarted { get; set; }
    }
    public class PDRPFormula
    {
        public string FormulasType { get; set; }
    }
    public class KPIYears
    {
        public int BeginYear { get; set; }
    }
    public class KPIsModel
    {

        public int KPIId { get; set; }

        [Required]
        [Display(Name = "KPIYear", ResourceType = typeof(KPI_Resources))]
        //[Display(Name = "Year")]
        public string KPIYear { get; set; }
        
        //*** Danny A000101.100 01/10/2019
        
        public int CompanyID { get; set; }

        [Display(Name = "KPIAreaEng", ResourceType = typeof(KPI_Resources))]
        [Required]
        //[Display(Name = "KPI Area (English)")]
        public string KPIAreaEng { get; set; }

        [Display(Name = "KPIDescriptionEng", ResourceType = typeof(KPI_Resources))]
        //[Display(Name = "KPI Description (English)")]
        public string KPIDescriptionEng { get; set; }

        [Display(Name = "KPIAreaArab", ResourceType = typeof(KPI_Resources))]
        //[Display(Name = "KPI Area (Arabic)")]
        public string KPIAreaArab { get; set; }

        [Display(Name = "KPIDescriptionArab", ResourceType = typeof(KPI_Resources))]
        //[Display(Name = "KPI Description (Arabic)")]
        public string KPIDescriptionArab { get; set; }


        [Required]
        [Range(1, 100)]
        [Display(Name = "KPIWeightage", ResourceType = typeof(KPI_Resources))]
        //[Display(Name = "Weightage")]
        public int KPIWeightage { get; set; }
        [Required]
        [Range(1, 5)]
        [Display(Name = "KPIOrder", ResourceType = typeof(KPI_Resources))]
        //[Display(Name = "No")]
        public int KPIOrder { get; set; }
        [Required]
        public byte KPIActive { get; set; }
        [Required]
        public int KPICreatedBy { get; set; }
        //[Required]
        //public DateTime KPICreatedOn { get; set; }
        //public int KPIModifiedBy { get; set; }
        //[Required]
        //public DateTime KPIModifiedOn { get; set; }

    }
}
