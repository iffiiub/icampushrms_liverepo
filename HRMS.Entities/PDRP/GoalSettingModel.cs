﻿using HRMS.Entities.Common;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.PDRP
{
    public class GoalSettingModel
    {

        public GoalSettingModel()
        {
            FormABusinessTargetsModelList = new List<AnnualGoalSettingFormAModel>();
            FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();

            FormBKPIModelList = new List<AnnualGoalSettingFormBModel>();
            FormBKPIModel = new AnnualGoalSettingFormBModel();

            FormCKPIModelList = new List<AnnualGoalSettingFormCModel>();
            FormCKPIModel = new AnnualGoalSettingFormCModel();
            PDRPA1UserAndCurrentUserDetails = new PDRPA1UserAndCurrentUserDetails(); //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
        }
        //*** F10
        public string IsReversed { get; set; }
        public string MidIsReversed { get; set; }
        public string FullIsReversed { get; set; }
        public int GolReqStatusID { get; set; }
        public int MidReqStatusID { get; set; }
        public int FullReqStatusID { get; set; }



        //[LocalizedDisplayName("RecruitR1BudgetedModel.Comments", true)]
        [Display(Name = "Requester/Approver Comments:")]
        public string Comments { get; set; }
        public string ReverseStateComment { get; set; }
        public bool EmployeeBasedGroup { get; set; }
       
        public int FormMode { get; set; }
        public int MidFormMode { get; set; }
        public int FullFormMode { get; set; }
        public int FormProcessID { get; set; }
        public int ID { get; set; }
        public int GoalSettingId { get; set; }
        public int RequestId { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string ProcessName { get; set; }
        public int PerformanceGroupId { get; set; }
        public string PerformanceGroupName { get; set; }
        public bool IsGoalSubmitted { get; set; }
        public int YearId { get; set; }
        public string Year { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string SettingGoalsStartDate { get; set; }
        public string SettingGoalsDueDate { get; set; }
        public int ReqStatusId { get; set; }
        public int RequesterEmployeeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsAddMode { get; set; }
        public int CompanyId { get; set; }
        public int CurrCompanyId { get; set; }
        public bool IsSubmitForLM { get; set; }
        public bool IsSignOffForm { get; set; }
        public bool IsMidSignOffForm { get; set; }
        public bool IsFullSignOffForm { get; set; }
        public bool IsFullyear { get; set; }
        public IEnumerable<AnnualGoalSettingFormAModel> FormABusinessTargetsModelList { get; set; }

        public decimal TotalBusinessTarget { get; set; }
        public AnnualGoalSettingFormAModel FormABusinessTargetsModel { get; set; }
        public ApprovalRequesterInfo ApprovalRequesterInfo { get; set; }
        public IEnumerable<AnnualGoalSettingFormBModel> FormBKPIModelList { get; set; }
        public AnnualGoalSettingFormBModel FormBKPIModel { get; set; }
        //*** F5
        public List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightages { get; set; }
        public List<AppraisalNonTeachDetailsRatingScaleWeightages_New> AppraisalNonTeachDetailsRatingScaleWeightages_New { get; set; }
        public IEnumerable<AnnualGoalSettingFormCModel> FormCKPIModelList { get; set; }
        public AnnualGoalSettingFormCModel FormCKPIModel { get; set; }
        public AnnualGoalSettingFormDModel FormDModel { get; set; }
        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }
        public string OverallScoreFormula { get; set; }
        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
        public List<ProfessionalCompetencies> ProfessionalCompetencies { get; set; }
        public List<BehavioralCompetencies> BehavioralCompetencies { get; set; }

        public PDRPA1UserAndCurrentUserDetails PDRPA1UserAndCurrentUserDetails { get; set; } //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
    }
    public class BehavioralCompetencies
    {
        public int No { get; set; }
        public string Score { get; set; }

        public int CompetencyID { get; set; }
        public int CompetencyTypeID { get; set; }
        public string CompetencyTypeName_1 { get; set; }
        public string CompetencyTypeName_2 { get; set; }
        public string CompetencyTypeName_3 { get; set; }
        public string CompetencyName_1 { get; set; }
        public string CompetencyName_2 { get; set; }
        public string CompetencyName_3 { get; set; }

        public string CompetencyDescription_1 { get; set; }
        public string CompetencyDescription_2 { get; set; }

        public string CompetencyDescription_3 { get; set; }


        public int IsActive { get; set; }

        public int IsDeleted { get; set; }

    }
    public class ProfessionalCompetencies
    {
        public int No { get; set; }
        public string Score { get; set; }

        public string FullScore { get; set; }



        public int CompetencyID { get; set; }
        public int CompetencyTypeID { get; set; }
        public string CompetencyTypeName_1 { get; set; }
        public string CompetencyTypeName_2 { get; set; }
        public string CompetencyTypeName_3 { get; set; }
        public string CompetencyName_1 { get; set; }
        public string CompetencyName_2 { get; set; }
        public string CompetencyName_3 { get; set; }

        public string CompetencyDescription_1 { get; set; }
        public string CompetencyDescription_2 { get; set; }

        public string CompetencyDescription_3 { get; set; }


        public int IsActive { get; set; }

        public int IsDeleted { get; set; }
    }
    public class ApprovalRequesterInfo : RequesterInfoViewModel
    {

    }
    public class GoalSettingOverAllRatingScalesModel
    {
        public int RatingScaleID { get; set; }
        public string RatingScaleCharacter { get; set; }
        public int RatingScaleNumber { get; set; }
        public decimal FinalScoreSlabMIN { get; set; }
        public decimal FinalScoreSlabMAX { get; set; }
        public string DefinitionName { get; set; }
        public string DefinitionDetails { get; set; }
        public int IsActive { get; set; }
        public string ScoreSlab { get; set; }
    }



    public class AnnualGoalSettingFormAModel
    {
        public int ID { get; set; }
        public int BusinessTargetNo { get; set; }
        public int BusinessTargetId { get; set; }
        public int PerformanceGroupId { get; set; }
        public int EmployeeId { get; set; }
        public string BusinessTargetDetails { get; set; }
        public string Accomplishments { get; set; }
        public string BusinessTargetsComments { get; set; }
        public string Rating { get; set; }
        public decimal Score { get; set; }

        public String Formula { get; set; }
      

        public int? Weight { get; set; }
        public int CompanyId { get; set; }
        public string BusinesstargetComments { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string CompetenciesComments { get; set; }
        public string CoreCompetenciesComments { get; set; }
        public int YearId { get; set; }
        public string JsonString { get; set; }
        public string JsonStringCurrRatingScales { get; set; }
        public int EmployeeID { get; set; }
        public int RequesterEmployeeId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string JobGrade { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string Grade { get; set; }

        public string FormState { get; set; }
        public int MidFormMode { get; set; }

        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }

        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
        public Decimal BusinessTargetsTota { get; set; }
        public Decimal ProfessionalCompetenciesAVG { get; set; }
        public Decimal BehavioralCompetenciesAVG { get; set; }
        public Decimal CompetenciesAVG { get; set; }
        public Decimal OverallAppraisalScore { get; set; }
        public string FormSubmit { get; set; }
        //*** F2
        public int GoalSettingId { get; set; }
        public string EmployeeName { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string RequestEmail { get; set; }
        public string Department { get; set; }
        public string LineManager { get; set; }
        public string DOJ { get; set; }
        //*** Naresh 2020-03-11 Save comments during save
        public string Comments { get; set; }
    }

    public class AnnualGoalSettingFormBModel
    {
        public int ID { get; set; }
        public int BusinessTargetNo { get; set; }
        public int KPIId { get; set; }
        public int PerformanceGroupId { get; set; }
        public int EmployeeId { get; set; }
       
        public string BusinessTargetDetails { get; set; }
        public string Accomplishments { get; set; }
        public int? Weight { get; set; }
        public string Rating { get; set; }
        public decimal Score { get; set; }
        public String Formula { get; set; }
        public int CompanyId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string KPIsComments { get; set; }
        public string CompetenciesComments { get; set; }
        public int YearId { get; set; }
        public string JsonString { get; set; }
        public string JsonStringCurrRatingScales { get; set; }
        public int EmployeeID { get; set; }
        public int RequesterEmployeeId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string JobGrade { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string Grade { get; set; }

        public string FormState { get; set; }

        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }

        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
        public Decimal BusinessTargetsTota { get; set; }
        public Decimal ProfessionalCompetenciesAVG { get; set; }
        public Decimal BehavioralCompetenciesAVG { get; set; }
        public Decimal CompetenciesAVG { get; set; }
        public Decimal OverallAppraisalScore { get; set; }
        public string FormSubmit { get; set; }
        //*** F2
        public int GoalSettingId { get; set; }
        public string EmployeeName { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string RequestEmail { get; set; }
        public string Department { get; set; }
        public string LineManager { get; set; }
        public string DOJ { get; set; }
        //*** Naresh 2020-03-11 Save comments during save
        public string Comments { get; set; }

    }
    public class AnnualGoalSettingFormModel
    {
        public int ID { get; set; }
        public int BusinessTargetNo { get; set; }
        public int KPIId { get; set; }
        public int PerformanceGroupId { get; set; }
        public int EmployeeId { get; set; }
        public string BusinessTargetDetails { get; set; }
        public string Accomplishments { get; set; }
        public int? Weight { get; set; }
        public int Rating { get; set; }
        public decimal Score { get; set; }
        public int CompanyId { get; set; }
        public string KPIsComments { get; set; }
        public string CompetenciesComments { get; set; }
        public int YearId { get; set; }
        public string JsonString { get; set; }
        public string JsonStringCurrRatingScales { get; set; }
        public int EmployeeID { get; set; }
        public int RequesterEmployeeId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string JobGrade { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string Grade { get; set; }

        public string FormState { get; set; }

        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }

        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
        public Decimal BusinessTargetsTota { get; set; }
        public Decimal ProfessionalCompetenciesAVG { get; set; }
        public Decimal BehavioralCompetenciesAVG { get; set; }
        public Decimal CompetenciesAVG { get; set; }
        public Decimal OverallAppraisalScore { get; set; }
        public string FormSubmit { get; set; }
        //*** F2
        public int GoalSettingId { get; set; }
        public string EmployeeName { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string RequestEmail { get; set; }
        public string Department { get; set; }
        public string LineManager { get; set; }
        public string DOJ { get; set; }
        //*** Naresh 2020-03-11 Save comments during save
        public string Comments { get; set; }
    }

    public class AnnualGoalSettingFormCModel
    {
        public int ID { get; set; }
        public int BusinessTargetNo { get; set; }
        public int KPIId { get; set; }
        public int PerformanceGroupId { get; set; }
        public int EmployeeId { get; set; }
        public string BusinessTargetDetails { get; set; }
        public string Accomplishments { get; set; }
        public int? Weight { get; set; }       
        public string Rating { get; set; }
        public decimal Score { get; set; }
        public int CompanyId { get; set; }
        public string KPIsComments { get; set; }
        public string CompetenciesComments { get; set; }
        public int YearId { get; set; }
        public string JsonString { get; set; }
        public string JsonStringCurrRatingScales { get; set; }
        public int EmployeeID { get; set; }
        public int RequesterEmployeeId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string JobGrade { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string Grade { get; set; }

        public string FormState { get; set; }

        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }

        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
        public Decimal BusinessTargetsTota { get; set; }
        public Decimal ProfessionalCompetenciesAVG { get; set; }
        public Decimal BehavioralCompetenciesAVG { get; set; }
        public Decimal CompetenciesAVG { get; set; }
        public Decimal OverallAppraisalScore { get; set; }
        public string FormSubmit { get; set; }
        //*** F2
        public int GoalSettingId { get; set; }
        public string EmployeeName { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string RequestEmail { get; set; }
        public string Department { get; set; }
        public string LineManager { get; set; }
        public string DOJ { get; set; } 
        //*** Naresh 2020-03-11 Save comments during save
        public string Comments { get; set; }
    }
    public class AnnualGoalSettingFormDModel
    {
        public int ID { get; set; }
        //public int KPIId { get; set; }
        public int PerformanceGroupId { get; set; }
        public int EmployeeId { get; set; }
        //public string BusinessTargetDetails { get; set; }
        //public int Weight { get; set; }
        //public int Rating { get; set; }
        //public decimal Score { get; set; }
        public int CompanyId { get; set; }
        //public string KPIsComments { get; set; }
        public string CompetenciesComments { get; set; }
        public int YearId { get; set; }
        public string JsonString { get; set; }
        public string JsonString1 { get; set; }
        public string JsonStringCurrRatingScales { get; set; }
        public int EmployeeID { get; set; }
        public int RequesterEmployeeId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string JobGrade { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string Grade { get; set; }

        public string FormState { get; set; }

        public string LineManagerEvaluationComments { get; set; }
        public string TrainingNeeds { get; set; }
        public string LineManagerMidYearPerformanceComments { get; set; }
        public string EmployeeMidYearPerformanceComments { get; set; }

        public string MyThreeKeyAchivements { get; set; }
        public string MyTwoDevelopmentAreas { get; set; }
        public string EmployeeCommentsonFullYearPerformance { get; set; }
        public string Strengths { get; set; }
        public string FullTrainingNeeds { get; set; }
        public string LineManagerCommentsonFullYearPerformance { get; set; }
        //public Decimal BusinessTargetsTota { get; set; }
        public Decimal MidProfessionalCompetenciesAVG { get; set; }
        public Decimal FullProfessionalCompetenciesAVG { get; set; }
        public Decimal ProfessionalCompetenciesAVG { get; set; }
        public Decimal BehavioralCompetenciesAVG { get; set; }
        //public Decimal BehavioralCompetenciesAVG { get; set; }
        //public Decimal CompetenciesAVG { get; set; }
        //public Decimal OverallAppraisalScore { get; set; }
        public string FormSubmit { get; set; }

        //*** F2
        public int GoalSettingId { get; set; }
        public string EmployeeName { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string RequestEmail { get; set; }
        public string Department { get; set; }
        public string LineManager { get; set; }
        public string DOJ { get; set; }
        //*** Naresh 2020-03-11 Save comments during save
        public string Comments { get; set; }
    }
    public class AppraisalNonTeachDetailsRatingScaleWeightages
    {
        public int RatingScaleID { get; set; }

        public string RatingScaleNumber { get; set; }

    }
    public class AppraisalNonTeachDetailsRatingScaleWeightages_New
    {
        public int RatingScaleID { get; set; }

        public int RatingScaleNumber { get; set; }

    }
    public class GoalSettingReportModel
    {
        public string BUName { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeOracleNumber { get; set; }
        public string IsGoalSettingInitialize { get; set; }
        public string RequestId { get; set; }
        public string Year { get; set; }
        public string StartDate { get; set; }
        public string DueDate { get; set; }
        public string GoalSettingSignOffDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
    }
}
