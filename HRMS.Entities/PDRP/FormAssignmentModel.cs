﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.PDRP
{
    public class FormAssignmentModel
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeeName { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string LineManager { get; set; }
        //*** Naresh 2020-02-26 Line Manager name display with his Id and BU
        public int LineManagerId { get; set; }
        public string LMBUName { get; set; }
        public int PerformanceGroupId { get; set; }
    }
}
