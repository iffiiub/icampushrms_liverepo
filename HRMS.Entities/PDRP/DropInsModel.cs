﻿using HRMS.Entities.Forms;
using HRMS.XMLResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.PDRP
{
    public class DropIns:BankModel
    {
        public int ID { get; set; }
        public int RequestID { get; set; }
        public int TeacherID { get; set; }
        public int isEmployeeAcknowledgedDropIn { get; set; }
        public string LessonTopic { get; set; }

        public int StudentsOnRollMale { get; set; }

        public int StudentsOnRollFemale { get; set; }
        public DateTime? VisitDATE { get; set; }
        public int DropInYearID { get; set; }
        public string DropInYear { get; set; }
        public string Branch { get; set; }
        public string GradeYear { get; set; }

        public string GroupSection { get; set; }

        public int StudentsPresentMale { get; set; }
        public int StudentsPresentFemale { get; set; }
        public int Total { get; set; }
        public decimal OverAllScore { get; set; }

    }
    
    public class DropInsModel
    {   
        //*** F4
        public int EmpCurrentCompanyID { get; set; }
        public DropInsModel()
        {

            this.DropInEmployeeModel = new DropInEmployeeModel();
        }
        public string isAddMode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string Feedback { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string Outline { get; set; }

        
        public string PreviousTargets { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public int? Targets { get; set; }
        public string ProficiencyScoreFormula { get; set; }
        public int ProficiencyScoreTotal { get; set; }
        public decimal ProficiencyScoreOverall { get; set; }
        public bool EmployeeAcknowledgedDropIn { get; set; }    
        public DropInEmployeeModel DropInEmployeeModel { get; set; }
        public List<DropInProficiencyLevel> DropInProficiencyLevel { get; set; }
        public List<DropInsRatingScaleWeightages> DropInsRatingScaleWeightages { get; set; }
        public List<DropInsRatingScaleWeightageDetails> DropInsRatingScaleWeightageDetails { get; set; }
        public BaseModel basemodel { get; set; }
    }
    public class DropInEmployeeModel : RequesterInfoViewModel
    {
        public int ID { get; set; }
        public int TeacherID { get; set; }
        public int TempTeacherID { get; set; }
        public int TeacherAlternateID { get; set; }
        public int ReqStatusID { get; set; }
        public string TeacherName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string LessonTopic { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsOnRollMale { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsOnRollFemale { get; set; }

        //[StringLength(50, ErrorMessageResourceType = typeof(Resources.Resources),
        //                  ErrorMessageResourceName = "FirstNameLong")]
        //[Range(0, 130, ErrorMessageResourceType = typeof(Resources.Resources),
        //               ErrorMessageResourceName = "AgeRange")]
        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]        
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        [Display(Name ="VisitDate", ResourceType = typeof(DropIn_Resources))]

        public DateTime? VisitDATE { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        [Display(Name = "Drop In Year")]
        public int DropinYearID { get; set; }


        [Display(Name = "DropInYear", ResourceType = typeof(DropIn_Resources))]
        public string DropinYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                   ErrorMessageResourceName = "Required")]
        public string Branch { get; set; }


        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                   ErrorMessageResourceName = "Required")]        
        public string GradeYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string GroupSection { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsPresentMale { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsPresentFemale { get; set; }


        [Required(ErrorMessageResourceType = typeof(DropIn_Resources),
                  ErrorMessageResourceName = "Validation12")]
        [Display(Name = "VisitTime", ResourceType = typeof(DropIn_Resources))]
        public string VisitTime { get; set; }
        public List<DropInsRatingScaleWeightageDetails> DropInsRatingScaleWeightageDetails { get; set; }

    }
    public class DropInsRatingScaleWeightageDetails
    {
        public int RatingScaleID { get; set; }
        public string RatingScaleCharacter { get; set; }
        public int RatingScaleNumber { get; set; }
        public int FinalScoreSlabMIN { get; set; }
        public int FinalScoreSlabMAX { get; set; }

        public string DefinitionName { get; set; }
        public string DefinitionDetails { get; set; }

        public string DefinitionName2 { get; set; }
        public string DefinitionDetails2 { get; set; }
        public int IsActive { get; set; }

    }
    public class DropInProficiencyLevel
    {
        public int ProficiencyLevelID { get; set; }
        public int ProficiencyID { get; set; }
        public string ProficiencyDetail { get; set; }
        public int ProficiencyGroupID { get; set; }
        public string ProficiencyGroupName { get; set; }
        //*** F5
        public string ProficiencyScore { get; set; }
        public string ProficiencyFormula { get; set; }
        //public int ProficiencyValue { get; set; }
        public int IsActive { get; set; }

        //*** F6
        public string CompanyIDs { get; set; }

        public int IsRadioOption { get; set; }
    }
    public class DropInsRatingScaleWeightages
    {
        public int RatingScaleID { get; set; }
        
        public int RatingScaleNumber { get; set; }
       
    }
    //*** F5
    public class DropInsRatingScaleWeightagesNA
    {
        public int RatingScaleID { get; set; }

        public String RatingScaleNumber { get; set; }

    }
    public class DropInStatusReportModel
    {
        public string EmployeeAlternativeID { get; set; }
        public string RequestNo { get; set; }
        public string EmployeeName { get; set; }
        public string OracleNo { get; set; }
        public string Department { get; set; }
        public string VisitDate { get; set; }
        public string ObservationYear { get; set; }
        public string LineManager { get; set; }
        public string EmployeeAcknowledgedDropIn { get; set; }
        public int Rating { get; set; }
        public decimal TotalScore { get; set; }
    }
}
