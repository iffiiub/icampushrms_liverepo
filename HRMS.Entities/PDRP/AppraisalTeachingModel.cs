﻿using HRMS.Entities.Forms;
using HRMS.XMLResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities.PDRP
{
    public class AppraisalTeachingModel
    { 

        public AppraisalTeachingModel()
        {
            this.AppraisalTeachingRatingScaleWeightages = new List<AppraisalTeachingRatingScaleWeightages>();
            this.AppraisalTeachingEmployeeModel = new AppraisalTeachingEmployeeModel();
        }
        public bool EmployeeBasedGroup { get; set; }
        public int FormProcessID { get; set; }
        public int InViewMode { get; set; }

        public int ApproverEmployeeID { get; set; }
        public int LoginID { get; set; }
        public string isAddMode { get; set; }
        public string KPIFormula { get; set; }

        //*** 0=Not Form Employee
        //*** 2=
        public string Comments { get; set; }
        public int FormMode { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                 ErrorMessageResourceName = "Required")]
        public string DropInComment { get; set; }
        public int DropInRating { get; set; }
        public decimal DropInScoreOverall { get; set; }
        public string FormulasDropin { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string ObservEvlComment { get; set; }
        public int ObservEvlRating { get; set; }
        public int ObservEvlID { get; set; }
        public decimal ObservEvlScoreOverall { get; set; }

        public string FormulasObservation { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string DevelopmentPlan { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public int PromotionPotantial { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string PromotionPotantialComment { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public int NextPromotion { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string NextPromotionComment { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public int LeavingRisk { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string LeavingRiskComment { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public int LeavingReason { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string LeavingReasonComment { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string TalentAreaImage { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string AppraisalComment { get; set; }
        
        //*** F4
        public int EmpCurrentCompanyID { get; set; }

        public AppraisalTeachingEmployeeModel AppraisalTeachingEmployeeModel { get; set; }
        public List<AppraisalTeachingProficiencyLevel> AppraisalTeachingProficiencyLevel { get; set; }
        public List<AppraisalTeachingRatingScaleWeightages> AppraisalTeachingRatingScaleWeightages { get; set; }
        public AppraisalKPIsListModel AppraisalKPIsListModel { get; set; }
        public List<DropInProficiencyLevel> DropInProficiencyLevel { get; set; }
        public BaseModel basemodel { get; set; }

        public SelectList AppraisalKPIYears { get; set; }
    }
    public class AppraisalKPIsListModel
    {
        public string KPIYear { get; set; }
        [Display(Name = "Total Weightage")]
        public decimal TotalKPIScore { get; set; }
        public List<AppraisalKPIsModel> AppraisalKPIsModel { get; set; }
       
        public List<AppraisalKPIYears> KPIYears { get; set; }
    }

    public class AppraisalKPIYears : KPIYears
    {

    }
    public class AppraisalKPIsModel: KPIsModel
    {
        
        [StringLength(3000)]
        public string Accomplishments { get; set; }
        public string FormatedFormla { get; set; }

        [Required(ErrorMessageResourceType = typeof(AnnualAppraisal_Resources), ErrorMessageResourceName = "SelectRating")]
        public int Ratings { get; set; }
        public int CompanyID { get; set; }
        public decimal Score { get; set; }
    }
    public class AppraisalWeightagePercentageModel 
    {

        public string Section { get; set; }
        public string SectionDescriptiopn { get; set; }
        public string Section2 { get; set; }
        public string SectionDescriptiopn2 { get; set; }
        public string Weightag { get; set; }
       
    }
    public class AppraisalTeachingEmployeeModel : RequesterInfoViewModel
    {
        public AppraisalTeachingEmployeeModel()
        {
            AppraisalTeachingRatingScaleWeightageDetails = new List<AppraisalTeachingRatingScaleWeightageDetails>();
            AppraisalWeightagePercentageModel = new List<AppraisalWeightagePercentageModel>();
        }
        public int ID { get; set; }
        public int TeacherID { get; set; }
        public int TeacherAlternateID { get; set; }
        public int ReqStatusID { get; set; }
        public string TeacherName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string LessonTopic { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        [Display(Name = "Visit DATE")]
        public DateTime? VisitDATE { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
   
        public int AppraisalTeachingYearID { get; set; }

        public string AppraisalTeachingYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string Branch { get; set; }


        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string GradeYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string GroupSection { get; set; }       


        [Required(ErrorMessage = "Please insert a valid Visiting Time {0}")]
        [Display(Name = "Visit Time")]
        public string VisitTime { get; set; }


        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string LastYearScore { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public decimal ThisYearScore { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string ThisYearScoreDescription { get; set; }
        public List<AppraisalTeachingRatingScaleWeightageDetails> AppraisalTeachingRatingScaleWeightageDetails { get; set; }
        public List<AppraisalWeightagePercentageModel> AppraisalWeightagePercentageModel { get; set; }

    }
    public class AppraisalTeachingRatingScaleWeightageDetails 
    {
        public int RatingScaleID { get; set; }
        public string RatingScaleCharacter { get; set; }
        public int RatingScaleNumber { get; set; }
        public int FinalScoreSlabMIN { get; set; }
        public int FinalScoreSlabMAX { get; set; }

        public string DefinitionName { get; set; }
        public string DefinitionDetails { get; set; }
        public string DefinitionName2 { get; set; }
        public string DefinitionDetails2 { get; set; }
        public int IsActive { get; set; }

    }
    public class AppraisalTeachingProficiencyLevel
    {
        public int ProficiencyLevelID { get; set; }
        public int ProficiencyMasterID { get; set; }
        //public string ProficiencyDetail { get; set; }
        //public int ProficiencyGroupID { get; set; }
        //public string ProficiencyGroupName { get; set; }

        public string Element { get; set; }
        [Display(Name = "1")]
        public string Element1 { get; set; }
        [Display(Name = "2")]
        public string Element2 { get; set; }
        [Display(Name = "3")]
        public string Element3 { get; set; }
        [Display(Name = "4")]
        public string Element4 { get; set; }
        [Display(Name = "5")]
        public string Element5 { get; set; }

        public int ProficiencyScore { get; set; }
        public string ProficiencyFormula { get; set; }
        public int ProficiencyValue { get; set; }
        public string Comments { get; set; }
        public int IsActive { get; set; }


    }
    public class AppraisalTeachingRatingScaleWeightages
    {
        public int RatingScaleID { get; set; }

        public int RatingScaleNumber { get; set; }

    }

    public class TeachingAppraisalStatusReportModel
    {
        public string OrganizationName { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string IsLessonObservationCompleted { get; set; }
        public string DropInsCompleted { get; set; }
        public string IsAppraisalInitiated { get; set; }
        public int RequestId { get; set; }
        public string Year { get; set; }
        public string StartDate { get; set; }
        public string DueDate { get; set; }
        public string AppraisalSignOffDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
    }

}
