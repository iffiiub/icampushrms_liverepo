﻿using HRMS.Entities.Forms;
using HRMS.XMLResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.PDRP
{

    public class ObservationsModel
    {

        public ObservationsModel()
        {

            this.ObservationsEmployeeModel = new ObservationsEmployeeModel();
        }
        public string isAddMode { get; set; }

        //*** F6
        public string Comments { get; set; }
        
        public int FormProcessID { get; set; }

        //*** 1=Observation LM Open / after save
        //*** 2=Post-Observation
        public int FormMode { get; set; }

        public int PDRPObservationEvlID { get; set; }
        public int PerObservationID { get; set; }
        public int PostObservationID { get; set; }
        public int PostObservationsReqStatusID { get; set; }
        public string ProficiencyScoreFormula { get; set; }
        public int ProficiencyScoreTotal { get; set; }
        public decimal ProficiencyScoreOverall { get; set; }
        public bool EmployeeAcknowledged { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public int  IsOnTrackYearlyPlans { get; set; }
        
        public string IsOnTrackYearlyPlansNo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string ObjectivesList { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),
                  ErrorMessageResourceName = "Required")]
        public string ObservationDayLesson { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public int  IsTaughtToThisClass { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources), ErrorMessageResourceName = "Required")]
        public string SpecificItemsToWatch { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string ItemsToBeAware { get; set; }


        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string ParticularlyWellLesson  { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string FurtherImprovementLesson { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string ChildrensHighAbility { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string ChildrensAverageAbility { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string ChildrensBelowAverageAbility { get; set; }
        //*** F4
        public int EmpCurrentCompanyID { get; set; }


        public ObservationsEmployeeModel ObservationsEmployeeModel { get; set; }
        public List<ObservationsProficiencyLevel> ObservationsProficiencyLevel { get; set; }
        public List<ObservationsRatingScaleWeightages> ObservationsRatingScaleWeightages { get; set; }
        public BaseModel basemodel { get; set; }
    }
    public class ObservationsEmployeeModel : RequesterInfoViewModel
    {
        public int ID { get; set; }
        public int TeacherID { get; set; }
        public int TeacherAlternateID { get; set; }
        public int ReqStatusID { get; set; }
        public string TeacherName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string LessonTopic { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Display(Name = "Unit / Theme / LessonTopic")]
        public string UnitThemeLessonTopic  { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string Subject { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string Period { get; set; }


        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Display(Name = " Yearly Plan - Week Number:")]
        //[Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
        //               ErrorMessageResourceName = "Range_1")]
        public string YearPlan { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsOnRollMale { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsOnRollFemale { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        
        [Display(Name = "Visit Date:")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? VisitDATE { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Display(Name = "Drop In Year")]
        public int ObservationsYearID { get; set; }

        public string ObservationsYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string Branch { get; set; }


        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string GradeYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string GroupSection { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsPresentMale { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int StudentsPresentFemale { get; set; }


        [Required(ErrorMessageResourceType = typeof(Observations_Resources), ErrorMessageResourceName = "Validation12")]
        [Display(Name = "Visit Time")]
        public string VisitTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]        
        [Display(Name = "Learning Support:")]
        public int LearningSupport { get; set; }

        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int SENStudentsMale { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        [Range(1, 100, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_1")]
        public int SENStudentsFemale { get; set; }
        public bool Starter { get; set; }
        public bool TeacherInput { get; set; }
        public bool MainActivity { get; set; }
        public bool Plenary { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string OBComments { get; set; }
        [Required(ErrorMessageResourceType = typeof(Common_Resources),ErrorMessageResourceName = "Required")]
        public string ProgressEvidence { get; set; }
        public List<ObservationsRatingScaleWeightageDetails> ObservationsRatingScaleWeightageDetails { get; set; }

    }
    public class ObservationsRatingScaleWeightageDetails
    {
        public int RatingScaleID { get; set; }
        public string RatingScaleCharacter { get; set; }
        public int RatingScaleNumber { get; set; }
        public int FinalScoreSlabMIN { get; set; }
        public int FinalScoreSlabMAX { get; set; }

        public string DefinitionName { get; set; }
        public string DefinitionDetails { get; set; }
        public string DefinitionName2 { get; set; }
        public string DefinitionDetails2 { get; set; }
        public int IsActive { get; set; }

    }
    public class ObservationsProficiencyLevel
    {
        public int ProficiencyLevelID { get; set; }
        public int ProficiencyMasterID { get; set; }
        //public string ProficiencyDetail { get; set; }
        //public int ProficiencyGroupID { get; set; }
        //public string ProficiencyGroupName { get; set; }

        public string Element { get; set; }
        [Display(Name = "1")]
        public string Element1 { get; set; }
        [Display(Name = "2")]
        public string Element2 { get; set; }
        [Display(Name = "3")]
        public string Element3 { get; set; }
        [Display(Name = "4")]
        public string Element4 { get; set; }
        [Display(Name="5")]
        public string Element5 { get; set; }
        public string Element_2 { get; set; }
        [Display(Name = "1")]
        public string Element1_2 { get; set; }
        [Display(Name = "2")]
        public string Element2_2 { get; set; }
        [Display(Name = "3")]
        public string Element3_2 { get; set; }
        [Display(Name = "4")]
        public string Element4_2 { get; set; }
        [Display(Name = "5")]
        public string Element5_2 { get; set; }
        //*** F5
        [Required(ErrorMessageResourceType = typeof(Observations_Resources), ErrorMessageResourceName = "SelectRating")]
        public string ProficiencyScore { get; set; }
        public string ProficiencyFormula { get; set; }
        public int ProficiencyValue { get; set; }
        [StringLength(100, ErrorMessageResourceType = typeof(Observations_Resources), ErrorMessageResourceName = "ObsEvlComment")]
        public string Comments { get; set; }
        public int IsActive { get; set; }


    }
    public class ObservationsRatingScaleWeightages
    {
        public int RatingScaleID { get; set; }

        public int RatingScaleNumber { get; set; }

    }

    public class ObservationStatusReportModel
    {
        public string OrganizationName { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string IsObservationProcessInitiated { get; set; }
        public string Year { get; set; }
        public string TotalScore { get; set; }
        public string TotalRating { get; set; }
        public string ObservationVisitDate { get; set; }
        public string PreObservationRequest { get; set; }
        public string PostObservationRequest { get; set; }
        public string ObservationRequest { get; set; }
        public string ObservationSignOffDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
    }

}
