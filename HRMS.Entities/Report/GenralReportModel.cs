﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class CompanyEmployeeDetailReportingModel
    {
        public List<GeneralModel> GeneralModel { set; get; }
        public string Variable1 { set; get; }
        public string Variable2 { set; get; }
        public string Variable3 { set; get; }
        public string Variable4 { set; get; }
        public string Variable5 { set; get; }
        public string Variable6 { set; get; }
        public string Variable7 { set; get; }
        public string Variable8 { set; get; }
        public string Variable9 { set; get; }
        public string Variable10 { set; get; }
        public string Variable11 { set; get; }
    }

    public class GeneralModel : BaseModel
    {
        public string Value1 { set; get; }
        public string Value2 { set; get; }
        public string Value3 { set; get; }
        public string Value4 { set; get; }
        public string Value5 { set; get; }
        public string Value6 { set; get; }
        public string Value7 { set; get; }
        public string Value8 { set; get; }
        public string Value9 { set; get; }
        public string Value10 { set; get; }
        public string Value11 { set; get; }
        public string Value12 { set; get; }
        public string Value13 { set; get; }
        public string Value14 { set; get; }
        public string Value15 { set; get; }
    }

    public class FingerPrintMachineDataReportModel
    {
        public string EmployeeID { get; set; }
        public string FingerPrintMachineID { get; set; }
        public string EmployeeName { get; set; }
        public string Department { get; set; }
        public string ShiftName { get; set; }
    }

    public class MinistryModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeAlternatId { get; set; }
        public string EmployeeName { get; set; }
        public string Nationality { get; set; }
        public string Position { get; set; }
        public string HireDate { get; set; }
        public string LastQualification { get; set; }
        public string Specialization { get; set; }
        public string DocumentNo { get; set; }
        public string MobileNumber { get; set; }
    }

    public class EmployeeProfessionalDevelopmentReportModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string TrainingName { get; set; }
        public string SubjectName { get; set; }
        public string CountryName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string TotalHour { get; set; }
    }

    public class StaffQualificationReportModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string QualificationName { get; set; }
        public string SubjectName { get; set; }
        public string CountryName { get; set; }
        public string Date { get; set; }
        public string NationalityName { get; set; }
        public string CategoryName { get; set; }
    }

    public class StaffDetailsReportModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeAlternativeId { get; set; }
        public string EmployeeName { get; set; }
        public string QualificationName { get; set; }
        public string NumberOfEmployeeOneYear { get; set; }
        public string NumberOfEmployeeThreeYear { get; set; }
        public string LargestNationalityGroup { get; set; }
        public string YearAtCompany { get; set; }
        public string Nationality { get; set; }
        public string ArabicCount { get; set; }
        public string EmployeehavingAcademicCount { get; set; }

    }

    public class EmployeeFinalSettlementReport
    {
        public List<EmployeeDetailsReportModel> EmployeeDetailsList { get; set; }
        public List<EmployeeSalaryDetailsReportModel> EmployeeSalaryList { get; set; }
        public List<EmployeeGratuityDetailsReportModel> EmployeeGratList { get; set; }
        public List<EmployeeGratuityLeaveDetailsModel> EmployeeLeaveList { get; set; }
        public List<EmployeeGratuityOtherBenefitsModel> EmployeeOtherBenefitsList { get; set; }
        public List<EmployeeGratuityOtherDeductionModel> EmployeeOtherDeductionList { get; set; }
        public List<FinalSettlmentDue> EmployeeFinalSettlementDue { get; set; }
        public List<BankDetails> EmpBankDetails { get; set; }
    }

    public class EmployeeDetailsReportModel
    {
        public string EmployeeName { get; set; }
        public string JoiningDate { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Nationality { get; set; }
        public string Status { get; set; }
        public string ContractType { get; set; }
        public string LastWorkingDate { get; set; }
        public string LengthofService { get; set; }
        public string ReasonforExit { get; set; }
        public int EmployeeId { get; set; }

    }

    public class EmployeeSalaryDetailsReportModel
    {
        public int EmployeeId { get; set; }
        public string AllowanceName { get; set; }
        public string Amount { get; set; }
        public string EarnedDays { get; set; }
        public Boolean isTax { get; set; }
    }

    public class EmployeeGratuityDetailsReportModel
    {
        public int EmployeeId { get; set; }
        public string GratuityDays { get; set; }
        public string GrossGratuity { get; set; }
        public string AdvanceOnGratuity { get; set; }
        public string GratuityPriorToNE { get; set; }
        public string TotalGratuity { get; set; }
    }

    public class EmployeeGratuityLeaveDetailsModel
    {
        public int EmployeeId { get; set; }
        public string RemainingLeaves { get; set; }
        public string RemainingLeavesAmount { get; set; }
    }

    public class EmployeeGratuityOtherBenefitsModel
    {
        public int EmployeeId { get; set; }
        public string CompensatoryLeaveBalance { get; set; }
        public string Airfare { get; set; }
        public string Other { get; set; }
        public string TotalOtherBenefit { get; set; }
    }

    public class EmployeeGratuityOtherDeductionModel
    {
        public int EmployeeId { get; set; }
        public string OtherDeduction { get; set; }
        public string AdvanceDeduction { get; set; }
        public string TotalDeduction { get; set; }
    }

    public class FinalSettlmentDue
    {
        public int EmployeeId { get; set; }
        public string TotalSalary { get; set; }
        public string TotalGratuity { get; set; }
        public string AnnualLeaveBalance { get; set; }
        public string TotalOtherBenefits { get; set; }
        public string TotalDeduction { get; set; }
        public string TotalFinalSettlement { get; set; }
    }

    public class BankDetails
    {
        public int EmployeeId { get; set; }
        public String BankName { get; set; }
        public string BranchName { get; set; }
        public string UniAccountNumber { get; set; }
        public string EmployeeName { get; set; }
    }

    public class EmployeeLeaveRegister
    {
        public int EmployeeId { get; set; }
        public string AutoId { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string ElLeaveFrom { get; set; }
        public string ElLeaveTo { get; set; }
        public string ALeaveFrom { get; set; }
        public string ALeaveTo { get; set; }
        public decimal ALeaveDay { get; set; }
        public decimal TLeaveDay { get; set; }
        public string LeaveType { get; set; }
        public string IsCompensate { get; set; }
        public string ExpectedRejoin { get; set; }
        public string RejoinOn { get; set; }
    }

    public class PayrollVerificationReport
    {
        public int EmployeeId { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string FullName { get; set; }
        public string PositionName { get; set; }
        public string IsActive { set; get; }
    }

    public class EmployeeMinistryDetail
    {
        public int EmployeeId { get; set; }
        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string QualificationEn { get; set; }
        public string QualificationAr { get; set; }
        public string SpecializationEn { get; set; }
        public string SpecializationAr { get; set; }
        public string UniversityEn { get; set; }
        public string UniversityAr { get; set; }
        public string JobCategoryEn { get; set; }
        public string JobCategoryAr { get; set; }
        public string JobTitleEn { get; set; }
        public string JobTitleAr { get; set; }
        public string DepartmentEn { get; set; }
        public string DepartmentAr { get; set; }
        public string DOB { get; set; }
        public string BirthPlaceEn { get; set; }
        public string BirthPlaceAr { get; set; }
        public string CiviliNo { get; set; }
        public string SocialStatusEn { get; set; }
        public string SocialStatusAr { get; set; }
        public string DOJ { get; set; }
        public string Experience { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string MOETitle_1 { get; set; }
        public string MOETitle_2 { get; set; }

    }

    public class EmployeeTestimoanl
    {
        public int EmployeeId { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string PassportNo { get; set; }
        public string EmpPosEn { get; set; }
        public string EmpPosAr { get; set; }
        public string PositionAr { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public string GenderTemplate1AR { get; set; }
        public string GenderTemplate2AR { get; set; }
        public string GenderTemplate3AR { get; set; }
        public string JobTitleEn { get; set; }
        public string JobTitleAr { get; set; }
        public string InstitutionText { get; set; }
        public string PassportText { get; set; }     
    }

    public class StaffRenewalData
    {
        public int EmployeeId { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string PassportNo { get; set; }
        public string EmpjobCategoryEn { get; set; }
        public string EmpjobCategoryAr { get; set; }
        public string BasicSalary { get; set; }
        public string Allowances { get; set; }
        public string TotalSalary { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public string JobTitleEn { get; set; }
        public string JobTitleAr { get; set; }
        public string GenderTemplateEn { get; set; }
        public string GenderTemplateAr { get; set; }
        public string JoiningDate { get; set; }
        public string EndgDate { get; set; }
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }
        public decimal AccomodationHistoryAmount { get; set; }
        public bool Furnished { get; set; }
        public decimal FurnishedAmount { get; set; }
        public bool Electricity { get; set; }
        public decimal ElectricityAmount { get; set; }
        public bool Water { get; set; }
        public decimal WaterAmount { get; set; }
        public decimal AirTicketAmount { get; set; }
        public decimal AirTicketDependantAmount { get; set; }
        public string EnAllowanceData { get; set; }
        public string ArAllowanceData { get; set; }
        public string GenderTemplate1Ar { get; set; }
        public string GenderTemplate2Ar { get; set; }
        public string GenderTemplate3Ar { get; set; }
        public string GenderTemplate4Ar { get; set; }
        public string GenderTemplate5Ar { get; set; }
        public string NoteAr { get; set; }
        public string NoteEn { get; set; }
    }

    public class EmployeeClarificationData
    {
        public int EmployeeId { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }
        public string EnAllowanceData { get; set; }
        public string ArAllowanceData { get; set; }
        public string TodayDate { get; set; }

    }

    public class EmployeeServiceCertificateData
    {
        public int EmployeeId { get; set; }
        public int EmployeeAlternativeID { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }      
        public string JoiningDate { get; set; }
        public string EndgDate { get; set; }
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }
        public string PayDocumentNumber { get; set; }
        public string EnAllowanceData { get; set; }
        public string ArAllowanceData { get; set; }

    }

    public class EmployeeSchoolJoiningDateData
    {
        public int EmployeeId { get; set; }
        public int EmployeeAlternativeID { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public string JoiningDate { get; set; }
        public string EndgDate { get; set; }
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }       
        public string EnAllowanceData { get; set; }
        public string ArAllowanceData { get; set; }

    }
    public class EmployeePermissionData
    {
        public int EmployeeId { get; set; }
        public int EmployeeAlternativeID { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }      
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }      
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }      
        public string EnAllowanceData { get; set; }
        public string ArAllowanceData { get; set; }

    }
    public class EmployeeAgreementData
    {
        public int EmployeeId { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string PassportNo { get; set; }
        public string EmpjobCategoryEn { get; set; }
        public string EmpjobCategoryAr { get; set; }
        public string BasicSalary { get; set; }
        public string Allowances { get; set; }
        public string TotalSalary { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public string JobTitleEn { get; set; }
        public string JobTitleAr { get; set; }
        public string GenderTemplateEn { get; set; }
        public string GenderTemplateAr { get; set; }
        public string JoiningDate { get; set; }
        public string EndgDate { get; set; }
        public string EmployeePositionEn { get; set; }
        public string EmployeePositionAr { get; set; }
        public string AgreementDate { get; set; }
        public string WeekdayEn { get; set; }
        public string WeekdayAr { get; set; }
        public string ContractStartDate { get; set; }
        public string ContractEndDate { get; set; }
        public string YearsofContract { get; set; }
        public string PaidLeaves { get; set; }
        public string ElectricityWaterAmount { get; set; }
        public string TransportationAmount { get; set; }      
        public string AirTicketAmount { get; set; }
        public string AirTicketDependantAmount { get; set; }
        public string AirTicketAmountAR { get; set; }
        public string InsuranceType { get; set; }
        public string InsuranceTypeAR { get; set; }
        public string Housing { get; set; }
        public string HousingAR { get; set; }
    }

    public class EmployeeSalaryTransferCertificate
    {
        public string EmployeeId { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public string PassportNo { get; set; }
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }
        public string BasicSalary { get; set; }
        public string Allowances { get; set; }
        public string OtherAllowances { get; set; }
        public string CurrencyFormat { get; set; }
        public string TotalSalary { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public string GenderTemplateEn { get; set; }
        public string GenderTemplateAr { get; set; }
        public string GenderTemplate1Ar { get; set; }
        public string GenderTemplate2Ar { get; set; }
        public string GenderTemplate3Ar { get; set; }
        public string GenderTemplate4Ar { get; set; }
        public string BankNameEN { get; set; }
        public string BankNameAR { get; set; }
        public string BanchNameEN { get; set; }
        public string BanchNameAR { get; set; }
        public string JoiningDate { get; set; }
        public string AccountNumber { get; set; }
    }
    
    public class EmployeeExcuseDetail
    {
        public string EmpId { get; set; }
        public string EmployeeName { get; set; }
        public string NoExAbsence { get; set; }
        public string NoNonExAbsence { get; set; }
        public string NoExLate { get; set; }
        public string NoNonExLate { get; set; }
        public string NoExEarly { get; set; }
        public string NoNonExEarly { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
    }
  
    public class EmployeeAbsentAndLateData
    {
        public int EmployeeAlternativeID { get; set; }
        public int EmployeeId { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }
        public string ShiftName { get; set; }
        public string Department { get; set; }
        public string Section { get; set; }
        public string PositionTitleEn { get; set; }
        public string PositionTitleAr { get; set; }
        public string JobCategory { get; set; }
        public string AuthSignEn { get; set; }
        public string AuthSignAr { get; set; }
        public int AbsentCount { get; set; }
        public int LateCount { get; set; }       
        public string AbsentAndLateDate { get; set; }
        public string Status { get; set; }
        public string LeaveType { get; set; }
        public string Deduct { get; set; }
        public string IsPaid { get; set; }
        public string Note { get; set; }
        public string EnAllowanceData { get; set; }
        public string ArAllowanceData { get; set; }

    }

    public class JordanIncomeTaxData
    {
        public int EmployeeId { get; set; }
        public int EmployeeAlternativeID { get; set; }
        public string EmpNameEn { get; set; }
        public string EmpNameAr { get; set; }        
        public string EmployeeSalary { get; set; }
        public string YearlySalary { get; set; }
        public string ExemptionYearlySalary { get; set; }
        public string OtherExemptions { get; set; }
        public string TaxableSalary { get; set; }
        public string Level1_Amount { get; set; }
        public string Level2_Amount { get; set; }
        public string Level3_Amount { get; set; }
        public string TotalTax { get; set; }
        public string TaxPerMonth { get; set; }
        public string Notes { get; set; }


    }
}
