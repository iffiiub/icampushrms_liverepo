﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
   public class AdditionReportModel
    {
        public string employeeId { get; set; }
        public string employeeName { get; set; }
        public decimal amount { get; set; }
        public string formDate { get; set; }
        public string toDate { get; set; }
        public string cycleOfsalary { get; set; }
        public string date { get; set; }
        public int installment { get; set; }
        public string academicOrReferance { get; set; }
        public string academicYear { get; set; }
        public string reason { get; set; }
        public string deductionType { get; set; }
        public string additionType { get; set; }
    }
}
