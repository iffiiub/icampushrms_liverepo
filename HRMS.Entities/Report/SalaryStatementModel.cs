﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class SalaryStatementModel
    {
        public SalaryStructureEmployeeInfo employeeIfo { get; set; }
        public List<PayrollInfo> payrollInfo { get; set; }
        public List<SalaryInfo> salaryInfo { get; set; }
        public LabelInfo labelinfo { get; set; }
        public List<CRDR> crdr { get; set; }
        public List<PaymentLabel> paymentlabel { get; set; }
        public List<PaytransferAccommodationInfo> paytransferInfo { get; set; }
        public List<PaytransferAccommodationInfo> accommodationInfo { get; set; }

        public List<Insurance> Insurance { get; set; }

        public List<CurrentAccomodationInfo> CurrentAccomodationInfo { get; set; }
    }

    public class SalaryStructureEmployeeInfo
    {
        public string schoolName { get; set; }
        public string campusName { get; set; }
        public string employeId { get; set; }
        public string employeName { get; set; }
        public string inductionDate { get; set; }
        public string position { get; set; }
        public string department { get; set; }
        public string bankName { get; set; }
        public string accNo { get; set; }
        public string Accomodation { get; set; }
        public string basicSalary { get; set; }
        public string costOfLiving { get; set; }
        public string insurenceAllownce { get; set; }
        public string transportAllownce { get; set; }
        public string Accomodation2 { get; set; }
        public string FurnishedAmount { get; set; }
        public string electricity { get; set; }
        public string water { get; set; }
        public string overTime { get; set; }
        public string total { get; set; }

    }

    public class PayrollInfo
    {
        public string cycle { get; set; }
        public string amount { get; set; }
        public string month { get; set; }
        public string addition { get; set; }
        public string deduction { get; set; }
        public string net { get; set; }
    }

    public class SalaryInfo
    {
        public string paySalaryAllowance { get; set; }
        public string amount { get; set; }
    }

    public class LabelInfo
    {
        public string amount { get; set; }
        public string FurnishedAmount { get; set; }
        public string ElectricityAmount { get; set; }
        public string WaterAmount { get; set; }
        public string Accommodationpaid { get; set; }
        public string Furpaid { get; set; }

    }

    public class CRDR
    {
        public string no { get; set; }
        public string description { get; set; }
        public string date { get; set; }
        public string amount { get; set; }
        public string year { get; set; }
    }

    public class PaymentLabel
    {
        public string cheque_date { get; set; }
        public string description { get; set; }
        public string PVNo { get; set; }
        public string DR_Amount { get; set; }
        public string cheque_no { get; set; }
    }

    public class PaytransferAccommodationInfo
    {
        public string PaySundriesDate { get; set; }
        public string description { get; set; }
        public string BankName_1 { get; set; }
        public string Amount { get; set; }
        public string AccountNumber { get; set; }
        public string PaySundriesID { get; set; }

    }

    public class Insurance
    {
        public string PolicyNumber { get; set; }
        public string InsuranceCardNumber { set; get; }
        public string IssueDate { set; get; }
        public string ExpiryDate { get; set; }
        public string DeductionAmount { set; get; }
        public string CancellationDate { get; set; }
        public string Amount { get; set; }
        public string AmountPaidBySchool { get; set; }
        public string Note { set; get; }
        public string InsuranceType { set; get; }
    }

    public class CurrentAccomodationInfo
    {
        public string Accomodation { get; set; }
        public string Furniture { get; set; }
        public string Electricity { get; set; }
        public string Water { get; set; }
        public string AccRemBalance { get; set; }
        public string FurRemBalance { get; set; }
        public string ElecRemBalance { get; set; }
        public string WaterRemBalance { get; set; }
        public string TotalAccomodationCost { get; set; }
    }

    public class SalaryReport
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string SalaryAllowances { get; set; }
        public Decimal Amount { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string DepartmentName { get; set; }
        public string Position { get; set; }

    }


    public class SectionList
    {
        public string text { get; set; }
        public int id { get; set; }
        public string dataFeild1 { get; set; }
        public string dataFeild2 { get; set; }
        public string selectedValue { get; set; }
    }
}
