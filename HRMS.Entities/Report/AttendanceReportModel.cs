﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AttendanceReportModel
    {
        public string DepID { set; get; }
        public string DepName { set; get; }
        public string DepHead { set; get; }
        public string DepBranch { set; get; }
        public string Section { set; get; }
        public string SectionHead { set; get; }
        public string Shift { set; get; }
        public string EmpID { set; get; }
        public int EmployeeID { set; get; }
        public int SuperVisiorId { set; get; }
        public string EmpName { set; get; }
        public string TotalDays { set; get; }
        public string WorkingDays { set; get; }
        public string WorksDays { set; get; }
        public string PresentDays { set; get; }
        public string AbsentDays { set; get; }
        public string AExDays { set; get; }
        public string ANonExDays { set; get; }
        public string LateinDays { set; get; }
        public string LExDays { set; get; }
        public string LNonExDays { set; get; }
        public string EarlyoutDays { set; get; }
        public string EExDays { set; get; }
        public string ENonExDays { set; get; }
        public string HolidayDays { set; get; }
        public string VacationDays { set; get; }
        public string OffDays { set; get; }
        public string ExtraDays { set; get; }
        public string SatPresent { set; get; }
        public string VacationId { set; get; }
        public string HolidayId { set; get; }
        public string LateMinAfter { set; get; }
        public string EarlyMinAfter { set; get; }
        public string ShiftWorkingHours { set; get; }
        public string WorkHours { set; get; }
        public string OverHours { set; get; }
        public string ShortHours { set; get; }
        public string ExShortHours { set; get; }
        public string NonExShortHours { set; get; }

        public string LateInMin { get; set; }
        public string ExLateInMin { get; set; }
        public string NonExLateInMin { get; set; }
        public string EarlyOutMin { get; set; }
        public string ExEarlyMin { get; set; }
        public string NonExEarlyMin { get; set; }

        public string ShiftInTimes { get; set; }
        public string ShiftOutTimes { get; set; }
        public string InTimes { get; set; }
        public string OutTimes { get; set; }
        public string status { get; set; }
        public string statusId { get; set; }
        public string Date { get; set; }
        public DateTime TDate { get; set; }
        public string TotalHours { set; get; }
        public string Ex { set; get; }
        public string NonEx { set; get; }
        public string OutEx { set; get; }
        public string OutNonEx { set; get; }
        public decimal AttendancePercent { set; get; }
        public string Remark { set; get; }
        public string GraceTime { set; get; }
        public string PayAbsentTypeName { set; get; }
        public string AttDate { get; set; }
        #region Monthly Summary Data

        public string StatusId1 { set; get; }
        public string LateExcused1 { set; get; }
        public string EarlyExcused1 { set; get; }
        public string AbsentExcused1 { set; get; }
        public string DayDate1 { set; get; }

        public string StatusId2 { set; get; }
        public string LateExcused2 { set; get; }
        public string EarlyExcused2 { set; get; }
        public string AbsentExcused2 { set; get; }
        public string DayDate2 { set; get; }

        public string StatusId3 { set; get; }
        public string LateExcused3 { set; get; }
        public string EarlyExcused3 { set; get; }
        public string AbsentExcused3 { set; get; }
        public string DayDate3 { set; get; }

        public string StatusId4 { set; get; }
        public string LateExcused4 { set; get; }
        public string EarlyExcused4 { set; get; }
        public string AbsentExcused4 { set; get; }
        public string DayDate4 { set; get; }

        public string StatusId5 { set; get; }
        public string LateExcused5 { set; get; }
        public string EarlyExcused5 { set; get; }
        public string AbsentExcused5 { set; get; }
        public string DayDate5 { set; get; }

        public string StatusId6 { set; get; }
        public string LateExcused6 { set; get; }
        public string EarlyExcused6 { set; get; }
        public string AbsentExcused6 { set; get; }
        public string DayDate6 { set; get; }

        public string StatusId7 { set; get; }
        public string LateExcused7 { set; get; }
        public string EarlyExcused7 { set; get; }
        public string AbsentExcused7 { set; get; }
        public string DayDate7 { set; get; }

        public string StatusId8 { set; get; }
        public string LateExcused8 { set; get; }
        public string EarlyExcused8 { set; get; }
        public string AbsentExcused8 { set; get; }
        public string DayDate8 { set; get; }

        public string StatusId9 { set; get; }
        public string LateExcused9 { set; get; }
        public string EarlyExcused9 { set; get; }
        public string AbsentExcused9 { set; get; }
        public string DayDate9 { set; get; }

        public string StatusId10 { set; get; }
        public string LateExcused10 { set; get; }
        public string EarlyExcused10 { set; get; }
        public string AbsentExcused10 { set; get; }
        public string DayDate10 { set; get; }

        public string StatusId11 { set; get; }
        public string LateExcused11 { set; get; }
        public string EarlyExcused11 { set; get; }
        public string AbsentExcused11 { set; get; }
        public string DayDate11 { set; get; }

        public string StatusId12 { set; get; }
        public string LateExcused12 { set; get; }
        public string EarlyExcused12 { set; get; }
        public string AbsentExcused12 { set; get; }
        public string DayDate12 { set; get; }

        public string StatusId13 { set; get; }
        public string LateExcused13 { set; get; }
        public string EarlyExcused13 { set; get; }
        public string AbsentExcused13 { set; get; }
        public string DayDate13 { set; get; }

        public string StatusId14 { set; get; }
        public string LateExcused14 { set; get; }
        public string EarlyExcused14 { set; get; }
        public string AbsentExcused14 { set; get; }
        public string DayDate14 { set; get; }

        public string StatusId15 { set; get; }
        public string LateExcused15 { set; get; }
        public string EarlyExcused15 { set; get; }
        public string AbsentExcused15 { set; get; }
        public string DayDate15 { set; get; }

        public string StatusId16 { set; get; }
        public string LateExcused16 { set; get; }
        public string EarlyExcused16 { set; get; }
        public string AbsentExcused16 { set; get; }
        public string DayDate16 { set; get; }

        public string StatusId17 { set; get; }
        public string LateExcused17 { set; get; }
        public string EarlyExcused17 { set; get; }
        public string AbsentExcused17 { set; get; }
        public string DayDate17 { set; get; }

        public string StatusId18 { set; get; }
        public string LateExcused18 { set; get; }
        public string EarlyExcused18 { set; get; }
        public string AbsentExcused18 { set; get; }
        public string DayDate18 { set; get; }

        public string StatusId19 { set; get; }
        public string LateExcused19 { set; get; }
        public string EarlyExcused19 { set; get; }
        public string AbsentExcused19 { set; get; }
        public string DayDate19 { set; get; }

        public string StatusId20 { set; get; }
        public string LateExcused20 { set; get; }
        public string EarlyExcused20 { set; get; }
        public string AbsentExcused20 { set; get; }
        public string DayDate20 { set; get; }

        public string StatusId21 { set; get; }
        public string LateExcused21 { set; get; }
        public string EarlyExcused21 { set; get; }
        public string AbsentExcused21 { set; get; }
        public string DayDate21 { set; get; }

        public string StatusId22 { set; get; }
        public string LateExcused22 { set; get; }
        public string EarlyExcused22 { set; get; }
        public string AbsentExcused22 { set; get; }
        public string DayDate22 { set; get; }

        public string StatusId23 { set; get; }
        public string LateExcused23 { set; get; }
        public string EarlyExcused23 { set; get; }
        public string AbsentExcused23 { set; get; }
        public string DayDate23 { set; get; }

        public string StatusId24 { set; get; }
        public string LateExcused24 { set; get; }
        public string EarlyExcused24 { set; get; }
        public string AbsentExcused24 { set; get; }
        public string DayDate24 { set; get; }

        public string StatusId25 { set; get; }
        public string LateExcused25 { set; get; }
        public string EarlyExcused25 { set; get; }
        public string AbsentExcused25 { set; get; }
        public string DayDate25 { set; get; }

        public string StatusId26 { set; get; }
        public string LateExcused26 { set; get; }
        public string EarlyExcused26 { set; get; }
        public string AbsentExcused26 { set; get; }
        public string DayDate26 { set; get; }

        public string StatusId27 { set; get; }
        public string LateExcused27 { set; get; }
        public string EarlyExcused27 { set; get; }
        public string AbsentExcused27 { set; get; }
        public string DayDate27 { set; get; }

        public string StatusId28 { set; get; }
        public string LateExcused28 { set; get; }
        public string EarlyExcused28 { set; get; }
        public string AbsentExcused28 { set; get; }
        public string DayDate28 { set; get; }

        public string StatusId29 { set; get; }
        public string LateExcused29 { set; get; }
        public string EarlyExcused29 { set; get; }
        public string AbsentExcused29 { set; get; }
        public string DayDate29 { set; get; }

        public string StatusId30 { set; get; }
        public string LateExcused30 { set; get; }
        public string EarlyExcused30 { set; get; }
        public string AbsentExcused30 { set; get; }
        public string DayDate30 { set; get; }

        public string StatusId31 { set; get; }
        public string LateExcused31 { set; get; }
        public string EarlyExcused31 { set; get; }
        public string AbsentExcused31 { set; get; }
        public string DayDate31 { set; get; }

        #endregion
    }
    public enum enumMonths
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }
}
