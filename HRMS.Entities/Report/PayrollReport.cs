﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Report
{
    public class PayrollReportDetails
    {
        public string EmployeeAlternativeId { get; set; }
        public string EmployeeName { get; set; }
        public string PayType { get; set; }
        public string TypeName { get; set; }
        public string TransactionDate { get; set; }
        public string Amount { get; set; }
    }

    public class WPSMonthlySalaryReport
    {
        public int CompanyId { get; set; }
        public string OrganizationCode { get; set; }
        public int EmployeeCount { get; set; }
        public decimal TotalAmount { get; set; }
        public string FileName { get; set; }
    }
}
