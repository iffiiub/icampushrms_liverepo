﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class GratuityReportModel
    {
        public int SrNo { get; set; }
        public string EmpID { get; set; }
        public string EmployeeAlternativeId { get; set; }
        public int IntEmployeeAlternativeId { get; set; }
        public string EmpName { get; set; }
        public string Position { get; set; }
        public string HireDate { get; set; }
        public string GratuityDate { get; set; }

        public string Amount { get; set; }

        public string AmountPerday { get; set; }

        public string ServicePeriod { get; set; }

        public string ContractTerms { get; set; }

        public string EOS { get; set; }
        public string EOSType { get; set; }

        public string ServicePeriodDays { get; set; }

        public string DepartmentName { get; set; }

        public string DeptID { get; set; }

        public string Grade { get; set; }

        public string Nationality { get; set; }

        public string Payroll { get; set; }
        public string Currency { get; set; }

        public string BasicSalary { get; set; }

        public string TotalSalary { get; set; }

        public string GratuityDays { get; set; }

        public string GratuityBeforeJuly1980 { get; set; }

        public string GratuityAlreadyPaid { get; set; }

        public string AnnualLeaveBalance { get; set; }
        public string CompensatoryLeaveBalance { get; set; }

        public string ESOtherPayments { get; set; }

        public string ESAirfare { get; set; }
        public string ESOtherDeductions { get; set; }
        public string TotalDeductions { get; set; }

        public decimal SettlementAmount { get; set; }
        public string LWPDuringService { get; set; }

        public string ESAnnualLeaveEncashment { get; set; }

        public decimal TotalEarnings { get; set; }

        public string AccountNumber { get; set; }

        public string UniAccountNumber { get; set; }

        public string BankName { get; set; }

        public string BranchName { get; set; }

        public string PaybleAmount { get; set; }
    }
}
