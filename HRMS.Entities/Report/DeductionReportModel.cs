﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class DeductionReportModel
    {
        public int EmployeeID { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Days { get; set; }
        public string TotalSalary { get; set; }
        public decimal TotalSalaryDecimal { get; set; }
        public string Amount { get; set; }
        public decimal AmountInDecimal { get; set; }
        public string FormDate { get; set; }
        public string ToDate { get; set; }
        public string EffectiveDate { get; set; }
        public string Confirmed { get; set; }
        public string ConfirmedDate { get; set; }
        public string Generated { get; set; }
        public string ExcuseType { get; set; }
        public string TotalLateEarlyOutMin { get; set; }
        public string cycleOfsalary { get; set; }
        public string date { get; set; }
        public int installment { get; set; }
        public string academicOrReferance { get; set; }
        public string academicYear { get; set; }
        public string reason { get; set; }
        public string deductionType { get; set; }
        public int LateInMinutes { get; set; }
        public int EarlyOutMinues { get; set; }
        public string ConfirmedDateLateIn { get; set; }
        public string ConfirmedDateEarlyOut { get; set; }
        public string GeneratedDateLateIn { get; set; }
        public string GeneratedDateEarlyOut { get; set; }
        public string AttendanceDate { get; set; }
        public DateTime AttendanceDateTime { get; set; }
        public decimal EarlyDedcutionAmount { get; set; }
        public decimal LateDedcutionAmount { get; set; }
        public string Status { get; set; }
    }

}
