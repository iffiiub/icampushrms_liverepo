﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Report
{
    public class EmployeeReport
    {
        public string EmpoyeeName { get; set; }
        public string SectionName { get; set; }
        public int SectionId { get; set; }
        public int EmployeeCount { get; set; }
    }
}
