﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Report
{
    public class ReportTemplate
    {
        public int ReportTemplateID { get; set; }
        public string ReportTemplateName { get; set; }
        public bool IsHeaderRedundancy { get; set; }
        public bool IsFooterRedundancy { get; set; }
        public bool IsHeaderVisibile { get; set; }
        public bool IsFooterVisible { get; set; }
        public bool IsHeaderLogoVisible { get; set; }
        public bool IsSchoolNameCampusVisible { get; set; }
        public int SchoolNameCampusFontId { get; set; }
        public string SchoolNameCampusFontName { get; set; }
        public short SchoolNameCampusFontSize { get; set; }
        public bool IsSchoolNameCampusFontBold { get; set; }
        public string SchoolSlogan { get; set; }

        public string SchoolSlogonFontName { get; set; }
        public bool IsSchoolSlogonVisible { get; set; }
        public int SchoolSlogonFontId { get; set; }
        public short SchoolSlogonFontSize { get; set; }
        public bool IsSchoolSlogonFontBold { get; set; }
        public string HeaderCustomMessage { get; set; }
        public bool IsHeaderCustomMessageVisible { get; set; }
        public string HeaderCustomMessageFontName { get; set; }
        public int HeaderCustomMessageFontId { get; set; }
        public short HeaderCustomMessageFontSize { get; set; }
        public bool IsHeaderCustomMessageFontBold { get; set; }
        public string FooterCustomMessage { get; set; }
        public bool IsFooterCustomMessageVisible { get; set; }
        public int FooterCustomMessageFontId { get; set; }
        public string FooterCustomMessageFontName { get; set; }
        public short FooterCustomMessageFontSize { get; set; }
        public bool IsFooterCustomMessageFontBold { get; set; }
        public bool IsReportNameCustomVisible { get; set; }
        public int ReportNameCustomFontId { get; set; }
        public string ReportNameCustomFontName { get; set; }
        public short ReportNameCustomFontSize { get; set; }
        public bool IsReportNameCustomFontBold { get; set; }
        public bool IsDateVisible { get; set; }
        public bool IsTimeVisible { get; set; }
        public bool IsPageNumberVisible { get; set; }
        public bool IsFooterSchoolContactVisible { get; set; }
        public int FooterSchoolContactFontId { get; set; }
        public string FooterSchoolContactFontName { get; set; }
        public short FooterSchoolContactFontSize { get; set; }
        public bool IsFooterSchoolContactFontBold { get; set; }
        public bool IsHeaderSchoolContactVisible { get; set; }
        public int HeaderSchoolContactFontId { get; set; }
        public string HeaderSchoolContactFontName { get; set; }
        public short HeaderSchoolContactFontSize { get; set; }
        public bool IsHeaderSchoolContactFontBold { get; set; }
        public bool IsFooterLogoVisible { get; set; }
        public string SignatureLabel { get; set; }
        public short KeyPositionID { get; set; }
        public bool IsSignatureVisible { get; set; }
        public bool IsSignatureLabelVisible { get; set; }
        public int SignatureLabelFontId { get; set; }
        public string SignatureLabelFontName { get; set; }
        public short SignatureLabelFontSize { get; set; }
        public bool IsSignatureLabelFontBold { get; set; }
        public bool IsUserPrintedVisible { get; set; }
        public bool IsHeaderTelephone1Visible { get; set; }
        public bool IsHeaderTelephone2Visible { get; set; }
        public bool IsHeaderFaxVisible { get; set; }
        public bool IsHeaderPoBoxVisible { get; set; }
        public bool IsHeaderEmailVisible { get; set; }
        public bool IsHeaderWebSiteVisible { get; set; }
        public bool IsFooterTelephone1Visible { get; set; }
        public bool isFooterTelephone2Visible { get; set; }
        public bool IsFooterFaxVisible { get; set; }
        public bool IsFooterPoBoxVisible { get; set; }
        public bool IsFooterEmailVisible { get; set; }
        public bool IsFooterWebSiteVisible { get; set; }
        public string HeaderContact { get; set; }
        public string FooterContact { get; set; }
        public short HeaderPhoneSequence { get; set; }
        public short HeaderFaxSequence { get; set; }
        public short HeaderPoBoxSequence { get; set; }
        public short HeaderEmailSequence { get; set; }
        public short HeaderWebsiteSequence { get; set; }
        public short FooterPhoneSequence { get; set; }
        public short FooterFaxSequence { get; set; }
        public short FooterPoBoxSequence { get; set; }
        public short FooterEmailSequence { get; set; }
        public short FooterWebsiteSequence { get; set; }
        public string Actions { get; set; }

    }
}
