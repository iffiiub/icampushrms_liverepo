﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Report
{
    public class AccommodationModel
    {
        public string Category { get; set; }
        public string AccommodationType { get; set; }
        public string AccommodationYear { get; set; }
        public string EmployeeAlternativeId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Type { get; set; }
        public string City { get; set; }
        public string ApartmentNo { get; set; }
        public decimal Amount { get; set; }
        public decimal Furniture { get; set; }
        public decimal Electricity { get; set; }
        public decimal Water { get; set; }
        public decimal AccommodationAmount { get; set; }

    }
}
