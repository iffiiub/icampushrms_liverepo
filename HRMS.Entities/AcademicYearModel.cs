﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AcademicYearModel
    {
        public int AcademicYearId { get; set; }
        public string Duration { get; set; }
        public bool IsActive { get; set; }
        public int Year { get; set; }
        public string BeginingInstructionDate { get; set; }
        public string EndingInstructionDate { get; set; }
        public bool IsNextYear { get; set; }
    }
}
