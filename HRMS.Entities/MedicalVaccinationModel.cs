﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class MedicalVaccinationModel : BaseModel
    {
        public int EmployeeVaccinationID { set; get; }
        
        public int EmployeeID { set; get; }
       
        public string EmployeeVaccinationDate { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int Vaccination { set; get; }
        public string VaccinationName { set; get; }
       public bool Administrated { set; get; }

        public string Note { set; get; }
        
        public int aTntTranMode { set; get; }

        	 




    }
}
