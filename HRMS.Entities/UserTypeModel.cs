﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class UserTypeModel
    {
        public int UserTypeID { set; get; }
        public string UserTypeName { get; set; }
        public string HomePageUrl { get; set; }
        public int ModuleID { get; set; }
        public bool IsActive { get; set; }
        public string UserTypeNameSYS { get; set; }
        public int CompanyId { get; set; }
        public int CreatedBy { set; get; }
        public string CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
      }
}
