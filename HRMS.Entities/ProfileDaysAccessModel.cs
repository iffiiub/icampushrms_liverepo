﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
   public class ProfileDaysAccessModel
    {

       public int ProfileId { get; set; }
       public int No_Of_Days { get; set; }

       public int CreatedBy { get; set; }
       public DateTime CreatedOn { get; set; }
       public int ModifiedBy { get; set; }
       public DateTime ModifiedOn { get; set; }


    }
}
