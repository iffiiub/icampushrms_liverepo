﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class StatusModel : BaseModel
    {
        public int StatusID { set; get; }
        public string StatusName { set; get; }
        public string Description { set; get; }
    }
}
