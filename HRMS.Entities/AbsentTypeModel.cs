﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AbsentTypeModel : BaseModel
    {
        public int PayAbsentTypeID { set; get; }

        [Required(ErrorMessage = "Please Enter Absent Type Name (EN)")]
        public string PayAbsentTypeName_1 { get; set; }
        public string PayAbsentTypeName_2 { set; get; }
        public string PayAbsentTypeName_3 { set; get; }
        public bool ISDeductable { set; get; }
        public int PaidType { set; get; }
        public bool AnnualLeave { set; get; }
        public string PaidTypeName { set; get; }
        public bool DeductAdjacentOffdays { get; set; }
        public bool DeductEnClosedOffdays { get; set; }
        public bool DeductAdjAcentHoliday { get; set; }

    }
}
