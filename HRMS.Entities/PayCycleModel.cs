﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayCycleModel
    {

        public int PayCycleID { get; set; }

        public string PayCycleName_1 { get; set; }

        public string PayCycleName_2 { get; set; }

        public string PayCycleName_3 { get; set; }

        public string DateFrom { get; set; }

        public string DateTo { get; set; }

        public bool active { get; set; }

        public int acyear { get; set; }

    }
}
