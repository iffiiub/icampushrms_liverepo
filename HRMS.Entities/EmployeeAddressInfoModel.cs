﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeAddressInfoModel
    {
        public int AddressInfoId { get; set; }

        public int EmployeeID { get; set; }

        [Display(Name = "Address Name:")]
        public string AddressName { get; set; }

        [Display(Name = "Address Type:")]
        public int AddressType { get; set; }


        public string AddressTypeName { get; set; }

        [Display(Name = "Address Line 1 (EN):", GroupName = "Logger")]
        public string AddressLine { get; set; }

        [Display(Name = "Address Line 2 (EN):", GroupName = "Logger")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Address Line 1 (AR):", GroupName = "Logger")]
        public string AddressLine1_2 { get; set; }

        [Display(Name = "Address Line 2 (AR):", GroupName = "Logger")]
        public string AddressLine2_2 { get; set; }

        [Display(Name = "Address Line 1 (FR):", GroupName = "Logger")]
        public string AddressLine1_3 { get; set; }

        [Display(Name = "Address Line 2 (FR):", GroupName = "Logger")]
        public string AddressLine2_3 { get; set; }

        [Display(Name = "Select Country:", GroupName = "Logger")]
        public int CountryID { get; set; }

        [Display(Name = "Select State:", GroupName = "Logger")]
        public int StateID { get; set; }

        [Display(Name = "Select City:", GroupName = "Logger")]
        public int CityID { get; set; }

        [Display(Name = "Enter Post Code:", GroupName = "Logger")]
        public string PostCode { get; set; }

        public bool IsPrimaryAddress { get; set; }

        [Display(Name = "Enter Apartment No:", GroupName = "Logger")]
        public string ApartmentNo { get; set; }

        [Display(Name = "Enter Street:", GroupName = "Logger")]
        public String Street { get; set; }

        [Display(Name = "Enter P.O.Box:", GroupName = "Logger")]
        public string PoBox { get; set; }

        [Display(Name = "Select Area:", GroupName = "Logger")]
        public int AreaId { get; set; }

        public string CountryName { get; set; }

        public string StateName { get; set; }

        public string CityName { get; set; }

        public string AreaName { get; set; }
    }
}
