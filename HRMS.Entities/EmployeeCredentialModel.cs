﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HRMS.Entities
{
    public class EmployeeCredentialModel
    {
        public int UserId { get; set; }
        public int EmployeeId { get; set; }

        public string EmployeeAlternativeID { get; set; }

        [Display(Name = "User name", GroupName = "Logger")]
        [Required(ErrorMessage = "Required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //public string NormalPassword { get; set; }

        [Display(Name = "Enabled", GroupName = "Logger")]
        public bool IsEnable { get; set; }

        public int Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string employeeName { get; set; }



    }
}
