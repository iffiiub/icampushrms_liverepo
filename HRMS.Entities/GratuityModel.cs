﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class GratuityModel
    {
        [Display(Name = "Salary Allowances:")]
        public int SalaryAllownceId { get; set; }

        [Display(Name = "IsStandard:")]
        public bool IsStandered { get; set; }

       // public bool AllowancesList { get; set; }

        private List<string> _selectedValues;
        private List<CheckListBoxItem> _items;

        public List<string> SelectedValues
        {
            get { return _selectedValues ?? (_selectedValues = new List<string>()); }
            set { _selectedValues = value; }
        }

        public List<CheckListBoxItem> Items
        {
            get { return _items ?? (_items = new List<CheckListBoxItem>()); }
            set { _items = value; }
        }
      
    }

    public class EmployeeGratuityModel
    {
        public string  EmpAlternativeID { get; set; }
        public int GratuityFSID { get; set; }
        public string EmployeeName { get; set; }
        private List<string> _selectedValues;
        private List<CheckListBoxItem> _items;


        [Display(Name = "Select Employees:")]
        public int EmployeeId { get; set; }
        public List<string> SelectedValues
        {
            get { return _selectedValues ?? (_selectedValues = new List<string>()); }
            set { _selectedValues = value; }
        }

        public List<CheckListBoxItem> Items
        {
            get { return _items ?? (_items = new List<CheckListBoxItem>()); }
            set { _items = value; }
        }

        public int GratuityFinalSettlementID { get; set; }

        public decimal GratuityBeforeNEAmount { get; set; }
        
        public decimal ESOthersAmount { get; set; }
        public decimal CompensatoryLeaveAmount { get; set; }
        public decimal GratuityAlreadyPaidAmount { get; set; }
        public decimal ESOthersDeductionAmount { get; set; }
        public int LastWorkingDays { get; set; }
        public bool isTaxInclude{get;set;}
        public int EOSType { get; set; }
        public string ToDate { get; set; }

    }

    public class GratuityTemplateSettings
    {
        public int GratuityTemplateSettingID { get; set; }
        public string SpName { get; set; }
        public bool IsActive { get; set; }
    }

}


