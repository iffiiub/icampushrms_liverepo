﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmergencyContactModel
    {
        public int EmergencyContactId { get; set; }

        public int EmployeeID { get; set; }

        [Required(ErrorMessage = "This feild is required")]
        [Display(Name="Contact Name", GroupName = "Logger")]
        public string ContactName { get; set; }

        [Required(ErrorMessage = "This feild is required")]
        [Display(Name = "Call in Emergency", GroupName = "Logger")]
        public bool CallInEmergancy { get; set; }

        [Required(ErrorMessage = "This feild is required")]
        [Display(Name = "Phone (Res)", GroupName = "Logger")]
        public string ResPhone { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [Display(Name = "Email", GroupName = "Logger")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This feild is required")]
        [Display(Name = "Relation", GroupName = "Logger")]
        public int RelationId { get; set; }

        public string RelationName { get; set; }

        [Display(Name = "Phone (Work)", GroupName = "Logger")]
        public string WorkPhone { get; set; }

        [Display(Name = "Mobile", GroupName = "Logger")]
        public string Mobile { get; set; }
    }    
}
