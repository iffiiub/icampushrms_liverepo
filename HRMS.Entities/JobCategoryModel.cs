﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class JobCategoryModel : BaseModel
    {
        public int EmployeeJobCategoryID { set; get; }
        public string EmployeeJobCategoryName_1 { set; get; }
        public string EmployeeJobCategoryName_2 { set; get; }
        public string EmployeeJobCategoryName_3 { set; get; }
    }
}
