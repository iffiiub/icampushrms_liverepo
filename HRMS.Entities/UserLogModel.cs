﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HRMS.Entities
{
    public class UserLogModel
    {
        public int userLogId { get; set; }
        public int userId { get; set; }
        public string userName { get; set; }
        public string description { get; set; }
        public string Date { get; set; }
        public string moduleName { get; set; }
        public string oprationType { get; set; }
        public string browserVersion { get; set; }
        public string browserName { get; set; }
        
    }

    public class LoggerDetail
    {
        public string ModuleName { get; set; }
        public string ModuleRefrence { get; set; }
        public string ModuleUrl { get; set; }
        public string NameOfProperty { get; set; }
        public int ValueOfProperty { get; set; }
        public int RefrenceId { get; set; }
        public List<ActionUrl> ActionUrlList { get; set; }
        public string DBFileName { get; set; }
        public string DBFunctionName { get; set; }
        public int NoOfParams { get; set; }
        public string DefaultParameter { get; set; }
        public string FilterParameter { get; set; }
    }

    public class ActionUrl
    {
        public string ActionName { get; set; }
        public string ModuleName { get; set; }
        public bool DirectSave { get; set; }
        public bool DirectUpdate { get; set; }
        public bool IsDelete { get; set; }
        public string NameOfProperty { get; set; }
        public int ValueOfProperty { get; set; }
        public string DBFileName { get; set; }
        public string DBFunctionName { get; set; }
        public int NoOfParams { get; set; }
        public string FilterParameter { get; set; }

        public ActionUrl()
        {
            NoOfParams = 1;
        }
    }
}
