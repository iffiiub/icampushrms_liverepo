﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeProfileModel : BaseModel
    {
        public int EmployeeProfileId { get; set; }
        public int? ProfileId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? AssignedDate { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
