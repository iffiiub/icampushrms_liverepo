﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeDependantModel : BaseModel
    {
        public int DependantId { set; get; }
        public int EmployeeId { set; get; }
        [Required(ErrorMessage = "This feild is required")]
        [Display(Name = "First Name")]
        public string FirstName { set; get; }

        //[Required(ErrorMessage = "This feild is required")]
        [Display(Name = "Middle Name")]
        public string MiddleName { set; get; }
        [Required(ErrorMessage = "This feild is required")]
        [Display(Name = "Last Name")]
        public string LastName { set; get; }

        [Required(ErrorMessage = "This feild is required")]
        public int Relationship { set; get; }
        [Display(Name = "Address 1")]
        public string Address1 { set; get; }
        [Display(Name = "Address 2")]
        public string Address2 { set; get; }
        public string City { set; get; }
        [Display(Name = "City / Town")]
        public string City_Town { set; get; }

        [Required(ErrorMessage = "This feild is required")]
        public int Country { set; get; }
        [Display(Name = "Phone 1")]
        public string Phone1 { set; get; }
        [Display(Name = "Phone 2")]
        public string Phone2 { set; get; }
        public int ModifiedBy { set; get; }

        public int StudentId { set; get; }

        public String RelationshipName { get; set; }
        public String CountryName { get; set; }
    }
}
