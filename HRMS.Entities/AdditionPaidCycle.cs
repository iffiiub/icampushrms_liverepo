﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PaySalaryAllowance : BaseModel
    {
        public int PaySalaryAllowanceID { get; set; }
        [Required(ErrorMessage= "This field is required.")]
        [Display(Name="Allowance Name (En):", GroupName = "Logger")]
        public string PaySalaryAllowanceName_1 { get; set; }

        [Display(Name = "Allowance Name (Ar):", GroupName = "Logger")]
        public string PaySalaryAllowanceName_2 { get; set; }

        [Display(Name = "Allowance Name (Fr):", GroupName = "Logger")]
        public string PaySalaryAllowanceName_3 { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Short Code (En):", GroupName = "Logger")]
        public string PaySalaryAllowanceShortName_1 { get; set; }

        [Display(Name = "Short Code (Ar):", GroupName = "Logger")]
        public string PaySalaryAllowanceShortName_2 { get; set; }

        [Display(Name = "Short Code (Fr):", GroupName = "Logger")]
        public string PaySalaryAllowanceShortName_3 { get; set; }

        [Display(Name = "Tran. Date:", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string TransactionDate { get; set; }

        [Display(Name = "Flag Add:", GroupName = "Logger")]
        public bool IsAddition { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Account Code:", GroupName = "Logger")]
        public string AccountCode { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Vacation Account Code:", GroupName = "Logger")]
        public string VacationAccountCode { get; set; }

        [Display(Name = "Include Ded:", GroupName = "Logger")]
        public bool IsIncludeInDeduction { get; set; }

        [Display(Name = "Sequence:", GroupName = "Logger")]
        public int Sequence { get; set; }

        public bool IsActive { get; set; }

        [Display(Name = "Vacn. Deductable:", GroupName = "Logger")]
        public bool IsVacactionDeductible { get; set; }

        [Display(Name = "Exclude In JV:")]
        public bool IsExcludeInJV { get; set; }

        [Display(Name = "Deductable With Joning Date:", GroupName = "Logger")]
        public bool IsDeductableOnHireDate { get; set; }

        public decimal CumulativeAddition { set; get; }

        public bool IsAccommodationAllowance { get; set; }

        public bool IsLockedUpdate { get; set; }
        public bool IsLockedDelete { get; set; }
    }

    public class PaySalary : BaseModel
    {
        public int PaySalaryID { get; set; }

        public int PaySalaryAllowanceID { get; set; }

        public int EmployeeId { get; set; }

        public float Amount { get; set; }

        public bool IsActive{ get; set; }

        public bool Addition { get; set; }

        public string Note { get; set; }

        public DateTime TrDate { get; set; }

        public int Currency { get; set; }

        public bool IsFixed { get; set; }

        public float Amount1 { get; set; }
    }

    public class PayAddition : BaseModel
    {
        public int PayAdditionID { get; set; }
        
        //[Range(1, 100, ErrorMessage = "Addition Type")]
        [Display(Name="Addition Type")]
        [Required(ErrorMessage = "Please select Addition Type")]
        public int PaySalaryAllowanceID { get; set; }

        public string PaySalaryAllowanceName { get; set; }

        public int EmployeeID { get; set; }

        [Required]
        [Display(Name = "Addition Date")]
        public String AdditionDate { get; set; }

        //[RegularExpression(@"^[1-9]\d*$", ErrorMessage = "Amount should be greater than 0")]
        [Required(ErrorMessage = "Please Enter Amount")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$", ErrorMessage = "Amount should be greater than 0")]
        public float Amount { get; set; }

        public string Description { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        public DateTime? TransactionDate { get; set; }

        public int AcademicYearID { get; set; }

        [Display(Name = "Extra Days")]
        public string ExtraDays { get; set; }

        [Display(Name = "Overtime")]
        public string OverTime { get; set; }
               
        public string OverTimeExtra { get; set; }

        public string OverTimeExtra1 { get; set; }

        [Display(Name = "Addition Cycles")]
        [Required(ErrorMessage = "Please select Addition Cycles")]
        [Range(0, int.MaxValue)]
        public string AdditionCycles { get; set; }
        public bool IsProcessed { get; set; }
    }

    public class PayCycle : BaseModel
    {
        public int PayCycleID { get; set; }
        [Display(Name = "Pay Cycle Name (En):", GroupName = "Logger")]
        [Required]
        public string PayCycleName_1 { get; set; }

        [Display(Name = "Pay Cycle Name (Ar):", GroupName = "Logger")]
        public string PayCycleName_2 { get; set; }

        [Display(Name = "Pay Cycle Name (Fr):", GroupName = "Logger")]
        public string PayCycleName_3 { get; set; }

        [Display(Name = "Date From:", GroupName = "Logger")]
        [Required]
        public string DateFrom { get; set; }

        [Display(Name = "Date To:", GroupName = "Logger")]
        [Required]
        public string DateTo { get; set; }

        [Display(Name="Active:", GroupName = "Logger")]
        public bool active { get; set; }

        [Display(Name="Ac. Year:", GroupName = "Logger")]
        public string acyear { get; set; }
        public string fromaDate { get; set; }
        public string toDate { get; set; }
        public double NoOfCycleDays { get; set; }
    }
}
