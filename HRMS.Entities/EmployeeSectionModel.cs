﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeSectionModel : BaseModel
    {
        public int EmployeeSectionID { get; set; }

        [Required(ErrorMessage = "Section Name(EN)")]
        [Display(Name = "Section Name(EN)", GroupName = "Logger")]
        public string EmployeeSectionName_1 { get; set; }

        [Display(Name = "Section Name(AR)", GroupName = "Logger")]
        public string EmployeeSectionName_2 { get; set; }

        [Display(Name = "Section Name(FR)", GroupName = "Logger")]
        public string EmployeeSectionName_3 { get; set; }

        [Required(ErrorMessage = "Head")]
        [Display(Name = "Head", GroupName = "Logger")]
        public int Head { get; set; }
        public string HeadName { get; set; }

        [Display(Name = "Assistant(1)", GroupName = "Logger")]
        public int? Assistant_1 { get; set; }

        public string AssistantName_1 { get; set; }

        [Display(Name = "Assistant(2)", GroupName = "Logger")]
        public int? Assistant_2 { get; set; }
        public string AssistantName_2 { get; set; }

        [Display(Name = "Assistant(3)", GroupName = "Logger")]
        public int? Assistant_3 { get; set; }
        public string AssistantName_3 { get; set; }

        [Display(Name = "Shift", GroupName = "Logger")]
        public int? shiftid { get; set; }
        public string ShiftName { get; set; }

        [Display(Name = "Department", GroupName = "Logger")]
        public int? DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int VacationDaysID { get; set; }
        public string VacationName { get; set; }
    }
}
