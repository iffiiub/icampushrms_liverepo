﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmploymentModeModel :BaseModel
    {
        public int EmploymentModeId { set; get; }
            public string EmploymentModeName1{set;get;}
            public string EmploymentModeName2 { set; get; }
            public string EmploymentModeName3 { set; get; }
    }
}
