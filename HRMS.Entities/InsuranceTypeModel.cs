﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class InsuranceTypeModel : BaseModel
    {
        public int HRInsuranceTypeID { set; get; }
        public string InsuranceTypeName_1 { get; set; }
        public string InsuranceTypeName_2 { set; get; }
        public string InsuranceTypeName_3 { set; get; }
        //public bool ISDeductable { set; get; }
        //public string PaidType { set; get; }
        //public bool AnnualLeave { set; get; }
    }

    public class InsuranceEligibilityList 
    {
        public int InsuranceEligibilityId { set; get; }
        public string InsuranceEligibilityName_1 { get; set; }
        public string InsuranceEligibilityName_2 { set; get; }
        public string InsuranceEligibilityName_3 { set; get; }
        public bool IsActive { set; get; }
        public bool IsDelete { set; get; }
        public int? CreatedBy { set; get; }
        public DateTime? CreatedDate { set; get; }
        public int? ModifiedBy { set; get; }
        public DateTime? ModifiedDate { set; get; }
       
    }

    public class InsuranceCategoryAmount
    {
        public int CatAmountID { set; get; }
        public int HRInsuranceTypeID { set; get; }
        public int ACYearID { set; get; }
        public decimal Amount { get; set; }
        public string InsuranceTypeName { set; get; }
        public string AcademicYear { set; get; }
        public bool IsActive { set; get; }
        public bool IsDelete { set; get; }
        public int? CreatedBy { set; get; }
        public DateTime? CreatedDate { set; get; }
        public int? ModifiedBy { set; get; }
        public DateTime? ModifiedDate { set; get; }

    }
}
