﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class DashBoardModel
    {
        public int PayDocumentTypeID { get; set; }
        public string PayDocumentTypeDescription_1 { get; set; }
        public int count { get; set; }

        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int Strength { get; set; }
        public string isActive { get; set; }

        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string SurName { get; set; }

        public int GenderID { get; set; }
        public string GenderName { get; set; }

        public string CountryName { get; set; }

        public string LAtLong { get; set; }

         public List<string> LAtLongList { get; set; }

        public string SectionName { get; set; }
        public string TotalEmployee { get; set; }
    }
}
