﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class HRDegreeModel : BaseModel
    {
        public int HRDegreeID { set; get; }
        [Display(Name = "Degree Name")]
        public string HRDegreeName_1 { set; get; }
        [Display(Name = "Degree Name 1")]
        public string HRDegreeName_2 { set; get; }
        [Display(Name = "Degree Name 1")]
        public string HRDegreeName_3 { set; get; }
    }
}
