﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class HolidayModel : BaseModel 
    {
        public int HolidayID { set; get; }

        [Display(Name = "Holiday Name(EN)", GroupName = "Logger")]
        public string HolidayName {set;get;}

        [Display(Name = "Holiday Name(AR)", GroupName = "Logger")]
        public string HolidayName_2 { get; set; }
        [Display(Name = "Holiday Name(FR)", GroupName = "Logger")]
        public string HolidayName_3 { get; set; }

        [Display(Name = "From", GroupName = "Logger")]
        public string From { set; get; }

        [Display(Name = "To", GroupName = "Logger")]
        public string to { set; get; }
       // public int? CompanyID { set; get; }
        public string CompanyIDs { set; get; }
        public string CompanyName { set; get; }
        public bool IsUsed { set; get; }
        // public int CreatedBy { set; get; }
        // public int ModifiedBy { set; get; }


    }
}
