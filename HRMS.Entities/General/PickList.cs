﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PickList
    {
        public string text { get; set; }
        public int id { get; set; }
        public string dataFeild1 { get; set; }
        public string dataFeild2 { get; set; }
        public string selectedValue { get; set; }
    }
}
