﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class PayAccountTypesModel
    {

        public int AccountTypeID { get; set; }

        public string AccountTypeName_1 { get; set; }

        public string AccountTypeName_2 { get; set; }

        public string AccountTypeName_3 { get; set; }

        public string tr_date { get; set; }

    }
}
