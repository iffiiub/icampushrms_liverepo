﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{

    public enum WeekdaysEnum
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7
    }


    public enum MonthsEnum
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    public enum ApplicableAfterTypeEnum
    { 
    Days =0,
        Months=1,
    
    }

    public enum TrueFalseEnum
    { 
    True =1,
        False=0,
    
    }

    public enum AttendenceReport
    {
        WorkingDays = 1071,
        WorkingHours = 1072,
        LateAndEarlyMinutes = 1073,
        DailyAttendanceByStatus = 1074,
        EmployeeAttendanceByStatus = 1075,
        MomthlyAttendance = 1076
    }
    public enum MasterDataType
    {
        LearningCategory = 1,
        TrainingActivityFormat = 2
    }
    public enum DevelopmentPlanCategory
    {
        JobDescription =1,
        PDRP =2,
        Disc=3
        
    }
}
