﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
   
    //*** F7
    public class XMLLanguages
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public string LanguageFaFlag { get; set; }
        public string LanguageName2 { get; set; }
        public string LanguageName3 { get; set; }
        public string LanguageShortName { get; set; }

        public string LanguageCulture { get; set; }
        public int IsActive { get; set; }
    }
}
