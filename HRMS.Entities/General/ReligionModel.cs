﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class ReligionModel : BaseModel
    {
        public int ReligionID { set; get; }
        [Required(ErrorMessage = "Required")]
        public string ReligionName_1 { set; get; }
        [Required(ErrorMessage = "Required")]
        public string ReligionName_2 { set; get; }
        [Required(ErrorMessage = "Required")]
        public string ReligionName_3 { set; get; }
        [Required(ErrorMessage = "Required")]
        public int ReligionCategoryID { set; get; }
        [Required(ErrorMessage = "Required")]
        public bool isIslam { set; get; }
        [Required(ErrorMessage = "Required")]
        public bool isChristian { set; get; }

        public List<ReligionModel> ReligionList { get; set; }
    }
}
