﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class AttendanceRemindersModel
    {
        public AttendanceRemindersModel()
        {
            RecieverEmployeeIDs = "";
        }
        public int AttReminderID { get; set; }
        [Display(Name = "Enable Reminder")]
        public bool EnableReminder { get; set; }
        [Display(Name = "Attendance Status")]
        public String AttendanceStatusIDs { get; set; }
        [Display(Name = "Send Reminder to Employee(s)")]
        public string RecieverEmployeeIDs { get; set; }
    }
}
