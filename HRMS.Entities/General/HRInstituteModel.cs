﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class HRInstituteModel : BaseModel
    {
       
        public int HRInstituteID { set; get; }

        [Display(Name = "Institute Name")]
        [Required(ErrorMessage = "This field is required.")]
        public string HRInstituteName_1 { set; get; }

        [Display(Name = "Institute Name")]
        public string HRInstituteName_2 { set; get; }

        [Display(Name = "Institute Name")]
        public string HRInstituteName_3 { set; get; }

        public bool IsActive { set; get; }

        public bool IsDeleted { set; get; }
    }
}
