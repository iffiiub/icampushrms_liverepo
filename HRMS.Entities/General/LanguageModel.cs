﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class LanguageModel
    {

        public int LanguageID { get; set; }

        public string LanguageName_1 { get; set; }

        public string LanguageName_2 { get; set; }

        public string LanguageName_3 { get; set; }

        public string LanguageShortName_1 { get; set; }

        public string LanguageShortName_2 { get; set; }

        public string LanguageShortName_3 { get; set; }



    }
}
