﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class PayCategoriesModel
    {
        public int CategoryID { get; set; }
        public string CategoryName_1 { get; set; }
        public string CategoryName_2 { get; set; }
        public string CategoryName_3 { get; set; }
        public int seq { get; set; }      
        public bool isDeleted { get; set; }       
        public List<CostCenter> lstCostCenters { get; set; }
    }

    public class CostCenter
    {
        public int PayCategoryID { get; set; }
        public int DimensionValueID { get; set; }
        public string DimensionName { get; set; }
        public string DimensionValueName { get; set; }
        public decimal Percentage { get; set; }
    }

}
