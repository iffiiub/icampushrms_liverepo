﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities.General
{
    public class MaritialStausModel
    {

        public int MaritalStatusID { get; set; }

        public string MaritalStatusName_1 { get; set; }

        public string MaritalStatusName_2 { get; set; }

        public string MaritalStatusName_3 { get; set; }

        public SelectList MaritalStatusList { get; set; }

    }
}
