﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class DocumentModel
    {
        public int PayDocumentId { get; set; }                
        public int EmployeeID { get; set; }
        public int PayDocumentTypeID { get; set; }
        public string PayDocumentNumber { get; set; }
        public int IssuePlaceID { get; set; }
        public string IssueDate { get; set; }
        public string ExpireDate { get; set; }
        public bool IsPrimary { get; set; }
        public string MotherName { get; set; }
        public bool IsPassportWithEmployee { get; set; }
        public string TakenDate { get; set;}
        public string Reason { get; set; }
        public int MustReturn { get; set; }
        public string ReturnDate { get; set;}
        public int SponsorID { get; set; }
        public string Note { get; set; }
        public int HRContractTypeID { get; set; }
        public int IssueCountryID { get; set; }
        public string MOLTitle { get; set; }
        public string VAcc1 { get; set; }
        public string VAcc2 { get; set; }
        public string VAcc3 { get; set; }
    }
}
