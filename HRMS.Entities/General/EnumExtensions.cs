﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public static class EnumExtensions
    {
        public static List<EnumData> GetDescriptions(Type type)
        {
            List<EnumData> lstEnumData = new List<EnumData>();
            var names = Enum.GetNames(type);
            foreach (var name in names)
            {
                var field = type.GetField(name);
                var value = (int)field.GetRawConstantValue();

                var fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                foreach (DescriptionAttribute fd in fds)
                {
                    lstEnumData.Add(new EnumData(value, fd.Description));
                }
            }
            return lstEnumData;
        }
    }

    public class EnumData
    {
        public EnumData(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
