﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class TrainingCompetencyDetailModel
    {
        public int ID { get; set; }
        public int CompetencyID { get; set; }
        [Display(Name = "Training")]
        public int TrainingID { get; set; }
        [Display(Name = "Training Name")]
        public string TrainingName { get; set; }

        [Display(Name = "Competency Title")]
        public string CompetencyTitle { get; set; }
        public string CompetencyIDs { get; set; }
    }
}
