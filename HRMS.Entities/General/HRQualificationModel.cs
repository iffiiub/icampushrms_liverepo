﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class HRQualificationModel : BaseModel
    {
        public int HRQualificationID { set; get; }
        [Display(Name = "Qualification Name")]
        public string HRQualificationName_1 { set; get; }
        [Display(Name = "Qualification Name 2")]
        public string HRQualificationName_2 { set; get; }
        [Display(Name = "Qualification Name 3")]
        public string HRQualificationName_3 { set; get; }

        public bool IsActive { set; get; }

        public bool IsDeleted { set; get; }
    }
}
