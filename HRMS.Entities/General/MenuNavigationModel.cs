﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class MenuNavigationModel : BaseModel
    {
        public int MainNavigationSubCategoryId { set; get; }
        public int MainNavigationId { set; get; }
        public string MainNavigationSubCategoryName { set; get; }
        public string MainNavigationName { set; get; }
        public string URL { set; get; }
        //*** Naresh 2020-03-17 New field to avail view level access
        public string ViewAccess { set; get; }
        public string MainNavigationHTMLLink { set; get; }
        public string MainNavigationSubCategoryHTMLLink { set; get; }
        public int SortingOrder { set; get; }
        public bool IsThirdNavigation { set; get; }
        public bool IsViewCheckBox { set; get; }
        public bool IsAddCheckbox { set; get; }
        public bool IsUpdateCheckbox { set; get; }
        public bool IsDeleteCheckbox { set; get; }
        public string NavOtherPermissionIDs { set; get; }
    }

    public class OtherPermissions
    {
        public int MainNavigationSubCategoryId { get; set; }
        public int OtherPermissionId { get; set; }
        public string OtherPermissionName { get; set; }
    }
}
