﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class HRExperiencePositionModel : BaseModel
    {
        public int HRExperiencePositionID { set; get; }
        public string HRExperiencePositionName_1 { set; get; }
        public string HRExperiencePositionName_2 { set; get; }
        public string HRExperiencePositionName_3 { set; get; }
    }
}
