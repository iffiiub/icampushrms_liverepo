﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class PayBanksModel
    {

        public int BankID { get; set; }

        public int? PayBanksModeID { get; set; }
        [Display(Name = "BankName_1", GroupName = "Logger")]
        public string BankName_1 { get; set; }

        [Display(Name = "BankName_2", GroupName = "Logger")]
        public string BankName_2 { get; set; }

        public string BankName_3 { get; set; }

        public string tr_Date { get; set; }

        public string acc_code { get; set; }
        [Display(Name = "Rtn Code", GroupName = "Logger")]

        public string RtnCode { get; set; }

        [Display(Name = "Swift Code", GroupName = "Logger")]
        public string SwiftCode { get; set; }
        public int? BankGroupID { get; set; }
        public string BankGroupName { get; set; }
        public bool CashBank { get; set; }       

    }

    public class PayBankGroup
    {
        public int BankGroupID { get; set; }
        public string BankGroupName_1 { get; set; }
        public string BankGroupName_2 { get; set; }
        public string BankGroupName_3 { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string SalaryPayableAccountCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class PayBanksModeModel
    {
        public int PayBanksModeID { get; set; }
        public string PayBanksModeName_1 { get; set; }
        public string PayBanksModeName_2 { get; set; }
        public string PayBanksModeName_3 { get; set; }       
        public bool IsActive { get; set; }              
    }

}
