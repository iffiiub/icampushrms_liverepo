﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class GenderModel
    {

        public int GenderID { get; set; }

        public string GenderName_1 { get; set; }

        public string GenderName_2 { get; set; }

        public string GenderName_3 { get; set; }

        public string GenderImagePath { get; set; }

        public List<GenderModel> Genderlist { get; set; }
        


    }
}
