﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class PayBankBranchModel
    {

        public int BranchID { get; set; }

        public string BranchName_1 { get; set; }

        public string BranchName_2 { get; set; }

        public string BranchName_3 { get; set; }

    }
}
