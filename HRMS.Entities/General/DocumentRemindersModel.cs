﻿using HRMS.XMLResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class DocumentRemindersModel
    {
        public DocumentRemindersModel()
        {
            RecieverEmployeeIDs = "";
        }
        public int DocReminderID { get; set; }
        [Display(Name = "About to Expire")]
        public bool AboutToExpire { get; set; }
        [Display(Name = "Already to Expired")]
        public bool AlreadyExpire { get; set; }
        [Display(Name = "Number of Days")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_2")]
        public int? ExpiringNumberOfDays { get; set; }

        [Display(Name = "Number of Days")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Common_Resources),
                       ErrorMessageResourceName = "Range_2")]
        public int? ExpiredNumberOfDays { get; set; }
        [Display(Name= "Frequency Type")]
        [Required]
        public string FrequencyType { get; set; }
        public string WeeklyDayName { get; set; }
        public int MonthlyDay { get; set; }
        public string RecieverEmployeeIDs { get; set; }
    }
}
