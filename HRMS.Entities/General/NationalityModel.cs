﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.General
{
    public class NationalityModel
    {

        public int NationalityID { get; set; }

        public string NationalityName_1 { get; set; }

        public string NationalityName_2 { get; set; }

        public string NationalityName_3 { get; set; }

        public int NationalityCategoryID { get; set; }

        public bool IsArab { get; set; }

        public bool IsCitizen { get; set; }

        public string NationalityShortName_1 { get; set; }

        public string NationalityShortName_2 { get; set; }

        public string NationalityShortName_3 { get; set; }

        public List<NationalityModel> NationalityList { get; set; }
    }

    public class NationalityCountModel
    {
        public int NationalityId { get; set; }
        public String NationalityName { get; set; }
        public int NationalityCount { get; set; }        
    }

    public class NationalityTypeModel
    {
        public int NationalityTypeId { get; set; }
        public string  NationalityType { get; set; }
        public string  NationalityTypeDesc { get; set; }
    }
}
