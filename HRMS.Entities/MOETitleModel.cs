﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class MOETitleModel
    {
        public int MOETitleID { get; set; }

        [Display(Name="MOE Title (En):", GroupName = "Logger")]
        public string MOETitleName_1 { get; set; }

        [Display(Name = "MOE Title (Ar):", GroupName = "Logger")]
        public string MOETitleName_2 { get; set; }

        [Display(Name = "MOE Title (Fr):", GroupName = "Logger")]
        public string MOETitleName_3 { get; set; }

        [Display(Name = "Short Code (En):", GroupName = "Logger")]
        public string MOETitleShortName_1 { get; set; }

        [Display(Name = "Short Code (Ar):", GroupName = "Logger")]
        public string MOETitleShortName_2 { get; set; }

        [Display(Name = "Short Code (Fr):", GroupName = "Logger")]
        public string MOETitleShortName_3 { get; set; }
    }
}
