﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PaySalaryModel
    {
        public int PaySalaryID { get; set; }

        [Display(Name = "Salary Allowances", GroupName = "Logger")]
        [Required(ErrorMessage = "This feild is required")]
        public int PaySalaryAllowanceID { get; set; }


        [Required(ErrorMessage = "This feild is required")]
        public int EmployeeId { get; set; }

        [Display(Name = "Amount", GroupName = "Logger")]
        [Required(ErrorMessage = "This feild is required")]
        public float Amount { get; set; }

        public bool IsActive { get; set; }

        public bool Addition { get; set; }

        [Display(Name = "Note", GroupName = "Logger")]
        public string Note { get; set; }

        [Display(Name = "Date", GroupName = "Logger")]
        [Required(ErrorMessage = "This feild is required")]
        public string TrDate { get; set; }

        public int Currency { get; set; }

        public bool IsFixed { get; set; }

        public float Amount1 { get; set; }

        public string PaySalaryAllowanceName_1 { get; set; }

        public float TotalAmount { get; set; }

        public string LastPaidDate { get; set; }

        public string AllowanceEndDate { get; set; }


    }
}
