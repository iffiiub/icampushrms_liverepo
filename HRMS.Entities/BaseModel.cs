﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.ViewModel;
using System.Configuration;

namespace HRMS.Entities
{
    public class BaseModel : UserContextViewModel
    {

        public string Message { get; set; }
        public string CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public string DeletedOn { get; set; }
    }

    public class ModelHelpers
    {
        public const string DateFormat = "{0:dd/MMM/yyyy}";
        public static string HRMSDateFormat
        {
            get { return ConfigurationManager.AppSettings["HRMSDateFormat"]; }
        }
        public static string HRMSDateStringFormat
        {
            get { return DateFormat; }
        }
        
    }
}
