﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayBatchModel : BaseModel
    {
        public int PayBatchID { set; get; }
        public string PayBatchName { set; get; }
        public string PayBatchFrom { set; get; }
        public string PayBatchTo { set; get; }
        public bool active { set; get; }
        public int acyear { set; get; }
    }
}
