﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeBankAccountInfoModel
    {
        public int BankAccountInfoId { get; set; }

        public int EmployeeID { get; set; }

        [Display(Name = "Bank Name:")]
        [Range(0, 500, ErrorMessage = "Please select bank")]
        public string BankId { get; set; }

        [Display(Name = "Account Number:")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account Name:")]
        public string AccountName { get; set; }
                
        [Display(Name = "Bank Address:")]
        [StringLength(255, MinimumLength = 1)]
        public string BankAddress { get; set; }

        [Display(Name = "Swift/BIC Code:")]
        public string SwiftBICCode { get; set; }

        [Display(Name = "Sort Code:")]
        public string SortCode { get; set; }

        [Display(Name = "IBAN Code:")]
        public string IBanCode { get; set; }

        [Display(Name = "Agent :")]
        public string AgentID { get; set; }

        public string BankName { get; set; }
    }
}
