﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class MedicalMainModel : BaseModel
    {
        public int EmployeeMedicalMainID { set; get; }

        public int EmployeeID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public double Height { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public double Weight { set; get; }

        [Required(ErrorMessage = "This field is required.")]
        public int Blood { set; get; }

        public string Alert1 { set; get; }
        public string Alerta1 { set; get; }
        public string Alert2 { set; get; }
        public string Alert3 { set; get; }
        public string Alert4 { set; get; }
        public string Alert5 { set; get; }
        public string Alert6 { set; get; }
        public string Alerta2 { set; get; }
        public string Alerta3 { set; get; }
        public string Alerta4 { set; get; }
        public string Alerta6 { set; get; }
        public string Alerta5 { set; get; }
        public string RightEye { set; get; }
        public string LeftEye { set; get; }
        public int aTntTranMode { set; get; }


        public string MedicalAlert { set; get; }
        public string MedicalAlertAr { set; get; }
        public int EmployeeMedicalAlertID { set; get; }



    }
}
