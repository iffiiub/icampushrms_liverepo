﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class SMTPConfig
    {
        [Display(Name = "From Email Address:")]
        [Required(ErrorMessage = "Please enter from email addres")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address")]
        public string FromEmailAddress { get; set; }

        [Display(Name = "From Display Name:")]
        [Required(ErrorMessage = "Please enter from display name")]
        public string FromDisplayName { get; set; }

        [Display(Name = "Reply Mail:")]
        [Required(ErrorMessage = "Please enter from reply mail")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address")]
        public string ReplyEmail { get; set; }

        [Display(Name = "SMTP Server:")]
        [Required(ErrorMessage = "Please enter SMTP server")]
        public string SMTPServer { get; set; }

        [Display(Name = "Port Number:")]
        [Required(ErrorMessage = "Please enter port number")]
        public int PortNumber { get; set; }

        [Display(Name = "Secure Connection:")]
        [Required(ErrorMessage = "Please enter secure conection")]
        public bool SecureConnection { get; set; }

        [Display(Name = "Username:")]
        [Required(ErrorMessage = "Please enter username")]
        public string Username { get; set; }

        [Display(Name = "Password:")]
        [Required(ErrorMessage = "Please enter secure conection")]
        public string Password { get; set; }

    }
}
