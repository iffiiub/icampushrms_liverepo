﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class WorkexperienceModel : BaseModel
    {
        public int WorkexperienceID { set; get; }
        public string EmployerName_1 {set;get;}
        public string EmployerName_2 {set;get;}
        public string EmployerName_3 { set; get; }
        public string WorkStartDate { set; get; }
        public string WorkEndDate { set; get; }
        public int HRExperiencePositionID { set; get; }
        public string WorkAddress { set; get; }
        public int CityID { set; get; }
        public int CountryID { set; get; }
        public int EmployeeID { set; get; }
        public int ExperienceMonths { set; get; }
        public int ExperienceYears { set; get; }
    }
}
