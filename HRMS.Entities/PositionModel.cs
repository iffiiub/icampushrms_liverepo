﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PositionModel
    {
        public int PositionID { set; get; }

        [Display(Name = "Position Code", GroupName = "Logger")]
        public string PositionCode { set; get; }

        [Display(Name = "Position Title(EN)", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required")]
        public string PositionTitle { set; get; }

        [Display(Name = "Position Title(AR)", GroupName = "Logger")]
        public string PositionTitle_2 { set; get; }

        [Display(Name = "Position Title(FR)", GroupName = "Logger")]
        public string PositionTitle_3 { set; get; }

        [Display(Name = "Active", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required")]
        public bool IsActive { set; get; }

        [Display(Name = "Department", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required")]
        public int DepartmentID { set; get; }


        [Required(ErrorMessage = "This field is required")]
        public int Reg_TempID { set; get; }

        [Display(Name = "Employment Type", GroupName = "Logger")]
        public int? EmploymentTypeID { set; get; }

        [Display(Name = "Job family", GroupName = "Logger")]
        public int? JobFamilyID { set; get; }
        public bool IsDeleted { set; get; }

        [Display(Name = "Academic", GroupName = "Logger")]
        public bool IsAcademic { set; get; }

        //*** Naresh 2020-03-18 CompanyId added and removed Department
        //public string departmentName { set; get; }
        public string CompanyName { set; get; }

        [Display(Name = "Organization", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required")]
        public int CompanyID { set; get; }
    }
}
