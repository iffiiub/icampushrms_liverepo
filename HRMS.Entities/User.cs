﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class User
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Password")]
        public string password { get; set; }

    }
}
