﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class TreeItemView
    {
        public int? id { get; set; }
        public int? parentId { get; set; }
        public string name { get; set; }
        public bool checkbox { get; set; }
        public string url { get; set; }
        public string ModuleCode { get; set; }
        public bool IsRootModule { get; set; }
    }
    public class ParentTree
    {
        public int id { get; set; }
        public string text { get; set; }
        public bool @checked { get; set; }
        public TreeAttributes attributes { get; set; }
        public List<ParentTree> children { get; set; }
    }
    public class TreeAttributes
    {
        public int HeaderRowNum { get; set; }
        public short LevelNo { get; set; }
    }

    public class TreeNavigationModel
    {
        public int ModuleID { get; set; }
        public int ActNavigationID { get; set; }
        public string ModuleName { get; set; }
        public string ModuleCode { get; set; }
        public string ModuleURL { get; set; }
        public int ParentModuleID { get; set; }
        public bool IsViewCheckbox { get; set; }
        public bool IsAddCheckBox { get; set; }
        public bool IsUpdateCheckbox { get; set; }
        public bool IsDeleteCheckbox { get; set; }
        public string OtherPermissionIds { get; set; }
        public bool IsThirdNavigation { get; set; }

    }

    public class UserRolePermissions
    {
        public int UserRoleID { get; set; }
        public int ModuleID { get; set; }
        public int ActNavigationID { get; set; }
        public string ModuleName { get; set; }
        public bool IsFullPermssion { get; set; }
        public bool? IsAddPermssion { get; set; }
        public bool? IsViewPermssion { get; set; }
        public bool? IsUpdatePermssion { get; set; }
        public bool? IsDeletePermssion { get; set; }       
        public List<OtherNavPermissions> lstOtherPermissions { get; set; } 
    }

    public class OtherNavPermissions
    {
        public int OtherPermissionId { get; set; }
        public string OtherPermissionName { get; set; }
        public int AssociatedNavigationId { get; set; }
        public bool IsPermitted { get; set; }
    }  
    public class CustomPermissionEdit
    {
        public short PermissionId { get; set; }        
        public bool GrantPermission { get; set; }        
        public string PermissionType { get; set; }
    }
}
