﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;

namespace HRMS.Entities
{
    public class LeaveTypeModel : BaseModel
    {
        public int LeaveTypeId { set; get; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Leave Type Name:")]
        public string LeaveTypeName { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Number Of Days:")]
        public int NoOfDays { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Frequency:")]
        public int RepeatedFor { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Applicable After:")]
        public int ApplicableAfterDigits { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Applicable After Type:")]
        public int ApplicableAfterTypeID { set; get; }


        [Display(Name = "Gender:")]
        public int GenderID { set; get; }


        [Display(Name = "Nationality:")]
        public int NationalityID { set; get; }

        [Display(Name = "Religion:")]
        public int ReligionID { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Paid/Unpaid:")]
        public int isCompensate { set; get; }

        [Display(Name = "Gender Name:")]
        public string GenderName_1 { get; set; }


        public string ReligionName_1 { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Annual Leave:")]
        public int AnnualLeave { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Service Leave Limit:")]
        public int LifetimeLeave { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Monthly Split:")]
        public int MonthlySplit { get; set; }


        [Display(Name = "Marital Status:")]
        public int MaritalStatusId { get; set; }

        //   public int CompanyID { get; set; }
        public string NationalityName_1 { get; set; }
        public List<GenderModel> genderList { get; set; }
        public List<ReligionModel> ReligionList { get; set; }
        public List<NationalityModel> NationalityList { get; set; }
        public List<MaritialStausModel> MaritalStatusList { get; set; }
    }

    public class LeaveSetting
    {
        public decimal AccumulateLeaveMonth { get; set; }
        public bool BeamHideFields { get; set; }
        public bool ShowVTCategoryField { get; set; }
    }

}
