﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class HrSettings
    {
        public string HrVersion { get; set; }
        public bool IsMicrosoftAuthEnabled { get; set; }
    }
}
