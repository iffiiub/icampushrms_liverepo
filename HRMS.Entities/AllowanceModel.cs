﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class AllowanceModel : BaseModel
    {
        public AllowanceModel()
        {
            this.transportationModel = new TransportationModel();
            this.gasCardModel = new GasCardModel();
            this.EmpAirFare = new EmployeeAirFare();
            this.InsuranceModel = new InsuranceModel();


        }

        public int aTntTranMode { set; get; }

        //Accommodation Info
        public int PayAccommodationID { set; get; }

        [Display(Name = "Type", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int HRAccommodationTypeID { set; get; }

        [Display(Name = "Type Name", GroupName = "Logger")]
        public string HRAccommodationTypeName { set; get; }

        [Display(Name = "Appartment", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string Appartment { set; get; }

        [Display(Name = "Housing", GroupName = "Logger")]
        public bool Housing { set; get; }

        [Display(Name = "Country", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int CountryID { set; get; }

        public string CountryName { set; get; }

        [Display(Name = "City", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int CityID { set; get; }

        public string CityName { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public bool? Allowance { set; get; }

        [Display(Name = "Building floor", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string BuildingFloor { set; get; }

        [Display(Name = "Street", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string Street { set; get; }

        [Display(Name = "Building", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string Building { set; get; }



        //Accommodation History
        public int PayEmployeeAccommodationHistoryID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public string PayEmployeeAccommodationHistoryYearID { set; get; }

        public string PayEmployeeAccommodationHistoryYear { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public double AccommodationHistoryAmount { set; get; }

        [Display(Name = "Furnished", GroupName = "Logger")]
        public bool Furnished { set; get; }
        [Required(ErrorMessage = "This field is required.")]

        [Display(Name = "Furnished Amount", GroupName = "Logger")]
        public double FurnishedAmount { set; get; }

        [Display(Name = "Electricity", GroupName = "Logger")]
        public bool Electricity { set; get; }
        [Required(ErrorMessage = "This field is required.")]

        [Display(Name = "Electricity Amount", GroupName = "Logger")]
        public double ElectricityAmount { set; get; }

        [Display(Name = "Water", GroupName = "Logger")]
        public bool Water { set; get; }

        [Display(Name = "Water Amount", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public double WaterAmount { set; get; }

        public int EmployeeID { set; get; }
        //public string PayEmployeeAbsentDate { set; get; }


        //Maintainence Info
        public int MaintainenceInfoID { set; get; }
        public string MaintainenceInfoDate { set; get; }
        public double MaintainenceInfoAmount { set; get; }
        public string MaintainenceInfoApartmentNo { set; get; }
        public string MaintainenceInfoDescription { set; get; }



        //Transporatation
        public TransportationModel transportationModel { get; set; }
        public GasCardModel gasCardModel { get; set; }

        public EmployeeAirFare EmpAirFare { get; set; }
        public InsuranceModel InsuranceModel { get; set; }

        public class TransportationModel
        {

            public int PayTransportationID { set; get; }
            public int PayTransportationTypeID { set; get; }
            public string PayTransportationTypeName { set; get; }
            [Required(ErrorMessage = "This field is required.")]

            [Display(Name = "Value", GroupName = "Logger")]
            public double PayTransportationValue { set; get; }

            [Display(Name = "Model name", GroupName = "Logger")]
            [Required(ErrorMessage = "This field is required.")]
            public string ModelName { set; get; }

            [Display(Name = "Recived date", GroupName = "Logger")]
            [Required(ErrorMessage = "This field is required.")]
            public string ReceivedDate { set; get; }
            [Required(ErrorMessage = "This field is required.")]

            [Display(Name = "Car insurance", GroupName = "Logger")]
            public double Ecurrence { set; get; }

            [Display(Name = "Car insurance year", GroupName = "Logger")]
            public int EccurenceYear { set; get; }

            [Display(Name = "Car maintenance", GroupName = "Logger")]
            [Required(ErrorMessage = "This field is required.")]
            public double Maintenance { set; get; }

            [Display(Name = "Car maintenance year", GroupName = "Logger")]
            public int MaintenanceYear { set; get; }
            public string UpdateDate { set; get; }
            //public int aTransportTntTranMode { set; get; }
            [Required(ErrorMessage = "This field is required.")]

            [Display(Name = "Car type", GroupName = "Logger")]
            public int CarTypeID { set; get; }
            public string CarTypeName { set; get; }

            [Display(Name = "Shuttle", GroupName = "Logger")]
            public bool IsShuttle { get; set; }

            [Display(Name = "Car provided by schoo", GroupName = "Logger")]
            public bool BySchoolCar { get; set; }

            [Display(Name = "Has own car", GroupName = "Logger")]
            public bool HasOwnCar { get; set; }
        }



        public class GasCardModel
        {
            public int PayGasCardID { set; get; }

            [Display(Name = "Amount", GroupName = "Logger")]
            [Required(ErrorMessage = "This field is required.")]
            public double PayGasCardValue { set; get; }

            [Display(Name = "Company", GroupName = "Logger")]
            [Required(ErrorMessage = "This field is required.")]
            public string Company { set; get; }

            [Display(Name = "Received Date", GroupName = "Logger")]
            [Required(ErrorMessage = "This field is required.")]
            public string GasCardReceivedDate { set; get; }
            [Required(ErrorMessage = "This field is required.")]

            [Display(Name = "Gas card number", GroupName = "Logger")]
            public string PayGasCardNumber { set; get; }

            [Display(Name = "Period", GroupName = "Logger")]
            public int PayGasCardPeriod { set; get; }
            public string PayGasCardPeriodName { set; get; }

        }



        public string PayEmployeeAbsentFromDate { set; get; }
        public string PayEmployeeAbsentToDate { set; get; }



        public string EmployeeName { set; get; }

    }

    public class EmployeeAirFare
    {
        public int EmployeeAirfareID { get; set; }

        [Display(Name = "Air ticket country", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int AirTicketCountryID { get; set; }

        [Display(Name = "Air ticket city", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int AirTicketCityID { get; set; }

        [Display(Name = "Air ticket class", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int AirTicketClassID { get; set; }

        [Display(Name = "Air ticket amount", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public decimal AirTicketAmount { get; set; }


        [Display(Name = "No. of tickets for dependants", GroupName = "Logger")]
        public int NoOfDependencies { get; set; }

        [Display(Name = "Ticket class for dependants", GroupName = "Logger")]
        public int? AirTicketClassIDForDependant { get; set; }

        [Display(Name = "Air ticket amount dependants", GroupName = "Logger")]
        public decimal AirTicketAmountDependannt { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public bool? AnnualAirTicket { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int AirfareFrequencyID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public int AirportListID { get; set; }
    }

    public class AirFareClasses
    {
        public int AirFareClassId { get; set; }
        public string AirFareClassName { get; set; }

    }
}
