﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class Types
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Text { get; set; }
        public string TextArabic { get; set; }
        public string TextFrench { get; set; }

        public int DepartmentId { get; set; }
        public int CityId { get; set; }

        public int CountryId { get; set; }

        public int StateId { get; set; }

        public int companyId { get; set; }
        public int CreatedBy { get; set; }
    }
}
