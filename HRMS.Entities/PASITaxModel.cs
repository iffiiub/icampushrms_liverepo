﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PASITaxModel : BaseModel
    {
        public int EmployeeID { set; get; }
        public string EffectiveDate { set; get; }
        public string EmployeeAlternativeID { set; get; }
        public string FullName { set; get; }
        public decimal Amount { set; get; }
        public decimal PasiPercentage { set; get; }
        public decimal PASI_TAX { set; get; }
        public int Cycleid { set; get; }
        public string Cyclename { set; get; }
        public int aTntTranMode { set; get; }
    }

    public class TaxSetting
    {
        public bool TaxForEmployee { get; set; }
        public bool TaxForAll { get; set; }
        public decimal TaxPercentage { get; set; }
        public bool EnableJordanTax { get; set; }
        public bool HideTaxPercentage { get; set; }
    }
}
