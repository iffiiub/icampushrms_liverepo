﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    class Settings
    {
    }

    public class PayAdditionSettingsModel
    {
        public float OverTimeWorkingHours { get; set; }
        public float OverTimeWorkingHoursExtra { get; set; }
        public float OverTimeWorkingHoursExtra1 { get; set; }
        public double PerDayFixAmount { get; set; }
    }

    public class EmployeeIdSetting
    {
        public string EmployeeIdPrefix { get; set; }
        public int EmployeeIdStartFrom { get; set; }
    }

    public class EmployeeWorkEmailSetting
    {
        public int EmailIdSettingID { get; set; }
        public string EmailIdSettingValue { get; set; }

        [Required(ErrorMessage = "Please select first name")]
        public int WorkEmailFirstOption { get; set; }
        [Required(ErrorMessage = "Please select operator")]
        public int WorkEmailSecondOption { get; set; }
        [Required(ErrorMessage = "Please select last name")]
        public int WorkEmailThirdOption { get; set; }
        [Required(ErrorMessage = "Please enter Mail domain name")]
        [RegularExpression("@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid domain name")]
        public string EmailDomainName { get; set; }
    }

    public class ValidationSetting
    {
        public string className { get; set; }
        public int ValidationSettingID { get; set; }
        public string ValidationKey { get; set; }
        public bool IsMandatory { get; set; }
        public string ErrorMessage { get; set; }
        public string DisplayText { get; set; }
        public bool LoggerEnabled { get; set; }
    }
    public class EmailSettings
    {
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
    }
}
