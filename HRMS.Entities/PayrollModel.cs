﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class PayrollModel
    {
        public string EmployeeName { get; set; }

        public bool isPayActive { get; set; }

        public bool isActive { get; set; }

        public string CategoryName_1 { get; set; }

        public int EmployeeID { get; set; }

        public int? CategoryID { get; set; }

        public IEnumerable<SelectListItem> CategoriesList { get; set; }

        public string EmployeeAlternativeID { get; set; }

        public int? active { get; set; }

    }

    public class PayrollSettings
    {
        public int PayCycleID { get; set; }

        public int? DepartmentID { get; set; }

        public int? BankID { get; set; }

        public int NoOfDays { get; set; }

        public bool RunFinal { get; set; }

        public bool VacationMonth { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public IEnumerable<SelectListItem> bankList { get; set; }
        public IEnumerable<SelectListItem> cyclesList { get; set; }
        public IEnumerable<SelectListItem> departmenrList { get; set; }

        public List<PayCycle> paycyclesModelList { get; set; }

        public int? CategoryID { get; set; }
        public IEnumerable<SelectListItem> CategoriesList { get; set; }
        public PayrollSettings()
        {
            this.paycyclesModelList = new List<PayCycle>();
        }

    }

    public class PayrollVerificationSetting
    {

        public int PayrollVerificationID { get; set; }
        public string ModuleName { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class BankListReport
    {
        public int EmployeeID { get; set; }

        public double Amount { get; set; }

        public string WorkEmail { get; set; }

        public string PersonalEmail { get; set; }

        public string AccountNumber { get; set; }

        public string EmpName { get; set; }

        public string BankName { get; set; }

        public string Category { get; set; }

        public string PayCycleName { get; set; }
    }


    public class PaySlipReport
    {
        public int EmployeeID { get; set; }

        public string ItemDescr { get; set; }

        public double amount { get; set; }

        public string type { get; set; }

        public string AccountNumber { get; set; }

        public string Bank { get; set; }

        public string Department { get; set; }

        public string Name { get; set; }

    }

    public class PaySlipSummary
    {
        public int EmployeeID { get; set; }

        public int CategoryId { get; set; }

        public double additionTotal { get; set; }

        public double salaryTotal { get; set; }

        public double deductionTotal { get; set; }

        public int cycle { get; set; }

        public double net { get; set; }

        public string cycleName { get; set; }

        public string DateFrom { get; set; }

        public string DateTo { get; set; }

        public string paydate { get; set; }

        public string schoolname { get; set; }

        public bool paySlipType { get; set; }

        public string TeacherName { get; set; }

        public string EmployeeAlternativeID { get; set; }

        public SelectList CategoryList { get; set; }

        public SelectList EmployeeList { get; set; }

        public List<PaySlipReport> paySlipReport { get; set; }

    }

    public class EmployeeMailList
    {
        public int EmployeeID { get; set; }

        public int EmployeeAlternativeID { get; set; }

        public string EmployeeName { get; set; }

    }

    public class PayrollTemplateSetting
    {
        public int PayRollTemplateSettingsID { get; set; }
        public string SPName { get; set; }
        public string DisplayName { get; set; }
        public bool IsActive { get; set; }
        public bool IsPriority { get; set; }        
    }
    public class JvsTemplateSetting
    {
        public int JvsTemplateSettingsID { get; set; }
        public string SPName { get; set; }
        public string DisplayName { get; set; }
        public bool IsActive { get; set; }
        public string AccountCode { get; set; }
        public bool EnableCostCenters { get; set; }
    }

    public class PayrollGeneralSetting
    {
        public int NumberOfCycleDaysInMonth { get; set; }
        public int DigitAfterDecimal { get; set; }
        public int PaySlipTemplateID { get; set; }
        public bool CountSalaryPerHireDate { get; set; }
        public bool CountSalaryForWorkDays { get; set; }
        public bool RunFinalPayrollWithJvs { get; set; }
        public int BasicSalaryAllowanceID { get; set; }
        public bool PayrollEmpFullName { get; set; }
        public bool IsValidationForCycle { get; set; }

        public bool IsconfirmRunPayroll { get; set; }
    }

    public class PayrollConfirmationRequest
    {
        public int PayrollConfirmationId { get; set; }
        public int TypeID { get; set; }
        public string TypeName { get; set; }
        public string Date { get; set; }
        public string RequestEmployeeName { get; set; }
        public string RequestConfirmationEmployeeName { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsCancel { get; set; }
        public string ActionType { get; set; }
    }

    public class PayrollRequestDetailModel
    {
        public string EmployeeName { get; set; }
        public string ItemName { get; set; }
        public string ActionType { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public string TrDate { get; set; }
        public string HeaderName { get; set; }
        public string DateHeaderName { get; set; }
        public int Installments { get; set; }
    }

    public class EmployeeSalaryDetails
    {
        public int EmployeeID { get; set; }
        public double FullSalary { get; set; }
        public double PerDaySalary { get; set; }
        public double PerHourSalary { get; set; }
        public double PerMinuteSalary { get; set; }
        //_DA including Deductible Allowances Only
        public double Salary_DA { get; set; }
        public double PerDaySalary_DA { get; set; }
        public double PerHourSalary_DA { get; set; }
        public double PerMinuteSalary_DA { get; set; }
    }

}
