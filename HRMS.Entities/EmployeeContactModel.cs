﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeContactModel
    {
        public string EmployeeName { get; set; }
        public string  EmpAltId { get; set; }
        public int EmployeeContactID { get; set; }

        public Boolean WorkEmailType { get; set; }

        public Boolean MobileNoType { get; set; }

        public int EmployeeID { get; set; }

        [Required(ErrorMessage = "Please enter address.")]
        public string AddressLine1_1 { get; set; }

        [Required(ErrorMessage = "Please enter address.")]
        public string AddressLine1_2 { get; set; }

        public string AddressLine1_3 { get; set; }

        public string AddressLine2_1 { get; set; }

        public string AddressLine2_2 { get; set; }

        public string AddressLine2_3 { get; set; }

        public string AppartmentNumber { get; set; }

        [Required(ErrorMessage = "Please enter pobox.")]
        public string POBOX { get; set; }

        [Required(ErrorMessage = "Please select city.")]
        public int CityID { get; set; }

        [Required(ErrorMessage = "Please select area.")]
        public int AreaID { get; set; }

        [Required(ErrorMessage = "Please select country.")]
        public int CountryID { get; set; }

        [Display(Name = "E-Mail:")]
        [Required(ErrorMessage = "Please enter email address.")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address.")]
        public string WebAddress { get; set; }

        [Display(Name = "Phone Number:")]
        [Required(ErrorMessage = "Enter mobile number.")]
        //    [RegularExpression("^(([1-9]{1})([0-9]{9})+)$", ErrorMessage = "Enter valid mobile number")]

        [RegularExpression("^[0-9+-]+$", ErrorMessage = "Enter valid mobile number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Mobile Number:", GroupName = "Logger")]
        //[Required(ErrorMessage = "Enter mobile number")]
        //[RegularExpression("^(([1-9]{1})([0-9]{9})+)$", ErrorMessage = "Enter valid mobile number")]
        [RegularExpression("^[0-9+-]+$", ErrorMessage = "Enter valid mobile number.")]
        public string MobileNumberHome { get; set; }

        [Display(Name = "Landline:", GroupName = "Logger")]
        //[RegularExpression("^(([1-9]{1})([0-9]{9})+)$", ErrorMessage = "Enter valid mobile number")]
        //[RegularExpression("^[0-9+-]+$", ErrorMessage = "Enter valid mobile number")]
        public string MobileNumberWork { get; set; }

        public string Fax { get; set; }

        [Display(Name = "Work E-Mail:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter email address")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address.")]
        public string WorkEmail { get; set; }

        [Display(Name = "Personal E-Mail:", GroupName = "Logger")]
        //[Required(ErrorMessage = "Please enter email address.")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address.")]
        public string PersonalEmail { get; set; }

        public string Street { get; set; }

        public int CreatedBy { get; set; }

        public int ModifiedBy { get; set; }


        [Required(ErrorMessage = "Please enter email type.")]
        public string EmailTypeName { get; set; }

        public int EmailType { get; set; }

        [Display(Name = "IM Contact:", GroupName = "Logger")]
        public string IMContact { get; set; }

        [Display(Name = "IM Contact Type:", GroupName = "Logger")]
        public int IMContactType { get; set; }

        public int WebAddressType { get; set; }

        public int MobileNumberType { get; set; }

        public List<EmployeeMultipleEmails> EmployeeMultipleEmails { get; set; }

        public List<EmployeeMultipleIMs> EmployeeMultipleIMs { get; set; }

        public List<EmployeeMultipleContacts> EmployeeMultipleContacts { get; set; }

        public List<EmployeeMultiplePersonalEmails> EmployeeMultiplePersonalEmails { get; set; }

        public List<EmployeeMultiplePhoneNumberHome> EmployeeMultiplePhoneNumberHome { get; set; }

        public string IMTypeName { get; set; }
    }

    public class EmployeeMultipleEmails
    {
        public int EmpEmailId { get; set; }

        public int EmployeeID { get; set; }

        public string EmailName { get; set; }

        public int EmailType { get; set; }

        public string EmailTypeName { get; set; }
    }

    public class EmployeeMultiplePersonalEmails
    {
        public string EmailName { get; set; }
    }

    public class EmployeeMultiplePhoneNumberHome
    {
        public string Contact { get; set; }
        public int EmpContactId { get; set; }
    }

    public class EmployeeMultipleIMs
    {
        public int EmpIMId { get; set; }

        public int EmployeeID { get; set; }

        public string IMName { get; set; }

        public int IMType { get; set; }

        public string IMTypeName { get; set; }
    }

    public class EmployeeMultipleContacts
    {
        public int EmpContactId { get; set; }

        public int EmployeeID { get; set; }

        public string Contact { get; set; }

        public int ContactType { get; set; }

        public string ContactTypeName { get; set; }
    }


}
