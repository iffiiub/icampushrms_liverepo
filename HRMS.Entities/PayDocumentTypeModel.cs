﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayDocumentTypeModel
    {
        public int PayDocumentTypeID { get; set; }
 
        public string PayDocumentTypeDescription_1 { get; set; }
        public string PayDocumentTypeDescription_2 { get; set; }
        public string PayDocumentTypeDescription_3 { get; set; }
        
        public bool isactive { get; set; }
 
    }
}
