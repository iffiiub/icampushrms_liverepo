﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using HRMS.Entities.Helpers;

namespace HRMS.Entities
{
    public class EmployeeDetailsModel
    {
        private List<string> _selectedValues;
        private List<CheckListBoxItem> _items;

        public int EmployeeID { get; set; }

        public string NameTitle { get; set; }

        public int CompanyID { get; set; }

        [Display(Name = "Employee Alternative ID")]
        public string EmployeeAlternativeID { get; set; }


        [Display(Name = "Title:", GroupName = "Logger")]
        public int TitleID { get; set; }

        [Display(Name = "First Name:", GroupName = "Logger")]
        
        [Required(ErrorMessage = "Please enter first name.")]
        public string FirstName_1 { get; set; }

        [Display(Name = "First Name(AR):", GroupName = "Logger")]
        public string FirstName_2 { get; set; }

        [Display(Name = "First Name(FR):", GroupName = "Logger")]
        public string FirstName_3 { get; set; }

        public string FullName { get; set; }

        [Display(Name = "Last Name:", GroupName = "Logger")]        
        [Required(ErrorMessage = "Please enter surname.")]
        public string SurName_1 { get; set; }

        [Display(Name = "Last Name(AR):", GroupName = "Logger")]
        public string SurName_2 { get; set; }

        [Display(Name = "Last Name(FR):", GroupName = "Logger")]
        public string SurName_3 { get; set; }

        [Display(Name = "Middle Name:", GroupName = "Logger")]
        public string MiddleName_1 { get; set; }

        [Display(Name = "Middle Name(AR):", GroupName = "Logger")]
        public string MiddleName_2 { get; set; }

        [Display(Name = "Middle Name(FR):", GroupName = "Logger")]
        public string MiddleName_3 { get; set; }

        [Display(Name = "Mother Name:", GroupName = "Logger")]
        public string MothersName_1 { get; set; }

        [Display(Name = "Mother Name(AR):", GroupName = "Logger")]
        public string MothersName_2 { get; set; }

        [Display(Name = "Mother Name(FR):", GroupName = "Logger")]
        public string MothersName_3 { get; set; }

        [Display(Name = "Select Gender:")]
        [Required(ErrorMessage = "Please select gender.")]
        public int? GenderID { get; set; }

        [Display(Name = "Marital Status:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select marital status.")]
        public int MaritalStatusID { get; set; }

        [Display(Name = "Nationality(ies):", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select nationality.")]
        public string NationalityID { get; set; }

        [Display(Name = "Passport With Visa:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select Passport with Visa.")]
        public int DefaultNationalityID { get; set; }

        [Display(Name = "Nationality ID Card No:", GroupName = "Logger")]
        public string NationalityIDCardNo { get; set; }

        public List<CheckListBoxItem> Items
        {
            get { return _items ?? (_items = new List<CheckListBoxItem>()); }
            set { _items = value; }
        }

        // the name of this list should be the same as of the CheckBoxes otherwise you will not get any result after post
        public List<string> SelectedValues
        {
            get { return _selectedValues ?? (_selectedValues = new List<string>()); }
            set { _selectedValues = value; }
        }

        [Display(Name = "Religion:", GroupName = "Logger")]
        public int? ReligionID { get; set; }

        [Display(Name = "Language(s):", GroupName = "Logger")]
        public int? HomeLanguageID { get; set; }

        [Display(Name = "Communication Language:", GroupName = "Logger")]
        public int? CommunicationLanguageID { get; set; }

        [Display(Name = "Date Of Birth:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select birth date.")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string BirthDate { get; set; }
        [Display(Name = "Birth Place:")]
        public int? BirthPlaceID { get; set; }
        [Display(Name = "Birth Country:", GroupName = "Logger")]
        public int BirthCountryID { get; set; }

        public bool? isSpecialNeeds { get; set; }
        [Display(Name = "User")]
        public int? UserID { get; set; }
        [Display(Name = "Family")]
        public int? FamilyID { get; set; }

        public bool? isActive { get; set; }

        [Display(Name = "SIS User Type:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select SIS user type.")]
        public int? usertypeid { get; set; }

        public int? MailGenerated { get; set; }

        public bool? isOnlineActive { get; set; }

        public string InitialPassword { get; set; }

        public string ProfileImage { get; set; }

        public int CreatedBy { get; set; }

        public int ModifiedBy { get; set; }

        public bool? isTerminated { get; set; }

        public IEnumerable<SelectListItem> DepartmentEmployeeList { get; set; }

        public IEnumerable<SelectListItem> DepartmentEmployeeNotInDepartmentList { get; set; }

        public List<SpokenLanguages> SpokenLanguages { get; set; }
        [Display(Name = "Gender")]
        public string GenderName { get; set; }

        public string MaritalStatusName { get; set; }

        public EmployeeBasicDetails BasicDetails { get; set; }

        public string Password { get; set; }
        public string CompanyName { get; set; }
        // public string NormalPassword { get; set; }

    }

    public class SpokenLanguages
    {
        public int SpokeId { get; set; }

        public int EmployeeID { get; set; }

        public int LanguageId { get; set; }

        public string LanguageName { get; set; }

        [Display(Name = "Speak")]
        public bool IsSpeak { get; set; }

        [Display(Name = "Read")]
        public bool IsRead { get; set; }

        [Display(Name = "Write")]
        public bool IsWrite { get; set; }

        [Display(Name = "Native")]
        public bool IsNative { get; set; }

        public string DisplayLanguages { get; set; }
    }


    public class EmployeeBasicDetails
    {
        public int ID { get; set; }
        public string AlternativeId { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public string Department { get; set; }
    }
}
