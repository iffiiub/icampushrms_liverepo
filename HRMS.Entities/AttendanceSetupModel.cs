﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AttendanceSetupModel
    {
        public int AttendanceSetupID { get; set; }

        public string AttendanceSetupStart { get; set; }

        public string   AttendanceSetupEnd { get; set; }

        public bool sat { get; set; }

        public bool sun { get; set; }

        public bool tue { get; set; }

        public bool wed { get; set; }

        public bool thr { get; set; }   

        public bool fri { get; set; }

        public int LateMinutes { get; set; }

        public double DeductionPercent { get; set; }

        public string StartingTime { get; set; }

        public string EndigTime { get; set; }

        public int AbsentDeductionTypeID { get; set; }

        public int LateDeductionTypeID { get; set; }

        public int AbsentTypeID { get; set; }

        public int LateTypeID { get; set; }

        public int ReportApprover { get; set; }
    }
}
