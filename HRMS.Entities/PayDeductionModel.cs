﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayDeductionModel
    {
        public int PayDeductionID { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public int DeductionTypeID { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string EffectiveDate { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public int PaidCycle { get; set; }

        public bool IsInstallment { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Total Amount")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$", ErrorMessage = "Amount should be greater than 0")]
        public double Amount { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public double Installment { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string Comments { get; set; }

        public int EmployeeID { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string RefNumber { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string TransactionDate { get; set; }

        [Range(typeof(int), "0", "1000", ErrorMessage = "Enter Value must be a number ")]
        [Required(ErrorMessage = "This field is required.")]
        public int Days { get; set; }


        public int PvId { get; set; }

        public string GenerateDate { get; set; }

        public string DeductionType { get; set; }
        public double AmountLeft { get; set; }

        public bool IsProcessed { get; set; }
        public List<StudentFOSTBDetails> StudentDetails { get; set; }
        public bool IsFamilyBalanceDeduction { get; set; }
    }

    public class StudentFOSTBDetails
    {
        public int FamilyID { get; set; }
        public int DeductionStdID { get; set; }
        public int StudentID { get; set; }
        public string StudentFullName { get; set; }
        public decimal Amount { get; set; }
        public int PayDeductionID { get; set; }
        public string DeductionDate { get; set; }
        public bool IsRegistered { get; set; }
        public bool IsProcessed { get; set; }

    }
    public class multipleDeduction
    {
        public int employeeId { get; set; }
        public int cycle { get; set; }
        public decimal amount { get; set; }
        public string comments { get; set; }
        public int PayEmployeeLateId { get; set; }
    }

    public class DeductionSetting
    {
        public int FilterDateFormat { get; set; }
        public int AbsentDeductionTypeId { get; set; }
        public bool PostAbsentRecords { get; set; }
        public int LateDeductionTypeId { get; set; }
        public int EarlyDeductionTypeId { get; set; }
        public int GeneralDeductionTypeId { get; set; }
        public int TaxPayDeductionTypeId { get; set; }
        public int FamilyBalanceDeductionTypeID { get; set; }
    }

    public class AbsentDeductionTemplateSetting
    {
        public int TemplateID { get; set; }
        public string TemplateName { get; set; }
    }

    public class RuleDeductionSetting
    {
        public bool AppliedDeductionRules { get; set; }
        public int AbsentCountForCalculation { get; set; }
    }
}
