﻿using HRMS.Entities.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class YearModel : BaseModel
    {
        public int YearID { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "StartMonth", GroupName = "Logger")]
        public MonthsEnum StartMonth { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "EndMonth", GroupName = "Logger")]
        public MonthsEnum EndMonth { get; set; }

      //  public int Year { get; set; }
        //public int CompanyID { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "WeekStartDay", GroupName = "Logger")]
        public WeekdaysEnum WeekStartDay { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "WeekEndDay", GroupName = "Logger")]
        public WeekdaysEnum WeekEndDay { get; set; }


        public string StartMonthName { get; set; }
        public string EndMonthName { get; set; }

        public string WeekStartDayName { get; set; }
        public string WeekEndDayName { get; set; }
    }
}
