﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class JobTitle
    {

        public int JobTitleID { get; set; }
        [Required]
        [Display(Name = "Job Title Name(EN)", GroupName = "Logger")]
        public string JobTitleName_1 { get; set; }
        [Display(Name = "Job Title Name(AR)", GroupName = "Logger")]
        public string JobTitleName_2 { get; set; }
        [Display(Name = "Job Title Name(FR)", GroupName = "Logger")]
        public string JobTitleName_3 { get; set; }
        [Display(Name = "Short Code(EN)", GroupName = "Logger")]
        public string JobTitleShortName_1 { get; set; }
        [Display(Name = "Short Code(AR)", GroupName = "Logger")]
        public string JobTitleShortName_2 { get; set; }
        [Display(Name = "Short Code(FR)", GroupName = "Logger")]
        public string JobTitleShortName_3 { get; set; }

    }

    public class ContractTerm
    {
        public int EmployeeContractTermsID { get; set; }
        public string ContarctTermName { get; set; }
    }
}
