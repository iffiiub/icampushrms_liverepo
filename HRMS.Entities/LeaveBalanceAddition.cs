﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class LeaveBalanceAddition
    {
        public int LeaveAdditionID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Employee Name", GroupName = "Logger")]
        public int EmployeeID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Vacation Type Name", GroupName = "Logger")]
        public int VacationTypeID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Leave Balance", GroupName = "Logger")]
        //  public decimal AdditionalBalance { get; set; }
        public decimal FullPaidDays { get; set; }
        public decimal HalfPaidDays { get; set; }
        public decimal UnPaidDays { get; set; }
        public DateTime LeaveEffectiveDate { get; set; }
        public string FormattedLeaveEffectiveDate { get; set; }
        public string Comments { get; set; }
        public short ACYearID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool IsAddMode { get; set; }
        public string EmployeeName { get; set; }
        public string VacationTypeName { get; set; }
        public int?  VTCategoryID { get; set; }
    }
}
