﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
   public class PaySalarySlipModel
    {
        public List<EmployeeInfo> lstEmployeeInfo { get; set; }
    }
    public class EmployeeInfo
    {
        public string  EmployeeName { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmpWorkEmail { get; set; }
        public string HireDate { get; set; }
        public string PositionTitle { get; set; }
    }

}
