﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeShiftModel
    {
        public List<EmployeeDetailsModel> employeeInShift { get; set; }
        public List<EmployeeDetailsModel> employeeNotInShift { get; set; }
        public int ShiftID { get; set; }
        public EmployeeShiftModel()
        {
            this.employeeInShift = new List<EmployeeDetailsModel>();
            this.employeeNotInShift = new List<EmployeeDetailsModel>();
        }

      
    }
}
