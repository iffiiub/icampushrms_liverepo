﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class TerminatedEmployee
    {
        public int EmployeeId { get; set; }

        
        [Display(Name="Reason")]
        [Required(ErrorMessage = "Please enter reason")]
        [AllowHtml]
        public string Reason { get; set; }
        
        [Display(Name = "Termination Date")]
        [Required(ErrorMessage = "Please enter termination date")]
        public string TerminatedDate { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }



    }
}
