﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class SendMailLogModel
    {
        public int paySendEmailLogID { get; set; }
        public int employeeID { get; set; }
        public int cycleId { get; set; }
        public bool sendFlag { get; set; }
        public string employeeEmailId { get; set; }
        public string payslipLink { get; set; }

    }
}
