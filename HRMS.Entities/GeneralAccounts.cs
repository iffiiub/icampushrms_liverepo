﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class GeneralAccounts
    {
        public int id { get; set; }
        public string type { get; set; }

        public string payCategories { get; set; }
        public int payCategoriesId { get; set; }

        public string allowanceName { get; set; }
        public int allowanceId { get; set; }

        public string accountCode { get; set; }
        public int accountCodeId { get; set; }

        public string vacationAccountCode { get; set; }
        public int vacationAccountCodeId { get; set; }
       
        public int allowanceType { get; set; }

        public int autoID { get; set; }

    }
}
