﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeLeaveReasonModel : BaseModel
    {
        public int EmployeeLeaveReasonId { set; get; }
        public string EmployeeLeaveReason { set; get; }
    }
}
