﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using HRMS.Entities.Common;

namespace HRMS.Entities
{
    public class PayDirectDepositModel
    {

        public int DirectDepositID { get; set; }

        public int EmployeeID { get; set; }

        // [Required(ErrorMessage = "This field is required")]

        [Display(Name = "Account Number:", GroupName = "Logger")]
        public string AccountNumber { get; set; }

        [Display(Name = "Bank ID:", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int? BankID { get; set; }

        //  [Required(ErrorMessage = "This field is required")]

        [Display(Name = "Account Type:", GroupName = "Logger")]
        public int AccountTypeID { get; set; }

        // [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Employee Name As In Bank:", GroupName = "Logger")]        
        public string EmpName { get; set; }

        [Display(Name = "Category", GroupName = "Logger")]
        public int CategoryID { get; set; }

        [Display(Name = "Branch", GroupName = "Logger")]
        public int BranchID { get; set; }

        public decimal AmountBlocked { get; set; }

        [Display(Name = "First Month Salary Blocked:", GroupName = "Logger")]
        public bool FirstSalaryBlocked { get; set; }

        //[Required(ErrorMessage = "This field is required")]
        [Display(Name = "Account Date:", GroupName = "Logger")]
        public string AccountDate { get; set; }


        [Display(Name = "Cancellation Date:", GroupName = "Logger")]
        public string CancellationDate { get; set; }

        [Display(Name = "Active:", GroupName = "Logger")]
        public bool Active { get; set; }

        // [Required(ErrorMessage = "This field is required")]
        [LocalizedDynamicValidators(KeyName = "UNI/IBAN number")]
        //[StringLength(4, ErrorMessage = "Name must not be more than 4 char")]
        [Display(Name = "UNI/IBAN number:", GroupName = "Logger")]
        public string UniAccountNumber { get; set; }
        public bool IsAccountNoUseAsIBAN { get; set; }

        public PayBanksModel payBankModel { get; set; }

        public PayBankBranchModel payBankBranchModel { get; set; }

        public PayAccountTypesModel payAccountTypesModel { get; set; }

        public PayCategoriesModel payCategoriesModel { get; set; }



        public PayDirectDepositModel()
        {
            this.payBankModel = new PayBanksModel();

            this.payBankBranchModel = new PayBankBranchModel();

            this.payAccountTypesModel = new PayAccountTypesModel();

            this.payCategoriesModel = new PayCategoriesModel();

            this.payDirectDepositList = new List<PayDirectDepositModel>();
        }


        public IEnumerable<SelectListItem> BankList { get; set; }

        public IEnumerable<SelectListItem> BranchList { get; set; }

        public IEnumerable<SelectListItem> AccountTypeList { get; set; }

        public IEnumerable<SelectListItem> CategoriesList { get; set; }

        public List<PayDirectDepositModel> payDirectDepositList { get; set; }
    }
}
