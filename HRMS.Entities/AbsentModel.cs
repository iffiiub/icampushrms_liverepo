﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AbsentModel : BaseModel
    {
        public int PayEmployeeAbsentID { set; get; }
        public int PayEmployeeAbsentHeaderID { get; set; }
        public int EmployeeID { set; get; }

        public string EmployeeAltID { get; set; }
        public string PayEmployeeAbsentFromDate { set; get; }
        public string PayEmployeeAbsentToDate { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int PayAbsentTypeID { set; get; }
        public string EmployeeName { set; get; }
        public string PayAbsentTypeName { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int AbsentPaidTypeID { set; get; }
        public string AbsentPaidTypeName { set; get; }
        public bool IsMedicalCertificateProvided { set; get; }
        public bool isActive { set; get; }

        public bool IsDeducted { get; set; }
        public bool IsConfirmed { set; get; }
        public string ConfirmedDate { set; get; }
        public string GenerateDate { set; get; }
        public string InTime { set; get; }
        public string OutTime { set; get; }
        public string EntryDate { set; get; }
        public string EffectiveDate { set; get; }

        public int aTntTranMode { set; get; }
        public string AbsenceReason { set; get; }

        public int StatusID { set; get; }

        public string SelectedIds { set; get; }
        public string Comments { get; set; }

    }
}
