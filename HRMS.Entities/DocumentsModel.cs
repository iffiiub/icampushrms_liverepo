﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class DocumentsModel
    {
        public int SrNo { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int DocTypeId { get; set; }
        public string DocType { get; set; }
        public int DocId { get; set; }

        [Required(ErrorMessage = "This feild is Required")]
        public string DocNo { get; set; }
        public string ExpiryDate { get; set; }

        public string PersonalNumber { get; set; }
        public string IssueDate { get; set; }

        public string IssueCountry { get; set; }

        public string IssuePlace { get; set; }
        public string FilePath { get; set; }
        public DateTime dModifiedExpiryDate { get; set; }

        public int EmpId { get; set; }

        [Required(ErrorMessage = "This feild is Required")]
        public int IssueCityID { get; set; }
        public bool IsPrimary { get; set; }
        public string MotherName { get; set; }
        public bool IsPassportWithEmployee { get; set; }
        public string TakenDate { get; set; }
        public string Reason { get; set; }
        public int MustReturn { get; set; }
        public string ReturnDate { get; set; }
        public int SponsorID { get; set; }
        public string SponsorName { get; set; }
        public string Note { get; set; }
        public int HRContractTypeID { get; set; }

        [Required(ErrorMessage = "This feild is Required")]
        public int IssueCountryID { get; set; }
        public int MOLTitleId { get; set; }
        public string MOLTitle { get; set; }
        public string MOLPersonalID { get; set; }
        public string VAcc1 { get; set; }
        public string VAcc2 { get; set; }
        public string VAcc3 { get; set; }
        public string DocumentFile { get; set; }
        public string DocumentFileName { get; set; }
        public string UidNo { get; set; }
        public string SponsorPassportNumber { get; set; }
        public string WorkPermit { get; set; }
        public string EmployeeStatus { get; set; }
        public string Remarks { get; set; }
        public bool permitCancelled { get; set; }
        public string Cancellationdate { get; set; }
        public bool IsSponsoredBySchool { get; set; }
        public bool IsExpired { get; set; }
        public int EmpDeptId { get; set; }
        public string DepartmentName { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public byte[] ActualDocumentFile { get; set; }
        public string FileContentType { get; set; }
    }

    public class DocumentTypeModel
    {
        public int docTypeId { get; set; }
        public string docTypeName { get; set; }
        public bool IsActive { get; set; }
    }

    public class LabourContract
    {
        public int SrNo { get; set; }
        public string EmployeeName { get; set; }
        public string Nationality { get; set; }
        public string Profession { get; set; }
        public string VisaExpiryDate { get; set; }
        public string HealthCardNo { get; set; }
        public string HealthCardIssueDate { get; set; }
        public string HealthExpiryDate { get; set; }

        public int CompanyId { get; set; }
    }

    public class DocumentDaySettingModel
    {
        public int DocumentTypeId { get; set; }
        public string DocumentTypeName { get; set; }
        public int DocumentsAboutToExpireDays { get; set; }
        public int DocumentExpiredDays { get; set; }        

    }

    public class EmployeeDocumentModel {
        public int PayDocumentImageId { get; set; }
        public int PayDocumentId { get; set; }
        public string ImageName { get; set; }
        public byte[] DocumentFile { get; set; }
        public string FileContentType { get; set; }
    }
}
