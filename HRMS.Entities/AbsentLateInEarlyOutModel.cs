﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AbsentLateInEarlyOutModel : BaseModel
    {
        public int PayEmployeeLateID { get; set; }
        public int EmployeeID { set; get; }
        public string PayEmployeeLateDate { set; get; }

        [Required(ErrorMessage = "This field is required.")]
        public int PayLateTypeID { set; get; }
        public bool IsExcused { set; get; }
        public string InTime { set; get; }

        public string OutTime { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public int LateMinutes { set; get; }

        [Required(ErrorMessage = "This field is required.")]
        public int EarlyMinutes { set; get; }
        public bool IsDeducted { set; get; }
        public bool IsConfirmed { set; get; }
        public string ConfirmedDate { set; get; }
        public string GenerateDate { set; get; }
        public string EffectiveDate { set; get; }
        public int statusID { set; get; }
        public bool IsManual { get; set; }
        public string Status { get; set; }
        public string LateReason { get; set; }
    }
}
