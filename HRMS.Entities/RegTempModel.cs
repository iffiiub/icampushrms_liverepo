﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class RegTempModel : BaseModel
    {
        public int Reg_TempID { set; get; }
        public string Reg_TempName { set; get; }
    }
}
