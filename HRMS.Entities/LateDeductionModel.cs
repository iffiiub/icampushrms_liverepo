﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class LateDeductionModel
    {
        //[Required(ErrorMessage = "This field is required")]
        public string FromDate { get; set; }
        //[Required(ErrorMessage = "This field is required")]
        public string ToDate { get; set; }

        public bool MinimumSetup { get; set; }

        public int LateMinutes { get; set; }

        public double Amount { get; set; }

        public string EmployeeName { get; set; }

        public int EmployeeID { get; set; }

        public string EmployeeAlterNativeId { get; set; }

        public int DeductedMinutes { get; set; }

        public int AllMinutes { get; set; }

        public double TotalSalary { get; set; }

        public double DeductionPercent { get; set; }

        public bool IsConfirmed { get; set; }

        public string GenerateDate { get; set; }

        public string ConfirmedDate { get; set; }

        public bool IsGenerated { get; set; }

        public string EffectiveDate { get; set; }

        public bool isDeducted { get; set; }

        public int LateMinutesToDed { get; set; }

        public string Comments { get; set; }


        public AttendanceSetupModel attendanceSetupModel { get; set; }

        public List<LateDeductionModel> lateDeductionList { get; set; }

        public List<PayLateTypeModel> payLateTypeList { get; set; }

        public string AttendanceDate { get; set; }

        public string Time { get; set; }

        public bool ? excused { get; set; }
        public bool deduct  { get; set; }

        public int DeductionType { get; set; }

        public int DeductableMinutes { get; set; }

        public int PayEmployeeLateID { get; set; }

        public bool DelayToOtherWorkers { get; set; }
        public int CountLateInCycle { get; set; }
        public int CycleID { get; set; }
        public LateDeductionModel()
        {
            this.attendanceSetupModel = new AttendanceSetupModel();
            this.lateDeductionList = new List<LateDeductionModel>();
            this.payLateTypeList = new List<PayLateTypeModel>();
        }
        public string LateReason { get; set; }
    }

    public class LateDeductionSetting
    {
        public int LateMinutes { get; set; }
        public decimal DeductionPercent { get; set; }
        public int AbsentTypeID { get; set; }
        public int LateTypeID { get; set; }
        public int MinutesPerMonth { get; set; }
        public int DigitAfterDecimalForLate { get; set; }
        
    }
}
