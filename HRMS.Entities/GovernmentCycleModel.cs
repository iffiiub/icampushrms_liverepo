﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class GovernmentCycleModel
    {
        public int GovernmentCycleID { get; set; }
        
        [Display(Name="Government Cycle Name (EN):", GroupName = "Logger")]
        public string GovernmentCycleName_1 { get; set; }

        [Display(Name = "Government Cycle Name (AR):", GroupName = "Logger")]
        public string GovernmentCycleName_2 { get; set; }

        [Display(Name = "Government Cycle Name (FR):", GroupName = "Logger")]
        public string GovernmentCycleName_3 { get; set; }
    }
}
