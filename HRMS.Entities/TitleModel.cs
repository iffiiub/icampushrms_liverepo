﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class TitleModel
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
    }
}
