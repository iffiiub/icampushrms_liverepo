﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class ShortLeaveRequestModel : BaseModel
    {
        [Required(ErrorMessage = "This field is required.")]
        public int ShortLeaveID { set; get; }

        [Required(ErrorMessage = "This field is required.")]
        public int EmployeeID { set; get; }
        public string OrganizationName { set; get; }
        public string EmployeeAlternativeID { set; get; }
        public string FullName { set; get; }
        public string LeaveDate { set; get; }
        public string LeaveTime { set; get; }
        public string ReturnTime { get; set; }
        public string ActualLeaveTime { get; set; }
        public string ActualReturnTime { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public int LateMinutes { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int EarlyMinutes { set; get; }
        public int statusID { set; get; }
        public bool IsManual { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public bool ReturnNextDay { get; set; }
        public string ApprovalStatus { set; get; }
        public int ApproverId { set; get; }
        public int RequesterEmployeeID { set; get; }
        public string ReviewNote { set; get; }
    }

    public enum ApprovalStatus
    {
        Pending = 0,
        Approved = 1,
        Rejected = 2
    }
}
