﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.XMLResources.Abstract;
using HRMS.XMLResources.Concrete;
using System.Web;

namespace HRMS.XMLResources
{
    public class AnnualAppraisal_Resources
    {
        //private static IResourceProvider resourceProvider = new DbResourceProvider(); //  new XmlResourceProvider(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Resources.xml"));
        //private static XmlResourceProvider resourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/Resources.xml"));
        private static XmlResourceProvider AnnualAppraisalresourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/Languages/AnnualAppraisal_Resources.xml"));

        public static string GetStingValue(string VariableName)
        {

            return AnnualAppraisalresourceProvider.GetResource(VariableName, CultureInfo.CurrentUICulture.Name, "AnnualAppraisa") as String;

        }


        /// <summary>Add person</summary>
        public static string Validation12
        {
            get
            {
                return AnnualAppraisalresourceProvider.GetResource("Validation12", CultureInfo.CurrentUICulture.Name, "AnnualAppraisa") as String;
            }
        }
        /// <summary>Add person</summary>
        public static string VisitDate
        {
            get
            {
                return AnnualAppraisalresourceProvider.GetResource("VisitDate", CultureInfo.CurrentUICulture.Name, "AnnualAppraisa") as String;
            }
        }

        ///// <summary>Age</summary>
        public static string VisitTime
        {
            get
            {
                return AnnualAppraisalresourceProvider.GetResource("VisitTime", CultureInfo.CurrentUICulture.Name, "AnnualAppraisa") as String;
            }
        }

        ///// <summary>Must be between 10 and 130</summary>
        public static string DropInYear
        {
            get
            {
                return AnnualAppraisalresourceProvider.GetResource("DropInYear", CultureInfo.CurrentUICulture.Name, "AnnualAppraisa") as String;
            }
        }

        public static string SelectRating
        {
            get
            {
                return AnnualAppraisalresourceProvider.GetResource("SelectRating", CultureInfo.CurrentUICulture.Name, "AnnualAppraisa") as String;
            }
        }

    }
}
