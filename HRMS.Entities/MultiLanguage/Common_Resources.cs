﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.XMLResources.Abstract;
using HRMS.XMLResources.Concrete;
using System.Web;

namespace HRMS.XMLResources
{
    public class Common_Resources
    {
        //private static IResourceProvider resourceProvider = new DbResourceProvider(); //  new XmlResourceProvider(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Resources.xml"));
        //private static IResourceProvider resourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/KPI_Resources.xml"));




        private static XmlResourceProvider CommonResource = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/Languages/CommonResources.xml"));
        public static string GetStingValue(string VariableName)
        {
            
            return CommonResource.GetResource(VariableName, CultureInfo.CurrentUICulture.Name, "Common") as String;

        }

        public static string Required
        {
            get
            {
                return CommonResource.GetResource("Required", CultureInfo.CurrentUICulture.Name, "Common") as String;
            }
        }

        public static string Range_1
        {
            get
            {
                return CommonResource.GetResource("Range_1", CultureInfo.CurrentUICulture.Name, "Common") as String;
            }
        }
        public static string Range_2
        {
            get
            {
                return CommonResource.GetResource("Range_2", CultureInfo.CurrentUICulture.Name, "Common") as String;
            }
        }
    }
}
