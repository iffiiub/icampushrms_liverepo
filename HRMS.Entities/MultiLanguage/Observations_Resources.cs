﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.XMLResources.Abstract;
using HRMS.XMLResources.Concrete;
using System.Web;

namespace HRMS.XMLResources
{
    public class Observations_Resources
    {
        //private static IResourceProvider resourceProvider = new DbResourceProvider(); //  new XmlResourceProvider(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Resources.xml"));
        //private static XmlResourceProvider resourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/Resources.xml"));
        private static XmlResourceProvider ObservationsresourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/Languages/Observations_Resources.xml"));

        public static string GetStingValue(string VariableName)
        {

            return ObservationsresourceProvider.GetResource(VariableName, CultureInfo.CurrentUICulture.Name, "Observations") as String;

        }


        /// <summary>Add person</summary>
        public static string Validation12
        {
            get
            {
                return ObservationsresourceProvider.GetResource("Validation12", CultureInfo.CurrentUICulture.Name, "Observations") as String;
            }
        }
        /// <summary>Add person</summary>
        public static string VisitDate
        {
            get
            {
                return ObservationsresourceProvider.GetResource("VisitDate", CultureInfo.CurrentUICulture.Name, "Observations") as String;
            }
        }

        ///// <summary>Age</summary>
        public static string VisitTime
        {
            get
            {
                return ObservationsresourceProvider.GetResource("VisitTime", CultureInfo.CurrentUICulture.Name, "Observations") as String;
            }
        }

        ///// <summary>Must be between 10 and 130</summary>
        public static string DropInYear
        {
            get
            {
                return ObservationsresourceProvider.GetResource("DropInYear", CultureInfo.CurrentUICulture.Name, "Observations") as String;
            }
        }

        public static string SelectRating
        {
            get
            {
                return ObservationsresourceProvider.GetResource("SelectRating", CultureInfo.CurrentUICulture.Name, "Observations") as String;
            }
        }

        public static string ObsEvlComment
        {
            get
            {
                return ObservationsresourceProvider.GetResource("ObsEvlComment", CultureInfo.CurrentUICulture.Name, "Observations") as String;
            }
        }
    }
}
