﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.XMLResources.Entities;

namespace HRMS.XMLResources.Abstract
{
    public abstract class BaseResourceProvider
    {
        // Cache list of resources
        private static Dictionary<string, ResourceEntry> KPIresources = null;
        private static object KPIlockResources = new object();

        private static Dictionary<string, ResourceEntry> Commonresources = null;
        private static object CommonlockResources = new object();

        private static Dictionary<string, ResourceEntry> DropInsresources = null;
        private static object DropInslockResources = new object();

        private static Dictionary<string, ResourceEntry> Observationsresources = null;
        private static object ObservationslockResources = new object();

        private static Dictionary<string, ResourceEntry> AnnualAppraisalsresources = null;
        private static object AnnualAppraisalslockResources = new object();
        public BaseResourceProvider() {
            Cache = true; // By default, enable caching for performance
        }

        protected bool Cache { get; set; } // Cache resources ?
         public object GetResource(string name, string culture, string FormName)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Resource name cannot be null or empty.");

            if (string.IsNullOrWhiteSpace(culture))
                throw new ArgumentException("Culture name cannot be null or empty.");

            // normalize
            culture = culture.ToLowerInvariant();

            
            switch (FormName)
            {
                case "Common":
                    if (Cache && Commonresources == null)
                    {
                        // Fetch all resources

                        lock (CommonlockResources)
                        {

                            if (Commonresources == null)
                            {
                                Commonresources = ReadResources().ToDictionary(r => string.Format("{0}.{1}", r.Culture.ToLowerInvariant(), r.Name));
                            }
                        }
                    }

                    if (Cache)
                    {
                        return Commonresources[string.Format("{0}.{1}", culture, name)].Value;
                    }
                    break;
                case "KPIs":
                   
                    if (Cache && KPIresources == null)
                    {
                        // Fetch all resources

                        lock (KPIlockResources)
                        {

                            if (KPIresources == null)
                            {
                                KPIresources = ReadResources().ToDictionary(r => string.Format("{0}.{1}", r.Culture.ToLowerInvariant(), r.Name));
                            }
                        }
                    }

                    if (Cache)
                    {
                        return KPIresources[string.Format("{0}.{1}", culture, name)].Value;
                    }
                    break;
                case "DropIns":
                   
                    if (Cache && DropInsresources == null)
                    {
                        // Fetch all resources

                        lock (DropInslockResources)
                        {

                            if (DropInsresources == null)
                            {
                                DropInsresources = ReadResources().ToDictionary(r => string.Format("{0}.{1}", r.Culture.ToLowerInvariant(), r.Name));
                            }
                        }
                    }

                    if (Cache)
                    {
                        return DropInsresources[string.Format("{0}.{1}", culture, name)].Value;
                    }
                    break;
                case "Observations":

                    if (Cache && Observationsresources == null)
                    {
                        // Fetch all resources

                        lock (ObservationslockResources)
                        {

                            if (Observationsresources == null)
                            {
                                Observationsresources = ReadResources().ToDictionary(r => string.Format("{0}.{1}", r.Culture.ToLowerInvariant(), r.Name));
                            }
                        }
                    }

                    if (Cache)
                    {
                        return Observationsresources[string.Format("{0}.{1}", culture, name)].Value;
                    }
                    break;
                case "AnnualAppraisa":

                    if (Cache && AnnualAppraisalsresources == null)
                    {
                        // Fetch all resources

                        lock (AnnualAppraisalslockResources)
                        {

                            if (AnnualAppraisalsresources == null)
                            {
                                AnnualAppraisalsresources = ReadResources().ToDictionary(r => string.Format("{0}.{1}", r.Culture.ToLowerInvariant(), r.Name));
                            }
                        }
                    }

                    if (Cache)
                    {
                        return AnnualAppraisalsresources[string.Format("{0}.{1}", culture, name)].Value;
                    }
                    break;
            }

            

            return ReadResource(name, culture).Value;

        }
       
       

        /// <summary>
        /// Returns all resources for all cultures. (Needed for caching)
        /// </summary>
        /// <returns>A list of resources</returns>
        protected abstract IList<ResourceEntry> ReadResources();


        /// <summary>
        /// Returns a single resource for a specific culture
        /// </summary>
        /// <param name="name">Resorce name (ie key)</param>
        /// <param name="culture">Culture code</param>
        /// <returns>Resource</returns>
        protected abstract ResourceEntry ReadResource(string name, string culture);
        
    }
}
