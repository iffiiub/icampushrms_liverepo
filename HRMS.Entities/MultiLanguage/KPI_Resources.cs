﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.XMLResources.Abstract;
using HRMS.XMLResources.Concrete;
using System.Web;

namespace HRMS.XMLResources
{
    public class KPI_Resources
    {
        //private static IResourceProvider resourceProvider = new DbResourceProvider(); //  new XmlResourceProvider(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Resources.xml"));
        //private static IResourceProvider resourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/KPI_Resources.xml"));

        private static XmlResourceProvider resourceProvider = new XmlResourceProvider(HttpContext.Current.Server.MapPath("~/App_Data/Languages/KPI_Resources.xml"));
        public static string GetStingValue(string VariableName)
        {
            
            //return resourceProvider.GetKPIResource(VariableName, CultureInfo.CurrentUICulture.Name) as String;
            return resourceProvider.GetResource(VariableName, CultureInfo.CurrentUICulture.Name, "KPIs") as String;

        }
        /// <summary>First name</summary>
        public static string KPIYear
        {
            get
            {
                return resourceProvider.GetResource("KPIYear", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }
        /// <summary>First name</summary>
        public static string KPIYears
        {
            get
            {
                return resourceProvider.GetResource("KPIYears", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }
        /// <summary>Add person</summary>
        public static string KPIOrder
        {
            get
            {
                return resourceProvider.GetResource("KPIOrder", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }

        /// <summary>Age</summary>
        public static string KPIAreaEng
        {
            get
            {
                return resourceProvider.GetResource("KPIAreaEng", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }

        /// <summary>Must be between 10 and 130</summary>
        public static string KPIAreaArab
        {
            get
            {
                return resourceProvider.GetResource("KPIAreaArab", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }

        /// <summary>Age is required</summary>
        public static string KPIDescriptionEng
        {
            get
            {
                return resourceProvider.GetResource("KPIDescriptionEng", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }

        /// <summary>Biography</summary>
        public static string KPIDescriptionArab
        {
            get
            {
                return resourceProvider.GetResource("KPIDescriptionArab", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }

        /// <summary>Choose your language</summary>
        public static string KPIWeightage
        {
            get
            {
                return resourceProvider.GetResource("KPIWeightage", CultureInfo.CurrentUICulture.Name, "KPIs") as String;
            }
        }

        

    }
}
