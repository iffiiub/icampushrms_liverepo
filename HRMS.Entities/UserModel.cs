﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class UserModel
    {
        public int UserId { set; get; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string EmailID { get; set; }
        public string MobileNumber { get; set; }
        public string Password { get; set; }
        public int CompanyId { set; get; }
        public string ConfirmPassword { get; set; }
        public string Address { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public int UserRoleId { set; get; }
        public List<string> UserRoles { set; get; }
        public string ProfileImage { get; set; }
        public Boolean IsActive { get; set; }
        public string Permission { set; get; }
        public string ViewPagesSubCategoryIDs { set; get; }
        public string AddPagesSubCategoryIDs { set; get; }
        public string UpdatePagesSubCategoryIDs { set; get; }
        public string DeletePagesSubCategoryIDs { set; get; }
        public string OtherPermissionIDs { set; get; }
        public bool IsLoginSuccess { get; set; }
        public string LoginMessage { get; set; }
    }
}
