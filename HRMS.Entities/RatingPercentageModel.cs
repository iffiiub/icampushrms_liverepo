﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class RatingPercentageModel
    {
        public int RatingPercentageID { get; set; }
        public string RatingName_1 { get; set; }
        public string RatingName_2 { get; set; }
        public string RatingName_3 { get; set; }
        public decimal RatingFrom { get; set; }
        public decimal RatingTo { get; set; }
    }
}
