﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class InsuranceModel : BaseModel
    {
        public int InsuranceID { set; get; }
        public string PolicyNumber { get; set; }
        public string CardNumber { set; get; }
        public string IssueDate { set; get; }

         public string ExpiryDate { get; set; }
        public string Deduction { set; get; }

        [Required(ErrorMessage = "This field is required.")]
        public int InsuranceType { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int InsuranceEligibility { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int CatAmountID { get; set; }
        public string CancellationDate { get; set; }
        public bool Returned { set; get; }
        public int EmployeeID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public decimal TotalAmount { get; set; }
        public bool AddDeduction { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public bool? Insurance { set; get; }

        public bool InsuredBySchool { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public decimal TotalAmountBySchool { get; set; }
        
        public string Note { set; get; }
        public bool Deduct { set; get; }

        public string InsuranceTypeName { set; get; }

       




        
       


    }
}
