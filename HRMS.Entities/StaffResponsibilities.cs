﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class StaffResponsibilities : BaseModel
    {
        public string SrNo { set; get; }
        public string Name { set; get; }
        public string Section { set; get; }
        public string Position { set; get; }
        public string JATitle { set; get; }
        public string JAHireDate { set; get; }
    }

    public enum enumStaffType
    {
        [Description("Administration")]
        Administration = 1,
        [Description("Coordinators")]
        Coordinators,
        [Description("KG")]
        KG,
        [Description("Gr. 1-4")]
        Gr14,
        [Description("5-12 Girls")]
        Girls,
        [Description("5-12 Boys")]
        Boys
    }
}
