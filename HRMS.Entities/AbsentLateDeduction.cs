﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    #region Absent Deduction
    public class AbsentDeductionEmployeeModel
    {
        public int PayEmployeeAbsentID { get; set; }
        public int PayEmployeeAbsentHeaderID { get; set; }

        public int EmployeeID { get; set; }
        public string PayEmployeeAbsentDate { get; set; }
        public int PayAbsentTypeID { get; set; }
        public int LeaveTypeID { get; set; }
        public bool IsMedicalCertificateProvided { get; set; }
        public bool IsDeducted { get; set; }
        public bool IsConfirmed { get; set; }

        public string ConfirmedDate { get; set; }
        public string GenerateDate { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string EffectiveDate { get; set; }

        public string EmployeeName { get; set; }
        public AttendanceSetupModel attendanceSetupModel { get; set; }
        public List<LateDeductionModel> lateDeductionList { get; set; }
        public List<PayLateTypeModel> payLateTypeList { get; set; }
    }

    public class AbsentsForCurrentMonthModel
    {
        public string AbsentID { get; set; }
        public int Days { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmployeeID { get; set; }
        public int PayEmployeeAbsentHeaderID { get; set; }
        public decimal Amount { get; set; }
        public string EmployeeName { get; set; }
        public int PayAbsentTypeID { get; set; }
        public string TotalDedSalary { get; set; }
        public string AbsentTypeName { get; set; }
        public bool IsConfirmed { get; set; }
        public int PayEmployeeAbsentID { get; set; }
        public string EffectiveDate { get; set; }
        public Nullable<int> StatusID { get; set; }
        public string Status { get; set; }
        public int CycleID { set; get; }
        public int NoOfNullCycleCount { get; set; }
    }

    public class AbsentsForCurrentMonthExcusedModel : AbsentsForCurrentMonthModel
    {

    }

    //public class PayDeductionModel
    //{
    //    public int PayDeductionID { get; set; }
    //    public int DeductionTypeID { get; set; }
    //    public DateTime EffectiveDate { get; set; }
    //    public decimal PaidCycle { get; set; }
    //    public bool IsInstallment { get; set; }
    //    public decimal Amount { get; set; }
    //    public decimal Installment { get; set; }
    //    public string Comments { get; set; }
    //    public int EmployeeID { get; set; }
    //    public string RefNumber { get; set; }
    //    public DateTime TransactionDate { get; set; }
    //    public int PvId { get; set; }
    //}

    //public class PayDeductionDetailModel
    //{
    //    public int PayDeductionDetailID { get; set; }
    //    public int PayDeductionID { get; set; }
    //    public DateTime DeductionDate { get; set; }
    //    public decimal Amount { get; set; }
    //    public string Comments { get; set; }
    //    public bool IsActive { get; set; }
    //}
    #endregion

    #region Late Deduction
    public class LateDeductionEmployeeModel
    {
        public int EmployeeID { get; set; }
        public decimal Amount { get; set; }
        public decimal LateMinutes { get; set; }
        public string EmployeeName { get; set; }
        public decimal DeductedMinutes { get; set; }
        public decimal AllMinutes { get; set; }
        public decimal TotalSalary { get; set; }
        public int LateMinutesToDed { get; set; }
        public decimal DeductionPercent { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsGenerated { get; set; }
        public string GenerateDate { get; set; }
        public string ConfirmedDAte { get; set; }
        public string EffectiveDate { get; set; }
    }

    public class LateDeduction
    {
        public int LateMinutes { get; set; }
        public decimal DeductionPercent { get; set; }
    }

    #endregion
}
