﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class ChangePassword
    {
        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Old Password")]
        public string oldPassword { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "New Password should be of minimum length 6")]
        [MaxLength(12, ErrorMessage = "New Password should be of maximum length 12")]
        public string newPassword { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Confirm New Password")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "New Password should be of minimum length 6")]
        [MaxLength(12, ErrorMessage = "New Password should be of maximum length 12")]
        [Compare("newPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirmNewPassword { get; set; }
    }
}
