using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;

namespace HRMS.Entities.Common
{
    /// <summary>
    ///     ResourceManager class used to access localized content in XML files
    /// </summary>
    public sealed class ResourceManager
    {
        public static SqlConnection sqlConnection { get; set; }
        public static SqlCommand sqlCommand { get; set; }
        public static SqlDataReader sqlReader { get; set; }

        public static string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        //  public static List<ValidationSetting> lstValidation = GetValidationSetting();
        public static List<ValidationSetting> lstValidation { get; set; }
        private ResourceManager() { }

        /// <summary>
        ///     Retrieves the RFC 1766 name of the current culture
        /// </summary>
        /// <remarks>
        ///     This would more likely belong in an utility/global class, but for simplicities sake, I've put it here
        /// </remarks>
        public static string CurrentCultureName
        {
            get { return System.Threading.Thread.CurrentThread.CurrentCulture.Name; }
        }

     
        

        /// <summary>
        ///     Returns 
        /// </summary>
        /// <param name="keyPath" type="string">The name of the resource we want</param>
        /// </param>
        /// <remarks>
        ///   When in DEBUG mode, an ApplicationException will be thrown if the key isn't found
        /// </remarks>
        public static List<ValidationSetting> GetValidationSetting()
        {
            List<ValidationSetting> ValidationSetting = null;
            try
            {
                ValidationSetting = new List<ValidationSetting>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Select * from Hr_ValidationSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                ValidationSetting vs;
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        vs = new ValidationSetting();
                        vs.ValidationSettingID = Convert.ToInt32(sqlDataReader["ValidationSettingId"].ToString());
                        vs.ValidationKey = sqlDataReader["ValidationKey"].ToString();
                        vs.IsMandatory = Convert.ToBoolean(sqlDataReader["IsMandatory"].ToString() == "" ? "false" : sqlDataReader["IsMandatory"].ToString());
                        vs.ErrorMessage= sqlDataReader["ErrorMessage"].ToString();
                        ValidationSetting.Add(vs);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ValidationSetting;
        }
        
    }
}
