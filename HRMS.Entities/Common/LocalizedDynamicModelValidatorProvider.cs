﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using HRMS.Entities.Helpers;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace HRMS.Entities.Common
{

    public class LocalizedDynamicModelValidatorProvider : DataAnnotationsModelValidatorProvider
    {
        protected override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context, IEnumerable<Attribute> attributes)
        {
            string KeyName = string.Empty;
            string className = string.Empty;
            IList<Attribute> newAttributes = new List<Attribute>(attributes);
            LocalizedDynamicValidatorsAttribute resourceMapperAttr = attributes.FirstOrDefault(a => a is LocalizedDynamicValidatorsAttribute) as LocalizedDynamicValidatorsAttribute;

            if (resourceMapperAttr != null)
            {
                className = metadata.ContainerType.Name;
                KeyName = resourceMapperAttr.KeyName;                
            }

            if (!string.IsNullOrEmpty(KeyName))
            {
                ValidationSetting vs = LocalizedDisplayMsg.GetValidationSetting(className, KeyName);
                if (vs != null)
                {
                    if (vs.IsMandatory)
                    {
                        var required = new LocalizedRequiredAttribute(vs.ErrorMessage);
                        newAttributes.Add(required);                        
                    }
                }
            }

            return base.GetValidators(metadata, context, newAttributes);
        }

    }

    public class LocalizedDisplayName : DisplayNameAttribute
    {
        public LocalizedDisplayName(string resourcePath,bool logger)
            : base(GetMessageFromResource(resourcePath,logger))
        { }

        private static string GetMessageFromResource(string resourcePath,bool logger)
        {
            string[] strArr = Regex.Split(resourcePath, "\\.");
            ValidationSetting vs = LocalizedDisplayMsg.GetValidationSetting(strArr[0], strArr[1]);
            vs.LoggerEnabled = logger;
            return vs.DisplayText;
        }
    }
    ////*** F7

    public class PDRPCommonXMLResources : DisplayNameAttribute
    {
        public PDRPCommonXMLResources(string logger) : base(GetCommonPDRPResource(logger))
        { }

        public static string GetCommonPDRPResource(string logger)
        {
            string vs = HRMS.XMLResources.Common_Resources.GetStingValue(logger);
            return vs;
        }
    }
    public class PDRPKPIFormResources : DisplayNameAttribute
    {
        public PDRPKPIFormResources(string logger) : base(GetKPIFromResource(logger))
        { }

        public static string GetKPIFromResource(string logger)
        {
            //string[] strArr = Regex.Split(resourcePath, "\\.");
            //ValidationSetting vs = LocalizedDisplayMsg.GetPDRPValidationSetting(strArr[0], strArr[1]);
            string vs = HRMS.XMLResources.KPI_Resources.GetStingValue(logger);
            return vs;
        }
    }
   
    public class PDRPDropInsXMLResources : DisplayNameAttribute
    {
        public PDRPDropInsXMLResources(string logger) : base(GetResource(logger))
        { }

        public static string GetResource(string logger)
        {
            string vs = HRMS.XMLResources.DropIn_Resources.GetStingValue(logger);
            return vs;
        }
    }
    public class PDRPObservationsXMLResources : DisplayNameAttribute
    {
        public PDRPObservationsXMLResources(string logger) : base(GetResource(logger))
        { }

        public static string GetResource(string logger)
        {
            string vs = HRMS.XMLResources.Observations_Resources.GetStingValue(logger);
            return vs;
        }
    }
    public class PDRPAnnualAppraisalXMLResources : DisplayNameAttribute
    {
        public PDRPAnnualAppraisalXMLResources(string logger) : base(GetResource(logger))
        { }

        public static string GetResource(string logger)
        {
            string vs = HRMS.XMLResources.AnnualAppraisal_Resources.GetStingValue(logger);
            return vs;
        }
    }
}
