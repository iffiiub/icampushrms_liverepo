﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

// Custom namespaces


namespace HRMS.Entities.Common
{
    /// <summary>
    /// For custom numeric and range validation
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class LocalizedRequiredAttribute : RequiredAttribute
    {
        static LocalizedRequiredAttribute()
        {
            // necessary to enable client side validation
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRequiredAttribute), typeof(RequiredAttributeAdapter));
        }
        
        public LocalizedRequiredAttribute(string Message) : base()
        {
            ErrorMessage = Message;
        }
    } 
}
