﻿using System;

namespace HRMS.Entities.Common
{
    /// <summary>
    /// To attach dynamic validators as configured in resource file
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class LocalizedDynamicValidatorsAttribute : Attribute
    {
        public string KeyName
        {
            get; set;
        }

        public LocalizedDynamicValidatorsAttribute()
        {
        }
    }
}
