﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayEmployeeAbsentHeaderModel : BaseModel
    {
        public int PayEmployeeAbsentHeaderID { set; get; }
        public int EmployeeID { set; get; }
        public int PayAbsentTypeID { set; get; }
        public string EntryDate { set; get; }
        public bool Active { set; get; }
    }
}
