﻿using HRMS.Entities.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class ShiftModel : BaseModel
    {
        public int ShiftID { get; set; }
        public int ShiftDetailsId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string ShiftName { get; set; }
        public int ShiftDay { get; set; }
        public string shiftDayName { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "*")]
        public string ShiftGraceIn { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "*")]
        public string ShiftGraceOut { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "*")]
        public string ShiftBreakHours { get; set; }      
        public bool AutoSync { get; set; }
        public string CutOffTime { get; set; }
        public string CutOffTimeStart { get; set; }
        public string CutOffTimeEnd { get; set; }
        public bool IsWeekend { get; set; }
        public string ShiftBreakHoursStart { get; set; }
        public string ShiftBreakHoursEnd { get; set; }
        public bool IncludeInAttendance { get; set; }
        public List<ShiftModel> ObjShiftModelList { get; set; }
        public bool isSpecialShift { get; set; }
        public int ShiftSpecialID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public List<WeekdaysEnum> Days;

    }

    public class shiftChangeModel
    {
        public int ShiftID { get; set; }
        public int shiftDay { get; set; }
        public bool isChanged { get; set; }
    }
}
