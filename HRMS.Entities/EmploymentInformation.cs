﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmploymentInformation
    {

        public int EmploymentId { get; set; }

        public int EmployeeID { get; set; }

        [Display(Name = "Employee Code:")]
        [Required(ErrorMessage = "Please enter employee code.")]
        public string EmployeeCode { get; set; }

        [Display(Name = "Select Department:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select department.")]
        public int DepartmentID { get; set; }

        [Display(Name = "Select Position:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select position.")]
        public int PositionID { get; set; }

        [Display(Name = "Select Line Manager / Supervisor:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select supervisor.")]
        public int SuperviserID { get; set; }

        [Display(Name = "Wage Rate (Hourly):")]
        [Required(ErrorMessage = "Please enter wage rate.")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,5})?$", ErrorMessage = "Enter numbers only.")]
        public double WageRate { get; set; }

        [Display(Name = "Client Charge Rate (Hourly):", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter client charge rate.")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,5})?$", ErrorMessage = "Enter numbers only.")]
        public double ClientChargeRate { get; set; }

        [Display(Name = "Joining Date:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter joining Date.")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string HireDate { get; set; }

        [Display(Name = "Resignation Date:", GroupName = "Logger")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string ResignationDate { get; set; }


        [Display(Name = "Select Contract Terms:", GroupName = "Logger")]
        [Required(ErrorMessage = "Contract terms is required.")]
        public string EmployeeContractTerms { get; set; }

        [Display(Name = "Select Contract Terms:", GroupName = "Logger")]
        [Required(ErrorMessage = "Contract terms is required.")]
        public int EmployeeContractTermId { get; set; }
        [Display(Name = "Select Employment Mode:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select employee mode.")]
        public string EmployementMode { get; set; }


        [Display(Name = "Select Job Title:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please select job title.")]
        public int JobTitleID { get; set; }

        [Display(Name = "Salary Package:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter salary package.")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,5})?$", ErrorMessage = "Enter numbers only.")]
        public int SalaryPackage { get; set; }

        [Display(Name = "Select School House:", GroupName = "Logger")]
        public int SchoolHouse { get; set; }

        [Display(Name = "Overseas recruited:", GroupName = "Logger")]
        public bool isOverseasrecruited { get; set; }

        [Display(Name = "Qualification:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter qualification.")]
        public string Qualification { get; set; }

        [Display(Name = "Bank Name:")]
        [Required(ErrorMessage = "Please enter bank name.")]
        public string BankName { get; set; }

        [Display(Name = "Account Number:")]
        [Required(ErrorMessage = "Please enter account number.")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account Name:")]
        [Required(ErrorMessage = "Please enter account name.")]
        public string AccountName { get; set; }

        [Display(Name = "Bank Address:")]
        [Required(ErrorMessage = "Please enter bank address.")]
        public string BankAddress { get; set; }

        [Display(Name = "Switch BC Code:")]
        [Required(ErrorMessage = "Please enter switch bc code.")]
        public string SwitchBCCode { get; set; }

        [Display(Name = "Sort Code:")]
        [Required(ErrorMessage = "Please enter sort code.")]
        public string SortCode { get; set; }

        [Display(Name = "IBAN Code:")]
        [Required(ErrorMessage = "Please enter IBAN code.")]
        public string IBANCode { get; set; }

        [Display(Name = "Agent ID:")]
        [Required(ErrorMessage = "Please enter agent ID.")]
        public string AgentID { get; set; }

        [Display(Name = "Created By:")]
        public int CreatedBy { get; set; }

        [Display(Name = "Modified By:")]
        public int ModifiedBy { get; set; }

        [Display(Name = "Department Name:")]
        public string DepartmentName { get; set; }

        [Display(Name = "Position Name:")]
        public string PositionName { get; set; }

        [Display(Name = "Supervisor Name:")]
        public string SupervisorName { get; set; }

        [Display(Name = "Employment Mode Name:")]
        public string EmploymentModeName { get; set; }

        [Display(Name = "Job Title Name;")]
        public string JobTitleName { get; set; }

        [Display(Name = "Qualification Name:")]
        public string QualificationName { get; set; }

        [Display(Name = "Company ID:", GroupName = "Logger")]
        public int CompanyId { set; get; }

        [Display(Name = "Employee Section ID:", GroupName = "Logger")]
        public int EmployeeSectionID { set; get; }

        public string EmployeeSectionName { get; set; }

        [Display(Name = "Employee Job Category ID:", GroupName = "Logger")]
        public int EmployeeJobCategoryID { set; get; }

        [Display(Name = "Shift ID:", GroupName = "Logger")]
        public int ShiftID { set; get; }

        [Display(Name = "Termination Reason:", GroupName = "Logger")]
        [Required(ErrorMessage = "Please enter Leave Reason.")]
        public int LeftReasonID { set; get; }

        [Display(Name = "Group Name ID:")]
        public int GroupNameID { set; get; }

        [Display(Name = "Termination Date:", GroupName = "Logger")]
        //[Required(ErrorMessage = "Please enter Leave Date")]
        public string LeftDate { get; set; }

        //[Display(Name = "Left Reason")]
        //public string LeftReason { set; get; }

        [Display(Name = "MOE Title:", GroupName = "Logger")]
        public int MOETitle { set; get; }
        [Display(Name = "MOE Hire Date:", GroupName = "Logger")]
        public string MOEHireDate { set; get; }
        [Display(Name = "MOE Left Date:", GroupName = "Logger")]
        public string MOELeftDate { set; get; }

        [Display(Name = "MOL Title:", GroupName = "Logger")]
        public int MOLTitle { set; get; }
        [Display(Name = "MOL Start Date:", GroupName = "Logger")]
        public string MOLStartDate { set; get; }
        [Display(Name = "MOL Personal ID:", GroupName = "Logger")]
        public string MOLPersonalId { set; get; }


        public bool EmployeeStatus { get; set; }

        [Display(Name = "Employee Status:", GroupName = "Logger")]
        public string EmpStatus { get; set; }

        [Display(Name = "Nationality Type:", GroupName = "Logger")]
        public int EmployeenationalityType { get; set; }

        //Task#9118
        [Display(Name = "Notice Period (Months):", GroupName = "Logger")]
        public string NoticePeriod { get; set; }

        [Display(Name = "Job Grade:", GroupName = "Logger")]
        public Int16? JobGradeID { set; get; }
        [Display(Name = "Contract Status:", GroupName = "Logger")]
        public string ContractStatus { set; get; }
        [Display(Name = "Family Spouse:", GroupName = "Logger")]
        public string FamilySpouse { set; get; }
        [Display(Name = "Annual Air Ticket:", GroupName = "Logger")]
        public bool? AnnualAirTicket { set; get; }
        [Display(Name = "Health Insurance:", GroupName = "Logger")]
        public bool? HealthInsurance { set; get; }
               
        [Display(Name = "Life Insurance:", GroupName = "Logger")]
        public bool? LifeInsurance { set; get; }
        [Display(Name = "Salary Basis:", GroupName = "Logger")]
        public Int16? SalaryBasisID { set; get; }
        [Display(Name = "Cost Center:", GroupName = "Logger")]
        public string CostCenter { set; get; }
        [Display(Name = "Cost Center Code:", GroupName = "Logger")]
        public string CostCenterCode { set; get; }
        [Display(Name = "Location Code:", GroupName = "Logger")]
        public string LocationCode { set; get; }
        [Display(Name = "Office Location:", GroupName = "Logger")]
        public string OfficeLocation { set; get; }
        [Display(Name = "Probation Period (Months):", GroupName = "Logger")]
        public int? ProbationPeriod { set; get; }
        [Display(Name = "Leave Entitlement Days:", GroupName = "Logger")]
        public Int16? LeaveEntitleDaysID { set; get; }
        [Display(Name = "Leave Entitlement Type:", GroupName = "Logger")]
        public Int16? LeaveEntitleTypeID { set; get; }
        [Display(Name = "Probation Completion Date:", GroupName = "Logger")]
        public string ProbationCompletionDate { set; get; }
        [Display(Name = "Project Data:", GroupName = "Logger")]
        public string ProjectData { set; get; }

        [Display(Name = "Category:")]
        public int? RecCategoryID { get; set; }

        [Display(Name = "Job Status:")]
        public int? JobStatusID { get; set; }

        [Display(Name = "PDRP Form:")]
        public int? PDRPFormId { get; set; }

        [Display(Name = "Division:")]
        public int? DivisionID { get; set; }

        [Display(Name = "Expected Joining Date:")]
        public string JoiningDate { get; set; }

        [Display(Name = "Extension:")]
        public string Extension { get; set; }

        [Display(Name = "Offer Letter Position:")]
        public int OfferLetterPositionID { get; set; }

        [Display(Name = "InsuranceCategory:")]
        public int InsuranceCategory { get; set; }
        [Display(Name = "InsuranceEligibility:")]
        public int InsuranceEligibility { get; set; }
        [Display(Name = "Accommodation:")]
        public bool Accommodation { get; set; }
        [Display(Name = "AccommodationType:")]
        public int AccommodationType { get; set; }

    }

    public class EmployeeHireDateInformation
    {
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
        public string HireDate { get; set; }
    }
}
