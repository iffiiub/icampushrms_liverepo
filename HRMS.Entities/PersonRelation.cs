﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PersonRelation
    {
        public int RelationId { get; set; }

        [Required]
        public string RelationName { get; set; }
    }
}
