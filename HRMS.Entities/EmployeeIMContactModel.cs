﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeIMContactModel : BaseModel
    {
        public int IMContactId { set; get; }
        public int EmployeeId { set; get; }
        public string IMContact { set; get; }
        public string IMContactType { set; get; }
    }
}
