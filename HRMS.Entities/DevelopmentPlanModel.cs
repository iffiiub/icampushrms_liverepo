﻿using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HRMS.Entities
{
    public enum DevelopmentPlanTypes
    {
        Active,
        Completed,
        All
    }
    public enum DevelopmentGoalStatuses
    {
        NotStarted = 1,
        InProgress = 2,
        Behind = 3,
        SuccessfullyCompleted = 4,
        Dropped = 5
    }
    public class EmployeeDevelopmentPlanListModel //: BaseModel
    {
        public int ID { get; set; }
        public int CompanyID { get; set; }
        public int SupervisorID { set; get; }
        public int DepartmentID { set; get; }
        public int PositionID { set; get; }
        public int EmployeeTotalDevelopmentPlans { set; get; }
        public int EmployeeTotalActiveDevelopmentPlans { set; get; }
        public int EmployeeTotalCompletedDevelopmentPlans { set; get; }
        public int OracleNumber { set; get; }
        public string EmployeeAlternativeID { set; get; }
        public string CompanyName { set; get; }
        public string DepartmentName { set; get; }
        public string EmployeeName { get; set; }
        public string SuperviserName { get; set; }
    }
    public class EmployeeDevelopmentGoalListModel
    {
        public int OracleNumber { set; get; }
        public string EmployeeName { get; set; }
        public string RequestNumber { get; set; }
        public string DevelopmentGoalName { get; set; }
        public string SkillsCategory { get; set; }
        public string GoalCategory { get; set; }
        public string StartDate { set; get; }
        public string EndDate { set; get; }
        public string Status { set; get; }
    }
    public class CategoriesModel : BaseModel
    {
        public int DPCategoryId { get; set; }
        public string CategoryName_1 { get; set; }
        public string CategoryName_2 { get; set; }
        public string CategoryName_3 { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingCompetenciesModel
    {
        public int CompetencyID { get; set; }
        public int TrainingID { get; set; }
        public string TrainingName_1 { get; set; }
    }

    public class FormStatusModel : BaseModel
    {
        public int ID { get; set; }
        public string StatusName_1 { get; set; }
        public string StatusName_2 { get; set; }
        public string StatusName_3 { get; set; }
        public bool IsActive { get; set; }
    }

    public class FormMasterDataModel : BaseModel
    {
        public int HR_DPFormMasterDataTypeID { get; set; }
        public int HR_DPFormMasterDataID { get; set; }
        public string OptionName_1 { get; set; }
        public string OptionName_2 { get; set; }
        public string OptionName_3 { get; set; }
        public bool IsActive { get; set; }
    }
    public class MasterDataModel
    {
        public MasterDataModel()
        {
            LearningCategories = new List<FormMasterDataModel>();
            ActivityFormat = new List<FormMasterDataModel>();
        }
        public List<FormMasterDataModel> LearningCategories { get; set; }
        public List<FormMasterDataModel> ActivityFormat { get; set; }
    }

    public class DevelopmentGoalSearchModel
    {
        public int? EmployeeID { set; get; }
        public int? CompanyId { set; get; }
        public string StatusIds { set; get; }
    }
    public class DevelopmentGoalViewModel
    {
        public int ID { get; set; }
        public int RequesterEmployeeID { set; get; }
        public int OracleNumber { set; get; }
        public int EmployeeAlternativeID { set; get; }

        public int RequestNumber { set; get; }
        public int? PositionID { get; set; }
        public int? DepartmentID { get; set; }
        public int CompanyID { set; get; }
        public int ReqStatusID { set; get; }
        public int DPCategoryID { set; get; }
        public string CategoryName_1 { set; get; }
        public string LearningCategory { set; get; }
        public int? SectionRecordID { get; set; }
        public string SectionName { set; get; }
        public string GoalName { set; get; }
        public string EmployeeName { set; get; }
        public string GoalDescription { set; get; }
        public string StatusName_1 { set; get; }
        public int LearningCategoryId { get; set; }
        public string StartDate { get; set; }
        public string CompletionDate { get; set; }
        public int StatusID { get; set; }
    }
    public class DevelopmentGoalSessionModel
    {
        public DevelopmentGoalSessionModel()
        {
        }
        public DevelopmentGoalSessionModel(DevelopmentGoalSessionModel developmentGoalSessionModel, int? DevelopmentGoalID, int? EmployeeID, int? CompanyID, int? DepartmentID, int? PositionID, int? CategoryID, int? SectionID, string SectionName, string SelectedKPIORCompetency, int? AnnualAppraisalID, int? JobDesignationCompetencyID, bool? HaveLoadedFromCategoryPage)
        {
            this.EmployeeID = EmployeeID ?? developmentGoalSessionModel.EmployeeID;
            this.CompanyID = CompanyID ?? developmentGoalSessionModel.CompanyID;
            this.DevelopmentGoalID = DevelopmentGoalID ?? developmentGoalSessionModel.DevelopmentGoalID;
            this.PositionID = PositionID ?? developmentGoalSessionModel.PositionID;
            this.DepartmentID = DepartmentID ?? developmentGoalSessionModel.DepartmentID;
            this.CategoryID = CategoryID ?? developmentGoalSessionModel.CategoryID;
            this.SectionID = SectionID ?? developmentGoalSessionModel.SectionID;
            this.SectionName = SectionName ?? developmentGoalSessionModel.SectionName;
            this.KPICompetency = SelectedKPIORCompetency ?? developmentGoalSessionModel.KPICompetency;
            this.AnnualAppraisalID = AnnualAppraisalID ?? developmentGoalSessionModel.AnnualAppraisalID;
            this.HaveLoadedFromCategoryPage = HaveLoadedFromCategoryPage ?? false;
            this.JobDesignationCompetencyID = JobDesignationCompetencyID ?? developmentGoalSessionModel.JobDesignationCompetencyID;
        }
        public int? EmployeeID { get; set; }
        public int? CompanyID { get; set; }
        public int? DevelopmentGoalID { get; set; }
        public int? PositionID { get; set; }
        public int? DepartmentID { get; set; }
        public int? CategoryID { get; set; }
        public int? SectionID { get; set; }
        public int? AnnualAppraisalID { get; set; }
        public int? JobDesignationCompetencyID { get; set; }
        public string SectionName { get; set; }
        public string KPICompetency { get; set; }
        public bool HaveLoadedFromCategoryPage { get; set; }

    }
    public class DevelopmentGoalModel
    {
        public DevelopmentGoalModel()
        {
            this.DevelopmentGoalDetailNotes = new List<DevelopmentGoalDetailNotes>();
            this.Attachments = new List<Forms.AllFormsFilesModel>();
            this.DevelopmentGoalActionSteps = new List<DevelopmentGoalActionSteps>();
        }
        public int AnnualAppraisalID { set; get; }
        public int ID { get; set; }
        public string RequestNumber { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int RequesterEmployeeID { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int CompanyId { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int EmployeeID { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int ReqStatusID { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int DPCategoryID { set; get; }
        public int JobDesignationCompetencyID { set; get; }

        public int SectionRecordID { set; get; }
        public string SectionName { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        [Display(Name = "Goal Name")]
        public string GoalName { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string GoalDescription { set; get; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string KPICompetency { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int LearningCategoryId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int ActivityTitleId { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int ActivityFormatId { get; set; }

        public string Cost { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string Description { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string SuggestedProvider { get; set; }
        [Display(Name = "Start Date")]
        [Required(ErrorMessage = "This field is mandatory")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat, ApplyFormatInEditMode = true)]
        public string StartDate { get; set; }
        [Display(Name = "Completion Date")]
        [Required(ErrorMessage = "This field is mandatory")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public string CompletionDate { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int StatusID { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string SuccessMeasures { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string SupportAndResources { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string TrainingStatus { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        //[DisplayFormat(DataFormatString = "{0:dd/mm/yy}", ApplyFormatInEditMode = true)]
        public DateTime RequestedDate { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string EmployeeEmail { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int LMEmployeeID { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int SecondLMEmployeeID { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int DepartmentID { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public int PositionID { get; set; }
        public List<DevelopmentGoalDetailNotes> DevelopmentGoalDetailNotes { get; set; }
        public List<Forms.AllFormsFilesModel> Attachments { get; set; }
        public List<DevelopmentGoalAttachmentModel> GoalNoteDetails { get; set; }
        public List<DevelopmentGoalActionSteps> DevelopmentGoalActionSteps { get; set; }
    }
    public class DevelopmentGoalAttachmentModel
    {
        public int ID { set; get; }
        public int FileID { set; get; }
        public int RowID { set; get; }
        public string Notes { set; get; }
    }
    public class DevelopmentGoalActionSteps
    {
        public int ID { set; get; }
        public int DevelopmentGoalID { set; get; }
        public string ActionStepDescription { set; get; }
    }

    public class DevelopmentGoalDetailNotes
    {
        public int ID { set; get; }
        public int DevelopmentGoalID { set; get; }
        public string Notes { set; get; }
        public int FileID { set; get; }
    }
    public class JDDesignationCompetenciesModel
    {
        public int JDDesignationCompetencyID { set; get; }
        public int PositionID { set; get; }
        public int JobDescriptionID { set; get; }
        public int CompetencyID { set; get; }
        public string Competency { set; get; }
        public string JobDescription { set; get; }
    }

    public class DevelopmentPlanKPIModel
    {
        public int KPIId { set; get; }
        public int KPIYear { set; get; }
        public int CompanyID { set; get; }
        public string KPIAreaEng { set; get; }
        public string KPIDescriptionEng { set; get; }
        public string KPIAreaArab { set; get; }
        public string KPIDescriptionArab { set; get; }
        public int KPIWeightage { set; get; }
        public int KPIOrder { set; get; }
    }

    public class DevelopmentPlanPDRPModel
    {
        public int EmployeeId { get; set; }
        public int CompanyId { get; set; }
        public int UserTypeId { get; set; }
        public int AnnualAppraisalID { get; set; }
        public int PerformanceGroupId { get; set; }

        public DevelopmentPlanPDRPModel()
        {
            this.FormKPIModelList = new List<AnnualGoalSettingFormModel>();
            this.ProfessionalCompetencies = new List<ProfessionalCompetencies>();
            this.AppraisalKPIsListModel = new AppraisalKPIsListModel();
            this.BehavioralCompetencies = new List<BehavioralCompetencies>();
            this.AppraisalTeachingModel = new AppraisalTeachingModel();
        }
        /// <summary>
        /// Business Tagests Model
        /// </summary>
        public List<AnnualGoalSettingFormModel> FormKPIModelList { get; set; }



        /// Professional Competencies
        public List<ProfessionalCompetencies> ProfessionalCompetencies { get; set; }
        /// Behaviour competencies
        public List<BehavioralCompetencies> BehavioralCompetencies { get; set; }
        public AppraisalKPIsListModel AppraisalKPIsListModel { get; set; }
        public AppraisalTeachingModel AppraisalTeachingModel { get; set; }
    }

    public class DevelopmentCompetency
    {
        public int CompentencyID { set; get; }
        public string Competencytitle_1 { set; get; }
    }
}
