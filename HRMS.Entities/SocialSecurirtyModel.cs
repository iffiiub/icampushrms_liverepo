﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class SocialSecurirtyModel
    {
        public int SSID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAltId { get; set; }
        public string EmployeeName { get; set; }
        public decimal Percentage { get; set; }
        public decimal Amount { get; set; }
        public decimal Salary { get; set; }
    }
}
