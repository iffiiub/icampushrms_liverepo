﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class InternalEmploymentModel : BaseModel
    {
        public int InternalEmploymentID { get; set; }

        [Display(Name = "Employee")]
        public int EmployeeID { get; set; }

        [Display(Name = "Location")]
        [Required(ErrorMessage = "This field is required.")]
        public int LocationID { get; set; }

        [Display(Name = "Location Name")]
        public string LocationName { get; set; }

        [Display(Name = "Department")]
        [Required(ErrorMessage = "This field is required.")]
        public int DepartmentID { get; set; }

        [Display(Name = "Department Name", GroupName = "Logger")]
        
        public string DepartmentName { get; set; }

        [Display(Name = "Position", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int PositionID { get; set; }

        [Display(Name = "Position Name")]
        public string PositionName { get; set; }

        [Display(Name = "Start Date", GroupName = "Logger")]
         
        public string StartDate { get; set; }

        [Display(Name = "End Date", GroupName = "Logger")]
        public string EndDate { get; set; }

        [Display(Name = "Comments", GroupName = "Logger")]
        public string Comments { get; set; }

        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Modified By")]
        public int ModifiedBy { get; set; }

        [Display(Name = "Modified On")]
        public DateTime ModifiedOn { get; set; }



        public IEnumerable<SelectListItem> LocationList { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        public IEnumerable<SelectListItem> PositionList { get; set; }

        public int AttatchmentId { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        public string Description { get; set; }
        [Display(Name = "File Size")]
        public string FileSize { get; set; }
        [Display(Name = "File Type")]
        public string FileType { get; set; }
        [Display(Name = "File Path")]
        public string FilePath { get; set; }

        public string FirstName_1 { get; set; }
        public string SurName_1 { get; set; }

        public LocationModel locationModel { get; set; }
        public DepartmentModel departmentModel { get; set; }
        public PositionModel positionModel { get; set; }
        public StateModel stateModel { get; set; }
        public CountryModel countryModel { get; set; }
        public CityModel cityModel { get; set; }


        public List<InternalEmploymentModel> objInternalEmploymentList { get; set; }

        //List<PositionModel> positionList { get; set; }
        //List<LocationModel> LocationList { get; set; }
        //List<DepartmentModel> DepartmentList { get; set; } 
        public InternalEmploymentModel()
        {

            this.objInternalEmploymentList = new List<InternalEmploymentModel>();
            //this.locationModel = new LocationModel();
            //this.departmentModel = new DepartmentModel();
            //this.positionModel = new PositionModel();
            //this.countryModel = new CountryModel();
            //this.stateModel = new StateModel();
            //this.cityModel = new CityModel();

        }
    }
}
