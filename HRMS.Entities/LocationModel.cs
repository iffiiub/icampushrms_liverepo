﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using HRMS.Entities.ViewModel;

namespace HRMS.Entities
{
    public class LocationModel : BaseModel
    {

        public int LocationID { get; set; }

        public int LocationCompanyID { get; set; }

        public string LocationName { get; set; }

        [Display(Name = "Country:")]
        [Required(ErrorMessage = "Please select country")]
        
        public int CountryID { get; set; }

        [Display(Name = "City:")]
        [Required(ErrorMessage = "Please select city")]
        
        public int CityID { get; set; }

        [Display(Name = "State:")]
        [Required(ErrorMessage = "Please select state")]
        
        public int StateID { get; set; }

        [Display(Name = "Email:")]
        [Required(ErrorMessage = "Please enter email")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address")]
        public string LocationEmail { get; set; }

        [Display(Name = "Phone:")]
        [RegularExpression("^(([1-9]{1})([0-9]{9})+)$", ErrorMessage = "Enter valid mobile number")]
        [Required(ErrorMessage = "Please enter phone")]
        public string Phone { get; set; }


        [Display(Name = "Fax:")]        
        public string Fax { get; set; }

        [Display(Name = "Post Code:")]
        [Required(ErrorMessage = "Please enter post code")]
        public string PostCode { get; set; }

        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }

        public List<EmployeeDetailsModel> employeeInLocation { get; set; }

        public LocationModel()
        {
            this.employeeInLocation = new List<EmployeeDetailsModel>();
        }
    }
}
