﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AttShiftTableModel
    {
        public int UserShiftId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeAlternativeId { get; set; }
        public string EmployeeName { get; set; }
        public string Day { get; set; }
        public string AttDate { get; set; }
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public int GreaceIn{ get; set; }
        public int GreaceOut { get; set; }
        public bool OffDays { get; set; }
        public bool Posted { get; set; }
        public bool IsUserShiftUpdated { get; set; }
    }
}
