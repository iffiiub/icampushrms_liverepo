﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeAccountInfoModel
    {
        // To get Roles list


        private List<string> _selectedValues;
        private List<CheckListBoxItem> _items;

        public int EmpAccInfoId { get; set; }

        public int EmployeeID { get; set; }

        [Display(Name = "Roles")]
        public string RolesID { get; set; }

        [Display(Name = "Account Status")]
        public int Status { get; set; }

        public List<CheckListBoxItem> Items {
            get { return _items ?? (_items = new List<CheckListBoxItem>()); }
            set { _items = value; } 
        }

        // the name of this list should be the same as of the CheckBoxes otherwise you will not get any result after post
        public List<string> SelectedValues
        {
            get { return _selectedValues ?? (_selectedValues = new List<string>()); }
            set { _selectedValues = value; }
        }

        // Constructors
        //public EmployeeAccountInfoModel() { }

        //public EmployeeAccountInfoModel(List<AccessRoleModel> roleModel)
        //{
        //    if (roleModel != null)
        //    {
        //        foreach (var item in roleModel)
        //            Items.Add(new CheckListBoxItem { Text = item.RoleName, Value = item.AccessRoleId.ToString() });
        //    }
        //    else
        //        Items = new List<CheckListBoxItem>();
        //}

        public string StatusName { get; set; }

        public string RoleNames { get; set; }
    }

    // represents single check box item
    public class CheckListBoxItem
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }


}
