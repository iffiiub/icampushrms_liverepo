﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AreaModel
    {
        public int AreaId { get; set; }

        public string AreaName { get; set; }

        public int CityId { get; set; }
    }
}
