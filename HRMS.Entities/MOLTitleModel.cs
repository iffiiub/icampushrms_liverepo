﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class MOLTitleModel : BaseModel
    {
        public int MOLTitleID { get; set; }

        [Display(Name = "MOL Title Name (EN):")]
        [Required(ErrorMessage = "Please enter mol title")]
        public string MOLTitleName_1 { get; set; }

        [Display(Name = "MOL Title Name (AR):")]
        public string MOLTitleName_2 { get; set; }

        [Display(Name = "MOL Title Name (FR):")]
        public string MOLTitleName_3 { get; set; }

        [Display(Name = "Short Code (EN):")]
        public string MOLTitleShortName_1 { get; set; }

        [Display(Name = "Short Code (AR):")]
        public string MOLTitleShortName_2 { get; set; }

        [Display(Name = "Short Code (FR):")]
        public string MOLTitleShortName_3 { get; set; }
    }
}
