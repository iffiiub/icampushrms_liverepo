﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class ProfileModel : BaseModel
    {
        public int ProfileId { get; set; }
      
        [Display(Name = "Profile Name")]
        [Required(ErrorMessage = "Required")]
        public string ProfileName { get; set; }

        [Display(Name = "Session Timeout(Minutes)")]
        [Required(ErrorMessage = "Required")]
        [Range(1, 10000, ErrorMessage = "Please enter valid timeout")]
        public int SessionTimeout { get; set; }
        

        public string checkids { get; set; } 
        
    }
}
