﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class WPSModel
    {
        public List<PayBanksModel> BankList { get; set; }
        public List<PayCycleModel> CycleList { get; set; }
        public List<WPSTemplateSettingsModel> TemplatesList { get; set; }
        public string BankIds { get; set; }
        public int CycleID { get; set; }
        public int TemplateID { get; set; }
        public string EstablishNumber { get; set; }
        public string BankCodeEmployer { get; set; }
        public int? CategoryID { get; set; }
        public IEnumerable<SelectListItem> CategoriesList { get; set; }
    }

    public class WPSTemplateSettingsModel
    {
        public int TemplateID { get; set; }
        public string TemplateName { get; set; }
        public string SPName { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
        public bool IsEnabled { get; set; }
        public int ExcelTableStartIndex { get; set; }
        public int MergeHeadersExcelCellsCount { get; set; }
        public int ExcelColumnNameRowNo { get; set; }
        public bool IsSpecificSIFFileFormat { get; set; }
        public bool IsFixedTemplate { get; set; }

}
    public class WPSSettingsModel
    {
        public bool IsHideExtraFilter { get; set; }

    }

    public class Template_WPS
    {
        public string EmpId { get; set; }
        public string Employee_Name { get; set; }
        public string Employee_Type { get; set; }
        public string GroupByColumn { get; set; }
        public string Salary { get; set; }
        public string VariablePay { get; set; }
        public string AccountNo { get; set; }
        public string AGENT_BANK_RTN_CODE { get; set; }
        public string MOL_PERSONID { get; set; }
        public string Sal_Month { get; set; }
        public string Sal_Year { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Leave { get; set; }       
    }
    public class Template_NonWPS
    {
        public string BankCode { get; set; }
        public string TranType { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryACIBAN { get; set; }
        public string AmountInAED { get; set; }
        public string PaymentDetails { get; set; }
        public string GroupByColumn { get; set; }
    }
    public class Template_NonWPSCBD
    {
        public string EmpId { get; set; }
        public string Employee_Name { get; set; }
        public string BankAccount { get; set; }
        public string Salary { get; set; }
        public string GroupByColumn { get; set; }
    }
    
}
