﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class PayDeductionTypeModel
    {
        public int DeductionTypeID { get; set; }

        [Display(Name = "DeductionTypeName(EN)", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string DeductionTypeName_1 { get; set; }

        [Display(Name = "DeductionTypeName(AR)", GroupName = "Logger")]
        public string DeductionTypeName_2 { get; set; }

        [Display(Name = "DeductionTypeName(FR)", GroupName = "Logger")]
        public string DeductionTypeName_3 { get; set; }

        [Display(Name = "Short Code (EN)", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string DeductionTypeShortName_1 { get; set; }

        [Display(Name = "Short Code (AR)", GroupName = "Logger")]
        public string DeductionTypeShortName_2 { get; set; }

        [Display(Name = "Short Code (FR)", GroupName = "Logger")]
        public string DeductionTypeShortName_3 { get; set; }
       

        [Display(Name = "Transaction Date", GroupName = "Logger")]       
        public string TransactionDate { get; set; }

        public bool IsActive { get; set; }

        [Display(Name = "Sequence", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int Sequence { get; set; }

        [Display(Name = "Acount code", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string acc_code { get; set; }

        [Display(Name = "Vacation acount code", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string VacationAccountCode { get; set; }

        public bool IsExcludeInJV { get; set; }

        public decimal CumulativeDeduction { set; get; }
        public bool IsFamilyBalanceDeduction { get; set; }
    }

    public class DeductionRule
    {
        public int DeductionRuleID { get; set; }
        public string DeductionRuleDesc { get; set; }
        public double FirstTimeDeduction { get; set; }
        public double SecondTimeDeduction { get; set; }
        public double ThirdTimeDeduction { get; set; }
        public double FourthTimeDeduction { get; set; }
        public bool isAbsentRule { get; set; }
        public bool isEarlyRule { get; set; }
        public int StartMinute { get; set; }
        public int EndMinute { get; set; }
        public bool DelayToOtherWorkers { get; set; }
    }
    public class EmployeeGeneralDeduction
    {
        public int EmployeeGeneralDeductionID { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeAltID { get; set; }
        public int DeductionRuleId { get; set; }
        public string DeductionDate { get; set; }
        public decimal DeductionAmount { get; set; }
        public string EmployeeName { get; set; }
        public string DeductionRuleDesc { get; set; }
        public bool isConfirmed { get; set; }
    }
}
