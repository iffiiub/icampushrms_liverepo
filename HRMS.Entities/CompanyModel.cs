﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HRMS.Entities
{
    public class CompanyModel //: BaseModel
    {
        public int CompanyId { get; set; }
        public string AlternativeCompanyID { get; set; }
        [LocalizedDisplayName("CompanyModel.name", true)]
        [LocalizedDynamicValidators(KeyName = "name")]
        public string name { get; set; }
        public string Name_2 { get; set; }
        public string Name_3 { get; set; }
        public string OrganizationCode { get; set; }
        public string ShortName_1 { get; set; }
        public string ShortName_2 { get; set; }
        public string ShortName_3 { get; set; }
        public string EmailId { get; set; }

        public string ContactPersonName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address_3 { get; set; }
        [LocalizedDisplayName("CompanyModel.City", true)]
        [LocalizedDynamicValidators(KeyName = "City")]
        public int City { get; set; }
        [LocalizedDisplayName("CompanyModel.Country", true)]
        [LocalizedDynamicValidators(KeyName = "Country")]
        public int Country { get; set; }
        [LocalizedDisplayName("CompanyModel.POBOX", true)]
        [LocalizedDynamicValidators(KeyName = "POBOX")]
        public string POBOX { get; set; }
        public string Phone { get; set; }
        public string Phone_2 { get; set; }
        public string Phone_3 { get; set; }
        public string Fax_1 { get; set; }
        public string Fax_2 { get; set; }
        public string Fax_3 { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [LocalizedDisplayName("CompanyModel.CompanyHead", true)]
        [LocalizedDynamicValidators(KeyName = "CompanyHead")]
        public int CompanyHead { get; set; }
        [LocalizedDisplayName("CompanyModel.Assistant1", true)]
        public int Assistant1 { get; set; }
        [LocalizedDisplayName("CompanyModel.Assistant2", true)]
        public int Assistant2 { get; set; }
        [LocalizedDisplayName("CompanyModel.Assistant3", true)]
        public int Assistant3 { get; set; }
        public string CampusName_1 { get; set; }
        public string CampusName_2 { get; set; }
        public string CampusName_3 { get; set; }
        public bool SchoolCompany { get; set; }
        public int CompanyTypeID { get; set; }
       
    }

    public class CompanyTypeModel
    {
        public int CompanyTypeID { get; set; }
        public string CompanyTypeName { get; set; }
    }
    public class OrganizationModel
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public int EmployeeID { get; set; }
    }
}
