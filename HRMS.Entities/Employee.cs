﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using System.Web.Mvc;
//using System.Web.Mvc;

namespace HRMS.Entities
{
    public class Employee : BaseModel
    {
        public Employee()
        {
            this.employeeDetailsModel = new EmployeeDetailsModel();
            this.employeeContactModel = new EmployeeContactModel();
            this.employmentInformation = new EmploymentInformation();
            this.genderModel = new GenderModel();
            this.employeeAddressInfoModel = new EmployeeAddressInfoModel();
            this.employeeBankAccountInfoModel = new EmployeeBankAccountInfoModel();
            this.employeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();
            this.employeeAccountInfoModel = new EmployeeAccountInfoModel();
            this.payDirectDepositModel = new PayDirectDepositModel();
            this.userGroupModel = new UserGroupModel();
        }
        public int EmployeeId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Required")]
        public string LastName { get; set; }

        public string ShiftName { get; set; }
        public int? ShiftId { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNo { get; set; }

        public int IsActive { get; set; }

        public string FullName { get; set; }

        public string EmployeeAlternativeID { get; set; }

        public string PositionName { get; set; }


        public EmployeeDetailsModel employeeDetailsModel { get; set; }
        public EmployeeContactModel employeeContactModel { get; set; }
        public List<EmployeeDependantModel> employeeDependentModel { get; set; }
        public EmploymentInformation employmentInformation { get; set; }
        public EmployeeAddressInfoModel employeeAddressInfoModel { get; set; }
        public EmployeeBankAccountInfoModel employeeBankAccountInfoModel { get; set; }
        public EmployeePersonalIdentityInfoModel employeePersonalIdentityInfoModel { get; set; }
        public EmployeeAccountInfoModel employeeAccountInfoModel { get; set; }

        public UserGroupModel userGroupModel { get; set; }
        public PayDirectDepositModel payDirectDepositModel { get; set; }
        public List<InternalEmploymentModel> employeeInternalEmploymentModel { get; set; }
        public List<PastEmploymentModel> employeePastEmploymentModel { get; set; }

        public List<ChildrenDetails> childrenList { get; set; }

        public List<GenderModel> genderList { get; set; }
        public List<EmployeeCoursedepartment> lstEmpCourseDepartment { get; set; }

        //public List<GenderModel> genderModelList { get; set;}
        //public List<LanguageModel> languageModelList { get; set; }
        //public List<MaritialStausModel> maritialStausModelList { get; set; }
        //public List<NationalityModel> nationalityModelList { get; set; }

        //public IEnumerable<SelectListItem> genderList { get; set; }
        public IEnumerable<SelectListItem> languageList { get; set; }
        public IEnumerable<SelectListItem> maritialStausList { get; set; }
        public IEnumerable<SelectListItem> nationalityList { get; set; }

        public GenderModel genderModel { get; set; }
        public LanguageModel languageModel { get; set; }
        public MaritialStausModel maritialStausModel { get; set; }
        public NationalityModel nationalityModel { get; set; }

        public List<UserRoleDetails> lstUserRoleDetails { get; set; }

    }

    public class ChildrenDetails
    {
        public int AlternativeID { set; get; }
        public int StudentID { set; get; }
        public string NameAsInPassport_1 { set; get; }

        public string GenderName_1 { set; get; }

        public string BirthDate2 { set; get; }

        public string GradeName_1 { set; get; }

        public string SectionName_1 { set; get; }

        public bool IsAddedtoDependent { set; get; }

        public string FirstName_1 { set; get; }
        public string MiddleName_1 { set; get; }

    }

    public class FamilyDetails
    {
        public int EmployeeId { get; set; }
        public string EmpAltId { get; set; }
        public string EmployeeName { get; set; }
        public int FamilyID { get; set; }
        public string FatherName_1 { get; set; }
        public string FamilyName_1 { get; set; }
        public double OutstandingBalance { get; set; }
        public bool IsOSTDeducted { get; set; }
        public double DeductedAmount { get; set; }
    }

    public class UserRoleDetails
    {
        public string userRole { set; get; }
        public bool isSelected { set; get; }
        public int userRoleId { set; get; }
    }

    public class SchoolHouse
    {
        public int HOID { get; set; }
        public string HousingName_1 { get; set; }

        public string HousingName_2 { get; set; }
        public string HousingName_3 { get; set; }
    }

    public class NationalitWiseData
    {
        public int DepartmentID { get; set; }
        public string DepartmentName_1 { get; set; }
        public string DepartmentName_2 { get; set; }
        public int NationalityID { get; set; }
        public string NationalityName { get; set; }
        public string NationalityName_2 { get; set; }
        public int MaleCount { get; set; }
        public int NonNationalityMaleCount { get; set; }
        public int FemaleCount { get; set; }
        public int NonNationalityFemaleCount { get; set; }
        public int TotalCount { get; set; }
    }

    public class EmployeeCoursedepartment
    {
        public string CourseName_1 { get; set; }
        public string CourseDepartmentName_1 { get; set; }
        public string GradeNameEn { get; set; }
    }
}
