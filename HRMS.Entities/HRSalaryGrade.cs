﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities
{
    public class HRSalaryGrade
    {
        public int? salaryGradeID { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Salry Grade Code")]
        public string salaryGradeCode { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Salry Grade Level")]
        public string salaryGradeLevel { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Description")]
        public string description { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Monthly Hours")]
        public decimal? monthlyHours { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Minimum Hour Price")]
        public decimal? minHourPrice { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Medium Hour Price")]
        public decimal? medHourPrice { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Maximum Hour Price")]
        public decimal? maxHourPrice { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Active")]
        public bool isActive { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Company Id")]
        public int? companyId { get; set; }

        public int? createdBy { get; set; }

        public DateTime createdOn { get; set; }

        public bool? isDeleted { get; set; }
    }
}
