﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class ReportBuilderModel
    {
        public List<ReportBuilderFieldsModel> SelectedFieldsList { get; set; }
        public List<PayReportBuilderWhereModel> WhereConditionsList { get; set; }
        public List<SelectListItem> AllFieldsList { get; set; }
        public List<SelectListItem> AllGroupByClauseList { get; set; }
        public List<SelectListItem> WhereConditionOperators { get; set; }
        public List<SelectListItem> LogicalOperators { get; set; }
        public List<SelectListItem> WhereConditionValues { get; set; }
        public string ReportName { get; set; }
        public string SelectedFields { get; set; }
        public int OrderBy { get; set; }
        public int GroupBy { get; set; }
        public string PayReportBuilderSavedReportsWhere { get; set; }
        public string WhereDescription { get; set; }
        public int UserID { get; set; }
        public int PayReportBuilderSavedReportsID { get; set; }
        public List<ReportBuilderModel> SavedReportList { get; set; }
    }

    public class ReportBuilderFieldsModel
    {
        public int PayReportBuilderFieldsID { get; set; }
        public string description { get; set; }
        public string sqlname { get; set; }
        public string sqlID { get; set; }
        public string sqlTable { get; set; }
        public string sqlTableDescription { get; set; }
        public string sqlTableID { get; set; }
    }

    public class PayReportBuilderWhereModel
    {
        public int PayReportBuilderWhereID { get; set; }
        public string PayReportBuilderWhere { get; set; }
        public string whereSQL { get; set; }
        public string Eqal { get; set; }
        public string UserID { get; set; }
        public int SavedReportId { get; set; }
    }

    public class ColumnMetaData
    {
        public ColumnMetaData(string colName)
        {
            ColumnName = colName;
        }

        public string ColumnName { get; set; }
    }

    public class LetterReportBuilder
    {
        public List<ReportBuilderFieldsModel> lstAllReportFields { get; set; }
        public List<ReportBuilderFieldsModel> lstReportFields { get; set; }
        public List<Employee> lstEmployee { get; set; }

        public List<LetterTemplates> lstLetterTemplates { get; set; }

    }
    public class LetterTemplates
    {
        public int LettersId { get; set; }
        public string Lettername { get; set; }
        public string HtmlContent { get; set; }
        public string LetterReportFields { get; set; }

        public LetterTemplates()
        {
            HtmlContent = "";
            LetterReportFields = "";
        }
    }
}
