﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class DailyAttendanceViewModel : BaseModel
    {
        public int PayEmployeeAbsentID { set; get; }
        public int PayEmployeeAbsentHeaderID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public int EmployeeID { set; get; }
        public int HRID { set; get; }
        public string PayEmployeeAbsentDate { set; get; }

        [Required(ErrorMessage = "This field is required.")]
        public int PayAbsentTypeID { set; get; }
        public int LeaveTypeID { set; get; }
        public bool IsMedicalCertificateProvided { set; get; }
        public bool IsDeducted { get; set; }
        public bool IsConfirmed { set; get; }
        public string ConfirmedDate { set; get; }
        public string GenerateDate { set; get; }
        public string InTime { set; get; }
        public string OutTime { set; get; }
        public string EffectiveDate { set; get; }
        public string Status { set; get; }
        public int StatusID { get; set; }
        public string AttDate { set; get; }
        public string EmployeeName { set; get; }
        public int DeptID { get; set; }
        public int SectionID { get; set; }
        public string SectionName { get; set; }

        public string HRAltID { get; set; }


        public int aTntTranMode { set; get; }

        public int PayEmployeeLateID { get; set; }
        public string PayEmployeeLateDate { set; get; }
        public int PayLateTypeID { set; get; }
        public bool IsExcused { set; get; }
        public int LateMinutes { set; get; }
        public int EarlyMinutes { set; get; }

        public string EntryDate { set; get; }
        public bool Active { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int AttendanceCategory { set; get; }

        public string HrHireDate { set; get; }
        public string HrInductionDate { set; get; }
        public string DepName { set; get; }
        public string HolidayName { set; get; }
        public string HolidayDate { set; get; }
        //public int LateMinute { set; get; }
        //public int earlyMinute { set; get; }
        public bool Proceed { get; set; }
        public bool LateExcused { set; get; }
        public bool earlyExcused { set; get; }
        public bool absentExcused { set; get; }
        public bool IsActive { set; get; }
        public string totalHrs { set; get; }
        public string shiftHrs { set; get; }
        public string overHrs { set; get; }
        public string shortHrs { set; get; }
        public string LateReason { get; set; }
        public string AttDay { get; set; } //  Task#9561

    }

    public class DailyAttendanceExportModel
    {
        public int EmployeeID { set; get; }
        public string AlternateEmployeeID { set; get; }        
        public string EmployeeName { set; get; }
        public string DepartmentName { set; get; }
        public string AttDate { set; get; }
        public string InTime { set; get; }
        public string OutTime { set; get; }
        public int StatusID { set; get; }        
        public string Status { set; get; }
        //public int StatusID { get; set; }
        public string HolidayName { set; get; }
        public int LateMinutes { set; get; }
        public int EarlyMinutes { set; get; }
        public string TotalHrs { set; get; }
        public string ShiftHrs { set; get; }


    }

}