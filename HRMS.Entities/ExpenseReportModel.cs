﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class ExpenseReportModel : BaseModel
    {
        public int ExpenseReportId { set; get; }
        [Required(ErrorMessage = "Required")]
        public string ReportTitle { set; get; }
        [Required(ErrorMessage = "Required")]
        public string ReportDescription { set; get; }
        public string ExtNo { set; get; }
        public int? ApproverId { set; get; }
        public string ProjectId { set; get; }
        public string Attachments { set; get; }
        public string Notes { set; get; }
        public Guid ExpenseReportTempId { set; get; }
    }
}
