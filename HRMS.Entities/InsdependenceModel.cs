﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class InsdependenceModel : BaseModel
    {
        public int InsuranceDependenceID { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int Relation { get; set; }
        public int EmployeeID { set; get; }
        public int TranMode { set; get; }


        public string RelationName { get; set; }

        public string InsuranceDependenceName_1 { set; get; }
        public string InsuranceDependenceName_2 { set; get; }
          public string CardNumber { set; get; } 

    
        public decimal Amount { set; get; }
        public int InsuredBySchool { set; get; }
        
        public string Note { set; get; }
         


 



    }
}
