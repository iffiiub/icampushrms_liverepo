﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class MunicipalityModel 
    {
        public int DocMunicipalityID { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Document No")]
        public string DocumentNo { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Issue Country")]
        public int IssueCountry { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Issue Place")]
        public int IssuePlace { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Issue Date")]
        public string IssueDate { set; get; }

        [Display(Name = "Expiry Date")]
        public string ExpiryDate { set; get; }

        [Display(Name = "Note")]
        public string Note { set; get; }

        [Display(Name = "Primary")]
        public bool IsPrimary { set; get; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Employee Id")]
        public int EmployeeId { set; get; }

        public bool IsDeleted { set; get; }
        [Display(Name = "Vacc1 Date")]
        public string Vacc1Date { set; get; }
        [Display(Name = "Vacc2 Date")]
        public string Vacc2Date { set; get; }
        [Display(Name = "Vacc3 Date")]
        public string Vacc3Date { set; get; }
        [Display(Name = "Municipality File")]
        public string MunicipalityFile { set; get; }
    }
}
