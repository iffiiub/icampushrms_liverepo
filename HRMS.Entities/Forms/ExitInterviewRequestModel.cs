﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class ExitInterviewRequestModel
    {
        public ExitInterviewRequestModel()
        {
            ExitInterviewReasonOptionList = new List<ExitInterviewReasonOptionModel>();
            ExitInterviewSupervisorOptionList = new List<ExitInterviewSupervisorOptionModel>();
            ExitInterviewJobOptionList = new List<ExitInterviewJobOptionModel>();
            ExitInterviewServiceOptionList = new List<ExitInterviewServiceOptionModel>();
            ExitInterviewRatingOptionList = new List<ExitInterviewRatingOptionModel>();

            ExitInterviewReasonDetailsList = new List<ExitInterviewReasonDetailsModel>();
            ExitInterviewServiceDetailsList = new List<ExitInterviewServiceDetailsModel>();
            ExitInterviewJobDetailsList = new List<ExitInterviewJobDetailsModel>();
            ExitInterviewSupervisorDetailsList = new List<ExitInterviewSupervisorDetailsModel>();
        }

        public List<ExitInterviewReasonOptionModel> ExitInterviewReasonOptionList { get; set; }
        public List<ExitInterviewSupervisorOptionModel> ExitInterviewSupervisorOptionList { get; set; }
        public List<ExitInterviewJobOptionModel> ExitInterviewJobOptionList { get; set; }
        public List<ExitInterviewServiceOptionModel> ExitInterviewServiceOptionList { get; set; }
        public List<ExitInterviewRatingOptionModel> ExitInterviewRatingOptionList { get; set; }

        public List<ExitInterviewReasonDetailsModel> ExitInterviewReasonDetailsList { get; set; }
        public List<ExitInterviewServiceDetailsModel> ExitInterviewServiceDetailsList { get; set; }
        public List<ExitInterviewJobDetailsModel> ExitInterviewJobDetailsList { get; set; }
        public List<ExitInterviewSupervisorDetailsModel> ExitInterviewSupervisorDetailsList { get; set; }

        public int ID { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.EmployeeID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeID")]
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.EmployeeFullName", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeFullName")]
        public string EmployeeFullName { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.OtherOptionsToStay", true)]
        [LocalizedDynamicValidators(KeyName = "OtherOptionsToStay")]
        public string OtherOptionsToStay { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.OtherBenefitsOffer", true)]
        [LocalizedDynamicValidators(KeyName = "OtherBenefitsOffer")]
        public string OtherBenefitsOffer { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.JobLeastInterest", true)]
        [LocalizedDynamicValidators(KeyName = "JobLeastInterest")]
        public string JobLeastInterest { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.JobMostInterest", true)]
        [LocalizedDynamicValidators(KeyName = "JobMostInterest")]
        public string JobMostInterest { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.NewJobGoodCompany", true)]
        [LocalizedDynamicValidators(KeyName = "NewJobGoodCompany")]
        public string NewJobGoodCompany { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.NewSalaryAndPosition", true)]
        [LocalizedDynamicValidators(KeyName = "NewSalaryAndPosition")]
        public string NewSalaryAndPosition { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.GroupRecommendation", true)]
        [LocalizedDynamicValidators(KeyName = "GroupRecommendation")]
        public string GroupRecommendation { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.ShareOtherThoughts", true)]
        public string ShareOtherThoughts { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.CompanyName", true)]
        [LocalizedDynamicValidators(KeyName = "CompanyName")]
        public string CompanyName { get; set; }
        public int PositionID { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.PositionTitle", true)]
        [LocalizedDynamicValidators(KeyName = "PositionTitle")]
        public string PositionTitle { get; set; }
        public string Project { get; set; }
        [LocalizedDisplayName("ExitInterviewRequestModel.MobileNo", true)]
        [LocalizedDynamicValidators(KeyName = "MobileNo")]
        public string MobileNo { get; set; }

        [LocalizedDisplayName("ExitInterviewRequestModel.EmailID", true)]
        [LocalizedDynamicValidators(KeyName = "EmailID")]
        public string EmailID { get; set; }
        public int DepartmentID { get; set; }

        [LocalizedDisplayName("ExitInterviewRequestModel.DepartmentName", true)]
        [LocalizedDynamicValidators(KeyName = "DepartmentName")]
        public string DepartmentName { get; set; }
        public int SuperviserID { get; set; }

        [LocalizedDisplayName("ExitInterviewRequestModel.SupervisorFullName", true)]
        [LocalizedDynamicValidators(KeyName = "SupervisorFullName")]
        public string SupervisorFullName { get; set; }
        public int DefaultNationalityID { get; set; }

        [LocalizedDisplayName("ExitInterviewRequestModel.NationalityName", true)]
        [LocalizedDynamicValidators(KeyName = "NationalityName")]
        public string NationalityName { get; set; }
        public int Grade { get; set; }
        public int RequesterEmployeeID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ReqStatusID { get; set; }
        public int FormProcessID { get; set; }
    }

    public class ExitInterviewReasonOptionModel
    {
        public int PrimaryReasonID { get; set; }
        public string PrimaryReasonName { get; set; }
    }

    public class ExitInterviewSupervisorOptionModel
    {
        public int SupOptionID { get; set; }
        public string SupOptionName { get; set; }
    }

    public class ExitInterviewJobOptionModel
    {
        public int JobOptionID { get; set; }
        public string JobOptionName { get; set; }
    }

    public class ExitInterviewServiceOptionModel
    {
        public int ServiceOptionID { get; set; }
        public string ServiceOptionName { get; set; }
    }

    public class ExitInterviewRatingOptionModel
    {
        public int RatingOptionID { get; set; }
        public string RatingOptionName { get; set; }
    }

    public class ExitInterviewReasonDetailsModel
    {
        public int ReasonDetailID { get; set; }
        public int ID { get; set; }
        public int PrimaryReasonID { get; set; }
    }

    public class ExitInterviewServiceDetailsModel
    {
        public int ServiceDetailID { get; set; }
        public int ID { get; set; }
        public int RatingOptionID { get; set; }
        public int ServiceOptionID { get; set; }
    }

    public class ExitInterviewJobDetailsModel
    {
        public int JobDetailID { get; set; }
        public int ID { get; set; }
        public int RatingOptionID { get; set; }
        public int JobOptionID { get; set; }
    }

    public class ExitInterviewSupervisorDetailsModel
    {
        public int SupervisorDetailID { get; set; }
        public int ID { get; set; }
        public int RatingOptionID { get; set; }
        public int SupOptionID { get; set; }
    }

    public class ExitInterviewStatusReportModel
    {
        public string BUName { get; set; }
        public string DepartmentName { get; set; }
        public int RequestID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string RequestDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }

}
