﻿using HRMS.Entities.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class PublicHolidayLeaveManagerModel
    {
        public int LeaveRequestID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }       
        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string WeekEnds { get; set; }
        public decimal TotalLeaveDays { get; set; }
        public string LapsingDate { get; set; }
        public decimal LeaveDaysFromLapsingDays { get; set; }
        public decimal LeaveDaysFromAccruedDays { get; set; }
        public decimal HolidayLeaveDays { get; set; }
        public decimal CreditBackDays { get; set; }
        public int? PublicHolidayLeaveCreditBackID { get; set; }
        public int HolidayID { get; set; }
        public decimal NoOfWeekEndDays { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
    }

    public class PublicHolidayLeaveManagerViewModel
    {
        public List<PublicHolidayLeaveManagerModel> PublicHolidayLeaveManagerModelList { get; set; }
        public List<HolidayModel>  HolidayDateList { get; set; }
        // public List<VacationTypeModel> VacationTypeModellList { get; set; }
    }
    public class PublicHolidayLeaveManagerSaveModel
    {      
        public int HolidayID { get; set; }
        public List<PublicHolidayLeaveManagerModel> PublicHolidayLeaveManagerModelList { get; set; }
        // public List<VacationTypeModel> VacationTypeModellList { get; set; }
    }

}
