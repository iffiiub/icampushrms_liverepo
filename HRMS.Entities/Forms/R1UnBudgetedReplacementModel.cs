﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using HRMS.Entities.Common;

namespace HRMS.Entities.Forms
{
    public class R1UnBudgetedReplacementModel
    {
        public bool IsReplacementRecord { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ReplacementEmployee", true)]
        [LocalizedDynamicValidators(KeyName = "ReplacementEmployee")]
        public string ReplacementEmployee { get; set; }
        public int EmployeeID { get; set; }
        public int ID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.CompanyID", true)]
        [LocalizedDynamicValidators(KeyName = "CompanyID")]
        public int CompanyID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.AcademicYearID", true)]
        [LocalizedDynamicValidators(KeyName = "AcademicYearID")]
        public Int16 AcademicYearID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ReplacedEmployeeID", true)]
        [LocalizedDynamicValidators(KeyName = "ReplacedEmployeeID")]
        public int ReplacedEmployeeID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.EmploymentModeID", true)]
        [LocalizedDynamicValidators(KeyName = "EmploymentModeID")]
        public int? EmploymentModeID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.UserTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "UserTypeID")]
        public int? UserTypeID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.RecCategoryID", true)]
        [LocalizedDynamicValidators(KeyName = "RecCategoryID")]
        public short? RecCategoryID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.EmployeeJobCategoryID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeJobCategoryID")]
        public int? EmployeeJobCategoryID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.DepartmentID", true)]
        [LocalizedDynamicValidators(KeyName = "DepartmentID")]
        public int? DepartmentID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ProjectData", true)]
        public string ProjectData { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.HMEmployeeID", true)]
        public int? HMEmployeeID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.HMDesignation", true)]
        public string HMDesignation { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.PositionLocation", true)]
        public string PositionLocation { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.HeadCount", true)]
        [LocalizedDynamicValidators(KeyName = "HeadCount")]
        public int HeadCount { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.JobGradeID", true)]
        public int? JobGradeID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.JobGradeName", true)]
        [LocalizedDynamicValidators(KeyName = "JobGradeName")]
        public string JobGradeName { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ReportingEmployeeID", true)]
        public int? ReportingEmployeeID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.MinExpRequired", true)]
        [LocalizedDynamicValidators(KeyName = "MinExpRequired")]
        public string MinExpRequired { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.SourceID", true)]
        [LocalizedDynamicValidators(KeyName = "SourceID")]
        public int? SourceID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.RecruitFromID", true)]
        [LocalizedDynamicValidators(KeyName = "RecruitFromID")]
        public Int16? RecruitFromID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.PositionID", true)]
        [LocalizedDynamicValidators(KeyName = "PositionID")]
        public int? PositionID { get; set; }

        [LocalizedDisplayName("R1UnBudgetedReplacementModel.SalaryRangesID", true)]
        [LocalizedDynamicValidators(KeyName = "SalaryRangesID")]
        public int? SalaryRangesID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.SalaryRanges", true)]
        [LocalizedDynamicValidators(KeyName = "SalaryRanges")]
        public string SalaryRanges { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ActualBudget", true)]
        [LocalizedDynamicValidators(KeyName = "ActualBudget")]
        public decimal ActualBudget { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.VehicleToolTrade", true)]
        [LocalizedDynamicValidators(KeyName = "VehicleToolTrade")]
        public bool? VehicleToolTrade { get; set; }

        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ContractStatus", true)]
        [LocalizedDynamicValidators(KeyName = "ContractStatus")]
        public string ContractStatus { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.FamilySpouse", true)]
        [LocalizedDynamicValidators(KeyName = "FamilySpouse")]
        public string FamilySpouse { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.AnnualAirTicket", true)]
        [LocalizedDynamicValidators(KeyName = "AnnualAirTicket")]
        public bool? AnnualAirTicket { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.AirfareFrequencyID", true)]
        [LocalizedDynamicValidators(KeyName = "AirfareFrequencyID")]
        public Int16? AirfareFrequencyID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.AirfareClassID", true)]
        [LocalizedDynamicValidators(KeyName = "AirfareClassID")]
        public int? AirfareClassID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.HealthInsurance", true)]
        [LocalizedDynamicValidators(KeyName = "HealthInsurance")]
        public bool? HealthInsurance { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.LifeInsurance", true)]
        [LocalizedDynamicValidators(KeyName = "LifeInsurance")]
        public bool? LifeInsurance { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.SalikTag", true)]
        [LocalizedDynamicValidators(KeyName = "SalikTag")]
        public bool? SalikTag { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.SalikAmount", true)]
        [LocalizedDynamicValidators(KeyName = "SalikAmount")]
        public decimal SalikAmount { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.PetrolCard", true)]
        [LocalizedDynamicValidators(KeyName = "PetrolCard")]
        public bool? PetrolCard { get; set; }

        [LocalizedDisplayName("R1UnBudgetedReplacementModel.PetrolCardAmount", true)]
        [LocalizedDynamicValidators(KeyName = "PetrolCardAmount")]
        public decimal PetrolCardAmount { get; set; }

        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ParkingCard", true)]
        [LocalizedDynamicValidators(KeyName = "ParkingCard")]
        public bool? ParkingCard { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ParkingAreas", true)]
        public string ParkingAreas { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.JobDescriptionFileID", true)]
        [LocalizedDynamicValidators(KeyName = "JobDescriptionFileID")]
        public int JobDescriptionFileID { get; set; }

        [LocalizedDisplayName("R1UnBudgetedReplacementModel.OrgChartFileID", true)]
        [LocalizedDynamicValidators(KeyName = "OrgChartFileID")]
        public int OrgChartFileID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.ManPowerFileID", true)]
        [LocalizedDynamicValidators(KeyName = "ManPowerFileID")]
        public int ManPowerFileID { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.DivisionID", true)]
        public int? DivisionID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        [LocalizedDisplayName("R1UnBudgetedReplacementModel.RequesterEmployeeID", true)]
        public int RequesterEmployeeID { get; set; }
        public string CompanyIDs { get; set; }
        [LocalizedDisplayName("R1UnBudgetedReplacementModel.Comments", true)]
        public string Comments { get; set; }
        public int RequestID { get; set; }
        public string DeletedFileIDs { get; set; }
        public int FormProcessID { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public bool RequestInitialize { get; set; }
        public int SepartionFormID { get; set; }
        public bool IsAddMode { get; set; }
        public string RecCategoryName { get; set; }
    }

    public class HiringR1UnBudgetedReplacementViewModel
    {
        public R1UnBudgetedReplacementModel R1UnBudgetedReplacementModel { get; set; }
        public int RequestID { get; set; }
        public string CompanyName { get; set; }
        public string EmploymentModeName { get; set; }
        public string EmployeeJobCategoryName { get; set; }
        public string DepartmentName { get; set; }
        public string HMEmployeeName { get; set; }
        public string JobGradeName { get; set; }
        public string ReportingEmployeeName { get; set; }
        public string SourceName { get; set; }
        public string RecruitFromName { get; set; }
        public string SalaryRangeName { get; set; }
        public string AirfareFrequencyName { get; set; }
        public string AirfareClassName { get; set; }
        public string JobDescriptionFileName { get; set; }
        public string OrgChartFileName { get; set; }
        public string ManPowerFileName { get; set; }
        public string AcademicYearName { get; set; }
        public string RequesterEmployeeName { get; set; }
        public string DivisionName { get; set; }
        public string RequestStatusName { get; set; }
        public string PositionTitle { get; set; }
        public string UserTypeName { get; set; }
        public short RecCategoryID { get; set; }
        public string RecCategoryName { get; set; }
    }

}
