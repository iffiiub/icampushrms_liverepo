﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{

    public class RequestFormsProcessModel
    {
        [Key]
        public int FormProcessID { get; set; }
        public int RequestID { get; set; }
        public Int16 FormID { get; set; }
        public int FormInstanceID { get; set; }

    }
}
