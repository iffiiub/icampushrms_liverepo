﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HRMS.Entities.Forms
{
    public class SalaryRangesModel
    {
        public int SalaryRangesID  { get; set; }
        [Display(Name = "Min Salary", GroupName = "Min Salary")]
        public int MinSalary { get; set; }
 
        [Display(Name = "Max Salary", GroupName = "Max Salary")]
        public int MaxSalary { get; set; }
       
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
      
        public int PositionID { get; set; }
        public string PostionName { get; set; }
        public bool IsDeleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int autoID { get; set; }
        public string SalaryRanges { get; set; }
    }
}
