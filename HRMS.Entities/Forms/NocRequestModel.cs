﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class NocRequestModel
    {
        [LocalizedDisplayName("NocRequestModel.NOCReasonID", false)]
        [LocalizedDynamicValidators(KeyName = "NOCReasonID")]
        public int NOCReasonID { get; set; }
        [LocalizedDisplayName("NocRequestModel.OtherReason", false)]
        [LocalizedDynamicValidators(KeyName = "OtherReason")]
        public string OtherReason { get; set; }
        [LocalizedDisplayName("NocRequestModel.CertificateAddressee", false)]
        [LocalizedDynamicValidators(KeyName = "CertificateAddressee")]
        public string CertificateAddressee { get; set; }
        [LocalizedDisplayName("NocRequestModel.CountryID", false)]
        [LocalizedDynamicValidators(KeyName = "CountryID")]
        public int CountryID { get; set; }
        [LocalizedDisplayName("NocRequestModel.NOCLanguage", false)]
        [LocalizedDynamicValidators(KeyName = "NOCLanguage")]
        public string NOCLanguage { get; set; }
        [LocalizedDisplayName("NocRequestModel.FreeZoneVisa", false)]      
        public bool FreeZoneVisa { get; set; }
        [LocalizedDisplayName("NocRequestModel.RequiredDate", false)]
        [LocalizedDynamicValidators(KeyName = "RequiredDate")]
        public string RequiredDate { get; set; }
        [LocalizedDisplayName("NocRequestModel.DateOfTravel", false)]
        [LocalizedDynamicValidators(KeyName = "DateOfTravel")]
        public string DateOfTravel { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public int ReqStatusID { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }
        public string Comments { get; set; }
        public int RequestID { get; set; }      
        public DateTime CreatedOn { get; set; }     
        public CerficateRequestFormModel certificateRequest { get; set; }
        public bool IsAddMode { get; set; }
        [LocalizedDisplayName("NocRequestModel.PurposeofVisit", false)]
        [LocalizedDynamicValidators(KeyName = "PurposeofVisit")]
        public short PurposeOfVisit { get; set; }
        [LocalizedDisplayName("NocRequestModel.TravelExpenseBorneBy", false)]
        [LocalizedDynamicValidators(KeyName = "TravelExpenseBorneBy")]
        public short TravelExpenseBorneBy { get; set; }
        [LocalizedDisplayName("NocRequestModel.ApprovedBusinessTravelFormId", false)]
        [LocalizedDynamicValidators(KeyName = "ApprovedBusinessTravelFormId")]
        public int ApprovedBusinessTravelFormId { get; set; }
        public string ApprovedBusinessTravelFormName { get; set; }
        [LocalizedDisplayName("NocRequestModel.SalaryDetail", false)]
        [LocalizedDynamicValidators(KeyName = "SalaryDetail")]
        public string SalaryDetail { get; set; }
    }

    public class NOCStatusReportModel
    {
        public string BUName { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string Category { get; set; }
        public string NOCLanguage { get; set; }
        public string AddressTo { get; set; }
        public string RequestReason { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
