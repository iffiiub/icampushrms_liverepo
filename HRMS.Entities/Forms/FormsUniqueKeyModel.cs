﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class FormsUniqueKeyModel
    {
        public Int16 FormID { get; set; }
        public string FormName { get; set; }
        public string Description { get; set; }
        public string CompanyType { get; set; }
        public string TableName { get; set; }
        public string ControllerName { get; set; }       
        public Int16? CertificateTypeID { get; set; }
        public Int16? LeaveTypeID { get; set; }
        public Int16? SeparationTypeID { get; set; }
        public Int16? ParentFormID { get; set; }
   

    }
}
