﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class CompOffPreApprovalModel
    {
        public int FormProcessID;

        public int RequestID { get; set; }

        [LocalizedDisplayName("CompOffPreApprovalModel.WorkingDate", true)]
        [LocalizedDynamicValidators(KeyName = "WorkingDate")]
        public string WorkingDate { get; set; }

        [LocalizedDisplayName("CompOffPreApprovalModel.AdditionalWorkingHours", true)]
        [LocalizedDynamicValidators(KeyName = "AdditionalWorkingHours")]
        public decimal AdditionalWorkingHours { get; set; }


        [LocalizedDisplayName("CompOffPreApprovalModel.TimeSheetFileID", true)]
        [LocalizedDynamicValidators(KeyName = "TimeSheetFileID")]
        public int TimeSheetFileID { get; set; }
        public string TimeSheetFileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int RequesterEmployeeID { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.Comments", true)]
        public string Comments { get; set; }
        public string ApproverComments { get; set; }
        public int RequestStatusID { get; set; }
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public int CompanyID { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsValid { get; set; }
        public string ValidationMessage { get; set; }
    }

    public class CompensatoryStatusReportModel
    {
        public string BU_Name { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string AttendanceDate { get; set; }
        public decimal CompensatoryHours { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }

    public class CompOffBalanceReportModel
    {
        public string BU_Name { get; set; }           
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string JoiningDate { get; set; }
        public string Email { get; set; }
        public string ApprovedDate { get; set; }
        public decimal CompOffBalance { get; set; }       
        public string ExpiryDate { get; set; }
    }
       
}
