﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HRMS.Entities.Forms
{
    public class RatingScaleModel
    {
        public int RatingScaleID { get; set; }
        public int Sequence { get; set; }
        public string RatingScaleName_1 { get; set; }
        public string RatingScaleCode { get; set; }
        public string RatingScaleDetail { get; set; }
    }
    public class ConfirmationJobEvaluationModel
    {
        public int JobEvaluationID { get; set; }
        public int Sequence { get; set; }
        public string JobEvaluationName_1 { get; set; }
        public string JobEvaluationName_2 { get; set; }
        public string JobEvaluationName_3 { get; set; }
        public string JobEvaluationDetail { get; set; }
        public int JobEvaluationDetailID { get; set; }      
        public int RatingScaleID { get; set; }
    }
    public class ConfirmationCodeOfConductModel
    {
        public int CodeOfConductID { get; set; }
        public int Sequence { get; set; }
        public string CodeOfConductName_1 { get; set; }
        public string CodeOfConductName_2 { get; set; }
        public string CodeOfConductName_3 { get; set; }
        public string CodeOfConductDetail { get; set; }
        public int RatingScaleID { get; set; }
    }
    public class ConfirmationOthersOptionModel
    {
        public int OthersOptionID { get; set; }
        public int Sequence { get; set; }
        public string OthersOptionName_1 { get; set; }
        public string OthersOptionName_2 { get; set; }
        public string OthersOptionName_3 { get; set; }
        public string OthersOptionDetail { get; set; }
        public int RatingScaleID { get; set; }
    }
    public class ConfirmationDetailModel
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("ConfirmationDetailModel.TypeEvaluationID", false)]
        [LocalizedDynamicValidators(KeyName = "TypeEvaluationID")]
        public int TypeEvaluationID { get; set; }
        [LocalizedDisplayName("ConfirmationDetailModel.ExtendedProbationPeriod", false)]
        [LocalizedDynamicValidators(KeyName = "ExtendedProbationPeriod")]
        public int ExtendedProbationPeriod { get; set; }
        [LocalizedDisplayName("ConfirmationDetailModel.FinalRatingScaleID", false)]
        [LocalizedDynamicValidators(KeyName = "FinalRatingScaleID")]
        public int FinalRatingScaleID { get; set; }
        public int OfferLetterFileID { get; set; }
        public int ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("ConfirmationDetailModel.ImprovePerformanceDetail", false)]
        [LocalizedDynamicValidators(KeyName = "ImprovePerformanceDetail")]
        public string ImprovePerformanceDetail { get; set; }
        [LocalizedDisplayName("ConfirmationDetailModel.TrainingExpDetail", false)]
        [LocalizedDynamicValidators(KeyName = "TrainingExpDetail")]
        public string TrainingExpDetail { get; set; }
        public string JobEvaluationName_3 { get; set; }
        public string JobEvaluationDetail { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName_1 { get; set; }
        public string Email { get; set; }
        public string TypeEvaluationName_1 { get; set; }
        public string LineManagerName { get; set; }
        public string LineManagerID { get; set; }
        public string PositionTitle { get; set; }
        public int RequestID { get; set; }
        public List<ConfirmationJobEvaluationModel> lstJobEvalModel { get; set; }
        public List<ConfirmationCodeOfConductModel> lstConductCode { get; set; }
        public List<ConfirmationOthersOptionModel> lstOtherOption { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public string OriginalFileName { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public string ProbationPeriod { get; set; }
        public string ProbationEndDate { get; set; }      
        public string ProjectName { get; set; }
        public string JobGradeName_1 { get; set; }
        public bool RequestInitialize { get; set; }
        public int FormProcessID { get; set; }
        public string ManagerDesignation { get; set; }
    }
    public class ProbationStatusReportModel
    {
        public string BU_Name { get; set; }
        public string Requester { get; set; }
        public string RequestDate { get; set; }
        public int RequestID { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string TypeOfEvaluation { get; set; }
        public string JoiningDate { get; set; }
        public string DepartmentName { get; set; }
        public string Project { get; set; }
        public string Designation { get; set; }
        public string Category { get; set; }
        public string ProbationCompletionDate { get; set; }
        public string LineManager { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public string ProbationPeriodType { get; set; }
        public int FormProcessID { get; set; }     
    }
}
