﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class ReleaseR1RequestModel
    {
        public int EmployeeProfileCreationID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.R1RequestID", false)]
        public int R1RequestID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.ProfileRequestID", false)]
        public int ProfileRequestID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.EmployeeID", false)]
        public int? EmployeeID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.EmployeeName", false)]
        public string EmployeeName { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.FormID", false)]
        public int FormID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.Status", false)]
        public string Status { get; set; }
        public int ProfileRequestStatusID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.ProfileRequestStatus", false)]
        public string ProfileRequestStatus { get; set; }
        public int FormProcessID { get; set; }
        public int ProfileFormProcessID { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.IsReleased", false)]
        public bool IsReleased { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.R1ReleasedBy", false)]
        public int? R1ReleasedBy { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.R1ReleaseComments", false)]
        public string R1ReleaseComments { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.R1ReleasedOn", false)]
        public string R1ReleasedOn { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.R1ReleasedBy", false)]
        public string R1ReleasedByName { get; set; }
        [LocalizedDisplayName("ReleaseR1RequestModel.CompanyName", false)]
        public string R1ReleaserCompanyName { get; set; }
        public string CompanyName { get; set; }

    }
}
