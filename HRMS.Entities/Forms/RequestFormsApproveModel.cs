﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class RequestFormsApproveModel
    {
        public int FormApprovalID { get; set; }
        [ForeignKey("RequestFormsProcessModel")]
        public int FormProcessID { get; set; }
        public Int16 GroupID { get; set; }
        public int ApproverEmployeeID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public string Comments { get; set; }

        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime ModifiedOn { get; set; }
        public string ApproverEmployeeName { get; set; }
        public string GroupName { get; set; }
        public virtual RequestFormsProcessModel RequestFormsProcessModel { get; set; }
        public virtual FormsWorkflowGroupModel FormsWorkflowGroupModel { get; set; }

    }
    public enum RequestStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3,
        Completed = 4

    }
    public class FormsTaskListViewModel
    {
        public int FormProcessID { get; set; }
        public Int16 GroupID { get; set; }
        public Int16 FormID { get; set; }
        public int RequestID { get; set; }
        public int ReqStatusID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public int RequesterEmployeeID { get; set; }
        public string GroupName { get; set; }
        public string ModifiedEmployeeName { get; set; }
        public string RequesterEmployeeName { get; set; }
        public Int16 ParentFormID { get; set; }
        public string RequesterCompanyName { get; set; }
        public string CompletedRejectedStatus { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCompanyName { get; set; }
        public int PDRP_EmployeeCompanyID { get; set; }
        public string PDRP_EmployeeCompanyName { get; set; }
        public int PDRP_EmployeeID { get; set; }
        public string PDRP_EmployeeName { get; set; }
        public int PDRP_AcademicYearID { get; set; }
        public string PDRP_AcademicYearName { get; set; }
        public string PDRP_ProcessName { get; set; }
							

    }
    public class RequestFormsTaskListViewModel
    {
        public List<FormsUniqueKeyModel> FormsUniqueKeyModelList { get; set; }
        public List<FormsTaskListViewModel> FormsTaskViewModelList { get; set; }
    }

}