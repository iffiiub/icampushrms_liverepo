﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
   public class FormsWorkflowModel 
    {
        public int WorkflowID { get; set; }
        public Int16 FormID { get; set; }
        public Int16 GroupID { get; set; }
        public Int16 CompanyTypeID { get; set; }
        public Int16? PriorityListID { get; set; }
        public bool FinalApprovalNotification { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool SchoolBasedGroup { get; set; }
        public Int16? BaseGroupID { get; set; }
        public string GroupName { get; set; }
        public string FormName { get; set; }
        public string PriorityListName { get; set; }
        public string BaseGroupName { get; set; }
    }
    public class FormsWorkflowModelSaver: BaseModel
    {
        public IList<FormsWorkflowModel> FormsWorkflowList { get; set; }      
      
    }
        public class FormsWorkflowModelResponse 
    {
        public IList<FormsWorkflowModel> FormsWorkflowList { get; set; }
        public IList<FormsUniqueKeyModel> FormsUniqueKeyList { get; set; }
        public IList<FormsWorkflowGroupModel> FormsWorkflowGroupList { get; set; }
        public IList<FormsWorkflowPriorityModel> FormsWorkflowPriorityList { get; set; }
    }
}
