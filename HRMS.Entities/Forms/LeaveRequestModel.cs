﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.Common;

namespace HRMS.Entities.Forms
{
    public class LeaveRequestModel
    {
        public int ID { get; set; }
        public int CompanyID { get; set; }
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("LeaveRequestModel.VacationTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "VacationTypeID")]
        public Int16 VacationTypeID { get; set; }
        public string Name { get; set; }
        public bool HalfDayLeave { get; set; }
        [LocalizedDisplayName("LeaveRequestModel.FromDate", true)]
        [LocalizedDynamicValidators(KeyName = "FromDate")]
        public string FromDate { get; set; }
        [LocalizedDisplayName("LeaveRequestModel.ToDate", true)]
        [LocalizedDynamicValidators(KeyName = "ToDate")]
        public string ToDate { get; set; }
        public bool InCountry { get; set; }

        public bool OutCountry { get; set; }
        [LocalizedDisplayName("LeaveRequestModel.CountryID", true)]
        [LocalizedDynamicValidators(KeyName = "CountryID")]
        public Int16 CountryID { get; set; }
        public string CountryName { get; set; }
        [LocalizedDisplayName("LeaveRequestModel.ContactInfo1", true)]
        [LocalizedDynamicValidators(KeyName = "ContactInfo1")]
        public string ContactInfo1 { get; set; }
        public string ContactInfo2 { get; set; }
        public string ContactInfo3 { get; set; }
        public int MadicalCerFileID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int RequestID { get; set; }
        [LocalizedDisplayName("LeaveRequestModel.Comments", true)]
        public string Comments { get; set; }
        public string MedicalCerficateName { get; set; }
        public decimal AvailableLeaveDays { get; set; }
        public decimal? LapsingDays { get; set; }
        public decimal? ExtendedLapsingDays { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? LapsingDate { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? ExtendedLapsingDate { get; set; }
        public decimal RemainingDays { get; set; }
        public decimal RequestedDays { get; set; }
        public List<EmployeeTaskHandoverModel> EmployeeTaskHandoverList { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public bool IsAddMode { get; set; }
        public int FormProcessID { get; set; }
        public bool CancelLeaveRequest { get; set; }
        public string CancelComments { get; set; }
        public string ModifiedByName { get; set; }

    }
    public class EmployeeTaskHandoverModel
    {
        public int TaskHandOverID { get; set; }
        public int ID { get; set; }
        public int? TaskAssignEmpID { get; set; }
        public int EmployeeId { get; set; }
        public string TaskDetail { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string FullName { get; set; }

        public string LastName { get; set; }

    }
    public class FormLeaverequestviewModel
    {
        public LeaveRequestModel leaverequestmodel { get; set; }
        public int ID { get; set; }
        public int RequestID { get; set; }
        public int CompanyID { get; set; }
        public int EmployeeID { get; set; }

        public Int16 VacationTypeID { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.LeaveTypeName", true)]
        public string Name { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.HalfDayLeave", true)]
        public bool HalfDayLeave { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.FromDate", true)]
        public string FromDate { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.ToDate", true)]
        public string ToDate { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.RequestedDays", true)]
        public string RequestedDays { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.RemainingDays", true)]
        public string RemainingDays { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.AvailableLeaves", true)]
        public string TotalAvailableLeaves { get; set; }
        public bool InCountry { get; set; }

        public bool OutCountry { get; set; }
        public Int16 CountryID { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.CountryName", true)]
        public string CountryName { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.ContactInfo_1", true)]
        public string ContactInfo1 { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.ContactInfo_2", true)]
        public string ContactInfo2 { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.ContactInfo_3", true)]
        public string ContactInfo3 { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.MadicalCerFileID", true)]
        public int MadicalCerFileID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int RequesterEmployeeID { get; set; }
        public List<EmployeeTaskHandoverModel> EmployeeTaskHandoverList { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public int TaskAssignEmpID { get; set; }
        public string TaskDetail { get; set; }
        public int FormProcessID { get; set; }
        public decimal AvailableLeaveDays { get; set; }
        public decimal? LapsingDays { get; set; }
        public decimal? ExtendedLapsingDays { get; set; }
        public string LapsingDate { get; set; }
        public string ExtendedLapsingDate { get; set; }
        //Pooja's Changes
        public bool CancelLeaveRequest { get; set; }
        public string CancelComments { get; set; }
        public string EmployeeName { get; set; }
        // public decimal RemainingDays { get; set; }

    }
    public class LeaveRequestStatusReportModel
    {
        public string BU_Name { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Category { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string LeaveType { get; set; }
        public string LeaveDateFrom { get; set; }
        public string LeaveDateTo { get; set; }
        public decimal RequestedDays { get; set; }
        public decimal BeforeAvailableLeaveDays { get; set; }
        public decimal AfterAvailableLeaveDays { get; set; }
        public decimal PublicHolidays { get; set; }
        public decimal LeaveAdjusted { get; set; }
        public decimal TotalDays { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
    public class ValidateProbationModel
    {
        public bool IsEmployeeUnderProbation { get; set; }
        public bool IsProbationCompletedBeforeProbationPeriod { get; set; }
        public bool IsProbationConfirmationPending { get; set; }
    }

}