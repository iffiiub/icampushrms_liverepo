﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using HRMS.Entities.Common;

namespace HRMS.Entities.Forms
{
   public class CerficateRequestFormModel
    {
        public int ID { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.CertificateTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "CertificateTypeID")]
        public Int16 CertificateTypeID { get; set; }
        public string CertificateTypeName { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.CertificateReasonID", true)]
        [LocalizedDynamicValidators(KeyName = "CertificateReasonID")]
        public Int16 CertificateReasonID { get; set; }
        public string CertificateReasonName { get; set; }
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.RequestPurpose", true)]
        [LocalizedDynamicValidators(KeyName = "RequestPurpose")]
        public string RequestPurpose { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.PassportFileID", true)]
        [LocalizedDynamicValidators(KeyName = "PassportFileID")]
        public int PassportFileID { get; set; }
        public string PassportFileName { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.VisaFileID", true)]
        [LocalizedDynamicValidators(KeyName = "VisaFileID")]
        public int VisaFileID { get; set; }      

        public string VisaFileName { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.BankNameAddress", true)]
        [LocalizedDynamicValidators(KeyName = "BankNameAddress")]
        public string BankNameAddress { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        [LocalizedDisplayName("CerficateRequestFormModel.RequesterEmployeeID", true)]
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.Comments", true)]
        public string Comments { get; set; }
        public int DocumentTypeId { get; set; }
        public int RequestID { get; set; }
        public string RequestStatusName { get; set; }
        public string RequesterEmployeeName { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public int FormProcessID { get; set; }
        public bool IsAddMode { get; set; }
    }

    public class CertificateRequestStatusReportModel
    {
        public string BUName { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string Category { get; set; }
        public string DepartmentName { get; set; }
        public string Project { get; set; }
        public string Designation { get; set; }
        public string RequestedCertificateType { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
