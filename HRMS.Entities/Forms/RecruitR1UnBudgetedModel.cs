﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using HRMS.Entities.Common;

namespace HRMS.Entities.Forms
{
    public class RecruitR1UnBudgetedModel
    {
        public int ID { get; set; }
        //[Required(ErrorMessage = "This field is required.")]
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.CompanyID", true)]
        [LocalizedDynamicValidators(KeyName = "CompanyID")]
        public int CompanyID { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.AcademicYearID", true)]
        [LocalizedDynamicValidators(KeyName = "AcademicYearID")]
        public Int16 AcademicYearID { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.EmploymentModeID", true)]
        [LocalizedDynamicValidators(KeyName = "EmploymentModeID")]
        public int? EmploymentModeID { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.UserTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "UserTypeID")]
        public int? UserTypeID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.RecCategoryID", true)]
        [LocalizedDynamicValidators(KeyName = "RecCategoryID")]
        public short? RecCategoryID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.EmployeeJobCategoryID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeJobCategoryID")]
        public int? EmployeeJobCategoryID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.DepartmentID", true)]
        [LocalizedDynamicValidators(KeyName = "DepartmentID")]
        public int? DepartmentID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ProjectData", true)]
        public string ProjectData { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.HMEmployeeID", true)]
        public int? HMEmployeeID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.HMDesignation", true)]
        public string HMDesignation { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.PositionLocation", true)]
        public string PositionLocation { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.HeadCount", true)]
        [LocalizedDynamicValidators(KeyName = "HeadCount")]
        public int HeadCount { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.JobGradeID", true)]
        public int? JobGradeID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ReportingEmployeeID", true)]
        public int? ReportingEmployeeID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.MinExpRequired", true)]
        [LocalizedDynamicValidators(KeyName = "MinExpRequired")]
        public string MinExpRequired { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.SourceID", true)]
        [LocalizedDynamicValidators(KeyName = "SourceID")]
        public int? SourceID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.RecruitFromID", true)]
        [LocalizedDynamicValidators(KeyName = "RecruitFromID")]
        public Int16? RecruitFromID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.PositionID", true)]
        [LocalizedDynamicValidators(KeyName = "PositionID")]
        public int? PositionID { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.SalaryRangesID", true)]
        [LocalizedDynamicValidators(KeyName = "SalaryRangesID")]
        public int? SalaryRangesID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.SalaryRanges", true)]
        [LocalizedDynamicValidators(KeyName = "SalaryRanges")]
        public string SalaryRanges { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ActualBudget", true)]
        [LocalizedDynamicValidators(KeyName = "ActualBudget")]
        public decimal ActualBudget { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.VehicleToolTrade", true)]
        [LocalizedDynamicValidators(KeyName = "VehicleToolTrade")]
        public bool? VehicleToolTrade { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ContractStatus", true)]
        [LocalizedDynamicValidators(KeyName = "ContractStatus")]
        public string ContractStatus { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.FamilySpouse", true)]
        [LocalizedDynamicValidators(KeyName = "FamilySpouse")]
        public string FamilySpouse { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.AnnualAirTicket", true)]
        [LocalizedDynamicValidators(KeyName = "AnnualAirTicket")]
        public bool? AnnualAirTicket { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.AirfareFrequencyID", true)]
        [LocalizedDynamicValidators(KeyName = "AirfareFrequencyID")]
        public Int16? AirfareFrequencyID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.AirfareClassID", true)]
        [LocalizedDynamicValidators(KeyName = "AirfareClassID")]
        public int? AirfareClassID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.HealthInsurance", true)]
        [LocalizedDynamicValidators(KeyName = "HealthInsurance")]
        public bool? HealthInsurance { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.LifeInsurance", true)]
        [LocalizedDynamicValidators(KeyName = "LifeInsurance")]
        public bool? LifeInsurance { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.SalikTag", true)]
        [LocalizedDynamicValidators(KeyName = "SalikTag")]
        public bool? SalikTag { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.SalikAmount", true)]
        public decimal SalikAmount { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.PetrolCard", true)]
        [LocalizedDynamicValidators(KeyName = "PetrolCard")]
        public bool? PetrolCard { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.PetrolCardAmount", true)]
        [LocalizedDynamicValidators(KeyName = "PetrolCardAmount")]
        public decimal PetrolCardAmount { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ParkingCard", true)]
        [LocalizedDynamicValidators(KeyName = "ParkingCard")]
        public bool? ParkingCard { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ParkingAreas", true)]
        public string ParkingAreas { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.JobDescriptionFileID", true)]
        [LocalizedDynamicValidators(KeyName = "JobDescriptionFileID")]
        public int JobDescriptionFileID { get; set; }

        [LocalizedDisplayName("RecruitR1UnBudgetedModel.OrgChartFileID", true)]
        [LocalizedDynamicValidators(KeyName = "OrgChartFileID")]
        public int OrgChartFileID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.ManPowerFileID", true)]
        [LocalizedDynamicValidators(KeyName = "ManPowerFileID")]
        public int ManPowerFileID { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.DivisionID", true)]
        public int? DivisionID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.RequesterEmployeeID", true)]
        public int RequesterEmployeeID { get; set; }       
        public string CompanyIDs { get; set; }
        [LocalizedDisplayName("RecruitR1UnBudgetedModel.Comments", true)]       
        public string Comments { get; set; }
        public int RequestID { get; set; }
        public string DeletedFileIDs { get; set; }
        public int FormProcessID { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public bool IsAddMode { get; set; }
        public string RecCategoryName { get; set; }

    }
    public class RecruitR1UnBudgetedViewModel
    {
        public RecruitR1UnBudgetedModel RecruitR1UnBudgetedModel { get; set; }
        public int RequestID { get; set; }
        public string CompanyName { get; set; }
        public string EmploymentModeName { get; set; }
        public string EmployeeJobCategoryName { get; set; }
        public string DepartmentName { get; set; }
        public string HMEmployeeName { get; set; }
        public string JobGradeName { get; set; }
        public string ReportingEmployeeName { get; set; }
        public string SourceName { get; set; }
        public string RecruitFromName { get; set; }
        public string SalaryRangeName { get; set; }
        public string AirfareFrequencyName { get; set; }      
        public string AirfareClassName { get; set; }
        public string JobDescriptionFileName { get; set; }
        public string OrgChartFileName { get; set; }
        public string ManPowerFileName { get; set; }
        public string AcademicYearName { get; set; }
        public string RequesterEmployeeName { get; set; }
        public string DivisionName { get; set; }
        public string RequestStatusName { get; set; }
        public string PositionTitle { get; set; }
        public string UserTypeName { get; set; }
        public short RecCategoryID { get; set; }
        public string RecCategoryName { get; set; }

    }

}