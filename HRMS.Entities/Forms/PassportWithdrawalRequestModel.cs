﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class PassportWithdrawalRequestModel
    {
        public PassportWithdrawalRequestModel()
        {
            RequestFormsApproveModelList = new List<RequestFormsApproveModel>();
        }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public int ID { get; set; }
        public int FormProcessID { get; set; }
        public int EmployeeID { get; set; }

        [LocalizedDisplayName("PassportWithdrawalRequestModel.PassportReasonID", true)]
        [LocalizedDynamicValidators(KeyName = "PassportReasonID")]
        public int PassportReasonID { get; set; }

        public string PassportReasonName { get; set; }

        [LocalizedDisplayName("PassportWithdrawalRequestModel.OtherReason", true)]
        [LocalizedDynamicValidators(KeyName = "OtherReason")]
        public string OtherReason { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("PassportWithdrawalRequestModel.WithdrawalDate", true)]
        [LocalizedDynamicValidators(KeyName = "WithdrawalDate")]
        public string WithdrawalDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("PassportWithdrawalRequestModel.PassportReturnDate", true)]
        [LocalizedDynamicValidators(KeyName = "PassportReturnDate")]
        public string PassportReturnDate { get; set; }
        public int ReqStatusID { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("PassportWithdrawalRequestModel.Comments", true)]
        [LocalizedDynamicValidators(KeyName = "Comments")]
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsAddMode { get; set; }
    }

    public class PassportWithdrawalStatusReportModel
    {
        public string BUName { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Project { get; set; }
        public string Designation { get; set; }
        public string Category { get; set; }
        public string RequestReason { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
