﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class ClearanceRequestModel
    {
        public int ID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int RequesterEmployeeID { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.Comments", false)]
        public string Comments { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.EmployeeID", false)]
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.EmployeeName", false)]
        public string EmployeeName { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.CompanyName", false)]
        public string CompanyName { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.DepartmentName", false)]
        public string DepartmentName { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.ProjectName", false)]
        public string ProjectName { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.PositionTitle", false)]
        public string PositionTitle { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.JobGradeName", false)]
        public string JobGradeName { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.MobileNumber", false)]
        public string MobileNumber { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.LineManager", false)]
        public string LineManager { get; set; }
        [LocalizedDisplayName("ClearanceRequestModel.NationalityName", false)]
        public string NationalityName { get; set; }
        public int UserID { get; set; }
        public List<ClearanceRequestOptionsModel> ClearanceRequestOptionsModel { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public bool IsUserExistsInGroup { get; set; }
        public int RequestID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int FormProcessID { get; set; }
    }
    public class ClearanceRequestOptionsModel
    {
        public int ClearanceOptionID { get; set; }
        public int GroupID { get; set; }
        public string ClearanceOptionName_1 { get; set; }
        public string GroupName { get; set; }
        public int ClearanceDetailID { get; set; }
        public int ID { get; set; }        
        public bool? ClearanceMark { get; set; }
        public decimal? Amount { get; set; }
        public string Remarks { get; set; }
        public bool IsUserExistsInGroup { get; set; }
        public bool IsExistInGroup { get; set; }
    }

    public class EndOfTermStatusReportModel
    {
        public string BUName { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public int RequestedBy { get; set; }
        public int RequestID { get; set; }
        public int ResignationNumber { get; set; }
        public string RequestDate { get; set; }
        public string Category { get; set; }
        public string Designation { get; set; }
        public string Project { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
