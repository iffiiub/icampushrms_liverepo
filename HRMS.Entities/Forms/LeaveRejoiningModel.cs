﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class LeaveRejoiningModel
    {
        public LeaveRejoiningModel()
        {
            LeaveRequestDetailModelList = new List<LeaveRequestDetailModel>();
        }
        public List<LeaveRequestDetailModel> LeaveRequestDetailModelList { get; set; }
        public int ID { get; set; }

        public int FormProcessID { get; set; }
        public int RequestID { get; set; }

        public int ID_LeaveRequest { get; set; }
        public int EmployeeID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("LeaveRejoiningModel.RequestDate", true)]
        public string RequestDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("LeaveRejoiningModel.ReJoiningDate", true)]
        public string ReJoiningDate { get; set; }

        [LocalizedDisplayName("LeaveRejoiningModel.LeavesToAdjust", true)]
        public int LeavesToAdjust { get; set; }
        public short ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("LeaveRejoiningModel.Comments", true)]
        public string Comments { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public bool IsAddMode { get; set; }
        public int? LeaveRequestFormRequestID { get; set; }
    }
    public class LeaveRequestDetailModel
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public int FormProcessID { get; set; }
        public int RequestID { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.VacationTypeID", true)]
        public int VacationTypeID { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.AvailableLeaves", true)]
        public string AvailableLeaveDays { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.ToBeLapsed", true)]
        public string ToBeLapsed { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("LeaveRequestDetailModel.LapseDate", true)]
        public string LapseDate { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.ExtendedLapsed", true)]
        public string ExtendedLapsed { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("LeaveRequestDetailModel.NewExtendedLapseDate", true)]
        public string NewExtendedLapseDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        [LocalizedDisplayName("LeaveRequestDetailModel.LeaveStartDate", true)]
        public string LeaveStartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        [LocalizedDisplayName("LeaveRequestDetailModel.LeaveEndDate", true)]
        public string LeaveEndDate { get; set; }
        [LocalizedDisplayName("LeaveRequestDetailModel.IsHalfDayLeave", true)]
        public bool IsHalfDayLeave { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.TotalDaysLeave", true)]
        public string TotalDaysLeave { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.RemainingLeaveDays", true)]
        public string RemainingLeaveDays { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.Location", true)]
        public string Location { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.InCountry", true)]
        public bool InCountry { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.OutCountry", true)]
        public bool OutCountry { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.Destination", true)]
        public int Destination { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.ContactInfo_1", true)]
        public string ContactInfo_1 { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.ContactInfo_2", true)]
        public string ContactInfo_2 { get; set; }

        [LocalizedDisplayName("LeaveRequestDetailModel.ContactInfo_3", true)]
        public string ContactInfo_3 { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string CreatedOn { get; set; }
    }
}
