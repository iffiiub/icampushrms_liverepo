﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using HRMS.Entities.Common;

namespace HRMS.Entities.Forms
{
    public class FormsEmployeeJoiningModel
    {
        public int ID { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.EmployeeID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeID")]
        public int EmployeeId { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.EmployeeName", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeName")]
        public string EmployeeName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.JoiningDate", true)]
        [LocalizedDynamicValidators(KeyName = "JoiningDate")]
        public string JoiningDate { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.OfferLetterFileID", true)]
        [LocalizedDynamicValidators(KeyName = "OfferLetterFileID")]
        public int OfferLetterFileID { get; set; }
      
        public string OfferletterFileName { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.RequesterEmployeeID", true)]
        public int RequesterEmployeeID { get; set; }
        [LocalizedDisplayName("FormsEmployeeJoiningModel.Comments", true)]
        public string Comments { get; set; }
        public int RequestID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int autoID { get; set; }
        public int CompanyID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int FormProcessID { get; set; }
        public string RequestStatusName { get; set; }
        public string RequesterEmployeeName { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
    }
    public class LeaveRejoiningStatusReport
    {
        public string BU_Name { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Category { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string LeaveType { get; set; }
        public string LeaveDateFrom { get; set; }
        public string LeaveDateTo { get; set; }
        public decimal RequestedDays { get; set; }
        public string ReJoiningDate { get; set; }
        public int LeavesToAdjust { get; set; }
        public decimal AfterAvailableLeaveDays { get; set; }        
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
