﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HRMS.Entities.Forms
{
    public class EmployeeProfileCreationFormModel
    {
        public EmployeeProfileCreationFormModel()
        {
            AllFormsFilesModelList = new List<AllFormsFilesModel>();
            RequestFormsApproveModelList = new List<RequestFormsApproveModel>();
            ProfileAssignAssetsModelList = new List<ProfileAssignAssetsModel>();
        }

        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public List<ProfileAssignAssetsModel> ProfileAssignAssetsModelList { get; set; }

        public int ID { get; set; }
        public int FormProcessID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.RequestID", true)]
        [LocalizedDynamicValidators(KeyName = "RequestID")]
        public int RequestID { get; set; }     
        public int FormId { get; set; }

        public int EmployeeCreationRequestId { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.EmploymentModeId", true)]
        [LocalizedDynamicValidators(KeyName = "EmploymentModeId")]
        public int EmploymentModeId { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.UserTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "UserTypeID")]
        public int UserTypeID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.EmployeeJobCategoryID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeJobCategoryID")]
        public int EmployeeJobCategoryID { get; set; }

        public int CompanyID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.CompanyName", true)]
        [LocalizedDynamicValidators(KeyName = "CompanyName")]
        public string CompanyName { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.DepartmentID", true)]
        [LocalizedDynamicValidators(KeyName = "DepartmentID")]
        public int DepartmentID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.ProjectData", true)]
        public string ProjectData { get; set; }

        public int PositionID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.PositionTitle", true)]
        [LocalizedDynamicValidators(KeyName = "PositionTitle")]
        public string PositionTitle { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.JobGradeID", true)]
        public int JobGradeID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.ContractStatus", true)]
        [LocalizedDynamicValidators(KeyName = "ContractStatus")]
        public string ContractStatus { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.FamilySpouse", true)]
        public string FamilySpouse { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.AnnualAirTicket", true)]
        [LocalizedDynamicValidators(KeyName = "AnnualAirTicket")]
        public bool AnnualAirTicket { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.AirfareFrequencyID", true)]
        [LocalizedDynamicValidators(KeyName = "AirfareFrequencyID")]
        public int AirfareFrequencyID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.AirfareClassID", true)]
        [LocalizedDynamicValidators(KeyName = "AirfareClassID")]
        public int AirfareClassID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.Accommodation", true)]
        [LocalizedDynamicValidators(KeyName = "Accommodation")]
        public bool Accommodation { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.AccommodationType", true)]
        [LocalizedDynamicValidators(KeyName = "AccommodationType")]
        public int AccommodationType { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.HealthInsurance", true)]
        [LocalizedDynamicValidators(KeyName = "HealthInsurance")]
        public bool HealthInsurance { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.InsuranceCategory", true)]
        [LocalizedDynamicValidators(KeyName = "InsuranceCategory")]
        public int InsuranceCategory { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.InsuranceEligibility", true)]
        [LocalizedDynamicValidators(KeyName = "InsuranceEligibility")]
        public int InsuranceEligibility { get; set; }


        [LocalizedDisplayName("EmployeeProfileCreationFormModel.LifeInsurance", true)]
        [LocalizedDynamicValidators(KeyName = "LifeInsurance")]
        public bool LifeInsurance { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.DivisionID", true)]
        [LocalizedDynamicValidators(KeyName = "DivisionID")]
        public int DivisionID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.NoticedPeriod", true)]
        [LocalizedDynamicValidators(KeyName = "NoticedPeriod")]
        public int NoticedPeriod { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.FirstName", true)]
        [LocalizedDynamicValidators(KeyName = "FirstName")]
        public string FirstName { get; set; }


        [LocalizedDisplayName("EmployeeProfileCreationFormModel.MiddleName", true)]
        [LocalizedDynamicValidators(KeyName = "MiddleName")]
        public string MiddleName { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.SurName", true)]
        [LocalizedDynamicValidators(KeyName = "SurName")]
        public string SurName { get; set; }

        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat, ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.JoiningDate", true)]
        [LocalizedDynamicValidators(KeyName = "JoiningDate")]
        public string JoiningDate { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.SalaryBasisID", true)]
        [LocalizedDynamicValidators(KeyName = "SalaryBasisID")]
        public int SalaryBasisID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.CostCenter", true)]
        public string CostCenter { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.CostCenterCode", true)]
        public string CostCenterCode { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.LocationCode", true)]
        public string LocationCode { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.OfficeLocation", true)]
        public string OfficeLocation { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.ProbationPeriod", true)]
        [LocalizedDynamicValidators(KeyName = "ProbationPeriod")]
        public int ProbationPeriod { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.SupervisorID", true)]
        [LocalizedDynamicValidators(KeyName = "SupervisorID")]
        public int SupervisorID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.CountryID", true)]
        [LocalizedDynamicValidators(KeyName = "CountryID")]
        public int? CountryID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.CityID", true)]
        [LocalizedDynamicValidators(KeyName = "CityID")]
        public int CityID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.AirportListID", true)]
        [LocalizedDynamicValidators(KeyName = "AirportListID")]
        public int AirportListID { get; set; }

        //[LocalizedDisplayName("EmployeeProfileCreationFormModel.LeaveEntitleID", true)]
        //[LocalizedDynamicValidators(KeyName = "LeaveEntitleID")]
        //public int LeaveEntitleID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.LeaveEntitleDaysID", true)]
        [LocalizedDynamicValidators(KeyName = "LeaveEntitleDaysID")]
        public int LeaveEntitleDaysID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.LeaveEntitleTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "LeaveEntitleTypeID")]
        public int LeaveEntitleTypeID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.NationalityID", true)]
        [LocalizedDynamicValidators(KeyName = "NationalityID")]
        public string NationalityID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.DefaultNationalityID", true)]
        [LocalizedDynamicValidators(KeyName = "DefaultNationalityID")]
        public int DefaultNationalityID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.PassportNo", true)]
        [LocalizedDynamicValidators(KeyName = "PassportNo")]
        public string PassportNo { get; set; }

        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat, ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.PassportIssueDate", true)]
        [LocalizedDynamicValidators(KeyName = "PassportIssueDate")]
        public string PassportIssueDate { get; set; }

        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat, ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.PassportExpiryDate", true)]
        [LocalizedDynamicValidators(KeyName = "PassportExpiryDate")]
        public string PassportExpiryDate { get; set; }


        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat, ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.DOB", true)]
        public string DOB { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.BirthCountryID", true)]
        [LocalizedDynamicValidators(KeyName = "BirthCountryID")]
        public int? BirthCountryID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.BirthCityID", true)]
        [LocalizedDynamicValidators(KeyName = "BirthCityID")]
        public int? BirthCityID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.GenderID", true)]
        [LocalizedDynamicValidators(KeyName = "GenderID")]
        public int? GenderID { get; set; }
       
        [LocalizedDynamicValidators(KeyName = "ReligionID")]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.ReligionID", true)]
        public int? ReligionID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.LanguageID", true)]
        public int? LanguageID { get; set; }

        [LocalizedDynamicValidators(KeyName = "MotherName")]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.MotherName", true)]
        public string MotherName { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.FatherName", true)]
        public string FatherName { get; set; }

        [LocalizedDynamicValidators(KeyName = "MobileNumber")]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.MobileNumber", true)]
        public string MobileNumber { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.LandlinePhone", true)]
        public string LandlinePhone { get; set; }

        [LocalizedDynamicValidators(KeyName = "MaritalStatusID")]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.MaritalStatusID", true)]
        public short? MaritalStatusID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.Extension", true)]
        public string Extension { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.InterviewFileID", true)]
        [LocalizedDynamicValidators(KeyName = "InterviewFileID")]
        public int InterviewFileID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.OfferLetterFileID", true)]
        [LocalizedDynamicValidators(KeyName = "OfferLetterFileID")]
        public int OfferLetterFileID { get; set; }
        public int ManPowerFileID { get; set; }

        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid email address.")]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.WorkEmailID", true)]
        [LocalizedDynamicValidators(KeyName = "WorkEmailID")]
        public string WorkEmailID { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int AssetType { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.Comments", true)]
        public string Comments { get; set; }
        public int ReqStatusID { get; set; }
        public int EmployeeID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.HRContractTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "HRContractTypeID")]
        public int? HRContractTypeID { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsAddMode { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.EmployeeAlternativeID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeAlternativeID")]
        public string EmployeeAlternativeID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.RecCategoryID", true)]
        [LocalizedDynamicValidators(KeyName = "RecCategoryID")]
        public int RecCategoryID { get; set; }

        [LocalizedDisplayName("EmployeeProfileCreationFormModel.JobStatusID", true)]
        [LocalizedDynamicValidators(KeyName = "JobStatusID")]
        public int JobStatusID { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.PDRPFormID", true)]
        [LocalizedDynamicValidators(KeyName = "PDRPFormID")]
        public int? PDRPFormID { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.IsReleased", true)]
        [LocalizedDynamicValidators(KeyName = "IsReleased")]
        public bool IsReleased { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.R1ReleasedBy", true)]
        [LocalizedDynamicValidators(KeyName = "R1ReleasedBy")]
        public int? R1ReleasedBy { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.R1ReleaseComments", true)]
        [LocalizedDynamicValidators(KeyName = "R1ReleaseComments")]
        public string R1ReleaseComments { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.R1ReleasedOn", true)]
        [LocalizedDynamicValidators(KeyName = "R1ReleasedOn")]
        public string R1ReleasedOn { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.R1ReleasedBy", true)]
        [LocalizedDynamicValidators(KeyName = "R1ReleasedBy")]
        public string R1ReleasedByName { get; set; }
        public string R1ReleaserCompanyName { get; set; }
        public Int16 IsUpdateDetailMode { get; set; }

    }

    public class ProfileAssignAssetsModel
    {
        public int AssetsID { get; set; }
        public int ID { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.AssetsTypeID", true)]
        public int AssetsTypeID { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.ModelName", true)]
        public string ModelName { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.ModelNumber", true)]
        public string ModelNumber { get; set; }
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.SerialNumber", true)]
        public string SerialNumber { get; set; }

        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat, ApplyFormatInEditMode = true)]
        [LocalizedDisplayName("EmployeeProfileCreationFormModel.IssueDate", true)]
        public string IssueDate { get; set; }
    }

    public class EmployeeProfileStatusReportModel
    {
        public string BUName { get; set; }
        public int R1ReferenceNo { get; set; }
        public string Requester { get; set; }
        public string RequestDate { get; set; }
        public int EmployeeProfile { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Project { get; set; }
        public string Designation { get; set; }
        public string ExpectedJoiningDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }        
    }

    public class EmployeeMasterReportModel
    {
        public string BusinessUnit { get; set; }
        public string OperatingUnitID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeType { get; set; }     
        public string ExpectedJoiningDate { get; set; }
        public string HireDate { get; set; }
        public string ProbationMonths { get; set; }
        public string NoticedPeriod { get; set; }
        public string Religion { get; set; }
        public string DOB { get; set; }
        public string BirthCity { get; set; }
        public string BirthCountry { get; set; }
        public string Citizenship { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string MotherName { get; set; }
        public string PositionOfferLetter { get; set; }
        public string PositionLabourContract { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationTitle { get; set; }
        public string Location { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string SalaryBasis { get; set; }
        public string FirstLineManagerOracleNo { get; set; }
        public string FirstLineManagerName { get; set; }
        public string FirstLineManagerEmail { get; set; }
        public string FirstLineManagerAD { get; set; }
        public string SponsorName { get; set; }
        public string IBAN { get; set; }
        public string BankName { get; set; }
        public string AnnualLeaveEntitlement { get; set; }
        public string AnnualLeaveCalculation { get; set; }
        public string TicketEntitlement { get; set; }
        public string TicketEntitlementFamily { get; set; }
        public string TicketCapedAmount { get; set; }
        public string TicketClass { get; set; }
        public string TicketFrequency { get; set; }
        public string HomeTownForTicket { get; set; }
        public string MedicalInsurance { get; set; }
        public string MedicalInsuranceFamily { get; set; }
        public string ContactNumber { get; set; }
        public string PassportNo { get; set; }
        public string PassportExpiryDate { get; set; }
        public string PassportCustody { get; set; }
        public string VisaNumber { get; set; }
        public string VisaExpiryDate { get; set; }
        public string EmiratesIDNumber { get; set; }
        public string EmiratesIDExpiryDate { get; set; }
        public string LabourCardNumber { get; set; }
        public string LabourCardExpiryDate { get; set; }
        public string SecondLineManagerName { get; set; }
        public string SecondLineManager { get; set; }
        public string HODName { get; set; }
        public string HODOracleNumber { get; set; }
        public string BUHeadName { get; set; }
        public string BUHeadOracleNumber { get; set; }
    }
}
