﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class CertificatePrintingModel
    {
        public string ComapanyName { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string CertificateAddress { get; set; }
        public string DocumentType { get; set; }
        public string CertificateType { get; set; }
        public int FormProcessID { get; set; }
        public int ReqStatusID { get; set; }
        public int FormId { get; set; }
        public int TemplateId { get; set; }
        public int EmployeeID { get; set; }
        public int ID { get; set; }
    }

    public class PrintCertificateEmployeeInfoModel
    {
        public PrintCertificateEmployeeInfoModel()
        {
            SalryCertificateList = new List<CertificateSalaryModel>();
        }
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public string OrganizationCode { get; set; }
        public string EntityName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeTitle { get; set; }
        public string Nationality { get; set; }
        public string PassportNo { get; set; }
        public string MOLDesignation { get; set; }
        public string HireDate { get; set; }
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Title4 { get; set; }
        public string TravelDate { get; set; }
        public string TotalSalary { get; set; }
        public string TodaysDate { get; set; }
        public string BankName { get; set; }
        public string EmploymentStatus { get; set; }
        public string LinkHRDesignation { get; set; }
        public string GrossSalary { get; set; }
        public string SalaryInText { get; set; }
        public string IBAN { get; set; }
        public string MOLBUName { get; set; }
        public string Purpose { get; set; }
        public IEnumerable<CertificateSalaryModel> SalryCertificateList;
        public decimal SalaryAmount { get; set; }
    }

    public class CertificateSalaryModel
    {
        public int EmployeeId { get; set; }
        public string SalaryType { get; set; }
        public double Amount { get; set; }
        public string AmountLabel { get; set; }
    }

    public enum NOCCertificateTemplate
    {
        NOCPersonalStandard = 1,
        NOCBusinessStandard = 2
    }

    public enum SalaryCertificateTemplate
    {
        SalaryCertificateStandard = 3,
        SalaryTransferLetter = 4       
    }
}
