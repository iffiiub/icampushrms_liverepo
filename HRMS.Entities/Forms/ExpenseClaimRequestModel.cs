﻿using HRMS.Entities.Common;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HRMS.Entities.Forms
{
    public class ExpenseClaimRequestModel
    {
        public ExpenseClaimRequestModel()
        {
            ExpenseClaimRequestDetailList = new List<ExpenseClaimRequestDetailModel>();
            AllFormsFilesModelList = new List<AllFormsFilesModel>();
            RequestFormsApproveModelList = new List<RequestFormsApproveModel>();
        }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public int ID { get; set; }
        public int FormProcessID { get; set; }
        public int ReqStatusID { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestModel.Comments", true)]
        public string Comments { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestModel.TotalExpenseAmountAED", true)]
        [LocalizedDynamicValidators(KeyName = "TotalExpenseAmountAED")]
        public decimal TotalExpenseAmountAED { get; set; }
        public List<ExpenseClaimRequestDetailModel> ExpenseClaimRequestDetailList { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsAddMode { get; set; }
    }
    public class ExpenseClaimRequestDetailModel
    {
        public int ID { get; set; }
        public int RowID { get; set; }

        public int ClaimDetailID { get; set; }
        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.Date", true)]
        [LocalizedDynamicValidators(KeyName = "Date")]
        public string Date { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.ClaimTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "ClaimTypeID")]
        public int ClaimTypeID { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.Description", true)]
        [LocalizedDynamicValidators(KeyName = "Description")]
        public string Description { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.CurrencyID", true)]
        [LocalizedDynamicValidators(KeyName = "CurrencyID")]
        public int CurrencyID { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.ExchangeRate", true)]
        [LocalizedDynamicValidators(KeyName = "ExchangeRate")]
        public string ExchangeRate { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.Amount", true)]
        [LocalizedDynamicValidators(KeyName = "Amount")]
        public string Amount { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.TotalAmountAED", true)]
        [LocalizedDynamicValidators(KeyName = "TotalAmountAED")]
        public string TotalAmountAED { get; set; }

        [LocalizedDisplayName("ExpenseClaimRequestDetailModel.ExpClaimFileID", true)]
        [LocalizedDynamicValidators(KeyName = "ExpClaimFileID")]
        public int ExpClaimFileID { get; set; }

        public string FileName { get; set; }
        public bool AlreadyFileExist { get; set; }
    }

    public class ExpenseClaimStatusReportModel
    {
        public string BU_Name { get; set; }
        public string Department { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }      
        public string Designation { get; set; }
        public string ProjectName { get; set; }
        public decimal TotalExpenseAmount { get; set; }
        public decimal Amount { get; set; }                   
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
