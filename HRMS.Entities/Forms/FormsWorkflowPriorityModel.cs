﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class FormsWorkflowPriorityModel 
    {
        public Int16 PriorityListID { get; set; }

        public string PriorityListName { get; set; }

    
    }
}
