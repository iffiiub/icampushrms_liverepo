﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class AdminDashboardModel
    {
        [LocalizedDisplayName("AdminDashboardModel.EmployeeId", false)]
        [LocalizedDynamicValidators(KeyName = "EmployeeId")]
        public int EmployeeId { get; set; }
        [LocalizedDisplayName("AdminDashboardModel.IsProbationConfirmationComplete", false)]
        [LocalizedDynamicValidators(KeyName = "IsProbationConfirmationComplete")]
        public bool IsProbationConfirmationComplete { get; set; }
        [LocalizedDisplayName("AdminDashboardModel.IsGenerateProbationConfirmationRequest", false)]
        [LocalizedDynamicValidators(KeyName = "IsGenerateProbationConfirmationRequest")]
        public bool IsGenerateProbationConfirmationRequest { get; set; }
        [LocalizedDisplayName("AdminDashboardModel.RequestId", false)]
        [LocalizedDynamicValidators(KeyName = "RequestId")]
        public int RequestId { get; set; }
    }
}
