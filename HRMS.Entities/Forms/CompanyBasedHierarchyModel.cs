﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
   public class CompanyBasedHierarchyModel
    {
        [Key]
       public int  CBHierarchyID { get; set; }
        public int CompanyID { get; set; }
        public string Name { get; set; }
        public Int16 GroupID { get; set; }
        public string GroupName { get; set; }
        [Display(Name = "EmployeeID", GroupName = "")]
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public int autoID { get; set; }

    }
}
