﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class SeparationRequestFormModel
    {
        //public SeparationRequestFormModel()
        //{
        //    separationRequestType = new SeparationRequestTypeModel();
        //}
        public int ID { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.EmployeeID", true)]
        [LocalizedDynamicValidators(KeyName = "EmployeeID")]
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.Nationality", true)]
        public string Nationality { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.DOJ", true)]
        public string DOJ { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.Company", true)]
        public string Company { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.Department", true)]
        public string Department { get; set; }

        [LocalizedDisplayName("SeparationRequestFormModel.Project", true)]
        public string Project { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.Designation", true)]
        public string Designation { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.JobGrade", true)]
        public string JobGrade { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.MobileNumber", true)]
        public string MobileNumber { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.LineManager", true)]
        public string LineManager { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.LineManagerID", true)]
        public string LineManagerID { get; set; }

        [LocalizedDisplayName("SeparationRequestFormModel.DateOfSeparation", true)]
        [LocalizedDynamicValidators(KeyName = "DateOfSeparation")]
        //[Required(AllowEmptyStrings = false)]
        public string DateOfSeparation { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.LastWorkingDay", true)]
        [LocalizedDynamicValidators(KeyName = "LastWorkingDay")]
        //[Required(AllowEmptyStrings = false)]
        public string LastWorkingDay { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.NoticedPeriodDays", true)]
        public int NoticedPeriodDays { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.SeparationTypeID", true)]
        [LocalizedDynamicValidators(KeyName = "SeparationTypeID")]
        public Int16 SeparationTypeID { get; set; }

        [LocalizedDisplayName("SeparationRequestFormModel.SupportingDocFileID", true)]
        [LocalizedDynamicValidators(KeyName = "SupportingDocFileID")]
        public int SupportingDocFileID { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.InitiateHiringReplacement", true)]
        [LocalizedDynamicValidators(KeyName = "InitiateHiringReplacement")]

        public bool InitiateHiringReplacement { get; set; }
        public int? FormProcessID_Replacement { get; set; }
        public int? ClearanceFormProcessID { get; set; }
        public int? ExitInterviewFormProcessID { get; set; }
        public int? ClearanceRequestStatus { get; set; }

        public int? ClearanceRequestID { get; set; }

        public string EmployeeAlternativeID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public int RequesterEmployeeID { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.CompanyID", true)]
        public int CompanyID { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.ReasonForSeparation", true)]
        public string ReasonForSeparation { get; set; }
        [LocalizedDisplayName("SeparationRequestFormModel.Comments", true)]
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int RequestID { get; set; }
        public int FormProcessID { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
      //  public SeparationRequestTypeModel separationRequestType { get; set; }
        public string EmployeeName { get; set; }
        public string RequestDate { get; set; }
        public int SeparationRequestFormID { get; set; }
        public string SeparationTypeName_1 { get; set; }
        public bool IsAddMode { get; set; }
        public int ExitInterviewReqStatusID { get; set; }      
        public bool CanOpenRequest { get; set; }
        public int ExitInterviewRequestID { get; set; }
    }

    public class SeparationRequestTypeModel
    {
        public Int16 SeparationTypeID { get; set; }
        public string SeparationTypeName_1 { get; set; }
        public string SeparationTypeName_2 { get; set; }
        public string SeparationTypeName_3 { get; set; }
        public bool IsActive { get; set; }

    }

    public class SeparationStatusReportModel
    {
        public string BUName { get; set; }
        public int RequestID { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Project { get; set; }
        public string Designation { get; set; }
        public string Category { get; set; }
        public string SeparationType { get; set; }
        public string DateOfResignation { get; set; }
        public string LastWorkingDay { get; set; }
        public string ReasonOfResignation { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
