﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class FormsWorkflowGroupModel 
    {

        public Int16 GroupID { get; set; }

        public string GroupName { get; set; }

        public string SubTitle { get; set; }
        public string Description { get; set; }
        public bool SchoolBasedGroup { get; set; }

        public bool IsITGroup { get; set; }
        public bool CompOffHRGroup { get; set; }
        public bool IsHrGroup { get; set; }

    }
}
