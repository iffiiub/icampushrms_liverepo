﻿using HRMS.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class InterimProbationRequestModel
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        [LocalizedDisplayName("InterimProbationRequestModel.TypeEvaluationID", false)]
        [LocalizedDynamicValidators(KeyName = "TypeEvaluationID")]
        public int TypeEvaluationID { get; set; }
        [LocalizedDisplayName("InterimProbationRequestModel.FinalRatingScaleID", false)]
        [LocalizedDynamicValidators(KeyName = "FinalRatingScaleID")]
        public int FinalRatingScaleID { get; set; }
        public int ReqStatusID { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int RequesterEmployeeID { get; set; }
        public int CompanyID { get; set; }
        [LocalizedDisplayName("InterimProbationRequestModel.EmployeeConfirmedDetails", false)]
        [LocalizedDynamicValidators(KeyName = "EmployeeConfirmedDetails")]
        public string EmployeeConfirmedDetails { get; set; }
        [LocalizedDisplayName("InterimProbationRequestModel.LineManagerComments", false)]
        [LocalizedDynamicValidators(KeyName = "LineManagerComments")]
        public string LineManagerComments { get; set; }
        public string JobEvaluationName_3 { get; set; }
        public string JobEvaluationDetail { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName_1 { get; set; }
        public string Email { get; set; }
        public string TypeEvaluationName_1 { get; set; }
        public string LineManagerName { get; set; }
        public string LineManagerID { get; set; }
        public string PositionTitle { get; set; }
        public int RequestID { get; set; }
        public List<InterimProbationJobEvaluationModel> lstJobEvalModel { get; set; }
        public List<InterimProbationCodeOfConductModel> lstConductCode { get; set; }
        public List<InterimProbationOthersModel> lstOtherOption { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public string ProbationPeriod { get; set; }
        public string ProbationEndDate { get; set; }
        public string ProjectName { get; set; }
        public string JobGradeName_1 { get; set; }
        public bool RequestInitialize { get; set; }
        public int FormProcessID { get; set; }
        public string ManagerDesignation { get; set; }
    }
    public class InterimProbationRatingScaleModel
    {
        public int RatingScaleID { get; set; }
        public int Sequence { get; set; }
        public string RatingScaleName_1 { get; set; }
        public string RatingScaleCode { get; set; }
        public string RatingScaleDetail { get; set; }
    }

    public class InterimProbationJobEvaluationModel
    {
        public int JobEvaluationID { get; set; }
        public int Sequence { get; set; }
        public string JobEvaluationName_1 { get; set; }
        public string JobEvaluationName_2 { get; set; }
        public string JobEvaluationName_3 { get; set; }
        public string JobEvaluationDetail { get; set; }
        public int JobEvaluationDetailID { get; set; }
        public int RatingScaleID { get; set; }
    }

    public class InterimProbationCodeOfConductModel
    {
        public int CodeOfConductID { get; set; }
        public int Sequence { get; set; }
        public string CodeOfConductName_1 { get; set; }
        public string CodeOfConductName_2 { get; set; }
        public string CodeOfConductName_3 { get; set; }
        public string CodeOfConductDetail { get; set; }
        public int RatingScaleID { get; set; }
    }

    public class InterimProbationOthersModel
    {
        public int OthersOptionID { get; set; }
        public int Sequence { get; set; }
        public string OthersOptionName_1 { get; set; }
        public string OthersOptionName_2 { get; set; }
        public string OthersOptionName_3 { get; set; }
        public string OthersOptionDetail { get; set; }
        public int RatingScaleID { get; set; }
    }

}