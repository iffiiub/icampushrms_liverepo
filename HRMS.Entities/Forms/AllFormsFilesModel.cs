﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class AllFormsFilesModel
    {
        public int FileID { get; set; }
        public string FileName { get; set; }
        public byte[] DocumentFile { get; set; }
        public string FileContentType { get; set; }
        public string FormFileIDName { get; set; }
    }
}
