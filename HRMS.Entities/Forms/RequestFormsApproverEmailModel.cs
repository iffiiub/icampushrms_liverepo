﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class RequestFormsApproverEmailModel
    {
        //[ForeignKey("RequestFormsProcessModel")]
        public int FormProcessID { get; set; }
        public int RequestID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string WorkEmail { get; set; }
        public bool IsRequester { get; set; }
        public bool IsApprover { get; set; }
        public bool IsNextApprover { get; set; }
        public Int16 GroupID { get; set; }
        public string GroupName { get; set; }
        public string EmployeeFullName { get; set; }
        public string BUName { get; set; }
        public string BUShortName_1 { get; set; }
        public string FormName { get; set; }

    }
}
