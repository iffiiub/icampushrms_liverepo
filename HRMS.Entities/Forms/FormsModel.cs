﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.Forms
{
    public class FormsModel
    {

    }

    public class RequesterInfoViewModel
    {
        public string RequestID { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? RequestDate { get; set; }
        public int RequesterID { get; set; }
        public string RequesterAlternativeID { get; set; }
        public string RequesterName { get; set; }
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public int SuperviserID { get; set; }
        public string SuperviserName { get; set; }
        public int PositionID { get; set; }
        public string PositionTitle { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string WorkEmail { get; set; }
        public string PersonalEmail { get; set; }
        public string MobileNumberWork { get; set; }
        public int NationalityID { get; set; }
        public string NationalityName { get; set; }
        public string PassportNo { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? PassportIssueDate { get; set; }
        [DisplayFormat(DataFormatString =  ModelHelpers.DateFormat)]
        public DateTime? PassportExpireDate { get; set; }
        public string VisaNo { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? VisaIssueDate { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? VisaExpireDate { get; set; }
        public string VisaType { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? HireDate { get; set; }
        [DisplayFormat(DataFormatString = ModelHelpers.DateFormat)]
        public DateTime? BirthDate { get; set; }
        public string ProjectData { get; set; }
        [Required(ErrorMessage = "This field is mandatory")]
        public string JobGrade { get; set; }
        public Int16 FormID { get; set; }
        public int FormInstanceID { get; set; }
        public RecruitR1BudgetedModel RecruitmentDetails { get; set; }
        public int AcademicYearID { get; set; }
        public string AcademicYearName { get; set; }
        public int EmployeeId { get; set; }
    }
}
