﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using HRMS.Entities.Common;

namespace HRMS.Entities.Forms
{
    public class PendingJoiningRequestModel
    {
        public int ID { get; set; }
       
        public int? EmployeeId { get; set; }
       
        public string EmployeeName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName{ get; set; }
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
       
        public string JoiningDate { get; set; }
       
        public int OfferLetterFileID { get; set; }
       
        public int RequesterEmployeeID { get; set; }
       
        public string Comments { get; set; }
        public int RequestID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int autoID { get; set; }
        public int CompanyID { get; set; }
        public Int16 ReqStatusID { get; set; }
        public List<AllFormsFilesModel> AllFormsFilesModelList { get; set; }
        public List<RequestFormsApproveModel> RequestFormsApproveModelList { get; set; }
        public string Designation { get; set; }
        public string Organization { get; set; }
        public string EmployeeAlternativeID { get; set; }
    }

    public class EmployeeJoiningStatusReportModel
    {
        public string BUName { get; set; }
        public int RequestID { get; set; }
        public string Requester { get; set; }
        public string RequestDate { get; set; }
        public string EmployeeID { get; set; }
        public string RequesterID { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string Project { get; set; }
        public string Designation { get; set; }
        public string Category { get; set; }
        public string ExpectedJoiningDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentApprover { get; set; }
        public int FormProcessID { get; set; }
    }
}
