﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AllRequestsListViewModel
    {
        public int FormApprovalID { get; set; }
        public int FormProcessID { get; set; }
        public int GroupID { get; set; }
        public int FormID { get; set; }
        public int RequestID { get; set; }
        public int ReqStatusID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public int RequesterEmployeeID { get; set; }
        public string GroupName { get; set; }
        public string ModifiedEmployeeName { get; set; }
        public string RequesterEmployeeName { get; set; }
        public short ParentFormID { get; set; }
        public string RequesterCompanyName { get; set; }
        public string CompletedRejectedStatus { get; set; }
        public string ApproverEmployeeName { get; set; }
        public string FormName { get; set; }
        public string Status { get; set; }
        public string ControllerName { get; set; }
    }
}
