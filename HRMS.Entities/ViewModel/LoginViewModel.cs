﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter the email address")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Please enter valid Email Address")]
        public string Email { get; set; }

          [Required(ErrorMessage = "Please enter User Name")]
        public string Username { get; set; }
       
        
        [Required(ErrorMessage = "Please enter the password")]
        //[RegularExpression("^([A-Za-z0-9]+)$", ErrorMessage = "Enter valid password(alphaNumeric)")]
        public string Password { get; set; }

        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
        public bool IsMicrosoftAuthVerified { get; set; }
      
    }
}
