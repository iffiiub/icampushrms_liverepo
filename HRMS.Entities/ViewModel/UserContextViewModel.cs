﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities.ViewModel
{
    public class UserContextViewModel
    {
        public int UserId { get; set; }
        public String Name { get; set; }
        public int CompanyId { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public int UserRoleId { get; set; }
        public string Dateformat { get; set; }
        public string Password { set; get; }
        public List<int> Permisssion { get; set; }
        public List<int> ReadonlyPages { get; set; }
        public List<string> UserRolls { get; set; }
        public string ViewPagesSubCategoryIDs { set; get; }
        public string AddPagesSubCategoryIDs { set; get; }
        public string UpdatePagesSubCategoryIDs { set; get; }
        public string DeletePagesSubCategoryIDs { set; get; }
        public string OtherPermissionIDs { set; get; }
    }
}
