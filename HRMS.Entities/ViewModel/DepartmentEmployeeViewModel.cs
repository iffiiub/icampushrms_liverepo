﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HRMS.Entities.ViewModel
{


    public class DepartmentEmployeeViewModel
    {
        public IEnumerable<SelectListItem> departmentList { get; set; }
        //public IEnumerable<SelectListItem> DepartmentEmployeeList { get; set; }
        //public IEnumerable<SelectListItem> DepartmentEmployeeNotInDepartmentList { get; set; }

        public int DepartmentId { get; set; }
        public List<EmployeeDetails> DepartmentEmployeeList { get; set; }
        public List<EmployeeDetails> DepartmentEmployeeNotInDepartmentList { get; set; }

        public DepartmentEmployeeViewModel()
        {
            this.DepartmentEmployeeList = new List<EmployeeDetails>();
            this.DepartmentEmployeeNotInDepartmentList = new List<EmployeeDetails>();
        }
    }
}
