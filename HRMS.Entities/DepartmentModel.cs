﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class DepartmentModel : BaseModel
    {
        public int DepartmentId { get; set; }
        [Required(ErrorMessage = "This field is required.")]

        [Display(Name = "Company Name", GroupName = "Logger")]
        public int CompanyId { get; set; }

        [Display(Name = "Department Name (EN)", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string DepartmentName_1 { get; set; }

        [Display(Name = "Department Name (AR)", GroupName = "Logger")]
        public string DepartmentName_2 { get; set; }

        [Display(Name = "Department Name(FR)", GroupName = "Logger")]
        public string DepartmentName_3 { get; set; }

        [Display(Name = "Description", GroupName = "Logger")]
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        [Display(Name = "Start Date", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.DateTime)]
        public string StartDate { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        [Display(Name = "Department Head", GroupName = "Logger")]
        public int DepartmentHead { get; set; }

        [Display(Name = "Assistant 1", GroupName = "Logger")]
        public int Assistant1 { get; set; }

        [Display(Name = "Assistant 2", GroupName = "Logger")]
        public int Assistant2 { get; set; }

        [Display(Name = "Assistant 3", GroupName = "Logger")]
        public int Assistant3 { get; set; }

        public int DepartmentLeader { get; set; }
        public IEnumerable<SelectListItem> departmentList { get; set; }
        public string ShortCode { get; set; }
        // [Required(ErrorMessage = "Please Select Shift")]

        [Display(Name = "Shift", GroupName = "Logger")]
        public int ShiftID { get; set; }
        public string ShiftName { get; set; }
        public string CompanyName { get; set; }

        public List<int> DepartmentEmployees { get; set; }
    }

    public class DepartmentViewModel
    {
        public SelectList Shifts { get; set; }
        public SelectList EmployeeList { get; set; }
    }
}
