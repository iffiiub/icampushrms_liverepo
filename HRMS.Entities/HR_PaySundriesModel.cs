﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class HR_PaySundriesModel : BaseModel
    {
        [Required(ErrorMessage = "This field is required.")]
        public int PaySundriesID { set; get; }
        public int EmployeeID { set; get; }
        public string PaySundriesDate { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int HRSundryID { set; get; }       
        public decimal Amount { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int AcYear { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int PayBatchID { set; get; }
        public int generate { set; get; }
        public string fullname { set; get; }
        public string PayBatchName { set; get; }
        public string SundryName { set; get; }
        public string AccountNumber { set; get; }
        public string EmpName { set; get; }
        public string BankName { set; get; }
        public int BankID { set; get; }
        public string AcademicYear { set; get; }
    }

    public class PaySundryType
    {
        public int PaySundryID { get; set; }
        public string SundryName_1 { set; get; }
        public string SundryName_2 { set; get; }
        public string SundryName_3 { set; get; }
        public string AccountCode { get; set; }
        public bool IsAccommodation { get; set; }
        public bool IsFurniture { get; set; }
        public bool IsDeleted { get; set; }

    }
}
