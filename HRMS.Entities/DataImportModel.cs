﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class DataImportModel
    {
        public enum DataImportType
        {
            PayAddition = 1,
            Insurance = 2,
            Accommodation = 3,
            Airfare = 4
        }

        public int ImportTypeID { get; set; }
        public string ImportTypeName { get; set; }
        public string FileTemplatePath { get; set; }
    }

    public class DataImportPayAddition
    {
        public string PaySalaryAllowanceName { get; set; }
        public string EmployeeID { get; set; }
        public DateTime AdditionDate { get; set; }
        public string FormattedAdditionDate { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }

    }

    public class DataImportInsurance
    {
        public string InsuranceCategory { get; set; }
        public string InsuranceEligibility { get; set; }
        public string CardNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string InsuranceAmount { get; set; }
        public string AmountPaidBySchool { get; set; }
        public string EmployeeID { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string FormattedExpiryDate { get; set; }
        public DateTime IssueDate { get; set; }
        public string FormattedIssueDate { get; set; }
        public DateTime? CancellationDate { get; set; }
        public string FormattedCancellationDate { get; set; }
        public string Note { get; set; }

    }
    public class DataImportAccommodation
    {
        public string EmployeeID { get; set; }
        public string Type { get; set; }
        public string ApartmentNo { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Street { get; set; }
        public string Country { get; set; }
        public string City { get; set; }

    }

    public class DataImportAirfare
    {
        public string EmployeeID { get; set; }
        public string AirTicketCountry { get; set; }
        public string AirTicketCity { get; set; }
        public string AirTicketClass { get; set; }
        public string AirTicketAmount { get; set; }
        public string NoOfDependencies { get; set; }
        public string TicketClassForDependant { get; set; }
        public string AirTicketAmountDependannt { get; set; }
        public bool? AnnualAirTicket { set; get; }
        public string AirfareFrequency { get; set; }
        public string Airport { get; set; }
    }
}
