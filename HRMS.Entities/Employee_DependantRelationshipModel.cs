﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class Employee_DependantRelationshipModel : BaseModel
    {
        public int DependantRelationshipId { set; get; }
        public string RelationshipName { set; get; }
    }
}
