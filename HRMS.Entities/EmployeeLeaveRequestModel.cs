﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmployeeLeaveRequestModel : BaseModel
    {
        public int EmployeeLeaveId { set; get; }
        [Required(ErrorMessage = "Required")]
        public int EmployeeId { set; get; }
        [Required(ErrorMessage = "Required")]
        public int ApproverId { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int Reason { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public string Description { set; get; }
        //public Boolean Type { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public int leaveType { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public string StartDate { set; get; }
        [Required(ErrorMessage = "This field is required.")]
        public string EndDate { set; get; }
        public Boolean IsApproved { set; get; }
        public string CreatedOn { set; get; }
        public int? CreatedBy { set; get; }
        public string DeletedOn { set; get; }
        public int? DeletedBy { set; get; }
        public string NotificationSentTo { set; get; }
        [Display(Name = "Gender")]
        public int GenderID { set; get; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Nationality")]
        public int NationalityID { set; get; }
        [Display(Name = "Religion")]
        public int ReligionID { set; get; }
        public int DaysTaken { set; get; }
    }
}
