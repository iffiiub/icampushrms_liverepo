﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class VacationModel
    {
        public int id { get; set; }
        public int employeeId { get; set; }
        public string employeeAlternativeId { get; set; }
        public string employeeName { get; set; }
        public int sectionId { get; set; }
        public string sectionName { get; set; }
        public int vacationTypeId { get; set; }
        public string vacationType { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int vacationDayId { get; set; }
        public string backToWork { get; set; }
        public string Comments { get; set; }
        public int departmentId { get; set; }
        //Portal Reques parameters        
        public int NoOfDays { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }

        [Required(ErrorMessage = "This feild is Required")]
        public string RejectReason { get; set; }
        public int MonitorId { get; set; }
        public string CreatedDate { get; set; }
        public int AccumulativeOBID { get; set; }
        public decimal OpeningBalance { get; set; }
        public string OpeningBalanceDate { get; set; }
    }

    public class VacationTypeModel : IValidatableObject
    {
        public int VacationTypeId { set; get; }

        [Display(Name = "Name", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public string Name { set; get; }

        [Display(Name = "Paid", GroupName = "Logger")]
        public string PaidType { set; get; }

        [Display(Name = "Per Annum", GroupName = "Logger")]
        public int AnnualLeave { set; get; }

        [Display(Name = "Available after", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int ApplicableAfterDigits { set; get; }

        [Display(Name = "Applicable after type", GroupName = "Logger")]
        public int ApplicableAfterTypeID { set; get; }
        [Required(ErrorMessage = "This field is required.")]

        [Display(Name = "Gender", GroupName = "Logger")]
        public int? GenderID { set; get; }
         public int isCompensate { set; get; }
        public int PerServiceVacation { get; set; }

        [Display(Name = "Marital status", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int MaritalStatusId { get; set; }
        public int MonthlySplit { get; set; }

        [Display(Name = "Nationality", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int NationalityID { set; get; }
        [Required(ErrorMessage = "This field is required.")]

        //[Display(Name = "Number of days", GroupName = "Logger")]
        //public int NoOfDays { set; get; }

        [Display(Name = "Number of full paid days", GroupName = "Logger")]
        public int FullPaidDays { set; get; }

        [Display(Name = "Number of half paid days", GroupName = "Logger")]
        public int HalfPaidDays { set; get; }
        [Display(Name = "Number of un paid days", GroupName = "Logger")]
        public int UnPaidDays { set; get; }
        [Display(Name = "Number of days", GroupName = "Logger")]
        //public int NoOfDays { set; get; }
        public int TotalNoOfDays { set; get; }

        [Display(Name = "Religion", GroupName = "Logger")]
        [Required(ErrorMessage = "This field is required.")]
        public int ReligionID { set; get; }

        [Display(Name = "Repeat per service", GroupName = "Logger")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Required(ErrorMessage = "This field is required.")]
        public int RepeatedFor { set; get; }
        //   public int CompanyID { get; set; }
        public string NationalityName_1 { get; set; }
        public string GenderName_1 { get; set; }
        public string ReligionName_1 { get; set; }
        public int CompanyId { get; set; }
        public List<VactionPositionModel> positionList { get; set; }
        public List<VacationDepartmentModel> departmentList { get; set; }
        public List<VacationjobTitleModel> jobTitleList { get; set; }
        public List<VacationJobCategoryModel> jobCategoryList { get; set; }

        public int AccumulativeVacationID { get; set; }

        [Display(Name = "Accrual leave", GroupName = "Logger")]
        public bool isAccumulatedLeave { get; set; }

        //[Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {0}")]
        [Display(Name = "Maximum number of days to accrued", GroupName = "Logger")]
        public int AccMaxNumberOfDays { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Accrues every", GroupName = "Logger")]
        public int AccumulatePeriodNumber { get; set; }

        [Display(Name = "Accrues every type", GroupName = "Logger")]
        public int AccumulateTimeSpanId { get; set; }

        [Display(Name = "Accrues days", GroupName = "Logger")]
        public decimal AccumulateNumber { get; set; }

        [Display(Name = "Accumulate rounding type", GroupName = "Logger")]
        public int AccumulateRoundingTypeId { get; set; }

        [Display(Name = "Accrues start after", GroupName = "Logger")]
        public int AccumulateAfter { get; set; }

        [Display(Name = "Accrues start after type", GroupName = "Logger")]
        public int AccumulateAfterPeriod { get; set; }

        [Display(Name = "Maximum number of days to carry forward", GroupName = "Logger")]
        public int MaxNoOfDaystocarryForword { get; set; }

        public int BalanceDigitAfterDecimal { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //if (isAccumulatedLeave && (AccMaxNumberOfDays == 0 || AccumulatePeriodNumber == 0))
            //{
            //    yield return new ValidationResult("Please enter a value bigger than 0");
            //}
            if (isAccumulatedLeave && (VTCategoryID == 0))
            {
                yield return new ValidationResult("Please select Vacation Type Category");
            }
        }

        [Display(Name = "Displaye balance", GroupName = "Logger")]
        public int DisplayLeaveBalanceTypeId { get; set; }

        [Display(Name = "Annual leave", GroupName = "Logger")]
        public bool isAnualLeave { get; set; }

        //[Required(ErrorMessage = "This field is required.")]
        public string AccumulationStartFrom { get; set; }
        public int? AccumulationStart { get; set; }
        [Display(Name = "Vacation Type Category", GroupName = "Logger")]
        //[Required(ErrorMessage = "This field is required.")]
        public int? VTCategoryID { get; set; }
        public int? LeaveEntitleTypeID { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class VactionPositionModel
    {
        public int vacationPositionId { get; set; }
        public int vacationtypeId { get; set; }
        public int positionId { get; set; }
        public string positionName { get; set; }
    }

    public class VacationDepartmentModel
    {
        public int vacationDepartmentId { get; set; }
        public int vacationtypeId { get; set; }
        public int departmentId { get; set; }
        public string departmentName { get; set; }
    }

    public class VacationjobTitleModel
    {
        public int vacationJobTitletId { get; set; }
        public int vacationtypeId { get; set; }
        public int jobTitleId { get; set; }
        public string jobTitleName { get; set; }
    }

    public class VacationJobCategoryModel
    {
        public int vacationJobCategoryId { get; set; }
        public int vacationtypeId { get; set; }
        public int jobCategoryId { get; set; }
        public string jobCategoryName { get; set; }
    }

    public class VactionEmoloyeeInformationModel
    {
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public string hiredate { get; set; }
        public int genderId { get; set; }
        public int religionId { get; set; }
        public int positionId { get; set; }
        public int maritalStatusId { get; set; }
        public int shiftId { get; set; }
        public int departmentId { get; set; }
        public int sectionId { get; set; }
        public int nationalityId { get; set; }
        public bool isRulePass { get; set; }
        public string ruleMessage { get; set; }
        public int JobTitleId { get; set; }
        public int JobCategoryId { get; set; }

    }
    public class CurrentAnniversaryModel
    {
        public int LeaveTypeID { get; set; }
        public int EmployeeID { get; set; }
        public int VTCategoryID { get; set; }
        public int LeavesTakenInCurrentAnniversary { get; set; }
        public int LeavesTakenInCurrentAcademicYear { get; set; }
        public int NoOfLeavesWithOutMedicalCertificate { get; set; }
        public int NoOfLeavesPerService { get; set; }
        
    }
    public class VacationTypeCategoryModel
    {
        public int VTCategoryID { get; set; }
        public string VTCategoryName_1 { get; set; }
        public string VTCategoryName_2 { get; set; }
        public string VTCategoryName_3 { get; set; }
        public bool IsActive { get; set; }

    }
    public class LeaveBalanceInfoModel
    {
        public int EmployeeID { get; set; }
        public int LeaveTypeID { get; set; }
        public string LeaveTypeName { get; set; }
        public decimal TakenLeave { get; set; }
        public decimal RemainingLeave { get; set; }
        public decimal TotalLeaveDays { get; set; }
        public bool IsActive { get; set; }
        public int VTCategoryID { get; set; }
        public decimal FullPaidDays { get; set; }
        public decimal HalfPaidDays { get; set; }
        public decimal UnPaidDays { get; set; }
        public DateTime? LapseDate { get; set; }
        public decimal? LapsingDays { get; set; }
        public DateTime? ExtendedLapseDate { get; set; }
        public decimal? ExtendedLapsingDays { get; set; }
        public bool IsAccumulatedLeave { get; set; }
        public int BalanceDigitAfterDecimal { get; set; }
        public Int16 LeaveEntitleTypeID { get; set; }
    }
}
