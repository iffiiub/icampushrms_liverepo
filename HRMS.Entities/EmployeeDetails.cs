﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class EmployeeDetails
    {
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public int TitleID { get; set; }
        public string FirstName_1 { get; set; }
        public string FirstName_2 { get; set; }
        public string FirstName_3 { get; set; }
        public string SurName_1 { get; set; }
        public string SurName_2 { get; set; }
        public string SurName_3 { get; set; }
        public string MiddleName_1 { get; set; }
        public string MiddleName_2 { get; set; }
        public string MiddleName_3 { get; set; }
        public string MothersName_1 { get; set; }
        public string MothersName_2 { get; set; }
        public string MothersName_3 { get; set; }
        public int GenderID { get; set; }
        public int MaritalStatusID { get; set; }
        public int NationalityID { get; set; }
        public int ReligionID { get; set; }
        public int HomeLanguageID { get; set; }
        public DateTime BirthDate { get; set; }
        public int BirthPlaceID { get; set; }
        public int BirthCountryID { get; set; }
        public bool isSpecialNeeds { get; set; }
        public int UserID { get; set; }
        public int FamilyID { get; set; }
        public bool isActive { get; set; }
        public bool isPayActive { get; set; }
        public int usertypeid { get; set; }
        public int MailGenerated { get; set; }
        public bool isOnlineActive { get; set; }
        public string InitialPassword { get; set; }

        [NotMapped]
        public string FullName { get; set; }
        [NotMapped]
        public int isLeader { get; set; }
    }
}
