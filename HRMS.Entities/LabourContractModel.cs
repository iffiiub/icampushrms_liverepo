﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace HRMS.Entities
{
    public class LabourContractModel : BaseModel
    {
        public int DocLabourContract { set; get; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Document No")]
        public string DocumentNo { set; get; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Issue Country")]
        public int IssueCountry { set; get; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Issue Place")]
        public int IssuePlace { set; get; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Issue Date")]
        public string IssueDate { set; get; }
        [Display(Name = "Expiry Date")]
        public string ExpiryDate { set; get; }
        [Display(Name = "Note")]
        public string Note { set; get; }
        [Display(Name = "Primary")]
        public bool IsPrimary { set; get; }
        [Required(ErrorMessage = "Required")]
        public int EmployeeId { set; get; }
        public bool IsDeleted { set; get; }
        public string SponsorName { set; get; }
        public int ContractType { set; get; }
        public string LabourContractFile { set; get; }


        public IEnumerable<SelectListItem> HRContractTypeModelList { get; set; }
    }
}
