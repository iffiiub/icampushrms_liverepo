﻿using HRMS.Entities.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class EmployeeTrainingModel
    {
        public EmployeeTrainingModel()
        {
            this.employeeDetailsModelList = new List<EmployeeProfessionalTrainingModel>();
        }
        public int DepartmentId { get; set; }
        public int CompanyId { get; set; }
        public int EmployeeTrainingID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public int CourseTitle { get; set; }
        public string CourseTitleName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Fullname { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Cost { get; set; }
        public bool Deductable { get; set; }
        public string FirstName_1 { get; set; }
        public string SurName_1 { get; set; }
        public string Trainee { get; set; }
        public int EmployeeID { get; set; }
        public string Location { get; set; }
        public bool isAssign { get; set; }
        public double Allowance { get; set; }
        public int Results { get; set; }

        public string Note { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public int CityId { get; set; }

        public string CityName { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public int CountryId { get; set; }

        public string CountryName { get; set; }
        public int EmployeeAlternativeID { get; set; }

        public bool isComplete { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public decimal Total { get; set; }

        public int GenderID { get; set; }
        public bool isPresent { get; set; }
        public bool isModified { get; set; }

        //2019-02-13 , Task#9521 related bugs 
        public string HRCourseName_1 { get; set; }

        // public CityModel CityModel { get; set; }
        // public CountryModel CountryModel { get; set; }
        public List<EmployeeProfessionalTrainingModel> employeeDetailsModelList { get; set; }
        // //public List<GenderModel> genderList { get; set; }
        // public CourseModel CourseModel { get; set; }
        //// public ProfessionalDevelopmentModel ProfessionalDevelopmentModel { get; set; }
        // public GenderModel GenderModel { get; set; }       
    }


    public class EmployeeProfessionalTrainingModel
    {
        public int ProfessionalTrainingID { get; set; }
        public bool IsPresent { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeAlternativeID { get; set; }
        public string EmployeeName { get; set; }
        public int DepartmentId { get; set; }
    }
}
