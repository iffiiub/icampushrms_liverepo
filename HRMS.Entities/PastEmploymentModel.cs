﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.ComponentModel.DataAnnotations;
namespace HRMS.Entities
{
    public class PastEmploymentModel
    {
        

        public int PastEmploymentID { get; set; }
        public int EmployeeID { get; set; }

        [Display(Name = "Company", GroupName = "Logger")]
        public string CompanyName { get; set; }

        [Display(Name = "Start Date", GroupName = "Logger")]
        public string StartDate { get; set; }

        //[Required(ErrorMessage = "Please enter end date")]
        
        [Display(Name = "End Date", GroupName = "Logger")]        
        public string EndDate { get; set; }

        [Display(Name = "Department", GroupName = "Logger")]
        public string Department { get; set; }

        [Display(Name = "Position", GroupName = "Logger")]
        public string Position { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }
        [Display(Name = "Reference", GroupName = "Logger")]
        [StringLength(255, ErrorMessage = "Only 255 characters allowed")]
        public string EmpReferences { get; set; }
        [StringLength(255, MinimumLength = 3, ErrorMessage = "3 or 255 characters allowed")]
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public string FirstName_1 { get; set; }
        public string SurName_1 { get; set; }

        public int AttatchmentId { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please enter description")]
        public string Description { get; set; }
        [Display(Name = "File Size")]
        public string FileSize { get; set; }
        [Display(Name = "File Type")]
        [Required(ErrorMessage = "Please enter file type")]
        public string FileType { get; set; }
        [Display(Name = "File Path")]
        public string FilePath { get; set; }

        public List<PastEmploymentModel> objPastEmploymentList { get; set; }

        public PastEmploymentModel()
        {
            this.objPastEmploymentList = new List<PastEmploymentModel>();
        }
    }
}
