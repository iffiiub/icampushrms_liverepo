﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.Entities
{
    public class ProfessionalDevelopmentModel : BaseModel
    {

        public int TrainingID { get; set; }
        public int EmployeeID { get; set; }
        public bool isComplete { get; set; }
        public bool isAssign { get; set; }
        public string AssignDate { get; set; }
        public int CompletedOn { get; set; }

    }
}
