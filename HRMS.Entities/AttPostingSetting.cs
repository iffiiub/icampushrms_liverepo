﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class AttPostingSetting
    {
        public bool IsAutoPostingDailyAttendance { get; set; }
        public int LateAndDidntSignOutPosting { get; set; }
    }
}
