﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Entities
{
    public class EmploymentTypeModel : BaseModel
    {
        public int EmploymentTypeID { set; get; }
        public string EmploymentTypeName { set; get; }
    }
}
