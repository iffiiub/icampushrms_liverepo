﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace HRMS.Entities.Helpers
{
    public class LocalizedDisplayMsg
    {
        public static string GetResourceString(string keyPath)
        {
            string[] strArr = Regex.Split(keyPath,".");
            return GetValidationSetting(strArr[0],strArr[1]).DisplayText;
        }

        public static ValidationSetting GetValidationSetting(string ClassName, string VariableName)
        {
            ValidationSetting validationSetting = new ValidationSetting();

            XDocument xDoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/App_Data/Languages/en-US/"+ClassName+".xml"));
            try
            {
                List<XElement> TargetElements = xDoc.Element("ValidationSetting")                   
                   .Elements("ClassField").Single(x => (string)x.Attribute("Name") == VariableName).Elements().ToList();

                if (TargetElements != null)
                {
                    validationSetting.DisplayText = TargetElements[0].Value;
                    validationSetting.IsMandatory = Convert.ToBoolean(TargetElements[1].Value);
                    validationSetting.ErrorMessage = TargetElements[2].Value;
                }
            }
            catch (Exception ex)
            {


            }
            return validationSetting;
        }


    }
}