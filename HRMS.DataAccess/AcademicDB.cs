﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class AcademicDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails InsertAcademic(AcademicModel AcademicModel)
        {

            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aSntEmployeeID", AcademicModel.EmployeeID) ,
                      new SqlParameter("@aSntHRQualificationID", AcademicModel.HRQualificationID) ,
                      new SqlParameter("@aSntHRDegreeID", AcademicModel.HRDegreeID),
                      new SqlParameter("@aDttStartDate",AcademicModel.StartDate==null?DBNull.Value:(object)DateTime.ParseExact(AcademicModel.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)) ,
                      new SqlParameter("@aDttEndDate",  DateTime.ParseExact(AcademicModel.EndDate,"dd/MM/yyyy",CultureInfo.InvariantCulture)) ,
                      new SqlParameter("@aIntHRInstituteID",AcademicModel.HRInstituteID ) ,
                      new SqlParameter("@aBitMinistryApproved", AcademicModel.MinistryApproved ),
                      new SqlParameter("@aNvrNameAsInDegree",AcademicModel.NameAsInDegree),
                      new SqlParameter("@aSntCountryID",AcademicModel.CountryID),
                      new SqlParameter("@aCompleted",AcademicModel.Completed),
                      new SqlParameter("@aRelevant",AcademicModel.Relevant)
                    };
                int AcademicId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspAcademicCUD", parameters));
                return new OperationDetails(true, "Academic saved successfully.", null, AcademicId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Academic.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateAcademic(AcademicModel AcademicModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@aTntTranMode",2) ,
                         new SqlParameter("@aIntAcademicID",AcademicModel.AcademicID) ,
                      new SqlParameter("@aSntEmployeeID", AcademicModel.EmployeeID) ,
                      new SqlParameter("@aSntHRQualificationID", AcademicModel.HRQualificationID) ,
                      new SqlParameter("@aSntHRDegreeID", AcademicModel.HRDegreeID),
                      new SqlParameter("@aDttStartDate",AcademicModel.StartDate==null?DBNull.Value:(object) DateTime.ParseExact(AcademicModel.StartDate,"dd/MM/yyyy",CultureInfo.InvariantCulture)) ,
                      new SqlParameter("@aDttEndDate",  DateTime.ParseExact(AcademicModel.EndDate,"dd/MM/yyyy",CultureInfo.InvariantCulture)) ,
                      new SqlParameter("@aIntHRInstituteID",AcademicModel.HRInstituteID ) ,
                      new SqlParameter("@aBitMinistryApproved", AcademicModel.MinistryApproved ),
                      new SqlParameter("@aNvrNameAsInDegree",AcademicModel.NameAsInDegree),
                      new SqlParameter("@aSntCountryID",AcademicModel.CountryID),
                      new SqlParameter("@aCompleted",AcademicModel.Completed),
                      new SqlParameter("@aRelevant",AcademicModel.Relevant)

                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspAcademicCUD", parameters);
                return new OperationDetails(true, "Academic updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Academic.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteAcademicById(int AcademicId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntAcademicID",AcademicId) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspAcademicCUD", parameters);
                return new OperationDetails(true, "Academic deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Academic.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AcademicModel> GetAcademicList(int EmployeeId)
        {
            List<AcademicModel> AcademicModelList = new List<AcademicModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetAcademicList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                // Check if the reader returned any rows
                if (reader.HasRows)
                {
                    // While the reader has rows we loop through them,
                    // create new PassportModel, and insert them into our list
                    AcademicModel AcademicModel;
                    while (reader.Read())
                    {
                        AcademicModel = new AcademicModel();
                        AcademicModel.AcademicID = Convert.ToInt32(reader["AcademicID"].ToString());
                        AcademicModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                        AcademicModel.HRQualificationID = Convert.ToInt32(reader["HRQualificationID"].ToString());
                        AcademicModel.HRDegreeID = Convert.ToInt32(reader["HRDegreeID"].ToString());
                        AcademicModel.StartDate = reader["StartDate"].ToString();
                        AcademicModel.EndDate = reader["EndDate"].ToString();
                        AcademicModel.HRInstituteID = Convert.ToInt32(reader["HRInstituteID"].ToString());
                        AcademicModel.MinistryApproved = Convert.ToBoolean(reader["MinistryApproved"].ToString());
                        AcademicModel.NameAsInDegree = reader["NameAsInDegree"].ToString();
                        AcademicModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                        AcademicModel.HRQualificationName_1 = reader["HRQualificationName_1"].ToString();
                        AcademicModel.HRDegreeName_1 = reader["HRDegreeName_1"].ToString();
                        AcademicModel.HRInstituteName_1 = reader["HRInstituteName_1"].ToString();
                        AcademicModel.CountryName_1 = reader["CountryName_1"].ToString();
                        AcademicModel.Completed = reader["Completed"] == DBNull.Value ? false : Convert.ToBoolean(reader["Completed"].ToString());
                        AcademicModelList.Add(AcademicModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModelList.", exception);
                throw;
            }
            finally
            {

            }
            return AcademicModelList;
        }

        public List<CertificateModel> GetcertificateList(int EmployeeId)
        {
            List<CertificateModel> CertificationList = new List<CertificateModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetProfessionalCertificationList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                // Check if the reader returned any rows
                if (reader.HasRows)
                {
                    // While the reader has rows we loop through them,
                    // create new PassportModel, and insert them into our list
                    CertificateModel CertificateModel;
                    while (reader.Read())
                    {
                        CertificateModel = new CertificateModel();
                        CertificateModel.CertificateID = Convert.ToInt32(reader["CertificateID"].ToString());
                        CertificateModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                        CertificateModel.CertificationName = reader["CertificateName"].ToString();
                        CertificateModel.CertificationDate = Convert.ToDateTime(reader["CertificationDate"].ToString()).ToString("dd/MM/yyyy");
                        CertificateModel.HRInstituteID = Convert.ToInt32(reader["HRInstituteID"].ToString());
                        CertificateModel.MinistryApproved = Convert.ToBoolean(reader["MinistryApproved"].ToString());
                        CertificateModel.NameAsInDegree = reader["NameAsInDegree"].ToString();
                        CertificateModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                        CertificateModel.HRInstituteName_1 = reader["HRInstituteName_1"].ToString();
                        CertificateModel.CountryName_1 = reader["CountryName_1"].ToString();
                        CertificationList.Add(CertificateModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModelList.", exception);
                throw;
            }
            finally
            {

            }
            return CertificationList;
        }

        public AcademicModel AcademicById(int AcademicId)
        {
            try
            {
                AcademicModel AcademicModel = new AcademicModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetAcademicByID", new SqlParameter("@aIntAcademicID", AcademicId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            AcademicModel.AcademicID = Convert.ToInt32(reader["AcademicID"].ToString());
                            AcademicModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            AcademicModel.HRQualificationID = Convert.ToInt32(reader["HRQualificationID"].ToString());
                            AcademicModel.HRDegreeID = Convert.ToInt32(reader["HRDegreeID"].ToString());
                            AcademicModel.StartDate = reader["StartDate"] == DBNull.Value ? "" : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["StartDate"].ToString());
                            AcademicModel.EndDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["EndDate"].ToString());
                            AcademicModel.HRInstituteID = Convert.ToInt32(reader["HRInstituteID"].ToString());
                            AcademicModel.MinistryApproved = Convert.ToBoolean(reader["MinistryApproved"].ToString());
                            AcademicModel.NameAsInDegree = reader["NameAsInDegree"].ToString();
                            AcademicModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                            AcademicModel.Completed = Convert.ToBoolean(reader["Completed"].ToString());
                            AcademicModel.Relevant = Convert.ToBoolean(reader["Relevant"].ToString());
                        }
                    }
                }
                return AcademicModel;
                // Finally, we return AcademicModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertCertificate(CertificateModel Certificate)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aSntEmployeeID", Certificate.EmployeeID) ,
                      new SqlParameter("@aCertificateName", Certificate.CertificationName),
                      new SqlParameter("@aCertificationDate", GeneralDB.CommonDB.SetCulturedDate(Certificate.CertificationDate)) ,
                      new SqlParameter("@aIntHRInstituteID",Certificate.HRInstituteID ) ,
                      new SqlParameter("@aBitMinistryApproved", Certificate.MinistryApproved ),
                      new SqlParameter("@aNvrNameAsInDegree",Certificate.NameAsInDegree),
                      new SqlParameter("@aSntCountryID",Certificate.CountryID)
                    };
                int CertificateId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_STP_ProfessionalCertificateCUD  ", parameters));
                return new OperationDetails(true, "Certificate details saved successfully.", null, CertificateId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Certificate details.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateCertificate(CertificateModel Certificate)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",2) ,
                      new SqlParameter("@aSntEmployeeID", Certificate.EmployeeID) ,
                      new SqlParameter("@aIntCertificateID", Certificate.CertificateID) ,
                      new SqlParameter("@aCertificateName", Certificate.CertificationName),
                      new SqlParameter("@aCertificationDate", GeneralDB.CommonDB.SetCulturedDate(Certificate.CertificationDate)) ,
                      new SqlParameter("@aIntHRInstituteID",Certificate.HRInstituteID ) ,
                      new SqlParameter("@aBitMinistryApproved", Certificate.MinistryApproved ),
                      new SqlParameter("@aNvrNameAsInDegree",Certificate.NameAsInDegree),
                      new SqlParameter("@aSntCountryID",Certificate.CountryID)
                    };
                int CertificateId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_STP_ProfessionalCertificateCUD  ", parameters));
                return new OperationDetails(true, "Certificate details update successfully.", null, CertificateId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Certificate details.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeletecertificateById(int CertificateID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntCertificateID",CertificateID) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_STP_ProfessionalCertificateCUD", parameters);
                return new OperationDetails(true, "Certificate deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Certificate.", exception);
                throw;
            }
            finally
            {

            }
        }

        public CertificateModel GetCertificateById(int CertificateId)
        {
            try
            {
                CertificateModel CertificateModel = new CertificateModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_STP_GetProfessionalCertifiicateByID", new SqlParameter("@aIntCertficateID", CertificateId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            CertificateModel.CertificateID = Convert.ToInt32(reader["CertificateId"].ToString());
                            CertificateModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            CertificateModel.CertificationName = reader["CertificateName"].ToString();
                            CertificateModel.CertificationDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["CertificationDate"].ToString());
                            CertificateModel.HRInstituteID = Convert.ToInt32(reader["HRInstituteID"].ToString());
                            CertificateModel.MinistryApproved = Convert.ToBoolean(reader["MinistryApproved"].ToString());
                            CertificateModel.NameAsInDegree = reader["NameAsInDegree"].ToString();
                            CertificateModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                        }
                    }
                }
                return CertificateModel;
                // Finally, we return AcademicModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public bool CheckRelevant(int EmpId, int AcdamicId)
        {
            bool isRelevantExist = false;

            try
            {
                AcademicModel acModel = new AcademicModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CheckRelevantQualification", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmpId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    isRelevantExist = true;
                    while (sqlDataReader.Read())
                    {
                        acModel.AcademicID = Convert.ToInt32(sqlDataReader["AcademicID"]);
                        if (acModel.AcademicID == AcdamicId)
                        {
                            isRelevantExist = false;
                        }
                    }

                }
                sqlConnection.Close();
                return isRelevantExist;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }
       
    }
}
