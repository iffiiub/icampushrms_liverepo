﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using HRMS.Entities.ViewModel;
using System.Globalization;
using HRMS.Entities.Report;

namespace HRMS.DataAccess
{
    public class ReportingDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public DataHelper dataHelper;
        public string excusedString { get; set; }
        public string nonexcusedString { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<DocumentsModel> GetDocumentList(string EmpIds, int? DeptId, int? SectionId, int? DocTypeId, bool isPrimary, bool isExpired)
        {
            List<DocumentsModel> documentList = new List<DocumentsModel>();
            int count = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeDocumentsReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmpIds", EmpIds);
                sqlCommand.Parameters.AddWithValue("@DeptId", DeptId);
                sqlCommand.Parameters.AddWithValue("@SectionId", SectionId);
                sqlCommand.Parameters.AddWithValue("@DocTypeId", DocTypeId);
                sqlCommand.Parameters.AddWithValue("@isPrimary", isPrimary);
                sqlCommand.Parameters.AddWithValue("@isExpired", isExpired);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    DocumentsModel documentModel;
                    while (reader.Read())
                    {
                        documentModel = new DocumentsModel();
                        documentModel.SrNo = ++count;
                        documentModel.EmployeeId = reader["EmployeeAltId"].ToString();
                        documentModel.EmployeeName = reader["EmployeeName"].ToString();
                        documentModel.DocTypeId = Convert.ToInt32(reader["DocTypeId"].ToString());
                        documentModel.DocType = reader["DocType"].ToString();
                        documentModel.DocNo = reader["DocumentNo"].ToString();
                        documentModel.IssueDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                        documentModel.ExpiryDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                        documentModel.IssueCountry = reader["IssueCountry"].ToString();
                        documentModel.IssuePlace = reader["IssuePlace"].ToString();
                        documentModel.FilePath = reader["FilePath"].ToString();
                        documentModel.EmpDeptId = Convert.ToInt32(reader["EmployeeDeptId"].ToString());
                        documentModel.DepartmentName = reader["DepartmentName"].ToString();
                        documentModel.SectionId = Convert.ToInt32(reader["EmployeeSecId"].ToString());
                        documentModel.SectionName = reader["SectionName"].ToString();
                        documentModel.IsExpired = reader["IsExpired"].ToString() == "" ? false : Convert.ToBoolean(reader["IsExpired"].ToString());
                        documentModel.IsPrimary = reader["IsPrimary"].ToString() == "" ? false : Convert.ToBoolean(reader["IsPrimary"].ToString());
                        documentList.Add(documentModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return documentList;
        }

        public List<DocumentsModel> GetEmployeeLabourCardStatusReporting(int UserId)
        {
            List<DocumentsModel> employeeList = null;
            try
            {
                employeeList = new List<DocumentsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetLabourCardStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);

                DocumentsModel employeeModel = new DocumentsModel();


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        employeeModel = new DocumentsModel();
                        employeeModel.SrNo = Convert.ToInt32(sqlDataReader["Row"] == DBNull.Value ? "0" : sqlDataReader["Row"]);
                        employeeModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "0" : sqlDataReader["EmployeeName"]);
                        employeeModel.EmployeeId = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.SponsorName = CommonDB.FilterString(Convert.ToString(sqlDataReader["SponsorName"] == DBNull.Value ? "" : sqlDataReader["SponsorName"]));
                        employeeModel.MOLTitle = sqlDataReader["MOLTitle"].ToString();
                        employeeModel.MOLPersonalID = Convert.ToString(sqlDataReader["MOLPersonalId"] == DBNull.Value ? "" : sqlDataReader["MOLPersonalId"]);
                        employeeModel.EmployeeStatus = CommonDB.FilterString(Convert.ToString(sqlDataReader["EmployeeStatus"] == DBNull.Value ? "" : sqlDataReader["EmployeeStatus"]));
                        employeeModel.Note = Convert.ToString(sqlDataReader["Remark"] == DBNull.Value ? "" : sqlDataReader["Remark"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<LabourContract> GetDetailStaffReporting(int UserId)
        {
            List<LabourContract> employeeList = null;
            try
            {
                employeeList = new List<LabourContract>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_getEmployeeDocumentDetailsStaf", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                LabourContract employeeModel = new LabourContract();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        employeeModel = new LabourContract();
                        employeeModel.SrNo = Convert.ToInt32(sqlDataReader["Row"] == DBNull.Value ? "0" : sqlDataReader["Row"]);
                        employeeModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "0" : sqlDataReader["EmployeeName"]);
                        employeeModel.Nationality = Convert.ToString(sqlDataReader["Nationality"] == DBNull.Value ? "" : sqlDataReader["Nationality"]);
                        employeeModel.Profession = Convert.ToString(sqlDataReader["Profession"] == DBNull.Value ? "" : sqlDataReader["Profession"]);
                        employeeModel.VisaExpiryDate = Convert.ToString(sqlDataReader["ExpiryDate"] == DBNull.Value ? "" : sqlDataReader["ExpiryDate"]);
                        employeeModel.HealthCardNo = Convert.ToString(sqlDataReader["No"] == DBNull.Value ? "" : sqlDataReader["No"]);
                        employeeModel.HealthCardIssueDate = Convert.ToString(sqlDataReader["IssueDate"] == DBNull.Value ? "" : sqlDataReader["IssueDate"]);
                        employeeModel.HealthExpiryDate = Convert.ToString(sqlDataReader["HealthExpiryDate"] == DBNull.Value ? "" : sqlDataReader["HealthExpiryDate"]);
                        employeeModel.CompanyId = sqlDataReader["CompanyId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["CompanyId"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<CompanyEmployeeDetailReportingModel> GetCompanyStaffDetailHearderReporting()
        {
            List<CompanyEmployeeDetailReportingModel> employeeList = null;
            try
            {
                employeeList = new List<CompanyEmployeeDetailReportingModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_getEmployeeStaffDetailsCount", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                CompanyEmployeeDetailReportingModel employeeModel = new CompanyEmployeeDetailReportingModel();


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        employeeModel = new CompanyEmployeeDetailReportingModel();
                        employeeModel.Variable1 = Convert.ToString(sqlDataReader["OneYearEmployeeCount"] == DBNull.Value ? "0" : sqlDataReader["OneYearEmployeeCount"]);
                        employeeModel.Variable2 = Convert.ToString(sqlDataReader["ThreeYearEmployeeCount"] == DBNull.Value ? "0" : sqlDataReader["ThreeYearEmployeeCount"]);
                        employeeModel.Variable3 = Convert.ToString(sqlDataReader["GuidenceCouncelers"] == DBNull.Value ? "" : sqlDataReader["GuidenceCouncelers"]);
                        employeeModel.Variable4 = Convert.ToString(sqlDataReader["CertificatesCount"] == DBNull.Value ? "" : sqlDataReader["CertificatesCount"]);
                        employeeModel.Variable5 = Convert.ToString(sqlDataReader["ArabicCount"] == DBNull.Value ? "" : sqlDataReader["ArabicCount"]);
                        employeeModel.Variable6 = Convert.ToString(sqlDataReader["KHDAArabiCount"] == DBNull.Value ? "" : sqlDataReader["KHDAArabiCount"]);
                        employeeModel.Variable7 = Convert.ToString(sqlDataReader["IslamicCount"] == DBNull.Value ? "" : sqlDataReader["IslamicCount"]);
                        employeeModel.Variable8 = Convert.ToString(sqlDataReader["KHDAIslamiCount"] == DBNull.Value ? "" : sqlDataReader["KHDAIslamiCount"]);
                        employeeModel.Variable9 = Convert.ToString(sqlDataReader["LargestNationality"] == DBNull.Value ? "" : sqlDataReader["LargestNationality"]);
                        employeeModel.Variable10 = Convert.ToString(sqlDataReader["LargestNationalityPercentage"] == DBNull.Value ? "" : sqlDataReader["LargestNationalityPercentage"]);
                        employeeModel.Variable11 = Convert.ToString(sqlDataReader["EmployeeTurnover"] == DBNull.Value ? "" : sqlDataReader["EmployeeTurnover"]);

                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<StaffDetailsReportModel> GetCompanyStaffDetailContentReporting(string QualificationIds, string txtYearAtCompany)
        {
            List<StaffDetailsReportModel> staffDetailsList = null;
            try
            {
                staffDetailsList = new List<StaffDetailsReportModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeStaffDetailsForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@QualificationIds", QualificationIds);
                sqlCommand.Parameters.AddWithValue("@txtYearAtCompany", txtYearAtCompany);
                StaffDetailsReportModel staffDetails;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        staffDetails = new StaffDetailsReportModel();
                        staffDetails.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        staffDetails.QualificationName = Convert.ToString(sqlDataReader["Qualification"]);
                        staffDetails.LargestNationalityGroup = Convert.ToString(sqlDataReader["NationalityCount"]);
                        staffDetails.Nationality = Convert.ToString(sqlDataReader["Nationality"]);
                        staffDetails.NumberOfEmployeeOneYear = Convert.ToString(sqlDataReader["EmployeeCountOneYear"]);
                        staffDetails.NumberOfEmployeeThreeYear = Convert.ToString(sqlDataReader["EmployeeCountThreeYear"]);
                        staffDetails.YearAtCompany = Convert.ToString(sqlDataReader["YearsAtCompany"]);
                        staffDetails.ArabicCount = Convert.ToString(sqlDataReader["EmployeeArabicCount"]);
                        staffDetails.EmployeehavingAcademicCount = Convert.ToString(sqlDataReader["EmployeehavingAcademicCount"]);
                        staffDetailsList.Add(staffDetails);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            List<StaffDetailsReportModel> staffDetailsGroupList = new List<StaffDetailsReportModel>();
            StaffDetailsReportModel model;
            foreach (var item in staffDetailsList.GroupBy(x => x.EmployeeName))
            {
                model = new StaffDetailsReportModel();
                model.EmployeeName = item.FirstOrDefault().EmployeeName;
                model.QualificationName = String.Join(",", item.Select(x => x.QualificationName).ToArray());
                model.LargestNationalityGroup = item.FirstOrDefault().LargestNationalityGroup;
                model.Nationality = item.FirstOrDefault().Nationality;
                model.NumberOfEmployeeOneYear = item.FirstOrDefault().NumberOfEmployeeOneYear;
                model.NumberOfEmployeeThreeYear = item.FirstOrDefault().NumberOfEmployeeThreeYear;
                model.YearAtCompany = item.FirstOrDefault().YearAtCompany;
                model.ArabicCount = item.FirstOrDefault().ArabicCount;
                model.EmployeehavingAcademicCount = item.FirstOrDefault().EmployeehavingAcademicCount;
                staffDetailsGroupList.Add(model);
            }
            return staffDetailsGroupList;
        }

        public List<GeneralModel> GetCompanyDetailReporting(string EmployeeId)
        {
            List<GeneralModel> employeeList = null;
            try
            {
                employeeList = new List<GeneralModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_getEmployeeCredentialsByEmployeeID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeId);
                GeneralModel employeeModel = new GeneralModel();


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        employeeModel = new GeneralModel();
                        employeeModel.Value1 = Convert.ToString(sqlDataReader["EmployeeName"] == null ? "" : sqlDataReader["EmployeeName"]);
                        employeeModel.Value2 = Convert.ToString(sqlDataReader["Username"] == null ? "" : sqlDataReader["Username"]);
                        employeeModel.Value3 = Convert.ToString(sqlDataReader["Password"] == null ? "" : sqlDataReader["Password"]);
                        employeeModel.Value4 = Convert.ToString(sqlDataReader["Website"] == null ? "" : sqlDataReader["Website"]);
                        employeeModel.Value5 = Convert.ToString(sqlDataReader["Name"] == null ? "" : sqlDataReader["Name"]);
                        employeeModel.Value6 = Convert.ToString(sqlDataReader["Logo"] == null ? "" : sqlDataReader["Logo"]);

                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }



        public GeneralModel GetStaffInformationReport()
        {
            GeneralModel model = new GeneralModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("GetStaffInformationReport", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.Fill(ds);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    model.Value1 = dt.Rows[0][0].ToString();
                    model.Value2 = dt.Rows[0][0].ToString();
                    model.Value3 = dt.Rows[0][0].ToString();
                    model.Value4 = dt.Rows[0][0].ToString();
                    model.Value5 = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return model;
        }

        public List<StaffResponsibilities> GetStaffResponsibilitiesReport(int staffTypeId)
        {
            List<StaffResponsibilities> lstStaff = new List<StaffResponsibilities>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetStaffResponsibilitiesReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ReportType", staffTypeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    StaffResponsibilities staff;
                    while (sqlDataReader.Read())
                    {
                        staff = new StaffResponsibilities();
                        staff.Name = sqlDataReader["EmployeeName"].ToString();
                        staff.Section = sqlDataReader["Department"].ToString();
                        staff.Position = sqlDataReader["Position"].ToString();
                        staff.JATitle = sqlDataReader["JobTitle"].ToString();
                        staff.JAHireDate = sqlDataReader["JobHireDate"].ToString();
                        lstStaff.Add(staff);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstStaff;
        }

        #region Attendance Report

        public List<AttendanceReportModel> GetAttendanceViewList(string FromDate, string ToDate, string pageID, int requestId, bool? detailId, int? superVisorId, int? EmployeeId, string statusId = null, bool? Proceed = null,
            bool? active = null, int? aIntDepartmentID = null, int? aIntSectionID = null, int? aIntShiftID = null, int? UserId = null, bool? isPercentageBasedOndays=false)
        {
            pageID = pageID.ToLower();
            List<AttendanceReportModel> attendanceReportModelList = new List<AttendanceReportModel>();
            List<RatingPercentageModel> ratingPercentage = new RatingPercentageSettingDB().GetRatingPercentageSetting();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_uspGetAttRecordsBydate", sqlConnection);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@attFrom", CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@attTo", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@aVarEmployeeIds", EmployeeId);
                if (statusId == "11")
                {
                    sqlCommand.Parameters.AddWithValue("@statusId", "3,8");
                }
                else if (statusId == "12")
                {
                    sqlCommand.Parameters.AddWithValue("@statusId", "4,8");
                }
                else if (statusId == "13")
                {
                    sqlCommand.Parameters.AddWithValue("@statusId", "11");
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@statusId", statusId);
                }
                sqlCommand.Parameters.AddWithValue("@Proceed", Proceed);
                sqlCommand.Parameters.AddWithValue("@active", active);
                sqlCommand.Parameters.AddWithValue("@aIntDepartmentID", aIntDepartmentID);
                sqlCommand.Parameters.AddWithValue("@aIntSectionID", aIntSectionID);
                sqlCommand.Parameters.AddWithValue("@aIntShiftID", aIntShiftID);
                //sqlCommand.Parameters.AddWithValue("@UserId", null);
                sqlCommand.Parameters.AddWithValue("@Request", requestId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                string todayDay = DateTime.Now.ToString("ddd dd/MM/yyy");
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        AttendanceReportModel AttendanceReportModel = new AttendanceReportModel();
                        AttendanceReportModel.DepID = sqlDataReader["DepID"] == DBNull.Value ? "0" : sqlDataReader["DepID"].ToString();
                        AttendanceReportModel.DepName = sqlDataReader["DepName"] == DBNull.Value ? "" : sqlDataReader["DepName"].ToString();
                        AttendanceReportModel.DepHead = sqlDataReader["DepHeadName"] == DBNull.Value ? "" : sqlDataReader["DepHeadName"].ToString();
                        AttendanceReportModel.Section = sqlDataReader["SectionName"] == DBNull.Value ? "" : sqlDataReader["SectionName"].ToString();
                        AttendanceReportModel.SectionHead = sqlDataReader["SectionHeadName"] == DBNull.Value ? "" : sqlDataReader["SectionHeadName"].ToString();
                        AttendanceReportModel.Shift = sqlDataReader["ShiftName"] == DBNull.Value ? "" : sqlDataReader["ShiftName"].ToString();
                        AttendanceReportModel.DepBranch = sqlDataReader["HRSchoolBranchName"] == DBNull.Value ? "" : sqlDataReader["ShiftName"].ToString();
                        AttendanceReportModel.EmployeeID =Convert.ToInt32(sqlDataReader["HRID"].ToString());
                        switch (pageID)
                        {
                            case "workingdays":
                                //Working Days Report
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.TotalDays = sqlDataReader["TotalDays"].ToString(); //sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.WorkingDays = sqlDataReader["TotalEmpWorksDays"].ToString(); //sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.WorksDays = sqlDataReader["TotalWorksDays"].ToString(); //sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.PresentDays = sqlDataReader["TotalPresentDays"] == DBNull.Value ? "" : sqlDataReader["TotalPresentDays"].ToString();
                                AttendanceReportModel.AbsentDays = sqlDataReader["TotalAbsentDays"] == DBNull.Value ? "" : sqlDataReader["TotalAbsentDays"].ToString();
                                AttendanceReportModel.AExDays = sqlDataReader["TotalExcusedAbsentDays"] == DBNull.Value ? "" : sqlDataReader["TotalExcusedAbsentDays"].ToString();
                                AttendanceReportModel.ANonExDays = sqlDataReader["TotalNotExcusedAbsentDays"] == DBNull.Value ? "" : sqlDataReader["TotalNotExcusedAbsentDays"].ToString();
                                AttendanceReportModel.LateinDays = sqlDataReader["TotalLateDays"] == DBNull.Value ? "" : sqlDataReader["TotalLateDays"].ToString();
                                AttendanceReportModel.LExDays = sqlDataReader["TotalExcusedLateDays"] == DBNull.Value ? "" : sqlDataReader["TotalExcusedLateDays"].ToString();
                                AttendanceReportModel.LNonExDays = sqlDataReader["TotalNotExcusedLateDays"] == DBNull.Value ? "" : sqlDataReader["TotalNotExcusedLateDays"].ToString();
                                AttendanceReportModel.EarlyoutDays = sqlDataReader["TotalEarlyDays"] == DBNull.Value ? "" : sqlDataReader["TotalEarlyDays"].ToString();
                                AttendanceReportModel.EExDays = sqlDataReader["TotalExcusedEarlyDays"] == DBNull.Value ? "" : sqlDataReader["TotalExcusedEarlyDays"].ToString();
                                AttendanceReportModel.ENonExDays = sqlDataReader["TotalnotExcusedEarlyDays"] == DBNull.Value ? "" : sqlDataReader["TotalnotExcusedEarlyDays"].ToString();
                                AttendanceReportModel.HolidayDays = sqlDataReader["TotalHoldayDays"] == DBNull.Value ? "" : sqlDataReader["TotalHoldayDays"].ToString();
                                AttendanceReportModel.VacationDays = sqlDataReader["TotalVacationDays"] == DBNull.Value ? "" : sqlDataReader["TotalVacationDays"].ToString();
                                AttendanceReportModel.OffDays = sqlDataReader["TotalOffDayDays"] == DBNull.Value ? "" : sqlDataReader["TotalOffDayDays"].ToString();
                                AttendanceReportModel.ExtraDays = sqlDataReader["TotalExtraDays"] == DBNull.Value ? "" : sqlDataReader["TotalExtraDays"].ToString();
                                AttendanceReportModel.WorkHours = sqlDataReader["TotalWorksHrs"].ToString();
                                AttendanceReportModel.OverHours = sqlDataReader["TotalOverHrs"].ToString();
                                break;
                            case "workinghours":
                                //Workin Hours Report
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.ShiftWorkingHours = sqlDataReader["TotalShiftWorksHrs"].ToString();
                                AttendanceReportModel.WorkHours = sqlDataReader["TotalWorksHrs"].ToString();
                                AttendanceReportModel.OverHours = sqlDataReader["TotalOverHrs"].ToString();
                                AttendanceReportModel.ShortHours = sqlDataReader["TotalShortHrs"].ToString();
                                AttendanceReportModel.ExShortHours = sqlDataReader["TotalExcusedShortHrs"].ToString();
                                AttendanceReportModel.NonExShortHours = sqlDataReader["TotalNotExcusedShortHrs"].ToString();
                                break;
                            case "lateandearlyminutes":

                                //Late And Early Min Report
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.ShiftWorkingHours = sqlDataReader["TotalShiftWorksHrs"].ToString();
                                AttendanceReportModel.WorkHours = sqlDataReader["TotalWorksHrs"].ToString();
                                AttendanceReportModel.LateInMin = sqlDataReader["TotalLateMinutes"].ToString();
                                AttendanceReportModel.ExLateInMin = sqlDataReader["TotalExcusedLateMinutes"].ToString();
                                AttendanceReportModel.NonExLateInMin = sqlDataReader["TotalNotExcusedLateMinutes"].ToString();
                                AttendanceReportModel.EarlyOutMin = sqlDataReader["TotalEarlyMinutes"].ToString();
                                AttendanceReportModel.ExEarlyMin = sqlDataReader["TotalExcusedEarlyMinutes"].ToString();
                                AttendanceReportModel.NonExEarlyMin = sqlDataReader["TotalNotExcusedEarlyMinutes"].ToString();
                                break;
                            case "attendancestatistics":
                                //Attendance Statistics Report
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.status = sqlDataReader["AttStatusID"].ToString();
                                AttendanceReportModel.Date = sqlDataReader["attDate1"].ToString();
                                AttendanceReportModel.LateMinAfter = sqlDataReader["lateMinuteAfter"].ToString();
                                AttendanceReportModel.EarlyMinAfter = sqlDataReader["earlyMinuteAfter"].ToString();
                                AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                AttendanceReportModel.EarlyOutMin = sqlDataReader["earlyMinute"].ToString();
                                AttendanceReportModel.LateInMin = sqlDataReader["lateMinute"].ToString();
                                AttendanceReportModel.VacationId = sqlDataReader["VacationId"].ToString();
                                AttendanceReportModel.HolidayId = sqlDataReader["HolidayID"].ToString();
                                AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                AttendanceReportModel.ShiftWorkingHours = sqlDataReader["ShiftHrs"].ToString();
                                break;
                            case "dailyattendancebystatus":
                                //Daily Attendance By  Status
                                string[] arr = statusId.Split(',');
                                if (arr.Length > 1)
                                {
                                    statusId = "All";
                                }
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.TDate = Convert.ToDateTime(sqlDataReader["AttDate"]);
                                switch (statusId)
                                {
                                    case "1":
                                        {
                                            //AttendanceReportModel.EmpID = sqlDataReader["HRID"] == DBNull.Value ? "0" : sqlDataReader["HRID"].ToString();
                                            //AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                            AttendanceReportModel.ShiftOutTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.InTimes = sqlDataReader["InTime"].ToString();
                                            AttendanceReportModel.OutTimes = sqlDataReader["OutTime"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            break;
                                        }
                                    case "2":
                                        {
                                            //AttendanceReportModel.EmpID = sqlDataReader["HRID"] == DBNull.Value ? "0" : sqlDataReader["HRID"].ToString();
                                            //AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.Ex = ((Convert.ToInt32(sqlDataReader["absentExcused"].ToString()) == 1) ? excusedString : nonexcusedString);
                                            AttendanceReportModel.NonEx = ((Convert.ToInt32(sqlDataReader["absentExcused"].ToString()) == 0) ? excusedString : nonexcusedString);
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            AttendanceReportModel.PayAbsentTypeName = sqlDataReader["PayAbsentTypeName"] == DBNull.Value ? "" : sqlDataReader["PayAbsentTypeName"].ToString();
                                            break;
                                        }
                                    case "3":
                                    case "11":
                                        {
                                            DateTime dt = new DateTime();
                                            AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                            AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                            dt = DateTime.Parse(sqlDataReader["ShiftStartTime"].ToString());
                                            AttendanceReportModel.InTimes = sqlDataReader["InTime"].ToString();
                                            AttendanceReportModel.GraceTime = dt.AddMinutes(Convert.ToInt32(sqlDataReader["ShiftGraceIn"].ToString() == "" ? "0" : sqlDataReader["ShiftGraceIn"].ToString())).ToString("HH:mm");
                                            AttendanceReportModel.LateInMin = sqlDataReader["lateMinute"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            if (sqlDataReader["islateexcused"].ToString() != "")
                                            {
                                                AttendanceReportModel.Ex = ((Convert.ToBoolean(sqlDataReader["islateexcused"].ToString()) == true) ? excusedString : nonexcusedString);
                                                //AttendanceReportModel.NonEx = ((Convert.ToBoolean(sqlDataReader["islatededuct"].ToString()) == true) ? excusedString : nonexcusedString);
                                                AttendanceReportModel.NonEx = ((Convert.ToBoolean(sqlDataReader["islateexcused"].ToString()) == true) ? nonexcusedString : excusedString);
                                            }

                                            break;
                                        }
                                    case "4":
                                    case "12":
                                        {
                                            DateTime dt = new DateTime();
                                            AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                            AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.ShiftOutTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            dt = DateTime.Parse(sqlDataReader["ShiftEndTime"].ToString());
                                            AttendanceReportModel.GraceTime = dt.AddMinutes(-(Convert.ToInt32(sqlDataReader["ShiftGraceOut"].ToString() == "" ? "0" : sqlDataReader["ShiftGraceOut"].ToString()))).ToString("HH:mm");
                                            AttendanceReportModel.OutTimes = sqlDataReader["OutTime"].ToString();
                                            AttendanceReportModel.EarlyOutMin = sqlDataReader["earlyMinute"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            if (sqlDataReader["isearlyexcused"].ToString() != "")
                                            {
                                                AttendanceReportModel.Ex = ((Convert.ToBoolean(sqlDataReader["isearlyexcused"].ToString()) == true) ? excusedString : nonexcusedString);
                                                AttendanceReportModel.NonEx = ((Convert.ToBoolean(sqlDataReader["isearlyexcused"].ToString()) == true) ? nonexcusedString : excusedString);
                                            }
                                            break;
                                        }
                                    case "8":
                                        {
                                            AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                            AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                            AttendanceReportModel.ShiftOutTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.InTimes = sqlDataReader["InTime"].ToString();
                                            AttendanceReportModel.LateInMin = sqlDataReader["lateMinute"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            if (sqlDataReader["islateexcused"].ToString() != "")
                                            {
                                                AttendanceReportModel.Ex = ((Convert.ToBoolean(sqlDataReader["islateexcused"].ToString()) == true) ? excusedString : nonexcusedString);
                                                AttendanceReportModel.NonEx = ((Convert.ToBoolean(sqlDataReader["islateexcused"].ToString()) == true) ? nonexcusedString : excusedString);
                                            }
                                            AttendanceReportModel.OutTimes = sqlDataReader["OutTime"].ToString();
                                            AttendanceReportModel.EarlyOutMin = sqlDataReader["earlyMinute"].ToString();
                                            if (sqlDataReader["isearlyexcused"].ToString() != "")
                                            {
                                                AttendanceReportModel.OutEx = ((Convert.ToBoolean(sqlDataReader["isearlyexcused"].ToString()) == true) ? excusedString : nonexcusedString);
                                                AttendanceReportModel.OutNonEx = ((Convert.ToBoolean(sqlDataReader["isearlyexcused"].ToString()) == true) ? nonexcusedString : excusedString);
                                            }
                                            break;
                                        }
                                    case "9":
                                        {
                                            AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                            AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                            AttendanceReportModel.ShiftOutTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.InTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            break;
                                        }
                                    case "13":
                                        {
                                            AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                            AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.ShiftInTimes = sqlDataReader["ShiftStartTime"].ToString();
                                            AttendanceReportModel.ShiftOutTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.InTimes = sqlDataReader["ShiftStartTime"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());                                           
                                            break;
                                        }
                                    case "10":
                                        {
                                            AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                            AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                            AttendanceReportModel.InTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.OutTimes = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.TotalHours = sqlDataReader["ShiftEndTime"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            break;
                                        }
                                    case "All":
                                        {
                                            AttendanceReportModel.InTimes = sqlDataReader["InTime"].ToString();
                                            AttendanceReportModel.OutTimes = sqlDataReader["OutTime"].ToString();
                                            AttendanceReportModel.EarlyOutMin = sqlDataReader["earlyMinute"].ToString();
                                            AttendanceReportModel.LateInMin = sqlDataReader["lateMinute"].ToString();
                                            AttendanceReportModel.status = sqlDataReader["AttStatus"].ToString();
                                            AttendanceReportModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                                            break;
                                        }
                                }

                                break;
                            case "employeedailyattendance":

                                //Daily Attendance Repot
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.ShiftInTimes = Convert.ToString(sqlDataReader["ShiftStartTime"]);
                                AttendanceReportModel.ShiftOutTimes = Convert.ToString(sqlDataReader["ShiftEndTime"]);
                                AttendanceReportModel.InTimes = sqlDataReader["InTime"].ToString();
                                AttendanceReportModel.OutTimes = sqlDataReader["OutTime"].ToString();
                                AttendanceReportModel.status = sqlDataReader["AttStatus"].ToString();
                                AttendanceReportModel.statusId = sqlDataReader["AttStatusID"].ToString();
                                AttendanceReportModel.TotalHours = sqlDataReader["TotalHrs"].ToString();
                                AttendanceReportModel.Date = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["attDate1"].ToString());
                                AttendanceReportModel.DayDate1= sqlDataReader["AttDay"].ToString();
                                AttendanceReportModel.TDate = Convert.ToDateTime(sqlDataReader["attDate1"]);
                                AttendanceReportModel.VacationId= sqlDataReader["VacationID"].ToString();
                                AttendanceReportModel.SuperVisiorId = Convert.ToInt32(sqlDataReader["SuperViserId"] == DBNull.Value ? "0" : sqlDataReader["SuperViserId"]);
                                //Daily Attendance Details Repot
                                if (detailId == true)
                                {
                                    DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["attDate1"].ToString());
                                    AttendanceReportModel.TotalHours = sqlDataReader["TotalHrs"].ToString();
                                    AttendanceReportModel.ShiftWorkingHours = sqlDataReader["ShiftHrs"].ToString();
                                    AttendanceReportModel.OverHours = sqlDataReader["OverHrs"].ToString();
                                    AttendanceReportModel.ShortHours = sqlDataReader["ShortHrs"].ToString();
                                    AttendanceReportModel.LateInMin = sqlDataReader["lateMinute"].ToString();
                                    AttendanceReportModel.EarlyOutMin = sqlDataReader["earlyMinute"].ToString();
                                    AttendanceReportModel.Ex = (sqlDataReader["LateExcused"].ToString() == "0") ? (nonexcusedString) : (excusedString);
                                    AttendanceReportModel.NonEx = (sqlDataReader["earlyExcused"].ToString() == "0") ? (nonexcusedString) : (excusedString);
                                }

                                break;
                            case "monthlyattendancesummary":
                                //Monthly Attendance Summary Report
                                AttendanceReportModel.EmpID = sqlDataReader["HRAltID"] == DBNull.Value ? "0" : sqlDataReader["HRAltID"].ToString();
                                AttendanceReportModel.EmpName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                                AttendanceReportModel.StatusId1 = sqlDataReader["SatusId1"].ToString();
                                AttendanceReportModel.LateExcused1 = sqlDataReader["LateExcused1"].ToString();
                                AttendanceReportModel.EarlyExcused1 = sqlDataReader["EarlyExcused1"].ToString();
                                AttendanceReportModel.AbsentExcused1 = sqlDataReader["AbsentExcused1"].ToString();
                                AttendanceReportModel.DayDate1 = sqlDataReader["DayDate1"].ToString();

                                AttendanceReportModel.StatusId2 = sqlDataReader["SatusId2"].ToString();
                                AttendanceReportModel.LateExcused2 = sqlDataReader["LateExcused2"].ToString();
                                AttendanceReportModel.EarlyExcused2 = sqlDataReader["EarlyExcused2"].ToString();
                                AttendanceReportModel.AbsentExcused2 = sqlDataReader["AbsentExcused2"].ToString();
                                AttendanceReportModel.DayDate2 = sqlDataReader["DayDate2"].ToString();

                                AttendanceReportModel.StatusId3 = sqlDataReader["SatusId3"].ToString();
                                AttendanceReportModel.LateExcused3 = sqlDataReader["LateExcused3"].ToString();
                                AttendanceReportModel.EarlyExcused3 = sqlDataReader["EarlyExcused3"].ToString();
                                AttendanceReportModel.AbsentExcused3 = sqlDataReader["AbsentExcused3"].ToString();
                                AttendanceReportModel.DayDate3 = sqlDataReader["DayDate3"].ToString();

                                AttendanceReportModel.StatusId4 = sqlDataReader["SatusId4"].ToString();
                                AttendanceReportModel.LateExcused4 = sqlDataReader["LateExcused4"].ToString();
                                AttendanceReportModel.EarlyExcused4 = sqlDataReader["EarlyExcused4"].ToString();
                                AttendanceReportModel.AbsentExcused4 = sqlDataReader["AbsentExcused4"].ToString();
                                AttendanceReportModel.DayDate4 = sqlDataReader["DayDate4"].ToString();

                                AttendanceReportModel.StatusId5 = sqlDataReader["SatusId5"].ToString();
                                AttendanceReportModel.LateExcused5 = sqlDataReader["LateExcused5"].ToString();
                                AttendanceReportModel.EarlyExcused5 = sqlDataReader["EarlyExcused5"].ToString();
                                AttendanceReportModel.AbsentExcused5 = sqlDataReader["AbsentExcused5"].ToString();
                                AttendanceReportModel.DayDate5 = sqlDataReader["DayDate5"].ToString();

                                AttendanceReportModel.StatusId6 = sqlDataReader["SatusId6"].ToString();
                                AttendanceReportModel.LateExcused6 = sqlDataReader["LateExcused6"].ToString();
                                AttendanceReportModel.EarlyExcused6 = sqlDataReader["EarlyExcused6"].ToString();
                                AttendanceReportModel.AbsentExcused6 = sqlDataReader["AbsentExcused6"].ToString();
                                AttendanceReportModel.DayDate6 = sqlDataReader["DayDate6"].ToString();

                                AttendanceReportModel.StatusId7 = sqlDataReader["SatusId7"].ToString();
                                AttendanceReportModel.LateExcused7 = sqlDataReader["LateExcused7"].ToString();
                                AttendanceReportModel.EarlyExcused7 = sqlDataReader["EarlyExcused7"].ToString();
                                AttendanceReportModel.AbsentExcused7 = sqlDataReader["AbsentExcused7"].ToString();
                                AttendanceReportModel.DayDate7 = sqlDataReader["DayDate7"].ToString();

                                AttendanceReportModel.StatusId8 = sqlDataReader["SatusId8"].ToString();
                                AttendanceReportModel.LateExcused8 = sqlDataReader["LateExcused8"].ToString();
                                AttendanceReportModel.EarlyExcused8 = sqlDataReader["EarlyExcused8"].ToString();
                                AttendanceReportModel.AbsentExcused8 = sqlDataReader["AbsentExcused8"].ToString();
                                AttendanceReportModel.DayDate8 = sqlDataReader["DayDate8"].ToString();

                                AttendanceReportModel.StatusId9 = sqlDataReader["SatusId9"].ToString();
                                AttendanceReportModel.LateExcused9 = sqlDataReader["LateExcused9"].ToString();
                                AttendanceReportModel.EarlyExcused9 = sqlDataReader["EarlyExcused9"].ToString();
                                AttendanceReportModel.AbsentExcused9 = sqlDataReader["AbsentExcused9"].ToString();
                                AttendanceReportModel.DayDate9 = sqlDataReader["DayDate9"].ToString();

                                AttendanceReportModel.StatusId10 = sqlDataReader["SatusId10"].ToString();
                                AttendanceReportModel.LateExcused10 = sqlDataReader["LateExcused10"].ToString();
                                AttendanceReportModel.EarlyExcused10 = sqlDataReader["EarlyExcused10"].ToString();
                                AttendanceReportModel.AbsentExcused10 = sqlDataReader["AbsentExcused10"].ToString();
                                AttendanceReportModel.DayDate10 = sqlDataReader["DayDate10"].ToString();

                                AttendanceReportModel.StatusId11 = sqlDataReader["SatusId11"].ToString();
                                AttendanceReportModel.LateExcused11 = sqlDataReader["LateExcused11"].ToString();
                                AttendanceReportModel.EarlyExcused11 = sqlDataReader["EarlyExcused11"].ToString();
                                AttendanceReportModel.AbsentExcused11 = sqlDataReader["AbsentExcused11"].ToString();
                                AttendanceReportModel.DayDate11 = sqlDataReader["DayDate11"].ToString();

                                AttendanceReportModel.StatusId12 = sqlDataReader["SatusId12"].ToString();
                                AttendanceReportModel.LateExcused12 = sqlDataReader["LateExcused12"].ToString();
                                AttendanceReportModel.EarlyExcused12 = sqlDataReader["EarlyExcused12"].ToString();
                                AttendanceReportModel.AbsentExcused12 = sqlDataReader["AbsentExcused12"].ToString();
                                AttendanceReportModel.DayDate12 = sqlDataReader["DayDate12"].ToString();

                                AttendanceReportModel.StatusId13 = sqlDataReader["SatusId13"].ToString();
                                AttendanceReportModel.LateExcused13 = sqlDataReader["LateExcused13"].ToString();
                                AttendanceReportModel.EarlyExcused13 = sqlDataReader["EarlyExcused13"].ToString();
                                AttendanceReportModel.AbsentExcused13 = sqlDataReader["AbsentExcused13"].ToString();
                                AttendanceReportModel.DayDate13 = sqlDataReader["DayDate13"].ToString();

                                AttendanceReportModel.StatusId14 = sqlDataReader["SatusId14"].ToString();
                                AttendanceReportModel.LateExcused14 = sqlDataReader["LateExcused14"].ToString();
                                AttendanceReportModel.EarlyExcused14 = sqlDataReader["EarlyExcused14"].ToString();
                                AttendanceReportModel.AbsentExcused14 = sqlDataReader["AbsentExcused14"].ToString();
                                AttendanceReportModel.DayDate14 = sqlDataReader["DayDate14"].ToString();

                                AttendanceReportModel.StatusId15 = sqlDataReader["SatusId15"].ToString();
                                AttendanceReportModel.LateExcused15 = sqlDataReader["LateExcused15"].ToString();
                                AttendanceReportModel.EarlyExcused15 = sqlDataReader["EarlyExcused15"].ToString();
                                AttendanceReportModel.AbsentExcused15 = sqlDataReader["AbsentExcused15"].ToString();
                                AttendanceReportModel.DayDate15 = sqlDataReader["DayDate15"].ToString();

                                AttendanceReportModel.StatusId16 = sqlDataReader["SatusId16"].ToString();
                                AttendanceReportModel.LateExcused16 = sqlDataReader["LateExcused16"].ToString();
                                AttendanceReportModel.EarlyExcused16 = sqlDataReader["EarlyExcused16"].ToString();
                                AttendanceReportModel.AbsentExcused16 = sqlDataReader["AbsentExcused16"].ToString();
                                AttendanceReportModel.DayDate16 = sqlDataReader["DayDate16"].ToString();

                                AttendanceReportModel.StatusId17 = sqlDataReader["SatusId17"].ToString();
                                AttendanceReportModel.LateExcused17 = sqlDataReader["LateExcused17"].ToString();
                                AttendanceReportModel.EarlyExcused17 = sqlDataReader["EarlyExcused17"].ToString();
                                AttendanceReportModel.AbsentExcused17 = sqlDataReader["AbsentExcused17"].ToString();
                                AttendanceReportModel.DayDate17 = sqlDataReader["DayDate17"].ToString();

                                AttendanceReportModel.StatusId18 = sqlDataReader["SatusId18"].ToString();
                                AttendanceReportModel.LateExcused18 = sqlDataReader["LateExcused18"].ToString();
                                AttendanceReportModel.EarlyExcused18 = sqlDataReader["EarlyExcused18"].ToString();
                                AttendanceReportModel.AbsentExcused18 = sqlDataReader["AbsentExcused18"].ToString();
                                AttendanceReportModel.DayDate18 = sqlDataReader["DayDate18"].ToString();

                                AttendanceReportModel.StatusId19 = sqlDataReader["SatusId19"].ToString();
                                AttendanceReportModel.LateExcused19 = sqlDataReader["LateExcused19"].ToString();
                                AttendanceReportModel.EarlyExcused19 = sqlDataReader["EarlyExcused19"].ToString();
                                AttendanceReportModel.AbsentExcused19 = sqlDataReader["AbsentExcused19"].ToString();
                                AttendanceReportModel.DayDate19 = sqlDataReader["DayDate19"].ToString();

                                AttendanceReportModel.StatusId20 = sqlDataReader["SatusId20"].ToString();
                                AttendanceReportModel.LateExcused20 = sqlDataReader["LateExcused20"].ToString();
                                AttendanceReportModel.EarlyExcused20 = sqlDataReader["EarlyExcused20"].ToString();
                                AttendanceReportModel.AbsentExcused20 = sqlDataReader["AbsentExcused20"].ToString();
                                AttendanceReportModel.DayDate20 = sqlDataReader["DayDate20"].ToString();

                                AttendanceReportModel.StatusId21 = sqlDataReader["SatusId21"].ToString();
                                AttendanceReportModel.LateExcused21 = sqlDataReader["LateExcused21"].ToString();
                                AttendanceReportModel.EarlyExcused21 = sqlDataReader["EarlyExcused21"].ToString();
                                AttendanceReportModel.AbsentExcused21 = sqlDataReader["AbsentExcused21"].ToString();
                                AttendanceReportModel.DayDate21 = sqlDataReader["DayDate21"].ToString();

                                AttendanceReportModel.StatusId22 = sqlDataReader["SatusId22"].ToString();
                                AttendanceReportModel.LateExcused22 = sqlDataReader["LateExcused22"].ToString();
                                AttendanceReportModel.EarlyExcused22 = sqlDataReader["EarlyExcused22"].ToString();
                                AttendanceReportModel.AbsentExcused22 = sqlDataReader["AbsentExcused22"].ToString();
                                AttendanceReportModel.DayDate22 = sqlDataReader["DayDate22"].ToString();

                                AttendanceReportModel.StatusId23 = sqlDataReader["SatusId23"].ToString();
                                AttendanceReportModel.LateExcused23 = sqlDataReader["LateExcused23"].ToString();
                                AttendanceReportModel.EarlyExcused23 = sqlDataReader["EarlyExcused23"].ToString();
                                AttendanceReportModel.AbsentExcused23 = sqlDataReader["AbsentExcused23"].ToString();
                                AttendanceReportModel.DayDate23 = sqlDataReader["DayDate23"].ToString();

                                AttendanceReportModel.StatusId24 = sqlDataReader["SatusId24"].ToString();
                                AttendanceReportModel.LateExcused24 = sqlDataReader["LateExcused24"].ToString();
                                AttendanceReportModel.EarlyExcused24 = sqlDataReader["EarlyExcused24"].ToString();
                                AttendanceReportModel.AbsentExcused24 = sqlDataReader["AbsentExcused24"].ToString();
                                AttendanceReportModel.DayDate24 = sqlDataReader["DayDate24"].ToString();

                                AttendanceReportModel.StatusId25 = sqlDataReader["SatusId25"].ToString();
                                AttendanceReportModel.LateExcused25 = sqlDataReader["LateExcused25"].ToString();
                                AttendanceReportModel.EarlyExcused25 = sqlDataReader["EarlyExcused25"].ToString();
                                AttendanceReportModel.AbsentExcused25 = sqlDataReader["AbsentExcused25"].ToString();
                                AttendanceReportModel.DayDate25 = sqlDataReader["DayDate25"].ToString();

                                AttendanceReportModel.StatusId26 = sqlDataReader["SatusId26"].ToString();
                                AttendanceReportModel.LateExcused26 = sqlDataReader["LateExcused26"].ToString();
                                AttendanceReportModel.EarlyExcused26 = sqlDataReader["EarlyExcused26"].ToString();
                                AttendanceReportModel.AbsentExcused26 = sqlDataReader["AbsentExcused26"].ToString();
                                AttendanceReportModel.DayDate26 = sqlDataReader["DayDate26"].ToString();

                                AttendanceReportModel.StatusId27 = sqlDataReader["SatusId27"].ToString();
                                AttendanceReportModel.LateExcused27 = sqlDataReader["LateExcused27"].ToString();
                                AttendanceReportModel.EarlyExcused27 = sqlDataReader["EarlyExcused27"].ToString();
                                AttendanceReportModel.AbsentExcused27 = sqlDataReader["AbsentExcused27"].ToString();
                                AttendanceReportModel.DayDate27 = sqlDataReader["DayDate27"].ToString();

                                AttendanceReportModel.StatusId28 = sqlDataReader["SatusId28"].ToString();
                                AttendanceReportModel.LateExcused28 = sqlDataReader["LateExcused28"].ToString();
                                AttendanceReportModel.EarlyExcused28 = sqlDataReader["EarlyExcused28"].ToString();
                                AttendanceReportModel.AbsentExcused28 = sqlDataReader["AbsentExcused28"].ToString();
                                AttendanceReportModel.DayDate28 = sqlDataReader["DayDate28"].ToString();

                                AttendanceReportModel.StatusId29 = sqlDataReader["SatusId29"].ToString();
                                AttendanceReportModel.LateExcused29 = sqlDataReader["LateExcused29"].ToString();
                                AttendanceReportModel.EarlyExcused29 = sqlDataReader["EarlyExcused29"].ToString();
                                AttendanceReportModel.AbsentExcused29 = sqlDataReader["AbsentExcused29"].ToString();
                                AttendanceReportModel.DayDate29 = sqlDataReader["DayDate29"].ToString();

                                AttendanceReportModel.StatusId30 = sqlDataReader["SatusId30"].ToString();
                                AttendanceReportModel.LateExcused30 = sqlDataReader["LateExcused30"].ToString();
                                AttendanceReportModel.EarlyExcused30 = sqlDataReader["EarlyExcused30"].ToString();
                                AttendanceReportModel.AbsentExcused30 = sqlDataReader["AbsentExcused30"].ToString();
                                AttendanceReportModel.DayDate30 = sqlDataReader["DayDate30"].ToString();

                                AttendanceReportModel.StatusId31 = sqlDataReader["SatusId31"].ToString();
                                AttendanceReportModel.LateExcused31 = sqlDataReader["LateExcused31"].ToString();
                                AttendanceReportModel.EarlyExcused31 = sqlDataReader["EarlyExcused31"].ToString();
                                AttendanceReportModel.AbsentExcused31 = sqlDataReader["AbsentExcused31"].ToString();
                                AttendanceReportModel.DayDate31 = sqlDataReader["DayDate31"].ToString();
                                break;
                        }

                        attendanceReportModelList.Add(AttendanceReportModel);

                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (pageID == "attendancestatistics")
            {
                DateTime fromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime toDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime currentdate = DateTime.Now;
                List<AttendanceReportModel> newList = new List<AttendanceReportModel>();
                AttendanceReportModel attendanceRecord;
                int totalSaturday = CommonDB.FindDayBetweenDates(fromDate, toDate, "Saturday");
                int totalSaturdayPresent = 0;
                int[] presentDaysArray = { 1, 3, 4, 8, 9, 11 };
                int[] absentDaysArray = { 2 };
                int[] holidayDaysArray = { 5 };
                int[] earlyMinArray = { 4, 8 };
                int[] lateMinArray = { 3, 8 };
                int totalPresentDays = 0;
                int totalWorkDays = 0;
                int totalAbsentDays = 0;
                int totalHolidaysDays = 0;
                int tempStatusId = 0;
                int totalEarlyMin = 0;
                int totalLateMin = 0;
                int totalWorkDaysMin = 0;
                int totalPresentDaysMin = 0;
                int totalDays = (int)((toDate - fromDate).TotalDays + 1);
                int statusTemp = attendanceReportModelList.Where(x => x.status == "2").Count();
                foreach (var item in attendanceReportModelList.GroupBy(x => x.EmpID))
                {
                    attendanceRecord = new AttendanceReportModel();
                    attendanceRecord.EmpID = item.FirstOrDefault().EmpID;
                    attendanceRecord.EmployeeID = item.FirstOrDefault().EmployeeID;
                    attendanceRecord.EmpName = item.FirstOrDefault().EmpName;
                    attendanceRecord.SatPresent = totalSaturday.ToString();
                    if (Convert.ToInt32(attendanceRecord.WorkingDays) > 0)
                    {
                        attendanceRecord.AttendancePercent = (Math.Round(((Convert.ToDecimal(attendanceRecord.PresentDays)) * 100) / (Convert.ToDecimal(attendanceRecord.WorkingDays)), 2));
                    }
                    foreach (var nestedItem in item)
                    {
                        tempStatusId = Convert.ToInt32(nestedItem.status);
                        currentdate = Convert.ToDateTime(nestedItem.Date);
                        TimeSpan shifthrs;
                        if (!(nestedItem.VacationId != "" || nestedItem.HolidayId != "" || nestedItem.ShiftInTimes == "00:00:00"))
                        {
                            totalWorkDays = totalWorkDays + 1;
                            shifthrs = TimeSpan.Parse(nestedItem.ShiftWorkingHours);
                            totalWorkDaysMin = totalWorkDaysMin + Convert.ToInt32(shifthrs.TotalMinutes);
                        }

                        if (presentDaysArray.Contains(tempStatusId))
                        {
                            totalPresentDays = totalPresentDays + 1;
                            totalSaturdayPresent = (CommonDB.CheckDayInDate(currentdate, "Saturday") ? (totalSaturdayPresent + 1) : totalSaturdayPresent);
                            shifthrs = TimeSpan.Parse(nestedItem.ShiftWorkingHours);
                            totalPresentDaysMin = totalPresentDaysMin + Convert.ToInt32(shifthrs.TotalMinutes);
                        }
                        if (absentDaysArray.Contains(tempStatusId))
                        {
                            totalAbsentDays = totalAbsentDays + 1;
                        }
                        if (holidayDaysArray.Contains(tempStatusId))
                        {
                            totalHolidaysDays = totalHolidaysDays + 1;
                        }
                        if (earlyMinArray.Contains(tempStatusId) && nestedItem.EarlyMinAfter != "0")
                        {
                            totalEarlyMin = totalEarlyMin + Convert.ToInt32(nestedItem.EarlyOutMin);
                        }
                        if (lateMinArray.Contains(tempStatusId) && nestedItem.LateMinAfter != "0")
                        {
                            totalLateMin = totalLateMin + Convert.ToInt32(nestedItem.LateInMin);
                        }

                    }
                    if (totalPresentDays > 0)
                    {
                        if (isPercentageBasedOndays==true)
                        {
                            attendanceRecord.AttendancePercent = Math.Round(Convert.ToDecimal((totalPresentDays * 100) /totalDays), 2);
                        }
                        else
                        {
                            attendanceRecord.AttendancePercent = Math.Round(Convert.ToDecimal(((totalPresentDaysMin - (totalLateMin + totalEarlyMin)) * 100) / totalWorkDaysMin), 2);
                        }

                    }
                    else
                        attendanceRecord.AttendancePercent = 0;
                    foreach (var iratingPercentage in ratingPercentage)
                    {
                        if (iratingPercentage.RatingFrom <= Convert.ToDecimal(attendanceRecord.AttendancePercent) && iratingPercentage.RatingTo >= Convert.ToDecimal(attendanceRecord.AttendancePercent))
                            attendanceRecord.Remark = iratingPercentage.RatingName_1;
                    }
                    attendanceRecord.AttendancePercent = attendanceRecord.AttendancePercent;
                    attendanceRecord.PresentDays = totalPresentDays.ToString();
                    attendanceRecord.TotalDays = totalDays.ToString();
                    attendanceRecord.WorkingDays = totalWorkDays.ToString();
                    attendanceRecord.AbsentDays = totalAbsentDays.ToString();
                    attendanceRecord.HolidayDays = totalHolidaysDays.ToString();
                    attendanceRecord.EarlyOutMin = totalEarlyMin.ToString();
                    attendanceRecord.LateInMin = totalLateMin.ToString();
                    attendanceRecord.SatPresent = attendanceRecord.SatPresent + "/" + totalSaturdayPresent;
                    totalPresentDays = 0;
                    totalWorkDays = 0;
                    totalAbsentDays = 0;
                    totalHolidaysDays = 0;
                    totalEarlyMin = 0;
                    totalLateMin = 0;
                    totalSaturdayPresent = 0;
                    totalWorkDaysMin = 0;
                    totalPresentDaysMin = 0;
                    newList.Add(attendanceRecord);
                }
                return newList;
            }
            else
            {
                if (pageID == "employeedailyattendance")
                {
                    if (superVisorId != null && superVisorId > 0)
                    {
                        return attendanceReportModelList.Where(x => x.statusId != "7" && x.SuperVisiorId == superVisorId).ToList();
                    }
                    else
                    {
                        return attendanceReportModelList.Where(x => x.statusId != "7").ToList();
                    }
                }
                else
                    return attendanceReportModelList;
            }
        }

        #endregion

        #region Deduction Report

        public IEnumerable<DeductionReportModel> GetAbsentdeductableReport(int? usedid, string startDate, string toDate, bool? bConfirmed, bool? bGenrated, bool? bSelectedLateFrom)
        {
            DataAccess.GeneralDB.CommonDB common = new DataAccess.GeneralDB.CommonDB();
            List<DeductionReportModel> undeductableList = new List<DeductionReportModel>();
            try
            {
                bool isDeductionRulesEnable = new DeductionDB().GetDeductionRuleSetting().AppliedDeductionRules;
                string AmountFormat = "";
                AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (isDeductionRulesEnable)
                {
                    sqlCommand = new SqlCommand("HR_Stp_GetAbsentWithDeductionRulesReport", sqlConnection);
                }
                else
                {
                    sqlCommand = new SqlCommand("HR_uspGetAbsentsForPeriod", sqlConnection);
                }

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.Parameters.AddWithValue("@aFromDate", CommonDB.SetCulturedDate(startDate));
                sqlCommand.Parameters.AddWithValue("@aToDate", CommonDB.SetCulturedDate(toDate));
                sqlCommand.Parameters.AddWithValue("@UserId", usedid);
                if (bConfirmed != null)
                {
                    sqlCommand.Parameters.AddWithValue("@aBitIsConfirmed", bConfirmed);
                }              
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionReportModel gModel;
                    while (sqlDataReader.Read())
                    {
                        gModel = new DeductionReportModel();
                        gModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        gModel.Id = sqlDataReader["EmployeeAlternativeId"].ToString();
                        gModel.Name = sqlDataReader["EmployeeName"].ToString();
                        gModel.Days = sqlDataReader["Days"].ToString();
                        gModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"].ToString() == "" ? "0" : sqlDataReader["Amount"].ToString()).ToString(AmountFormat);
                        gModel.AmountInDecimal = Convert.ToDecimal(sqlDataReader["Amount"].ToString() == "" ? "0" : sqlDataReader["Amount"].ToString());
                        // gModel.FormDate = Convert.ToDateTime(sqlDataReader["FromDate"].ToString()).ToString("dd/MM/yyyy ddd");
                        gModel.FormDate = (Convert.ToInt32(gModel.Days) >= 4 ? DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToDateTime(sqlDataReader["FromDate"].ToString()).ToShortDateString()) + " - " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToDateTime(sqlDataReader["ToDate"].ToString()).ToShortDateString()) : DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToDateTime(sqlDataReader["FromDate"].ToString()).ToShortDateString())) + "</label>";
                        gModel.ToDate = Convert.ToDateTime(sqlDataReader["ToDate"].ToString()).ToString("dd/MM/yyyy ddd");
                        gModel.AttendanceDateTime = Convert.ToDateTime(sqlDataReader["FromDate"].ToString());
                        gModel.EffectiveDate = sqlDataReader["EffectiveDate"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["EffectiveDate"].ToString()).ToString("dd/MM/yyyy");
                        gModel.Confirmed = (sqlDataReader["IsConfirmed"].ToString() == "" || sqlDataReader["IsConfirmed"].ToString() == "0") ? "No" : "Yes";
                        gModel.ConfirmedDate = sqlDataReader["ConfirmedDate"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["ConfirmedDate"].ToString()).ToString("dd/MM/yyyy");
                        //gModel.Generated = sqlDataReader["GenerateDate"].ToString();
                        gModel.ExcuseType = sqlDataReader["AbsentTypeName"].ToString();
                        gModel.TotalSalary = Convert.ToDecimal(sqlDataReader["TotalDedSalary"]).ToString(AmountFormat);
                        gModel.TotalSalaryDecimal = Convert.ToDecimal(sqlDataReader["TotalDedSalary"]);
                        gModel.Status = (sqlDataReader["StatusID"].ToString() != "") ? common.AbsentStatus[Convert.ToInt32(sqlDataReader["StatusID"].ToString())] : "";
                        undeductableList.Add(gModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return undeductableList.AsEnumerable();
        }

        public IEnumerable<DeductionReportModel> GetLateDeductibleReport(int mode, string startDate, string toDate, bool? bConfirmed, bool? bGenrated,int UserId)
        {
            List<DeductionReportModel> undeductableList = new List<DeductionReportModel>();

            try
            {
                string AmountFormat = "";
                AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetLateAndEarlyDeductiondetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (startDate != null)
                    sqlCommand.Parameters.AddWithValue("@aDttFromdate", GeneralDB.CommonDB.SetCulturedDate(startDate));
                if (toDate != null)
                    sqlCommand.Parameters.AddWithValue("@aDttTodate", GeneralDB.CommonDB.SetCulturedDate(toDate));

                if (bConfirmed != null)
                {
                    sqlCommand.Parameters.AddWithValue("@aBitIsConfirmed", bConfirmed);
                }
                if (bGenrated != null)
                {
                    sqlCommand.Parameters.AddWithValue("@IsDeducted", bGenrated);
                }
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionReportModel gModel;
                    while (sqlDataReader.Read())
                    {
                        gModel = new DeductionReportModel();
                        gModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        gModel.Id = sqlDataReader["EmployeeAlternativeID"].ToString();
                        gModel.Name = sqlDataReader["EmployeeName"].ToString();
                        gModel.TotalSalary = Convert.ToDecimal(sqlDataReader["TotalSalary"].ToString() == "" ? "0" : sqlDataReader["TotalSalary"].ToString()).ToString(AmountFormat);
                        gModel.TotalSalaryDecimal= Convert.ToDecimal(sqlDataReader["TotalSalary"].ToString() == "" ? "0" : sqlDataReader["TotalSalary"].ToString());
                        gModel.LateInMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString() == "" ? "0" : sqlDataReader["LateMinutes"].ToString());
                        gModel.LateDedcutionAmount = Convert.ToDecimal(sqlDataReader["LateDeducatableAmount"].ToString() == "" ? "0" : sqlDataReader["LateDeducatableAmount"].ToString());
                        gModel.AttendanceDate = sqlDataReader["AttendanceDate"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["AttendanceDate"]).ToString("dd/MM/yyyy");
                        gModel.ConfirmedDateLateIn = sqlDataReader["ConfirmedDateLateIn"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["ConfirmedDateLateIn"]).ToString("dd/MM/yyyy");
                        gModel.GeneratedDateLateIn = sqlDataReader["GenerateDateLateIn"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["GenerateDateLateIn"]).ToString("dd/MM/yyyy");
                        undeductableList.Add(gModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return undeductableList.AsEnumerable();
        }

        public IEnumerable<DeductionReportModel> GetEarlyDeductibleReport(int mode, string startDate, string toDate, bool? bConfirmed, bool? bGenrated,int UserId)
        {
            List<DeductionReportModel> undeductableList = new List<DeductionReportModel>();

            try
            {
                string AmountFormat = "";
                AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetLateAndEarlyDeductiondetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (startDate != null)
                    sqlCommand.Parameters.AddWithValue("@aDttFromdate", GeneralDB.CommonDB.SetCulturedDate(startDate));
                if (toDate != null)
                    sqlCommand.Parameters.AddWithValue("@aDttTodate", GeneralDB.CommonDB.SetCulturedDate(toDate));

                if (bConfirmed != null)
                {
                    sqlCommand.Parameters.AddWithValue("@aBitIsConfirmed", bConfirmed);
                }
                if (bGenrated != null)
                {
                    sqlCommand.Parameters.AddWithValue("@IsDeducted", bGenrated);
                }
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionReportModel gModel;
                    while (sqlDataReader.Read())
                    {
                        gModel = new DeductionReportModel();
                        gModel.Id = sqlDataReader["EmployeeAlternativeID"].ToString();
                        gModel.Name = sqlDataReader["EmployeeName"].ToString();
                        gModel.TotalSalary = Convert.ToDecimal(sqlDataReader["TotalSalary"].ToString() == "" ? "0" : sqlDataReader["TotalSalary"].ToString()).ToString(AmountFormat);
                        gModel.EarlyOutMinues = Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString() == "" ? "0" : sqlDataReader["EarlyMinutes"].ToString());
                        gModel.EarlyDedcutionAmount = Convert.ToDecimal(sqlDataReader["EarlyDeducatableAmount"].ToString() == "" ? "0" : sqlDataReader["EarlyDeducatableAmount"].ToString());
                        gModel.AttendanceDate = sqlDataReader["AttendanceDate"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["AttendanceDate"]).ToString("dd/MM/yyyy");
                        gModel.ConfirmedDateEarlyOut = sqlDataReader["ConfirmedDateEarlyOut"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["ConfirmedDateEarlyOut"]).ToString("dd/MM/yyyy");
                        gModel.GeneratedDateEarlyOut = sqlDataReader["GenerateDateEarlyOut"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["GenerateDateEarlyOut"]).ToString("dd/MM/yyyy");
                        undeductableList.Add(gModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return undeductableList.AsEnumerable();
        }

        public IEnumerable<DeductionReportModel> GetAbsentUndeductableReport(string Startdate, string EndDate, int userId)
        {
            List<DeductionReportModel> undeductableList = new List<DeductionReportModel>();
            try
            {
                DataAccess.GeneralDB.CommonDB common = new DataAccess.GeneralDB.CommonDB();
                string Amountformat = "";
                Amountformat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetAbsentsForCurrentMonthExcused", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                DateTime FromDate = DateTime.ParseExact(Startdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime ToDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@aFromDate", FromDate);
                sqlCommand.Parameters.AddWithValue("@aToDate", ToDate);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionReportModel gModel;
                    while (sqlDataReader.Read())
                    {
                        gModel = new DeductionReportModel();
                        gModel.Id = sqlDataReader["EmployeeAlternativeId"].ToString();
                        gModel.Name = sqlDataReader["EmployeeName"].ToString();
                        // gModel.TotalSalary = Convert.ToDecimal(sqlDataReader["TotalDedSalary"].ToString() == "" ? "0" : sqlDataReader["TotalDedSalary"].ToString()).ToString(Amountformat);
                        gModel.Days = sqlDataReader["Days"].ToString();
                        // gModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"].ToString() == "" ? "0" : sqlDataReader["Amount"].ToString()).ToString(Amountformat);
                        gModel.FormDate = Convert.ToDateTime(sqlDataReader["FromDate"].ToString()).ToString("dd/MM/yyyy ddd");
                        gModel.ToDate = Convert.ToDateTime(sqlDataReader["ToDate"].ToString()).ToString("dd/MM/yyyy ddd");
                        gModel.ExcuseType = sqlDataReader["AbsentTypeName"].ToString();
                        gModel.Status = (sqlDataReader["StatusID"].ToString() != null ? common.AbsentStatus[Convert.ToInt32(sqlDataReader["StatusID"].ToString())] : "");
                        undeductableList.Add(gModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return undeductableList.AsEnumerable();
        }

        public IEnumerable<DeductionReportModel> GetDeductionReport(int deductionType, string fromdate, string todate,int UserId)
        {
            List<DeductionReportModel> reportList = new List<DeductionReportModel>();
            try
            {
                string Amountformat = "";
                Amountformat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPayDeductionForReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@deductiontype", deductionType);
                sqlCommand.Parameters.AddWithValue("@fromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromdate));
                sqlCommand.Parameters.AddWithValue("@toDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(todate));
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionReportModel iReportModel;
                    while (sqlDataReader.Read())
                    {
                        iReportModel = new DeductionReportModel();
                        iReportModel.Id = sqlDataReader["EmployeeId"].ToString();
                        iReportModel.Name = sqlDataReader["EmployeeName"].ToString();
                        iReportModel.cycleOfsalary = sqlDataReader["PayCycleName"].ToString();
                        iReportModel.date = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EffectiveDate"].ToString());
                        iReportModel.Amount = sqlDataReader["Amount"].ToString();
                        iReportModel.reason = sqlDataReader["Comments"].ToString();
                        iReportModel.deductionType = sqlDataReader["DeductionTypeName_1"].ToString();
                        iReportModel.academicOrReferance = (sqlDataReader["RefNumber"].ToString() != "" ? sqlDataReader["RefNumber"].ToString() : sqlDataReader["AcademicYear"].ToString());
                        iReportModel.installment = Convert.ToInt32(sqlDataReader["Installment"].ToString());
                        reportList.Add(iReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return reportList.AsEnumerable();
        }

        #endregion

        #region Addition Report

        public IEnumerable<AdditionReportModel> GetAdditionReport(int additionType, string fromdate, string todate,int UserId)
        {
            List<AdditionReportModel> reportList = new List<AdditionReportModel>();
            try
            {
                string Amountformat = "";
                Amountformat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPayAddtionForReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@additiontype", additionType);
                sqlCommand.Parameters.AddWithValue("@fromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromdate));
                sqlCommand.Parameters.AddWithValue("@toDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(todate));
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AdditionReportModel iReportModel;
                    while (sqlDataReader.Read())
                    {
                        iReportModel = new AdditionReportModel();
                        iReportModel.employeeId = sqlDataReader["EmployeeId"].ToString();
                        iReportModel.employeeName = sqlDataReader["EmployeeName"].ToString();
                        iReportModel.cycleOfsalary = sqlDataReader["PayCycleName"].ToString();
                        iReportModel.date = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AdditionDate"].ToString());
                        iReportModel.amount = Convert.ToDecimal(sqlDataReader["Amount"].ToString());
                        iReportModel.reason = sqlDataReader["Description"].ToString();
                        iReportModel.additionType = sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        iReportModel.academicOrReferance = sqlDataReader["AcademicYear"].ToString();
                        reportList.Add(iReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return reportList.AsEnumerable();
        }

        #endregion

        public List<GratuityReportModel> GetGratuityListReport(string empIds, int? deptId)
        {
            List<GratuityReportModel> GratuityReportList = new List<GratuityReportModel>();
            try
            {
                string AmountFormat = "";
                AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_USPEmployeeGratuityList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (empIds == "")
                    sqlCommand.Parameters.AddWithValue("@EmployeeeId", DBNull.Value);
                else
                    sqlCommand.Parameters.AddWithValue("@EmployeeeId", empIds);

                if (deptId == null)
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", DBNull.Value);
                else
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", deptId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                if (sqlDataReader.HasRows)
                {
                    int i = 1;
                    GratuityReportModel gratuityModel;
                    while (sqlDataReader.Read())
                    {
                        gratuityModel = new GratuityReportModel();
                        gratuityModel.EmpID = sqlDataReader["EmployeeId"].ToString();
                        gratuityModel.EmployeeAlternativeId = sqlDataReader["EmployeeAlternativeID"].ToString();
                        gratuityModel.EmpName = sqlDataReader["EmployeeName"].ToString();
                        gratuityModel.Position = sqlDataReader["Position"].ToString();
                        if (!string.IsNullOrEmpty(sqlDataReader["HireDate"].ToString()))
                        {
                            gratuityModel.HireDate = Convert.ToDateTime(sqlDataReader["HireDate"].ToString()).ToString("dd MMM yyyy ");
                        }
                        else
                        {
                            gratuityModel.HireDate = sqlDataReader["HireDate"].ToString();
                        }
                        if (!string.IsNullOrEmpty(sqlDataReader["LastDate"].ToString()))
                        {
                            gratuityModel.GratuityDate = Convert.ToDateTime(sqlDataReader["LastDate"].ToString()).ToString("dd MMM yyyy ");
                        }
                        else
                        {
                            gratuityModel.GratuityDate = sqlDataReader["LastDate"].ToString();
                        }
                        gratuityModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"] == DBNull.Value ? 0 : sqlDataReader["Amount"]).ToString(AmountFormat);
                        gratuityModel.AmountPerday = Convert.ToDecimal(sqlDataReader["AmountPerday"] == DBNull.Value || sqlDataReader["AmountPerday"] == "" ? 0 : sqlDataReader["AmountPerday"]).ToString(AmountFormat);
                        gratuityModel.ServicePeriod = sqlDataReader["ServicePeriod"].ToString();
                        gratuityModel.ContractTerms = sqlDataReader["employeecontractterms"].ToString();
                        gratuityModel.EOS = Convert.ToDecimal(sqlDataReader["EOS"].ToString() == "" || sqlDataReader["EOS"] == DBNull.Value ? "0" : sqlDataReader["EOS"].ToString()).ToString(AmountFormat);
                        gratuityModel.EOSType = sqlDataReader["EOSType"].ToString();
                        gratuityModel.ServicePeriodDays = sqlDataReader["ServicePeriodDays"].ToString();
                        gratuityModel.DepartmentName = sqlDataReader["DepartmentName"].ToString();
                        gratuityModel.DeptID = sqlDataReader["DepartmentID"].ToString();
                        gratuityModel.Grade = sqlDataReader["SalaryGradeName"].ToString();
                        gratuityModel.Nationality = sqlDataReader["Nationality"].ToString();
                        gratuityModel.ESAirfare = (sqlDataReader["Airfare"] == DBNull.Value ? 0 : Convert.ToDecimal(sqlDataReader["Airfare"])).ToString(AmountFormat);
                        gratuityModel.ServicePeriodDays = sqlDataReader["ServicePeriodDays"].ToString();
                        gratuityModel.SrNo = i;
                        i++;
                        GratuityReportList.Add(gratuityModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return GratuityReportList;
        }

        public DataTable GetGratuityListData(string empIds, int? deptId)
        {
            DataTable dtreport = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_USPEmployeeGratuityList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                if (empIds == "")
                    sqlCommand.Parameters.AddWithValue("@EmployeeeId", DBNull.Value);
                else
                    sqlCommand.Parameters.AddWithValue("@EmployeeeId", empIds);

                if (deptId == null)
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", DBNull.Value);
                else
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", deptId);

                dtreport.Load(sqlCommand.ExecuteReader());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            string[] columnNames = dtreport.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            if (columnNames.Any(x => x.Contains("EmployeeID")))
                dtreport.Columns.Remove("EmployeeID");

            if (columnNames.Any(x => x.Contains("DepartmentName")))
                dtreport.Columns.Remove("DepartmentName");

            if (columnNames.Any(x => x.Contains("DepartmentID")))
                dtreport.Columns.Remove("DepartmentID");

            if (columnNames.Any(x => x.Contains("SalaryGradeName")))
                dtreport.Columns.Remove("SalaryGradeName");

            if (columnNames.Any(x => x.Contains("Nationality")))
                dtreport.Columns.Remove("Nationality");

            if (columnNames.Any(x => x.Contains("Airfare")))
                dtreport.Columns.Remove("Airfare");

            dtreport.Columns["ServicePeriod"].SetOrdinal(5);
            dtreport.Columns["ServicePeriodDays"].SetOrdinal(6);
            dtreport.Columns["Amount"].SetOrdinal(7);
            dtreport.Columns["AmountPerday"].SetOrdinal(8);
            dtreport.Columns["EOS"].SetOrdinal(9);
            dtreport.Columns["employeecontractterms"].SetOrdinal(10);
            dtreport.Columns["EOSType"].SetOrdinal(11);
            dtreport.Columns["EmployeeAlternativeID"].ColumnName = "Empoloyee ID";
            dtreport.Columns["EmployeeName"].ColumnName = "Empoloyee Name";
            dtreport.Columns["HireDate"].ColumnName = "Hire Date";
            dtreport.Columns["LastDate"].ColumnName = "Last Gratuity Date";
            dtreport.Columns["AmountPerday"].ColumnName = "Amount Per day";
            dtreport.Columns["ServicePeriod"].ColumnName = "Service Period Year";
            dtreport.Columns["ServicePeriodDays"].ColumnName = "Service Period Days";
            dtreport.Columns["employeecontractterms"].ColumnName = "Contract Term";
            dtreport.Columns["EOSType"].ColumnName = "EOS Type";
            dtreport.Columns["Amount"].ColumnName = "Basic";
            return dtreport;

        }

        public GeneralAccSetting GetGeneralAccountSetting()
        {
            GeneralAccSetting setting = new GeneralAccSetting();
            List<GratuityReportModel> GratuityReportList = new List<GratuityReportModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Select * FROM dbo.Acc_GeneralSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    int i = 1;

                    while (sqlDataReader.Read())
                    {

                        setting.CurrencyCode = sqlDataReader["CurrencyCode"].ToString();
                        setting.CurrencyName = sqlDataReader["CurrencyName"].ToString();
                        setting.FilsName = sqlDataReader["FilsName"].ToString();


                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return setting;
        }

        public List<FingerPrintMachineDataReportModel> GetFingerPrintMachineData(int UserId)
        {
            List<FingerPrintMachineDataReportModel> lstFingerprintData = new List<FingerPrintMachineDataReportModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetFingerPrintMachineData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    int i = 1;
                    FingerPrintMachineDataReportModel objFingerPrintMachineData;
                    while (sqlDataReader.Read())
                    {
                        objFingerPrintMachineData = new FingerPrintMachineDataReportModel();
                        objFingerPrintMachineData.EmployeeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        objFingerPrintMachineData.FingerPrintMachineID = sqlDataReader["FingerPrintMachineID"].ToString();
                        objFingerPrintMachineData.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        objFingerPrintMachineData.Department = sqlDataReader["Department"].ToString();
                        objFingerPrintMachineData.ShiftName = sqlDataReader["ShiftName"].ToString();
                        lstFingerprintData.Add(objFingerPrintMachineData);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstFingerprintData;
        }
           

        #region LetterReport

        public List<LetterTemplates> GetLettersTemplate()
        {
            List<LetterTemplates> letterTemplateList = new List<LetterTemplates>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select PayReportBuilderLettersID,name from HR_PayReportBuilderLetters", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    int i = 1;
                    LetterTemplates templateModel;
                    while (sqlDataReader.Read())
                    {
                        templateModel = new LetterTemplates();
                        templateModel.LettersId = Convert.ToInt32(sqlDataReader["PayReportBuilderLettersID"]);
                        templateModel.Lettername = sqlDataReader["name"].ToString();
                        letterTemplateList.Add(templateModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return letterTemplateList;
        }

        public bool CheckTemplateAvailablity(int LetterId, string LetterName)
        {
            int count = 0;
            bool isAvailable = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select COUNT(PayReportBuilderLettersID) AS COUNT from HR_PayReportBuilderLetters WHERE LTRIM(RTRIM(LOWER(name))) ='" + LetterName + "' AND PayReportBuilderLettersID !=" + LetterId, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        count = Convert.ToInt32(sqlDataReader["COUNT"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (count > 0)
            {
                isAvailable = true;
            }
            return isAvailable;
        }

        public bool DeleteLetterTemplate(int templateId)
        {
            int count = 0;
            bool isdeleted = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("DELETE  from HR_PayReportBuilderLetters WHERE PayReportBuilderLettersID= " + templateId + "", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.RecordsAffected > 0)
                {
                    isdeleted = true;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return isdeleted;
        }

        public OperationDetails UpdateLetterTemplate(int LetterId, string LetterTemplateName, string Fields, int TranType, string HtmlContent)
        {
            OperationDetails op = new OperationDetails();
            DataTable dt = new DataTable();
            List<GratuityReportModel> GratuityReportList = new List<GratuityReportModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("HR_uspPayReportBuilderLettersCUD", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@aNvrname", LetterTemplateName);
                da.SelectCommand.Parameters.AddWithValue("@aTntTranMode", TranType);
                da.SelectCommand.Parameters.AddWithValue("@aIntPayReportBuilderLettersID", LetterId);
                da.SelectCommand.Parameters.AddWithValue("@HtmlContent", HtmlContent);
                da.SelectCommand.Parameters.AddWithValue("@Feilds", Fields);
                SqlParameter Output = new SqlParameter("@output", SqlDbType.VarChar, 250);
                Output.Direction = ParameterDirection.Output;
                da.SelectCommand.Parameters.Add(Output);
                da.Fill(dt);
                //SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!string.IsNullOrEmpty(Output.ToString()))
                {
                    op.Success = true;
                }
                else
                {
                    op.Success = false;
                }


            }
            catch (Exception exception)
            {
                op.Success = false;
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }
        #endregion

        public List<AccommodationModel> GetAccommodationList(int? EeployeeID, string academicYearId,int UserId)
        {
            List<AccommodationModel> AccommodationList = new List<AccommodationModel>();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            dataHelper = new DataHelper();
            parameterList.Add(new SqlParameter("@EmployeeID", EeployeeID));
            parameterList.Add(new SqlParameter("@UserId", UserId));
            if (academicYearId != "")
                parameterList.Add(new SqlParameter("@AccomodationHistoryYear", academicYearId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_uspGetAccommodationHistory");
            AccommodationModel accommodationModel;
            foreach (DataRow dr in dt.Rows)
            {
                accommodationModel = new AccommodationModel();
                accommodationModel.Category = dr["CategoryName_1"].ToString();
                accommodationModel.AccommodationYear = dr["PayEmployeeAccommodationHistoryYear"].ToString();
                accommodationModel.EmployeeAlternativeId = dr["EmployeeAlternativeID"].ToString();
                accommodationModel.EmployeeName = dr["EmployeeName"].ToString();
                accommodationModel.AccommodationType = dr["HRAccommodationTypeName_1"].ToString();
                accommodationModel.City = dr["CityName_1"].ToString();
                accommodationModel.ApartmentNo = dr["Appartment"].ToString();
                accommodationModel.Amount = Convert.ToDecimal(dr["Amount"].ToString());
                accommodationModel.Furniture = Convert.ToDecimal(dr["FurnishedAmount"].ToString());
                accommodationModel.Water = Convert.ToDecimal(dr["WaterAmount"].ToString());
                accommodationModel.Electricity = Convert.ToDecimal(dr["ElectricityAmount"].ToString());
                accommodationModel.AccommodationAmount = Convert.ToDecimal(dr["AccommodationAmount"].ToString());
                AccommodationList.Add(accommodationModel);
            }
            return AccommodationList;
        }


        public List<PayrollVerificationReport> GetMissingBankInfoEmployeeList()
        {
            List<PayrollVerificationReport> employeeVarificationList = null;
            try
            {
                employeeVarificationList = new List<PayrollVerificationReport>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetMissingBankInfoEmployeeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PayrollVerificationReport employeeVarificationModel;
                    while (sqlDataReader.Read())
                    {
                        employeeVarificationModel = new PayrollVerificationReport();
                        employeeVarificationModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlternativeID"].ToString());
                        employeeVarificationModel.FullName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeVarificationModel.PositionName = Convert.ToString(sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"]);
                        employeeVarificationModel.IsActive = Convert.ToString(sqlDataReader["isPayActive"] == DBNull.Value ? "No" : (Convert.ToBoolean(sqlDataReader["isPayActive"]) ? "Yes" : "No"));
                        employeeVarificationList.Add(employeeVarificationModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeVarificationList;
        }

        public List<PayrollVerificationReport> GetMissingSalaryInfoEmployeeList()
        {
            List<PayrollVerificationReport> employeeVarificationList = null;
            try
            {
                employeeVarificationList = new List<PayrollVerificationReport>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetMissingSalaryInfoEmployeeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PayrollVerificationReport employeeVarificationModel;
                    while (sqlDataReader.Read())
                    {
                        employeeVarificationModel = new PayrollVerificationReport();
                        employeeVarificationModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlternativeID"].ToString());
                        employeeVarificationModel.FullName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeVarificationModel.PositionName = Convert.ToString(sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"]);
                        employeeVarificationModel.IsActive = Convert.ToString(sqlDataReader["isPayActive"] == DBNull.Value ? "No" : (Convert.ToBoolean(sqlDataReader["isPayActive"]) ? "Yes" : "No"));
                        employeeVarificationList.Add(employeeVarificationModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeVarificationList;
        }


        public IEnumerable<MinistryModel> GetMinistryReportVersion1(int UserId)
        {
            List<MinistryModel> listMinistry = new List<MinistryModel>();
            MinistryModel ministryModel;
            dataHelper = new DataHelper();
            List<SqlParameter> listSqlParameter = new List<SqlParameter>();
            listSqlParameter.Add(new SqlParameter("@UserId", UserId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(listSqlParameter, "Hr_Stp_GetMinistryReportVersion1");
            foreach (DataRow dr in dt.Rows)
            {
                ministryModel = new MinistryModel();
                ministryModel.EmployeeName = dr["EmployeeName"].ToString();
                ministryModel.Nationality = dr["Nationality"].ToString();
                ministryModel.Position = dr["Position"].ToString();
                ministryModel.HireDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["HireDate"].ToString());
                ministryModel.LastQualification = dr["LastQualification"].ToString();
                ministryModel.Specialization = dr["Specialization"].ToString();
                ministryModel.DocumentNo = dr["DocumentNo"].ToString();
                ministryModel.MobileNumber = dr["MobileNumber"].ToString();
                listMinistry.Add(ministryModel);
            }
            return listMinistry.AsEnumerable();
        }


        public List<NationalitWiseData> GetNationalitWiseGenderCountDetails(string deptids, string nationalityIds)
        {
            List<NationalitWiseData> employeeCountList = null;
            try
            {
                employeeCountList = new List<NationalitWiseData>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetGenderWiseEmployeeCountASPerDepartmentAndNationality", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@NationalityIds", nationalityIds);
                sqlCommand.Parameters.AddWithValue("@DepartmentIds", deptids);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    NationalitWiseData employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new NationalitWiseData();
                        employeeModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"] == DBNull.Value ? "0" : sqlDataReader["DepartmentID"]);
                        employeeModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_1"].ToString());
                        employeeModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_2"].ToString());
                        employeeModel.NationalityID = Convert.ToInt32(sqlDataReader["DefaultNationalityID"] == DBNull.Value ? "0" : sqlDataReader["DefaultNationalityID"]);
                        employeeModel.NationalityName = Convert.ToString(sqlDataReader["NationalityName_1"] == DBNull.Value ? "" : sqlDataReader["NationalityName_1"]);
                        employeeModel.NationalityName_2 = Convert.ToString(sqlDataReader["NationalityName_2"] == DBNull.Value ? "" : sqlDataReader["NationalityName_2"]);
                        employeeModel.MaleCount = Convert.ToInt32(sqlDataReader["MaleCount"] == DBNull.Value ? "0" : sqlDataReader["MaleCount"]);
                        employeeModel.NonNationalityMaleCount = Convert.ToInt32(sqlDataReader["NonNationalityMaleCount"] == DBNull.Value ? "0" : sqlDataReader["NonNationalityMaleCount"]);
                        employeeModel.FemaleCount = Convert.ToInt32(sqlDataReader["FemaleCount"] == DBNull.Value ? "0" : sqlDataReader["FemaleCount"]);
                        employeeModel.NonNationalityFemaleCount = Convert.ToInt32(sqlDataReader["NonNationalityFemaleCount"] == DBNull.Value ? "0" : sqlDataReader["NonNationalityFemaleCount"]);
                        employeeCountList.Add(employeeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeCountList;
        }

        public List<NationalitWiseData> GetdepartmentWiseGenderCountDetails(int UserId)
        {
            List<NationalitWiseData> employeeCountList = null;
            try
            {
                employeeCountList = new List<NationalitWiseData>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetGenderCountAsPerDepartment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    NationalitWiseData employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new NationalitWiseData();
                        employeeModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"] == DBNull.Value ? "0" : sqlDataReader["DepartmentID"]);
                        employeeModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_1"].ToString());
                        employeeModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_2"].ToString());
                        employeeModel.MaleCount = Convert.ToInt32(sqlDataReader["MaleCount"] == DBNull.Value ? "0" : sqlDataReader["MaleCount"]);
                        employeeModel.FemaleCount = Convert.ToInt32(sqlDataReader["FemaleCount"] == DBNull.Value ? "0" : sqlDataReader["FemaleCount"]);
                        employeeModel.TotalCount = Convert.ToInt32(sqlDataReader["TotalCount"] == DBNull.Value ? "0" : sqlDataReader["TotalCount"]);
                        employeeCountList.Add(employeeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeCountList;
        }

        public DataTable GetLeaveBalanceData(string EmployeeIds, string departmentid, string LeaveTypes, bool UsedLeaveEmployees, DateTime todate)
        {
            DataTable dtreport = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_GetLeaveBalanceInfo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                if (EmployeeIds == "" || EmployeeIds == null)
                {
                    sqlCommand.Parameters.AddWithValue("@EmployeeIds", DBNull.Value);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmployeeIds);
                }
                if (departmentid == "" || departmentid == null)
                {
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", DBNull.Value);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentid);
                }
                if (LeaveTypes == "" || LeaveTypes == null)
                {
                    sqlCommand.Parameters.AddWithValue("@LeaveTypes", DBNull.Value);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@LeaveTypes", LeaveTypes);
                }
                sqlCommand.Parameters.AddWithValue("@UsedLeaveEmployees", UsedLeaveEmployees);
                sqlCommand.Parameters.AddWithValue("@FutureDate", todate);
                dtreport.Load(sqlCommand.ExecuteReader());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dtreport;

        }

        public IEnumerable<StaffQualificationReportModel> GetQualificationReportDetails(int employeeId, int nationalityId, int jobCategory,int UserId)
        {
            List<StaffQualificationReportModel> staffQualificationList = new List<StaffQualificationReportModel>();
            StaffQualificationReportModel staffQualificationModel;
            dataHelper = new DataHelper();
            List<SqlParameter> listSqlParameter = new List<SqlParameter>();
            listSqlParameter.Add(new SqlParameter("@NationalityId", nationalityId));
            listSqlParameter.Add(new SqlParameter("@EmployeeId", employeeId));
            listSqlParameter.Add(new SqlParameter("@jobCategory", jobCategory));
            listSqlParameter.Add(new SqlParameter("@UserId", UserId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(listSqlParameter, "Hr_Stp_GetEmployeeQualificationDataForReport");
            foreach (DataRow dr in dt.Rows)
            {
                staffQualificationModel = new StaffQualificationReportModel();
                staffQualificationModel.EmployeeName = dr["EmployeeName"].ToString();
                staffQualificationModel.EmployeeId = Convert.ToInt32(dr["EmployeeId"].ToString());
                staffQualificationModel.CountryName = dr["CountryName_1"].ToString();
                staffQualificationModel.Date = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["Date"].ToString());
                staffQualificationModel.QualificationName = dr["Level"].ToString();
                staffQualificationModel.SubjectName = dr["Subject"].ToString();
                staffQualificationModel.CategoryName = dr["EmployeeJobCategoryName_1"].ToString();
                staffQualificationModel.NationalityName = dr["NationalityName_1"].ToString();
                staffQualificationList.Add(staffQualificationModel);
            }
            return staffQualificationList.OrderBy(x => x.EmployeeName).AsEnumerable();
        }

        public IEnumerable<EmployeeProfessionalDevelopmentReportModel> GetProfessionalDevelopmentReportDetails(int trainingId, int employeeId, int countryId, string fromDate, string toDate,int UserId)
        {            
            string[] formats = new string[] {"dd-MM-yyyy", "d/MM/yyyy"};
            List<EmployeeProfessionalDevelopmentReportModel> trainingList = new List<EmployeeProfessionalDevelopmentReportModel>();
            EmployeeProfessionalDevelopmentReportModel trainingModel;
            dataHelper = new DataHelper();
            List<SqlParameter> listSqlParameter = new List<SqlParameter>();
            listSqlParameter.Add(new SqlParameter("@TrainingId", trainingId));
            listSqlParameter.Add(new SqlParameter("@EmployeeId", employeeId));
            listSqlParameter.Add(new SqlParameter("@CountryId", countryId));
            listSqlParameter.Add(new SqlParameter("@UserId", UserId));
            if (fromDate != "")
            {                
                listSqlParameter.Add(new SqlParameter("@FromDate", DateTime.ParseExact(fromDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)));
                listSqlParameter.Add(new SqlParameter("@ToDate", DateTime.ParseExact(toDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)));
            }
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(listSqlParameter, "Hr_Stp_GetEmployeeTrainingDataForReport");
            foreach (DataRow dr in dt.Rows)
            {
                trainingModel = new EmployeeProfessionalDevelopmentReportModel();
                trainingModel.EmployeeName = dr["EmployeeName"].ToString();
                trainingModel.EmployeeId = Convert.ToInt32(dr["EmployeeId"].ToString());             
                trainingModel.StartDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["StartDate"].ToString());
                trainingModel.EndDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["EndDate"].ToString());
                trainingModel.TrainingName = dr["TrainingName"].ToString();
                trainingModel.CountryName = dr["CountryName_1"].ToString();
                trainingModel.TotalHour = dr["TotalHour"].ToString();
                trainingList.Add(trainingModel);
            }
            return trainingList.OrderBy(x => x.EmployeeName).AsEnumerable();
        }

        public EmployeeFinalSettlementReport GetFinalSettlementReport(string EmployeeIDs)
        {
            EmployeeFinalSettlementReport objEmployeeFinalSettlement = new EmployeeFinalSettlementReport();
            string AmountFormat = "";
            AmountFormat = new PayrollDB().GetAmountFormat();
            dataHelper = new DataHelper();
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            sqlCommand = new SqlCommand("Hr_Stp_GetFinalSettlementDetails ", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // Passing values
            sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmployeeIDs);

            DataSet objDS = new DataSet();
            SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
            objDA.Fill(objDS);
            sqlConnection.Close();
            objEmployeeFinalSettlement.EmployeeDetailsList = new List<EmployeeDetailsReportModel>();
            DataTable dt = objDS.Tables[0];
            int EmpDetailsCount = objDS.Tables[0].Rows.Count;
            EmployeeDetailsReportModel EmployeeDet;
            foreach (DataRow dr in dt.Rows)
            {
                EmployeeDet = new EmployeeDetailsReportModel();
                EmployeeDet.EmployeeName = dr["EmployeeName"].ToString();
                EmployeeDet.EmployeeId = Convert.ToInt32(dr["EmployeeId"].ToString());
                EmployeeDet.Nationality = dr["NationalityName_1"].ToString();
                EmployeeDet.JoiningDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["HireDate"].ToString());
                EmployeeDet.Designation = dr["PositionTitle"].ToString();
                EmployeeDet.Department = dr["departmentName_1"].ToString();
                EmployeeDet.Status = dr["EmploymentModeName1"].ToString().Trim();
                EmployeeDet.ContractType = dr["EmployeeContractTerms"].ToString();
                EmployeeDet.LengthofService = dr["Serviceperiod"].ToString();
                EmployeeDet.LastWorkingDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["LastDate"].ToString());
                EmployeeDet.ReasonforExit = dr["EOSType"].ToString();
                objEmployeeFinalSettlement.EmployeeDetailsList.Add(EmployeeDet);
            }
            objEmployeeFinalSettlement.EmployeeSalaryList = new List<EmployeeSalaryDetailsReportModel>();
            EmployeeSalaryDetailsReportModel EmployeeSal;
            dt = objDS.Tables[1];
            foreach (DataRow dr in dt.Rows)
            {
                EmployeeSal = new EmployeeSalaryDetailsReportModel();
                EmployeeSal.EmployeeId = Convert.ToInt32(dr["EmployeeId"].ToString());
                EmployeeSal.AllowanceName = dr["ItemDescr"].ToString().Trim();
                if (!string.IsNullOrEmpty(EmployeeSal.AllowanceName))
                {
                    EmployeeSal.Amount = string.IsNullOrEmpty(dr["amount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["amount"].ToString()).ToString(AmountFormat);
                }
                EmployeeSal.EarnedDays = dr["LastWorkingDays"].ToString();
                EmployeeSal.isTax = Convert.ToBoolean(dr["isTax"].ToString());
                objEmployeeFinalSettlement.EmployeeSalaryList.Add(EmployeeSal);
            }
            objEmployeeFinalSettlement.EmployeeGratList = new List<EmployeeGratuityDetailsReportModel>();
            EmployeeGratuityDetailsReportModel EmpGratuity;
            dt = objDS.Tables[2];
            foreach (DataRow dr in dt.Rows)
            {
                EmpGratuity = new EmployeeGratuityDetailsReportModel();
                EmpGratuity.EmployeeId = Convert.ToInt32(dr["EmployeeId"].ToString());
                EmpGratuity.GratuityDays = dr["ServicePeriod"].ToString();
                EmpGratuity.AdvanceOnGratuity = string.IsNullOrEmpty(dr["GratuityAlreadyPaidAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["GratuityAlreadyPaidAmount"].ToString()).ToString(AmountFormat);
                EmpGratuity.GratuityPriorToNE = string.IsNullOrEmpty(dr["GratuityBeforeNEAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["GratuityBeforeNEAmount"].ToString()).ToString(AmountFormat);
                EmpGratuity.TotalGratuity = string.IsNullOrEmpty(dr["TotalGratuity"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["TotalGratuity"].ToString()).ToString(AmountFormat);
                EmpGratuity.GrossGratuity = string.IsNullOrEmpty(dr["GrossGratuity"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["GrossGratuity"].ToString()).ToString(AmountFormat);
                objEmployeeFinalSettlement.EmployeeGratList.Add(EmpGratuity);
            }
            if (objEmployeeFinalSettlement.EmployeeGratList.Count < EmpDetailsCount)
            {
                int GratCount = objEmployeeFinalSettlement.EmployeeGratList.Count;
                for (int i = GratCount; i < EmpDetailsCount; i++)
                {
                    EmpGratuity = new EmployeeGratuityDetailsReportModel();
                    EmpGratuity.EmployeeId = objEmployeeFinalSettlement.EmployeeDetailsList[i].EmployeeId;
                    EmpGratuity.AdvanceOnGratuity = AmountFormat;
                    EmpGratuity.GratuityPriorToNE = AmountFormat;
                    EmpGratuity.TotalGratuity = AmountFormat;
                    objEmployeeFinalSettlement.EmployeeGratList.Add(EmpGratuity);
                }
            }
            objEmployeeFinalSettlement.EmployeeLeaveList = new List<EmployeeGratuityLeaveDetailsModel>();
            EmployeeGratuityLeaveDetailsModel objLeaveDetails;
            dt = objDS.Tables[3];
            foreach (DataRow dr in dt.Rows)
            {
                objLeaveDetails = new EmployeeGratuityLeaveDetailsModel();
                objLeaveDetails.EmployeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                objLeaveDetails.RemainingLeaves = dr["RemainingLeaves"].ToString();
                objLeaveDetails.RemainingLeavesAmount = string.IsNullOrEmpty(dr["RemainingLeavesAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["RemainingLeavesAmount"].ToString()).ToString(AmountFormat);
                objEmployeeFinalSettlement.EmployeeLeaveList.Add(objLeaveDetails);
            }
            if (objEmployeeFinalSettlement.EmployeeLeaveList.Count < EmpDetailsCount)
            {
                int LeavesCount = objEmployeeFinalSettlement.EmployeeLeaveList.Count;
                for (int i = LeavesCount; i < EmpDetailsCount; i++)
                {
                    objLeaveDetails = new EmployeeGratuityLeaveDetailsModel();
                    objLeaveDetails.EmployeeId = objEmployeeFinalSettlement.EmployeeDetailsList[i].EmployeeId;
                    objLeaveDetails.RemainingLeavesAmount = AmountFormat;
                    objEmployeeFinalSettlement.EmployeeLeaveList.Add(objLeaveDetails);
                }
            }
            objEmployeeFinalSettlement.EmployeeOtherBenefitsList = new List<EmployeeGratuityOtherBenefitsModel>();
            EmployeeGratuityOtherBenefitsModel objOtherBenefits;
            dt = objDS.Tables[4];
            foreach (DataRow dr in dt.Rows)
            {
                objOtherBenefits = new EmployeeGratuityOtherBenefitsModel();
                objOtherBenefits.EmployeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                objOtherBenefits.Airfare = string.IsNullOrEmpty(dr["AirTicketAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["AirTicketAmount"].ToString()).ToString(AmountFormat);
                objOtherBenefits.Other = string.IsNullOrEmpty(dr["ESOthersAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["ESOthersAmount"].ToString()).ToString(AmountFormat);
                objOtherBenefits.CompensatoryLeaveBalance = string.IsNullOrEmpty(dr["CompensatoryLeaveAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["CompensatoryLeaveAmount"].ToString()).ToString(AmountFormat);
                objOtherBenefits.TotalOtherBenefit = (Convert.ToDecimal(objOtherBenefits.Airfare) + Convert.ToDecimal(objOtherBenefits.Other) + Convert.ToDecimal(objOtherBenefits.CompensatoryLeaveBalance)).ToString(AmountFormat);
                objEmployeeFinalSettlement.EmployeeOtherBenefitsList.Add(objOtherBenefits);
            }
            if (objEmployeeFinalSettlement.EmployeeOtherBenefitsList.Count < EmpDetailsCount)
            {
                int otherbBenefitsCount = objEmployeeFinalSettlement.EmployeeOtherBenefitsList.Count;
                for (int i = otherbBenefitsCount; i < EmpDetailsCount; i++)
                {
                    objOtherBenefits = new EmployeeGratuityOtherBenefitsModel();
                    objOtherBenefits.EmployeeId = objEmployeeFinalSettlement.EmployeeDetailsList[i].EmployeeId;
                    objOtherBenefits.Airfare = AmountFormat;
                    objOtherBenefits.Other = AmountFormat;
                    objOtherBenefits.CompensatoryLeaveBalance = AmountFormat;
                    objEmployeeFinalSettlement.EmployeeOtherBenefitsList.Add(objOtherBenefits);
                }
            }
            objEmployeeFinalSettlement.EmployeeOtherDeductionList = new List<EmployeeGratuityOtherDeductionModel>();
            EmployeeGratuityOtherDeductionModel Otherdeduction = new EmployeeGratuityOtherDeductionModel();
            dt = objDS.Tables[5];
            foreach (DataRow dr in dt.Rows)
            {
                Otherdeduction = new EmployeeGratuityOtherDeductionModel();
                Otherdeduction.EmployeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                Otherdeduction.OtherDeduction = string.IsNullOrEmpty(dr["ESOthersDeductionAmount"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["ESOthersDeductionAmount"].ToString()).ToString(AmountFormat);
                Otherdeduction.AdvanceDeduction = string.IsNullOrEmpty(dr["AdvanceDeduction"].ToString()) ? AmountFormat : Convert.ToDecimal(dr["AdvanceDeduction"].ToString()).ToString(AmountFormat);
                Otherdeduction.TotalDeduction = (Convert.ToDecimal(Otherdeduction.OtherDeduction) + Convert.ToDecimal(Otherdeduction.AdvanceDeduction)).ToString(AmountFormat);
                objEmployeeFinalSettlement.EmployeeOtherDeductionList.Add(Otherdeduction);
            }
            if (objEmployeeFinalSettlement.EmployeeOtherDeductionList.Count < EmpDetailsCount)
            {
                int deductioncount = objEmployeeFinalSettlement.EmployeeOtherDeductionList.Count;
                for (int i = deductioncount; i < EmpDetailsCount; i++)
                {
                    Otherdeduction = new EmployeeGratuityOtherDeductionModel();
                    Otherdeduction.EmployeeId = objEmployeeFinalSettlement.EmployeeDetailsList[i].EmployeeId;
                    Otherdeduction.OtherDeduction = AmountFormat;
                    objEmployeeFinalSettlement.EmployeeOtherDeductionList.Add(Otherdeduction);
                }

            }
            objEmployeeFinalSettlement.EmployeeFinalSettlementDue = new List<FinalSettlmentDue>();
            FinalSettlmentDue objFinalSettlmentDue;
            foreach (var item in objEmployeeFinalSettlement.EmployeeDetailsList)
            {
                objFinalSettlmentDue = new FinalSettlmentDue();
                objFinalSettlmentDue.EmployeeId = item.EmployeeId;
                objFinalSettlmentDue.TotalSalary = (Convert.ToDecimal(objEmployeeFinalSettlement.EmployeeSalaryList.Where(x => x.EmployeeId == item.EmployeeId).Where(x => x.isTax == false).Sum(m => Convert.ToDecimal(m.Amount)))
                                                   - Convert.ToDecimal(objEmployeeFinalSettlement.EmployeeSalaryList.Where(x => x.EmployeeId == item.EmployeeId).Where(x => x.isTax == true).Sum(m => Convert.ToDecimal(m.Amount)))).ToString(AmountFormat);

                if (objEmployeeFinalSettlement.EmployeeGratList.Where(x => x.EmployeeId == item.EmployeeId).Count() > 0)
                    objFinalSettlmentDue.TotalGratuity = objEmployeeFinalSettlement.EmployeeGratList.Where(x => x.EmployeeId == item.EmployeeId).FirstOrDefault().TotalGratuity;
                else
                    objFinalSettlmentDue.TotalGratuity = "0";

                if (objEmployeeFinalSettlement.EmployeeOtherBenefitsList.Where(x => x.EmployeeId == item.EmployeeId).Count() > 0)
                    objFinalSettlmentDue.TotalOtherBenefits = objEmployeeFinalSettlement.EmployeeOtherBenefitsList.Where(x => x.EmployeeId == item.EmployeeId).Single().TotalOtherBenefit;
                else
                    objFinalSettlmentDue.TotalOtherBenefits = "0";

                if (objEmployeeFinalSettlement.EmployeeLeaveList.Where(x => x.EmployeeId == item.EmployeeId).Count() > 0)
                    objFinalSettlmentDue.AnnualLeaveBalance = objEmployeeFinalSettlement.EmployeeLeaveList.Where(x => x.EmployeeId == item.EmployeeId).Single().RemainingLeavesAmount;
                else
                    objFinalSettlmentDue.AnnualLeaveBalance = "0";

                objFinalSettlmentDue.TotalDeduction = (Convert.ToDecimal(objEmployeeFinalSettlement.EmployeeOtherDeductionList.Where(x => x.EmployeeId == item.EmployeeId).Single().OtherDeduction) + Convert.ToDecimal(objEmployeeFinalSettlement.EmployeeOtherDeductionList.Where(x => x.EmployeeId == item.EmployeeId).Single().AdvanceDeduction)).ToString(AmountFormat);
                objFinalSettlmentDue.TotalFinalSettlement = ((Convert.ToDecimal(objFinalSettlmentDue.TotalSalary) + Convert.ToDecimal(objFinalSettlmentDue.TotalGratuity) + Convert.ToDecimal(objFinalSettlmentDue.AnnualLeaveBalance) + Convert.ToDecimal(objFinalSettlmentDue.TotalOtherBenefits)) - Convert.ToDecimal(objFinalSettlmentDue.TotalDeduction)).ToString(AmountFormat);
                objEmployeeFinalSettlement.EmployeeFinalSettlementDue.Add(objFinalSettlmentDue);
            }

            objEmployeeFinalSettlement.EmpBankDetails = new List<BankDetails>();
            BankDetails Bank = new BankDetails();
            dt = objDS.Tables[6];
            foreach (DataRow dr in dt.Rows)
            {
                Bank = new BankDetails();
                Bank.EmployeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                Bank.BankName = dr["BankName_1"].ToString();
                Bank.BranchName = dr["BranchName_1"].ToString();
                Bank.UniAccountNumber = dr["UniAccountNumber"].ToString();
                Bank.EmployeeName = dr["EmployeeName"].ToString();
                objEmployeeFinalSettlement.EmpBankDetails.Add(Bank);
            }
            if (objEmployeeFinalSettlement.EmpBankDetails.Count < EmpDetailsCount)
            {
                int bankcount = objEmployeeFinalSettlement.EmpBankDetails.Count;
                for (int i = bankcount; i < EmpDetailsCount; i++)
                {
                    Bank = new BankDetails();
                    Bank.EmployeeId = objEmployeeFinalSettlement.EmployeeDetailsList[i].EmployeeId;
                    Bank.EmployeeName = objEmployeeFinalSettlement.EmployeeDetailsList[i].EmployeeName;
                    Bank.BankName = "";
                    Bank.BranchName = "";
                    Bank.UniAccountNumber = "";
                    objEmployeeFinalSettlement.EmpBankDetails.Add(Bank);
                }

            }
            return objEmployeeFinalSettlement;
        }

        public IEnumerable<SalaryReport> GetSalaryReportData(string employeeIds, int? departmentId, int? payCategory)
        {
            List<SalaryReport> salaryReportList = new List<SalaryReport>();
            dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            if (departmentId != null)
                parameterList.Add(new SqlParameter("@DepartmentId", departmentId));
            if (payCategory != null)
                parameterList.Add(new SqlParameter("@PayCategotyId", payCategory));
            parameterList.Add(new SqlParameter("@EmployeeIds", employeeIds));
            DataTable DT = new DataTable();
            DT = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_GetSalaryReportData");
            SalaryReport sSalaryReport;
            foreach (DataRow dr in DT.Rows)
            {
                sSalaryReport = new SalaryReport();
                if (dr["SalaryAllowance"].ToString() == "Total")
                {
                    sSalaryReport.FromDate = "";
                    sSalaryReport.ToDate = "";
                    sSalaryReport.DepartmentName = "";
                    sSalaryReport.Position = "";

                }
                else
                {
                    sSalaryReport.FromDate = CommonDB.GetFormattedDate_DDMMYYYY(dr["StartDate"].ToString());
                    sSalaryReport.ToDate = CommonDB.GetFormattedDate_DDMMYYYY(dr["ToDate"].ToString());
                    sSalaryReport.DepartmentName = dr["Departmentname"].ToString();
                    sSalaryReport.Position = dr["PositionTitle"].ToString();
                }
                sSalaryReport.EmployeeId = dr["EmployeeID"].ToString();
                sSalaryReport.EmployeeName = dr["EmployeeName"].ToString();
                sSalaryReport.Amount = Convert.ToDecimal(dr["Amount"].ToString());
                sSalaryReport.SalaryAllowances = dr["SalaryAllowance"].ToString();
                salaryReportList.Add(sSalaryReport);
            }
            return salaryReportList;
        }

        public List<EmployeeLeaveRegister> GetEmployeeLeaveRegister(string startDate, string endDate)
        {
            List<EmployeeLeaveRegister> employeeLeaveRegisterList = new List<EmployeeLeaveRegister>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeLeaveRegister", sqlConnection);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                DateTime dStartDate = DateTime.ParseExact(startDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dToDate = DateTime.ParseExact(endDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@StartDate", dStartDate);
                sqlCommand.Parameters.AddWithValue("@ToDate", dToDate);

                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeLeaveRegister employeeLeaveRegister;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        employeeLeaveRegister = new EmployeeLeaveRegister();
                        employeeLeaveRegister.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        employeeLeaveRegister.AutoId = Convert.ToString(dr["AutoId"]);
                        employeeLeaveRegister.EmployeeAlternativeID = Convert.ToString(dr["EmployeeAlternativeID"]);
                        employeeLeaveRegister.ElLeaveFrom = dr["ElLeaveFrom"].ToString() != "" ? GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["ElLeaveFrom"].ToString()) : "";
                        employeeLeaveRegister.ElLeaveTo = dr["ElLeaveTo"].ToString() != "" ? GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["ElLeaveTo"].ToString()) : "";
                        employeeLeaveRegister.ALeaveFrom = dr["ALeaveFrom"].ToString() != "" ? GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["ALeaveFrom"].ToString()) : "";
                        employeeLeaveRegister.ALeaveTo = dr["ALeaveTo"].ToString() != "" ? GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["ALeaveTo"].ToString()) : "";
                        employeeLeaveRegister.ExpectedRejoin = dr["ExpectedRejoin"].ToString() != "" ? GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["ExpectedRejoin"].ToString()) : "";
                        employeeLeaveRegister.RejoinOn = dr["RejoinOn"].ToString() != "" ? GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["RejoinOn"].ToString()) : "";
                        employeeLeaveRegister.ALeaveDay = Convert.ToDecimal(dr["ALeaveDay"].ToString() != "" ? dr["ALeaveDay"].ToString() : "0");
                        employeeLeaveRegister.TLeaveDay = Convert.ToDecimal(dr["TLeaveDay"].ToString() != "" ? dr["TLeaveDay"].ToString() : "0");
                        employeeLeaveRegister.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        employeeLeaveRegister.IsCompensate = Convert.ToString(dr["IsCompensate"]);
                        employeeLeaveRegister.LeaveType = Convert.ToString(dr["LeaveType"]);
                        employeeLeaveRegisterList.Add(employeeLeaveRegister);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeLeaveRegisterList;
        }

        public DataTable GetEmployeeStatisticsByNationalityData(string nationalityId, int statisticCount,int UserId)
        {
            DataTable dtreport = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_EmployeeStatisticsByNationality", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.Parameters.AddWithValue("@NationalityId", nationalityId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@StatisticCount", statisticCount);
                dtreport.Load(sqlCommand.ExecuteReader());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dtreport;

        }

        public List<EmployeeMinistryDetail> GetEmployeeMinistryReport(string DepartmentIds, string JobCategories,int UserId)
        {
            List<EmployeeMinistryDetail> lstEmpMinistrydetails = new List<EmployeeMinistryDetail>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("hr_Stp_GetEmployeeMinistryReportData", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@DepartmentIds", DepartmentIds);
                sqlCommand.Parameters.AddWithValue("@JobCategories", JobCategories);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeMinistryDetail employeeMinistryDetail;
                if (dr.HasRows)
                {
                    int i = 0;
                    while (dr.Read())
                    {
                        employeeMinistryDetail = new EmployeeMinistryDetail();
                        employeeMinistryDetail.Id = i++;
                        employeeMinistryDetail.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        employeeMinistryDetail.NameEn = Convert.ToString(dr["NameEn"]);
                        employeeMinistryDetail.NameAr = Convert.ToString(dr["NameAr"]);
                        employeeMinistryDetail.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        employeeMinistryDetail.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        employeeMinistryDetail.SpecializationEn = Convert.ToString(dr["SpecializationEn"]);
                        employeeMinistryDetail.SpecializationAr = Convert.ToString(dr["SpecializationAr"]);
                        employeeMinistryDetail.QualificationEn = Convert.ToString(dr["QualificationEn"]);
                        employeeMinistryDetail.QualificationAr = Convert.ToString(dr["QualificationAr"]);
                        employeeMinistryDetail.UniversityEn = Convert.ToString(dr["UniversityEn"]);
                        employeeMinistryDetail.UniversityAr = Convert.ToString(dr["UniversityAr"]);
                        employeeMinistryDetail.JobTitleEn = Convert.ToString(dr["JobTitleEn"]);
                        employeeMinistryDetail.JobTitleAr = Convert.ToString(dr["JobTitleAr"]);
                        employeeMinistryDetail.JobCategoryEn = Convert.ToString(dr["JobCategoryEn"]);
                        employeeMinistryDetail.JobCategoryAr = Convert.ToString(dr["JobCategoryAr"]);
                        employeeMinistryDetail.DepartmentEn = Convert.ToString(dr["DepartmentEn"]);
                        employeeMinistryDetail.DepartmentAr = Convert.ToString(dr["DepartmentAr"]);
                        employeeMinistryDetail.DOB = dr["DOB"].ToString() != "" ? CommonDB.GetFormattedDate_DDMMYYYY(dr["DOB"].ToString()) : "";
                        employeeMinistryDetail.BirthPlaceEn = dr["BirthPlaceEn"].ToString();
                        employeeMinistryDetail.BirthPlaceAr = dr["BirthPlaceAr"].ToString();
                        employeeMinistryDetail.CiviliNo = dr["CiviliNo"].ToString();
                        employeeMinistryDetail.SocialStatusEn = Convert.ToString(dr["SocialStatusEn"]);
                        employeeMinistryDetail.SocialStatusAr = Convert.ToString(dr["SocialStatusAr"]);
                        employeeMinistryDetail.DOJ = dr["DOJ"].ToString() != "" ? Convert.ToDateTime(dr["DOJ"].ToString()).ToString("yyyy") : "";
                        employeeMinistryDetail.Experience = Convert.ToString(dr["Experience"]);
                        employeeMinistryDetail.Email = Convert.ToString(dr["Email"]);
                        employeeMinistryDetail.PhoneNo = Convert.ToString(dr["PhoneNo"]);
                        employeeMinistryDetail.MOETitle_1 = Convert.ToString(dr["MOETitle_1"]);
                        employeeMinistryDetail.MOETitle_2 = Convert.ToString(dr["MOETitle_2"]);
                        employeeMinistryDetail.NameAr = string.IsNullOrEmpty(employeeMinistryDetail.NameAr.Trim()) ? employeeMinistryDetail.NameEn : employeeMinistryDetail.NameAr;
                        employeeMinistryDetail.JobCategoryAr = string.IsNullOrEmpty(employeeMinistryDetail.JobCategoryAr.Trim()) ? employeeMinistryDetail.JobCategoryEn : employeeMinistryDetail.JobCategoryAr;
                        employeeMinistryDetail.SpecializationAr = string.IsNullOrEmpty(employeeMinistryDetail.SpecializationAr.Trim()) ? employeeMinistryDetail.SpecializationEn : employeeMinistryDetail.SpecializationAr;
                        employeeMinistryDetail.QualificationAr = string.IsNullOrEmpty(employeeMinistryDetail.QualificationAr.Trim()) ? employeeMinistryDetail.QualificationEn : employeeMinistryDetail.QualificationAr;
                        employeeMinistryDetail.UniversityAr = string.IsNullOrEmpty(employeeMinistryDetail.UniversityAr.Trim()) ? employeeMinistryDetail.UniversityEn : employeeMinistryDetail.UniversityAr;
                        employeeMinistryDetail.JobTitleAr = string.IsNullOrEmpty(employeeMinistryDetail.JobTitleAr.Trim()) ? employeeMinistryDetail.JobTitleEn : employeeMinistryDetail.JobTitleAr;
                        employeeMinistryDetail.DepartmentAr = string.IsNullOrEmpty(employeeMinistryDetail.DepartmentAr.Trim()) ? employeeMinistryDetail.DepartmentEn : employeeMinistryDetail.DepartmentAr;
                        lstEmpMinistrydetails.Add(employeeMinistryDetail);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmpMinistrydetails;
        }

        public List<EmployeeTestimoanl> GetEmployeeTestimonalReport(string EmpIds, string SignedByEn, string SignedByAr)
        {
            List<EmployeeTestimoanl> lstEmpTestimonal = new List<EmployeeTestimoanl>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeTestimonalReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeTestimoanl empTestimonal;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        empTestimonal = new EmployeeTestimoanl();
                        empTestimonal.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        empTestimonal.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        empTestimonal.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        empTestimonal.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        empTestimonal.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        empTestimonal.PassportNo = Convert.ToString(dr["PassportNumber"]);
                        empTestimonal.EmpPosEn = Convert.ToString(dr["PositionEn"]);
                        empTestimonal.EmpPosAr = Convert.ToString(dr["PositionAr"]);
                        empTestimonal.JobTitleEn = Convert.ToString(dr["JobTitleEn"]);
                        empTestimonal.JobTitleAr = Convert.ToString(dr["JobTitleAr"]);
                        empTestimonal.FromDate = dr["FromDate"].ToString() == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(dr["FromDate"].ToString());
                        empTestimonal.ToDate = CommonDB.GetFormattedDate_DDMMYYYY(dr["ToDate"].ToString());
                        empTestimonal.AuthSignEn = SignedByEn;
                        empTestimonal.AuthSignAr = SignedByAr;
                        empTestimonal.GenderTemplate1AR = Convert.ToString(dr["GenderTemplate1AR"]);
                        empTestimonal.GenderTemplate2AR = Convert.ToString(dr["GenderTemplate2AR"]);
                        empTestimonal.GenderTemplate3AR = Convert.ToString(dr["GenderTemplate3AR"]);
                        empTestimonal.InstitutionText= Convert.ToString(dr["InstitutionText"]);
                        empTestimonal.PassportText = Convert.ToString(dr["PassportText"]);
                        lstEmpTestimonal.Add(empTestimonal);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmpTestimonal;
        }

        public List<StaffRenewalData> GetStaffRenewalReport(string EmpIds, string SignedByEn, string SignedByAr, string noteEn, string noteAr, int ModeBit = 0)
        {
            List<StaffRenewalData> lstStaffRenewal = new List<StaffRenewalData>();
            string spName = "";
            if (ModeBit == 1)
            {
                spName = "Hr_Stp_GetTransferSponsorshipReport";
            }
            else if (ModeBit == 0)
            {
                spName = "Hr_Stp_GetStaffRenewalReport";
            }
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(spName, sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                StaffRenewalData staffRenewal;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EnAllowanceData = "<line-height=\"1.7\">";
                        string ArAllowanceData = "<line-height=\"1.5\">";
                        staffRenewal = new StaffRenewalData();
                        staffRenewal.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        staffRenewal.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        staffRenewal.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        staffRenewal.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        staffRenewal.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        staffRenewal.PassportNo = Convert.ToString(dr["PassportNumber"]);
                        staffRenewal.EmpjobCategoryEn = Convert.ToString(dr["JobCategoryEn"]);
                        staffRenewal.EmpjobCategoryAr = Convert.ToString(dr["JobCategoryAr"]);
                        staffRenewal.BasicSalary = Convert.ToString(dr["BasicSalary"]);
                        staffRenewal.Allowances = Convert.ToString(dr["Allowances"]);
                        decimal basicSal = Convert.ToDecimal(staffRenewal.BasicSalary == "" ? "0" : staffRenewal.BasicSalary);
                        decimal Allowance = Convert.ToDecimal(staffRenewal.Allowances == "" ? "0" : staffRenewal.Allowances);
                        staffRenewal.TotalSalary = (basicSal + Allowance).ToString();
                        if (staffRenewal.TotalSalary == "0")
                        {
                            staffRenewal.TotalSalary = "";
                        }
                        staffRenewal.AuthSignEn = SignedByEn;
                        staffRenewal.AuthSignAr = SignedByAr;
                       
                        staffRenewal.JobTitleEn = Convert.ToString(dr["JobTitleEn"]);
                        staffRenewal.JobTitleAr = Convert.ToString(dr["JobTitleAr"]);
                        staffRenewal.GenderTemplateEn = Convert.ToString(dr["GenderTemplateEn"]);
                        staffRenewal.GenderTemplateAr = Convert.ToString(dr["GenderTemplateAr"]);
                        staffRenewal.JoiningDate = dr["HireDate"].ToString() == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(dr["HireDate"].ToString());
                        staffRenewal.EndgDate = dr["ResignationDate"].ToString() == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(dr["ResignationDate"].ToString());
                        staffRenewal.PositionTitleEn = dr["PositionTitleEn"].ToString();
                        staffRenewal.PositionTitleAr = dr["PositionTitleAr"].ToString();
                        staffRenewal.AccomodationHistoryAmount = dr["AccomodationHistoryAmount"].ToString() == "" ? 0 : Convert.ToDecimal(dr["AccomodationHistoryAmount"].ToString());
                        staffRenewal.Furnished = Convert.ToBoolean(dr["Furnished"]);
                        staffRenewal.FurnishedAmount = dr["FurnishedAmount"].ToString() == "" ? 0 : Convert.ToDecimal(dr["FurnishedAmount"].ToString());
                        staffRenewal.Electricity = Convert.ToBoolean(dr["Electricity"]);
                        staffRenewal.ElectricityAmount = dr["ElectricityAmount"].ToString() == "" ? 0 : Convert.ToDecimal(dr["ElectricityAmount"].ToString());
                        staffRenewal.Water = Convert.ToBoolean(dr["Water"]);
                        staffRenewal.WaterAmount = dr["WaterAmount"].ToString() == "" ? 0 : Convert.ToDecimal(dr["WaterAmount"].ToString());
                        staffRenewal.AirTicketAmount = dr["AirTicketAmount"].ToString() == "" ? 0 : Convert.ToDecimal(dr["AirTicketAmount"].ToString());
                        staffRenewal.AirTicketDependantAmount = dr["AirTicketDependantAmount"].ToString() == "" ? 0 : Convert.ToDecimal(dr["AirTicketDependantAmount"].ToString());
                        staffRenewal.GenderTemplate1Ar = Convert.ToString(dr["GenderTemplate1Ar"]);
                        staffRenewal.GenderTemplate2Ar = Convert.ToString(dr["GenderTemplate2Ar"]);
                        staffRenewal.GenderTemplate3Ar = Convert.ToString(dr["GenderTemplate3Ar"]);
                        staffRenewal.GenderTemplate5Ar = Convert.ToString(dr["GenderTemplate5Ar"]);
                        if (staffRenewal.AccomodationHistoryAmount > 0)
                        {
                            EnAllowanceData = EnAllowanceData + "<b>Accommodation Allowance: </b>" + staffRenewal.AccomodationHistoryAmount;
                            ArAllowanceData = ArAllowanceData + "<b>علاوة سكن: </b><font-size=\"12\">" + staffRenewal.AccomodationHistoryAmount + "</font-size>";
                        }
                        if (staffRenewal.Furnished)
                        {
                            EnAllowanceData = EnAllowanceData + "<br><b>Furnished Allowance: </b>" + staffRenewal.FurnishedAmount;
                            ArAllowanceData = ArAllowanceData + "<br><b>علاوة مفروشة: </b><font-size=\"12\">" + staffRenewal.FurnishedAmount + "</font-size>";
                        }
                        if (staffRenewal.Electricity)
                        {
                            EnAllowanceData = EnAllowanceData + "<br><b>Electricity Allowance: </b>" + staffRenewal.ElectricityAmount;
                            ArAllowanceData = ArAllowanceData + "<br><b>علاوة الكهرباء: </b><font-size=\"12\">" + staffRenewal.ElectricityAmount + "</font-size>";
                        }
                        if (staffRenewal.Water)
                        {
                            EnAllowanceData = EnAllowanceData + "<br><b>Water Allowance: </b>" + staffRenewal.WaterAmount;
                            ArAllowanceData = ArAllowanceData + "<br><b>علاوة المياه: </b><font-size=\"12\">" + staffRenewal.WaterAmount + "</font-size>";
                        }
                        if (staffRenewal.AirTicketAmount > 0)
                        {
                            EnAllowanceData = EnAllowanceData + "<br><b>Air Ticket Allowance: </b>" + staffRenewal.AirTicketAmount;
                            ArAllowanceData = ArAllowanceData + "<br><b>علاوة تذكرة الطيراني: </b><font-size=\"12\">" + staffRenewal.AirTicketAmount + "</font-size>";
                        }
                        if (staffRenewal.AirTicketDependantAmount > 0)
                        {
                            EnAllowanceData = EnAllowanceData + "<br><b>Air Ticket Dependent Allowance: </b>" + staffRenewal.AirTicketDependantAmount;
                            ArAllowanceData = ArAllowanceData + "<br><b>علاوة تذكرة طيران بدل الاعتماد: </b><font-size=\"12\">" + staffRenewal.AirTicketDependantAmount + "</font-size>";
                        }
                        EnAllowanceData = EnAllowanceData + "</line-height>";
                        ArAllowanceData = ArAllowanceData + "</line-height>";
                        staffRenewal.EnAllowanceData = EnAllowanceData;
                        staffRenewal.ArAllowanceData = ArAllowanceData;
                        staffRenewal.NoteEn = "<line-height=\"1.5\">"+ noteEn+ "</line-height>";
                        staffRenewal.NoteAr = "<line-height=\"1.5\">" + noteAr + "</line-height>";
                        lstStaffRenewal.Add(staffRenewal);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstStaffRenewal;
        }

        public List<EmployeeClarificationData> GetEmployeeClarificationReport(string EmpIds, string SignedByEn, string SignedByAr)
        {
            List<EmployeeClarificationData> lstEmployeeClarification = new List<EmployeeClarificationData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetEmployeeClarificationReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeClarificationData employeeClarification;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EnAllowanceData = "<line-height=\"1.7\">";
                        string ArAllowanceData = "<line-height=\"1.5\">";
                        employeeClarification = new EmployeeClarificationData();
                        employeeClarification.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        employeeClarification.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        employeeClarification.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        employeeClarification.AuthSignEn = SignedByEn;
                        employeeClarification.AuthSignAr = SignedByAr;
                        employeeClarification.TodayDate =CommonDB.GetFormattedDate_DDMMYYYY(System.DateTime.Now.ToString());
                        employeeClarification.PositionTitleEn = dr["PositionTitleEn"].ToString();
                        employeeClarification.PositionTitleAr = dr["PositionTitleAr"].ToString();
                        EnAllowanceData = EnAllowanceData + "</line-height>";
                        ArAllowanceData = ArAllowanceData + "</line-height>";
                        employeeClarification.EnAllowanceData = EnAllowanceData;
                        employeeClarification.ArAllowanceData = ArAllowanceData;
                        lstEmployeeClarification.Add(employeeClarification);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmployeeClarification;
        }

        public List<EmployeeServiceCertificateData> GetEmployeeServiceCertificateReport(string EmpIds, string SignedByEn, string SignedByAr)
        {
            List<EmployeeServiceCertificateData> lstEmployeeServiceCertificate = new List<EmployeeServiceCertificateData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeServiceCertificate", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeServiceCertificateData employeeServiceCertificate;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EnAllowanceData = "<line-height=\"1.7\">";
                        string ArAllowanceData = "<line-height=\"1.5\">";
                        employeeServiceCertificate = new EmployeeServiceCertificateData();
                        employeeServiceCertificate.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        employeeServiceCertificate.EmployeeAlternativeID = Convert.ToInt32(dr["EmployeeAlternativeID"]);
                        employeeServiceCertificate.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        employeeServiceCertificate.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        employeeServiceCertificate.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        employeeServiceCertificate.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        employeeServiceCertificate.AuthSignEn = SignedByEn;
                        employeeServiceCertificate.AuthSignAr = SignedByAr;
                        employeeServiceCertificate.JoiningDate = dr["HireDate"].ToString() == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(dr["HireDate"].ToString());
                        employeeServiceCertificate.EndgDate = dr["ResignationDate"].ToString() == "" ? CommonDB.GetFormattedDate_DDMMYYYY(System.DateTime.Now.ToString()) : CommonDB.GetFormattedDate_DDMMYYYY(dr["ResignationDate"].ToString());
                        employeeServiceCertificate.PositionTitleEn = dr["PositionTitleEn"].ToString();
                        employeeServiceCertificate.PositionTitleAr = dr["PositionTitleAr"].ToString();
                        employeeServiceCertificate.PayDocumentNumber = dr["PayDocumentNumber"].ToString();
                        EnAllowanceData = EnAllowanceData + "</line-height>";
                        ArAllowanceData = ArAllowanceData + "</line-height>";
                        employeeServiceCertificate.EnAllowanceData = EnAllowanceData;
                        employeeServiceCertificate.ArAllowanceData = ArAllowanceData;
                        lstEmployeeServiceCertificate.Add(employeeServiceCertificate);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmployeeServiceCertificate;
        }

        public List<EmployeeSchoolJoiningDateData> GetEmployeeSchoolJoiningDateReport(string EmpIds, string SignedByEn, string SignedByAr)
        {
            List<EmployeeSchoolJoiningDateData> lstEmployeeSchoolJoiningDate = new List<EmployeeSchoolJoiningDateData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeServiceCertificate", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeSchoolJoiningDateData employeeSchoolJoiningDate;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EnAllowanceData = "<line-height=\"1.7\">";
                        string ArAllowanceData = "<line-height=\"1.5\">";
                        employeeSchoolJoiningDate = new EmployeeSchoolJoiningDateData();
                        employeeSchoolJoiningDate.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        employeeSchoolJoiningDate.EmployeeAlternativeID = Convert.ToInt32(dr["EmployeeAlternativeID"]);
                        employeeSchoolJoiningDate.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        employeeSchoolJoiningDate.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        employeeSchoolJoiningDate.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        employeeSchoolJoiningDate.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        employeeSchoolJoiningDate.AuthSignEn = SignedByEn;
                        employeeSchoolJoiningDate.AuthSignAr = SignedByAr;
                        employeeSchoolJoiningDate.JoiningDate = dr["HireDate"].ToString() == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(dr["HireDate"].ToString());
                        employeeSchoolJoiningDate.EndgDate = dr["ResignationDate"].ToString() == "" ? CommonDB.GetFormattedDate_DDMMYYYY(System.DateTime.Now.ToString()) : CommonDB.GetFormattedDate_DDMMYYYY(dr["ResignationDate"].ToString());
                        employeeSchoolJoiningDate.PositionTitleEn = dr["PositionTitleEn"].ToString();
                        employeeSchoolJoiningDate.PositionTitleAr = dr["PositionTitleAr"].ToString();
                        EnAllowanceData = EnAllowanceData + "</line-height>";
                        ArAllowanceData = ArAllowanceData + "</line-height>";
                        employeeSchoolJoiningDate.EnAllowanceData = EnAllowanceData;
                        employeeSchoolJoiningDate.ArAllowanceData = ArAllowanceData;
                        lstEmployeeSchoolJoiningDate.Add(employeeSchoolJoiningDate);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmployeeSchoolJoiningDate;
        }
        public List<EmployeePermissionData> GetEmployeePermissionReport(string EmpIds, string SignedByEn, string SignedByAr)
        {
            List<EmployeePermissionData> lstEmployeePermission = new List<EmployeePermissionData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeServiceCertificate", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeePermissionData employeePermission;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EnAllowanceData = "<line-height=\"1.7\">";
                        string ArAllowanceData = "<line-height=\"1.5\">";
                        employeePermission = new EmployeePermissionData();
                        employeePermission.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        employeePermission.EmployeeAlternativeID = Convert.ToInt32(dr["EmployeeAlternativeID"]);
                        employeePermission.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        employeePermission.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        employeePermission.AuthSignEn = SignedByEn;
                        employeePermission.AuthSignAr = SignedByAr;
                        employeePermission.PositionTitleEn = dr["PositionTitleEn"].ToString();
                        employeePermission.PositionTitleAr = dr["PositionTitleAr"].ToString();
                        EnAllowanceData = EnAllowanceData + "</line-height>";
                        ArAllowanceData = ArAllowanceData + "</line-height>";
                        employeePermission.EnAllowanceData = EnAllowanceData;
                        employeePermission.ArAllowanceData = ArAllowanceData;
                        lstEmployeePermission.Add(employeePermission);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmployeePermission;
        }

        public List<EmployeeAgreementData> GetEmpAgreementReport(string EmpIds, string SignedByEn, string SignedByAr, string AgrDate, string YearsofContract, string CStartDate, string CEndDate, string PaidLeaves)
        {
            List<EmployeeAgreementData> lstEmployeeAgreementData = new List<EmployeeAgreementData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeAgreementDetails", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeAgreementData empAgreementData;
                string amountFormate = new PayrollDB().GetAmountFormat();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        empAgreementData = new EmployeeAgreementData();
                        empAgreementData.EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
                        empAgreementData.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        empAgreementData.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        empAgreementData.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        empAgreementData.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        empAgreementData.PassportNo = Convert.ToString(dr["PassportNumber"]);
                        empAgreementData.EmpjobCategoryEn = Convert.ToString(dr["JobCategoryEn"]);
                        empAgreementData.EmpjobCategoryAr = Convert.ToString(dr["JobCategoryAr"]);
                        empAgreementData.BasicSalary = Convert.ToDecimal(dr["BasicSalary"]).ToString(amountFormate);
                        empAgreementData.Allowances = Convert.ToDecimal(dr["Allowances"]).ToString(amountFormate);
                        empAgreementData.TotalSalary = (Convert.ToDecimal(dr["BasicSalary"]) + Convert.ToDecimal(dr["Allowances"])).ToString(amountFormate);
                        empAgreementData.AuthSignEn = SignedByEn;
                        empAgreementData.AuthSignAr = SignedByAr;
                        empAgreementData.JobTitleEn = Convert.ToString(dr["JobTitleEn"]);
                        empAgreementData.JobTitleAr = Convert.ToString(dr["JobTitleAr"]);
                        empAgreementData.GenderTemplateEn = Convert.ToString(dr["GenderTemplateEn"]);
                        empAgreementData.GenderTemplateAr = Convert.ToString(dr["GenderTemplateAr"]);
                        empAgreementData.JoiningDate = dr["HireDate"].ToString() == "" ? "" : Convert.ToDateTime(dr["HireDate"]).ToString("dd/MM/yyyy");
                        empAgreementData.EndgDate = dr["ResignationDate"].ToString() == "" ? "" : Convert.ToDateTime(dr["ResignationDate"]).ToString("dd/MM/yyyy");
                        empAgreementData.EmployeePositionEn = Convert.ToString(dr["EmployeePositionEn"]);
                        empAgreementData.EmployeePositionAr = Convert.ToString(dr["EmployeePositionAr"]);
                        empAgreementData.ElectricityWaterAmount = Convert.ToString(dr["ElectricityWaterAmount"]);
                        empAgreementData.TransportationAmount = Convert.ToString(dr["TransportationAmount"]);                        
                        empAgreementData.AirTicketAmount = Convert.ToString(dr["AirTicketAmount"]);
                        empAgreementData.AirTicketAmountAR = Convert.ToString(dr["AirTicketAmountAR"]);
                        empAgreementData.Housing = Convert.ToString(dr["Housing"]);
                        empAgreementData.HousingAR = Convert.ToString(dr["HousingAR"]);
                        empAgreementData.InsuranceType = Convert.ToString(dr["InsuranceType"]);
                        empAgreementData.InsuranceTypeAR = Convert.ToString(dr["InsuranceTypeAR"]);
                        empAgreementData.AgreementDate = AgrDate;
                        empAgreementData.PaidLeaves = PaidLeaves;
                        empAgreementData.YearsofContract = YearsofContract;
                        CommonDB objCommonDB = new CommonDB();
                        DateTime AgDate = new DateTime();
                        if (!string.IsNullOrEmpty(AgrDate))
                        {
                            AgDate = DateTime.ParseExact(AgrDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            empAgreementData.AgreementDate = AgDate.ToString("dd/MM/yyyy");
                            string weekDay = AgDate.DayOfWeek.ToString();
                            int DayId = objCommonDB.weekDays.Where(x => x.Key == weekDay).FirstOrDefault().Value;
                            empAgreementData.WeekdayAr = objCommonDB.weekDaysArabic.Where(x => x.Key == DayId).FirstOrDefault().Value;
                            empAgreementData.WeekdayEn = weekDay;
                        }                       
                        empAgreementData.ContractStartDate = CStartDate;
                        empAgreementData.ContractEndDate = CEndDate;
                        lstEmployeeAgreementData.Add(empAgreementData);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmployeeAgreementData;
        }

        public List<EmployeeReport> GetEmployeeSectionReport(string SectionIdds)
        {
            dataHelper = new DataHelper();
            List<EmployeeReport> EmployeeReportList = new List<EmployeeReport>();
            EmployeeReport employeeReport = null;
            DataTable Dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>() { new SqlParameter("@SectionIds", SectionIdds) }, "Hr_StpGetEmployeeBySection");
            foreach (DataRow dr in Dt.Rows)
            {
                employeeReport = new EmployeeReport();
                employeeReport.EmpoyeeName = dr["EmployeeName"].ToString();
                employeeReport.SectionName = dr["EmployeeSectionName_1"].ToString();
                employeeReport.SectionId = Convert.ToInt32(dr["EmployeeSectionId"].ToString());
                EmployeeReportList.Add(employeeReport);
            }
            return EmployeeReportList;
        }

        public LetterTemplates GetLetterTemplateById(int LettersId)
        {
            dataHelper = new DataHelper();
            LetterTemplates letterTemplates = new LetterTemplates();
            DataTable Dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>() { new SqlParameter("@aIntPayReportBuilderLettersID", LettersId) }, "HR_uspGetPayReportBuilderLettersByID");
            foreach (DataRow dr in Dt.Rows)
            {
                letterTemplates.LettersId = Convert.ToInt32(dr["PayReportBuilderLettersID"]);
                letterTemplates.Lettername = dr["name"].ToString();
                letterTemplates.HtmlContent = dr["HtmlContent"].ToString();
                letterTemplates.LetterReportFields = dr["LetterReportFields"].ToString();
            }
            return letterTemplates;
        }

        public IEnumerable<PayrollReportDetails> GetPayrollReportDetails(string EmployeeIds, string TypeIds, string FromDate, string ToDate)
        {

            List<PayrollReportDetails> lstPayrollReportDetails = new List<PayrollReportDetails>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPayrollReportdata", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmployeeIds);
                sqlCommand.Parameters.AddWithValue("@TypeIds", TypeIds);
                sqlCommand.Parameters.AddWithValue("@FromDate", CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                PayrollReportDetails payrollReportDetails;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        payrollReportDetails = new PayrollReportDetails();
                        payrollReportDetails.EmployeeAlternativeId = Convert.ToString(dr["EmployeeAlternativeId"]);
                        payrollReportDetails.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        payrollReportDetails.PayType = Convert.ToString(dr["PayType"]);
                        payrollReportDetails.TransactionDate = CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(dr["TransactionDate"]));
                        payrollReportDetails.Amount = Convert.ToString(dr["Amount"]);
                        payrollReportDetails.TypeName = Convert.ToString(dr["TypeName"]);
                        lstPayrollReportDetails.Add(payrollReportDetails);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstPayrollReportDetails;
        }

        public List<EmployeeSalaryTransferCertificate> GetSalaryTransferCertificateReport(string EmpIds, string SignedByEn, string SignedByAr)
        {
            List<EmployeeSalaryTransferCertificate> lstEmployeeSalaryTransferCertificate = new List<EmployeeSalaryTransferCertificate>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSalaryTransferCertificate", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeSalaryTransferCertificate employeeSalaryTransferCertificate;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        employeeSalaryTransferCertificate = new EmployeeSalaryTransferCertificate();
                        employeeSalaryTransferCertificate.EmployeeId = Convert.ToString(dr["EmployeeId"]);
                        employeeSalaryTransferCertificate.EmpNameEn = Convert.ToString(dr["EmployeeNameEn"]);
                        employeeSalaryTransferCertificate.EmpNameAr = Convert.ToString(dr["EmployeeNameAr"]);
                        employeeSalaryTransferCertificate.NationalityEn = Convert.ToString(dr["NationalityEn"]);
                        employeeSalaryTransferCertificate.NationalityAr = Convert.ToString(dr["NationalityAr"]);
                        employeeSalaryTransferCertificate.PassportNo = Convert.ToString(dr["PassportNumber"]);
                        employeeSalaryTransferCertificate.BasicSalary = Convert.ToString(dr["BasicSalary"]);
                        employeeSalaryTransferCertificate.Allowances = Convert.ToString(dr["Allowances"]);
                        employeeSalaryTransferCertificate.OtherAllowances = Convert.ToString(dr["OtherAllowances"]) == "" ? "0" : Convert.ToString(dr["OtherAllowances"]);
                        employeeSalaryTransferCertificate.CurrencyFormat = Convert.ToString(dr["CurrencyFormat"]);
                        decimal basicSal = Convert.ToDecimal(employeeSalaryTransferCertificate.BasicSalary == "" ? "0" : employeeSalaryTransferCertificate.BasicSalary);
                        decimal Allowance = Convert.ToDecimal(employeeSalaryTransferCertificate.Allowances == "" ? "0" : employeeSalaryTransferCertificate.Allowances);                      
                        employeeSalaryTransferCertificate.TotalSalary = (basicSal + Allowance).ToString();
                        if (employeeSalaryTransferCertificate.TotalSalary == "0")
                        {
                            employeeSalaryTransferCertificate.TotalSalary = "";
                        }
                        employeeSalaryTransferCertificate.AuthSignEn = SignedByEn;
                        employeeSalaryTransferCertificate.AuthSignAr = SignedByAr;
                        employeeSalaryTransferCertificate.GenderTemplateEn = Convert.ToString(dr["GenderTemplateEn"]);
                        employeeSalaryTransferCertificate.GenderTemplate1Ar = Convert.ToString(dr["GenderTemplate1Ar"]);
                        employeeSalaryTransferCertificate.GenderTemplate2Ar = Convert.ToString(dr["GenderTemplate2Ar"]);
                        employeeSalaryTransferCertificate.GenderTemplate3Ar = Convert.ToString(dr["GenderTemplate3Ar"]);
                        employeeSalaryTransferCertificate.GenderTemplate4Ar = Convert.ToString(dr["GenderTemplate4Ar"]);
                        employeeSalaryTransferCertificate.GenderTemplateAr = Convert.ToString(dr["GenderTemplateAr"]);
                        employeeSalaryTransferCertificate.PositionTitleEn = dr["PositionTitleEn"].ToString();
                        employeeSalaryTransferCertificate.PositionTitleAr = dr["PositionTitleAr"].ToString();
                        employeeSalaryTransferCertificate.BankNameEN = dr["BankNameEN"].ToString();
                        employeeSalaryTransferCertificate.BankNameAR = dr["BankNameAR"].ToString();
                        employeeSalaryTransferCertificate.BanchNameEN = dr["BanchNameEN"].ToString();
                        employeeSalaryTransferCertificate.BanchNameAR = dr["BanchNameAR"].ToString();
                        employeeSalaryTransferCertificate.AccountNumber = dr["AccountNumber"].ToString();
                        employeeSalaryTransferCertificate.JoiningDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["JoiningDate"].ToString());
                        lstEmployeeSalaryTransferCertificate.Add(employeeSalaryTransferCertificate);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return lstEmployeeSalaryTransferCertificate;
        }
             

        public List<EmployeeExcuseDetail> GetEmployeeExcuseDetailReport(string FromDate, string ToDate, string EmployeeIds, string SelectCase, bool DetailsReport,int UserId)
        {
            List<EmployeeExcuseDetail> listEmployeeExcuseDetail = new List<EmployeeExcuseDetail>();
            EmployeeExcuseDetail employeeExcuseDetail;
            using (sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeNoofExcuses", sqlConnection))
                {
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 9000;
                    sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                    sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                    sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmployeeIds);
                    sqlCommand.Parameters.AddWithValue("@SelectCase", @SelectCase);
                    sqlCommand.Parameters.AddWithValue("@Mode", DetailsReport ? 2 : 1);
                    sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                    SqlDataReader dr = sqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        employeeExcuseDetail = new EmployeeExcuseDetail();
                        employeeExcuseDetail.EmpId = dr["EmployeeId"].ToString();
                        employeeExcuseDetail.EmployeeName = dr["EmployeeName"].ToString();
                        if (!DetailsReport)
                        {
                            employeeExcuseDetail.NoExAbsence = dr["NoExAbsent"].ToString();
                            employeeExcuseDetail.NoNonExAbsence = dr["NoNonExAbsent"].ToString();
                            employeeExcuseDetail.NoExLate = dr["NoExLateIn"].ToString();
                            employeeExcuseDetail.NoNonExLate = dr["NoNonExLateIn"].ToString();
                            employeeExcuseDetail.NoExEarly = dr["NoExEarlyOut"].ToString();
                            employeeExcuseDetail.NoNonExEarly = dr["NoNonExEarlyOut"].ToString();
                        }
                        else
                        {
                            employeeExcuseDetail.Date = CommonDB.GetFormattedDate_DDMMYYYY(dr["Date"].ToString());
                            employeeExcuseDetail.Type = dr["Type"].ToString();
                        }
                        listEmployeeExcuseDetail.Add(employeeExcuseDetail);
                    }
                }
            }
            return listEmployeeExcuseDetail;
        }

        public DataTable GetEmployeeSalaryDataForCycle(string EmployeeIds, int CycleId, bool IsArabic)
        {
            DataTable DtReport = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_StpGetSalaryDataForCycle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmployeeIds);
                sqlCommand.Parameters.AddWithValue("@PayCycleId", CycleId);
                sqlCommand.Parameters.AddWithValue("@IsArabic", IsArabic);
                DtReport.Load(sqlCommand.ExecuteReader());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return DtReport;

        }

        public List<EmployeeAbsentAndLateData> GetEmployeeLateAbsentData(string EmpIds, string StartDate, string EndDate)
        {
            List<EmployeeAbsentAndLateData> lstEmployeeAbsentAndLateData = new List<EmployeeAbsentAndLateData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetEmployeeAbsentAndLateRecords", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@empid", EmpIds);
                sqlCommand.Parameters.AddWithValue("@StartDate", CommonDB.SetCulturedDate(StartDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", CommonDB.SetCulturedDate(EndDate));
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                EmployeeAbsentAndLateData employeeAbsentAndLateData;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EnAllowanceData = "<line-height=\"1.7\">";
                        string ArAllowanceData = "<line-height=\"1.5\">";
                        employeeAbsentAndLateData = new EmployeeAbsentAndLateData();
                        employeeAbsentAndLateData.EmployeeAlternativeID = Convert.ToInt32(dr["EmployeeAlternativeID"]);
                        employeeAbsentAndLateData.EmployeeId = Convert.ToInt32(dr["EmployeeID"]);
                        employeeAbsentAndLateData.AbsentAndLateDate = Convert.ToString(dr["LeaveDate"]);
                        employeeAbsentAndLateData.EmpNameEn = Convert.ToString(dr["EmployeeName"]);

                        employeeAbsentAndLateData.ShiftName = Convert.ToString(dr["LeaveShift"]);
                        employeeAbsentAndLateData.Department = Convert.ToString(dr["Department"]);
                        employeeAbsentAndLateData.Section = Convert.ToString(dr["Section"]);
                        employeeAbsentAndLateData.PositionTitleEn = Convert.ToString(dr["Position"]);
                        employeeAbsentAndLateData.JobCategory = Convert.ToString(dr["Job Category"]);
                        employeeAbsentAndLateData.AbsentCount = dr["AbsentCount"].ToString() == "" ? 0 : Convert.ToInt32(dr["AbsentCount"].ToString());
                        employeeAbsentAndLateData.LateCount = dr["LateCount"].ToString() == "" ? 0 : Convert.ToInt32(dr["LateCount"].ToString());

                        employeeAbsentAndLateData.Status = Convert.ToString(dr["LeaveStatus"]);
                        employeeAbsentAndLateData.LeaveType = Convert.ToString(dr["LeaveType"]);
                        employeeAbsentAndLateData.Deduct = Convert.ToString(dr["Deduct"]);
                        employeeAbsentAndLateData.IsPaid = Convert.ToString(dr["Paid"]);
                        EnAllowanceData = EnAllowanceData + "</line-height>";
                        ArAllowanceData = ArAllowanceData + "</line-height>";
                        employeeAbsentAndLateData.EnAllowanceData = EnAllowanceData;
                        employeeAbsentAndLateData.ArAllowanceData = ArAllowanceData;
                        lstEmployeeAbsentAndLateData.Add(employeeAbsentAndLateData);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmployeeAbsentAndLateData;
        }

        public DataSet GetAllEmployeeDataForReport(string EmpIds)
        {
            DataSet ds = new DataSet();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetEmployeeCurrentSalaryReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@empid", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = sqlCommand;

                mySqlDataAdapter.Fill(ds);

                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public DataSet GetPaySlipStatementReport(string EmpIds, string CycleIds, int OperationBit)
        {
            DataSet ds = new DataSet();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSalaryReportByCycle", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmpIds);
                sqlCommand.Parameters.AddWithValue("@PayCycleId", CycleIds);
                sqlCommand.Parameters.AddWithValue("@OperationBit", OperationBit);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = sqlCommand;

                mySqlDataAdapter.Fill(ds);

                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public DataSet GetPayCycleSummaryReport(string PayCycleId, string CategoryIds)
        {
            DataSet ds = new DataSet();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPayCycleSummaryReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PayCycleId", PayCycleId);
                sqlCommand.Parameters.AddWithValue("@CategoryIds", CategoryIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = sqlCommand;

                mySqlDataAdapter.Fill(ds);

                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public DataSet GetSalariesBySectionReport(string SectionId, string OrganizationIds)
        {
            DataSet ds = new DataSet();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSalariesBySectionReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@SectionId", SectionId);
                sqlCommand.Parameters.AddWithValue("@OrganizationIds", OrganizationIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = sqlCommand;

                mySqlDataAdapter.Fill(ds);

                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public List<JordanIncomeTaxData> GetJordanIncomeTaxReport(string EmpIds)
        {
            List<JordanIncomeTaxData> lstJordanIncomeTax = new List<JordanIncomeTaxData>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_JordanIncomeTaxReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmpIds);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                JordanIncomeTaxData jordanIncomeTax;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {

                        jordanIncomeTax = new JordanIncomeTaxData();
                        jordanIncomeTax.EmpNameAr = Convert.ToString(dr["EmployeeName"]);
                        jordanIncomeTax.EmployeeSalary = Convert.ToString(dr["EmployeeSalary"]);
                        jordanIncomeTax.YearlySalary = Convert.ToString(dr["YearlySalary"]);
                        jordanIncomeTax.ExemptionYearlySalary = Convert.ToString(dr["ExemptionYearlySalary"]);
                        jordanIncomeTax.OtherExemptions = Convert.ToString(dr["OtherExemptions"]);
                        jordanIncomeTax.TaxableSalary = Convert.ToString(dr["TaxableSalary"]);
                        jordanIncomeTax.Level1_Amount = Convert.ToString(dr["Level1_Amount"]);
                        jordanIncomeTax.Level2_Amount = Convert.ToString(dr["Level2_Amount"]);
                        jordanIncomeTax.Level3_Amount = Convert.ToString(dr["Level3_Amount"]);
                        jordanIncomeTax.TotalTax = Convert.ToString(dr["TotalTax"]);
                        jordanIncomeTax.TaxPerMonth = Convert.ToString(dr["TaxPerMonth"]);
                        jordanIncomeTax.Notes = Convert.ToString(dr["Notes"]);

                        lstJordanIncomeTax.Add(jordanIncomeTax);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstJordanIncomeTax;
        }

        public List<ReportTemplate> GetAllReportTemplateInfo(int reportTemplateID)
        {
            List<ReportTemplate> lstReportTemplate = new List<ReportTemplate>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_JordanIncomeTaxReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", reportTemplateID);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                ReportTemplate reportTemplate;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {

                        reportTemplate = new ReportTemplate();
                        reportTemplate.ReportTemplateID = Convert.ToInt32(dr["ReportTemplateID"]);
                        reportTemplate.ReportTemplateName = Convert.ToString(dr["ReportTemplateName"]);
                        reportTemplate.IsHeaderRedundancy = Convert.ToBoolean(dr["IsHeaderRedundancy"].ToString()==""?"false": dr["IsHeaderRedundancy"].ToString());
                        reportTemplate.IsFooterRedundancy = Convert.ToBoolean(dr["IsFooterRedundancy"].ToString() == "" ? "false" : dr["IsFooterRedundancy"].ToString());
                        reportTemplate.IsHeaderVisibile = Convert.ToBoolean(dr["IsHeaderVisibile"].ToString() == "" ? "false" : dr["IsHeaderVisibile"].ToString());
                        reportTemplate.IsFooterVisible = Convert.ToBoolean(dr["IsFooterVisible"].ToString() == "" ? "false" : dr["IsFooterVisible"].ToString());
                        reportTemplate.IsHeaderLogoVisible = Convert.ToBoolean(dr["IsHeaderLogoVisible"].ToString() == "" ? "false" : dr["IsHeaderLogoVisible"].ToString());
                        reportTemplate.IsSchoolNameCampusVisible = Convert.ToBoolean(dr["IsSchoolNameCampusVisible"].ToString() == "" ? "false" : dr["IsSchoolNameCampusVisible"].ToString());
                        reportTemplate.SchoolNameCampusFontId = Convert.ToInt32(dr["SchoolNameCampusFontId"].ToString());
                        reportTemplate.SchoolNameCampusFontName = Convert.ToString(dr["SchoolNameCampusFontName"].ToString());
                        reportTemplate.SchoolNameCampusFontSize = Convert.ToInt16(dr["SchoolNameCampusFontSize"].ToString());
                        reportTemplate.IsSchoolNameCampusFontBold = Convert.ToBoolean(dr["IsSchoolNameCampusFontBold"].ToString() == "" ? "false" : dr["IsSchoolNameCampusFontBold"].ToString());
                        reportTemplate.SchoolSlogan = dr["SchoolSlogan"].ToString();
                        reportTemplate.IsSchoolSlogonVisible= Convert.ToBoolean(dr["IsSchoolSlogonVisible"].ToString() == "" ? "false" : dr["IsSchoolSlogonVisible"].ToString());
                        reportTemplate.SchoolSlogonFontId= Convert.ToInt32(dr["SchoolSlogonFontId"].ToString());
                        reportTemplate.SchoolSlogonFontSize= Convert.ToInt16(dr["SchoolSlogonFontSize"].ToString());
                        reportTemplate.IsSchoolSlogonFontBold = Convert.ToBoolean(dr["IsSchoolSlogonFontBold"].ToString() == "" ? "false" : dr["IsSchoolSlogonFontBold"].ToString());
                        reportTemplate.HeaderCustomMessage = dr["HeaderCustomMessage"].ToString();
                        reportTemplate.IsHeaderCustomMessageVisible = Convert.ToBoolean(dr["IsHeaderCustomMessageVisible"].ToString() == "" ? "false" : dr["IsHeaderCustomMessageVisible"].ToString());
                        reportTemplate.HeaderCustomMessageFontId= Convert.ToInt32(dr["HeaderCustomMessageFontId"].ToString());
                        reportTemplate.HeaderCustomMessageFontSize= Convert.ToInt16(dr["HeaderCustomMessageFontSize"].ToString());
                        reportTemplate.IsHeaderCustomMessageFontBold= Convert.ToBoolean(dr["IsHeaderCustomMessageFontBold"].ToString() == "" ? "false" : dr["IsHeaderCustomMessageFontBold"].ToString());
                        reportTemplate.FooterCustomMessage = dr["FooterCustomMessage"].ToString();
                        reportTemplate.IsFooterCustomMessageVisible= Convert.ToBoolean(dr["IsFooterCustomMessageVisible"].ToString() == "" ? "false" : dr["IsFooterCustomMessageVisible"].ToString());
                        reportTemplate.FooterCustomMessageFontId = Convert.ToInt32(dr["FooterCustomMessageFontId"].ToString());
                        reportTemplate.HeaderCustomMessageFontName = dr["HeaderCustomMessageFontName"].ToString();
                        reportTemplate.FooterCustomMessageFontSize= Convert.ToInt16(dr["FooterCustomMessageFontSize"].ToString());
                        reportTemplate.IsFooterCustomMessageFontBold= Convert.ToBoolean(dr["IsFooterCustomMessageFontBold"].ToString() == "" ? "false" : dr["IsFooterCustomMessageFontBold"].ToString());
                        reportTemplate.IsReportNameCustomVisible= Convert.ToBoolean(dr["IsReportNameCustomVisible"].ToString() == "" ? "false" : dr["IsReportNameCustomVisible"].ToString());
                        reportTemplate.ReportNameCustomFontId= Convert.ToInt32(dr["ReportNameCustomFontId"].ToString());
                        reportTemplate.ReportNameCustomFontName = dr["ReportNameCustomFontName"].ToString();
                        reportTemplate.ReportNameCustomFontSize= Convert.ToInt16(dr["ReportNameCustomFontSize"].ToString());
                        reportTemplate.IsReportNameCustomFontBold= Convert.ToBoolean(dr["IsReportNameCustomFontBold"].ToString() == "" ? "false" : dr["IsReportNameCustomFontBold"].ToString());
                        reportTemplate.IsDateVisible= Convert.ToBoolean(dr["IsDateVisible"].ToString() == "" ? "false" : dr["IsDateVisible"].ToString());
                        reportTemplate.IsTimeVisible = Convert.ToBoolean(dr["IsTimeVisible"].ToString() == "" ? "false" : dr["IsTimeVisible"].ToString());
                        reportTemplate.IsPageNumberVisible= Convert.ToBoolean(dr["IsPageNumberVisible"].ToString() == "" ? "false" : dr["IsPageNumberVisible"].ToString());
                        reportTemplate.IsFooterSchoolContactVisible = Convert.ToBoolean(dr["IsFooterSchoolContactVisible"].ToString() == "" ? "false" : dr["IsFooterSchoolContactVisible"].ToString());
                        reportTemplate.FooterSchoolContactFontId= Convert.ToInt32(dr["FooterSchoolContactFontId"].ToString());
                        
                       // FooterSchoolContactFontName FooterSchoolContactFontSize IsFooterSchoolContactFontBold IsHeaderSchoolContactVisible HeaderSchoolContactFontId HeaderSchoolContactFontName HeaderSchoolContactFontSize IsHeaderSchoolContactFontBold   IsFooterLogoVisible SignatureLabel  KeyPositionID IsSignatureVisible  IsSignatureLabelVisible SignatureLabelFontId    SignatureLabelFontName SignatureLabelFontSize  IsSignatureLabelFontBold IsUserPrintedVisible    IsHeaderTelephone1Visible IsHeaderTelephone2Visible   IsHeaderFaxVisible IsHeaderPoBoxVisible    IsHeaderEmailVisible IsHeaderWebSiteVisible  IsFooterTelephone1Visible isFooterTelephone2Visible   IsFooterFaxVisible IsFooterPoBoxVisible    IsFooterEmailVisible IsFooterWebSiteVisible  HeaderContact FooterContact   HeaderPhoneSequence HeaderFaxSequence   HeaderPoBoxSequence HeaderEmailSequence HeaderWebsiteSequence FooterPhoneSequence FooterFaxSequence FooterPoBoxSequence FooterEmailSequence FooterWebsiteSequence   FooterCustomMessageFontName

                        lstReportTemplate.Add(reportTemplate);
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstReportTemplate;
        }

        #region WPS Monthly salary report
        public DataTable GetWPSMonthlySalaryReportData(string complanyIds, int? payCycleId)
        {
            DataTable dtreport = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_WPSMonthlySalaryReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                if (complanyIds == "" || complanyIds == null)
                {
                    sqlCommand.Parameters.AddWithValue("@CompanyIds", DBNull.Value);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@CompanyIds", complanyIds);
                }
                if (!payCycleId.HasValue || payCycleId == null)
                {
                    sqlCommand.Parameters.AddWithValue("@PayCycleId", DBNull.Value);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@PayCycleId", payCycleId);
                }              
                dtreport.Load(sqlCommand.ExecuteReader());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dtreport;

        }      
        #endregion
    }
}

