﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class EmergencyContactDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        private GeneralDB.DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<EmergencyContactModel> GetAllEmergencyContactList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, int EmployeeId)
        {
            List<EmergencyContactModel> emergencyContactList = null;
            try
            {
                emergencyContactList = new List<EmergencyContactModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetEmergencyContact", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);

                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmergencyContactModel emergencyContact;
                    while (sqlDataReader.Read())
                    {
                        emergencyContact = new EmergencyContactModel();
                        emergencyContact.EmergencyContactId = Convert.ToInt32(sqlDataReader["EmergencyContactId"].ToString());
                        emergencyContact.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        emergencyContact.ContactName = Convert.ToString(sqlDataReader["Contact Name"]);

                        emergencyContact.CallInEmergancy = Convert.ToBoolean(sqlDataReader["CallInEmergancy"]);
                        emergencyContact.ResPhone = Convert.ToString(sqlDataReader["ResPhone"]);
                        emergencyContact.Email = Convert.ToString(sqlDataReader["Email"]);
                        emergencyContact.RelationId = Convert.ToInt32(sqlDataReader["RelationId"]);
                        emergencyContact.WorkPhone = Convert.ToString(sqlDataReader["Work Phone"]);
                        emergencyContact.Mobile = Convert.ToString(sqlDataReader["Mobile"]);
                        emergencyContact.RelationName = Convert.ToString(sqlDataReader["Relation Name"]);
                        emergencyContactList.Add(emergencyContact);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return emergencyContactList;
        }


        public DataSet GetEmergencyContactDatasSet(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, int EmployeeId)
        {
            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_uspGetEmergencyContact", sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
            da.SelectCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
            da.SelectCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
            da.SelectCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
            da.SelectCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
            SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
            TotalCount.Direction = ParameterDirection.Output;
            da.SelectCommand.Parameters.Add(TotalCount);
            DataSet ds = new DataSet();
            da.Fill(ds);
            
            return ds;

        }



        /// <summary>
        /// Insert/Update EmergencyContactModel
        /// </summary>
        /// <param name="emergencyContactModel">emergencyContactModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmergencyContact(EmergencyContactModel emergencyContactModel)
        {
            string Message = string.Empty;
            string strNationality = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_InsertUpdateEmergencyContact", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmergencyContactId", emergencyContactModel.EmergencyContactId);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", emergencyContactModel.EmployeeID == null ? 0 : emergencyContactModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ContactName", emergencyContactModel.ContactName == null ? "" : emergencyContactModel.ContactName);
                sqlCommand.Parameters.AddWithValue("@CallInEmergancy", emergencyContactModel.CallInEmergancy);

                sqlCommand.Parameters.AddWithValue("@ResPhone", emergencyContactModel.ResPhone == null ? "" : emergencyContactModel.ResPhone);
                sqlCommand.Parameters.AddWithValue("@Email", emergencyContactModel.Email == null ? "" : emergencyContactModel.Email);
                sqlCommand.Parameters.AddWithValue("@RelationId", emergencyContactModel.RelationId == null ? 0 : emergencyContactModel.RelationId);
                sqlCommand.Parameters.AddWithValue("@WorkPhone", emergencyContactModel.WorkPhone == null ? "" : emergencyContactModel.WorkPhone);
                sqlCommand.Parameters.AddWithValue("@Mobile", emergencyContactModel.Mobile == null ? "" : emergencyContactModel.Mobile);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        public EmergencyContactModel GetEmergencyContact(int ContactId)
        {
            EmergencyContactModel objEmergencyContactModel = new EmergencyContactModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_EmployeeEmergencyContactById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                
                sqlCommand.Parameters.AddWithValue("@EmergencyContactId", ContactId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        objEmergencyContactModel.EmergencyContactId = Convert.ToInt32(sqlDataReader["EmergencyContactId"].ToString());
                        objEmergencyContactModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        objEmergencyContactModel.RelationId = Convert.ToInt32(sqlDataReader["RelationId"].ToString());
                        objEmergencyContactModel.CallInEmergancy = Convert.ToBoolean(sqlDataReader["CallInEmergancy"].ToString());

                        objEmergencyContactModel.ContactName = sqlDataReader["ContactName"].ToString();
                        objEmergencyContactModel.ResPhone = sqlDataReader["ResPhone"].ToString();
                        objEmergencyContactModel.Email = sqlDataReader["Email"].ToString();
                        objEmergencyContactModel.WorkPhone = sqlDataReader["WorkPhone"].ToString();
                        objEmergencyContactModel.Mobile = sqlDataReader["Mobile"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return objEmergencyContactModel;
        }


        public OperationDetails DeleteEmergencyContactById(int ID)
        {
            try
            {
                //SqlParameter[] parameters =
                //    {    
                //      new SqlParameter("@EmergencyContactID",ID) 
                //    };
                //SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_LabourContractByID", parameters);
                //return new OperationDetails(true, "Emergency Contact deleted successfully", null);

                dataHelper = new GeneralDB.DataHelper();
                string query = "delete from HR_EmployeeEmergencyContact where EmergencyContactId=" + ID;
                if (dataHelper.SimpleCommandText(query))
                    return new OperationDetails(true, "Record deleted successfully", null);
                else
                    return new OperationDetails(false, "Technical error has occurred", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
