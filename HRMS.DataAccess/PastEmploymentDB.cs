﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data;
namespace HRMS.DataAccess
{
    public class PastEmploymentDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Add past employment details
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails AddPastEmployment(PastEmploymentModel pastEmploymentModel)
        {
            int insertedRecord = 0;
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_InsertPastEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", pastEmploymentModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CompanyName", pastEmploymentModel.CompanyName);
                sqlCommand.Parameters.AddWithValue("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(pastEmploymentModel.StartDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(pastEmploymentModel.EndDate));
                sqlCommand.Parameters.AddWithValue("@Department", pastEmploymentModel.Department);
                sqlCommand.Parameters.AddWithValue("@Position", pastEmploymentModel.Position);
                sqlCommand.Parameters.AddWithValue("@Role", pastEmploymentModel.Role);
                sqlCommand.Parameters.AddWithValue("@EmpReferences", pastEmploymentModel.EmpReferences);
                sqlCommand.Parameters.AddWithValue("@Comments", pastEmploymentModel.Comments);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", pastEmploymentModel.CreatedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();
                insertedRecord = Convert.ToInt32(OperationId.Value);


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, insertedRecord); ;
        }

        /// <summary>
        /// Update Past Employment
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails UpdatePastEmployment(PastEmploymentModel pastEmploymentModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdatePastEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PastEmploymentID", pastEmploymentModel.PastEmploymentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", pastEmploymentModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CompanyName", pastEmploymentModel.CompanyName);
                sqlCommand.Parameters.AddWithValue("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(pastEmploymentModel.StartDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(pastEmploymentModel.EndDate));
                sqlCommand.Parameters.AddWithValue("@Department", pastEmploymentModel.Department);
                sqlCommand.Parameters.AddWithValue("@Position", pastEmploymentModel.Position);
                sqlCommand.Parameters.AddWithValue("@Role", pastEmploymentModel.Role);
                sqlCommand.Parameters.AddWithValue("@EmpReferences", pastEmploymentModel.EmpReferences);
                sqlCommand.Parameters.AddWithValue("@Comments", pastEmploymentModel.Comments);
                sqlCommand.Parameters.AddWithValue("ModifiedBy", pastEmploymentModel.ModifiedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, "Success", null, pastEmploymentModel.PastEmploymentID);
        }

        /// <summary>
        /// Delete Delete PastEmployment
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails DeletePastEmployment(int PastEmploymentID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_PastEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PastEmploymentID", PastEmploymentID);
                //sqlCommand.Parameters.AddWithValue("@EmployeeID", pastEmploymentModel.EmployeeID);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, PastEmploymentID);
        }


        /// <summary>
        /// Get Past Employment Details
        /// </summary>
        /// <param name="PastEmploymentID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public PastEmploymentModel GetPastEmploymentByID(int PastEmploymentID)
        {
            PastEmploymentModel pastEmploymentModel = new PastEmploymentModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Get_PastEmploymentbyID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PastEmploymentID", PastEmploymentID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        pastEmploymentModel.PastEmploymentID = Convert.ToInt32(sqlDataReader["PastEmploymentID"].ToString());
                        pastEmploymentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        pastEmploymentModel.CompanyName = sqlDataReader["CompanyName"] == DBNull.Value ? "" : sqlDataReader["CompanyName"].ToString();
                        pastEmploymentModel.StartDate = sqlDataReader["StartDate"].ToString();
                        pastEmploymentModel.EndDate = sqlDataReader["EndDate"].ToString();
                        pastEmploymentModel.Department = sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"].ToString();
                        pastEmploymentModel.Position = sqlDataReader["Position"] == DBNull.Value ? "" : sqlDataReader["Position"].ToString();
                        pastEmploymentModel.Role = sqlDataReader["Role"] == DBNull.Value ? "" : sqlDataReader["Role"].ToString();
                        pastEmploymentModel.EmpReferences = sqlDataReader["EmpReferences"] == DBNull.Value ? "" : sqlDataReader["EmpReferences"].ToString();
                        pastEmploymentModel.Comments = sqlDataReader["Comments"] == DBNull.Value ? "" : sqlDataReader["Comments"].ToString();
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return pastEmploymentModel;
        }


        /// <summary>
        /// Get Past Employment Details by Employee ID with sorting ang paging crieteria
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="totalCount"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public List<PastEmploymentModel> GetPastEmploymentList(int EmployeeID)
        {
            List<PastEmploymentModel> PastEmploymentList = null;
            try
            {
                PastEmploymentList = new List<PastEmploymentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPastEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PastEmploymentModel pastEmploymentModel;
                    while (sqlDataReader.Read())
                    {
                        pastEmploymentModel = new PastEmploymentModel();
                        pastEmploymentModel.PastEmploymentID = Convert.ToInt32(sqlDataReader["PastEmploymentID"].ToString());
                        pastEmploymentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        pastEmploymentModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        pastEmploymentModel.StartDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        pastEmploymentModel.EndDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EndDate"].ToString());
                        pastEmploymentModel.Department = Convert.ToString(sqlDataReader["Department"]);
                        pastEmploymentModel.Position = Convert.ToString(sqlDataReader["Position"]);
                        pastEmploymentModel.Role = Convert.ToString(sqlDataReader["Role"]);
                        pastEmploymentModel.EmpReferences = Convert.ToString(sqlDataReader["EmpReferences"]);
                        pastEmploymentModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        pastEmploymentModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"]);
                        pastEmploymentModel.SurName_1 = Convert.ToString(sqlDataReader["SurName_1"]);
                        PastEmploymentList.Add(pastEmploymentModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PastEmploymentList;
        }

        /// <summary>
        /// Get Past Employment Documents by PastEmploymentID
        /// </summary>
        /// <param name="PastEmploymentID"></param>
        /// <returns></returns>
        public List<PastEmploymentModel> GetPastEmploymentDocs(int PastEmploymentID)
        {
            List<PastEmploymentModel> PastEmploymentList = null;
            try
            {
                PastEmploymentList = new List<PastEmploymentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetPastEmploymentDocs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PastEmploymentId", PastEmploymentID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PastEmploymentModel pastEmploymentModel;
                    while (sqlDataReader.Read())
                    {
                        pastEmploymentModel = new PastEmploymentModel();
                        pastEmploymentModel.AttatchmentId = Convert.ToInt32(sqlDataReader["AttatchmentID"].ToString());
                        pastEmploymentModel.PastEmploymentID = Convert.ToInt32(sqlDataReader["PastEmploymentId"].ToString());
                        pastEmploymentModel.FileName = Convert.ToString(sqlDataReader["FileName"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        pastEmploymentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        pastEmploymentModel.FileSize = Convert.ToString(sqlDataReader["FileSize"]);
                        pastEmploymentModel.FileType = Convert.ToString(sqlDataReader["FileType"]);
                        pastEmploymentModel.FilePath = Convert.ToString(sqlDataReader["FilePath"]);

                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();

                        PastEmploymentList.Add(pastEmploymentModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PastEmploymentList;
        }

        /// <summary>
        /// Insert Past Employment Docs
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails InsertPastEmploymentDocs(PastEmploymentModel pastEmploymentModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertPastEmploymentDocs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PastEmploymentID", pastEmploymentModel.PastEmploymentID);
                sqlCommand.Parameters.AddWithValue("@FileName", pastEmploymentModel.FileName);
                sqlCommand.Parameters.AddWithValue("@Description", pastEmploymentModel.Description);
                sqlCommand.Parameters.AddWithValue("@FileSize", pastEmploymentModel.FileSize);
                sqlCommand.Parameters.AddWithValue("@FileType", pastEmploymentModel.FileType);
                sqlCommand.Parameters.AddWithValue("@FilePath", pastEmploymentModel.FilePath);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", pastEmploymentModel.CreatedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, 0);
        }

        /// <summary>
        /// Delete PastE mployment Docs By PastEmploymentID
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public string DeletePastEmploymentDocs(int AttatchmentID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_DeletePastEmploymentDocs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AttatchmentID", AttatchmentID);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }



    }
}
