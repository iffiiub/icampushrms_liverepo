﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class RatingPercentageSettingDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        public DataAccess.GeneralDB.DataHelper dataHelper;

        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<RatingPercentageModel> GetRatingPercentageSetting()
        {
            List<RatingPercentageModel> ratingPercentageList = new List<RatingPercentageModel>();
            RatingPercentageModel ratingPercentageModel;
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_GetRatingPercentageSetting");
            foreach (DataRow dr in dt.Rows)
            {
                ratingPercentageModel = new RatingPercentageModel();
                ratingPercentageModel.RatingPercentageID = Convert.ToInt32(dr["RatingPercentageID"].ToString());
                ratingPercentageModel.RatingName_1 = dr["RatingName_1"].ToString();
                ratingPercentageModel.RatingName_2 = dr["RatingName_2"].ToString();
                ratingPercentageModel.RatingName_3 = dr["RatingName_3"].ToString();
                ratingPercentageModel.RatingFrom = Convert.ToDecimal(dr["RatingFrom"].ToString());
                ratingPercentageModel.RatingTo = Convert.ToDecimal(dr["RatingTo"].ToString());
                ratingPercentageList.Add(ratingPercentageModel);
            }
            return ratingPercentageList;
        }
    }
}
