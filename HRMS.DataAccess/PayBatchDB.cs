﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HRMS.DataAccess
{
    public class PayBatchDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PayBatchModel> GetAllPayBatch(int academicYearId, string mode)
        {
            List<PayBatchModel> PayBatchList = null;
            try
            {
                PayBatchList = new List<PayBatchModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_Get_PayBatch", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AcademicYearId", academicYearId);
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayBatchModel PayBatchModel;
                    while (sqlDataReader.Read())
                    {
                        PayBatchModel = new PayBatchModel();

                        PayBatchModel.PayBatchID = Convert.ToInt32(sqlDataReader["PayBatchID"].ToString());
                        PayBatchModel.active = Convert.ToBoolean(sqlDataReader["active"].ToString() == "True" ? true : false);
                        PayBatchModel.PayBatchName = sqlDataReader["PayBatchName"] == DBNull.Value ? "" : sqlDataReader["PayBatchName"].ToString();
                        PayBatchList.Add(PayBatchModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PayBatchList.OrderBy(x => x.PayBatchName).ToList();
        }
        public List<PayBatchModel> GetAllPayBatchByAcademicYear(int academicYearId)
        {
            List<PayBatchModel> PayBatchList = null;
            try
            {
                PayBatchList = new List<PayBatchModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_PayBatchByAcademicYear", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AcademicYearId", academicYearId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayBatchModel PayBatchModel;
                    while (sqlDataReader.Read())
                    {
                        PayBatchModel = new PayBatchModel();

                        PayBatchModel.PayBatchID = Convert.ToInt32(sqlDataReader["PayBatchID"].ToString());
                        PayBatchModel.PayBatchName = sqlDataReader["PayBatchName"] == DBNull.Value ? "" : sqlDataReader["PayBatchName"].ToString();
                        PayBatchList.Add(PayBatchModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PayBatchList.OrderBy(x => x.PayBatchName).ToList();
        }
        public OperationDetails InsertPayBatch(PayBatchModel PayBatchModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@PayBatchName",PayBatchModel.PayBatchName) ,
                      new SqlParameter("@acyear",PayBatchModel.acyear) ,
                      new SqlParameter("@PayBatchFrom",DataAccess.GeneralDB.CommonDB.SetCulturedDate(PayBatchModel.PayBatchFrom)) ,
                      new SqlParameter("@PayBatchTo",DataAccess.GeneralDB.CommonDB.SetCulturedDate(PayBatchModel.PayBatchTo)),
                      new SqlParameter("@active",PayBatchModel.active)
                    };
                int PayBatchId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "Hr_Stp_Add_PayBatch", parameters));
                return new OperationDetails(true, "Pay Batch saved successfully.", null, PayBatchId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting PayBatch.", exception);
                //throw;
            }
            finally
            {

            }
        }
    }
}
