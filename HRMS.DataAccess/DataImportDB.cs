﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;

namespace HRMS.DataAccess
{
    public class DataImportDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<string> GetColumnList(int DataImportTypeId)
        {
            if (DataImportTypeId == (int)DataImportModel.DataImportType.PayAddition)
            {
                return GetPayAdditionColumnList();
            }
            else if (DataImportTypeId == (int)DataImportModel.DataImportType.Insurance)
            {
                return GetInsuranceColumnList();
            }
            else if (DataImportTypeId == (int)DataImportModel.DataImportType.Accommodation)
            {
                return GetAccommodationColumnList();
            }
            else if (DataImportTypeId == (int)DataImportModel.DataImportType.Airfare)
            {
                return GetAirfareColumnList();
            }
            return null;
        }
        public List<string> GetPayAdditionColumnList()
        {
            List<string> ColumnsList = new List<string>();
            ColumnsList.Add("Employee ID");
            ColumnsList.Add("Pay Salary Allowance Name (EN)");
            ColumnsList.Add("Addition Date");
            ColumnsList.Add("Amount");
            ColumnsList.Add("Description");
            return ColumnsList;
        }

        public List<string> GetInsuranceColumnList()
        {
            List<string> ColumnsList = new List<string>();
            ColumnsList.Add("Employee ID");
            ColumnsList.Add("Insurance Category");
            ColumnsList.Add("Insurance Eligibility");
            ColumnsList.Add("Card Number");
            ColumnsList.Add("Policy Number");
            ColumnsList.Add("Insurance Amount");
            ColumnsList.Add("Amount Paid By School");
            ColumnsList.Add("Expiry Date");
            ColumnsList.Add("Issue Date");
            ColumnsList.Add("Cancellation Date");
            ColumnsList.Add("Note");
            return ColumnsList;
        }

        public List<string> GetAccommodationColumnList()
        {
            List<string> ColumnsList = new List<string>();
            ColumnsList.Add("Employee ID");
            ColumnsList.Add("Type");
            ColumnsList.Add("Apartment No");
            ColumnsList.Add("Building");
            ColumnsList.Add("Floor");
            ColumnsList.Add("Street");
            ColumnsList.Add("Country");
            ColumnsList.Add("City");
            return ColumnsList;
        }

        public List<string> GetAirfareColumnList()
        {
            List<string> ColumnsList = new List<string>();
            ColumnsList.Add("Employee ID");
            ColumnsList.Add("Air Ticket Country");
            ColumnsList.Add("Air Ticket City");
            ColumnsList.Add("Airport");
            ColumnsList.Add("Air Ticket Amount");
            ColumnsList.Add("Air Ticket Frequency");
            ColumnsList.Add("Air Ticket Class");
            ColumnsList.Add("No Of Tickets For Dependants");
            ColumnsList.Add("Air Ticket Amount For Dependants");
            ColumnsList.Add("Ticket Class For Dependants");
            return ColumnsList;
        }

        public List<DataImportModel> GetDataImportTypeList()
        {
            List<DataImportModel> objDataImportList = new List<DataImportModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetDataImportTypeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objDataImportList.Add(new DataImportModel()
                        {
                            ImportTypeID = Convert.ToInt32(sqlDataReader["ImportTypeID"].ToString()),
                            ImportTypeName = Convert.ToString(sqlDataReader["ImportTypeName"])
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objDataImportList;
        }

        public string GetDataImportFileTemplatePath(int ImportTypeID)
        {
            string FileTemplatePath = string.Empty;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetDataImportFileTemplatePath", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ImportTypeID", ImportTypeID);
                FileTemplatePath = Convert.ToString(sqlCommand.ExecuteScalar());
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return FileTemplatePath;
        }
        public DataTable ReadExcel(string fileName, string fileExt, out string ErrorMessage)
        {
            ErrorMessage = "";
            string connectionString = string.Empty;
            DataTable DtExcel = new DataTable();
            try
            {
                if (fileExt.CompareTo(".xls") == 0)
                    connectionString = string.Format("provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HRD=Yes;IMEX=1';", fileName); //for below excel 2007  
                else
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=NO';", fileName); //for above excel 2007  
                using (OleDbConnection con = new OleDbConnection(connectionString))
                {
                    con.Open();
                    string SheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" }).Rows[0].Field<string>("TABLE_NAME");
                    OleDbDataAdapter oleAdpt = new OleDbDataAdapter("select * from [" + SheetName + "]", con); //here we read data from sheet1  
                    oleAdpt.Fill(DtExcel); //fill excel data into dataTable  
                    if (con != null)
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
            DtExcel = DtExcel.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull || string.IsNullOrWhiteSpace(field as string))).CopyToDataTable(); // remove empty rows 
            return DtExcel;
        }


        public List<EmployeeDetailsModel> GetEmployeeIdsRelatedAlternativeIds(string EmployeeAlternativeIDs)
        {
            List<EmployeeDetailsModel> objEmployeeDetailsModelList = new List<EmployeeDetailsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeIdsRelatedAlternativeIds", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeAlternativeIDs", EmployeeAlternativeIDs);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmployeeDetailsModelList.Add(new EmployeeDetailsModel() { EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString()), EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objEmployeeDetailsModelList;
        }

        public OperationDetails SavePreviewDataImport(int ImportTypeID, string ImportFileName, string ImportFilePath, int CreatedBy, DateTime CreatedOn)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePreviewDataImport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ImportTypeID", ImportTypeID);
                sqlCommand.Parameters.AddWithValue("@ImportFileName", ImportFileName);
                sqlCommand.Parameters.AddWithValue("@ImportFilePath", ImportFilePath);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CreatedOn", CreatedOn);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveImportDataPayAddition(List<PayAddition> PayAdditionList, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("PaySalaryAllowanceID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("EmployeeAlternativeID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AdditionDate", System.Type.GetType("System.DateTime"));
                resultTable.Columns.Add("amount", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("Description", System.Type.GetType("System.String"));
                resultTable.Columns.Add("IsActive", System.Type.GetType("System.Boolean"));
                resultTable.Columns.Add("TransactionDate", System.Type.GetType("System.DateTime"));
                resultTable.Columns.Add("AcademicYearID", System.Type.GetType("System.Int32"));

                foreach (PayAddition pay in PayAdditionList)
                {
                    resultTable.Rows.Add(
                                            pay.PaySalaryAllowanceID,
                                            pay.EmployeeID,
                                            pay.AdditionDate,
                                            pay.Amount,
                                            pay.Description,
                                            0,
                                            null,
                                            null
                                        );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveImportDataPayAddition", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MultipleAllowance", resultTable);
                sqlCommand.Parameters.AddWithValue("@DataImportID", DataImportID);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveImportDataInsurance(List<InsuranceModel> insurances, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("InsuranceCategory", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("InsuranceEligibility", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("CardNumber", System.Type.GetType("System.String"));
                resultTable.Columns.Add("PolicyNumber", System.Type.GetType("System.String"));
                resultTable.Columns.Add("InsuranceAmount", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AmountPaidBySchool", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("EmployeeAlternativeID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("ExpiryDate", System.Type.GetType("System.DateTime"));
                resultTable.Columns.Add("IssueDate", System.Type.GetType("System.DateTime"));
                resultTable.Columns.Add("CancellationDate", System.Type.GetType("System.DateTime"));
                resultTable.Columns.Add("Note", System.Type.GetType("System.String"));

                foreach (InsuranceModel insurance in insurances)
                {
                    resultTable.Rows.Add(
                                            insurance.InsuranceType,
                                            insurance.InsuranceEligibility,
                                            insurance.CardNumber,
                                            insurance.PolicyNumber,
                                            insurance.TotalAmount,
                                            insurance.TotalAmountBySchool,
                                            insurance.EmployeeID,
                                            insurance.ExpiryDate,
                                            insurance.IssueDate,
                                            insurance.CancellationDate,
                                            insurance.Note
                                        );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveImportDataInsurance", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Hr_DataImportInsuranceTableType", resultTable);
                sqlCommand.Parameters.AddWithValue("@DataImportID", DataImportID);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveImportDataAccommodation(List<AllowanceModel> accommodationList, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("HRAccommodationTypeID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("Appartment", System.Type.GetType("System.String"));
                resultTable.Columns.Add("Building", System.Type.GetType("System.String"));
                resultTable.Columns.Add("BuildingFloor", System.Type.GetType("System.String"));
                resultTable.Columns.Add("Street", System.Type.GetType("System.String"));
                resultTable.Columns.Add("CountryID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("CityID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("EmployeeAlternativeID", System.Type.GetType("System.Int32"));

                foreach (AllowanceModel allowance in accommodationList)
                {
                    resultTable.Rows.Add(
                                           allowance.HRAccommodationTypeID,
                                           allowance.Appartment,
                                           allowance.Building,
                                           allowance.BuildingFloor,
                                           allowance.Street,
                                           allowance.CountryID,
                                           allowance.CityID,
                                           allowance.EmployeeID
                                        );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveImportDataAccommodation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Hr_DataImportAccommodationTableType", resultTable);
                sqlCommand.Parameters.AddWithValue("@DataImportID", DataImportID);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveImportDataAirafare(List<EmployeeAirFare> employeeAirFares, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                
                resultTable.Columns.Add("AirTicketCountryId", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirTicketCityId", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirTicketClassId", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirTicketAmount", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("NoOfDependants", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirTicketDependantClassId", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirTicketDependantAmount", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("EmployeeAirfareID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirfareFrequencyID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("AirportListID", System.Type.GetType("System.Int32"));

                foreach (EmployeeAirFare airFare in employeeAirFares)
                {
                    resultTable.Rows.Add(
                                            airFare.AirTicketCountryID,
                                            airFare.AirTicketCityID,
                                            airFare.AirTicketClassID,
                                            airFare.AirTicketAmount,
                                            airFare.NoOfDependencies,
                                            airFare.AirTicketClassIDForDependant,
                                            airFare.AirTicketAmountDependannt,
                                            airFare.EmployeeAirfareID,
                                            airFare.AirfareFrequencyID,
                                            airFare.AirportListID
                                        );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveImportDataAirfare", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Hr_DataImportAirfareTableType", resultTable);
                sqlCommand.Parameters.AddWithValue("@DataImportID", DataImportID);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
    }
}
