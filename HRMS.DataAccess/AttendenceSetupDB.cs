﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data;


namespace HRMS.DataAccess
{
    public class AttendenceSetupDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public AttendanceSetupModel GetLateDeductionMinutesAndPecentage()
        {
            AttendanceSetupModel attendanceSetupModel = new AttendanceSetupModel();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetLateDeductionMinutes", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        attendanceSetupModel = new AttendanceSetupModel();

                        attendanceSetupModel.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        attendanceSetupModel.DeductionPercent = Convert.ToDouble(sqlDataReader["DeductionPercent"]);

                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return attendanceSetupModel;
        }

        /// <summary>
        /// Update Attendence Setup 
        /// </summary>
        /// <param name="attendanceSetupModel"></param>
        /// <returns></returns>
        public OperationDetails UpdateAttendanceSetup(AttendanceSetupModel attendanceSetupModel)
        {
            string Message = "";
            int rowCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspUpdateAttendanceSetup", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntLateMinutes", attendanceSetupModel.LateMinutes);
                sqlCommand.Parameters.AddWithValue("@aNumDeductionPercent", attendanceSetupModel.DeductionPercent);



                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                rowCount = Convert.ToInt32(OperationId.Value);
                Message = "success";

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, rowCount);
        }


        public MachineSetup GetMachinSettings()
        {
            MachineSetup machineSetup = new MachineSetup();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select TableName from HR_AttMechineSetup", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {                      
                        machineSetup.TableName  = sqlDataReader["TableName"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return machineSetup;
        }

        public AttPostingSetting GetAttPostingSetting()
        {
            AttPostingSetting attPostingSetting = new AttPostingSetting();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetAttPostingSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        attPostingSetting.IsAutoPostingDailyAttendance = Convert.ToBoolean(sqlDataReader["IsAutoPostingDailyAttendance"] == DBNull.Value ? "0" : sqlDataReader["IsAutoPostingDailyAttendance"].ToString());
                        attPostingSetting.LateAndDidntSignOutPosting = Convert.ToInt32(sqlDataReader["LateAndDidntSignOutPosting"] == DBNull.Value ? "0" : sqlDataReader["LateAndDidntSignOutPosting"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return attPostingSetting;
        }

    }
}
