﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class ShiftDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        DataAccess.GeneralDB.DataHelper dataHelper;
        public OperationDetails InsertShift(ShiftModel ShiftModel, int mode)
        {

            try
            {

                //SqlParameter[] parameters =
                //    {

                //    new SqlParameter("@ShiftID",ShiftModel.ShiftID) ,
                //    new SqlParameter("@ShiftName", ShiftModel.ShiftName) ,
                //    new SqlParameter("@CompanyId", ShiftModel.CompanyId),
                //    new SqlParameter("@IncludeInAttendance", ShiftModel.IncludeInAttendance),
                //    new SqlParameter("@mode", mode),
                //     };

                //SqlParameter TotalCount = new SqlParameter("@UpdatedID", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //da.SelectCommand.Parameters.Add(TotalCount);

                //int ShiftID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                //                    "HR_stp_Add_Shift", parameters));

                //return new OperationDetails(true, "Shift saved successfully.", null, ShiftID);

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_Add_Shift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ShiftID", ShiftModel.ShiftID);
                sqlCommand.Parameters.AddWithValue("@ShiftName", ShiftModel.ShiftName);
                sqlCommand.Parameters.AddWithValue("@CompanyId", ShiftModel.CompanyId);                
                sqlCommand.Parameters.AddWithValue("@IncludeInAttendance", ShiftModel.IncludeInAttendance);
                sqlCommand.Parameters.AddWithValue("@mode", mode);


                SqlParameter AffectedID = new SqlParameter("@UpdatedID", SqlDbType.Int);
                AffectedID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(AffectedID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                return new OperationDetails(true, "Shift saved successfully.", null, Convert.ToInt32(AffectedID.Value.ToString()));
            }


            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Shift .", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertShiftDetails(ShiftModel ShiftModel, int mode)
        {

            try
            {
                if (ShiftModel.ShiftStart == null)
                {
                    ShiftModel.ShiftStart = "00:00:00";
                }
                if (ShiftModel.ShiftEnd == null)
                {
                    ShiftModel.ShiftEnd = "00:00:00";
                }
                SqlParameter[] parameter =
                    {
                     new SqlParameter("@ShiftId", ShiftModel.ShiftID) ,
                     new SqlParameter("@ShiftDay", ShiftModel.ShiftDay) ,
                     new SqlParameter("@ShiftStart", ShiftModel.ShiftStart),
                     new SqlParameter("@ShiftEnd", ShiftModel.ShiftEnd ) ,
                     new SqlParameter("@ShiftGraceIn",ShiftModel.ShiftGraceIn ) ,
                     new SqlParameter("@ShiftGraceOut", ShiftModel.ShiftGraceOut ),
                     new SqlParameter("@ShiftBreakHours",ShiftModel.ShiftBreakHours) ,
                     new SqlParameter("@AutoSync", ShiftModel.AutoSync) ,
                     new SqlParameter("@CutOffTime", ShiftModel.CutOffTime),
                     new SqlParameter("@IsWeekend", ShiftModel.IsWeekend),
                     new SqlParameter("@mode", mode),
                     new SqlParameter("@shiftDetailsId", ShiftModel.ShiftDetailsId),
                     new SqlParameter("@isSpecialShift", ShiftModel.isSpecialShift)

                                       };
                int ShiftDetailsID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                       "stp_Add_HR_ShiftDetails", parameter));


                return new OperationDetails(true, "Shift saved successfully.", null, ShiftDetailsID);

            }


            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Shift .", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertSpecialShiftDetails(ShiftModel ShiftModel,int ShiftId,DataTable dt)
        {
            try
            {
                SqlParameter[] parameter =
                    {
                     new SqlParameter("@ShiftId", ShiftId),                     
                     new SqlParameter("@StartDate",DateTime.ParseExact(ShiftModel.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)),
                     new SqlParameter("@EndDate",DateTime.ParseExact(ShiftModel.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)),
                     new SqlParameter("@HR_MultipleSpecialShiftDetails",dt)
                                       };

                int ShiftDetailsID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                       "Hr_Stp_AddSpecialShiftDetails", parameter));
                return new OperationDetails(true, "Special Shift saved successfully.", null, ShiftDetailsID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Shift .", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateShiftDetails(int ShiftId, int ShiftIdToUpdate)
        {
            try
            {
                SqlParameter[] parameter =
                    {
                     new SqlParameter("@ShiftId", ShiftId),                     
                     new SqlParameter("@ShiftIdToUpdate",ShiftIdToUpdate)
                                       };

                int ShiftDetailsID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                       "Hr_Stp_UpdateShiftDetailsAsPerExisting", parameter));
                return new OperationDetails(true, "Shift saved successfully.", null, ShiftDetailsID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Shift .", exception);
                //throw;
            }
            finally
            {

            }
        }

        //public OperationDetails UpdateHealth(HealthModel HealthModel)
        //{
        //    try
        //    {
        //        SqlParameter[] parameters =
        //            {    
        //                new SqlParameter("@DocHealthId",HealthModel.DocHealthId),
        //              new SqlParameter("@DocumentNo",HealthModel.DocumentNo) ,
        //              new SqlParameter("@IssueCountry", HealthModel.IssueCountry) ,
        //              new SqlParameter("@IssuePlace", HealthModel.IssuePlace) ,
        //              new SqlParameter("@IssueDate", HealthModel.IssueDate),
        //              new SqlParameter("@ExpiryDate", HealthModel.ExpiryDate ) ,
        //              new SqlParameter("@Note",HealthModel.Note ) ,
        //              new SqlParameter("@IsPrimary", HealthModel.IsPrimary )


        //            };
        //        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
        //                           "stp_Update_HR_Health", parameters);
        //        return new OperationDetails(true, "Health Document   updated successfully", null);
        //    }
        //    catch (Exception exception)
        //    {
        //        return new OperationDetails(false, "Error : while updating Health Document.", exception);
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        public List<ShiftModel> GetAllShiftNames(int CompanyId)
        {
            try
            {
                SqlParameter[] parameters =
                    {


                    new SqlParameter("@CompanyId", CompanyId)


                    };
                List<ShiftModel> ShiftModelList = new List<ShiftModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetAllShifts", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        ShiftModel ShiftModel;
                        while (reader.Read())
                        {
                            ShiftModel = new ShiftModel();


                            ShiftModel.ShiftID = Convert.ToInt32(reader["Shiftid"]);
                            ShiftModel.ShiftName = Convert.ToString(reader["Shift"]);
                            ShiftModelList.Add(ShiftModel);
                        }
                    }
                }
                return ShiftModelList.OrderBy(x => x.ShiftName).ToList();
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }

        }

        public List<Employee> GetAllEmployeesInShift(int ShiftID,int UserId)
        {
            try
            {
                List<Employee> EmployeeModelList = new List<Employee>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@shiftID",ShiftID),
                         new SqlParameter("@UserId",UserId)

                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GETEmployeeAssignedInShift", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        Employee EmployeeModel;
                        while (reader.Read())
                        {
                            EmployeeModel = new Employee();

                            EmployeeModel.EmployeeId = Convert.ToInt32(reader["EmployeeID"]);
                            EmployeeModel.FirstName = Convert.ToString(reader["FirstName_1"]);

                            EmployeeModel.LastName = Convert.ToString(reader["SurName_1"]);
                            EmployeeModel.FullName = Convert.ToString(reader["FullName"]);
                            EmployeeModelList.Add(EmployeeModel);
                        }
                    }
                }
                return EmployeeModelList.Where(x => x.EmployeeId != 0).ToList();
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<Employee> GetAllEmployeesNotInShift(int ShiftID,int UserId)
        {
            try
            {
                List<Employee> EmployeeModelList = new List<Employee>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@shiftID",ShiftID),
                        new SqlParameter("@UserId",UserId)

                    };


                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GETEmployeeNotAssignedInShift", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        Employee EmployeeModel;
                        while (reader.Read())
                        {
                            EmployeeModel = new Employee();

                            EmployeeModel.EmployeeId = Convert.ToInt32(reader["EmployeeID"]);
                            EmployeeModel.FirstName = Convert.ToString(reader["FirstName_1"]);

                            EmployeeModel.LastName = Convert.ToString(reader["SurName_1"]);
                            EmployeeModel.ShiftName = Convert.ToString(reader["ShiftName"]);
                            EmployeeModel.FullName = Convert.ToString(reader["FullName"]);
                            if (reader["ShiftID"] != DBNull.Value)
                            {
                                EmployeeModel.ShiftId = Convert.ToInt32(reader["ShiftID"]);
                            }

                            EmployeeModelList.Add(EmployeeModel);
                        }
                    }
                }
                return EmployeeModelList.Where(x => x.EmployeeId != 0).ToList();
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }


        }

        public List<ShiftModel> GetShiftById(int ShiftID)
        {
            List<ShiftModel> objshiflist = new List<ShiftModel>();
            try
            {

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@ShiftID",ShiftID),

                    };


                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_ShiftByID", parameters))
                {

                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            ShiftModel objShiftModel = new ShiftModel();
                            objShiftModel.ShiftID = ShiftID;
                            objShiftModel.ShiftName = Convert.ToString(reader["ShiftName"]);
                            objShiftModel.ShiftDay = Convert.ToInt32(reader["ShiftDay"] == DBNull.Value ? "0" : reader["ShiftDay"]);
                            objShiftModel.ShiftStart = Convert.ToString(reader["ShiftStart"]);
                            objShiftModel.ShiftEnd = Convert.ToString(reader["ShiftEnd"]);
                            objShiftModel.ShiftGraceIn = Convert.ToString(reader["ShiftGraceIn"]);
                            objShiftModel.ShiftGraceOut = Convert.ToString(reader["ShiftGraceOut"]);
                            objShiftModel.ShiftBreakHours = Convert.ToString(reader["ShiftBreakHours"]);
                            objShiftModel.AutoSync = Convert.ToBoolean(reader["AutoSync"] == DBNull.Value ? 0 : reader["AutoSync"]);
                            objShiftModel.CutOffTime = Convert.ToString(reader["CutOffTime"]);

                            objshiflist.Add(objShiftModel);
                        }
                    }
                }
                return objshiflist;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }


        }

        public List<ShiftModel> GetShiftByName(string ShiftName)
        {
            try
            {
                List<ShiftModel> objShiftModelList = new List<ShiftModel>();

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@ShiftName",ShiftName),

                    };


                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_ShiftByName", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            ShiftModel objShiftModel = new ShiftModel();
                            objShiftModel.ShiftName = Convert.ToString(reader["ShiftName"]);
                            objShiftModel.ShiftDay = Convert.ToInt32(reader["ShiftDay"]);
                            objShiftModel.ShiftStart = Convert.ToString(reader["ShiftStart"]);
                            objShiftModel.ShiftEnd = Convert.ToString(reader["ShiftEnd"]);
                            objShiftModel.ShiftGraceIn = Convert.ToString(reader["ShiftGraceIn"]);
                            objShiftModel.ShiftGraceOut = Convert.ToString(reader["ShiftGraceOut"]);
                            objShiftModel.ShiftBreakHours = Convert.ToString(reader["ShiftBreakHours"]);
                            objShiftModel.IncludeInAttendance = Convert.ToBoolean(reader["IncludeInAttendance"]);
                            objShiftModel.AutoSync = Convert.ToBoolean(reader["AutoSync"]);
                            objShiftModel.CutOffTime = Convert.ToString(reader["CutOffTime"]);

                            objShiftModelList.Add(objShiftModel);

                        }
                    }
                }
                return objShiftModelList;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }


        }

        public List<ShiftModel> GetShiftDetails(int ShiftID)
        {
            try
            {
                List<ShiftModel> objShiftModelList = new List<ShiftModel>();

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@ShiftID",ShiftID),

                    };


                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_ShiftByID", parameters))
                {
                    // Check if the reader returned any rows
                    DataAccess.GeneralDB.CommonDB commonDb = new GeneralDB.CommonDB();
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            ShiftModel objShiftModel = new ShiftModel();
                            objShiftModel.ShiftID = Convert.ToInt32(reader["ShiftID"]);
                            objShiftModel.ShiftName = Convert.ToString(reader["ShiftName"] == DBNull.Value ? "" : Convert.ToString(reader["ShiftName"]));
                            objShiftModel.IncludeInAttendance = Convert.ToBoolean(reader["IncludeInAttendance"] == DBNull.Value ? 0 : reader["IncludeInAttendance"]);
                            objShiftModel.ShiftDetailsId = Convert.ToInt32(reader["ShiftDetailsId"]);
                            objShiftModel.ShiftDay = Convert.ToInt32(reader["ShiftDay"] == DBNull.Value ? 0 : reader["ShiftDay"]);
                            objShiftModel.ShiftStart = Convert.ToString(reader["ShiftStart"] == DBNull.Value ? "" : reader["ShiftStart"]);
                            objShiftModel.ShiftEnd = Convert.ToString(reader["ShiftEnd"] == DBNull.Value ? "" : reader["ShiftEnd"]);
                            objShiftModel.ShiftGraceIn = Convert.ToString(reader["ShiftGraceIn"] == DBNull.Value ? "" : reader["ShiftGraceIn"]);
                            objShiftModel.ShiftGraceOut = Convert.ToString(reader["ShiftGraceOut"] == DBNull.Value ? "" : reader["ShiftGraceOut"]);
                            objShiftModel.ShiftBreakHours = Convert.ToString(reader["ShiftBreakHours"] == DBNull.Value ? "" : reader["ShiftBreakHours"]);
                            objShiftModel.AutoSync = Convert.ToBoolean(reader["AutoSync"] == DBNull.Value ? 0 : reader["AutoSync"]);
                            objShiftModel.CutOffTime = Convert.ToString(reader["CutOffTime"] == DBNull.Value ? "" : reader["CutOffTime"]);
                            objShiftModel.IsWeekend = Convert.ToBoolean(reader["IsWeekend"] == DBNull.Value ? 0 : reader["IsWeekend"]);
                            objShiftModel.shiftDayName = commonDb.weekDays.Where(x => x.Value == objShiftModel.ShiftDay).FirstOrDefault().Key;
                            objShiftModelList.Add(objShiftModel);

                        }
                    }
                }
                return objShiftModelList;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }


        }

        public List<ShiftModel> GetSpecialShiftDetails(int ShiftID)
        {
            try
            {
                List<ShiftModel> objShiftModelList = new List<ShiftModel>();

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@ShiftID",ShiftID),

                    };


                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetSpecialShiftDetails", parameters))
                {
                    // Check if the reader returned any rows
                    DataAccess.GeneralDB.CommonDB commonDb = new GeneralDB.CommonDB();
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            ShiftModel objShiftModel = new ShiftModel();
                            objShiftModel.ShiftID = Convert.ToInt32(reader["ShiftID"]);
                            objShiftModel.ShiftName = Convert.ToString(reader["ShiftName"] == DBNull.Value ? "" : Convert.ToString(reader["ShiftName"]));
                            objShiftModel.IncludeInAttendance = Convert.ToBoolean(reader["IncludeInAttendance"] == DBNull.Value ? 0 : reader["IncludeInAttendance"]);
                            objShiftModel.ShiftDetailsId = Convert.ToInt32(reader["ShiftDetailsId"]);
                            objShiftModel.ShiftDay = Convert.ToInt32(reader["ShiftDay"] == DBNull.Value ? 0 : reader["ShiftDay"]);
                            objShiftModel.ShiftStart = Convert.ToString(reader["ShiftStart"] == DBNull.Value ? "" : reader["ShiftStart"]);
                            objShiftModel.ShiftEnd = Convert.ToString(reader["ShiftEnd"] == DBNull.Value ? "" : reader["ShiftEnd"]);
                            objShiftModel.ShiftGraceIn = Convert.ToString(reader["ShiftGraceIn"] == DBNull.Value ? "" : reader["ShiftGraceIn"]);
                            objShiftModel.ShiftGraceOut = Convert.ToString(reader["ShiftGraceOut"] == DBNull.Value ? "" : reader["ShiftGraceOut"]);
                            objShiftModel.ShiftBreakHours = Convert.ToString(reader["ShiftBreakHours"] == DBNull.Value ? "" : reader["ShiftBreakHours"]);
                            objShiftModel.AutoSync = Convert.ToBoolean(reader["AutoSync"] == DBNull.Value ? 0 : reader["AutoSync"]);
                            objShiftModel.CutOffTime = Convert.ToString(reader["CutOffTime"] == DBNull.Value ? "" : reader["CutOffTime"]);
                            objShiftModel.IsWeekend = Convert.ToBoolean(reader["IsWeekend"] == DBNull.Value ? 0 : reader["IsWeekend"]);
                            objShiftModel.shiftDayName = commonDb.weekDays.Where(x => x.Value == objShiftModel.ShiftDay).FirstOrDefault().Key;
                            objShiftModel.isSpecialShift = Convert.ToBoolean(reader["SpecialShift"] == DBNull.Value ? 0 : reader["SpecialShift"]);
                            objShiftModel.StartDate = reader["StartDate"].ToString() == "" ? " " : Convert.ToDateTime(reader["StartDate"].ToString()).ToString("dd/MM/yyyy");
                            objShiftModel.EndDate = reader["EndDate"].ToString() == "" ? " " : Convert.ToDateTime(reader["EndDate"].ToString()).ToString("dd/MM/yyyy");
                            objShiftModel.ShiftSpecialID = Convert.ToInt32(reader["ShiftSpecialID"] == DBNull.Value ? 0 : reader["ShiftSpecialID"]);
                            objShiftModelList.Add(objShiftModel);

                        }
                    }
                    else {
                        for (int i = 0; i <= 6; i++)
                        {
                            ShiftModel objShiftModel = new ShiftModel();
                            objShiftModel.ShiftID = -1;
                            objShiftModel.ShiftName = "";
                            objShiftModel.IncludeInAttendance = true;
                            objShiftModel.ShiftDetailsId = -1;
                            objShiftModel.ShiftDay = i;
                            objShiftModel.ShiftStart ="";
                            objShiftModel.ShiftEnd = "";
                            objShiftModel.ShiftGraceIn ="";
                            objShiftModel.ShiftGraceOut = "";
                            objShiftModel.ShiftBreakHours = "";
                            objShiftModel.AutoSync = true;
                            objShiftModel.CutOffTime = "";
                            objShiftModel.IsWeekend = false;
                            objShiftModel.shiftDayName = commonDb.weekDays.Where(x => x.Value == i).FirstOrDefault().Key;
                            objShiftModel.isSpecialShift = true;
                            objShiftModel.StartDate = "";
                            objShiftModel.EndDate = "";
                            objShiftModel.ShiftSpecialID = -1;
                            objShiftModelList.Add(objShiftModel);
                        }
                        
                    }
                }
                return objShiftModelList;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }


        }

        public OperationDetails DeleteShiftById(int ShiftID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@ShiftID",ShiftID)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_ShiftID", parameters);
                return new OperationDetails(true, "Shift deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Shift.", exception);
                throw;
            }
            finally
            {

            }
        }
        //}

        //public OperationDetails DeleteHealthById(int HealthId)
        //{
        //    try
        //    {
        //        SqlParameter[] parameters =
        //            {    
        //              new SqlParameter("@DocHealthId",HealthId) 
        //            };
        //        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_HEalth", parameters);
        //        return new OperationDetails(true, "Health Document deleted successfully", null);

        //    }
        //    catch (Exception exception)
        //    {
        //        return new OperationDetails(false, "Error : while deleting Health Document.", exception);
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        //public List<HealthModel> GetHealthListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder, out int totalCount)
        //{
        //    int TotalCount = 0;
        //    SqlParameter[] parameters =
        //            {    
        //                new SqlParameter("@OffsetRows",offset),
        //              new SqlParameter("@FetchRows",rowCount),
        //              new SqlParameter("@SortColumn",sortColumn),
        //              new SqlParameter("@SortOrder",sortOrder)
        //            };
        //    // Create a list to hold the LabourContract		
        //    List<HealthModel> HealthModelList = new List<HealthModel>();
        //    try
        //    {
        //        using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_HealthWithPaging", parameters))
        //        {
        //            // Check if the reader returned any rows
        //            if (reader.HasRows)
        //            {
        //                // While the reader has rows we loop through them,
        //                // create new Position, and insert them into our list
        //                HealthModel HealthModel;
        //                while (reader.Read())
        //                {
        //                    HealthModel = new HealthModel();
        //                    TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
        //                    HealthModel.DocHealthId = Convert.ToInt32(reader["DocHealthId"]);
        //                    HealthModel.DocumentNo = reader["DocumentNo"].ToString();
        //                    HealthModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
        //                    HealthModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
        //                    HealthModel.IssueDate = Convert.ToDateTime(reader["IssueDate"].ToString());
        //                    HealthModel.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"].ToString());
        //                    HealthModel.Note = reader["Note"].ToString();
        //                    HealthModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());

        //                    HealthModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());

        //                    HealthModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
        //                    HealthModelList.Add(HealthModel);
        //                }
        //            }
        //        }
        //        // Finally, we return our list of HealthModelList
        //        totalCount = TotalCount;
        //        return HealthModelList;
        //    }
        //    catch (Exception exception)
        //    {
        //        // return new OperationDetails(false, "Error : while fetching LabourContract List.", exception);
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        /// <summary>
        /// Insert Employee Shift
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
     
        public OperationDetails InsertEmployeeShift(int shiftId, string EmployeeIDs)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_Remove_HR_Shift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                
                sqlCommand.Parameters.AddWithValue("@ShiftID", shiftId);
                sqlCommand.Parameters.AddWithValue("@EmployeeIDs", EmployeeIDs);
                sqlCommand.Parameters.AddWithValue("@OperationType", "I");       
                SqlParameter output = new SqlParameter("@OperationMessage", SqlDbType.NVarChar,400);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = Convert.ToString(output.Value);
                sqlConnection.Close();
                if (Message == "Success")
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Employee added to shift Successfully.";
                    operationDetails.CssClass = "success";
                }
                else {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while adding Employee to shift.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }


        /// <summary>
        /// Remove Employee From Shift
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
       

        public OperationDetails RemoveEmployeeShift(int shiftId, string EmployeeIDs)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_Remove_HR_Shift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShiftID", shiftId);
                sqlCommand.Parameters.AddWithValue("@EmployeeIDs", EmployeeIDs);
                sqlCommand.Parameters.AddWithValue("@OperationType", "R");
                SqlParameter output = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 400);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = Convert.ToString(output.Value);
                sqlConnection.Close();
                if (Message == "Success")
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Employee unassigned from shift Successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while unassigning Employee from shift.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }
        public OperationDetails UpdateAtt_ShiftTableByUser(int shiftId, int EmployeeId, int shiftDay, int mode, string shiftStart, string shiftEnd, string graceIn, string graceOut, string cutOffTime, string shiftBreakHours, bool isWeekEnd, string shiftName)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            DataAccess.GeneralDB.CommonDB commonDb = new GeneralDB.CommonDB();
            OperationDetails operationDetails;
            string Message = "";
            try
            {
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@shiftId", shiftId) ,
                    new SqlParameter("@date", date) ,
                    new SqlParameter("@employeeId",EmployeeId),
                    new SqlParameter("@ShiftDay",commonDb.weekDays.Where(x => x.Value == shiftDay).FirstOrDefault().Key),
                    new SqlParameter("@mode",mode) ,
                    new SqlParameter("@ShiftStart", shiftStart),
                    new SqlParameter("@ShiftEnd", shiftEnd),
                    new SqlParameter("@GraceIn",  graceIn),
                    new SqlParameter("@GraceOut", graceOut),
                    new SqlParameter("@CutOffTime", cutOffTime),
                    new SqlParameter("@ShiftBreakHours", shiftBreakHours),
                    new SqlParameter("@IsWeekEnd", isWeekEnd),
                    new SqlParameter("@shiftName", shiftName),
                    };
                SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "Hr_Stp_UpdateShiftDetails_AttShiftTable", parameters);

                //UpdateEmployeeShift_Att_ShiftTableByUser(shiftId, EmployeeID);
                operationDetails = new OperationDetails(true, "Record updated successfully", null, 0);

            }
            catch (Exception exception)
            {
                operationDetails = new OperationDetails(false, "Technical error has occurred", null, 0);
            }
            finally
            {

            }
            return operationDetails;
        }

        public List<ShiftModel> GetShiftDetailsByEmployee(int employeeId)
        {
            List<ShiftModel> shiftDetails = new List<ShiftModel>();
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeId", employeeId)
                    };
                ShiftModel shiftModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetShiftDetailForEmployee", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            shiftModel = new ShiftModel();
                            shiftModel.ShiftID = Convert.ToInt32(reader["ShiftID"]);
                            shiftModel.ShiftName = Convert.ToString(reader["ShiftName"]);
                            shiftModel.ShiftStart = Convert.ToString(reader["ShiftStart"]);
                            shiftModel.ShiftEnd = Convert.ToString(reader["ShiftEnd"]);
                            shiftModel.CutOffTime = Convert.ToString(reader["CutOffTime"]);
                            shiftModel.IncludeInAttendance = Convert.ToBoolean(reader["IncludeInAttendance"]);
                            shiftModel.IsWeekend = Convert.ToBoolean(reader["IsWeekend"]);
                            shiftModel.ShiftDay = Convert.ToInt32(reader["ShiftDay"]);
                            shiftModel.ShiftGraceIn = Convert.ToString(reader["ShiftGraceIn"].ToString() == "" ? "0" : reader["ShiftGraceIn"]);
                            shiftModel.ShiftGraceOut = Convert.ToString(reader["ShiftGraceOut"].ToString() == "" ? "0" : reader["ShiftGraceOut"]);
                            shiftDetails.Add(shiftModel);
                        }
                    }
                }
                return shiftDetails;
            }
            catch
            {
                return shiftDetails;
            }
        }

        public List<AttShiftTableModel> GetFuturRecordAttShiftTableData(int ShiftId, string EmpId, bool IsUserShiftUpdated)
        {
            List<AttShiftTableModel> attShiftTableModelList;
            using (sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetFuturRecordFromAttShiftTableByUser", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShiftId", ShiftId);
                sqlCommand.Parameters.AddWithValue("@IsUserShiftUpdated", IsUserShiftUpdated);
                sqlCommand.Parameters.AddWithValue("@EmpIds", EmpId);
                sqlCommand.CommandTimeout = 600;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    attShiftTableModelList = new List<AttShiftTableModel>();
                    while (sqlDataReader.Read())
                    {
                        AttShiftTableModel attShiftTableModel;
                        attShiftTableModel = new AttShiftTableModel();
                        attShiftTableModel.UserShiftId = Convert.ToInt32(sqlDataReader["UserShiftId"]);
                        attShiftTableModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        attShiftTableModel.Day = Convert.ToString(sqlDataReader["AttDay"]);
                        attShiftTableModel.AttDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                        attShiftTableModel.ShiftId = Convert.ToInt32(sqlDataReader["ShiftID"]);
                        attShiftTableModel.InTime = Convert.ToString(sqlDataReader["InTime"]);
                        attShiftTableModel.OutTime = Convert.ToString(sqlDataReader["OutTime"]);
                        attShiftTableModel.GreaceIn = Convert.ToInt32(sqlDataReader["GraceIn"]);
                        attShiftTableModel.GreaceOut = Convert.ToInt32(sqlDataReader["GraceOut"]);
                        attShiftTableModel.OffDays = Convert.ToBoolean(sqlDataReader["OffDay"] == DBNull.Value ? "false" : sqlDataReader["OffDay"]);
                        attShiftTableModel.Posted = Convert.ToBoolean(sqlDataReader["Posted"]);
                        attShiftTableModel.IsUserShiftUpdated = Convert.ToBoolean(sqlDataReader["IsUserShiftUpdated"]);
                        attShiftTableModelList.Add(attShiftTableModel);
                    }
                }
                else
                {
                    attShiftTableModelList = new List<AttShiftTableModel>();
                }

            }
            return attShiftTableModelList;
        }

        public List<AttShiftTableModel> GetAttShiftTableData(int EmpId, string FromDate, string ToDate,int UserId)
        {
            List<AttShiftTableModel> attShiftTableModelList;
            
            using (sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
               
                sqlCommand = new SqlCommand("HR_CURD_AttShiftTableByUser", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmpId", EmpId);
                sqlCommand.Parameters.AddWithValue("@FromDate", CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@Mode", 1);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandTimeout = 600;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    attShiftTableModelList = new List<AttShiftTableModel>();
                    while (sqlDataReader.Read())
                    {
                        AttShiftTableModel attShiftTableModel;
                        attShiftTableModel = new AttShiftTableModel();
                        attShiftTableModel.UserShiftId = Convert.ToInt32(sqlDataReader["UserShiftId"]);
                        attShiftTableModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        attShiftTableModel.Day = Convert.ToString(sqlDataReader["AttDay"]);
                        attShiftTableModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString()); 
                        attShiftTableModel.ShiftId = Convert.ToInt32(sqlDataReader["ShiftID"]);
                        attShiftTableModel.InTime = Convert.ToString(sqlDataReader["InTime"]);
                        attShiftTableModel.OutTime = Convert.ToString(sqlDataReader["OutTime"]);
                        attShiftTableModel.ShiftName = Convert.ToString(sqlDataReader["ShiftName"]);
                        attShiftTableModel.GreaceIn = sqlDataReader["GraceIn"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["GraceIn"]);
                        attShiftTableModel.GreaceOut = sqlDataReader["GraceOut"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["GraceOut"]);
                        attShiftTableModel.OffDays = Convert.ToBoolean(sqlDataReader["OffDay"] == DBNull.Value ? "false" : sqlDataReader["OffDay"]);
                        attShiftTableModel.Posted = Convert.ToBoolean(sqlDataReader["Posted"] == DBNull.Value ? "false" : sqlDataReader["Posted"]);
                        attShiftTableModel.IsUserShiftUpdated = Convert.ToBoolean(sqlDataReader["IsUserShiftUpdated"] == DBNull.Value ? "false" : sqlDataReader["IsUserShiftUpdated"]);
                        attShiftTableModelList.Add(attShiftTableModel);
                    }
                }
                else
                {
                    attShiftTableModelList = new List<AttShiftTableModel>();
                }

            }
            return attShiftTableModelList;
        }

        public AttShiftTableModel GetAttShiftTableDataById(int UsershiftId)
        {
            AttShiftTableModel attShiftTableModel = new AttShiftTableModel();
            List<AttShiftTableModel> attShiftTableModelList;
            using (sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetAttShiftTableByUserById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserShiftId", UsershiftId);
                sqlCommand.CommandTimeout = 600;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    attShiftTableModelList = new List<AttShiftTableModel>();
                    while (sqlDataReader.Read())
                    {
                        attShiftTableModel.UserShiftId = Convert.ToInt32(sqlDataReader["UserShiftId"]);
                        attShiftTableModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        attShiftTableModel.Day = Convert.ToString(sqlDataReader["AttDay"]);
                        attShiftTableModel.AttDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                        attShiftTableModel.ShiftId = Convert.ToInt32(sqlDataReader["ShiftID"]);
                        attShiftTableModel.InTime = Convert.ToString(sqlDataReader["InTime"]);
                        attShiftTableModel.OutTime = Convert.ToString(sqlDataReader["OutTime"]);
                        attShiftTableModel.ShiftName = Convert.ToString(sqlDataReader["ShiftName"]);
                        attShiftTableModel.GreaceIn = sqlDataReader["GraceIn"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["GraceIn"]);
                        attShiftTableModel.GreaceOut = sqlDataReader["GraceOut"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["GraceOut"]);
                        attShiftTableModel.OffDays = Convert.ToBoolean(sqlDataReader["OffDay"] == DBNull.Value ? "false" : sqlDataReader["OffDay"]);
                        attShiftTableModel.Posted = Convert.ToBoolean(sqlDataReader["Posted"] == DBNull.Value ? "false" : sqlDataReader["Posted"]);
                        attShiftTableModel.IsUserShiftUpdated = Convert.ToBoolean(sqlDataReader["IsUserShiftUpdated"] == DBNull.Value ? "false" : sqlDataReader["IsUserShiftUpdated"]);
                    }
                }
                else
                {

                }
            }
            return attShiftTableModel;
        }

        public bool UpadateAttShiftTableData(AttShiftTableModel attShiftTableModel)
        {
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> sqlParameterList = new List<SqlParameter>();
            sqlParameterList.Add(new SqlParameter("@UserShiftId", attShiftTableModel.UserShiftId));
            sqlParameterList.Add(new SqlParameter("@InTime", attShiftTableModel.InTime));
            sqlParameterList.Add(new SqlParameter("@OutTime", attShiftTableModel.OutTime));
            sqlParameterList.Add(new SqlParameter("@GraceIn", attShiftTableModel.GreaceIn));
            sqlParameterList.Add(new SqlParameter("@GraceOut", attShiftTableModel.GreaceOut));           
            sqlParameterList.Add(new SqlParameter("@Mode", 2));
            return dataHelper.ExcutestoredProcedure(sqlParameterList, "HR_CURD_AttShiftTableByUser");
        }

        public OperationDetails UpdatMultipleUserShift(List<AttShiftTableModel> attShiftList,int mode)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("UserShiftId", typeof(Int32));
            dt.Columns.Add("ShiftId", typeof(Int32));
            dt.Columns.Add("AttDate", typeof(DateTime));
            dt.Columns.Add("EmployeeId", typeof(Int32));
            dt.Columns.Add("InTime", typeof(string));
            dt.Columns.Add("OutTime", typeof(string));
            dt.Columns.Add("GreaceIn", typeof(Int32));
            dt.Columns.Add("GreaceOut", typeof(Int32));
            foreach (var item in attShiftList)
            {
                dt.Rows.Add(item.UserShiftId, item.ShiftId, item.AttDate, item.EmployeeId, item.InTime, item.OutTime, item.GreaceIn, item.GreaceOut);
            }
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@MultipleUserShift", dt));
            paramList.Add(new SqlParameter("@mode", mode));
            if (dataHelper.ExcutestoredProcedure(paramList, "Hr_Stp_UpdateAttShiftTabel"))
            {
                return new OperationDetails() { Message = "User shift updated successfully", Success = true };
            }
            else
            {
                return new OperationDetails() { Message = "Technical error has occurred", Success = false };
            }
        }

        public List<EmployeeSectionModel> GetAllSectionInShift(int ShiftID,int UserId)
        {
            try
            {
                List<EmployeeSectionModel> SectionShiftModel = new List<EmployeeSectionModel>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@shiftID",ShiftID),
                         new SqlParameter("@UserId",UserId)
                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetSectionsInShift", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        EmployeeSectionModel objShiftSection;
                        while (reader.Read())
                        {
                            objShiftSection = new EmployeeSectionModel();
                            objShiftSection.shiftid = Convert.ToInt32(reader["ShiftID"]);
                            objShiftSection.EmployeeSectionID = Convert.ToInt32(reader["EmployeeSectionID"]);
                            objShiftSection.EmployeeSectionName_1 = Convert.ToString(reader["EmployeeSectionName"]);
                            SectionShiftModel.Add(objShiftSection);
                        }
                    }
                }
                return SectionShiftModel;
            }
            catch (Exception exception)
            {                
                throw;
            }
            finally
            {

            }
        }

        public List<EmployeeSectionModel> GetAllSectionNotInShift(int ShiftID,int UserId)
        {
            try
            {
                List<EmployeeSectionModel> SectionShiftModel = new List<EmployeeSectionModel>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@shiftID",ShiftID),
                        new SqlParameter("@UserId",UserId)
                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetSectionsNotInShift", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        EmployeeSectionModel objShiftSection;
                        while (reader.Read())
                        {
                            objShiftSection = new EmployeeSectionModel();
                            objShiftSection.EmployeeSectionID = Convert.ToInt32(reader["EmployeeSectionID"]);                          
                            objShiftSection.EmployeeSectionName_1 = Convert.ToString(reader["EmployeeSectionName_1"]);
                            objShiftSection.ShiftName= Convert.ToString(reader["ShiftName"]);
                            SectionShiftModel.Add(objShiftSection);
                        }
                    }
                }
                return SectionShiftModel;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertSectionsToShift(int shiftId, string SectionIds)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Add_RemoveSectionsShift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShiftID", shiftId);
                sqlCommand.Parameters.AddWithValue("@SectionIds", SectionIds);
                sqlCommand.Parameters.AddWithValue("@OperationType", "I");
                SqlParameter output = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 400);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = Convert.ToString(output.Value);
                sqlConnection.Close();
                if (Message == "Success")
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Section added to shift Successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while adding Section to shift.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public List<Employee> GetEmployeeBySectionsIds(string SectionIDs)
        {
            try
            {
                List<Employee> EmpIds = new List<Employee>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@SectionIDs",SectionIDs),
                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetEmployeeBySection", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        Employee objEmployeeBySec;
                       
                        while (reader.Read())
                        {
                            objEmployeeBySec = new Employee();
                            objEmployeeBySec.EmployeeId = Convert.ToInt32(reader["EmployeeID"]);
                            objEmployeeBySec.FirstName= Convert.ToString(reader["FirstName_1"]);
                            objEmployeeBySec.LastName = Convert.ToString(reader["SurName_1"]);
                            objEmployeeBySec.ShiftName= Convert.ToString(reader["ShiftName"]);
                            objEmployeeBySec.employmentInformation.EmployeeSectionName= Convert.ToString(reader["EmployeeSectionName_1"]);
                            EmpIds.Add(objEmployeeBySec);
                        }
                    }
                }
                return EmpIds;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails RemoveSectionsFromShift(int shiftId, string SectionIds)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Add_RemoveSectionsShift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShiftID", shiftId);
                sqlCommand.Parameters.AddWithValue("@SectionIds", SectionIds);
                sqlCommand.Parameters.AddWithValue("@OperationType", "R");
                SqlParameter output = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 400);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = Convert.ToString(output.Value);
                sqlConnection.Close();
                if (Message == "Success")
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Section unassigned from shift Successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while unassigning Section from shift.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public List<EmployeeSectionModel> GetAllDepartmentInShift(int ShiftID, int UserId)
        {
            try
            {
                List<EmployeeSectionModel> SectionShiftModel = new List<EmployeeSectionModel>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@shiftID",ShiftID),
                        new SqlParameter("@UserId",UserId)

                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetDepartmentsInShift", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        EmployeeSectionModel objShiftSection;
                        while (reader.Read())
                        {
                            objShiftSection = new EmployeeSectionModel();
                            objShiftSection.shiftid = Convert.ToInt32(reader["ShiftID"]);
                            objShiftSection.DepartmentID = Convert.ToInt32(reader["DepartmentID"]);
                            objShiftSection.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            SectionShiftModel.Add(objShiftSection);
                        }
                    }
                }
                return SectionShiftModel;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<EmployeeSectionModel> GetAllDepartmentNotInShift(int ShiftID,int UserId)
        {
            try
            {
                List<EmployeeSectionModel> SectionShiftModel = new List<EmployeeSectionModel>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@shiftID",ShiftID),
                         new SqlParameter("@UserId",UserId)
                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetDepartmentsNotInShift", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        EmployeeSectionModel objShiftSection;
                        while (reader.Read())
                        {
                            objShiftSection = new EmployeeSectionModel();
                            objShiftSection.DepartmentID = Convert.ToInt32(reader["DepartmentID"]);
                            objShiftSection.DepartmentName = Convert.ToString(reader["DepartmentName_1"]);
                            objShiftSection.ShiftName = Convert.ToString(reader["ShiftName"]);
                            SectionShiftModel.Add(objShiftSection);
                        }
                    }
                }
                return SectionShiftModel;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertDepartmentssToShift(int shiftId, string SectionIds)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Add_RemoveDepartmentsShift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShiftID", shiftId);
                sqlCommand.Parameters.AddWithValue("@DeptIds", SectionIds);
                sqlCommand.Parameters.AddWithValue("@OperationType", "I");
                SqlParameter output = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 400);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = Convert.ToString(output.Value);
                sqlConnection.Close();
                if (Message == "Success")
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Department added to shift Successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while adding Section to shift.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public List<Employee> GetEmployeeByDeptIds(string DeptIds)
        {
            try
            {
                List<Employee> EmpIds = new List<Employee>();
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@DeptIDs",DeptIds),
                    };

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetEmployeeByDepartment", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        Employee objEmployeeBySec;

                        while (reader.Read())
                        {
                            objEmployeeBySec = new Employee();
                            objEmployeeBySec.EmployeeId = Convert.ToInt32(reader["EmployeeID"]);
                            objEmployeeBySec.FirstName = Convert.ToString(reader["FirstName_1"]);
                            objEmployeeBySec.LastName = Convert.ToString(reader["SurName_1"]);
                            objEmployeeBySec.ShiftName = Convert.ToString(reader["ShiftName"]);
                            objEmployeeBySec.employmentInformation.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            EmpIds.Add(objEmployeeBySec);
                        }
                    }
                }
                return EmpIds;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails RemoveDepartmentsFromShift(int shiftId, string DeptIds)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Add_RemoveDepartmentsShift", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShiftID", shiftId);
                sqlCommand.Parameters.AddWithValue("@DeptIDs", DeptIds);
                sqlCommand.Parameters.AddWithValue("@OperationType", "R");
                SqlParameter output = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 400);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = Convert.ToString(output.Value);
                sqlConnection.Close();
                if (Message == "Success")
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Department unassigned from shift Successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while unassigning department from shift.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }
    }
}
