﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class PersonRelationDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PersonRelation> GetAllPersonRelationList()
        {
            List<PersonRelation> personRelationList = null;
            try
            {
                personRelationList = new List<PersonRelation>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_GetAll_PersonRelation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PersonRelation personRelation;
                    while (sqlDataReader.Read())
                    {
                        personRelation = new PersonRelation();

                        personRelation.RelationId = Convert.ToInt32(sqlDataReader["RelationId"].ToString());
                        personRelation.RelationName = sqlDataReader["RelationName"] == DBNull.Value ? "" : sqlDataReader["RelationName"].ToString();
                        personRelationList.Add(personRelation);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return personRelationList;
        }
    }
}
