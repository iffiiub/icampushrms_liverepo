﻿using HRMS.DataAccess.GeneralDB;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HRMS.DataAccess.PDRP
{

    public class DropInsModelDB : PDRPCommonModelDB
    {
        public List<DropIns> GetAllDropInsList(int TeacherID, int DropInYear)
        {
            List<DropIns> DropInsList = null;
            try
            {
                DropInsList = new List<DropIns>();

                SqlParameter[] sqlParameters = new SqlParameter[2];
                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@DropInYear";
                sqlParameter.Value = DropInYear;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@TeacherID";
                sqlParameter.Value = TeacherID;
                sqlParameters[1] = sqlParameter;

                SqlDataReader sqlDataReader = ExecuteProcedure("HR_stp_SelectAllPDRPDropIns", sqlParameters);


                if (sqlDataReader.HasRows)
                {
                    DropIns DropIns;
                    while (sqlDataReader.Read())
                    {
                        DropIns = new DropIns();


                        DropIns.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? 0 : sqlDataReader["ID"]);
                        DropIns.TeacherID = Convert.ToInt32(sqlDataReader["TeacherID"] == DBNull.Value ? 0 : sqlDataReader["TeacherID"]);
                        DropIns.LessonTopic = Convert.ToString(sqlDataReader["LessonTopic"] == DBNull.Value ? "" : sqlDataReader["LessonTopic"]);
                        //  DropIns.VisitDATE = Convert.ToDateTime(sqlDataReader["VisitDATE"] == DBNull.Value ? "" : sqlDataReader["VisitDATE"]);
                        DropIns.VisitDATE = !string.IsNullOrEmpty(Convert.ToString(sqlDataReader["VisitDATE"])) ? Convert.ToDateTime(sqlDataReader["VisitDATE"]) : (DateTime?)null;
                        DropIns.DropInYear = Convert.ToString(sqlDataReader["DropInYear"] == DBNull.Value ? "" : sqlDataReader["DropInYear"]);
                        DropIns.DropInYearID = Convert.ToInt32(sqlDataReader["DropInYearID"] == DBNull.Value ? 0 : sqlDataReader["DropInYearID"]);
                        DropIns.Total = Convert.ToInt32(sqlDataReader["Total"] == DBNull.Value ? 0 : sqlDataReader["Total"]);

                        DropIns.OverAllScore = Convert.ToDecimal(sqlDataReader["OverAllScore"] == DBNull.Value ? 0 : sqlDataReader["OverAllScore"]);
                        //*** F4
                        DropIns.isEmployeeAcknowledgedDropIn = Convert.ToInt32(sqlDataReader["EmployeeAcknowledgedDropIn"] == DBNull.Value ? 0 : sqlDataReader["EmployeeAcknowledgedDropIn"]);
                        DropIns.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? 0 : sqlDataReader["RequestID"]);

                        DropInsList.Add(DropIns);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DropInsList.ToList();
        }
        public List<DropIns> GetTop3DropInsList(int TeacherID, int DropInYear, int? CompanyID = null)
        {
            List<DropIns> DropInsList = null;
            try
            {
                DropInsList = new List<DropIns>();
                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("@DropInYear",DropInYear),
                    new SqlParameter("@TeacherID",TeacherID),
                    new SqlParameter("@CompanyID",CompanyID == null ? (object)DBNull.Value : CompanyID)
                };
                SqlDataReader sqlDataReader = ExecuteProcedure("HR_stp_SelectTop3PDRPDropIns", sqlParameters);
                if (sqlDataReader.HasRows)
                {
                    DropIns DropIns;
                    while (sqlDataReader.Read())
                    {
                        DropIns = new DropIns();

                        DropIns.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? 0 : sqlDataReader["ID"]);
                        DropIns.TeacherID = Convert.ToInt32(sqlDataReader["TeacherID"] == DBNull.Value ? 0 : sqlDataReader["TeacherID"]);
                        DropIns.LessonTopic = Convert.ToString(sqlDataReader["LessonTopic"] == DBNull.Value ? "" : sqlDataReader["LessonTopic"]);
                        //  DropIns.VisitDATE = Convert.ToDateTime(sqlDataReader["VisitDATE"] == DBNull.Value ? "" : sqlDataReader["VisitDATE"]);
                        DropIns.VisitDATE = !string.IsNullOrEmpty(Convert.ToString(sqlDataReader["VisitDATE"])) ? Convert.ToDateTime(sqlDataReader["VisitDATE"]) : (DateTime?)null;
                        DropIns.DropInYear = Convert.ToString(sqlDataReader["DropInYear"] == DBNull.Value ? "" : sqlDataReader["DropInYear"]);
                        DropIns.DropInYearID = Convert.ToInt32(sqlDataReader["DropInYearID"] == DBNull.Value ? 0 : sqlDataReader["DropInYearID"]);
                        DropIns.Total = Convert.ToInt32(sqlDataReader["Total"] == DBNull.Value ? 0 : sqlDataReader["Total"]);
                        DropIns.OverAllScore = Convert.ToDecimal(sqlDataReader["OverAllScore"] == DBNull.Value ? 0 : sqlDataReader["OverAllScore"]);

                        //*** F4
                        DropIns.isEmployeeAcknowledgedDropIn = Convert.ToInt32(sqlDataReader["EmployeeAcknowledgedDropIn"] == DBNull.Value ? 0 : sqlDataReader["EmployeeAcknowledgedDropIn"]);
                        DropIns.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? 0 : sqlDataReader["RequestID"]);
                        DropInsList.Add(DropIns);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DropInsList.ToList();
        }

        public List<PDRPTeachersModel> IsLM(int DropInsYear, List<PDRPTeachersModel> objDropInsTeachersList, int EmployeeID, int? SupervisorId, string MyDropIn, int? CompanyID, int userId)
        {
            int IsLM = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT COUNT(*) IsLM FROM Hr_EmploymentInformation WHERE SuperviserID=" + EmployeeID.ToString(), sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        IsLM = Convert.ToInt32(reader["IsLM"] == DBNull.Value ? 0 : reader["IsLM"]);
                    }
                }

                if (IsLM > 0)
                {
                    //*** Naresh 2020-03-17 Admin DropIns
                    if (MyDropIn == "Admin")
                    {
                        objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, SupervisorId, CompanyID, MyDropIn, userId));
                    }
                    else if (MyDropIn != "Yes")
                    {
                        objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, null, EmployeeID, null, MyDropIn, userId));
                        objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null, null, MyDropIn, userId));
                    }
                    else
                    {
                        objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null, null, MyDropIn, userId));
                    }

                }
                else
                {
                    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null, null, MyDropIn, userId));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objDropInsTeachersList;
        }
        public DropInsModel GetDropInsEmployeeInformation(DropInsModel DropInsModel, string language)
        {
            //DropInEmployeeModel objRequesterInfoViewModel = new DropInEmployeeModel();

            DropInsModel.DropInsRatingScaleWeightageDetails = new List<DropInsRatingScaleWeightageDetails>();

            DropInsRatingScaleWeightageDetails objDropInsRatingScaleWeightageDetails;
            //PDRPDropInProficiencyLevel objPDRPDropInProficiencyLevel;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@EmployeeID";
                sqlParameter.Value = DropInsModel.DropInEmployeeModel.TeacherID;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@DropInsYear";
                sqlParameter.Value = DropInsModel.DropInEmployeeModel.DropinYearID;
                sqlParameters[1] = sqlParameter;


                SqlDataReader reader = ExecuteProcedure("Hr_Stp_GetEmployeeInfoforPDRP", sqlParameters);


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        DropInsModel.DropInEmployeeModel.RequestID = reader["RequestID"].ToString();
                        DropInsModel.DropInEmployeeModel.TeacherID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? 0 : reader["EmployeeID"]);
                        DropInsModel.DropInEmployeeModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                        //DropInsModel.DropInEmployeeModel.RequesterAlternativeID = Convert.ToString(reader["EmployeeAlternativeID"]);

                        DropInsModel.DropInEmployeeModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? 0 : reader["PositionID"]);
                        DropInsModel.DropInEmployeeModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                        DropInsModel.DropInEmployeeModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? 0 : reader["DepartmentID"]);

                        DropInsModel.DropInEmployeeModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);

                        DropInsModel.DropInEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);

                        DropInsModel.DropInEmployeeModel.DropinYear = Convert.ToString(reader["DropinYear"]);
                        DropInsModel.DropInEmployeeModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);
                        if (language == "en-gb")
                        {
                            DropInsModel.DropInEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                            DropInsModel.DropInEmployeeModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            DropInsModel.DropInEmployeeModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                            DropInsModel.DropInEmployeeModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        }
                        else if (language == "ar")
                        {
                            DropInsModel.DropInEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeNameAr"]);
                            if (string.IsNullOrWhiteSpace(DropInsModel.DropInEmployeeModel.TeacherName))
                                DropInsModel.DropInEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);

                            DropInsModel.DropInEmployeeModel.DepartmentName = Convert.ToString(reader["DepartmentNameAr"]);
                            if (string.IsNullOrWhiteSpace(DropInsModel.DropInEmployeeModel.DepartmentName))
                                DropInsModel.DropInEmployeeModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);

                            DropInsModel.DropInEmployeeModel.SuperviserName = Convert.ToString(reader["SuperviserNameAr"]);
                            if (string.IsNullOrWhiteSpace(DropInsModel.DropInEmployeeModel.SuperviserName))
                                DropInsModel.DropInEmployeeModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);

                            DropInsModel.DropInEmployeeModel.CompanyName = Convert.ToString(reader["CompanyNameAr"]);
                            if (string.IsNullOrWhiteSpace(DropInsModel.DropInEmployeeModel.SuperviserName))
                                DropInsModel.DropInEmployeeModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        }
                    }
                }

                reader.NextResult();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objDropInsRatingScaleWeightageDetails = new DropInsRatingScaleWeightageDetails();

                        objDropInsRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        objDropInsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        objDropInsRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                        objDropInsRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                        objDropInsRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);

                        DropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails.Add(objDropInsRatingScaleWeightageDetails);
                    }
                }

                reader.NextResult();
                reader.NextResult();
                reader.NextResult();
                reader.NextResult();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        DropInsModel.PreviousTargets = Convert.ToString(reader["PreviousTargets"]);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return DropInsModel;
        }
        public DropInsModel GetDropInsEmployeeCurrCompany(DropInsModel DropInsModel)
        {
            try
            {
                DropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails = new List<DropInsRatingScaleWeightageDetails>();

                DropInsRatingScaleWeightageDetails objDropInsRatingScaleWeightageDetails;
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@PDRPDropInsID",DropInsModel.DropInEmployeeModel.ID),
                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPDropIns", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {


                        //*** F4
                        DropInsModel.EmpCurrentCompanyID = Convert.ToInt32(reader["EmpCurrentCompanyID"] == DBNull.Value ? 0 : reader["EmpCurrentCompanyID"]);
                        //Dropin Created Company
                        DropInsModel.DropInEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        DropInsModel.DropInEmployeeModel.RequestID = Convert.ToString(reader["RequestID"]);
                     
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return DropInsModel;
        }
        public DropInsModel GetDropInsEmployeeDetails(DropInsModel DropInsModel)
        {

            try
            {
                DropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails = new List<DropInsRatingScaleWeightageDetails>();

                DropInsRatingScaleWeightageDetails objDropInsRatingScaleWeightageDetails;
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@PDRPDropInsID",DropInsModel.DropInEmployeeModel.ID),
                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPDropIns", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        DropInsModel.DropInEmployeeModel.TempTeacherID = Convert.ToInt32(reader["TeacherID"] == DBNull.Value ? 0 : reader["TeacherID"]);
                        DropInsModel.DropInEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                        //DropInsModel.DropInEmployeeModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? 0 : reader["PositionID"]);
                        DropInsModel.DropInEmployeeModel.PositionTitle = Convert.ToString(reader["Designation"]);
                        //DropInsModel.DropInEmployeeModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? 0 : reader["DepartmentID"]);
                        DropInsModel.DropInEmployeeModel.DepartmentName = Convert.ToString(reader["Department"]);
                        //DropInsModel.DropInEmployeeModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);
                        DropInsModel.DropInEmployeeModel.SuperviserName = Convert.ToString(reader["LineManager"]);
                        //DropInsModel.DropInEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        DropInsModel.DropInEmployeeModel.CompanyName = Convert.ToString(reader["School"]);
                        //DropInsModel.DropInEmployeeModel.DropinYear = Convert.ToString(reader["DropinYear"]);
                        //DropInsModel.DropInEmployeeModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);
                        DropInsModel.DropInEmployeeModel.VisitDATE = !string.IsNullOrEmpty(Convert.ToString(reader["VisitDATE"])) ? Convert.ToDateTime(reader["VisitDATE"]) : (DateTime?)null;
                        DropInsModel.DropInEmployeeModel.VisitTime = !string.IsNullOrEmpty(Convert.ToString(reader["VisitTime"])) ? Convert.ToDateTime(reader["VisitTime"]).ToString("hh:mm tt") : string.Empty;
                        //  DropInsModel.DropInEmployeeModel.VisitTime = !string.IsNullOrEmpty(Convert.ToString(reader["VisitDATE"])) ? (Convert.ToDateTime(reader["VisitDATE"]).TimeOfDay.Ticks == 0 ? string.Empty : Convert.ToDateTime(reader["VisitDATE"]).ToString("hh:mm tt")) : string.Empty;
                        DropInsModel.DropInEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        DropInsModel.DropInEmployeeModel.ReqStatusID = Convert.ToInt32(reader["ReqStatusID"] == DBNull.Value ? 0 : reader["ReqStatusID"]);
                        DropInsModel.DropInEmployeeModel.LessonTopic = Convert.ToString(reader["LessonTopic"]);
                        DropInsModel.DropInEmployeeModel.StudentsOnRollMale = Convert.ToInt32(reader["StudentsOnRollMale"] == DBNull.Value ? 0 : reader["StudentsOnRollMale"]);
                        DropInsModel.DropInEmployeeModel.StudentsOnRollFemale = Convert.ToInt32(reader["StudentsOnRollFemale"] == DBNull.Value ? 0 : reader["StudentsOnRollFemale"]);
                        //DropInsModel.DropInEmployeeModel.VisitDATE = Convert.ToDateTime(reader["VisitDATE"]);
                        ////DropInsModel.DropInEmployeeModel.VisitDATE = Convert.ToString(reader["VisitDATE"]);
                        //DropInsModel.DropInEmployeeModel.VisitTime = Convert.ToDateTime(reader["VisitDATE"]).ToString("hh:mm tt");

                        DropInsModel.DropInEmployeeModel.DropinYearID = Convert.ToInt32(reader["DropinYearID"] == DBNull.Value ? 0 : reader["DropinYearID"]);
                        DropInsModel.DropInEmployeeModel.DropinYear = Convert.ToString(reader["DropinYear"]);
                        DropInsModel.DropInEmployeeModel.Branch = Convert.ToString(reader["Branch"]);
                        DropInsModel.DropInEmployeeModel.GradeYear = Convert.ToString(reader["GradeYear"]);
                        DropInsModel.DropInEmployeeModel.GroupSection = Convert.ToString(reader["GroupSection"]);
                        DropInsModel.DropInEmployeeModel.StudentsPresentMale = Convert.ToInt32(reader["StudentsPresentMale"] == DBNull.Value ? 0 : reader["StudentsPresentMale"]);
                        DropInsModel.DropInEmployeeModel.StudentsPresentFemale = Convert.ToInt32(reader["StudentsPresentFemale"] == DBNull.Value ? 0 : reader["StudentsPresentFemale"]);
                        DropInsModel.PreviousTargets = Convert.ToString(reader["PreviousTargets"]);
                        DropInsModel.Outline = Convert.ToString(reader["Outline"]);
                        DropInsModel.Feedback = Convert.ToString(reader["Feedback"]);
                        if (reader["Targets"] != DBNull.Value)
                            DropInsModel.Targets = Convert.ToInt32(reader["Targets"]);
                        DropInsModel.EmployeeAcknowledgedDropIn = Convert.ToBoolean(reader["EmployeeAcknowledgedDropIn"]);
                        DropInsModel.ProficiencyScoreTotal = Convert.ToInt32(reader["Total"] == DBNull.Value ? 0 : reader["Total"]);
                        DropInsModel.ProficiencyScoreOverall = Convert.ToDecimal(reader["OverAllScore"] == DBNull.Value ? 0 : reader["OverAllScore"]);
                        //*** F4
                        DropInsModel.EmpCurrentCompanyID = Convert.ToInt32(reader["EmpCurrentCompanyID"] == DBNull.Value ? 0 : reader["EmpCurrentCompanyID"]);
                        //Nithin 06-05-2020
                        DropInsModel.DropInEmployeeModel.RequestID = Convert.ToString(reader["RequestID"]);

                    }
                }
                reader.NextResult();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objDropInsRatingScaleWeightageDetails = new DropInsRatingScaleWeightageDetails();

                        objDropInsRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        objDropInsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        objDropInsRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                        objDropInsRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                        objDropInsRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                        objDropInsRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);


                        DropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails.Add(objDropInsRatingScaleWeightageDetails);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return DropInsModel;
        }
        public DropInsModel GetDropInProficiencyLevel(DropInsModel objDropInsModel)
        {
            //DropInManageModel objDropInsModel = new DropInManageModel();


            objDropInsModel.DropInProficiencyLevel = new List<DropInProficiencyLevel>();
            objDropInsModel.DropInsRatingScaleWeightages = new List<DropInsRatingScaleWeightages>();
            DropInProficiencyLevel objPDRPDropInProficiencyLevel;
            DropInsRatingScaleWeightages objPDRPDropInsRatingScaleWeightages;
            try
            {
                string strProcedure = "";
                SqlParameter[] sqlParameters = new SqlParameter[1];
                SqlParameter sqlParameter = new SqlParameter();




                if (objDropInsModel.isAddMode == "1") //*** New Dropin
                {
                    strProcedure = "Hr_Stp_GetPDRPDropInProficiencyLevel";

                }
                else if (objDropInsModel.isAddMode == "2") //*** Edit Dropin
                {
                    strProcedure = "Hr_Stp_GetEmployeePDRPDropInProficiencies";
                    sqlParameter.ParameterName = "@PDRPDropInsID";
                    sqlParameter.Value = objDropInsModel.DropInEmployeeModel.ID;
                    sqlParameters[0] = sqlParameter;
                }

                SqlDataReader reader = objDropInsModel.isAddMode == "2" ? ExecuteProcedure(strProcedure, sqlParameters) : ExecuteProcedure(strProcedure);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string ComID = Convert.ToString(reader["CompanyIDs"] == DBNull.Value ? "" : reader["CompanyIDs"]);
                        if (objDropInsModel.isAddMode == "2") //*** Edit Dropin
                        {
                            ComID = "0";
                        }
                        if (objDropInsModel.DropInEmployeeModel.CompanyID.ToString() == ComID || ComID == "0")
                        {
                            objPDRPDropInProficiencyLevel = new DropInProficiencyLevel();


                            objPDRPDropInProficiencyLevel.ProficiencyID = Convert.ToInt32(reader["ProficiencyID"]);
                            if (Thread.CurrentThread.CurrentUICulture.Name == "ar")
                            {
                                objPDRPDropInProficiencyLevel.ProficiencyDetail = Convert.ToString(reader["ProficiencyDetail2"]);
                            }
                            else
                            {
                                objPDRPDropInProficiencyLevel.ProficiencyDetail = Convert.ToString(reader["ProficiencyDetail"]);
                            }

                            objPDRPDropInProficiencyLevel.ProficiencyGroupID = Convert.ToInt32(reader["ProficiencyGroupID"] == DBNull.Value ? 0 : reader["ProficiencyGroupID"]);
                            if (Thread.CurrentThread.CurrentUICulture.Name == "ar")
                            {
                                objPDRPDropInProficiencyLevel.ProficiencyGroupName = Convert.ToString(reader["ProficiencyGroupName2"]);
                            }
                            else
                            {
                                objPDRPDropInProficiencyLevel.ProficiencyGroupName = Convert.ToString(reader["ProficiencyGroupName"]);
                            }

                            //*** F6
                            objPDRPDropInProficiencyLevel.CompanyIDs = Convert.ToString(reader["CompanyIDs"] == DBNull.Value ? "" : reader["CompanyIDs"]);
                            objPDRPDropInProficiencyLevel.IsRadioOption = Convert.ToInt32(reader["IsRadioOption"] == DBNull.Value ? 0 : reader["IsRadioOption"]);
                            if (objDropInsModel.isAddMode == "2")
                            {
                                //*** F6
                                if (objPDRPDropInProficiencyLevel.IsRadioOption == 1)
                                {
                                    objPDRPDropInProficiencyLevel.ProficiencyScore = Convert.ToString(reader["ProficiencyScore"] == DBNull.Value ? "" : reader["ProficiencyScore"].ToString());
                                }
                                else
                                {
                                    //*** F5
                                    objPDRPDropInProficiencyLevel.ProficiencyScore = Convert.ToString(reader["ProficiencyScore"] == DBNull.Value ? "" : (reader["ProficiencyScore"].ToString() == "0" ? "N/A" : reader["ProficiencyScore"].ToString()));
                                }

                                objPDRPDropInProficiencyLevel.ProficiencyFormula = Convert.ToString(reader["ProficiencyFormula"]);
                                if (objDropInsModel.ProficiencyScoreFormula == null || objDropInsModel.ProficiencyScoreFormula == "")
                                {
                                    objDropInsModel.ProficiencyScoreFormula = Convert.ToString(objPDRPDropInProficiencyLevel.ProficiencyFormula);
                                }
                            }


                            objDropInsModel.DropInProficiencyLevel.Add(objPDRPDropInProficiencyLevel);
                        }
                    }
                }
                for (int i = 0; i <= objDropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails.Count; i++)
                {
                    objPDRPDropInsRatingScaleWeightages = new DropInsRatingScaleWeightages();
                    if (i == 0)
                    {
                        objPDRPDropInsRatingScaleWeightages.RatingScaleID = 0;
                        objPDRPDropInsRatingScaleWeightages.RatingScaleNumber = 0;
                        objDropInsModel.DropInsRatingScaleWeightages.Add(objPDRPDropInsRatingScaleWeightages);
                    }
                    else
                    {
                        objPDRPDropInsRatingScaleWeightages.RatingScaleID = Convert.ToInt32(objDropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails[i - 1].RatingScaleID);
                        objPDRPDropInsRatingScaleWeightages.RatingScaleNumber = Convert.ToInt32(objDropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails[i - 1].RatingScaleNumber);
                        objDropInsModel.DropInsRatingScaleWeightages.Add(objPDRPDropInsRatingScaleWeightages);
                    }
                }





            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objDropInsModel;
        }

        public string SaveDropIns(DropInsModel objDropInsModel, int ReqStatusID)
        {
            string Result = "";
            try
            {
                DataTable RatingScaleTable = new DataTable();
                RatingScaleTable.Columns.Add("RatingScaleCharacter");
                RatingScaleTable.Columns.Add("RatingScaleNumber");
                RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
                RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
                RatingScaleTable.Columns.Add("DefinitionName");
                RatingScaleTable.Columns.Add("DefinitionDetails");
                RatingScaleTable.Columns.Add("DefinitionName2");
                RatingScaleTable.Columns.Add("DefinitionDetails2");
                foreach (DropInsRatingScaleWeightageDetails Level in objDropInsModel.DropInEmployeeModel.DropInsRatingScaleWeightageDetails)
                {

                    RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails, Level.DefinitionName2, Level.DefinitionDetails2);

                }
                DataTable ProficiencyLevelTable = new DataTable();
                ProficiencyLevelTable.Columns.Add("ProficiencyID");
                ProficiencyLevelTable.Columns.Add("PDRPDropInsID");
                ProficiencyLevelTable.Columns.Add("ProficiencyScore");
                ProficiencyLevelTable.Columns.Add("ProficiencyFormula");
                //ProficiencyLevelTable.Columns.Add("ProficiencyValue");
                //int PDRPDropInsID = DBNull.Value;
                string Formla = objDropInsModel.ProficiencyScoreFormula;
                foreach (DropInProficiencyLevel Level in objDropInsModel.DropInProficiencyLevel)
                {
                    //if (Level.ProficiencyScore > 0) //*** F5
                    if (Level.ProficiencyScore != null && Level.IsRadioOption != 1)
                    {
                        string FormatedFormla = Formla.Replace("Rating", Level.ProficiencyScore.ToString());
                        if (Level.ProficiencyScore == "N/A")
                        { ProficiencyLevelTable.Rows.Add(Level.ProficiencyID, objDropInsModel.DropInEmployeeModel.ID, 0, FormatedFormla); }
                        else
                        { ProficiencyLevelTable.Rows.Add(Level.ProficiencyID, objDropInsModel.DropInEmployeeModel.ID, Level.ProficiencyScore, FormatedFormla); }

                    }
                    else
                    {
                        ProficiencyLevelTable.Rows.Add(Level.ProficiencyID, objDropInsModel.DropInEmployeeModel.ID, Level.ProficiencyScore, "");
                    }
                }
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@CompanyID",objDropInsModel.DropInEmployeeModel.CompanyID),
                         new SqlParameter("@ReqStatusID", ReqStatusID),
                         new SqlParameter("@RequesterEmployeeID",objDropInsModel.DropInEmployeeModel.RequesterID),
                         new SqlParameter("@TeacherID",objDropInsModel.DropInEmployeeModel.TeacherID),
                         new SqlParameter("@LessonTopic",objDropInsModel.DropInEmployeeModel.LessonTopic),
                         new SqlParameter("@StudentsOnRollMale",objDropInsModel.DropInEmployeeModel.StudentsOnRollMale),
                         new SqlParameter("@StudentsOnRollFemale",objDropInsModel.DropInEmployeeModel.StudentsOnRollFemale),
                         new SqlParameter("@VisitDATE",objDropInsModel.DropInEmployeeModel.VisitDATE==null?(object)DBNull.Value:objDropInsModel.DropInEmployeeModel.VisitDATE),
                         new SqlParameter("@DropInYear",objDropInsModel.DropInEmployeeModel.DropinYearID),
                         new SqlParameter("@Branch",objDropInsModel.DropInEmployeeModel.Branch),
                         new SqlParameter("@GradeYear",objDropInsModel.DropInEmployeeModel.GradeYear),
                         new SqlParameter("@GroupSection",objDropInsModel.DropInEmployeeModel.GroupSection),
                         new SqlParameter("@StudentsPresentMale",objDropInsModel.DropInEmployeeModel.StudentsPresentMale),
                         new SqlParameter("@StudentsPresentFemale",objDropInsModel.DropInEmployeeModel.StudentsPresentFemale),
                         new SqlParameter("@Total",objDropInsModel.ProficiencyScoreTotal),
                         new SqlParameter("@OverAllScore",objDropInsModel.ProficiencyScoreOverall),
                         new SqlParameter("@Feedback",objDropInsModel.Feedback),
                         new SqlParameter("@Outline",objDropInsModel.Outline),
                         new SqlParameter("@PreviousTargets",objDropInsModel.PreviousTargets),
                         new SqlParameter("@Targets",objDropInsModel.Targets),
                         new SqlParameter("@CreatedBy",objDropInsModel.DropInEmployeeModel.RequesterID),
                         new SqlParameter("@PDRPDropInsID", objDropInsModel.DropInEmployeeModel.ID),
                         new SqlParameter("@EmployeeAcknowledgedDropIn", objDropInsModel.EmployeeAcknowledgedDropIn),
                         new SqlParameter("@EmployeeName",objDropInsModel.DropInEmployeeModel.TeacherName),
                         new SqlParameter("@Department",objDropInsModel.DropInEmployeeModel.DepartmentName),
                         new SqlParameter("@Designation",objDropInsModel.DropInEmployeeModel.PositionTitle),
                         new SqlParameter("@LineManager",objDropInsModel.DropInEmployeeModel.SuperviserName),
                         new SqlParameter("@School",objDropInsModel.DropInEmployeeModel.CompanyName),
                        new SqlParameter("@ProficiencyLevelTable",ProficiencyLevelTable),
                        new SqlParameter("@RatingScaleTable",RatingScaleTable),
                        new SqlParameter("@VisitTime",string.IsNullOrEmpty(objDropInsModel.DropInEmployeeModel.VisitTime)?(object)DBNull.Value:"2020/01/01" + " " +objDropInsModel.DropInEmployeeModel.VisitTime)

                    };
                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SavePDRPDropIns", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Result = reader["Result"].ToString() + "," + reader["ID"].ToString() + "," + reader["RequestID"].ToString();
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return Result;
        }
        public List<RequestFormsApproverEmailModel> GetApproverEmailList(string pdrpDropInsID, int? requesterEmployeeID)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetNextApproverDetailsForDropIns", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PDRPDropInsID", pdrpDropInsID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", requesterEmployeeID != null ? requesterEmployeeID : (object)DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    RequestFormsApproverEmailModel requestFormsApproverEmailModel;
                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();
                        requestFormsApproverEmailModel.EmployeeFullName = reader["EmployeeFullName"].ToString();
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestFormsApproverEmailModel.IsRequester = bool.Parse(Convert.ToString(reader["IsRequester"]));
                        requestFormsApproverEmailModel.IsApprover = bool.Parse(Convert.ToString(reader["IsApprover"]));
                        requestFormsApproverEmailModel.IsNextApprover = bool.Parse(Convert.ToString(reader["IsNextApprover"]));
                        requestFormsApproverEmailModel.BUName = Convert.ToString(reader["BUName"]);
                        requestFormsApproverEmailModel.BUShortName_1 = Convert.ToString(reader["BUShortName_1"]);
                        requestFormsApproverEmailModel.FormName = Convert.ToString(reader["FormName"]);
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }

        #region Drop In Status Report
        public List<DropInStatusReportModel> GetDropInStatusReportData(int? dropInYearId, int? companyId, int? employeeId, int userId)
        {
            List<DropInStatusReportModel> dropInStatusReportList = null;
            try
            {
                dropInStatusReportList = new List<DropInStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DropInStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DropInYear", dropInYearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    DropInStatusReportModel dropInStatusModel;
                    while (sqlDataReader.Read())
                    {
                        dropInStatusModel = new DropInStatusReportModel();
                        dropInStatusModel.RequestNo = Convert.ToString(sqlDataReader["RequestNo"] == DBNull.Value ? "" : sqlDataReader["RequestNo"].ToString());
                        dropInStatusModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        dropInStatusModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        dropInStatusModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        dropInStatusModel.VisitDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["VisitDate"] == DBNull.Value ? "" : sqlDataReader["VisitDate"]));
                        dropInStatusModel.ObservationYear = Convert.ToString(sqlDataReader["ObservationYear"] == DBNull.Value ? "" : sqlDataReader["ObservationYear"]);
                        dropInStatusModel.LineManager = Convert.ToString(sqlDataReader["LineManager"] == DBNull.Value ? "" : sqlDataReader["LineManager"]);
                        dropInStatusModel.EmployeeAcknowledgedDropIn = Convert.ToString(sqlDataReader["EmployeeAcknowledgedDropIn"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAcknowledgedDropIn"]);
                        dropInStatusModel.Rating = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"]);
                        dropInStatusModel.TotalScore = Convert.ToDecimal(sqlDataReader["TotalScore"] == DBNull.Value ? "" : sqlDataReader["TotalScore"]);
                        dropInStatusReportList.Add(dropInStatusModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dropInStatusReportList;
        }
        #endregion
    }
}
