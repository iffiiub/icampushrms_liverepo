﻿using HRMS.Entities;
using HRMS.Entities.PDRP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.DataAccess.PDRP
{
    public class FormAssignmentDB: PDRPCommonModelDB
    {      
        public IEnumerable<FormAssignmentModel> GetPDRPFormAssignmentDataList(int? companyId, int? departmentId, int? lineManagerId, int? performanceGroupId, List<SelectListItem> performanceGroups, int userId)
        {
            IList<FormAssignmentModel> formAssignmentModelList = null;
            try
            {
                formAssignmentModelList = new List<FormAssignmentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPNonTeachFormAssignmentData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@LineManagerId", lineManagerId);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    FormAssignmentModel formAssignmentModel;
                    while (sqlDataReader.Read())
                    {
                        formAssignmentModel = new FormAssignmentModel();
                        formAssignmentModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        formAssignmentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        formAssignmentModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"] == DBNull.Value ? "0" : sqlDataReader["CompanyName"].ToString());
                        formAssignmentModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        formAssignmentModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        formAssignmentModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        formAssignmentModel.Email = Convert.ToString(sqlDataReader["Email"] == DBNull.Value ? "" : sqlDataReader["Email"]);
                        formAssignmentModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        formAssignmentModel.LineManager = Convert.ToString(sqlDataReader["LineManager"] == DBNull.Value ? "" : sqlDataReader["LineManager"]);
                        formAssignmentModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        //*** Naresh 2020-02-26 Line Manager name display with his Id and BU
                        formAssignmentModel.LineManagerId = Convert.ToInt32(sqlDataReader["LineManagerId"] == DBNull.Value ? "0" : sqlDataReader["LineManagerId"].ToString());
                        formAssignmentModel.LMBUName = Convert.ToString(sqlDataReader["LMBUName"] == DBNull.Value ? "" : sqlDataReader["LMBUName"]);
                        formAssignmentModelList.Add(formAssignmentModel);
                    }
                }

                sqlDataReader.NextResult(); //*** Reading Performance Group NAmes
                if (sqlDataReader.HasRows)
                {
                    
                    while (sqlDataReader.Read())
                    {
                        if (performanceGroups.Count == 0)
                        {
                            performanceGroups.Add(new SelectListItem() { Text = "Select", Value = "0" });
                        }
                        performanceGroups.Add(new SelectListItem() { Text = Convert.ToString(sqlDataReader["PerformanceGroupName"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupName"].ToString()), Value = Convert.ToString(sqlDataReader["PerformanceGroupID"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupID"].ToString()) });
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return formAssignmentModelList;
        }

        public OperationDetails SaveFormAssignmentData(IEnumerable<FormAssignmentModel> formGroupSettings,int CreatedBy)
        {
            DataTable formAssignmentSetting = new DataTable();
            formAssignmentSetting.Columns.Add("ID", typeof(int));
            formAssignmentSetting.Columns.Add("EmployeeID", typeof(int));
            formAssignmentSetting.Columns.Add("PerformancegroupId", typeof(int));

            foreach (var item in formGroupSettings)
            {
                formAssignmentSetting.Rows.Add(item.ID, item.EmployeeID, item.PerformanceGroupId);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPNonTeachSaveFormAssignment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormAssignmentSetting", formAssignmentSetting);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public List<Employee> GetLineManagerList(int UserId, int CompanyID)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_PDRPNonTeachGetLineManagerList", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FullName = Convert.ToString(sqlDataReader["EmloyeeFullName"] == DBNull.Value ? "" : sqlDataReader["EmloyeeFullName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }
    }

   
}
