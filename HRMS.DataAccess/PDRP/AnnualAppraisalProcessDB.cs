﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.PDRP
{
    public class AnnualAppraisalProcessDB: PDRPCommonModelDB
    {
        
        public OperationDetails Reintialized(int? GoalSettingId, int? UserId, int FormProcessID)
        {

            OperationDetails oprationDetails = new OperationDetails();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPMidYearReinitialize", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);


                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 4000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                oprationDetails.Message = "Reintialized successfully.";

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public OperationDetails ReintializeFullYear(int? GoalSettingId, int? UserId, int FormProcessID, string Comment)
        {

            OperationDetails oprationDetails = new OperationDetails();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPFullYearReinitialize", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                sqlCommand.Parameters.AddWithValue("@Comment", Comment);


                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 4000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                oprationDetails.Message = "Reintialized successfully.";

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public List<RequestFormsApproverEmailModel> GetFirstApproverEmailList(int formProcessID, int RequestID)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            RequestFormsApproverEmailModel requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetFirstApproverEmailList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = RequestID;
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();
                        //requestFormsApproverEmailModel.EmployeeFullName = reader["EmployeeFullName"].ToString();
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.IsRequester = false;
                        requestFormsApproverEmailModel.IsApprover = false;
                        requestFormsApproverEmailModel.IsNextApprover = false;
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(Convert.ToString(reader["GroupID"]));
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }
        //*** F24 LM Comment and Employee Comment
        public OperationDetails MidEmployeeSignOffForms( int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? goalSettingId, string comments, string EmployeeMidYearPerformanceComments)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPMidEmployeeSignOffForms", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                //sqlCommand.Parameters.AddWithValue("@MIdYearFormId", MIdYearFormId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", EmployeeMidYearPerformanceComments);
                //sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingId);
                sqlCommand.Parameters.AddWithValue("@Comment", comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Submitted successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails FullEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? goalSettingId, string comments, string MyThreeKeyAchivements, string MyTwoDevelopmentAreas, string EmployeeCommentsonFullYearPerformance)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPFullEmployeeSignOffForms", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                //sqlCommand.Parameters.AddWithValue("@MIdYearFormId", MIdYearFormId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                //sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingId);
                sqlCommand.Parameters.AddWithValue("@Comment", comments);
                sqlCommand.Parameters.AddWithValue("@MyThreeKeyAchivements", MyThreeKeyAchivements);
                sqlCommand.Parameters.AddWithValue("@MyTwoDevelopmentAreas", MyTwoDevelopmentAreas);
                sqlCommand.Parameters.AddWithValue("@EmployeeCommentsonFullYearPerformance", EmployeeCommentsonFullYearPerformance);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Submitted successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveLog(int? ID, int UserId, int? performanceGroupId)
        {
            string ProName = "";
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (performanceGroupId == 1)
                {
                    ProName = "HR_Stp_PDRPSaveMidAppraisalSettings_LOG";
                }
                else if (performanceGroupId == 2)
                {
                    ProName = "HR_Stp_PDRPSaveMidAppraisalSettings_LOG";
                }
                else if (performanceGroupId == 3)
                {
                    ProName = "HR_Stp_PDRPSaveMidAppraisalSettings_LOG";
                }
                else if (performanceGroupId == 3)
                {
                    ProName = "HR_Stp_PDRPSaveMidAppraisalSettings_LOG";
                }
                sqlCommand = new SqlCommand(ProName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", UserId);

                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveLogFull(int? ID, int UserId, int? performanceGroupId)
        {
            string ProName = "";
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (performanceGroupId == 1)
                {
                    ProName = "HR_Stp_PDRPSaveFullAppraisalFormA_LOG";
                }
                else if (performanceGroupId == 2)
                {
                    ProName = "HR_Stp_PDRPSaveFullAppraisalFormB_LOG";
                }
                else if (performanceGroupId == 3)
                {
                    ProName = "HR_Stp_PDRPSaveFullAppraisalFormC_LOG";
                }
                else if (performanceGroupId == 4)
                {
                    ProName = "HR_Stp_PDRPSaveFullAppraisalFormD_LOG";
                }
                sqlCommand = new SqlCommand(ProName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", UserId);

                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails FormStatUpdate(int? id, int FormState, int? performanceGroupId, int? formProcessID, int? employeeId)
        {
            OperationDetails operationDetails = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPMidFormStatUpdate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", id);
                sqlCommand.Parameters.AddWithValue("@FormState", FormState);
                //sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlDataReader sqlReader = sqlCommand.ExecuteReader();


                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {

                        //operationDetails.Message = sqlReader["Result"].ToString();

                        if (sqlReader["Result"].ToString().Contains("SUCCESS"))
                        {
                            operationDetails.Success = true;
                            operationDetails.CssClass = "success";
                            operationDetails.Message = "Updated successfully";
                        }
                        else
                        {
                            operationDetails.Success = false;
                            operationDetails.Message = "Technical error has occurred";
                            operationDetails.CssClass = "error";
                        }

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails FullFormStatUpdate(int? id, int FormState, int? performanceGroupId, int? formProcessID, int? employeeId)
        {
            OperationDetails operationDetails = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPFullFormStatUpdate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", id);
                sqlCommand.Parameters.AddWithValue("@FormState", FormState);
                //sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlDataReader sqlReader = sqlCommand.ExecuteReader();


                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {

                        //operationDetails.Message = sqlReader["Result"].ToString();

                        if (sqlReader["Result"].ToString().Contains("SUCCESS"))
                        {
                            operationDetails.Success = true;
                            operationDetails.CssClass = "success";
                            operationDetails.Message = "Updated successfully";
                        }
                        else
                        {
                            operationDetails.Success = false;
                            operationDetails.Message = "Technical error has occurred";
                            operationDetails.CssClass = "error";
                        }

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveAnnualGoalSettingsFormCData(int RequestID, IEnumerable<AnnualGoalSettingFormAModel> goalSettingBusinnessTargetsSettings, int? userId, AnnualGoalSettingFormAModel businessTargetModel)
        {
            //DataTable businessTargetSettings = new DataTable();
            //businessTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            //businessTargetSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            //businessTargetSettings.Columns.Add("Weight", typeof(int));

            //foreach (var item in goalSettingBusinnessTargetsSettings)
            //{
            //    businessTargetSettings.Rows.Add(item.BusinessTargetId, item.BusinessTargetDetails, item.Weight);
            //}
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveAnnualMidYearFormData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", businessTargetModel.GoalSettingId);

                sqlCommand.Parameters.AddWithValue("@UserId", userId);

                sqlCommand.Parameters.AddWithValue("@MidFormState", businessTargetModel.MidFormMode);

                sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", businessTargetModel.LineManagerEvaluationComments);
                sqlCommand.Parameters.AddWithValue("@TrainingNeeds", businessTargetModel.TrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", businessTargetModel.LineManagerMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", businessTargetModel.EmployeeMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveAnnualGoalSettingsFormBData(int RequestID, IEnumerable<AnnualGoalSettingFormAModel> goalSettingBusinnessTargetsSettings, int? userId, AnnualGoalSettingFormAModel businessTargetModel)
        {
            DataTable businessTargetSettings = new DataTable();
            businessTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            businessTargetSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            businessTargetSettings.Columns.Add("Weight", typeof(int));

            foreach (var item in goalSettingBusinnessTargetsSettings)
            {
                businessTargetSettings.Rows.Add(item.BusinessTargetId, item.BusinessTargetDetails, item.Weight);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveAnnualMidYearFormBData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormAID", businessTargetModel.ID);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormASettings", businessTargetSettings);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", businessTargetModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", businessTargetModel.BusinesstargetComments);
                sqlCommand.Parameters.AddWithValue("@CompetenciesComments", businessTargetModel.CompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", businessTargetModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", businessTargetModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", businessTargetModel.JobGrade);
                sqlCommand.Parameters.AddWithValue("@FormState", businessTargetModel.FormState);

                sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", businessTargetModel.LineManagerEvaluationComments);
                sqlCommand.Parameters.AddWithValue("@TrainingNeeds", businessTargetModel.TrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", businessTargetModel.LineManagerMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", businessTargetModel.EmployeeMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveAnnualGoalSettingsFormAData(int RequestID,IEnumerable<AnnualGoalSettingFormAModel> goalSettingBusinnessTargetsSettings, int? userId, AnnualGoalSettingFormAModel businessTargetModel)
        {
            DataTable businessTargetSettings = new DataTable();
            businessTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            businessTargetSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            businessTargetSettings.Columns.Add("Weight", typeof(int));

            foreach (var item in goalSettingBusinnessTargetsSettings)
            {
                businessTargetSettings.Rows.Add(item.BusinessTargetId, item.BusinessTargetDetails, item.Weight);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveAnnualMidYearFormAData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormAID", businessTargetModel.ID);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormASettings", businessTargetSettings);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", businessTargetModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", businessTargetModel.BusinesstargetComments);
                sqlCommand.Parameters.AddWithValue("@CompetenciesComments", businessTargetModel.CompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", businessTargetModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", businessTargetModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", businessTargetModel.JobGrade);
                sqlCommand.Parameters.AddWithValue("@FormState", businessTargetModel.FormState);

                sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", businessTargetModel.LineManagerEvaluationComments);
                sqlCommand.Parameters.AddWithValue("@TrainingNeeds", businessTargetModel.TrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", businessTargetModel.LineManagerMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", businessTargetModel.EmployeeMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails AnnualAppraisalMidFormALMSubmit(int? id, int? employeeId, int? formId, int? goalSettingId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveAnnualAppraisalMidFormALMSubmit", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", id);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                //sqlCommand.Parameters.AddWithValue("@FormId", formId);
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingId);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Submitted successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveFullYearAppraisalFormAData(int RequestID,IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings, List<ProfessionalCompetencies> ProfessionalCompetencies, List<BehavioralCompetencies> BehavioralCompetencies, int? userId, AnnualAppraisalProcessModel AnnualAppraisalProcessModel, int formId)
        {
            DataTable midYearTargetSettings = new DataTable();
            midYearTargetSettings.Columns.Add("BusinessTargetNo", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetDetails", typeof(string));
            midYearTargetSettings.Columns.Add("Accomplishments", typeof(string));
            midYearTargetSettings.Columns.Add("Weight", typeof(int));
            midYearTargetSettings.Columns.Add("Rating", typeof(int));
            midYearTargetSettings.Columns.Add("Score", typeof(decimal));

            foreach (var item in businessTargetMidYearAppraisalSettings)
            {           
                midYearTargetSettings.Rows.Add(item.BusinessTargetNo, item.BusinessTargetId, item.BusinessTargetDetails, item.Accomplishments, item.Weight, item.Rating, (decimal.Divide(item.Weight,100) * item.Rating));
            }

            DataTable dtProfessionalCompetencies = new DataTable();
            dtProfessionalCompetencies.Columns.Add("ProfessionalCompetenciesNo", typeof(int));
            dtProfessionalCompetencies.Columns.Add("Rating", typeof(string));
            dtProfessionalCompetencies.Columns.Add("CompetencyID", typeof(int));

            foreach (var item in ProfessionalCompetencies)
            {
                dtProfessionalCompetencies.Rows.Add(item.No, item.Score,item.CompetencyID);
            }

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePDRPFullYearFormAData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", AnnualAppraisalProcessModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormAID", AnnualAppraisalProcessModel.ID);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormASettings", midYearTargetSettings);
                sqlCommand.Parameters.AddWithValue("@PDRPProfessionalCompetenciesTY", dtProfessionalCompetencies);
                //sqlCommand.Parameters.AddWithValue("@PDRPBehavioralCompetenciesTY", dtBehavioralCompetencies);


                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.PerformanceGroupId);
                //sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsComments);
                //sqlCommand.Parameters.AddWithValue("@CompetenciesComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CoreCompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", AnnualAppraisalProcessModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", AnnualAppraisalProcessModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JobGrade);
                //sqlCommand.Parameters.AddWithValue("@FormState", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormState);
                sqlCommand.Parameters.AddWithValue("@FullFormState", AnnualAppraisalProcessModel.FullFormMode);

                //sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerEvaluationComments);
                //sqlCommand.Parameters.AddWithValue("@TrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.TrainingNeeds);
                //sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerMidYearPerformanceComments);
                //sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeMidYearPerformanceComments);

                sqlCommand.Parameters.AddWithValue("@MyThreeKeyAchivements", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyThreeKeyAchivements);
                sqlCommand.Parameters.AddWithValue("@MyTwoDevelopmentAreas", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyTwoDevelopmentAreas);
                sqlCommand.Parameters.AddWithValue("@EmployeeCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeCommentsonFullYearPerformance);
                sqlCommand.Parameters.AddWithValue("@Strengths", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.Strengths);
                sqlCommand.Parameters.AddWithValue("@FullTrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FullTrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerCommentsonFullYearPerformance);

                sqlCommand.Parameters.AddWithValue("@BusinessTargetsTota", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsTota);
                sqlCommand.Parameters.AddWithValue("@ProfessionalCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.ProfessionalCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@BehavioralCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BehavioralCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@CompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@OverallAppraisalScore", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallAppraisalScore);
                sqlCommand.Parameters.AddWithValue("@OverallScoreFormula", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallScoreFormula);
                sqlCommand.Parameters.AddWithValue("@FormSubmit", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormSubmit);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@Comments", AnnualAppraisalProcessModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** Naresh 2020-03-01 Message modified
                operationDetails.Message = "The request data save successfully.";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveFullYearAppraisalFormBData(int RequestID, IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings, List<ProfessionalCompetencies> ProfessionalCompetencies, List<BehavioralCompetencies> BehavioralCompetencies, int? userId, AnnualAppraisalProcessModel AnnualAppraisalProcessModel,int formId)
        {
            DataTable midYearTargetSettings = new DataTable();
            midYearTargetSettings.Columns.Add("BusinessTargetNo", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetDetails", typeof(string));
            midYearTargetSettings.Columns.Add("Accomplishments", typeof(string));
            midYearTargetSettings.Columns.Add("Weight", typeof(int));
            midYearTargetSettings.Columns.Add("Rating", typeof(int));
            midYearTargetSettings.Columns.Add("Score", typeof(decimal));

            foreach (var item in businessTargetMidYearAppraisalSettings)
            {
                midYearTargetSettings.Rows.Add(item.BusinessTargetNo, item.BusinessTargetId, item.BusinessTargetDetails, item.Accomplishments, item.Weight, item.Rating, (decimal.Divide(item.Weight, 100) * item.Rating));
            }

            DataTable dtProfessionalCompetencies = new DataTable();
            dtProfessionalCompetencies.Columns.Add("ProfessionalCompetenciesNo", typeof(int));
            dtProfessionalCompetencies.Columns.Add("Rating", typeof(string));
            dtProfessionalCompetencies.Columns.Add("CompetencyID", typeof(int));

            foreach (var item in ProfessionalCompetencies)
            {
                dtProfessionalCompetencies.Rows.Add(item.No, item.Score, item.CompetencyID);
            }

            DataTable dtBehavioralCompetencies = new DataTable();
            dtBehavioralCompetencies.Columns.Add("BehavioralCompetenciesNo", typeof(int));
            dtBehavioralCompetencies.Columns.Add("Rating", typeof(string));
            dtBehavioralCompetencies.Columns.Add("CompetencyID", typeof(int));
            foreach (var item in BehavioralCompetencies)
            {
                dtBehavioralCompetencies.Rows.Add(item.No, item.Score, item.CompetencyID);
            }

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePDRPFullYearFormBData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", AnnualAppraisalProcessModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormBID", AnnualAppraisalProcessModel.ID);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormBSettings", midYearTargetSettings);
                sqlCommand.Parameters.AddWithValue("@PDRPProfessionalCompetenciesTY", dtProfessionalCompetencies);
                sqlCommand.Parameters.AddWithValue("@PDRPBehavioralCompetenciesTY", dtBehavioralCompetencies);


                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.PerformanceGroupId);
                //sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsComments);
                //sqlCommand.Parameters.AddWithValue("@CompetenciesComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CoreCompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", AnnualAppraisalProcessModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", AnnualAppraisalProcessModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JobGrade);
                //sqlCommand.Parameters.AddWithValue("@FormState", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormState);
                sqlCommand.Parameters.AddWithValue("@FullFormState", AnnualAppraisalProcessModel.FullFormMode);

                //sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerEvaluationComments);
                //sqlCommand.Parameters.AddWithValue("@TrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.TrainingNeeds);
                //sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerMidYearPerformanceComments);
                //sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeMidYearPerformanceComments);

                sqlCommand.Parameters.AddWithValue("@MyThreeKeyAchivements", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyThreeKeyAchivements);
                sqlCommand.Parameters.AddWithValue("@MyTwoDevelopmentAreas", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyTwoDevelopmentAreas);
                sqlCommand.Parameters.AddWithValue("@EmployeeCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeCommentsonFullYearPerformance);
                sqlCommand.Parameters.AddWithValue("@Strengths", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.Strengths);
                sqlCommand.Parameters.AddWithValue("@FullTrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FullTrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerCommentsonFullYearPerformance);

                sqlCommand.Parameters.AddWithValue("@BusinessTargetsTota", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsTota);
                sqlCommand.Parameters.AddWithValue("@ProfessionalCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.ProfessionalCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@BehavioralCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BehavioralCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@CompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@OverallAppraisalScore", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallAppraisalScore);
                sqlCommand.Parameters.AddWithValue("@OverallScoreFormula", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallScoreFormula);
                sqlCommand.Parameters.AddWithValue("@FormSubmit", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormSubmit);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", AnnualAppraisalProcessModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** Naresh 2020-03-01 Message modified
                operationDetails.Message = "The request data save successfully.";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }


        public OperationDetails SaveFullYearAppraisalFormCData(int RequestID, IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings, List<ProfessionalCompetencies> ProfessionalCompetencies, List<BehavioralCompetencies> BehavioralCompetencies, int? userId, AnnualAppraisalProcessModel AnnualAppraisalProcessModel, int formId)
        {
            DataTable midYearTargetSettings = new DataTable();
            midYearTargetSettings.Columns.Add("BusinessTargetNo", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetDetails", typeof(string));
            midYearTargetSettings.Columns.Add("Accomplishments", typeof(string));
            midYearTargetSettings.Columns.Add("Weight", typeof(int));
            midYearTargetSettings.Columns.Add("Rating", typeof(int));
            midYearTargetSettings.Columns.Add("Score", typeof(decimal));

            foreach (var item in businessTargetMidYearAppraisalSettings)
            {
                midYearTargetSettings.Rows.Add(item.BusinessTargetNo, item.BusinessTargetId, item.BusinessTargetDetails, item.Accomplishments, item.Weight, item.Rating, (decimal.Divide(item.Weight, 100) * item.Rating));
            }

            DataTable dtProfessionalCompetencies = new DataTable();
            dtProfessionalCompetencies.Columns.Add("ProfessionalCompetenciesNo", typeof(int));
            dtProfessionalCompetencies.Columns.Add("Rating", typeof(string));
            dtProfessionalCompetencies.Columns.Add("CompetencyID", typeof(int));

            foreach (var item in ProfessionalCompetencies)
            {
                dtProfessionalCompetencies.Rows.Add(item.No, item.Score,item.CompetencyID);
            }

            DataTable dtBehavioralCompetencies = new DataTable();
            dtBehavioralCompetencies.Columns.Add("BehavioralCompetenciesNo", typeof(int));
            dtBehavioralCompetencies.Columns.Add("Rating", typeof(string));
            dtBehavioralCompetencies.Columns.Add("CompetencyID", typeof(int));

            foreach (var item in BehavioralCompetencies)
            {
                dtBehavioralCompetencies.Rows.Add(item.No, item.Score,item.CompetencyID);
            }

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePDRPFullYearFormCData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", AnnualAppraisalProcessModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormCID", AnnualAppraisalProcessModel.ID);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormCSettings", midYearTargetSettings);
                sqlCommand.Parameters.AddWithValue("@PDRPProfessionalCompetenciesTY", dtProfessionalCompetencies);
                sqlCommand.Parameters.AddWithValue("@PDRPBehavioralCompetenciesTY", dtBehavioralCompetencies);


                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.PerformanceGroupId);
                //sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsComments);
                //sqlCommand.Parameters.AddWithValue("@CompetenciesComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CoreCompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", AnnualAppraisalProcessModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", AnnualAppraisalProcessModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JobGrade);
                //sqlCommand.Parameters.AddWithValue("@FormState", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormState);
                sqlCommand.Parameters.AddWithValue("@FullFormState", AnnualAppraisalProcessModel.FullFormMode);

                //sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerEvaluationComments);
                //sqlCommand.Parameters.AddWithValue("@TrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.TrainingNeeds);
                //sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerMidYearPerformanceComments);
                //sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeMidYearPerformanceComments);

                sqlCommand.Parameters.AddWithValue("@MyThreeKeyAchivements", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyThreeKeyAchivements);
                sqlCommand.Parameters.AddWithValue("@MyTwoDevelopmentAreas", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyTwoDevelopmentAreas);
                sqlCommand.Parameters.AddWithValue("@EmployeeCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeCommentsonFullYearPerformance);
                sqlCommand.Parameters.AddWithValue("@Strengths", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.Strengths);
                sqlCommand.Parameters.AddWithValue("@FullTrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FullTrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerCommentsonFullYearPerformance);

                sqlCommand.Parameters.AddWithValue("@BusinessTargetsTota", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsTota);
                sqlCommand.Parameters.AddWithValue("@ProfessionalCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.ProfessionalCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@BehavioralCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BehavioralCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@CompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@OverallAppraisalScore", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallAppraisalScore);
                sqlCommand.Parameters.AddWithValue("@OverallScoreFormula", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallScoreFormula);
                sqlCommand.Parameters.AddWithValue("@FormSubmit", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormSubmit);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", AnnualAppraisalProcessModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** Naresh 2020-03-01 Message modified
                operationDetails.Message = "The request data save successfully.";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveFullYearAppraisalFormDData(int RequestID, IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings, List<ProfessionalCompetencies> ProfessionalCompetencies, int? userId, AnnualAppraisalProcessModel AnnualAppraisalProcessModel, int formId)
        {


            DataTable dtProfessionalCompetencies = new DataTable();
            dtProfessionalCompetencies.Columns.Add("ProfessionalCompetenciesNo", typeof(int));
            dtProfessionalCompetencies.Columns.Add("MidRating", typeof(string));
            dtProfessionalCompetencies.Columns.Add("FullRating", typeof(string));
            dtProfessionalCompetencies.Columns.Add("CompetencyID", typeof(int));

            foreach (var item in ProfessionalCompetencies)
            {
                dtProfessionalCompetencies.Rows.Add(item.No, item.Score, item.FullScore,item.CompetencyID);
            }

            //DataTable dtBehavioralCompetencies = new DataTable();
            //dtBehavioralCompetencies.Columns.Add("BehavioralCompetenciesNo", typeof(int));
            //dtBehavioralCompetencies.Columns.Add("Rating", typeof(string));

            //foreach (var item in BehavioralCompetencies)
            //{
            //    dtBehavioralCompetencies.Rows.Add(item.No, item.Score);
            //}

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePDRPFullYearFormDData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", AnnualAppraisalProcessModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormDID", AnnualAppraisalProcessModel.ID);
                //sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormCSettings", midYearTargetSettings);
                sqlCommand.Parameters.AddWithValue("@PDRPProfessionalCompetenciesTY", dtProfessionalCompetencies);
                //sqlCommand.Parameters.AddWithValue("@PDRPBehavioralCompetenciesTY", dtBehavioralCompetencies);


                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.PerformanceGroupId);
                //sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsComments);
                //sqlCommand.Parameters.AddWithValue("@CompetenciesComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CoreCompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", AnnualAppraisalProcessModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", AnnualAppraisalProcessModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JobGrade);
                //sqlCommand.Parameters.AddWithValue("@FormState", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormState);
                sqlCommand.Parameters.AddWithValue("@FullFormState", AnnualAppraisalProcessModel.FullFormMode);

                //sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerEvaluationComments);
                //sqlCommand.Parameters.AddWithValue("@TrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.TrainingNeeds);
                //sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerMidYearPerformanceComments);
                //sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeMidYearPerformanceComments);

                sqlCommand.Parameters.AddWithValue("@MyThreeKeyAchivements", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyThreeKeyAchivements);
                sqlCommand.Parameters.AddWithValue("@MyTwoDevelopmentAreas", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyTwoDevelopmentAreas);
                sqlCommand.Parameters.AddWithValue("@EmployeeCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeCommentsonFullYearPerformance);
                sqlCommand.Parameters.AddWithValue("@Strengths", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.Strengths);
                sqlCommand.Parameters.AddWithValue("@FullTrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FullTrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerCommentsonFullYearPerformance);

                //sqlCommand.Parameters.AddWithValue("@BusinessTargetsTota", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsTota);
                sqlCommand.Parameters.AddWithValue("@ProfessionalCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.ProfessionalCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@BehavioralCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BehavioralCompetenciesAVG);
                //sqlCommand.Parameters.AddWithValue("@CompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CompetenciesAVG);
                //sqlCommand.Parameters.AddWithValue("@OverallAppraisalScore", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallAppraisalScore);
                //sqlCommand.Parameters.AddWithValue("@OverallScoreFormula", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallScoreFormula);
                sqlCommand.Parameters.AddWithValue("@FormSubmit", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormSubmit);
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", AnnualAppraisalProcessModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** Naresh 2020-03-01 Message modified
                operationDetails.Message = "The request data save successfully.";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveAnnualAppraisalProcessData(IEnumerable<AnnualAppraisalProcessModel> annualAppraisalprocessSettings, int userId, int companyId,int PeriodId)
        {
            DataTable annualAppraisalSettings = new DataTable();
            annualAppraisalSettings.Columns.Add("EmployeeID", typeof(int));
            annualAppraisalSettings.Columns.Add("PerformancegroupId", typeof(int));
            annualAppraisalSettings.Columns.Add("YearId", typeof(int));
            annualAppraisalSettings.Columns.Add("PeriodId", typeof(int));
            annualAppraisalSettings.Columns.Add("CompanyId", typeof(int));
            annualAppraisalSettings.Columns.Add("EvaluationStartDate", typeof(DateTime));
            annualAppraisalSettings.Columns.Add("EvaluationDueDate", typeof(DateTime));
            annualAppraisalSettings.Columns.Add("ProcessName", typeof(string));
            annualAppraisalSettings.Columns.Add("GoalSettingId", typeof(int));

            foreach (var item in annualAppraisalprocessSettings)
            {
                annualAppraisalSettings.Rows.Add(item.EmployeeID, item.PerformanceGroupId, item.YearId, item.PeriodId, item.CompanyId,
                    DateTime.ParseExact(item.EvaluationStartDate, HRMSDateFormat, CultureInfo.InvariantCulture)
                    , DateTime.ParseExact(item.EvaluationDueDate, HRMSDateFormat, CultureInfo.InvariantCulture) //*** F17 Date Format in server
                , item.ProcessName, item.GoalSettingId);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (PeriodId == 1)
                {
                    sqlCommand = new SqlCommand("HR_Stp_PDRPSaveMidAppraisalProcessSettings", sqlConnection);
                }
                else
                {
                    sqlCommand = new SqlCommand("HR_Stp_PDRPSaveFullAppraisalProcessSettings", sqlConnection);

                }
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualAppraisalProcessSettings", annualAppraisalSettings);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "The request(s) are generated successfully.";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public AnnualAppraisalProcessModel GetAnnualAppraisalProcessDataByFormProcessId(int? formprocessId,int FormID)
        {
            AnnualAppraisalProcessModel annualAppraisalSettingModel = new AnnualAppraisalProcessModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (FormID == 40 || FormID == 46 || FormID == 42 || FormID == 47)
                {
                    sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormDataByFormProcessId", sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@FormID", FormID);
                }
                //else if (FormID == 42)
                //{
                //    sqlCommand = new SqlCommand("HR_Stp_GetAnnualAppraisalProcessDataByFormProcessId", sqlConnection);
                //}
                //else if (FormID == 41)
                //{
                //    sqlCommand = new SqlCommand("HR_Stp_MidYearEmployeeAppraisalDetails", sqlConnection);
                //}
                //else if (FormID == 44)
                //{
                //    sqlCommand = new SqlCommand("HR_Stp_MidYearEmployeeAppraisalDetailsB", sqlConnection);
                //}
                //else if (FormID == 45)
                //{
                //    sqlCommand = new SqlCommand("HR_Stp_MidYearEmployeeAppraisalDetailsC", sqlConnection);
                //}
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formprocessId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        annualAppraisalSettingModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        annualAppraisalSettingModel.RequestId = Convert.ToInt32(sqlDataReader["RequestId"] == DBNull.Value ? "0" : sqlDataReader["RequestId"].ToString());
                        annualAppraisalSettingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        annualAppraisalSettingModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        annualAppraisalSettingModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        annualAppraisalSettingModel.PeriodId = Convert.ToInt32(sqlDataReader["PeriodId"] == DBNull.Value ? "0" : sqlDataReader["PeriodId"]);
                        annualAppraisalSettingModel.EvaluationStartDate = Convert.ToString(sqlDataReader["EvaluationStartDate"] == DBNull.Value ? "" : sqlDataReader["EvaluationStartDate"]);
                        annualAppraisalSettingModel.EvaluationDueDate = Convert.ToString(sqlDataReader["EvaluationDueDate"] == DBNull.Value ? "" : sqlDataReader["EvaluationDueDate"]);
                        annualAppraisalSettingModel.ReqStatusId = Convert.ToInt32(sqlDataReader["ReqStatusId"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusId"].ToString());
                        annualAppraisalSettingModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        annualAppraisalSettingModel.IsSubmitForLM = Convert.ToBoolean(sqlDataReader["IsSubmitForLM"] == DBNull.Value ? "0" : sqlDataReader["IsSubmitForLM"].ToString());
                        annualAppraisalSettingModel.IsSignOffForm = Convert.ToBoolean(sqlDataReader["IsSignOffForm"] == DBNull.Value ? "0" : sqlDataReader["IsSignOffForm"].ToString());
                        annualAppraisalSettingModel.AnnualAppraisalProcessId = Convert.ToInt32(sqlDataReader["AnnualAppraisalSettingsID"] == DBNull.Value ? "0" : sqlDataReader["AnnualAppraisalSettingsID"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return annualAppraisalSettingModel;
        }


        #region Mid Year Appraisal Form
        public AnnualAppraisalProcessModel GetFull_MidYearAppraisalFormDetails(int? employeeId,int PeriodId,int YearID,int RequestID)
        {
            AnnualAppraisalProcessModel annualAppraisalSettingModel = new AnnualAppraisalProcessModel();
            MidYearAppraisalForm midYearAppraisalFormModel = new MidYearAppraisalForm();
            BusinessTargetsMidYearAppraisal businessTargetMidYearFormModel = null;
            IList<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalList = null;
            ProfessionalCompetencies ProfessionalCompetencies;
            List<ProfessionalCompetencies> ProfessionalCompetenciesList =new List<ProfessionalCompetencies>();
            BehavioralCompetencies BehavioralCompetencies;
            List<BehavioralCompetencies> BehavioralCompetenciesList = new List<BehavioralCompetencies>();
            MidYearAppraisalDetails midYearAppraisalDetails = new MidYearAppraisalDetails();
            try
            {
                businessTargetMidYearAppraisalList = new List<BusinessTargetsMidYearAppraisal>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (PeriodId==2)
                {
                    sqlCommand = new SqlCommand("HR_Stp_GetPDRPFullyearAppraisalFormDetails", sqlConnection);
                }
                else 
                {
                    sqlCommand = new SqlCommand("HR_Stp_GetMidyearAppraisalFormDetails", sqlConnection);
                }
                
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                if (PeriodId == 2)
                {
                    sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                }
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        string iType = "";
                        midYearAppraisalFormModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        if (PeriodId == 2)
                        {
                            iType = Convert.ToString(sqlDataReader["Type"] == DBNull.Value ? "" : sqlDataReader["Type"]);
                            if (iType == "MID")
                            {
                                midYearAppraisalFormModel.ID = 0;
                            }
                        }


                        midYearAppraisalFormModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        midYearAppraisalFormModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        midYearAppraisalFormModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        midYearAppraisalFormModel.BusinessTargetsComments = Convert.ToString(sqlDataReader["BusinessTargetsComments"] == DBNull.Value ? "" : sqlDataReader["BusinessTargetsComments"]);
                        midYearAppraisalFormModel.CoreCompetenciesComments = Convert.ToString(sqlDataReader["CoreCompetenciesComments"] == DBNull.Value ? "" : sqlDataReader["CoreCompetenciesComments"]);
                        midYearAppraisalFormModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        midYearAppraisalFormModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusID"].ToString());
                        midYearAppraisalFormModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        if (PeriodId == 2)
                        {
                            midYearAppraisalFormModel.ApproverEmployeeID = Convert.ToInt32(sqlDataReader["ApproverEmployeeID"] == DBNull.Value ? "0" : sqlDataReader["ApproverEmployeeID"].ToString());
                        }
                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        businessTargetMidYearFormModel = new BusinessTargetsMidYearAppraisal();
                        businessTargetMidYearFormModel.BusinessTargetId = Convert.ToInt32(sqlDataReader["BusinessTargetId"] == DBNull.Value ? "0" : sqlDataReader["BusinessTargetId"].ToString());
                        businessTargetMidYearFormModel.BusinessTargetDetails = Convert.ToString(sqlDataReader["BusinessTargetDetails"] == DBNull.Value ? "" : sqlDataReader["BusinessTargetDetails"].ToString());
                        businessTargetMidYearFormModel.Accomplishments = Convert.ToString(sqlDataReader["Accomplishments"] == DBNull.Value ? "" : sqlDataReader["Accomplishments"].ToString());
                        businessTargetMidYearFormModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? "0" : sqlDataReader["Weight"]);
                        businessTargetMidYearFormModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"]);
                        if (PeriodId == 2)
                        {
                            businessTargetMidYearFormModel.Rating = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"]);
                            //businessTargetMidYearFormModel.Score = Convert.ToInt32(sqlDataReader["Score"] == DBNull.Value ? "0" : sqlDataReader["Score"]);
                            //*** F5                            
                            businessTargetMidYearFormModel.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                        }
                        businessTargetMidYearAppraisalList.Add(businessTargetMidYearFormModel);
                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {

                        midYearAppraisalDetails.MidYearAppraisalDetailId = Convert.ToInt32(sqlDataReader["MidYearAppraisalDetailId"] == DBNull.Value ? "0" : sqlDataReader["MidYearAppraisalDetailId"].ToString());

                        midYearAppraisalDetails.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        midYearAppraisalDetails.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        midYearAppraisalDetails.LineManagerEvaluationComments = Convert.ToString(sqlDataReader["LineManagerEvaluationComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerEvaluationComments"]);
                        midYearAppraisalDetails.TrainingNeeds = Convert.ToString(sqlDataReader["TrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["TrainingNeeds"]);
                        midYearAppraisalDetails.LineManagerMidYearPerformanceComments = Convert.ToString(sqlDataReader["LineManagerMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerMidYearPerformanceComments"]);
                        midYearAppraisalDetails.EmployeeMidYearPerformanceComments = Convert.ToString(sqlDataReader["EmployeeMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["EmployeeMidYearPerformanceComments"]);
                        if (PeriodId == 2)
                        {
                            midYearAppraisalFormModel.MyThreeKeyAchivements = Convert.ToString(sqlDataReader["MyThreeKeyAchivements"] == DBNull.Value ? "" : sqlDataReader["MyThreeKeyAchivements"]);
                            midYearAppraisalFormModel.MyTwoDevelopmentAreas = Convert.ToString(sqlDataReader["MyTwoDevelopmentAreas"] == DBNull.Value ? "" : sqlDataReader["MyTwoDevelopmentAreas"]);
                            midYearAppraisalFormModel.EmployeeCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["EmployeeCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["EmployeeCommentsonFullYearPerformance"]);
                            midYearAppraisalFormModel.Strengths = Convert.ToString(sqlDataReader["Strengths"] == DBNull.Value ? "" : sqlDataReader["Strengths"]);
                            midYearAppraisalFormModel.FullTrainingNeeds = Convert.ToString(sqlDataReader["FullTrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["FullTrainingNeeds"]);
                            midYearAppraisalFormModel.LineManagerCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["LineManagerCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["LineManagerCommentsonFullYearPerformance"]);

                            midYearAppraisalFormModel.BusinessTargetsTota = Convert.ToDecimal(sqlDataReader["BusinessTargetsTota"] == DBNull.Value ? 0 : sqlDataReader["BusinessTargetsTota"]);
                            midYearAppraisalFormModel.ProfessionalCompetenciesAVG = Convert.ToDecimal(sqlDataReader["ProfessionalCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["ProfessionalCompetenciesAVG"]);
                            midYearAppraisalFormModel.BehavioralCompetenciesAVG = Convert.ToDecimal(sqlDataReader["BehavioralCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["BehavioralCompetenciesAVG"]);
                            midYearAppraisalFormModel.CompetenciesAVG = Convert.ToDecimal(sqlDataReader["CompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["CompetenciesAVG"]);
                            midYearAppraisalFormModel.OverallAppraisalScore = Convert.ToDecimal(sqlDataReader["OverallAppraisalScore"] == DBNull.Value ? 0 : sqlDataReader["OverallAppraisalScore"]);
                        }
                    }
                    if (PeriodId == 2)
                    {
                        sqlDataReader.NextResult();
                        while (sqlDataReader.Read())
                        {
                            ProfessionalCompetencies = new ProfessionalCompetencies();
                            ProfessionalCompetencies.No= Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                            //*** F5
                            //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                            ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                            ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                        }
                        sqlDataReader.NextResult();
                        while (sqlDataReader.Read())
                        {
                            BehavioralCompetencies = new BehavioralCompetencies();
                            BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                            //*** F5
                            //BehavioralCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                            BehavioralCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                            BehavioralCompetenciesList.Add(BehavioralCompetencies);
                        }
                    }

                }
                annualAppraisalSettingModel.MidYearAppraisalFormModel = midYearAppraisalFormModel;
                annualAppraisalSettingModel.BusinessTargetMidYearAppraisalList = businessTargetMidYearAppraisalList;
                annualAppraisalSettingModel.MidYearAppraisalDetailsModel = midYearAppraisalDetails;
                annualAppraisalSettingModel.ProfessionalCompetencies = ProfessionalCompetenciesList;
                annualAppraisalSettingModel.BehavioralCompetencies = BehavioralCompetenciesList;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return annualAppraisalSettingModel;
        }

        public OperationDetails SaveMidYearAppraisalFormData(IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings, int? userId, MidYearAppraisalForm midYearAppraisalFormModel)
        {
            DataTable midYearTargetSettings = new DataTable();
            midYearTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            midYearTargetSettings.Columns.Add("Weight", typeof(int));
            midYearTargetSettings.Columns.Add("PerformanceGroupId", typeof(int));

            foreach (var item in businessTargetMidYearAppraisalSettings)
            {
                midYearTargetSettings.Rows.Add(item.BusinessTargetId, item.BusinessTargetDetails, item.Weight, item.PerformanceGroupId);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveMidYearAppraisalFormData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MidYearAppraisalFormModelId", midYearAppraisalFormModel.ID);
                sqlCommand.Parameters.AddWithValue("@MidYearBusinessTargetSettings", midYearTargetSettings);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", midYearAppraisalFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", midYearAppraisalFormModel.PerformanceGroupId);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsComments", midYearAppraisalFormModel.BusinessTargetsComments);
                sqlCommand.Parameters.AddWithValue("@CoreCompetenciesComments", midYearAppraisalFormModel.CoreCompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", midYearAppraisalFormModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", midYearAppraisalFormModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", midYearAppraisalFormModel.LineManagerEvaluationComments);
                sqlCommand.Parameters.AddWithValue("@TrainingNeeds", midYearAppraisalFormModel.TrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", midYearAppraisalFormModel.LineManagerMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", midYearAppraisalFormModel.EmployeeMidYearPerformanceComments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails SaveFullYearAppraisalFormData(IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings, List<ProfessionalCompetencies> ProfessionalCompetencies, List<BehavioralCompetencies> BehavioralCompetencies, int? userId, AnnualAppraisalProcessModel AnnualAppraisalProcessModel)
        {
            DataTable midYearTargetSettings = new DataTable();
            midYearTargetSettings.Columns.Add("BusinessTargetNo", typeof(int));
            midYearTargetSettings.Columns.Add("BusinessTargetDetails", typeof(string));
            midYearTargetSettings.Columns.Add("Accomplishments", typeof(string));
            midYearTargetSettings.Columns.Add("Weight", typeof(int));
            midYearTargetSettings.Columns.Add("Rating", typeof(int));
            midYearTargetSettings.Columns.Add("Score", typeof(int));

            foreach (var item in businessTargetMidYearAppraisalSettings)
            {
                midYearTargetSettings.Rows.Add(item.BusinessTargetId, item.BusinessTargetDetails, item.Accomplishments, item.Weight, item.Rating, (item.Weight* item.Rating));
            }

            DataTable dtProfessionalCompetencies = new DataTable();
            dtProfessionalCompetencies.Columns.Add("ProfessionalCompetenciesNo", typeof(int));
            dtProfessionalCompetencies.Columns.Add("Rating", typeof(string));
           
            foreach (var item in ProfessionalCompetencies)
            {
                dtProfessionalCompetencies.Rows.Add(item.No,  item.Score);
            }

            DataTable dtBehavioralCompetencies = new DataTable();
            dtBehavioralCompetencies.Columns.Add("BehavioralCompetenciesNo", typeof(int));
            dtBehavioralCompetencies.Columns.Add("Rating", typeof(string));

            foreach (var item in BehavioralCompetencies)
            {
                dtBehavioralCompetencies.Rows.Add(item.No, item.Score);
            }

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePDRPFullYearAppraisalFormData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MidYearAppraisalFormModelId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.ID);


                sqlCommand.Parameters.AddWithValue("@PDRPBusinessTargetsFullYearAppraisalTY", midYearTargetSettings);
                sqlCommand.Parameters.AddWithValue("@PDRPProfessionalCompetenciesTY", dtProfessionalCompetencies);
                sqlCommand.Parameters.AddWithValue("@PDRPBehavioralCompetenciesTY", dtBehavioralCompetencies);


                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.PerformanceGroupId);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsComments);
                sqlCommand.Parameters.AddWithValue("@CoreCompetenciesComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CoreCompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CompanyId);

                sqlCommand.Parameters.AddWithValue("@LineManagerEvaluationComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerEvaluationComments);
                sqlCommand.Parameters.AddWithValue("@TrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.TrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerMidYearPerformanceComments);
                sqlCommand.Parameters.AddWithValue("@EmployeeMidYearPerformanceComments", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeMidYearPerformanceComments);

                sqlCommand.Parameters.AddWithValue("@MyThreeKeyAchivements", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyThreeKeyAchivements);
                sqlCommand.Parameters.AddWithValue("@MyTwoDevelopmentAreas", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.MyTwoDevelopmentAreas);
                sqlCommand.Parameters.AddWithValue("@EmployeeCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.EmployeeCommentsonFullYearPerformance);
                sqlCommand.Parameters.AddWithValue("@Strengths", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.Strengths);
                sqlCommand.Parameters.AddWithValue("@FullTrainingNeeds", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FullTrainingNeeds);
                sqlCommand.Parameters.AddWithValue("@LineManagerCommentsonFullYearPerformance", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.LineManagerCommentsonFullYearPerformance);

                sqlCommand.Parameters.AddWithValue("@BusinessTargetsTota", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BusinessTargetsTota);
                sqlCommand.Parameters.AddWithValue("@ProfessionalCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.ProfessionalCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@BehavioralCompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.BehavioralCompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@CompetenciesAVG", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.CompetenciesAVG);
                sqlCommand.Parameters.AddWithValue("@OverallAppraisalScore", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.OverallAppraisalScore);
                sqlCommand.Parameters.AddWithValue("@FormSubmit", AnnualAppraisalProcessModel.MidYearAppraisalFormModel.FormSubmit);
                sqlCommand.Parameters.AddWithValue("@RequestId", AnnualAppraisalProcessModel.RequestId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion

        #region Save Forms Approval for Employee sign of forms
        public OperationDetails SaveFormApprovalForEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? annualAppraisalProcessId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveFormApprovalForEmployeeSignOffMidYearAppraisalForms", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@MidYearAppraisalFormModelId", id);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@AnnualAppraisalProcessId", annualAppraisalProcessId);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Submitted successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion

    }
}
