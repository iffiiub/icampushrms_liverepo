﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.DataAccess.PDRP
{
    public class GoalSettingDB : PDRPCommonModelDB
    {
        #region Goal Setting
       
        
        
        public OperationDetails SaveGoalSettingsData(IEnumerable<GoalSettingModel> goalformSettings, int userId, int companyId)
        {
            DataTable goalSettings = new DataTable();
            goalSettings.Columns.Add("EmployeeID", typeof(int));
            goalSettings.Columns.Add("PerformancegroupId", typeof(int));
            goalSettings.Columns.Add("YearId", typeof(int));
            goalSettings.Columns.Add("SettingGoalsStartDate", typeof(DateTime));
            goalSettings.Columns.Add("SettingGoalsDueDate", typeof(DateTime));
            goalSettings.Columns.Add("ProcessName", typeof(string));

            foreach (var item in goalformSettings)
            {
                goalSettings.Rows.Add(item.EmployeeID, item.PerformanceGroupId, item.YearId,
                    DateTime.ParseExact(item.SettingGoalsStartDate, HRMSDateFormat, CultureInfo.InvariantCulture), //*** F17 Date Format in server
                    DateTime.ParseExact(item.SettingGoalsDueDate, HRMSDateFormat, CultureInfo.InvariantCulture), item.ProcessName);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                //sqlCommand = new SqlCommand("HR_Stp_SaveGoalSettings", sqlConnection);
                //sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //sqlCommand.Parameters.AddWithValue("@GoalInitializationFormSettings", goalSettings);
                //sqlCommand.Parameters.AddWithValue("@UserId", userId);
                //sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                //SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter[] parameters =
                  {
                         new SqlParameter("@GoalInitializationFormSettings",goalSettings),
                         new SqlParameter("@UserId",userId),
                         new SqlParameter("@CompanyId",companyId),
                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                 "HR_Stp_PDRPSaveGoalSettings", parameters);

                //sqlCommand.ExecuteNonQuery();
                string Result = "";
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Result = reader["Result"].ToString();
                        if (Result != "SUCCESS")
                        {
                            operationDetails.Success = false;
                            operationDetails.CssClass = "error";
                            operationDetails.Message = "Line Manager is not set for Following Employees:</br>" + Result;
                        }
                        else
                        {
                            //Result = reader["FormProcessID"].ToString();
                            //Result = reader["GroupID"].ToString();
                            //Result = reader["ApproverID"].ToString();
                            //Result = reader["RequestID"].ToString();

                            //dtResult.Rows.Add(reader["FormProcessID"].ToString());

                            operationDetails.Success = true;
                            operationDetails.CssClass = "success";
                            operationDetails.Message = "The request(s) are generated successfully.";
                        }

                    }
                }
                sqlConnection.Close();
                //operationDetails.Success = true;
                //operationDetails.Message = "Updated successfully";
                //operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public GoalSettingModel GetGoalSettingFormDataByFormProcessId(int? formprocessId)
        {
            GoalSettingModel goalSettingModel = new GoalSettingModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormDataByFormProcessId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formprocessId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        goalSettingModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        goalSettingModel.RequestId = Convert.ToInt32(sqlDataReader["RequestId"] == DBNull.Value ? "0" : sqlDataReader["RequestId"].ToString());
                        goalSettingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        goalSettingModel.SettingGoalsStartDate = Convert.ToString(sqlDataReader["SettingGoalsStartDate"] == DBNull.Value ? "" : sqlDataReader["SettingGoalsStartDate"]);
                        goalSettingModel.SettingGoalsDueDate = Convert.ToString(sqlDataReader["SettingGoalsDueDate"] == DBNull.Value ? "" : sqlDataReader["SettingGoalsDueDate"]);
                        goalSettingModel.ReqStatusId = Convert.ToInt32(sqlDataReader["ReqStatusId"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusId"].ToString());
                        goalSettingModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        goalSettingModel.IsSubmitForLM = Convert.ToBoolean(sqlDataReader["IsSubmitForLM"] == DBNull.Value ? "0" : sqlDataReader["IsSubmitForLM"].ToString());
                        goalSettingModel.IsSignOffForm = Convert.ToBoolean(sqlDataReader["IsSignOffForm"] == DBNull.Value ? "0" : sqlDataReader["IsSignOffForm"].ToString());
                        goalSettingModel.GoalSettingId = Convert.ToInt32(sqlDataReader["GoalSettingId"] == DBNull.Value ? "0" : sqlDataReader["GoalSettingId"].ToString());
                        goalSettingModel.FormMode = Convert.ToInt32(sqlDataReader["FormMode"] == DBNull.Value ? 0 : sqlDataReader["FormMode"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModel;
        }
        public GoalSettingModel GetGoalSettingFormDataByFormProcessId(int? formprocessId, int FormID)
        {
            GoalSettingModel goalSettingModel = new GoalSettingModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormDataByFormProcessId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formprocessId);
                sqlCommand.Parameters.AddWithValue("@FormID", FormID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        goalSettingModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        goalSettingModel.RequestId = Convert.ToInt32(sqlDataReader["RequestId"] == DBNull.Value ? "0" : sqlDataReader["RequestId"].ToString());
                        goalSettingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        goalSettingModel.SettingGoalsStartDate = Convert.ToString(sqlDataReader["SettingGoalsStartDate"] == DBNull.Value ? "" : sqlDataReader["SettingGoalsStartDate"]);
                        goalSettingModel.SettingGoalsDueDate = Convert.ToString(sqlDataReader["SettingGoalsDueDate"] == DBNull.Value ? "" : sqlDataReader["SettingGoalsDueDate"]);
                        goalSettingModel.ReqStatusId = Convert.ToInt32(sqlDataReader["ReqStatusId"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusId"].ToString());
                        goalSettingModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        goalSettingModel.IsSubmitForLM = Convert.ToBoolean(sqlDataReader["IsSubmitForLM"] == DBNull.Value ? "0" : sqlDataReader["IsSubmitForLM"].ToString());
                        goalSettingModel.IsSignOffForm = Convert.ToBoolean(sqlDataReader["IsSignOffForm"] == DBNull.Value ? "0" : sqlDataReader["IsSignOffForm"].ToString());
                        goalSettingModel.GoalSettingId = Convert.ToInt32(sqlDataReader["GoalSettingId"] == DBNull.Value ? "0" : sqlDataReader["GoalSettingId"].ToString());
                        //***F9
                        goalSettingModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        goalSettingModel.CurrCompanyId = Convert.ToInt32(sqlDataReader["CurrCompanyId"] == DBNull.Value ? "0" : sqlDataReader["CurrCompanyId"].ToString());

                        //*** F12, F13
                        //if (FormID == 36 || FormID == 37 || FormID == 38 || FormID == 41 || FormID == 42)
                        //{
                            goalSettingModel.Comments = Convert.ToString(sqlDataReader["Comments"] == DBNull.Value ? "" : sqlDataReader["Comments"].ToString());
                        //}
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModel;
        }
        public IEnumerable<GoalSettingOverAllRatingScalesModel> GetOverAllRatingScalesDetails(int YearID, int CompanyID, int GoalSettingId, int PerformanceGroupId)
        {
            IList<GoalSettingOverAllRatingScalesModel> overAllRatingSclaesList = null;
            try
            {
                overAllRatingSclaesList = new List<GoalSettingOverAllRatingScalesModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetOverAllRatingScalesData", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", PerformanceGroupId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    GoalSettingOverAllRatingScalesModel overAllRatingSclaeModel;
                    while (sqlDataReader.Read())
                    {
                        overAllRatingSclaeModel = new GoalSettingOverAllRatingScalesModel();
                        overAllRatingSclaeModel.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"] == DBNull.Value ? "0" : sqlDataReader["RatingScaleID"].ToString());
                        overAllRatingSclaeModel.RatingScaleCharacter = Convert.ToString(sqlDataReader["RatingScaleCharacter"] == DBNull.Value ? "" : sqlDataReader["RatingScaleCharacter"].ToString());
                        overAllRatingSclaeModel.RatingScaleNumber = Convert.ToInt32(sqlDataReader["RatingScaleNumber"] == DBNull.Value ? "" : sqlDataReader["RatingScaleNumber"]);
                        overAllRatingSclaeModel.FinalScoreSlabMIN = Convert.ToDecimal(sqlDataReader["FinalScoreSlabMIN"] == DBNull.Value ? "" : sqlDataReader["FinalScoreSlabMIN"]);
                        overAllRatingSclaeModel.FinalScoreSlabMAX = Convert.ToDecimal(sqlDataReader["FinalScoreSlabMAX"] == DBNull.Value ? "" : sqlDataReader["FinalScoreSlabMAX"]);
                        overAllRatingSclaeModel.DefinitionName = Convert.ToString(sqlDataReader["DefinitionName"] == DBNull.Value ? "" : sqlDataReader["DefinitionName"]);
                        overAllRatingSclaeModel.DefinitionDetails = Convert.ToString(sqlDataReader["DefinitionDetails"] == DBNull.Value ? "" : sqlDataReader["DefinitionDetails"]);
                        overAllRatingSclaeModel.ScoreSlab = Convert.ToString(sqlDataReader["ScoreSlab"] == DBNull.Value ? "" : sqlDataReader["ScoreSlab"]);
                        overAllRatingSclaesList.Add(overAllRatingSclaeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return overAllRatingSclaesList;
        }
        #endregion





        #region Annual Goal Setting Form A        
        public GoalSettingModel GetGoalSettingFormAModelDetails(int formProcessID, int? employeeId, int CompanyID, int YearID,int FormNow)
        {
            GoalSettingModel goalSettingModel = new GoalSettingModel();
            AnnualGoalSettingFormAModel goalSettingFormAModel = new AnnualGoalSettingFormAModel();
            AnnualGoalSettingFormAModel goalSettingBusinessTargetFormAModel = null;
            IList<AnnualGoalSettingFormAModel> goalSettingFormAModelList = null;

            ProfessionalCompetencies ProfessionalCompetencies;
            List<ProfessionalCompetencies> ProfessionalCompetenciesList = new List<ProfessionalCompetencies>();

            BehavioralCompetencies BehavioralCompetencies;
            List<BehavioralCompetencies> BehavioralCompetenciesList = new List<BehavioralCompetencies>();
            try
            {
                goalSettingFormAModelList = new List<AnnualGoalSettingFormAModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormAModelDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                //*** F3
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessID);
                sqlCommand.Parameters.AddWithValue("@FormNow", FormNow);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        goalSettingFormAModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        goalSettingFormAModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingFormAModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        goalSettingFormAModel.BusinesstargetComments = Convert.ToString(sqlDataReader["BusinessTargetsComments"] == DBNull.Value ? "" : sqlDataReader["BusinessTargetsComments"]);
                        goalSettingFormAModel.CompetenciesComments = Convert.ToString(sqlDataReader["CoreCompetenciesComments"] == DBNull.Value ? "" : sqlDataReader["CoreCompetenciesComments"]);
                        goalSettingFormAModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        goalSettingFormAModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        goalSettingFormAModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingFormAModel.JobGrade = Convert.ToString(sqlDataReader["Grade"] == DBNull.Value ? "" : sqlDataReader["Grade"].ToString());
                        goalSettingFormAModel.FormState = Convert.ToString(sqlDataReader["FormState"] == DBNull.Value ? "" : sqlDataReader["FormState"].ToString());
                        goalSettingModel.FormMode = Convert.ToInt32((sqlDataReader["FormState"] == DBNull.Value || sqlDataReader["FormState"].ToString() == "") ? 0 : sqlDataReader["FormState"]);
                        //*** F3
                        goalSettingModel.FormMode = Convert.ToInt32((sqlDataReader["FormState"] == DBNull.Value || sqlDataReader["FormState"].ToString() == "") ? 0 : sqlDataReader["FormState"]);
                        goalSettingModel.MidFormMode = Convert.ToInt32((sqlDataReader["MidFormState"] == DBNull.Value || sqlDataReader["MidFormState"].ToString() == "") ? 0 : sqlDataReader["MidFormState"]);
                        goalSettingModel.IsMidSignOffForm = Convert.ToBoolean(sqlDataReader["IsMidSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsMidSignOffForm"]);
                        goalSettingModel.FullFormMode = Convert.ToInt32((sqlDataReader["FullFormState"] == DBNull.Value || sqlDataReader["FullFormState"].ToString() == "") ? 0 : sqlDataReader["FullFormState"]);
                        goalSettingModel.IsFullSignOffForm = Convert.ToBoolean(sqlDataReader["IsFullSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsFullSignOffForm"]);

                        goalSettingFormAModel.LineManagerEvaluationComments = Convert.ToString(sqlDataReader["LineManagerEvaluationComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerEvaluationComments"].ToString());
                        goalSettingFormAModel.TrainingNeeds = Convert.ToString(sqlDataReader["TrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["TrainingNeeds"].ToString());
                        goalSettingFormAModel.LineManagerMidYearPerformanceComments = Convert.ToString(sqlDataReader["LineManagerMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerMidYearPerformanceComments"].ToString());
                        goalSettingFormAModel.EmployeeMidYearPerformanceComments = Convert.ToString(sqlDataReader["EmployeeMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["EmployeeMidYearPerformanceComments"].ToString());

                        goalSettingFormAModel.MyThreeKeyAchivements = Convert.ToString(sqlDataReader["MyThreeKeyAchivements"] == DBNull.Value ? "" : sqlDataReader["MyThreeKeyAchivements"].ToString());
                        goalSettingFormAModel.MyTwoDevelopmentAreas = Convert.ToString(sqlDataReader["MyTwoDevelopmentAreas"] == DBNull.Value ? "" : sqlDataReader["MyTwoDevelopmentAreas"].ToString());
                        goalSettingFormAModel.EmployeeCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["EmployeeCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["EmployeeCommentsonFullYearPerformance"].ToString());
                        goalSettingFormAModel.Strengths = Convert.ToString(sqlDataReader["Strengths"] == DBNull.Value ? "" : sqlDataReader["Strengths"].ToString());
                        goalSettingFormAModel.FullTrainingNeeds = Convert.ToString(sqlDataReader["FullTrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["FullTrainingNeeds"].ToString());
                        goalSettingFormAModel.LineManagerCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["LineManagerCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["LineManagerCommentsonFullYearPerformance"].ToString());

                        goalSettingFormAModel.BusinessTargetsTota = Convert.ToDecimal(sqlDataReader["BusinessTargetsTota"] == DBNull.Value ? 0 : sqlDataReader["BusinessTargetsTota"]);
                        goalSettingFormAModel.ProfessionalCompetenciesAVG = Convert.ToDecimal(sqlDataReader["ProfessionalCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["ProfessionalCompetenciesAVG"]);
                        goalSettingFormAModel.BehavioralCompetenciesAVG = Convert.ToDecimal(sqlDataReader["BehavioralCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["BehavioralCompetenciesAVG"]);
                        goalSettingFormAModel.CompetenciesAVG = Convert.ToDecimal(sqlDataReader["CompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["CompetenciesAVG"]);
                        goalSettingFormAModel.OverallAppraisalScore = Convert.ToDecimal(sqlDataReader["OverallAppraisalScore"] == DBNull.Value ? 0 : sqlDataReader["OverallAppraisalScore"]);
                        goalSettingModel.OverallScoreFormula = Convert.ToString(sqlDataReader["OverallScoreFormula"] == DBNull.Value ? "" : sqlDataReader["OverallScoreFormula"].ToString());
                        //*** F2
                        goalSettingFormAModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        goalSettingFormAModel.Company = Convert.ToString(sqlDataReader["Company"]);
                        goalSettingFormAModel.Designation = Convert.ToString(sqlDataReader["Designation"]);
                        goalSettingFormAModel.RequestEmail = Convert.ToString(sqlDataReader["RequestEmail"]);
                        goalSettingFormAModel.Department = Convert.ToString(sqlDataReader["Department"]);
                        goalSettingFormAModel.LineManager = Convert.ToString(sqlDataReader["LineManager"]);
                        goalSettingFormAModel.DOJ = Convert.ToString(sqlDataReader["DOJ"]);

                        //***F10
                        goalSettingModel.IsReversed = Convert.ToString(sqlDataReader["IsReversed"] == DBNull.Value ? null : sqlDataReader["IsReversed"]);
                        goalSettingModel.MidIsReversed = Convert.ToString(sqlDataReader["MidIsReversed"] == DBNull.Value ? null : sqlDataReader["MidIsReversed"]);
                        goalSettingModel.FullIsReversed = Convert.ToString(sqlDataReader["FullIsReversed"] == DBNull.Value ? null : sqlDataReader["FullIsReversed"]);
                        goalSettingModel.GolReqStatusID = Convert.ToInt32(sqlDataReader["GolReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["GolReqStatusID"].ToString());
                        goalSettingModel.MidReqStatusID = Convert.ToInt32(sqlDataReader["MidReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["MidReqStatusID"].ToString());
                        goalSettingModel.FullReqStatusID = Convert.ToInt32(sqlDataReader["FullReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["FullReqStatusID"].ToString());
                    }

                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        goalSettingBusinessTargetFormAModel = new AnnualGoalSettingFormAModel();
                        goalSettingBusinessTargetFormAModel.BusinessTargetId = Convert.ToInt32(sqlDataReader["BusinessTargetId"] == DBNull.Value ? "0" : sqlDataReader["BusinessTargetId"].ToString());
                        goalSettingBusinessTargetFormAModel.BusinessTargetDetails = Convert.ToString(sqlDataReader["BusinessTargetDetails"] == DBNull.Value ? "0" : sqlDataReader["BusinessTargetDetails"].ToString());
                        goalSettingBusinessTargetFormAModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? null : sqlDataReader["Weight"]);
                        if (goalSettingBusinessTargetFormAModel.Weight == 0)
                            goalSettingBusinessTargetFormAModel.Weight = null;
                        //goalSettingBusinessTargetFormAModel.Rating = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? 0 : sqlDataReader["Rating"]);
                        //*** F5                        
                        goalSettingBusinessTargetFormAModel.Rating = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                        goalSettingBusinessTargetFormAModel.Score = Convert.ToDecimal(sqlDataReader["Score"] == DBNull.Value ? 0.00 : sqlDataReader["Score"]);
                        goalSettingBusinessTargetFormAModel.Accomplishments = Convert.ToString(sqlDataReader["Accomplishments"] == DBNull.Value ? "" : sqlDataReader["Accomplishments"].ToString());
                        goalSettingFormAModelList.Add(goalSettingBusinessTargetFormAModel);
                    }



                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }
                    //sqlDataReader.NextResult();
                    //while (sqlDataReader.Read())
                    //{
                    //    BehavioralCompetencies = new BehavioralCompetencies();
                    //    BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                    //    BehavioralCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                    //    BehavioralCompetenciesList.Add(BehavioralCompetencies);
                    //}

                }
                else
                {
                    sqlDataReader.NextResult();
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }
                }
                goalSettingModel.FormABusinessTargetsModelList = goalSettingFormAModelList;
                goalSettingModel.FormABusinessTargetsModel = goalSettingFormAModel;

                goalSettingModel.ProfessionalCompetencies = ProfessionalCompetenciesList;
                goalSettingModel.BehavioralCompetencies = BehavioralCompetenciesList;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModel;
        }

        public OperationDetails SaveAnnualGoalSettingsFormAData(IEnumerable<AnnualGoalSettingFormAModel> goalSettingBusinnessTargetsSettings, IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel, int? userId, AnnualGoalSettingFormAModel businessTargetModel, int formId)
        {
            //*** F2
            DataTable RatingScaleTable = new DataTable();
            RatingScaleTable.Columns.Add("RatingScaleCharacter");
            RatingScaleTable.Columns.Add("RatingScaleNumber");
            RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
            RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
            RatingScaleTable.Columns.Add("DefinitionName");
            RatingScaleTable.Columns.Add("DefinitionDetails");
            foreach (GoalSettingOverAllRatingScalesModel Level in GoalSettingOverAllRatingScalesModel)
            {

                RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails);

            }

            DataTable businessTargetSettings = new DataTable();
            businessTargetSettings.Columns.Add("BusinessTargetNo", typeof(int));
            businessTargetSettings.Columns.Add("BusinessTargetId", typeof(int));
            businessTargetSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            businessTargetSettings.Columns.Add("Weight", typeof(int));

            foreach (var item in goalSettingBusinnessTargetsSettings)
            {
                businessTargetSettings.Rows.Add(item.BusinessTargetNo, item.BusinessTargetId, item.BusinessTargetDetails, item.Weight);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPSaveGoalSettingsFormAData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormAID", businessTargetModel.ID);
                sqlCommand.Parameters.AddWithValue("@BusinessTargetsFormASettings", businessTargetSettings);
                sqlCommand.Parameters.AddWithValue("@RatingScaleTable", RatingScaleTable); //*** F2
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", businessTargetModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@BusinesstargetComments", businessTargetModel.BusinesstargetComments);
                sqlCommand.Parameters.AddWithValue("@CompetenciesComments", businessTargetModel.CompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", businessTargetModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", businessTargetModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", businessTargetModel.JobGrade);
                sqlCommand.Parameters.AddWithValue("@FormState", businessTargetModel.FormState);
                //*** F2
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", businessTargetModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@EmployeeName", businessTargetModel.EmployeeName);
                sqlCommand.Parameters.AddWithValue("@Company", businessTargetModel.Company);
                sqlCommand.Parameters.AddWithValue("@Designation", businessTargetModel.Designation);
                sqlCommand.Parameters.AddWithValue("@RequestEmail", businessTargetModel.RequestEmail);
                sqlCommand.Parameters.AddWithValue("@Department", businessTargetModel.Department);
                sqlCommand.Parameters.AddWithValue("@LineManager", businessTargetModel.LineManager);
                sqlCommand.Parameters.AddWithValue("@DOJ", businessTargetModel.DOJ); //*** F17 Date Format in server
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", businessTargetModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
               
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion

        #region Annual Goal Setting Form B
        public GoalSettingModel GetGoalSettingFormBModelDetails(int formProcessID, int? employeeId, int CompanyID, int YearID, int FormNow)
        {
            GoalSettingModel goalSettingModel = new GoalSettingModel();
            AnnualGoalSettingFormBModel goalSettingFormBModel = new AnnualGoalSettingFormBModel();
            AnnualGoalSettingFormBModel goalSettingKPIFormBModel = null;
            IList<AnnualGoalSettingFormBModel> goalSettingFormBModelList = null;

            ProfessionalCompetencies ProfessionalCompetencies;
            List<ProfessionalCompetencies> ProfessionalCompetenciesList = new List<ProfessionalCompetencies>();

            BehavioralCompetencies BehavioralCompetencies;
            List<BehavioralCompetencies> BehavioralCompetenciesList = new List<BehavioralCompetencies>();
            try
            {
                goalSettingFormBModelList = new List<AnnualGoalSettingFormBModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormBModelDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                //*** F3
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessID);
                sqlCommand.Parameters.AddWithValue("@FormNow", FormNow);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        goalSettingFormBModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        goalSettingFormBModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingFormBModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        goalSettingFormBModel.KPIsComments = Convert.ToString(sqlDataReader["KPIsComments"] == DBNull.Value ? "" : sqlDataReader["KPIsComments"]);
                        goalSettingFormBModel.CompetenciesComments = Convert.ToString(sqlDataReader["CoreCompetenciesComments"] == DBNull.Value ? "" : sqlDataReader["CoreCompetenciesComments"]);
                        goalSettingFormBModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        goalSettingFormBModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        goalSettingFormBModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingFormBModel.JobGrade = Convert.ToString(sqlDataReader["Grade"] == DBNull.Value ? "" : sqlDataReader["Grade"].ToString());
                        goalSettingFormBModel.FormState = Convert.ToString(sqlDataReader["FormState"] == DBNull.Value ? "" : sqlDataReader["FormState"].ToString());
                        goalSettingModel.FormMode = Convert.ToInt32(sqlDataReader["FormState"] == DBNull.Value ? 0 : sqlDataReader["FormState"]);
                        //*** F3
                        goalSettingModel.FormMode = Convert.ToInt32((sqlDataReader["FormState"] == DBNull.Value || sqlDataReader["FormState"].ToString() == "") ? 0 : sqlDataReader["FormState"]);
                        goalSettingModel.MidFormMode = Convert.ToInt32((sqlDataReader["MidFormState"] == DBNull.Value || sqlDataReader["MidFormState"].ToString() == "") ? 0 : sqlDataReader["MidFormState"]);
                        goalSettingModel.IsMidSignOffForm = Convert.ToBoolean(sqlDataReader["IsMidSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsMidSignOffForm"]);
                        goalSettingModel.FullFormMode = Convert.ToInt32((sqlDataReader["FullFormState"] == DBNull.Value || sqlDataReader["FullFormState"].ToString() == "") ? 0 : sqlDataReader["FullFormState"]);
                        goalSettingModel.IsFullSignOffForm = Convert.ToBoolean(sqlDataReader["IsFullSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsFullSignOffForm"]);

                        goalSettingFormBModel.LineManagerEvaluationComments = Convert.ToString(sqlDataReader["LineManagerEvaluationComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerEvaluationComments"].ToString());
                        goalSettingFormBModel.TrainingNeeds = Convert.ToString(sqlDataReader["TrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["TrainingNeeds"].ToString());
                        goalSettingFormBModel.LineManagerMidYearPerformanceComments = Convert.ToString(sqlDataReader["LineManagerMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerMidYearPerformanceComments"].ToString());
                        goalSettingFormBModel.EmployeeMidYearPerformanceComments = Convert.ToString(sqlDataReader["EmployeeMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["EmployeeMidYearPerformanceComments"].ToString());

                        goalSettingFormBModel.MyThreeKeyAchivements = Convert.ToString(sqlDataReader["MyThreeKeyAchivements"] == DBNull.Value ? "" : sqlDataReader["MyThreeKeyAchivements"].ToString());
                        goalSettingFormBModel.MyTwoDevelopmentAreas = Convert.ToString(sqlDataReader["MyTwoDevelopmentAreas"] == DBNull.Value ? "" : sqlDataReader["MyTwoDevelopmentAreas"].ToString());
                        goalSettingFormBModel.EmployeeCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["EmployeeCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["EmployeeCommentsonFullYearPerformance"].ToString());
                        goalSettingFormBModel.Strengths = Convert.ToString(sqlDataReader["Strengths"] == DBNull.Value ? "" : sqlDataReader["Strengths"].ToString());
                        goalSettingFormBModel.FullTrainingNeeds = Convert.ToString(sqlDataReader["FullTrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["FullTrainingNeeds"].ToString());
                        goalSettingFormBModel.LineManagerCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["LineManagerCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["LineManagerCommentsonFullYearPerformance"].ToString());

                        goalSettingFormBModel.BusinessTargetsTota = Convert.ToDecimal(sqlDataReader["BusinessTargetsTota"] == DBNull.Value ? 0 : sqlDataReader["BusinessTargetsTota"]);
                        goalSettingFormBModel.ProfessionalCompetenciesAVG = Convert.ToDecimal(sqlDataReader["ProfessionalCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["ProfessionalCompetenciesAVG"]);
                        goalSettingFormBModel.BehavioralCompetenciesAVG = Convert.ToDecimal(sqlDataReader["BehavioralCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["BehavioralCompetenciesAVG"]);
                        goalSettingFormBModel.CompetenciesAVG = Convert.ToDecimal(sqlDataReader["CompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["CompetenciesAVG"]);
                        goalSettingFormBModel.OverallAppraisalScore = Convert.ToDecimal(sqlDataReader["OverallAppraisalScore"] == DBNull.Value ? 0 : sqlDataReader["OverallAppraisalScore"]);
                        goalSettingModel.OverallScoreFormula = Convert.ToString(sqlDataReader["OverallScoreFormula"] == DBNull.Value ? "" : sqlDataReader["OverallScoreFormula"].ToString());



                        //*** F2
                        goalSettingFormBModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        goalSettingFormBModel.Company = Convert.ToString(sqlDataReader["Company"]);
                        goalSettingFormBModel.Designation = Convert.ToString(sqlDataReader["Designation"]);
                        goalSettingFormBModel.RequestEmail = Convert.ToString(sqlDataReader["RequestEmail"]);
                        goalSettingFormBModel.Department = Convert.ToString(sqlDataReader["Department"]);                
                        goalSettingFormBModel.LineManager    = Convert.ToString(sqlDataReader["LineManager"]);
                        goalSettingFormBModel.DOJ = Convert.ToString(sqlDataReader["DOJ"]);

                        //***F10
                        goalSettingModel.IsReversed = Convert.ToString(sqlDataReader["IsReversed"] == DBNull.Value ? null : sqlDataReader["IsReversed"]);
                        goalSettingModel.MidIsReversed = Convert.ToString(sqlDataReader["MidIsReversed"] == DBNull.Value ? null : sqlDataReader["MidIsReversed"]);
                        goalSettingModel.FullIsReversed = Convert.ToString(sqlDataReader["FullIsReversed"] == DBNull.Value ? null : sqlDataReader["FullIsReversed"]);
                        goalSettingModel.GolReqStatusID = Convert.ToInt32(sqlDataReader["GolReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["GolReqStatusID"].ToString());
                        goalSettingModel.MidReqStatusID = Convert.ToInt32(sqlDataReader["MidReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["MidReqStatusID"].ToString());
                        goalSettingModel.FullReqStatusID = Convert.ToInt32(sqlDataReader["FullReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["FullReqStatusID"].ToString());

                    }

                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        goalSettingKPIFormBModel = new AnnualGoalSettingFormBModel();
                        goalSettingKPIFormBModel.KPIId = Convert.ToInt32(sqlDataReader["KPIId"] == DBNull.Value ? "0" : sqlDataReader["KPIId"].ToString());
                        goalSettingKPIFormBModel.BusinessTargetDetails = Convert.ToString(sqlDataReader["BusinessTargetDetails"] == DBNull.Value ? "0" : sqlDataReader["BusinessTargetDetails"].ToString());
                        //goalSettingKPIFormBModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? 0 : sqlDataReader["Weight"]);
                        goalSettingKPIFormBModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? null : sqlDataReader["Weight"]);
                        if (goalSettingKPIFormBModel.Weight == 0)
                            goalSettingKPIFormBModel.Weight = null;
                        //goalSettingKPIFormBModel.Rating = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? 0 : sqlDataReader["Rating"]);
                        //*** F5                        
                        goalSettingKPIFormBModel.Rating = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        goalSettingKPIFormBModel.Score = Convert.ToDecimal(sqlDataReader["Score"] == DBNull.Value ? 0.00 : sqlDataReader["Score"]);
                        goalSettingKPIFormBModel.Accomplishments = Convert.ToString(sqlDataReader["Accomplishments"] == DBNull.Value ? "" : sqlDataReader["Accomplishments"].ToString());
                        goalSettingFormBModelList.Add(goalSettingKPIFormBModel);
                    }



                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }

                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        BehavioralCompetencies = new BehavioralCompetencies();
                        BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                        //*** F5
                        //BehavioralCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        BehavioralCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        BehavioralCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        BehavioralCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        BehavioralCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        BehavioralCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        BehavioralCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        BehavioralCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        BehavioralCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        BehavioralCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        BehavioralCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        BehavioralCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        BehavioralCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        BehavioralCompetenciesList.Add(BehavioralCompetencies);
                    }

                }
                else
                {
                    sqlDataReader.NextResult();
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        BehavioralCompetencies = new BehavioralCompetencies();
                        BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                        //*** F5
                        //BehavioralCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        BehavioralCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        BehavioralCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        BehavioralCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        BehavioralCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        BehavioralCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        BehavioralCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        BehavioralCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        BehavioralCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        BehavioralCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        BehavioralCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        BehavioralCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        BehavioralCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        BehavioralCompetenciesList.Add(BehavioralCompetencies);

                    }
                }
                goalSettingModel.FormBKPIModelList = goalSettingFormBModelList;
                goalSettingModel.FormBKPIModel = goalSettingFormBModel;

                goalSettingModel.ProfessionalCompetencies = ProfessionalCompetenciesList;
                goalSettingModel.BehavioralCompetencies = BehavioralCompetenciesList;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModel;
        }   
        
        
        public OperationDetails SaveAnnualGoalSettingsFormBData(IEnumerable<AnnualGoalSettingFormBModel> kpiFormBSettings, IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel, int? userId, AnnualGoalSettingFormBModel goalSettingFormBModel, int formId)
        {
            //*** F2
            DataTable RatingScaleTable = new DataTable();
            RatingScaleTable.Columns.Add("RatingScaleCharacter");
            RatingScaleTable.Columns.Add("RatingScaleNumber");
            RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
            RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
            RatingScaleTable.Columns.Add("DefinitionName");
            RatingScaleTable.Columns.Add("DefinitionDetails");
            foreach (GoalSettingOverAllRatingScalesModel Level in GoalSettingOverAllRatingScalesModel)
            {

                RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails);

            }



            DataTable kpiFormBGoalSettings = new DataTable();
            kpiFormBGoalSettings.Columns.Add("BusinessTargetNo", typeof(int));
            kpiFormBGoalSettings.Columns.Add("KPIId", typeof(int));
            kpiFormBGoalSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            kpiFormBGoalSettings.Columns.Add("Weight", typeof(int));

            foreach (var item in kpiFormBSettings)
            {
                kpiFormBGoalSettings.Rows.Add(item.BusinessTargetNo, item.KPIId, item.BusinessTargetDetails, item.Weight);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPSaveGoalSettingsFormBData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormBID", goalSettingFormBModel.ID);
                sqlCommand.Parameters.AddWithValue("@KPIFormBSettings", kpiFormBGoalSettings);
                sqlCommand.Parameters.AddWithValue("@RatingScaleTable", RatingScaleTable); //*** F2
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", goalSettingFormBModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@KPIsComments", goalSettingFormBModel.KPIsComments);
                sqlCommand.Parameters.AddWithValue("@CompetenciesComments", goalSettingFormBModel.CompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", goalSettingFormBModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", goalSettingFormBModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", goalSettingFormBModel.JobGrade);
                sqlCommand.Parameters.AddWithValue("@FormState", goalSettingFormBModel.FormState);
                //*** F2
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingFormBModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@EmployeeName", goalSettingFormBModel.EmployeeName);
                sqlCommand.Parameters.AddWithValue("@Company", goalSettingFormBModel.Company);
                sqlCommand.Parameters.AddWithValue("@Designation", goalSettingFormBModel.Designation);
                sqlCommand.Parameters.AddWithValue("@RequestEmail", goalSettingFormBModel.RequestEmail);
                sqlCommand.Parameters.AddWithValue("@Department", goalSettingFormBModel.Department);
                sqlCommand.Parameters.AddWithValue("@LineManager", goalSettingFormBModel.LineManager);
                sqlCommand.Parameters.AddWithValue("@DOJ", goalSettingFormBModel.DOJ); //*** F17 Date Format in server)
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", goalSettingFormBModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                //if (goalSettingModel.FormBKPIModel.EmployeeName.ApprovalRequesterInfo.RequesterName)
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** F12
                if (goalSettingFormBModel.ID == null || goalSettingFormBModel.ID == 0)
                {
                    goalSettingFormBModel.ID = operationDetails.InsertedRowId;
                }
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion

        #region Annual Goal Setting Form C       
        public GoalSettingModel GetGoalSettingFormCModelDetails(int formProcessID, int? employeeId,int CompanyID,int YearID, int FormNow)
        {
            GoalSettingModel goalSettingModel = new GoalSettingModel();
            AnnualGoalSettingFormCModel goalSettingFormCModel = new AnnualGoalSettingFormCModel();
            AnnualGoalSettingFormCModel goalSettingKPIFormCModel = null;
            IList<AnnualGoalSettingFormCModel> goalSettingFormCModelList = null;

            ProfessionalCompetencies ProfessionalCompetencies;
            List<ProfessionalCompetencies> ProfessionalCompetenciesList = new List<ProfessionalCompetencies>();

            BehavioralCompetencies BehavioralCompetencies;
            List<BehavioralCompetencies> BehavioralCompetenciesList = new List<BehavioralCompetencies>();
            try
            {
                goalSettingFormCModelList = new List<AnnualGoalSettingFormCModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormCModelDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                //*** F3
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessID);
                sqlCommand.Parameters.AddWithValue("@FormNow", FormNow);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        goalSettingFormCModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        goalSettingFormCModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingFormCModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        goalSettingFormCModel.KPIsComments = Convert.ToString(sqlDataReader["KPIsComments"] == DBNull.Value ? "" : sqlDataReader["KPIsComments"]);
                        goalSettingFormCModel.CompetenciesComments = Convert.ToString(sqlDataReader["CoreCompetenciesComments"] == DBNull.Value ? "" : sqlDataReader["CoreCompetenciesComments"]);
                        goalSettingFormCModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        goalSettingFormCModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        goalSettingFormCModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingFormCModel.JobGrade = Convert.ToString(sqlDataReader["Grade"] == DBNull.Value ? "" : sqlDataReader["Grade"].ToString());
                        goalSettingFormCModel.FormState = Convert.ToString(sqlDataReader["FormState"] == DBNull.Value ? "" : sqlDataReader["FormState"].ToString());
                        //*** F3
                        goalSettingModel.FormMode = Convert.ToInt32((sqlDataReader["FormState"] == DBNull.Value || sqlDataReader["FormState"].ToString() == "") ? 0 : sqlDataReader["FormState"]);
                        goalSettingModel.MidFormMode = Convert.ToInt32((sqlDataReader["MidFormState"] == DBNull.Value || sqlDataReader["MidFormState"].ToString() == "") ? 0 : sqlDataReader["MidFormState"]);
                        goalSettingModel.IsMidSignOffForm = Convert.ToBoolean(sqlDataReader["IsMidSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsMidSignOffForm"]);
                        goalSettingModel.FullFormMode = Convert.ToInt32((sqlDataReader["FullFormState"] == DBNull.Value || sqlDataReader["FullFormState"].ToString() == "") ? 0 : sqlDataReader["FullFormState"]);
                        goalSettingModel.IsFullSignOffForm = Convert.ToBoolean(sqlDataReader["IsFullSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsFullSignOffForm"]);

                        goalSettingFormCModel.LineManagerEvaluationComments = Convert.ToString(sqlDataReader["LineManagerEvaluationComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerEvaluationComments"].ToString());
                        goalSettingFormCModel.TrainingNeeds = Convert.ToString(sqlDataReader["TrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["TrainingNeeds"].ToString());
                        goalSettingFormCModel.LineManagerMidYearPerformanceComments = Convert.ToString(sqlDataReader["LineManagerMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerMidYearPerformanceComments"].ToString());
                        goalSettingFormCModel.EmployeeMidYearPerformanceComments = Convert.ToString(sqlDataReader["EmployeeMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["EmployeeMidYearPerformanceComments"].ToString());

                        goalSettingFormCModel.MyThreeKeyAchivements = Convert.ToString(sqlDataReader["MyThreeKeyAchivements"] == DBNull.Value ? "" : sqlDataReader["MyThreeKeyAchivements"].ToString());
                        goalSettingFormCModel.MyTwoDevelopmentAreas = Convert.ToString(sqlDataReader["MyTwoDevelopmentAreas"] == DBNull.Value ? "" : sqlDataReader["MyTwoDevelopmentAreas"].ToString());
                        goalSettingFormCModel.EmployeeCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["EmployeeCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["EmployeeCommentsonFullYearPerformance"].ToString());
                        goalSettingFormCModel.Strengths = Convert.ToString(sqlDataReader["Strengths"] == DBNull.Value ? "" : sqlDataReader["Strengths"].ToString());
                        goalSettingFormCModel.FullTrainingNeeds = Convert.ToString(sqlDataReader["FullTrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["FullTrainingNeeds"].ToString());
                        goalSettingFormCModel.LineManagerCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["LineManagerCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["LineManagerCommentsonFullYearPerformance"].ToString());

                        goalSettingFormCModel.BusinessTargetsTota = Convert.ToDecimal(sqlDataReader["BusinessTargetsTota"] == DBNull.Value ? 0 : sqlDataReader["BusinessTargetsTota"]);
                        goalSettingFormCModel.ProfessionalCompetenciesAVG = Convert.ToDecimal(sqlDataReader["ProfessionalCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["ProfessionalCompetenciesAVG"]);
                        goalSettingFormCModel.BehavioralCompetenciesAVG = Convert.ToDecimal(sqlDataReader["BehavioralCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["BehavioralCompetenciesAVG"]);
                        goalSettingFormCModel.CompetenciesAVG = Convert.ToDecimal(sqlDataReader["CompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["CompetenciesAVG"]);
                        goalSettingFormCModel.OverallAppraisalScore = Convert.ToDecimal(sqlDataReader["OverallAppraisalScore"] == DBNull.Value ? 0 : sqlDataReader["OverallAppraisalScore"]);
                        goalSettingModel.OverallScoreFormula = Convert.ToString(sqlDataReader["OverallScoreFormula"] == DBNull.Value ? "" : sqlDataReader["OverallScoreFormula"].ToString());



                        //*** F2
                        goalSettingFormCModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        goalSettingFormCModel.Company = Convert.ToString(sqlDataReader["Company"]);
                        goalSettingFormCModel.Designation = Convert.ToString(sqlDataReader["Designation"]);
                        goalSettingFormCModel.RequestEmail = Convert.ToString(sqlDataReader["RequestEmail"]);
                        goalSettingFormCModel.Department = Convert.ToString(sqlDataReader["Department"]);
                        goalSettingFormCModel.LineManager = Convert.ToString(sqlDataReader["LineManager"]);
                        goalSettingFormCModel.DOJ = Convert.ToString(sqlDataReader["DOJ"]);

                        //***F10
                        goalSettingModel.IsReversed = Convert.ToString(sqlDataReader["IsReversed"] == DBNull.Value ? null : sqlDataReader["IsReversed"]);
                        goalSettingModel.MidIsReversed = Convert.ToString(sqlDataReader["MidIsReversed"] == DBNull.Value ? null : sqlDataReader["MidIsReversed"]);
                        goalSettingModel.FullIsReversed = Convert.ToString(sqlDataReader["FullIsReversed"] == DBNull.Value ? null : sqlDataReader["FullIsReversed"]);
                        goalSettingModel.GolReqStatusID = Convert.ToInt32(sqlDataReader["GolReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["GolReqStatusID"].ToString());
                        goalSettingModel.MidReqStatusID = Convert.ToInt32(sqlDataReader["MidReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["MidReqStatusID"].ToString());
                        goalSettingModel.FullReqStatusID = Convert.ToInt32(sqlDataReader["FullReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["FullReqStatusID"].ToString());
                    }

                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        goalSettingKPIFormCModel = new AnnualGoalSettingFormCModel();
                        goalSettingKPIFormCModel.KPIId = Convert.ToInt32(sqlDataReader["KPIId"] == DBNull.Value ? "0" : sqlDataReader["KPIId"].ToString());
                        goalSettingKPIFormCModel.BusinessTargetDetails = Convert.ToString(sqlDataReader["BusinessTargetDetails"] == DBNull.Value ? "0" : sqlDataReader["BusinessTargetDetails"].ToString());
                        //goalSettingKPIFormCModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? 0 : sqlDataReader["Weight"]);
                        goalSettingKPIFormCModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? null : sqlDataReader["Weight"]);
                        if (goalSettingKPIFormCModel.Weight == 0)
                            goalSettingKPIFormCModel.Weight = null;
                        //goalSettingKPIFormCModel.Rating = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? 0 : sqlDataReader["Rating"]);
                        //*** F5                        
                        goalSettingKPIFormCModel.Rating = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                        goalSettingKPIFormCModel.Score = Convert.ToDecimal(sqlDataReader["Score"] == DBNull.Value ? 0.00 : sqlDataReader["Score"]);
                        //*** Naresh 2020-03-01 Accomplishments was not populated
                        goalSettingKPIFormCModel.Accomplishments = Convert.ToString(sqlDataReader["Accomplishments"] == DBNull.Value ? "" : sqlDataReader["Accomplishments"].ToString());
                        goalSettingFormCModelList.Add(goalSettingKPIFormCModel);
                    }



                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);
                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        BehavioralCompetencies = new BehavioralCompetencies();
                        BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                        //*** F5
                        //BehavioralCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        BehavioralCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));


                        BehavioralCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        BehavioralCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        BehavioralCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        BehavioralCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        BehavioralCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        BehavioralCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        BehavioralCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        BehavioralCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        BehavioralCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        BehavioralCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        BehavioralCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        BehavioralCompetenciesList.Add(BehavioralCompetencies);
                    }

                }
                else
                {
                    sqlDataReader.NextResult();
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        BehavioralCompetencies = new BehavioralCompetencies();
                        BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                        //*** F5
                        //BehavioralCompetencies.Score = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                        BehavioralCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));

                        BehavioralCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        BehavioralCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        BehavioralCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        BehavioralCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        BehavioralCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        BehavioralCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        BehavioralCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        BehavioralCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        BehavioralCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        BehavioralCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        BehavioralCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        BehavioralCompetenciesList.Add(BehavioralCompetencies);

                    }
                }
                goalSettingModel.FormCKPIModelList = goalSettingFormCModelList;
                goalSettingModel.FormCKPIModel = goalSettingFormCModel;

                goalSettingModel.ProfessionalCompetencies = ProfessionalCompetenciesList;
                goalSettingModel.BehavioralCompetencies = BehavioralCompetenciesList;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModel;
        }       
        
        public OperationDetails SaveAnnualGoalSettingsFormDData( IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel, int? userId, List<ProfessionalCompetencies> ProfessionalCompetencies, AnnualGoalSettingFormDModel goalSettingFormDModel, int formId)
        {
            //*** F2
            DataTable RatingScaleTable = new DataTable();
            RatingScaleTable.Columns.Add("RatingScaleCharacter");
            RatingScaleTable.Columns.Add("RatingScaleNumber");
            RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
            RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
            RatingScaleTable.Columns.Add("DefinitionName");
            RatingScaleTable.Columns.Add("DefinitionDetails");
            foreach (GoalSettingOverAllRatingScalesModel Level in GoalSettingOverAllRatingScalesModel)
            {

                RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails);

            }
            DataTable dtProfessionalCompetencies = new DataTable();
            dtProfessionalCompetencies.Columns.Add("ProfessionalCompetenciesNo", typeof(int));
            dtProfessionalCompetencies.Columns.Add("MidRating", typeof(string));
            //dtProfessionalCompetencies.Columns.Add("FullRating", typeof(string));
            dtProfessionalCompetencies.Columns.Add("CompetencyID", typeof(int));

            foreach (var item in ProfessionalCompetencies)
            {
                dtProfessionalCompetencies.Rows.Add(item.No, item.Score,item.CompetencyID);
            }
            //DataTable kpiFormCGoalSettings = new DataTable();
            //kpiFormCGoalSettings.Columns.Add("BusinessTargetNo", typeof(int));
            //kpiFormCGoalSettings.Columns.Add("KPIId", typeof(int));
            //kpiFormCGoalSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            //kpiFormCGoalSettings.Columns.Add("Weight", typeof(int));

            //foreach (var item in kpiFormCSettings)
            //{
            //    kpiFormCGoalSettings.Rows.Add(item.BusinessTargetNo, item.KPIId, item.BusinessTargetDetails, item.Weight);
            //}
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPSaveAnnualGoalSettingsFormDData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormDID", goalSettingFormDModel.ID);
                //sqlCommand.Parameters.AddWithValue("@KPIFormCSettings", kpiFormCGoalSettings);
                sqlCommand.Parameters.AddWithValue("@RatingScaleTable", RatingScaleTable); //*** F2
                sqlCommand.Parameters.AddWithValue("@PDRPProfessionalCompetenciesTY", dtProfessionalCompetencies);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", goalSettingFormDModel.EmployeeId);
                //sqlCommand.Parameters.AddWithValue("@KPIsComments", goalSettingFormDModel.KPIsComments);
                sqlCommand.Parameters.AddWithValue("@CompetenciesComments", goalSettingFormDModel.CompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", goalSettingFormDModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", goalSettingFormDModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", goalSettingFormDModel.JobGrade);
                sqlCommand.Parameters.AddWithValue("@FormState", goalSettingFormDModel.FormState);
                //*** F2
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingFormDModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@EmployeeName", goalSettingFormDModel.EmployeeName);
                sqlCommand.Parameters.AddWithValue("@Company", goalSettingFormDModel.Company);
                sqlCommand.Parameters.AddWithValue("@Designation", goalSettingFormDModel.Designation);
                sqlCommand.Parameters.AddWithValue("@RequestEmail", goalSettingFormDModel.RequestEmail);
                sqlCommand.Parameters.AddWithValue("@Department", goalSettingFormDModel.Department);
                sqlCommand.Parameters.AddWithValue("@LineManager", goalSettingFormDModel.LineManager);
                sqlCommand.Parameters.AddWithValue("@DOJ", goalSettingFormDModel.DOJ); //*** F17 Date Format in server)

                sqlCommand.Parameters.AddWithValue("@ProfessionalCompetenciesAVG", goalSettingFormDModel.ProfessionalCompetenciesAVG);
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", goalSettingFormDModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                //sqlCommand.Parameters.AddWithValue("@BehavioralCompetenciesAVG", goalSettingFormDModel.BehavioralCompetenciesAVG);


                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** F12
                if (goalSettingFormDModel.ID == null || goalSettingFormDModel.ID == 0)
                {
                    goalSettingFormDModel.ID = operationDetails.InsertedRowId;
                }
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion

        #region Annual Goal Setting Form D
        public GoalSettingModel GetGoalSettingFormDModelDetails(int formProcessID, int? employeeId, int CompanyID, int YearID, int FormNow)
        {
            GoalSettingModel goalSettingModel = new GoalSettingModel();
            AnnualGoalSettingFormDModel goalSettingFormDModel = new AnnualGoalSettingFormDModel();
            AnnualGoalSettingFormDModel goalSettingKPIFormDModel = null;
            IList<AnnualGoalSettingFormDModel> goalSettingFormDModelList = null;

            ProfessionalCompetencies ProfessionalCompetencies;
            List<ProfessionalCompetencies> ProfessionalCompetenciesList = new List<ProfessionalCompetencies>();

            //BehavioralCompetencies BehavioralCompetencies;
            //List<BehavioralCompetencies> BehavioralCompetenciesList = new List<BehavioralCompetencies>();
            try
            {
                //goalSettingFormDModelList = new List<AnnualGoalSettingFormDModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetGoalSettingFormDModelDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                //*** F3
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessID);
                sqlCommand.Parameters.AddWithValue("@FormNow", FormNow);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        goalSettingFormDModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        goalSettingFormDModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingFormDModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        //goalSettingFormDModel.KPIsComments = Convert.ToString(sqlDataReader["KPIsComments"] == DBNull.Value ? "" : sqlDataReader["KPIsComments"]);
                        goalSettingFormDModel.CompetenciesComments = Convert.ToString(sqlDataReader["CoreCompetenciesComments"] == DBNull.Value ? "" : sqlDataReader["CoreCompetenciesComments"]);
                        goalSettingFormDModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        goalSettingFormDModel.RequesterEmployeeId = Convert.ToInt32(sqlDataReader["RequesterEmployeeId"] == DBNull.Value ? "0" : sqlDataReader["RequesterEmployeeId"].ToString());
                        goalSettingFormDModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingFormDModel.JobGrade = Convert.ToString(sqlDataReader["Grade"] == DBNull.Value ? "" : sqlDataReader["Grade"].ToString());
                        goalSettingFormDModel.FormState = Convert.ToString(sqlDataReader["FormState"] == DBNull.Value ? "" : sqlDataReader["FormState"].ToString());
                        goalSettingModel.FormMode = Convert.ToInt32(sqlDataReader["FormState"] == DBNull.Value ? 0 : sqlDataReader["FormState"]);
                        //*** F3
                        goalSettingModel.FormMode = Convert.ToInt32((sqlDataReader["FormState"] == DBNull.Value || sqlDataReader["FormState"].ToString() == "") ? 0 : sqlDataReader["FormState"]);
                        goalSettingModel.MidFormMode = Convert.ToInt32((sqlDataReader["MidFormState"] == DBNull.Value || sqlDataReader["MidFormState"].ToString() == "") ? 0 : sqlDataReader["MidFormState"]);
                        goalSettingModel.IsMidSignOffForm = Convert.ToBoolean(sqlDataReader["IsMidSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsMidSignOffForm"]);
                        goalSettingModel.FullFormMode = Convert.ToInt32((sqlDataReader["FullFormState"] == DBNull.Value || sqlDataReader["FullFormState"].ToString() == "") ? 0 : sqlDataReader["FullFormState"]);
                        goalSettingModel.IsFullSignOffForm = Convert.ToBoolean(sqlDataReader["IsFullSignOffForm"] == DBNull.Value ? 0 : sqlDataReader["IsFullSignOffForm"]);

                        goalSettingFormDModel.LineManagerEvaluationComments = Convert.ToString(sqlDataReader["LineManagerEvaluationComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerEvaluationComments"].ToString());
                        goalSettingFormDModel.TrainingNeeds = Convert.ToString(sqlDataReader["TrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["TrainingNeeds"].ToString());
                        goalSettingFormDModel.LineManagerMidYearPerformanceComments = Convert.ToString(sqlDataReader["LineManagerMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["LineManagerMidYearPerformanceComments"].ToString());
                        goalSettingFormDModel.EmployeeMidYearPerformanceComments = Convert.ToString(sqlDataReader["EmployeeMidYearPerformanceComments"] == DBNull.Value ? "" : sqlDataReader["EmployeeMidYearPerformanceComments"].ToString());

                        goalSettingFormDModel.MyThreeKeyAchivements = Convert.ToString(sqlDataReader["MyThreeKeyAchivements"] == DBNull.Value ? "" : sqlDataReader["MyThreeKeyAchivements"].ToString());
                        goalSettingFormDModel.MyTwoDevelopmentAreas = Convert.ToString(sqlDataReader["MyTwoDevelopmentAreas"] == DBNull.Value ? "" : sqlDataReader["MyTwoDevelopmentAreas"].ToString());
                        goalSettingFormDModel.EmployeeCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["EmployeeCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["EmployeeCommentsonFullYearPerformance"].ToString());
                        goalSettingFormDModel.Strengths = Convert.ToString(sqlDataReader["Strengths"] == DBNull.Value ? "" : sqlDataReader["Strengths"].ToString());
                        goalSettingFormDModel.FullTrainingNeeds = Convert.ToString(sqlDataReader["FullTrainingNeeds"] == DBNull.Value ? "" : sqlDataReader["FullTrainingNeeds"].ToString());
                        goalSettingFormDModel.LineManagerCommentsonFullYearPerformance = Convert.ToString(sqlDataReader["LineManagerCommentsonFullYearPerformance"] == DBNull.Value ? "" : sqlDataReader["LineManagerCommentsonFullYearPerformance"].ToString());

                        goalSettingFormDModel.MidProfessionalCompetenciesAVG = Convert.ToDecimal(sqlDataReader["ProfessionalCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["ProfessionalCompetenciesAVG"]);
                        goalSettingFormDModel.FullProfessionalCompetenciesAVG = Convert.ToDecimal(sqlDataReader["BehavioralCompetenciesAVG"] == DBNull.Value ? 0 : sqlDataReader["BehavioralCompetenciesAVG"]);

                        //***F10
                        goalSettingModel.IsReversed = Convert.ToString(sqlDataReader["IsReversed"] == DBNull.Value ? null : sqlDataReader["IsReversed"]);
                        goalSettingModel.MidIsReversed = Convert.ToString(sqlDataReader["MidIsReversed"] == DBNull.Value ? null : sqlDataReader["MidIsReversed"]);
                        goalSettingModel.FullIsReversed = Convert.ToString(sqlDataReader["FullIsReversed"] == DBNull.Value ? null : sqlDataReader["FullIsReversed"]);
                        goalSettingModel.GolReqStatusID = Convert.ToInt32(sqlDataReader["GolReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["GolReqStatusID"].ToString());
                        goalSettingModel.MidReqStatusID = Convert.ToInt32(sqlDataReader["MidReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["MidReqStatusID"].ToString());
                        goalSettingModel.FullReqStatusID = Convert.ToInt32(sqlDataReader["FullReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["FullReqStatusID"].ToString());
                    }

                   

                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToDecimal(sqlDataReader["Rating"] == DBNull.Value ? 0.00 : sqlDataReader["Rating"]);
                        //ProfessionalCompetencies.FullScore = Convert.ToDecimal(sqlDataReader["FullRating"] == DBNull.Value ? 0.00 : sqlDataReader["FullRating"]);
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                        ProfessionalCompetencies.FullScore = Convert.ToString(sqlDataReader["FullRating"] == DBNull.Value ? "" : (sqlDataReader["FullRating"].ToString() == "0" ? "N/A" : sqlDataReader["FullRating"].ToString()));


                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);


                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }


                }
                else
                {
                    sqlDataReader.NextResult();
                    //sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                        //ProfessionalCompetencies.Score = Convert.ToDecimal(sqlDataReader["Rating"] == DBNull.Value ? 0.00 : sqlDataReader["Rating"]);
                        //ProfessionalCompetencies.FullScore = Convert.ToDecimal(sqlDataReader["FullRating"] == DBNull.Value ? 0.00 : sqlDataReader["FullRating"]);
                        //*** F5                        
                        ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "" : (sqlDataReader["Rating"].ToString() == "0" ? "N/A" : sqlDataReader["Rating"].ToString()));
                        ProfessionalCompetencies.FullScore = Convert.ToString(sqlDataReader["FullRating"] == DBNull.Value ? "" : (sqlDataReader["FullRating"].ToString() == "0" ? "N/A" : sqlDataReader["FullRating"].ToString()));

                        ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);


                        ProfessionalCompetenciesList.Add(ProfessionalCompetencies);

                    }
                }

                goalSettingModel.FormDModel = goalSettingFormDModel;

                goalSettingModel.ProfessionalCompetencies = ProfessionalCompetenciesList;

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModel;
        }
        public OperationDetails SaveAnnualGoalSettingsFormCData(IEnumerable<AnnualGoalSettingFormCModel> kpiFormCSettings, IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel, int? userId, AnnualGoalSettingFormCModel goalSettingFormCModel, int formId)
        {
            //*** F2
            DataTable RatingScaleTable = new DataTable();
            RatingScaleTable.Columns.Add("RatingScaleCharacter");
            RatingScaleTable.Columns.Add("RatingScaleNumber");
            RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
            RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
            RatingScaleTable.Columns.Add("DefinitionName");
            RatingScaleTable.Columns.Add("DefinitionDetails");
            foreach (GoalSettingOverAllRatingScalesModel Level in GoalSettingOverAllRatingScalesModel)
            {

                RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails);

            }

            DataTable kpiFormCGoalSettings = new DataTable();
            kpiFormCGoalSettings.Columns.Add("BusinessTargetNo", typeof(int));
            kpiFormCGoalSettings.Columns.Add("KPIId", typeof(int));
            kpiFormCGoalSettings.Columns.Add("BusinessTargetSetails", typeof(string));
            kpiFormCGoalSettings.Columns.Add("Weight", typeof(int));

            foreach (var item in kpiFormCSettings)
            {
                kpiFormCGoalSettings.Rows.Add(item.BusinessTargetNo, item.KPIId, item.BusinessTargetDetails, item.Weight);
            }
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPSaveAnnualGoalSettingsFormCData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormCID", goalSettingFormCModel.ID);
                sqlCommand.Parameters.AddWithValue("@KPIFormCSettings", kpiFormCGoalSettings);
                sqlCommand.Parameters.AddWithValue("@RatingScaleTable", RatingScaleTable); //*** F2
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", goalSettingFormCModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@KPIsComments", goalSettingFormCModel.KPIsComments);
                sqlCommand.Parameters.AddWithValue("@CompetenciesComments", goalSettingFormCModel.CompetenciesComments);
                sqlCommand.Parameters.AddWithValue("@YearId", goalSettingFormCModel.YearId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", goalSettingFormCModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Grade", goalSettingFormCModel.JobGrade);
                sqlCommand.Parameters.AddWithValue("@FormState", goalSettingFormCModel.FormState);
                //*** F2
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingFormCModel.GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@EmployeeName", goalSettingFormCModel.EmployeeName);
                sqlCommand.Parameters.AddWithValue("@Company", goalSettingFormCModel.Company);
                sqlCommand.Parameters.AddWithValue("@Designation", goalSettingFormCModel.Designation);
                sqlCommand.Parameters.AddWithValue("@RequestEmail", goalSettingFormCModel.RequestEmail);
                sqlCommand.Parameters.AddWithValue("@Department", goalSettingFormCModel.Department);
                sqlCommand.Parameters.AddWithValue("@LineManager", goalSettingFormCModel.LineManager);
                sqlCommand.Parameters.AddWithValue("@DOJ", goalSettingFormCModel.DOJ); //*** F17 Date Format in server
                //*** Naresh 2020-03-12 Save comment during save
                sqlCommand.Parameters.AddWithValue("@Comments", goalSettingFormCModel.Comments);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value.ToString());
                //*** F12
                if (goalSettingFormCModel.ID == null || goalSettingFormCModel.ID == 0)
                {
                    goalSettingFormCModel.ID = operationDetails.InsertedRowId;
                }
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion

        #region Forms Approval for Goal Setting
        public OperationDetails Reintialized(int? GoalSettingId, int? UserId, int FormProcessID,string Comment)
        {

            OperationDetails oprationDetails = new OperationDetails();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGoalSettingReintializ", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GoalSettingId", GoalSettingId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                sqlCommand.Parameters.AddWithValue("@Comment", Comment);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 4000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                oprationDetails.Message = "Reintialized successfully.";

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public OperationDetails FormStatUpdate(int? id, int FormState, int? performanceGroupId,  int? formProcessID,int? employeeId)
        {
            OperationDetails operationDetails = new OperationDetails();
           
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPFormStatUpdate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", id);
                sqlCommand.Parameters.AddWithValue("@FormState", FormState);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlDataReader sqlReader = sqlCommand.ExecuteReader();


                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                       
                        //operationDetails.Message = sqlReader["Result"].ToString();
                       
                        if (sqlReader["Result"].ToString().Contains("SUCCESS"))
                        {
                            operationDetails.Success = true;
                            operationDetails.CssClass = "success";
                            operationDetails.Message = "Updated successfully";
                        }
                        else
                        {
                            operationDetails.Success = false;
                            operationDetails.Message = "Technical error has occurred";
                            operationDetails.CssClass = "error";
                        }
                        
                    }
                }
                sqlConnection.Close();               
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails SaveFormApprovalForGoalSettingFormsLineManager(int? id, int? employeeId, int? formId, int? goalSettingId)
        {
            OperationDetails operationDetails = new OperationDetails();
            //RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveFormApprovalForGoalSettingFormsLineManager", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", id);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingId);
                //SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlReader = sqlCommand.ExecuteReader();


                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        //FormProcessID = sqlReader["FormProcessID"].ToString();
                        //GroupID = sqlReader["GroupID"].ToString();
                        //ApproverID = sqlReader["ApproverID"].ToString();
                        //RequestID = sqlReader["RequestID"].ToString();
                        operationDetails.Message = sqlReader["Result"].ToString();

                        operationDetails.InsertedRowId = Convert.ToInt32(sqlReader["FormProcessID"] == DBNull.Value ? 0 : sqlReader["FormProcessID"]);
                        if (operationDetails.Message.Contains("SUCCESS"))
                        {
                            operationDetails.Success = true;
                            operationDetails.CssClass = "success";
                        }
                        else
                        {
                            operationDetails.Success = false;
                            operationDetails.Message = "Technical error has occurred";
                            operationDetails.CssClass = "error";
                        }
                        //requestFormsProcessModel.RequestID = Convert.ToInt32(sqlReader["RequestID"] == DBNull.Value ? 0 : sqlReader["RequestID"]);
                    }
                }
                sqlConnection.Close();

                //operationDetails.Success = true;
                //operationDetails.Message = "submitted successfully";
                //operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public OperationDetails TaskForEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveFormApprovalForEmployeeSignOffForms", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormId", id);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Submitted successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion



        #region Submit Employee Sign Off forms
        public OperationDetails SubmitEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? goalSettingId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPSubmitEmployeeSignOffForms", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@AnnualGoalSettingFormId", id);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);
                sqlCommand.Parameters.AddWithValue("@PerformanceGroupId", performanceGroupId);
                sqlCommand.Parameters.AddWithValue("@GoalSettingId", goalSettingId);
                sqlCommand.Parameters.AddWithValue("@Comment", comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Submitted successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        #endregion
        public OperationDetails SaveLog(int? ID, int UserId, int? performanceGroupId)
        {
            string ProName = "";
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (performanceGroupId == 1)
                {
                    ProName = "HR_Stp_PDRPSaveGoalSettingsFormAData_LOG";
                }
                else if (performanceGroupId == 2)
                {
                    ProName = "HR_Stp_PDRPSaveGoalSettingsFormBData_LOG";
                }
                else if (performanceGroupId == 3)
                {
                    ProName = "HR_Stp_PDRPSaveGoalSettingsFormCData_LOG";
                }
               
                sqlCommand = new SqlCommand(ProName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", UserId);

                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public GoalSettingModel GetAppraisalNonTeachProficiencyLevel(GoalSettingModel objAnnualAppraisalProcessModel,int YearID, int CompanyID)
        {
            //*** F5


            objAnnualAppraisalProcessModel.AppraisalNonTeachDetailsRatingScaleWeightages = new List<AppraisalNonTeachDetailsRatingScaleWeightages>();
            objAnnualAppraisalProcessModel.AppraisalNonTeachDetailsRatingScaleWeightages_New = new List<AppraisalNonTeachDetailsRatingScaleWeightages_New>();

            AppraisalNonTeachDetailsRatingScaleWeightages objAppraisalNonTeachDetailsRatingScaleWeightages;
            AppraisalNonTeachDetailsRatingScaleWeightages_New AppraisalNonTeachDetailsRatingScaleWeightages_New;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPDRPNonTeachAppraisalProficiencies", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@YearID", YearID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);                

                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objAppraisalNonTeachDetailsRatingScaleWeightages = new AppraisalNonTeachDetailsRatingScaleWeightages();
                        

                        objAppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        objAppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = Convert.ToString(reader["RatingScaleNumber"] == DBNull.Value ? "" : reader["RatingScaleNumber"].ToString());



                        objAnnualAppraisalProcessModel.AppraisalNonTeachDetailsRatingScaleWeightages.Add(objAppraisalNonTeachDetailsRatingScaleWeightages);

                        
                        AppraisalNonTeachDetailsRatingScaleWeightages_New = new AppraisalNonTeachDetailsRatingScaleWeightages_New();

                        AppraisalNonTeachDetailsRatingScaleWeightages_New.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        AppraisalNonTeachDetailsRatingScaleWeightages_New.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);



                        objAnnualAppraisalProcessModel.AppraisalNonTeachDetailsRatingScaleWeightages_New.Add(AppraisalNonTeachDetailsRatingScaleWeightages_New);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objAnnualAppraisalProcessModel;
        }
    }
}
