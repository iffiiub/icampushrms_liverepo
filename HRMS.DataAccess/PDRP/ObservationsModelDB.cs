﻿
using HRMS.Entities;
using HRMS.Entities.Common;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.PDRP
{
    public class ObservationsModelDB : PDRPCommonModelDB
    {
        public OperationDetails SetObservationVisitDateMulty(List<PDRPTeachersModel> SelectedTeachersList, PDRPTeachersModel SelectedTeacher, int YearId)
        {

            OperationDetails oprationDetails = new OperationDetails();
            //if (GetKPICount(SelectedTeacher.CompanyId, YearId) == "0")
            //{
            //    oprationDetails.Success = false;
            //    oprationDetails.CssClass = "error";
            //    oprationDetails.Message = PDRPObservationsXMLResources.GetResource("ThereisnoKPIs");
            //}
            //else
            //{

                DataTable dtObservationProcessTable = new DataTable();
                dtObservationProcessTable.Columns.Add("EmployeeID");

                //DataColumn VisitDate = new DataColumn("VisitDate");
                //VisitDate.DataType = System.Type.GetType("DateTime");

                dtObservationProcessTable.Columns.Add("VisitDate", System.Type.GetType("System.DateTime"));
                dtObservationProcessTable.Columns.Add("PreObservationDueDate");

                foreach (var item in SelectedTeachersList)
                {
                    DateTime VisitDateTime = Convert.ToDateTime(Convert.ToDateTime(item.VisitDate));
                    dtObservationProcessTable.Rows.Add(item.EmployeeId, VisitDateTime, item.PreObservationDate);
                }

                try
                {

                    SqlParameter[] parameters =
                       {
                         new SqlParameter("@ObservationProcessTable",dtObservationProcessTable),
                         new SqlParameter("@ObservationProcessYearID",YearId),
                         new SqlParameter("@CreatedBy",SelectedTeacher.CreatedBy),
                         //new SqlParameter("@CompanyID",SelectedTeacher.CompanyId),
                    };

                    SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                       "HR_stp_PDRPSaveObservationProcessVisitDate", parameters);

                    oprationDetails.Success = true;
                    oprationDetails.CssClass = "success";
                    oprationDetails.Message = PDRPObservationsXMLResources.GetResource("Savedsuccessfully");

                    try
                    {

                        SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_PDRPObservationsEmailSending");
                    }
                    catch (Exception ex)
                    {
                        oprationDetails.CssClass = "error";
                        oprationDetails.Message = PDRPObservationsXMLResources.GetResource("SavedError");
                    }
                }
                catch (Exception exception)
                {
                    oprationDetails.Success = false;
                    oprationDetails.CssClass = "error";
                    oprationDetails.Message = exception.Message;

                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }

                }
            //}
            return oprationDetails;
        }
        public ObservationsModel GetPreObservationDetail(ObservationsModel objObservationsModel)
        {
            try
            {
                SqlParameter[] parameters =
                  {
                         new SqlParameter("@ID",objObservationsModel.ObservationsEmployeeModel.ID),

                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPPreObservation", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //objObservationsModel.ObservationsEmployeeModel.RequestID = reader["ID"].ToString();
                        objObservationsModel.ObservationsEmployeeModel.ReqStatusID = Convert.ToInt32(reader["ReqStatusID"] == DBNull.Value ? 0 : reader["ReqStatusID"]);
                        objObservationsModel.ObservationsEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        objObservationsModel.ObservationsEmployeeModel.RequesterID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? 0 : reader["RequesterEmployeeID"]);

                        objObservationsModel.ObservationsEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                        objObservationsModel.ObservationsEmployeeModel.PositionTitle = Convert.ToString(reader["Designation"]);
                        objObservationsModel.ObservationsEmployeeModel.DepartmentName = Convert.ToString(reader["Department"]);
                        objObservationsModel.ObservationsEmployeeModel.SuperviserName = Convert.ToString(reader["LineManager"]);
                        objObservationsModel.ObservationsEmployeeModel.CompanyName = Convert.ToString(reader["School"]);

                        objObservationsModel.ObservationsEmployeeModel.TeacherID = Convert.ToInt32(reader["TeacherID"] == DBNull.Value ? 0 : reader["TeacherID"]);
                        objObservationsModel.ObservationsEmployeeModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                        objObservationsModel.ObservationsEmployeeModel.LessonTopic = Convert.ToString(reader["LessonTopic"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsOnRollMale = Convert.ToInt32(reader["StudentsOnRollMale"] == DBNull.Value ? 0 : reader["StudentsOnRollMale"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsOnRollFemale = Convert.ToInt32(reader["StudentsOnRollFemale"] == DBNull.Value ? 0 : reader["StudentsOnRollFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.VisitDATE = Convert.ToDateTime(reader["VisitDATE"]);
                        objObservationsModel.ObservationsEmployeeModel.VisitTime = Convert.ToDateTime(reader["VisitDATE"]).ToString("hh:mm tt");
                        objObservationsModel.ObservationsEmployeeModel.ObservationsYear = Convert.ToString(reader["ObservationsYear"]);
                        objObservationsModel.ObservationsEmployeeModel.ObservationsYearID = Convert.ToInt32(reader["AcademicYearID"] == DBNull.Value ? 0 : reader["AcademicYearID"]);
                        objObservationsModel.ObservationsEmployeeModel.Branch = Convert.ToString(reader["Branch"]);
                        objObservationsModel.ObservationsEmployeeModel.GradeYear = Convert.ToString(reader["GradeYear"]);
                        objObservationsModel.ObservationsEmployeeModel.GroupSection = Convert.ToString(reader["GroupSection"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsPresentMale = Convert.ToInt32(reader["StudentsPresentMale"] == DBNull.Value ? 0 : reader["StudentsPresentMale"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsPresentFemale = Convert.ToInt32(reader["StudentsPresentFemale"] == DBNull.Value ? 0 : reader["StudentsPresentFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.UnitThemeLessonTopic = Convert.ToString(reader["UnitThemeLessonTopic"]);
                        objObservationsModel.ObservationsEmployeeModel.Subject = Convert.ToString(reader["Subject"]);
                        objObservationsModel.ObservationsEmployeeModel.Period = Convert.ToString(reader["Period"]);
                        objObservationsModel.ObservationsEmployeeModel.YearPlan = Convert.ToString(reader["YearPlan"]);
                        objObservationsModel.IsOnTrackYearlyPlans = Convert.ToInt32(reader["IsOnTrackYearlyPlans"] == DBNull.Value ? 0 : reader["IsOnTrackYearlyPlans"]);
                        objObservationsModel.IsOnTrackYearlyPlansNo = Convert.ToString(reader["IsOnTrackYearlyPlansNo"]);
                        objObservationsModel.ObjectivesList = Convert.ToString(reader["ObjectivesList"]);
                        objObservationsModel.ObservationDayLesson = Convert.ToString(reader["ObservationDayLesson"]);
                        objObservationsModel.IsTaughtToThisClass = Convert.ToInt32(reader["IsTaughtToThisClass"] == DBNull.Value ? 0 : reader["IsTaughtToThisClass"]);
                        objObservationsModel.SpecificItemsToWatch = Convert.ToString(reader["SpecificItemsToWatch"]);
                        objObservationsModel.ItemsToBeAware = Convert.ToString(reader["ItemsToBeAware"]);
                        //*** F4
                        objObservationsModel.EmpCurrentCompanyID = Convert.ToInt32(reader["EmpCurrentCompanyID"] == DBNull.Value ? 0 : reader["EmpCurrentCompanyID"]);
                        //*** F6
                        objObservationsModel.Comments = Convert.ToString(reader["Comments"]);
                        //28-04-2020 Nithin included RequestID
                        objObservationsModel.ObservationsEmployeeModel.RequestID = Convert.ToString(reader["RequestID"]);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objObservationsModel;
        }
        public ObservationsModel GetPostObservationDetail(ObservationsModel objObservationsModel)
        {
            try
            {
                SqlParameter[] parameters =
                  {
                         new SqlParameter("@ID",objObservationsModel.ObservationsEmployeeModel.ID),

                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPPostObservation", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objObservationsModel.ObservationsEmployeeModel.RequestID = reader["ID"].ToString();
                        objObservationsModel.ObservationsEmployeeModel.ReqStatusID = Convert.ToInt32(reader["ReqStatusID"] == DBNull.Value ? 0 : reader["ReqStatusID"]);
                        objObservationsModel.ObservationsEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        objObservationsModel.ObservationsEmployeeModel.RequesterID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? 0 : reader["RequesterEmployeeID"]);
                        objObservationsModel.ObservationsEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                        objObservationsModel.ObservationsEmployeeModel.PositionTitle = Convert.ToString(reader["Designation"]);
                        objObservationsModel.ObservationsEmployeeModel.DepartmentName = Convert.ToString(reader["Department"]);
                        objObservationsModel.ObservationsEmployeeModel.SuperviserName = Convert.ToString(reader["LineManager"]);
                        objObservationsModel.ObservationsEmployeeModel.CompanyName = Convert.ToString(reader["School"]);
                        objObservationsModel.ObservationsEmployeeModel.TeacherID = Convert.ToInt32(reader["TeacherID"] == DBNull.Value ? 0 : reader["TeacherID"]);
                        objObservationsModel.ObservationsEmployeeModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                        objObservationsModel.ObservationsEmployeeModel.LessonTopic = Convert.ToString(reader["LessonTopic"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsOnRollMale = Convert.ToInt32(reader["StudentsOnRollMale"] == DBNull.Value ? 0 : reader["StudentsOnRollMale"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsOnRollFemale = Convert.ToInt32(reader["StudentsOnRollFemale"] == DBNull.Value ? 0 : reader["StudentsOnRollFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.VisitDATE = Convert.ToDateTime(reader["VisitDATE"]);
                        objObservationsModel.ObservationsEmployeeModel.VisitTime = Convert.ToDateTime(reader["VisitDATE"]).ToString("hh:mm tt");
                        objObservationsModel.ObservationsEmployeeModel.ObservationsYear = Convert.ToString(reader["ObservationsYear"]);
                        objObservationsModel.ObservationsEmployeeModel.ObservationsYearID = Convert.ToInt32(reader["AcademicYearID"] == DBNull.Value ? 0 : reader["AcademicYearID"]);
                        objObservationsModel.ObservationsEmployeeModel.Branch = Convert.ToString(reader["Branch"]);
                        objObservationsModel.ObservationsEmployeeModel.GradeYear = Convert.ToString(reader["GradeYear"]);
                        objObservationsModel.ObservationsEmployeeModel.GroupSection = Convert.ToString(reader["GroupSection"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsPresentMale = Convert.ToInt32(reader["StudentsPresentMale"] == DBNull.Value ? 0 : reader["StudentsPresentMale"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsPresentFemale = Convert.ToInt32(reader["StudentsPresentFemale"] == DBNull.Value ? 0 : reader["StudentsPresentFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.UnitThemeLessonTopic = Convert.ToString(reader["UnitThemeLessonTopic"]);
                        objObservationsModel.ObservationsEmployeeModel.Subject = Convert.ToString(reader["Subject"]);
                        objObservationsModel.ObservationsEmployeeModel.Period = Convert.ToString(reader["Period"]);
                        objObservationsModel.ObservationsEmployeeModel.YearPlan = Convert.ToString(reader["YearPlan"]);
                        objObservationsModel.IsOnTrackYearlyPlans = Convert.ToInt32(reader["IsOnTrackYearlyPlans"] == DBNull.Value ? 0 : reader["IsOnTrackYearlyPlans"]);
                        objObservationsModel.IsOnTrackYearlyPlansNo = Convert.ToString(reader["IsOnTrackYearlyPlansNo"]);
                        objObservationsModel.ObjectivesList = Convert.ToString(reader["ObjectivesList"]);
                        objObservationsModel.ObservationDayLesson = Convert.ToString(reader["ObservationDayLesson"]);
                        objObservationsModel.IsTaughtToThisClass = Convert.ToInt32(reader["IsTaughtToThisClass"] == DBNull.Value ? 0 : reader["IsTaughtToThisClass"]);
                        objObservationsModel.SpecificItemsToWatch = Convert.ToString(reader["SpecificItemsToWatch"]);
                        objObservationsModel.ItemsToBeAware = Convert.ToString(reader["ItemsToBeAware"]);
                        objObservationsModel.PDRPObservationEvlID = Convert.ToInt32(reader["PDRPObservationEvlID"] == DBNull.Value ? 0 : reader["PDRPObservationEvlID"]);
                        objObservationsModel.ParticularlyWellLesson = Convert.ToString(reader["ParticularlyWellLesson"]);
                        objObservationsModel.FurtherImprovementLesson = Convert.ToString(reader["FurtherImprovementLesson"]);
                        objObservationsModel.ChildrensHighAbility = Convert.ToString(reader["ChildrensHighAbility"]);
                        objObservationsModel.ChildrensAverageAbility = Convert.ToString(reader["ChildrensAverageAbility"]);
                        objObservationsModel.ChildrensBelowAverageAbility = Convert.ToString(reader["ChildrensBelowAverageAbility"]);
                        //*** F4
                        objObservationsModel.EmpCurrentCompanyID = Convert.ToInt32(reader["EmpCurrentCompanyID"] == DBNull.Value ? 0 : reader["EmpCurrentCompanyID"]);

                        //*** F6
                        objObservationsModel.Comments = Convert.ToString(reader["Comments"]);
                        //28-04-2020 Nithin included RequestID
                        objObservationsModel.ObservationsEmployeeModel.RequestID = Convert.ToString(reader["RequestID"]);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objObservationsModel;
        }
        public ObservationsModel GetObservationEvlDetail(ObservationsModel objObservationsModel)
        {
            try
            {
                SqlParameter[] parameters =
                  {
                         new SqlParameter("@ID",objObservationsModel.ObservationsEmployeeModel.ID),

                    };
                ObservationsRatingScaleWeightageDetails ObservationsRatingScaleWeightageDetails;
                objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails = new List<ObservationsRatingScaleWeightageDetails>();
                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPObservationEvl", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objObservationsModel.ObservationsEmployeeModel.ID = int.Parse(reader["ID"].ToString());
                        // Nithin, 05-06-2020, Binding RequestID From DB
                        objObservationsModel.ObservationsEmployeeModel.RequestID = Convert.ToString(reader["RequestID"]);
                        objObservationsModel.ObservationsEmployeeModel.ReqStatusID = Convert.ToInt32(reader["ReqStatusID"] == DBNull.Value ? 0 : reader["ReqStatusID"]);
                        objObservationsModel.ObservationsEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        objObservationsModel.ObservationsEmployeeModel.RequesterID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? 0 : reader["RequesterEmployeeID"]);

                        objObservationsModel.ObservationsEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                        objObservationsModel.ObservationsEmployeeModel.PositionTitle = Convert.ToString(reader["Designation"]);
                        objObservationsModel.ObservationsEmployeeModel.DepartmentName = Convert.ToString(reader["Department"]);
                        objObservationsModel.ObservationsEmployeeModel.SuperviserName = Convert.ToString(reader["LineManager"]);
                        objObservationsModel.ObservationsEmployeeModel.CompanyName = Convert.ToString(reader["School"]);

                        objObservationsModel.ObservationsEmployeeModel.TeacherID = Convert.ToInt32(reader["TeacherID"] == DBNull.Value ? 0 : reader["TeacherID"]);
                        objObservationsModel.ObservationsEmployeeModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                        objObservationsModel.ObservationsEmployeeModel.Subject = Convert.ToString(reader["Subject"]);
                        objObservationsModel.ObservationsEmployeeModel.LessonTopic = Convert.ToString(reader["LessonTopic"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsOnRollMale = Convert.ToInt32(reader["StudentsOnRollMale"] == DBNull.Value ? 0 : reader["StudentsOnRollMale"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsOnRollFemale = Convert.ToInt32(reader["StudentsOnRollFemale"] == DBNull.Value ? 0 : reader["StudentsOnRollFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.LearningSupport = Convert.ToInt32(reader["LearningSupport"] == DBNull.Value ? 0 : reader["LearningSupport"]);
                        objObservationsModel.ObservationsEmployeeModel.Branch = Convert.ToString(reader["Branch"]);
                        objObservationsModel.ObservationsEmployeeModel.GradeYear = Convert.ToString(reader["GradeYear"]);
                        objObservationsModel.ObservationsEmployeeModel.GroupSection = Convert.ToString(reader["GroupSection"]);
                        objObservationsModel.ObservationsEmployeeModel.Period = Convert.ToString(reader["Period"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsPresentMale = Convert.ToInt32(reader["StudentsPresentMale"] == DBNull.Value ? 0 : reader["StudentsPresentMale"]);
                        objObservationsModel.ObservationsEmployeeModel.StudentsPresentFemale = Convert.ToInt32(reader["StudentsPresentFemale"] == DBNull.Value ? 0 : reader["StudentsPresentFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.SENStudentsMale = Convert.ToInt32(reader["SENStudentsMale"] == DBNull.Value ? 0 : reader["SENStudentsMale"]);
                        objObservationsModel.ObservationsEmployeeModel.SENStudentsFemale = Convert.ToInt32(reader["SENStudentstFemale"] == DBNull.Value ? 0 : reader["SENStudentstFemale"]);
                        objObservationsModel.ObservationsEmployeeModel.VisitDATE = Convert.ToDateTime(reader["VisitDATE"]);
                        objObservationsModel.ObservationsEmployeeModel.VisitTime = Convert.ToDateTime(reader["VisitDATE"]).ToString("hh:mm tt");
                        objObservationsModel.ObservationsEmployeeModel.ObservationsYear = Convert.ToString(reader["ObservationsYear"]);
                        objObservationsModel.ObservationsEmployeeModel.ObservationsYearID = Convert.ToInt32(reader["AcademicYearID"] == DBNull.Value ? 0 : reader["AcademicYearID"]);
                        objObservationsModel.ObservationsEmployeeModel.Starter = Convert.ToBoolean(reader["Starter"] == DBNull.Value ? false : reader["Starter"]);
                        objObservationsModel.ObservationsEmployeeModel.TeacherInput = Convert.ToBoolean(reader["TeacherInput"] == DBNull.Value ? false : reader["TeacherInput"]);
                        objObservationsModel.ObservationsEmployeeModel.MainActivity = Convert.ToBoolean(reader["MainActivity"] == DBNull.Value ? false : reader["MainActivity"]);
                        objObservationsModel.ObservationsEmployeeModel.Plenary = Convert.ToBoolean(reader["Plenary"] == DBNull.Value ? false : reader["Plenary"]);

                        objObservationsModel.ObservationsEmployeeModel.ProgressEvidence = Convert.ToString(reader["ProgressEvidence"]);
                        objObservationsModel.ProficiencyScoreTotal = Convert.ToInt32(reader["Total"] == DBNull.Value ? 0 : reader["Total"]);
                        objObservationsModel.ProficiencyScoreOverall = Convert.ToDecimal(reader["OverAllScore"] == DBNull.Value ? 0 : reader["OverAllScore"]);
                        objObservationsModel.FormMode = Convert.ToInt32(reader["FormMode"] == DBNull.Value ? 0 : reader["FormMode"]);
                        objObservationsModel.PerObservationID = Convert.ToInt32(reader["PerObservationID"] == DBNull.Value ? 0 : reader["PerObservationID"]);
                        objObservationsModel.PostObservationID = Convert.ToInt32(reader["PostObservationID"] == DBNull.Value ? 0 : reader["PostObservationID"]);
                        objObservationsModel.PostObservationsReqStatusID = Convert.ToInt32(reader["PostObservationsReqStatusID"] == DBNull.Value ? 0 : reader["PostObservationsReqStatusID"]);
                        //*** F4
                        objObservationsModel.EmpCurrentCompanyID = Convert.ToInt32(reader["EmpCurrentCompanyID"] == DBNull.Value ? 0 : reader["EmpCurrentCompanyID"]);
                        //*** F6
                        objObservationsModel.ObservationsEmployeeModel.OBComments = Convert.ToString(reader["ObEvlComments"]);
                        objObservationsModel.Comments = Convert.ToString(reader["Comments"]);
                        // Nithin, 05-06-2020, Binding FormProcessID From DB
                        objObservationsModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? 0 : reader["FormProcessID"]);

                    }
                }
                reader.NextResult();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ObservationsRatingScaleWeightageDetails = new ObservationsRatingScaleWeightageDetails();

                        ObservationsRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        ObservationsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        ObservationsRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                        ObservationsRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                        ObservationsRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);


                        objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails.Add(ObservationsRatingScaleWeightageDetails);
                    }
                }
                reader.NextResult();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ObservationsRatingScaleWeightageDetails = new ObservationsRatingScaleWeightageDetails();

                        ObservationsRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        ObservationsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        ObservationsRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                        ObservationsRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                        ObservationsRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                        ObservationsRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);

                        objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails.Add(ObservationsRatingScaleWeightageDetails);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objObservationsModel;
        }
        public ObservationsEmployeeModel GetObservationsEmployeeInformation(ObservationsEmployeeModel objRequesterInfoViewModel)
        {
            //DropInEmployeeModel objRequesterInfoViewModel = new DropInEmployeeModel();

            objRequesterInfoViewModel.ObservationsRatingScaleWeightageDetails = new List<ObservationsRatingScaleWeightageDetails>();

            ObservationsRatingScaleWeightageDetails objObservationsRatingScaleWeightageDetails;
            //PDRPDropInProficiencyLevel objPDRPDropInProficiencyLevel;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@EmployeeID";
                sqlParameter.Value = objRequesterInfoViewModel.TeacherID;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@DropInsYear";
                sqlParameter.Value = objRequesterInfoViewModel.ObservationsYearID;
                sqlParameters[1] = sqlParameter;
                SqlDataReader reader = ExecuteProcedure("Hr_Stp_GetEmployeeInfoforPDRP", sqlParameters);


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //objRequesterInfoViewModel.RequestID = reader["RequestID"].ToString();
                        objRequesterInfoViewModel.TeacherID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? 0 : reader["EmployeeID"]);
                        objRequesterInfoViewModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                        //objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["EmployeeAlternativeID"]);
                        objRequesterInfoViewModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                        objRequesterInfoViewModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? 0 : reader["PositionID"]);
                        objRequesterInfoViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                        objRequesterInfoViewModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? 0 : reader["DepartmentID"]);
                        objRequesterInfoViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                        objRequesterInfoViewModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);
                        objRequesterInfoViewModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                        objRequesterInfoViewModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        objRequesterInfoViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        objRequesterInfoViewModel.ObservationsYear = Convert.ToString(reader["DropinYear"]);

                    }
                }

                reader.NextResult();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objObservationsRatingScaleWeightageDetails = new ObservationsRatingScaleWeightageDetails();

                        objObservationsRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        objObservationsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        objObservationsRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                        objObservationsRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                        objObservationsRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                        objObservationsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        objObservationsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                        objObservationsRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                        objObservationsRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);

                        objRequesterInfoViewModel.ObservationsRatingScaleWeightageDetails.Add(objObservationsRatingScaleWeightageDetails);
                    }
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objRequesterInfoViewModel;
        }
        //OLD GetObservationsEmployeeInformationOb
        public ObservationsEmployeeModel GetPreObservationsEmployeeInformation(ObservationsEmployeeModel objRequesterInfoViewModel, string culture)
        {
            //DropInEmployeeModel objRequesterInfoViewModel = new DropInEmployeeModel();



            ObservationsRatingScaleWeightageDetails objObservationsRatingScaleWeightageDetails;
            //PDRPDropInProficiencyLevel objPDRPDropInProficiencyLevel;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@EmployeeID";
                sqlParameter.Value = objRequesterInfoViewModel.TeacherID;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@AcademicYearID";
                sqlParameter.Value = objRequesterInfoViewModel.ObservationsYear;
                sqlParameters[1] = sqlParameter;

                SqlDataReader reader = ExecuteProcedure("Hr_Stp_GetEmployeeInfoforPDRPOb", sqlParameters);


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //objRequesterInfoViewModel.RequestID = reader["RequestID"].ToString();
                        objRequesterInfoViewModel.TeacherID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? 0 : reader["EmployeeID"]);
                        objRequesterInfoViewModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                        //objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["EmployeeAlternativeID"]);

                        objRequesterInfoViewModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? 0 : reader["PositionID"]);

                        objRequesterInfoViewModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? 0 : reader["DepartmentID"]);

                        objRequesterInfoViewModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);

                        //*** F4 REMOVED 
                        //objRequesterInfoViewModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);


                        if (objRequesterInfoViewModel.TeacherName == "")
                        {
                            //objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherName =!string.IsNullOrWhiteSpace(Convert.ToString(reader["EmployeeNameAr"]))? Convert.ToString(reader["EmployeeNameAr"]): Convert.ToString(reader["EmployeeName"]);
                            if (culture == "ar")
                            {
                                objRequesterInfoViewModel.TeacherName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["EmployeeNameAr"])) ? Convert.ToString(reader["EmployeeNameAr"]) : Convert.ToString(reader["EmployeeName"]);
                                objRequesterInfoViewModel.PositionTitle = !string.IsNullOrWhiteSpace(Convert.ToString(reader["PositionTitleAr"])) ? Convert.ToString(reader["PositionTitleAr"]) : Convert.ToString(reader["PositionTitle"]);
                                objRequesterInfoViewModel.DepartmentName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["DepartmentNameAr"])) ? Convert.ToString(reader["DepartmentNameAr"]) : Convert.ToString(reader["DepartmentName"]);
                                objRequesterInfoViewModel.SuperviserName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["SuperviserNameAr"])) ? Convert.ToString(reader["SuperviserNameAr"]) : Convert.ToString(reader["SuperviserName"]);
                                objRequesterInfoViewModel.CompanyName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["CompanyNameAr"])) ? Convert.ToString(reader["CompanyNameAr"]) : Convert.ToString(reader["CompanyName"]);
                            }
                            else
                            {
                                objRequesterInfoViewModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                                objRequesterInfoViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                                objRequesterInfoViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                                objRequesterInfoViewModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                                objRequesterInfoViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                            }
                        }
                    }
                }

                reader.NextResult();
                if (objRequesterInfoViewModel.ObservationsRatingScaleWeightageDetails == null || objRequesterInfoViewModel.ObservationsRatingScaleWeightageDetails.Count == 0)
                {
                    objRequesterInfoViewModel.ObservationsRatingScaleWeightageDetails = new List<ObservationsRatingScaleWeightageDetails>();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            objObservationsRatingScaleWeightageDetails = new ObservationsRatingScaleWeightageDetails();

                            objObservationsRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                            objObservationsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                            objObservationsRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                            objObservationsRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                            objObservationsRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                            objObservationsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                            objObservationsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                            objObservationsRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                            objObservationsRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);

                            objRequesterInfoViewModel.ObservationsRatingScaleWeightageDetails.Add(objObservationsRatingScaleWeightageDetails);
                        }
                    }
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objRequesterInfoViewModel;
        }
        public ObservationsModel GetObservationsEmployeeDetails(ObservationsModel ObservationsModel)
        {

            try
            {

                SqlParameter[] parameters =
                    {
                         new SqlParameter("@PDRPDropInsID",ObservationsModel.ObservationsEmployeeModel.ID),
                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPDropIns", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ObservationsModel.ObservationsEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                        ObservationsModel.ObservationsEmployeeModel.ReqStatusID = Convert.ToInt32(reader["ReqStatusID"] == DBNull.Value ? 0 : reader["ReqStatusID"]);
                        ObservationsModel.ObservationsEmployeeModel.LessonTopic = Convert.ToString(reader["LessonTopic"]);
                        ObservationsModel.ObservationsEmployeeModel.StudentsOnRollMale = Convert.ToInt32(reader["StudentsOnRollMale"] == DBNull.Value ? 0 : reader["StudentsOnRollMale"]);
                        ObservationsModel.ObservationsEmployeeModel.StudentsOnRollFemale = Convert.ToInt32(reader["StudentsOnRollFemale"] == DBNull.Value ? 0 : reader["StudentsOnRollFemale"]);
                        ObservationsModel.ObservationsEmployeeModel.VisitDATE = Convert.ToDateTime(reader["VisitDATE"]);                       
                        ObservationsModel.ObservationsEmployeeModel.ObservationsYearID = Convert.ToInt32(reader["DropinYearID"] == DBNull.Value ? 0 : reader["DropinYearID"]);
                        ObservationsModel.ObservationsEmployeeModel.ObservationsYear = Convert.ToString(reader["DropinYear"]);
                        ObservationsModel.ObservationsEmployeeModel.Branch = Convert.ToString(reader["Branch"]);
                        ObservationsModel.ObservationsEmployeeModel.GradeYear = Convert.ToString(reader["GradeYear"]);
                        ObservationsModel.ObservationsEmployeeModel.GroupSection = Convert.ToString(reader["GroupSection"]);
                        ObservationsModel.ObservationsEmployeeModel.StudentsPresentMale = Convert.ToInt32(reader["StudentsPresentMale"] == DBNull.Value ? 0 : reader["StudentsPresentMale"]);
                        ObservationsModel.ObservationsEmployeeModel.StudentsPresentFemale = Convert.ToInt32(reader["StudentsPresentFemale"] == DBNull.Value ? 0 : reader["StudentsPresentFemale"]);
                        //ObservationsModel.Outline = Convert.ToString(reader["Feedback"]);
                        //ObservationsModel.Feedback = Convert.ToString(reader["Outline"]);
                        //ObservationsModel.PreviousTargets = Convert.ToString(reader["PreviousTargets"]);
                        //ObservationsModel.Targets = Convert.ToInt32(reader["Targets"] == DBNull.Value ? 0 : reader["Targets"]);
                        //ObservationsModel.EmployeeAcknowledgedDropIn = Convert.ToBoolean(reader["EmployeeAcknowledgedDropIn"]);
                        //ObservationsModel.ProficiencyScoreTotal = Convert.ToInt32(reader["Total"] == DBNull.Value ? 0 : reader["Total"]);
                        //ObservationsModel.ProficiencyScoreOverall = Convert.ToInt32(reader["OverAllScore"] == DBNull.Value ? 0 : reader["OverAllScore"]);

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return ObservationsModel;
        }

        public ObservationsModel GetObservationsProficiencyLevel(ObservationsModel objObservationsModel)
        {
            //DropInManageModel objDropInsModel = new DropInManageModel();


            objObservationsModel.ObservationsProficiencyLevel = new List<ObservationsProficiencyLevel>();
            objObservationsModel.ObservationsRatingScaleWeightages = new List<ObservationsRatingScaleWeightages>();
            ObservationsProficiencyLevel objObservationsProficiencyLevel;
            ObservationsRatingScaleWeightages objObservationsRatingScaleWeightages;
            try
            {
                string strProcedure = "";
                SqlParameter[] sqlParameters = new SqlParameter[1];
                SqlParameter sqlParameter = new SqlParameter();





                strProcedure = "Hr_Stp_GetPDRPObservationProficiencies";
                sqlParameter.ParameterName = "@PDRPObservationEvlID";
                sqlParameter.Value = objObservationsModel.ObservationsEmployeeModel.ID;
                sqlParameters[0] = sqlParameter;


                SqlDataReader reader = ExecuteProcedure(strProcedure, sqlParameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objObservationsProficiencyLevel = new ObservationsProficiencyLevel();

                        objObservationsProficiencyLevel.ProficiencyMasterID = Convert.ToInt32(reader["ProficiencyMasterID"] == DBNull.Value ? 0 : reader["ProficiencyMasterID"]);
                        objObservationsProficiencyLevel.ProficiencyLevelID = Convert.ToInt32(reader["PDRPObservationEvlID"] == DBNull.Value ? 0 : reader["PDRPObservationEvlID"]);
                        objObservationsProficiencyLevel.Element = Convert.ToString(reader["Element"] == DBNull.Value ? "" : reader["Element"].ToString());
                        objObservationsProficiencyLevel.Element1 = Convert.ToString(reader["Element1"] == DBNull.Value ? "" : reader["Element1"].ToString());
                        objObservationsProficiencyLevel.Element2 = Convert.ToString(reader["Element2"] == DBNull.Value ? "" : reader["Element2"].ToString());
                        objObservationsProficiencyLevel.Element3 = Convert.ToString(reader["Element3"] == DBNull.Value ? "" : reader["Element3"].ToString());
                        objObservationsProficiencyLevel.Element4 = Convert.ToString(reader["Element4"] == DBNull.Value ? "" : reader["Element4"].ToString());
                        objObservationsProficiencyLevel.Element5 = Convert.ToString(reader["Element5"] == DBNull.Value ? "" : reader["Element5"].ToString());
                        objObservationsProficiencyLevel.Comments = Convert.ToString(reader["Comments"] == DBNull.Value ? "" : reader["Comments"].ToString());
                        objObservationsProficiencyLevel.Element_2 = Convert.ToString(reader["Element_2"] == DBNull.Value ? "" : reader["Element_2"].ToString());
                        objObservationsProficiencyLevel.Element1_2 = Convert.ToString(reader["Element1_2"] == DBNull.Value ? "" : reader["Element1_2"].ToString());
                        objObservationsProficiencyLevel.Element2_2 = Convert.ToString(reader["Element2_2"] == DBNull.Value ? "" : reader["Element2_2"].ToString());
                        objObservationsProficiencyLevel.Element3_2 = Convert.ToString(reader["Element3_2"] == DBNull.Value ? "" : reader["Element3_2"].ToString());
                        objObservationsProficiencyLevel.Element4_2 = Convert.ToString(reader["Element4_2"] == DBNull.Value ? "" : reader["Element4_2"].ToString());
                        objObservationsProficiencyLevel.Element5_2 = Convert.ToString(reader["Element5_2"] == DBNull.Value ? "" : reader["Element5_2"].ToString());
                        objObservationsProficiencyLevel.Comments = Convert.ToString(reader["Comments"] == DBNull.Value ? "" : reader["Comments"].ToString());
                        //*** F5
                        objObservationsProficiencyLevel.ProficiencyScore = Convert.ToString(reader["ProficiencyScore"] == DBNull.Value ? "" : (reader["ProficiencyScore"].ToString() == "0" ? "N/A" : reader["ProficiencyScore"].ToString()));
                        //objObservationsProficiencyLevel.ProficiencyScore = Convert.ToInt32(reader["ProficiencyScore"] == DBNull.Value ? 0 : reader["ProficiencyScore"]);

                        objObservationsProficiencyLevel.ProficiencyFormula = Convert.ToString(reader["ProficiencyFormula"]);
                        objObservationsProficiencyLevel.ProficiencyValue = Convert.ToInt32(reader["ProficiencyValue"] == DBNull.Value ? 0 : reader["ProficiencyValue"]);
                        if (objObservationsModel.ProficiencyScoreFormula == null || objObservationsModel.ProficiencyScoreFormula == "")
                        {
                            objObservationsModel.ProficiencyScoreFormula = Convert.ToString(objObservationsProficiencyLevel.ProficiencyFormula);
                        }
                        //}
                        objObservationsModel.ObservationsProficiencyLevel.Add(objObservationsProficiencyLevel);
                    }
                }
                for (int i = 0; i <= objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails.Count; i++)
                {
                    objObservationsRatingScaleWeightages = new ObservationsRatingScaleWeightages();
                    if (i == 0)
                    {
                        objObservationsRatingScaleWeightages.RatingScaleID = 0;
                        //objPDRPDropInsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        objObservationsRatingScaleWeightages.RatingScaleNumber = 0;
                        //objPDRPDropInsRatingScaleWeightageDetails.FinalScoreSlab = Convert.ToString(reader["FinalScoreSlab"]);
                        //objPDRPDropInsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        //objPDRPDropInsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);


                        objObservationsModel.ObservationsRatingScaleWeightages.Add(objObservationsRatingScaleWeightages);
                    }
                    else
                    {
                        objObservationsRatingScaleWeightages.RatingScaleID = Convert.ToInt32(objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails[i - 1].RatingScaleID);
                        //objPDRPDropInsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        objObservationsRatingScaleWeightages.RatingScaleNumber = Convert.ToInt32(objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails[i - 1].RatingScaleNumber);
                        //objPDRPDropInsRatingScaleWeightageDetails.FinalScoreSlab = Convert.ToString(reader["FinalScoreSlab"]);
                        //objPDRPDropInsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        //objPDRPDropInsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);


                        objObservationsModel.ObservationsRatingScaleWeightages.Add(objObservationsRatingScaleWeightages);
                    }
                }



                //reader.NextResult();

                //if (reader.HasRows)
                //{
                //    while (reader.Read())
                //    {
                //        objObservationsRatingScaleWeightages = new ObservationsRatingScaleWeightages();

                //        objObservationsRatingScaleWeightages.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                //        //objPDRPDropInsRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                //        objObservationsRatingScaleWeightages.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                //        //objPDRPDropInsRatingScaleWeightageDetails.FinalScoreSlab = Convert.ToString(reader["FinalScoreSlab"]);
                //        //objPDRPDropInsRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                //        //objPDRPDropInsRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);


                //        objObservationsModel.ObservationsRatingScaleWeightages.Add(objObservationsRatingScaleWeightages);
                //    }
                //}

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objObservationsModel;
        }

        public RequestFormsProcessModel SavePreObservations(ObservationsModel objObservationsModel, int ReqStatusID, int CreatedBy)
        {
            string Result = "", FormProcessID = "", GroupID = "", RequestID = "", ApproverID = "";
            //OperationDetails oprationDetails = new OperationDetails();
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                string VisitDateTime = Convert.ToDateTime(Convert.ToDateTime(objObservationsModel.ObservationsEmployeeModel.VisitDATE).ToShortDateString()).ToString("yyyy/MM/dd");
                VisitDateTime = VisitDateTime + " " + objObservationsModel.ObservationsEmployeeModel.VisitTime;

                SqlParameter[] parameters =
                    {
                         new SqlParameter("@RequestID", objObservationsModel.ObservationsEmployeeModel.RequestID),
                         new SqlParameter("@PDRPPreObservationID", objObservationsModel.ObservationsEmployeeModel.ID),
                         new SqlParameter("@CompanyID",objObservationsModel.ObservationsEmployeeModel.CompanyID),
                         new SqlParameter("@ReqStatusID", ReqStatusID),
                         new SqlParameter("@RequesterEmployeeID",objObservationsModel.ObservationsEmployeeModel.RequesterID),
                         new SqlParameter("@TeacherID",objObservationsModel.ObservationsEmployeeModel.TeacherID),
                         new SqlParameter("@LessonTopic",objObservationsModel.ObservationsEmployeeModel.LessonTopic),
                         new SqlParameter("@StudentsOnRollMale",objObservationsModel.ObservationsEmployeeModel.StudentsOnRollMale),
                         new SqlParameter("@StudentsOnRollFemale",objObservationsModel.ObservationsEmployeeModel.StudentsOnRollFemale),
                         new SqlParameter("@VisitDATE",VisitDateTime),

                         new SqlParameter("@ObservationsYear",objObservationsModel.ObservationsEmployeeModel.ObservationsYearID),

                         new SqlParameter("@Branch",objObservationsModel.ObservationsEmployeeModel.Branch),
                         new SqlParameter("@GradeYear",objObservationsModel.ObservationsEmployeeModel.GradeYear),
                         new SqlParameter("@GroupSection",objObservationsModel.ObservationsEmployeeModel.GroupSection),
                         new SqlParameter("@StudentsPresentMale",objObservationsModel.ObservationsEmployeeModel.StudentsPresentMale),
                         new SqlParameter("@StudentsPresentFemale",objObservationsModel.ObservationsEmployeeModel.StudentsPresentFemale),

                         new SqlParameter("@UnitThemeLessonTopic",objObservationsModel.ObservationsEmployeeModel.UnitThemeLessonTopic),
                         new SqlParameter("@Subject",objObservationsModel.ObservationsEmployeeModel.Subject),
                         new SqlParameter("@Period",objObservationsModel.ObservationsEmployeeModel.Period),
                         new SqlParameter("@YearPlan",objObservationsModel.ObservationsEmployeeModel.YearPlan),
                         new SqlParameter("@IsOnTrackYearlyPlans",objObservationsModel.IsOnTrackYearlyPlans),
                         new SqlParameter("@IsOnTrackYearlyPlansNo",objObservationsModel.IsOnTrackYearlyPlansNo),
                         new SqlParameter("@ObjectivesList",objObservationsModel.ObjectivesList),
                         new SqlParameter("@ObservationDayLesson",objObservationsModel.ObservationDayLesson),
                         new SqlParameter("@IsTaughtToThisClass",objObservationsModel.IsTaughtToThisClass),
                         new SqlParameter("@SpecificItemsToWatch",objObservationsModel.SpecificItemsToWatch),
                         new SqlParameter("@ItemsToBeAware",objObservationsModel.ItemsToBeAware),

                         new SqlParameter("@EmployeeName",objObservationsModel.ObservationsEmployeeModel.TeacherName),
                         new SqlParameter("@Department",objObservationsModel.ObservationsEmployeeModel.DepartmentName),
                         new SqlParameter("@Designation",objObservationsModel.ObservationsEmployeeModel.PositionTitle),
                         new SqlParameter("@LineManager",objObservationsModel.ObservationsEmployeeModel.SuperviserName),
                         new SqlParameter("@School",objObservationsModel.ObservationsEmployeeModel.CompanyName),
                         new SqlParameter("@Comments",objObservationsModel.Comments),
                         new SqlParameter("@CreatedBy",CreatedBy),

                    };
                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SavePDRPPreObservation", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        FormProcessID = reader["FormProcessID"].ToString();
                        GroupID = reader["GroupID"].ToString();
                        ApproverID = reader["ApproverID"].ToString();
                        RequestID = reader["RequestID"].ToString();
                        Result = reader["Result"].ToString();

                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? 0 : reader["FormProcessID"]);
                        requestFormsProcessModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? 0 : reader["RequestID"]);

                        //*** Geting Previous FormProcessID form Compleation email Option
                        try
                        {
                            requestFormsProcessModel.FormInstanceID = Convert.ToInt32(reader["PreFormProcessID"] == DBNull.Value ? 0 : reader["PreFormProcessID"]);
                        }
                        catch (Exception)
                        { }
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return requestFormsProcessModel;
        }
        public RequestFormsProcessModel SavePostObservations(ObservationsModel objObservationsModel, int ReqStatusID)
        {
            string Result = "", FormProcessID = "", GroupID = "", RequestID = "", ApproverID = "";
            //OperationDetails oprationDetails = new OperationDetails();
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                string VisitDateTime = Convert.ToDateTime(Convert.ToDateTime(objObservationsModel.ObservationsEmployeeModel.VisitDATE).ToShortDateString()).ToString("yyyy/MM/dd");
                VisitDateTime = VisitDateTime + " " + objObservationsModel.ObservationsEmployeeModel.VisitTime;

                SqlParameter[] parameters =
                    {
                         new SqlParameter("@RequestID", objObservationsModel.ObservationsEmployeeModel.RequestID),
                         new SqlParameter("@PDRPPostObservationID", objObservationsModel.ObservationsEmployeeModel.ID),
                         new SqlParameter("@CompanyID",objObservationsModel.ObservationsEmployeeModel.CompanyID),
                         new SqlParameter("@ReqStatusID", ReqStatusID),
                         new SqlParameter("@RequesterEmployeeID",objObservationsModel.ObservationsEmployeeModel.RequesterID),

                         new SqlParameter("@LessonTopic",objObservationsModel.ObservationsEmployeeModel.LessonTopic),

                         new SqlParameter("@VisitDATE",VisitDateTime),

                         //new SqlParameter("@ObservationsYear",objObservationsModel.ObservationsEmployeeModel.ObservationsYearID),

                         new SqlParameter("@Branch",objObservationsModel.ObservationsEmployeeModel.Branch),
                         new SqlParameter("@GradeYear",objObservationsModel.ObservationsEmployeeModel.GradeYear),
                         new SqlParameter("@GroupSection",objObservationsModel.ObservationsEmployeeModel.GroupSection),

                         new SqlParameter("@ParticularlyWellLesson",objObservationsModel.ParticularlyWellLesson),
                         new SqlParameter("@FurtherImprovementLesson",objObservationsModel.FurtherImprovementLesson),
                         new SqlParameter("@ChildrensHighAbility",objObservationsModel.ChildrensHighAbility),
                         new SqlParameter("@ChildrensAverageAbility",objObservationsModel.ChildrensAverageAbility),
                         new SqlParameter("@ChildrensBelowAverageAbility",objObservationsModel.ChildrensBelowAverageAbility),


                          new SqlParameter("@Comments",objObservationsModel.Comments),
                         new SqlParameter("@CreatedBy",objObservationsModel.ObservationsEmployeeModel.RequesterID),

                    };
                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SavePDRPPostObservation", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        FormProcessID = reader["FormProcessID"].ToString();
                        GroupID = reader["GroupID"].ToString();
                        ApproverID = reader["ApproverID"].ToString();
                        RequestID = reader["RequestID"].ToString();
                        Result = reader["Result"].ToString();

                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? 0 : reader["FormProcessID"]);
                        requestFormsProcessModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? 0 : reader["RequestID"]);

                        //*** Geting Previous FormProcessID form Compleation email Option
                        try
                        {
                            requestFormsProcessModel.FormInstanceID = Convert.ToInt32(reader["PreFormProcessID"] == DBNull.Value ? 0 : reader["PreFormProcessID"]);
                        }
                        catch (Exception)
                        { }
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return requestFormsProcessModel;
        }
        public RequestFormsProcessModel SaveObservationsEvl(ObservationsModel objObservationsModel, int ReqStatusID)
        {
            string Result = "", FormProcessID = "", GroupID = "", RequestID = "", ApproverID = "";
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                DataTable RatingScaleTable = new DataTable();
                RatingScaleTable.Columns.Add("RatingScaleCharacter");
                RatingScaleTable.Columns.Add("RatingScaleNumber");
                RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
                RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
                RatingScaleTable.Columns.Add("DefinitionName");
                RatingScaleTable.Columns.Add("DefinitionDetails");
                RatingScaleTable.Columns.Add("DefinitionName2");
                RatingScaleTable.Columns.Add("DefinitionDetails2");
                foreach (ObservationsRatingScaleWeightageDetails Level in objObservationsModel.ObservationsEmployeeModel.ObservationsRatingScaleWeightageDetails)
                {

                    RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails, Level.DefinitionName2, Level.DefinitionDetails2);

                }

                if (objObservationsModel.FormMode == 3)
                {
                    ReqStatusID = 4;
                }

                DataTable ProficiencyLevelTable = new DataTable();
                ProficiencyLevelTable.Columns.Add("ProficiencyMasterID");
                ProficiencyLevelTable.Columns.Add("PDRPObservationEvlID");
                ProficiencyLevelTable.Columns.Add("ProficiencyScore");
                ProficiencyLevelTable.Columns.Add("ProficiencyFormula");
                ProficiencyLevelTable.Columns.Add("ProficiencyValue", System.Type.GetType("System.Decimal"));
                ProficiencyLevelTable.Columns.Add("Comments");
                //int PDRPDropInsID = DBNull.Value;
                string Formla = objObservationsModel.ProficiencyScoreFormula;
                foreach (ObservationsProficiencyLevel Level in objObservationsModel.ObservationsProficiencyLevel)
                {
                    //if (Level.ProficiencyScore > 0) //*** F5
                    if (Level.ProficiencyScore != null)
                    {
                        string FormatedFormla = Formla.Replace("Rating", Level.ProficiencyScore.ToString());
                        if (Level.ProficiencyScore == "N/A")
                        { ProficiencyLevelTable.Rows.Add(Level.ProficiencyMasterID, objObservationsModel.ObservationsEmployeeModel.ID, 0, FormatedFormla, GetFormulaValue(Formla, 0), Level.Comments); }
                        else
                        {
                            ProficiencyLevelTable.Rows.Add(Level.ProficiencyMasterID, objObservationsModel.ObservationsEmployeeModel.ID, Level.ProficiencyScore, FormatedFormla, GetFormulaValue(Formla, Convert.ToInt32(Level.ProficiencyScore)), Level.Comments);
                        }
                    }
                }
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@RequestID", objObservationsModel.ObservationsEmployeeModel.RequestID),
                         new SqlParameter("@CompanyID",objObservationsModel.ObservationsEmployeeModel.CompanyID),
                         new SqlParameter("@ReqStatusID", ReqStatusID),
                         new SqlParameter("@RequesterEmployeeID",objObservationsModel.ObservationsEmployeeModel.RequesterID),
                         new SqlParameter("@TeacherID",objObservationsModel.ObservationsEmployeeModel.TeacherID),
                         new SqlParameter("@Subject",objObservationsModel.ObservationsEmployeeModel.Subject),
                         new SqlParameter("@LessonTopic",objObservationsModel.ObservationsEmployeeModel.LessonTopic),
                         new SqlParameter("@StudentsOnRollMale",objObservationsModel.ObservationsEmployeeModel.StudentsOnRollMale),
                         new SqlParameter("@StudentsOnRollFemale",objObservationsModel.ObservationsEmployeeModel.StudentsOnRollFemale),
                         new SqlParameter("@LearningSupport",objObservationsModel.ObservationsEmployeeModel.LearningSupport),
                         new SqlParameter("@Branch",objObservationsModel.ObservationsEmployeeModel.Branch),
                         new SqlParameter("@GradeYear",objObservationsModel.ObservationsEmployeeModel.GradeYear),
                         new SqlParameter("@GroupSection",objObservationsModel.ObservationsEmployeeModel.GroupSection),
                         new SqlParameter("@Period",objObservationsModel.ObservationsEmployeeModel.Period),
                         new SqlParameter("@StudentsPresentMale",objObservationsModel.ObservationsEmployeeModel.StudentsPresentMale),
                         new SqlParameter("@StudentsPresentFemale",objObservationsModel.ObservationsEmployeeModel.StudentsPresentFemale),
                         new SqlParameter("@SENStudentsMale",objObservationsModel.ObservationsEmployeeModel.SENStudentsMale),
                         new SqlParameter("@SENStudentsFemale",objObservationsModel.ObservationsEmployeeModel.SENStudentsFemale),

                         new SqlParameter("@VisitDATE",objObservationsModel.ObservationsEmployeeModel.VisitDATE),
                         new SqlParameter("@ObservationsYear",objObservationsModel.ObservationsEmployeeModel.ObservationsYearID),
                         new SqlParameter("@Starter",objObservationsModel.ObservationsEmployeeModel.Starter),
                         new SqlParameter("@TeacherInput",objObservationsModel.ObservationsEmployeeModel.TeacherInput),
                         new SqlParameter("@MainActivity",objObservationsModel.ObservationsEmployeeModel.MainActivity),
                         new SqlParameter("@Plenary",objObservationsModel.ObservationsEmployeeModel.Plenary),

                         new SqlParameter("@Total",objObservationsModel.ProficiencyScoreTotal),
                         new SqlParameter("@OverAllScore",objObservationsModel.ProficiencyScoreOverall),
                         new SqlParameter("@FormMode",objObservationsModel.FormMode),
                         new SqlParameter("@ObEvlComments",objObservationsModel.ObservationsEmployeeModel.OBComments),
                         new SqlParameter("@ProgressEvidence",objObservationsModel.ObservationsEmployeeModel.ProgressEvidence),



                         new SqlParameter("@CreatedBy",objObservationsModel.ObservationsEmployeeModel.RequesterID),
                         new SqlParameter("@ID", objObservationsModel.ObservationsEmployeeModel.ID),

                         new SqlParameter("@PDRPObservationTable",ProficiencyLevelTable),
                         new SqlParameter("@RatingScaleTable",RatingScaleTable),
                          new SqlParameter("@Comments",objObservationsModel.Comments),

                    };
                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_SavePDRPObservationEvaluation", parameters);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        FormProcessID = reader["FormProcessID"].ToString();
                        GroupID = reader["GroupID"].ToString();
                        ApproverID = reader["ApproverID"].ToString();
                        RequestID = reader["RequestID"].ToString();
                        Result = reader["Result"].ToString();

                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? 0 : reader["FormProcessID"]);
                        requestFormsProcessModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? 0 : reader["RequestID"]);
                        try
                        {
                            //*** Geting Previous FormProcessID form Compleation email Option
                            requestFormsProcessModel.FormInstanceID = Convert.ToInt32(reader["PreFormProcessID"] == DBNull.Value ? 0 : reader["PreFormProcessID"]);
                        }
                        catch (Exception)
                        { }
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return requestFormsProcessModel;
        }

        #region Observation Process Status Report
        public List<ObservationStatusReportModel> GetObservationtatusReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? yearId, int userId)
        {
            List<ObservationStatusReportModel> observationStatusReportList = null;
            try
            {
                observationStatusReportList = new List<ObservationStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_ObservationProcessStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@CurrentStatus", statusId);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(toDate));
                sqlCommand.Parameters.AddWithValue("@YearId", yearId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ObservationStatusReportModel observationStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        observationStatusReportModel = new ObservationStatusReportModel();
                        observationStatusReportModel.OrganizationName = Convert.ToString(sqlDataReader["OrganizationName"] == DBNull.Value ? "" : sqlDataReader["OrganizationName"].ToString());
                        observationStatusReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        observationStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        observationStatusReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        observationStatusReportModel.IsObservationProcessInitiated = Convert.ToString(sqlDataReader["IsObservationProcessInitiated"] == DBNull.Value ? "0" : sqlDataReader["IsObservationProcessInitiated"]);
                        observationStatusReportModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        observationStatusReportModel.TotalScore = Convert.ToString(sqlDataReader["TotalScore"] == DBNull.Value ? "" : sqlDataReader["TotalScore"]);
                        observationStatusReportModel.TotalRating = Convert.ToString(sqlDataReader["TotalRating"] == DBNull.Value ? "" : sqlDataReader["TotalRating"]);
                        observationStatusReportModel.ObservationVisitDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ObservationVisitDate"] == DBNull.Value ? "" : sqlDataReader["ObservationVisitDate"]));
                        observationStatusReportModel.PreObservationRequest = Convert.ToString(sqlDataReader["PreObservationRequest"] == DBNull.Value ? "" : sqlDataReader["PreObservationRequest"]);
                        observationStatusReportModel.PostObservationRequest = Convert.ToString(sqlDataReader["PostObservationRequest"] == DBNull.Value ? "" : sqlDataReader["PostObservationRequest"]);
                        observationStatusReportModel.ObservationRequest = Convert.ToString(sqlDataReader["ObservationRequest"] == DBNull.Value ? "" : sqlDataReader["ObservationRequest"]);
                        observationStatusReportModel.ObservationSignOffDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ObservationSignOffDate"] == DBNull.Value ? "" : sqlDataReader["ObservationSignOffDate"]));
                        observationStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        observationStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        observationStatusReportList.Add(observationStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return observationStatusReportList;
        }
        #endregion
    }
}
