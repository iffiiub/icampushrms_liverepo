﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.Forms;
using System.Data.SqlClient;
using HRMS.Entities.PDRP;
using System.Data;
using HRMS.Entities;
using System.Web.Mvc;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess.PDRP
{

    public class PDRPCommonModelDB : CommonDB
    {
        //*** F25 Peding task for Employee on Reject
        /// <summary>
        /// Rejecting request
        /// </summary>
        /// <param name="requestFormsApproveModel"></param>
        /// <returns></returns>
        public OperationDetails RejectPDRPRequestForm(RequestFormsApproveModel requestFormsApproveModel, int EmployeeID, int FormID, int FormInstanceID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_RejectPDRPRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", requestFormsApproveModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ApproverEmployeeID", requestFormsApproveModel.ApproverEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", requestFormsApproveModel.Comments);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@FormID", FormID);
                sqlCommand.Parameters.AddWithValue("@FormInstanceID", FormInstanceID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
        //*** F10
        public int GetCorrectFormProcessID(int FormInstanceID, int FormID)
        {
            int FormProcessID = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT FormProcessID from HR_RequestFormsProcess WHERE FormID="+ FormID+" AND FormInstanceID=" + FormInstanceID, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? 0 : reader["FormProcessID"]);
                    }
                }

            }
            catch (Exception exception)
            {
                return FormProcessID;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return FormProcessID;
        }

        //*** F4
        public int IsAnnualAppraisalStarted(int? YearID, int? EmployeeID)
        {
            int IsAP = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                //*** This command true only is A1 of Annual appraisal is saves. the second line check if annual appraisal is initiated or not
                //sqlCommand = new SqlCommand("SELECT COUNT(*) Count FROM  HR_PDRPAnnualAppraisalTeaching WHERE TeacherID=" + EmployeeID.ToString() + " AND AppraisalYear=" + DropInsYear.ToString() + " AND DropInsRate IS NOT NULL", sqlConnection);
                sqlCommand = new SqlCommand("SELECT COUNT(*) Count FROM  HR_PDRPAnnualAppraisalTeaching  " +
                    "WHERE TeacherID=" + EmployeeID.ToString() + " AND AppraisalYear=" + YearID.ToString() + " AND CompanyID=(SELECT CompanyID FROM HR_EmployeeDetail WHERE EmployeeID=" + EmployeeID.ToString() + ")", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        IsAP = Convert.ToInt32(reader["Count"] == DBNull.Value ? 0 : reader["Count"]);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsAP;
        }
        public int GetNextApproverGroupAndEmployee(int formProcessID)
        {
            int NextApproverEmployeeID = 0;
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            RequestFormsApproverEmailModel requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetNextApproverGroupAndEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@IsFirstPriority", 1);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        NextApproverEmployeeID = Convert.ToInt32(reader["NextApproverEmployeeID"] == DBNull.Value ? 0 : reader["NextApproverEmployeeID"]);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return NextApproverEmployeeID;
        }
        public OperationDetails CheckAnnualAppraisalWorkFlowAllEmployee(IEnumerable<GoalSettingModel> SelectedTeachersList, DataTable dtResult, int PeriodId)
        {
            string Result = "";
            OperationDetails oprationDetails = new OperationDetails();
            DataTable dtAppraisalTeachinTable = new DataTable();
            dtAppraisalTeachinTable.Columns.Add("EmployeeID");
            dtAppraisalTeachinTable.Columns.Add("FormID");
            foreach (var item in SelectedTeachersList)
            {
                int FrmID = 0;
                if (item.PerformanceGroupId == 4) // *** Form D
                {
                    if (PeriodId == 1)
                    {
                        FrmID = 46;
                    }
                    else
                    {
                        FrmID = 47;
                    }
                }
                else
                {
                    if (PeriodId == 1)
                    {
                        FrmID = 40;
                    }
                    else
                    {
                        FrmID = 42;
                    }
                }
                dtAppraisalTeachinTable.Rows.Add(item.EmployeeID, FrmID);
            }

            try
            {

                SqlParameter[] parameters =
                   {
                         new SqlParameter("@RequesterEmployeeID",dtAppraisalTeachinTable)

                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_PDRPGoalsettingWorkFlowAllEmployee", parameters);


                if (reader.HasRows)
                {

                    //dtResult.Columns.Add("FormProcessID");
                    while (reader.Read())
                    {
                        Result = reader["Result"].ToString();
                        if (Result.Trim().Length > 0)
                        {
                            oprationDetails.Success = false;
                            oprationDetails.CssClass = "error";
                            oprationDetails.Message = Result;
                        }
                        else
                        {
                            oprationDetails.Success = true;
                            oprationDetails.CssClass = "success";
                            oprationDetails.Message = "Saved successfully.";
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public OperationDetails CheckGoalsettingWorkFlowAllEmployee(IEnumerable<GoalSettingModel> SelectedTeachersList, DataTable dtResult, int FormID)
        {
            string Result = "";
            OperationDetails oprationDetails = new OperationDetails();
            DataTable dtAppraisalTeachinTable = new DataTable();
            dtAppraisalTeachinTable.Columns.Add("EmployeeID");
            dtAppraisalTeachinTable.Columns.Add("FormID");
            foreach (var item in SelectedTeachersList)
            {
                dtAppraisalTeachinTable.Rows.Add(item.EmployeeID, FormID);
            }

            try
            {

                SqlParameter[] parameters =
                   {
                         new SqlParameter("@RequesterEmployeeID",dtAppraisalTeachinTable)
                         //new SqlParameter("@FormID",FormID)

                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_PDRPGoalsettingWorkFlowAllEmployee", parameters);


                if (reader.HasRows)
                {

                    //dtResult.Columns.Add("FormProcessID");
                    while (reader.Read())
                    {
                        Result = reader["Result"].ToString();
                        if (Result.Trim().Length > 0)
                        {
                            oprationDetails.Success = false;
                            oprationDetails.CssClass = "error";
                            oprationDetails.Message = Result;
                        }
                        else
                        {
                            oprationDetails.Success = true;
                            oprationDetails.CssClass = "success";
                            oprationDetails.Message = "Saved successfully.";
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public IEnumerable<GoalSettingModel> GetGoalSettingDataList(int? companyId, int? yearId, List<SelectListItem> performanceGroups, int userId)
        {
            IList<GoalSettingModel> goalSettingModelList = null;
            try
            {
                goalSettingModelList = new List<GoalSettingModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPNonTeachGetGoalSettingData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@YearId", yearId);
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    GoalSettingModel goalSettingModel;
                    while (sqlDataReader.Read())
                    {
                        goalSettingModel = new GoalSettingModel();
                        goalSettingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        goalSettingModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                        goalSettingModel.PerformanceGroupName = Convert.ToString(sqlDataReader["PerformanceGroupName"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupName"]);
                        goalSettingModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        goalSettingModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        goalSettingModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        goalSettingModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        goalSettingModel.SettingGoalsStartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["SettingGoalsStartDate"] == DBNull.Value ? "" : sqlDataReader["SettingGoalsStartDate"]));
                        goalSettingModel.SettingGoalsDueDate = GetFormattedDate_DDMMYYYY( Convert.ToString(sqlDataReader["SettingGoalsDueDate"] == DBNull.Value ? "" : sqlDataReader["SettingGoalsDueDate"]));
                        goalSettingModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"]);
                        goalSettingModel.IsGoalSubmitted = Convert.ToBoolean(sqlDataReader["IsGoalSubmitted"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSubmitted"]);
                        goalSettingModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        //goalSettingModel.ProcessName = Convert.ToString(sqlDataReader["ProcessName"] == DBNull.Value ? "" : sqlDataReader["ProcessName"]);
                        goalSettingModelList.Add(goalSettingModel);
                    }
                }
                sqlDataReader.NextResult(); //*** Reading Performance Group NAmes
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        if (performanceGroups.Count == 0)
                        {
                            performanceGroups.Add(new SelectListItem() { Text = "Select", Value = "0" });
                        }
                        performanceGroups.Add(new SelectListItem() { Text = Convert.ToString(sqlDataReader["PerformanceGroupName"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupName"].ToString()), Value = Convert.ToString(sqlDataReader["PerformanceGroupID"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupID"].ToString()) });
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingModelList;
        }
        //*** F10
        public OperationDetails ReverseTask(int FormID,int PerformanceGroupId, int FormInstanceID,int UserID)
        {
            OperationDetails OperationDetails = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (FormID == 35)
                {
                    if (PerformanceGroupId == 1)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormA", sqlConnection);
                    }
                    else if (PerformanceGroupId == 2)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormB", sqlConnection);
                    }
                    else if (PerformanceGroupId == 3)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormC", sqlConnection);
                    }
                }
                else if (FormID == 40)
                {
                    if (PerformanceGroupId == 1)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormA_Mid", sqlConnection);
                    }
                    else if (PerformanceGroupId == 2)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormB_Mid", sqlConnection);
                    }
                    else if (PerformanceGroupId == 3)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormC_Mid", sqlConnection);
                    }

                }
                else if (FormID == 46)
                {
                    if (PerformanceGroupId == 4)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormD_Mid", sqlConnection);
                    }
                }
                else if (FormID == 42)
                {
                    if (PerformanceGroupId == 1)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormA_Full", sqlConnection);
                    }
                    else if (PerformanceGroupId == 2)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormB_Full", sqlConnection);
                    }
                    else if (PerformanceGroupId == 3)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormC_Full", sqlConnection);
                    }

                }
                else if (FormID == 47)
                {
                    if (PerformanceGroupId == 4)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_PDRPReversaTaskFormD_Full", sqlConnection);
                    }
                }




                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormID", FormID);
                sqlCommand.Parameters.AddWithValue("@FormInstanceID", FormInstanceID);

                sqlCommand.Parameters.AddWithValue("@UserID", UserID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    AnnualAppraisalReverceData annualGoalProcessModel;
                    while (sqlDataReader.Read())
                    {
                        string Result = "";
                        Result=Convert.ToString(sqlDataReader["Result"] == DBNull.Value ? "" : sqlDataReader["Result"]);
                        if (Result == "SUCCESS")
                        {
                            OperationDetails.Message = "Task Reversed successfully";
                            OperationDetails.Success = true;
                            OperationDetails.CssClass = "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OperationDetails.Success = false;
                OperationDetails.CssClass = "error";
                OperationDetails.Message = "Error occured while Reversing the Task.";
            }

            return OperationDetails;
        }
        public IEnumerable<AnnualAppraisalReverceData> GetAnnualAppraisalReverseList(int? companyId, int? yearId, List<SelectListItem> performanceGroups, int PeriodId, int userId)
        {
            IList<AnnualAppraisalReverceData> annualAppraisalProcessList = null;
            try
            {
                annualAppraisalProcessList = new List<AnnualAppraisalReverceData>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetAnnualAppraisalReverseData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@YearId", yearId);

                sqlCommand.Parameters.AddWithValue("@PeriodId", PeriodId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    AnnualAppraisalReverceData annualGoalProcessModel;
                    while (sqlDataReader.Read())
                    {
                        annualGoalProcessModel = new AnnualAppraisalReverceData();
                        annualGoalProcessModel.IsReversed = Convert.ToString(sqlDataReader["IsReversed"] == DBNull.Value ? null : sqlDataReader["IsReversed"]);
                        annualGoalProcessModel.MidIsReversed = Convert.ToString(sqlDataReader["MidIsReversed"] == DBNull.Value ? null : sqlDataReader["MidIsReversed"]);
                        annualGoalProcessModel.FullIsReversed = Convert.ToString(sqlDataReader["FullIsReversed"] == DBNull.Value ? null : sqlDataReader["FullIsReversed"]);
                        annualGoalProcessModel.FormID = Convert.ToInt32(sqlDataReader["FormID"] == DBNull.Value ? 0 : sqlDataReader["FormID"]);
                        annualGoalProcessModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? 0 : sqlDataReader["PerformanceGroupId"]);
                        annualGoalProcessModel.FormInstanceID = Convert.ToInt32(sqlDataReader["FormInstanceID"] == DBNull.Value ? 0 : sqlDataReader["FormInstanceID"]);
                        annualGoalProcessModel.RequestId = Convert.ToInt32(sqlDataReader["RequestId"] == DBNull.Value ? 0 : sqlDataReader["RequestId"]);
                        annualGoalProcessModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        annualGoalProcessModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        annualGoalProcessModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        annualGoalProcessModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        annualGoalProcessModel.ReqStatusId = Convert.ToInt32(sqlDataReader["ReqStatusId"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusId"]);
                        annualGoalProcessModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"]);
                        annualGoalProcessModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"] == DBNull.Value ? "" : sqlDataReader["CompanyName"]);
                        annualGoalProcessModel.ProcessInitializeBy = Convert.ToInt32(sqlDataReader["ProcessInitializeBy"] == DBNull.Value ? "0" : sqlDataReader["ProcessInitializeBy"]);
                        annualGoalProcessModel.ProcessInitializeByName = Convert.ToString(sqlDataReader["ProcessInitializeByName"] == DBNull.Value ? "" : sqlDataReader["ProcessInitializeByName"]);

                        annualGoalProcessModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"] == DBNull.Value ? "0" : sqlDataReader["DepartmentID"]);
                        annualGoalProcessModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_1"]);
                        annualGoalProcessModel.Cnt = Convert.ToInt32(sqlDataReader["Cnt"] == DBNull.Value ? "0" : sqlDataReader["Cnt"]);
                        annualGoalProcessModel.PeriodId = PeriodId;
                        annualGoalProcessModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);

                        annualAppraisalProcessList.Add(annualGoalProcessModel);
                    }
                }
               
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return annualAppraisalProcessList;
        }
        public IEnumerable<AnnualAppraisalProcessModel> GetAnnualAppraisalProcessList(int? companyId, int? yearId, bool? isFormDCategory, List<SelectListItem> performanceGroups, int PeriodId, int userId)
        {
            IList<AnnualAppraisalProcessModel> annualAppraisalProcessList = null;
            try
            {
                annualAppraisalProcessList = new List<AnnualAppraisalProcessModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetAnnualAppraisalProcessData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@YearId", yearId);
                sqlCommand.Parameters.AddWithValue("@IsFormDCategory", isFormDCategory);
                sqlCommand.Parameters.AddWithValue("@PeriodId", PeriodId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    AnnualAppraisalProcessModel annualGoalProcessModel;
                    while (sqlDataReader.Read())
                    {
                        annualGoalProcessModel = new AnnualAppraisalProcessModel();
                        annualGoalProcessModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        annualGoalProcessModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? 0 : sqlDataReader["PerformanceGroupId"]);
                        annualGoalProcessModel.PerformanceGroupName = Convert.ToString(sqlDataReader["PerformanceGroupName"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupName"]);
                        annualGoalProcessModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        annualGoalProcessModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        annualGoalProcessModel.YearId = Convert.ToInt32(sqlDataReader["YearId"] == DBNull.Value ? "0" : sqlDataReader["YearId"]);
                        annualGoalProcessModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        //*** Naresh 2020-02-27 To display intermidiate status
                        annualGoalProcessModel.MidEvaluationStartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["MidEvaluationStartDate"] == DBNull.Value ? "" : sqlDataReader["MidEvaluationStartDate"]));
                        annualGoalProcessModel.FullEvaluationStartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["FullEvaluationStartDate"] == DBNull.Value ? "" : sqlDataReader["FullEvaluationStartDate"]));
                        annualGoalProcessModel.EvaluationStartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["EvaluationStartDate"] == DBNull.Value ? "" : sqlDataReader["EvaluationStartDate"]));
                        annualGoalProcessModel.EvaluationDueDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["EvaluationDueDate"] == DBNull.Value ? "" : sqlDataReader["EvaluationDueDate"]));
                        annualGoalProcessModel.IsGoalSubmitted = Convert.ToBoolean(sqlDataReader["IsGoalSubmitted"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSubmitted"]);
                        annualGoalProcessModel.IsMidYearAppraisalSubmitted = Convert.ToBoolean(sqlDataReader["IsMidYearAppraisalSubmitted"] == DBNull.Value ? "0" : sqlDataReader["IsMidYearAppraisalSubmitted"]);
                        annualGoalProcessModel.IsFullYearAppraisalSubmitted = Convert.ToBoolean(sqlDataReader["IsFullYearAppraisalSubmitted"] == DBNull.Value ? "0" : sqlDataReader["IsFullYearAppraisalSubmitted"]);
                        annualGoalProcessModel.PeriodId = PeriodId;
                        annualGoalProcessModel.GoalSettingId = Convert.ToInt32(sqlDataReader["GoalSettingId"] == DBNull.Value ? 0 : sqlDataReader["GoalSettingId"]);
                        //*** This line may not Need.  Need to test Jisha's Bug on Initialising
                        //annualGoalProcessModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());

                        annualAppraisalProcessList.Add(annualGoalProcessModel);
                    }
                }
                sqlDataReader.NextResult(); //*** Reading Performance Group NAmes
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        if (performanceGroups.Count == 0)
                        {
                            performanceGroups.Add(new SelectListItem() { Text = "Select", Value = "0" });
                        }
                        performanceGroups.Add(new SelectListItem() { Text = Convert.ToString(sqlDataReader["PerformanceGroupName"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupName"].ToString()), Value = Convert.ToString(sqlDataReader["PerformanceGroupID"] == DBNull.Value ? "" : sqlDataReader["PerformanceGroupID"].ToString()) });
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return annualAppraisalProcessList;
        }
       
        // *** F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
        public PDRPA1UserAndCurrentUserDetails FindA1UserAndEmployeeBasedGroup(int EmployeeID, int ManagerID, int GroupID, int ReqStatusId, int FormProcessID)
        {
            PDRPA1UserAndCurrentUserDetails PDRPA1UserAndCurrentUserDetails = new PDRPA1UserAndCurrentUserDetails();
            try
            {
                //sqlConnection = new SqlConnection(connectionString);
                //sqlConnection.Open();
                //sqlCommand = new SqlCommand("SELECT (CASE WHEN SchoolBasedGroup=1 THEN 0 ELSE 1 END) SchoolBasedGroup FROM HR_FormsWorkflowGroups WHERE GroupID IN(SELECT GroupID from HR_EmployeeBasedHierarchy WHERE EmployeeID=" + EmployeeID + " AND ManagerID =" + ManagerID + " AND GroupID =" + GroupID + ")", sqlConnection);
                //sqlCommand.CommandType = System.Data.CommandType.Text;
                //SqlDataReader reader = sqlCommand.ExecuteReader();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetRequestGroupID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ManagerID", ManagerID);
                sqlCommand.Parameters.AddWithValue("@GroupID", GroupID);
                sqlCommand.Parameters.AddWithValue("@ReqStatusId", ReqStatusId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup = Convert.ToBoolean(sqlDataReader["SchoolBasedGroup"] == DBNull.Value ? false : sqlDataReader["SchoolBasedGroup"]);
                    }
                }
                sqlDataReader.NextResult();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        PDRPA1UserAndCurrentUserDetails.A1User = Convert.ToInt32(sqlDataReader["A1User"] == DBNull.Value ? 0 : sqlDataReader["A1User"]);
                        PDRPA1UserAndCurrentUserDetails.A1UserGroupID = Convert.ToInt32(sqlDataReader["A1UserGroupID"] == DBNull.Value ? 0 : sqlDataReader["A1UserGroupID"]);
                        PDRPA1UserAndCurrentUserDetails.CurUserGroupID = Convert.ToInt32(sqlDataReader["CurUserGroupID"] == DBNull.Value ? 0 : sqlDataReader["CurUserGroupID"]);

                    }
                }
                sqlDataReader.NextResult();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        PDRPA1UserAndCurrentUserDetails.TaskCount = Convert.ToInt32(sqlDataReader["TaskCount"] == DBNull.Value ? 0 : sqlDataReader["TaskCount"]);

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return PDRPA1UserAndCurrentUserDetails;
        }
        public bool FindEmployeeBasedGroup(int EmployeeID, int ManagerID, int GroupID,int ReqStatusId,int FormProcessID)
        {
            bool EmployeeBasedGorup = false;
            try
            {
                //sqlConnection = new SqlConnection(connectionString);
                //sqlConnection.Open();
                //sqlCommand = new SqlCommand("SELECT (CASE WHEN SchoolBasedGroup=1 THEN 0 ELSE 1 END) SchoolBasedGroup FROM HR_FormsWorkflowGroups WHERE GroupID IN(SELECT GroupID from HR_EmployeeBasedHierarchy WHERE EmployeeID=" + EmployeeID + " AND ManagerID =" + ManagerID + " AND GroupID =" + GroupID + ")", sqlConnection);
                //sqlCommand.CommandType = System.Data.CommandType.Text;
                //SqlDataReader reader = sqlCommand.ExecuteReader();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetRequestGroupID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ManagerID", ManagerID);
                sqlCommand.Parameters.AddWithValue("@GroupID", GroupID);
                sqlCommand.Parameters.AddWithValue("@ReqStatusId", ReqStatusId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        EmployeeBasedGorup = Convert.ToBoolean(sqlDataReader["SchoolBasedGroup"] == DBNull.Value ? false : sqlDataReader["SchoolBasedGroup"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return EmployeeBasedGorup;
        }
        public ApprovalRequesterInfo GetRequesterEmployeeInfoDetailsForPDRPNonTeachers(GoalSettingModel GoalSettingModel, int employeeID, bool? isAddMode, int? formProcessId)
        {
            ApprovalRequesterInfo objRequesterInfoViewModel = new ApprovalRequesterInfo();

            RecruitR1BudgetedViewModel objRecruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetRequesterEmployeeInfoDetailsForPDRPNonTeachers", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                //sqlCommand.Parameters.AddWithValue("@IsAddMode", isAddMode.HasValue ? isAddMode : (object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessId.HasValue ? formProcessId : (object)DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (formProcessId == 0 || !formProcessId.HasValue)
                        {
                            objRequesterInfoViewModel.RequesterID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                            objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["EmployeeAlternativeID"]);
                            objRequesterInfoViewModel.RequestID = Convert.ToString(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                            objRequesterInfoViewModel.RequesterName = Convert.ToString(reader["EmployeeName"]);
                            objRequesterInfoViewModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                            objRequesterInfoViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            objRequesterInfoViewModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            objRequesterInfoViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                            objRequesterInfoViewModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? "0" : reader["SuperviserID"].ToString());
                            objRequesterInfoViewModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                            objRequesterInfoViewModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                            objRequesterInfoViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                            objRequesterInfoViewModel.WorkEmail = Convert.ToString(reader["WorkEmail"]);
                            objRequesterInfoViewModel.HireDate = reader["HireDate"] != DBNull.Value ? Convert.ToDateTime(reader["HireDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? "0" : reader["SuperviserID"].ToString());
                            objRequesterInfoViewModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                            //objRequesterInfoViewModel.AcademicYearID = Convert.ToInt32(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                            //objRequesterInfoViewModel.AcademicYearName = Convert.ToString(reader["AcademicYearName"] == DBNull.Value ? "" : reader["AcademicYearName"].ToString());
                        }
                        else
                        {
                            objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["RequesterAlternativeID"]);
                            objRequesterInfoViewModel.RequestID = reader["RequestID"].ToString();
                            objRequesterInfoViewModel.FormID = Convert.ToInt16(reader["FormID"]);
                            objRequesterInfoViewModel.FormInstanceID = Convert.ToInt32(reader["FormInstanceID"]);
                            objRequesterInfoViewModel.RequesterName = reader["RequesterName"].ToString();
                            objRequesterInfoViewModel.RequestDate = reader["RequestDate"] != DBNull.Value ? Convert.ToDateTime(reader["RequestDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.CompanyName = reader["Company"].ToString();
                            objRequesterInfoViewModel.DepartmentName = reader["Department"].ToString();
                            objRequesterInfoViewModel.PositionTitle = reader["Designation"].ToString();
                            objRequesterInfoViewModel.ProjectData = reader["Project"].ToString();
                            objRequesterInfoViewModel.WorkEmail = reader["RequesterEmail"].ToString();
                            objRequesterInfoViewModel.JobGrade = reader["JobGrade"].ToString();
                            objRequesterInfoViewModel.SuperviserName = reader["LineManager"].ToString();
                            objRequesterInfoViewModel.NationalityName = reader["Nationality"].ToString();
                            objRequesterInfoViewModel.BirthDate = reader["DateOfBirth"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfBirth"]) : (DateTime?)null;
                            objRequesterInfoViewModel.HireDate = reader["DateOfJoining"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfJoining"]) : (DateTime?)null;
                            objRequesterInfoViewModel.PassportNo = reader["PassportNo"].ToString();
                            objRequesterInfoViewModel.PassportIssueDate = reader["PassportIssueDate"] != DBNull.Value ? Convert.ToDateTime(reader["PassportIssueDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.PassportExpireDate = reader["PassportExpiryDate"] != DBNull.Value ? Convert.ToDateTime(reader["PassportExpiryDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.VisaNo = reader["VisaNo"].ToString();
                            objRequesterInfoViewModel.VisaExpireDate = reader["VisaExpiryDate"] != DBNull.Value ? Convert.ToDateTime(reader["VisaExpiryDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.AcademicYearID = Convert.ToInt32(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                            objRequesterInfoViewModel.AcademicYearName = Convert.ToString(reader["AcademicYearName"] == DBNull.Value ? "" : reader["AcademicYearName"].ToString());

                        }
                    }
                }
                GoalSettingModel.ApprovalRequesterInfo = objRequesterInfoViewModel;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objRequesterInfoViewModel;
        }
        public string GetYear(string Qstring)
        {
            string CompanyName = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(Qstring, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        CompanyName = Convert.ToString(reader["Name"] == DBNull.Value ? "" : reader["Name"].ToString());
                    }
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return CompanyName;
        }
        public string GetKPICount(int CompanyID, int YearID)
        {
            string Cnt = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT COUNT(*) Cnt FROM HR_PDRP_YearlyKPIs WHERE  KPIYear= " + YearID + " AND CompanyID=" + CompanyID.ToString(), sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        Cnt = Convert.ToString(reader["Cnt"] == DBNull.Value ? "" : reader["Cnt"].ToString());
                    }
                }

                //if (IsLM > 0)
                //{
                //    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null));
                //    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, null, EmployeeID));
                //}
                //else
                //{
                //    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null));
                //}
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return Cnt;
        }
        public string GetCompanyName(int CompanyID)
        {
            string CompanyName = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT Name FROM HR_Company WHERE CompanyId=" + CompanyID.ToString(), sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        CompanyName = Convert.ToString(reader["Name"] == DBNull.Value ? "" : reader["Name"].ToString());
                    }
                }

                //if (IsLM > 0)
                //{
                //    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null));
                //    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, null, EmployeeID));
                //}
                //else
                //{
                //    objDropInsTeachersList.AddRange(GetAllLMEmployeeList(DropInsYear, EmployeeID, null));
                //}
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return CompanyName;
        }
        public List<PDRPTeachersModel> GetAllLMEmployeeList(int DropInsYear, int? EmployeeID, int? SuperviserID, int? CompanyID, string dropInMode,int userId)
        {
            List<PDRPTeachersModel> employeeList = null;
            try
            {
                employeeList = new List<PDRPTeachersModel>();
                //*** Naresh 2020-03-17 View My DropIns/Admin DropIns
                var sqlParameters = new List<SqlParameter>();
                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@DropInsYear";
                sqlParameter.Value = DropInsYear;
                sqlParameters.Add(sqlParameter);

                if (EmployeeID.HasValue && EmployeeID > 0)
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = "@EmployeeID";
                    sqlParameter.Value = EmployeeID;
                    sqlParameters.Add(sqlParameter);
                }
                if (SuperviserID.HasValue)
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = "@SuperviserID";
                    sqlParameter.Value = SuperviserID;
                    sqlParameters.Add(sqlParameter);
                }

                if (CompanyID.HasValue)
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = "@CompanyID";
                    sqlParameter.Value = CompanyID;
                    sqlParameters.Add(sqlParameter);
                }
                if (!String.IsNullOrEmpty(dropInMode))
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = "@Mode";
                    sqlParameter.Value = dropInMode;
                    sqlParameters.Add(sqlParameter);
                }
                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@UserId";
                sqlParameter.Value = userId;
                sqlParameters.Add(sqlParameter);

                SqlDataReader sqlDataReader = ExecuteProcedure("Hr_Stp_GetEmployeeInfoforPDRP", sqlParameters.ToArray());

                if (sqlDataReader.HasRows)
                {
                    PDRPTeachersModel DropInsTeachersModel;
                    while (sqlDataReader.Read())
                    {
                        DropInsTeachersModel = new PDRPTeachersModel();
                        DropInsTeachersModel.employeeContactModel = new EmployeeContactModel();
                        DropInsTeachersModel.employmentInformation = new EmploymentInformation();
                        DropInsTeachersModel.employeeDetailsModel.BasicDetails = new EmployeeBasicDetails();

                        DropInsTeachersModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? 0 : sqlDataReader["EmployeeId"]);
                        DropInsTeachersModel.employeeDetailsModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? 0 : sqlDataReader["EmployeeAlternativeID"]);
                        DropInsTeachersModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        DropInsTeachersModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        DropInsTeachersModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? 0 : sqlDataReader["CompanyId"]);

                        DropInsTeachersModel.employmentInformation.PositionID = Convert.ToInt32(sqlDataReader["PositionID"] == DBNull.Value ? 0 : sqlDataReader["PositionID"]);
                        DropInsTeachersModel.employmentInformation.PositionName = Convert.ToString(sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"]).Trim();

                        DropInsTeachersModel.employeeDetailsModel.isActive = Convert.ToBoolean(sqlDataReader["isActive"]);

                        DropInsTeachersModel.employeeDetailsModel.BasicDetails.Department = Convert.ToString(sqlDataReader["DepartmentName_1"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_1"]);
                        DropInsTeachersModel.employeeDetailsModel.usertypeid = sqlDataReader["usertypeid"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["usertypeid"]);
                        DropInsTeachersModel.DropInsCount = Convert.ToInt32(sqlDataReader["Count"] == DBNull.Value ? 0 : sqlDataReader["Count"]);
                        DropInsTeachersModel.KPICount = Convert.ToInt32(sqlDataReader["KPICount"] == DBNull.Value ? 0 : sqlDataReader["KPICount"]);
                        DropInsTeachersModel.VisitDate = Convert.ToString(sqlDataReader["VisitDate"] == DBNull.Value ? "" : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["VisitDate"])));
                        DropInsTeachersModel.VisitTime = Convert.ToString(sqlDataReader["VisitDate"] == DBNull.Value ? "" : Convert.ToDateTime(sqlDataReader["VisitDate"]).ToString("hh:mm tt"));
                        DropInsTeachersModel.PreObservationDate = Convert.ToString(sqlDataReader["PreObservationDueDate"] == DBNull.Value ? "" : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["PreObservationDueDate"])));
                        DropInsTeachersModel.employmentInformation.SuperviserID = Convert.ToInt32(sqlDataReader["SuperviserID"] == DBNull.Value ? 0 : sqlDataReader["SuperviserID"]);

                        employeeList.Add(DropInsTeachersModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }
        public List<PDRPTeachersModel> GetAllApprovalEmployeeList(int ApprovalYear, int? EmployeeID, int? SuperviserID, int CompanyID)
        {
            List<PDRPTeachersModel> employeeList = null;
            try
            {
                employeeList = new List<PDRPTeachersModel>();

                SqlParameter[] sqlParameters = new SqlParameter[3];
                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@ApprovalYear";
                sqlParameter.Value = ApprovalYear;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@CompanyID";
                sqlParameter.Value = CompanyID;
                sqlParameters[1] = sqlParameter;

                sqlParameter = new SqlParameter();
                if (EmployeeID.HasValue)
                {
                    sqlParameter.ParameterName = "@EmployeeID";
                    sqlParameter.Value = EmployeeID;
                }
                else
                {
                    sqlParameter.ParameterName = "@SuperviserID";
                    sqlParameter.Value = SuperviserID;
                }
                sqlParameters[2] = sqlParameter;




                SqlDataReader sqlDataReader = ExecuteProcedure("Hr_Stp_GetApprovalEmployeeInfoforPDRP", sqlParameters);

                if (sqlDataReader.HasRows)
                {
                    PDRPTeachersModel ApprovalTeachersModel;
                    while (sqlDataReader.Read())
                    {
                        ApprovalTeachersModel = new PDRPTeachersModel();
                        ApprovalTeachersModel.employeeContactModel = new EmployeeContactModel();
                        ApprovalTeachersModel.employmentInformation = new EmploymentInformation();
                        ApprovalTeachersModel.employeeDetailsModel.BasicDetails = new EmployeeBasicDetails();

                        ApprovalTeachersModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? 0 : sqlDataReader["EmployeeId"]);
                        ApprovalTeachersModel.employeeDetailsModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? 0 : sqlDataReader["EmployeeAlternativeID"]);
                        ApprovalTeachersModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        ApprovalTeachersModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        ApprovalTeachersModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? 0 : sqlDataReader["CompanyId"]);

                        ApprovalTeachersModel.employmentInformation.PositionID = Convert.ToInt32(sqlDataReader["PositionID"] == DBNull.Value ? 0 : sqlDataReader["PositionID"]);
                        ApprovalTeachersModel.employmentInformation.PositionName = Convert.ToString(sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"]).Trim();

                        ApprovalTeachersModel.employeeDetailsModel.isActive = Convert.ToBoolean(sqlDataReader["isActive"]);

                        ApprovalTeachersModel.employeeDetailsModel.BasicDetails.Department = Convert.ToString(sqlDataReader["DepartmentName_1"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_1"]);
                        ApprovalTeachersModel.employeeDetailsModel.usertypeid = sqlDataReader["usertypeid"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["usertypeid"]);
                        ApprovalTeachersModel.DropInsCount = Convert.ToInt32(sqlDataReader["Count"] == DBNull.Value ? "0" : sqlDataReader["Count"]);
                        ApprovalTeachersModel.VisitDate = Convert.ToString(sqlDataReader["EvaluationDate"] == DBNull.Value ? "" : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["EvaluationDate"])));
                        ApprovalTeachersModel.VisitTime = Convert.ToString(sqlDataReader["EvaluationDate"] == DBNull.Value ? "" : Convert.ToDateTime(sqlDataReader["EvaluationDate"]).ToString("hh:mm tt"));
                        ApprovalTeachersModel.PreObservationDate = Convert.ToString(sqlDataReader["EvaluationDueDate"] == DBNull.Value ? "" : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["EvaluationDueDate"])));
                        ApprovalTeachersModel.KPICount = Convert.ToInt32(sqlDataReader["KPICount"] == DBNull.Value ? "0" : sqlDataReader["KPICount"]);
                        ApprovalTeachersModel.Observation = Convert.ToInt32(sqlDataReader["Observation"] == DBNull.Value ? "0" : sqlDataReader["Observation"]);


                        employeeList.Add(ApprovalTeachersModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }
        public SqlDataReader ExecuteProcedure(string ProcedureName, SqlParameter[] sqlParameters)
        {
            SqlDataReader reader;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(ProcedureName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                foreach (SqlParameter Param in sqlParameters)
                {
                    sqlCommand.Parameters.Add(Param);
                }


                reader = sqlCommand.ExecuteReader();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                //if (sqlReader != null)
                //{
                //    sqlReader.Close();
                //}
                //if (sqlConnection != null)
                //{
                //    sqlConnection.Close();
                //}
            }

            return reader;
        }

        public SqlDataReader ExecuteProcedure(string ProcedureName)
        {
            SqlDataReader reader;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(ProcedureName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //foreach (SqlParameter Param in sqlParameters)
                //{
                //    sqlCommand.Parameters.Add(Param);
                //}


                reader = sqlCommand.ExecuteReader();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                //if (sqlReader != null)
                //{
                //    sqlReader.Close();
                //}
                //if (sqlConnection != null)
                //{
                //    sqlConnection.Close();
                //}
            }

            return reader;
        }
        public decimal GetFormulaValue(string Formla, int Rating)
        {
            decimal result = 0;
            try
            {
                string FormatedFormla = Formla.Replace("Rating", Rating.ToString());
                DataTable dt = new DataTable();

                //double answer = (double)dt.Compute(FormatedFormla.ToString(), "");

                //FinalResult = FinalResult+GetPDRPFormulaValue(FormatedFormla);

                string S = dt.Compute(FormatedFormla.ToString(), "").ToString();
                result = Math.Round(Convert.ToDecimal(dt.Compute(FormatedFormla.ToString(), "")), 2);
            }
            catch (Exception ex)
            {

            }
            return result;

        }
        public decimal GetFormulaValue(string Formla, int Rating, int Weightage)
        {
            decimal result = 0;
            try
            {
                if (Rating > 0)
                {
                    string FormatedFormla = Formla.Replace("Rating", Rating.ToString()).Replace("Weightage", Weightage.ToString());
                    DataTable dt = new DataTable();

                    string S = dt.Compute(FormatedFormla.ToString(), "").ToString();
                    result = Convert.ToDecimal(dt.Compute(FormatedFormla.ToString(), ""));

                    //result = Convert.ToInt32(Math.Round((double)dt.Compute(FormatedFormla.ToString(), ""), 0));
                }
                else
                {
                    result = 0;
                }
            }
            catch
            {

            }
            return result;

        }
        public string GetTeachingScoreSlab(decimal ThisYearScore)
        {
            string ScoreSlab = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT DefinitionName FROM HR_PDRPDropInsRatingScaleWeightageMaster WHERE (" + ThisYearScore.ToString() + ">=FinalScoreSlabMIN AND " + ThisYearScore.ToString() + "<= FinalScoreSlabMAX)", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        ScoreSlab = Convert.ToString(reader["DefinitionName"] == DBNull.Value ? "" : reader["DefinitionName"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ScoreSlab;
        }
        public PDRPFormulasTotalScore GetPDRPFormulaCalculations(IEnumerable<int> Scorevalues, string FormulaType, int FormulaYearID)
        {
            //*** In Formula "Rating", "Count" are predifined test and values will be supplied from form. All others are fixed values from DB. Any operators can be used
            Decimal FinalResult = 0;
            string Formla = GetPDRPFormula(FormulaType, FormulaYearID).Replace("Count", Scorevalues == null ? "0" : Scorevalues.Count().ToString());

            //string expression = Formla;    //"cost * item / 100" (IT MUST BE SEPARATED WITH SPACES!)


            //DynamicFormula math = new DynamicFormula();
            //math.Expression = expression;   //Let's define the expression
            if (Formla == "")
            {

            }
            else
            {
                if (Scorevalues != null)
                {
                    foreach (int Rating in Scorevalues)
                    {
                        FinalResult = FinalResult + GetFormulaValue(Formla, Rating);
                        //string FormatedFormla = Formla.Replace("Rating", Rating.ToString());
                        //DataTable dt = new DataTable();

                        ////double answer = (double)dt.Compute(FormatedFormla.ToString(), "");

                        ////FinalResult = FinalResult+GetPDRPFormulaValue(FormatedFormla);
                        //FinalResult = FinalResult + Convert.ToInt32(Math.Round((double)dt.Compute(FormatedFormla.ToString(), ""), 0));

                    }
                }
            }
            PDRPFormulasTotalScore PDRPFormulasTotalScore = new PDRPFormulasTotalScore();
            PDRPFormulasTotalScore.TotalScore = Math.Round(FinalResult, 2);
            PDRPFormulasTotalScore.Total = GetPDRPFormulaSlab(FinalResult);
            PDRPFormulasTotalScore.Formula = Formla;
            return PDRPFormulasTotalScore;
        }
        public PDRPFormulasTotalScore GetPDRPAnualFormulaCalculations(float Scorevalues1, float Scorevalues2, string FormulaType, int FormulaYearID)
        {
            //*** In Formula "Rating", "Count" are predifined test and values will be supplied from form. All others are fixed values from DB. Any operators can be used
            decimal FinalResult = 0;
            string Formla = GetPDRPFormula(FormulaType, FormulaYearID);

            //string expression = Formla;    //"cost * item / 100" (IT MUST BE SEPARATED WITH SPACES!)


            //DynamicFormula math = new DynamicFormula();
            //math.Expression = expression;   //Let's define the expression
            int j = 0;
            if (Formla == "")
            {

            }
            else
            {
                //if (Scorevalues != null)
                //{
                //    foreach (int Rating in Scorevalues)
                //    {

                //        if (j == 0)
                //        {
                Formla = Formla.Replace("BusTar", Scorevalues1.ToString());
                //}
                //else
                //{
                Formla = Formla.Replace("Comp", Scorevalues2.ToString());
                //    }

                //    j++;


                //}

                FinalResult = FinalResult + GetFormulaValue(Formla, 0);

                //}
            }
            PDRPFormulasTotalScore PDRPFormulasTotalScore = new PDRPFormulasTotalScore();
            PDRPFormulasTotalScore.TotalScore = FinalResult;
            PDRPFormulasTotalScore.Total = GetPDRPFormulaSlab(FinalResult);
            PDRPFormulasTotalScore.Formula = Formla;
            return PDRPFormulasTotalScore;
        }
        public PDRPFormulasTotalScore GetPDRPFormulaCalculations(IList<int> Scorevalues, IList<int> Weightages, string FormulaType, int FormulaYearID)
        {
            //*** In Formula "Rating", "Count" are predifined test and values will be supplied from form. All others are fixed values from DB. Any operators can be used
            decimal FinalResult = 0;
            string Formla = GetPDRPFormula(FormulaType, FormulaYearID);

            //string expression = Formla;    //"cost * item / 100" (IT MUST BE SEPARATED WITH SPACES!)


            //DynamicFormula math = new DynamicFormula();
            //math.Expression = expression;   //Let's define the expression
            PDRPFormulasTotalScore PDRPFormulasTotalScore = new PDRPFormulasTotalScore();
            PDRPFormulasTotalScore.ItemTotal = new List<decimal>();
            if (Formla == "")
            {

            }
            else
            {
                if (Scorevalues != null)
                {
                    for (int i = 0; i < Scorevalues.Count(); i++)
                    {
                        decimal ItemTotal = GetFormulaValue(Formla, Scorevalues[i], Weightages[i]);
                        PDRPFormulasTotalScore.ItemTotal.Add(ItemTotal);
                        FinalResult = FinalResult + ItemTotal;
                        //string FormatedFormla = Formla.Replace("Rating", Rating.ToString());
                        //DataTable dt = new DataTable();

                        ////double answer = (double)dt.Compute(FormatedFormla.ToString(), "");

                        ////FinalResult = FinalResult+GetPDRPFormulaValue(FormatedFormla);
                        //FinalResult = FinalResult + Convert.ToInt32(Math.Round((double)dt.Compute(FormatedFormla.ToString(), ""), 0));

                    }
                }
            }
            PDRPFormulasTotalScore.TotalScore = FinalResult;
            //PDRPFormulasTotalScore.Total = GetPDRPFormulaSlab(FinalResult);
            PDRPFormulasTotalScore.Formula = Formla;
            return PDRPFormulasTotalScore;
        }
        public string GetPDRPFormula(string FormulaType, int FormulaYearID)
        {
            //string PDRPFormula = "";
            PDRPFormulas PDRPFormulas = new PDRPFormulas();
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@FormulaType";
                sqlParameter.Value = FormulaType;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@FormulaYearID";
                sqlParameter.Value = FormulaYearID;
                sqlParameters[1] = sqlParameter;

                SqlDataReader reader = ExecuteProcedure("HR_stp_GetPDRPFormula", sqlParameters);
                //sqlConnection = new SqlConnection(connectionString);
                //sqlConnection.Open();
                //sqlCommand = new SqlCommand("HR_stp_GetPDRPFormula", sqlConnection);
                //sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //sqlCommand.Parameters.AddWithValue("@FormulaType", FormulaType);

                //SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        PDRPFormulas.FormulasId = Convert.ToInt32(reader["FormulasId"]);
                        PDRPFormulas.Formulas = Convert.ToString(reader["Formulas"]);
                        //PDRPFormulas.IsActive = Convert.ToInt32(reader["IsActive"] == DBNull.Value ? 0 : reader["IsActive"]);



                    }
                }
                else
                {

                    PDRPFormulas.Formulas = "";
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return PDRPFormulas.Formulas;
        }
        Decimal GetPDRPFormulaSlab(Decimal FinalResult)
        {
            Decimal PDRPFormulaValue = 0;
            //PDRPFormulas PDRPFormulas = new PDRPFormulas();
            try
            {
                if (FinalResult > 0)
                {
                    SqlParameter[] sqlParameters = new SqlParameter[1];

                    SqlParameter sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = "@FinalResult";
                    sqlParameter.Value = FinalResult;
                    sqlParameters[0] = sqlParameter;

                    SqlDataReader reader = ExecuteProcedure("HR_stp_PDRPCalculationSlab", sqlParameters);

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            PDRPFormulaValue = Convert.ToInt32(reader["Value"]);

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return PDRPFormulaValue;
        }
        ////*** F7
        //public List<PDRPLanguage> GetLanguageList(int? LanguageID)
        //{
        //    int PDRPFormulaValue = 0;
        //    List<PDRPLanguage> PDRPLanguageList = new List<PDRPLanguage>();
        //    PDRPLanguage PDRPLanguage;

        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[1];

        //        SqlParameter sqlParameter = new SqlParameter();
        //        sqlParameter.ParameterName = "@LanguageID";
        //        sqlParameter.Value = LanguageID;

        //        SqlDataReader reader;
        //        if (LanguageID != null)
        //        {
        //            sqlParameters[0] = sqlParameter;
        //            reader = ExecuteProcedure("HR_stp_PDRPGetLanguageList", sqlParameters);
        //        }
        //        else
        //        {
        //            reader = ExecuteProcedure("HR_stp_PDRPGetLanguageList");
        //        }   
        //        if (reader.HasRows)
        //        {
        //            while (reader.Read())
        //            {
        //                PDRPLanguage = new PDRPLanguage();

        //                PDRPLanguage.LanguageID = Convert.ToInt32(reader["LanguageID"] == DBNull.Value ? 0 : reader["LanguageID"]);
        //                PDRPLanguage.LanguageName = Convert.ToString(reader["LanguageName"] == DBNull.Value ? "" : reader["LanguageName"]);
        //                PDRPLanguage.LanguageFaFlag = Convert.ToString(reader["LanguageFaFlag"] == DBNull.Value ? "" : reader["LanguageFaFlag"]);
        //                PDRPLanguage.LanguageName2 = Convert.ToString(reader["LanguageName2"] == DBNull.Value ? "" : reader["LanguageName2"]);
        //                PDRPLanguage.LanguageName3 = Convert.ToString(reader["LanguageName3"] == DBNull.Value ? "" : reader["LanguageName3"]);
        //                PDRPLanguage.LanguageShortName = Convert.ToString(reader["LanguageShortName"] == DBNull.Value ? "" : reader["LanguageShortName"]);

        //                PDRPLanguageList.Add(PDRPLanguage);
        //            }
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }
        //    }
        //    return PDRPLanguageList;
        //}


    }

}
