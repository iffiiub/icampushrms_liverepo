﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.Forms;
using System.Data;
using System.Web.Mvc;
using HRMS.DataAccess.PDRP;

namespace HRMS.DataAccess.FormsDB
{
    public class KPIsModuleDB : PDRPCommonModelDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;



        /// <summary>
        /// Add KPI Details
        /// </summary>
        /// <param name="KPIModel"></param>
        /// <returns></returns>
        public string AddKPIs(int CompanyID, int[] KPIOrder, string[] KPIAreaEng, string[] KPIDescriptionEng, string[] KPIAreaArab, string[] KPIDescriptionArab, string[] txtKPIWeightage, string ddlKPIYear, string CreatedBy)
        {
            string Message = "";
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                //SqlDataReader sqlDataReader;
                SqlTransaction transaction;
                // Start a local transaction.
                transaction = sqlConnection.BeginTransaction("SampleTransaction");

                sqlCommand = new SqlCommand("HR_TB_PDRPInsertYearlyKPIs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Transaction = transaction;

                KPIsListModel KPIsListModel = new KPIsListModel();
                //List<KPIsModel> KPIsList = new List<KPIsModel>();

                for (int i = 0; i < KPIAreaEng.Length; i++)
                {
                    sqlCommand.Parameters.Clear();
                    //if (KPIAreaEng[i].Trim().Length > 0)
                    //{
                        sqlCommand.Parameters.AddWithValue("@KPIYear", ddlKPIYear);
                        sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                        sqlCommand.Parameters.AddWithValue("@KPIAreaEng", KPIAreaEng[i].Trim());
                        sqlCommand.Parameters.AddWithValue("@KPIDescriptionEng", KPIDescriptionEng[i].Trim());
                        sqlCommand.Parameters.AddWithValue("@KPIAreaArab", KPIAreaArab[i].Trim());
                        sqlCommand.Parameters.AddWithValue("@KPIDescriptionArab", KPIDescriptionArab[i].Trim());
                        sqlCommand.Parameters.AddWithValue("@KPIWeightage", txtKPIWeightage[i]);
                        sqlCommand.Parameters.AddWithValue("@KPIOrder", KPIOrder[i]);
                        sqlCommand.Parameters.AddWithValue("@KPIActive", 1);
                        sqlCommand.Parameters.AddWithValue("@KPICreatedBy", CreatedBy);
                        sqlCommand.Parameters.AddWithValue("@KPICount", KPIAreaEng.Length);

                        SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                        OperationMessage.Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.Add(OperationMessage);
                        sqlCommand.ExecuteNonQuery();
                        Message = OperationMessage.Value.ToString();
                        if (Message != "SUCCESS")
                        {
                            transaction.Rollback();
                            sqlConnection.Close();

                            return Message;
                        }
                    //}
                }

                transaction.Commit();


                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Select KPI Details
        /// </summary>
        /// <param name="KPIModel"></param>
        /// <returns></returns>
        public KPIsListModel GetKPIsById(string Year, bool boolBlank, int CompanyID)
        {
            KPIsListModel KPIsListModel = new KPIsListModel();
            KPIsModel KIPsModel = new KPIsModel();
            List<KPIsModel> KPIList = new List<KPIsModel>();

            SelectListItem KPIYears = new SelectListItem();
            List<SelectListItem> KPIYearsList = new List<SelectListItem>();
            KPIsListModel.PDRPFormulasList = new List<PDRPFormula>();
            PDRPFormula PDRPFormulas;

            try
            {
                //connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_PDRPSelectCurrentYearKPIs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Year", Year);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);

                //SqlDataAdapter Ad = new SqlDataAdapter(sqlCommand);
                //DataSet ds = new DataSet();
                //Ad.Fill(ds);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                //*** Danny A000101.100 01/10/2019
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        KIPsModel = new KPIsModel();

                        KIPsModel.KPIId = Convert.ToInt32(sqlDataReader["KPIId"].ToString());
                        KIPsModel.KPIYear = Convert.ToString(sqlDataReader["KPIYear"]);
                        KIPsModel.CompanyID = CompanyID;
                        KIPsModel.KPIAreaEng = Convert.ToString(sqlDataReader["KPIAreaEng"]);
                        KIPsModel.KPIDescriptionEng = Convert.ToString(sqlDataReader["KPIDescriptionEng"]);
                        KIPsModel.KPIAreaArab = Convert.ToString(sqlDataReader["KPIAreaArab"]);
                        KIPsModel.KPIDescriptionArab = Convert.ToString(sqlDataReader["KPIDescriptionArab"]);
                        KIPsModel.KPIWeightage = Convert.ToInt32(sqlDataReader["KPIWeightage"] == DBNull.Value ? 0 : sqlDataReader["KPIWeightage"]);
                        KPIList.Add(KIPsModel);
                    }
                    if (KPIList.Count < 10)
                    {
                        for (int i = KPIList.Count; i < 10; i++)
                        {
                            KIPsModel = new KPIsModel();

                            KIPsModel.KPIId = 0;
                            KIPsModel.KPIYear = Year;
                            KIPsModel.CompanyID = CompanyID;
                            KIPsModel.KPIAreaEng = "";
                            KIPsModel.KPIDescriptionEng = "";
                            KIPsModel.KPIAreaArab = "";
                            KIPsModel.KPIDescriptionArab = "";
                            KIPsModel.KPIWeightage = 0;
                            KPIList.Add(KIPsModel);
                        }
                    }
                }
                else
                {
                    if (boolBlank == true) //*** To return a blank Value
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            KIPsModel = new KPIsModel();
                            KIPsModel.KPIYear = Year;
                            KPIList.Add(KIPsModel);
                        }
                    }
                }
                sqlDataReader.NextResult(); //*** Get Formula Counts
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        PDRPFormulas = new PDRPFormula();

                        PDRPFormulas.FormulasType= sqlDataReader["FormulasType"].ToString();

                        KPIsListModel.PDRPFormulasList.Add(PDRPFormulas);
                    }
                }
                sqlDataReader.NextResult();//*** Get RatingScale Count
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        PDRPFormulas = new PDRPFormula();

                        PDRPFormulas.FormulasType = sqlDataReader["RatingScale"].ToString();

                        KPIsListModel.PDRPFormulasList.Add(PDRPFormulas);
                    }
                }
                sqlDataReader.NextResult();//*** Get  Weighting Percentage Count
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        PDRPFormulas = new PDRPFormula();

                        PDRPFormulas.FormulasType = sqlDataReader["WeightingPercentage"].ToString();

                        KPIsListModel.PDRPFormulasList.Add(PDRPFormulas);
                    }
                }
                sqlDataReader.NextResult(); //*** Get  Annual Appraisal Started
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        //PDRPFormulas = new PDRPFormula();

                        KPIsListModel.IsAnnualAppraisalStarted = Convert.ToBoolean(sqlDataReader["COUNT"] == DBNull.Value ? false : sqlDataReader["COUNT"]);

                       
                    }
                }
                
                KPIsListModel.KPIYear = Year;
                KPIsListModel.KPIsList = KPIList;
                KPIsListModel.CompanyID = CompanyID;
                //KPIsListModel.KPIYears = KPIYearsList;

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return KPIsListModel;
        }
    }
}
