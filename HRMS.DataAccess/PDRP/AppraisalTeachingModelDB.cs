﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Common;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRMS.DataAccess.PDRP
{
    public class AppraisalTeachingModelDB : PDRPCommonModelDB
    {
        public AppraisalKPIsListModel GetKPIsById(string Year, int AppraisalTeachingID, bool boolBlank, AppraisalKPIsListModel KPIsListModel, int CompanyID)
        {
            //AppraisalKPIsListModel KPIsListModel = new AppraisalKPIsListModel();
            AppraisalKPIsModel KIPsModel = new AppraisalKPIsModel();
            List<AppraisalKPIsModel> KPIList = new List<AppraisalKPIsModel>();
            //List<AppraisalWeightagePercentageModel> AppraisalWeightagePercentageModelList = new List<AppraisalWeightagePercentageModel>();
            //AppraisalWeightagePercentageModel AppraisalWeightagePercentageModel;
            //KPIsListModel.AppraisalWeightagePercentageModel = new List<AppraisalWeightagePercentageModel>();

            SelectListItem KPIYears = new SelectListItem();
            List<SelectListItem> KPIYearsList = new List<SelectListItem>();
            try
            {
                SqlParameter[] parameters =
                   {
                         new SqlParameter("@Year",Year),
                         new SqlParameter("@CompanyID", CompanyID),
                         new SqlParameter("@AppraisalTeachingID",AppraisalTeachingID)
                        
                    };

                SqlDataReader sqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_STP_SelectPDRPAppraisalKPIs", parameters);


                //*** Danny A000101.100 01/10/2019
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        KIPsModel = new AppraisalKPIsModel();

                        KIPsModel.KPIId = Convert.ToInt32(sqlDataReader["KPIId"].ToString());
                        KIPsModel.KPIYear = Convert.ToString(sqlDataReader["KPIYear"]);
                        KIPsModel.CompanyID = CompanyID;
                        KIPsModel.KPIAreaEng = Convert.ToString(sqlDataReader["KPIAreaEng"]);
                        KIPsModel.KPIDescriptionEng = Convert.ToString(sqlDataReader["KPIDescriptionEng"]);
                        KIPsModel.KPIAreaArab = Convert.ToString(sqlDataReader["KPIAreaArab"]);
                        KIPsModel.KPIDescriptionArab = Convert.ToString(sqlDataReader["KPIDescriptionArab"]);
                        KIPsModel.KPIWeightage = Convert.ToInt32(sqlDataReader["KPIWeightage"] == DBNull.Value ? 0 : sqlDataReader["KPIWeightage"]);  
                        KIPsModel.Accomplishments = Convert.ToString(sqlDataReader["Accomplishments"]);
                        KIPsModel.Score = Convert.ToDecimal(sqlDataReader["Score"] == DBNull.Value ? 0 : sqlDataReader["Score"]);
                        KIPsModel.Ratings = Convert.ToInt32(sqlDataReader["Ratings"] == DBNull.Value ? 0 : sqlDataReader["Ratings"]);
                        KIPsModel.FormatedFormla = Convert.ToString(sqlDataReader["FormatedFormla"]);

                        KPIList.Add(KIPsModel);
                    }
                    
                }
                

                KPIsListModel.KPIYear = Year;
                KPIsListModel.AppraisalKPIsModel = KPIList;
                //KPIsListModel.KPIYears = KPIYearsList;

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return KPIsListModel;
        }
        public AppraisalTeachingModel GetAppraisalTeachingEmployeeInformation(AppraisalTeachingModel objAppraisalTeachingModel, string culture)
        {

            //AppraisalTeachingEmployeeModel objRequesterInfoViewModel = new AppraisalTeachingEmployeeModel();




            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@EmployeeID";
                sqlParameter.Value = objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@DropInsYear";
                sqlParameter.Value = objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYearID;
                sqlParameters[1] = sqlParameter;
                SqlDataReader reader = ExecuteProcedure("Hr_Stp_GetEmployeeInfoforPDRP", sqlParameters);
                SetAppraisalTeachingInformation(objAppraisalTeachingModel, reader,culture);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return objAppraisalTeachingModel;
        }

        public static void SetAppraisalTeachingInformation(AppraisalTeachingModel objAppraisalTeachingModel, SqlDataReader reader,string culture)
        {
            AppraisalTeachingRatingScaleWeightageDetails objAppraisalTeachingRatingScaleWeightageDetails;
            AppraisalWeightagePercentageModel AppraisalWeightagePercentageModel;

            //PDRPDropInProficiencyLevel objPDRPDropInProficiencyLevel;
            AppraisalTeachingRatingScaleWeightages AppraisalTeachingRatingScaleWeightages;

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //objRequesterInfoViewModel.RequestID = reader["RequestID"].ToString();
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? 0 : reader["EmployeeID"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);
                    //objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["EmployeeAlternativeID"]);
                    
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? 0 : reader["PositionID"]);
                    
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? 0 : reader["DepartmentID"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? 0 : reader["SuperviserID"]);
                    
                    //*** F4
                    objAppraisalTeachingModel.EmpCurrentCompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                    //objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYear = Convert.ToString(reader["DropinYear"]);
                    //Nithin 11-05-2020
                    //If Appraisal is saved already, EmployeeName, PositionTitle,DepartmentName,SuperviserName,CompanyName will be fetched from Appraisal table, So no need to bind from Master tables
                    if (string.IsNullOrEmpty(objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherName))
                    {
                        if (culture == "ar")
                        {
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["EmployeeNameAr"])) ? Convert.ToString(reader["EmployeeNameAr"]) : Convert.ToString(reader["EmployeeName"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.PositionTitle = !string.IsNullOrWhiteSpace(Convert.ToString(reader["PositionTitleAr"])) ? Convert.ToString(reader["PositionTitleAr"]) : Convert.ToString(reader["PositionTitle"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.DepartmentName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["DepartmentNameAr"])) ? Convert.ToString(reader["DepartmentNameAr"]) : Convert.ToString(reader["DepartmentName"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.SuperviserName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["SuperviserNameAr"])) ? Convert.ToString(reader["SuperviserNameAr"]) : Convert.ToString(reader["SuperviserName"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyName = !string.IsNullOrWhiteSpace(Convert.ToString(reader["CompanyNameAr"])) ? Convert.ToString(reader["CompanyNameAr"]) : Convert.ToString(reader["CompanyName"]);
                        }
                        else
                        {
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                            objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        }
                    }

                }
            }

            reader.NextResult();

            if (objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails == null || objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails.Count == 0)
            {
                if (reader.HasRows)
                {
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails = new List<AppraisalTeachingRatingScaleWeightageDetails>();
                    while (reader.Read())
                    {
                        objAppraisalTeachingRatingScaleWeightageDetails = new AppraisalTeachingRatingScaleWeightageDetails();

                        objAppraisalTeachingRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                        objAppraisalTeachingRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);

                        objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails.Add(objAppraisalTeachingRatingScaleWeightageDetails);
                    }
                }
            }
            reader.NextResult();
            reader.NextResult();
            reader.NextResult();
            if (objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel == null || objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel.Count == 0)
            {
                objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel = new List<AppraisalWeightagePercentageModel>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        AppraisalWeightagePercentageModel = new AppraisalWeightagePercentageModel();
                        AppraisalWeightagePercentageModel.Section = reader["Section"].ToString();
                        AppraisalWeightagePercentageModel.SectionDescriptiopn = reader["SectionDescriptiopn"].ToString();
                        AppraisalWeightagePercentageModel.Section2 = reader["Section2"].ToString();
                        AppraisalWeightagePercentageModel.SectionDescriptiopn2 = reader["SectionDescriptiopn2"].ToString();
                        AppraisalWeightagePercentageModel.Weightag = reader["Weightag"].ToString();
                        objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel.Add(AppraisalWeightagePercentageModel);
                    }
                }
            }

            for (int i = 0; i <= objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails.Count; i++)
            {

                AppraisalTeachingRatingScaleWeightages = new AppraisalTeachingRatingScaleWeightages();
                if (i == 0)
                {
                    AppraisalTeachingRatingScaleWeightages.RatingScaleID = 0;

                    AppraisalTeachingRatingScaleWeightages.RatingScaleNumber = 0;



                    objAppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages.Add(AppraisalTeachingRatingScaleWeightages);
                }
                else
                {
                    AppraisalTeachingRatingScaleWeightages.RatingScaleID = Convert.ToInt32(objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails[i - 1].RatingScaleID);
                    AppraisalTeachingRatingScaleWeightages.RatingScaleNumber = Convert.ToInt32(objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails[i - 1].RatingScaleNumber);

                    objAppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages.Add(AppraisalTeachingRatingScaleWeightages);
                }
            }
            reader.NextResult();
        }

        public OperationDetails CheckAnnualWorkFlowAllEmployee(List<PDRPTeachersModel> SelectedTeachersList,  DataTable dtResult)
        {
            string Result = "";
            OperationDetails oprationDetails = new OperationDetails();
            DataTable dtAppraisalTeachinTable = new DataTable();
            dtAppraisalTeachinTable.Columns.Add("EmployeeID");           

            foreach (var item in SelectedTeachersList)
            {
                dtAppraisalTeachinTable.Rows.Add(item.EmployeeId);
                
            }

            try
            {

                SqlParameter[] parameters =
                   {
                         new SqlParameter("@RequesterEmployeeID",dtAppraisalTeachinTable)                         
                         
                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_PDRPCheckAnnualWorkFlowAllEmployee", parameters);


                if (reader.HasRows)
                {

                    //dtResult.Columns.Add("FormProcessID");
                    while (reader.Read())
                    {
                        Result = reader["Result"].ToString();
                        if (Result.Trim().Length>0)
                        {
                            oprationDetails.Success = false;
                            oprationDetails.CssClass = "error";
                            oprationDetails.Message = Result;
                        }
                        else
                        {
                            oprationDetails.Success = true;
                            oprationDetails.CssClass = "success";
                            oprationDetails.Message = PDRPAnnualAppraisalXMLResources.GetResource("Savedsuccessfully");
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public OperationDetails SetAppraisalVisitDateMulty(List<PDRPTeachersModel> SelectedTeachersList, PDRPTeachersModel SelectedTeacher, int YearId, DataTable dtResult)
        {
            string Result = "";
            OperationDetails oprationDetails = new OperationDetails();
            DataTable dtAppraisalTeachinTable = new DataTable();
            dtAppraisalTeachinTable.Columns.Add("EmployeeID");
            dtAppraisalTeachinTable.Columns.Add("EvaluationDate", System.Type.GetType("System.DateTime"));
            dtAppraisalTeachinTable.Columns.Add("EvaluationDueDate", System.Type.GetType("System.DateTime"));

            foreach (var item in SelectedTeachersList)
            {
                dtAppraisalTeachinTable.Rows.Add(item.EmployeeId, item.VisitDate, item.PreObservationDate);
            }

            try
            {

                SqlParameter[] parameters =
                   {
                         new SqlParameter("@AppraisalDateTable",dtAppraisalTeachinTable),
                         new SqlParameter("@AppraisalYearID",YearId),
                         new SqlParameter("@CreatedBy",SelectedTeacher.CreatedBy),
                         //new SqlParameter("@CompanyID",SelectedTeacher.CompanyId),
                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SavePDRPAppraisalDate", parameters);


                if (reader.HasRows)
                {
                    
                    dtResult.Columns.Add("FormProcessID");
                    while (reader.Read())
                    {
                        Result = reader["Result"].ToString();
                        if (Result != "SUCCESS")
                        {
                            oprationDetails.Success = false;
                            oprationDetails.CssClass = "error";
                            oprationDetails.Message = PDRPAnnualAppraisalXMLResources.GetResource("LineManagernotset")+"</br>" + Result;
                        }
                        else
                        {
                            Result = reader["FormProcessID"].ToString();
                            Result = reader["GroupID"].ToString();
                            Result = reader["ApproverID"].ToString();
                            Result = reader["RequestID"].ToString();

                            dtResult.Rows.Add(reader["FormProcessID"].ToString());

                            oprationDetails.Success = true;
                            oprationDetails.CssClass = "success";
                            oprationDetails.Message = PDRPAnnualAppraisalXMLResources.GetResource("Savedsuccessfully");
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }
        public AppraisalTeachingModel GetAppraisalTeachingHeader(AppraisalTeachingModel objAppraisalTeachingModel)
        {
            try
            {
                SqlParameter[] parameters =
                  {
                         new SqlParameter("@ID",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID),
                         new SqlParameter("@RequestID",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestID),

                    };

                SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_SelectPDRPAppraisalTeaching", parameters);
                objAppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages = new List<AppraisalTeachingRatingScaleWeightages>();
                AppraisalTeachingRatingScaleWeightages objAppraisalTeachingRatingScaleWeightages;
                SetAppraisalTeachingHeader(objAppraisalTeachingModel, reader);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objAppraisalTeachingModel;
        }

        public static void SetAppraisalTeachingHeader(AppraisalTeachingModel objAppraisalTeachingModel, SqlDataReader reader)
        {
            objAppraisalTeachingModel.AppraisalKPIsListModel = new AppraisalKPIsListModel();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestID = reader["RequestID"].ToString();
                    //objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? 0 : reader["ID"]); 
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ReqStatusID = Convert.ToInt32(reader["ReqStatusID"] == DBNull.Value ? 0 : reader["ReqStatusID"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? 0 : reader["CompanyID"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequesterID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? 0 : reader["RequesterEmployeeID"]);

                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID = Convert.ToInt32(reader["TeacherID"] == DBNull.Value ? 0 : reader["TeacherID"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherAlternateID = Convert.ToInt32(reader["EmployeeAlternativeID"] == DBNull.Value ? 0 : reader["EmployeeAlternativeID"]);

                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYear = Convert.ToString(reader["AcademicYearName_1"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYearID = Convert.ToInt32(reader["AppraisalYear"] == DBNull.Value ? 0 : reader["AppraisalYear"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.Branch = Convert.ToString(reader["Branch"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.GradeYear = Convert.ToString(reader["GradeYear"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.GroupSection = Convert.ToString(reader["GroupSection"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.LastYearScore = Convert.ToString(reader["LastYearScore"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ThisYearScore = Convert.ToDecimal(reader["ThisYearScore"] == DBNull.Value ? 0 : reader["ThisYearScore"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ThisYearScoreDescription = Convert.ToString(reader["ThisYearScoreDescription"]);
                    objAppraisalTeachingModel.DropInRating = Convert.ToInt32(reader["DropInRating"] == DBNull.Value ? 0 : reader["DropInRating"]);
                    objAppraisalTeachingModel.DropInScoreOverall = Convert.ToDecimal(reader["DropInScoreOverall"] == DBNull.Value ? 0 : reader["DropInScoreOverall"]);
                    objAppraisalTeachingModel.DropInComment = Convert.ToString(reader["DropInComment"]);
                    objAppraisalTeachingModel.FormulasDropin = Convert.ToString(reader["FormulasDropin"]);
                    objAppraisalTeachingModel.FormulasObservation = Convert.ToString(reader["FormulasObservation"]);

                    objAppraisalTeachingModel.ObservEvlRating = Convert.ToInt32(reader["ObservEvlRating"] == DBNull.Value ? 0 : reader["ObservEvlRating"]);
                    objAppraisalTeachingModel.ObservEvlScoreOverall = Convert.ToDecimal(reader["ObservEvlScoreOverall"] == DBNull.Value ? 0 : reader["ObservEvlScoreOverall"]);
                    objAppraisalTeachingModel.ObservEvlComment = Convert.ToString(reader["ObservEvlComment"]);

                    objAppraisalTeachingModel.DevelopmentPlan = Convert.ToString(reader["DevelopmentPlan"]);

                    objAppraisalTeachingModel.PromotionPotantial = Convert.ToInt32(reader["PromotionPotantial"] == DBNull.Value ? 0 : reader["PromotionPotantial"]);
                    objAppraisalTeachingModel.PromotionPotantialComment = Convert.ToString(reader["PromotionPotantialComment"]);

                    objAppraisalTeachingModel.NextPromotion = Convert.ToInt32(reader["NextPromotion"] == DBNull.Value ? 0 : reader["NextPromotion"]);
                    objAppraisalTeachingModel.NextPromotionComment = Convert.ToString(reader["NextPromotionComment"]);

                    objAppraisalTeachingModel.LeavingRisk = Convert.ToInt32(reader["LeavingRisk"] == DBNull.Value ? 0 : reader["LeavingRisk"]);
                    objAppraisalTeachingModel.LeavingRiskComment = Convert.ToString(reader["LeavingRiskComment"]);

                    objAppraisalTeachingModel.LeavingReason = Convert.ToInt32(reader["LeavingReason"] == DBNull.Value ? 0 : reader["LeavingReason"]);
                    objAppraisalTeachingModel.LeavingReasonComment = Convert.ToString(reader["LeavingReasonComment"]);
                    objAppraisalTeachingModel.KPIFormula = Convert.ToString(reader["Formla"]);
                    objAppraisalTeachingModel.FormMode = Convert.ToInt32(reader["FormMode"] == DBNull.Value ? 0 : reader["FormMode"]);
                    objAppraisalTeachingModel.ApproverEmployeeID = Convert.ToInt32(reader["ApproverEmployeeID"] == DBNull.Value ? 0 : reader["ApproverEmployeeID"]);
                    objAppraisalTeachingModel.AppraisalKPIsListModel.TotalKPIScore = Convert.ToDecimal(reader["TotalKPIScore"] == DBNull.Value ? 0 : reader["TotalKPIScore"]);

                    objAppraisalTeachingModel.ObservEvlID = Convert.ToInt32(reader["ObservID"] == DBNull.Value ? 0 : reader["ObservID"]);

                    objAppraisalTeachingModel.TalentAreaImage = Convert.ToString(reader["TalentAreaImage"]);

                    objAppraisalTeachingModel.AppraisalComment = Convert.ToString(reader["AppraisalComment"]);

                    //*** F4
                    objAppraisalTeachingModel.EmpCurrentCompanyID = Convert.ToInt32(reader["EmpCurrentCompanyID"] == DBNull.Value ? 0 : reader["EmpCurrentCompanyID"]);
                    //*** F6
                    objAppraisalTeachingModel.Comments = Convert.ToString(reader["Comments"]);
                    // Nithin 11-05-2020
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherName = Convert.ToString(reader["EmployeeName"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);

                }
            }




            reader.NextResult();

            if (reader.HasRows)
            {
                AppraisalTeachingRatingScaleWeightageDetails objAppraisalTeachingRatingScaleWeightageDetails;
                objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails = new List<AppraisalTeachingRatingScaleWeightageDetails>();
                while (reader.Read())
                {
                    objAppraisalTeachingRatingScaleWeightageDetails = new AppraisalTeachingRatingScaleWeightageDetails();

                    objAppraisalTeachingRatingScaleWeightageDetails.RatingScaleID = Convert.ToInt32(reader["RatingScaleID"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.RatingScaleCharacter = Convert.ToString(reader["RatingScaleCharacter"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.RatingScaleNumber = Convert.ToInt32(reader["RatingScaleNumber"] == DBNull.Value ? 0 : reader["RatingScaleNumber"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.FinalScoreSlabMIN = Convert.ToInt32(reader["FinalScoreSlabMIN"] == DBNull.Value ? 0 : reader["FinalScoreSlabMIN"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.FinalScoreSlabMAX = Convert.ToInt32(reader["FinalScoreSlabMAX"] == DBNull.Value ? 0 : reader["FinalScoreSlabMAX"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.DefinitionName = Convert.ToString(reader["DefinitionName"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.DefinitionDetails = Convert.ToString(reader["DefinitionDetails"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.DefinitionName2 = Convert.ToString(reader["DefinitionName2"]);
                    objAppraisalTeachingRatingScaleWeightageDetails.DefinitionDetails2 = Convert.ToString(reader["DefinitionDetails2"]);

                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails.Add(objAppraisalTeachingRatingScaleWeightageDetails);
                }
            }


            reader.NextResult();
            if (reader.HasRows)
            {
                AppraisalWeightagePercentageModel AppraisalWeightagePercentageModel;
                objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel = new List<AppraisalWeightagePercentageModel>();
                while (reader.Read())
                {
                    AppraisalWeightagePercentageModel = new AppraisalWeightagePercentageModel();

                    AppraisalWeightagePercentageModel.Section = reader["Section"].ToString();
                    AppraisalWeightagePercentageModel.SectionDescriptiopn = reader["SectionDescriptiopn"].ToString();
                    AppraisalWeightagePercentageModel.Section2 = reader["Section2"].ToString();
                    AppraisalWeightagePercentageModel.SectionDescriptiopn2 = reader["SectionDescriptiopn2"].ToString();
                    AppraisalWeightagePercentageModel.Weightag = reader["Weightag"].ToString();



                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel.Add(AppraisalWeightagePercentageModel);
                }
            }

            reader.NextResult();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.LastYearScore = Convert.ToString(reader["LastYearScore"]);
                }
            }
        }


        public string GetAppraisalBeginYear(int YearID)
        {
            string BeginYear = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT BeginYear FROM GEN_AcademicYear WHERE AcademicYearID = " + YearID.ToString(), sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        BeginYear = reader["BeginYear"].ToString();
                    }
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return BeginYear;
        }
        
        public OperationDetails ReInitialize(string RequestID)
        {
            OperationDetails oprationDetails = new OperationDetails();
            try
            {
               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_ReInitializePDRPAnnualAppraisalTeaching", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
               


                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
              
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                oprationDetails.Message = PDRPAnnualAppraisalXMLResources.GetResource("Reintializedsuccessfully");
            }
            
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            

            return oprationDetails;
        }
        public RequestFormsProcessModel SaveAppraisalTeaching(AppraisalTeachingModel objAppraisalTeachingModel, int ReqStatusID)
        {
            OperationDetails oprationDetails = new OperationDetails();
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            if (objAppraisalTeachingModel.FormMode==7)//*** Reinitialize
            {
                oprationDetails= ReInitialize(objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestID);
                if (oprationDetails.InsertedRowId > 0)
                {
                    objAppraisalTeachingModel.FormMode = 2;
                }
            }
            if ((objAppraisalTeachingModel.FormMode == 7 && oprationDetails.InsertedRowId > 0) ||
                objAppraisalTeachingModel.FormMode != 7)
            {
                string Result = "", FormProcessID = "", GroupID = "", RequestID = "", ApproverID = "";
                //OperationDetails oprationDetails = new OperationDetails();
               
                try
                {
                    DataTable RatingScaleTable = new DataTable();
                    RatingScaleTable.Columns.Add("RatingScaleCharacter");
                    RatingScaleTable.Columns.Add("RatingScaleNumber");
                    RatingScaleTable.Columns.Add("FinalScoreSlabMIN");
                    RatingScaleTable.Columns.Add("FinalScoreSlabMAX");
                    RatingScaleTable.Columns.Add("DefinitionName");
                    RatingScaleTable.Columns.Add("DefinitionDetails");
                    RatingScaleTable.Columns.Add("DefinitionName2");
                    RatingScaleTable.Columns.Add("DefinitionDetails2");
                    foreach (AppraisalTeachingRatingScaleWeightageDetails Level in objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingRatingScaleWeightageDetails)
                    {

                        RatingScaleTable.Rows.Add(Level.RatingScaleCharacter, Level.RatingScaleNumber, Level.FinalScoreSlabMIN, Level.FinalScoreSlabMAX, Level.DefinitionName, Level.DefinitionDetails, Level.DefinitionName2, Level.DefinitionDetails2);

                    }

                    DataTable WeightingPercentages = new DataTable();
                    WeightingPercentages.Columns.Add("Section");
                    WeightingPercentages.Columns.Add("SectionDescriptiopn");
                    WeightingPercentages.Columns.Add("Section2");
                    WeightingPercentages.Columns.Add("SectionDescriptiopn2");
                    WeightingPercentages.Columns.Add("Weightag");
                    foreach (AppraisalWeightagePercentageModel Level in objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalWeightagePercentageModel)
                    {

                        WeightingPercentages.Rows.Add(Level.Section, Level.SectionDescriptiopn, Level.Section2, Level.SectionDescriptiopn2, Level.Weightag);

                    }

                    DataTable AppraisalTeachingTable = new DataTable();
                    AppraisalTeachingTable.Columns.Add("YearlyKPIsID");
                    AppraisalTeachingTable.Columns.Add("KPIOrder");
                    AppraisalTeachingTable.Columns.Add("Accomplishments");
                    AppraisalTeachingTable.Columns.Add("Ratings");
                    AppraisalTeachingTable.Columns.Add("Score", System.Type.GetType("System.Decimal"));
                    AppraisalTeachingTable.Columns.Add("FormatedFormla");
                    //int PDRPDropInsID = DBNull.Value;
                    string Formla = objAppraisalTeachingModel.KPIFormula;
                    int Row = 1;
                    foreach (AppraisalKPIsModel Level in objAppraisalTeachingModel.AppraisalKPIsListModel.AppraisalKPIsModel)
                    {
                        if (Level.Score > 0)
                        {
                            string FormatedFormla = Formla.Replace("Rating", Level.Ratings.ToString()).Replace("Weightage", Level.KPIWeightage.ToString());
                            AppraisalTeachingTable.Rows.Add(Level.KPIId, Row, Level.Accomplishments, Level.Ratings, Level.Score, FormatedFormla);
                            Row++;
                        }
                    }


                    //string VisitDateTime = Convert.ToDateTime(Convert.ToDateTime(objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.VisitDATE).ToShortDateString()).ToString("yyyy/MM/dd");
                    //VisitDateTime = VisitDateTime + " " + objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.VisitTime;

                    SqlParameter[] parameters =
                        {
                         new SqlParameter("@RequestID", objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestID),
                         new SqlParameter("@AppraisalTeachingID", objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID),
                         new SqlParameter("@CompanyID",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID),
                         new SqlParameter("@ReqStatusID", ReqStatusID),
                         //new SqlParameter("@RequesterEmployeeID",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequesterID),
                         new SqlParameter("@TeacherID",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID),
                         new SqlParameter("@LastYearScore",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.LastYearScore),
                         new SqlParameter("@ThisYearScore",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ThisYearScore),
                         new SqlParameter("@ThisYearScoreDescription",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.ThisYearScoreDescription),


                         new SqlParameter("@Branch",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.Branch),
                         new SqlParameter("@GradeYear",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.GradeYear),
                         new SqlParameter("@GroupSection",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.GroupSection),
                         new SqlParameter("@TotalKPIScore",objAppraisalTeachingModel.AppraisalKPIsListModel.TotalKPIScore),
                         new SqlParameter("@FormMode",objAppraisalTeachingModel.FormMode),
                         new SqlParameter("@DropInComment",objAppraisalTeachingModel.DropInComment),
                         new SqlParameter("@ObservEvlComment",objAppraisalTeachingModel.ObservEvlComment),
                         new SqlParameter("@DevelopmentPlan",objAppraisalTeachingModel.DevelopmentPlan),
                         new SqlParameter("@PromotionPotantial",objAppraisalTeachingModel.PromotionPotantial),
                         new SqlParameter("@PromotionPotantialComment",objAppraisalTeachingModel.PromotionPotantialComment),
                         new SqlParameter("@NextPromotion",objAppraisalTeachingModel.NextPromotion),
                         new SqlParameter("@NextPromotionComment",objAppraisalTeachingModel.NextPromotionComment),
                         new SqlParameter("@LeavingRisk",objAppraisalTeachingModel.LeavingRisk),
                         new SqlParameter("@LeavingRiskComment",objAppraisalTeachingModel.LeavingRiskComment),
                         new SqlParameter("@LeavingReason",objAppraisalTeachingModel.LeavingReason),
                         new SqlParameter("@LeavingReasonComment",objAppraisalTeachingModel.LeavingReasonComment),
                         new SqlParameter("@TalentAreaImage",objAppraisalTeachingModel.TalentAreaImage),
                         new SqlParameter("@AppraisalComment",objAppraisalTeachingModel.AppraisalComment),

                         new SqlParameter("@EmployeeName",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherName),
                         new SqlParameter("@Department",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.DepartmentName),
                         new SqlParameter("@Designation",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.PositionTitle),
                         new SqlParameter("@LineManager",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.SuperviserName),
                         new SqlParameter("@School",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyName),

                         new SqlParameter("@DropInsRate",objAppraisalTeachingModel.DropInRating),
                         new SqlParameter("@DropInsScore",objAppraisalTeachingModel.DropInScoreOverall),
                         new SqlParameter("@ObservationRate",objAppraisalTeachingModel.ObservEvlRating),
                         new SqlParameter("@ObservationScore",objAppraisalTeachingModel.ObservEvlScoreOverall),
                         new SqlParameter("@FormulasDropIn",objAppraisalTeachingModel.FormulasDropin),
                         new SqlParameter("@FormulasObservation",objAppraisalTeachingModel.FormulasObservation),

                         new SqlParameter("@CreatedBy",objAppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequesterID),
                         new SqlParameter("@AppraisalTeachingTable",AppraisalTeachingTable),
                         new SqlParameter("@RatingScaleTable",RatingScaleTable),
                         new SqlParameter("@AnnualWeightingPercentages",WeightingPercentages),
                         new SqlParameter("@Comments",objAppraisalTeachingModel.Comments),

                    };
                    SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure,
                                       "HR_Stp_SavePDRPAnnualAppraisalTeaching", parameters);

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            FormProcessID = reader["FormProcessID"].ToString();
                            GroupID = reader["GroupID"].ToString();
                            ApproverID = reader["ApproverID"].ToString();
                            RequestID = reader["RequestID"].ToString();
                            Result = reader["Result"].ToString();

                            requestFormsProcessModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? 0 : reader["FormProcessID"]);
                            requestFormsProcessModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? 0 : reader["RequestID"]);
                        }
                    }


                }
                catch (Exception exception)
                {
                    throw exception;
                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return requestFormsProcessModel;
        }

        #region Teaching Appraisal Status Report 
        public List<TeachingAppraisalStatusReportModel> GetTeachingAppraisalStatusReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int userId)
        {
            List<TeachingAppraisalStatusReportModel> teachingAppraisalStatusList = null;
            try
            {
                teachingAppraisalStatusList = new List<TeachingAppraisalStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_TeachingAppraisalStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@CurrentStatus", statusId);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(toDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    TeachingAppraisalStatusReportModel teachingAppraisalStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        teachingAppraisalStatusReportModel = new TeachingAppraisalStatusReportModel();
                        teachingAppraisalStatusReportModel.OrganizationName = Convert.ToString(sqlDataReader["OrganizationName"] == DBNull.Value ? "" : sqlDataReader["OrganizationName"].ToString());
                        teachingAppraisalStatusReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        teachingAppraisalStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        teachingAppraisalStatusReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        teachingAppraisalStatusReportModel.IsLessonObservationCompleted = Convert.ToString(sqlDataReader["IsLessonObservationCompleted"] == DBNull.Value ? "0" : sqlDataReader["IsLessonObservationCompleted"]);
                        teachingAppraisalStatusReportModel.DropInsCompleted = Convert.ToString(sqlDataReader["DropInsCompleted"] == DBNull.Value ? "" : sqlDataReader["DropInsCompleted"]);
                        teachingAppraisalStatusReportModel.IsAppraisalInitiated = Convert.ToString(sqlDataReader["IsAppraisalInitiated"] == DBNull.Value ? "" : sqlDataReader["IsAppraisalInitiated"]);
                        teachingAppraisalStatusReportModel.RequestId = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"]);
                        teachingAppraisalStatusReportModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        teachingAppraisalStatusReportModel.StartDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["StartDate"] == DBNull.Value ? "" : sqlDataReader["StartDate"]));
                        teachingAppraisalStatusReportModel.DueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DueDate"] == DBNull.Value ? "" : sqlDataReader["DueDate"]));
                        teachingAppraisalStatusReportModel.AppraisalSignOffDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["AppraisalSignOffDate"] == DBNull.Value ? "" : sqlDataReader["AppraisalSignOffDate"]));
                        teachingAppraisalStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        teachingAppraisalStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        teachingAppraisalStatusList.Add(teachingAppraisalStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return teachingAppraisalStatusList;
        }
        #endregion

        #region PDRP Non-Teaching Reports

        public List<GoalSettingReportModel> GetGoalSettingReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? year)
        {
            List<GoalSettingReportModel> goalSettingList = null;
            try
            {
                goalSettingList = new List<GoalSettingReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetGoalSettingReports", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@CurrentStatus", statusId);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(toDate));
                sqlCommand.Parameters.AddWithValue("@Year", year);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    GoalSettingReportModel goalSettingReportModel;
                    while (sqlDataReader.Read())
                    {
                        goalSettingReportModel = new GoalSettingReportModel();
                        goalSettingReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"].ToString());
                        goalSettingReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        goalSettingReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        goalSettingReportModel.EmployeeOracleNumber = Convert.ToString(sqlDataReader["EmployeeOracleNumber"] == DBNull.Value ? "" : sqlDataReader["EmployeeOracleNumber"]);
                        goalSettingReportModel.IsGoalSettingInitialize = Convert.ToString(sqlDataReader["IsGoalSettingInitialize"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSettingInitialize"]);
                        goalSettingReportModel.RequestId = Convert.ToString(sqlDataReader["RequestID"] == DBNull.Value ? "" : sqlDataReader["RequestID"]);
                        goalSettingReportModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        goalSettingReportModel.StartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["StartDate"] == DBNull.Value ? "" : sqlDataReader["StartDate"]));
                        goalSettingReportModel.DueDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DueDate"] == DBNull.Value ? "" : sqlDataReader["DueDate"]));
                        goalSettingReportModel.GoalSettingSignOffDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["GoalSettingSignOffDate"] == DBNull.Value ? "" : sqlDataReader["GoalSettingSignOffDate"]));
                        goalSettingReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        goalSettingReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        goalSettingList.Add(goalSettingReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return goalSettingList;
        }

        public List<MidYearEvaluationReportModel> GetMidYearEvaluationReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? year)
        {
            List<MidYearEvaluationReportModel> midyearEvaluatiopnList = null;
            try
            {
                midyearEvaluatiopnList = new List<MidYearEvaluationReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAnnualYearAppraisalReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@CurrentStatus", statusId);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(toDate));
                sqlCommand.Parameters.AddWithValue("@Year", year);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    MidYearEvaluationReportModel midYearReportModel;
                    while (sqlDataReader.Read())
                    {
                        midYearReportModel = new MidYearEvaluationReportModel();
                        midYearReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"].ToString());
                        midYearReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        midYearReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        midYearReportModel.EmployeeOracleNumber = Convert.ToString(sqlDataReader["EmployeeOracleNumber"] == DBNull.Value ? "" : sqlDataReader["EmployeeOracleNumber"]);
                        midYearReportModel.IsGoalSettingInitialize = Convert.ToString(sqlDataReader["IsGoalSettingInitialize"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSettingInitialize"]);
                        midYearReportModel.IsGoalSettingCompleted = Convert.ToString(sqlDataReader["IsGoalSettingCompleted"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSettingCompleted"]);
                        midYearReportModel.IsMidYrEvaluationInitialize = Convert.ToString(sqlDataReader["IsMidYrEvaluationInitialize"] == DBNull.Value ? "0" : sqlDataReader["IsMidYrEvaluationInitialize"]);
                        midYearReportModel.RequestID = Convert.ToString(sqlDataReader["RequestID"] == DBNull.Value ? "" : sqlDataReader["RequestID"]);
                        midYearReportModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        midYearReportModel.StartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["StartDate"] == DBNull.Value ? "" : sqlDataReader["StartDate"]));
                        midYearReportModel.DueDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DueDate"] == DBNull.Value ? "" : sqlDataReader["DueDate"]));
                        midYearReportModel.MidYearSignOffDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["MidYearSignOffDate"] == DBNull.Value ? "" : sqlDataReader["MidYearSignOffDate"]));
                        midYearReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        midYearReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        midyearEvaluatiopnList.Add(midYearReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return midyearEvaluatiopnList;
        }


        public List<FullYearEvaluationReportModel> GetFullYearEvaluationReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? year)
        {
            List<FullYearEvaluationReportModel> fullyearEvaluatiopnList = null;
            try
            {
                fullyearEvaluatiopnList = new List<FullYearEvaluationReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetFullYearAppraisalReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@CurrentStatus", statusId);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(toDate));
                sqlCommand.Parameters.AddWithValue("@Year", year);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    FullYearEvaluationReportModel fullYearReportModel;
                    while (sqlDataReader.Read())
                    {
                        fullYearReportModel = new FullYearEvaluationReportModel();
                        fullYearReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"].ToString());
                        fullYearReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        fullYearReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        fullYearReportModel.EmployeeOracleNumber = Convert.ToString(sqlDataReader["EmployeeOracleNumber"] == DBNull.Value ? "" : sqlDataReader["EmployeeOracleNumber"]);
                        fullYearReportModel.IsGoalSettingInitialize = Convert.ToString(sqlDataReader["IsGoalSettingInitialize"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSettingInitialize"]);
                        fullYearReportModel.IsGoalSettingCompleted = Convert.ToString(sqlDataReader["IsGoalSettingCompleted"] == DBNull.Value ? "0" : sqlDataReader["IsGoalSettingCompleted"]);
                        fullYearReportModel.IsMidYrEvaluationInitialize = Convert.ToString(sqlDataReader["IsMidYrEvaluationInitialize"] == DBNull.Value ? "0" : sqlDataReader["IsMidYrEvaluationInitialize"]);
                        fullYearReportModel.IsMidYrEvaluationCompleted = Convert.ToString(sqlDataReader["IsMidYrEvaluationCompleted"] == DBNull.Value ? "0" : sqlDataReader["IsMidYrEvaluationCompleted"]);
                        fullYearReportModel.IsFullYrEvaluationInitialize = Convert.ToString(sqlDataReader["IsFullYrEvaluationInitialize"] == DBNull.Value ? "0" : sqlDataReader["IsFullYrEvaluationInitialize"]);
                        fullYearReportModel.RequestID = Convert.ToString(sqlDataReader["RequestID"] == DBNull.Value ? "" : sqlDataReader["RequestID"]);
                        fullYearReportModel.Year = Convert.ToString(sqlDataReader["Year"] == DBNull.Value ? "" : sqlDataReader["Year"]);
                        fullYearReportModel.StartDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["StartDate"] == DBNull.Value ? "" : sqlDataReader["StartDate"]));
                        fullYearReportModel.DueDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DueDate"] == DBNull.Value ? "" : sqlDataReader["DueDate"]));
                        fullYearReportModel.FullYearSignOffDate = GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["FullYearSignOffDate"] == DBNull.Value ? "" : sqlDataReader["FullYearSignOffDate"]));
                        fullYearReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        fullYearReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        fullyearEvaluatiopnList.Add(fullYearReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return fullyearEvaluatiopnList;
        }
        #endregion
    }
}
