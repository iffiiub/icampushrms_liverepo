﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using HRMS.Entities.ViewModel;
using HRMS.Entities.General;

namespace HRMS.DataAccess
{
    public class YearDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        /// <summary>
        /// Add YEar Details
        /// </summary>
        /// <param name="YearModel"></param>
        /// <returns></returns>
        public OperationDetails AddYear(YearModel YearModel)
        {
            string Message = "";
            int insertedRowId = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Year", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
 
                sqlCommand.Parameters.AddWithValue("@StartMonth", YearModel.StartMonth);
                sqlCommand.Parameters.AddWithValue("@EndMonth", YearModel.EndMonth);                

              //  sqlCommand.Parameters.AddWithValue("@Year", YearModel.Year);
                sqlCommand.Parameters.AddWithValue("@CompanyId", YearModel.CompanyId);

                sqlCommand.Parameters.AddWithValue("@CreatedBy", YearModel.CreatedBy);

                sqlCommand.Parameters.AddWithValue("@WeekStartDay", YearModel.WeekStartDay);
                sqlCommand.Parameters.AddWithValue("@WeekEndDay", YearModel.WeekEndDay);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();


                Message = OperationMessage.Value.ToString();
                insertedRowId = Convert.ToInt32(OperationId.Value);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, insertedRowId);
        }


        /// <summary>
        /// Update Profile Details
        /// </summary>
        /// <param name="YearModel"></param>
        /// <returns></returns>
        public OperationDetails UpdateYear(YearModel YearModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_Year", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@YearID", YearModel.YearID);
                sqlCommand.Parameters.AddWithValue("@StartMonth", YearModel.StartMonth);
                sqlCommand.Parameters.AddWithValue("@EndMonth", YearModel.EndMonth);
              
             //   sqlCommand.Parameters.AddWithValue("@Year", YearModel.Year);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", YearModel.ModifiedBy);

                sqlCommand.Parameters.AddWithValue("@WeekStartDay", YearModel.WeekStartDay);
                sqlCommand.Parameters.AddWithValue("@WeekEndDay", YearModel.WeekEndDay);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, YearModel.YearID);
        }



        public YearModel Get_YearByYearId(int YearId)
        {
            YearModel YearModel = new YearModel();
            List<YearModel> YearModelList = null;
            try
            {
                YearModelList = new List<YearModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_YearByYearID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@YearId", YearId);

                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {                         
                        YearModel.YearID = Convert.ToInt32(sqlDataReader["YearID"].ToString());
                        YearModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        YearModel.StartMonth = (MonthsEnum)int.Parse(sqlDataReader["StartMonth"].ToString());
                        YearModel.EndMonth = (MonthsEnum)int.Parse(sqlDataReader["EndMonth"].ToString());
                       // YearModel.Year = Convert.ToInt32(sqlDataReader["Year"]);
                        YearModel.WeekStartDay = (WeekdaysEnum)int.Parse(sqlDataReader["WeekStartDay"].ToString());
                        YearModel.WeekEndDay = (WeekdaysEnum)int.Parse(sqlDataReader["WeekEndDay"].ToString());
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return YearModel;
        }


        public YearModel Get_YearByCompanyId(int CompanyId)
        {
            YearModel YearModel = new YearModel();
            List<YearModel> YearModelList = null;
            try
            {
                YearModelList = new List<YearModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_YearByCompanyID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyId);

                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        YearModel.YearID = Convert.ToInt32(sqlDataReader["YearID"].ToString());
                        YearModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        YearModel.StartMonth = (MonthsEnum)int.Parse(sqlDataReader["StartMonth"].ToString());
                        YearModel.EndMonth = (MonthsEnum)int.Parse(sqlDataReader["EndMonth"].ToString());
                        // YearModel.Year = Convert.ToInt32(sqlDataReader["Year"]);
                        YearModel.WeekStartDay = (WeekdaysEnum)int.Parse(sqlDataReader["WeekStartDay"].ToString());
                        YearModel.WeekEndDay = (WeekdaysEnum)int.Parse(sqlDataReader["WeekEndDay"].ToString());
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return YearModel;
        }

        /// <summary>
        /// Get All Year
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<YearModel> GetAllYears()
        {
            List<YearModel> YearModelList = null;
            try
            {
                YearModelList = new List<YearModel>();                
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Year", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;              
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    YearModel YearModel;
                    while (sqlDataReader.Read())
                    {
                        YearModel = new YearModel();
                        YearModel.YearID = Convert.ToInt32(sqlDataReader["YearID"].ToString());
                        YearModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        YearModel.StartMonth = (MonthsEnum)int.Parse(sqlDataReader["StartMonth"].ToString());
                        YearModel.EndMonth = (MonthsEnum)int.Parse(sqlDataReader["EndMonth"].ToString());
                        YearModel.WeekStartDay = (WeekdaysEnum)int.Parse(sqlDataReader["WeekStartDay"].ToString());
                        YearModel.WeekEndDay = (WeekdaysEnum)int.Parse(sqlDataReader["WeekEndDay"].ToString());
                        YearModelList.Add(YearModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return YearModelList;
        }


        public string DeleteYear(YearModel YearModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_Year", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@YearID", YearModel.YearID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

         

    }
}
