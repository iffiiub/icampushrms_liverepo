﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class EmployeeBasedHierarchyDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public List<WorkFlowGroupModel> GetWorkFlowGroupModelList(bool IsSchoolBasedGroup)
        {
            List<WorkFlowGroupModel> ObjWorkFlowGroupModelList = new List<WorkFlowGroupModel>();
            WorkFlowGroupModel ObjWorkFlowGroupModel = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetFormsWorkflowGroups", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SchoolBasedGroup", IsSchoolBasedGroup);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        ObjWorkFlowGroupModel = new WorkFlowGroupModel();

                        ObjWorkFlowGroupModel.GroupID = Convert.ToInt32(sqlDataReader["GroupID"].ToString());
                        ObjWorkFlowGroupModel.GroupName = Convert.ToString(sqlDataReader["GroupName"]);
                        ObjWorkFlowGroupModel.SubTitle = Convert.ToString(sqlDataReader["SubTitle"]);
                        ObjWorkFlowGroupModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        ObjWorkFlowGroupModel.Order = Convert.ToInt32(sqlDataReader["Order"].ToString());
                        ObjWorkFlowGroupModelList.Add(ObjWorkFlowGroupModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ObjWorkFlowGroupModelList;
        }

        public List<EmployeeHierarchyModel> GetEmployeeHierarchyModelList(int? userId, int? employeeId, int? departmentId, int? companyId)
        {
            List<EmployeeHierarchyModel> ObjEmployeeHierachyModelList = new List<EmployeeHierarchyModel>();
            EmployeeHierarchyModel ObjEmployeeHierachyModel = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeBasedHierarchy", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", departmentId);
                //*** Naresh 2020-03-22 Company filter added
                if (companyId.HasValue && companyId.Value > 0)
                    sqlCommand.Parameters.AddWithValue("@CompanyID", companyId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        ObjEmployeeHierachyModel = new EmployeeHierarchyModel();
                        ObjEmployeeHierachyModel.EBHierarchyID = sqlDataReader["EBHierarchyID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["EBHierarchyID"]);
                        ObjEmployeeHierachyModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        ObjEmployeeHierachyModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);


                        ObjEmployeeHierachyModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        ObjEmployeeHierachyModel.ManagerID = sqlDataReader["ManagerID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ManagerID"]);
                        ObjEmployeeHierachyModel.ManagerAlternativeID = Convert.ToString(sqlDataReader["ManagerAlternativeID"]);
                        ObjEmployeeHierachyModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"]);
                        ObjEmployeeHierachyModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        ObjEmployeeHierachyModel.ManagerName = !string.IsNullOrEmpty(Convert.ToString(sqlDataReader["ManagerAlternativeID"]))? "("+ Convert.ToString(sqlDataReader["ManagerAlternativeID"])+") "+ Convert.ToString(sqlDataReader["ManagerName"]):"";
                        ObjEmployeeHierachyModel.GroupID = sqlDataReader["GroupID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["GroupID"]);
                        ObjEmployeeHierachyModelList.Add(ObjEmployeeHierachyModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ObjEmployeeHierachyModelList;
        }

        public OperationDetails UpdateEmployeeHierarchy(List<EmployeeHierarchyModel> employeeHeirarchyList, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("EmployeeID");
                resultTable.Columns.Add("ManagerID");
                resultTable.Columns.Add("GroupID");

                foreach (EmployeeHierarchyModel empHier in employeeHeirarchyList)
                {
                    resultTable.Rows.Add(empHier.EmployeeID, empHier.ManagerID, empHier.GroupID);
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateEmployeeBasedHierarchy", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@HR_EmployeeHierarchy", resultTable);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
            }
            catch(Exception ex)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails DeleteEmployeeHierarchy(int employeeId, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DeleteEmployeeBasedHierarchy", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeId);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 1000);
                OutputMessage.Direction = ParameterDirection.Output;              
                sqlCommand.Parameters.Add(OutputMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                operationDetails.Message = OutputMessage.Value.ToString();
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.CssClass = "success";
                    operationDetails.Success = true;
                }
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                }
                //operationDetails.Message = "Deleted successfully";
            }
            catch(Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = ex.Message;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails IsPendingApprovalExists(int employeeID, int groupID, int managerID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_IsPendingApprovalExists", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                sqlCommand.Parameters.AddWithValue("@GroupID", groupID);
                sqlCommand.Parameters.AddWithValue("@ManagerID", managerID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
    }
}
