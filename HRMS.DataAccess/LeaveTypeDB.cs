﻿using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class LeaveTypeDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        DataAccess.GeneralDB.DataHelper dataHelper;
   
        public LeaveTypeModel GetLeaveTypeById(int LeaveTypeId)
        {
            try
            {
                LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_LeaveTypeByLeaveTypeID", new SqlParameter("@LeaveTypeID", LeaveTypeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            LeaveTypeModel.LeaveTypeId = reader["LeaveTypeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LeaveTypeId"]);

                            LeaveTypeModel.LeaveTypeName = Convert.ToString(reader["LeaveTypeName"]);
                            LeaveTypeModel.NoOfDays = reader["NoOfDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["NoOfDays"]);
                            LeaveTypeModel.RepeatedFor = reader["RepeatedFor"] == DBNull.Value ? 0 : Convert.ToInt32(reader["RepeatedFor"]);
                            LeaveTypeModel.ApplicableAfterDigits = reader["ApplicableAfterDigits"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ApplicableAfterDigits"]);
                            LeaveTypeModel.ApplicableAfterTypeID = reader["ApplicableAfterTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ApplicableAfterTypeID"]);
                            LeaveTypeModel.GenderID = reader["GenderID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["GenderID"]);
                            LeaveTypeModel.NationalityID = reader["NationalityID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["NationalityID"]);
                            LeaveTypeModel.ReligionID = reader["ReligionID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ReligionID"]);
                            LeaveTypeModel.isCompensate = reader["isCompensate"] == DBNull.Value ? 0 : Convert.ToInt32(reader["isCompensate"]);
                            LeaveTypeModel.NationalityName_1 = reader["NationalityName_1"].ToString();
                            LeaveTypeModel.ReligionName_1 = reader["ReligionName_1"].ToString();
                            LeaveTypeModel.GenderName_1 = reader["GenderName_1"].ToString();
                            LeaveTypeModel.AnnualLeave = reader["AnnualLeave"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AnnualLeave"]);
                            LeaveTypeModel.LifetimeLeave = reader["LifetimeLeave"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LifetimeLeave"]);
                            LeaveTypeModel.MonthlySplit = reader["MonthlySplit"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MonthlySplit"]);
                            LeaveTypeModel.MaritalStatusId = reader["MaritalStatusId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MaritalStatusId"]);



                        }
                    }
                }
                return LeaveTypeModel;
                // Finally, we return Position

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<LeaveTypeModel> GetLeaveTypeName()
        {
            List<LeaveTypeModel> LeaveTypeModelList = new List<LeaveTypeModel>();
            try
            {

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_LeaveTypeByLeaveTypeName"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();


                            LeaveTypeModel.LeaveTypeId = reader["LeaveTypeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LeaveTypeId"]);

                            LeaveTypeModel.LeaveTypeName = Convert.ToString(reader["LeaveTypeName"]);

                            LeaveTypeModelList.Add(LeaveTypeModel);


                        }
                    }
                }
                return LeaveTypeModelList; ;


            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<LeaveReason> GetLeaveReasonListWithPaging()
        {
            SqlParameter[] parameters =
                    {
                    };
            List<LeaveReason> LeaveTypeModelList = new List<LeaveReason>();
            try
            {

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_LeaveReason", parameters))
                {
                    if (reader.HasRows)
                    {
                        LeaveReason LeaveTypeModel;
                        while (reader.Read())
                        {
                            LeaveTypeModel = new LeaveReason();
                            LeaveTypeModel.ID = Convert.ToInt32(reader["ID"].ToString());
                            LeaveTypeModel.LeaveReason_1 = reader["LeaveReson_1"].ToString().Trim();
                            LeaveTypeModel.LeaveReason_2 = reader["LeaveReson_2"].ToString().Trim();
                            LeaveTypeModel.LeaveReason_3 = reader["LeaveReson_3"].ToString().Trim();

                            LeaveTypeModelList.Add(LeaveTypeModel);
                        }
                    }
                }

                return LeaveTypeModelList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<LeaveTypeModel> GetLeaveTypeForEmployee(Employee objemployee, int ApplicableAfterDigit)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeId",objemployee.EmployeeId),
                      new SqlParameter("@genderid",objemployee.employeeDetailsModel.GenderID),
                      new SqlParameter("@Nationalityid", objemployee.employeeDetailsModel.NationalityID),
                      new SqlParameter("@religionid",objemployee.employeeDetailsModel.ReligionID),
                       new SqlParameter("@Applicableafterdigits",ApplicableAfterDigit),

                        new SqlParameter("@MaritalStatusID",objemployee.employeeDetailsModel.MaritalStatusID),
                      new SqlParameter("@companyID",objemployee.CompanyId)
                    };


                List<LeaveTypeModel> LeaveTypeModellist = new List<LeaveTypeModel>();
                LeaveTypeModel LeaveTypeModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_LeaveTypeForEmployee", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            LeaveTypeModel = new LeaveTypeModel();
                            //   LeaveTypeModel.LeaveTypeId = Convert.ToInt32(reader["LeaveTypeId"] == DBNull.Value ? "0" :  Convert.ToString(reader["LeaveTypeId"]));
                            LeaveTypeModel.LeaveTypeId = reader["LeaveTypeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LeaveTypeId"]);

                            LeaveTypeModel.LeaveTypeName = Convert.ToString(reader["LeaveTypeName"]);
                            LeaveTypeModel.NoOfDays = reader["NoOfDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["NoOfDays"]);
                            LeaveTypeModel.RepeatedFor = reader["RepeatedFor"] == DBNull.Value ? 0 : Convert.ToInt32(reader["RepeatedFor"]);
                            LeaveTypeModel.ApplicableAfterDigits = reader["ApplicableAfterDigits"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ApplicableAfterDigits"]);
                            LeaveTypeModel.ApplicableAfterTypeID = reader["ApplicableAfterTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ApplicableAfterTypeID"]);
                            LeaveTypeModel.GenderID = reader["GenderID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["GenderID"]);
                            LeaveTypeModel.NationalityID = reader["NationalityID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["NationalityID"]);
                            LeaveTypeModel.ReligionID = reader["ReligionID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ReligionID"]);
                            LeaveTypeModel.isCompensate = reader["isCompensate"] == DBNull.Value ? 0 : Convert.ToInt32(reader["isCompensate"]);
                            //LeaveTypeModel.NationalityName_1 = reader["NationalityName_1"].ToString();
                            //LeaveTypeModel.ReligionName_1 = reader["ReligionName_1"].ToString();
                            //LeaveTypeModel.GenderName_1 = reader["GenderName_1"].ToString();
                            LeaveTypeModel.AnnualLeave = reader["AnnualLeave"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AnnualLeave"]);
                            LeaveTypeModel.LifetimeLeave = reader["LifetimeLeave"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LifetimeLeave"]);
                            LeaveTypeModel.MonthlySplit = reader["MonthlySplit"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MonthlySplit"]);
                            LeaveTypeModel.MaritalStatusId = reader["MaritalStatusId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MaritalStatusId"]);
                            LeaveTypeModellist.Add(LeaveTypeModel);

                        }
                    }
                }
                return LeaveTypeModellist;
                // Finally, we return Position

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public int[] GetLeaveTypeForLifetimeleave(int leavetypeid, int employeeid, int RepeatedFor)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@leavetypeid",leavetypeid),
                        new SqlParameter("@EmployeeId",employeeid),

                    };



                LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_LeaveTypeForLifetimeleave", parameters))
                {
                    // Check if the reader returned any rows
                    int[] a = new int[2];
                    if (reader.HasRows)
                    {


                        reader.Read();

                        int LeaveRepeated = Convert.ToInt32(reader["LeaveCount"]);
                        int NoOfDays = Convert.ToInt32(reader["DaysTaken"]);


                        a[0] = LeaveRepeated;
                        a[1] = NoOfDays;
                        return a;
                    }
                    else
                    {
                        a[0] = 0;
                        a[1] = 0;
                        return a;
                    }
                }


            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public int[] GetNoOfDaysForLifetimeleave(int leavetypeid, int employeeid, int RepeatedFor)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@leavetypeid",leavetypeid),
                        new SqlParameter("@EmployeeId",employeeid),

                    };



                LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_stp_Get_NoOfDaysForLifetimeleave", parameters))
                {
                    // Check if the reader returned any rows
                    int NoOfRepeats;
                    int NoOfDays;
                    int[] a = new int[2];
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        reader.Read();
                        NoOfRepeats = Convert.ToInt32(reader["LeaveCount"]);
                        NoOfDays = Convert.ToInt32(reader["DaysTaken"]);
                        a[0] = NoOfRepeats;
                        a[1] = NoOfDays;
                        return a;
                    }
                    else
                    {
                        a[0] = 0;
                        a[1] = 0;
                        return a;
                    }
                }


            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public int[] GetLeaveTypeForAnnualleaveNoOFRepeatAndDays(int leavetypeid, int employeeid, string startdate)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@leavetypeid",leavetypeid),
                        new SqlParameter("@EmployeeId",employeeid),
                       new SqlParameter("@StartDate",startdate),
                    };



                LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_LeaveTypeForAnnualleaveNoOFRepeat", parameters))
                {
                    int NoOfRepeats;
                    int NoOfDays;
                    int[] a = new int[2];
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        reader.Read();
                        NoOfRepeats = Convert.ToInt32(reader["NoOFRepeat"]);
                        NoOfDays = Convert.ToInt32(reader["DaysTaken"]);
                        a[0] = NoOfRepeats;
                        a[1] = NoOfDays;
                        return a;
                    }
                    else
                    {
                        a[0] = 0;
                        a[1] = 0;
                        return a;
                    }
                }


            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public int[] GetLeaveTypeForAnnualleaveIsMonthlySplit(int leavetypeid, int employeeid, string StartDate, string EndDate)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@leavetypeid",leavetypeid),
                        new SqlParameter("@EmployeeId",employeeid),

                         new SqlParameter("@StartDate",StartDate),
                        new SqlParameter("@EndDate",EndDate),


                    };



                LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Employee_MonthlySplitleave", parameters))
                {
                    int NoOfRepeats;
                    int NoOfDays;
                    int[] a = new int[2];
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        reader.Read();
                        NoOfRepeats = Convert.ToInt32(reader["NoOFRepeat"]);
                        NoOfDays = Convert.ToInt32(reader["DaysTaken"]);
                        a[0] = NoOfRepeats;
                        a[1] = NoOfDays;
                        return a;
                    }
                    else
                    {
                        a[0] = 0;
                        a[1] = 0;
                        return a;
                    }
                }


            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public int GetLeaveTypeForAnnualleave(int leavetypeid, int employeeid)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@ReasonID",leavetypeid),
                        new SqlParameter("@EmployeeId",employeeid),

                    };



                LeaveTypeModel LeaveTypeModel = new LeaveTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_LeaveTypeForAnnualleave", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        reader.Read();
                        int NoOfDays = Convert.ToInt32(reader["DaysTaken"]);
                        return NoOfDays;
                    }
                    else
                    {

                        return 0;
                    }
                }


            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<LeaveReason> GetLeaveReasonList(int ID, int mode)
        {
            List<LeaveReason> objLeaveTypeModel = new List<LeaveReason>();
            LeaveReason leaveReasonModel = new LeaveReason();
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@ID",ID),
                       new SqlParameter("@Mode",mode),
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_LeaveReason", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            //int NoOfDays = Convert.ToInt32(reader["DaysTaken"]);
                            leaveReasonModel = new LeaveReason();
                            leaveReasonModel.ID = Convert.ToInt32(reader["ID"].ToString());
                            leaveReasonModel.LeaveReason_1 = reader["LeaveReson_1"].ToString();
                            leaveReasonModel.LeaveReason_2 = reader["LeaveReson_2"].ToString();
                            leaveReasonModel.LeaveReason_3 = reader["LeaveReson_3"].ToString();
                            objLeaveTypeModel.Add(leaveReasonModel);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
            return objLeaveTypeModel;
        }

        public LeaveReason GetLeaveReasonById(int ID)
        {
            List<LeaveReason> objLeaveTypeModel = new List<LeaveReason>();
            LeaveReason leaveReasonModel = new LeaveReason();
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@ID",ID),
                       new SqlParameter("@Mode",1),
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_LeaveReason", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            //int NoOfDays = Convert.ToInt32(reader["DaysTaken"]);
                            leaveReasonModel = new LeaveReason();
                            leaveReasonModel.ID = Convert.ToInt32(reader["ID"].ToString());
                            leaveReasonModel.LeaveReason_1 = reader["LeaveReson_1"].ToString();
                            leaveReasonModel.LeaveReason_2 = reader["LeaveReson_2"].ToString();
                            leaveReasonModel.LeaveReason_3 = reader["LeaveReson_3"].ToString();
                            objLeaveTypeModel.Add(leaveReasonModel);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
            return objLeaveTypeModel.FirstOrDefault();
        }

        public string InsertUpdateLeaveReason(LeaveReason ObjLeaveReason)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_InsertUpdate_HR_LeaveReason", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", ObjLeaveReason.ID);
                sqlCommand.Parameters.AddWithValue("@LeaveReason_1", ObjLeaveReason.LeaveReason_1);
                sqlCommand.Parameters.AddWithValue("@LeaveReason_2", ObjLeaveReason.LeaveReason_2);
                sqlCommand.Parameters.AddWithValue("@LeaveReason_3", ObjLeaveReason.LeaveReason_3);

                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                //sqlConnection.Close();

                //Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return Message;

        }

        public string DeleteLeaveReason(int ID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_LeaveReason", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", ID);

                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                //Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return Message;

        }

        public LeaveSetting GetLeaveSetting()
        {
            dataHelper = new GeneralDB.DataHelper();
            string sp = "Hr_Stp_Get_Hr_LeaveSettings";
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>(), sp);
            LeaveSetting leaveSetting = new LeaveSetting();
            foreach (DataRow dr in dt.Rows)
            {
                leaveSetting.AccumulateLeaveMonth = dr["AccumulateLeaveMonth"] == DBNull.Value ? 12 : Convert.ToDecimal(dr["AccumulateLeaveMonth"]);
                leaveSetting.BeamHideFields = dr["BeamHideFields"] == DBNull.Value ? false : Convert.ToBoolean(dr["BeamHideFields"]);
                leaveSetting.ShowVTCategoryField = dr["ShowVTCategoryField"] == DBNull.Value ? false : Convert.ToBoolean(dr["ShowVTCategoryField"]); 
            }
            return leaveSetting;
        }
        public List<PickList> GetLeaveEntitlementTypeList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeaveEntitlementTypeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["LeaveEntitleTypeID"].ToString()), text = Convert.ToString(sqlDataReader["LeaveEntitlePeriod"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

    }
}
