﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class HealthDB : DBHelper
    {
        public OperationDetails InsertHealth(HealthModel HealthModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    

                        new SqlParameter("@DocumentNo",HealthModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", HealthModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", HealthModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(HealthModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(HealthModel.ExpiryDate)) ,
                      new SqlParameter("@Note",HealthModel.Note ) ,
                      new SqlParameter("@IsPrimary", HealthModel.IsPrimary ),
                     
                      new SqlParameter("@HealthFile", HealthModel.HealthFile ),
                      new SqlParameter("@EmployeeId", HealthModel.EmployeeId)
                    };
                int DocVisaId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_Health", parameters));
                return new OperationDetails(true, "Health saved successfully.", null, DocVisaId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Health .", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateHealth(HealthModel HealthModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@DocHealthId",HealthModel.DocHealthId),
                      new SqlParameter("@DocumentNo",HealthModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", HealthModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", HealthModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(HealthModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(HealthModel.ExpiryDate)) ,
                      new SqlParameter("@Note",HealthModel.Note ) ,
                      new SqlParameter("@IsPrimary", HealthModel.IsPrimary ),
                      new SqlParameter("@HealthFile", HealthModel.HealthFile )
                      
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Health", parameters);
                return new OperationDetails(true, "Health Document   updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Health Document.", exception);
                throw;
            }
            finally
            {

            }
        }

        public HealthModel HealthById(int DocHealthId)
        {
            try
            {
                HealthModel HealthModel = new HealthModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_HealthByID", new SqlParameter("@DocHealthId", DocHealthId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            HealthModel.DocHealthId = Convert.ToInt32(reader["DocHealthId"].ToString());
                            HealthModel.DocumentNo = reader["DocumentNo"].ToString();
                            HealthModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            HealthModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            HealthModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                            HealthModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            HealthModel.Note = reader["Note"].ToString();
                            HealthModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString()) ;
                            HealthModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]) ;
                            
                            HealthModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            HealthModel.HealthFile = reader["HealthFile"].ToString();
                        }
                    }
                }
                return HealthModel;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteHealthById(int HealthId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@DocHealthId",HealthId) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_HEalth", parameters);
                return new OperationDetails(true, "Health Document deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Health Document.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<HealthModel> GetHealthListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder,string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {    
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the LabourContract		
            List<HealthModel> HealthModelList = new List<HealthModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_HealthWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        HealthModel HealthModel;
                        while (reader.Read())
                        {
                            HealthModel = new HealthModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            HealthModel.DocHealthId = Convert.ToInt32(reader["DocHealthId"]);
                            HealthModel.DocumentNo = reader["DocumentNo"].ToString();
                            HealthModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            HealthModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            HealthModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            HealthModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            HealthModel.Note = reader["Note"].ToString();
                            HealthModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                           
                            HealthModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            HealthModel.HealthFile = reader["HealthFile"].ToString();
                            HealthModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            HealthModelList.Add(HealthModel);
                        }
                    }
                }
                // Finally, we return our list of HealthModelList
                totalCount = TotalCount;
                return HealthModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching LabourContract List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
