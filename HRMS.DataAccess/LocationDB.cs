﻿using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class LocationDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Insert Location Details
        /// </summary>
        /// <param name="locationModel"></param>
        /// <returns></returns>   
        public OperationDetails InsertLocation(LocationModel locationModel)
        {
            string Message = "";
            int LocationId=0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Location", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyID", locationModel.LocationCompanyID);
                sqlCommand.Parameters.AddWithValue("@LocationName", "");
                sqlCommand.Parameters.AddWithValue("@CountryID", locationModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@StateID", locationModel.StateID);
                sqlCommand.Parameters.AddWithValue("@CityID", locationModel.CityID);
                sqlCommand.Parameters.AddWithValue("@Email", locationModel.LocationEmail);
                sqlCommand.Parameters.AddWithValue("@Phone", locationModel.Phone);
                sqlCommand.Parameters.AddWithValue("@Fax", locationModel.Fax);
                sqlCommand.Parameters.AddWithValue("@PostCode", locationModel.PostCode);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", locationModel.CreatedBy);

                
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId= new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                LocationId = Convert.ToInt32(OperationId.Value);
                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, LocationId);
        }

        /// <summary>
        /// Update Location Details
        /// </summary>
        /// <param name="locationModel"></param>
        /// <returns></returns>
        public OperationDetails UpdateLocation(LocationModel locationModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_Location", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@LocationID", locationModel.LocationID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", locationModel.LocationCompanyID);
                sqlCommand.Parameters.AddWithValue("@LocationName", "");
                sqlCommand.Parameters.AddWithValue("@CountryID", locationModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@StateID", locationModel.StateID);
                sqlCommand.Parameters.AddWithValue("@CityID", locationModel.CityID);
                sqlCommand.Parameters.AddWithValue("@Email", locationModel.LocationEmail);
                sqlCommand.Parameters.AddWithValue("@Phone", locationModel.Phone);
                sqlCommand.Parameters.AddWithValue("@Fax", locationModel.Fax);
                sqlCommand.Parameters.AddWithValue("@PostCode", locationModel.PostCode);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", locationModel.ModifiedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);           

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();                
                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, locationModel.LocationID);
        }

        /// <summary>
        /// Delete Location
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns></returns>
        public OperationDetails DeleteLocation(int LocationId)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_Location", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LocationID", LocationId);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, LocationId);
        }

        /// <summary>
        /// Get Locations with paging,sorting
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<LocationModel> GetLocationList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder,int CompanyId, out int totalCount)
        {
            List<LocationModel> LocationList = null;
            try
            {
                LocationList = new List<LocationModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetLocations", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LocationModel locationModel;
                    while (sqlDataReader.Read())
                    {
                        locationModel = new LocationModel();
                        locationModel.LocationID = Convert.ToInt32(sqlDataReader["LocationID"].ToString());
                        locationModel.LocationName = Convert.ToString(sqlDataReader["LocationName"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        locationModel.LocationCompanyID = Convert.ToInt32(sqlDataReader["CompanyID"]);
                        locationModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"]);
                        locationModel.StateID = Convert.ToInt32(sqlDataReader["StateID"]);                        
                        locationModel.CityID = Convert.ToInt32(sqlDataReader["CityID"]);
                        locationModel.LocationEmail = Convert.ToString(sqlDataReader["Email"]);
                        locationModel.Phone=Convert.ToString(sqlDataReader["Phone"]);
                        locationModel.PostCode=Convert.ToString(sqlDataReader["PostCode"]);
                        locationModel.Fax  = Convert.ToString(sqlDataReader["Fax"]);
                        locationModel.CountryName=Convert.ToString(sqlDataReader["CountryName"]);
                        locationModel.CityName=Convert.ToString(sqlDataReader["CityName"]);
                        locationModel.StateName = Convert.ToString(sqlDataReader["StateName"]);
                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();

                        LocationList.Add(locationModel);
                    }
                }
                sqlConnection.Close();

                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return LocationList;
        }


        /// <summary>
        /// Get Location By LocationID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LocationModel GetLocationByID(int id)
        {
            LocationModel locationModel = new LocationModel();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_LocationById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@LocationID", id);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        locationModel = new LocationModel();

                        locationModel.LocationID = Convert.ToInt32(sqlDataReader["LocationID"].ToString());
                        locationModel.LocationName = Convert.ToString(sqlDataReader["LocationName"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        locationModel.LocationCompanyID = Convert.ToInt32(sqlDataReader["CompanyID"]);
                        locationModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"]);
                        locationModel.StateID = Convert.ToInt32(sqlDataReader["StateID"]);
                        locationModel.CityID = Convert.ToInt32(sqlDataReader["CityID"]);
                        locationModel.LocationEmail = Convert.ToString(sqlDataReader["Email"]);
                        locationModel.Phone = Convert.ToString(sqlDataReader["Phone"]);
                        locationModel.PostCode = Convert.ToString(sqlDataReader["PostCode"]);
                        locationModel.Fax = Convert.ToString(sqlDataReader["Fax"]);
                        locationModel.CountryName = Convert.ToString(sqlDataReader["CountryName"]);
                        locationModel.CityName = Convert.ToString(sqlDataReader["CityName"]);
                        locationModel.StateName = Convert.ToString(sqlDataReader["StateName"]);


                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return locationModel;
        }

        /// <summary>
        /// Get All Employees in Location
        /// </summary>
        /// <param name="locationid"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public List<EmployeeDetailsModel> GetEmployeeByLocationId(int locationid, int CompanyId)
        {


            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeByLocationId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LocationID", locationid);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    EmployeeDetailsModel EmployeeDetails;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDetails = new EmployeeDetailsModel();
                        EmployeeDetails.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDetails.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"].ToString());
                        EmployeeDetails.SurName_1 = Convert.ToString(sqlDataReader["Surname_1"].ToString());
                        EmployeeDetailsList.Add(EmployeeDetails);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();

                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();

                }
            }
            return EmployeeDetailsList;


        }


       


        /// <summary>
        /// Get All Employees not in Location
        /// </summary>
        /// <param name="locationid"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public List<EmployeeDetailsModel> GetEmployeeNotInLocationId(int locationid, int CompanyId)
        {
            //departmentid = 1;

            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeNotInLocationId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LocationID", locationid);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    EmployeeDetailsModel EmployeeDetails;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDetails = new EmployeeDetailsModel();
                        EmployeeDetails.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDetails.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"].ToString());
                        EmployeeDetails.SurName_1 = Convert.ToString(sqlDataReader["Surname_1"].ToString());
                        EmployeeDetailsList.Add(EmployeeDetails);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();

                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();

                }
            }
            return EmployeeDetailsList;


        }


        public List<EmployeeDetailsModel> GetEmployeeNotInLocationId(int locationid, int CompanyId,string search)
        {
            //departmentid = 1;

            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeNotInLocationIdSearch", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LocationID", locationid);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                sqlCommand.Parameters.AddWithValue("@search", search);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    EmployeeDetailsModel EmployeeDetails;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDetails = new EmployeeDetailsModel();
                        EmployeeDetails.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDetails.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"].ToString());
                        EmployeeDetails.SurName_1 = Convert.ToString(sqlDataReader["Surname_1"].ToString());
                        EmployeeDetailsList.Add(EmployeeDetails);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();

                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();

                }
            }
            return EmployeeDetailsList;


        }

        /// <summary>
        /// Insert Employee Location
        /// </summary>
        /// <param name="LocationID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public string InsertEmployeeLocation(int LocationID,int EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Location_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@LocationID", LocationID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
              
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);                

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                
                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Remove Employee From Location
        /// </summary>
        /// <param name="LocationID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public string RemoveEmployeeLocation(int LocationID, int EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Remove_HR_Location_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@LocationID", LocationID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }
        


        /// <summary>
        /// Get Location By CompanyID
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <returns></returns>
        public List<LocationModel> GetLocationName()
        {
            List<LocationModel> locationList = new List<LocationModel>();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_LocationName", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

               // sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LocationModel locationModel = null;
                    while (sqlDataReader.Read())
                    {
                        locationModel = new LocationModel();

                        locationModel.LocationID = Convert.ToInt32(sqlDataReader["LocationID"].ToString());
                        locationModel.LocationName = Convert.ToString(sqlDataReader["LocationName"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                      
                        // locationModel.LocationName = Convert.ToString(sqlDataReader["CountryName"].ToString() + "," + sqlDataReader["StateName"] + "," + sqlDataReader["CityName"]);
                        locationList.Add(locationModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return locationList;
        }


        /// <summary>
        /// Get Location By CompanyID
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <returns></returns>
        public List<LocationModel> GetLocationByCompanyID(int CompanyID)
        {
            List<LocationModel> locationList = new List<LocationModel>();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_LocationByCompanyId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LocationModel locationModel = null;
                    while (sqlDataReader.Read())
                    {
                        locationModel = new LocationModel();

                        locationModel.LocationID = Convert.ToInt32(sqlDataReader["LocationID"].ToString());
                       locationModel.LocationName = Convert.ToString(sqlDataReader["LocationName"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        locationModel.LocationCompanyID = Convert.ToInt32(sqlDataReader["CompanyID"]);
                        locationModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"]);
                        locationModel.StateID = Convert.ToInt32(sqlDataReader["StateID"]);
                        locationModel.CityID = Convert.ToInt32(sqlDataReader["CityID"]);
                        locationModel.LocationEmail = Convert.ToString(sqlDataReader["Email"]);
                        locationModel.Phone = Convert.ToString(sqlDataReader["Phone"]);
                        locationModel.PostCode = Convert.ToString(sqlDataReader["PostCode"]);
                        locationModel.Fax = Convert.ToString(sqlDataReader["Fax"]);
                        locationModel.CountryName = Convert.ToString(sqlDataReader["CountryName"]);
                        locationModel.CityName = Convert.ToString(sqlDataReader["CityName"]);
                        locationModel.StateName = Convert.ToString(sqlDataReader["StateName"]);
                       // locationModel.LocationName = Convert.ToString(sqlDataReader["CountryName"].ToString() + "," + sqlDataReader["StateName"] + "," + sqlDataReader["CityName"]);
                        locationList.Add(locationModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return locationList;
        }


    }
}
