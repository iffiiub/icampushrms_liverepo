﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class HRSalaryGradeDB
    {
        public SqlConnection objConnection { get; set; }
        public SqlCommand objCommand { get; set; }
        public SqlDataReader objReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// If passed SalaryGradeID = 0 then , Insert otherWise will update that Id row
        /// </summary>
        /// <param name="objHRSalaryGrade">pass whose values has to be changed while Updating</param>
        /// <returns>Inserted Or Updated HRSalaryGrade Id</returns>
        public int InsertUpdateHRSalaryGrade(HRSalaryGrade objHRSalaryGrade)
        {
            int result = 0;
            try
            {
                objConnection = new SqlConnection(connectionString);
                objConnection.Open();
                objCommand = new SqlCommand("stp_HR_SalaryGrade_InsertUpdate", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.Parameters.AddWithValue("@SalaryGradeID", objHRSalaryGrade.salaryGradeID);
                objCommand.Parameters.AddWithValue("@SalaryGradeCode", objHRSalaryGrade.salaryGradeCode);
                objCommand.Parameters.AddWithValue("@SalaryGradeLevel", objHRSalaryGrade.salaryGradeLevel);
                objCommand.Parameters.AddWithValue("@Description", objHRSalaryGrade.description);
                objCommand.Parameters.AddWithValue("@MonthlyHours", objHRSalaryGrade.monthlyHours);
                objCommand.Parameters.AddWithValue("@MinHourPrice", objHRSalaryGrade.minHourPrice);
                objCommand.Parameters.AddWithValue("@MedHourPrice", objHRSalaryGrade.medHourPrice);
                objCommand.Parameters.AddWithValue("@MaxHourPrice", objHRSalaryGrade.maxHourPrice);
                objCommand.Parameters.AddWithValue("@isActive", objHRSalaryGrade.isActive);
                objCommand.Parameters.AddWithValue("@CompanyId", objHRSalaryGrade.companyId);
                objCommand.Parameters.AddWithValue("@CreatedBy", objHRSalaryGrade.createdBy);
                objCommand.Parameters.AddWithValue("@IsDeleted", objHRSalaryGrade.isDeleted);

                result = Convert.ToInt16(objCommand.ExecuteScalar());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objConnection != null && objConnection.State == ConnectionState.Open)
                {
                    objConnection.Close();
                }
            }
        }

        public int DeleteHRSalaryGrade(HRSalaryGrade objHRSalaryGrade)
        {
            int result = 0;
            try
            {
                objConnection = new SqlConnection(connectionString);
                objConnection.Open();
                objCommand = new SqlCommand("stp_HR_SalaryGrade_Delete", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.Parameters.AddWithValue("@SalaryGradeID", objHRSalaryGrade.salaryGradeID);
                
                objCommand.Parameters.AddWithValue("@CreatedBy", objHRSalaryGrade.createdBy);
                objCommand.Parameters.AddWithValue("@IsDeleted", objHRSalaryGrade.isDeleted);

                result = Convert.ToInt16(objCommand.ExecuteScalar());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objConnection != null && objConnection.State == ConnectionState.Open)
                {
                    objConnection.Close();
                }
            }
        }


        public List<HRSalaryGrade> GetHRSalaryGrade(int CompanyId, int SalaryGradeId, int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, out int totalCount)
        {
            List<HRSalaryGrade> HRSalaryGradeList = null;
            try
            {
                HRSalaryGradeList = new List<HRSalaryGrade>();
                objConnection = new SqlConnection(connectionString);
                objConnection.Open();
                objCommand = new SqlCommand("stp_HR_SalaryGrade_Get", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;
               
                objCommand.Parameters.AddWithValue("@SalaryGradeID", SalaryGradeId);
                objCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                objCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                objCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                objCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                objCommand.Parameters.AddWithValue("@SortOrder", sortOrder);

                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                objCommand.Parameters.Add(TotalCount);

                SqlDataReader objDataReader = objCommand.ExecuteReader();
                if (objDataReader.HasRows)
                {
                    HRSalaryGrade objHRSalaryGrade = null;
                    while (objDataReader.Read())
                    {
                        objHRSalaryGrade = new HRSalaryGrade();
                        objHRSalaryGrade.salaryGradeID = Convert.ToInt16(objDataReader["SalaryGradeID"].ToString());
                        objHRSalaryGrade.salaryGradeCode = objDataReader["SalaryGradeCode"].ToString();
                        objHRSalaryGrade.salaryGradeLevel = objDataReader["SalaryGradeLevel"].ToString();
                        objHRSalaryGrade.description = objDataReader["Description"].ToString();
                        objHRSalaryGrade.monthlyHours = Convert.ToDecimal(objDataReader["MonthlyHours"].ToString());
                        objHRSalaryGrade.minHourPrice = Convert.ToDecimal(objDataReader["MinHourPrice"].ToString());
                        objHRSalaryGrade.medHourPrice = Convert.ToDecimal(objDataReader["MedHourPrice"].ToString());
                        objHRSalaryGrade.maxHourPrice = Convert.ToDecimal(objDataReader["MaxHourPrice"].ToString());
                        objHRSalaryGrade.isActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        objHRSalaryGrade.companyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());

                        HRSalaryGradeList.Add(objHRSalaryGrade);
                    }
                }
                objConnection.Close();
                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;
                return HRSalaryGradeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            
            {
                if (objConnection != null && objConnection.State == ConnectionState.Open)
                {
                    objConnection.Close();
                }
            }
        }
    }
}
