﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class DepartmentDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails DepartmentInsertUpdate(DepartmentModel DepartmentModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            int iResult = 0;
            string Message = string.Empty;
            try
            {
                DataTable resultTable = new DataTable();

                resultTable.Columns.Add("EmployeeID", System.Type.GetType("System.Int32"));
                foreach (int employee in DepartmentModel.DepartmentEmployees)
                {
                    resultTable.Rows.Add(employee);
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_DepartmentInsertUpdate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@HR_DepartmentEmployementInformationTableType", resultTable);

                sqlCommand.Parameters.AddWithValue("@DepartmentId", DepartmentModel.DepartmentId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", DepartmentModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@DepartmentName_1", DepartmentModel.DepartmentName_1 == null ? "" : DepartmentModel.DepartmentName_1);
                sqlCommand.Parameters.AddWithValue("@DepartmentName_2", DepartmentModel.DepartmentName_2 == null ? "" : DepartmentModel.DepartmentName_2);
                sqlCommand.Parameters.AddWithValue("@DepartmentName_3", DepartmentModel.DepartmentName_3 == null ? "" : DepartmentModel.DepartmentName_3);
                sqlCommand.Parameters.AddWithValue("@Description", DepartmentModel.Description == null ? "" : DepartmentModel.Description);
                sqlCommand.Parameters.AddWithValue("@StartDate", GeneralDB.CommonDB.SetCulturedDate(DepartmentModel.StartDate));
                sqlCommand.Parameters.AddWithValue("@CreatedBy", DepartmentModel.CreatedBy == null ? 0 : DepartmentModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", DepartmentModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@DepartmentHead", DepartmentModel.DepartmentHead);
                sqlCommand.Parameters.AddWithValue("@Assistant1", DepartmentModel.Assistant1);
                sqlCommand.Parameters.AddWithValue("@Assistant2", DepartmentModel.Assistant2);
                sqlCommand.Parameters.AddWithValue("@Assistant3", DepartmentModel.Assistant3);
                sqlCommand.Parameters.AddWithValue("@DepartmentLeader", DepartmentModel.DepartmentLeader);
                sqlCommand.Parameters.AddWithValue("@ShiftID", DepartmentModel.ShiftID);
                sqlCommand.Parameters.AddWithValue("@ShortCode", DepartmentModel.ShortCode == null ? "" : DepartmentModel.ShortCode);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                // Insert Spoken Languages
                //sqlConnection.Open();                
                sqlDataReader.Close();

            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        /// <summary>
        /// Delete Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public string DeleteDepartment(int id)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_Department", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DepartmentID", id);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public DepartmentModel GetDepartment(int id)
        {
            DepartmentModel DepartmentModel = new DepartmentModel();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_DepartmentById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DepartmentID", id);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        DepartmentModel = new DepartmentModel();

                        DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
                        DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
                        DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        DepartmentModel.StartDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        DepartmentModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
                        DepartmentModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        DepartmentModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        DepartmentModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        DepartmentModel.ShiftID = sqlDataReader["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ShiftID"].ToString());
                        DepartmentModel.ShortCode = Convert.ToString(sqlDataReader["ShortCode"]);
                        DepartmentModel.DepartmentLeader = sqlDataReader["DepartmentLeader"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentLeader"].ToString());

                        //DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        //DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"]);
                        //DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        //DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
                        //DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
                        //DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        //DepartmentModel.StartDate = Convert.ToDateTime(sqlDataReader["StartDate"].ToString());
                        //DepartmentModel.CreatedBy = Convert.ToInt32(sqlDataReader["CreatedBy"].ToString());

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return DepartmentModel;
        }

        /// <summary>
        /// Get All Active Users
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<DepartmentModel> GetAllDepartment(int? companyId = null, int? userId=null)
        {
            List<DepartmentModel> DepartmentModelList = null;
            try
            {
                DepartmentModelList = new List<DepartmentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Department", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (companyId.HasValue && companyId.Value > 0)
                    sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DepartmentModel DepartmentModel;
                    while (sqlDataReader.Read())
                    {
                        DepartmentModel = new DepartmentModel();

                        DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]).Trim();
                        DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]).Trim();
                        DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]).Trim();
                        DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]).Trim();
                        DepartmentModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
                        DepartmentModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        DepartmentModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        DepartmentModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        DepartmentModel.ShiftID = sqlDataReader["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ShiftID"].ToString());
                        DepartmentModel.ShortCode = Convert.ToString(sqlDataReader["ShortCode"]).Trim();
                        DepartmentModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]).Trim();

                        //if (sqlDataReader["StartDate"])

                        if (!string.IsNullOrEmpty(sqlDataReader["StartDate"].ToString()))
                        {
                            DepartmentModel.StartDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        }
                        else
                        {
                            DepartmentModel.StartDate = DateTime.Now.ToString("dd/MM/yyyy");
                        }

                        // DepartmentModel.CreatedBy =Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) ;

                        DepartmentModelList.Add(DepartmentModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DepartmentModelList;
        }

        /// <summary>
        /// Get All Department by Company Id
        /// </summary>
        /// <returns></returns>
        public List<DepartmentModel> GetAllDepartmentByCompanyId(int CompanyId, int UserId)
        {
            List<DepartmentModel> DepartmentModelList = null;
            try
            {
                DepartmentModelList = new List<DepartmentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_DepartmentByCompanyId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DepartmentModel DepartmentModel;
                    while (sqlDataReader.Read())
                    {
                        DepartmentModel = new DepartmentModel();

                        DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
                        DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
                        DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        DepartmentModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
                        DepartmentModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        DepartmentModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        DepartmentModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        DepartmentModel.ShiftID = sqlDataReader["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ShiftID"].ToString());
                        DepartmentModel.ShortCode = Convert.ToString(sqlDataReader["ShortCode"]);

                        DepartmentModelList.Add(DepartmentModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DepartmentModelList.OrderBy(x => x.DepartmentName_1).ToList();
        }
        public List<DepartmentModel> GetDepartmentListWithPermission(int UserId)
        {
            List<DepartmentModel> ObjDepartmentModelList = new List<DepartmentModel>();
            DepartmentModel ObjDepartmentModel = null;
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_All_DepartmentList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        ObjDepartmentModel = new DepartmentModel();

                        ObjDepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        ObjDepartmentModel.CompanyId = sqlDataReader["CompanyId"] == DBNull.Value ? 1 : Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        ObjDepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        ObjDepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
                        ObjDepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
                        ObjDepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        ObjDepartmentModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
                        ObjDepartmentModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        ObjDepartmentModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        ObjDepartmentModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        ObjDepartmentModel.ShiftID = sqlDataReader["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ShiftID"].ToString());
                        ObjDepartmentModel.ShortCode = Convert.ToString(sqlDataReader["ShortCode"]);

                        ObjDepartmentModelList.Add(ObjDepartmentModel);

                    }
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return ObjDepartmentModelList;
        }
        public List<DepartmentModel> GetDepartmentList(int CompanyId)
        {
            List<DepartmentModel> ObjDepartmentModelList = new List<DepartmentModel>();
            DepartmentModel ObjDepartmentModel = null;
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_DepartmentList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);


                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        ObjDepartmentModel = new DepartmentModel();

                        ObjDepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        ObjDepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        ObjDepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        ObjDepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
                        ObjDepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
                        ObjDepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        ObjDepartmentModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
                        ObjDepartmentModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        ObjDepartmentModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        ObjDepartmentModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        ObjDepartmentModel.ShiftID = sqlDataReader["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ShiftID"].ToString());
                        ObjDepartmentModel.ShortCode = Convert.ToString(sqlDataReader["ShortCode"]);

                        ObjDepartmentModelList.Add(ObjDepartmentModel);

                    }
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return ObjDepartmentModelList;
        }
        public DataSet GetAllDepartmentDataset(int? companyId)
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_stp_DepartmentListExport", sqlConnection);
            if (companyId.HasValue && companyId.Value > 0)
                da.SelectCommand.Parameters.AddWithValue("@CompanyId", companyId);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }
        public IEnumerable<DepartmentModel> GetDepartmentListByUserId(int UserId)
        {
            try
            {
                List<DepartmentModel> deptList = new List<DepartmentModel>();
                DepartmentModel deptModel;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeeDepartmentByUserId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            deptModel = new DepartmentModel();
                            deptModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                            deptModel.DepartmentName_1 = sqlDataReader["DepartmentName_1"].ToString();
                            deptList.Add(deptModel);
                        }
                    }
                }

                return deptList.OrderBy(x => x.DepartmentName_1).AsEnumerable();
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
