﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace HRMS.DataAccess
{
    public class ShiftGRoupDB
    {


        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
       
        /// <summary>
        /// Add Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
     
       
        public string AddShiftGRoup(ShiftGRoupModel ShiftGRoupModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_Add_ShiftGRoup", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                sqlCommand.Parameters.AddWithValue("@GroupNameEn", ShiftGRoupModel.GroupNameEn == null ? "" : ShiftGRoupModel.GroupNameEn);
                sqlCommand.Parameters.AddWithValue("@GroupNameAr", ShiftGRoupModel.GroupNameAr == null ? "" : ShiftGRoupModel.GroupNameAr);
                sqlCommand.Parameters.AddWithValue("@GroupNameFr", ShiftGRoupModel.GroupNameFr == null ? "" : ShiftGRoupModel.GroupNameFr);
                sqlCommand.Parameters.AddWithValue("@Head", ShiftGRoupModel.Head);
                  sqlCommand.Parameters.AddWithValue("@Section", ShiftGRoupModel.Section);
                sqlCommand.Parameters.AddWithValue("@Shift", ShiftGRoupModel.Shift);

                sqlCommand.Parameters.AddWithValue("@Assistant1", ShiftGRoupModel.Assistant1);
                sqlCommand.Parameters.AddWithValue("@Assistant2", ShiftGRoupModel.Assistant2);
                sqlCommand.Parameters.AddWithValue("@Assistant3", ShiftGRoupModel.Assistant3);


                sqlCommand.Parameters.AddWithValue("@CreatedBy", ShiftGRoupModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyID", ShiftGRoupModel.CompanyId);
                  

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Update Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public string UpdateShiftGRoup(ShiftGRoupModel ShiftGRoupModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateShiftGroupByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GroupNameID", ShiftGRoupModel.GroupNameID);
                sqlCommand.Parameters.AddWithValue("@GroupNameEn", ShiftGRoupModel.GroupNameEn);
                sqlCommand.Parameters.AddWithValue("@GroupNameAr", ShiftGRoupModel.GroupNameAr == null ? "" : ShiftGRoupModel.GroupNameAr);
                sqlCommand.Parameters.AddWithValue("@GroupNameFr", ShiftGRoupModel.GroupNameFr == null ? "" : ShiftGRoupModel.GroupNameFr);
                sqlCommand.Parameters.AddWithValue("@Head", ShiftGRoupModel.Head == null ? 0 : ShiftGRoupModel.Head);
                sqlCommand.Parameters.AddWithValue("@Section", ShiftGRoupModel.Section == null ? 0 : ShiftGRoupModel.Section);
                sqlCommand.Parameters.AddWithValue("@Shift", ShiftGRoupModel.Shift == null ? 0 : ShiftGRoupModel.Shift);

                sqlCommand.Parameters.AddWithValue("@Assistant1", ShiftGRoupModel.Assistant1 == null ? 0 : ShiftGRoupModel.Assistant1);
                sqlCommand.Parameters.AddWithValue("@Assistant2", ShiftGRoupModel.Assistant2 == null ? 0 : ShiftGRoupModel.Assistant2);
                sqlCommand.Parameters.AddWithValue("@Assistant3", ShiftGRoupModel.Assistant3 == null ? 0 : ShiftGRoupModel.Assistant3);

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", ShiftGRoupModel.ModifiedBy);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        /// <summary>
        /// Delete Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public string DeleteShiftGRoupByID(int id)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_ShiftGRoupID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GroupNameID", id);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        public ShiftGRoupModel GetShiftGRoupByID(int id)
        {
            ShiftGRoupModel ShiftGRoupModel = new ShiftGRoupModel();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_ShiftGRoupID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GroupNameID", id);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        ShiftGRoupModel = new ShiftGRoupModel();

                        ShiftGRoupModel.GroupNameID = Convert.ToInt32(sqlDataReader["GroupNameID"] == DBNull.Value ? "0" : sqlDataReader["GroupNameID"].ToString());

                        ShiftGRoupModel.GroupNameEn = Convert.ToString(sqlDataReader["GroupNameEn"]);
                        ShiftGRoupModel.GroupNameAr = Convert.ToString(sqlDataReader["GroupNameAr"]);
                        ShiftGRoupModel.GroupNameFr = Convert.ToString(sqlDataReader["GroupNameFr"]);
                        ShiftGRoupModel.Head = Convert.ToInt32(sqlDataReader["Head"] == DBNull.Value ? "0" : sqlDataReader["Head"].ToString());
                        ShiftGRoupModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        ShiftGRoupModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        ShiftGRoupModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());

                        ShiftGRoupModel.Section = Convert.ToInt32(sqlDataReader["Section"] == DBNull.Value ? "0" : sqlDataReader["Section"].ToString());
                        ShiftGRoupModel.Shift = Convert.ToInt32(sqlDataReader["Shift"] == DBNull.Value ? "0" : sqlDataReader["Shift"].ToString());
                     

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return ShiftGRoupModel;
        }

        /// <summary>
        /// Get All Active Users
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<ShiftGRoupModel> GetAllShiftGRoup(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, int CompanyId, string search) 
        {
            List<ShiftGRoupModel> ShiftGRoupModelList = null;
            try
            {
                ShiftGRoupModelList = new List<ShiftGRoupModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_ShiftGRoup", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", "GroupNameID");
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                sqlCommand.Parameters.AddWithValue("@search", search);
                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
                if (sqlDataReader1.HasRows)
                {
                    ShiftGRoupModel ShiftGRoupModel;
                    while (sqlDataReader1.Read())
                    {
                        ShiftGRoupModel = new ShiftGRoupModel();

                        ShiftGRoupModel.GroupNameID = Convert.ToInt32(sqlDataReader1["GroupNameID"] == DBNull.Value ? "0" : sqlDataReader1["GroupNameID"].ToString());

                        ShiftGRoupModel.GroupNameEn = Convert.ToString(sqlDataReader1["GroupNameEn"]);
                        ShiftGRoupModel.GroupNameAr = Convert.ToString(sqlDataReader1["GroupNameAr"]);
                        ShiftGRoupModel.GroupNameFr = Convert.ToString(sqlDataReader1["GroupNameFr"]);

                        ShiftGRoupModel.Head = Convert.ToInt32(sqlDataReader1["Head"] == DBNull.Value ? "0" : sqlDataReader1["Head"].ToString());
                      
                       // ShiftGRoupModel.Head = Convert.ToInt32(sqlDataReader1["Head"]);
                        //ShiftGRoupModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        //ShiftGRoupModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        //ShiftGRoupModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());

                        //ShiftGRoupModel.Section = Convert.ToInt32(sqlDataReader["Section"]);
                        //ShiftGRoupModel.Shift = Convert.ToInt32(sqlDataReader["Shift"]);
                        
                        //if (sqlDataReader["StartDate"])

                       // ShiftGRoupModel.CreatedBy =Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) ;

                        ShiftGRoupModelList.Add(ShiftGRoupModel);
                    }
                }
               
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ShiftGRoupModelList;
        }

        public List<ShiftGRoupModel> GetAllShiftGRoupList()
        {
            List<ShiftGRoupModel> ShiftGRoupModelList = null;
            try
            {
                ShiftGRoupModelList = new List<ShiftGRoupModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_ShiftGRoupList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;




                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ShiftGRoupModel ShiftGRoupModel;
                    while (sqlDataReader.Read())
                    {
                        ShiftGRoupModel = new ShiftGRoupModel();
                        ShiftGRoupModel.GroupNameID = Convert.ToInt32(sqlDataReader["GroupNameID"] == DBNull.Value ? "0" : sqlDataReader["GroupNameID"].ToString());

                   //     ShiftGRoupModel.GroupNameID = Convert.ToInt32(sqlDataReader["GroupNameID"].ToString());

                        ShiftGRoupModel.GroupNameEn = Convert.ToString(sqlDataReader["GroupNameEn"]);
                        ShiftGRoupModel.GroupNameAr = Convert.ToString(sqlDataReader["GroupNameAr"]);
                        ShiftGRoupModel.GroupNameFr = Convert.ToString(sqlDataReader["GroupNameFr"]);
                        ShiftGRoupModel.Head = Convert.ToInt32(string.IsNullOrWhiteSpace(sqlDataReader["Head"].ToString()) ? 0 : sqlDataReader["Head"]);
                            
 
                        
                        //ShiftGRoupModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        //ShiftGRoupModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        //ShiftGRoupModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());

                        //ShiftGRoupModel.Section = Convert.ToInt32(sqlDataReader["Section"]);
                        //ShiftGRoupModel.Shift = Convert.ToInt32(sqlDataReader["Shift"]);

                        //if (sqlDataReader["StartDate"])

                        // ShiftGRoupModel.CreatedBy =Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) ;

                        ShiftGRoupModelList.Add(ShiftGRoupModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ShiftGRoupModelList;
        }

        //public List<ShiftGRoupModel> GetDepartmentList()
        //{
        //    List<ShiftGRoupModel> ObjShiftGRoupModelList = new List<ShiftGRoupModel>();
        //    ShiftGRoupModel ObjShiftGRoupModel = null;
        //    try
        //    {

        //        connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_Get_DepartmentList", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                 
        //        // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
        //        // TotalCount.Direction = ParameterDirection.Output;
        //        //sqlCommand.Parameters.Add(TotalCount);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {

        //            while (sqlDataReader.Read())
        //            {
        //                ObjShiftGRoupModel = new ShiftGRoupModel();

        //                ObjShiftGRoupModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
        //                ObjShiftGRoupModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
        //                ObjShiftGRoupModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
        //                ObjShiftGRoupModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
        //                ObjShiftGRoupModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
        //                ObjShiftGRoupModel.Description = Convert.ToString(sqlDataReader["Description"]);
        //                ObjShiftGRoupModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
        //                ObjShiftGRoupModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
        //                ObjShiftGRoupModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
        //                ObjShiftGRoupModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());

        //                ObjShiftGRoupModelList.Add(ObjShiftGRoupModel);
                   
        //            }
        //        }
             


        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }

        //    return ObjShiftGRoupModelList;
        //}


        //public List<ShiftGRoupModel> GetDepartmentList(int CompanyId)
        //{
        //    List<ShiftGRoupModel> ObjShiftGRoupModelList = new List<ShiftGRoupModel>();
        //    ShiftGRoupModel ObjShiftGRoupModel = null;
        //    try
        //    {

        //        connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_Get_DepartmentList", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //        sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);

                 
        //        // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
        //        // TotalCount.Direction = ParameterDirection.Output;
        //        //sqlCommand.Parameters.Add(TotalCount);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {

        //            while (sqlDataReader.Read())
        //            {
        //                ObjShiftGRoupModel = new ShiftGRoupModel();

        //                ObjShiftGRoupModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
        //                ObjShiftGRoupModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
        //                ObjShiftGRoupModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
        //                ObjShiftGRoupModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
        //                ObjShiftGRoupModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
        //                ObjShiftGRoupModel.Description = Convert.ToString(sqlDataReader["Description"]);
        //                ObjShiftGRoupModel.DepartmentHead = sqlDataReader["DepartmentHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentHead"].ToString());
        //                ObjShiftGRoupModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
        //                ObjShiftGRoupModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
        //                ObjShiftGRoupModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());

        //                ObjShiftGRoupModelList.Add(ObjShiftGRoupModel);
                    
        //            }
        //        }
 


        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }

        //    return ObjShiftGRoupModelList;
        //} 
    }
}
