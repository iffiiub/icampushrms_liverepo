﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class SocialSecurityDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        DataAccess.GeneralDB.DataHelper dataHelper;

        public List<SocialSecurirtyModel> GetSocialSecurityList()
        {
            List<SocialSecurirtyModel> SocialSecurirtyList = null;
            try
            {
                int decimalCount = 2;
                decimalCount = new PayrollDB().GetDigitAfterDecimal();
                SocialSecurirtyList = new List<SocialSecurirtyModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSocialSecurityDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    SocialSecurirtyModel SocialSecurirty;
                    while (sqlDataReader.Read())
                    {
                        SocialSecurirty = new SocialSecurirtyModel();
                        SocialSecurirty.SSID = Convert.ToInt32(sqlDataReader["SSID"].ToString());
                        SocialSecurirty.EmployeeAltId = sqlDataReader["EmployeeAlternativeID"].ToString();
                        SocialSecurirty.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        SocialSecurirty.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        SocialSecurirty.Percentage = Convert.ToDecimal(sqlDataReader["Percentage"]);
                        SocialSecurirty.Amount = Math.Round(Convert.ToDecimal(sqlDataReader["Amount"].ToString()), decimalCount);
                        SocialSecurirty.Salary = Math.Round(Convert.ToDecimal(sqlDataReader["FullSalary"].ToString()), decimalCount);
                        SocialSecurirtyList.Add(SocialSecurirty);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return SocialSecurirtyList;
        }

        public OperationDetails SavePasiTaxPerSetting(int employeeId, decimal Percentage, int? SSID,decimal taxAmount)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateSocialSecurityPercentage", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeId);
                sqlCommand.Parameters.AddWithValue("@Percentage", Percentage);
                sqlCommand.Parameters.AddWithValue("@TaxAmount", taxAmount);
                if (SSID == 0)
                {
                    sqlCommand.Parameters.AddWithValue("@SSID", DBNull.Value);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@SSID", SSID);
                }
                SqlParameter IsSuccess = new SqlParameter("@IsSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                sqlCommand.ExecuteReader();
                op.Success = Convert.ToBoolean(IsSuccess.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public int GetDeduTypeIdForSS()
        {
            int DedTypeId = 0;
            dataHelper = new GeneralDB.DataHelper();
            DataTable dt = dataHelper.ExcuteCommandText("select SocialSecurityDeductionID FROM HR_TaxSettings");

            foreach (DataRow dr in dt.Rows)
            {
                DedTypeId = Convert.ToInt32(dr["SocialSecurityDeductionID"].ToString());
            }
            return DedTypeId;
        }

        public OperationDetails InsertSocialSecurityintoPayDeduct(SocialSecurirtyModel SocialSecurirty, int cycleid, int Deductiontype, string EffectiveDate, string cyclename)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode", 1),
                        new SqlParameter("@aTntDeductionTypeID", Deductiontype),
                        new SqlParameter("@aNumPaidCycle", 1),
                        new SqlParameter("@CycleID", cycleid),
                        new SqlParameter("@aBitIsInstallment", 0),
                        new SqlParameter("@aNumAmount", SocialSecurirty.Amount),
                        new SqlParameter("@aNumInstallment", 1),
                        new SqlParameter("@aNvrComments", string.Format("Social Security for {0}", cyclename)),
                        new SqlParameter("@aIntEmployeeID", SocialSecurirty.EmployeeID),
                        new SqlParameter("@aNvrRefNumber", string.Format("Social Security for {0}", cyclename)),
                        new SqlParameter("@aDttEffectiveDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate( EffectiveDate)),
                        new SqlParameter ("@aDttTransactionDate", DateTime.Now),
                        new SqlParameter ("@aIntPvId", DBNull.Value),
                        new SqlParameter ("@output", 0)
                    };

                parameters[13].Direction = ParameterDirection.Output;
                int AbsentId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_xspPayDeductionCUD ", parameters));
                return new OperationDetails(true, "Social security saved successfully.", null, AbsentId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred .", exception);
            }
            finally
            {

            }
        }

        public OperationDetails CheackPayDetailIsPresent(int cycleID, int deductionTypeID, string EmployeeIDs)
        {
            string query = "Select * from HR_PayDeduction where CycleID =" + cycleID + " and DeductionTypeID = " + deductionTypeID + " and EmployeeID in (" + EmployeeIDs+")";
            OperationDetails details = new OperationDetails();
            try
            {
                dataHelper = new GeneralDB.DataHelper();
                DataTable table = dataHelper.ExcuteCommandText(query);


                if (table.Rows.Count > 0)
                {
                    details.Success = false;
                    details.Message = "Record is already present";
                }
                else
                {
                    details.Success = true;
                    details.Message = "Record is not present";
                }
                return details;
            }
            catch
            {
                return details;

            }
        }

        public OperationDetails UpdateSocialSecurityDeduction(List<SocialSecurirtyModel> List, string effectiveDate, int cycleId)
        {            
            int DeductionTypeid = GetDeduTypeIdForSS();
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int count = 1;
            try
            {
                DataTable pasiTaxTable = new DataTable();
                pasiTaxTable.Columns.Add("EmployeeID", typeof(int));
                pasiTaxTable.Columns.Add("Amount", typeof(decimal));
                pasiTaxTable.Columns.Add("RowId", typeof(int));
                foreach (var item in List)
                {
                    pasiTaxTable.Rows.Add(item.EmployeeID, item.Amount, count++);
                }
                List<SqlParameter> parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("@SocialTaxTable", pasiTaxTable));
                parameterList.Add(new SqlParameter("@effectiveDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(effectiveDate)));
                parameterList.Add(new SqlParameter("@cycleId", cycleId));
                parameterList.Add(new SqlParameter("@deductionTypeId", DeductionTypeid));
                bool result = dataHelper.ExcutestoredProcedure(parameterList, "Hr_Stp_UpdateSocialSecurityDeduction");
                oDetails.Success = true;
                oDetails.Message = "Records reposted successfully.";
                oDetails.CssClass = "success";
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "Technical error has occured";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }
    }
}
