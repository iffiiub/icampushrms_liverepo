﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class ShortLeaveRequestDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public List<ShortLeaveRequestModel> GetShortLeaveRequestList(int empid = 0, int UserID = 0, string FromDate = "", string ToDate = "")
        {
            List<ShortLeaveRequestModel> ShortLeaveRequestModelList = null;
            try
            {
                ShortLeaveRequestModelList = new List<ShortLeaveRequestModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveRequestPaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(FromDate))
                {
                    sqlCommand.Parameters.AddWithValue("@fromDate", CommonDB.SetCulturedDate(FromDate));
                }
                if (!string.IsNullOrEmpty(ToDate))
                {
                    sqlCommand.Parameters.AddWithValue("@toDate", CommonDB.SetCulturedDate(ToDate));
                }
                if (empid != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@empid", empid);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@empid", null);
                }
                sqlCommand.Parameters.AddWithValue("@UserId", UserID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ShortLeaveRequestModel shortLeaveRequestModel;
                    while (sqlDataReader.Read())
                    {
                        shortLeaveRequestModel = new ShortLeaveRequestModel();
                        shortLeaveRequestModel.ShortLeaveID = Convert.ToInt32(sqlDataReader["ShortLeaveID"].ToString());
                        shortLeaveRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        shortLeaveRequestModel.RequesterEmployeeID = sqlDataReader["RequesterEmployeeID"].ToString() != "" ? Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString()) : 0;
                        shortLeaveRequestModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        shortLeaveRequestModel.LeaveDate = sqlDataReader["LeaveDate"].ToString();
                        shortLeaveRequestModel.LeaveTime = sqlDataReader["LeaveTime"].ToString();
                        shortLeaveRequestModel.ReturnTime = sqlDataReader["ReturnTime"].ToString();
                        shortLeaveRequestModel.ActualLeaveTime = sqlDataReader["ActualLeaveTime"].ToString();
                        shortLeaveRequestModel.ActualReturnTime = sqlDataReader["ActualReturnTime"].ToString();
                        shortLeaveRequestModel.LateMinutes = sqlDataReader["LateMinutes"].ToString() != "" ? Convert.ToInt32(sqlDataReader["LateMinutes"].ToString()) : 0;
                        shortLeaveRequestModel.EarlyMinutes = sqlDataReader["EarlyMinutes"].ToString() != "" ? Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString()) : 0;
                        shortLeaveRequestModel.OrganizationName = sqlDataReader["OrganizationName"].ToString();
                        shortLeaveRequestModel.ApprovalStatus = sqlDataReader["ApprovalStatus"].ToString();
                        shortLeaveRequestModel.Status = sqlDataReader["Status"].ToString();
                        shortLeaveRequestModel.ApproverId = sqlDataReader["ApproverId"].ToString() != "" ? Convert.ToInt32(sqlDataReader["ApproverId"].ToString()) : 0;
                        shortLeaveRequestModel.CreatedOn = sqlDataReader["CreatedOn"].ToString();
                        shortLeaveRequestModel.CreatedBy = sqlDataReader["CreatedBy"].ToString() != "" ? Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) : 0;
                        shortLeaveRequestModel.ModifiedOn = sqlDataReader["ModifiedOn"].ToString();
                        shortLeaveRequestModel.ModifiedBy = sqlDataReader["ModifiedBy"].ToString() != "" ? Convert.ToInt32(sqlDataReader["ModifiedBy"].ToString()) : 0;
                        ShortLeaveRequestModelList.Add(shortLeaveRequestModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ShortLeaveRequestModelList;
        }
        public ShortLeaveRequestModel GetShortLeaveListById(int shortLeaveID)
        {
            ShortLeaveRequestModel shortLeaveRequestModel = new ShortLeaveRequestModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveRequestById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", shortLeaveID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        shortLeaveRequestModel.ShortLeaveID = Convert.ToInt32(sqlDataReader["ShortLeaveID"].ToString());
                        shortLeaveRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        shortLeaveRequestModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        shortLeaveRequestModel.FullName = sqlDataReader["FullName"].ToString();
                        shortLeaveRequestModel.LeaveDate = sqlDataReader["LeaveDate"].ToString();
                        shortLeaveRequestModel.LeaveTime = sqlDataReader["LeaveTime"].ToString();
                        shortLeaveRequestModel.ReturnTime = sqlDataReader["ReturnTime"].ToString();
                        shortLeaveRequestModel.ActualLeaveTime = sqlDataReader["ActualLeaveTime"].ToString();
                        shortLeaveRequestModel.ActualReturnTime = sqlDataReader["ActualReturnTime"].ToString();
                        shortLeaveRequestModel.LateMinutes = sqlDataReader["LateMinutes"].ToString() != "" ? Convert.ToInt32(sqlDataReader["LateMinutes"].ToString()) : 0;
                        shortLeaveRequestModel.EarlyMinutes = sqlDataReader["EarlyMinutes"].ToString() != "" ? Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString()) : 0;
                        shortLeaveRequestModel.Comments = sqlDataReader["Comments"].ToString();
                        shortLeaveRequestModel.ReturnNextDay = Convert.ToBoolean(sqlDataReader["ReturnNextDay"] == DBNull.Value ? 0 : sqlDataReader["ReturnNextDay"]);
                        shortLeaveRequestModel.ApprovalStatus = sqlDataReader["ApprovalStatus"].ToString();
                        shortLeaveRequestModel.Status = sqlDataReader["Status"].ToString();
                        shortLeaveRequestModel.CreatedOn = sqlDataReader["CreatedOn"].ToString();
                        shortLeaveRequestModel.CreatedBy = sqlDataReader["CreatedBy"].ToString() != "" ? Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) : 0;
                        shortLeaveRequestModel.ModifiedOn = sqlDataReader["ModifiedOn"].ToString();
                        shortLeaveRequestModel.ModifiedBy = sqlDataReader["ModifiedBy"].ToString() != "" ? Convert.ToInt32(sqlDataReader["ModifiedBy"].ToString()) : 0;
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return shortLeaveRequestModel;
        }
        public OperationDetails AddShortLeave(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveRequestCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objShortLeaveRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objShortLeaveRequestModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@LeaveDate", DateTime.ParseExact(objShortLeaveRequestModel.LeaveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@LeaveTime", objShortLeaveRequestModel.LeaveTime.ToString());
                sqlCommand.Parameters.AddWithValue("@ReturnTime", objShortLeaveRequestModel.ReturnTime);
                sqlCommand.Parameters.AddWithValue("@ActualLeaveTime", objShortLeaveRequestModel.ActualLeaveTime);
                sqlCommand.Parameters.AddWithValue("@ActualReturnTime", objShortLeaveRequestModel.ActualReturnTime);
                sqlCommand.Parameters.AddWithValue("@LateMinutes", objShortLeaveRequestModel.LateMinutes);
                sqlCommand.Parameters.AddWithValue("@EarlyMinutes", objShortLeaveRequestModel.EarlyMinutes);
                sqlCommand.Parameters.AddWithValue("@Comments", objShortLeaveRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@ReturnNextDay", objShortLeaveRequestModel.ReturnNextDay);
                sqlCommand.Parameters.AddWithValue("@Operation", "Save");
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", objShortLeaveRequestModel.ShortLeaveID);
                sqlCommand.Parameters.AddWithValue("@UserId", objShortLeaveRequestModel.UserId);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.InsertedRowId = Convert.ToInt32(OperationId.Value);
                op.Success = op.InsertedRowId > 0;
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave request record saved successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while inserting short leave record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while inserting short leave record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public OperationDetails EditShortLeave(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveRequestCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LeaveDate", DateTime.ParseExact(objShortLeaveRequestModel.LeaveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@LeaveTime", objShortLeaveRequestModel.LeaveTime);
                sqlCommand.Parameters.AddWithValue("@ReturnTime", objShortLeaveRequestModel.ReturnTime);
                sqlCommand.Parameters.AddWithValue("@ActualLeaveTime", objShortLeaveRequestModel.ActualLeaveTime);
                sqlCommand.Parameters.AddWithValue("@ActualReturnTime", objShortLeaveRequestModel.ActualReturnTime);
                sqlCommand.Parameters.AddWithValue("@LateMinutes", objShortLeaveRequestModel.LateMinutes);
                sqlCommand.Parameters.AddWithValue("@EarlyMinutes", objShortLeaveRequestModel.EarlyMinutes);
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", objShortLeaveRequestModel.ShortLeaveID);
                sqlCommand.Parameters.AddWithValue("@ReturnNextDay", objShortLeaveRequestModel.ReturnNextDay);
                sqlCommand.Parameters.AddWithValue("@Comments", objShortLeaveRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@ApprovalStatus", objShortLeaveRequestModel.ApprovalStatus);
                sqlCommand.Parameters.AddWithValue("@ReviewNote", objShortLeaveRequestModel.ReviewNote);
                sqlCommand.Parameters.AddWithValue("@UserId", objShortLeaveRequestModel.UserId);
                sqlCommand.Parameters.AddWithValue("@Operation", "Edit");
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave request record updated successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while updating short leave request record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while updating short leave request record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public OperationDetails DeleteShortLeaveRequest(int id, int userId)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveRequestCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Operation", "Delete");
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", id);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave request record deleted successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while deleting short leave request record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while deleting short leave request record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public OperationDetails ReviewShortLeaveRequest(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveRequestCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", objShortLeaveRequestModel.ShortLeaveID);
                sqlCommand.Parameters.AddWithValue("@ApprovalStatus", objShortLeaveRequestModel.ApprovalStatus.ToString());
                sqlCommand.Parameters.AddWithValue("@ReviewNote", objShortLeaveRequestModel.ReviewNote);
                sqlCommand.Parameters.AddWithValue("@Operation", "Review");
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    if (objShortLeaveRequestModel.ApprovalStatus == ApprovalStatus.Approved.ToString())
                        op.Message = "Short leave request approved successfully.";
                    else
                        op.Message = "Short leave request rejected successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while updating short leave request record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while updating short leave request record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public DataSet ShortLeaveExport(int UserID, int empid = 0, string startDate = "", string endDate = "")
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_STP_GetShortLeavePaging", sqlConnection);
            da.SelectCommand.Parameters.AddWithValue("@UserId", UserID);
            if (!string.IsNullOrEmpty(startDate))
            {
                da.SelectCommand.Parameters.AddWithValue("@fromDate", CommonDB.SetCulturedDate(startDate));
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                da.SelectCommand.Parameters.AddWithValue("@toDate", CommonDB.SetCulturedDate(endDate));
            }
            if (empid != 0)
            {
                da.SelectCommand.Parameters.AddWithValue("@empid", empid);
            }
            else
            {
                da.SelectCommand.Parameters.AddWithValue("@empid", null);
            }
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            da.Fill(ds);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(1);
            ds.Tables[0].Columns["EmployeeID"].ColumnName = "Emp ID";
            ds.Tables[0].Columns["EmployeeName"].ColumnName = "Employee Name";
            ds.Tables[0].Columns["LeaveDate"].ColumnName = "Leave Date";
            ds.Tables[0].Columns["LeaveTime"].ColumnName = "Leave Time";
            ds.Tables[0].Columns["ReturnTime"].ColumnName = "Return Time";
            ds.Tables[0].Columns["ActualLeaveTime"].ColumnName = "Actual Leave Time";
            ds.Tables[0].Columns["ActualReturnTime"].ColumnName = "Actual Return Time";
            ds.Tables[0].Columns["LateMinutes"].ColumnName = "Late Minutes";
            ds.Tables[0].Columns["EarlyMinutes"].ColumnName = "Early Minutes";
            return ds;

        }
        public ShortLeaveSettingsModel GetShortLeaveSettings()
        {
            ShortLeaveSettingsModel shortLeavesettingModel = new ShortLeaveSettingsModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        shortLeavesettingModel.ShortLeaveSettingId = Convert.ToInt32(sqlDataReader["ShortLeaveSettingId"].ToString());
                        string ApproverId = string.IsNullOrEmpty(sqlDataReader["ApproverMasterId"].ToString()) ? "0" : sqlDataReader["ApproverMasterId"].ToString();
                        shortLeavesettingModel.ApproverMasterId = Convert.ToInt32(ApproverId);
                        shortLeavesettingModel.EmployeeIdsToNotifyUponApproval = sqlDataReader["EmployeeIdsToNotifyUponApproval"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return shortLeavesettingModel;
        }
        public List<ShortLeaveApproverMaster> GetShortLeaveApproverMasterDetails()
        {
            List<ShortLeaveApproverMaster> ShortLeaveRequestModelList = null;
            try
            {
                ShortLeaveRequestModelList = new List<ShortLeaveApproverMaster>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveApproverMasterDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ShortLeaveApproverMaster ShortLeaveRequestModel;
                    while (sqlDataReader.Read())
                    {
                        ShortLeaveRequestModel = new ShortLeaveApproverMaster();
                        ShortLeaveRequestModel.ShortLeaveApproverMasterId = Convert.ToInt32(sqlDataReader["ShortLeaveApproverMasterId"].ToString());
                        ShortLeaveRequestModel.ApproverType = sqlDataReader["ApproverType"].ToString();
                        ShortLeaveRequestModel.MappingTableName = sqlDataReader["MappingTableName"].ToString();
                        ShortLeaveRequestModel.MappingColumnName = sqlDataReader["MappingColumnName"].ToString();
                        ShortLeaveRequestModelList.Add(ShortLeaveRequestModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ShortLeaveRequestModelList;
        }
        public OperationDetails AddEditShortLeaveSettings(ShortLeaveSettingsModel shortLeaveSettingsModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddEditShortLeaveSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShortLeaveSettingId", shortLeaveSettingsModel.ShortLeaveSettingId);
                if (shortLeaveSettingsModel.ApproverMasterId != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@ApproverMasterId", shortLeaveSettingsModel.ApproverMasterId);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@ApproverMasterId", null);
                }
                sqlCommand.Parameters.AddWithValue("@EmployeeIdsToNotifyUponApproval", shortLeaveSettingsModel.EmployeeIdsToNotifyUponApproval);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.InsertedRowId = Convert.ToInt32(OperationId.Value);
                op.Success = op.InsertedRowId > 0;
                if (op.Success)
                {
                    op.InsertedRowId = shortLeaveSettingsModel.ShortLeaveSettingId == 0 ? op.InsertedRowId : 0;
                    op.CssClass = "success";
                    op.Message = "Short leave settings saved successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while inserting short leave setting.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while inserting short leave setting.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
        public OperationDetails UpdateShortLeaveActualTimes(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_UpdateShortLeaveRequestActualTimes", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", objShortLeaveRequestModel.ShortLeaveID);
                sqlCommand.Parameters.AddWithValue("@ActualLeaveTime", objShortLeaveRequestModel.ActualLeaveTime);
                sqlCommand.Parameters.AddWithValue("@ActualReturnTime", objShortLeaveRequestModel.ActualReturnTime);
                sqlCommand.Parameters.AddWithValue("@LateMinutes", objShortLeaveRequestModel.LateMinutes);
                sqlCommand.Parameters.AddWithValue("@EarlyMinutes", objShortLeaveRequestModel.EarlyMinutes);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave request updated successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while updating short leave record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while updating short leave record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public int GetShortLeaveApproverEmployeeId(int requesterEmployeeId)
        {
            int approverId = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveApproverEmployeeId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", requesterEmployeeId);
                object id = sqlCommand.ExecuteScalar();
                if (!id.Equals(DBNull.Value))
                    approverId = Convert.ToInt32(id);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return approverId;
        }
        public List<string> GetEmployeePunches(int userId, string shortLeaveDate)
        {
            List<string> employeePunchesList = null;
            try
            {
                employeePunchesList = new List<string>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetEmployeePunches", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                if (!string.IsNullOrEmpty(shortLeaveDate))
                    sqlCommand.Parameters.AddWithValue("@ShortLeaveDate", DateTime.ParseExact(shortLeaveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                else
                    sqlCommand.Parameters.AddWithValue("@ShortLeaveDate", DBNull.Value);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        employeePunchesList.Add(sqlDataReader["CheckTime"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return employeePunchesList;
        }
        public EmailSettings GetShortLeaveRequestEmailBody(string templateCode, int shortLeaveRequestId, int approverId)
        {
            EmailSettings emailSettings = new EmailSettings();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveEmailBody", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TemplateCode", templateCode);
                sqlCommand.Parameters.AddWithValue("@ShortLeaveRequestId", shortLeaveRequestId);
                sqlCommand.Parameters.AddWithValue("@ApproverId", approverId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        emailSettings.EmailSubject = sqlDataReader["EmailSubject"].ToString();
                        emailSettings.EmailBody = sqlDataReader["EmailBody"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return emailSettings;
        }
        public List<Employee> GetShortLeaveEmailList(string employeeId)
        {
            List<Employee> employeeEmailList = null;
            try
            {
                employeeEmailList = new List<Employee>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveEmailList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee objEmployee;
                    while (sqlDataReader.Read())
                    {
                        objEmployee = new Employee();
                        objEmployee.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        objEmployee.EmailId = sqlDataReader["EmailId"].ToString();
                        employeeEmailList.Add(objEmployee);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return employeeEmailList;
        }
        public int GetShortLeaveRequestsCountForApprover(int userId)
        {
            int count = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeaveRequestForApprover", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                object id = sqlCommand.ExecuteScalar();
                if (id != null)
                    count = Convert.ToInt32(id);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return count;
        }
        public int GetPendingShortLeaveRequestCountForHR()
        {
            int count = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetPendingShortLeaveRequestCountForHR", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                object id = sqlCommand.ExecuteScalar();
                if (id != null)
                    count = Convert.ToInt32(id);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return count;
        }
    }
}
