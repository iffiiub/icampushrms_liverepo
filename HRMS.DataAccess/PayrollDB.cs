﻿using HRMS.Entities;
using HRMS.Entities.General;
using HRMS.Entities.ViewModel;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class PayrollDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        GeneralDB.DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PayrollModel> GetAllPayRollList(PayrollModel payrollModel)
        {
            List<PayrollModel> payrollList = null;
            try
            {
                payrollList = new List<PayrollModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetRunPayrollData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntCategoryID", payrollModel.CategoryID);
                sqlCommand.Parameters.AddWithValue("@aNvrSearchName", payrollModel.EmployeeName);
                sqlCommand.Parameters.AddWithValue("@aBitActive", payrollModel.isActive);
                sqlCommand.Parameters.AddWithValue("@aBitPayActive", payrollModel.isPayActive);


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayrollModel objpayrollModel;
                    while (sqlDataReader.Read())
                    {
                        objpayrollModel = new PayrollModel();
                        objpayrollModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        objpayrollModel.isPayActive = Convert.ToBoolean(sqlDataReader["isPayActive"]);
                        objpayrollModel.CategoryName_1 = Convert.ToString(sqlDataReader["CategoryName_1"]);
                        objpayrollModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        objpayrollModel.CategoryID = Convert.ToInt32(sqlDataReader["CategoryID"]);
                        objpayrollModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        payrollList.Add(objpayrollModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payrollList;
        }

        public OperationDetails UpdateEmployeeDetailPayActive(PayrollModel payrollModel)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspUpdateEmployeeDetailPayActive", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", payrollModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aBitIsPayActive", payrollModel.isPayActive);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public List<PayCycle> getPaycycles(PayrollModel payrollModel)
        {
            List<PayCycle> payCycleList = null;
            try
            {
                payCycleList = new List<PayCycle>();
                payrollModel.isPayActive = true;


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetCycles", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", payrollModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aBitIsPayActive", payrollModel.active);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayCycle payCycle;
                    while (sqlDataReader.Read())
                    {
                        payCycle = new PayCycle();
                        payCycle.PayCycleID = Convert.ToInt32(sqlDataReader["PayCycleID"]);
                        payCycle.PayCycleName_1 = Convert.ToString(sqlDataReader["PayCycleName_1"]);
                        payCycle.PayCycleName_2 = Convert.ToString(sqlDataReader["PayCycleName_2"]);
                        payCycle.PayCycleName_3 = Convert.ToString(sqlDataReader["PayCycleName_3"]);
                        payCycle.fromaDate = Convert.ToString(sqlDataReader["DateFrom"]);
                        payCycle.toDate = Convert.ToString(sqlDataReader["DateTo"]);
                        payCycle.active = Convert.ToBoolean(sqlDataReader["active"]);
                        payCycle.acyear = Convert.ToString(sqlDataReader["acyear"]);
                        payCycleList.Add(payCycle);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payCycleList;
        }

        public OperationDetails RunPayrollByDepartment(PayrollSettings payrollSettings, int employeeId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspRunPayrollByDepartment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aSntCycleID", payrollSettings.PayCycleID);
                sqlCommand.Parameters.AddWithValue("@aIntDays", payrollSettings.NoOfDays);
                sqlCommand.Parameters.AddWithValue("@aBitFinal", payrollSettings.RunFinal);
                sqlCommand.Parameters.AddWithValue("@aBitIsVacation", payrollSettings.VacationMonth);
                sqlCommand.Parameters.AddWithValue("@aSntDepartment", payrollSettings.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutMessage";
                param.DbType = DbType.String;
                param.Direction = ParameterDirection.Output;
                param.Size = 400;
                sqlCommand.Parameters.Add(param);

                sqlCommand.ExecuteReader();
                Message = sqlCommand.Parameters["@OutMessage"].Value == DBNull.Value || sqlCommand.Parameters["@OutMessage"].Value == "" ? "success" : sqlCommand.Parameters["@OutMessage"].Value.ToString();
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public OperationDetails RunPayrollByBank(PayrollSettings payrollSettings, int employeeId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspRunPayrollByBank", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aSntCycleID", payrollSettings.PayCycleID);
                sqlCommand.Parameters.AddWithValue("@aIntDays", payrollSettings.NoOfDays);
                sqlCommand.Parameters.AddWithValue("@aBitFinal", payrollSettings.RunFinal);
                sqlCommand.Parameters.AddWithValue("@aBitIsVacation", payrollSettings.VacationMonth);
                sqlCommand.Parameters.AddWithValue("@aSntBankID", payrollSettings.BankID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutMessage";
                param.DbType = DbType.String;
                param.Direction = ParameterDirection.Output;
                param.Size = 400;
                sqlCommand.Parameters.Add(param);

                sqlCommand.ExecuteReader();
                Message = sqlCommand.Parameters["@OutMessage"].Value == DBNull.Value || sqlCommand.Parameters["@OutMessage"].Value == "" ? "success" : sqlCommand.Parameters["@OutMessage"].Value.ToString();
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public OperationDetails RunPayrollByDepartmentAndBank(PayrollSettings payrollSettings, int employeeId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspRunPayrollByDepartmentAndBank", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aSntCycleID", payrollSettings.PayCycleID);
                sqlCommand.Parameters.AddWithValue("@aIntDays", payrollSettings.NoOfDays);
                sqlCommand.Parameters.AddWithValue("@aBitFinal", payrollSettings.RunFinal);
                sqlCommand.Parameters.AddWithValue("@aBitIsVacation", payrollSettings.VacationMonth);
                sqlCommand.Parameters.AddWithValue("@aSntBankID", payrollSettings.BankID);
                sqlCommand.Parameters.AddWithValue("@aSntDepartment", payrollSettings.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutMessage";
                param.DbType = DbType.String;
                param.Direction = ParameterDirection.Output;
                param.Size = 400;
                sqlCommand.Parameters.Add(param);

                sqlCommand.ExecuteReader();
                Message = sqlCommand.Parameters["@OutMessage"].Value == DBNull.Value || sqlCommand.Parameters["@OutMessage"].Value == "" ? "success" : sqlCommand.Parameters["@OutMessage"].Value.ToString();
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public OperationDetails RunPayroll(PayrollSettings payrollSettings, int employeeId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspRunPayroll", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntCycleID", payrollSettings.PayCycleID);
                sqlCommand.Parameters.AddWithValue("@aIntDays", payrollSettings.NoOfDays);
                sqlCommand.Parameters.AddWithValue("@aBitFinal", payrollSettings.RunFinal);
                sqlCommand.Parameters.AddWithValue("@aBitIsVacation", payrollSettings.VacationMonth);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutMessage";
                param.DbType = DbType.String;
                param.Direction = ParameterDirection.Output;
                param.Size = 400;
                sqlCommand.Parameters.Add(param);

                sqlCommand.ExecuteReader();
                Message = sqlCommand.Parameters["@OutMessage"].Value == DBNull.Value || sqlCommand.Parameters["@OutMessage"].Value == "" ? "success" : sqlCommand.Parameters["@OutMessage"].Value.ToString();
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public DataTable getBankListReport(string cycleid)
        {
            DataTable dt = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspBankListReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);
                dt.Load(sqlCommand.ExecuteReader());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dt;
        }

        public List<PaySlipReport> GetPaySlipReport(string cycleid, int transId, string employeeid)
        {
            List<PaySlipReport> paySlipReportList = null;
            try
            {
                paySlipReportList = new List<PaySlipReport>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayslipData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);
                sqlCommand.Parameters.AddWithValue("@aTntTrans", transId);
                sqlCommand.Parameters.AddWithValue("@aIntWhereID", employeeid);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySlipReport paySlipReport;
                    while (sqlDataReader.Read())
                    {
                        paySlipReport = new PaySlipReport();
                        paySlipReport.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        paySlipReport.amount = Convert.ToDouble(sqlDataReader["amount"]);
                        paySlipReport.ItemDescr = Convert.ToString(sqlDataReader["ItemDescr"]);
                        paySlipReport.type = Convert.ToString(sqlDataReader["type"]);
                        paySlipReport.AccountNumber = Convert.ToString(sqlDataReader["AccountNumber"]);
                        paySlipReport.Name = Convert.ToString(sqlDataReader["Name"]);
                        paySlipReport.Bank = Convert.ToString(sqlDataReader["Bank"]);
                        paySlipReport.Department = Convert.ToString(sqlDataReader["Department"]);
                        paySlipReportList.Add(paySlipReport);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySlipReportList;
        }

        public List<PaySlipSummary> GetPaySlipSummary(string cycleid, int transId, string employeeid)
        {
            List<PaySlipSummary> paySlipSummaryList = null;
            try
            {
                paySlipSummaryList = new List<PaySlipSummary>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayslipSummary", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);
                sqlCommand.Parameters.AddWithValue("@aTntTrans", transId);
                sqlCommand.Parameters.AddWithValue("@aIntWhereID", employeeid);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySlipSummary paySlipSummary;
                    while (sqlDataReader.Read())
                    {
                        paySlipSummary = new PaySlipSummary();
                        paySlipSummary.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        paySlipSummary.additionTotal = Convert.ToDouble(sqlDataReader["additionTotal"]);
                        paySlipSummary.salaryTotal = Convert.ToDouble(sqlDataReader["salaryTotal"]);
                        paySlipSummary.deductionTotal = Convert.ToDouble(sqlDataReader["deductionTotal"]);
                        paySlipSummary.cycle = Convert.ToInt32(sqlDataReader["cycle"]);
                        paySlipSummary.net = Convert.ToDouble(sqlDataReader["net"]);
                        paySlipSummary.cycleName = Convert.ToString(sqlDataReader["cycleName"]);
                        paySlipSummary.DateFrom = Convert.ToString(sqlDataReader["DateFrom"]);
                        paySlipSummary.DateTo = Convert.ToString(sqlDataReader["DateTo"]);
                        paySlipSummary.paydate = Convert.ToString(sqlDataReader["paydate"]);
                        paySlipSummary.schoolname = Convert.ToString(sqlDataReader["schoolname"]);
                        paySlipSummaryList.Add(paySlipSummary);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySlipSummaryList;
        }

        //public OperationDetails GetReport(int cycleid, int bankid)
        //{
        //    string Message = "";
        //    int id = 0;
        //    try
        //    {
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("HR_uspGenBankPayListData", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
        //        //{
        //        sqlCommand.Parameters.AddWithValue("@aVchyCycleList", cycleid);
        //        sqlCommand.Parameters.AddWithValue("@aIntBankID", bankid);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {
        //            PaySlipSummary paySlipSummary;
        //            while (sqlDataReader.Read())
        //            {


        //            }
        //        }
        //        Message = "success";
        //        sqlConnection.Close();
        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return new OperationDetails(true, Message, null, id);
        //}

        public DataTable GetReport(string cycleid, int? bankid, int? categoryId, int? departmentId, out bool HeaderRepeated, out string HeaderColumnNames)
        {
            var payrollTempateSetting = GetPayrollTemplateSetting().FirstOrDefault(x => x.IsActive == true);
            string SpName = "";
            SpName = payrollTempateSetting.SPName;
            DataTable dtreport = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(SpName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aVchyCycleList", cycleid);
                sqlCommand.Parameters.AddWithValue("@aIntCategoryId", categoryId);
                sqlCommand.Parameters.AddWithValue("@aIntBankID", bankid);
                sqlCommand.Parameters.AddWithValue("@aIntDepartmentId", departmentId);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@HeaderRepeated";
                param.DbType = DbType.Int32;
                param.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(param);

                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@HeaderColumns";
                param1.DbType = DbType.String;
                param1.Size = 500;
                param1.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(param1);

                dtreport.Load(sqlCommand.ExecuteReader());
                HeaderRepeated = Convert.ToBoolean(sqlCommand.Parameters["@HeaderRepeated"].Value);
                if (!(sqlCommand.Parameters["@HeaderColumns"].Value == null))
                {
                    if (sqlCommand.Parameters["@HeaderColumns"].Value.ToString() == "")
                        HeaderColumnNames = string.Empty;
                    else
                        HeaderColumnNames = sqlCommand.Parameters["@HeaderColumns"].Value.ToString();
                }
                else
                {
                    HeaderColumnNames = string.Empty;
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dtreport;

        }

        public List<PayCategoriesModel> GetTeacherCategoriesList(string cycleList)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aIntCycleID", cycleList)
                    };
            // Create a list to hold the Absent		
            List<PayCategoriesModel> teacherCategoriesList = new List<PayCategoriesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetTeacherDetailsByPayCycle", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PayCategoriesModel payCategoriesModel;
                        while (reader.Read())
                        {
                            payCategoriesModel = new PayCategoriesModel();
                            payCategoriesModel.CategoryID = Convert.ToInt32(reader["CategoryID"].ToString());
                            payCategoriesModel.CategoryName_1 = Convert.ToString(reader["CategoryName_1"].ToString());
                            teacherCategoriesList.Add(payCategoriesModel);
                        }
                    }
                }
                return teacherCategoriesList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PaySlipSummary> GetTeacherEmployeeList(string cycleList)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aIntCycleID", cycleList)
                    };
            // Create a list to hold the Absent		
            List<PaySlipSummary> teacherEmployeeList = new List<PaySlipSummary>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetTeacherCategoryByPayCycle", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PaySlipSummary paySlipSummary;
                        while (reader.Read())
                        {
                            paySlipSummary = new PaySlipSummary();
                            paySlipSummary.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            paySlipSummary.TeacherName = Convert.ToString(reader["TeacherName"].ToString());
                            teacherEmployeeList.Add(paySlipSummary);
                        }
                    }
                }
                return teacherEmployeeList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<BankListReport> getEmailListReport(int cycleid)
        {
            List<BankListReport> bankListReportList = null;
            try
            {
                bankListReportList = new List<BankListReport>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspBankListReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    BankListReport bankListReport;
                    while (sqlDataReader.Read())
                    {
                        bankListReport = new BankListReport();
                        bankListReport.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        bankListReport.Amount = Convert.ToDouble(sqlDataReader["Amount"]);
                        bankListReport.WorkEmail = Convert.ToString(sqlDataReader["WorkEmail"]);
                        bankListReport.PersonalEmail = Convert.ToString(sqlDataReader["PersonalEmail"]);
                        bankListReport.AccountNumber = Convert.ToString(sqlDataReader["AccountNumber"]);
                        bankListReport.EmpName = Convert.ToString(sqlDataReader["EmpName"]);
                        bankListReport.BankName = Convert.ToString(sqlDataReader["BANKNAME"]);
                        bankListReport.Category = Convert.ToString(sqlDataReader["Category"]);
                        bankListReport.PayCycleName = Convert.ToString(sqlDataReader["PayCycleName_1"]);
                        bankListReportList.Add(bankListReport);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return bankListReportList;
        }

        public List<PaySlipSummary> GetTeacherMailList(string cycleIds)
        {
            List<PaySlipSummary> paySlipSummaryList = null;
            try
            {
                paySlipSummaryList = new List<PaySlipSummary>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetTeacherMailListByStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntCycleID", cycleIds);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySlipSummary paySlipSummary;
                    while (sqlDataReader.Read())
                    {
                        paySlipSummary = new PaySlipSummary();
                        paySlipSummary.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        paySlipSummary.TeacherName = Convert.ToString(sqlDataReader["TeacherName"]);
                        paySlipSummary.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        paySlipSummaryList.Add(paySlipSummary);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySlipSummaryList;
        }

        public DataSet GetPaySlipData_DataSet(string cycleid, int transId, string employeeid)
        {
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("HR_uspGetPayslipData", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);
                da.SelectCommand.Parameters.AddWithValue("@aTntTrans", transId);
                da.SelectCommand.Parameters.AddWithValue("@aIntWhereID", employeeid);
                da.Fill(ds);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public DataSet GetPaySlipSummary_DataSet(string cycleid, int transId, string employeeid)
        {
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("HR_uspGetPayslipSummary", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);
                da.SelectCommand.Parameters.AddWithValue("@aTntTrans", transId);
                da.SelectCommand.Parameters.AddWithValue("@aIntWhereID", employeeid);
                da.Fill(ds);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public DataSet GetTeachers_DataSet(string cycleid, int transId, string employeeid)
        {
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("HR_uspGetTeacherByPayrollCycle", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@aIntCycleID", cycleid);
                da.SelectCommand.Parameters.AddWithValue("@aTntTrans", transId);
                da.SelectCommand.Parameters.AddWithValue("@aIntWhereID", employeeid);
                da.Fill(ds);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public List<EmployeeDetailsModel> GetEmployeeNotinPayroll()
        {
            List<EmployeeDetailsModel> employeeList = new List<EmployeeDetailsModel>();
            EmployeeDetailsModel iEmployeeDetail;
            dataHelper = new GeneralDB.DataHelper();
            try
            {

                List<SqlParameter> parameterList = new List<SqlParameter>();
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SelectEmployeeNotInPayroll");
                foreach (DataRow dr in dt.Rows)
                {
                    iEmployeeDetail = new EmployeeDetailsModel();
                    iEmployeeDetail.FirstName_1 = dr["FirstName_1"].ToString();
                    iEmployeeDetail.MiddleName_1 = dr["MiddleName_1"].ToString();
                    iEmployeeDetail.SurName_1 = dr["SurName_1"].ToString();
                    iEmployeeDetail.EmployeeID = Convert.ToInt32(dr["EmployeeID"].ToString());
                    iEmployeeDetail.FullName = dr["FirstName_1"].ToString() + " " + dr["SurName_1"].ToString();
                    employeeList.Add(iEmployeeDetail);
                }
            }
            catch (Exception e)
            {
                employeeList = new List<EmployeeDetailsModel>();
            }
            return employeeList;
        }

        public int GetDigitAfterDecimal()
        {
            int DigitAfterDecimal = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select DigitAfterDecimal from HR_PayRollSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {

                        DigitAfterDecimal = Convert.ToInt32(sqlDataReader["DigitAfterDecimal"].ToString());

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DigitAfterDecimal;
        }

        public int GetNoOfDaysInCycle()
        {
            int NoOfDaysInCycle = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select NumberOfCycleDaysInMonth from HR_PayRollSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        NoOfDaysInCycle = Convert.ToInt32(sqlDataReader["NumberOfCycleDaysInMonth"] == DBNull.Value || Convert.ToString(sqlDataReader["NumberOfCycleDaysInMonth"]) == "" ? 30 : Convert.ToInt32(sqlDataReader["NumberOfCycleDaysInMonth"].ToString()));
                        //NoOfDaysInCycle if in setting it is Null or empty then default will be 30 if it 0 then it will check for no of days in that cycle as pe effective date

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return NoOfDaysInCycle;
        }

        public int GetNoOfDaysInCycleForDate(DateTime aDate)
        {
            int NoOfDaysInCycle = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select dbo.HR_FN_CalculatePayrollDay(@aDate) as NoOFDays", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@aDate", aDate);
                //SqlParameter NoOfDays = new SqlParameter("@aNoofDaysInCycle", SqlDbType.Int)
                //{
                //    Direction = ParameterDirection.Output
                //};
                //sqlCommand.Parameters.Add(NoOfDays);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        NoOfDaysInCycle = Convert.ToInt32(sqlDataReader["NoOFDays"] == DBNull.Value || sqlDataReader["NoOFDays"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["NoOFDays"].ToString()));
                        //NoOfDaysInCycle if it is 0 that means Cycle is not genrated for that perticular month
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return NoOfDaysInCycle;
        }

        public string GetAmountFormat()
        {
            int DigitAfterDecimal = 0;
            string AmountFormat = "";
            try
            {
                DigitAfterDecimal = GetDigitAfterDecimal();

                if (DigitAfterDecimal == 0)
                {
                    AmountFormat = "0";
                }
                else if (DigitAfterDecimal == 1)
                {
                    AmountFormat = "0.0";
                }
                else if (DigitAfterDecimal == 2)
                {
                    AmountFormat = "0.00";
                }
                else if (DigitAfterDecimal == 3)
                {
                    AmountFormat = "0.000";
                }
            }
            catch (Exception ex)
            {
                AmountFormat = "0.00";
            }
            return AmountFormat;
        }

        public int GetPayslipTemplateID()
        {
            int templateID = 1;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select PaySlipTemplateID from HR_PayRollSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {

                        templateID = Convert.ToInt32(sqlDataReader["PaySlipTemplateID"].ToString());

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                templateID = 1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return templateID;
        }

        public OperationDetails UpdateJvsSetting(bool isRunWithJvs)
        {
            OperationDetails op = new OperationDetails();
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("update HR_PayRollSettings set RunFinalPayrollWithJvs=@isRunWithJvs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                sqlCommand.Parameters.AddWithValue("@isRunWithJvs", isRunWithJvs);
                sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = true;
                op.Message = "JVs setting updated successfully";
                op.CssClass = "success";
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while updating JVs setting";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public bool GetJvsSettingFromPayroll()
        {
            bool isRunWithJvs = false;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select RunFinalPayrollWithJvs from HR_PayRollSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        isRunWithJvs = Convert.ToBoolean(sqlDataReader["RunFinalPayrollWithJvs"] == DBNull.Value ? "false" : sqlDataReader["RunFinalPayrollWithJvs"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return isRunWithJvs;
        }

        public bool UpdatePayrollTemplateSetting(int templateSettingId)
        {
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@Mode", 2));
            sqlParmeterList.Add(new SqlParameter("@Id", templateSettingId));
            return dataHelper.ExcutestoredProcedure(sqlParmeterList, "Hr_Stp_CURD_HR_PayRollTemplateSettings");
        }

        public List<PayrollTemplateSetting> GetPayrollTemplateSetting()
        {
            dataHelper = new GeneralDB.DataHelper();
            List<PayrollTemplateSetting> listtPayrollTemplateSetting = new List<PayrollTemplateSetting>();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@Mode", 1));
            sqlParmeterList.Add(new SqlParameter("@Id", 0));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParmeterList, "Hr_Stp_CURD_HR_PayRollTemplateSettings");
            PayrollTemplateSetting payrollTemplateSetting;
            foreach (DataRow Dr in dt.Rows)
            {
                payrollTemplateSetting = new PayrollTemplateSetting();
                payrollTemplateSetting.PayRollTemplateSettingsID = Convert.ToInt32(Dr["PayRollTemplateSettingsID"]);
                payrollTemplateSetting.SPName = Convert.ToString(Dr["SPName"]);
                payrollTemplateSetting.DisplayName = Convert.ToString(Dr["DisplayName"]);
                payrollTemplateSetting.IsActive = Convert.ToBoolean(Dr["IsActive"]);
                payrollTemplateSetting.IsPriority = Convert.ToBoolean(Dr["IsPriority"]);
                listtPayrollTemplateSetting.Add(payrollTemplateSetting);
            }
            return listtPayrollTemplateSetting;
        }
        public bool UpdateJvsTemplateSetting(int templateSettingId, string AccountCode)
        {
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@Mode", 2));
            sqlParmeterList.Add(new SqlParameter("@Id", templateSettingId));
            sqlParmeterList.Add(new SqlParameter("@AccountCode", AccountCode));
            return dataHelper.ExcutestoredProcedure(sqlParmeterList, "Hr_Stp_CURD_HR_JVsTemplateSettings");
        }

        public List<JvsTemplateSetting> GetJvsTemplateSetting()
        {
            dataHelper = new GeneralDB.DataHelper();
            List<JvsTemplateSetting> listJvsTemplateSetting = new List<JvsTemplateSetting>();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@Mode", 1));
            sqlParmeterList.Add(new SqlParameter("@Id", 0));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParmeterList, "Hr_Stp_CURD_HR_JVsTemplateSettings");
            JvsTemplateSetting jvsTemplateSetting;
            foreach (DataRow Dr in dt.Rows)
            {
                jvsTemplateSetting = new JvsTemplateSetting();
                jvsTemplateSetting.JvsTemplateSettingsID = Convert.ToInt32(Dr["JvsTemplateSettingsID"]);
                jvsTemplateSetting.SPName = Convert.ToString(Dr["SPName"]);
                jvsTemplateSetting.DisplayName = Convert.ToString(Dr["DisplayName"]);
                jvsTemplateSetting.IsActive = Convert.ToBoolean(Dr["IsActive"]);
                jvsTemplateSetting.AccountCode = Convert.ToString(Dr["SalaryPayableAccountCode"]);
                jvsTemplateSetting.EnableCostCenters = Convert.ToBoolean(Dr["EnableCostCenters"].ToString() == "" ? "false" : Dr["EnableCostCenters"].ToString());
                listJvsTemplateSetting.Add(jvsTemplateSetting);
            }
            return listJvsTemplateSetting;
        }

        public bool GetCostCenterValidation(out int ActiveCount)
        {
            bool isCostCenterValidated = false;
            ActiveCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetCostCenterValidation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter IsValidated = new SqlParameter("@IsValidated", SqlDbType.Bit);
                IsValidated.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsValidated);
                SqlParameter ActiveCCCount = new SqlParameter("@ActiveCCCount", SqlDbType.Int);
                ActiveCCCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(ActiveCCCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                isCostCenterValidated = Convert.ToBoolean(IsValidated.Value);
                ActiveCount = Convert.ToInt32(ActiveCCCount.Value);

            }
            catch (Exception ex)
            {

            }
            return isCostCenterValidated;
        }

        public List<CostCenter> GetCostCenterDetails()
        {
            dataHelper = new GeneralDB.DataHelper();
            List<CostCenter> lstCostCenters = new List<CostCenter>();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParmeterList, "HR_Stp_GetCostCenterDetails");
            CostCenter objCostCenter;
            foreach (DataRow Dr in dt.Rows)
            {
                objCostCenter = new CostCenter();
                objCostCenter.DimensionValueID = Convert.ToInt32(Dr["DimensionValueID"]);
                objCostCenter.DimensionName = Convert.ToString(Dr["DimensionName"]);
                objCostCenter.DimensionValueName = Convert.ToString(Dr["DimensionValueName"]);
                lstCostCenters.Add(objCostCenter);
            }
            return lstCostCenters;
        }

        public List<CostCenter> GetPayCategoryCostCenterDetails(int PayCategoryId)
        {
            dataHelper = new GeneralDB.DataHelper();
            List<CostCenter> lstCostCenters = new List<CostCenter>();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@payCategoryId", PayCategoryId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParmeterList, "HR_Stp_GetPayCategoryCostCenterDetails");
            CostCenter objCostCenter;
            foreach (DataRow Dr in dt.Rows)
            {
                objCostCenter = new CostCenter();
                objCostCenter.DimensionValueID = Convert.ToInt32(Dr["DimensionValueID"]);
                objCostCenter.DimensionName = Convert.ToString(Dr["DimensionName"]);
                objCostCenter.DimensionValueName = Convert.ToString(Dr["DimensionValueName"]);
                objCostCenter.PayCategoryID = Convert.ToInt32(Dr["CategoryID"]);
                objCostCenter.Percentage = Convert.ToDecimal(Dr["Percentage"]);
                lstCostCenters.Add(objCostCenter);
            }
            return lstCostCenters;
        }


        public OperationDetails UpdatePayCategoryDetails(PayCategoriesModel objPayCategory,int Mode,DataTable dt)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CRUDPayCategory", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);
                sqlCommand.Parameters.AddWithValue("@CategoryID", objPayCategory.CategoryID);
                sqlCommand.Parameters.AddWithValue("@CategoryName_1", objPayCategory.CategoryName_1);
                sqlCommand.Parameters.AddWithValue("@CategoryName_2", objPayCategory.CategoryName_2);
                sqlCommand.Parameters.AddWithValue("@CategoryName_3", objPayCategory.CategoryName_3);
                sqlCommand.Parameters.AddWithValue("@PayCategorySeq", objPayCategory.seq);
                sqlCommand.Parameters.AddWithValue("@Hr_PayCategoryCostCenterDetails", dt);
                SqlParameter IsSuccess = new SqlParameter("@isSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                sqlCommand.ExecuteReader();
                op.Success = Convert.ToBoolean(IsSuccess.Value);  
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public List<EmployeeInfo> GetPaySlipEmployeeInfo()
        {
            List<EmployeeInfo> employeeList = new List<EmployeeInfo>();
            EmployeeInfo iEmployeeDetail;
            dataHelper = new GeneralDB.DataHelper();
            try
            {

                List<SqlParameter> parameterList = new List<SqlParameter>();
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_stp_GetEmployeeSalarySlipDetails");
                foreach (DataRow dr in dt.Rows)
                {
                    iEmployeeDetail = new EmployeeInfo();
                    iEmployeeDetail.EmployeeName = dr["FullName"].ToString();
                    iEmployeeDetail.EmployeeAlternativeID = dr["EmployeeAlterNativeID"].ToString();
                    iEmployeeDetail.EmployeeID = Convert.ToInt32(dr["EmployeeID"].ToString());
                    iEmployeeDetail.PositionTitle = dr["PositionTitle"].ToString();
                    iEmployeeDetail.EmpWorkEmail = dr["WorkEmail"].ToString();
                    iEmployeeDetail.HireDate = dr["HireDate"].ToString() == "" ? "" : Convert.ToDateTime(dr["HireDate"].ToString()).ToString("dd/MM/yyyy");
                    employeeList.Add(iEmployeeDetail);
                }
            }
            catch (Exception e)
            {
                employeeList = new List<EmployeeInfo>();
            }
            return employeeList;
        }

        public OperationDetails RetrieveFinalRun(int CycleId)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_RertrieveFinalRun", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values                
                sqlCommand.Parameters.AddWithValue("@CycleId", CycleId);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Final run retrieved successfully";
                }
                else
                {
                    op.Message = "Error while retrieving final run";
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while retrieving final run";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public PayrollGeneralSetting GetPayrollGeneralSetting()
        {
            PayrollGeneralSetting payrollGeneralSetting = new PayrollGeneralSetting();
            dataHelper = new GeneralDB.DataHelper();
            try
            {
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>(), "Hr_Stp_GetPayrollSettings");
                foreach (DataRow dr in dt.Rows)
                {
                    payrollGeneralSetting.NumberOfCycleDaysInMonth = Convert.ToInt32(dr["NumberOfCycleDaysInMonth"] == DBNull.Value ? 0 : dr["NumberOfCycleDaysInMonth"]);
                    payrollGeneralSetting.DigitAfterDecimal = Convert.ToInt32(dr["DigitAfterDecimal"] == DBNull.Value ? 0 : dr["DigitAfterDecimal"]);
                    payrollGeneralSetting.PaySlipTemplateID = Convert.ToInt32(dr["PaySlipTemplateID"] == DBNull.Value ? 0 : dr["PaySlipTemplateID"]);
                    payrollGeneralSetting.CountSalaryPerHireDate = Convert.ToBoolean(dr["CountSalaryPerHireDate"] == DBNull.Value ? false : dr["CountSalaryPerHireDate"]);
                    payrollGeneralSetting.CountSalaryForWorkDays = Convert.ToBoolean(dr["CountSalaryForWorkDays"] == DBNull.Value ? false : dr["CountSalaryForWorkDays"]);
                    payrollGeneralSetting.RunFinalPayrollWithJvs = Convert.ToBoolean(dr["RunFinalPayrollWithJvs"] == DBNull.Value ? false : dr["RunFinalPayrollWithJvs"]);
                    payrollGeneralSetting.NumberOfCycleDaysInMonth = Convert.ToInt32(dr["NumberOfCycleDaysInMonth"] == DBNull.Value ? 0 : dr["NumberOfCycleDaysInMonth"]);
                    payrollGeneralSetting.PayrollEmpFullName = Convert.ToBoolean(dr["PayrollEmpFullName"] == DBNull.Value ? false : dr["PayrollEmpFullName"]);
                    payrollGeneralSetting.IsValidationForCycle = Convert.ToBoolean(dr["IsValidationForCycle"] == DBNull.Value ? false : dr["IsValidationForCycle"]);
                    payrollGeneralSetting.IsconfirmRunPayroll = Convert.ToBoolean(dr["IsconfirmRunPayroll"] == DBNull.Value ? false : dr["IsconfirmRunPayroll"]);
                }
            }
            catch (Exception e)
            {

            }
            return payrollGeneralSetting;
        }

        public List<PayrollVerificationSetting> GetPayrollVerificationSetting()
        {
            List<PayrollVerificationSetting> lstSetting = new List<PayrollVerificationSetting>();
            PayrollVerificationSetting verificationSetting;
            dataHelper = new GeneralDB.DataHelper();
            try
            {
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>(), "Hr_Stp_GetPayrollVerificationSetting");
                foreach (DataRow dr in dt.Rows)
                {
                    verificationSetting = new PayrollVerificationSetting();
                    verificationSetting.PayrollVerificationID = Convert.ToInt32(dr["PayrollVerificationID"] == DBNull.Value ? 0 : dr["PayrollVerificationID"]);
                    verificationSetting.ModuleName = Convert.ToString(dr["ModuleTypeName"]);
                    verificationSetting.IsEnabled = Convert.ToBoolean(dr["IsActive"] == DBNull.Value ? "false" : dr["IsActive"]);
                    lstSetting.Add(verificationSetting);


                }
            }
            catch (Exception e)
            {

            }
            return lstSetting;
        }
        public List<PayrollConfirmationRequest> GetPayrollConfirmationRequestDate(string Mode, bool IsActive, bool IsConfirmed, int EmployeeId)
        {
            List<PayrollConfirmationRequest> ListPayrollConfirmationRequest = new List<PayrollConfirmationRequest>();
            PayrollConfirmationRequest PayrollConfirmationRequest;
            dataHelper = new GeneralDB.DataHelper();
            try
            {
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(
                    new List<SqlParameter>() {
                        new SqlParameter("@Mode", Mode),
                        new SqlParameter("@IsCancel", IsActive),
                        new SqlParameter("@IsConfirmed", IsConfirmed),
                        new SqlParameter("@EmployeeId",EmployeeId)
                    },
                    "Hr_Stp_GetPayrollConfirmationRequestDetails");
                foreach (DataRow dr in dt.Rows)
                {
                    PayrollConfirmationRequest = new PayrollConfirmationRequest();
                    PayrollConfirmationRequest.PayrollConfirmationId = Convert.ToInt32(dr["PayrollConfirmationId"] == DBNull.Value ? 0 : dr["PayrollConfirmationId"]);
                    PayrollConfirmationRequest.TypeID = Convert.ToInt32(dr["TempTablePkId"] == DBNull.Value ? false : dr["TempTablePkId"]);
                    PayrollConfirmationRequest.TypeName = Convert.ToString(dr["TypeName"] == DBNull.Value ? "" : dr["TypeName"]);
                    PayrollConfirmationRequest.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                    PayrollConfirmationRequest.RequestEmployeeName = Convert.ToString(dr["RequestEmployeeName"] == DBNull.Value ? "" : dr["RequestEmployeeName"]);
                    PayrollConfirmationRequest.RequestConfirmationEmployeeName = Convert.ToString(dr["RequestConfirmationEmployeeName"] == DBNull.Value ? "" : dr["RequestConfirmationEmployeeName"]);
                    PayrollConfirmationRequest.IsConfirmed = Convert.ToBoolean(dr["IsConfirmed"] == DBNull.Value ? false : dr["IsConfirmed"]);
                    PayrollConfirmationRequest.IsCancel = Convert.ToBoolean(dr["IsCancel"] == DBNull.Value ? false : dr["IsCancel"]);
                    PayrollConfirmationRequest.ActionType = Convert.ToString(dr["ActionType"] == DBNull.Value ? "" : dr["ActionType"]);
                    ListPayrollConfirmationRequest.Add(PayrollConfirmationRequest);
                }
            }
            catch (Exception e)
            {

            }
            return ListPayrollConfirmationRequest;
        }

        public OperationDetails PayrollRequestConfirmation(int CycleId, int RequestEmployeeId)
        {
            dataHelper = new GeneralDB.DataHelper();
            OperationDetails operationDetails;
            if (dataHelper.ExcutestoredProcedure(new List<SqlParameter>() {
                new SqlParameter("@PayrollConfirmationId", CycleId),
                new SqlParameter("@RequestEmployeeId", RequestEmployeeId)
            }
            , "Hr_Stp_ConfirmPayrollRequest"))
            {
                operationDetails = new OperationDetails() { CssClass = "success", Message = "Payroll request has been confirmed", Success = true };
            }
            else
            {
                operationDetails = new OperationDetails() { CssClass = "error", Message = "Techinical error has occurred", Success = false };
            }
            return operationDetails;
        }

        public OperationDetails PayrollRequestCancelation(int CycleId, int RequestEmployeeId)
        {
            dataHelper = new GeneralDB.DataHelper();
            OperationDetails operationDetails;
            if (dataHelper.ExcutestoredProcedure(new List<SqlParameter>() {
                new SqlParameter("@PayrollConfirmationId", CycleId),
                new SqlParameter("@RequestEmployeeId", RequestEmployeeId)
            }
            , "Hr_Stp_CancelPayrollRequest"))
            {
                operationDetails = new OperationDetails() { CssClass = "success", Message = "Payroll request has been canceled", Success = true };
            }
            else
            {
                operationDetails = new OperationDetails() { CssClass = "error", Message = "Techinical error has occurred", Success = false };
            }
            return operationDetails;
        }

        public PayrollRequestDetailModel GetPayrollRequestDetails(int PayrollConfirmationId, string Type)
        {
            dataHelper = new GeneralDB.DataHelper();
            PayrollRequestDetailModel payrollRequestDetailModel = new PayrollRequestDetailModel();
            DataTable Dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>() {
                new SqlParameter("@PayrollConfirmationId", PayrollConfirmationId),
                new SqlParameter("@Type", Type)
            }
              , "Hr_Stp_GetDetailsForRequest");
            foreach (DataRow dr in Dt.Rows)
            {
                payrollRequestDetailModel.EmployeeName = dr["EmployeeName"].ToString();
                payrollRequestDetailModel.ActionType = dr["ActionType"].ToString();
                payrollRequestDetailModel.Amount = Convert.ToDecimal(dr["Amount"].ToString() == "" ? "0" : dr["Amount"].ToString());
                payrollRequestDetailModel.Note = dr["Note"].ToString();
                payrollRequestDetailModel.ItemName = dr["ItemName"].ToString();
                payrollRequestDetailModel.TrDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dr["TrDate"].ToString());
                payrollRequestDetailModel.HeaderName = dr["HeaderName"].ToString();
                payrollRequestDetailModel.DateHeaderName = dr["DateHeaderName"].ToString();
                payrollRequestDetailModel.Installments = dr["Installments"].ToString() == "" ? 0 : Convert.ToInt32(dr["Installments"].ToString());
            }
            return payrollRequestDetailModel;
        }

        public List<PayDeductionDetailModel> GetAllPayDeductionDetailsListForPayrollRequest(int PayrollConfirmationId)
        {
            List<PayDeductionDetailModel> payDeductionDetailModelList = null;
            try
            {
                payDeductionDetailModelList = new List<PayDeductionDetailModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetDedcutionDetailsForPayrollRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayrollConfirmationId", PayrollConfirmationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayDeductionDetailModel payDeductionDetailModel;
                    while (sqlDataReader.Read())
                    {
                        payDeductionDetailModel = new PayDeductionDetailModel();
                        //payDeductionDetailModel.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        //payDeductionDetailModel.PayDeductionDetailID = Convert.ToInt32(sqlDataReader["PayDeductionDetailID"].ToString());
                        payDeductionDetailModel.Amount = Convert.ToDouble(sqlDataReader["Amount"]);
                        payDeductionDetailModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        payDeductionDetailModel.DeductionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DeductionDate"].ToString());
                        payDeductionDetailModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        payDeductionDetailModelList.Add(payDeductionDetailModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDeductionDetailModelList;
        }

        public OperationDetails UpdatePayrollverificationSetting(List<PayrollVerificationSetting> lstSetting)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("VerificationSettingId");
                resultTable.Columns.Add("IsEnabled");

                foreach (PayrollVerificationSetting setting in lstSetting)
                {
                    resultTable.Rows.Add(setting.PayrollVerificationID,
                                         setting.IsEnabled
                                         );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdatePayrollVerificationSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MultipleSetting", resultTable);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Payroll verification setting updated successfully.";
                operationDetails.CssClass = "success";

            }
            catch
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public bool CheckPayCycleGenerated(int cycleId)
        {
            bool IsGenerated = false;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckPayCycleGenerated", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CycleID", cycleId);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IsGenerated";
                param.DbType = DbType.Boolean;
                param.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(param);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                IsGenerated = Convert.ToBoolean(param.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IsGenerated;
        }

        public List<string> GetActivePayActiveEmployees(bool isActive, bool isPayActive)
        {
            List<string> lstEmp = new List<string>();
            try
            {               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetActivePayActiveEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@isActive", isActive);
                sqlCommand.Parameters.AddWithValue("@isPayactive", isPayActive);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                string EmployeeName = "";
                if (sqlDataReader.HasRows)
                {                  
                    while (sqlDataReader.Read())
                    {
                        EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        lstEmp.Add(EmployeeName);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstEmp;
        }
    }
}
