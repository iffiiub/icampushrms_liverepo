﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using HRMS.Entities;


namespace HRMS.DataAccess
{
    public class DashBoardDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<DocumentsModel> GetExpiredDocumentList(int EmployeeID, int mode)
        {
            List<DocumentsModel> docModelList = null;
            try
            {
                docModelList = new List<DocumentsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExpriedDocumentList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@mode", mode);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DocumentsModel docModel;
                    while (sqlDataReader.Read())
                    {
                        docModel = new DocumentsModel();
                        docModel.EmployeeId = sqlDataReader["EmployeeId"].ToString();
                        docModel.DocTypeId = Convert.ToInt32(sqlDataReader["DocTypeId"].ToString());
                        docModel.DocType = sqlDataReader["DocType"].ToString();
                        docModel.DocId = Convert.ToInt32(sqlDataReader["DocId"].ToString());
                        docModel.DocNo = sqlDataReader["DocNo"].ToString();
                        docModel.dModifiedExpiryDate = Convert.ToDateTime(sqlDataReader["ExpiryDate"].ToString());
                        docModel.FilePath = sqlDataReader["FilePath"].ToString();
                        docModelList.Add(docModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return docModelList.OrderBy(x=>x.DocType).ToList();
        }



        public List<DashBoardModel> GetPostDatedExpiredDocumentList(int EmployeeID)
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {
                dashBoardModelList = new List<DashBoardModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPostdatedExpiredDocumentsListLimitedNew", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;
                    while (sqlDataReader.Read())
                    {
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.PayDocumentTypeID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        dashBoardModel.PayDocumentTypeDescription_1 = sqlDataReader["Desrciption"].ToString();
                        dashBoardModel.count = Convert.ToInt32(sqlDataReader["totalcount"].ToString());

                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }


        public List<DashBoardModel> GetEmployeeByDepartmentList()
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {
                dashBoardModelList = new List<DashBoardModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetEmployeeByDepartmentSummaryForDashBoard", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;
                    while (sqlDataReader.Read())
                    {
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        dashBoardModel.DepartmentName = sqlDataReader["DepartmentName"].ToString();
                        dashBoardModel.Strength = Convert.ToInt32(sqlDataReader["Strength"].ToString());

                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }

        //public List<DashBoardModel> GetEmployeeByDepartmentList()
        //{
        //    List<DashBoardModel> dashBoardModelList = null;
        //    try
        //    {
        //        dashBoardModelList = new List<DashBoardModel>();

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("HR_uspGetEmployeeByDepartmentSummaryForDashBoard", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {
        //            DashBoardModel dashBoardModel;
        //            while (sqlDataReader.Read())
        //            {
        //                dashBoardModel = new DashBoardModel();
        //                dashBoardModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
        //                dashBoardModel.DepartmentName = sqlDataReader["DepartmentName"].ToString();
        //                dashBoardModel.Strength = Convert.ToInt32(sqlDataReader["Strength"].ToString());

        //                dashBoardModelList.Add(dashBoardModel);
        //            }
        //        }
        //        sqlConnection.Close();


        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return dashBoardModelList;
        //}

        public List<DashBoardModel> GetEmployeeCountry(int companyid)
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {

                List<string> LAtLongList;
                dashBoardModelList = new List<DashBoardModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetNationalityLatLong", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", companyid);


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;

                    while (sqlDataReader.Read())
                    {
                        LAtLongList = new List<string>();
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.TotalEmployee = sqlDataReader["TotalEmployee"].ToString();
                        var s = sqlDataReader["LatLong"].ToString().Split(new[] { "," }, StringSplitOptions.None);
                        LAtLongList.Add(s[0]);
                        LAtLongList.Add(s[1]);
                        dashBoardModel.LAtLongList = LAtLongList;

                        //       dashBoardModel.LAtLongList.Add(sqlDataReader["LatLong"].ToString());

                        // dashBoardModel.LAtLong = sqlDataReader["LatLong"].ToString();
                        dashBoardModel.CountryName = sqlDataReader["CountryName"].ToString();

                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }

        public List<DashBoardModel> GetEmployeeStatusList()
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {
                dashBoardModelList = new List<DashBoardModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_UspGetEmployeeStatusForDashboard", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;
                    while (sqlDataReader.Read())
                    {
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.isActive = sqlDataReader["isActive"].ToString();
                        dashBoardModel.Strength = Convert.ToInt32(sqlDataReader["Strength"].ToString());

                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }

        public List<DashBoardModel> GetEmployeeWithNoLabourCardList()
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {
                dashBoardModelList = new List<DashBoardModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetEmployeesHavingNoLabourCard", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;
                    while (sqlDataReader.Read())
                    {
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        dashBoardModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        dashBoardModel.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        dashBoardModel.SurName = sqlDataReader["SurName"].ToString();

                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }

        public List<DashBoardModel> GetEmployeeWithGender()
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {
                dashBoardModelList = new List<DashBoardModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetEmployeeBySex", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;
                    while (sqlDataReader.Read())
                    {
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
                        dashBoardModel.GenderName = sqlDataReader["GenderName"].ToString();
                        dashBoardModel.Strength = Convert.ToInt32(sqlDataReader["Strength"].ToString());


                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }

        public List<DashBoardModel> GetSectionAndEmployeeList(int CompanyID)
        {
            List<DashBoardModel> dashBoardModelList = null;
            try
            {
                dashBoardModelList = new List<DashBoardModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_SectionAndEmployeeCount", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DashBoardModel dashBoardModel;
                    while (sqlDataReader.Read())
                    {
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.SectionName = sqlDataReader["EmployeeSectionName_1"].ToString();
                        dashBoardModel.TotalEmployee = sqlDataReader["TotalEmployee"].ToString();



                        dashBoardModelList.Add(dashBoardModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return dashBoardModelList;
        }

        public List<DocumentsModel> GetExpiringDocumentsList()
        {
            List<DocumentsModel> docModelList = null;
            try
            {
                docModelList = new List<DocumentsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExpringDocumentList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DocumentsModel docModel;
                    while (sqlDataReader.Read())
                    {
                        docModel = new DocumentsModel();
                        docModel.EmployeeId = sqlDataReader["EmployeeId"].ToString();
                        docModel.DocTypeId = Convert.ToInt32(sqlDataReader["DocTypeId"].ToString());
                        docModel.DocType = sqlDataReader["DocType"].ToString();
                        docModel.DocId = Convert.ToInt32(sqlDataReader["DocId"].ToString());
                        docModel.DocNo = sqlDataReader["DocNo"].ToString();
                        docModel.dModifiedExpiryDate = Convert.ToDateTime(sqlDataReader["ExpiryDate"].ToString());
                        docModel.FilePath = sqlDataReader["FilePath"].ToString();
                        docModelList.Add(docModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return docModelList.OrderBy(x => x.DocType).ToList();
        }
    }
}
