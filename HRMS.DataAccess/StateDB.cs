﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class StateDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<StateModel> GetAllStatesByCountryId(string CountryId)
        {
            List<StateModel> stateList = new List<StateModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllStatesByCountryId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@CountryId", int.Parse(CountryId));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    StateModel countryModel;
                    while (sqlDataReader.Read())
                    {
                        countryModel = new StateModel();
                        countryModel.StateId = Convert.ToInt32(sqlDataReader["StateID"].ToString());
                        countryModel.StateName = Convert.ToString(sqlDataReader["StateName"]);
                        stateList.Add(countryModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return stateList;
        }
    }
}
