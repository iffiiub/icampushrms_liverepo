﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;
using System.Configuration;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class MunicipalityDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        GeneralDB.DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;        
      
        public OperationDetails AddEditMunicipalityDocument(DocumentsModel documentsModel, int tranMode)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayDocumentCUD1", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", tranMode);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", documentsModel.EmpId); 
                sqlCommand.Parameters.AddWithValue("@PayDocumentId", documentsModel.DocId);
                sqlCommand.Parameters.AddWithValue("@PayDocumentTypeID",7);
                sqlCommand.Parameters.AddWithValue("@PayDocumentNumber", documentsModel.DocNo);
                sqlCommand.Parameters.AddWithValue("@IssuePlaceID", documentsModel.IssueCityID);
                sqlCommand.Parameters.AddWithValue("@IssueDate",CommonDB.SetCulturedDate(documentsModel.IssueDate));
                sqlCommand.Parameters.AddWithValue("@ExpireDate", CommonDB.SetCulturedDate(documentsModel.ExpiryDate));
                sqlCommand.Parameters.AddWithValue("@IsPrimary", documentsModel.IsPrimary);
                sqlCommand.Parameters.AddWithValue("@MotherName",documentsModel.MotherName);                 
                sqlCommand.Parameters.AddWithValue("@IsPassportWithEmployee", documentsModel.IsPassportWithEmployee);
                sqlCommand.Parameters.AddWithValue("@TakenDate", CommonDB.SetCulturedDate(documentsModel.TakenDate));
                sqlCommand.Parameters.AddWithValue("@Reason", documentsModel.Reason);
                sqlCommand.Parameters.AddWithValue("@MustReturn", documentsModel.MustReturn);
                sqlCommand.Parameters.AddWithValue("@ReturnDate", CommonDB.SetCulturedDate(documentsModel.ReturnDate));
                sqlCommand.Parameters.AddWithValue("@SponsorID", documentsModel.SponsorID);
                sqlCommand.Parameters.AddWithValue("@Note", documentsModel.Note);
                sqlCommand.Parameters.AddWithValue("@HRContractTypeID", documentsModel.HRContractTypeID);
                sqlCommand.Parameters.AddWithValue("@IssueCountryID", documentsModel.IssueCountryID);
                sqlCommand.Parameters.AddWithValue("@MOLTitle",documentsModel.MOLTitleId);
                sqlCommand.Parameters.AddWithValue("@VAcc1", CommonDB.SetCulturedDate(documentsModel.VAcc1));
                sqlCommand.Parameters.AddWithValue("@VAcc2", CommonDB.SetCulturedDate(documentsModel.VAcc2));
                sqlCommand.Parameters.AddWithValue("@VAcc3", CommonDB.SetCulturedDate(documentsModel.VAcc3));
                sqlCommand.Parameters.AddWithValue("@FileName",documentsModel.DocumentFileName);
                sqlCommand.Parameters.AddWithValue("@Filepath", documentsModel.DocumentFile);

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@output";
                param.DbType = DbType.Int32;
                param.Direction = ParameterDirection.Output;                
                sqlCommand.Parameters.Add(param);

                sqlCommand.ExecuteReader();
                int MunicipalityId = sqlCommand.Parameters["@output"].Value == DBNull.Value || sqlCommand.Parameters["@output"].Value == "" ? 0 :Convert.ToInt32(sqlCommand.Parameters["@output"].Value.ToString());
                sqlConnection.Close();
                if (MunicipalityId > 0)
                {
                    if (tranMode == 1)
                    {
                        op = new OperationDetails(true, "Municipality saved successfully.", null, MunicipalityId);
                    }
                    else if (tranMode == 2)
                    {
                        op = new OperationDetails(true, "Municipality updated successfully.", null, MunicipalityId);
                    }
                    else
                    {
                        op = new OperationDetails(true, "Municipality deleted successfully.", null, MunicipalityId);
                    }
                }
                else
                {
                    if (tranMode == 1)
                    {
                        op = new OperationDetails(false, "Error occurred while saving municipality.", null, MunicipalityId);
                    }
                    else if (tranMode == 2)
                    {
                        op = new OperationDetails(true, "Error occurred while updating municipality.", null, MunicipalityId);
                    }
                    else
                    {
                        op = new OperationDetails(true, "Error occurred while deleting municipality.", null, MunicipalityId);
                    }
                }
                return op;
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Municipality.", exception);
                //throw;
            }
            finally
            {
                
            }
        }

        public List<DocumentsModel> GetMuncipalityDocumentList(int EmployeeId)
        {
            try
            {
                List<DocumentsModel> lstDocumentList = new List<DocumentsModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeDocuments", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@PayDocumentTypeID",7);
               
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DocumentsModel documentModel;
                    while (sqlDataReader.Read())
                    {
                        documentModel = new DocumentsModel();
                        documentModel.DocId = Convert.ToInt32(sqlDataReader["PayDocumentID"]);
                        documentModel.DocNo = Convert.ToString(sqlDataReader["PayDocumentNumber"]);
                        documentModel.IssueDate = sqlDataReader["IssueDate"]==DBNull.Value||Convert.ToString(sqlDataReader["IssueDate"])==""?"":Convert.ToDateTime(sqlDataReader["IssueDate"]).ToString("dd/MM/yyyy");
                        documentModel.IssuePlace= Convert.ToString(sqlDataReader["CityName_1"]);
                        documentModel.ExpiryDate = sqlDataReader["ExpireDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ExpireDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ExpireDate"]).ToString("dd/MM/yyyy");;
                        documentModel.IsPrimary = Convert.ToBoolean(sqlDataReader["IsPrimary"]);
                        documentModel.MotherName = Convert.ToString(sqlDataReader["MotherName"]);
                        documentModel.IsPassportWithEmployee= sqlDataReader["IsPassportWithEmployee"]==DBNull.Value ||Convert.ToString(sqlDataReader["IsPassportWithEmployee"])==""?false:Convert.ToBoolean(sqlDataReader["IsPassportWithEmployee"]);
                        documentModel.TakenDate = sqlDataReader["TakenDate"] == DBNull.Value || Convert.ToString(sqlDataReader["TakenDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["TakenDate"]).ToString("dd/MM/yyyy"); ;
                        documentModel.Reason= Convert.ToString(sqlDataReader["Reason"]);
                        documentModel.MustReturn= sqlDataReader["MustReturn"] == DBNull.Value || Convert.ToString(sqlDataReader["MustReturn"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["MustReturn"]);
                        documentModel.ReturnDate= sqlDataReader["ReturnDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ReturnDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ReturnDate"]).ToString("dd/MM/yyyy"); ;
                        documentModel.SponsorID= sqlDataReader["SponsorID"] == DBNull.Value || Convert.ToString(sqlDataReader["SponsorID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["SponsorID"]);
                        documentModel.Note= Convert.ToString(sqlDataReader["Note"]);
                        documentModel.HRContractTypeID= sqlDataReader["HRContractTypeID"] == DBNull.Value || Convert.ToString(sqlDataReader["HRContractTypeID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["HRContractTypeID"]);
                        documentModel.IssueCountryID= sqlDataReader["IssueCountryID"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueCountryID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["IssueCountryID"]);
                        documentModel.IssueCountry= Convert.ToString(sqlDataReader["IssueCountry"]);
                        documentModel.MOLTitleId= sqlDataReader["MolTitleId"].ToString()==""?0: Convert.ToInt32(sqlDataReader["MolTitleId"]);
                        documentModel.VAcc1= sqlDataReader["VAcc1"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc1"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc1"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc2 = sqlDataReader["VAcc2"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc2"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc2"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc3 = sqlDataReader["VAcc3"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc3"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc3"]).ToString("dd/MM/yyyy");
                        documentModel.DocumentFile = Convert.ToString(sqlDataReader["FilePath"]);
                        lstDocumentList.Add(documentModel);
                    }
                }
                sqlConnection.Close();
                return lstDocumentList;
            }
            catch (Exception exception)
            {
               
               throw;
            }
            finally
            {

            }
        }

        public DocumentsModel GetMunicipalityDocument(int DocId)
        {
            try
            {
                DocumentsModel documentModel= new DocumentsModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDocumentByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aIntPayDocumentID", DocId);               

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    
                    while (sqlDataReader.Read())
                    {                        
                        documentModel.DocId= sqlDataReader["PayDocumentID"] == DBNull.Value || Convert.ToString(sqlDataReader["PayDocumentID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["PayDocumentID"]); 
                        documentModel.DocNo = Convert.ToString(sqlDataReader["PayDocumentNumber"]);
                        documentModel.IssueDate = sqlDataReader["IssueDate"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["IssueDate"]).ToString("dd/MM/yyyy");
                        documentModel.IssueCityID= sqlDataReader["IssuePlaceID"] == DBNull.Value || Convert.ToString(sqlDataReader["IssuePlaceID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["IssuePlaceID"]);
                        documentModel.IssuePlace = Convert.ToString(sqlDataReader["IssuePlaceName"]); 
                        documentModel.ExpiryDate = sqlDataReader["ExpireDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ExpireDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ExpireDate"]).ToString("dd/MM/yyyy"); 
                        documentModel.IsPrimary = Convert.ToBoolean(sqlDataReader["IsPrimary"]);
                        documentModel.MotherName = Convert.ToString(sqlDataReader["MotherName"]);
                        documentModel.IsPassportWithEmployee = sqlDataReader["IsPassportWithEmployee"] == DBNull.Value || Convert.ToString(sqlDataReader["IsPassportWithEmployee"]) == "" ? false : Convert.ToBoolean(sqlDataReader["IsPassportWithEmployee"]);
                        documentModel.TakenDate = sqlDataReader["TakenDate"] == DBNull.Value || Convert.ToString(sqlDataReader["TakenDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["TakenDate"]).ToString("dd/MM/yyyy");
                        documentModel.Reason = Convert.ToString(sqlDataReader["Reason"]);
                        documentModel.MustReturn = sqlDataReader["MustReturn"] == DBNull.Value || Convert.ToString(sqlDataReader["MustReturn"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["MustReturn"]);
                        documentModel.ReturnDate = sqlDataReader["ReturnDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ReturnDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ReturnDate"]).ToString("dd/MM/yyyy"); 
                        documentModel.SponsorID = sqlDataReader["SponsorID"] == DBNull.Value || Convert.ToString(sqlDataReader["SponsorID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["SponsorID"]);
                        documentModel.Note = Convert.ToString(sqlDataReader["Note"]);
                        documentModel.HRContractTypeID = sqlDataReader["HRContractTypeID"] == DBNull.Value || Convert.ToString(sqlDataReader["HRContractTypeID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["HRContractTypeID"]);
                        documentModel.IssueCountryID = sqlDataReader["IssueCountryID"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueCountryID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["IssueCountryID"]);
                        documentModel.IssueCountry= Convert.ToString(sqlDataReader["IssueCountry"]);
                        documentModel.MOLTitleId = sqlDataReader["MolTitleId"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["MolTitleId"]);
                        documentModel.VAcc1 = sqlDataReader["VAcc1"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc1"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc1"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc2 = sqlDataReader["VAcc2"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc2"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc2"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc3 = sqlDataReader["VAcc3"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc3"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc3"]).ToString("dd/MM/yyyy");                        
                    }
                }
                sqlConnection.Close();
                return documentModel;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

    }
}
