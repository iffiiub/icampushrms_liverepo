﻿using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class LeaveReasonDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        #region Leave Reason
        public List<LeaveReason> GetAllLeaveReason()
        {
            // Create a list to hold the Employee Leave Request		
            List<LeaveReason> leaveReasonList = new List<LeaveReason>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetAllLeaveReason"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new EmployeeLeaveRequest, and insert them into our list
                        LeaveReason leaveReasonModel;
                        while (reader.Read())
                        {
                            leaveReasonModel = new LeaveReason();
                            leaveReasonModel.ID = Convert.ToInt32(reader["ID"].ToString());
                            leaveReasonModel.LeaveReason_1 = Convert.ToString(reader["LeaveReson_1"].ToString());
                            leaveReasonModel.LeaveReason_2 = Convert.ToString(reader["LeaveReson_2"].ToString());
                            leaveReasonModel.LeaveReason_3 = Convert.ToString(reader["LeaveReson_3"].ToString());
                            leaveReasonList.Add(leaveReasonModel);
                        }
                    }
                }
                // Finally, we return our list of employeeLeaveRequest
                return leaveReasonList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeLeaveRequest List.", exception);
                throw;
            }
            finally
            {

            }
        }      

        #endregion
    }
}
