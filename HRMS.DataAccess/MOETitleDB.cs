﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class MOETitleDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        
        /// <summary>
        /// Get all MOE Title
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<MOETitleModel> GetAllMOETitle()
         {
            List<MOETitleModel> moeTitleList = null;
            try
            {
                moeTitleList = new List<MOETitleModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetMOETitle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    MOETitleModel moeTitle;
                    while (sqlDataReader.Read())
                    {
                        moeTitle = new MOETitleModel();
                        moeTitle.MOETitleID = Convert.ToInt32(sqlDataReader["MOETitleID"].ToString());
                        moeTitle.MOETitleName_1 = sqlDataReader["MOE Title Name_1"] == DBNull.Value ? "" : sqlDataReader["MOE Title Name_1"].ToString().Trim();
                        moeTitle.MOETitleName_2 = sqlDataReader["MOE Title Name_2"] == DBNull.Value ? "" : sqlDataReader["MOE Title Name_2"].ToString().Trim();
                        moeTitle.MOETitleName_3 = sqlDataReader["MOE Title Name_3"] == DBNull.Value ? "" : sqlDataReader["MOE Title Name_3"].ToString().Trim();
                        moeTitle.MOETitleShortName_1 = sqlDataReader["MOE Title ShortName_1"] == DBNull.Value ? "" : sqlDataReader["MOE Title ShortName_1"].ToString().Trim();
                        moeTitle.MOETitleShortName_2 = sqlDataReader["MOE Title ShortName_2"] == DBNull.Value ? "" : sqlDataReader["MOE Title ShortName_2"].ToString().Trim();
                        moeTitle.MOETitleShortName_3 = sqlDataReader["MOE Title ShortName_3"] == DBNull.Value ? "" : sqlDataReader["MOE Title ShortName_3"].ToString().Trim();
                        moeTitleList.Add(moeTitle);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return moeTitleList;
        }


        public DataSet GetAllMOETitleDatasSet()
        {

            sqlConnection = new SqlConnection(connectionString);
            string Query = "select MOETitleName_1 as [MOE Title (En)], "+
                           "MOETitleName_2 as [MOE Title (Ar)], "
                           +"MOETitleName_1 as [MOE Title (Fr)] from GEN_MOETitle where MOETitleId > 0  order by [MOE Title (En)] asc";
            SqlDataAdapter da = new SqlDataAdapter(Query, sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }


        public List<MOETitleModel> GetAllMOETitleList()
        {
            List<MOETitleModel> moeTitleList = null;
            try
            {
                moeTitleList = new List<MOETitleModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetMOETitleList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    MOETitleModel moeTitle;
                    while (sqlDataReader.Read())
                    {
                        moeTitle = new MOETitleModel();

                        moeTitle.MOETitleID = Convert.ToInt32(sqlDataReader["MOETitleID"].ToString());
                        moeTitle.MOETitleName_1 = sqlDataReader["MOETitleName_1"] == DBNull.Value ? "" : sqlDataReader["MOETitleName_1"].ToString();
                        moeTitle.MOETitleName_2 = sqlDataReader["MOETitleName_2"] == DBNull.Value ? "" : sqlDataReader["MOETitleName_2"].ToString();
                        moeTitle.MOETitleName_3 = sqlDataReader["MOETitleName_3"] == DBNull.Value ? "" : sqlDataReader["MOETitleName_3"].ToString();

                        moeTitle.MOETitleShortName_1 = sqlDataReader["MOETitleShortName_1"] == DBNull.Value ? "" : sqlDataReader["MOETitleShortName_1"].ToString();
                        moeTitle.MOETitleShortName_2 = sqlDataReader["MOETitleShortName_2"] == DBNull.Value ? "" : sqlDataReader["MOETitleShortName_2"].ToString();
                        moeTitle.MOETitleShortName_3 = sqlDataReader["MOETitleShortName_3"] == DBNull.Value ? "" : sqlDataReader["MOETitleShortName_3"].ToString();

                        moeTitleList.Add(moeTitle);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return moeTitleList;
        }


        public MOETitleModel GetMOETitle(int id)
        {
            MOETitleModel moeTitle = new MOETitleModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetMOETitleByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MOETitleID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        moeTitle = new MOETitleModel();

                        moeTitle.MOETitleID = Convert.ToInt32(sqlDataReader["MOETitleID"].ToString());
                        moeTitle.MOETitleName_1 = sqlDataReader["MOETitleName_1"] == DBNull.Value ? "" : sqlDataReader["MOETitleName_1"].ToString();
                        moeTitle.MOETitleName_2 = sqlDataReader["MOETitleName_2"] == DBNull.Value ? "" : sqlDataReader["MOETitleName_2"].ToString();
                        moeTitle.MOETitleName_3 = sqlDataReader["MOETitleName_3"] == DBNull.Value ? "" : sqlDataReader["MOETitleName_3"].ToString();

                        moeTitle.MOETitleShortName_1 = sqlDataReader["MOETitleShortName_1"] == DBNull.Value ? "" : sqlDataReader["MOETitleShortName_1"].ToString();
                        moeTitle.MOETitleShortName_2 = sqlDataReader["MOETitleShortName_2"] == DBNull.Value ? "" : sqlDataReader["MOETitleShortName_2"].ToString();
                        moeTitle.MOETitleShortName_3 = sqlDataReader["MOETitleShortName_3"] == DBNull.Value ? "" : sqlDataReader["MOETitleShortName_3"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return moeTitle;
        }

        /// <summary>
        /// Add/Update/Delete MOE Title
        /// </summary>
        /// <param name="moeTitle"></param>
        /// <param name="operationType"></param>
        /// <returns></returns>
        public string AddUpdateDeleteMOETitle(MOETitleModel moeTitle, int operationType)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspMOETitleCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MOETitleName_1", moeTitle.MOETitleName_1);
                sqlCommand.Parameters.AddWithValue("@MOETitleName_2", moeTitle.MOETitleName_2);
                sqlCommand.Parameters.AddWithValue("@MOETitleName_3", moeTitle.MOETitleName_3);

                sqlCommand.Parameters.AddWithValue("@MOETitleShortName_1", moeTitle.MOETitleShortName_1);
                sqlCommand.Parameters.AddWithValue("@MOETitleShortName_2", moeTitle.MOETitleShortName_2);
                sqlCommand.Parameters.AddWithValue("@MOETitleShortName_3", moeTitle.MOETitleShortName_3);

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationType);

                if (operationType == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@MOETitleID", 0);                    
                }
                else if (operationType == 2)
                {
                    sqlCommand.Parameters.AddWithValue("@MOETitleID", moeTitle.MOETitleID);                    
                }
                else if (operationType == 3)
                {
                    sqlCommand.Parameters.AddWithValue("MOETitleID", moeTitle.MOETitleID);                    
                }

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }
    }
}
