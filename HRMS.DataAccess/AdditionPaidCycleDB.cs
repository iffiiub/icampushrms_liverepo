﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class AdditionPaidCycleDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        private DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        #region Pay Cycle
        /// <summary>
        /// Add/Update/Delete Pay Cycle
        /// </summary>
        /// <param name="payCycle"></param>
        /// <returns></returns>
        public string AddUpdateDeletePayCycle(PayCycle payCycle, string operationType)
        {
            string Message = "";
            try
            {
                //DateTime? dateFrom= payCycle.DateFrom != "" ? (DateTime?)DateTime.Parse(payCycle.DateFrom, new CultureInfo("en-GB")) : null;
                //DateTime? dateTo = payCycle.DateTo != "" ? (DateTime?)DateTime.Parse(payCycle.DateTo, new CultureInfo("en-GB")) : null;

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayCycleCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayCycleName_1", payCycle.PayCycleName_1 == null ? "" : payCycle.PayCycleName_1);
                sqlCommand.Parameters.AddWithValue("@PayCycleName_2", payCycle.PayCycleName_2 == null ? "" : payCycle.PayCycleName_2);
                sqlCommand.Parameters.AddWithValue("@PayCycleName_3", payCycle.PayCycleName_3 == null ? "" : payCycle.PayCycleName_3);
                sqlCommand.Parameters.AddWithValue("@acyear", payCycle.acyear == null ? "2015" : payCycle.acyear);
                sqlCommand.Parameters.AddWithValue("@active", payCycle.active);

                if (operationType == "insert")
                {
                    sqlCommand.Parameters.AddWithValue("@PayCycleID", 0);
                    sqlCommand.Parameters.AddWithValue("@DateFrom", GeneralDB.CommonDB.SetCulturedDate(payCycle.DateFrom));
                    sqlCommand.Parameters.AddWithValue("@DateTo", GeneralDB.CommonDB.SetCulturedDate(payCycle.DateTo));
                    sqlCommand.Parameters.AddWithValue("@OperationType", "I");
                }
                else if (operationType == "update")
                {
                    sqlCommand.Parameters.AddWithValue("@PayCycleID", payCycle.PayCycleID);
                    sqlCommand.Parameters.AddWithValue("@DateFrom", GeneralDB.CommonDB.SetCulturedDate(payCycle.DateFrom));
                    sqlCommand.Parameters.AddWithValue("@DateTo", GeneralDB.CommonDB.SetCulturedDate(payCycle.DateTo));
                    sqlCommand.Parameters.AddWithValue("@OperationType", "U");
                }
                else if (operationType == "delete")
                {
                    sqlCommand.Parameters.AddWithValue("@DateFrom", DateTime.Now);
                    sqlCommand.Parameters.AddWithValue("@DateTo", DateTime.Now);
                    sqlCommand.Parameters.AddWithValue("@PayCycleID", payCycle.PayCycleID);
                    sqlCommand.Parameters.AddWithValue("@OperationType", "D");
                }

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Get all PayCycle
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<PayCycle> GetAllPayCycle()
        {
            List<PayCycle> payCycleList = null;
            try
            {
                payCycleList = new List<PayCycle>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_SelectPayCycle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayCycle payCycle;
                    while (sqlDataReader.Read())
                    {
                        payCycle = new PayCycle();

                        payCycle.PayCycleID = Convert.ToInt32(sqlDataReader["PayCycleID"].ToString());
                        payCycle.PayCycleName_1 = sqlDataReader["Pay Cycle Name_1"] == DBNull.Value ? "" : sqlDataReader["Pay Cycle Name_1"].ToString().Trim();
                        payCycle.PayCycleName_2 = sqlDataReader["Pay Cycle Name_2"] == DBNull.Value ? "" : sqlDataReader["Pay Cycle Name_2"].ToString().Trim();
                        payCycle.PayCycleName_3 = sqlDataReader["Pay Cycle Name_3"] == DBNull.Value ? "" : sqlDataReader["Pay Cycle Name_3"].ToString().Trim();
                        payCycle.DateFrom = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateFrom"].ToString());
                        payCycle.DateTo = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateTo"].ToString());
                        payCycle.active = Convert.ToBoolean(sqlDataReader["Active"].ToString());
                        payCycle.acyear = sqlDataReader["Academic Year"] == DBNull.Value ? "" : sqlDataReader["Academic Year"].ToString().Trim();
                        payCycleList.Add(payCycle);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payCycleList;
        }

        public DataSet GetAllPayCycleDataSet()
        {

            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            string Query = "SELECT PayCycleName_1 AS [Pay Cycle Name (En)] , PayCycleName_2 AS [Pay Cycle Name (Ar)] , PayCycleName_3 AS [Pay Cycle Name (Fr)]" +
                           ", CONVERT(VARCHAR, DateFrom, 106) AS [From Date], CONVERT(VARCHAR, DateTo, 106) AS [To Date],Convert(nvarchar(50), ACYEAR.BeginYear)+'-'+Convert(nvarchar(50), ACYEAR.EndYear) AS [Academic Year], Active " +
                           "FROM[dbo].[HR_PayCycle] PAY LEFT OUTER JOIN Gen_AcademicYear ACYEAR on PAY.acyear = ACYEAR.AcademicYearId Where PAY.PayCycleID > 0 order by [Pay Cycle Name (En)] asc";
            SqlDataAdapter da = new SqlDataAdapter(Query, sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.Text;

            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;

        }

        public PayCycle GetPayCycle(int id)
        {
            PayCycle payCycle = new PayCycle();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_PayCycleById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayCycleID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        payCycle = new PayCycle();

                        payCycle.PayCycleID = Convert.ToInt32(sqlDataReader["PayCycleID"].ToString());
                        payCycle.PayCycleName_1 = sqlDataReader["PayCycleName_1"] == DBNull.Value ? "" : sqlDataReader["PayCycleName_1"].ToString();
                        payCycle.PayCycleName_2 = sqlDataReader["PayCycleName_2"] == DBNull.Value ? "" : sqlDataReader["PayCycleName_2"].ToString();
                        payCycle.PayCycleName_3 = sqlDataReader["PayCycleName_3"] == DBNull.Value ? "" : sqlDataReader["PayCycleName_3"].ToString();
                        payCycle.DateFrom = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateFrom"].ToString());
                        payCycle.DateTo = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateTo"].ToString());
                        payCycle.active = Convert.ToBoolean(sqlDataReader["active"].ToString());
                        payCycle.acyear = sqlDataReader["acyear"] == DBNull.Value ? "" : sqlDataReader["acyear"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return payCycle;
        }
        #endregion

        #region Pay Addition
        /// <summary>
        /// Get all PayAddition
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<PayAddition> GetAllPayAdditionForEmployee(int EmployeeId)
        {
            List<PayAddition> payAdditionList = null;
            try
            {
                payAdditionList = new List<PayAddition>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayAdditionNew", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayAddition payAddition;
                    while (sqlDataReader.Read())
                    {
                        payAddition = new PayAddition();
                        payAddition.PayAdditionID = Convert.ToInt32(sqlDataReader["PayAdditionID"].ToString());
                        payAddition.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        payAddition.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        payAddition.PaySalaryAllowanceName = sqlDataReader["PaySalaryAllowanceName"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName"].ToString();
                        payAddition.AdditionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AdditionDate"].ToString());
                        payAddition.Amount = sqlDataReader["Amount"] == DBNull.Value ? 0 : float.Parse(sqlDataReader["Amount"].ToString());
                        payAddition.Description = sqlDataReader["Description"] == DBNull.Value ? "" : sqlDataReader["Description"].ToString();
                        payAddition.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        payAddition.TransactionDate = sqlDataReader["TransactionDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(sqlDataReader["TransactionDate"].ToString());
                        payAddition.AcademicYearID = Convert.ToInt32(sqlDataReader["AcademicYearID"].ToString());
                        payAddition.IsProcessed= Convert.ToBoolean(sqlDataReader["IsProcessed"].ToString());
                        payAdditionList.Add(payAddition);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payAdditionList;
        }

        public PayAddition GetPayAddition(int id)
        {
            PayAddition payAddition = new PayAddition();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayAdditionByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntPayAdditionID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        payAddition = new PayAddition();

                        payAddition.PayAdditionID = Convert.ToInt32(sqlDataReader["PayAdditionID"].ToString());
                        payAddition.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        payAddition.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        payAddition.AdditionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AdditionDate"].ToString());
                        // payAddition.AdditionDate = Convert.ToDateTime(sqlDataReader["AdditionDate"]).ToString("MM/dd/yyyy"); 
                        payAddition.Amount = sqlDataReader["Amount"] == DBNull.Value ? 0 : float.Parse(sqlDataReader["Amount"].ToString());
                        payAddition.Description = sqlDataReader["Description"] == DBNull.Value ? "" : sqlDataReader["Description"].ToString();

                        payAddition.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        payAddition.TransactionDate = sqlDataReader["TransactionDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(sqlDataReader["TransactionDate"].ToString());
                        payAddition.AcademicYearID = Convert.ToInt32(sqlDataReader["AcademicYearID"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return payAddition;
        }

        /// <summary>
        /// Add/Update/Delete Pay Addition
        /// </summary>
        /// <param name="payAddition"></param>
        /// <returns></returns>
        public string AddUpdateDeletePayAddition(PayAddition payAddition, int operationType)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayAdditionCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntPaySalaryAllowanceID", payAddition.PaySalaryAllowanceID);
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", payAddition.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aNumAmount", payAddition.Amount == null ? 0 : payAddition.Amount);
                sqlCommand.Parameters.AddWithValue("@aNvrDescription", payAddition.Description);

                sqlCommand.Parameters.AddWithValue("@aBitIsActive", payAddition.IsActive);
                sqlCommand.Parameters.AddWithValue("@aTntAcademicYearID", payAddition.AcademicYearID);

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationType);

                if (operationType == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@aIntPayAdditionID", 0);
                    sqlCommand.Parameters.AddWithValue("@aDttAdditionDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(payAddition.AdditionDate));
                    // sqlCommand.Parameters.AddWithValue("@aDttAdditionDate", GeneralDB.CommonDB.SetCulturedDate(payAddition.AdditionDate));
                    sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", DateTime.Now);
                }
                else if (operationType == 2)
                {
                    sqlCommand.Parameters.AddWithValue("@aIntPayAdditionID", payAddition.PayAdditionID);
                    sqlCommand.Parameters.AddWithValue("@aDttAdditionDate", GeneralDB.CommonDB.SetCulturedDate(payAddition.AdditionDate));
                    sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", DateTime.Now);
                }
                else if (operationType == 3)
                {
                    sqlCommand.Parameters.AddWithValue("@aIntPayAdditionID", payAddition.PayAdditionID);
                    sqlCommand.Parameters.AddWithValue("@aDttAdditionDate", DateTime.Now);
                    sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", DateTime.Now);
                }

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();
                // Message=

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public OperationDetails AddMultipleAllowance(List<PayAddition> lstPayAddition, int RequestEmployeeId, bool Permission)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("PaySalaryAllowanceID");
                resultTable.Columns.Add("EmployeeID");
                resultTable.Columns.Add("AdditionDate");
                resultTable.Columns.Add("amount");
                resultTable.Columns.Add("Description");
                resultTable.Columns.Add("IsActive");
                resultTable.Columns.Add("TransactionDate");
                resultTable.Columns.Add("AcademicYearID");

                foreach (PayAddition pay in lstPayAddition)
                {
                    resultTable.Rows.Add(pay.PaySalaryAllowanceID,
                                         pay.EmployeeID,
                                         pay.AdditionDate,
                                         pay.Amount,
                                         pay.Description,
                                         pay.IsActive,
                                         pay.TransactionDate,
                                         pay.AcademicYearID
                                         );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspAddMultiplePayAddition", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MultipleAllowance", resultTable);
                sqlCommand.Parameters.AddWithValue("@RequestEmployeeId", @RequestEmployeeId);
                sqlCommand.Parameters.AddWithValue("@Permission", Permission);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Pay addition added successfully.";

            }
            catch
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }
        public OperationDetails updatePayAdditionStatus(PayAddition payAddition)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdatePayAdditionStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayAdditionID", payAddition.PayAdditionID);
                sqlCommand.Parameters.AddWithValue("@isActive", payAddition.IsActive);




                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, payAddition.PayAdditionID);
        }




        /*
        private double CalculateExtraHour(int EmployeeID)
        {
            double value = -1, Amount = 0;
            short techerID = Convert.ToInt16(EmployeeID);
            DataSet dsSalary = _paySalaryBusiness.GetTeacherFullSalary(techerID);
            double salary = Convert.ToDouble(dsSalary.Tables[0].Rows[0].ItemArray[0].ToString());
            if (rtxtExtraHour.Text != null && rtxtExtraHour.Text != "")
            //if (rtxtOverTime.Text != null && rtxtOverTime.Text != "")
            //if(rtxtOverTimeText != "")
            {
                value = Convert.ToDouble(rtxtExtraHour.Text.ToString());
                //value = Convert.ToDouble(rtxtOverTimeText);
            }
            if (value != -1 && salary != 0)
            {
                Amount = (salary / 30) * value;
            }
            return Amount;
        }

        private double CalculateOverTime(int EmployeeID)
        {
            double value = -1, Amount = 0;
            short techerID = Convert.ToInt16(EmployeeID);
            DataSet dsSalary = _paySalaryBusiness.GetTeacherFullSalary(techerID);
            double salary = Convert.ToDouble(dsSalary.Tables[0].Rows[0].ItemArray[0].ToString());
            if (rtxtOverTime.Text != null && rtxtOverTime.Text != "")
            {
                value = Convert.ToDouble(rtxtOverTime.Text.ToString());
            }
            if (value != -1 && salary != 0)
            {
                Amount = ((salary / 28) / 4) * value;
            }
            return Amount;
        }
        */
        #endregion

        #region Salary Allowance
        /// <summary>
        /// Add/Update/Delete Pay Cycle
        /// </summary>
        /// <param name="paySalaryAllowance"></param>
        /// <returns></returns>
        /// 




        public string AddUpdateDeletePaySalaryAllowance(PaySalaryAllowance paySalaryAllowance, string operationType)
        {
            string Message = "";
            try
            {
                if (paySalaryAllowance.TransactionDate == null)
                    paySalaryAllowance.TransactionDate = "";
                DateTime? tranDate = paySalaryAllowance.TransactionDate != "" ? (DateTime?)DateTime.Parse(paySalaryAllowance.TransactionDate, new CultureInfo("en-GB")) : null;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPaySalaryAllowanceCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceName_1", paySalaryAllowance.PaySalaryAllowanceName_1 == null ? "" : paySalaryAllowance.PaySalaryAllowanceName_1);
                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceName_2", paySalaryAllowance.PaySalaryAllowanceName_2 == null ? "" : paySalaryAllowance.PaySalaryAllowanceName_2);
                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceName_3", paySalaryAllowance.PaySalaryAllowanceName_3 == null ? "" : paySalaryAllowance.PaySalaryAllowanceName_3);

                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceShortName_1", paySalaryAllowance.PaySalaryAllowanceShortName_1 == null ? "" : paySalaryAllowance.PaySalaryAllowanceShortName_1);
                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceShortName_2", paySalaryAllowance.PaySalaryAllowanceShortName_2 == null ? "" : paySalaryAllowance.PaySalaryAllowanceShortName_2);
                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceShortName_3", paySalaryAllowance.PaySalaryAllowanceShortName_3 == null ? "" : paySalaryAllowance.PaySalaryAllowanceShortName_3);

                sqlCommand.Parameters.AddWithValue("@IsAddition", paySalaryAllowance.IsAddition);
                sqlCommand.Parameters.AddWithValue("@IsIncludeInDeduction", paySalaryAllowance.IsIncludeInDeduction);
                sqlCommand.Parameters.AddWithValue("@Sequence", paySalaryAllowance.Sequence);
                sqlCommand.Parameters.AddWithValue("@IsVacactionDeductible", paySalaryAllowance.IsVacactionDeductible);
                sqlCommand.Parameters.AddWithValue("@IsExcludeInJV", paySalaryAllowance.IsExcludeInJV);
                sqlCommand.Parameters.AddWithValue("@IsDeductableOnHireDate", paySalaryAllowance.IsDeductableOnHireDate);
                sqlCommand.Parameters.AddWithValue("@AccountCode", paySalaryAllowance.AccountCode == null ? "" : paySalaryAllowance.AccountCode);
                sqlCommand.Parameters.AddWithValue("@VacationAccountCode", paySalaryAllowance.VacationAccountCode == null ? "" : paySalaryAllowance.VacationAccountCode);
                sqlCommand.Parameters.AddWithValue("@IsAccommodationAllowance", paySalaryAllowance.IsAccommodationAllowance);

                if (operationType == "insert")
                {
                    sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceID", 0);
                    sqlCommand.Parameters.AddWithValue("@TransactionDate", tranDate);
                    sqlCommand.Parameters.AddWithValue("@OperationType", "I");
                }
                else if (operationType == "update")
                {
                    sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceID", paySalaryAllowance.PaySalaryAllowanceID);
                    sqlCommand.Parameters.AddWithValue("@TransactionDate", tranDate);
                    sqlCommand.Parameters.AddWithValue("@OperationType", "U");
                }
                else if (operationType == "delete")
                {
                    sqlCommand.Parameters.AddWithValue("@TransactionDate", DateTime.Now);
                    sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceID", paySalaryAllowance.PaySalaryAllowanceID);
                    sqlCommand.Parameters.AddWithValue("@OperationType", "D");
                }

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Get all PaySalaryAllowance
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<PaySalaryAllowance> GetAllPaySalaryAllowance()
        {
            List<PaySalaryAllowance> paySalaryAllowanceList = null;
            try
            {
                paySalaryAllowanceList = new List<PaySalaryAllowance>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_Select_PaySalaryAllowance", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySalaryAllowance paySalaryAllowance;
                    while (sqlDataReader.Read())
                    {
                        paySalaryAllowance = new PaySalaryAllowance();

                        paySalaryAllowance.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        paySalaryAllowance.PaySalaryAllowanceName_1 = sqlDataReader["PaySalaryAllowanceName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_1"].ToString().Trim();
                        paySalaryAllowance.PaySalaryAllowanceName_2 = sqlDataReader["PaySalaryAllowanceName_2"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_2"].ToString().Trim();
                        paySalaryAllowance.PaySalaryAllowanceName_3 = sqlDataReader["PaySalaryAllowanceName_3"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_3"].ToString().Trim();

                        paySalaryAllowance.TransactionDate = sqlDataReader["TransactionDate"].ToString() != "" ? DateTime.Parse(sqlDataReader["TransactionDate"].ToString()).ToString("dd/MM/yyyy") : "";

                        paySalaryAllowance.PaySalaryAllowanceShortName_1 = sqlDataReader["PaySalaryAllowanceShortName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceShortName_1"].ToString().Trim();
                        paySalaryAllowance.PaySalaryAllowanceShortName_2 = sqlDataReader["PaySalaryAllowanceShortName_2"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceShortName_2"].ToString().Trim();
                        paySalaryAllowance.PaySalaryAllowanceShortName_3 = sqlDataReader["PaySalaryAllowanceShortName_3"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceShortName_3"].ToString().Trim();

                        paySalaryAllowance.AccountCode = sqlDataReader["AccountCode"] == DBNull.Value ? "" : sqlDataReader["AccountCode"].ToString().Trim();

                        paySalaryAllowance.IsAddition = Convert.ToBoolean(sqlDataReader["IsAddition"] == DBNull.Value ? null : sqlDataReader["IsAddition"].ToString());
                        paySalaryAllowance.IsIncludeInDeduction = Convert.ToBoolean(sqlDataReader["IsIncludeInDeduction"] == DBNull.Value ? null : sqlDataReader["IsIncludeInDeduction"].ToString());
                        paySalaryAllowance.Sequence = Convert.ToInt32(sqlDataReader["Sequence"] == DBNull.Value ? "0" : sqlDataReader["Sequence"].ToString());

                        paySalaryAllowance.IsVacactionDeductible = Convert.ToBoolean(sqlDataReader["IsVacactionDeductible"] == DBNull.Value ? null : sqlDataReader["IsVacactionDeductible"].ToString());
                        paySalaryAllowance.IsExcludeInJV = Convert.ToBoolean(sqlDataReader["IsExcludeInJV"] == DBNull.Value ? null : sqlDataReader["IsExcludeInJV"].ToString());
                        paySalaryAllowance.IsLockedUpdate = Convert.ToBoolean(sqlDataReader["IsLockedUpdate"]);
                        paySalaryAllowance.IsLockedDelete = Convert.ToBoolean(sqlDataReader["IsLockedDelete"]);
                        paySalaryAllowanceList.Add(paySalaryAllowance);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySalaryAllowanceList;
        }


        public DataSet GetAllPaySalaryAllowanceDatasSet()
        {

            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            string Query = "SELECT PaySalaryAllowanceName_1 AS [Allowance Name (En)],PaySalaryAllowanceName_2 AS [Allowance Name (Ar)] ," +
                        "PaySalaryAllowanceName_3 AS [Allowance Name (Fr)],PaySalaryAllowanceShortName_1 AS [Short Name (En)] ," +
                        "PaySalaryAllowanceShortName_2 AS [Short Name (Ar)],PaySalaryAllowanceShortName_3 AS [Short Name (Fr)] ," +
                        "CONVERT(VARCHAR, TransactionDate, 103) AS[Transaction Date] ,AccountCode AS[Account Code]," +
                        "Sequence,IsAddition as [Addition],CASE WHEN IsIncludeInDeduction = 1 THEN 'YES' WHEN IsIncludeInDeduction = 0 THEN 'NO' END AS[Include Deduction]," +
                        "IsActive as [Active]" +
                        " FROM[dbo].[GEN_PaySalaryAllowance] where PaySalaryAllowanceID > 0  order by [Allowance Name (En)] asc";
            SqlDataAdapter da = new SqlDataAdapter(Query, sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;

        }
        public PaySalaryAllowance GetPaySalaryAllowance(int id)
        {
            PaySalaryAllowance paySalaryAllowance = new PaySalaryAllowance();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_PaySalaryAllowanceId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        paySalaryAllowance = new PaySalaryAllowance();

                        paySalaryAllowance.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        paySalaryAllowance.PaySalaryAllowanceName_1 = sqlDataReader["PaySalaryAllowanceName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        paySalaryAllowance.PaySalaryAllowanceName_2 = sqlDataReader["PaySalaryAllowanceName_2"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_2"].ToString();
                        paySalaryAllowance.PaySalaryAllowanceName_3 = sqlDataReader["PaySalaryAllowanceName_3"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_3"].ToString();
                        paySalaryAllowance.TransactionDate = sqlDataReader["TransactionDate"] == DBNull.Value ? "" : Convert.ToDateTime(sqlDataReader["TransactionDate"].ToString()).ToString("dd/MM/yyyy");
                        paySalaryAllowance.PaySalaryAllowanceShortName_1 = sqlDataReader["PaySalaryAllowanceShortName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceShortName_1"].ToString();
                        paySalaryAllowance.PaySalaryAllowanceShortName_2 = sqlDataReader["PaySalaryAllowanceShortName_2"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceShortName_2"].ToString();
                        paySalaryAllowance.PaySalaryAllowanceShortName_3 = sqlDataReader["PaySalaryAllowanceShortName_3"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceShortName_3"].ToString();
                        paySalaryAllowance.AccountCode = sqlDataReader["AccountCode"] == DBNull.Value ? "" : sqlDataReader["AccountCode"].ToString();
                        paySalaryAllowance.VacationAccountCode = sqlDataReader["VacationAccountCode"] == DBNull.Value ? "" : sqlDataReader["VacationAccountCode"].ToString();
                        paySalaryAllowance.IsAddition = Convert.ToBoolean(sqlDataReader["IsAddition"] == DBNull.Value ? null : sqlDataReader["IsAddition"].ToString());
                        paySalaryAllowance.IsIncludeInDeduction = Convert.ToBoolean(sqlDataReader["IsIncludeInDeduction"] == DBNull.Value ? null : sqlDataReader["IsIncludeInDeduction"].ToString());
                        paySalaryAllowance.Sequence = Convert.ToInt32(sqlDataReader["Sequence"] == DBNull.Value ? "0" : sqlDataReader["Sequence"].ToString());
                        paySalaryAllowance.IsVacactionDeductible = Convert.ToBoolean(sqlDataReader["IsVacactionDeductible"] == DBNull.Value ? null : sqlDataReader["IsVacactionDeductible"].ToString());
                        paySalaryAllowance.IsDeductableOnHireDate = Convert.ToBoolean(sqlDataReader["IsDeductableOnHireDate"] == DBNull.Value ? "false" : sqlDataReader["IsDeductableOnHireDate"].ToString());
                        paySalaryAllowance.IsAccommodationAllowance = Convert.ToBoolean(sqlDataReader["IsAccommodationAllowance"] == DBNull.Value ? "false" : sqlDataReader["IsAccommodationAllowance"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return paySalaryAllowance;
        }

        public List<PaySalaryAllowance> GetAllPaySalaryAllowanceList(bool Active, bool Addition)
        {
            List<PaySalaryAllowance> paySalaryAllowanceList = null;
            try
            {
                paySalaryAllowanceList = new List<PaySalaryAllowance>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAll_HR_PaySalaryAllowance", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@Active", Active);
                sqlCommand.Parameters.AddWithValue("@Addition", Addition);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySalaryAllowance paySalaryAllowance;
                    while (sqlDataReader.Read())
                    {
                        paySalaryAllowance = new PaySalaryAllowance();

                        paySalaryAllowance.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        paySalaryAllowance.PaySalaryAllowanceName_1 = sqlDataReader["PaySalaryAllowanceName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        paySalaryAllowanceList.Add(paySalaryAllowance);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySalaryAllowanceList;
        }
        #endregion

        public bool CheckPayAllowanceAllocation(int id)
        {
            bool returnval = true;
            DataAccess.GeneralDB.DataHelper dataHelper = new GeneralDB.DataHelper();
            string query = "select count(PaySalaryId) as count from hr_paySalary where IsActive = 1 AND PaySalaryAllowanceID = " + id;
            DataTable dt = dataHelper.ExcuteCommandText(query);
            int count = Convert.ToInt32(dt.Rows[0]["count"].ToString());
            dt = new DataTable();
            if (count > 0)
                returnval = false;
            else
                returnval = true;

            return returnval;
        }

        public DataSet GetPayAdditionDataTable(int EmployeeId)
        {
            sqlConnection = new SqlConnection(connectionString);
            DataTable dt = new DataTable();
            PayrollDB objPayrollDB = new PayrollDB();
            int noOfDecimals = 2;
            noOfDecimals = objPayrollDB.GetDigitAfterDecimal();
            string query = "SELECT  PaySalaryAllowanceName_1 as [Addition Type], " +
                            "ED.FirstName_1 + ' ' + ED.SurName_1  As[Employee Name] , " +
                            "CONVERT(nvarchar(11), AdditionDate, 103) as Date, " +
                            "CONVERT(Numeric(18," + noOfDecimals + "),IsNull(Amount, 0)) As Amount, " +
                            "IsNull (Description, '') As Description," +
                            "IsNull (PAYADD.IsActive, 0) As [Active] " +
                            "FROM [dbo].HR_PayAddition PAYADD " +
                            "LEFT OUTER JOIN [GEN_PaySalaryAllowance] ALLOW on PAYADD.PaySalaryAllowanceID = ALLOW.PaySalaryAllowanceID " +
                            "inner join HR_EmployeeDetail ED on PAYADD.EmployeeID = ED.EmployeeID " +
                            "where PAYADD.EmployeeID = " + EmployeeId + " order by [Addition Type]";
            SqlDataAdapter da = new SqlDataAdapter(query, sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        #region DashboardFlash
        public List<PaySalaryAllowance> GetDashboardFlashForAdditions()
        {
            List<PaySalaryAllowance> paySalaryAllowanceList = null;
            try
            {
                paySalaryAllowanceList = new List<PaySalaryAllowance>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("DashboardFlashForPayRollAdditions", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySalaryAllowance paySalaryAllowance;
                    while (sqlDataReader.Read())
                    {
                        paySalaryAllowance = new PaySalaryAllowance();

                        paySalaryAllowance.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        paySalaryAllowance.PaySalaryAllowanceName_1 = sqlDataReader["PaySalaryAllowanceName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        paySalaryAllowance.CumulativeAddition = Convert.ToDecimal(sqlDataReader["CummulativeAmount"].ToString());
                        paySalaryAllowanceList.Add(paySalaryAllowance);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySalaryAllowanceList;
        }

        #endregion

        public bool ValidateCycleBeforeActivate(int CycleId)
        {
            bool isFinal = false;
            try
            {
                DocumentsModel documentModel = new DocumentsModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select top 1 * from HR_payactivesalary where Cycle=@CycleId order by PayActiveSalaryID desc", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@CycleId", CycleId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        isFinal = sqlDataReader["final"].ToString() == "1" ? true : false;
                    }
                }
                sqlConnection.Close();
                return isFinal;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public bool ValidateCycleBeforeInsertTion(string FromDate, string ToDate, int PayCycleID)
        {
            bool isValid = false;
            try
            {
                DocumentsModel documentModel = new DocumentsModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Sp_ValidataeCycleInsertUpdate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FromDate", CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@cycleId", PayCycleID);
                SqlParameter isSuccess = new SqlParameter("@isSuccees", SqlDbType.Bit);
                isSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(isSuccess);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                isValid = Convert.ToBoolean(isSuccess.Value.ToString());
                return isValid;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public bool CheckDeductibleSalaryAllowancePresent()
        {
            bool returnval = true;
            DataAccess.GeneralDB.DataHelper dataHelper = new GeneralDB.DataHelper();
            string query = "select count(PaySalaryAllowanceID) as count from GEN_PaySalaryAllowance where IsActive = 1 AND isIncludeInDeduction =1 ";
            DataTable dt = dataHelper.ExcuteCommandText(query);
            int count = Convert.ToInt32(dt.Rows[0]["count"].ToString());
            dt = new DataTable();
            if (count > 0)
                returnval = true;
            else
                returnval = false;

            return returnval;
        }

        public PayAdditionSettingsModel GetPayAdditionSettings()
        {
            PayAdditionSettingsModel payAdditionSettingsModel = new PayAdditionSettingsModel(); ;
            dataHelper = new DataHelper();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@Mode", 1));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParmeterList, "Hr_Stp_CURD_PayAdditionSetting");
            if (dt.Rows.Count > 0)
            {

                payAdditionSettingsModel.OverTimeWorkingHours = float.Parse(dt.Rows[0]["OverTimeWorkingHours"].ToString() == "" ? "0" : dt.Rows[0]["OverTimeWorkingHours"].ToString());
                payAdditionSettingsModel.OverTimeWorkingHoursExtra = float.Parse(dt.Rows[0]["OvertimeExtra"].ToString() == "" ? "0" : dt.Rows[0]["OvertimeExtra"].ToString());
                payAdditionSettingsModel.OverTimeWorkingHoursExtra1 = float.Parse(dt.Rows[0]["OvertimeExtra1"].ToString() == "" ? "0" : dt.Rows[0]["OvertimeExtra1"].ToString());
                payAdditionSettingsModel.PerDayFixAmount= Convert.ToDouble(dt.Rows[0]["PerDayFixAmount"].ToString() == null ? "0" : dt.Rows[0]["PerDayFixAmount"].ToString());
            }
            return payAdditionSettingsModel;
        }

        public OperationDetails SavePayrollConfirmationRequestForAddition(PayAddition payAdditionModel, int RequestEmployeeId, int Mode)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_PayrollRequestForPayAddition", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayAdditionID", payAdditionModel.PayAdditionID);
                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceID", payAdditionModel.PaySalaryAllowanceID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", payAdditionModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@AdditionDate", CommonDB.SetCulturedDate(payAdditionModel.AdditionDate));
                sqlCommand.Parameters.AddWithValue("@Amount", payAdditionModel.Amount);
                sqlCommand.Parameters.AddWithValue("@Description", payAdditionModel.Description);
                sqlCommand.Parameters.AddWithValue("@IsActive", payAdditionModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@TransactionDate", DateTime.Now);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", payAdditionModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequestEmployeeId", @RequestEmployeeId);
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                if (Mode != 3)
                {
                    return new OperationDetails(true, "Payroll request for addition details saved successfully.", null, id);
                }
                else
                {
                    return new OperationDetails(true, "Payroll request for deleted addition details saved successfully.", null, id);
                }
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
            }
            finally
            {

            }
        }

    }
}
