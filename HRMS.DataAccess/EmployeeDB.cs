﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using HRMS.Entities.ViewModel;
using System.Globalization;
using Microsoft.ApplicationBlocks.Data;


namespace HRMS.DataAccess
{
    public class EmployeeDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        public DataAccess.GeneralDB.DataHelper dataHelper;

        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        //public List<EmployeeVisaInformationReporting> GetALLEmployeeListForVisaInformationReporting()
        //{
        //    List<EmployeeVisaInformationReporting> employeeList = null;
        //    try
        //    {
        //        employeeList = new List<EmployeeVisaInformationReporting>();

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("TestCaseStimulsoftPrototypeEmployeeDetailListing", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        EmployeeVisaInformationReporting employeeModel = new EmployeeVisaInformationReporting();
        //        //employeeModel.AlternativeId = "Sr. No.";
        //        //employeeModel.AlternativeId = "Alternative ID";
        //        //employeeModel.FullName = "Full Name";
        //        //employeeModel.Country = "Country";
        //        //employeeModel.EmailAddress = "Email Address";
        //        //employeeModel.CompanyName = "Company Name";
        //        //employeeModel.Country = "Visa Number";
        //        //employeeModel.EmailAddress = "Visa Issue Date";
        //        //employeeModel.CompanyName = "Visa Expiration Date";
        //        //employeeList.Add(employeeModel);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {

        //            while (sqlDataReader.Read())
        //            {
        //                employeeModel = new EmployeeVisaInformationReporting();
        //                employeeModel.Row = Convert.ToString(sqlDataReader["Row"] == null ? "0" : sqlDataReader["Row"]);
        //                employeeModel.AlternativeId = Convert.ToString(sqlDataReader["AlternativeId"] == null ? "0" : sqlDataReader["AlternativeId"]);
        //                employeeModel.FullName = Convert.ToString(sqlDataReader["FullName"] == null ? "" : sqlDataReader["FullName"]);
        //                employeeModel.Country = Convert.ToString(sqlDataReader["Country"] == null ? "" : sqlDataReader["Country"]);
        //                employeeModel.EmailAddress = Convert.ToString(sqlDataReader["EmailAddress"] == null ? "" : sqlDataReader["EmailAddress"]);
        //                employeeModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"] == null ? "" : sqlDataReader["CompanyName"]);
        //                employeeModel.VisaNumber = Convert.ToString(sqlDataReader["VisaNumber"] == null ? "" : sqlDataReader["VisaNumber"]);
        //                employeeModel.VisaIssueDate = Convert.ToString(sqlDataReader["VisaIssueDate"] == null ? "" : sqlDataReader["VisaIssueDate"]);
        //                employeeModel.VisaExpirationDate = Convert.ToString(sqlDataReader["VisaExpirationDate"] == null ? "" : sqlDataReader["VisaExpirationDate"]);
        //                employeeList.Add(employeeModel);
        //            }
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return employeeList;
        //}
        public List<Employee> GetAllEmployeeList(int UserId, Boolean? isActive, int? companyId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Employee", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", UserId));
                sqlCommand.Parameters.Add(new SqlParameter("@isActive", isActive));
                if (companyId.HasValue && companyId.Value > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyId", companyId));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.employeeContactModel = new EmployeeContactModel();
                        employeeModel.employmentInformation = new EmploymentInformation();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.employeeDetailsModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlternativeID"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.MiddleName = Convert.ToString(sqlDataReader["MiddleName"] == DBNull.Value ? "" : sqlDataReader["MiddleName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.genderModel.GenderName_1 = Convert.ToString(sqlDataReader["GenderName"] == DBNull.Value ? "" : sqlDataReader["GenderName"]);
                        employeeModel.employmentInformation.PositionName = Convert.ToString(sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"]).Trim();
                        employeeModel.employeeContactModel.MobileNumber = Convert.ToString(sqlDataReader["MobileNumberHome"] == DBNull.Value ? "" : sqlDataReader["MobileNumberHome"]).Trim();
                        employeeModel.employeeDetailsModel.BirthDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DOB"].ToString());
                        employeeModel.employeeDetailsModel.isActive = Convert.ToBoolean(sqlDataReader["isActive"]);
                        //2019-02-11
                        employeeModel.employmentInformation.HireDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HireDate"].ToString());
                        employeeList.Add(employeeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public DataSet GetEmployeeDataSet(string employeeStatus, int UserId,int? companyId, out int totalCount)
        {

            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_stp_GetEmployeeListBySearch", sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@isactive", employeeStatus);
            da.SelectCommand.Parameters.AddWithValue("@search", "");
            da.SelectCommand.Parameters.AddWithValue("@UserId", UserId);
            if (companyId.HasValue && companyId.Value > 0)
                da.SelectCommand.Parameters.Add(new SqlParameter("@CompanyId", companyId));

            SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
            TotalCount.Direction = ParameterDirection.Output;
            da.SelectCommand.Parameters.Add(TotalCount);
            DataSet ds = new DataSet();
            da.Fill(ds);
            totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;
            return ds;

        }
        public int GetFirstActiveEmployee()
        {
            int EmployeeId = 0;
            try
            {


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_Top_Active_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;





                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {


                        EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());

                    }
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeId;
        }

        public List<Employee> GetEmployeeList()
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Employee_List", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();
                        employeeModel.FullName = employeeModel.FirstName + " " + employeeModel.LastName;
                        employeeList.Add(employeeModel);
                    }
                    //sqlConnection.Close();

                    //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<Employee> GetALLEmployeeListWithPermission(int UserId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Stp_Select_All_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();

                        employeeModel.FullName = employeeModel.FirstName + " " + employeeModel.LastName;

                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);

                        employeeList.Add(employeeModel);
                    }
                }
                //sqlConnection.Close();

                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<Employee> GetALLEmployeeListWithAlternatioveId(int UserId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Stp_Select_All_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["FullName"] == DBNull.Value ? "" : sqlDataReader["FullName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<Employee> GetALLEmployeeListAsPerCycleId(int? CycleId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetAllEmployeeAsPerCycle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayCycleId", CycleId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.FullName = "(" + employeeModel.EmployeeAlternativeID + ") " + employeeModel.FirstName + " " + employeeModel.LastName;
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<Employee> GetALLEmployeeListNotinSelection(string EmployeeIds)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_Select_All_EmployeeNotInSelection", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SelectedEmployeeIds", EmployeeIds);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();

                        employeeModel.FullName = employeeModel.FirstName + " " + employeeModel.LastName;

                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);

                        employeeList.Add(employeeModel);
                    }
                }
                //sqlConnection.Close();

                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<Employee> GetALLEmployeeListWitFullName()
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Stp_Select_All_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.MiddleName = Convert.ToString(sqlDataReader["MiddleName"] == DBNull.Value ? "" : sqlDataReader["MiddleName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.FullName = "(" + employeeModel.EmployeeAlternativeID + ") " + employeeModel.FirstName + " " + employeeModel.MiddleName + " " + employeeModel.LastName;
                        employeeList.Add(employeeModel);
                    }
                }
                //sqlConnection.Close();

                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }
        public List<AirFareClasses> GetAllAirFareClasses()
        {
            List<AirFareClasses> AirfareClasseslist = new List<AirFareClasses>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeAirfareClasses", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    AirFareClasses AirFareClassesModel;
                    while (sqlDataReader.Read())
                    {
                        AirFareClassesModel = new AirFareClasses();
                        AirFareClassesModel.AirFareClassId = Convert.ToInt32(sqlDataReader["AirFareClassId"].ToString());
                        AirFareClassesModel.AirFareClassName = Convert.ToString(sqlDataReader["AirfareClassName"]);
                        AirfareClasseslist.Add(AirFareClassesModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return AirfareClasseslist;
        }
        public List<Employee> GetALLEmployeeByUserId(int userId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_GetEmployeesByUserId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.MiddleName = Convert.ToString(sqlDataReader["MiddleName"] == DBNull.Value ? "" : sqlDataReader["MiddleName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        var companyShortName = Convert.ToString(sqlDataReader["CompanyShortName"] == DBNull.Value ? "" : (" - " + sqlDataReader["CompanyShortName"]));
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.FullName = "(" + employeeModel.EmployeeAlternativeID + ") " + employeeModel.FirstName + " " + employeeModel.MiddleName + " " + employeeModel.LastName + companyShortName;
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<Employee> GetALLEmployeeByUserIdAndSectionId(int userId, int? sectionId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_GetEmployeesBySectionID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@employeeSectionID", sectionId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeModel.IsActive = Convert.ToInt32(sqlDataReader["isactive"]);
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.FullName = "(" + employeeModel.EmployeeAlternativeID + ") " + employeeModel.FirstName + " " + employeeModel.LastName;
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public string AddEmployee(Employee employeeModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FirstName", employeeModel.FirstName);
                sqlCommand.Parameters.AddWithValue("@LastName", employeeModel.LastName);
                sqlCommand.Parameters.AddWithValue("@CompanyId", employeeModel.CompanyId);
                //sqlCommand.Parameters.AddWithValue("@EmailId", employeeModel.EmailId);
                //sqlCommand.Parameters.AddWithValue("@PhoneNo", employeeModel.PhoneNo);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", employeeModel.CreatedBy);



                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public OperationDetails AddEmailType(string EmailTypeName)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Gen_InsertEmailType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Name", EmailTypeName);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                op.Message = "Email type added successfully";
                op.Success = true;

            }
            catch (Exception exception)
            {
                op.Message = "Error while adding email type!";
                op.Success = true;
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }


        public string UpdateEmployee(Employee employeeModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@FirstName", employeeModel.FirstName);
                sqlCommand.Parameters.AddWithValue("@LastName", employeeModel.LastName);
                sqlCommand.Parameters.AddWithValue("@CompanyId", employeeModel.CompanyId);
                //sqlCommand.Parameters.AddWithValue("@EmailId", employeeModel.EmailId);
                //sqlCommand.Parameters.AddWithValue("@PhoneNo", employeeModel.PhoneNo);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeModel.ModifiedBy);



                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        public string DeleteEmployee(Employee employeeModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeModel.EmployeeId);



                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        public List<EmployeeAddressInfoModel> GetAddressTypeList()
        {
            List<EmployeeAddressInfoModel> EmployeeAddressInfoModelList = null;
            try
            {
                EmployeeAddressInfoModelList = new List<EmployeeAddressInfoModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeAddressTypeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;





                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeAddressInfoModel EmployeeAddressInfoModel;
                    while (sqlDataReader.Read())
                    {
                        EmployeeAddressInfoModel = new EmployeeAddressInfoModel();

                        EmployeeAddressInfoModel.AddressType = Convert.ToInt32(sqlDataReader["EmployeeAddressTypeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAddressTypeID"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        EmployeeAddressInfoModel.AddressTypeName = Convert.ToString(sqlDataReader["AddressTypeName"] == DBNull.Value ? "" : sqlDataReader["AddressTypeName"]);


                        EmployeeAddressInfoModelList.Add(EmployeeAddressInfoModel);
                    }
                }
                //sqlConnection.Close();

                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeAddressInfoModelList;
        }


        public Employee GetEmployee(int id)
        {
            Employee employeeModel = new Employee();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return employeeModel;
        }

        public List<Employee> GetNonCredentialsEmployee(int employeeId)
        {
            List<Employee> objEmployeeList = new List<Employee>();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_NonCredential_Employee", sqlConnection);
                sqlCommand.Parameters.AddWithValue("EmployeeId", employeeId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        objEmployeeList.Add(employeeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return objEmployeeList;
        }

        /// <summary>
        /// Delete Employee Details by EmployeeId
        /// </summary>
        /// <param name="employeeModel"></param>
        /// <returns></returns>
        public string DeleteEmployeeDetails(Employee employeeModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_DeleteEmployeeDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //EmployeeDetails Table
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeModel.employeeDetailsModel.EmployeeID);
                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.Int);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        Message = sqlDataReader["NewID"].ToString();
                    }

                }

                if (Message == "-1")
                {
                    Message = "Error";
                }
                else
                {
                    Message = "Success";
                }

                sqlConnection.Close();

                //Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Insert/Update Employee Personal Information
        /// </summary>
        /// <param name="employeeDetailsModel">EmployeeDetailsModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmployeePersonalInformation(EmployeeDetailsModel employeeDetailsModel)
        {
            string Message = string.Empty;
            string strNationality = string.Empty;
            int iResult = 0;
            try
            {
                // Set Employee Roles as Comma separated string
                foreach (var item in employeeDetailsModel.SelectedValues)
                {
                    strNationality += item + ",";
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmployeeDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeDetailsModel.EmployeeID == null ? 0 : employeeDetailsModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", employeeDetailsModel.CompanyID == null ? 0 : employeeDetailsModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@EmployeeAlternativeID", employeeDetailsModel.EmployeeAlternativeID == null ? "" : employeeDetailsModel.EmployeeAlternativeID);
                sqlCommand.Parameters.AddWithValue("@TitleID", employeeDetailsModel.TitleID == null ? 0 : employeeDetailsModel.TitleID);
                sqlCommand.Parameters.AddWithValue("@FirstName_1", employeeDetailsModel.FirstName_1 == null ? "" : employeeDetailsModel.FirstName_1);

                sqlCommand.Parameters.AddWithValue("@FirstName_2", employeeDetailsModel.FirstName_2 == null ? "" : employeeDetailsModel.FirstName_2);
                sqlCommand.Parameters.AddWithValue("@FirstName_3", employeeDetailsModel.FirstName_3 == null ? "" : employeeDetailsModel.FirstName_3);
                sqlCommand.Parameters.AddWithValue("@SurName_1", employeeDetailsModel.SurName_1 == null ? "" : employeeDetailsModel.SurName_1);
                sqlCommand.Parameters.AddWithValue("@SurName_2", employeeDetailsModel.SurName_2 == null ? "" : employeeDetailsModel.SurName_2);
                sqlCommand.Parameters.AddWithValue("@SurName_3", employeeDetailsModel.SurName_3 == null ? "" : employeeDetailsModel.SurName_3);

                sqlCommand.Parameters.AddWithValue("@MiddleName_1", employeeDetailsModel.MiddleName_1 == null ? "" : employeeDetailsModel.MiddleName_1);
                sqlCommand.Parameters.AddWithValue("@MiddleName_2", employeeDetailsModel.MiddleName_2 == null ? "" : employeeDetailsModel.MiddleName_2);
                sqlCommand.Parameters.AddWithValue("@MiddleName_3", employeeDetailsModel.MiddleName_3 == null ? "" : employeeDetailsModel.MiddleName_3);
                sqlCommand.Parameters.AddWithValue("@MothersName_1", employeeDetailsModel.MothersName_1 == null ? "" : employeeDetailsModel.MothersName_1);
                sqlCommand.Parameters.AddWithValue("@MothersName_2", employeeDetailsModel.MothersName_2 == null ? "" : employeeDetailsModel.MothersName_2);

                sqlCommand.Parameters.AddWithValue("@MothersName_3", employeeDetailsModel.MothersName_3 == null ? "" : employeeDetailsModel.MothersName_3);
                sqlCommand.Parameters.AddWithValue("@GenderID", employeeDetailsModel.GenderID == null ? 0 : employeeDetailsModel.GenderID);
                sqlCommand.Parameters.AddWithValue("@MaritalStatusID", employeeDetailsModel.MaritalStatusID == null ? 0 : employeeDetailsModel.MaritalStatusID);
                sqlCommand.Parameters.AddWithValue("@NationalityID", employeeDetailsModel.NationalityID);
                sqlCommand.Parameters.AddWithValue("@NationalityIDCardNo", employeeDetailsModel.NationalityIDCardNo);
                sqlCommand.Parameters.AddWithValue("@DefaultNationalityID", employeeDetailsModel.DefaultNationalityID);
                sqlCommand.Parameters.AddWithValue("@ReligionID", employeeDetailsModel.ReligionID == null ? 0 : employeeDetailsModel.ReligionID);
                sqlCommand.Parameters.AddWithValue("@BirthDate", CommonDB.SetCulturedDate(employeeDetailsModel.BirthDate));
                sqlCommand.Parameters.AddWithValue("@BirthPlaceID", employeeDetailsModel.BirthPlaceID == null ? 0 : employeeDetailsModel.BirthPlaceID);
                sqlCommand.Parameters.AddWithValue("@BirthCountryID", employeeDetailsModel.BirthCountryID == null ? 0 : employeeDetailsModel.BirthCountryID);
                sqlCommand.Parameters.AddWithValue("@isSpecialNeeds", employeeDetailsModel.isSpecialNeeds == null ? false : employeeDetailsModel.isSpecialNeeds);

                sqlCommand.Parameters.AddWithValue("@UserID", employeeDetailsModel.UserID == null ? 0 : employeeDetailsModel.UserID);
                sqlCommand.Parameters.AddWithValue("@FamilyID", employeeDetailsModel.FamilyID == null ? 0 : employeeDetailsModel.FamilyID);
                sqlCommand.Parameters.AddWithValue("@isActive", employeeDetailsModel.isActive == null ? true : employeeDetailsModel.isActive);
                sqlCommand.Parameters.AddWithValue("@usertypeid", employeeDetailsModel.usertypeid == null ? 0 : employeeDetailsModel.usertypeid);

                sqlCommand.Parameters.AddWithValue("@MailGenerated", employeeDetailsModel.MailGenerated == null ? 0 : employeeDetailsModel.MailGenerated);
                sqlCommand.Parameters.AddWithValue("@isOnlineActive", employeeDetailsModel.isOnlineActive == null ? false : employeeDetailsModel.isOnlineActive);
                sqlCommand.Parameters.AddWithValue("@InitialPassword", employeeDetailsModel.InitialPassword == null ? "" : employeeDetailsModel.InitialPassword);
                sqlCommand.Parameters.AddWithValue("@ProfileImage", employeeDetailsModel.ProfileImage == null ? "" : employeeDetailsModel.ProfileImage);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeDetailsModel.ModifiedBy == null ? 0 : employeeDetailsModel.ModifiedBy);

                //sqlCommand.Parameters.AddWithValue("@NormalPassword", employeeDetailsModel.NormalPassword == null ? "" : employeeDetailsModel.NormalPassword);
                sqlCommand.Parameters.AddWithValue("@Password", employeeDetailsModel.Password == null ? "" : employeeDetailsModel.Password);
                if (employeeDetailsModel.HomeLanguageID != null)
                    sqlCommand.Parameters.AddWithValue("@HomelanguageId", employeeDetailsModel.HomeLanguageID);
                else
                    sqlCommand.Parameters.AddWithValue("@HomelanguageId", DBNull.Value);
                if (employeeDetailsModel.CommunicationLanguageID != null)
                    sqlCommand.Parameters.AddWithValue("@CommunicationLanguageID", employeeDetailsModel.CommunicationLanguageID);
                else
                    sqlCommand.Parameters.AddWithValue("@CommunicationLanguageID", DBNull.Value);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                // Insert Spoken Languages
                //sqlConnection.Open();                
                sqlDataReader.Close();
                if (employeeDetailsModel.SpokenLanguages != null)
                {
                    foreach (var item in employeeDetailsModel.SpokenLanguages)
                    {
                        sqlCommand = new SqlCommand("stp_HR_InsertSpokenLanguages", sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        // Passing values
                        sqlCommand.Parameters.AddWithValue("@EmployeeID", iResult);
                        sqlCommand.Parameters.AddWithValue("@LanguageId", item.LanguageId);
                        sqlCommand.Parameters.AddWithValue("@IsSpeak", item.IsSpeak);
                        sqlCommand.Parameters.AddWithValue("@IsRead", item.IsRead);
                        sqlCommand.Parameters.AddWithValue("@IsWrite", item.IsWrite);
                        sqlCommand.Parameters.AddWithValue("@IsNative", item.IsNative);

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Close();
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        /// <summary>
        /// Insert/Update Employee Contact Details
        /// </summary>
        /// <param name="employeeContactModel">EmployeeContactModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmployeeContactDetails(EmployeeContactModel employeeContactModel, EmployeeAddressInfoModel AddressModel, IList<string> txtDynamicEmails, IList<string> txtDynamicPersonalEmail, IList<string> ddDynamicEmailType, IList<string> txtDynamicIM, IList<string> ddDynamicIMType, IList<string> txtDynamicMobile, IList<string> txtDynamicPhone, IList<string> ddDynamicMobileType)
        {
            string Message = string.Empty;
            int iResult = 0;

            EmployeeWorkEmailSetting objMailsetting = new EmployeeWorkEmailSetting();
            EmployeeDB objEmpDB = new EmployeeDB();
            objMailsetting = objEmpDB.GetEmployeeWorkEmailSetting();
            DataAccess.GeneralDB.CommonDB commonDb = new CommonDB();
            string Formula = commonDb.WorkEmailFirstOption.Where(x => x.Key == objMailsetting.WorkEmailFirstOption).FirstOrDefault().Value + "+'" +
                           commonDb.WorkEmailSecondOption.Where(x => x.Key == objMailsetting.WorkEmailSecondOption).FirstOrDefault().Value + "'+" +
                           commonDb.WorkEmailThirdOption.Where(x => x.Key == objMailsetting.WorkEmailThirdOption).FirstOrDefault().Value;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmployeeContact", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeContactModel.EmployeeID);

                sqlCommand.Parameters.AddWithValue("@AddressLine1_1", AddressModel.AddressLine == null ? "" : AddressModel.AddressLine);
                sqlCommand.Parameters.AddWithValue("@AddressLine2_1", AddressModel.AddressLine2 == null ? "" : AddressModel.AddressLine2);

                sqlCommand.Parameters.AddWithValue("@AddressLine1_2", AddressModel.AddressLine1_2 == null ? "" : AddressModel.AddressLine1_2);
                sqlCommand.Parameters.AddWithValue("@AddressLine2_2", AddressModel.AddressLine2_2 == null ? "" : AddressModel.AddressLine2_2);

                sqlCommand.Parameters.AddWithValue("@AddressLine1_3", AddressModel.AddressLine1_3 == null ? "" : AddressModel.AddressLine1_3);
                sqlCommand.Parameters.AddWithValue("@AddressLine2_3", AddressModel.AddressLine2_3 == null ? "" : AddressModel.AddressLine2_3);

                sqlCommand.Parameters.AddWithValue("@AppartmentNumber", AddressModel.ApartmentNo == null ? "" : AddressModel.ApartmentNo);
                sqlCommand.Parameters.AddWithValue("@POBOX", AddressModel.PoBox == null ? "" : AddressModel.PoBox);
                sqlCommand.Parameters.AddWithValue("@CityID", AddressModel.CityID);
                sqlCommand.Parameters.AddWithValue("@AreaID", AddressModel.AreaId);
                sqlCommand.Parameters.AddWithValue("@CountryID", AddressModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@Street", AddressModel.Street == null ? "" : AddressModel.Street);
                sqlCommand.Parameters.AddWithValue("@PostCode", AddressModel.PostCode == null ? "" : AddressModel.PostCode);
                sqlCommand.Parameters.AddWithValue("@StateID", AddressModel.StateID);
                sqlCommand.Parameters.AddWithValue("@Fax", employeeContactModel.Fax == null ? "" : employeeContactModel.Fax);
                sqlCommand.Parameters.AddWithValue("@WorkEmail", employeeContactModel.WorkEmail == null ? "" : employeeContactModel.WorkEmail);
                sqlCommand.Parameters.AddWithValue("@PersonalEmail", employeeContactModel.PersonalEmail == null ? "" : employeeContactModel.PersonalEmail);

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeContactModel.ModifiedBy);

                sqlCommand.Parameters.AddWithValue("@IMContact", employeeContactModel.IMContact == null ? "" : employeeContactModel.IMContact);
                sqlCommand.Parameters.AddWithValue("@IMContactType", employeeContactModel.IMContactType);
                sqlCommand.Parameters.AddWithValue("@MobileNumberHome", employeeContactModel.MobileNumberHome == null ? "" : employeeContactModel.MobileNumberHome);
                sqlCommand.Parameters.AddWithValue("@MobileNumberWork", employeeContactModel.MobileNumberWork == null ? "" : employeeContactModel.MobileNumberWork);
                sqlCommand.Parameters.AddWithValue("@WorkEmailFormula", Formula);
                sqlCommand.Parameters.AddWithValue("@DomainName", objMailsetting.EmailDomainName);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlDataReader.Close();

                // Insert Dynamic Emails
                if (txtDynamicEmails != null)
                {
                    for (int i = 0; i < txtDynamicEmails.Count; i++)
                    {
                        sqlCommand = new SqlCommand("stp_HR_InsertEmployeeMultipleEmails", sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        // Passing values
                        sqlCommand.Parameters.AddWithValue("@EmployeeID", iResult);
                        sqlCommand.Parameters.AddWithValue("@EmailName", txtDynamicEmails[i].ToString());
                        sqlCommand.Parameters.AddWithValue("@EmailType", ddDynamicEmailType[i].ToString());

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Close();
                    }
                }
                if (txtDynamicPersonalEmail != null)
                {
                    for (int i = 0; i < txtDynamicPersonalEmail.Count; i++)
                    {
                        sqlCommand = new SqlCommand("stp_HR_InsertEmployeeMultiplePersonalEmail", sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        // Passing values
                        sqlCommand.Parameters.AddWithValue("@EmployeeID", iResult);
                        sqlCommand.Parameters.AddWithValue("@EmailName", txtDynamicPersonalEmail[i].ToString());
                        //  sqlCommand.Parameters.AddWithValue("@EmailType", ddDynamicEmailType[i].ToString());

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Close();
                    }
                }

                // Insert Dynamic IMs
                if (txtDynamicIM != null)
                {
                    for (int i = 0; i < txtDynamicIM.Count; i++)
                    {
                        sqlCommand = new SqlCommand("stp_HR_InsertEmployeeMultipleIMs", sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        // Passing values
                        sqlCommand.Parameters.AddWithValue("@EmployeeID", iResult);
                        sqlCommand.Parameters.AddWithValue("@IMName", txtDynamicIM[i].ToString());
                        sqlCommand.Parameters.AddWithValue("@IMType", ddDynamicIMType[i].ToString());

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Close();
                    }
                }

                // Insert Dynamic Contacts
                if (txtDynamicMobile != null)
                {
                    for (int i = 0; i < txtDynamicMobile.Count; i++)
                    {
                        sqlCommand = new SqlCommand("stp_HR_InsertEmployeeMultipleContacts", sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        // Passing values
                        sqlCommand.Parameters.AddWithValue("@EmployeeID", iResult);
                        sqlCommand.Parameters.AddWithValue("@Contact", txtDynamicMobile[i].ToString());
                        sqlCommand.Parameters.AddWithValue("@ContactType", ddDynamicMobileType[i].ToString());

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Close();
                    }
                }
                if (txtDynamicPhone != null)
                {
                    for (int i = 0; i < txtDynamicPhone.Count; i++)
                    {
                        sqlCommand = new SqlCommand("stp_HR_InsertEmployeeMultiplePhone", sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        // Passing values
                        sqlCommand.Parameters.AddWithValue("@EmployeeID", iResult);
                        sqlCommand.Parameters.AddWithValue("@Contact", txtDynamicPhone[i].ToString());
                        //        sqlCommand.Parameters.AddWithValue("@ContactType", ddDynamicMobileType[i].ToString());

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Close();
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        public OperationDetails DeleteFromMultipleContactsFields(int EmployeeId)
        {
            OperationDetails op = new OperationDetails();

            return op;
        }
        /// <summary>
        /// Insert/Update Employment Information
        /// </summary>
        /// <param name="employmentInformation">EmploymentInformation model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmploymentInformation(EmploymentInformation employmentInformation)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmploymentInformation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employmentInformation.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@EmployeeCode", employmentInformation.EmployeeCode == null ? "" : employmentInformation.EmployeeCode);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", employmentInformation.DepartmentID == null ? 0 : employmentInformation.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@PositionID", employmentInformation.PositionID == null ? 0 : employmentInformation.PositionID);
                sqlCommand.Parameters.AddWithValue("@SuperviserID", employmentInformation.SuperviserID == null ? 0 : employmentInformation.SuperviserID);
                sqlCommand.Parameters.AddWithValue("@WageRate", employmentInformation.WageRate == null ? 0 : employmentInformation.WageRate);
                sqlCommand.Parameters.AddWithValue("@ClientChargeRate", employmentInformation.ClientChargeRate == null ? 0 : employmentInformation.ClientChargeRate);
                sqlCommand.Parameters.AddWithValue("@HireDate", CommonDB.SetCulturedDate(employmentInformation.HireDate));
                sqlCommand.Parameters.AddWithValue("@ResignationDate", CommonDB.SetCulturedDate(employmentInformation.ResignationDate));
                sqlCommand.Parameters.AddWithValue("@HRContractTypeID", employmentInformation.EmployeeContractTermId);
                sqlCommand.Parameters.AddWithValue("@EmployementMode", employmentInformation.EmployementMode == null ? "" : employmentInformation.EmployementMode);
                sqlCommand.Parameters.AddWithValue("@JobTitleID", employmentInformation.JobTitleID == null ? 0 : employmentInformation.JobTitleID);
                sqlCommand.Parameters.AddWithValue("@SalaryPackage", employmentInformation.SalaryPackage == null ? 0 : employmentInformation.SalaryPackage);
                sqlCommand.Parameters.AddWithValue("@HOID", employmentInformation.SchoolHouse == null ? System.Data.SqlTypes.SqlInt32.Null : employmentInformation.SchoolHouse);
                sqlCommand.Parameters.AddWithValue("@Qualification", employmentInformation.Qualification == null ? "" : employmentInformation.Qualification);

                sqlCommand.Parameters.AddWithValue("@BankName", employmentInformation.BankName == null ? "" : employmentInformation.BankName);
                sqlCommand.Parameters.AddWithValue("@AccountNumber", employmentInformation.AccountNumber == null ? "" : employmentInformation.AccountNumber);
                sqlCommand.Parameters.AddWithValue("@AccountName", employmentInformation.AccountName == null ? "" : employmentInformation.AccountName);
                sqlCommand.Parameters.AddWithValue("@BankAddress", employmentInformation.BankAddress == null ? "" : employmentInformation.BankAddress);
                sqlCommand.Parameters.AddWithValue("@SwitchBCCode", employmentInformation.SwitchBCCode == null ? "" : employmentInformation.SwitchBCCode);
                sqlCommand.Parameters.AddWithValue("@SortCode", employmentInformation.SortCode == null ? "" : employmentInformation.SortCode);
                sqlCommand.Parameters.AddWithValue("@IBANCode", employmentInformation.IBANCode == null ? "" : employmentInformation.IBANCode);

                sqlCommand.Parameters.AddWithValue("@AgentID", employmentInformation.AgentID == null ? "" : employmentInformation.AgentID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employmentInformation.ModifiedBy == null ? 0 : employmentInformation.ModifiedBy);

                sqlCommand.Parameters.AddWithValue("@CompanyId", employmentInformation.CompanyId == null ? 0 : employmentInformation.CompanyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeSectionID", employmentInformation.EmployeeSectionID == null ? 0 : employmentInformation.EmployeeSectionID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", employmentInformation.EmployeeJobCategoryID == null ? 0 : employmentInformation.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@ShiftID", employmentInformation.ModifiedBy == null ? 0 : employmentInformation.ShiftID);
                sqlCommand.Parameters.AddWithValue("@GroupNameID", employmentInformation.GroupNameID == null ? 0 : employmentInformation.GroupNameID);

                sqlCommand.Parameters.AddWithValue("@LeftDate", CommonDB.SetCulturedDate(employmentInformation.LeftDate));

                sqlCommand.Parameters.AddWithValue("@LeftReasonID", employmentInformation.LeftReasonID == null ? 0 : employmentInformation.LeftReasonID);              //Changed string to int for Left Reason dropdown
                sqlCommand.Parameters.AddWithValue("@MOETitle", employmentInformation.MOETitle == null ? 0 : employmentInformation.MOETitle);

                sqlCommand.Parameters.AddWithValue("@MOEHireDate", CommonDB.SetCulturedDate(employmentInformation.MOEHireDate));
                sqlCommand.Parameters.AddWithValue("@MOELeftDate", CommonDB.SetCulturedDate(employmentInformation.MOELeftDate));

                sqlCommand.Parameters.AddWithValue("@MOLTitle", employmentInformation.MOLTitle == null ? 0 : employmentInformation.MOLTitle);
                sqlCommand.Parameters.AddWithValue("@MOLStartDate", CommonDB.SetCulturedDate(employmentInformation.MOLStartDate));

                sqlCommand.Parameters.AddWithValue("@MOLPersonalId", employmentInformation.MOLPersonalId == null ? (object)DBNull.Value : employmentInformation.MOLPersonalId);
                sqlCommand.Parameters.AddWithValue("@EmployeeStatus", employmentInformation.EmpStatus == null ? "" : employmentInformation.EmpStatus);

                sqlCommand.Parameters.AddWithValue("@OverseasRecruited", employmentInformation.isOverseasrecruited);
                sqlCommand.Parameters.AddWithValue("@NationalityTypeId", employmentInformation.EmployeenationalityType);
                //Task#9118
                sqlCommand.Parameters.AddWithValue("@NoticePeriod", employmentInformation.NoticePeriod);
                sqlCommand.Parameters.AddWithValue("@ProjectData", employmentInformation.ProjectData == null ? (object)DBNull.Value : employmentInformation.ProjectData);
                sqlCommand.Parameters.AddWithValue("@JobGradeID", employmentInformation.JobGradeID == null ? (object)DBNull.Value : employmentInformation.JobGradeID);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", employmentInformation.ContractStatus == null ? (object)DBNull.Value : employmentInformation.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", employmentInformation.FamilySpouse == null ? (object)DBNull.Value : employmentInformation.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", employmentInformation.AnnualAirTicket == null ? (object)DBNull.Value : employmentInformation.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", employmentInformation.HealthInsurance == null ? (object)DBNull.Value : employmentInformation.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", employmentInformation.LifeInsurance == null ? (object)DBNull.Value : employmentInformation.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalaryBasisID", employmentInformation.SalaryBasisID == null ? (object)DBNull.Value : employmentInformation.SalaryBasisID);
                sqlCommand.Parameters.AddWithValue("@CostCenter", employmentInformation.CostCenter == null ? (object)DBNull.Value : employmentInformation.CostCenter);
                sqlCommand.Parameters.AddWithValue("@CostCenterCode", employmentInformation.CostCenterCode == null ? (object)DBNull.Value : employmentInformation.CostCenterCode);
                sqlCommand.Parameters.AddWithValue("@LocationCode", employmentInformation.LocationCode == null ? (object)DBNull.Value : employmentInformation.LocationCode);
                sqlCommand.Parameters.AddWithValue("@OfficeLocation", employmentInformation.OfficeLocation == null ? (object)DBNull.Value : employmentInformation.OfficeLocation);
                sqlCommand.Parameters.AddWithValue("@ProbationPeriod", employmentInformation.ProbationPeriod == null ? (object)DBNull.Value : employmentInformation.ProbationPeriod);
                sqlCommand.Parameters.AddWithValue("@LeaveEntitleDaysID", employmentInformation.LeaveEntitleDaysID == null ? (object)DBNull.Value : employmentInformation.LeaveEntitleDaysID);
                sqlCommand.Parameters.AddWithValue("@LeaveEntitleTypeID", employmentInformation.LeaveEntitleTypeID == null ? (object)DBNull.Value : employmentInformation.LeaveEntitleTypeID);
                sqlCommand.Parameters.AddWithValue("@ProbationCompletionDate", string.IsNullOrEmpty(employmentInformation.ProbationCompletionDate) ? (object)DBNull.Value : Convert.ToDateTime(employmentInformation.ProbationCompletionDate));

                sqlCommand.Parameters.AddWithValue("@DivisionId", employmentInformation.DivisionID == null ? (object)DBNull.Value : employmentInformation.DivisionID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", employmentInformation.RecCategoryID == null ? (object)DBNull.Value : employmentInformation.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@JobStatusID", employmentInformation.JobStatusID == null ? (object)DBNull.Value : employmentInformation.JobStatusID);
                sqlCommand.Parameters.AddWithValue("@Extension", employmentInformation.Extension == null ? (object)DBNull.Value : employmentInformation.Extension);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", CommonDB.SetCulturedDate(employmentInformation.JoiningDate));
                sqlCommand.Parameters.AddWithValue("@PDRPFormId", employmentInformation.PDRPFormId == null || employmentInformation.PDRPFormId == 0 ? (object)DBNull.Value : employmentInformation.PDRPFormId);
                sqlCommand.Parameters.AddWithValue("@OfferLetterPositionID", employmentInformation.OfferLetterPositionID == null ? (object)DBNull.Value : employmentInformation.OfferLetterPositionID);
                sqlCommand.Parameters.AddWithValue("@HomeTown", "");

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        /// <summary>
        /// Insert/Update Employee Address Information
        /// </summary>
        /// <param name="employeeAddressInfoModel">EmployeeAddressInfoModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmployeeAddressInformation(EmployeeAddressInfoModel employeeAddressInfoModel)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmployeeAddressInfo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeAddressInfoModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@AddressName", employeeAddressInfoModel.AddressName == null ? "" : employeeAddressInfoModel.AddressName);
                sqlCommand.Parameters.AddWithValue("@AddressType", employeeAddressInfoModel.AddressType == null ? 0 : employeeAddressInfoModel.AddressType);
                sqlCommand.Parameters.AddWithValue("@AddressLine", employeeAddressInfoModel.AddressLine == null ? "" : employeeAddressInfoModel.AddressLine);
                sqlCommand.Parameters.AddWithValue("@AddressLine2", employeeAddressInfoModel.AddressLine2 == null ? "" : employeeAddressInfoModel.AddressLine2);

                sqlCommand.Parameters.AddWithValue("@CountryID", employeeAddressInfoModel.CountryID == null ? 0 : employeeAddressInfoModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@StateID", employeeAddressInfoModel.StateID == null ? 0 : employeeAddressInfoModel.StateID);
                sqlCommand.Parameters.AddWithValue("@CityID", employeeAddressInfoModel.CityID == null ? 0 : employeeAddressInfoModel.CityID);
                sqlCommand.Parameters.AddWithValue("@PostCode", employeeAddressInfoModel.PostCode == null ? "" : employeeAddressInfoModel.PostCode);
                sqlCommand.Parameters.AddWithValue("@IsPrimaryAddress", employeeAddressInfoModel.IsPrimaryAddress == null ? false : employeeAddressInfoModel.IsPrimaryAddress);

                sqlCommand.Parameters.AddWithValue("@ApartmentNo", employeeAddressInfoModel.ApartmentNo == null ? "" : employeeAddressInfoModel.ApartmentNo);
                sqlCommand.Parameters.AddWithValue("@Street", employeeAddressInfoModel.Street == null ? "" : employeeAddressInfoModel.Street);
                sqlCommand.Parameters.AddWithValue("@PoBox", employeeAddressInfoModel.PoBox == null ? "" : employeeAddressInfoModel.PoBox);
                sqlCommand.Parameters.AddWithValue("@AreaId", employeeAddressInfoModel.AreaId == null ? 0 : employeeAddressInfoModel.AreaId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        /// <summary>
        /// Insert/Update Employee Bank Account Information
        /// </summary>
        /// <param name="employeeBankAccountInfoModel">EmployeeBankAccountInfoModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmployeeBankAccountInformation(EmployeeBankAccountInfoModel employeeBankAccountInfoModel)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmployeeBankAccountInfo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeBankAccountInfoModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@BankId", employeeBankAccountInfoModel.BankId);
                sqlCommand.Parameters.AddWithValue("@AccountNumber", employeeBankAccountInfoModel.AccountNumber == null ? "" : employeeBankAccountInfoModel.AccountNumber);
                sqlCommand.Parameters.AddWithValue("@AccountName", employeeBankAccountInfoModel.AccountName == null ? "" : employeeBankAccountInfoModel.AccountName);
                sqlCommand.Parameters.AddWithValue("@BankAddress", employeeBankAccountInfoModel.BankAddress == null ? "" : employeeBankAccountInfoModel.BankAddress);

                sqlCommand.Parameters.AddWithValue("@SwiftBICCode", employeeBankAccountInfoModel.SwiftBICCode == null ? "" : employeeBankAccountInfoModel.SwiftBICCode);
                sqlCommand.Parameters.AddWithValue("@SortCode", employeeBankAccountInfoModel.SortCode == null ? "" : employeeBankAccountInfoModel.SortCode);
                sqlCommand.Parameters.AddWithValue("@IBanCode", employeeBankAccountInfoModel.IBanCode == null ? "" : employeeBankAccountInfoModel.IBanCode);
                sqlCommand.Parameters.AddWithValue("@AgentID", employeeBankAccountInfoModel.AgentID == null ? "" : employeeBankAccountInfoModel.AgentID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        /// <summary>
        /// Insert/Update Employee Personal Identity Information
        /// </summary>
        /// <param name="employeePersonalIdentityInfoModel">EmployeePersonalIdentityInfoModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateEmployeePeronalIdentityInformation(EmployeePersonalIdentityInfoModel employeePersonalIdentityInfoModel)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmployeePersonalIdentityInfo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values

                sqlCommand.Parameters.AddWithValue("@DocPassportID", employeePersonalIdentityInfoModel.DocPassportId);
                sqlCommand.Parameters.AddWithValue("@DocVisaID", employeePersonalIdentityInfoModel.DocVisaId);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeePersonalIdentityInfoModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PassportNumber", employeePersonalIdentityInfoModel.PassportNumber);

                sqlCommand.Parameters.AddWithValue("@PassportIssueDate", CommonDB.SetCulturedDate(employeePersonalIdentityInfoModel.PassportIssueDate));
                sqlCommand.Parameters.AddWithValue("@PassportExpiryDate", CommonDB.SetCulturedDate(employeePersonalIdentityInfoModel.PassportExpiryDate));

                sqlCommand.Parameters.AddWithValue("@VisaNumber", employeePersonalIdentityInfoModel.VisaNumber);
                sqlCommand.Parameters.AddWithValue("@VisaIssueDate", CommonDB.SetCulturedDate(employeePersonalIdentityInfoModel.VisaIssueDate));
                sqlCommand.Parameters.AddWithValue("@VisaExpirationDate", CommonDB.SetCulturedDate(employeePersonalIdentityInfoModel.VisaExpirationDate));

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        /// <summary>
        /// Insert/Update Employee Account Information
        /// </summary>
        /// <param name="employeeAccountInfoModel">EmployeeAccountInfoModel model</param>
        /// <returns></returns>

        public OperationDetails UpdateEmployeeAccountInformation(UserGroupModel employeeUserRoleModel)
        {
            string Message = string.Empty;
            string strRoles = string.Empty;
            int iResult = 0;
            try
            {
                // Set Employee Roles as Comma separated string


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_InsertUpdateEmployeeAccountInfo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values 
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeUserRoleModel.EmployeeID);
                if (employeeUserRoleModel.SelectedValues.Count == 0)
                {
                    sqlCommand.Parameters.AddWithValue("@GroupID", DBNull.Value);
                }
                else
                {
                    string Selectedvals = string.Join(",", employeeUserRoleModel.SelectedValues);
                    sqlCommand.Parameters.AddWithValue("@GroupID", Selectedvals);
                }
                sqlCommand.Parameters.AddWithValue("@LeftDate", string.IsNullOrEmpty(employeeUserRoleModel.LeftDate) ? DBNull.Value : CommonDB.SetCulturedDate(employeeUserRoleModel.LeftDate));

                sqlCommand.Parameters.AddWithValue("@LeftReasonID", employeeUserRoleModel.LeftReasonID == null ? 0 : employeeUserRoleModel.LeftReasonID);

                //sqlCommand.Parameters.AddWithValue("@Status", employeeUserRoleModel.Status);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = Convert.ToString(OperationMessage.Value);
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        /// <summary>
        /// Get All details of Employee at the time of Edit
        /// </summary>
        /// <param name="EmployeeId">EmployeeId</param>
        /// <returns></returns>
        public Employee GetAllDetailsOfEmployee(int EmployeeId)
        {
            Employee objEmployee = new Employee();

            try
            {
                if (EmployeeId == 0)
                {
                    objEmployee.employeeDetailsModel = new EmployeeDetailsModel();

                    // Add default spoken languages
                    objEmployee.employeeDetailsModel.SpokenLanguages = new List<SpokenLanguages>();
                    SpokenLanguages objSpokenLanguages;
                    LanguageDB objLangDB = new LanguageDB();
                    List<LanguageModel> languageList = objLangDB.GetAllLanguages();

                    // Add to spoken Languages
                    foreach (var item in languageList)
                    {
                        objSpokenLanguages = new SpokenLanguages();
                        objSpokenLanguages.LanguageId = item.LanguageID;
                        objSpokenLanguages.LanguageName = item.LanguageName_1;

                        objEmployee.employeeDetailsModel.SpokenLanguages.Add(objSpokenLanguages);
                    }

                    // Get Nationality
                    NationalityDB objNationalityDB = new NationalityDB();
                    List<NationalityModel> lstNationalityModel = new List<NationalityModel>();
                    lstNationalityModel = objNationalityDB.getAllNationalities();

                    if (lstNationalityModel != null)
                    {
                        foreach (var item in lstNationalityModel)
                            objEmployee.employeeDetailsModel.Items.Add(new CheckListBoxItem { Text = item.NationalityName_1, Value = item.NationalityID.ToString() });
                    }
                    else
                        objEmployee.employeeDetailsModel.Items = new List<CheckListBoxItem>();

                    objEmployee.employeeContactModel = new EmployeeContactModel();
                    //Initialize Multiple Emails, Contacts, IMs
                    objEmployee.employeeContactModel.EmployeeMultipleEmails = new List<EmployeeMultipleEmails>();
                    objEmployee.employeeContactModel.EmployeeMultipleIMs = new List<EmployeeMultipleIMs>();
                    objEmployee.employeeContactModel.EmployeeMultipleContacts = new List<EmployeeMultipleContacts>();
                    objEmployee.employeeContactModel.EmployeeMultiplePersonalEmails = new List<EmployeeMultiplePersonalEmails>();

                    objEmployee.employeeAddressInfoModel = new EmployeeAddressInfoModel();
                    objEmployee.employmentInformation = new EmploymentInformation();
                    objEmployee.employeeBankAccountInfoModel = new EmployeeBankAccountInfoModel();
                    objEmployee.employeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();
                    objEmployee.employeeAccountInfoModel = new EmployeeAccountInfoModel();

                    // Get Access Roles
                    objEmployee.userGroupModel = new UserGroupModel();
                    UserGroupDB objUserGroupDB = new UserGroupDB();
                    List<UserGroupModel> objAccessRoleModel = new List<UserGroupModel>();
                    objAccessRoleModel = objUserGroupDB.GetAllUserGroups();

                    if (objAccessRoleModel != null)
                    {
                        foreach (var item in objAccessRoleModel)
                            objEmployee.userGroupModel.Items.Add(new CheckListBoxItem { Text = item.UserGroupName, Value = item.UserGroupId.ToString() });
                    }
                    else
                        objEmployee.userGroupModel.Items = new List<CheckListBoxItem>();

                    //Get User Roles                    


                }
                else
                {
                    objEmployee.userGroupModel = new UserGroupModel();
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    sqlCommand = new SqlCommand("stp_GetAllDetailsOfEmployee", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    // Passing values
                    sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);

                    DataSet objDS = new DataSet();
                    SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
                    objDA.Fill(objDS);
                    sqlConnection.Close();


                    #region Personal Information
                    if (objDS.Tables[0].Rows.Count > 0)
                    {
                        objEmployee.employeeDetailsModel = new EmployeeDetailsModel();

                        // Add default spoken languages
                        objEmployee.employeeDetailsModel.SpokenLanguages = new List<SpokenLanguages>();
                        SpokenLanguages objSpokenLanguages;
                        LanguageDB objLangDB = new LanguageDB();
                        List<LanguageModel> languageList = objLangDB.GetAllLanguages();

                        // Add to spoken Languages
                        foreach (var item in languageList)
                        {
                            objSpokenLanguages = new SpokenLanguages();
                            objSpokenLanguages.LanguageId = item.LanguageID;
                            objSpokenLanguages.LanguageName = item.LanguageName_1;

                            objEmployee.employeeDetailsModel.SpokenLanguages.Add(objSpokenLanguages);
                        }

                        // Get Nationality
                        NationalityDB objNationalityDB = new NationalityDB();
                        List<NationalityModel> lstNationalityModel = new List<NationalityModel>();
                        lstNationalityModel = objNationalityDB.getAllNationalities();

                        if (lstNationalityModel != null)
                        {
                            foreach (var item in lstNationalityModel)
                                objEmployee.employeeDetailsModel.Items.Add(new CheckListBoxItem { Text = item.NationalityName_1, Value = item.NationalityID.ToString() });
                        }
                        else
                            objEmployee.employeeDetailsModel.Items = new List<CheckListBoxItem>();

                        // Set Selected Values
                        List<string> lstString = new List<string>(objDS.Tables[0].Rows[0]["NationalityID"].ToString().Split(','));

                        objEmployee.employeeDetailsModel.EmployeeID = objDS.Tables[0].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["EmployeeID"]);
                        objEmployee.employeeDetailsModel.EmployeeAlternativeID = objDS.Tables[0].Rows[0]["EmployeeAlternativeID"] == DBNull.Value ? "" : Convert.ToString(objDS.Tables[0].Rows[0]["EmployeeAlternativeID"]);
                        objEmployee.employeeDetailsModel.TitleID = objDS.Tables[0].Rows[0]["TitleID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["TitleID"]);
                        objEmployee.employeeDetailsModel.FirstName_1 = Convert.ToString(objDS.Tables[0].Rows[0]["FirstName_1"]);
                        objEmployee.employeeDetailsModel.FirstName_2 = Convert.ToString(objDS.Tables[0].Rows[0]["FirstName_2"]);
                        objEmployee.employeeDetailsModel.FirstName_3 = Convert.ToString(objDS.Tables[0].Rows[0]["FirstName_3"]);
                        objEmployee.employeeDetailsModel.SurName_1 = Convert.ToString(objDS.Tables[0].Rows[0]["SurName_1"]);
                        objEmployee.employeeDetailsModel.SurName_2 = Convert.ToString(objDS.Tables[0].Rows[0]["SurName_2"]);
                        objEmployee.employeeDetailsModel.SurName_3 = Convert.ToString(objDS.Tables[0].Rows[0]["SurName_3"]);
                        objEmployee.employeeDetailsModel.MiddleName_1 = Convert.ToString(objDS.Tables[0].Rows[0]["MiddleName_1"]);
                        objEmployee.employeeDetailsModel.MiddleName_2 = Convert.ToString(objDS.Tables[0].Rows[0]["MiddleName_2"]);
                        objEmployee.employeeDetailsModel.MiddleName_3 = Convert.ToString(objDS.Tables[0].Rows[0]["MiddleName_3"]);
                        objEmployee.employeeDetailsModel.MothersName_1 = Convert.ToString(objDS.Tables[0].Rows[0]["MothersName_1"]);
                        objEmployee.employeeDetailsModel.MothersName_2 = Convert.ToString(objDS.Tables[0].Rows[0]["MothersName_2"]);
                        objEmployee.employeeDetailsModel.MothersName_3 = Convert.ToString(objDS.Tables[0].Rows[0]["MothersName_3"]);
                        objEmployee.employeeDetailsModel.BirthDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["BirthDate"].ToString());
                        objEmployee.employeeDetailsModel.GenderID = objDS.Tables[0].Rows[0]["GenderID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["GenderID"]);
                        objEmployee.employeeDetailsModel.BirthCountryID = objDS.Tables[0].Rows[0]["BirthCountryID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["BirthCountryID"].ToString());
                        objEmployee.employeeDetailsModel.BirthPlaceID = objDS.Tables[0].Rows[0]["BirthPlaceID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["BirthPlaceID"].ToString());
                        objEmployee.employeeDetailsModel.ReligionID = objDS.Tables[0].Rows[0]["ReligionID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["ReligionID"].ToString());
                        objEmployee.employeeDetailsModel.NationalityID = Convert.ToString(objDS.Tables[0].Rows[0]["NationalityID"]);
                        objEmployee.employeeDetailsModel.NationalityIDCardNo = Convert.ToString(objDS.Tables[0].Rows[0]["NationalityIDCardNo"]);
                        objEmployee.employeeDetailsModel.DefaultNationalityID = objDS.Tables[0].Rows[0]["DefaultNationalityID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["DefaultNationalityID"].ToString());
                        objEmployee.employeeDetailsModel.SelectedValues = lstString;
                        objEmployee.employeeDetailsModel.MaritalStatusID = objDS.Tables[0].Rows[0]["MaritalStatusID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["MaritalStatusID"].ToString());
                        if (objDS.Tables[0].Rows[0]["HomeLanguageID"] != DBNull.Value)
                            objEmployee.employeeDetailsModel.HomeLanguageID = Convert.ToInt32(objDS.Tables[0].Rows[0]["HomeLanguageID"].ToString());
                        if (objDS.Tables[0].Rows[0]["CommunicationLanguageID"] != DBNull.Value)
                            objEmployee.employeeDetailsModel.CommunicationLanguageID = Convert.ToInt32(objDS.Tables[0].Rows[0]["CommunicationLanguageID"].ToString());
                        objEmployee.employeeDetailsModel.usertypeid = objDS.Tables[0].Rows[0]["usertypeid"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["usertypeid"].ToString());

                        if (Convert.ToBoolean(objDS.Tables[0].Rows[0]["isActive"].ToString()) == true)
                        {
                            objEmployee.userGroupModel.Status = 1;
                            objEmployee.IsActive = 1;
                            objEmployee.employeeDetailsModel.isActive = true;
                        }
                        else
                        {
                            objEmployee.userGroupModel.Status = 0;
                            objEmployee.IsActive = 0;
                            objEmployee.employeeDetailsModel.isActive = false;
                        }

                    }
                    else
                    {
                        objEmployee.employeeDetailsModel = new EmployeeDetailsModel();
                        // Add default spoken languages
                        objEmployee.employeeDetailsModel.SpokenLanguages = new List<SpokenLanguages>();
                        SpokenLanguages objSpokenLanguages;
                        LanguageDB objLangDB = new LanguageDB();
                        List<LanguageModel> languageList = objLangDB.GetAllLanguages();

                        // Add to spoken Languages
                        foreach (var item in languageList)
                        {
                            objSpokenLanguages = new SpokenLanguages();
                            objSpokenLanguages.LanguageId = item.LanguageID;
                            objSpokenLanguages.LanguageName = item.LanguageName_1;

                            objEmployee.employeeDetailsModel.SpokenLanguages.Add(objSpokenLanguages);
                        }

                        // Get Nationality
                        NationalityDB objNationalityDB = new NationalityDB();
                        List<NationalityModel> lstNationalityModel = new List<NationalityModel>();
                        lstNationalityModel = objNationalityDB.getAllNationalities();

                        if (lstNationalityModel != null)
                        {
                            foreach (var item in lstNationalityModel)
                                objEmployee.employeeDetailsModel.Items.Add(new CheckListBoxItem { Text = item.NationalityName_1, Value = item.NationalityID.ToString() });
                        }
                        else
                            objEmployee.employeeDetailsModel.Items = new List<CheckListBoxItem>();
                    }

                    #endregion

                    #region Contact Details
                    // Contact Details
                    if (objDS.Tables[1].Rows.Count > 0)
                    {
                        objEmployee.employeeContactModel = new EmployeeContactModel();
                        objEmployee.employeeContactModel.EmployeeID = int.Parse(objDS.Tables[1].Rows[0]["EmployeeID"].ToString());
                        objEmployee.employeeContactModel.WorkEmail = Convert.ToString(objDS.Tables[1].Rows[0]["WorkEmail"]);
                        objEmployee.employeeContactModel.PersonalEmail = Convert.ToString(objDS.Tables[1].Rows[0]["PersonalEmail"]);

                        objEmployee.employeeContactModel.IMContact = Convert.ToString(objDS.Tables[1].Rows[0]["IMContact"]);
                        objEmployee.employeeContactModel.IMContactType = objDS.Tables[1].Rows[0]["IMContactType"] == DBNull.Value ? 0 : int.Parse(objDS.Tables[1].Rows[0]["IMContactType"].ToString());
                        objEmployee.employeeContactModel.MobileNumberHome = Convert.ToString(objDS.Tables[1].Rows[0]["MobileNumberHome"]);
                        objEmployee.employeeContactModel.MobileNumberWork = Convert.ToString(objDS.Tables[1].Rows[0]["MobileNumberWork"]);

                        //Initialize Multiple Emails, Contacts, IMs
                        objEmployee.employeeContactModel.EmployeeMultipleEmails = new List<EmployeeMultipleEmails>();
                        objEmployee.employeeContactModel.EmployeeMultipleIMs = new List<EmployeeMultipleIMs>();
                        objEmployee.employeeContactModel.EmployeeMultipleContacts = new List<EmployeeMultipleContacts>();
                        objEmployee.employeeContactModel.EmployeeMultiplePersonalEmails = new List<EmployeeMultiplePersonalEmails>();
                        objEmployee.employeeContactModel.EmployeeMultiplePhoneNumberHome = new List<EmployeeMultiplePhoneNumberHome>();
                    }
                    else
                    {
                        objEmployee.employeeContactModel = new EmployeeContactModel();
                        //Initialize Multiple Emails, Contacts, IMs
                        objEmployee.employeeContactModel.EmployeeMultipleEmails = new List<EmployeeMultipleEmails>();
                        objEmployee.employeeContactModel.EmployeeMultipleIMs = new List<EmployeeMultipleIMs>();
                        objEmployee.employeeContactModel.EmployeeMultipleContacts = new List<EmployeeMultipleContacts>();
                        objEmployee.employeeContactModel.EmployeeMultiplePersonalEmails = new List<EmployeeMultiplePersonalEmails>();
                        objEmployee.employeeContactModel.EmployeeMultiplePhoneNumberHome = new List<EmployeeMultiplePhoneNumberHome>();
                    }
                    #endregion

                    #region Address Information
                    // Address Information
                    if (objDS.Tables[1].Rows.Count > 0)
                    {
                        objEmployee.employeeAddressInfoModel = new EmployeeAddressInfoModel();
                        objEmployee.employeeAddressInfoModel.EmployeeID = objDS.Tables[1].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : int.Parse(objDS.Tables[1].Rows[0]["EmployeeID"].ToString());
                        //objEmployee.employeeAddressInfoModel.AddressName = Convert.ToString(objDS.Tables[1].Rows[0]["AddressName"]);
                        //objEmployee.employeeAddressInfoModel.AddressType = objDS.Tables[1].Rows[0]["AddressType"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["AddressType"]);
                        objEmployee.employeeAddressInfoModel.AddressLine = Convert.ToString(objDS.Tables[1].Rows[0]["AddressLine1_1"]);
                        objEmployee.employeeAddressInfoModel.AddressLine2 = Convert.ToString(objDS.Tables[1].Rows[0]["AddressLine2_1"]);
                        objEmployee.employeeAddressInfoModel.AddressLine1_2 = Convert.ToString(objDS.Tables[1].Rows[0]["AddressLine1_2"]);
                        objEmployee.employeeAddressInfoModel.AddressLine2_2 = Convert.ToString(objDS.Tables[1].Rows[0]["AddressLine2_2"]);
                        objEmployee.employeeAddressInfoModel.AddressLine1_3 = Convert.ToString(objDS.Tables[1].Rows[0]["AddressLine1_3"]);
                        objEmployee.employeeAddressInfoModel.AddressLine2_3 = Convert.ToString(objDS.Tables[1].Rows[0]["AddressLine2_3"]);

                        objEmployee.employeeAddressInfoModel.CountryID = objDS.Tables[1].Rows[0]["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["CountryID"].ToString());
                        objEmployee.employeeAddressInfoModel.StateID = objDS.Tables[1].Rows[0]["StateID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["StateID"].ToString());
                        objEmployee.employeeAddressInfoModel.CityID = objDS.Tables[1].Rows[0]["CityID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["CityID"].ToString());
                        //objEmployee.employeeAddressInfoModel.IsPrimaryAddress = objDS.Tables[1].Rows[0]["IsPrimaryAddress"] == DBNull.Value ? true : bool.Parse(objDS.Tables[1].Rows[0]["IsPrimaryAddress"].ToString());
                        objEmployee.employeeAddressInfoModel.ApartmentNo = Convert.ToString(objDS.Tables[1].Rows[0]["AppartmentNumber"]);
                        objEmployee.employeeAddressInfoModel.PostCode = Convert.ToString(objDS.Tables[1].Rows[0]["PostCode"]);
                        objEmployee.employeeAddressInfoModel.Street = Convert.ToString(objDS.Tables[1].Rows[0]["Street"]);
                        objEmployee.employeeAddressInfoModel.PoBox = Convert.ToString(objDS.Tables[1].Rows[0]["POBOX"]);
                        objEmployee.employeeAddressInfoModel.AreaId = objDS.Tables[1].Rows[0]["AreaId"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["AreaId"].ToString());
                    }
                    else
                        objEmployee.employeeAddressInfoModel = new EmployeeAddressInfoModel();


                    #endregion

                    #region Employment Information
                    if (objDS.Tables[2].Rows.Count > 0)
                    {
                        objEmployee.employmentInformation = new EmploymentInformation();
                        objEmployee.employmentInformation.EmployeeID = objDS.Tables[2].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : int.Parse(objDS.Tables[2].Rows[0]["EmployeeID"].ToString());
                        objEmployee.employmentInformation.EmployeeCode = Convert.ToString(objDS.Tables[2].Rows[0]["EmployeeCode"]);
                        objEmployee.employmentInformation.DepartmentID = objDS.Tables[2].Rows[0]["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["DepartmentID"].ToString());
                        objEmployee.employmentInformation.PositionID = objDS.Tables[2].Rows[0]["PositionID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["PositionID"].ToString());
                        objEmployee.employmentInformation.SuperviserID = objDS.Tables[2].Rows[0]["SuperviserID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["SuperviserID"].ToString());

                        objEmployee.employmentInformation.WageRate = objDS.Tables[2].Rows[0]["WageRate"] == DBNull.Value ? 0 : Convert.ToDouble(objDS.Tables[2].Rows[0]["WageRate"].ToString());
                        objEmployee.employmentInformation.ClientChargeRate = objDS.Tables[2].Rows[0]["ClientChargeRate"] == DBNull.Value ? 0 : Convert.ToDouble(objDS.Tables[2].Rows[0]["ClientChargeRate"].ToString());

                        objEmployee.employmentInformation.HireDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[2].Rows[0]["HireDate"].ToString());
                        objEmployee.employmentInformation.ResignationDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[2].Rows[0]["ResignationDate"].ToString());
                        objEmployee.employmentInformation.EmployeeContractTermId = objDS.Tables[2].Rows[0]["HRContractTypeID"].ToString() == "" ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["HRContractTypeID"].ToString());
                        objEmployee.employmentInformation.EmployeeContractTerms = Convert.ToString(objDS.Tables[2].Rows[0]["EmployeeContractTerms"]);
                        objEmployee.employmentInformation.EmployementMode = Convert.ToString(objDS.Tables[2].Rows[0]["EmployementMode"]);
                        objEmployee.employmentInformation.JobTitleID = objDS.Tables[2].Rows[0]["JobTitleID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["JobTitleID"].ToString());
                        objEmployee.employmentInformation.SalaryPackage = objDS.Tables[2].Rows[0]["SalaryPackage"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["SalaryPackage"].ToString());
                        objEmployee.employmentInformation.Qualification = Convert.ToString(objDS.Tables[2].Rows[0]["Qualification"]);
                        objEmployee.employmentInformation.OfferLetterPositionID= objDS.Tables[2].Rows[0]["OfferLetterPositionId"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["OfferLetterPositionId"].ToString());
                        objEmployee.employmentInformation.CompanyId = objDS.Tables[2].Rows[0]["CompanyId"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["CompanyId"].ToString());
                        objEmployee.employmentInformation.EmployeeSectionID = objDS.Tables[2].Rows[0]["EmployeeSectionID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["EmployeeSectionID"].ToString());
                        objEmployee.employmentInformation.EmployeeJobCategoryID = objDS.Tables[2].Rows[0]["EmployeeJobCategoryID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["EmployeeJobCategoryID"].ToString());
                        objEmployee.employmentInformation.ShiftID = objDS.Tables[2].Rows[0]["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["ShiftID"].ToString());
                        objEmployee.employmentInformation.GroupNameID = objDS.Tables[2].Rows[0]["GroupNameID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["GroupNameID"].ToString());
                        objEmployee.employmentInformation.LeftDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[2].Rows[0]["LeftDate"].ToString());
                        objEmployee.employmentInformation.LeftReasonID = objDS.Tables[2].Rows[0]["LeftReason"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["LeftReason"].ToString());                  //Changed string to int for Left Reason dropdown
                        objEmployee.employmentInformation.MOETitle = objDS.Tables[2].Rows[0]["MOETitle"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["MOETitle"].ToString());
                        objEmployee.employmentInformation.MOEHireDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[2].Rows[0]["MOEHireDate"].ToString());
                        objEmployee.employmentInformation.MOELeftDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[2].Rows[0]["MOELeftDate"].ToString());
                        objEmployee.employmentInformation.MOLTitle = objDS.Tables[2].Rows[0]["MOLTitle"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["MOLTitle"].ToString());
                        objEmployee.employmentInformation.MOLStartDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[2].Rows[0]["MOLStartDate"].ToString());
                        objEmployee.employmentInformation.MOLPersonalId = Convert.ToString(objDS.Tables[2].Rows[0]["MOLPersonalId"]);
                        objEmployee.employmentInformation.EmpStatus = Convert.ToString(objDS.Tables[2].Rows[0]["EmployeeStatus"]);
                        objEmployee.employmentInformation.SchoolHouse = objDS.Tables[2].Rows[0]["HOID"] == DBNull.Value || objDS.Tables[2].Rows[0]["HOID"].ToString() == "" ? 0 : Convert.ToInt32(objDS.Tables[2].Rows[0]["HOID"].ToString());
                        objEmployee.employmentInformation.isOverseasrecruited = objDS.Tables[2].Rows[0]["OverseasRecruited"] == DBNull.Value ? false : Convert.ToBoolean(objDS.Tables[2].Rows[0]["OverseasRecruited"]);
                        objEmployee.employmentInformation.EmployeenationalityType = Convert.ToInt32(objDS.Tables[2].Rows[0]["NationalityType"].ToString());
                        objEmployee.userGroupModel.LeftDate = objEmployee.employmentInformation.LeftDate;
                        objEmployee.userGroupModel.LeftReasonID = objEmployee.employmentInformation.LeftReasonID;
                        // 2019-02-11 Task#9118
                        objEmployee.employmentInformation.NoticePeriod = Convert.ToString(objDS.Tables[2].Rows[0]["NoticePeriod"]);
                        objEmployee.employmentInformation.ProjectData = Convert.ToString(objDS.Tables[2].Rows[0]["ProjectData"]);
                        objEmployee.employmentInformation.JobGradeID = Convert.ToInt16(objDS.Tables[2].Rows[0]["JobGradeID"]);
                        objEmployee.employmentInformation.ContractStatus = Convert.ToString(objDS.Tables[2].Rows[0]["ContractStatus"]);
                        objEmployee.employmentInformation.FamilySpouse = Convert.ToString(objDS.Tables[2].Rows[0]["FamilySpouse"]);
                        objEmployee.employmentInformation.AnnualAirTicket = Convert.ToBoolean(objDS.Tables[2].Rows[0]["AnnualAirTicket"]);
                        objEmployee.employmentInformation.HealthInsurance = Convert.ToBoolean(objDS.Tables[2].Rows[0]["HealthInsurance"]);
                        objEmployee.employmentInformation.LifeInsurance = Convert.ToBoolean(objDS.Tables[2].Rows[0]["LifeInsurance"]);
                        objEmployee.employmentInformation.SalaryBasisID = Convert.ToInt16(objDS.Tables[2].Rows[0]["SalaryBasisID"]);
                        objEmployee.employmentInformation.CostCenter = Convert.ToString(objDS.Tables[2].Rows[0]["CostCenter"]);
                        objEmployee.employmentInformation.CostCenterCode = Convert.ToString(objDS.Tables[2].Rows[0]["CostCenterCode"]);
                        objEmployee.employmentInformation.LocationCode = Convert.ToString(objDS.Tables[2].Rows[0]["LocationCode"]);
                        objEmployee.employmentInformation.OfficeLocation = Convert.ToString(objDS.Tables[2].Rows[0]["OfficeLocation"]);
                        objEmployee.employmentInformation.ProbationPeriod = Convert.ToInt32(objDS.Tables[2].Rows[0]["ProbationPeriod"]);
                        objEmployee.employmentInformation.LeaveEntitleDaysID = Convert.ToInt16(objDS.Tables[2].Rows[0]["LeaveEntitleDaysID"]);
                        objEmployee.employmentInformation.LeaveEntitleTypeID = Convert.ToInt16(objDS.Tables[2].Rows[0]["LeaveEntitleTypeID"]);
                        objEmployee.employmentInformation.ProbationCompletionDate = Convert.ToString(objDS.Tables[2].Rows[0]["ProbationCompletionDate"]);
                        objEmployee.employmentInformation.PDRPFormId = Convert.ToInt32(objDS.Tables[2].Rows[0]["PDRPFormId"]);
                        objEmployee.employmentInformation.DivisionID = Convert.ToInt32(objDS.Tables[2].Rows[0]["DivisionID"]);
                        objEmployee.employmentInformation.JoiningDate = Convert.ToString(objDS.Tables[2].Rows[0]["JoiningDate"]);
                        objEmployee.employmentInformation.RecCategoryID = Convert.ToInt32(objDS.Tables[2].Rows[0]["RecCategoryID"]);
                        objEmployee.employmentInformation.Extension = Convert.ToString(objDS.Tables[2].Rows[0]["Extension"]);
                        objEmployee.employmentInformation.JobStatusID = Convert.ToInt32(objDS.Tables[2].Rows[0]["JobStatusID"]);
                        //objEmployee.employmentInformation.InsuranceCategory = Convert.ToInt32(objDS.Tables[2].Rows[0]["InsuranceCategory"]);
                        //objEmployee.employmentInformation.InsuranceEligibility = Convert.ToInt32(objDS.Tables[2].Rows[0]["InsuranceEligibility"]);
                        //objEmployee.employmentInformation.Accommodation = Convert.ToBoolean(objDS.Tables[2].Rows[0]["Accommodation"]);
                        //objEmployee.employmentInformation.AccommodationType = Convert.ToInt32(objDS.Tables[2].Rows[0]["AccommodationType"]);
                    }
                    else
                        objEmployee.employmentInformation = new EmploymentInformation();

                    #endregion

                    #region Bank Account Information

                    // PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();

                    //  objEmployee.payDirectDepositModel = payDirectDepositDB.GetPayDirectDepositFullInformationforEmployee(EmployeeId);

                    #endregion

                    if (objDS.Tables[3].Rows.Count > 0)
                    {
                        objEmployee.payDirectDepositModel.DirectDepositID = Convert.ToInt32(objDS.Tables[3].Rows[0]["DirectDepositID"].ToString());
                        objEmployee.payDirectDepositModel.EmployeeID = Convert.ToInt32(objDS.Tables[3].Rows[0]["EmployeeID"]);
                        objEmployee.payDirectDepositModel.BankID = Convert.ToInt32((objDS.Tables[3].Rows[0]["BankID"].ToString()) == "" ? "0" : objDS.Tables[3].Rows[0]["BankID"].ToString());
                        objEmployee.payDirectDepositModel.AccountTypeID = Convert.ToInt32(objDS.Tables[3].Rows[0]["AccountTypeID"]);
                        objEmployee.payDirectDepositModel.AccountNumber = Convert.ToString(objDS.Tables[3].Rows[0]["AccountNumber"]);
                        objEmployee.payDirectDepositModel.EmpName = Convert.ToString(objDS.Tables[3].Rows[0]["EmpName"]);
                        objEmployee.payDirectDepositModel.CategoryID = Convert.ToInt32(objDS.Tables[3].Rows[0]["CategoryID"]);
                        objEmployee.payDirectDepositModel.BranchID = Convert.ToInt32(objDS.Tables[3].Rows[0]["BranchID"]);
                        objEmployee.payDirectDepositModel.AmountBlocked = objDS.Tables[3].Rows[0]["AmountBlocked"] == DBNull.Value ? 0 : Convert.ToDecimal(objDS.Tables[3].Rows[0]["AmountBlocked"]);
                        objEmployee.payDirectDepositModel.FirstSalaryBlocked = objDS.Tables[3].Rows[0]["AmountBlocked"] == DBNull.Value ? false : Convert.ToBoolean(objDS.Tables[3].Rows[0]["FirstSalaryBlocked"]);
                        objEmployee.payDirectDepositModel.AccountDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[3].Rows[0]["AccountDate"].ToString());
                        objEmployee.payDirectDepositModel.CancellationDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[3].Rows[0]["CancellationDate"].ToString());
                        objEmployee.payDirectDepositModel.Active = objDS.Tables[3].Rows[0]["active"] == DBNull.Value ? false : Convert.ToBoolean(objDS.Tables[3].Rows[0]["active"]);
                        objEmployee.payDirectDepositModel.UniAccountNumber = Convert.ToString(objDS.Tables[3].Rows[0]["UniAccountNumber"]);
                        objEmployee.payDirectDepositModel.payBankModel.BankName_1 = Convert.ToString(objDS.Tables[3].Rows[0]["BankName_1"]);
                        objEmployee.payDirectDepositModel.payBankModel.BankName_2 = Convert.ToString(objDS.Tables[3].Rows[0]["BankName_2"]);
                        objEmployee.payDirectDepositModel.payBankModel.BankName_3 = Convert.ToString(objDS.Tables[3].Rows[0]["BankName_3"]);
                        objEmployee.payDirectDepositModel.payBankBranchModel.BranchName_1 = Convert.ToString(objDS.Tables[3].Rows[0]["BranchName_1"]);
                        objEmployee.payDirectDepositModel.payBankBranchModel.BranchName_2 = Convert.ToString(objDS.Tables[3].Rows[0]["BranchName_2"]);
                        objEmployee.payDirectDepositModel.payBankBranchModel.BranchName_3 = Convert.ToString(objDS.Tables[3].Rows[0]["BranchName_3"]);
                        objEmployee.payDirectDepositModel.payAccountTypesModel.AccountTypeName_1 = Convert.ToString(objDS.Tables[3].Rows[0]["AccountTypeName_1"]);
                        objEmployee.payDirectDepositModel.payAccountTypesModel.AccountTypeName_2 = Convert.ToString(objDS.Tables[3].Rows[0]["AccountTypeName_2"]);
                        objEmployee.payDirectDepositModel.payAccountTypesModel.AccountTypeName_3 = Convert.ToString(objDS.Tables[3].Rows[0]["AccountTypeName_3"]);
                        objEmployee.payDirectDepositModel.payCategoriesModel.CategoryName_1 = Convert.ToString(objDS.Tables[3].Rows[0]["CategoryName_1"]);
                        objEmployee.payDirectDepositModel.payCategoriesModel.CategoryName_2 = Convert.ToString(objDS.Tables[3].Rows[0]["CategoryName_2"]);
                        objEmployee.payDirectDepositModel.payCategoriesModel.CategoryName_3 = Convert.ToString(objDS.Tables[3].Rows[0]["CategoryName_3"]);
                        objEmployee.payDirectDepositModel.IsAccountNoUseAsIBAN = Convert.ToBoolean(objDS.Tables[3].Rows[0]["IsAccountNoUseAsIBAN"]);
                    }
                    else
                        objEmployee.payDirectDepositModel = new PayDirectDepositModel();




                    #region Personal Identity Information
                    if (objDS.Tables[4].Rows.Count > 0)
                    {
                        objEmployee.employeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();
                        objEmployee.employeePersonalIdentityInfoModel.EmployeeID = objDS.Tables[4].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[4].Rows[0]["EmployeeID"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.PassportNumber = Convert.ToString(objDS.Tables[4].Rows[0]["DocumentNo"]);

                        objEmployee.employeePersonalIdentityInfoModel.PassportIssueDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[4].Rows[0]["IssueDate"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.PassportExpiryDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[4].Rows[0]["ExpiryDate"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.DocPassportId = Convert.ToInt32(objDS.Tables[4].Rows[0]["DocPassportId"].ToString());

                    }
                    if (objDS.Tables[10].Rows.Count > 0)
                    {
                        objEmployee.employeePersonalIdentityInfoModel.DocVisaId = Convert.ToInt32(objDS.Tables[10].Rows[0]["DocVisaId"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.EmployeeID = objDS.Tables[10].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[10].Rows[0]["EmployeeID"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.VisaNumber = Convert.ToString(objDS.Tables[10].Rows[0]["DocumentNo"]);
                        objEmployee.employeePersonalIdentityInfoModel.VisaIssueDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[10].Rows[0]["IssueDate"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.VisaExpirationDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[10].Rows[0]["ExpiryDate"].ToString());

                    }
                    if (objDS.Tables[4].Rows.Count == 0 && objDS.Tables[10].Rows.Count == 0)
                    {
                        objEmployee.employeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();
                    }


                    #endregion

                    #region Account Information
                    if (objDS.Tables[5].Rows.Count > 0)
                    {
                        // Get Access Roles
                        UserGroupDB objUserGroupDB = new UserGroupDB();
                        List<UserGroupModel> objUserGroupModel = new List<UserGroupModel>();
                        objUserGroupModel = objUserGroupDB.GetAllUserGroups();

                        if (objUserGroupModel != null)
                        {
                            foreach (var item in objUserGroupModel)
                                objEmployee.userGroupModel.Items.Add(new CheckListBoxItem { Text = item.UserGroupName, Value = item.UserGroupId.ToString() });
                        }
                        else
                            objEmployee.employeeAccountInfoModel.Items = new List<CheckListBoxItem>();

                        // Set Selected Values
                        // List<string> lstString = new List<string>(objDS.Tables[5].Rows[0]["UserGroupID"].ToString().Split(','));
                        //objEmployee.userGroupModel.EmployeeID = objDS.Tables[5].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[5].Rows[0]["EmployeeID"].ToString());

                        foreach (DataRow item in objDS.Tables[5].Rows)
                        {
                            objEmployee.userGroupModel.SelectedValues.Add(item["UserGroupID"].ToString());
                            objEmployee.userGroupModel.EmployeeID = objDS.Tables[5].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[5].Rows[0]["EmployeeID"].ToString());
                        }
                        //objEmployee.userGroupModel.SelectedValues = lstString;
                        //objEmployee.userGroupModel.Status = objDS.Tables[5].Rows[0]["Status"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[5].Rows[0]["Status"].ToString());
                    }
                    else
                    {

                        // Get Access Roles
                        UserGroupDB objUserGroupDB = new UserGroupDB();
                        List<UserGroupModel> objUserGroupModel = new List<UserGroupModel>();
                        objUserGroupModel = objUserGroupDB.GetAllUserGroups();

                        if (objUserGroupModel != null)
                        {
                            foreach (var item in objUserGroupModel)
                                objEmployee.userGroupModel.Items.Add(new CheckListBoxItem { Text = item.UserGroupName, Value = item.UserGroupId.ToString() });
                        }
                        else
                            objEmployee.userGroupModel.Items = new List<CheckListBoxItem>();
                    }

                    #endregion

                    // Spoken Languages
                    if (objDS.Tables[6].Rows.Count > 0)
                    {
                        // Add to spoken Languages
                        foreach (var item in objEmployee.employeeDetailsModel.SpokenLanguages)
                        {
                            for (int i = 0; i < objDS.Tables[6].Rows.Count; i++)
                            {
                                if (item.LanguageId == int.Parse(objDS.Tables[6].Rows[i]["LanguageId"].ToString()))
                                {
                                    item.IsSpeak = bool.Parse(objDS.Tables[6].Rows[i]["IsSpeak"].ToString());
                                    item.IsRead = bool.Parse(objDS.Tables[6].Rows[i]["IsRead"].ToString());
                                    item.IsWrite = bool.Parse(objDS.Tables[6].Rows[i]["IsWrite"].ToString());
                                    item.IsNative = bool.Parse(objDS.Tables[6].Rows[i]["IsNative"].ToString());
                                }
                            }
                        }
                    }

                    // Multiple Emails
                    if (objDS.Tables[7].Rows.Count > 0)
                    {
                        // Add to multiple Emails
                        for (int i = 0; i < objDS.Tables[7].Rows.Count; i++)
                        {
                            objEmployee.employeeContactModel.EmployeeMultipleEmails.Add(new EmployeeMultipleEmails
                            {
                                EmpEmailId = int.Parse(objDS.Tables[7].Rows[i]["EmpEmailId"].ToString()),
                                EmployeeID = int.Parse(objDS.Tables[7].Rows[i]["EmployeeID"].ToString()),
                                EmailName = objDS.Tables[7].Rows[i]["EmailName"].ToString(),
                                EmailType = int.Parse(objDS.Tables[7].Rows[i]["EmailType"].ToString())
                            });
                        }
                    }
                    else
                    {
                        objEmployee.employeeContactModel.EmployeeMultipleEmails = new List<EmployeeMultipleEmails>();
                    }

                    // Multiple IMs
                    if (objDS.Tables[8].Rows.Count > 0)
                    {
                        // Add to multiple IMs
                        for (int i = 0; i < objDS.Tables[8].Rows.Count; i++)
                        {
                            objEmployee.employeeContactModel.EmployeeMultipleIMs.Add(new EmployeeMultipleIMs
                            {
                                EmpIMId = int.Parse(objDS.Tables[8].Rows[i]["EmpIMId"].ToString()),
                                EmployeeID = int.Parse(objDS.Tables[8].Rows[i]["EmployeeID"].ToString()),
                                IMName = objDS.Tables[8].Rows[i]["IMName"].ToString(),
                                IMType = int.Parse(objDS.Tables[8].Rows[i]["IMType"].ToString())
                            });
                        }
                    }

                    // Multiple Contacts
                    if (objDS.Tables[9].Rows.Count > 0)
                    {
                        // Add to multiple Contacts
                        for (int i = 0; i < objDS.Tables[9].Rows.Count; i++)
                        {
                            objEmployee.employeeContactModel.EmployeeMultipleContacts.Add(new EmployeeMultipleContacts
                            {
                                EmpContactId = int.Parse(objDS.Tables[9].Rows[i]["EmpContactId"].ToString()),
                                EmployeeID = int.Parse(objDS.Tables[9].Rows[i]["EmployeeID"].ToString()),
                                Contact = objDS.Tables[9].Rows[i]["Contact"].ToString(),
                                ContactType = int.Parse(objDS.Tables[9].Rows[i]["ContactType"].ToString())
                            });
                        }
                    }
                    if (objDS.Tables[11].Rows.Count > 0)
                    {
                        for (int i = 0; i < objDS.Tables[11].Rows.Count; i++)
                        {
                            objEmployee.employeeContactModel.EmployeeMultiplePersonalEmails.Add(new EmployeeMultiplePersonalEmails
                            {
                                EmailName = objDS.Tables[11].Rows[i]["EmailName"].ToString()
                            });
                        }
                    }
                    if (objDS.Tables[12].Rows.Count > 0)
                    {
                        for (int i = 0; i < objDS.Tables[12].Rows.Count; i++)
                        {
                            objEmployee.employeeContactModel.EmployeeMultiplePhoneNumberHome.Add(new EmployeeMultiplePhoneNumberHome
                            {
                                Contact = objDS.Tables[12].Rows[i]["Contact"].ToString(),
                                EmpContactId = Convert.ToInt32(objDS.Tables[12].Rows[i]["EmpContactId"].ToString())
                            });
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }

            return objEmployee;
        }

        /// <summary>
        /// Get All details of Employee at the time of View Profile
        /// </summary>
        /// <param name="EmployeeId">EmployeeId</param>
        /// <returns></returns>
        public Employee ViewEmployeeProfile(int EmployeeId)
        {
            Employee objEmployee = new Employee();
            int FamilyID = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_ViewEmployeeProfile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);

                DataSet objDS = new DataSet();
                SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
                objDA.Fill(objDS);
                sqlConnection.Close();
                string Status = "";
                // Personal Information
                if (objDS.Tables[0].Rows.Count > 0)
                {
                    string strTitle = string.Empty;
                    string strNationality = string.Empty;
                    string strGender = string.Empty;
                    FamilyID = objDS.Tables[0].Rows[0]["FamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["FamilyID"]);
                    Status = Convert.ToBoolean(objDS.Tables[0].Rows[0]["IsActive"]) == true ? "Active" : "InActive";
                    objEmployee.employeeDetailsModel = new EmployeeDetailsModel();

                    // Add default spoken languages
                    objEmployee.employeeDetailsModel.SpokenLanguages = new List<SpokenLanguages>();

                    // Get Nationality and Set as a string
                    // Set Selected Values
                    List<string> lstString = new List<string>(objDS.Tables[0].Rows[0]["NationalityID"].ToString().Split(','));

                    NationalityDB objNationalityDB = new NationalityDB();
                    List<NationalityModel> lstNationalityModel = new List<NationalityModel>();
                    lstNationalityModel = objNationalityDB.getAllNationalities();

                    if (lstNationalityModel != null)
                    {
                        foreach (var str in lstString)
                        {
                            if (str != "")
                            {
                                foreach (var item in lstNationalityModel)
                                {
                                    if (item.NationalityID == int.Parse(str.ToString()))
                                    {
                                        strNationality += item.NationalityName_1 + ",";
                                    }
                                }
                            }
                        }
                        if (strNationality != "")
                        {
                            strNationality = strNationality.Substring(0, strNationality.Length - 1);
                        }
                        else
                        {
                            strNationality = "--";

                        }
                    }
                    else
                        strNationality = "--";


                    // Set Title
                    //switch (int.Parse(objDS.Tables[0].Rows[0]["TitleID"].ToString() == "" ? "0" : objDS.Tables[0].Rows[0]["TitleID"].ToString()))
                    //{
                    //    case 0:
                    //        strTitle = "";
                    //        break;
                    //    case 1:
                    //        strTitle = "Mr.";
                    //        break;
                    //    case 2:
                    //        strTitle = "Mrs.";
                    //        break;
                    //    case 3:
                    //        strTitle = "Miss.";
                    //        break;
                    //}

                    // Set Gender
                    switch (int.Parse(objDS.Tables[0].Rows[0]["GenderID"].ToString() == "" ? "1" : objDS.Tables[0].Rows[0]["GenderID"].ToString()))
                    {
                        case 1:
                            strGender = "Male";
                            break;
                        case 0:
                            strGender = "Female";
                            break;
                    }
                    objEmployee.employeeDetailsModel.EmployeeAlternativeID = objDS.Tables[0].Rows[0]["EmployeeAlterNativeID"].ToString();
                    objEmployee.employeeDetailsModel.EmployeeID = int.Parse(objDS.Tables[0].Rows[0]["EmployeeID"].ToString());
                    objEmployee.employeeDetailsModel.NameTitle = objDS.Tables[0].Rows[0]["NameTitle"].ToString();
                    objEmployee.employeeDetailsModel.FirstName_1 = objDS.Tables[0].Rows[0]["FirstName_1"].ToString();
                    objEmployee.employeeDetailsModel.SurName_1 = objDS.Tables[0].Rows[0]["SurName_1"].ToString();
                    objEmployee.employeeDetailsModel.MiddleName_1 = objDS.Tables[0].Rows[0]["MiddleName_1"].ToString();
                    objEmployee.employeeDetailsModel.BirthDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["BirthDate"].ToString());
                    objEmployee.employeeDetailsModel.GenderName = strGender;
                    objEmployee.employeeDetailsModel.NationalityID = strNationality;
                    objEmployee.employeeDetailsModel.MaritalStatusName = objDS.Tables[0].Rows[0]["MaritalStatusName"].ToString();
                    objEmployee.employeeDetailsModel.ProfileImage = objDS.Tables[0].Rows[0]["ProfileImage"].ToString();
                    objEmployee.employeeDetailsModel.CompanyName = objDS.Tables[0].Rows[0]["CompanyName"].ToString();
                    objEmployee.employeeDetailsModel.CompanyID = int.Parse(objDS.Tables[0].Rows[0]["CompanyID"].ToString());
                }
                else
                {
                    objEmployee.employeeDetailsModel = new EmployeeDetailsModel();
                    // Add default spoken languages
                    objEmployee.employeeDetailsModel.SpokenLanguages = new List<SpokenLanguages>();
                }

                // Spoken Languages
                if (objDS.Tables[1].Rows.Count > 0)
                {
                    string strDisplayLang = string.Empty;
                    // Add to spoken Languages
                    for (int i = 0; i < objDS.Tables[1].Rows.Count; i++)
                    {
                        strDisplayLang = string.Empty;
                        if (bool.Parse(objDS.Tables[1].Rows[i]["IsSpeak"].ToString()) && strDisplayLang == string.Empty)
                            strDisplayLang = "Speak" + ",";

                        if (bool.Parse(objDS.Tables[1].Rows[i]["IsRead"].ToString()))
                        {
                            if (strDisplayLang == string.Empty)
                                strDisplayLang = "Read" + ",";
                            else
                                strDisplayLang += "Read" + ",";
                        }

                        if (bool.Parse(objDS.Tables[1].Rows[i]["IsWrite"].ToString()))
                        {
                            if (strDisplayLang == string.Empty)
                                strDisplayLang = "Write" + ",";
                            else
                                strDisplayLang += "Write" + ",";
                        }

                        if (bool.Parse(objDS.Tables[1].Rows[i]["IsNative"].ToString()))
                        {
                            if (strDisplayLang == string.Empty)
                                strDisplayLang = "Native" + ",";
                            else
                                strDisplayLang += "Native" + ",";
                        }

                        if (strDisplayLang != string.Empty)
                        {
                            objEmployee.employeeDetailsModel.SpokenLanguages.Add(new SpokenLanguages
                            {
                                LanguageId = int.Parse(objDS.Tables[1].Rows[i]["LanguageId"].ToString()),
                                LanguageName = objDS.Tables[1].Rows[i]["LanguageName"].ToString(),
                                DisplayLanguages = strDisplayLang.Length == 0 ? "" : strDisplayLang.Substring(0, strDisplayLang.Length - 1)
                            });
                        }
                    }
                }

                // Contact Details
                if (objDS.Tables[2].Rows.Count > 0)
                {
                    objEmployee.employeeContactModel = new EmployeeContactModel();
                    //Initialize Multiple Emails
                    objEmployee.employeeContactModel.EmployeeMultipleEmails = new List<EmployeeMultipleEmails>();
                    objEmployee.employeeContactModel.EmployeeMultipleEmails.Add(new EmployeeMultipleEmails { EmailName = objDS.Tables[2].Rows[0]["PersonalEmail"].ToString(), EmailTypeName = "Personal" });
                    objEmployee.employeeContactModel.EmployeeMultipleEmails.Add(new EmployeeMultipleEmails { EmailName = objDS.Tables[2].Rows[0]["WorkEmail"].ToString(), EmailTypeName = "Work" });

                    // Initialize IMs
                    objEmployee.employeeContactModel.EmployeeMultipleIMs = new List<EmployeeMultipleIMs>();
                    objEmployee.employeeContactModel.EmployeeMultipleIMs.Add(new EmployeeMultipleIMs { IMName = objDS.Tables[2].Rows[0]["IMContact"].ToString(), IMTypeName = objDS.Tables[2].Rows[0]["IMTypeName"].ToString() });

                    // Initialize Contacts
                    objEmployee.employeeContactModel.EmployeeMultipleContacts = new List<EmployeeMultipleContacts>();
                    objEmployee.employeeContactModel.EmployeeMultipleContacts.Add(new EmployeeMultipleContacts { Contact = objDS.Tables[2].Rows[0]["MobileNumberHome"].ToString(), ContactTypeName = "Mobile Number" });
                    objEmployee.employeeContactModel.EmployeeMultipleContacts.Add(new EmployeeMultipleContacts { Contact = objDS.Tables[2].Rows[0]["MobileNumberWork"].ToString(), ContactTypeName = "Landline" });
                }
                else
                {
                    objEmployee.employeeContactModel = new EmployeeContactModel();
                    //Initialize Multiple Emails, Contacts, IMs
                    objEmployee.employeeContactModel.EmployeeMultipleEmails = new List<EmployeeMultipleEmails>();
                    objEmployee.employeeContactModel.EmployeeMultipleIMs = new List<EmployeeMultipleIMs>();
                    objEmployee.employeeContactModel.EmployeeMultipleContacts = new List<EmployeeMultipleContacts>();
                }

                //commented code : to show only first contact details - Trupti
                // Multiple Emails 
                //if (objDS.Tables[3].Rows.Count > 0)
                //{
                //    // Add to multiple Emails
                //    for (int i = 0; i < objDS.Tables[3].Rows.Count; i++)
                //    {
                //        objEmployee.employeeContactModel.EmployeeMultipleEmails.Add(new EmployeeMultipleEmails { EmailName = objDS.Tables[3].Rows[i]["EmailName"].ToString(), EmailTypeName = objDS.Tables[3].Rows[i]["EmailTypeName"].ToString() });
                //    }
                //}

                //// Multiple IMs
                //if (objDS.Tables[4].Rows.Count > 0)
                //{
                //    // Add to multiple IMs
                //    for (int i = 0; i < objDS.Tables[4].Rows.Count; i++)
                //    {
                //        objEmployee.employeeContactModel.EmployeeMultipleIMs.Add(new EmployeeMultipleIMs { IMName = objDS.Tables[4].Rows[i]["IMName"].ToString(), IMTypeName = objDS.Tables[4].Rows[i]["IMTypeName"].ToString() });
                //    }
                //}

                //// Multiple Contacts
                //if (objDS.Tables[5].Rows.Count > 0)
                //{
                //    // Add to multiple Contacts
                //    for (int i = 0; i < objDS.Tables[5].Rows.Count; i++)
                //    {
                //        objEmployee.employeeContactModel.EmployeeMultipleContacts.Add(new EmployeeMultipleContacts { Contact = objDS.Tables[5].Rows[i]["Contact"].ToString(), ContactTypeName = objDS.Tables[5].Rows[i]["ContactTypeName"].ToString() });
                //    }
                //}

                // Address Information
                if (objDS.Tables[2].Rows.Count > 0)
                {
                    objEmployee.employeeAddressInfoModel = new EmployeeAddressInfoModel();
                    objEmployee.employeeAddressInfoModel.AddressLine = objDS.Tables[2].Rows[0]["AddressLine1_1"].ToString();
                    objEmployee.employeeAddressInfoModel.AddressLine2 = objDS.Tables[2].Rows[0]["AddressLine2_1"].ToString();
                    objEmployee.employeeAddressInfoModel.AddressLine1_2 = objDS.Tables[2].Rows[0]["AddressLine1_2"].ToString();
                    objEmployee.employeeAddressInfoModel.AddressLine2_2 = objDS.Tables[2].Rows[0]["AddressLine2_2"].ToString();
                    objEmployee.employeeAddressInfoModel.AddressLine1_3 = objDS.Tables[2].Rows[0]["AddressLine1_3"].ToString();
                    objEmployee.employeeAddressInfoModel.AddressLine2_3 = objDS.Tables[2].Rows[0]["AddressLine2_3"].ToString();
                    objEmployee.employeeAddressInfoModel.PostCode = objDS.Tables[2].Rows[0]["PostCode"].ToString();

                    objEmployee.employeeAddressInfoModel.ApartmentNo = objDS.Tables[2].Rows[0]["AppartmentNumber"].ToString();
                    objEmployee.employeeAddressInfoModel.Street = objDS.Tables[2].Rows[0]["Street"].ToString();
                    objEmployee.employeeAddressInfoModel.PoBox = objDS.Tables[2].Rows[0]["PoBox"].ToString();

                    objEmployee.employeeAddressInfoModel.CountryName = objDS.Tables[2].Rows[0]["CountryName"].ToString() == "" ? "--" : objDS.Tables[2].Rows[0]["CountryName"].ToString();
                    objEmployee.employeeAddressInfoModel.StateName = objDS.Tables[2].Rows[0]["StateName"].ToString() == "" ? "--" : objDS.Tables[2].Rows[0]["StateName"].ToString();
                    objEmployee.employeeAddressInfoModel.CityName = objDS.Tables[2].Rows[0]["CityName"].ToString() == "" ? "--" : objDS.Tables[2].Rows[0]["CityName"].ToString();
                    objEmployee.employeeAddressInfoModel.AreaName = objDS.Tables[2].Rows[0]["AreaName"].ToString() == "" ? "--" : objDS.Tables[2].Rows[0]["AreaName"].ToString();
                }
                else
                    objEmployee.employeeAddressInfoModel = new EmployeeAddressInfoModel();


                // Employment Information
                if (objDS.Tables[6].Rows.Count > 0)
                {
                    string strEmploymentMode = string.Empty;
                    string strJobTitle = string.Empty;
                    string strQualification = string.Empty;
                    strEmploymentMode = objDS.Tables[6].Rows[0]["EmployementModeName"].ToString();
                    strQualification = objDS.Tables[6].Rows[0]["HrQualificationName"].ToString();
                    strJobTitle = objDS.Tables[6].Rows[0]["JobTitle"].ToString();
                    objEmployee.employmentInformation = new EmploymentInformation();
                    objEmployee.employmentInformation.EmployeeCode = objDS.Tables[6].Rows[0]["EmployeeCode"].ToString();
                    objEmployee.employmentInformation.WageRate = double.Parse(objDS.Tables[6].Rows[0]["WageRate"].ToString() == "" ? "0" : objDS.Tables[6].Rows[0]["WageRate"].ToString());
                    objEmployee.employmentInformation.ClientChargeRate = double.Parse(objDS.Tables[6].Rows[0]["ClientChargeRate"].ToString() == "" ? "0" : objDS.Tables[6].Rows[0]["ClientChargeRate"].ToString());
                    objEmployee.employmentInformation.HireDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[6].Rows[0]["HireDate"].ToString());
                    objEmployee.employmentInformation.ResignationDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[6].Rows[0]["ResignationDate"].ToString());
                    objEmployee.employmentInformation.EmployeeContractTerms = objDS.Tables[6].Rows[0]["EmployeeContractTerms"].ToString();
                    objEmployee.employmentInformation.SalaryPackage = Int32.Parse(objDS.Tables[6].Rows[0]["SalaryPackage"].ToString() == "" ? "0" : objDS.Tables[6].Rows[0]["SalaryPackage"].ToString());

                    objEmployee.employmentInformation.DepartmentName = objDS.Tables[6].Rows[0]["DepartmentName"].ToString();
                    objEmployee.employmentInformation.PositionName = objDS.Tables[6].Rows[0]["PositionName"].ToString();
                    objEmployee.employmentInformation.SupervisorName = objDS.Tables[6].Rows[0]["SupervisorName"].ToString();
                    objEmployee.employmentInformation.isOverseasrecruited = objDS.Tables[6].Rows[0]["OverseasRecruited"].ToString() == "" ? false : Convert.ToBoolean(objDS.Tables[6].Rows[0]["OverseasRecruited"].ToString());
                    objEmployee.employmentInformation.EmploymentModeName = strEmploymentMode;
                    objEmployee.employmentInformation.JobTitleName = strJobTitle;
                    objEmployee.employmentInformation.QualificationName = strQualification;
                    //Task#9118
                    objEmployee.employmentInformation.NoticePeriod = objDS.Tables[6].Rows[0]["NoticePeriod"].ToString();
                }
                else
                    objEmployee.employmentInformation = new EmploymentInformation();


                // Bank Account Information



                // PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();

                //  objEmployee.payDirectDepositModel = payDirectDepositDB.GetPayDirectDepositFullInformationforEmployee(EmployeeId);

                if (objDS.Tables[7].Rows.Count > 0)
                {
                    objEmployee.payDirectDepositModel.DirectDepositID = Convert.ToInt32(objDS.Tables[7].Rows[0]["DirectDepositID"].ToString());
                    objEmployee.payDirectDepositModel.EmployeeID = Convert.ToInt32(objDS.Tables[7].Rows[0]["EmployeeID"]);
                    objEmployee.payDirectDepositModel.BankID = Convert.ToInt32(objDS.Tables[7].Rows[0]["BankID"]);
                    objEmployee.payDirectDepositModel.AccountTypeID = Convert.ToInt32(objDS.Tables[7].Rows[0]["AccountTypeID"]);
                    objEmployee.payDirectDepositModel.AccountNumber = Convert.ToString(objDS.Tables[7].Rows[0]["AccountNumber"]);
                    objEmployee.payDirectDepositModel.EmpName = Convert.ToString(objDS.Tables[7].Rows[0]["EmpName"]);
                    objEmployee.payDirectDepositModel.CategoryID = Convert.ToInt32(objDS.Tables[7].Rows[0]["CategoryID"]);
                    objEmployee.payDirectDepositModel.BranchID = Convert.ToInt32(objDS.Tables[7].Rows[0]["BranchID"]);
                    objEmployee.payDirectDepositModel.AmountBlocked = objDS.Tables[7].Rows[0]["AmountBlocked"] == DBNull.Value ? 0 : Convert.ToDecimal(objDS.Tables[7].Rows[0]["AmountBlocked"]);
                    objEmployee.payDirectDepositModel.FirstSalaryBlocked = objDS.Tables[7].Rows[0]["AmountBlocked"] == DBNull.Value ? false : Convert.ToBoolean(objDS.Tables[7].Rows[0]["FirstSalaryBlocked"]);
                    objEmployee.payDirectDepositModel.AccountDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[7].Rows[0]["AccountDate"].ToString());
                    objEmployee.payDirectDepositModel.CancellationDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[7].Rows[0]["CancellationDate"].ToString());
                    objEmployee.payDirectDepositModel.Active = objDS.Tables[7].Rows[0]["active"] == DBNull.Value ? false : Convert.ToBoolean(objDS.Tables[7].Rows[0]["active"]);
                    objEmployee.payDirectDepositModel.UniAccountNumber = Convert.ToString(objDS.Tables[7].Rows[0]["UniAccountNumber"]);
                    objEmployee.payDirectDepositModel.payBankModel.BankName_1 = Convert.ToString(objDS.Tables[7].Rows[0]["BankName_1"]);
                    objEmployee.payDirectDepositModel.payBankModel.BankName_2 = Convert.ToString(objDS.Tables[7].Rows[0]["BankName_2"]);
                    objEmployee.payDirectDepositModel.payBankModel.BankName_3 = Convert.ToString(objDS.Tables[7].Rows[0]["BankName_3"]);
                    objEmployee.payDirectDepositModel.payBankBranchModel.BranchName_1 = Convert.ToString(objDS.Tables[7].Rows[0]["BranchName_1"]);
                    objEmployee.payDirectDepositModel.payBankBranchModel.BranchName_2 = Convert.ToString(objDS.Tables[7].Rows[0]["BranchName_2"]);
                    objEmployee.payDirectDepositModel.payBankBranchModel.BranchName_3 = Convert.ToString(objDS.Tables[7].Rows[0]["BranchName_3"]);
                    objEmployee.payDirectDepositModel.payAccountTypesModel.AccountTypeName_1 = Convert.ToString(objDS.Tables[7].Rows[0]["AccountTypeName_1"]);
                    objEmployee.payDirectDepositModel.payAccountTypesModel.AccountTypeName_2 = Convert.ToString(objDS.Tables[7].Rows[0]["AccountTypeName_2"]);
                    objEmployee.payDirectDepositModel.payAccountTypesModel.AccountTypeName_3 = Convert.ToString(objDS.Tables[7].Rows[0]["AccountTypeName_3"]);
                    objEmployee.payDirectDepositModel.payCategoriesModel.CategoryName_1 = Convert.ToString(objDS.Tables[7].Rows[0]["CategoryName_1"]);
                    objEmployee.payDirectDepositModel.payCategoriesModel.CategoryName_2 = Convert.ToString(objDS.Tables[7].Rows[0]["CategoryName_2"]);
                    objEmployee.payDirectDepositModel.payCategoriesModel.CategoryName_3 = Convert.ToString(objDS.Tables[7].Rows[0]["CategoryName_3"]);
                }
                else
                    //objEmployee.employeeBankAccountInfoModel = new EmployeeBankAccountInfoModel();
                    objEmployee.payDirectDepositModel = new PayDirectDepositModel();


                // Personal Identity Information
                if (objDS.Tables[8].Rows.Count > 0 || objDS.Tables[13].Rows.Count > 0)
                {
                    if (objDS.Tables[8].Rows.Count > 0)
                    {
                        objEmployee.employeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();
                        objEmployee.employeePersonalIdentityInfoModel.EmployeeID = objDS.Tables[8].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[8].Rows[0]["EmployeeID"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.PassportNumber = Convert.ToString(objDS.Tables[8].Rows[0]["PayDocumentNumber"]);
                        objEmployee.employeePersonalIdentityInfoModel.PassportIssueDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[8].Rows[0]["IssueDate"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.PassportExpiryDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[8].Rows[0]["ExpireDate"].ToString());
                    }
                    if (objDS.Tables[13].Rows.Count > 0)
                    {
                        objEmployee.employeePersonalIdentityInfoModel.EmployeeID = objDS.Tables[13].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[13].Rows[0]["EmployeeID"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.VisaNumber = Convert.ToString(objDS.Tables[13].Rows[0]["PayDocumentNumber"]);
                        objEmployee.employeePersonalIdentityInfoModel.VisaIssueDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[13].Rows[0]["IssueDate"].ToString());
                        objEmployee.employeePersonalIdentityInfoModel.VisaExpirationDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[13].Rows[0]["ExpireDate"].ToString());
                    }
                }
                else
                    objEmployee.employeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();


                // Account Information
                if (objDS.Tables[9].Rows.Count > 0)
                {
                    string strRoles = string.Empty;

                    objEmployee.employeeAccountInfoModel = new EmployeeAccountInfoModel();

                    objEmployee.employeeAccountInfoModel.StatusName = Status;
                    // Set Selected Values
                    List<string> lstString = new List<string>();
                    foreach (DataRow item in objDS.Tables[9].Rows)
                    {
                        lstString.Add(item["UserGroupId"].ToString());

                    }
                    // Get Access Roles
                    AccessRoleDB objAccessRoleDB = new AccessRoleDB();
                    List<UserGroupModel> objUserGroupModel = new List<UserGroupModel>();
                    objUserGroupModel = objAccessRoleDB.GetAllAccessRoles();

                    foreach (var items in lstString)
                    {
                        if (items != null && items != "" && items != " ")
                        {
                            foreach (var roles in objUserGroupModel)
                            {
                                if (int.Parse(items) == roles.UserGroupId)
                                    strRoles += roles.UserGroupName + ",";
                            }
                        }
                    }
                    objEmployee.employeeAccountInfoModel.RoleNames = strRoles.TrimEnd(',');

                }
                else
                {
                    objEmployee.employeeAccountInfoModel = new EmployeeAccountInfoModel();
                    objEmployee.employeeAccountInfoModel.StatusName = Status;
                }

                // Internal Employment
                if (objDS.Tables[10].Rows.Count > 0)
                {
                    objEmployee.employeeInternalEmploymentModel = new List<InternalEmploymentModel>();
                    InternalEmploymentModel objInternalEmploymentModel;

                    for (int i = 0; i < objDS.Tables[10].Rows.Count; i++)
                    {
                        objInternalEmploymentModel = new InternalEmploymentModel();
                        objInternalEmploymentModel.InternalEmploymentID = int.Parse(objDS.Tables[10].Rows[i]["InternalEmploymentID"].ToString());
                        //objInternalEmploymentModel.LocationID = int.Parse(objDS.Tables[10].Rows[i]["LocationID"] == null ? "0" : objDS.Tables[10].Rows[i]["LocationID"].ToString());
                        //objInternalEmploymentModel.DepartmentID = int.Parse(objDS.Tables[10].Rows[i]["DepartmentID"] == null ? "0" : objDS.Tables[10].Rows[i]["DepartmentID"].ToString());
                        //objInternalEmploymentModel.PositionID = int.Parse(objDS.Tables[10].Rows[i]["PositionID"] == null ? "0" : objDS.Tables[10].Rows[i]["PositionID"].ToString());
                        objInternalEmploymentModel.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[10].Rows[i]["StartDate"].ToString());
                        objInternalEmploymentModel.EndDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[10].Rows[i]["EndDate"].ToString());

                        objInternalEmploymentModel.Comments = objDS.Tables[10].Rows[i]["Comments"].ToString();
                        objInternalEmploymentModel.PositionName = objDS.Tables[10].Rows[i]["PositionName"].ToString();
                        objInternalEmploymentModel.DepartmentName = objDS.Tables[10].Rows[i]["DepartmentName"].ToString();
                        objInternalEmploymentModel.LocationName = objDS.Tables[10].Rows[i]["LocationName"].ToString();

                        // Add to List
                        objEmployee.employeeInternalEmploymentModel.Add(objInternalEmploymentModel);
                    }
                }
                else
                {
                    objEmployee.employeeInternalEmploymentModel = new List<InternalEmploymentModel>();
                }

                // Past Employment
                if (objDS.Tables[11].Rows.Count > 0)
                {
                    objEmployee.employeePastEmploymentModel = new List<PastEmploymentModel>();
                    PastEmploymentModel objPastEmploymentModel;

                    for (int i = 0; i < objDS.Tables[11].Rows.Count; i++)
                    {
                        objPastEmploymentModel = new PastEmploymentModel();
                        objPastEmploymentModel.PastEmploymentID = int.Parse(objDS.Tables[11].Rows[i]["PastEmploymentID"].ToString());
                        objPastEmploymentModel.CompanyName = objDS.Tables[11].Rows[i]["CompanyName"].ToString();
                        objPastEmploymentModel.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[11].Rows[i]["StartDate"].ToString());
                        objPastEmploymentModel.EndDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[11].Rows[i]["EndDate"].ToString());
                        objPastEmploymentModel.Department = objDS.Tables[11].Rows[i]["Department"].ToString();

                        objPastEmploymentModel.Position = objDS.Tables[11].Rows[i]["Position"].ToString();
                        objPastEmploymentModel.Role = objDS.Tables[11].Rows[i]["Role"].ToString();
                        objPastEmploymentModel.EmpReferences = objDS.Tables[11].Rows[i]["EmpReferences"].ToString();
                        objPastEmploymentModel.Comments = objDS.Tables[11].Rows[i]["Comments"].ToString();

                        // Add to List
                        objEmployee.employeePastEmploymentModel.Add(objPastEmploymentModel);
                    }
                }
                else
                {
                    objEmployee.employeePastEmploymentModel = new List<PastEmploymentModel>();
                }

                // Dependent Details
                if (objDS.Tables.Count > 13)
                {
                    if (objDS.Tables[12].Rows.Count > 0)
                    {
                        objEmployee.employeeDependentModel = new List<EmployeeDependantModel>();
                        EmployeeDependantModel employeeDependentModel;

                        for (int i = 0; i < objDS.Tables[12].Rows.Count; i++)
                        {
                            employeeDependentModel = new EmployeeDependantModel();
                            employeeDependentModel.DependantId = Convert.ToInt32(objDS.Tables[12].Rows[i]["DependantId"].ToString());
                            employeeDependentModel.FirstName = objDS.Tables[12].Rows[i]["FirstName"].ToString();
                            employeeDependentModel.LastName = objDS.Tables[12].Rows[i]["LastName"].ToString();
                            employeeDependentModel.Address1 = objDS.Tables[12].Rows[i]["Address1"].ToString();
                            employeeDependentModel.Address2 = objDS.Tables[12].Rows[i]["Address2"].ToString();
                            employeeDependentModel.Phone1 = objDS.Tables[12].Rows[i]["Phone1"].ToString();
                            employeeDependentModel.Phone2 = objDS.Tables[12].Rows[i]["Phone2"].ToString();
                            employeeDependentModel.Country = Convert.ToInt32(objDS.Tables[12].Rows[i]["Country"].ToString());
                            employeeDependentModel.City = objDS.Tables[12].Rows[i]["City"].ToString();
                            employeeDependentModel.RelationshipName = objDS.Tables[12].Rows[i]["RelationshipName"].ToString();
                            employeeDependentModel.CountryName = objDS.Tables[12].Rows[i]["CountryName"].ToString();
                            objEmployee.employeeDependentModel.Add(employeeDependentModel);
                        }
                    }
                    else
                        objEmployee.employeeDependentModel = new List<EmployeeDependantModel>();
                }
                if (objDS.Tables.Count > 14)
                {
                    if (objDS.Tables[14].Rows.Count > 0)
                    {
                        objEmployee.lstEmpCourseDepartment = new List<EmployeeCoursedepartment>();
                        EmployeeCoursedepartment empCourseDepartment;

                        for (int i = 0; i < objDS.Tables[14].Rows.Count; i++)
                        {
                            empCourseDepartment = new EmployeeCoursedepartment();
                            empCourseDepartment.CourseName_1 = objDS.Tables[14].Rows[i]["CourseName_1"].ToString();
                            empCourseDepartment.CourseDepartmentName_1 = objDS.Tables[14].Rows[i]["CourseDepartmentName_1"].ToString();
                            empCourseDepartment.GradeNameEn = objDS.Tables[14].Rows[i]["GradeName_1"].ToString();
                            objEmployee.lstEmpCourseDepartment.Add(empCourseDepartment);
                        }
                    }
                    else
                        objEmployee.lstEmpCourseDepartment = new List<EmployeeCoursedepartment>();
                }
                if (FamilyID > 0)
                {
                    objEmployee.childrenList = GetAllChildrenDetails(FamilyID);
                }
                else
                {
                    objEmployee.childrenList = new List<ChildrenDetails>();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }

            return objEmployee;
        }

        public DataSet GetEmployeeProfiletoExport(int EmployeeId)
        {
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeProfileForExport", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // Passing values
            sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);

            DataSet objDS = new DataSet();
            SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
            objDA.Fill(objDS);
            return objDS;
        }
        public List<ChildrenDetails> GetAllChildrenDetails(int FamilyId)
        {

            // Create a list to hold the Absent		
            List<ChildrenDetails> ChildrenList = new List<ChildrenDetails>();
            try
            {
                String Query = "SELECT dbo.STD_StudentDetails.AlternativeID, dbo.STD_StudentDetails.StudentID, dbo.STD_StudentDetails.NameAsInPassport_1, dbo.GEN_Gender.GenderName_1," +
                               " dbo.STD_StudentDetails.FirstName_1,dbo.STD_StudentDetails.MiddleName_1," +
                               " dbo.STD_StudentDetails.BirthDate2, dbo.GEN_Grade.GradeName_1, dbo.GEN_Section.SectionName_1 FROM  dbo.STD_StudentDetails INNER JOIN" +
                               " dbo.STD_CurrentAcademicInformation ON dbo.STD_StudentDetails.StudentID = dbo.STD_CurrentAcademicInformation.StudentID INNER JOIN" +
                               " dbo.GEN_Gender ON dbo.STD_StudentDetails.GenderID = dbo.GEN_Gender.GenderID INNER JOIN dbo.GEN_Grade ON dbo.STD_CurrentAcademicInformation.GradeID = dbo.GEN_Grade.GradeID INNER JOIN" +
                               " dbo.GEN_Section ON dbo.STD_CurrentAcademicInformation.SectionID = dbo.GEN_Section.SectionID WHERE(dbo.STD_StudentDetails.isRegistered = 1) AND(dbo.STD_StudentDetails.FamilyID = " + FamilyId + ")";

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Query))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        ChildrenDetails Children;
                        while (reader.Read())
                        {
                            Children = new ChildrenDetails();
                            Children.AlternativeID = Convert.ToInt32(reader["AlternativeID"].ToString());
                            Children.StudentID = Convert.ToInt32(reader["StudentID"].ToString());
                            Children.NameAsInPassport_1 = reader["NameAsInPassport_1"].ToString();
                            Children.GenderName_1 = reader["GenderName_1"].ToString();
                            try
                            {
                                Children.BirthDate2 = (reader["BirthDate2"] == DBNull.Value ? "" : Convert.ToDateTime(reader["BirthDate2"].ToString()).ToString("dd MMM yyyy"));
                            }
                            catch
                            {
                                Children.BirthDate2 = "";
                            }
                            Children.SectionName_1 = reader["SectionName_1"].ToString();
                            Children.GradeName_1 = reader["GradeName_1"].ToString();
                            Children.FirstName_1 = reader["FirstName_1"].ToString();
                            Children.MiddleName_1 = reader["MiddleName_1"].ToString();
                            Children.IsAddedtoDependent = CheckAddedtoDependent(Children.StudentID);
                            ChildrenList.Add(Children);
                        }
                    }
                }
                return ChildrenList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public ChildrenDetails GetChildDetails(int studentId)
        {

            // Create a list to hold the Absent		
            ChildrenDetails Children = new ChildrenDetails();
            try
            {
                String Query = "SELECT dbo.STD_StudentDetails.AlternativeID, dbo.STD_StudentDetails.StudentID, dbo.STD_StudentDetails.NameAsInPassport_1, dbo.GEN_Gender.GenderName_1," +
                               " dbo.STD_StudentDetails.FirstName_1,dbo.STD_StudentDetails.MiddleName_1," +
                               " dbo.STD_StudentDetails.BirthDate2, dbo.GEN_Grade.GradeName_1, dbo.GEN_Section.SectionName_1 FROM  dbo.STD_StudentDetails INNER JOIN" +
                               " dbo.STD_CurrentAcademicInformation ON dbo.STD_StudentDetails.StudentID = dbo.STD_CurrentAcademicInformation.StudentID INNER JOIN" +
                               " dbo.GEN_Gender ON dbo.STD_StudentDetails.GenderID = dbo.GEN_Gender.GenderID INNER JOIN dbo.GEN_Grade ON dbo.STD_CurrentAcademicInformation.GradeID = dbo.GEN_Grade.GradeID INNER JOIN" +
                               " dbo.GEN_Section ON dbo.STD_CurrentAcademicInformation.SectionID = dbo.GEN_Section.SectionID WHERE(dbo.STD_StudentDetails.isRegistered = 1) AND(dbo.STD_StudentDetails.StudentID = " + studentId + ")";

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Query))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {
                            Children = new ChildrenDetails();
                            Children.AlternativeID = Convert.ToInt32(reader["AlternativeID"].ToString());
                            Children.StudentID = Convert.ToInt32(reader["StudentID"].ToString());
                            Children.NameAsInPassport_1 = reader["NameAsInPassport_1"].ToString();
                            Children.GenderName_1 = reader["GenderName_1"].ToString();
                            Children.BirthDate2 = Convert.ToDateTime(reader["BirthDate2"].ToString()).ToString("dd MMM yyyy ");
                            Children.SectionName_1 = reader["SectionName_1"].ToString();
                            Children.GradeName_1 = reader["GradeName_1"].ToString();
                            Children.FirstName_1 = reader["FirstName_1"].ToString();
                            Children.MiddleName_1 = reader["MiddleName_1"].ToString();
                            Children.IsAddedtoDependent = CheckAddedtoDependent(Children.StudentID);

                        }
                    }
                }
                return Children;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public bool CheckAddedtoDependent(int StudentId)
        {
            bool isAddedDependant = false;

            try
            {
                String Query = "SELECT * FROM dbo.HR_Employee_Dependant where StudentID=" + StudentId;

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Query))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        isAddedDependant = true;
                    }
                }
                return isAddedDependant;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }

        }

        public List<FamilyDetails> GetAllFamilyDetails(int EmpId)
        {
            List<FamilyDetails> lstFamilyDetails = new List<FamilyDetails>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetFamilyDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                // sqlCommand.Parameters.AddWithValue("@EmployeeId", EmpId);

                DataSet objDS = new DataSet();
                SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
                objDA.Fill(objDS);
                sqlConnection.Close();
                if (objDS.Tables[0].Rows.Count > 0)
                {
                    FamilyDetails objFamily;
                    for (int i = 0; i < objDS.Tables[0].Rows.Count; i++)
                    {
                        objFamily = new FamilyDetails();
                        objFamily.FamilyID = int.Parse(objDS.Tables[0].Rows[i]["FamilyID"].ToString());
                        objFamily.FamilyName_1 = objDS.Tables[0].Rows[i]["FamilyName_1"].ToString();
                        objFamily.FatherName_1 = objDS.Tables[0].Rows[i]["FatherName_1"].ToString();
                        // Add to List
                        lstFamilyDetails.Add(objFamily);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
            return lstFamilyDetails;
        }

        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <param name="employeeDetailsModel"></param>
        /// <returns></returns>
        public OperationDetails UploadProfileImage(EmployeeDetailsModel employeeDetailsModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UploadProfileImage", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeDetailsModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ProfileImage", employeeDetailsModel.ProfileImage);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeDetailsModel.ModifiedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, employeeDetailsModel.EmployeeID);
        }

        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <param name="employeeDetailsModel"></param>
        /// <returns></returns>
        public OperationDetails RemoveProfileImage(EmployeeDetailsModel employeeDetailsModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_RemoveProfileImage", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeDetailsModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeDetailsModel.ModifiedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, employeeDetailsModel.EmployeeID);
        }

        /// <summary>
        /// Update Employee Status Active or Deactive
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public OperationDetails UpdateEmployeeStatus(Employee employee)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateEmployeeStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employee.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@isActive", employee.employeeDetailsModel.isActive);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employee.ModifiedBy);



                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, employee.EmployeeId);
        }

        /// <summary>
        /// Terminate Employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public OperationDetails TerminateEmployee(TerminatedEmployee termintaedemployee)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_TerminateEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", termintaedemployee.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Reason", termintaedemployee.Reason);
                sqlCommand.Parameters.AddWithValue("@TerminatedDate", CommonDB.SetCulturedDate(termintaedemployee.TerminatedDate));
                sqlCommand.Parameters.AddWithValue("@CreatedBy", termintaedemployee.CreatedBy);



                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, termintaedemployee.EmployeeId);
        }

        public Employee GEtBasicEmployeeDetails(int id)
        {

            Employee objEmployee = new Employee();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetBasicDetailsOfEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeId", id);

                DataSet objDS = new DataSet();
                SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
                objDA.Fill(objDS);
                sqlConnection.Close();
                // Contact Details
                if (objDS.Tables[0].Rows.Count > 0)
                {
                    objEmployee.employmentInformation = new EmploymentInformation();
                    objEmployee.employmentInformation.EmployeeID = objDS.Tables[0].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : int.Parse(objDS.Tables[0].Rows[0]["EmployeeID"].ToString());
                    objEmployee.employmentInformation.EmployeeCode = Convert.ToString(objDS.Tables[0].Rows[0]["EmployeeCode"]);
                    objEmployee.employmentInformation.DepartmentID = objDS.Tables[0].Rows[0]["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["DepartmentID"].ToString());
                    objEmployee.employmentInformation.PositionID = objDS.Tables[0].Rows[0]["PositionID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["PositionID"].ToString());
                    objEmployee.employmentInformation.SuperviserID = objDS.Tables[0].Rows[0]["SuperviserID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["SuperviserID"].ToString());

                    objEmployee.employmentInformation.WageRate = objDS.Tables[0].Rows[0]["WageRate"] == DBNull.Value ? 0 : Convert.ToDouble(objDS.Tables[0].Rows[0]["WageRate"].ToString());
                    objEmployee.employmentInformation.ClientChargeRate = objDS.Tables[0].Rows[0]["ClientChargeRate"] == DBNull.Value ? 0 : Convert.ToDouble(objDS.Tables[0].Rows[0]["ClientChargeRate"].ToString());
                    objEmployee.employmentInformation.HireDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["HireDate"].ToString());
                    objEmployee.employmentInformation.ResignationDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["ResignationDate"].ToString());
                    objEmployee.employmentInformation.EmployeeContractTerms = Convert.ToString(objDS.Tables[0].Rows[0]["EmployeeContractTerms"]);

                    objEmployee.employmentInformation.EmployementMode = Convert.ToString(objDS.Tables[0].Rows[0]["EmployementMode"]);
                    objEmployee.employmentInformation.JobTitleID = objDS.Tables[0].Rows[0]["JobTitleID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["JobTitleID"].ToString());
                    objEmployee.employmentInformation.SalaryPackage = objDS.Tables[0].Rows[0]["SalaryPackage"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["SalaryPackage"].ToString());
                    objEmployee.employmentInformation.Qualification = Convert.ToString(objDS.Tables[0].Rows[0]["Qualification"]);

                    objEmployee.employmentInformation.CompanyId = objDS.Tables[0].Rows[0]["CompanyId"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["CompanyId"].ToString());
                    objEmployee.employmentInformation.EmployeeSectionID = objDS.Tables[0].Rows[0]["EmployeeSectionID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["EmployeeSectionID"].ToString());
                    objEmployee.employmentInformation.EmployeeJobCategoryID = objDS.Tables[0].Rows[0]["EmployeeJobCategoryID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["EmployeeJobCategoryID"].ToString());
                    objEmployee.employmentInformation.ShiftID = objDS.Tables[0].Rows[0]["ShiftID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["ShiftID"].ToString());
                    objEmployee.employmentInformation.GroupNameID = objDS.Tables[0].Rows[0]["GroupNameID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["GroupNameID"].ToString());
                    objEmployee.employmentInformation.LeftDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["LeftDate"].ToString());
                    objEmployee.employmentInformation.LeftReasonID = objDS.Tables[0].Rows[0]["LeftReason"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["LeftReason"].ToString());                      //Changed string to int for Left Reason dropdown
                    objEmployee.employmentInformation.MOETitle = objDS.Tables[0].Rows[0]["MOETitle"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["MOETitle"].ToString());
                    objEmployee.employmentInformation.MOEHireDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["MOEHireDate"].ToString());
                    objEmployee.employmentInformation.MOELeftDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["MOELeftDate"].ToString());
                    objEmployee.employmentInformation.MOLTitle = objDS.Tables[0].Rows[0]["MOLTitle"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[0].Rows[0]["MOLTitle"].ToString());
                    objEmployee.employmentInformation.MOLStartDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[0].Rows[0]["MOLStartDate"].ToString());
                    objEmployee.employmentInformation.MOLPersonalId = Convert.ToString(objDS.Tables[0].Rows[0]["MOLPersonalId"]);

                    objEmployee.employmentInformation.PositionName = Convert.ToString(objDS.Tables[0].Rows[0]["PositionTitle"]);
                    objEmployee.employmentInformation.DepartmentName = Convert.ToString(objDS.Tables[0].Rows[0]["DepartmentName_1"]);
                    objEmployee.employmentInformation.isOverseasrecruited = objDS.Tables[0].Rows[0]["OverseasRecruited"] == DBNull.Value ? false : Convert.ToBoolean(objDS.Tables[0].Rows[0]["OverseasRecruited"]);
                    //    objEmployee.employmentInformation.EmpStatus = Convert.ToString(objDS.Tables[0].Rows[0]["EmployeeStatus"]);
                    objEmployee.employmentInformation.AnnualAirTicket = Convert.ToBoolean(objDS.Tables[0].Rows[0]["AnnualAirTicket"]);
                    objEmployee.employmentInformation.LifeInsurance = Convert.ToBoolean(objDS.Tables[0].Rows[0]["LifeInsurance"]);
                    objEmployee.employmentInformation.HealthInsurance = Convert.ToBoolean(objDS.Tables[0].Rows[0]["HealthInsurance"]);
                    objEmployee.employmentInformation.Accommodation = Convert.ToBoolean(objDS.Tables[0].Rows[0]["Accommodation"]);
                }

                if (objDS.Tables[1].Rows.Count > 0)
                {
                    objEmployee.employeeDetailsModel.EmployeeID = objDS.Tables[1].Rows[0]["EmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["EmployeeID"]);
                    objEmployee.employeeDetailsModel.EmployeeAlternativeID = objDS.Tables[1].Rows[0]["EmployeeAlternativeID"] == DBNull.Value ? "" : Convert.ToString(objDS.Tables[1].Rows[0]["EmployeeAlternativeID"]);
                    objEmployee.employeeDetailsModel.TitleID = objDS.Tables[1].Rows[0]["TitleID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["TitleID"]);
                    objEmployee.employeeDetailsModel.FirstName_1 = Convert.ToString(objDS.Tables[1].Rows[0]["FirstName_1"]);
                    objEmployee.employeeDetailsModel.FirstName_2 = Convert.ToString(objDS.Tables[1].Rows[0]["FirstName_2"]);
                    objEmployee.employeeDetailsModel.FirstName_3 = Convert.ToString(objDS.Tables[1].Rows[0]["FirstName_3"]);
                    objEmployee.employeeDetailsModel.SurName_1 = Convert.ToString(objDS.Tables[1].Rows[0]["SurName_1"]);
                    objEmployee.employeeDetailsModel.SurName_2 = Convert.ToString(objDS.Tables[1].Rows[0]["SurName_2"]);
                    objEmployee.employeeDetailsModel.SurName_3 = Convert.ToString(objDS.Tables[1].Rows[0]["SurName_3"]);
                    objEmployee.employeeDetailsModel.MiddleName_1 = Convert.ToString(objDS.Tables[1].Rows[0]["MiddleName_1"]);
                    objEmployee.employeeDetailsModel.MiddleName_2 = Convert.ToString(objDS.Tables[1].Rows[0]["MiddleName_2"]);
                    objEmployee.employeeDetailsModel.MiddleName_3 = Convert.ToString(objDS.Tables[1].Rows[0]["MiddleName_3"]);
                    objEmployee.employeeDetailsModel.MothersName_1 = Convert.ToString(objDS.Tables[1].Rows[0]["MothersName_1"]);
                    objEmployee.employeeDetailsModel.MothersName_2 = Convert.ToString(objDS.Tables[1].Rows[0]["MothersName_2"]);
                    objEmployee.employeeDetailsModel.MothersName_3 = Convert.ToString(objDS.Tables[1].Rows[0]["MothersName_3"]);
                    objEmployee.employeeDetailsModel.BirthDate = CommonDB.GetFormattedDate_DDMMYYYY(objDS.Tables[1].Rows[0]["BirthDate"].ToString());
                    objEmployee.employeeDetailsModel.GenderID = objDS.Tables[1].Rows[0]["GenderID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["GenderID"]);
                    objEmployee.employeeDetailsModel.BirthCountryID = objDS.Tables[1].Rows[0]["BirthCountryID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["BirthCountryID"].ToString());
                    objEmployee.employeeDetailsModel.BirthPlaceID = objDS.Tables[1].Rows[0]["BirthPlaceID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["BirthPlaceID"].ToString());
                    objEmployee.employeeDetailsModel.ReligionID = objDS.Tables[1].Rows[0]["ReligionID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["ReligionID"].ToString());
                    // objEmployee.employeeDetailsModel.NationalityID = Convert.ToString(objDS.Tables[1].Rows[0]["NationalityID"]);
                    //  objEmployee.employeeDetailsModel.SelectedValues = lstString;
                    objEmployee.employeeDetailsModel.MaritalStatusID = objDS.Tables[1].Rows[0]["MaritalStatusID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["MaritalStatusID"].ToString());
                    objEmployee.employeeDetailsModel.HomeLanguageID = objDS.Tables[1].Rows[0]["HomeLanguageID"] == DBNull.Value ? 0 : Convert.ToInt32(objDS.Tables[1].Rows[0]["HomeLanguageID"].ToString());
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }

            return objEmployee;
        }

        public List<EmployeeDetailsModel> GetAllEmployeeByCompanyId(int CompanyId)
        {
            List<EmployeeDetailsModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDetailsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeByCompanyId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeDetailsModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeDetailsModel();

                        employeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["FirstName"] + " " + sqlDataReader["LastName"]);
                        employeeModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName"]);
                        employeeModel.SurName_1 = Convert.ToString(sqlDataReader["LastName"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }
        public List<EmployeeDetailsModel> GetAllEmployeeForAdminByCompanyID(int CompanyId)
        {
            List<EmployeeDetailsModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDetailsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetAllEmployeeForAdminByCompanyID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeDetailsModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeDetailsModel();

                        employeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["FullName"]);
                        employeeModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName"]);
                        employeeModel.SurName_1 = Convert.ToString(sqlDataReader["LastName"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }
        public List<EmployeeDetailsModel> GetAllEmployeeByCompanyAndUser(int CompanyID, int UserID)
        {
            List<EmployeeDetailsModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDetailsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeByCompanyAndUser", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@UserID", UserID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeDetailsModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeDetailsModel();

                        employeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["FullName"]);
                        employeeModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName"]);
                        employeeModel.SurName_1 = Convert.ToString(sqlDataReader["LastName"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }
        public List<EmployeeDetailsModel> GetAllEmployeeByCompanyIdandEmpName(int CompanyId, string EmployeeName)
        {
            List<EmployeeDetailsModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDetailsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_SearchEmployeeByCompanyAndEmpName", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Name", EmployeeName);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeDetailsModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeDetailsModel();

                        employeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        //employeeModel.FullName = Convert.ToString(sqlDataReader["FirstName"] + " " + sqlDataReader["LastName"]);
                        employeeModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"]);
                        employeeModel.SurName_1 = Convert.ToString(sqlDataReader["SurName_1"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }

        public List<Employee> GetEmployeeList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, string isActive, out int totalCount)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetEmployeeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
                sqlCommand.Parameters.AddWithValue("@isActive", isActive);

                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.employeeDetailsModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlternativeID"].ToString());

                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? 0 : sqlDataReader["CompanyId"]);
                        employeeModel.genderModel.GenderName_1 = Convert.ToString(sqlDataReader["GenderName"] == DBNull.Value ? "" : sqlDataReader["GenderName"]);
                        employeeModel.employeeDetailsModel.BirthDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DOB"].ToString());
                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();
                        employeeModel.employeeDetailsModel.isActive = Convert.ToBoolean(sqlDataReader["isActive"]);
                        employeeModel.employmentInformation.PositionName = Convert.ToString(sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"]);
                        employeeList.Add(employeeModel);
                    }
                }
                sqlConnection.Close();

                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public List<EmployeeDetailsModel> GetAllEmployee()
        {
            List<EmployeeDetailsModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDetailsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_getEmployees", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeDetailsModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeDetailsModel();

                        employeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        //employeeModel.FullName = Convert.ToString(sqlDataReader["FirstName"] + " " + sqlDataReader["LastName"]);
                        employeeModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName"]);
                        employeeModel.SurName_1 = Convert.ToString(sqlDataReader["LastName"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();

                // employeeList = employeeList.OrderBy(x => x.FirstName_1).ToList();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }

        public OperationDetails UpdateEmploymeeLeaveReason(EmploymentInformation employmentInformation)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateEmployeeLeaveReason", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employmentInformation.EmployeeID);

                sqlCommand.Parameters.AddWithValue("@LeftDate", CommonDB.SetCulturedDate(employmentInformation.LeftDate));
                sqlCommand.Parameters.AddWithValue("@LeftReasonID", employmentInformation.LeftReasonID);              //Changed string to int for Left Reason dropdown


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        public OperationDetails UpdateEmploymeeJoingDateWithStatus(EmploymentInformation employmentInformation)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateEmployeeJoingDateWithStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employmentInformation.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", CommonDB.SetCulturedDate(employmentInformation.HireDate));
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        public OperationDetails UpdateEmployeeFamilyId(int EmployeeId, int familyId)
        {
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int rowCount = 1;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateEmployeeFamilyId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@employeeid", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@selectedFamilyID", familyId);
                SqlParameter Success = new SqlParameter("@isUpdated", SqlDbType.Bit);
                Success.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Success);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                bool isUpdated = Convert.ToBoolean(Success.Value.ToString() == "" ? "false" : Success.Value.ToString());
                if (isUpdated)
                {
                    oDetails.Success = true;
                    oDetails.CssClass = "success";
                }
                else
                {
                    oDetails.Success = false;
                    oDetails.CssClass = "warning";
                }
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public List<SchoolHouse> GetSchoolHouseList()
        {
            List<SchoolHouse> schoolHouseList = null;
            try
            {
                schoolHouseList = new List<SchoolHouse>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT * FROM Gen_Housing", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    SchoolHouse schoolHouseModel;
                    while (sqlDataReader.Read())
                    {
                        schoolHouseModel = new SchoolHouse();
                        schoolHouseModel.HOID = Convert.ToInt32(sqlDataReader["HOID"].ToString());
                        schoolHouseModel.HousingName_1 = Convert.ToString(sqlDataReader["HousingName_1"]);
                        schoolHouseList.Add(schoolHouseModel);

                    }
                }
                sqlConnection.Close();

                // employeeList = employeeList.OrderBy(x => x.FirstName_1).ToList();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return schoolHouseList;
        }

        public List<EmploymentInformation> GetEmployeeHireDateInformation()
        {
            List<EmploymentInformation> employmentInformationList = null;
            try
            {
                employmentInformationList = new List<EmploymentInformation>();
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("SELECT ED.EmployeeID, EI.HireDate FROM HR_EmploymentInformation AS EI inner join HR_EmployeeDetail ED ON EI.EmployeeID = ED.EmployeeID WHERE ED.isDeleted = 0 AND isActive = 1 AND ED.EmployeeID > 0");
                foreach (DataRow dr in dt.Rows)
                {
                    EmploymentInformation EmploymentInformation = new EmploymentInformation();
                    EmploymentInformation.EmployeeID = Convert.ToInt32(dr["EmployeeID"].ToString());
                    EmploymentInformation.HireDate = Convert.ToString(dr["HireDate"] == DBNull.Value ? "" : dr["HireDate"]);
                    employmentInformationList.Add(EmploymentInformation);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employmentInformationList;
        }

        public List<EmployeeHireDateInformation> GetEmployeeInformationWithHireDate()
        {
            List<EmployeeHireDateInformation> employmentInformationList = null;
            try
            {
                employmentInformationList = new List<EmployeeHireDateInformation>();
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("SELECT ED.EmployeeAlternativeID , ED.FirstName_1, ED.SurName_1, ED.EmployeeID, EI.HireDate FROM HR_EmploymentInformation AS EI inner join HR_EmployeeDetail ED ON EI.EmployeeID = ED.EmployeeID WHERE ED.isDeleted = 0 AND isActive = 1 AND ED.EmployeeID > 0  ORDER BY ED.FirstName_1 ");
                foreach (DataRow dr in dt.Rows)
                {
                    EmployeeHireDateInformation employeeHireDateInformation = new EmployeeHireDateInformation();
                    employeeHireDateInformation.EmployeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                    employeeHireDateInformation.HireDate = Convert.ToString(dr["HireDate"] == DBNull.Value ? "" : dr["HireDate"]);
                    employeeHireDateInformation.EmployeeName = "(" + dr["EmployeeAlternativeID"].ToString() + ") " + dr["FirstName_1"].ToString() + " " + dr["SurName_1"].ToString();
                    employmentInformationList.Add(employeeHireDateInformation);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employmentInformationList;
        }

        public List<EmployeeHireDateInformation> GetEmployeeInformationWithHireAndTerminationDate(DateTime dDate)
        {
            List<EmployeeHireDateInformation> employmentInformationList = null;
            try
            {
                string query = "SELECT ED.EmployeeAlternativeID, "
                         + "ED.FirstName_1 ,"
                         + "ED.SurName_1 ,"
                         + "ED.EmployeeID ,"
                         + "EI.HireDate ,"
                         + "EI.leftdate "
                         + "FROM    HR_EmploymentInformation AS EI "
                         + "INNER JOIN HR_EmployeeDetail ED ON EI.EmployeeID = ED.EmployeeID "
                         + "WHERE ED.EmployeeID > 0 "
                         + "AND(ED.isActive = 1 "
                         + "AND EI.HireDate <= '" + dDate.ToString("MM/dd/yyyy") + "' "
                         + "AND(EI.LeftDate IS NULL "
                         + "OR EI.LeftDate >= '" + dDate.ToString("MM/dd/yyyy") + "' "
                         + ") ) OR (ED.isActive <> 1 AND EI.HireDate <= '" + dDate.ToString("MM/dd/yyyy") + "' "
                         + " AND EI.LeftDate >= '" + dDate.ToString("MM/dd/yyyy") + "' "
                         + ") ORDER BY ED.FirstName_1";

                employmentInformationList = new List<EmployeeHireDateInformation>();
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText(query);
                foreach (DataRow dr in dt.Rows)
                {
                    EmployeeHireDateInformation employeeHireDateInformation = new EmployeeHireDateInformation();
                    employeeHireDateInformation.EmployeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                    employeeHireDateInformation.HireDate = Convert.ToString(dr["HireDate"] == DBNull.Value ? "" : dr["HireDate"]);
                    employeeHireDateInformation.EmployeeName = "(" + dr["EmployeeAlternativeID"].ToString() + ") " + dr["FirstName_1"].ToString() + " " + dr["SurName_1"].ToString();
                    employmentInformationList.Add(employeeHireDateInformation);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employmentInformationList;
        }

        public bool checkHireDateIsGreater(DateTime compareDate, string hireDate)
        {
            bool isHireDateGreater = false;
            if (hireDate == "")
                isHireDateGreater = false;
            else
            {
                if (compareDate >= Convert.ToDateTime(hireDate))
                {
                    isHireDateGreater = false;
                }
                else
                    isHireDateGreater = true;
            }
            return isHireDateGreater;
        }

        public bool UploadPhoto(byte[] photo, int employeeId, string extention)
        {
            dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@Image", photo));
            parameterList.Add(new SqlParameter("@EmployeeId", employeeId));
            parameterList.Add(new SqlParameter("@Extention", extention));
            parameterList.Add(new SqlParameter("@mode", "2"));
            return dataHelper.ExcutestoredProcedure(parameterList, "Hr_Stp_CURD_UserPhoto");
        }

        public byte[] GetUserImage(int employeeId)
        {
            dataHelper = new DataHelper();
            List<byte> dataContain = new List<byte>();
            List<SqlParameter> parameterList = new List<SqlParameter>();

            parameterList.Add(new SqlParameter("@EmployeeId", employeeId));
            parameterList.Add(new SqlParameter("@mode", "1"));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_CURD_UserPhoto");
            foreach (DataRow dr in dt.Rows)
            {
                dataContain = ((byte[])dr["Photo"]).ToList();
            }
            return dataContain.ToArray();
        }

        public EmployeeIdSetting GetEmployeeIdSetting()
        {
            EmployeeIdSetting objEmployeeIdSetting = new EmployeeIdSetting();
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Select EmployeeAlterIDPrefix,EmployeeAlterIDStartFrom FROM HR_EmployeeAlternativeID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmployeeIdSetting = new EmployeeIdSetting();
                        objEmployeeIdSetting.EmployeeIdStartFrom = Convert.ToInt32(sqlDataReader["EmployeeAlterIDStartFrom"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlterIDStartFrom"].ToString());
                        objEmployeeIdSetting.EmployeeIdPrefix = Convert.ToString(sqlDataReader["EmployeeAlterIDPrefix"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlterIDPrefix"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objEmployeeIdSetting;
        }

        public OperationDetails AddEmpAlternativeIdSettings(string Prefix, int? EmployeeIdStartFrom)
        {
            OperationDetails op = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddEditEmployeeAlternativeIdSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@Prefix", Prefix);
                sqlCommand.Parameters.AddWithValue("@IdStartFrom", EmployeeIdStartFrom);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Message = OperationMessage.Value.ToString();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while saving Employee Alternative Id setting.";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public OperationDetails DeleteEmpAlternativeIdSettings()
        {
            OperationDetails op = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("DELETE FROM HR_EmployeeAlternativeID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = true;
                op.CssClass = "success";
                op.Message = "Employee alternative id setting deleted successfully";
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while deleting Employee Alternative Id setting.";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public bool checkUniqueNationalityId(int EmployeeId, string NationalityIdCardNo)
        {
            bool isUnique = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckUniqueNationalityIDCardNo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@NationalityCardNo", NationalityIdCardNo);
                SqlParameter Opsuccess = new SqlParameter("@isUniq", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                isUnique = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;

            }
            catch (Exception exception)
            {
                isUnique = false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return isUnique;
        }
        public EmployeeWorkEmailSetting GetEmployeeWorkEmailSetting()
        {
            EmployeeWorkEmailSetting workEmailSetting = null;
            try
            {
                workEmailSetting = new EmployeeWorkEmailSetting(); ;
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("select MailDomainName,Formula from dbo.GEN_UserNameFormulas CROSS JOIN HR_MailSettings where ID=2");
                foreach (DataRow dr in dt.Rows)
                {
                    workEmailSetting = new EmployeeWorkEmailSetting();
                    workEmailSetting.EmailDomainName = Convert.ToString(dr["MailDomainName"] == DBNull.Value ? "" : dr["MailDomainName"]);
                    workEmailSetting.EmailIdSettingValue = Convert.ToString(dr["Formula"] == DBNull.Value ? "" : dr["Formula"]);
                    if (!string.IsNullOrEmpty(workEmailSetting.EmailIdSettingValue))
                    {
                        string[] arr = workEmailSetting.EmailIdSettingValue.Split('-');
                        workEmailSetting.WorkEmailFirstOption = Convert.ToInt32(arr[0]);
                        workEmailSetting.WorkEmailSecondOption = Convert.ToInt32(arr[1]);
                        workEmailSetting.WorkEmailThirdOption = Convert.ToInt32(arr[2]);
                    }

                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return workEmailSetting;
        }

        public List<EmployeeContactModel> GetAllEmployeeWorkEmailDetails()
        {
            List<EmployeeContactModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeContactModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT ED.EmployeeID,ED.EmployeeAlternativeID,ED.FirstName_1+' '+ED.SurName_1 + ' - ' + ISNULL(HC.ShortName_1,'') AS EmployeeName, EC.WorkEmail FROM dbo.HR_EmployeeContact EC INNER JOIN HR_EmployeeDetail ED on  EC.EmployeeID = ED.EmployeeID INNER JOIN HR_Company HC ON HC.CompanyId=ED.CompanyId WHERE ED.isActive = 1 and ED.EmployeeID != 0  order by EmployeeName", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeContactModel EmployeeContact;
                    while (sqlDataReader.Read())
                    {
                        EmployeeContact = new EmployeeContactModel();

                        EmployeeContact.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeContact.EmpAltId = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        EmployeeContact.WorkEmail = Convert.ToString(sqlDataReader["WorkEmail"]);
                        EmployeeContact.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        employeeList.Add(EmployeeContact);
                    }
                }
                sqlConnection.Close();

                employeeList = employeeList.OrderBy(x => x.EmployeeName).ToList();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }

        public EmployeeContactModel GetEmployeeWorkEmailDetailsByEmployeeID(int EmployeeId)
        {
            EmployeeContactModel objEmployeeContact = null;
            try
            {
                objEmployeeContact = new EmployeeContactModel();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT ED.EmployeeID,ED.EmployeeAlternativeID,ED.FirstName_1+' '+ISNULL(ED.SurName_1,'') AS EmployeeName, EC.WorkEmail FROM HR_EmployeeDetail ED LEFT JOIN  dbo.HR_EmployeeContact EC on  EC.EmployeeID = ED.EmployeeID WHERE ED.EmployeeID = " + EmployeeId + "  order by EmployeeName", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmployeeContact = new EmployeeContactModel();
                        objEmployeeContact.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        objEmployeeContact.EmpAltId = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        objEmployeeContact.WorkEmail = Convert.ToString(sqlDataReader["WorkEmail"]);
                        objEmployeeContact.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objEmployeeContact;
        }

        public OperationDetails SaveEmployeeWorkEmailSetting(string Formula, string DomainName)
        {
            OperationDetails op = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_AddWorkEmailSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@Formula", Formula);
                sqlCommand.Parameters.AddWithValue("@DomainName", DomainName);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    op.Message = "Work Email Setting saved successfully.";
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = "Error occurred while saving Work Email Setting.";
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while saving Employee Alternative Id setting.";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public OperationDetails CreateEmployeeWorkEmails(int Mode, string Formula, string DomainName)
        {
            OperationDetails op = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CreateEmployeeWorkEmails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);
                sqlCommand.Parameters.AddWithValue("@Formula", Formula);
                sqlCommand.Parameters.AddWithValue("@DomainName", DomainName);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    op.Message = "Work Email Setting saved successfully.";
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = "Error occurred while saving Work Email Setting.";
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while saving Employee Alternative Id setting.";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public EmployeeDetails GetEmployeedetailsById(int EmployeeId)
        {
            EmployeeDetails employee = null;
            try
            {
                employee = new EmployeeDetails();
                DataHelper dataHelper = new DataHelper();
                List<SqlParameter> lstParam = new List<SqlParameter>();
                lstParam.Add(new SqlParameter("@EmployeeId", EmployeeId));
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(lstParam, "Hr_Stp_GetEmployeeDetailsbyId");
                foreach (DataRow dr in dt.Rows)
                {
                    employee.EmployeeID = Convert.ToInt32(dr["EmployeeID"].ToString());
                    employee.EmployeeAlternativeID = dr["EmployeeAlternativeID"].ToString();
                    employee.FirstName_1 = Convert.ToString(dr["FirstName_1"] == DBNull.Value ? "" : dr["FirstName_1"]);
                    employee.SurName_1 = dr["SurName_1"].ToString();
                    employee.usertypeid = Convert.ToInt32(dr["UserTypeId"].ToString());
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employee;
        }

        //Expand Function and StoredProcedure as per need
        public EmploymentInformation GetEmpoymentInfoByEmployeeID(int EmployeeId)
        {
            EmploymentInformation objEmpInfo = new EmploymentInformation();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmploymentInFoByEmployeeId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmpInfo.EmployeeContractTermId = Convert.ToInt32(sqlDataReader["HRContractTypeID"].ToString());
                        objEmpInfo.MOLTitle = Convert.ToInt32(sqlDataReader["MOLTitle"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objEmpInfo;
        }

        public List<Employee> GetAllEmployeeActiveByCredential(int UserId)
        {
            List<Employee> objEmployeeList = new List<Employee>();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllEmployeeActiveByCredential", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"].ToString());
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.Name = "(" + employeeModel.EmployeeAlternativeID + ") " + Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyID"] == DBNull.Value ? "0" : sqlDataReader["CompanyID"]);
                        objEmployeeList.Add(employeeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return objEmployeeList;
        }

        public List<Employee> GetEmployeeByActive(bool? isActive, int UserId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeeByActive", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@IsActive", isActive);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeModel.employeeDetailsModel.usertypeid = Convert.ToInt32(sqlDataReader["usertypeid"] == DBNull.Value ? "0" : sqlDataReader["usertypeid"].ToString());
                        employeeModel.employeeDetailsModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyId"] == DBNull.Value ? "0" : sqlDataReader["CompanyId"].ToString());
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<Employee> GetIncrementedSalaryEmployee()
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetIncrementedSalaryEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<Employee> GetEmployeeJoinInLastMonth()
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeeJoinInLastMonth", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<Employee> GetEmployeeFromGratuity()
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeeFromGratuity", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return employeeList.OrderBy(x => x.FirstName).ToList();
        }

        public List<Employee> GetSupervisiorList()
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSupurVisor", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = "(" + Convert.ToString(sqlDataReader["EmployeeAlternativeID"]) + ") " + Convert.ToString(sqlDataReader["EmployeeName"].ToString());
                        employeeModel.FullName = Convert.ToString(sqlDataReader["EmployeeName"].ToString());
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.OrderBy(x => x.FirstName).ToList();
        }

        public List<Employee> GetEmployeeBySupervisiorId(int superVisiorId, bool? isActive)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeeBySuperVisor", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@superVisorId", superVisiorId);
                sqlCommand.Parameters.AddWithValue("@IsActive", isActive);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = "(" + Convert.ToString(sqlDataReader["EmployeeAlternativeID"]) + ") " + Convert.ToString(sqlDataReader["EmployeeName"].ToString());
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public IEnumerable<SectionList> GetAllSectionList()
        {
            DataHelper datahelper = new GeneralDB.DataHelper();
            string query = "SELECT  EmployeeSectionID,EmployeeSectionName_1 FROM [dbo].[GEN_EmployeeSection] WHERE   ISNULL(IsDeleted, 0) = 0 ORDER BY EmployeeSectionID ASC";
            List<SectionList> sectionList = new List<SectionList>();
            DataTable dt = datahelper.ExcuteCommandText(query);
            SectionList sectionmodel;
            foreach (DataRow dr in dt.Rows)
            {
                sectionmodel = new SectionList();
                sectionmodel.id = Convert.ToInt32(dr["EmployeeSectionID"].ToString());
                sectionmodel.text = dr["EmployeeSectionName_1"].ToString();
                sectionList.Add(sectionmodel);
            }
            return sectionList.AsEnumerable();
        }

        public Employee GetEmployeeInfoByUserID(int UserID)
        {
            Employee employeeModel = new Employee();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeInfoByUserId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();
                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        employeeModel.EmailId = Convert.ToString(sqlDataReader["PersonalEmail"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["FullName"] == DBNull.Value ? "" : sqlDataReader["FullName"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return employeeModel;
        }

        public List<Employee> GetEmployeesForReplacement(int UserId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeesForReplacement", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public EmployeeContactModel GetEmployeeConatctDetail(int employeeId)
        {
            EmployeeContactModel employeeContactModel = new EmployeeContactModel();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetEmployeeContact", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        employeeContactModel = new EmployeeContactModel();
                        employeeContactModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        employeeContactModel.PersonalEmail = Convert.ToString(sqlDataReader["PersonalEmail"]);

                        employeeContactModel.IMContact = Convert.ToString(sqlDataReader["IMContact"]);
                        employeeContactModel.IMContactType = Convert.ToInt32(sqlDataReader["IMContactType"] == DBNull.Value ? "0" : sqlDataReader["IMContactType"].ToString());
                        employeeContactModel.MobileNumberHome = Convert.ToString(sqlDataReader["MobileNumberHome"]);
                        employeeContactModel.MobileNumberWork = Convert.ToString(sqlDataReader["MobileNumberWork"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return employeeContactModel;
        }

        public OperationDetails UpdateContactInformation(EmployeeContactModel employeeContactModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateEmployeeContact", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeContactModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PersonalEmail", employeeContactModel.PersonalEmail == null ? "" : employeeContactModel.PersonalEmail);
                sqlCommand.Parameters.AddWithValue("@IMContact", employeeContactModel.IMContact == null ? "" : employeeContactModel.IMContact);
                sqlCommand.Parameters.AddWithValue("@IMContactType", employeeContactModel.IMContactType);
                sqlCommand.Parameters.AddWithValue("@MobileNumberHome", employeeContactModel.MobileNumberHome == null ? "" : employeeContactModel.MobileNumberHome);
                sqlCommand.Parameters.AddWithValue("@MobileNumberWork", employeeContactModel.MobileNumberWork == null ? "" : employeeContactModel.MobileNumberWork);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeContactModel.ModifiedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, employeeContactModel.EmployeeID);

        }


        public EmployeeAddressInfoModel GetEmployeeAddressInformation(int employeeId)
        {
            EmployeeAddressInfoModel employeeAddressInfoModel = new EmployeeAddressInfoModel();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetEmployeeAddress", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        employeeAddressInfoModel = new EmployeeAddressInfoModel();
                        employeeAddressInfoModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeAddressInfoModel.AddressLine = Convert.ToString(sqlDataReader["AddressLine1_1"]);
                        employeeAddressInfoModel.AddressLine2 = Convert.ToString(sqlDataReader["AddressLine2_1"]);
                        employeeAddressInfoModel.AddressLine1_2 = Convert.ToString(sqlDataReader["AddressLine1_2"]);
                        employeeAddressInfoModel.AddressLine2_2 = Convert.ToString(sqlDataReader["AddressLine2_2"]);
                        employeeAddressInfoModel.AddressLine1_3 = Convert.ToString(sqlDataReader["AddressLine1_3"]);
                        employeeAddressInfoModel.AddressLine2_3 = Convert.ToString(sqlDataReader["AddressLine2_3"]);

                        employeeAddressInfoModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"] == DBNull.Value ? "0" : sqlDataReader["CountryID"].ToString());
                        employeeAddressInfoModel.StateID = Convert.ToInt32(sqlDataReader["StateID"] == DBNull.Value ? "0" : sqlDataReader["StateID"].ToString());
                        employeeAddressInfoModel.CityID = Convert.ToInt32(sqlDataReader["CityID"] == DBNull.Value ? "0" : sqlDataReader["CityID"].ToString());
                        employeeAddressInfoModel.ApartmentNo = Convert.ToString(sqlDataReader["AppartmentNumber"]);
                        employeeAddressInfoModel.PostCode = Convert.ToString(sqlDataReader["PostCode"]);
                        employeeAddressInfoModel.Street = Convert.ToString(sqlDataReader["Street"]);
                        employeeAddressInfoModel.PoBox = Convert.ToString(sqlDataReader["PoBox"]);
                        employeeAddressInfoModel.AreaId = Convert.ToInt32(sqlDataReader["AreaId"] == DBNull.Value ? "0" : sqlDataReader["AreaId"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return employeeAddressInfoModel;
        }

        public OperationDetails UpdateAddressInformation(EmployeeAddressInfoModel employeeAddressInfo,int userId)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateEmployeeAddress", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeAddressInfo.EmployeeID);

                sqlCommand.Parameters.AddWithValue("@AddressLine1_1", employeeAddressInfo.AddressLine == null ? "" : employeeAddressInfo.AddressLine);
                sqlCommand.Parameters.AddWithValue("@AddressLine2_1", employeeAddressInfo.AddressLine2 == null ? "" : employeeAddressInfo.AddressLine2);

                sqlCommand.Parameters.AddWithValue("@AddressLine1_2", employeeAddressInfo.AddressLine1_2 == null ? "" : employeeAddressInfo.AddressLine1_2);
                sqlCommand.Parameters.AddWithValue("@AddressLine2_2", employeeAddressInfo.AddressLine2_2 == null ? "" : employeeAddressInfo.AddressLine2_2);

                sqlCommand.Parameters.AddWithValue("@AddressLine1_3", employeeAddressInfo.AddressLine1_3 == null ? "" : employeeAddressInfo.AddressLine1_3);
                sqlCommand.Parameters.AddWithValue("@AddressLine2_3", employeeAddressInfo.AddressLine2_3 == null ? "" : employeeAddressInfo.AddressLine2_3);

                sqlCommand.Parameters.AddWithValue("@AppartmentNumber", employeeAddressInfo.ApartmentNo == null ? "" : employeeAddressInfo.ApartmentNo);
                sqlCommand.Parameters.AddWithValue("@POBOX", employeeAddressInfo.PoBox == null ? "" : employeeAddressInfo.PoBox);
                sqlCommand.Parameters.AddWithValue("@CityID", employeeAddressInfo.CityID);
                sqlCommand.Parameters.AddWithValue("@AreaID", employeeAddressInfo.AreaId);
                sqlCommand.Parameters.AddWithValue("@CountryID", employeeAddressInfo.CountryID);
                sqlCommand.Parameters.AddWithValue("@Street", employeeAddressInfo.Street == null ? "" : employeeAddressInfo.Street);
                sqlCommand.Parameters.AddWithValue("@PostCode", employeeAddressInfo.PostCode == null ? "" : employeeAddressInfo.PostCode);
                sqlCommand.Parameters.AddWithValue("@StateID", employeeAddressInfo.StateID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", userId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, employeeAddressInfo.EmployeeID);

        }
        public OperationDetails IsEmployeeWorkEmailExists(string workEmail, int? profileRequestInstanceID, int? employeeID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_IsEmployeeWorkEmailExists", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@WorkEmail", workEmail);
                sqlCommand.Parameters.AddWithValue("@ProfileRequestInstanceID", profileRequestInstanceID==null?(object)DBNull.Value: profileRequestInstanceID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID == null ? (object)DBNull.Value : employeeID);
                // sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", userID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 1000);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
    }
}
