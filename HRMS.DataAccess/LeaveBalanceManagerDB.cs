﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class LeaveBalanceManagerDB : BaseDB
    {
        public List<LeaveBalanceManagerModel> GetLeaveBalanceManagerList(int? userID, int? employeeID, int? departmentID, int? companyId)
        {
            List<LeaveBalanceManagerModel> leaveBalanceManagerModelList = new List<LeaveBalanceManagerModel>();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetLeaveBalanceManager", sqlConnection);
                if (employeeID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@pEmployeeID", employeeID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@pEmployeeID", DBNull.Value));
                if (departmentID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@DepartmentID", departmentID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@DepartmentID", DBNull.Value));
                if (companyId > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", companyId));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", DBNull.Value));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", userID));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 300;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DateTime DateValue;
                if (sqlDataReader.HasRows)
                {
                    LeaveBalanceManagerModel leaveBalanceManagerModel;
                    while (sqlDataReader.Read())
                    {
                        leaveBalanceManagerModel = new LeaveBalanceManagerModel();
                        leaveBalanceManagerModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        leaveBalanceManagerModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        leaveBalanceManagerModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"].ToString());
                        leaveBalanceManagerModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"].ToString());
                        leaveBalanceManagerModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"].ToString());                        
                        if (DateTime.TryParse(sqlDataReader["HireDate"].ToString(), out DateValue))
                            leaveBalanceManagerModel.HireDate = DateValue;
                        else
                            leaveBalanceManagerModel.HireDate = (DateTime?)null; 
                        leaveBalanceManagerModel.LapsingDays = decimal.Parse(sqlDataReader["LapsingDays"].ToString());
                        if (DateTime.TryParse(sqlDataReader["LapseDate"].ToString(), out DateValue))
                            leaveBalanceManagerModel.LapseDate = DateValue;
                        else
                            leaveBalanceManagerModel.LapseDate = (DateTime?)null;
                       
                        leaveBalanceManagerModel.ExtendedLapsingDays = sqlDataReader["ExtendedLapsingDays"] != DBNull.Value ? decimal.Parse(sqlDataReader["ExtendedLapsingDays"].ToString()) : (decimal?)null;
                        if (DateTime.TryParse(sqlDataReader["ExtendedLapseDate"].ToString(), out DateValue))
                            leaveBalanceManagerModel.ExtendedLapseDate = DateValue;
                        else
                            leaveBalanceManagerModel.ExtendedLapseDate = (DateTime?)null;
                        leaveBalanceManagerModel.VacationTypeID = Convert.ToInt16(sqlDataReader["VacationTypeID"].ToString());
                        leaveBalanceManagerModel.VacationTypeName = Convert.ToString(sqlDataReader["VacationTypeName"].ToString());
                        leaveBalanceManagerModel.TotalDays = float.Parse(sqlDataReader["TotalDays"].ToString());
                        leaveBalanceManagerModel.BalanceLeaveDays = decimal.Parse(sqlDataReader["BalanceLeaveDays"].ToString());
                        leaveBalanceManagerModel.IsActive = bool.Parse(sqlDataReader["IsActive"].ToString());
                        leaveBalanceManagerModel.IsAccumulatedLeave = bool.Parse(sqlDataReader["IsAccumulatedLeave"].ToString());
                        leaveBalanceManagerModel.VTCategoryID = sqlDataReader["VTCategoryID"] != DBNull.Value ? Convert.ToInt32(sqlDataReader["VTCategoryID"]) : 0;
                        leaveBalanceManagerModel.AnnualVacationTypeID = sqlDataReader["AnnualVacationTypeID"] != DBNull.Value ? Convert.ToInt32(sqlDataReader["AnnualVacationTypeID"]) : 0;
                        leaveBalanceManagerModelList.Add(leaveBalanceManagerModel);
                    }
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return leaveBalanceManagerModelList;
        }

     

        public OperationDetails UpdateAccumulativeAllVacation(List<AccumulativeAllVacations> accumulativeAllVacationList, int userId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("EmployeeID");
                resultTable.Columns.Add("VacationTypeID");
                resultTable.Columns.Add("NoOfDays");
                resultTable.Columns.Add("ExtendedDays");
                resultTable.Columns.Add("ExtendedDate",typeof(DateTime));
                resultTable.Columns.Add("LapsingDate", typeof(DateTime));
                resultTable.Columns.Add("LapsingDays"); 
                foreach (AccumulativeAllVacations obj in accumulativeAllVacationList)
                {
                    resultTable.Rows.Add(obj.EmployeeID, obj.VacationTypeID, obj.Days, obj.ExtendedDays, obj.ExtendedDate, obj.LapsingDate, obj.LapsingDays);
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateAccumulativeAllVacation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", userId);
                sqlCommand.Parameters.AddWithValue("@HR_AccumulativeVacation", resultTable);
                SqlParameter OperationMessage = new SqlParameter("@Output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
            }
            catch (Exception ex)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred "+ex.Message;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public List<LeaveAccrualReportModel> GetLeaveAccrualReport(int? companyID, int? departmentID, int? employeeID, int userID, string fromDate, string todate)
        {
            List<LeaveAccrualReportModel> leaveAccrualReportList = new List<LeaveAccrualReportModel>();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetLeaveAccrualReport", sqlConnection);
                if (companyID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", companyID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", DBNull.Value));
                if (departmentID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@DepartmentID", departmentID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@DepartmentID", DBNull.Value));
                if (employeeID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@EmployeeID", employeeID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@EmployeeID", DBNull.Value));
                if(!string.IsNullOrEmpty(fromDate))
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(fromDate));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@FromDate", DBNull.Value));
                if (!string.IsNullOrEmpty(todate))
                    sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(todate));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@ToDate", DBNull.Value));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", userID));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 300;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
              
                if (sqlDataReader.HasRows)
                {
                    LeaveAccrualReportModel leaveAccrualReportModel;
                    while (sqlDataReader.Read())
                    {
                        leaveAccrualReportModel = new LeaveAccrualReportModel();
                        leaveAccrualReportModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        leaveAccrualReportModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        leaveAccrualReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        leaveAccrualReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"]);
                        leaveAccrualReportModel.DesignationName = Convert.ToString(sqlDataReader["DesignationName"]);
                        leaveAccrualReportModel.EmailID = Convert.ToString(sqlDataReader["EmailID"].ToString());

                        leaveAccrualReportModel.AccrualDate= !string.IsNullOrEmpty(sqlDataReader["AccrualDate"].ToString())? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AccrualDate"].ToString()):"";
                        //if (DateTime.TryParse(sqlDataReader["AccrualDate"].ToString(), out DateValue))
                        //    leaveAccrualReportModel.AccrualDate = DateValue;
                        leaveAccrualReportModel.AccrualDays = Convert.ToString(sqlDataReader["AccrualDays"].ToString());
                        leaveAccrualReportModel.TotalAccrualDaysBefore = Convert.ToString(sqlDataReader["TotalAccrualDaysBefore"]);
                        leaveAccrualReportModel.TotalAccrualDaysAfter = Convert.ToString(sqlDataReader["TotalAccrualDaysAfter"]);
                        leaveAccrualReportModel.ModifiedByName = Convert.ToString(sqlDataReader["ModifiedByName"]);
                        leaveAccrualReportModel.ModifiedOn = !string.IsNullOrEmpty(sqlDataReader["ModifiedOn"].ToString()) ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["ModifiedOn"].ToString()) : "";
                        //if (DateTime.TryParse(sqlDataReader["ModifiedOn"].ToString(), out DateValue))
                        //    leaveAccrualReportModel.ModifiedOn = DateValue;
                        //else
                        //  leaveAccrualReportModel.ModifiedOn = (DateTime?)null;
                        // leaveAccrualReportModel.BalanceUpdated = string.IsNullOrEmpty(Convert.ToString(sqlDataReader["BalanceUpdated"])) ? (decimal?)null: decimal.Parse(sqlDataReader["BalanceUpdated"].ToString());
                        leaveAccrualReportModel.BalanceUpdated = string.IsNullOrEmpty(Convert.ToString(sqlDataReader["BalanceUpdated"])) ? "--" : Convert.ToString(sqlDataReader["BalanceUpdated"]);
                        leaveAccrualReportList.Add(leaveAccrualReportModel);
                    }
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return leaveAccrualReportList;
        }

        public OperationDetails SaveLeaveAccrual(string accrualDate)
        {          
            OperationDetails operationDetails = new OperationDetails();           
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DynamicallyInsertAccVacations_BEAM", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TodayDate", DateTime.ParseExact(accrualDate, GeneralDB.CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture));              
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Saved successfully";
            }
            catch (Exception ex)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred " + ex.Message;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
    }
}
