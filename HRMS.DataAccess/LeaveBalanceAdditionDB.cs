﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class LeaveBalanceAdditionDB: BaseDB
    {
        public OperationDetails UpdateLeaveBalanceAdditionData(LeaveBalanceAddition leaveBalance)
        {
            OperationDetails operationDetails = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateLeaveBalanceAdditionData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LeaveAdditionId", leaveBalance.LeaveAdditionID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", leaveBalance.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", leaveBalance.VacationTypeID);
                sqlCommand.Parameters.AddWithValue("@FullPaidDays", leaveBalance.FullPaidDays);
                sqlCommand.Parameters.AddWithValue("@HalfPaidDays", leaveBalance.HalfPaidDays);
                sqlCommand.Parameters.AddWithValue("@UnPaidDays", leaveBalance.UnPaidDays);
                sqlCommand.Parameters.AddWithValue("@Comments", leaveBalance.Comments);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", leaveBalance.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", leaveBalance.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@IsAddMode", leaveBalance.IsAddMode);

                //SqlParameter OperationMessage = new SqlParameter("@Output", SqlDbType.Int);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                //SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                //sqlConnection.Close();
                //operationDetails.Success = true;
                //operationDetails.Message = "Record is saved successfully";
                //operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 2000);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                operationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                operationDetails.Success = operationDetails.InsertedRowId > 0 ? true : false;
                operationDetails.Message = OutputMessage.Value.ToString();

            }
            catch 
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred ";
                operationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public LeaveBalanceAddition GetLeaveBalanceAdditionDetails(int? leaveAdditionID)
        {
            LeaveBalanceAddition leavebalanceModel = new LeaveBalanceAddition();          
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeaveBalanceAdditionDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@LeaveAdditionID", leaveAdditionID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        leavebalanceModel.LeaveAdditionID = Convert.ToInt32(sqlDataReader["LeaveAdditionID"]);
                        leavebalanceModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        leavebalanceModel.VacationTypeID = Convert.ToInt16(sqlDataReader["VacationTypeID"]);
                        leavebalanceModel.FullPaidDays = Convert.ToDecimal(sqlDataReader["FullPaidDays"]);
                        leavebalanceModel.HalfPaidDays = Convert.ToDecimal(sqlDataReader["HalfPaidDays"]);
                        leavebalanceModel.UnPaidDays = Convert.ToDecimal(sqlDataReader["UnPaidDays"]);
                        leavebalanceModel.LeaveEffectiveDate = Convert.ToDateTime(sqlDataReader["LeaveEffectiveDate"]);
                        leavebalanceModel.Comments = sqlDataReader["Comments"].ToString();
                        leavebalanceModel.ACYearID = Convert.ToInt16(sqlDataReader["ACYearID"]);
                        leavebalanceModel.CreatedBy = Convert.ToInt32(sqlDataReader["CreatedBy"]);
                        leavebalanceModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"]);
                        leavebalanceModel.ModifiedBy = Convert.ToInt32(sqlDataReader["ModifiedBy"]);
                        leavebalanceModel.ModifiedOn = Convert.ToDateTime(sqlDataReader["ModifiedOn"]);
                        leavebalanceModel.VTCategoryID = sqlDataReader["VTCategoryID"] != null ? Convert.ToInt32(sqlDataReader["VTCategoryID"]) : (int?)null;
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return leavebalanceModel;
        }

        public List<LeaveBalanceAddition> GetLeaveBalanceAdditionList(int? employeeId, int? vacationTypeId)
        {
            List<LeaveBalanceAddition> LeaveBalanceAdditionModelList = null;
            try
            {
                LeaveBalanceAdditionModelList = new List<LeaveBalanceAddition>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeaveBalanceAdditionList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@VacationTypeId", vacationTypeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    LeaveBalanceAddition leavebalanceModel;
                    while (sqlDataReader.Read())
                    {
                        leavebalanceModel = new LeaveBalanceAddition();

                        leavebalanceModel.LeaveAdditionID = Convert.ToInt32(sqlDataReader["LeaveAdditionID"]);
                        leavebalanceModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        leavebalanceModel.VacationTypeID = Convert.ToInt16(sqlDataReader["VacationTypeID"]);
                        leavebalanceModel.FullPaidDays = Convert.ToDecimal(sqlDataReader["FullPaidDays"]);
                        leavebalanceModel.HalfPaidDays = Convert.ToDecimal(sqlDataReader["HalfPaidDays"]);
                        leavebalanceModel.UnPaidDays = Convert.ToDecimal(sqlDataReader["UnPaidDays"]);
                        leavebalanceModel.LeaveEffectiveDate = Convert.ToDateTime(sqlDataReader["LeaveEffectiveDate"]);
                        leavebalanceModel.Comments = sqlDataReader["Comments"].ToString();
                        leavebalanceModel.ACYearID = Convert.ToInt16(sqlDataReader["ACYearID"]);
                        leavebalanceModel.CreatedBy = Convert.ToInt32(sqlDataReader["CreatedBy"]);
                        leavebalanceModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"]);
                        leavebalanceModel.ModifiedBy = Convert.ToInt32(sqlDataReader["ModifiedBy"]);
                        leavebalanceModel.ModifiedOn = Convert.ToDateTime(sqlDataReader["ModifiedOn"]);
                        leavebalanceModel.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        leavebalanceModel.VacationTypeName = sqlDataReader["VacationTypeName"].ToString();

                        LeaveBalanceAdditionModelList.Add(leavebalanceModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return LeaveBalanceAdditionModelList;
        }

        public OperationDetails DeleteLeaveBalance(int leaveAdditionId, int userID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DeleteLeaveBalance", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LeaveAdditionId", leaveAdditionId);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", userID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 2000);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                operationDetails.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                operationDetails.Message = OutputMessage.Value.ToString();
                operationDetails.Success = operationDetails.InsertedRowId > 0 ? true : false;

            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public List<VacationTypeModel> GetAllVacationTypeListData()
        {
            List<VacationTypeModel> vacationTypeList = null;
            try
            {
                vacationTypeList = new List<VacationTypeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllVacationTypeListData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    VacationTypeModel vacatioTypeModel;
                    while (sqlDataReader.Read())
                    {
                        vacatioTypeModel = new VacationTypeModel();

                        vacatioTypeModel.VacationTypeId = Convert.ToInt32(sqlDataReader["VacationTypeId"] == DBNull.Value ? "0" : sqlDataReader["VacationTypeId"].ToString());
                        vacatioTypeModel.Name = Convert.ToString(sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString());
                        vacatioTypeModel.AnnualLeave = Convert.ToInt32(sqlDataReader["AnnualLeave"] == DBNull.Value ? "0" : sqlDataReader["AnnualLeave"].ToString());
                        vacatioTypeModel.PaidType = Convert.ToString(sqlDataReader["PaidType"] == DBNull.Value ? "" : sqlDataReader["PaidType"].ToString());
                        vacatioTypeModel.ApplicableAfterDigits = Convert.ToInt32(sqlDataReader["ApplicableAfterDigits"] == DBNull.Value ? "0" : sqlDataReader["ApplicableAfterDigits"].ToString());
                        vacatioTypeModel.ApplicableAfterTypeID = Convert.ToInt32(sqlDataReader["ApplicableAfterTypeID"] == DBNull.Value ? "0" : sqlDataReader["ApplicableAfterTypeID"].ToString());
                        vacatioTypeModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyID"] == DBNull.Value ? "0" : sqlDataReader["CompanyID"].ToString());
                        vacatioTypeModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"] == DBNull.Value ? "0" : sqlDataReader["GenderID"].ToString());
                        vacatioTypeModel.isCompensate = Convert.ToInt32(sqlDataReader["isCompensate"] == DBNull.Value ? "0" : sqlDataReader["isCompensate"].ToString());
                        vacatioTypeModel.IsDeleted = Convert.ToBoolean(sqlDataReader["IsDeleted"] == DBNull.Value ? "0" : sqlDataReader["IsDeleted"].ToString());
                        vacatioTypeModel.PerServiceVacation = Convert.ToInt32(sqlDataReader["PerServiceVacation"] == DBNull.Value ? "0" : sqlDataReader["PerServiceVacation"].ToString());
                        vacatioTypeModel.MaritalStatusId = Convert.ToInt32(sqlDataReader["MaritalStatusId"] == DBNull.Value ? "0" : sqlDataReader["MaritalStatusId"].ToString());
                        vacatioTypeModel.MonthlySplit = Convert.ToInt32(sqlDataReader["MonthlySplit"] == DBNull.Value ? "0" : sqlDataReader["MonthlySplit"].ToString());
                        vacatioTypeModel.NationalityID = Convert.ToInt32(sqlDataReader["NationalityID"] == DBNull.Value ? "0" : sqlDataReader["NationalityID"].ToString());
                        vacatioTypeModel.TotalNoOfDays = Convert.ToInt32(sqlDataReader["NoOfDays"] == DBNull.Value ? "0" : sqlDataReader["NoOfDays"].ToString());
                        vacatioTypeModel.ReligionID = Convert.ToInt32(sqlDataReader["ReligionID"] == DBNull.Value ? "0" : sqlDataReader["ReligionID"].ToString());
                        vacatioTypeModel.RepeatedFor = Convert.ToInt32(sqlDataReader["RepeatedFor"] == DBNull.Value ? "0" : sqlDataReader["RepeatedFor"].ToString());
                        vacatioTypeModel.VTCategoryID = Convert.ToInt32(sqlDataReader["VTCategoryID"] == DBNull.Value ? "0" : sqlDataReader["VTCategoryID"].ToString());
                        vacatioTypeModel.FullPaidDays = Convert.ToInt32(sqlDataReader["FullPaidDays"] == DBNull.Value ? "0" : sqlDataReader["FullPaidDays"].ToString());
                        vacatioTypeModel.HalfPaidDays = Convert.ToInt32(sqlDataReader["HalfPaidDays"] == DBNull.Value ? "0" : sqlDataReader["HalfPaidDays"].ToString());
                        vacatioTypeModel.UnPaidDays = Convert.ToInt32(sqlDataReader["UnPaidDays"] == DBNull.Value ? "0" : sqlDataReader["UnPaidDays"].ToString());
                        vacatioTypeModel.LeaveEntitleTypeID = Convert.ToInt32(sqlDataReader["LeaveEntitleTypeID"] == DBNull.Value ? "0" : sqlDataReader["LeaveEntitleTypeID"].ToString());

                        vacationTypeList.Add(vacatioTypeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return vacationTypeList;
        }
    }
}
