﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class AllowanceDB : DBHelper
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        //public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        //AccommodationInfo

        public OperationDetails InsertAccommodationInfo(AllowanceModel AllowanceModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",1) ,
                        new SqlParameter("@aIntHRAccommodationTypeID", AllowanceModel.HRAccommodationTypeID) ,
                        new SqlParameter("@CountryID", AllowanceModel.CountryID) ,
                        new SqlParameter("@CityID", AllowanceModel.CityID) ,
                        new SqlParameter("@Street", AllowanceModel.Street) ,
                        new SqlParameter("@Building",AllowanceModel.Building ) ,
                        new SqlParameter("@BuildingFloor",AllowanceModel.BuildingFloor ) ,
                        new SqlParameter("@Appartment", AllowanceModel.Appartment ),
                        new SqlParameter("@EmployeeID",AllowanceModel.EmployeeID),
                        new SqlParameter("@Housing",AllowanceModel.Housing)
                    };
                int PayAccommodationID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayAccommodationInfoCUD  ", parameters));
                return new OperationDetails(true, "Accommodation information save successfully", null, PayAccommodationID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }
        
        public OperationDetails UpdateAccommodationInfo(AllowanceModel AllowanceModel)
        {

            AllowanceModel.HRAccommodationTypeID = 1;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",2) ,
                        new SqlParameter("@aIntPayAccommodationID", AllowanceModel.PayAccommodationID) ,
                        new SqlParameter("@aIntHRAccommodationTypeID", AllowanceModel.HRAccommodationTypeID) ,
                        new SqlParameter("@CountryID", AllowanceModel.CountryID) ,
                        new SqlParameter("@CityID", AllowanceModel.CityID) ,
                        new SqlParameter("@Street", AllowanceModel.Street) ,
                        new SqlParameter("@Building",AllowanceModel.Building ) ,
                        new SqlParameter("@BuildingFloor",AllowanceModel.BuildingFloor ) ,
                        new SqlParameter("@Appartment", AllowanceModel.Appartment ),
                        new SqlParameter("@Housing",AllowanceModel.Housing)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspPayAccommodationInfoCUD", parameters);
                return new OperationDetails(true, "Accommodation information update successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }
        
        public AllowanceModel AccommodationInfoByEmployeeId(int EmployeeId)
        {
            try
            {
                AllowanceModel AllowanceModel = new AllowanceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayAccommodation", new SqlParameter("@aSntTeacherID", EmployeeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllowanceModel.PayAccommodationID = Convert.ToInt32(reader["PayAccommodationID"].ToString());
                            AllowanceModel.HRAccommodationTypeID = Convert.ToInt32(reader["HRAccommodationTypeID"].ToString());
                            AllowanceModel.CityID = Convert.ToInt32(reader["CityID"].ToString());
                            AllowanceModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                            AllowanceModel.Street = reader["Street"].ToString();
                            AllowanceModel.Building = reader["Building"].ToString();
                            AllowanceModel.BuildingFloor = reader["BuildingFloor"].ToString();
                            AllowanceModel.Appartment = reader["Appartment"].ToString();
                            AllowanceModel.Housing = Convert.ToBoolean(reader["Housing"].ToString());
                        }
                    }
                }
                return AllowanceModel;
                // Finally, we return AbsentModel
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {
            }
        }

        public List<PickList> GetAccommodationtype()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetAccommodationtype"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            objPickList.Add(new PickList() { id = Convert.ToInt32(reader["HRAccommodationTypeID"].ToString()), text = Convert.ToString(reader["HRAccommodationTypeName_1"]) });
                        }
                    }
                }
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
            return objPickList;
        }

        //AccommodationHistory

        public OperationDetails InsertAccommodationHistory(AllowanceModel AllowanceModel)
        {
            try
            {

                if (AllowanceModel.PayEmployeeAccommodationHistoryID > 0)
                {

                    AllowanceModel.aTntTranMode = 2;
                }
                else
                {

                    AllowanceModel.aTntTranMode = 1;
                }



                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",AllowanceModel.aTntTranMode) ,
                        new SqlParameter("@PayEmployeeAccommodationHistoryID", AllowanceModel.PayEmployeeAccommodationHistoryID) ,

                      new SqlParameter("@PayEmployeeAccommodationHistoryYear", AllowanceModel.PayEmployeeAccommodationHistoryYearID) ,
                      new SqlParameter("@amount", AllowanceModel.AccommodationHistoryAmount) ,
                       new SqlParameter("@furnished", AllowanceModel.Furnished) ,
                      new SqlParameter("@FurnishedAmount",AllowanceModel.FurnishedAmount ) ,
                      new SqlParameter("@electricity",AllowanceModel.Electricity ) ,
                      new SqlParameter("@ElectricityAmount", AllowanceModel.ElectricityAmount ),
                      new SqlParameter("@water",AllowanceModel.Water ) ,
                      new SqlParameter("@WaterAmount", AllowanceModel.WaterAmount ),
                      new SqlParameter("@EmployeeID",AllowanceModel.EmployeeID)






                    };
                int PayAccommodationID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayAccommodationHistoryCUD  ", parameters));
                return new OperationDetails(true, "Accommodation history save successfully", null, PayAccommodationID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }
        
        public OperationDetails UpdateAccommodationHistory(AllowanceModel AllowanceModel)
        {


            AllowanceModel.HRAccommodationTypeID = 1;
            try
            {
                SqlParameter[] parameters =
                    {
                       new SqlParameter("@aTntTranMode",2) ,
                       new SqlParameter("@PayEmployeeAccommodationHistoryID", AllowanceModel.PayEmployeeAccommodationHistoryID) ,
                       new SqlParameter("@PayEmployeeAccommodationHistoryYear", AllowanceModel.PayEmployeeAccommodationHistoryYearID) ,
                      new SqlParameter("@amount", AllowanceModel.AccommodationHistoryAmount) ,
                       new SqlParameter("@furnished", AllowanceModel.Furnished) ,
                      new SqlParameter("@FurnishedAmount",AllowanceModel.FurnishedAmount ) ,
                      new SqlParameter("@electricity",AllowanceModel.Electricity ) ,
                      new SqlParameter("@ElectricityAmount", AllowanceModel.ElectricityAmount ),
                      new SqlParameter("@water",AllowanceModel.Water ) ,
                      new SqlParameter("@WaterAmount", AllowanceModel.WaterAmount ),

                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspPayAccommodationHistoryCUD ", parameters);
                return new OperationDetails(true, "Accommodation history save successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }
        
        public List<AllowanceModel> AccommodationHistoryByEmployeeId(int EmployeeId)
        {
            try
            {
                List<AllowanceModel> objAllowanceModelList = new List<AllowanceModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayAccommodationHistory", new SqlParameter("@aSntTeacherID", EmployeeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            AllowanceModel AllowanceModel = new AllowanceModel();

                            AllowanceModel.PayEmployeeAccommodationHistoryID = Convert.ToInt32(reader["PayEmployeeAccommodationHistoryID"].ToString());
                            AllowanceModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            AllowanceModel.PayEmployeeAccommodationHistoryYear = reader["PayEmployeeAccommodationHistoryYear"].ToString();

                            AllowanceModel.AccommodationHistoryAmount = Convert.ToDouble(reader["amount"].ToString());
                            AllowanceModel.Furnished = Convert.ToBoolean(reader["furnished"].ToString());
                            AllowanceModel.FurnishedAmount = Convert.ToDouble(reader["FurnishedAmount"].ToString());
                            AllowanceModel.Electricity = Convert.ToBoolean(reader["electricity"].ToString());
                            AllowanceModel.ElectricityAmount = Convert.ToDouble(reader["ElectricityAmount"].ToString());
                            AllowanceModel.Water = Convert.ToBoolean(reader["water"].ToString());
                            AllowanceModel.WaterAmount = Convert.ToDouble(reader["WaterAmount"].ToString());

                            objAllowanceModelList.Add(AllowanceModel);

                        }
                    }
                }
                return objAllowanceModelList;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public AllowanceModel AccommodationHistoryByHistoryID(int PayEmployeeAccommodationHistoryID)
        {
            try
            {
                AllowanceModel AllowanceModel = new AllowanceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayAccommodationHistoryBYID", new SqlParameter("@PayEmployeeAccommodationHistoryID", PayEmployeeAccommodationHistoryID)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {



                            AllowanceModel.PayEmployeeAccommodationHistoryID = Convert.ToInt32(reader["PayEmployeeAccommodationHistoryID"].ToString());
                            AllowanceModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            AllowanceModel.PayEmployeeAccommodationHistoryYearID = reader["PayEmployeeAccommodationHistoryYear"].ToString();

                            AllowanceModel.AccommodationHistoryAmount = Convert.ToDouble(reader["amount"].ToString());
                            AllowanceModel.Furnished = Convert.ToBoolean(reader["furnished"].ToString());
                            AllowanceModel.FurnishedAmount = Convert.ToDouble(reader["FurnishedAmount"].ToString());
                            AllowanceModel.Electricity = Convert.ToBoolean(reader["electricity"].ToString());
                            AllowanceModel.ElectricityAmount = Convert.ToDouble(reader["ElectricityAmount"].ToString());
                            AllowanceModel.Water = Convert.ToBoolean(reader["water"].ToString());
                            AllowanceModel.WaterAmount = Convert.ToDouble(reader["WaterAmount"].ToString());


                        }
                    }
                }
                return AllowanceModel;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteAccommodationHistory(int id)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@PayEmployeeAccommodationHistoryID",id) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspPayAccommodationHistoryCUD", parameters);
                return new OperationDetails(true, "Accommodation History deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Accommodation History.", exception);
                throw;
            }
            finally
            {

            }
        }
         
        //transportation
        public OperationDetails AddTransportation(AllowanceModel AllowanceModel)
        {
            try
            {

                if (AllowanceModel.transportationModel.PayTransportationID > 0)
                {

                    AllowanceModel.aTntTranMode = 2;
                }
                else
                {

                    AllowanceModel.aTntTranMode = 1;
                }



                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",AllowanceModel.aTntTranMode) ,
                        new SqlParameter("@PayTransportationID", AllowanceModel.transportationModel.PayTransportationID) ,
                      new SqlParameter("@PayTransportationType", AllowanceModel.transportationModel.CarTypeID) ,
                      new SqlParameter("@PayTransportationValue", AllowanceModel.transportationModel.PayTransportationValue) ,
                       new SqlParameter("@ModelName", AllowanceModel.transportationModel.ModelName) ,
                      new SqlParameter("@ReceivedDate",CommonDB.SetCulturedDate(AllowanceModel.transportationModel.ReceivedDate)) ,
                      new SqlParameter("@Ecurrence",AllowanceModel.transportationModel.Ecurrence ) ,
                      new SqlParameter("@EccurenceYear", AllowanceModel.transportationModel.EccurenceYear ),
                      new SqlParameter("@Maintenance",AllowanceModel.transportationModel.Maintenance ) ,
                      new SqlParameter("@MaintenanceYear", AllowanceModel.transportationModel.MaintenanceYear ),
                      new SqlParameter("@EmployeeID",AllowanceModel.EmployeeID),
                      new SqlParameter("@UpdateDate",CommonDB.SetCulturedDate(AllowanceModel.transportationModel.UpdateDate)),
                      new SqlParameter("@IsShuttle",AllowanceModel.transportationModel.IsShuttle),
                      new SqlParameter("@BySchoolCar",AllowanceModel.transportationModel.BySchoolCar),
                      new SqlParameter("@HasOwnCar",AllowanceModel.transportationModel.HasOwnCar),
                    };
                int PayTransportationID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayTransportationCUD  ", parameters));
                return new OperationDetails(true, "Transportation information saved successfully", null, PayTransportationID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddAirfareDetails(AllowanceModel AllowanceModel)
        {
            try
            {

                if (AllowanceModel.EmpAirFare.EmployeeAirfareID > 0)
                {

                    AllowanceModel.aTntTranMode = 2;
                }
                else
                {

                    AllowanceModel.aTntTranMode = 1;
                }



                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",AllowanceModel.aTntTranMode) ,
                        new SqlParameter("@AirfareID", AllowanceModel.EmpAirFare.EmployeeAirfareID) ,
                      new SqlParameter("@AirfareCountryId", AllowanceModel.EmpAirFare.AirTicketCountryID) ,
                      new SqlParameter("@AirTicketCityId", AllowanceModel.EmpAirFare.AirTicketCityID) ,
                       new SqlParameter("@AirTicketClassId", AllowanceModel.EmpAirFare.AirTicketClassID) ,
                      new SqlParameter("@AirTicketAmount",AllowanceModel.EmpAirFare.AirTicketAmount) ,
                      new SqlParameter("@NoOfDependants",AllowanceModel.EmpAirFare.NoOfDependencies) ,
                      new SqlParameter("@AirTicketDependantClassId", AllowanceModel.EmpAirFare.AirTicketClassIDForDependant),
                      new SqlParameter("@AirTicketDependantAmount",AllowanceModel.EmpAirFare.AirTicketAmountDependannt),
                      new SqlParameter("@AnnualAirTicket",AllowanceModel.EmpAirFare.AnnualAirTicket),
                       new SqlParameter("@EmployeeId",AllowanceModel.EmployeeID),
                       new SqlParameter("@AirfareFrequencyID",AllowanceModel.EmpAirFare.AirfareFrequencyID),
                       new SqlParameter("@AirportListID",AllowanceModel.EmpAirFare.AirportListID)

                    };
                int PayTransportationID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_Stp_EmployeeAirFareCUD", parameters));
                return new OperationDetails(true, "Airfare information saved successfully", null, PayTransportationID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public AllowanceModel TransportationByEmployeeID(int id)
        {
            try
            {
                AllowanceModel AllowanceModel = new AllowanceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayTransportation", new SqlParameter("@aSntEmployeeID", id)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllowanceModel.transportationModel.PayTransportationID = Convert.ToInt32(reader["PayTransportationID"].ToString());
                            AllowanceModel.transportationModel.CarTypeID = Convert.ToInt32(reader["PayTransportationType"].ToString());
                            AllowanceModel.transportationModel.PayTransportationValue = Convert.ToDouble(reader["PayTransportationValue"].ToString());
                            AllowanceModel.transportationModel.ModelName = reader["ModelName"].ToString();
                            AllowanceModel.transportationModel.ReceivedDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["ReceivedDate"].ToString());
                            // AllowanceModel.transportationModel.ReceivedDate =  reader["ReceivedDate"].ToString();
                            AllowanceModel.transportationModel.Ecurrence = Convert.ToDouble(reader["Ecurrence"].ToString());
                            AllowanceModel.transportationModel.EccurenceYear = Convert.ToInt32(reader["EccurenceYear"].ToString());
                            AllowanceModel.transportationModel.Maintenance = Convert.ToDouble(reader["Maintenance"].ToString());
                            AllowanceModel.transportationModel.MaintenanceYear = Convert.ToInt32(reader["MaintenanceYear"].ToString());
                            AllowanceModel.transportationModel.IsShuttle = Convert.ToBoolean(reader["IsShuttle"].ToString());
                            AllowanceModel.transportationModel.BySchoolCar = Convert.ToBoolean(reader["BySchoolCar"].ToString());
                            AllowanceModel.transportationModel.HasOwnCar = Convert.ToBoolean(reader["HasOwnCar"].ToString());
                        }
                    }
                }
                return AllowanceModel;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public AllowanceModel GetEmployeeAirFarebyEmployeeId(int id)
        {
            try
            {
                AllowanceModel Allowance = new AllowanceModel();
                EmployeeAirFare EmpAirFare = new EmployeeAirFare();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetEmployeeAirfareDetails", new SqlParameter("@aSntEmployeeID", id)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        
                        while (reader.Read())
                        {
                            EmpAirFare.EmployeeAirfareID = Convert.ToInt32(reader["AirfareId"].ToString());
                            EmpAirFare.AirTicketCountryID = Convert.ToInt32(reader["AirTicketCountryId"].ToString());
                            EmpAirFare.AirTicketCityID = Convert.ToInt32(reader["AiTicketCityId"].ToString());
                            EmpAirFare.AirTicketClassID = Convert.ToInt32(reader["AirTicketClassId"].ToString());
                            EmpAirFare.AirTicketAmount = Convert.ToDecimal(reader["AirTicketAmount"].ToString());
                            EmpAirFare.NoOfDependencies = Convert.ToInt32(reader["NoOfDependants"].ToString());
                            EmpAirFare.AirTicketClassIDForDependant = Convert.ToInt32(reader["AirTicketDependantClassId"].ToString()==""?"0": reader["AirTicketDependantClassId"].ToString());
                            EmpAirFare.AirTicketAmountDependannt = Convert.ToDecimal(reader["AirTicketDependantAmount"].ToString());
                            EmpAirFare.AirfareFrequencyID = Convert.ToInt32(reader["AirfareFrequencyID"].ToString() == "" ? "0" : reader["AirfareFrequencyID"].ToString());
                            EmpAirFare.AirportListID = Convert.ToInt32(reader["AirportListID"].ToString() == "" ? "0" : reader["AirportListID"].ToString());
                        }
                    }
                    else
                    {
                        EmpAirFare = new EmployeeAirFare();
                    }
                }
                Allowance.EmpAirFare = EmpAirFare;
                return Allowance;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AllowanceModel.TransportationModel> GetCarTypeList()
        {
            try
            {
                List<AllowanceModel.TransportationModel> AllowanceModelList = new List<AllowanceModel.TransportationModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_CarTypeList"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllowanceModel.TransportationModel AllowanceModel = new AllowanceModel.TransportationModel();
                            AllowanceModel.CarTypeID = Convert.ToInt32(reader["CarTypeID"].ToString());
                            AllowanceModel.CarTypeName = reader["CarTypeName_1"].ToString();
                            AllowanceModelList.Add(AllowanceModel);
                        }
                    }
                }
                return AllowanceModelList;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }
        
        public OperationDetails InsertGasCardInfo(AllowanceModel AllowanceModel)
        {
            try
            {
                //string Leavetype = Convert.ToString(AbsentModel.LeaveTypeID);
                //if (Leavetype == 0)
                //{

                //    Leavetype = null;
                //}


                if (AllowanceModel.gasCardModel.PayGasCardID > 0)
                {

                    AllowanceModel.aTntTranMode = 2;
                }
                else
                {

                    AllowanceModel.aTntTranMode = 1;
                }

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",AllowanceModel.aTntTranMode) ,

                      new SqlParameter("@PayGasCardID", AllowanceModel.gasCardModel.PayGasCardID) ,

                      new SqlParameter("@PayGasCardValue", AllowanceModel.gasCardModel.PayGasCardValue) ,
                      new SqlParameter("@Company", AllowanceModel.gasCardModel.Company  ) ,
                       new SqlParameter("@ReceivedDate",CommonDB.SetCulturedDate(AllowanceModel.gasCardModel.GasCardReceivedDate)),
                    //  new SqlParameter("@aSntPayAbsentTypeID", AllowanceModel.PayAbsentTypeID),
                      new SqlParameter("@EmployeeID",AllowanceModel.EmployeeID ) ,
                      new SqlParameter("@PayGasCardNumber",AllowanceModel.gasCardModel.PayGasCardNumber ) ,
                      new SqlParameter("@PayGasCardPeriod", AllowanceModel.gasCardModel.PayGasCardPeriod )




                    };
                int PayAccommodationID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayGasCardCUD", parameters));
                return new OperationDetails(true, "Gas card information save successfully", null, PayAccommodationID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public List<AllowanceModel.GasCardModel> GasCardInfoEmployeeId(int EmployeeId)
        {
            try
            {
                List<AllowanceModel.GasCardModel> objGasCardModelList = new List<AllowanceModel.GasCardModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetGasCardList", new SqlParameter("@aSntTeacherID", EmployeeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            AllowanceModel AllowanceModel = new AllowanceModel();

                            AllowanceModel.gasCardModel.PayGasCardID = Convert.ToInt32(reader["PayGasCardID"].ToString());
                            AllowanceModel.gasCardModel.PayGasCardValue = Convert.ToDouble(reader["PayGasCardValue"].ToString());
                            AllowanceModel.gasCardModel.Company = reader["Company"].ToString();
                            // AllowanceModel.transportationModel.ReceivedDate = Convert.ToDateTime(reader["ReceivedDate"].ToString()).ToShortDateString();
                            AllowanceModel.gasCardModel.GasCardReceivedDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["ReceivedDate"].ToString());
                            AllowanceModel.gasCardModel.PayGasCardNumber = reader["PayGasCardNumber"].ToString();
                            AllowanceModel.gasCardModel.PayGasCardPeriod = Convert.ToInt32(reader["PayGasCardPeriod"].ToString());


                            if (AllowanceModel.gasCardModel.PayGasCardPeriod == 0)
                            {
                                AllowanceModel.gasCardModel.PayGasCardPeriodName = "N/A";

                            }
                            else if (AllowanceModel.gasCardModel.PayGasCardPeriod == 1)
                            {
                                AllowanceModel.gasCardModel.PayGasCardPeriodName = "Monthly";
                            }
                            else
                            {
                                AllowanceModel.gasCardModel.PayGasCardPeriodName = "Yearly";
                            }






                            objGasCardModelList.Add(AllowanceModel.gasCardModel);

                        }
                    }
                }
                return objGasCardModelList;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }
        
        public AllowanceModel GasCardInfoByID(int id)
        {
            try
            {
                AllowanceModel AllowanceModel = new AllowanceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetGasCardByID", new SqlParameter("@PayGasCardID", id)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {



                            AllowanceModel.gasCardModel.PayGasCardID = Convert.ToInt32(reader["PayGasCardID"].ToString());
                            AllowanceModel.gasCardModel.PayGasCardValue = Convert.ToDouble(reader["PayGasCardValue"].ToString());
                            AllowanceModel.gasCardModel.Company = reader["Company"].ToString();
                            AllowanceModel.gasCardModel.GasCardReceivedDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["ReceivedDate"].ToString());
                            AllowanceModel.gasCardModel.PayGasCardNumber = reader["PayGasCardNumber"].ToString();
                            AllowanceModel.gasCardModel.PayGasCardPeriod = Convert.ToInt32(reader["PayGasCardPeriod"].ToString());



                        }
                    }
                }
                return AllowanceModel;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }
        
        public OperationDetails DeleteGasCard(int id)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@PayGasCardID",id) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspPayGasCardCUD", parameters);
                return new OperationDetails(true, "Gas Card deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Gas Card.", exception);
                throw;
            }
            finally
            {

            }
        }
                
        public List<AllowanceModel> GetAcademicYearList()
        {

            // Create a list to hold the Absent		
            List<AllowanceModel> AllowanceModelList = new List<AllowanceModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_AcademicYearList"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        AllowanceModel AllowanceModel;
                        while (reader.Read())
                        {
                            AllowanceModel = new AllowanceModel();
                            AllowanceModel.PayEmployeeAccommodationHistoryYearID = reader["AcademicYearId"].ToString();
                            AllowanceModel.PayEmployeeAccommodationHistoryYear = reader["Duration"].ToString();


                            AllowanceModelList.Add(AllowanceModel);
                        }
                    }
                }
                return AllowanceModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }
        
    }
}
