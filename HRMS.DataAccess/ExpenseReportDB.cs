﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class ExpenseReportDB : DBHelper
    {
        public OperationDetails InsertExpenseReport(ExpenseReportModel expenseReportModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@ReportTitle",expenseReportModel.ReportTitle) ,
                      new SqlParameter("@ReportDescription", expenseReportModel.ReportDescription) ,
                      new SqlParameter("@ExtNo", expenseReportModel.ExtNo) ,
                      new SqlParameter("@ApproverId", expenseReportModel.ApproverId),
                      new SqlParameter("@ProjectId", expenseReportModel.ProjectId)  ,
                      new SqlParameter("@Attachments",expenseReportModel.Attachments) ,
                      new SqlParameter("@Notes", expenseReportModel.Notes),
                      new SqlParameter("@CreatedOn",expenseReportModel.CreatedOn) ,
                      new SqlParameter("@CreatedBy", expenseReportModel.CreatedBy)
                    };
                int ExpenseReportId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_Hr_ExpenseReport", parameters));
                return new OperationDetails(true, "Expense Report saved successfully.", null, ExpenseReportId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Expense Report.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertExpenseReportDetails(ExpenseReportDetails expenseReportDetails)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@ExpenseReportId",expenseReportDetails.ExpenseReportId) ,
                      new SqlParameter("@Category", expenseReportDetails.Category) ,
                      new SqlParameter("@Description", expenseReportDetails.Description) ,
                      new SqlParameter("@Units", expenseReportDetails.Units),
                      new SqlParameter("@CostPerUnit", expenseReportDetails.CostPerUnit)  ,
                      new SqlParameter("@Tax",expenseReportDetails.Tax) ,
                      new SqlParameter("@Receipts", expenseReportDetails.Receipts),
                      new SqlParameter("@Total",expenseReportDetails.Total) ,
                      new SqlParameter("@BillTo", expenseReportDetails.BillTo),
                      new SqlParameter("@CreatedOn",expenseReportDetails.CreatedOn) ,
                      new SqlParameter("@CreatedBy", expenseReportDetails.CreatedBy)
                    };
                int ExpenseReportDetailId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_ExpenseReportDetails", parameters));
                return new OperationDetails(true, "Expense Report Details saved successfully.", null, ExpenseReportDetailId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Expense Report Details.", exception);
                //throw;
            }
            finally
            {

            }
        }
    }
}
