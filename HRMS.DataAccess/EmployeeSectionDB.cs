﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class EmployeeSectionDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        public List<EmployeeSectionModel> GetEmployeeSectionList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, out int totalCount)
        {
            List<EmployeeSectionModel> employeeSectionList = null;
            try
            {
                employeeSectionList = new List<EmployeeSectionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_Gen_EmployeeSection", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);


                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeSectionModel employeeSectionModel;
                    while (sqlDataReader.Read())
                    {
                        employeeSectionModel = new EmployeeSectionModel();
                        employeeSectionModel.EmployeeSectionID = Convert.ToInt32(sqlDataReader["EmployeeSectionID"].ToString());
                        employeeSectionModel.EmployeeSectionName_1 = Convert.ToString(sqlDataReader["EmployeeSectionName_1"]);
                        employeeSectionModel.EmployeeSectionName_2 = Convert.ToString(sqlDataReader["EmployeeSectionName_2"]);
                        employeeSectionModel.EmployeeSectionName_3 = Convert.ToString(sqlDataReader["EmployeeSectionName_3"]);
                        employeeSectionModel.Head = Convert.ToInt32(sqlDataReader["Head"]);
                        employeeSectionList.Add(employeeSectionModel);
                    }
                }
                sqlConnection.Close();

                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeSectionList;
        }

        public List<EmployeeSectionModel> GetEmployeeSection(int UserId)
        {
            List<EmployeeSectionModel> employeeSectionList = null;
            try
            {
                employeeSectionList = new List<EmployeeSectionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_Gen_EmployeeSectionList", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeSectionModel employeeSectionModel;
                    while (sqlDataReader.Read())
                    {
                        employeeSectionModel = new EmployeeSectionModel();

                        employeeSectionModel.EmployeeSectionID = Convert.ToInt32(sqlDataReader["EmployeeSectionID"].ToString());
                        employeeSectionModel.EmployeeSectionName_1 = Convert.ToString(sqlDataReader["EmployeeSectionName_1"]);
                        employeeSectionModel.EmployeeSectionName_2 = Convert.ToString(sqlDataReader["EmployeeSectionName_2"]);
                        employeeSectionModel.EmployeeSectionName_3 = Convert.ToString(sqlDataReader["EmployeeSectionName_3"]);
                        employeeSectionModel.DepartmentID = Convert.ToInt32(string.IsNullOrWhiteSpace(sqlDataReader["DepartmentID"].ToString()) ? 0 : sqlDataReader["DepartmentID"]);
                        //employeeSectionModel.Head = Convert.ToInt32(sqlDataReader["Head"] == null  ? 0 : sqlDataReader["Head"]);
                        employeeSectionModel.Head = Convert.ToInt32(string.IsNullOrWhiteSpace(sqlDataReader["Head"].ToString()) ? 0 : sqlDataReader["Head"]);
                        employeeSectionList.Add(employeeSectionModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeSectionList.OrderBy(x => x.EmployeeSectionName_1).ToList();
        }

        public List<EmployeeSectionModel> GetEmployeeSectionByDepartmentID(int DepartmentId)
        {
            List<EmployeeSectionModel> employeeSectionList = null;
            try
            {
                employeeSectionList = new List<EmployeeSectionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_GetEmployeeSectionListByDepartmentID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentId", DepartmentId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeSectionModel employeeSectionModel;
                    while (sqlDataReader.Read())
                    {
                        employeeSectionModel = new EmployeeSectionModel();

                        employeeSectionModel.EmployeeSectionID = Convert.ToInt32(sqlDataReader["EmployeeSectionID"].ToString());
                        employeeSectionModel.EmployeeSectionName_1 = Convert.ToString(sqlDataReader["EmployeeSectionName_1"]);
                        employeeSectionModel.EmployeeSectionName_2 = Convert.ToString(sqlDataReader["EmployeeSectionName_2"]);
                        employeeSectionModel.EmployeeSectionName_3 = Convert.ToString(sqlDataReader["EmployeeSectionName_3"]);
                        employeeSectionModel.DepartmentID = Convert.ToInt32(string.IsNullOrWhiteSpace(sqlDataReader["DepartmentID"].ToString()) ? 0 : sqlDataReader["DepartmentID"]);
                        //employeeSectionModel.Head = Convert.ToInt32(sqlDataReader["Head"] == null  ? 0 : sqlDataReader["Head"]);
                        employeeSectionModel.Head = Convert.ToInt32(string.IsNullOrWhiteSpace(sqlDataReader["Head"].ToString()) ? 0 : sqlDataReader["Head"]);
                        employeeSectionList.Add(employeeSectionModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeSectionList.OrderBy(x => x.EmployeeSectionName_1).ToList();
        }
          

        public OperationDetails EmployeeSectionCrud(EmployeeSectionModel employeeSection, int operationId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("GEN_uspEmployeeSectionCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationId);
                sqlCommand.Parameters.AddWithValue("@aTntEmployeeSectionID", employeeSection.EmployeeSectionID);
                sqlCommand.Parameters.AddWithValue("@aNvrEmployeeSectionName_1", employeeSection.EmployeeSectionName_1);
                sqlCommand.Parameters.AddWithValue("@aNvrEmployeeSectionName_2", employeeSection.EmployeeSectionName_2);
                sqlCommand.Parameters.AddWithValue("@aNvrEmployeeSectionName_3", employeeSection.EmployeeSectionName_3);
                sqlCommand.Parameters.AddWithValue("@aIntHead", employeeSection.Head);
                sqlCommand.Parameters.AddWithValue("@aIntAssistant_1", employeeSection.Assistant_1);
                sqlCommand.Parameters.AddWithValue("@aIntAssistant_2", employeeSection.Assistant_2);
                sqlCommand.Parameters.AddWithValue("@aIntAssistant_3", employeeSection.Assistant_3);
                sqlCommand.Parameters.AddWithValue("@aIntshiftid", employeeSection.shiftid);
                sqlCommand.Parameters.AddWithValue("@aIntDepartmentID", employeeSection.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@aIntVacationDaysID", employeeSection.VacationDaysID);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                id = Convert.ToInt32(output.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }


        public EmployeeSectionModel GetEmployeeSectionById(int id)
        {
            EmployeeSectionModel employeeSectionModel = null;

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetEmployeeSectionById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@employeeSectionID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        employeeSectionModel = new EmployeeSectionModel();
                        employeeSectionModel.EmployeeSectionID = Convert.ToInt32(sqlDataReader["EmployeeSectionID"].ToString());
                        employeeSectionModel.EmployeeSectionName_1 = Convert.ToString(sqlDataReader["EmployeeSectionName_1"].ToString());
                        employeeSectionModel.EmployeeSectionName_2 = Convert.ToString(sqlDataReader["EmployeeSectionName_2"]);
                        employeeSectionModel.EmployeeSectionName_3 = Convert.ToString(sqlDataReader["EmployeeSectionName_3"]);
                        employeeSectionModel.Head = sqlDataReader["Head"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Head"]);
                        employeeSectionModel.HeadName = sqlDataReader["HeadName"].ToString();
                        employeeSectionModel.Assistant_1 = sqlDataReader["Assistant_1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant_1"]);
                        employeeSectionModel.AssistantName_1 = sqlDataReader["AssistantName_1"].ToString();
                        employeeSectionModel.Assistant_2 = sqlDataReader["Assistant_2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant_2"]);
                        employeeSectionModel.AssistantName_2 = sqlDataReader["AssistantName_2"].ToString();
                        employeeSectionModel.Assistant_3 = sqlDataReader["Assistant_3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant_3"]);
                        employeeSectionModel.AssistantName_3 = sqlDataReader["AssistantName_3"].ToString();
                        employeeSectionModel.shiftid = sqlDataReader["shiftid"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["shiftid"]);
                        employeeSectionModel.ShiftName = sqlDataReader["ShiftName"].ToString();
                        employeeSectionModel.DepartmentID = sqlDataReader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DepartmentID"]);
                        employeeSectionModel.DepartmentName = sqlDataReader["Department"].ToString();
                        employeeSectionModel.VacationDaysID = sqlDataReader["VacationDaysID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["VacationDaysID"]);
                        employeeSectionModel.VacationName = sqlDataReader["VacationName"].ToString();
                    }

                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return employeeSectionModel;
        }

        public List<EmployeeSectionModel> GetEmployeeSectionListPaging()
        {
            List<EmployeeSectionModel> EmployeeSectionList = new List<EmployeeSectionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetEmployeeSectionsWithPaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader reader = sqlCommand.ExecuteReader();

                // Check if the reader returned any rows
                if (reader.HasRows)
                {
                    // While the reader has rows we loop through them,
                    // create new PassportModel, and insert them into our list
                    EmployeeSectionModel EmployeeSection;
                    while (reader.Read())
                    {
                        EmployeeSection = new EmployeeSectionModel();
                        EmployeeSection.EmployeeSectionID = Convert.ToInt32(reader["EmployeeSectionID"].ToString());
                        EmployeeSection.EmployeeSectionName_1 = reader["EmployeeSectionName_1"].ToString().Trim();
                        EmployeeSection.EmployeeSectionName_2 = reader["EmployeeSectionName_2"].ToString().Trim();
                        EmployeeSection.EmployeeSectionName_3 = reader["EmployeeSectionName_3"].ToString().Trim();
                        EmployeeSection.HeadName = reader["Head"].ToString().Trim();
                        EmployeeSection.AssistantName_1 = reader["Assistant_1"].ToString().Trim();
                        EmployeeSection.AssistantName_2 = reader["Assistant_2"].ToString().Trim();
                        EmployeeSection.AssistantName_3 = reader["Assistant_3"].ToString().Trim();
                        EmployeeSection.ShiftName = reader["ShiftName"].ToString().Trim();
                        EmployeeSection.DepartmentName = reader["Department"].ToString().Trim();
                        EmployeeSectionList.Add(EmployeeSection);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModelList.", exception);
                throw;
            }
            finally
            {

            }
            return EmployeeSectionList;
        }

        public OperationDetails AddEditEmployeeSection(EmployeeSectionModel EmpSection, int trnMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateEmployeeSection", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", trnMode);
                sqlCommand.Parameters.AddWithValue("@EmployeeSectionId", EmpSection.EmployeeSectionID);
                sqlCommand.Parameters.AddWithValue("@EmployeeSectionName_1", EmpSection.EmployeeSectionName_1);
                sqlCommand.Parameters.AddWithValue("@EmployeeSectionName_2", EmpSection.EmployeeSectionName_2);
                sqlCommand.Parameters.AddWithValue("@EmployeeSectionName_3", EmpSection.EmployeeSectionName_3);
                sqlCommand.Parameters.AddWithValue("@Head", EmpSection.Head);
                sqlCommand.Parameters.AddWithValue("@Assistant_1", EmpSection.Assistant_1);
                sqlCommand.Parameters.AddWithValue("@Assistant_2", EmpSection.Assistant_2);
                sqlCommand.Parameters.AddWithValue("@Assistant_3", EmpSection.Assistant_3);
                sqlCommand.Parameters.AddWithValue("@Shift", EmpSection.Head);
                sqlCommand.Parameters.AddWithValue("@Department", EmpSection.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", EmpSection.VacationDaysID);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting Academic.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }

        public IEnumerable<EmployeeSectionModel> GetSectionListByUserId(int UserId)
        {
            try
            {
                List<EmployeeSectionModel> sectionList = new List<EmployeeSectionModel>();
                EmployeeSectionModel sectionModel;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeSectionByUserId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    // Check if the reader returned any rows
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            sectionModel = new EmployeeSectionModel();
                            sectionModel.EmployeeSectionID = Convert.ToInt32(sqlDataReader["EmployeeSectionID"].ToString());
                            sectionModel.EmployeeSectionName_1 = sqlDataReader["EmployeeSectionName_1"].ToString();
                            sectionList.Add(sectionModel);
                        }
                    }
                }
                return sectionList.OrderBy(x => x.EmployeeSectionName_1).AsEnumerable();
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
