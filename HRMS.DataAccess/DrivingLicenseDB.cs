﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class DrivingLicenseDB : DBHelper
    {
        public OperationDetails InsertDrivingLicense(DrivingLicenseModel DrivingLicenseModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    

                        new SqlParameter("@DocumentNo",DrivingLicenseModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", DrivingLicenseModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", DrivingLicenseModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(DrivingLicenseModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(DrivingLicenseModel.ExpiryDate)) ,
                      new SqlParameter("@Note",DrivingLicenseModel.Note ) ,
                      new SqlParameter("@IsPrimary", DrivingLicenseModel.IsPrimary ),
                     new SqlParameter("@DrivingLicenseFile", DrivingLicenseModel.DrivingLicenseFile ),
                      
                      new SqlParameter("@EmployeeId", DrivingLicenseModel.EmployeeId), 
                    };
                int DocVisaId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_DrivingLicense", parameters));
                return new OperationDetails(true, "DrivingLicense  saved successfully.", null, DocVisaId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting DrivingLicense .", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateDrivingLicense(DrivingLicenseModel DrivingLicenseModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@DocDrivingLicenseId",DrivingLicenseModel.DocDrivingLicenseId),
                      new SqlParameter("@DocumentNo",DrivingLicenseModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", DrivingLicenseModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", DrivingLicenseModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(DrivingLicenseModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(DrivingLicenseModel.ExpiryDate)) ,
                      new SqlParameter("@Note",DrivingLicenseModel.Note ) ,
                      new SqlParameter("@IsPrimary", DrivingLicenseModel.IsPrimary ),
                       new SqlParameter("@DrivingLicenseFile", DrivingLicenseModel.DrivingLicenseFile )
                     
                      
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_DrivingLicense", parameters);
                return new OperationDetails(true, "DrivingLicense   updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating DrivingLicenseDocument.", exception);
                throw;
            }
            finally
            {

            }
        }

        public DrivingLicenseModel DrivingLicenseById(int DocDrivingLicenseId)
        {
            try
            {
                DrivingLicenseModel DrivingLicenseModel = new DrivingLicenseModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_DrivingLicenseByID", new SqlParameter("@DocDrivingLicenseId", DocDrivingLicenseId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            DrivingLicenseModel.DocDrivingLicenseId = Convert.ToInt32(reader["DocDrivingLicenseId"].ToString());
                            DrivingLicenseModel.DocumentNo = reader["DocumentNo"].ToString();
                            DrivingLicenseModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            DrivingLicenseModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            DrivingLicenseModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString()) ;
                            DrivingLicenseModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString()) ;
                            DrivingLicenseModel.Note = reader["Note"].ToString() ;
                            DrivingLicenseModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString()) ;
                            DrivingLicenseModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]) ;
                            DrivingLicenseModel.DrivingLicenseFile = Convert.ToString(reader["DrivingLicenseFile"]);
                            DrivingLicenseModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                          
                        }
                    }
                }
                return DrivingLicenseModel;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteDrivingLicenseByID(int DocDrivingLicenseId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@DocDrivingLicenseId",DocDrivingLicenseId) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_DrivingLicense", parameters);
                return new OperationDetails(true, "DrivingLicense Document deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting DrivingLicense Document.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<DrivingLicenseModel> GetDrivingLicenseListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder,string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {    
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the LabourContract		
            List<DrivingLicenseModel> DrivingLicenseModelList = new List<DrivingLicenseModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_DrivingLicenseWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        DrivingLicenseModel DrivingLicenseModel;
                        while (reader.Read())
                        {
                            DrivingLicenseModel = new DrivingLicenseModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            DrivingLicenseModel.DocDrivingLicenseId = Convert.ToInt32(reader["DocDrivingLicenseId"].ToString());
                            DrivingLicenseModel.DocumentNo = reader["DocumentNo"].ToString();
                            DrivingLicenseModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            DrivingLicenseModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            DrivingLicenseModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            DrivingLicenseModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            DrivingLicenseModel.Note = reader["Note"].ToString();
                            DrivingLicenseModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                           
                            DrivingLicenseModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            DrivingLicenseModel.DrivingLicenseFile = Convert.ToString(reader["DrivingLicenseFile"]);
                            
                            DrivingLicenseModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            DrivingLicenseModelList.Add(DrivingLicenseModel);
                        }
                    }
                }
                // Finally, we return our list of DrivingLicenseModelList
                totalCount = TotalCount;
                return DrivingLicenseModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching LabourContract List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
