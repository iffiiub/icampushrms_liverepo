﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using System.Configuration;



namespace HRMS.DataAccess
{


    public class EmployeeLeaveRequestDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        #region CURD Operation EmployeeLeaveRequestModule
        /// <summary>
        /// Insert Employee Leave Request into EmployeeLeaveRequest table
        /// Created By : Hardik 
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails InsertEmployeeLeaveRequest(EmployeeLeaveRequestModel employeeLeaveRequestModel)
        {
            string Message = "";
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@EmployeeId",employeeLeaveRequestModel.EmployeeId) ,
                      new SqlParameter("@ApproverId", employeeLeaveRequestModel.ApproverId) ,
                      new SqlParameter("@Reason", employeeLeaveRequestModel.Reason) ,
                      new SqlParameter("@Description", employeeLeaveRequestModel.Description),
                      new SqlParameter("@LeaveType", employeeLeaveRequestModel.leaveType) ,
                      new SqlParameter("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(employeeLeaveRequestModel.StartDate)) ,
                      new SqlParameter("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(employeeLeaveRequestModel.EndDate)),
                      new SqlParameter("@IsApproved", employeeLeaveRequestModel.IsApproved),
                      new SqlParameter("@CreatedBy", employeeLeaveRequestModel.CreatedBy) ,
                      new SqlParameter("@NotificationSentTo",employeeLeaveRequestModel.NotificationSentTo)
                    };
                int Id = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                  "Hr_Stp_Add_Employee_LeaveRequest", parameters));
                return new OperationDetails(true, "Leave Request saved successfully.", null, Id);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Leave Request.", exception);
                //throw;
            }
            finally
            {
            }
        }





        //        int LeaveRequestId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
        //                            "stp_Add_HR_Employee_LeaveRequest", parameters));
        //        return new OperationDetails(true, "Employee leave request saved successfully.", null, LeaveRequestId);
        //    }
        //    catch (Exception exception)
        //    {
        //        return new OperationDetails(false, "Error : while inseting LeaveRequestId.", exception);
        //        //throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        /// <summary>
        /// update Employee Leave Request into EmployeeLeaveRequest table
        /// Created By : Hardik 
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails UpdateEmployeeLeaveRequest(EmployeeLeaveRequestModel employeeLeaveRequestModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeLeaveId",employeeLeaveRequestModel.EmployeeLeaveId) ,
                       new SqlParameter("@EmployeeId",employeeLeaveRequestModel.EmployeeId) ,
                      new SqlParameter("@ApproverId",employeeLeaveRequestModel.ApproverId) ,
                      new SqlParameter("@Reason", employeeLeaveRequestModel.Reason) ,
                      new SqlParameter("@Description", employeeLeaveRequestModel.Description) ,
                      new SqlParameter("@LeaveType", employeeLeaveRequestModel.leaveType),
                      new SqlParameter("@StartDate", employeeLeaveRequestModel.StartDate) ,
                      new SqlParameter("@EndDate",employeeLeaveRequestModel.EndDate)  ,
                      new SqlParameter("@IsApproved", employeeLeaveRequestModel.IsApproved) ,
                      new SqlParameter("@CreatedBy", employeeLeaveRequestModel.CreatedBy),
                      new SqlParameter("@NotificationSentTo", employeeLeaveRequestModel.NotificationSentTo) ,
                      new SqlParameter("@GenderID", employeeLeaveRequestModel.GenderID) ,
                      new SqlParameter("@NationalityID", employeeLeaveRequestModel.NationalityID),
                      new SqlParameter("@ReligionID", employeeLeaveRequestModel.ReligionID)

                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Employee_LeaveRequest", parameters);
                return new OperationDetails(true, "Employee Leave Request updated successfully.", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating employee Leave Request.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all Employee Leave Request from the EmployeeLeaveRequest table
        /// Created By : Hardik 
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>List of Employee Leave Request</returns>
        public List<EmployeeLeaveRequestModel> GetEmployeeLeaveRequestList()
        {
            // Create a list to hold the Employee Leave Request		
            List<EmployeeLeaveRequestModel> employeeLeaveRequestModelList = new List<EmployeeLeaveRequestModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Employee_LeaveRequestList"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new EmployeeLeaveRequest, and insert them into our list
                        EmployeeLeaveRequestModel employeeLeaveRequestModel;
                        while (reader.Read())
                        {
                            employeeLeaveRequestModel = new EmployeeLeaveRequestModel();
                            employeeLeaveRequestModel.EmployeeLeaveId = Convert.ToInt32(reader["EmployeeLeaveId"].ToString());
                            employeeLeaveRequestModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            employeeLeaveRequestModel.ApproverId = Convert.ToInt32(reader["ApproverId"].ToString());
                            employeeLeaveRequestModel.Reason = Convert.ToInt32(reader["Reason"].ToString());
                            employeeLeaveRequestModel.Description = reader["Description"].ToString();
                            employeeLeaveRequestModel.leaveType = Convert.ToInt32(reader["LeaveType"].ToString());
                            employeeLeaveRequestModel.StartDate = reader["StartDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["StartDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.EndDate = reader["EndDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["EndDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.IsApproved = Convert.ToBoolean(reader["IsApproved"].ToString());
                            employeeLeaveRequestModel.CreatedOn = reader["CreatedOn"].ToString();
                            employeeLeaveRequestModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                            employeeLeaveRequestModel.NotificationSentTo = reader["NotificationSentTo"].ToString();
                            employeeLeaveRequestModelList.Add(employeeLeaveRequestModel);
                        }
                    }
                }
                // Finally, we return our list of employeeLeaveRequest
                return employeeLeaveRequestModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeLeaveRequest List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get Employee Leave Request By id from the EmployeeLeaveRequest table
        /// Created By : Hardik 
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>Employee Leave Request</returns>
        public EmployeeLeaveRequestModel GetEmployeeLeaveRequestByEmployeeLeaveId(int EmployeeLeaveId)
        {
            try
            {
                EmployeeLeaveRequestModel employeeLeaveRequestModel = new EmployeeLeaveRequestModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_Employee_LeaveRequestByEmployeeLeaveId", new SqlParameter("@EmployeeLeaveId", EmployeeLeaveId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            employeeLeaveRequestModel.EmployeeLeaveId = Convert.ToInt32(reader["EmployeeLeaveId"].ToString());
                            employeeLeaveRequestModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            employeeLeaveRequestModel.ApproverId = Convert.ToInt32(reader["ApproverId"].ToString());
                            employeeLeaveRequestModel.Reason = Convert.ToInt32(reader["Reason"].ToString());
                            employeeLeaveRequestModel.Description = reader["Description"].ToString();
                            employeeLeaveRequestModel.leaveType = Convert.ToInt32(reader["LeaveType"].ToString());
                            employeeLeaveRequestModel.StartDate = reader["StartDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["StartDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.EndDate = reader["EndDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["EndDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.IsApproved = Convert.ToBoolean(reader["IsApproved"].ToString());
                            employeeLeaveRequestModel.CreatedOn = reader["CreatedOn"].ToString();
                            employeeLeaveRequestModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                            employeeLeaveRequestModel.NotificationSentTo = reader["NotificationSentTo"].ToString();
                        }
                    }
                }
                return employeeLeaveRequestModel;
                // Finally, we return employeeLeaveRequest

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeLeaveRequest.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Delete Employee Leave Request from the EmployeeLeaveRequest table
        /// Created By : Hardik 
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails DeleteEmployeeLeaveRequestByEmployeeLeaveId(int EmployeeLeaveId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@EmployeeLeaveId",EmployeeLeaveId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Hr_Stp_Delete_Employee_LeaveRequestByEmployeeLeaveId", parameters);
                return new OperationDetails(true, "Employee Leave deleted successfully.", null);

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while deleting Employee Leave.", exception);
                throw;
            }
            finally
            {

            }
        }


        /// <summary>
        /// Get All details of Employee at the time of Edit
        /// </summary>
        /// <param name="EmployeeId">EmployeeId</param>
        /// <returns></returns>
        public Employee GetProfileDetailsOfEmployee(int EmployeeId)
        {

            Employee Employee = new Employee();
            using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetEmployeeDetailsForLeave", new SqlParameter("@EmployeeID", EmployeeId)))
            {
                // Check if the reader returned any rows
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Employee.EmployeeId = EmployeeId;
                        Employee.employeeDetailsModel.GenderID = Convert.ToInt32(reader["GenderID"].ToString());
                        Employee.employeeDetailsModel.ReligionID = Convert.ToInt32(reader["ReligionID"].ToString());
                        Employee.employeeDetailsModel.NationalityID = reader["NationalityID"].ToString();
                        Employee.employeeDetailsModel.MaritalStatusID = Convert.ToInt32(reader["MaritalStatusID"].ToString());
                        Employee.employmentInformation.HireDate = reader["HireDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["HireDate"])).ToString("dd/MM/yyyy");

                    }
                }
            }
            return Employee;
            // Finally, we return Employee

        }


        /// <summary>
        /// Get Employee Leave Request By id from the EmployeeLeaveRequest table
        /// Created By : Hardik 
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>Employee Leave Request</returns>
        public List<EmployeeLeaveRequestModel> GetEmployeeLeaveRequestByEmployeeId(int EmployeeId)
        {
            try
            {
                List<EmployeeLeaveRequestModel> employeeLeaveRequestModelList = new List<EmployeeLeaveRequestModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_Employee_LeaveRequestByEmployeeId", new SqlParameter("@EmployeeId", EmployeeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        EmployeeLeaveRequestModel employeeLeaveRequestModel;
                        while (reader.Read())
                        {
                            employeeLeaveRequestModel = new EmployeeLeaveRequestModel();
                            employeeLeaveRequestModel.EmployeeLeaveId = Convert.ToInt32(reader["EmployeeLeaveId"].ToString());
                            employeeLeaveRequestModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            employeeLeaveRequestModel.ApproverId = Convert.ToInt32(reader["ApproverId"].ToString());
                            employeeLeaveRequestModel.Reason = Convert.ToInt32(reader["Reason"].ToString());
                            employeeLeaveRequestModel.Description = reader["Description"].ToString();
                            employeeLeaveRequestModel.leaveType = Convert.ToInt32(reader["LeaveType"].ToString());
                            employeeLeaveRequestModel.StartDate = reader["StartDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["StartDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.EndDate = reader["EndDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["EndDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.IsApproved = Convert.ToBoolean(reader["IsApproved"].ToString());
                            employeeLeaveRequestModel.CreatedOn = reader["CreatedOn"].ToString();
                            employeeLeaveRequestModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                            employeeLeaveRequestModel.NotificationSentTo = reader["NotificationSentTo"].ToString();

                            employeeLeaveRequestModelList.Add(employeeLeaveRequestModel);
                        }
                    }
                }
                return employeeLeaveRequestModelList;
                // Finally, we return employeeLeaveRequest

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeLeaveRequest.", exception);
                throw;
            }
            finally
            {

            }
        }


        public EmployeeLeaveRequestModel GetEmployeeLeaveRequestByEmployeeIdAndLeaveId(int EmployeeId, int LeaveTypeId)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                      new SqlParameter("@EmployeeId", EmployeeId),
                       new SqlParameter("@LeaveTypeId",LeaveTypeId)

                    };

                EmployeeLeaveRequestModel employeeLeaveRequestModel = new EmployeeLeaveRequestModel();
                // List<EmployeeLeaveRequestModel> employeeLeaveRequestModelList = new List<EmployeeLeaveRequestModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_stp_Get_Employee_LeaveReasonByEmployeeIdAndLeaveId", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {

                            employeeLeaveRequestModel.EmployeeLeaveId = Convert.ToInt32(reader["EmployeeLeaveId"].ToString());
                            employeeLeaveRequestModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            employeeLeaveRequestModel.ApproverId = Convert.ToInt32(reader["ApproverId"].ToString());
                            employeeLeaveRequestModel.Reason = Convert.ToInt32(reader["Reason"].ToString());
                            employeeLeaveRequestModel.Description = reader["Description"].ToString();
                            employeeLeaveRequestModel.leaveType = Convert.ToInt32(reader["LeaveType"].ToString());
                            employeeLeaveRequestModel.StartDate = reader["StartDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["StartDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.EndDate = reader["EndDate"] == DBNull.Value ? "" : Convert.ToDateTime(Convert.ToString(reader["EndDate"])).ToString("dd/MM/yyyy");
                            employeeLeaveRequestModel.IsApproved = Convert.ToBoolean(reader["IsApproved"].ToString());
                            employeeLeaveRequestModel.CreatedOn = reader["CreatedOn"].ToString();
                            employeeLeaveRequestModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                            employeeLeaveRequestModel.NotificationSentTo = reader["NotificationSentTo"].ToString();
                        }
                    }
                }
                return employeeLeaveRequestModel;
                // Finally, we return employeeLeaveRequest

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeLeaveRequest.", exception);
                throw;
            }
            finally
            {

            }
        }


        public List<EmployeeLeaveRequestModel> GetEmployeeLeaveReasonByEmployeeId(int EmployeeId)
        {
            try
            {
                List<EmployeeLeaveRequestModel> employeeLeaveRequestModelList = new List<EmployeeLeaveRequestModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_Get_Employee_LeaveReasonByEmployeeId", new SqlParameter("@EmployeeId", EmployeeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        EmployeeLeaveRequestModel employeeLeaveRequestModel;
                        while (reader.Read())
                        {
                            employeeLeaveRequestModel = new EmployeeLeaveRequestModel();
                            employeeLeaveRequestModel.Reason = Convert.ToInt32(reader["Reason"].ToString());
                            employeeLeaveRequestModel.DaysTaken = Convert.ToInt32(reader["DaysTaken"].ToString());

                            employeeLeaveRequestModelList.Add(employeeLeaveRequestModel);
                        }
                    }
                }
                return employeeLeaveRequestModelList;
                // Finally, we return employeeLeaveRequest

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeLeaveRequest.", exception);
                throw;
            }
            finally
            {

            }
        }

        #endregion


    }
}
