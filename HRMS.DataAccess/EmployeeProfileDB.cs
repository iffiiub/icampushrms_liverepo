﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data;
namespace HRMS.DataAccess
{
    class EmployeeProfileDB
    {

        public string connectionString { get; set; }
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        /// <summary>
        /// Add Employee Profile Details
        /// </summary>
        /// <param name="employeeprofilemodel"></param>
        /// <returns></returns>
        public string AddEmployeeProfile(EmployeeProfileModel employeeprofilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_EmployeeProfile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ProfileId", employeeprofilemodel.ProfileId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeprofilemodel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@AssignedDate", employeeprofilemodel.AssignedDate);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", employeeprofilemodel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CreatedOn", employeeprofilemodel.CreatedOn);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Update Employee Profile Details
        /// </summary>
        /// <param name="employeeprofilemodel"></param>
        /// <returns></returns>
        public string UpdateEmployeeProfile(EmployeeProfileModel employeeprofilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_EmployeeProfile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeProfileId", employeeprofilemodel.EmployeeProfileId);
                sqlCommand.Parameters.AddWithValue("@ProfileId", employeeprofilemodel.ProfileId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeprofilemodel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@AssignedDate", employeeprofilemodel.AssignedDate);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", employeeprofilemodel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CreatedOn", employeeprofilemodel.CreatedOn);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Delete Employee Profile Details By EmployeeProfileId
        /// </summary>
        /// <param name="employeeprofilemodel"></param>
        /// <returns></returns>
        public string DeleteEmployeeProfile(EmployeeProfileModel employeeprofilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_EmployeeProfile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeProfileId", employeeprofilemodel.EmployeeProfileId);
                
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Get All Employee Profiles
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<EmployeeProfileModel> GetAllEmployeeProfiles(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder)
        {
            List<EmployeeProfileModel> employeeProfileList = null;
            try
            {
                employeeProfileList = new List<EmployeeProfileModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeProfile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);


                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeProfileModel employeeProfileModel;
                    while (sqlDataReader.Read())
                    {
                        employeeProfileModel = new EmployeeProfileModel();

                        employeeProfileModel.EmployeeProfileId = Convert.ToInt32(sqlDataReader["EmployeeProfileId"].ToString());
                        employeeProfileModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        employeeProfileModel.ProfileId = Convert.ToInt32(sqlDataReader["ProfileId"]);
                        employeeProfileModel.AssignedDate = Convert.ToDateTime(sqlDataReader["AssignedDate"]);
                        employeeProfileModel.CreatedBy = Convert.ToInt32(sqlDataReader["CreatedBy"]);
                        employeeProfileModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"]);
                        employeeProfileModel.ModifiedBy = Convert.ToInt32(sqlDataReader["ModifiedBy"].ToString());
                        employeeProfileModel.ModifiedOn = Convert.ToDateTime(sqlDataReader["ModifiedOn"]);
                        employeeProfileList.Add(employeeProfileModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeProfileList;
        }





    }
}
