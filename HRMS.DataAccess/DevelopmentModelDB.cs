﻿using HRMS.DataAccess.GeneralDB;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.CommonExtensionsHelper;
using HRMS.Entities.Forms;
using HRMS.Entities.General;
using HRMS.Entities.PDRP;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace HRMS.DataAccess.FormsDB
{
    public class DevelopmentModelDB : FormsDB
    {

        public List<DevelopmentGoalViewModel> GetAllDevelopmentPlans(DevelopmentGoalSearchModel developmentGoalSearchModel)
        {
            List<DevelopmentGoalViewModel> developmentGoals = new List<DevelopmentGoalViewModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DPGetDevelopmentPlans", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", (developmentGoalSearchModel.CompanyId.HasValue == true ? (Object)developmentGoalSearchModel.CompanyId.Value : (Object)DBNull.Value));
                sqlCommand.Parameters.AddWithValue("@EmployeeId", (developmentGoalSearchModel.EmployeeID.HasValue == true ? (Object)developmentGoalSearchModel.EmployeeID.Value : (Object)DBNull.Value));
                sqlCommand.Parameters.AddWithValue("@StatusIds", developmentGoalSearchModel.StatusIds);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    DevelopmentGoalViewModel goal;
                    while (sqlDataReader.Read())
                    {
                        goal = new DevelopmentGoalViewModel();

                        goal.OracleNumber = sqlDataReader["OracleNumber"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["OracleNumber"]);
                        goal.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["EmployeeAlternativeID"]);
                        goal.ID = Convert.ToInt32(sqlDataReader["DevelopmentGoalId"] == DBNull.Value ? 0 : sqlDataReader["DevelopmentGoalId"]);
                        goal.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"] == DBNull.Value ? "" : sqlDataReader["CompanyID"]);
                        goal.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"] == DBNull.Value ? "" : sqlDataReader["DepartmentID"]);
                        goal.PositionID = Convert.ToInt32(sqlDataReader["PositionID"] == DBNull.Value ? "" : sqlDataReader["PositionID"]);
                        goal.SectionRecordID = Convert.ToInt32(sqlDataReader["SectionRecordID"] == DBNull.Value ? "" : sqlDataReader["SectionRecordID"]);
                        goal.SectionName = Convert.ToString(sqlDataReader["SectionName"] == DBNull.Value ? "" : sqlDataReader["SectionName"]);
                        goal.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        goal.RequestNumber = Convert.ToInt32(sqlDataReader["RequestNumber"] == DBNull.Value ? 0 : sqlDataReader["RequestNumber"]);
                        goal.LearningCategory = Convert.ToString(sqlDataReader["LearningCategory"] == DBNull.Value ? 0 : sqlDataReader["LearningCategory"]);
                        goal.CategoryName_1 = Convert.ToString(sqlDataReader["CategoryName_1"] == DBNull.Value ? "" : sqlDataReader["CategoryName_1"]);
                        goal.GoalName = Convert.ToString(sqlDataReader["GoalName"] == DBNull.Value ? "" : sqlDataReader["GoalName"]);
                        goal.GoalDescription = Convert.ToString(sqlDataReader["GoalDescription"] == DBNull.Value ? 0 : sqlDataReader["GoalDescription"]);
                        goal.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"] == DBNull.Value ? string.Empty : sqlDataReader["StartDate"].ToString());
                        goal.CompletionDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["CompletionDate"] == DBNull.Value ? string.Empty : sqlDataReader["CompletionDate"].ToString());
                        goal.StatusName_1 = Convert.ToString(sqlDataReader["StatusName_1"] == DBNull.Value ? 0 : sqlDataReader["StatusName_1"]);
                        developmentGoals.Add(goal);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return developmentGoals;
        }
        public List<RequestFormsApproverEmailModel> GetDevelopmentGoalRequesterEmailList(int DevelopmentGoalId)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DpGetRequesterEmailDetailsForDevelopmentPlan", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DevelopmentGoalId", DevelopmentGoalId);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    RequestFormsApproverEmailModel requestFormsApproverEmailModel;
                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();
                        requestFormsApproverEmailModel.EmployeeFullName = reader["EmployeeFullName"].ToString();
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestFormsApproverEmailModel.IsRequester = bool.Parse(Convert.ToString(reader["IsRequester"]));
                        requestFormsApproverEmailModel.IsApprover = bool.Parse(Convert.ToString(reader["IsApprover"]));
                        requestFormsApproverEmailModel.IsNextApprover = bool.Parse(Convert.ToString(reader["IsNextApprover"]));
                        requestFormsApproverEmailModel.BUName = Convert.ToString(reader["BUName"]);
                        requestFormsApproverEmailModel.BUShortName_1 = Convert.ToString(reader["BUShortName_1"]);
                        requestFormsApproverEmailModel.FormName = Convert.ToString(reader["FormName"]);
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }
        /// <summary>
        /// All Development Plans
        /// </summary>
        /// <param name="DevelopmentGoalId"></param>
        /// <returns></returns>
        public DevelopmentGoalModel SelectDevelopmentPlan(int DevelopmentGoalId)
        {
            var developmentGoals = new DevelopmentGoalModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DPSelectDevelopmentPlanDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Id", DevelopmentGoalId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        developmentGoals = new DevelopmentGoalModel();
                        developmentGoals.ID = Convert.ToInt32(sqlDataReader["ID"]);
                        developmentGoals.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"]);
                        developmentGoals.JobDesignationCompetencyID = Convert.ToInt32(sqlDataReader["JobDesignationCompetencyID"] == DBNull.Value ? 0 : sqlDataReader["JobDesignationCompetencyID"]);
                        developmentGoals.CompanyId = Convert.ToInt32(sqlDataReader["CompanyID"]);
                        developmentGoals.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        developmentGoals.RequestNumber = Convert.ToString(sqlDataReader["RequestNumber"] == DBNull.Value ? 0 : sqlDataReader["RequestNumber"]);
                        developmentGoals.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"]);
                        developmentGoals.DPCategoryID = Convert.ToInt32(sqlDataReader["DPCategoryID"]);
                        developmentGoals.AnnualAppraisalID = Convert.ToInt32(sqlDataReader["AnnualAppraisalID"] == DBNull.Value ? 0 : sqlDataReader["AnnualAppraisalID"]);
                        developmentGoals.SectionName = Convert.ToString(sqlDataReader["SectionName"] == DBNull.Value ? string.Empty : sqlDataReader["SectionName"]);
                        developmentGoals.SectionRecordID = Convert.ToInt32(sqlDataReader["SectionRecordID"] == DBNull.Value ? 0 : sqlDataReader["SectionRecordID"]);
                        developmentGoals.GoalName = Convert.ToString(sqlDataReader["GoalName"]);
                        developmentGoals.GoalDescription = Convert.ToString(sqlDataReader["GoalDescription"]);
                        developmentGoals.KPICompetency = Convert.ToString(sqlDataReader["KPICompetency"]);
                        developmentGoals.LearningCategoryId = Convert.ToInt32(sqlDataReader["LearningCategoryId"]);
                        developmentGoals.ActivityTitleId = Convert.ToInt32(sqlDataReader["ActivityTitleId"] == DBNull.Value ? 0 : sqlDataReader["ActivityTitleId"]);
                        developmentGoals.ActivityFormatId = Convert.ToInt32(sqlDataReader["ActivityFormatId"] == DBNull.Value ? 0 : sqlDataReader["ActivityFormatId"]);
                        developmentGoals.Cost = Convert.ToString(sqlDataReader["Cost"]);
                        developmentGoals.Description = Convert.ToString(sqlDataReader["Description"] == DBNull.Value ? string.Empty : sqlDataReader["Description"]);
                        developmentGoals.SuggestedProvider = Convert.ToString(sqlDataReader["SuggestedProvider"]);
                        developmentGoals.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["StartDate"]));
                        developmentGoals.CompletionDate = CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["CompletionDate"]));
                        developmentGoals.StatusID = Convert.ToInt32(sqlDataReader["StatusID"]);
                        developmentGoals.SuccessMeasures = Convert.ToString(sqlDataReader["SuccessMeasures"] == DBNull.Value ? string.Empty : sqlDataReader["SuccessMeasures"]);
                        developmentGoals.SupportAndResources = Convert.ToString(sqlDataReader["SupportAndResources"]);
                        developmentGoals.TrainingStatus = Convert.ToString(sqlDataReader["TrainingStatus"]);
                        developmentGoals.RequestedDate = Convert.ToDateTime(sqlDataReader["RequestedDate"]);
                        developmentGoals.EmployeeEmail = Convert.ToString(sqlDataReader["EmployeeEmail"]);
                        developmentGoals.LMEmployeeID = Convert.ToInt32(sqlDataReader["LMEmployeeID"]);
                        developmentGoals.SecondLMEmployeeID = Convert.ToInt32(sqlDataReader["SecondLMEmployeeID"]);
                        developmentGoals.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"]);
                        developmentGoals.PositionID = Convert.ToInt32(sqlDataReader["PositionID"]);
                    }
                }
                sqlDataReader.NextResult();
                developmentGoals.DevelopmentGoalDetailNotes = new List<DevelopmentGoalDetailNotes>();
                if (sqlDataReader.HasRows)
                {
                    DevelopmentGoalDetailNotes developmentGoalDetailNotes;
                    while (sqlDataReader.Read())
                    {
                        developmentGoalDetailNotes = new DevelopmentGoalDetailNotes();
                        developmentGoalDetailNotes.DevelopmentGoalID = Convert.ToInt32(sqlDataReader["DevelopmentGoalId"]);
                        developmentGoalDetailNotes.ID = Convert.ToInt32(sqlDataReader["Id"]);
                        developmentGoalDetailNotes.Notes = Convert.ToString(sqlDataReader["Notes"]);
                        developmentGoalDetailNotes.FileID = Convert.ToInt32(sqlDataReader["FileId"]);
                        developmentGoals.DevelopmentGoalDetailNotes.Add(developmentGoalDetailNotes);
                    }

                }

                sqlDataReader.NextResult();
                developmentGoals.DevelopmentGoalActionSteps = new List<DevelopmentGoalActionSteps>();
                if (sqlDataReader.HasRows)
                {
                    DevelopmentGoalActionSteps developmentGoalActionSteps;
                    while (sqlDataReader.Read())
                    {
                        developmentGoalActionSteps = new DevelopmentGoalActionSteps();
                        developmentGoalActionSteps.DevelopmentGoalID = Convert.ToInt32(sqlDataReader["DevelopmentGoalId"]);
                        developmentGoalActionSteps.ID = Convert.ToInt32(sqlDataReader["Id"]);
                        developmentGoalActionSteps.ActionStepDescription = Convert.ToString(sqlDataReader["ActionStepDescription"]);
                        developmentGoals.DevelopmentGoalActionSteps.Add(developmentGoalActionSteps);
                    }

                }
                AllFormsFilesModel allFormsFilesModel = null;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                developmentGoals.Attachments = new List<AllFormsFilesModel>();
                foreach (var developmentPlanNotes in developmentGoals.DevelopmentGoalDetailNotes)
                {
                    if (developmentPlanNotes.FileID > 0)
                    {
                        allFormsFilesModel = new AllFormsFilesModel();
                        allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(developmentPlanNotes.FileID);
                        //allFormsFilesModel.FormFileIDName = developmentPlanNotes.FileID;
                        developmentGoals.Attachments.Add(allFormsFilesModel);
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return developmentGoals;
        }
        public List<EmployeeDevelopmentPlanListModel> GetEmployeeDevelopmentPlan(int? CompanyId, int? DepartmentId, int LoginPersonId, bool IsMyPlanScreen)
        {
            List<EmployeeDevelopmentPlanListModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDevelopmentPlanListModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SelectEmployeeDevelopmentPlans", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", (CompanyId.HasValue == true ? (Object)CompanyId.Value : (Object)DBNull.Value));
                sqlCommand.Parameters.AddWithValue("@DepartmentId", (DepartmentId.HasValue == true ? (Object)DepartmentId.Value : (Object)DBNull.Value));
                sqlCommand.Parameters.AddWithValue("@LoginPersonId", LoginPersonId);
                sqlCommand.Parameters.AddWithValue("@IsMyPlanScreen", IsMyPlanScreen);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                // SqlDataReader sqlDataReader = ExecuteProcedure("HR_Stp_SelectEmployeeDevelopmentPlans", sqlParameters);

                if (sqlDataReader.HasRows)
                {
                    EmployeeDevelopmentPlanListModel EmployeeDevelopmentPlanListModel;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDevelopmentPlanListModel = new EmployeeDevelopmentPlanListModel();

                        EmployeeDevelopmentPlanListModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        EmployeeDevelopmentPlanListModel.OracleNumber = sqlDataReader["OracleNumber"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["OracleNumber"]);
                        EmployeeDevelopmentPlanListModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"] == DBNull.Value ? 0 : sqlDataReader["CompanyID"]);
                        EmployeeDevelopmentPlanListModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"] == DBNull.Value ? "" : sqlDataReader["CompanyName"]);
                        EmployeeDevelopmentPlanListModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"] == DBNull.Value ? 0 : sqlDataReader["DepartmentID"]);
                        EmployeeDevelopmentPlanListModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"] == DBNull.Value ? 0 : sqlDataReader["PositionID"]);
                        EmployeeDevelopmentPlanListModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        EmployeeDevelopmentPlanListModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        EmployeeDevelopmentPlanListModel.EmployeeTotalDevelopmentPlans = Convert.ToInt32(sqlDataReader["EmployeeTotalDevelopmentPlans"] == DBNull.Value ? 0 : sqlDataReader["EmployeeTotalDevelopmentPlans"]);
                        EmployeeDevelopmentPlanListModel.EmployeeTotalActiveDevelopmentPlans = Convert.ToInt32(sqlDataReader["TotalActiveDevelopmentPlans"] == DBNull.Value ? 0 : sqlDataReader["TotalActiveDevelopmentPlans"]);
                        EmployeeDevelopmentPlanListModel.EmployeeTotalCompletedDevelopmentPlans = Convert.ToInt32(sqlDataReader["TotalCompletedDevelopmentPlans"] == DBNull.Value ? 0 : sqlDataReader["TotalCompletedDevelopmentPlans"]);

                        employeeList.Add(EmployeeDevelopmentPlanListModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.ToList();
        }

        public List<CategoriesModel> GetAllCategories()
        {
            List<CategoriesModel> CategoriesModelList = null;
            try
            {
                CategoriesModelList = new List<CategoriesModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_DPSelectCategories", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CategoriesModel CategoriesModel;
                    while (sqlDataReader.Read())
                    {
                        CategoriesModel = new CategoriesModel();

                        CategoriesModel.DPCategoryId = Convert.ToInt32(sqlDataReader["DPCategoryId"].ToString());
                        CategoriesModel.CategoryName_1 = Convert.ToString(sqlDataReader["CategoryName_1"]).Trim();
                        CategoriesModel.CategoryName_2 = Convert.ToString(sqlDataReader["CategoryName_2"]).Trim();
                        CategoriesModel.CategoryName_3 = Convert.ToString(sqlDataReader["CategoryName_3"]).Trim();

                        CategoriesModelList.Add(CategoriesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return CategoriesModelList;
        }

        public List<FormStatusModel> GetAllFormStatus()
        {
            List<FormStatusModel> FormStatusModelList = null;
            try
            {
                FormStatusModelList = new List<FormStatusModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_DPSelectFormStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FormStatusModel FormStatusModel;
                    while (sqlDataReader.Read())
                    {
                        FormStatusModel = new FormStatusModel();

                        FormStatusModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        FormStatusModel.StatusName_1 = Convert.ToString(sqlDataReader["StatusName_1"]).Trim();
                        FormStatusModel.StatusName_2 = Convert.ToString(sqlDataReader["StatusName_2"]).Trim();
                        FormStatusModel.StatusName_3 = Convert.ToString(sqlDataReader["StatusName_3"]).Trim();

                        FormStatusModelList.Add(FormStatusModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return FormStatusModelList;
        }

        public MasterDataModel GetAllFormMasterData(int? masterdataTypeId = null)
        {
            MasterDataModel masterData = new MasterDataModel();
            try
            {
                List<FormMasterDataModel> FormMasterDataModelList = new List<FormMasterDataModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_DPSelectFormMasterData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MasterDataTypeId", (masterdataTypeId.HasValue == true ? (Object)masterdataTypeId.Value : (Object)DBNull.Value));
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FormMasterDataModel FormMasterDataModel;
                    while (sqlDataReader.Read())
                    {
                        FormMasterDataModel = new FormMasterDataModel();

                        FormMasterDataModel.HR_DPFormMasterDataTypeID = Convert.ToInt32(sqlDataReader["HR_DPFormMasterDataTypeID"].ToString());
                        FormMasterDataModel.HR_DPFormMasterDataID = Convert.ToInt32(sqlDataReader["HR_DPFormMasterDataID"].ToString());
                        FormMasterDataModel.OptionName_1 = Convert.ToString(sqlDataReader["OptionName_1"]).Trim();
                        FormMasterDataModel.OptionName_2 = Convert.ToString(sqlDataReader["OptionName_2"]).Trim();
                        FormMasterDataModel.OptionName_3 = Convert.ToString(sqlDataReader["OptionName_3"]).Trim();
                        FormMasterDataModelList.Add(FormMasterDataModel);
                    }
                }
                //MasterDataModel
                foreach (var materdataTypeId in FormMasterDataModelList.Select(m => m.HR_DPFormMasterDataTypeID).Distinct())
                {
                    if (materdataTypeId == (int)MasterDataType.LearningCategory)
                    {
                        masterData.LearningCategories = FormMasterDataModelList.Where(m => m.HR_DPFormMasterDataTypeID == materdataTypeId).ToList();
                    }
                    if (materdataTypeId == (int)MasterDataType.TrainingActivityFormat)
                    {
                        masterData.ActivityFormat = FormMasterDataModelList.Where(m => m.HR_DPFormMasterDataTypeID == materdataTypeId).ToList();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return masterData;
        }
        public List<TrainingCompetenciesModel> GetAllTrainingCompetencies(int CompetenciesID)
        {
            List<TrainingCompetenciesModel> TrainingCompetenciesModelList = null;
            try
            {
                TrainingCompetenciesModelList = new List<TrainingCompetenciesModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SelectTrainingCompetencies", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@CompetencyID", CompetenciesID);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    TrainingCompetenciesModel TrainingCompetenciesModel;
                    while (sqlDataReader.Read())
                    {
                        TrainingCompetenciesModel = new TrainingCompetenciesModel();

                        TrainingCompetenciesModel.TrainingID = Convert.ToInt32(sqlDataReader["TrainingID"].ToString());
                        TrainingCompetenciesModel.TrainingName_1 = Convert.ToString(sqlDataReader["TrainingName_1"]).Trim();

                        TrainingCompetenciesModelList.Add(TrainingCompetenciesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return TrainingCompetenciesModelList;
        }

        public List<TrainingCompetenciesModel> GetAllActiveTrainingCompetencies(int companyId)
        {
            List<TrainingCompetenciesModel> TrainingCompetenciesModelList = null;
            try
            {
                TrainingCompetenciesModelList = new List<TrainingCompetenciesModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SelectAllTrainingCompetencies", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@CompanyID", companyId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    TrainingCompetenciesModel TrainingCompetenciesModel;
                    while (sqlDataReader.Read())
                    {
                        TrainingCompetenciesModel = new TrainingCompetenciesModel();

                        TrainingCompetenciesModel.TrainingID = Convert.ToInt32(sqlDataReader["TrainingID"].ToString());
                        TrainingCompetenciesModel.TrainingName_1 = Convert.ToString(sqlDataReader["TrainingName_1"]).Trim();

                        TrainingCompetenciesModelList.Add(TrainingCompetenciesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return TrainingCompetenciesModelList;
        }


        public List<DevelopmentCompetency> GetAllActiveCompetencies(int? trainingId)
        {
            List<DevelopmentCompetency> CompetenciesModelList = null;
            try
            {
                CompetenciesModelList = new List<DevelopmentCompetency>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SelectAllCompetencies", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@TrainingId", trainingId ?? (object)DBNull.Value);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DevelopmentCompetency CompetenciesModel;
                    while (sqlDataReader.Read())
                    {
                        CompetenciesModel = new DevelopmentCompetency();

                        CompetenciesModel.CompentencyID = Convert.ToInt32(sqlDataReader["CompentencyID"].ToString());
                        CompetenciesModel.Competencytitle_1 = Convert.ToString(sqlDataReader["Competencytitle_1"]).Trim();

                        CompetenciesModelList.Add(CompetenciesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return CompetenciesModelList;
        }
        public List<JDDesignationCompetenciesModel> GetAllJobCompetencies(int PositionID)
        {
            List<JDDesignationCompetenciesModel> JDDesignationCompetenciesModelList = null;
            try
            {
                JDDesignationCompetenciesModelList = new List<JDDesignationCompetenciesModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SelectJobCompetencies", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PositionID", PositionID);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    JDDesignationCompetenciesModel JDDesignationCompetenciesModel;
                    while (sqlDataReader.Read())
                    {
                        JDDesignationCompetenciesModel = new JDDesignationCompetenciesModel();

                        JDDesignationCompetenciesModel.JDDesignationCompetencyID = Convert.ToInt32(sqlDataReader["JDDesignationCompetencyID"].ToString());
                        JDDesignationCompetenciesModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"].ToString());
                        JDDesignationCompetenciesModel.JobDescriptionID = Convert.ToInt32(sqlDataReader["JobDescriptionID"].ToString());
                        JDDesignationCompetenciesModel.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"].ToString());
                        JDDesignationCompetenciesModel.Competency = Convert.ToString(sqlDataReader["Competency"]).Trim();
                        JDDesignationCompetenciesModel.JobDescription = Convert.ToString(sqlDataReader["JobDescription"]).Trim();

                        JDDesignationCompetenciesModelList.Add(JDDesignationCompetenciesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return JDDesignationCompetenciesModelList;
        }
        private DataTable ConvertGoalAttachmentToDataTable(List<DevelopmentGoalAttachmentModel> attachmentModels)
        {


            DataTable filesTable = new DataTable();
            //resultTable.Columns.Add("WorkflowID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("ID", System.Type.GetType("System.Int16"));
            filesTable.Columns.Add("FileID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("RowID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("Notes");

            foreach (DevelopmentGoalAttachmentModel attachmentModel in attachmentModels)
            {
                filesTable.Rows.Add(attachmentModel.ID,
                                     attachmentModel.FileID,
                                     attachmentModel.RowID,
                                     attachmentModel.Notes);
            }

            return filesTable;
        }
        private DataTable ConvertGoalActionsToDataTable(List<DevelopmentGoalActionSteps> attachmentModels)
        {


            DataTable filesTable = new DataTable();
            //resultTable.Columns.Add("WorkflowID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("ID", System.Type.GetType("System.Int16"));
            filesTable.Columns.Add("DevelopmentGoalID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("ActionStepDescription");

            foreach (DevelopmentGoalActionSteps actionSteps in attachmentModels)
            {
                filesTable.Rows.Add(actionSteps.ID,
                                     actionSteps.DevelopmentGoalID,
                                     actionSteps.ActionStepDescription
                                    );
            }

            return filesTable;
        }
        public OperationDetails InsertDevelopmentGoal(DevelopmentGoalModel objDevelopmentGoalModel)
        {
            try
            {
                /*Insert Update Development Plan*/
                SqlParameter[] parameters =
                    {
                              new SqlParameter("@ID", objDevelopmentGoalModel.ID) ,
                              new SqlParameter("@RequesterEmployeeID", objDevelopmentGoalModel.RequesterEmployeeID) ,
                              new SqlParameter("@CompanyID", objDevelopmentGoalModel.CompanyId) ,
                              new SqlParameter("@EmployeeID", objDevelopmentGoalModel.EmployeeID),
                              new SqlParameter("@ReqStatusID", objDevelopmentGoalModel.ReqStatusID ) ,
                              new SqlParameter("@DPCategoryID",objDevelopmentGoalModel.DPCategoryID ) ,
                              new SqlParameter("@JobDesignationCompetencyID", objDevelopmentGoalModel.JobDesignationCompetencyID ),
                              new SqlParameter("@AnnualAppraisalID",objDevelopmentGoalModel.AnnualAppraisalID),
                              new SqlParameter("@SectionRecordID",objDevelopmentGoalModel.SectionRecordID),
                              new SqlParameter("@SectionName",objDevelopmentGoalModel.SectionName) ,
                              new SqlParameter("@GoalName", objDevelopmentGoalModel.GoalName) ,
                              new SqlParameter("@GoalDescription", objDevelopmentGoalModel.GoalDescription),
                              new SqlParameter("@KPICompetency", objDevelopmentGoalModel.KPICompetency),
                              new SqlParameter("@LearningCategoryId", objDevelopmentGoalModel.LearningCategoryId),
                              new SqlParameter("@ActivityTitleId", objDevelopmentGoalModel.ActivityTitleId),
                              new SqlParameter("@ActivityFormatId", objDevelopmentGoalModel.ActivityFormatId),
                              new SqlParameter("@Cost", objDevelopmentGoalModel.Cost ?? string.Empty),
                              new SqlParameter("@Description", objDevelopmentGoalModel.Description),
                              new SqlParameter("@SuggestedProvider", objDevelopmentGoalModel.SuggestedProvider),
                              new SqlParameter("@StartDate", CommonDB.SetCulturedDate(objDevelopmentGoalModel.StartDate)),
                              new SqlParameter("@CompletionDate", CommonDB.SetCulturedDate(objDevelopmentGoalModel.CompletionDate)),
                              new SqlParameter("@StatusID", objDevelopmentGoalModel.StatusID),
                              new SqlParameter("@SuccessMeasures", objDevelopmentGoalModel.SuccessMeasures),
                              new SqlParameter("@SupportAndResources", objDevelopmentGoalModel.SupportAndResources),
                              new SqlParameter("@TrainingStatus", objDevelopmentGoalModel.TrainingStatus),
                              new SqlParameter("@RequestedDate", objDevelopmentGoalModel.RequestedDate),
                              new SqlParameter("@EmployeeEmail", objDevelopmentGoalModel.EmployeeEmail),
                              new SqlParameter("@LMEmployeeID", objDevelopmentGoalModel.LMEmployeeID),
                              new SqlParameter("@SecondLMEmployeeID", objDevelopmentGoalModel.SecondLMEmployeeID),
                              new SqlParameter("@DepartmentID", objDevelopmentGoalModel.DepartmentID),
                              new SqlParameter("@PositionID", objDevelopmentGoalModel.PositionID),
                              new SqlParameter("@AllFiles", ConvertFilesListToTable(objDevelopmentGoalModel.Attachments)),
                              new SqlParameter("@GoalDetailNotes", ConvertGoalAttachmentToDataTable(objDevelopmentGoalModel.GoalNoteDetails)),
                              new SqlParameter("@ActionSteps", ConvertGoalActionsToDataTable(objDevelopmentGoalModel.DevelopmentGoalActionSteps))
            };

                int developmentGoalId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_Stp_DPInsertUpdateDevelopmentPlan  ", parameters));
                return new OperationDetails(true, "Development Goal Form saved successfully.", null, developmentGoalId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Workexperience.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public List<DevelopmentPlanKPIModel> SelectDevelopmentPlanKPI(int DropInsYear, int AppraisalTeachingID, int Year, int CompanyID)
        {
            List<DevelopmentPlanKPIModel> developmentPlanKPIModelList = new List<DevelopmentPlanKPIModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SelectDevelopmentPlanKPI", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DropInsYear", DropInsYear);
                sqlCommand.Parameters.AddWithValue("@AppraisalTeachingID", AppraisalTeachingID);
                sqlCommand.Parameters.AddWithValue("@Year", Year);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    DevelopmentPlanKPIModel developmentPlanKPIModel;
                    while (reader.Read())
                    {
                        developmentPlanKPIModel = new DevelopmentPlanKPIModel();
                        developmentPlanKPIModel.KPIId = Convert.ToInt32(reader["KPIId"] == DBNull.Value ? "0" : reader["KPIId"].ToString());
                        developmentPlanKPIModel.KPIYear = Convert.ToInt32(reader["KPIYear"] == DBNull.Value ? "0" : reader["KPIYear"].ToString());
                        developmentPlanKPIModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        developmentPlanKPIModel.KPIAreaEng = Convert.ToString(reader["KPIAreaEng"] == DBNull.Value ? string.Empty : reader["KPIAreaEng"].ToString());
                        developmentPlanKPIModel.KPIDescriptionEng = Convert.ToString(reader["KPIDescriptionEng"] == DBNull.Value ? string.Empty : reader["KPIDescriptionEng"].ToString());
                        developmentPlanKPIModel.KPIAreaArab = Convert.ToString(reader["KPIAreaArab"]);
                        developmentPlanKPIModel.KPIDescriptionArab = Convert.ToString(reader["KPIDescriptionArab"]);
                        developmentPlanKPIModel.KPIWeightage = Convert.ToInt32(reader["KPIWeightage"] == DBNull.Value ? string.Empty : reader["KPIWeightage"].ToString());
                        developmentPlanKPIModel.KPIOrder = Convert.ToInt32(reader["KPIOrder"] == DBNull.Value ? string.Empty : reader["KPIOrder"].ToString());
                        developmentPlanKPIModelList.Add(developmentPlanKPIModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return developmentPlanKPIModelList;
        }
        public void DevelopmentPlanNonTeachingModel(DevelopmentPlanPDRPModel DevelopmentPlanPDRPModel, SqlDataReader sqlDataReader)
        {
            ProfessionalCompetencies ProfessionalCompetencies = new ProfessionalCompetencies();
            BehavioralCompetencies BehavioralCompetencies = new BehavioralCompetencies();
            AnnualGoalSettingFormModel AnnualGoalSettingFormModel = new AnnualGoalSettingFormModel();

            if (sqlDataReader.HasRows)
            {
                // sqlDataReader.NextResult();
                while (sqlDataReader.Read())
                {
                    ProfessionalCompetencies = new ProfessionalCompetencies();
                    ProfessionalCompetencies.No = Convert.ToInt32(sqlDataReader["ProfessionalCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["ProfessionalCompetenciesNo"].ToString());
                    ProfessionalCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                    ProfessionalCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                    ProfessionalCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                    ProfessionalCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                    ProfessionalCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                    ProfessionalCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                    ProfessionalCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                    ProfessionalCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                    ProfessionalCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                    ProfessionalCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                    ProfessionalCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                    ProfessionalCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                    ProfessionalCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                    ProfessionalCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);
                    DevelopmentPlanPDRPModel.ProfessionalCompetencies.Add(ProfessionalCompetencies);

                }

            }
            if (DevelopmentPlanPDRPModel.PerformanceGroupId == 2 || DevelopmentPlanPDRPModel.PerformanceGroupId == 3)
            {
                sqlDataReader.NextResult();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        BehavioralCompetencies = new BehavioralCompetencies();
                        BehavioralCompetencies.No = Convert.ToInt32(sqlDataReader["BehavioralCompetenciesNo"] == DBNull.Value ? "0" : sqlDataReader["BehavioralCompetenciesNo"].ToString());
                        BehavioralCompetencies.Score = Convert.ToString(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());


                        BehavioralCompetencies.CompetencyID = Convert.ToInt32(sqlDataReader["CompetencyID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyID"].ToString());
                        BehavioralCompetencies.CompetencyTypeID = Convert.ToInt32(sqlDataReader["CompetencyTypeID"] == DBNull.Value ? "0" : sqlDataReader["CompetencyTypeID"].ToString());

                        BehavioralCompetencies.CompetencyTypeName_1 = Convert.ToString(sqlDataReader["CompetencyTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_1"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_2 = Convert.ToString(sqlDataReader["CompetencyTypeName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_2"].ToString());
                        BehavioralCompetencies.CompetencyTypeName_3 = Convert.ToString(sqlDataReader["CompetencyTypeName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyTypeName_3"].ToString());

                        BehavioralCompetencies.CompetencyName_1 = Convert.ToString(sqlDataReader["CompetencyName_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_1"].ToString());
                        BehavioralCompetencies.CompetencyName_2 = Convert.ToString(sqlDataReader["CompetencyName_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_2"].ToString());
                        BehavioralCompetencies.CompetencyName_3 = Convert.ToString(sqlDataReader["CompetencyName_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyName_3"].ToString());

                        BehavioralCompetencies.CompetencyDescription_1 = Convert.ToString(sqlDataReader["CompetencyDescription_1"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_1"].ToString());
                        BehavioralCompetencies.CompetencyDescription_2 = Convert.ToString(sqlDataReader["CompetencyDescription_2"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_2"].ToString());
                        BehavioralCompetencies.CompetencyDescription_3 = Convert.ToString(sqlDataReader["CompetencyDescription_3"] == DBNull.Value ? "" : sqlDataReader["CompetencyDescription_3"].ToString());

                        BehavioralCompetencies.IsActive = Convert.ToInt32(sqlDataReader["IsActive"] == DBNull.Value ? 0 : sqlDataReader["IsActive"]);
                        BehavioralCompetencies.IsDeleted = Convert.ToInt32(sqlDataReader["IsDeleted"] == DBNull.Value ? 0 : sqlDataReader["IsDeleted"]);

                        DevelopmentPlanPDRPModel.BehavioralCompetencies.Add(BehavioralCompetencies);
                    }

                }
            }
            sqlDataReader.NextResult();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    AnnualGoalSettingFormModel = new AnnualGoalSettingFormModel();
                    AnnualGoalSettingFormModel.KPIId = Convert.ToInt32(sqlDataReader["KPIId"] == DBNull.Value ? "0" : sqlDataReader["KPIId"].ToString());
                    AnnualGoalSettingFormModel.BusinessTargetDetails = Convert.ToString(sqlDataReader["BusinessTargetDetails"] == DBNull.Value ? "" : sqlDataReader["BusinessTargetDetails"].ToString());
                    AnnualGoalSettingFormModel.Weight = Convert.ToInt32(sqlDataReader["Weight"] == DBNull.Value ? "0" : sqlDataReader["Weight"].ToString());
                    AnnualGoalSettingFormModel.Accomplishments = Convert.ToString(sqlDataReader["Accomplishments"] == DBNull.Value ? "" : sqlDataReader["Accomplishments"].ToString());
                    AnnualGoalSettingFormModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"] == DBNull.Value ? "0" : sqlDataReader["PerformanceGroupId"].ToString());
                    AnnualGoalSettingFormModel.Rating = Convert.ToInt32(sqlDataReader["Rating"] == DBNull.Value ? "0" : sqlDataReader["Rating"].ToString());
                    AnnualGoalSettingFormModel.Score = Convert.ToDecimal(sqlDataReader["Score"] == DBNull.Value ? "0" : sqlDataReader["Score"].ToString());

                    DevelopmentPlanPDRPModel.FormKPIModelList.Add(AnnualGoalSettingFormModel);
                }
            }

        }
        public void DevelopmentPlanTeachingModel(DevelopmentPlanPDRPModel developmentPlanPDRPModel, SqlDataReader reader, string culture)
        {
            AppraisalTeachingEmployeeModel AppraisalTeachingEmployeeModel = new AppraisalTeachingEmployeeModel();
            AppraisalTeachingRatingScaleWeightageDetails objAppraisalTeachingRatingScaleWeightageDetails = new AppraisalTeachingRatingScaleWeightageDetails();
            AppraisalWeightagePercentageModel AppraisalWeightagePercentageModel = new AppraisalWeightagePercentageModel();
            AppraisalKPIsModel KIPsModel = new AppraisalKPIsModel();
            developmentPlanPDRPModel.AppraisalTeachingModel.AppraisalKPIsListModel = new AppraisalKPIsListModel();
            //AppraisalTeachingModelDB.SetAppraisalTeachingInformation(developmentPlanPDRPModel.AppraisalTeachingModel, reader, culture);
            //reader.NextResult();
            AppraisalTeachingModelDB.SetAppraisalTeachingHeader(developmentPlanPDRPModel.AppraisalTeachingModel, reader);


            reader.NextResult();

            if (reader.HasRows)
            {
                developmentPlanPDRPModel.AppraisalKPIsListModel.AppraisalKPIsModel = new List<AppraisalKPIsModel>();
                while (reader.Read())
                {
                    KIPsModel = new AppraisalKPIsModel();

                    KIPsModel.KPIId = Convert.ToInt32(reader["KPIId"].ToString());
                    KIPsModel.KPIYear = Convert.ToString(reader["KPIYear"]);
                    // KIPsModel.CompanyID = CompanyID;
                    KIPsModel.KPIAreaEng = Convert.ToString(reader["KPIAreaEng"]);
                    KIPsModel.KPIDescriptionEng = Convert.ToString(reader["KPIDescriptionEng"]);
                    KIPsModel.KPIAreaArab = Convert.ToString(reader["KPIAreaArab"]);
                    KIPsModel.KPIDescriptionArab = Convert.ToString(reader["KPIDescriptionArab"]);
                    KIPsModel.KPIWeightage = Convert.ToInt32(reader["KPIWeightage"] == DBNull.Value ? 0 : reader["KPIWeightage"]);
                    KIPsModel.Accomplishments = Convert.ToString(reader["Accomplishments"]);
                    KIPsModel.Score = Convert.ToDecimal(reader["Score"] == DBNull.Value ? 0 : reader["Score"]);
                    KIPsModel.Ratings = Convert.ToInt32(reader["Ratings"] == DBNull.Value ? 0 : reader["Ratings"]);
                    KIPsModel.FormatedFormla = Convert.ToString(reader["FormatedFormla"]);

                    developmentPlanPDRPModel.AppraisalKPIsListModel.AppraisalKPIsModel.Add(KIPsModel);
                }

            }


            //DevelopmentPlanPDRPModel.AppraisalKPIsListModel.KPIYear = Year;
            developmentPlanPDRPModel.AppraisalKPIsListModel.AppraisalKPIsModel = developmentPlanPDRPModel.AppraisalKPIsListModel.AppraisalKPIsModel;

        }

        public DevelopmentPlanPDRPModel GetDevelopmentPlanPDRPInformation(int EmployeeID, int CompanyID, string culture)
        {
            DevelopmentPlanPDRPModel developmentPlanPDRPModel = new DevelopmentPlanPDRPModel();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@EmployeeID";
                sqlParameter.Value = EmployeeID;
                sqlParameters[0] = sqlParameter;

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@CompanyId";
                sqlParameter.Value = CompanyID;
                sqlParameters[1] = sqlParameter;
                SqlDataReader sqlDataReader = ExecuteProcedure("HR_STP_SelectDevelopmentPlanPDRPDetail", sqlParameters);
                int userType = 0;
                developmentPlanPDRPModel.EmployeeId = EmployeeID;
                developmentPlanPDRPModel.CompanyId = CompanyID;

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        int.TryParse(sqlDataReader["UserType"].ToString(), out userType);

                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        developmentPlanPDRPModel.PerformanceGroupId = Convert.ToInt32(sqlDataReader["PerformanceGroupId"].ToString());
                        developmentPlanPDRPModel.AnnualAppraisalID = Convert.ToInt32(sqlDataReader["ApprisalId"].ToString());
                    }

                    if (userType == 1)
                    {
                        sqlDataReader.NextResult();
                        DevelopmentPlanTeachingModel(developmentPlanPDRPModel, sqlDataReader, culture);
                        developmentPlanPDRPModel.UserTypeId = userType;
                    }
                    else
                    {

                        sqlDataReader.NextResult();
                        DevelopmentPlanNonTeachingModel(developmentPlanPDRPModel, sqlDataReader);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return developmentPlanPDRPModel;
        }
        public SqlDataReader ExecuteProcedure(string ProcedureName, SqlParameter[] sqlParameters)
        {
            SqlDataReader reader;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(ProcedureName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                foreach (SqlParameter Param in sqlParameters)
                {
                    sqlCommand.Parameters.Add(Param);
                }


                reader = sqlCommand.ExecuteReader();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                //if (sqlReader != null)
                //{
                //    sqlReader.Close();
                //}
                //if (sqlConnection != null)
                //{
                //    sqlConnection.Close();
                //}
            }

            return reader;
        }
    }
}
