﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class UserGroupDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails InsertUserGroup(UserGroupModel UserGroupModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@GroupName",UserGroupModel.UserGroupName) ,
                      new SqlParameter("@Description", UserGroupModel.Description) ,
                      new SqlParameter("@RoleIds", UserGroupModel.RoleIds),                     
                      new SqlParameter("@UserListAccessID",UserGroupModel.UserListAccessID),
                      new SqlParameter("@CompanyIds",UserGroupModel.CompanyIds)
                      //new SqlParameter("@CompanyTypeID",UserGroupModel.CompanyTypeID)
                    };
                int UserGroupId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "[stp_InsertUserGroupDetails]", parameters));
                return new OperationDetails(true, "User Group saved successfully.", null, UserGroupId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, exception.Message, null);
            }
            finally
            {

            }
        }

        public OperationDetails InsertUserGroupEmployees(UserGroupModel UserGroupModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@UserGroupId",UserGroupModel.UserGroupId),
                      new SqlParameter("@EmployeeIds", UserGroupModel.EmployeeIds)
                    };
                int UserGroupId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "[stp_InsertUserGroupEmployees]", parameters));
                return new OperationDetails(true, "User Group Employees saved successfully.", null, UserGroupId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, exception.Message, null);
            }
            finally
            {

            }
        }

        public OperationDetails UpdateUserGroup(UserGroupModel UserGroupModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@UserGroupId",UserGroupModel.UserGroupId) ,
                      new SqlParameter("@GroupName",UserGroupModel.UserGroupName) ,
                      new SqlParameter("@Description",UserGroupModel.Description) ,
                      new SqlParameter("@RoleIds", UserGroupModel.RoleIds) ,                     
                      new SqlParameter("@UserListAccessID",UserGroupModel.UserListAccessID),
                      //new SqlParameter("@CompanyTypeID",UserGroupModel.CompanyTypeID)
                      new SqlParameter("@CompanyIds",UserGroupModel.CompanyIds)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "[stp_UpdateUserGroupDetails]", parameters);
                return new OperationDetails(true, "User Group updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, exception.Message,null);
            }
            finally
            {

            }
        }

        public OperationDetails DeleteUserGroup(int UserGroupId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@UserGroupId",UserGroupId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_DeleteUserGroup", parameters);
                return new OperationDetails(true, "User Group deleted successfully", null);
            }
            catch (Exception ex)
            {
                return new OperationDetails(false, "Error: "+ ex.Message, null);
                throw;
            }
            finally
            {

            }
        }

        public List<UserGroupModel> GetAllUserGroupsList()
        {
            List<UserGroupModel> UserGroupModelList = new List<UserGroupModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllUserGroupsList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    if (reader.HasRows)
                    {
                        UserGroupModel UserGroupModel;
                        while (reader.Read())
                        {
                            UserGroupModel = new UserGroupModel();
                            UserGroupModel.UserGroupId = Convert.ToInt32(reader["UserGroupId"].ToString());
                            UserGroupModel.UserGroupName = reader["GroupName"].ToString();
                            UserGroupModel.Description = reader["Description"].ToString();
                            UserGroupModel.RoleNames = reader["UserRoleNames"].ToString();
                            UserGroupModel.RoleIds = reader["UserRoleIds"].ToString();
                            UserGroupModel.ListAccessName = reader["ListAccessName"].ToString();
                            UserGroupModel.CompanyIds = Convert.ToString(reader["CompanyIds"]);
                            UserGroupModel.CompanyNames = Convert.ToString(reader["CompanyNames"]);
                            UserGroupModel.IsEditable = bool.Parse(reader["IsEditable"].ToString());
                            UserGroupModelList.Add(UserGroupModel);
                        }
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return UserGroupModelList;
        }

        public List<UserGroupModel> GetAllUserGroups()
        {
            List<UserGroupModel> UserGroupModelList = new List<UserGroupModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllUserGroups", sqlConnection);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    if (reader.HasRows)
                    {
                        UserGroupModel UserGroupModel;
                        while (reader.Read())
                        {
                            UserGroupModel = new UserGroupModel();
                            UserGroupModel.UserGroupId = Convert.ToInt32(reader["UserGroupId"].ToString());
                            UserGroupModel.UserGroupName = reader["GroupName"].ToString();
                            UserGroupModel.Description = reader["Description"].ToString();
                            UserGroupModel.RoleNames = reader["UserRoleNames"].ToString();
                            UserGroupModel.RoleIds = reader["UserRoleIds"].ToString();
                            UserGroupModelList.Add(UserGroupModel);
                        }
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return UserGroupModelList;
        }

        public UserGroupModel GetUserGroupById(int UserGroupId)
        {
            UserGroupModel UserGroupModel = new UserGroupModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetUserGroupById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserGroupId", UserGroupId);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            UserGroupModel.UserGroupId = Convert.ToInt32(reader["UserGroupId"].ToString());
                            UserGroupModel.UserGroupName = reader["GroupName"].ToString();
                            UserGroupModel.Description = reader["Description"].ToString();
                            UserGroupModel.RoleNames = reader["UserRoleNames"].ToString();
                            UserGroupModel.RoleIds = reader["UserRoleIds"].ToString();                           
                            UserGroupModel.UserListAccessID = Convert.ToInt32(reader["UserListAccessID"].ToString());
                            UserGroupModel.CompanyIds = Convert.ToString(reader["CompanyIds"].ToString());
                        }
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return UserGroupModel;
        }    

        public List<Employee> GetAllEmployeesByUserGroup(int UserGroupId)
        {
            List<Employee> EmployeeList = new List<Employee>();
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@UserGroupId",UserGroupId)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "[stp_GetAllEmployeesByUserGroup]", parameters))
                {
                    if (reader.HasRows)
                    {
                        Employee employee;
                        while (reader.Read())
                        {
                            employee = new Employee();
                            employee.EmployeeId = Convert.ToInt32(reader["EmployeeID"].ToString());
                            employee.FullName = reader["EmployeeName"].ToString();
                            EmployeeList.Add(employee);
                        }
                    }
                }
                return EmployeeList.Where(x => x.EmployeeId != 0).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }

        public List<Employee> GetAllEmployees()
        {
            List<Employee> EmployeeList = new List<Employee>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "[stp_GetAllEmployees]"))
                {
                    if (reader.HasRows)
                    {
                        Employee employee;
                        while (reader.Read())
                        {
                            employee = new Employee();
                            employee.EmployeeId = Convert.ToInt32(reader["EmployeeID"].ToString());
                            employee.FullName = reader["EmployeeName"].ToString();
                            EmployeeList.Add(employee);
                        }
                    }
                }
                return EmployeeList.Where(x => x.EmployeeId != 0).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }

        public List<UserGroupListAccess> GetUserGroupListAccess()
        {
            List<UserGroupListAccess> _UserGroupListAccessList = new List<UserGroupListAccess>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "[Hr_Stp_GetUserGroupListAccess]"))
                {
                    if (reader.HasRows)
                    {
                        UserGroupListAccess _UserGroupListAccess;
                        while (reader.Read())
                        {
                            _UserGroupListAccess = new UserGroupListAccess();
                            _UserGroupListAccess.UserListAccessID = Convert.ToInt32(reader["UserListAccessID"].ToString());
                            _UserGroupListAccess.ListAccessName = reader["ListAccessName"].ToString();
                            _UserGroupListAccessList.Add(_UserGroupListAccess);
                        }
                    }
                }
                return _UserGroupListAccessList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<CompanyTypeModel> GetCompanyTypeList()
        {
            List<CompanyTypeModel> _CompanyTypeList = new List<CompanyTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "[Hr_Stp_GetCompanyTypeList]"))
                {
                    if (reader.HasRows)
                    {
                        CompanyTypeModel _CompanyTypeModel;
                        while (reader.Read())
                        {
                            _CompanyTypeModel = new CompanyTypeModel();
                            _CompanyTypeModel.CompanyTypeID = Convert.ToInt32(reader["CompanyTypeID"].ToString());
                            _CompanyTypeModel.CompanyTypeName = reader["CompanyTypeName"].ToString();
                            _CompanyTypeList.Add(_CompanyTypeModel);
                        }
                    }
                }
                return _CompanyTypeList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<UserGroupModel> GetAllUserGroupsByUserId(int UserGroupId)
        {
            List<UserGroupModel> UserGroupModelList = new List<UserGroupModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllUserGroupListAccess", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserGroupId);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    if (reader.HasRows)
                    {
                        UserGroupModel UserGroupModel;
                        while (reader.Read())
                        {
                            UserGroupModel = new UserGroupModel();
                            UserGroupModel.UserGroupId = Convert.ToInt32(reader["UserGroupId"].ToString());
                            UserGroupModel.UserGroupName = reader["GroupName"].ToString();
                            UserGroupModel.Description = reader["Description"].ToString();
                            UserGroupModel.UserListAccessID = Convert.ToInt32(reader["UserListAccessID"].ToString());
                            UserGroupModelList.Add(UserGroupModel);
                        }
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return UserGroupModelList;
        }
    }
}
