﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class AreaDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<AreaModel> GetAllAreaByCityId(string CityId)
        {
            List<AreaModel> areaList = new List<AreaModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllAreaByCityId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@CityId", int.Parse(CityId));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    AreaModel areaModel;
                    while (sqlDataReader.Read())
                    {
                        areaModel = new AreaModel();
                        areaModel.AreaId = Convert.ToInt32(sqlDataReader["AreaID"].ToString());
                        areaModel.AreaName = Convert.ToString(sqlDataReader["AreaName_1"]);
                        areaList.Add(areaModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return areaList;
        }
    }
}
