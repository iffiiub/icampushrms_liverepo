﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{

    public class VacationDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        GeneralDB.DataHelper dataHelper;

        public IEnumerable<PickList> GetVactionTypeList()
        {
            try
            {
                List<PickList> pickList = new List<PickList>();
                PickList pickModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GetVacation"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            pickModel = new PickList();
                            pickModel.id = Convert.ToInt32(reader["VactionID"].ToString());
                            pickModel.text = reader["VacationName"].ToString();
                            pickModel.dataFeild1 = Convert.ToBoolean(reader["isCompensate"] == DBNull.Value ? "false" : reader["isCompensate"]) ? "Yes" : "No";
                            pickModel.dataFeild2 = Convert.ToBoolean(reader["AnnualLeave"] == DBNull.Value ? "false" : reader["AnnualLeave"]) ? "Yes" : "No";
                            pickList.Add(pickModel);
                        }
                    }
                }
                return pickList.OrderBy(x => x.text).AsEnumerable();
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public IEnumerable<PickList> GetEmployeeList()
        {
            try
            {
                List<PickList> pickList = new List<PickList>();
                PickList pickModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GetEmployeeList"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            pickModel = new PickList();
                            pickModel.id = Convert.ToInt32(reader["EmployeeID"].ToString());
                            pickModel.text = reader["Name"].ToString();
                            pickList.Add(pickModel);
                        }
                    }
                }
                return pickList.OrderBy(x => x.text).AsEnumerable();
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }
        public IEnumerable<PickList> GetShiftList()
        {
            try
            {
                List<PickList> pickList = new List<PickList>();
                PickList pickModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GetAllShifts"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            pickModel = new PickList();
                            pickModel.id = Convert.ToInt32(reader["Shiftid"].ToString());
                            pickModel.text = reader["Shift"].ToString();
                            pickList.Add(pickModel);
                        }
                    }
                }
                return pickList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public IEnumerable<VacationModel> GetVactionData(int UserId, int? sectionID, int? departmentID, int? ID)
        {
            try
            {
                SqlParameter[] parameters =
                   {
                        new SqlParameter("@UserId",UserId),
                        new SqlParameter("@DepartmentID",departmentID),
                        new SqlParameter("@SectionID",sectionID),
                        new SqlParameter("@ID",ID)
                   };
                List<VacationModel> vacationList = new List<VacationModel>();
                VacationModel vacationModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GetVactionRecord", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationModel = new VacationModel();
                            vacationModel.id = Convert.ToInt32(reader["ID"].ToString());
                            vacationModel.employeeId = Convert.ToInt32(reader["EMPId"].ToString());
                            vacationModel.employeeAlternativeId = reader["EmployeeAlternativeID"].ToString();
                            vacationModel.employeeName = reader["EmpName"].ToString();
                            vacationModel.fromDate = (reader["StartDate"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["StartDate"].ToString()));
                            vacationModel.toDate = (reader["EndDate"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["EndDate"].ToString()));
                            vacationModel.sectionId = Convert.ToInt32(reader["sectionId"].ToString());
                            vacationModel.departmentId = Convert.ToInt32(reader["DepartmentId"].ToString());
                            vacationModel.vacationTypeId = Convert.ToInt32(reader["VacationTypeID"].ToString());
                            vacationModel.vacationType = reader["VacationType"].ToString();
                            vacationList.Add(vacationModel);
                        }
                    }
                }
                return vacationList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<VacationModel> GetEmpUnpaidLeaveData(string FromDate, string ToDate, int IsPaid, int UserId)
        {
            try
            {
                List<VacationModel> vacationList = new List<VacationModel>();
                VacationModel vacationModel;
                DateTime Start = new DateTime();
                DateTime End = new DateTime();
                if (!string.IsNullOrEmpty(FromDate))
                {
                    Start = DateTime.ParseExact(FromDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture);
                    End = DateTime.ParseExact(ToDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture);
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetEmpUnpaidLeave", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (FromDate == "" || ToDate == "")
                {
                    sqlCommand.Parameters.AddWithValue("@StartDate", null);
                    sqlCommand.Parameters.AddWithValue("@EndDate", null);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@StartDate", Start);
                    sqlCommand.Parameters.AddWithValue("@EndDate", End);
                }
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@IsPaid", IsPaid);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        vacationModel = new VacationModel();
                        vacationModel.employeeAlternativeId = reader["Emp ID"].ToString();
                        vacationModel.employeeName = reader["Employee Name"].ToString();
                        vacationModel.vacationType = reader["Leave Type"].ToString();
                        vacationModel.fromDate = (reader["From Date"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["From Date"].ToString()));
                        vacationModel.toDate = (reader["To Date"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["To Date"].ToString()));
                        vacationList.Add(vacationModel);
                    }
                }
                sqlConnection.Close();
                return vacationList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public VacationModel GetVactionModelById(int id)
        {
            VacationModel vactionmodel = new VacationModel();
            IEnumerable<VacationModel> modelList = GetVactionData(0, null, null, id);
            if (modelList.Count() > 0)
            {
                vactionmodel.id = modelList.FirstOrDefault().id;
                vactionmodel.departmentId = modelList.FirstOrDefault().departmentId;
                vactionmodel.employeeId = modelList.FirstOrDefault().employeeId;
                vactionmodel.employeeName = modelList.FirstOrDefault().employeeName;
                vactionmodel.fromDate = modelList.FirstOrDefault().fromDate;
                vactionmodel.toDate = modelList.FirstOrDefault().toDate;
                vactionmodel.sectionId = modelList.FirstOrDefault().sectionId;
                vactionmodel.vacationType = modelList.FirstOrDefault().vacationType;
                vactionmodel.vacationTypeId = modelList.FirstOrDefault().vacationTypeId;
            }
            return vactionmodel;
        }

        public bool UpdateVaction(VacationModel model)
        {
            bool check = false;
            dataHelper = new GeneralDB.DataHelper();
            try
            {
                SqlParameter[] parameters =
                   {
                        new SqlParameter("@VactionID",model.vacationTypeId),
                        new SqlParameter("@StartDate",GeneralDB.CommonDB.SetCulturedDate( model.fromDate)),
                        new SqlParameter("@EndDate",GeneralDB.CommonDB.SetCulturedDate( model.toDate)),
                        new SqlParameter("@EmployeeId",model.employeeId),
                        new SqlParameter("@ID",model.id)
                   };

                var op = SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_stp_UpdateVactionRecord", parameters);
                parameters = null;
                SqlParameter[] parameters1 =
                     {
                        new SqlParameter("@VactionID", model.vacationTypeId),
                   };
                var op1 = SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                   "HR_stp_Upadate_Att_ShiftTableByUser", parameters1);

                string commandtext = "SELECT EmployeeID FROM hr_vacation where ID=" + model.id;

                DataTable dt = dataHelper.ExcuteCommandText(commandtext);
                foreach (DataRow r in dt.Rows)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string query = "UPDATE Att_ShiftTableByUser SET VacationID = " + model.vacationTypeId + "WHERE  EmployeeID=" + r["EmployeeID"].ToString() + " AND AttDate BETWEEN '" + GeneralDB.CommonDB.SetCulturedDate(model.fromDate) + "' AND '" + GeneralDB.CommonDB.SetCulturedDate(model.fromDate) + "'";
                        check = dataHelper.SimpleCommandText(query);
                    }
                }
                check = true;
            }
            catch (Exception exception)
            {
                check = false;
                throw;
            }
            finally
            {

            }
            return check;
        }

        public bool DeleteVaction(int id)
        {
            bool check = false;
            dataHelper = new GeneralDB.DataHelper();

            string commandtext = "HR_stp_DeleteVactionRecord";
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@VacationID", id));
            check = dataHelper.ExcutestoredProcedure(sqlParams, commandtext);
            if (check)
            {
                commandtext = "UPDATE Att_ShiftTableByUser SET VacationID =Null WHERE  VacationID=" + id;
                check = dataHelper.SimpleCommandText(commandtext);
            }
            return check;
        }

        public OperationDetails SaveVationDate(string mode, int id, string fromDate, string toDate, int typeId, int userid)
        {
            OperationDetails op = new OperationDetails();
            // bool chk = false;
            try
            {
                string modetype = "";
                string command = "";
                VacationModel model;
                DataTable dt = new DataTable();
                dataHelper = new GeneralDB.DataHelper();
                DateTime startDate = DateTime.ParseExact(fromDate, CommonDB.HRMSDateFormat, System.Globalization.CultureInfo.InvariantCulture);
                if (mode == "shift")
                {
                    modetype = "@shiftId";
                    command = "HR_stp_GetEmployeeForVactionRecordbyShift";
                }
                if (mode == "department")
                {
                    modetype = "@department";
                    command = "HR_stp_GetEmployeeForVactionRecordbyDepartment";
                }
                if (mode == "section")
                {
                    modetype = "@section";
                    command = "HR_stp_GetEmployeeForVactionRecordbySection";
                }
                if (mode == "employee")
                {
                    model = new VacationModel();
                    model.employeeId = id;
                    model.vacationTypeId = typeId;
                    model.fromDate = fromDate;
                    model.toDate = toDate;
                    AddVaction(model);
                    AddAcumulativeVaction(model);
                    op.Success = true;
                    op.Message = "Leave is saved successfully";
                    op.CssClass = "success";

                }
                else
                {
                    try
                    {
                        SqlParameter[] parameters =
                           {
                        new SqlParameter(modetype,id)
                   };
                        List<VacationModel> vacationList = new List<VacationModel>();
                        using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, command, parameters))
                        {
                            // Check if the reader returned any rows
                            if (reader.HasRows)
                            {
                                dt.Load(reader);
                            }
                        }
                        foreach (DataRow row in dt.Rows)
                        {
                            try
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    if (row["HireDate"].ToString() != "")
                                    {
                                        if (Convert.ToDateTime(row["HireDate"].ToString()) < startDate)
                                        {
                                            model = new VacationModel();
                                            model.employeeId = Convert.ToInt32(row["EmployeeID"].ToString());
                                            model.vacationTypeId = typeId;
                                            model.fromDate = fromDate;
                                            model.toDate = toDate;
                                            AddVaction(model);
                                            AddAcumulativeVaction(model);
                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }
                        op.Success = true;
                        op.Message = "Leave is saved successfully";
                        op.CssClass = "success";
                    }
                    catch (Exception exception)
                    {
                        op.Success = false;
                        op.Message = "Error occurred while saving leave";
                        op.CssClass = "error";
                        throw;
                    }
                    finally
                    {

                    }
                }
            }
            catch
            {
                op.Success = false;
                op.Message = "Error occurred while saving leave";
                op.CssClass = "error";
            }
            return op;
        }

        public string AddVaction(VacationModel vacationmodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_StoreVactionRecord", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", vacationmodel.employeeId);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", vacationmodel.vacationTypeId);
                sqlCommand.Parameters.AddWithValue("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(vacationmodel.fromDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(vacationmodel.toDate));
                sqlCommand.Parameters.AddWithValue("@Status", true);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public string AddAcumulativeVaction(VacationModel vacationmodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_StoreAcumulativeVactionRecord", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", vacationmodel.employeeId);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", vacationmodel.vacationTypeId);
                sqlCommand.Parameters.AddWithValue("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(vacationmodel.fromDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(vacationmodel.toDate));
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public IEnumerable<PickList> GetVactionTypeListAsPerEmployeeId(int EmployeeId)
        {
            try
            {
                List<PickList> pickList = new List<PickList>();
                PickList pickModel;
                SqlParameter[] parameters =
                        {
                        new SqlParameter("@EmployeeID",EmployeeId)
                   };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_GetLeaveTypeAsPerSelectedEmployee", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            pickModel = new PickList();
                            pickModel.id = Convert.ToInt32(reader["id"].ToString());
                            pickModel.text = reader["Name"].ToString();
                            pickModel.dataFeild1 = Convert.ToBoolean(reader["isCompensate"] == DBNull.Value ? "false" : reader["isCompensate"]) ? "Yes" : "No";
                            pickModel.dataFeild2 = Convert.ToBoolean(reader["AnnualLeave"] == DBNull.Value ? "false" : reader["AnnualLeave"]) ? "Yes" : "No";
                            pickList.Add(pickModel);
                        }
                    }
                }
                return pickList.OrderBy(x => x.text).AsEnumerable();
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<VacationModel> GetEmployeeOpeningBalance()
        {
            try
            {

                List<VacationModel> vacationList = new List<VacationModel>();
                VacationModel vacationModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetEmployeeOpeningBalanceDetails"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationModel = new VacationModel();
                            vacationModel.employeeId = Convert.ToInt32(reader["EmployeeID"].ToString());
                            vacationModel.employeeAlternativeId = reader["EmployeeAlternativeID"].ToString();
                            vacationModel.employeeName = reader["EmployeeName"].ToString();
                            vacationModel.vacationTypeId = Convert.ToInt32(reader["VacationTypeID"].ToString());
                            vacationModel.vacationType = reader["Name"].ToString();
                            vacationModel.OpeningBalance = reader["OpeningNumberOfDays"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["OpeningNumberOfDays"]);
                            vacationModel.OpeningBalanceDate = reader["BalanceDate"] == DBNull.Value ? "" : Convert.ToDateTime(reader["BalanceDate"]).ToString(CommonDB.HRMSDateFormat);
                            vacationModel.AccumulativeOBID = reader["AccumulativeOBID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulativeOBID"].ToString());
                            vacationList.Add(vacationModel);
                        }
                    }
                }
                return vacationList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddMultipleAllowance(List<VacationModel> listOpeningBalance)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("OBID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("VacationTypeID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("OpeningBalance");
                resultTable.Columns.Add("EmployeeID", System.Type.GetType("System.Int32"));
                resultTable.Columns.Add("BalanceDate", System.Type.GetType("System.DateTime"));

                foreach (VacationModel vac in listOpeningBalance)
                {
                    resultTable.Rows.Add(vac.AccumulativeOBID,
                                         vac.vacationTypeId,
                                         vac.OpeningBalance,
                                         vac.employeeId,
                                         vac.OpeningBalanceDate
                                         );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_InsertUpdateOpeningBalance", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MultipleOpeningBalce", resultTable);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlParameter Output = new SqlParameter("@output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                operationDetails.Success = true;
                if (Convert.ToInt32(Output.Value) == -1)
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while updating accrual leave balance.";
                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    operationDetails.Message = "Accrual leave balance details updated successfully.";
                }


            }
            catch (Exception ex)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public bool IsLeaveDatesAlreadyExists(int employeeID, string fromDate, string toDate)
        {
            bool IsExists = false;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",employeeID),
                        new SqlParameter("@FromDate",DateTime.ParseExact(fromDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture)),
                        new SqlParameter("@ToDate",DateTime.ParseExact(toDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture))                        
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_IsLeaveDatesAlreadyExists", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IsExists = Convert.ToBoolean(reader["IsExists"].ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsExists;
        }
        public bool IsLeaveDatesAlreadyExistsForCurrentReq(int employeeID, string fromDate, string toDate, int? LeaveRequestID)
        {
            bool IsExists = false;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",employeeID),
                        new SqlParameter("@FromDate",DateTime.ParseExact(fromDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture)),
                        new SqlParameter("@ToDate",DateTime.ParseExact(toDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture)),
                         new SqlParameter("@LeaveRequestID",LeaveRequestID)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_IsLeaveDatesAlreadyExistsForCurrentReq", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IsExists = Convert.ToBoolean(reader["IsExists"].ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsExists;
        }

        public int GetNoOfWorkingDays(int employeeID, string fromDate, string toDate)
        {
            int noOfWorkingDays = 0;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",employeeID),
                        new SqlParameter("@FromDate",DateTime.ParseExact(fromDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture)),
                        new SqlParameter("@ToDate",DateTime.ParseExact(toDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture))
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetNoOfWorkingDays", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            noOfWorkingDays = Convert.ToInt32(reader["NoOfWorkingDays"].ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return noOfWorkingDays;
        }

        public VacationModel GetValidLeaveRequestDates(int employeeID, int vacationTypeID, string fromDate, string toDate)
        {
            try
            {              
                VacationModel vacationModel = new VacationModel();
                SqlParameter[] parameters =
                 {
                        new SqlParameter("@EmployeeID",employeeID),
                        new SqlParameter("@VacationTypeID",vacationTypeID),
                        new SqlParameter("@FromDate",DateTime.ParseExact(fromDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture)),
                        new SqlParameter("@ToDate",DateTime.ParseExact(toDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture))

                  };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetValidLeaveRequestDates",parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationModel = new VacationModel();
                            vacationModel.fromDate = Convert.ToDateTime(reader["FromDate"]).ToString(CommonDB.HRMSDateFormat);
                            vacationModel.toDate = Convert.ToDateTime(reader["ToDate"]).ToString(CommonDB.HRMSDateFormat);
                            vacationModel.NoOfDays = Convert.ToInt32(reader["NoOfLeaveDays"]);
                        }
                    }
                }
                return vacationModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }

        #region VactionType

        public List<VacationTypeModel> GetVacationTypeListWithPaging(int CompanyId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@CompanyID",CompanyId)
                    };
            List<VacationTypeModel> LeaveTypeModelList = new List<VacationTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_STP_Select_HR_VacationType", parameters))
                {
                    if (reader.HasRows)
                    {
                        VacationTypeModel LeaveTypeModel;
                        while (reader.Read())
                        {
                            LeaveTypeModel = new VacationTypeModel();
                            LeaveTypeModel.VacationTypeId = reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"].ToString()) : 0;
                            LeaveTypeModel.Name = reader["Name"].ToString().Trim();
                            //LeaveTypeModel.NoOfDays = reader["NoOfDays"] != DBNull.Value ? Convert.ToInt32(reader["NoOfDays"]) : 0;
                            LeaveTypeModel.FullPaidDays = reader["FullPaidDays"] != DBNull.Value ? Convert.ToInt32(reader["FullPaidDays"]) : 0;
                            LeaveTypeModel.HalfPaidDays = reader["HalfPaidDays"] != DBNull.Value ? Convert.ToInt32(reader["HalfPaidDays"]) : 0;
                            LeaveTypeModel.UnPaidDays = reader["UnPaidDays"] != DBNull.Value ? Convert.ToInt32(reader["UnPaidDays"]) : 0;
                            LeaveTypeModel.RepeatedFor = reader["RepeatedFor"] != DBNull.Value ? Convert.ToInt32(reader["RepeatedFor"]) : 0;
                            LeaveTypeModel.ApplicableAfterDigits = reader["ApplicableAfterDigits"] != DBNull.Value ? Convert.ToInt32(reader["ApplicableAfterDigits"]) : 0;
                            LeaveTypeModel.ApplicableAfterTypeID = reader["ApplicableAfterTypeID"] != DBNull.Value ? Convert.ToInt32(reader["ApplicableAfterTypeID"]) : 0;
                            LeaveTypeModel.GenderName_1 = reader["GenderName_1"].ToString().Trim();
                            LeaveTypeModel.NationalityName_1 = reader["NationalityName_1"].ToString().Trim();
                            LeaveTypeModel.ReligionName_1 = reader["ReligionName_1"].ToString().Trim();
                            // LeaveTypeModel.isCompensate = reader["isCompensate"] != DBNull.Value ? Convert.ToInt32(reader["isCompensate"]) : 0;
                            LeaveTypeModel.AnnualLeave = reader["AnnualLeave"] != DBNull.Value ? Convert.ToInt32(reader["AnnualLeave"]) : 0;
                            LeaveTypeModel.PerServiceVacation = reader["PerServiceVacation"] != DBNull.Value ? Convert.ToInt32(reader["PerServiceVacation"]) : 0;
                            LeaveTypeModel.MonthlySplit = reader["MonthlySplit"] != DBNull.Value ? Convert.ToInt32(reader["MonthlySplit"]) : 0;
                            LeaveTypeModel.PaidType = Convert.ToString(reader["PaidType"]).Trim();
                            LeaveTypeModel.PerServiceVacation = reader["Term"] != DBNull.Value ? Convert.ToInt32(reader["Term"]) : 0;
                            LeaveTypeModel.VTCategoryID = reader["VTCategoryID"] != DBNull.Value ? Convert.ToInt32(reader["VTCategoryID"]) : 0;
                           // LeaveTypeModel.LeaveEntitleTypeID = reader["LeaveEntitleTypeID"] != DBNull.Value ? Convert.ToInt32(reader["LeaveEntitleTypeID"]) : 0;
                            LeaveTypeModel.AccumulativeVacationID = reader["AccumulativeVacationID"] != DBNull.Value ? Convert.ToInt32(reader["AccumulativeVacationID"]) : 0;                            
                            LeaveTypeModelList.Add(LeaveTypeModel);
                        }
                    }
                }
                return LeaveTypeModelList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public VacationTypeModel GetVacationTypeById(int LeaveTypeId)
        {
            try
            {
                VacationTypeModel vacationTypeModel = new VacationTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_STP_Get_HR_VacationTypeByID", new SqlParameter("@VacationTypeID", LeaveTypeId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationTypeModel.VacationTypeId = reader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Id"]);
                            vacationTypeModel.Name = Convert.ToString(reader["Name"]);                     
                            vacationTypeModel.FullPaidDays = reader["FullPaidDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["FullPaidDays"]);
                            vacationTypeModel.HalfPaidDays = reader["HalfPaidDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["HalfPaidDays"]);
                            vacationTypeModel.UnPaidDays = reader["UnPaidDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UnPaidDays"]);                        
                            vacationTypeModel.TotalNoOfDays = reader["TotalNoOfDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalNoOfDays"]);
                            vacationTypeModel.RepeatedFor = reader["RepeatedFor"] == DBNull.Value ? 0 : Convert.ToInt32(reader["RepeatedFor"]);
                            vacationTypeModel.ApplicableAfterDigits = reader["ApplicableAfterDigits"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ApplicableAfterDigits"]);
                            vacationTypeModel.ApplicableAfterTypeID = reader["ApplicableAfterTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ApplicableAfterTypeID"]);
                            vacationTypeModel.GenderID = reader["GenderID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["GenderID"]);
                            vacationTypeModel.NationalityID = reader["NationalityID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["NationalityID"]);
                            vacationTypeModel.ReligionID = reader["ReligionID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ReligionID"]);
                            //vacationTypeModel.isCompensate = reader["isCompensate"] == DBNull.Value ? 0 : Convert.ToInt32(reader["isCompensate"]);
                            vacationTypeModel.NationalityName_1 = reader["NationalityName_1"].ToString();
                            vacationTypeModel.ReligionName_1 = reader["ReligionName_1"].ToString();
                            vacationTypeModel.GenderName_1 = reader["GenderName_1"].ToString();
                            vacationTypeModel.AnnualLeave = reader["AnnualLeave"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AnnualLeave"]);
                            vacationTypeModel.PerServiceVacation = reader["PerServiceVacation"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PerServiceVacation"]);
                            vacationTypeModel.MonthlySplit = reader["MonthlySplit"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MonthlySplit"]);
                            vacationTypeModel.MaritalStatusId = reader["MaritalStatusId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MaritalStatusId"]);
                            vacationTypeModel.VTCategoryID = reader["VTCategoryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["VTCategoryId"]);
                            //vacationTypeModel.LeaveEntitleTypeID = reader["LeaveEntitleTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LeaveEntitleTypeID"]);
                            vacationTypeModel.isAnualLeave = Convert.ToBoolean(reader["AnnualAndGratuityLeave"]);
                            if (reader["AccumulativeVacationID"] != DBNull.Value)
                            {
                                bool isDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? "false" : reader["IsDeleted"]);
                                if (!isDeleted)
                                {
                                    vacationTypeModel.AccumulativeVacationID = reader["AccumulativeVacationID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulativeVacationID"]);
                                    vacationTypeModel.isAccumulatedLeave = true;
                                }
                            }
                            vacationTypeModel.AccMaxNumberOfDays = reader["MaxNumberOfDays"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MaxNumberOfDays"]);
                            vacationTypeModel.AccumulatePeriodNumber = reader["AccumulatePeriodNumber"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulatePeriodNumber"]);
                            vacationTypeModel.AccumulateTimeSpanId = reader["AccumulatePeriod"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulatePeriod"]);
                            vacationTypeModel.AccumulateNumber = reader["AccumulateNumber"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["AccumulateNumber"]);
                            vacationTypeModel.AccumulateRoundingTypeId = reader["AccumulativeDaysRounding"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulativeDaysRounding"]);
                            vacationTypeModel.AccumulateAfter = reader["AccumulateAfter"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulateAfter"]);
                            vacationTypeModel.AccumulateAfterPeriod = reader["AccumulateAfterPeriod"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulateAfterPeriod"]);
                            vacationTypeModel.MaxNoOfDaystocarryForword = reader["MaxNoOfDaystocarryForword"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MaxNoOfDaystocarryForword"]);
                            vacationTypeModel.DisplayLeaveBalanceTypeId = reader["BalanceDigitAfterDecimal"] == DBNull.Value ? 0 : Convert.ToInt32(reader["BalanceDigitAfterDecimal"]);
                            vacationTypeModel.AccumulationStartFrom = reader["AccumulationStartDate"] == DBNull.Value ? "" : Convert.ToDateTime(reader["AccumulationStartDate"].ToString()).ToString(CommonDB.HRMSDateFormat);
                            vacationTypeModel.AccumulationStart = reader["AccumulationStart"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AccumulationStart"]);
                        }
                    }
                }
                return vacationTypeModel;
                // Finally, we return Position

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public string InsertVacationType(VacationTypeModel VacationTypeModel, string positionIds, string deparmentids, string jobTitleIds, string jobCategoryIds)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_Add_HR_VacationType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Name", VacationTypeModel.Name);
                //sqlCommand.Parameters.AddWithValue("@NoOfDays", VacationTypeModel.NoOfDays);
                sqlCommand.Parameters.AddWithValue("@FullPaidDays", VacationTypeModel.FullPaidDays);
                sqlCommand.Parameters.AddWithValue("@HalfPaidDays", VacationTypeModel.HalfPaidDays);
                sqlCommand.Parameters.AddWithValue("@UnPaidDays", VacationTypeModel.UnPaidDays);
                sqlCommand.Parameters.AddWithValue("@RepeatedFor", VacationTypeModel.RepeatedFor);
                sqlCommand.Parameters.AddWithValue("@ApplicableAfterDigits", VacationTypeModel.ApplicableAfterDigits);
                sqlCommand.Parameters.AddWithValue("@ApplicableAfterTypeID", VacationTypeModel.ApplicableAfterTypeID);
                sqlCommand.Parameters.AddWithValue("@GenderID", VacationTypeModel.GenderID);
                sqlCommand.Parameters.AddWithValue("@NationalityID", VacationTypeModel.NationalityID);
                sqlCommand.Parameters.AddWithValue("@ReligionID", VacationTypeModel.ReligionID);
                //sqlCommand.Parameters.AddWithValue("@isCompensate", VacationTypeModel.isCompensate);
                sqlCommand.Parameters.AddWithValue("@isDeleted", 0);
                sqlCommand.Parameters.AddWithValue("@AnnualLeave", VacationTypeModel.AnnualLeave);
                sqlCommand.Parameters.AddWithValue("@PerServiceVacation", VacationTypeModel.PerServiceVacation);
                sqlCommand.Parameters.AddWithValue("@MonthlySplit", VacationTypeModel.MonthlySplit);
                sqlCommand.Parameters.AddWithValue("@MaritalStatusId", VacationTypeModel.MaritalStatusId);
                sqlCommand.Parameters.AddWithValue("@CompanyID", VacationTypeModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@isAccumulatedLeave", VacationTypeModel.isAccumulatedLeave);
                sqlCommand.Parameters.AddWithValue("@MaxNoOfDaystocarryForword", VacationTypeModel.MaxNoOfDaystocarryForword);
                sqlCommand.Parameters.AddWithValue("@DisplayLeaveBalanceTypeId", VacationTypeModel.DisplayLeaveBalanceTypeId);
                sqlCommand.Parameters.AddWithValue("@AccMaxNumberOfDays", VacationTypeModel.AccMaxNumberOfDays);
                sqlCommand.Parameters.AddWithValue("@AccumulativeDaysRounding", VacationTypeModel.AccumulateRoundingTypeId);
                sqlCommand.Parameters.AddWithValue("@AccumulatePeriodNumber", VacationTypeModel.AccumulatePeriodNumber);
                sqlCommand.Parameters.AddWithValue("@AccumulatePeroid", VacationTypeModel.AccumulateTimeSpanId);
                sqlCommand.Parameters.AddWithValue("@AccumulateNumber", VacationTypeModel.AccumulateNumber);
                sqlCommand.Parameters.AddWithValue("@AccumulateAfter", VacationTypeModel.AccumulateAfter);
                sqlCommand.Parameters.AddWithValue("@AccumulateAfterPeriod", VacationTypeModel.AccumulateAfterPeriod);
                sqlCommand.Parameters.AddWithValue("@positionIds", positionIds);
                sqlCommand.Parameters.AddWithValue("@departmentIds", deparmentids);
                sqlCommand.Parameters.AddWithValue("@jobTitleIds", jobTitleIds);
                sqlCommand.Parameters.AddWithValue("@jobcategoryIds", jobCategoryIds);
                sqlCommand.Parameters.AddWithValue("@isAnual", VacationTypeModel.isAnualLeave);
                sqlCommand.Parameters.AddWithValue("@VTCategoryID", VacationTypeModel.VTCategoryID == null?(object)DBNull.Value : VacationTypeModel.VTCategoryID==0? (object)DBNull.Value: VacationTypeModel.VTCategoryID);
               // sqlCommand.Parameters.AddWithValue("@LeaveEntitleTypeID", VacationTypeModel.LeaveEntitleTypeID);
                
                if (!string.IsNullOrEmpty(VacationTypeModel.AccumulationStartFrom))
                {
                    sqlCommand.Parameters.AddWithValue("@AccumulationStartDate", DateTime.ParseExact(VacationTypeModel.AccumulationStartFrom, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture));
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@AccumulationStartDate", DBNull.Value);
                }
                sqlCommand.Parameters.AddWithValue("@AccumulativeStart", VacationTypeModel.AccumulationStart == null ? 3 : VacationTypeModel.AccumulationStart);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                sqlCommand.ExecuteNonQuery();

                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return Message;
        }

        public bool isAvailableVacationType(VacationTypeModel VacationTypeModel, int mode)
        {
            bool isAvailable = true;
            int count = 0;
            string VacName = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select COUNT(id) as VacCount from HR_VacationType where Name like '" + VacationTypeModel.Name.Trim() + "'", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                //sqlCommand.Parameters.AddWithValue("@Name", VacationTypeModel.Name);
                SqlDataReader dr = sqlCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        count = dr["VacCount"] != DBNull.Value ? Convert.ToInt32(dr["VacCount"]) : 0;
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            if (mode == 2)
            {
                try
                {
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    sqlCommand = new SqlCommand("select Name from HR_VacationType where id = '" + VacationTypeModel.VacationTypeId + "'", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.Text;
                    //sqlCommand.Parameters.AddWithValue("@Name", VacationTypeModel.Name);
                    SqlDataReader dr = sqlCommand.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            VacName = dr["Name"].ToString();
                        }
                    }
                    sqlConnection.Close();


                }
                catch (Exception exception)
                {
                    throw exception;
                    //throw;
                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }
                }

            }

            if (count > 0)
            {
                isAvailable = true;
            }
            else
            {
                isAvailable = false;
            }

            if (VacName == VacationTypeModel.Name && mode == 2)
            {
                isAvailable = false;
            }
            return isAvailable;
        }
        public string UpdateLeaveType(VacationTypeModel vacationTypeModel, string positionIds, string deparmentids, string jobTitleIds, string jobCategoryIds)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_Update_HR_VacationType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@VacationTypeId", vacationTypeModel.VacationTypeId);
                sqlCommand.Parameters.AddWithValue("@Name", vacationTypeModel.Name);
                //  sqlCommand.Parameters.AddWithValue("@NoOfDays", vacationTypeModel.NoOfDays);
                sqlCommand.Parameters.AddWithValue("@FullPaidDays", vacationTypeModel.FullPaidDays);
                sqlCommand.Parameters.AddWithValue("@HalfPaidDays", vacationTypeModel.HalfPaidDays);
                sqlCommand.Parameters.AddWithValue("@UnPaidDays", vacationTypeModel.UnPaidDays);
                sqlCommand.Parameters.AddWithValue("@RepeatedFor", vacationTypeModel.RepeatedFor);
                sqlCommand.Parameters.AddWithValue("@ApplicableAfterDigits", vacationTypeModel.ApplicableAfterDigits);
                sqlCommand.Parameters.AddWithValue("@ApplicableAfterTypeID", vacationTypeModel.ApplicableAfterTypeID);
                sqlCommand.Parameters.AddWithValue("@GenderID", vacationTypeModel.GenderID);
                sqlCommand.Parameters.AddWithValue("@NationalityID", vacationTypeModel.NationalityID);
                sqlCommand.Parameters.AddWithValue("@ReligionID", vacationTypeModel.ReligionID);
                //  sqlCommand.Parameters.AddWithValue("@isCompensate", vacationTypeModel.isCompensate);
                sqlCommand.Parameters.AddWithValue("@AnnualLeave", vacationTypeModel.AnnualLeave);
                sqlCommand.Parameters.AddWithValue("@PerServiceVacation", vacationTypeModel.PerServiceVacation);
                sqlCommand.Parameters.AddWithValue("@MonthlySplit", vacationTypeModel.MonthlySplit);
                sqlCommand.Parameters.AddWithValue("@MaritalStatusId", vacationTypeModel.MaritalStatusId);
                sqlCommand.Parameters.AddWithValue("@positionIds", positionIds);
                sqlCommand.Parameters.AddWithValue("@departmentIds", deparmentids);
                sqlCommand.Parameters.AddWithValue("@jobTitleIds", jobTitleIds);
                sqlCommand.Parameters.AddWithValue("@jobcategoryIds", jobCategoryIds);
                sqlCommand.Parameters.AddWithValue("@AccumulativeVacationID", vacationTypeModel.AccumulativeVacationID);
                sqlCommand.Parameters.AddWithValue("@isAccumulatedLeave", vacationTypeModel.isAccumulatedLeave);
                sqlCommand.Parameters.AddWithValue("@MaxNoOfDaystocarryForword", vacationTypeModel.MaxNoOfDaystocarryForword);
                sqlCommand.Parameters.AddWithValue("@DisplayLeaveBalanceTypeId", vacationTypeModel.DisplayLeaveBalanceTypeId);
                sqlCommand.Parameters.AddWithValue("@AccMaxNumberOfDays", vacationTypeModel.AccMaxNumberOfDays);
                sqlCommand.Parameters.AddWithValue("@AccumulativeDaysRounding", vacationTypeModel.AccumulateRoundingTypeId);
                sqlCommand.Parameters.AddWithValue("@AccumulatePeriodNumber", vacationTypeModel.AccumulatePeriodNumber);
                sqlCommand.Parameters.AddWithValue("@AccumulatePeroid", vacationTypeModel.AccumulateTimeSpanId);
                sqlCommand.Parameters.AddWithValue("@AccumulateNumber", vacationTypeModel.AccumulateNumber);
                sqlCommand.Parameters.AddWithValue("@AccumulateAfter", vacationTypeModel.AccumulateAfter);
                sqlCommand.Parameters.AddWithValue("@AccumulateAfterPeriod", vacationTypeModel.AccumulateAfterPeriod);
                sqlCommand.Parameters.AddWithValue("@isAnual", vacationTypeModel.isAnualLeave);
                sqlCommand.Parameters.AddWithValue("@VTCategoryID", vacationTypeModel.VTCategoryID);
               // sqlCommand.Parameters.AddWithValue("@LeaveEntitleTypeID", vacationTypeModel.LeaveEntitleTypeID);
                if (!string.IsNullOrEmpty(vacationTypeModel.AccumulationStartFrom))
                {
                    sqlCommand.Parameters.AddWithValue("@AccumulationStartDate", DateTime.ParseExact(vacationTypeModel.AccumulationStartFrom, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture));
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@AccumulationStartDate", DBNull.Value);
                }
                sqlCommand.Parameters.AddWithValue("@Accumulationstart", vacationTypeModel.AccumulationStart == null ? 3 : vacationTypeModel.AccumulationStart);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return Message;
        }

        public OperationDetails DeleteVacationTypeById(int VacationTypeId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@VacationTypeId",VacationTypeId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_STP_Delete_HR_VacationType", parameters);
                return new OperationDetails(true, "Success", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting LeaveType.", exception);
                throw;
            }
            finally
            {

            }
        }

        public void VacationRules(string mode, int id, string fromDate, string toDate, int typeId)
        {

        }
        public void checkAnualLeave(int employeeId, int vacationId)
        {
            bool isAnnual = false;
            string allowanceIds = "";
            DataAccess.GeneralDB.DataHelper dataHelper = new GeneralDB.DataHelper();
            string query = "SELECT * FROM HR_VacationType WHERE id =" + vacationId;
            DataTable dt = dataHelper.ExcuteCommandText(query);
            foreach (DataRow dr in dt.Rows)
            {
                isAnnual = Convert.ToBoolean(dr[""].ToString() != "" ? dr[""].ToString() : "false");
            }
        }

        public List<VactionPositionModel> getPositionByVactionId(int vacationId)
        {
            DataHelper dataHelper = new DataHelper();
            DataTable dt = dataHelper.ExcuteCommandText("SELECT VP.*,HP.PositionTitle  FROM HR_VacationType_Position VP INNER JOIN HR_Position HP ON VP.PositionId = HP.PositionID WHERE VP.VacationtypeId = " + vacationId);
            List<VactionPositionModel> vacationpostionList = new List<VactionPositionModel>();
            VactionPositionModel iVacationpostion;
            foreach (DataRow dr in dt.Rows)
            {
                iVacationpostion = new VactionPositionModel();
                iVacationpostion.positionId = Convert.ToInt32(dr["PositionId"].ToString());
                iVacationpostion.vacationtypeId = Convert.ToInt32(dr["VacationtypeId"].ToString());
                iVacationpostion.vacationPositionId = Convert.ToInt32(dr["VacationtypepositionId"].ToString());
                iVacationpostion.positionName = dr["PositionTitle"].ToString();
                vacationpostionList.Add(iVacationpostion);
            }
            return vacationpostionList;
        }

        public List<VacationDepartmentModel> getDepartmentByVactionId(int vacationId)
        {
            DataHelper dataHelper = new DataHelper();
            DataTable dt = dataHelper.ExcuteCommandText("SELECT VD.*, HD.DepartmentName_1  FROM HR_VacationType_Department VD INNER JOIN HR_Department HD ON VD.DepartmentID = HD.DepartmentID  WHERE VD.VacationTypeId = " + vacationId);
            List<VacationDepartmentModel> vacationdepartmentList = new List<VacationDepartmentModel>();
            VacationDepartmentModel iVacationdepartment;
            foreach (DataRow dr in dt.Rows)
            {
                iVacationdepartment = new VacationDepartmentModel();
                iVacationdepartment.departmentId = Convert.ToInt32(dr["DepartmentId"].ToString());
                iVacationdepartment.vacationtypeId = Convert.ToInt32(dr["VacationtypeId"].ToString());
                iVacationdepartment.vacationDepartmentId = Convert.ToInt32(dr["VacationTypeDepartmentId"].ToString());
                iVacationdepartment.departmentName = dr["DepartmentName_1"].ToString();
                vacationdepartmentList.Add(iVacationdepartment);
            }
            return vacationdepartmentList;
        }

        public List<VacationjobTitleModel> getjobTitleByVactionId(int vacationId)
        {
            DataHelper dataHelper = new DataHelper();
            DataTable dt = dataHelper.ExcuteCommandText("SELECT VJ.*, HJ.JobTitleName_1  FROM Hr_VacationType_JobTitle VJ INNER JOIN HR_JobTitle HJ ON VJ.JobTitleId = HJ.jobTitleID WHERE VJ.VacationTypeId= " + vacationId);
            List<VacationjobTitleModel> vacationjobtitleList = new List<VacationjobTitleModel>();
            VacationjobTitleModel iVacationjobtitle;
            foreach (DataRow dr in dt.Rows)
            {
                iVacationjobtitle = new VacationjobTitleModel();
                iVacationjobtitle.jobTitleId = Convert.ToInt32(dr["JobTitleId"].ToString());
                iVacationjobtitle.vacationtypeId = Convert.ToInt32(dr["VacationtypeId"].ToString());
                iVacationjobtitle.vacationJobTitletId = Convert.ToInt32(dr["VacationJobTitleId"].ToString());
                iVacationjobtitle.jobTitleName = dr["JobTitleName_1"].ToString();
                vacationjobtitleList.Add(iVacationjobtitle);
            }
            return vacationjobtitleList;
        }

        public List<VacationJobCategoryModel> getJobCategoryByVactionId(int vacationId)
        {
            DataHelper dataHelper = new DataHelper();
            DataTable dt = dataHelper.ExcuteCommandText("SELECT VJ.*,EJ.EmployeeJobCategoryName_1  FROM HR_VacationType_JobCategory VJ INNER JOIN GEN_EmployeeJobCategory EJ ON VJ.JobCategoryId = EJ.EmployeeJobCategoryID WHERE VJ.VacationTypeId=  " + vacationId);
            List<VacationJobCategoryModel> vacationjobcategoryList = new List<VacationJobCategoryModel>();
            VacationJobCategoryModel iVacationjobcategory;
            foreach (DataRow dr in dt.Rows)
            {
                iVacationjobcategory = new VacationJobCategoryModel();
                iVacationjobcategory.jobCategoryId = Convert.ToInt32(dr["JobCategoryId"].ToString());
                iVacationjobcategory.vacationtypeId = Convert.ToInt32(dr["VacationTypeId"].ToString());
                iVacationjobcategory.vacationJobCategoryId = Convert.ToInt32(dr["VacationTypeJobCategoryId"].ToString());
                iVacationjobcategory.jobCategoryName = dr["EmployeeJobCategoryName_1"].ToString();
                vacationjobcategoryList.Add(iVacationjobcategory);
            }
            return vacationjobcategoryList;
        }

        public IEnumerable<VacationModel> GetVacationForAnnualAndPerservice(int mode, int employeeId, int vacationTypeId)
        {
            dataHelper = new DataHelper();
            List<VacationModel> VacationModelList = new List<VacationModel>();
            VacationModel iVacationModel;
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@employeeId", employeeId));
            sqlParams.Add(new SqlParameter("@mode", mode));
            sqlParams.Add(new SqlParameter("@vacationTypeId", vacationTypeId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParams, "Hr_Stp_GetVacationDataByAnnualOrPerService");
            foreach (DataRow dr in dt.Rows)
            {
                iVacationModel = new VacationModel();
                iVacationModel.employeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                iVacationModel.vacationTypeId = Convert.ToInt32(dr["VacationTypeID"].ToString());
                iVacationModel.fromDate = dr["StartDate"].ToString();
                iVacationModel.toDate = dr["EndDate"].ToString();
                VacationModelList.Add(iVacationModel);
            }
            return VacationModelList.AsEnumerable();
        }

        public IEnumerable<VactionEmoloyeeInformationModel> GetEmployeeInformationForVacation(int mode, int typeId)
        {
            List<VactionEmoloyeeInformationModel> employeeInformationList = null;
            try
            {
                employeeInformationList = new List<VactionEmoloyeeInformationModel>();
                DataHelper dataHelper = new DataHelper();
                List<SqlParameter> sqlparms = new List<SqlParameter>();
                sqlparms.Add(new SqlParameter("@mode", mode));
                sqlparms.Add(new SqlParameter("@typeId", typeId));
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlparms, "Stp_GetEmployeeInformationForVacation");
                foreach (DataRow dr in dt.Rows)
                {
                    VactionEmoloyeeInformationModel employeeInformation = new VactionEmoloyeeInformationModel();
                    employeeInformation.employeeId = Convert.ToInt32(dr["EmployeeID"].ToString());
                    employeeInformation.employeeName = dr["EmployeeName"].ToString();
                    employeeInformation.hiredate = Convert.ToString(dr["HireDate"] == DBNull.Value ? "" : dr["HireDate"]);
                    employeeInformation.genderId = Convert.ToInt32(dr["GenderID"] == DBNull.Value ? "0" : dr["GenderID"].ToString());
                    employeeInformation.religionId = Convert.ToInt32(dr["ReligionID"] == DBNull.Value ? "0" : dr["ReligionID"].ToString());
                    employeeInformation.positionId = Convert.ToInt32(dr["PositionID"] == DBNull.Value ? "0" : dr["PositionID"].ToString());
                    employeeInformation.maritalStatusId = Convert.ToInt32(dr["MaritalStatusID"] == DBNull.Value ? "0" : dr["MaritalStatusID"].ToString());
                    employeeInformation.nationalityId = Convert.ToInt32(dr["DefaultNationalityID"] == DBNull.Value ? "0" : dr["DefaultNationalityID"].ToString());
                    employeeInformation.departmentId = Convert.ToInt32(dr["DepartmentID"] == DBNull.Value ? "0" : dr["DepartmentID"].ToString());
                    employeeInformation.shiftId = Convert.ToInt32(dr["ShiftID"] == DBNull.Value ? "0" : dr["ShiftID"].ToString());
                    employeeInformation.sectionId = Convert.ToInt32(dr["EmployeeSectionID"] == DBNull.Value ? "0" : dr["EmployeeSectionID"].ToString());
                    employeeInformation.JobCategoryId = Convert.ToInt32(dr["EmployeeJobCategoryID"] == DBNull.Value ? "0" : dr["EmployeeJobCategoryID"].ToString());
                    employeeInformation.JobTitleId = Convert.ToInt32(dr["JobTitleID"] == DBNull.Value ? "0" : dr["JobTitleID"].ToString());
                    employeeInformationList.Add(employeeInformation);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeInformationList.OrderBy(x => x.employeeName).AsEnumerable();
        }

        public float GetAcumulatedLeavesofEmployeeByVacationType(int employeeId, int vacationTypeId)
        {
            float totalAccVacDaysCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAcLeavesofEmployeeByVacType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //sqlCommand.Parameters.AddWithValue("@Name", VacationTypeModel.Name);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", vacationTypeId);
                SqlParameter TotalAccvacDays = new SqlParameter("@TotalAccvacDays", SqlDbType.Float);
                TotalAccvacDays.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalAccvacDays);
                sqlCommand.ExecuteNonQuery();
                totalAccVacDaysCount = float.Parse(TotalAccvacDays.Value == DBNull.Value ? "0" : TotalAccvacDays.Value.ToString());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return totalAccVacDaysCount;
        }

        public string GetHireDate(int employeeId)
        {
            string HireDate = "";
            try
            {
                dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("SELECT CONVERT(NVARCHAR,HireDate,103) AS HireDate FROM Hr_EmploymentInformation WHERE EmployeeID=" + employeeId);
                List<VactionPositionModel> vacationpostionList = new List<VactionPositionModel>();
                foreach (DataRow dr in dt.Rows)
                {
                    HireDate = dr["HireDate"].ToString() == "" ? "0" : dr["HireDate"].ToString();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return HireDate;
        }

        public float GetAlreadyTakenLeaveDays(int employeeId, int vacationTypeId, DateTime HireDate, int isAnual)
        {
            float totalAccVacDaysCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_GetAlreadyTakenVacationsPerYear", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", vacationTypeId);
                sqlCommand.Parameters.AddWithValue("@HireDate", HireDate);
                sqlCommand.Parameters.AddWithValue("@isAnual", isAnual);
                SqlParameter NoOfDaysTaken = new SqlParameter("@NoOfDaysTaken", SqlDbType.Float);
                NoOfDaysTaken.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(NoOfDaysTaken);
                sqlCommand.ExecuteNonQuery();
                totalAccVacDaysCount = float.Parse(NoOfDaysTaken.Value == DBNull.Value ? "0" : NoOfDaysTaken.Value.ToString());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return totalAccVacDaysCount;
        }
        public float GetAlreadyTakenLeaveDays(int employeeId, int vacationTypeId, int isAnual)
        {
            float totalAccVacDaysCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_GetAlreadyTakenVacationsPerYear", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", vacationTypeId);
                //sqlCommand.Parameters.AddWithValue("@HireDate", HireDate);
                sqlCommand.Parameters.AddWithValue("@isAnual", isAnual);
                SqlParameter NoOfDaysTaken = new SqlParameter("@NoOfDaysTaken", SqlDbType.Float);
                NoOfDaysTaken.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(NoOfDaysTaken);
                sqlCommand.ExecuteNonQuery();
                totalAccVacDaysCount = float.Parse(NoOfDaysTaken.Value == DBNull.Value ? "0" : NoOfDaysTaken.Value.ToString());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return totalAccVacDaysCount;
        }
        public bool CheckIsAnyLeaveGratuityAnnual(int vacationTypeId)
        {
            bool isAnnual = false;
            int VacId = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT id FROM Hr_VacationType WHERE AnnualAndGratuityLeave=1", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        VacId = Convert.ToInt32(sqlDataReader["id"]);
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            if (VacId == 0)
            {
                isAnnual = false;
            }
            else
            {
                if (VacId == vacationTypeId)
                {
                    isAnnual = false;
                }
                else
                {
                    isAnnual = true;
                }
            }
            return isAnnual;
        }

        public bool CheckIfCountSalaryForWorkDays()
        {
            bool isCountSalaryForWorkingDays = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select CountSalaryForWorkDays from Hr_payrollsettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        isCountSalaryForWorkingDays = Convert.ToBoolean(sqlDataReader["CountSalaryForWorkDays"] == DBNull.Value ? "False" : sqlDataReader["CountSalaryForWorkDays"].ToString());
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return isCountSalaryForWorkingDays;
        }

        public IEnumerable<VacationModel> GetPortalVacationRequests()
        {
            try
            {
                List<VacationModel> vacationList = new List<VacationModel>();
                VacationModel vacationModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GetPortalLeaveRequest"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationModel = new VacationModel();
                            vacationModel.id = Convert.ToInt32(reader["requestID"].ToString());
                            vacationModel.employeeId = Convert.ToInt32(reader["teacherID"].ToString());
                            vacationModel.employeeAlternativeId = reader["EmployeeAlternativeID"].ToString();
                            vacationModel.employeeName = reader["EmpName"].ToString();
                            vacationModel.fromDate = (reader["start_date"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["start_date"].ToString()));
                            vacationModel.toDate = (reader["end_date"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["end_date"].ToString()));
                            vacationModel.vacationTypeId = Convert.ToInt32(reader["categoryID"].ToString());
                            vacationModel.vacationType = reader["VacationType"].ToString();
                            vacationModel.NoOfDays = Convert.ToInt32(reader["no_days"].ToString());
                            vacationModel.Reason = reader["reason"].ToString();
                            vacationModel.StatusId = Convert.ToInt32(reader["StatusID"] == DBNull.Value ? "0" : reader["StatusID"].ToString());
                            vacationModel.Status = Convert.ToString(reader["Status"].ToString());
                            vacationModel.Comments = Convert.ToString(reader["Comments"].ToString());
                            vacationModel.RejectReason = Convert.ToString(reader["reject_reason"].ToString());
                            vacationModel.MonitorId = Convert.ToInt32(reader["monitorID"] == DBNull.Value ? "0" : reader["monitorID"].ToString());
                            vacationModel.CreatedDate = (reader["createddate"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["createddate"].ToString()));
                            vacationList.Add(vacationModel);
                        }
                    }
                }
                return vacationList;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public VacationModel GetPortalVacationRequestByID(int RequestID)
        {
            try
            {
                SqlParameter[] parameters =
                 {
                        new SqlParameter("@ReqId",RequestID),
                   };

                VacationModel vacationModel = new VacationModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_stp_GetPortalLeaveRequestByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationModel = new VacationModel();
                            vacationModel.id = Convert.ToInt32(reader["requestID"].ToString());
                            vacationModel.employeeId = Convert.ToInt32(reader["teacherID"].ToString());
                            vacationModel.employeeAlternativeId = reader["EmployeeAlternativeID"].ToString();
                            vacationModel.employeeName = reader["EmpName"].ToString();
                            vacationModel.fromDate = (reader["start_date"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["start_date"].ToString()));
                            vacationModel.toDate = (reader["end_date"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["end_date"].ToString()));
                            vacationModel.vacationTypeId = Convert.ToInt32(reader["categoryID"].ToString());
                            vacationModel.vacationType = reader["VacationType"].ToString();
                            vacationModel.NoOfDays = Convert.ToInt32(reader["no_days"].ToString());
                            vacationModel.Reason = reader["reason"].ToString();
                            vacationModel.StatusId = Convert.ToInt32(reader["StatusID"] == DBNull.Value ? "0" : reader["StatusID"].ToString());
                            vacationModel.Status = Convert.ToString(reader["Status"].ToString());
                            vacationModel.Comments = Convert.ToString(reader["Comments"].ToString());
                            vacationModel.RejectReason = Convert.ToString(reader["reject_reason"].ToString());
                            vacationModel.MonitorId = Convert.ToInt32(reader["monitorID"] == DBNull.Value ? "0" : reader["monitorID"].ToString());
                            vacationModel.CreatedDate = (reader["createddate"] == DBNull.Value ? DateTime.Now.ToString() : GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["createddate"].ToString()));

                        }
                    }
                }
                return vacationModel;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdatePortalLeaveRequestStatus(int requestID, int statusID)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_UpdatePortalLeaveRequestStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ReqId", requestID);
                sqlCommand.Parameters.AddWithValue("@StatusID", statusID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;

        }

        public OperationDetails UpdatePortalLeaveRequestStatus(VacationModel vc, int userID)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_UpdatePortalLeaveRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ReqId", vc.id);
                sqlCommand.Parameters.AddWithValue("@StatusID", vc.StatusId);
                sqlCommand.Parameters.AddWithValue("@RejectReason", vc.RejectReason);
                sqlCommand.Parameters.AddWithValue("@Comments", vc.Comments);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = true;
                op.Message = "Portal leave request rejected.";
                op.CssClass = "success";
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = exception.Message;
                op.CssClass = "error";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;

        }

        public VacationTypeModel GetAccrualVacationLiinkedWithPosition(int PositionID)
        {
            try
            {
                VacationTypeModel vacationTypeModel = new VacationTypeModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetAccrualVacTypeLinkedWithPosition", new SqlParameter("@PositionTypeID", PositionID)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            vacationTypeModel.VacationTypeId = reader["VacationTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["VacationTypeID"]);
                            vacationTypeModel.Name = Convert.ToString(reader["Name"]);
                        }
                    }
                }
                return vacationTypeModel;
                // Finally, we return Position

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        public IEnumerable<PickList> GetAccrualVactionTypeList()
        {
            try
            {
                List<PickList> pickList = new List<PickList>();
                PickList pickModel;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetAccrualVacationType"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            pickModel = new PickList();
                            pickModel.id = Convert.ToInt32(reader["VacationTypeID"].ToString());
                            pickModel.text = reader["Name"].ToString();
                            pickList.Add(pickModel);
                        }
                    }
                }
                return pickList.OrderBy(x => x.text).AsEnumerable();
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool checkAccrualLeavesAvailibility(int EmployeeID, out decimal TotalAccvacDays)
        {
            bool isAccrualAvl = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeAccrualBalance", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlParameter isPresent = new SqlParameter("@AccrualPresent", SqlDbType.Bit);
                isPresent.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(isPresent);

                SqlParameter AccvacDays = new SqlParameter("@TotalAccvacDays", SqlDbType.Float);
                AccvacDays.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(AccvacDays);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                isAccrualAvl = Convert.ToBoolean(isPresent.Value.ToString());
                TotalAccvacDays = Convert.ToDecimal(AccvacDays.Value.ToString());
                return isAccrualAvl;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

        }

        public OperationDetails CarryForwardEmployeeAccrualLeaveDetails(int VacationTypeId, int VacPoitionID, bool IsPositionAccrual, int EmployeeId)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateEmployeeAccrualLeave", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@VactypeId", VacationTypeId);
                sqlCommand.Parameters.AddWithValue("@VacPositionId", VacPoitionID);
                sqlCommand.Parameters.AddWithValue("@IsPositionAccrual", IsPositionAccrual);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                SqlParameter IsSuccess = new SqlParameter("@IsSucccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(IsSuccess.Value.ToString());
                if (op.Success)
                {
                    op.Message = "Employee accrual leaves carry forwarded successfully.";
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = "Error while updating employee accrual leave balance";
                    op.CssClass = "error";
                }

            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = exception.Message;
                op.CssClass = "error";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;

        }
        #endregion

        public bool IsEmployeeUnderProbation(int EmployeeID)
        {
            bool IsEmployeeUnderProbation = false;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",EmployeeID)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_IsEmployeeUnderProbation", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IsEmployeeUnderProbation = Convert.ToBoolean(reader["IsEmployeeUnderProbation"].ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsEmployeeUnderProbation;
        }

        public CurrentAnniversaryModel GetLeavesTakenInCurrentAnniversary(int EmployeeID, int LeaveTypeID)
        {
            CurrentAnniversaryModel objLeaves = new CurrentAnniversaryModel();
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@LeaveTypeID",LeaveTypeID)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetLeavesTakenInCurrentAnniversary", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            objLeaves.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            objLeaves.LeaveTypeID = Convert.ToInt32(reader["LeaveTypeID"].ToString());
                            objLeaves.VTCategoryID = Convert.ToInt32(reader["VTCategoryID"].ToString());
                            objLeaves.LeavesTakenInCurrentAnniversary = Convert.ToInt32(reader["LeavesTakenInCurrentAnniversary"].ToString());
                            objLeaves.LeavesTakenInCurrentAcademicYear = Convert.ToInt32(reader["LeavesTakenInCurrentAcademicYear"].ToString());
                            objLeaves.NoOfLeavesWithOutMedicalCertificate = Convert.ToInt32(reader["NoOfLeavesWithOutMedicalCertificate"].ToString());
                          //  objLeaves.NoOfLeavesPerService = Convert.ToInt32(reader["NoOfLeavesPerService"].ToString());

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objLeaves;
        }

        public bool IsOffDayBeforeOrAfterAppliedLeave(int EmployeeID, string StartDate, string EndDate)
        {
            bool IsOffDay = false;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@StartDate",DateTime.ParseExact(StartDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture)),
                        new SqlParameter("@EndDate",DateTime.ParseExact(EndDate, CommonDB.HRMSDateFormat, CultureInfo.InvariantCulture))

                // new SqlParameter("@StartDate",Convert.ToDateTime(StartDate)),
                // new SqlParameter("@EndDate",Convert.ToDateTime(EndDate))
            };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_IsOffDayBeforeOrAfterAppliedLeave", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IsOffDay = Convert.ToBoolean(reader["IsOffDayBeforeOrAfterAppliedLeave"].ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsOffDay;
        }
        
        public List<VacationTypeCategoryModel> GetAllVacationTypeCategory(int? VTCategoryID, bool? IsActive)
        {
            VacationTypeCategoryModel vacationtypecategorymodel = null;
            List<VacationTypeCategoryModel> vacationtypecategorymodellist = null;
            try
            {
                vacationtypecategorymodellist = new List<VacationTypeCategoryModel>();

                DataHelper dataHelper = new DataHelper();
                List<SqlParameter> sqlparms = new List<SqlParameter>();
                sqlparms.Add(new SqlParameter("@VTCategoryID", VTCategoryID == null ? (object)DBNull.Value : VTCategoryID));
                sqlparms.Add(new SqlParameter("@IsActive", IsActive == null ? (object)DBNull.Value : IsActive));
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlparms, "HR_Stp_GetAllVacationTypeCategory");
                foreach (DataRow dr in dt.Rows)
                {
                    vacationtypecategorymodel = new VacationTypeCategoryModel();
                    vacationtypecategorymodel.VTCategoryID = Convert.ToInt32(dr["VTCategoryID"].ToString());
                    vacationtypecategorymodel.VTCategoryName_1 = dr["VTCategoryName_1"].ToString();
                    vacationtypecategorymodel.VTCategoryName_2 = dr["VTCategoryName_2"].ToString();
                    vacationtypecategorymodel.VTCategoryName_3 = dr["VTCategoryName_3"].ToString();
                    vacationtypecategorymodel.IsActive = Convert.ToBoolean(dr["IsActive"].ToString());

                    vacationtypecategorymodellist.Add(vacationtypecategorymodel);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return vacationtypecategorymodellist;
        }
        //Pooja's Changes
        public OperationDetails CancelLeaveRequest(int? formProcessId, string cancelComments, int userId)
        {
            OperationDetails operationDetails = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateCancelLeaveRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessId);
                sqlCommand.Parameters.AddWithValue("@CancelComments", cancelComments);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);

                SqlParameter OperationMessage = new SqlParameter("@Output", SqlDbType.Int);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Leave Request is cancelled successfully";
                operationDetails.InsertedRowId = Convert.ToInt32(OperationMessage.Value);

            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }
        public List<LeaveBalanceInfoModel> GetLeaveBalanceInfoById(int EmployeeID)
        {
            List<LeaveBalanceInfoModel> lstLeaveBalanceInfoModel = new List<LeaveBalanceInfoModel>();
            LeaveBalanceInfoModel objLeaveBalanceInfoModel;
            try
            {
                SqlParameter[] parameters =
                    {
                        //new SqlParameter("@EmployeeID",EmployeeID)       
                        new SqlParameter("@pEmployeeID",EmployeeID),
                         new SqlParameter("@DepartmentID",DBNull.Value),
                         new SqlParameter("@UserID",Convert.ToInt32(0))
                    };
                // using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_stp_GetLeaveBalanceInfoById", parameters))
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetLeaveBalanceManager", parameters)) 
                {
                    DateTime DateValue;
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            objLeaveBalanceInfoModel = new LeaveBalanceInfoModel();
                            objLeaveBalanceInfoModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            objLeaveBalanceInfoModel.LeaveTypeName = Convert.ToString(reader["VacationTypeName"].ToString());
                            objLeaveBalanceInfoModel.LeaveTypeID = Convert.ToInt32(reader["VacationTypeID"].ToString());
                            objLeaveBalanceInfoModel.TotalLeaveDays = Convert.ToDecimal(reader["TotalDays"].ToString());
                            objLeaveBalanceInfoModel.TakenLeave = Convert.ToDecimal(reader["TotalLeaveDays"].ToString());
                            objLeaveBalanceInfoModel.VTCategoryID = Convert.ToInt32(reader["VTCategoryID"].ToString());
                            if (objLeaveBalanceInfoModel.VTCategoryID == 9)
                                objLeaveBalanceInfoModel.RemainingLeave = Math.Round(Convert.ToDecimal(reader["BalanceLeaveDays"].ToString()), 2);
                            else
                                objLeaveBalanceInfoModel.RemainingLeave = Convert.ToDecimal(reader["BalanceLeaveDays"].ToString());                           
                            objLeaveBalanceInfoModel.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                         
                            objLeaveBalanceInfoModel.FullPaidDays = Convert.ToDecimal(reader["FullPaidDays"].ToString());
                            objLeaveBalanceInfoModel.HalfPaidDays = Convert.ToDecimal(reader["HalfPaidDays"].ToString());
                            objLeaveBalanceInfoModel.UnPaidDays = Convert.ToDecimal(reader["UnPaidDays"].ToString());
                            objLeaveBalanceInfoModel.IsActive = Convert.ToBoolean(reader["IsActive"].ToString()); 
                            objLeaveBalanceInfoModel.IsAccumulatedLeave = Convert.ToBoolean(reader["IsAccumulatedLeave"].ToString());
                            if (DateTime.TryParse(reader["LapseDate"].ToString(), out DateValue))
                                objLeaveBalanceInfoModel.LapseDate = DateValue;
                            else
                                objLeaveBalanceInfoModel.LapseDate = (DateTime?)null;
                            objLeaveBalanceInfoModel.LapsingDays = reader["LapsingDays"] != DBNull.Value ? decimal.Parse(reader["LapsingDays"].ToString()) : (decimal?)null;
                            objLeaveBalanceInfoModel.ExtendedLapsingDays = reader["ExtendedLapsingDays"] != DBNull.Value ? decimal.Parse(reader["ExtendedLapsingDays"].ToString()) : (decimal?)null;
                            if (DateTime.TryParse(reader["ExtendedLapseDate"].ToString(), out DateValue))
                                objLeaveBalanceInfoModel.ExtendedLapseDate = DateValue;
                            else
                                objLeaveBalanceInfoModel.ExtendedLapseDate = (DateTime?)null;
                            objLeaveBalanceInfoModel.BalanceDigitAfterDecimal = Convert.ToInt32(reader["BalanceDigitAfterDecimal"].ToString());
                            objLeaveBalanceInfoModel.LeaveEntitleTypeID = Convert.ToInt16(reader["LeaveEntitleTypeID"].ToString());
                            // objLeaveBalanceInfoModel = new LeaveBalanceInfoModel();
                            //objLeaveBalanceInfoModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            //objLeaveBalanceInfoModel.LeaveTypeName = Convert.ToString(reader["LeaveTypeName"].ToString());                           
                            //objLeaveBalanceInfoModel.LeaveTypeID = Convert.ToInt32(reader["LeaveTypeID"].ToString());
                            //objLeaveBalanceInfoModel.TotalLeaveDays = Convert.ToDecimal(reader["TotalLeaveDays"].ToString());
                            //objLeaveBalanceInfoModel.TakenLeave = Convert.ToDecimal(reader["TakenLeave"].ToString());
                            //objLeaveBalanceInfoModel.RemainingLeave = Convert.ToDecimal(reader["RemainingLeave"].ToString());
                            //objLeaveBalanceInfoModel.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                            //objLeaveBalanceInfoModel.VTCategoryID = Convert.ToInt32(reader["VTCategoryID"].ToString());
                            //objLeaveBalanceInfoModel.FullPaidDays = Convert.ToInt32(reader["FullPaidDays"].ToString());
                            //objLeaveBalanceInfoModel.HalfPaidDays = Convert.ToInt32(reader["HalfPaidDays"].ToString());
                            //objLeaveBalanceInfoModel.UnPaidDays = Convert.ToInt32(reader["UnPaidDays"].ToString());
                            lstLeaveBalanceInfoModel.Add(objLeaveBalanceInfoModel);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return lstLeaveBalanceInfoModel;
        }

    }
}
