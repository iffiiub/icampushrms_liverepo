﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
   public class UserLoggerDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        GeneralDB.DataHelper dataHelper;
        public OperationDetails AddUserLog(UserLogModel UserLogModel)
        {
            dataHelper = new GeneralDB.DataHelper();
            OperationDetails oDetail = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_AddUserLog", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserLogModel.userId);
                sqlCommand.Parameters.AddWithValue("@UserName", UserLogModel.userName);
                //sqlCommand.Parameters.AddWithValue("@Description", UserLogModel.description);
                sqlCommand.Parameters.Add("@Description", SqlDbType.NVarChar, -1);               
                sqlCommand.Parameters["@Description"].Value = UserLogModel.description;
                // sqlCommand.Parameters.AddWithValue("@Date", UserLogModel.Date);//commented getting datetime in db
                sqlCommand.Parameters.AddWithValue("@OprationType", UserLogModel.oprationType);
                sqlCommand.Parameters.AddWithValue("@ModuleName", UserLogModel.moduleName);
                sqlCommand.Parameters.AddWithValue("@BrowserName", UserLogModel.browserName);
                sqlCommand.Parameters.AddWithValue("@BrowserVersion", UserLogModel.browserVersion);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                oDetail.Success = true;
                oDetail.Message = "Record save successfully";
            }
            catch (Exception e)
            {
                oDetail.Success = false;
                oDetail.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oDetail;
        }

    }
}
