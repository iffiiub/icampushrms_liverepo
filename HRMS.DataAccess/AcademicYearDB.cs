﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Threading;

namespace HRMS.DataAccess
{
    public class AcademicYearDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<AcademicYearModel> GetAllAcademicYearList()
        {
            List<AcademicYearModel> academicYearList = null;
            try
            {
                academicYearList = new List<AcademicYearModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_GetAll_AcademicYear", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AcademicYearModel academicYearModel;
                    while (sqlDataReader.Read())
                    {
                        academicYearModel = new AcademicYearModel();

                        academicYearModel.AcademicYearId = Convert.ToInt32(sqlDataReader["AcademicYearId"].ToString());
                        academicYearModel.Duration = sqlDataReader["Duration"] == DBNull.Value ? "" : sqlDataReader["Duration"].ToString();
                        academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        academicYearModel.BeginingInstructionDate = Convert.ToString(sqlDataReader["BeginningInstructionalDate"])==""? Convert.ToString(sqlDataReader["BeginningInstructionalDate"]):Convert.ToDateTime((sqlDataReader["BeginningInstructionalDate"])).ToString("dd/MM/yyyy");
                        academicYearModel.EndingInstructionDate = Convert.ToString(sqlDataReader["EndingInstructionalDate"]) == "" ? Convert.ToString(sqlDataReader["EndingInstructionalDate"]) : Convert.ToDateTime((sqlDataReader["EndingInstructionalDate"])).ToString("dd/MM/yyyy");
                        academicYearModel.IsNextYear = Convert.ToBoolean(sqlDataReader["IsNextYear"].ToString());
                        academicYearList.Add(academicYearModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return academicYearList;
        }

        public List<AcademicYearModel> GetAllAcademicYear()
        {
            List<AcademicYearModel> academicYearList = null;
            try
            {
                academicYearList = new List<AcademicYearModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllAcademicYearForSundries", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AcademicYearModel academicYearModel;
                    while (sqlDataReader.Read())
                    {
                        academicYearModel = new AcademicYearModel();

                        academicYearModel.AcademicYearId = Convert.ToInt32(sqlDataReader["AcademicYearId"].ToString());
                        academicYearModel.Duration = sqlDataReader["Duration"] == DBNull.Value ? "" : sqlDataReader["Duration"].ToString();
                        academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        academicYearList.Add(academicYearModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return academicYearList;
        }
        public List<AcademicYearModel> GetAllAcademicYearLanguage()
        {
            List<AcademicYearModel> academicYearList = null;
            try
            {
                academicYearList = new List<AcademicYearModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllAcademicYearForSundries", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AcademicYearModel academicYearModel;
                    while (sqlDataReader.Read())
                    {
                        academicYearModel = new AcademicYearModel();

                        academicYearModel.AcademicYearId = Convert.ToInt32(sqlDataReader["AcademicYearId"].ToString());
                        if(Thread.CurrentThread.CurrentUICulture.Name=="ar")
                        {
                            academicYearModel.Duration = sqlDataReader["AcademicYearName_2"] == DBNull.Value ? "" : sqlDataReader["AcademicYearName_2"].ToString();
                        }
                        else
                        {
                            academicYearModel.Duration = sqlDataReader["Duration"] == DBNull.Value ? "" : sqlDataReader["Duration"].ToString();
                        }
                        
                        academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        academicYearList.Add(academicYearModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return academicYearList;
        }
        public List<AcademicYearModel> GetAcademicYears()
        {
            List<AcademicYearModel> academicYearList = null;
            try
            {
                academicYearList = new List<AcademicYearModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetAcademicYearsForAbsentDeduction", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AcademicYearModel academicYearModel;
                    while (sqlDataReader.Read())
                    {
                        academicYearModel = new AcademicYearModel();

                        academicYearModel.Year = Convert.ToInt32(sqlDataReader["Year"].ToString());
                        academicYearList.Add(academicYearModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return academicYearList.OrderBy(x=>x.Year).ToList();
        }
        public AcademicYearModel GetAcademicYearById(int AcademicYearId)
        {
            AcademicYearModel academicYearModel = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetAcademicYearById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AcademicYearId", AcademicYearId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    academicYearModel = new AcademicYearModel();
                    while (sqlDataReader.Read())
                    {
                        academicYearModel = new AcademicYearModel();
                        academicYearModel.AcademicYearId = Convert.ToInt32(sqlDataReader["AcademicYearId"].ToString());
                        academicYearModel.Duration = sqlDataReader["Duration"] == DBNull.Value ? "" : sqlDataReader["Duration"].ToString();
                        academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return academicYearModel;
        }
    }
}
