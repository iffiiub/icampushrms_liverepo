﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class VisaDB : DBHelper
    {
        public OperationDetails InsertVisa(VisaModel VisaModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {

                        new SqlParameter("@DocumentNo",VisaModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", VisaModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", VisaModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(VisaModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(VisaModel.ExpiryDate)) ,
                      new SqlParameter("@Note",VisaModel.Note ) ,
                      new SqlParameter("@IsPrimary", VisaModel.IsPrimary ),

                      new SqlParameter("@SponsoredBy", VisaModel.SponsoredBy) ,
                      new SqlParameter("@SponsorName",VisaModel.SponsorName) ,
                      new SqlParameter("@SponsorPassportNo", VisaModel.SponsorPassportNo),
                      new SqlParameter("@EmployeeId", VisaModel.EmployeeId),
                       new SqlParameter("@VisaFile", VisaModel.VisaFile),
                        new SqlParameter("@UidNo", VisaModel.UidNo)
                    };
                int DocVisaId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_Visa", parameters));
                return new OperationDetails(true, "Visa saved successfully.", null, DocVisaId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Visa .", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateVisa(VisaModel VisaModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@DocVisaId",VisaModel.DocVisaId),
                      new SqlParameter("@DocumentNo",VisaModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", VisaModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", VisaModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(VisaModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(VisaModel.ExpiryDate)) ,
                      new SqlParameter("@Note",VisaModel.Note ) ,
                      new SqlParameter("@IsPrimary", VisaModel.IsPrimary ),
                      new SqlParameter("@SponsoredBy", VisaModel.SponsoredBy),

                      new SqlParameter("@SponsorName",VisaModel.SponsorName) ,
                      new SqlParameter("@SponsorPassportNo", VisaModel.SponsorPassportNo),
                      new SqlParameter("@VisaFile", VisaModel.VisaFile),
                        new SqlParameter("@UidNo", VisaModel.UidNo)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Visa", parameters);
                return new OperationDetails(true, "Visa   updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Visa.", exception);
                throw;
            }
            finally
            {

            }
        }

        public VisaModel VisaById(int VisaId)
        {
            try
            {
                VisaModel VisaModel = new VisaModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_VisaByID", new SqlParameter("@DocVisaId", VisaId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            VisaModel.DocVisaId = Convert.ToInt32(reader["DocVisaId"].ToString());
                            VisaModel.DocumentNo = Convert.ToString(reader["DocumentNo"]);
                            VisaModel.IssueCountry = reader["IssueCountry"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssueCountry"]);
                            VisaModel.IssuePlace = reader["IssuePlace"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssuePlace"]);
                            VisaModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                            VisaModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            VisaModel.Note = Convert.ToString(reader["Note"]);
                            VisaModel.IsPrimary = reader["IssueCountry"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsPrimary"].ToString());
                            VisaModel.SponsoredBy = reader["SponsoredBy"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SponsoredBy"].ToString());
                            VisaModel.SponsorName = Convert.ToString(reader["SponsorName"].ToString());
                            VisaModel.SponsorPassportNo = Convert.ToString(reader["SponsorPassportNo"].ToString());
                            VisaModel.EmployeeId = reader["EmployeeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmployeeId"].ToString());
                            VisaModel.VisaFile = Convert.ToString(reader["VisaFile"]);
                            VisaModel.UidNo = Convert.ToString(reader["UidNo"]);
                        }
                    }
                }
                return VisaModel;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Visa.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteVisaById(int VisaId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@DocVisaId",VisaId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_Visa", parameters);
                return new OperationDetails(true, "Visa deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Visa.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<VisaModel> GetVisaListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder, string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the 		
            List<VisaModel> VisaModelList = new List<VisaModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_VisaWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create , and insert them into our list
                        VisaModel VisaModel;
                        while (reader.Read())
                        {
                            VisaModel = new VisaModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            VisaModel.DocVisaId = Convert.ToInt32(reader["DocVisaId"].ToString());
                            VisaModel.DocumentNo = Convert.ToString(reader["DocumentNo"]);
                            VisaModel.IssueCountry = reader["IssueCountry"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssueCountry"]);
                            VisaModel.IssuePlace = reader["IssuePlace"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssuePlace"]);
                            VisaModel.IssueDate = Convert.ToString(reader["IssueDate"]);
                            VisaModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"]);
                            VisaModel.Note = Convert.ToString(reader["Note"]);
                            VisaModel.IsPrimary = reader["IssueCountry"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsPrimary"].ToString());
                            VisaModel.SponsoredBy = reader["SponsoredBy"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SponsoredBy"].ToString());
                            VisaModel.SponsorName = Convert.ToString(reader["SponsorName"].ToString());
                            VisaModel.SponsorPassportNo = Convert.ToString(reader["SponsorPassportNo"].ToString());
                            VisaModel.EmployeeId = reader["EmployeeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmployeeId"].ToString());
                            VisaModel.VisaFile = Convert.ToString(reader["VisaFile"]);
                            VisaModel.UidNo = Convert.ToString(reader["UidNo"]);
                            VisaModelList.Add(VisaModel);
                        }
                    }
                }
                // Finally, we return our list of VisaModelList
                totalCount = TotalCount;
                return VisaModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching visa List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
