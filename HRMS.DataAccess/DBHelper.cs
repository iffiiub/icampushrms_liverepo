﻿using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;

namespace HRMS.DataAccess
{
    public class DBHelper
    {
        public string connectionString { get; set; }

        public DBHelper()
        {
            connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        }

        /// <summary>
        /// Get all Status from the Status table
        /// Created By : Pradnesh Raut
        /// Created Date : 14 April 2015
        /// </summary>
        /// <returns>List of Status</returns>
        public List<StatusModel> GetStatusList()
        {
            // Create a list to hold the Status		
            List<StatusModel> statusModelList = new List<StatusModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Status"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Status, and insert them into our list
                        StatusModel statusModel;
                        while (reader.Read())
                        {
                            statusModel = new StatusModel();
                            statusModel.StatusID = Convert.ToInt32(reader["StatusID"].ToString());
                            statusModel.StatusName = reader["StatusName"].ToString();
                            statusModel.Description = reader["Description"].ToString();
                            statusModelList.Add(statusModel);
                        }
                    }
                }
                // Finally, we return our list of status
                return statusModelList.OrderBy(a => a.StatusName).ToList();
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching status List.", exception);
                throw;
            }
            finally
            {

            }
        }


        /// <summary>
        /// Get all Employment Type from the EmploymentType table
        /// Created By : Pradnesh Raut
        /// Created Date : 14 April 2015
        /// </summary>
        /// <returns>List of EmploymentType</returns>
        public List<EmploymentTypeModel> GetEmploymentTypeList()
        {
            // Create a list to hold the EmploymentType		
            List<EmploymentTypeModel> employmentTypeModelList = new List<EmploymentTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_EmploymentType"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new EmploymentType, and insert them into our list
                        EmploymentTypeModel employmentTypeModel;
                        while (reader.Read())
                        {
                            employmentTypeModel = new EmploymentTypeModel();
                            employmentTypeModel.EmploymentTypeID = Convert.ToInt32(reader["EmploymentTypeID"].ToString());
                            employmentTypeModel.EmploymentTypeName = reader["EmploymentTypeName_1"].ToString();
                            employmentTypeModelList.Add(employmentTypeModel);
                        }
                    }
                }
                // Finally, we return our list of employmentType
                return employmentTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employmentType List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all RegTemp from the HR_RegTemp table
        /// Created By : Pradnesh Raut
        /// Created Date : 14 April 2015
        /// </summary>
        /// <returns>List of RegTemp</returns>
        public List<RegTempModel> GetRegTempList()
        {
            // Create a list to hold the RegTemp		
            List<RegTempModel> regTempModelList = new List<RegTempModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_RegTemp"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new RegTemp, and insert them into our list
                        RegTempModel regTempModel;
                        while (reader.Read())
                        {
                            regTempModel = new RegTempModel();
                            regTempModel.Reg_TempID = Convert.ToInt32(reader["Reg_TempID"].ToString());
                            regTempModel.Reg_TempName = reader["Reg_TempName"].ToString();
                            regTempModelList.Add(regTempModel);
                        }
                    }
                }
                // Finally, we return our list of RegTemp
                return regTempModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching RegTemp List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all JobFamily from the HR_JobFamily table
        /// Created By : Pradnesh Raut
        /// Created Date : 14 April 2015
        /// </summary>
        /// <returns>List of JobFamily</returns>
        public List<JobFamilyModel> GetJobFamilyList()
        {
            // Create a list to hold the JobFamily		
            List<JobFamilyModel> JobFamilyModelList = new List<JobFamilyModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_JobFamily"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new JobFamily, and insert them into our list
                        JobFamilyModel JobFamilyModel;
                        while (reader.Read())
                        {
                            JobFamilyModel = new JobFamilyModel();
                            JobFamilyModel.JobFamilyID = Convert.ToInt32(reader["JobFamilyID"].ToString());
                            JobFamilyModel.JobFamilyName = reader["JobFamilyName"].ToString();
                            JobFamilyModel.JobFamilyNameFrench = reader["JobFamilyNameFrench"].ToString();
                            JobFamilyModel.JobFamilyDescription = reader["JobFamilyDescription"].ToString();
                            JobFamilyModelList.Add(JobFamilyModel);
                        }
                    }
                }
                // Finally, we return our list of JobFamily
                return JobFamilyModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching JobFamily List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Insert Job Family into JobFamily table
        /// Created By : Pradnesh Raut
        /// Created Date : 15 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails InsertJobFamily(JobFamilyModel jobFamilyModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@JobFamilyName", jobFamilyModel.JobFamilyName) ,
                       new SqlParameter("@JobFamilyNameFrench", jobFamilyModel.JobFamilyNameFrench) ,
                      new SqlParameter("@JobFamilyDescription", jobFamilyModel.JobFamilyDescription)

                    };
                int JobFamilyId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_HR_JobFamily", parameters));
                return new OperationDetails(true, "Job Family inserted successfully.", null, JobFamilyId);
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while inserting JobFamily.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all employee dependent relationships from the HR_Employee_DependantRelationship table
        /// Created By : Pradnesh Raut
        /// Created Date : 19 April 2015
        /// </summary>
        /// <returns>List of all employee dependent relationships</returns>
        public List<Employee_DependantRelationshipModel> GetEmployee_DependantRelationshipList()
        {
            // Create a list to hold the Employee_DependantRelationship		
            List<Employee_DependantRelationshipModel> Employee_DependantRelationshipModelList = new List<Employee_DependantRelationshipModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Employee_DependantRelationship"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Employee_DependantRelationship, and insert them into our list
                        Employee_DependantRelationshipModel employee_DependantRelationshipModel;
                        while (reader.Read())
                        {
                            employee_DependantRelationshipModel = new Employee_DependantRelationshipModel();
                            employee_DependantRelationshipModel.DependantRelationshipId = Convert.ToInt32(reader["DependantRelationshipId"].ToString());
                            employee_DependantRelationshipModel.RelationshipName = reader["RelationshipName"].ToString();
                            Employee_DependantRelationshipModelList.Add(employee_DependantRelationshipModel);
                        }
                    }
                }
                // Finally, we return our list of JobFamily
                return Employee_DependantRelationshipModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching JobFamily List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all Employee leave reason from the HR_EmployeeLeaveReason table
        /// Created By : Hardik
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>List of all Employee leave reason</returns>
        public List<LeaveTypeModel> GetEmployeeLeaveTypeList()
        {
            // Create a list to hold the EmployeeLeaveReason List		
            List<LeaveTypeModel> LeaveTypeModelList = new List<LeaveTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_EmployeeLeaveType"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new EmployeeLeaveReasonModel, and insert them into our list
                        LeaveTypeModel LeaveTypeModel;
                        while (reader.Read())
                        {
                            LeaveTypeModel = new LeaveTypeModel();
                            LeaveTypeModel.LeaveTypeId = Convert.ToInt32(reader["AbsentPaidTypeID"].ToString());
                            LeaveTypeModel.LeaveTypeName = reader["AbsentPaidTypeName"].ToString();
                            LeaveTypeModelList.Add(LeaveTypeModel);
                        }
                    }
                }
                // Finally, we return our list of EmployeeLeaveReasonList
                return LeaveTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching EmployeeLeaveReason List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Insert Employee leave reason into HR_EmployeeLeaveReason table
        /// Created By : hardik
        /// Created Date : 21 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails InsertEmployeeLeaveReason(EmployeeLeaveReasonModel employeeLeaveReasonModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@EmployeeLeaveReason", employeeLeaveReasonModel.EmployeeLeaveReason)

                    };
                int LeaveReasonId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_HR_EmployeeLeaveReason", parameters));
                return new OperationDetails(true, "Leave reason inserted successfully.", null, LeaveReasonId);
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while inserting leave reasons.", exception);
                throw;
            }
            finally
            {

            }
        }


        public List<EmploymentModeModel> GetEmploymentModeList()
        {
            // Create a list to hold the EmploymentMode		
            List<EmploymentModeModel> EmploymentModeList = new List<EmploymentModeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_GEN_EmploymentMode"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new EmploymentModeModel, and insert them into our list
                        EmploymentModeModel EmploymentModeModel;
                        while (reader.Read())
                        {
                            EmploymentModeModel = new EmploymentModeModel();
                            EmploymentModeModel.EmploymentModeId = Convert.ToInt32(reader["EmploymentModeId"].ToString());
                            EmploymentModeModel.EmploymentModeName1 = reader["EmploymentModeName1"].ToString();
                            EmploymentModeModel.EmploymentModeName2 = reader["EmploymentModeName2"].ToString();
                            EmploymentModeModel.EmploymentModeName3 = reader["EmploymentModeName3"].ToString();
                            EmploymentModeList.Add(EmploymentModeModel);
                        }
                    }
                }
                // Finally, we return our list of EmploymentModeModel
                return EmploymentModeList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching EmploymentModeModel List.", exception);
                throw;
            }
            finally
            {

            }
        }


        public OperationDetails InsertEmploymentMode(EmploymentModeModel EmploymentModeModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@EmploymentModeName1", EmploymentModeModel.EmploymentModeName1) ,
                       new SqlParameter("@EmploymentModeName2", EmploymentModeModel.EmploymentModeName2) ,
                      new SqlParameter("@EmploymentModeName3", EmploymentModeModel .EmploymentModeName3)

                    };
                int EmploymentModeId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_GEN_EmploymentMode", parameters));
                return new OperationDetails(true, "Employment Mode inserted successfully.", null, EmploymentModeId);
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while inserting EmploymentMode.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<MenuNavigationModel> GetMenuNavigationList()
        {
            List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_GEN_MainNavigation"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        MenuNavigationModel objMenuNavigationModel;
                        while (reader.Read())
                        {
                            objMenuNavigationModel = new MenuNavigationModel();
                            objMenuNavigationModel.MainNavigationId = Convert.ToInt32(reader["MainNavigationId"].ToString());
                            objMenuNavigationModel.MainNavigationName = reader["MainNavigationName"].ToString();
                            objMenuNavigationModel.MainNavigationSubCategoryId = Convert.ToInt32(reader["MainNavigationSubCategoryId"].ToString());
                            objMenuNavigationModel.MainNavigationSubCategoryName = reader["MainNavigationSubCategoryName"].ToString();
                            objMenuNavigationModel.URL = reader["URL"].ToString();
                            objMenuNavigationModel.MainNavigationHTMLLink = reader["MainNavigationHTMLLink"].ToString();
                            objMenuNavigationModel.MainNavigationSubCategoryHTMLLink = reader["MainNavigationSubCategoryHTMLLink"].ToString();
                            objMenuNavigationModel.SortingOrder = reader["MainNavigationSubCategoryId"].ToString() != "" ? Convert.ToInt32(reader["SortingOrder"].ToString()) : 1;
                            objMenuNavigationModel.IsThirdNavigation = Convert.ToBoolean(reader["IsThirdNavigation"]);
                            objMenuNavigationModel.IsViewCheckBox = Convert.ToBoolean(reader["IsViewCheckBox"]);
                            objMenuNavigationModel.IsAddCheckbox = Convert.ToBoolean(reader["IsAddCheckbox"]);
                            objMenuNavigationModel.IsUpdateCheckbox = Convert.ToBoolean(reader["IsUpdateCheckbox"]);
                            objMenuNavigationModel.IsDeleteCheckbox = Convert.ToBoolean(reader["IsDeleteCheckbox"]);
                            objMenuNavigationModel.NavOtherPermissionIDs = reader["OtherPermissionIDs"].ToString();
                            //*** Naresh 2020-03-17 Populate ViewAccess
                            objMenuNavigationModel.ViewAccess = reader["ViewAccess"] != null ? reader["ViewAccess"].ToString() : "";
                            objMenuNavigationModelList.Add(objMenuNavigationModel);
                        }
                    }
                }
                return objMenuNavigationModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Menu Navigation Model List.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<MenuNavigationModel> GetMainMenuNavigationList()
        {
            List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, "select * from Gen_MainNavigation Where IsActive=1"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        MenuNavigationModel objMenuNavigationModel;
                        while (reader.Read())
                        {
                            objMenuNavigationModel = new MenuNavigationModel();
                            objMenuNavigationModel.MainNavigationId = Convert.ToInt32(reader["MainNavigationId"].ToString());
                            objMenuNavigationModel.MainNavigationName = reader["MainNavigationName"].ToString();
                            objMenuNavigationModelList.Add(objMenuNavigationModel);
                        }
                    }
                }
                return objMenuNavigationModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Menu Navigation Model List.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<TreeNavigationModel> GetMenuNavigationTree(int? ParentNavId, int? UserRole)
        {
            List<TreeNavigationModel> objMenuNavigationModelList = new List<TreeNavigationModel>();
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@ParentNavID",ParentNavId),
                        new SqlParameter("@UserRoleId",UserRole)
                };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetNavigationTree", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        TreeNavigationModel objTreeMenuNavigationModel;
                        while (reader.Read())
                        {
                            objTreeMenuNavigationModel = new TreeNavigationModel();
                            objTreeMenuNavigationModel.ModuleID = Convert.ToInt32(reader["ModuleID"].ToString() == "" ? "0" : reader["ModuleID"].ToString());
                            objTreeMenuNavigationModel.ModuleName = reader["ModuleName"].ToString();
                            objTreeMenuNavigationModel.ModuleURL = reader["ModuleURL"].ToString();
                            objTreeMenuNavigationModel.ActNavigationID = Convert.ToInt32(reader["ActNavigationID"].ToString());
                            objTreeMenuNavigationModel.ParentModuleID = Convert.ToInt32(reader["ParentModuleID"].ToString() == "" ? "0" : reader["ParentModuleID"].ToString());
                            objTreeMenuNavigationModel.IsViewCheckbox = Convert.ToBoolean(reader["IsViewCheckbox"].ToString() == "" ? "false" : reader["IsViewCheckbox"].ToString());
                            objTreeMenuNavigationModel.IsAddCheckBox = Convert.ToBoolean(reader["IsAddCheckBox"].ToString() == "" ? "false" : reader["IsAddCheckBox"].ToString());
                            objTreeMenuNavigationModel.IsUpdateCheckbox = Convert.ToBoolean(reader["IsUpdateCheckbox"].ToString() == "" ? "false" : reader["IsUpdateCheckbox"].ToString());
                            objTreeMenuNavigationModel.IsDeleteCheckbox = Convert.ToBoolean(reader["IsDeleteCheckbox"].ToString() == "" ? "false" : reader["IsDeleteCheckbox"].ToString());
                            objTreeMenuNavigationModel.OtherPermissionIds = Convert.ToString(reader["OtherPermissionIds"]);
                            objTreeMenuNavigationModel.IsThirdNavigation = Convert.ToBoolean(Convert.ToString(reader["IsThirdNavigation"]));
                            objMenuNavigationModelList.Add(objTreeMenuNavigationModel);
                        }
                    }
                }
                return objMenuNavigationModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Menu Navigation Model List.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<MenuNavigationModel> GetListOfByPassPermissionExeceptCreate()
        {
            List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetListOfByPassPermissionExeceptCreate"))
                {
                    if (reader.HasRows)
                    {
                        MenuNavigationModel objMenuNavigationModel;
                        while (reader.Read())
                        {
                            objMenuNavigationModel = new MenuNavigationModel();
                            objMenuNavigationModel.URL = reader["URL"].ToString();
                            objMenuNavigationModelList.Add(objMenuNavigationModel);
                        }
                    }
                }
                return objMenuNavigationModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }

    }
}
