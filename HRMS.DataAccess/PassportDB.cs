﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class PassportDB : DBHelper
    {
        public OperationDetails InsertPassport(PassportModel PassportModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@DocumentNo",PassportModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", PassportModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", PassportModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(PassportModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(PassportModel.ExpiryDate )) ,
                      new SqlParameter("@Note",PassportModel.Note ) ,
                      new SqlParameter("@IsPrimary", PassportModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", PassportModel.EmployeeId),
                      new SqlParameter("@IsDeleted", 0) ,
                      new SqlParameter("@PassportWithEmployee",PassportModel.PassportWithEmployee),
                      new SqlParameter("@PassportFile",PassportModel.PassportFile)
                    };
                int DocPassportId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_Passport", parameters));
                return new OperationDetails(true, "Passport saved successfully.", null, DocPassportId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Passport.", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdatePassport(PassportModel PassportModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@DocPassportId",PassportModel.DocPassportId),
                      new SqlParameter("@DocumentNo",PassportModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", PassportModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", PassportModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(PassportModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(PassportModel.ExpiryDate)) ,
                      new SqlParameter("@Note",PassportModel.Note ) ,
                      new SqlParameter("@IsPrimary", PassportModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", PassportModel.EmployeeId),
                      new SqlParameter("@IsDeleted", 0) ,
                      new SqlParameter("@PassportWithEmployee",PassportModel.PassportWithEmployee) ,
                      new SqlParameter("@PassportFile",PassportModel.PassportFile)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Passport", parameters);
                return new OperationDetails(true, "Passport updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Passport.", exception);
                throw;
            }
            finally
            {

            }
        }

        public PassportModel PassportById(int PassportId)
        {
            try
            {
                PassportModel PassportModel = new PassportModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_PassportByID", new SqlParameter("@PassportId", PassportId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            PassportModel.DocPassportId = reader["DocPassportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DocPassportId"].ToString());
                            PassportModel.DocumentNo = Convert.ToString(reader["DocumentNo"]);
                            PassportModel.IssueCountry = reader["IssueCountry"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssueCountry"].ToString());
                            PassportModel.IssuePlace = reader["IssuePlace"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssuePlace"].ToString());
                            PassportModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                            PassportModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            PassportModel.Note = Convert.ToString(reader["Note"]);
                            PassportModel.IsPrimary = reader["IsPrimary"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsPrimary"].ToString());
                            PassportModel.EmployeeId = reader["EmployeeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmployeeId"]);
                            PassportModel.IsDeleted = reader["IsDeleted"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                            PassportModel.PassportWithEmployee = reader["PassportWithEmployee"] == DBNull.Value ? false : Convert.ToBoolean(reader["PassportWithEmployee"].ToString());
                            PassportModel.PassportFile = Convert.ToString(reader["PassportFile"]);



                            //PassportModel.DocPassportId = Convert.ToInt32(reader["DocPassportId"].ToString());
                            //PassportModel.DocumentNo = reader["DocumentNo"].ToString();
                            //PassportModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            //PassportModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            //PassportModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            //PassportModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            //PassportModel.Note = reader["Note"].ToString();
                            //PassportModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            //PassportModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            //PassportModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            //PassportModel.PassportWithEmployee = Convert.ToBoolean(reader["PassportWithEmployee"].ToString());
                            //PassportModel.PassportFile = reader["PassportFile"].ToString();
                        }
                    }
                }
                return PassportModel;
                // Finally, we return PassportModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching PassportModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeletePassportById(int PassportId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@PassportId",PassportId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_PassportByID", parameters);
                return new OperationDetails(true, "Passport deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Passport.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PassportModel> GetPassportListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder, string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                       new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the PassportModel		
            List<PassportModel> PassportModelList = new List<PassportModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_PassportWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PassportModel PassportModel;
                        while (reader.Read())
                        {
                            PassportModel = new PassportModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            PassportModel.DocPassportId = reader["DocPassportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DocPassportId"].ToString());
                            PassportModel.DocumentNo = Convert.ToString(reader["DocumentNo"]);
                            PassportModel.IssueCountry = reader["IssueCountry"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssueCountry"].ToString());
                            PassportModel.IssuePlace = reader["IssuePlace"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IssuePlace"].ToString());
                            PassportModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            PassportModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            PassportModel.Note = Convert.ToString(reader["Note"]);
                            PassportModel.IsPrimary = reader["IsPrimary"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsPrimary"].ToString());
                            PassportModel.EmployeeId = reader["EmployeeId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmployeeId"]);
                            PassportModel.IsDeleted = reader["IsDeleted"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                            PassportModel.PassportWithEmployee = reader["PassportWithEmployee"] == DBNull.Value ? false : Convert.ToBoolean(reader["PassportWithEmployee"].ToString());
                            PassportModel.PassportFile = Convert.ToString(reader["PassportFile"]);
                            PassportModelList.Add(PassportModel);
                        }
                    }
                }
                // Finally, we return our list of PassportModelList
                totalCount = TotalCount;
                return PassportModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching PassportModel List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
