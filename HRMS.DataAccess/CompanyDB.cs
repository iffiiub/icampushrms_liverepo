﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Threading;

namespace HRMS.DataAccess
{
    public class CompanyDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        /// <summary>
        /// Add Company Details
        /// </summary>
        /// <param name="companyModel"></param>
        /// <returns></returns>
        public string AddCompany(CompanyModel companyModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Company", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Name", companyModel.name);
                sqlCommand.Parameters.AddWithValue("@Name_2", companyModel.Name_2);
                sqlCommand.Parameters.AddWithValue("@Name_3", companyModel.Name_3);
                sqlCommand.Parameters.AddWithValue("@AlternativeCompanyID", string.IsNullOrEmpty(companyModel.AlternativeCompanyID) ? null : companyModel.AlternativeCompanyID);
                sqlCommand.Parameters.AddWithValue("@OrganizationCode", companyModel.OrganizationCode);
                sqlCommand.Parameters.AddWithValue("@ShortName_1", companyModel.ShortName_1);
                sqlCommand.Parameters.AddWithValue("@ShortName_2", companyModel.ShortName_2);
                sqlCommand.Parameters.AddWithValue("@ShortName_3", companyModel.ShortName_3);
                sqlCommand.Parameters.AddWithValue("@CampusName_1", companyModel.CampusName_1);
                sqlCommand.Parameters.AddWithValue("@CampusName_2", companyModel.CampusName_2);
                sqlCommand.Parameters.AddWithValue("@CampusName_3", companyModel.CampusName_3);
                sqlCommand.Parameters.AddWithValue("@EmailId", companyModel.EmailId);
                sqlCommand.Parameters.AddWithValue("@Phone", companyModel.Phone);
                sqlCommand.Parameters.AddWithValue("@Phone_2", companyModel.Phone_2);
                sqlCommand.Parameters.AddWithValue("@Phone_3", companyModel.Phone_3);
                sqlCommand.Parameters.AddWithValue("@Fax_1", companyModel.Fax_1);
                sqlCommand.Parameters.AddWithValue("@Fax_2", companyModel.Fax_2);
                sqlCommand.Parameters.AddWithValue("@Fax_3", companyModel.Fax_3);
                sqlCommand.Parameters.AddWithValue("@ContactPersonName", companyModel.ContactPersonName);
                sqlCommand.Parameters.AddWithValue("@Address1", companyModel.Address1);
                sqlCommand.Parameters.AddWithValue("@Address2", companyModel.Address2);
                sqlCommand.Parameters.AddWithValue("@Address_3", companyModel.Address_3);
                sqlCommand.Parameters.AddWithValue("@City", companyModel.City);
                sqlCommand.Parameters.AddWithValue("@Country", companyModel.Country);
                sqlCommand.Parameters.AddWithValue("@POBOX", companyModel.POBOX);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", companyModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyHead", companyModel.CompanyHead);
                sqlCommand.Parameters.AddWithValue("@Assistant1", companyModel.Assistant1);
                sqlCommand.Parameters.AddWithValue("@Assistant2", companyModel.Assistant2);
                sqlCommand.Parameters.AddWithValue("@Assistant3", companyModel.Assistant3);
                sqlCommand.Parameters.AddWithValue("@SchoolCompany", companyModel.SchoolCompany);
                sqlCommand.Parameters.AddWithValue("@CompanyTypeID", companyModel.CompanyTypeID >= 0 ? companyModel.CompanyTypeID : (object)DBNull.Value);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Update Company Details
        /// </summary>
        /// <param name="companyModel"></param>
        /// <returns></returns>
        public string UpdateCompany(CompanyModel companyModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_Company", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyModel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@Name", companyModel.name);
                sqlCommand.Parameters.AddWithValue("@OrganizationCode", companyModel.OrganizationCode);
                sqlCommand.Parameters.AddWithValue("@ShortName_1", companyModel.ShortName_1);
                sqlCommand.Parameters.AddWithValue("@ShortName_2", companyModel.ShortName_2);
                sqlCommand.Parameters.AddWithValue("@ShortName_3", companyModel.ShortName_3);
                sqlCommand.Parameters.AddWithValue("@EmailId", companyModel.EmailId);
                sqlCommand.Parameters.AddWithValue("@Phone", companyModel.Phone);
                sqlCommand.Parameters.AddWithValue("@ContactPersonName", companyModel.ContactPersonName);
                sqlCommand.Parameters.AddWithValue("@Address1", companyModel.Address1);
                sqlCommand.Parameters.AddWithValue("@Address2", companyModel.Address2);
                sqlCommand.Parameters.AddWithValue("@City", companyModel.City);
                sqlCommand.Parameters.AddWithValue("@Country", companyModel.Country);
                sqlCommand.Parameters.AddWithValue("@POBOX", companyModel.POBOX);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", companyModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyHead", companyModel.CompanyHead);
                sqlCommand.Parameters.AddWithValue("@Assistant1", companyModel.Assistant1);
                sqlCommand.Parameters.AddWithValue("@Assistant2", companyModel.Assistant2);
                sqlCommand.Parameters.AddWithValue("@Assistant3", companyModel.Assistant3);
                sqlCommand.Parameters.AddWithValue("@Name_2", companyModel.Name_2);
                sqlCommand.Parameters.AddWithValue("@Name_3", companyModel.Name_3);
                sqlCommand.Parameters.AddWithValue("@CampusName_1", companyModel.CampusName_1);
                sqlCommand.Parameters.AddWithValue("@CampusName_2", companyModel.CampusName_2);
                sqlCommand.Parameters.AddWithValue("@CampusName_3", companyModel.CampusName_3);
                sqlCommand.Parameters.AddWithValue("@Address_3", companyModel.Address_3);
                sqlCommand.Parameters.AddWithValue("@Phone_2", companyModel.Phone_2);
                sqlCommand.Parameters.AddWithValue("@Phone_3", companyModel.Phone_3);
                sqlCommand.Parameters.AddWithValue("@Fax_1", companyModel.Fax_1);
                sqlCommand.Parameters.AddWithValue("@Fax_2", companyModel.Fax_2);
                sqlCommand.Parameters.AddWithValue("@Fax_3", companyModel.Fax_3);
                sqlCommand.Parameters.AddWithValue("@SchoolCompany", companyModel.SchoolCompany);
                sqlCommand.Parameters.AddWithValue("@CompanyTypeID", companyModel.CompanyTypeID >= 0 ? companyModel.CompanyTypeID : (object)DBNull.Value);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return Message;
        }

        /// <summary>
        /// Delete Company Details By Company Id
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public int DeleteCompany(CompanyModel companymodel)
        {
            int numberOfRecords = 0;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_Company", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyId", companymodel.CompanyId);


                numberOfRecords = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return numberOfRecords;
        }


        /// <summary>
        /// Get all companies
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<CompanyModel> GetAllCompany()
        {
            List<CompanyModel> companyModelList = null;
            try
            {
                companyModelList = new List<CompanyModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Company", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CompanyModel companyModel;
                    while (sqlDataReader.Read())
                    {
                        companyModel = new CompanyModel();
                        companyModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        companyModel.name = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString().Trim();
                        companyModel.OrganizationCode = sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString().Trim();
                        companyModel.EmailId = sqlDataReader["EmailId"] == DBNull.Value ? "" : sqlDataReader["EmailId"].ToString().Trim();
                        companyModel.Phone = sqlDataReader["Phone"] == DBNull.Value ? "" : sqlDataReader["Phone"].ToString().Trim();
                        companyModel.ContactPersonName = sqlDataReader["ContactPersonName"] == DBNull.Value ? "" : sqlDataReader["ContactPersonName"].ToString().Trim();
                        companyModel.Address1 = sqlDataReader["Address1"] == DBNull.Value ? "" : sqlDataReader["Address1"].ToString().Trim();
                        companyModel.Address2 = sqlDataReader["Address2"] == DBNull.Value ? "" : sqlDataReader["Address2"].ToString().Trim();
                        companyModel.City = string.IsNullOrEmpty(sqlDataReader["City"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["City"].ToString());
                        companyModel.Country = string.IsNullOrEmpty(sqlDataReader["Country"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Country"].ToString());
                        companyModel.POBOX = sqlDataReader["POBOX"] == DBNull.Value ? "" : sqlDataReader["POBOX"].ToString().Trim();
                        companyModel.CountryName = sqlDataReader["CountryName"] == DBNull.Value ? "" : Convert.ToString(sqlDataReader["CountryName"]).Trim();
                        companyModel.CityName = sqlDataReader["CityName"] == DBNull.Value ? "" : Convert.ToString(sqlDataReader["CityName"]).Trim();
                        companyModel.CompanyHead = string.IsNullOrEmpty(sqlDataReader["CompanyHead"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["CompanyHead"].ToString());
                        companyModel.Assistant1 = string.IsNullOrEmpty(sqlDataReader["Assistant1"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        companyModel.Assistant2 = string.IsNullOrEmpty(sqlDataReader["Assistant2"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        companyModel.Assistant3 = string.IsNullOrEmpty(sqlDataReader["Assistant3"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        companyModelList.Add(companyModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return companyModelList;
        }
        public List<OrganizationModel> GetCompaniesForDropdown(int userId)
        {
            List<OrganizationModel> companyModelList = null;
            try
            {
                companyModelList = new List<OrganizationModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT * FROM HR_FN_GetCompaniesByUserID(@UserId)", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    OrganizationModel companyModel;
                    while (sqlDataReader.Read())
                    {
                        companyModel = new OrganizationModel();
                        companyModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        companyModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        companyModel.CompanyName = sqlDataReader["CompanyName"] == DBNull.Value ? "" : sqlDataReader["CompanyName"].ToString().Trim();
                        companyModelList.Add(companyModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return companyModelList;
        }

        public List<CompanyModel> GetAllCompanyLanguage()
        {
            List<CompanyModel> companyModelList = null;
            try
            {
                companyModelList = new List<CompanyModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Company", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CompanyModel companyModel;
                    while (sqlDataReader.Read())
                    {
                        companyModel = new CompanyModel();
                        companyModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        if (Thread.CurrentThread.CurrentUICulture.Name == "ar")
                        {
                            companyModel.name = sqlDataReader["Name_2"] == DBNull.Value ? "" : sqlDataReader["Name_2"].ToString().Trim();
                        }
                        else
                        {
                            companyModel.name = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString().Trim();
                        }

                        companyModel.OrganizationCode = sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString().Trim();
                        companyModel.EmailId = sqlDataReader["EmailId"] == DBNull.Value ? "" : sqlDataReader["EmailId"].ToString().Trim();
                        companyModel.Phone = sqlDataReader["Phone"] == DBNull.Value ? "" : sqlDataReader["Phone"].ToString().Trim();
                        companyModel.ContactPersonName = sqlDataReader["ContactPersonName"] == DBNull.Value ? "" : sqlDataReader["ContactPersonName"].ToString().Trim();
                        companyModel.Address1 = sqlDataReader["Address1"] == DBNull.Value ? "" : sqlDataReader["Address1"].ToString().Trim();
                        companyModel.Address2 = sqlDataReader["Address2"] == DBNull.Value ? "" : sqlDataReader["Address2"].ToString().Trim();
                        companyModel.City = string.IsNullOrEmpty(sqlDataReader["City"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["City"].ToString());
                        companyModel.Country = string.IsNullOrEmpty(sqlDataReader["Country"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Country"].ToString());
                        companyModel.POBOX = sqlDataReader["POBOX"] == DBNull.Value ? "" : sqlDataReader["POBOX"].ToString().Trim();
                        companyModel.CountryName = sqlDataReader["CountryName"] == DBNull.Value ? "" : Convert.ToString(sqlDataReader["CountryName"]).Trim();
                        companyModel.CityName = sqlDataReader["CityName"] == DBNull.Value ? "" : Convert.ToString(sqlDataReader["CityName"]).Trim();
                        companyModel.CompanyHead = string.IsNullOrEmpty(sqlDataReader["CompanyHead"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["CompanyHead"].ToString());
                        companyModel.Assistant1 = string.IsNullOrEmpty(sqlDataReader["Assistant1"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        companyModel.Assistant2 = string.IsNullOrEmpty(sqlDataReader["Assistant2"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        companyModel.Assistant3 = string.IsNullOrEmpty(sqlDataReader["Assistant3"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        companyModelList.Add(companyModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return companyModelList;
        }
        public DataSet GetAllCompanyDataset()
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_stp_CompanyDetailsExport", sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }

        public List<CompanyModel> GetAllCompanyList()
        {
            List<CompanyModel> companyModelList = null;
            try
            {
                companyModelList = new List<CompanyModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_CompanyList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CompanyModel companyModel;
                    while (sqlDataReader.Read())
                    {
                        companyModel = new CompanyModel();

                        companyModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        companyModel.name = sqlDataReader["name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();
                        companyModel.EmailId = sqlDataReader["EmailId"] == DBNull.Value ? "" : sqlDataReader["EmailId"].ToString();
                        companyModel.Phone = sqlDataReader["Phone"] == DBNull.Value ? "" : sqlDataReader["Phone"].ToString();
                        companyModel.ContactPersonName = sqlDataReader["ContactPersonName"] == DBNull.Value ? "" : sqlDataReader["ContactPersonName"].ToString();
                        companyModel.Address1 = sqlDataReader["Address1"] == DBNull.Value ? "" : sqlDataReader["Address1"].ToString();
                        companyModel.Address2 = sqlDataReader["Address2"] == DBNull.Value ? "" : sqlDataReader["Address2"].ToString();
                        companyModel.City = string.IsNullOrEmpty(sqlDataReader["City"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["City"].ToString());
                        companyModel.Country = sqlDataReader["Country"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Country"].ToString());
                        companyModel.POBOX = sqlDataReader["POBOX"] == DBNull.Value ? "" : sqlDataReader["POBOX"].ToString();
                        companyModel.CompanyHead = sqlDataReader["CompanyHead"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["CompanyHead"].ToString());
                        companyModel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        companyModel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        companyModel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        companyModelList.Add(companyModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return companyModelList;
        }


        public CompanyModel GetCompany(int id)
        {
            CompanyModel companymodel = new CompanyModel();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_CompanyById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyId", id);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        companymodel = new CompanyModel();

                        companymodel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        companymodel.AlternativeCompanyID = Convert.ToString(sqlDataReader["AlternativeCompanyID"]);
                        companymodel.name = Convert.ToString(sqlDataReader["name"]);
                        companymodel.Name_2 = Convert.ToString(sqlDataReader["Name_2"]);
                        companymodel.Name_3 = Convert.ToString(sqlDataReader["Name_3"]);
                        companymodel.OrganizationCode = sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString().Trim();
                        companymodel.ShortName_1 = Convert.ToString(sqlDataReader["ShortName_1"]);
                        companymodel.ShortName_2 = Convert.ToString(sqlDataReader["ShortName_2"]);
                        companymodel.ShortName_3 = Convert.ToString(sqlDataReader["ShortName_3"]);
                        companymodel.CampusName_1 = Convert.ToString(sqlDataReader["CampusName_1"]);
                        companymodel.CampusName_2 = Convert.ToString(sqlDataReader["CampusName_2"]);
                        companymodel.CampusName_3 = Convert.ToString(sqlDataReader["CampusName_3"]);
                        companymodel.Fax_1 = Convert.ToString(sqlDataReader["Fax_1"]);
                        companymodel.Fax_2 = Convert.ToString(sqlDataReader["Fax_2"]);
                        companymodel.Fax_3 = Convert.ToString(sqlDataReader["Fax_3"]);
                        companymodel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        companymodel.ContactPersonName = Convert.ToString(sqlDataReader["ContactPersonName"]);
                        companymodel.Address1 = Convert.ToString(sqlDataReader["Address1"]);
                        companymodel.Address2 = Convert.ToString(sqlDataReader["Address2"]);
                        companymodel.Address_3 = Convert.ToString(sqlDataReader["Address_3"]);
                        companymodel.City = sqlDataReader["City"] == DBNull.Value || Convert.ToString(sqlDataReader["City"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["City"].ToString());
                        companymodel.Country = sqlDataReader["Country"] == DBNull.Value || Convert.ToString(sqlDataReader["Country"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["Country"].ToString());
                        companymodel.POBOX = Convert.ToString(sqlDataReader["POBOX"]);
                        companymodel.Phone = Convert.ToString(sqlDataReader["Phone"]);
                        companymodel.Phone_2 = Convert.ToString(sqlDataReader["Phone_2"]);
                        companymodel.Phone_3 = Convert.ToString(sqlDataReader["Phone_3"]);
                        companymodel.SchoolCompany = sqlDataReader["SchoolCompany"] == DBNull.Value || Convert.ToString(sqlDataReader["SchoolCompany"]) == "" ? false : Convert.ToBoolean(sqlDataReader["SchoolCompany"]);
                        companymodel.CompanyHead = sqlDataReader["CompanyHead"] == DBNull.Value || Convert.ToString(sqlDataReader["CompanyHead"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["CompanyHead"].ToString());
                        companymodel.Assistant1 = sqlDataReader["Assistant1"] == DBNull.Value || Convert.ToString(sqlDataReader["Assistant1"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["Assistant1"].ToString());
                        companymodel.Assistant2 = sqlDataReader["Assistant2"] == DBNull.Value || Convert.ToString(sqlDataReader["Assistant2"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["Assistant2"].ToString());
                        companymodel.Assistant3 = sqlDataReader["Assistant3"] == DBNull.Value || Convert.ToString(sqlDataReader["Assistant3"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["Assistant3"].ToString());
                        companymodel.CompanyTypeID = sqlDataReader["CompanyTypeID"] == DBNull.Value || Convert.ToString(sqlDataReader["CompanyTypeID"]) == "" ? 1 : Convert.ToInt32(sqlDataReader["CompanyTypeID"].ToString());
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return companymodel;
        }

        public bool checkSchoolOrgnization(int checkSchoolOrgnizationId)
        {
            bool isSchoolOrgnization = false;
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_checkSchoolOrgnization", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                int? isCompanyId = (int?)sqlCommand.ExecuteScalar();
                if (isCompanyId == null)
                {
                    isSchoolOrgnization = false;
                }
                else
                {
                    if (isCompanyId == checkSchoolOrgnizationId)
                    {
                        isSchoolOrgnization = false;
                    }
                    else
                    {
                        isSchoolOrgnization = true;
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return isSchoolOrgnization;
        }
    }
}
