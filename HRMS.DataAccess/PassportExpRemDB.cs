﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class PassportExpRemDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PassportExpirationReminderModel> GetAllPassportExpReminder()
        {
            List<PassportExpirationReminderModel> passportExpRemList = new List<PassportExpirationReminderModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllPassportExpReminder", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PassportExpirationReminderModel passportModel;
                    while (sqlDataReader.Read())
                    {
                        passportModel = new PassportExpirationReminderModel();
                        passportModel.PassportExpRemId = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        passportModel.PassportExpRemName = Convert.ToString(sqlDataReader["Name"]);
                        passportExpRemList.Add(passportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return passportExpRemList;
        }
    }
}
