﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class LabourDB : DBHelper
    {
        public OperationDetails InsertLabour(LabourModel LabourModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@DocumentNo",LabourModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", LabourModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", LabourModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(LabourModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(LabourModel.ExpiryDate)) ,
                      new SqlParameter("@Note",LabourModel.Note ) ,
                      new SqlParameter("@IsPrimary", LabourModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", LabourModel.EmployeeId),
                      new SqlParameter("@IsDeleted", LabourModel.IsDeleted),
                      new SqlParameter("@LabourFile", LabourModel.LabourFile)
                    };
                int LabourId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_Labour", parameters));
                return new OperationDetails(true, "Labour saved successfully.", null, LabourId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Labour.", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateLabour(LabourModel LabourModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@DocLabour",LabourModel.DocLabour),
                      new SqlParameter("@DocumentNo",LabourModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", LabourModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", LabourModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(LabourModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(LabourModel.ExpiryDate)) ,
                      new SqlParameter("@Note",LabourModel.Note ) ,
                      new SqlParameter("@IsPrimary", LabourModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", LabourModel.EmployeeId),
                      new SqlParameter("@IsDeleted", LabourModel.IsDeleted),
                      new SqlParameter("@LabourFile", LabourModel.LabourFile)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Labour", parameters);
                return new OperationDetails(true, "Labour updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Labour.", exception);
                throw;
            }
            finally
            {

            }
        }

        public LabourModel LabourById(int LabourId)
        {
            try
            {
                LabourModel LabourModel = new LabourModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_LabourByID", new SqlParameter("@LabourID", LabourId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            LabourModel.DocLabour = Convert.ToInt32(reader["DocLabour"].ToString());
                            LabourModel.DocumentNo = reader["DocumentNo"].ToString();
                            LabourModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            LabourModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            LabourModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                            LabourModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            LabourModel.Note = reader["Note"].ToString();
                            LabourModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            LabourModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            LabourModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            LabourModel.LabourFile = reader["LabourFile"].ToString();
                        }
                    }
                }
                return LabourModel;
                // Finally, we return Labour

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteLabourById(int LabourId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@LabourID",LabourId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_LabourByID", parameters);
                return new OperationDetails(true, "Labour deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Labour.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<LabourModel> GetLabourListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder, string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the Labour	
            List<LabourModel> LabourModelList = new List<LabourModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_LabourWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        LabourModel LabourModel;
                        while (reader.Read())
                        {
                            LabourModel = new LabourModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            LabourModel.DocLabour = Convert.ToInt32(reader["DocLabour"].ToString());
                            LabourModel.DocumentNo = reader["DocumentNo"].ToString();
                            LabourModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            LabourModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            LabourModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            LabourModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            LabourModel.Note = reader["Note"].ToString();
                            LabourModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            LabourModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            LabourModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            LabourModel.LabourFile = reader["LabourFile"].ToString();
                            LabourModelList.Add(LabourModel);
                        }
                    }
                }
                // Finally, we return our list of LabourModelList
                totalCount = TotalCount;
                return LabourModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
