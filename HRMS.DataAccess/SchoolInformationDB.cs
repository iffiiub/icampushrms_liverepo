﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Security.AccessControl;

namespace HRMS.DataAccess
{
    public class SchoolInformationDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public static string HRMSDateFormat
        {
            get { return ConfigurationManager.AppSettings["HRMSDateFormat"]; }
        }
        public SchoolInformation GetSchoolInformation()
        {
            SchoolInformation schoolInfoModel = new SchoolInformation();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_getCompanyInformation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        schoolInfoModel = new SchoolInformation();

                        schoolInfoModel.SchoolID = Convert.ToInt32(sqlDataReader["SchoolID"].ToString());
                        schoolInfoModel.SchoolName_1 = Convert.ToString(sqlDataReader["SchoolName_1"]);
                        schoolInfoModel.SchoolName_2 = Convert.ToString(sqlDataReader["SchoolName_2"]);
                        schoolInfoModel.SchoolName_3 = Convert.ToString(sqlDataReader["SchoolName_3"]);
                        schoolInfoModel.SchoolShortName_1 = Convert.ToString(sqlDataReader["SchoolShortName_1"]);
                        schoolInfoModel.SchoolShortName_2 = Convert.ToString(sqlDataReader["SchoolShortName_2"]);
                        schoolInfoModel.SchoolShortName_3 = Convert.ToString(sqlDataReader["SchoolShortName_3"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return schoolInfoModel;
        }

        public SchoolInformation GetBasicSchoolInformation(bool getFormatedData = false)
        {
            SchoolInformation schoolInfoModel = new SchoolInformation();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetBasicSchoolInformation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        schoolInfoModel = new SchoolInformation();
                        schoolInfoModel.SchoolID = Convert.ToInt32(sqlDataReader["SchoolID"].ToString());
                        schoolInfoModel.Logo = Convert.ToString(sqlDataReader["Logo"]);
                        schoolInfoModel.SchoolName_1 = Convert.ToString(sqlDataReader["SchoolName_1"]);
                        schoolInfoModel.SchoolName_2 = Convert.ToString(sqlDataReader["SchoolName_2"]);
                        schoolInfoModel.CampusName_1 = Convert.ToString(sqlDataReader["CampusName_1"]);
                        schoolInfoModel.CampusName_2 = Convert.ToString(sqlDataReader["CampusName_2"]);
                        schoolInfoModel.Address = Convert.ToString(sqlDataReader["Address_1"]);
                        schoolInfoModel.Address_2 = Convert.ToString(sqlDataReader["Address_2"]);
                        schoolInfoModel.City = Convert.ToString(sqlDataReader["CityName_1"]);
                        schoolInfoModel.City_2 = Convert.ToString(sqlDataReader["CityName_2"]);
                        schoolInfoModel.Country = Convert.ToString(sqlDataReader["CountryName_1"]);
                        schoolInfoModel.Country_2 = Convert.ToString(sqlDataReader["CountryName_2"]);
                        schoolInfoModel.POBox = Convert.ToString(sqlDataReader["Pobox"]);
                        schoolInfoModel.Phone = Convert.ToString(sqlDataReader["Telephone1"]);
                        schoolInfoModel.Phone_2 = Convert.ToString(sqlDataReader["Telephone2"]);
                        schoolInfoModel.Fax = Convert.ToString(sqlDataReader["Fax1"]);
                        schoolInfoModel.Fax_2 = Convert.ToString(sqlDataReader["Fax2"]);
                        schoolInfoModel.EmailId = Convert.ToString(sqlDataReader["Email"]);
                        schoolInfoModel.Website = Convert.ToString(sqlDataReader["WebSite"]);
                        schoolInfoModel.AreaName_1 = Convert.ToString(sqlDataReader["AreaName_1"]);
                        schoolInfoModel.AreaName_2 = Convert.ToString(sqlDataReader["AreaName_2"]);
                        schoolInfoModel.ReportLogo = "~/images/logo_report.jpg";
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (getFormatedData)
                return GetFormatedSchollInformation(schoolInfoModel);
            else
                return schoolInfoModel;
        }

        public DataSet GetBasicSchoolInformationDataset()
        {
            DataSet ds = new DataSet();
            try
            {
                //connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                //sqlConnection = new SqlConnection(connectionString);
                //sqlConnection.Open();
                //sqlCommand = new SqlCommand("HR_GetBasicSchoolInformation", sqlConnection);
                //sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                //if (sqlDataReader.HasRows)
                //{
                //    if (sqlDataReader.Read())
                //    {
                //        schoolInfoModel = new SchoolInformation();
                //        schoolInfoModel.SchoolID = Convert.ToInt32(sqlDataReader["SchoolID"].ToString());
                //        schoolInfoModel.Logo = Convert.ToString(sqlDataReader["Logo"]);
                //        schoolInfoModel.SchoolName_1 = Convert.ToString(sqlDataReader["SchoolName_1"]);
                //        schoolInfoModel.CampusName_1 = Convert.ToString(sqlDataReader["CampusName_1"]);
                //        schoolInfoModel.Address = Convert.ToString(sqlDataReader["Address_1"]);
                //        schoolInfoModel.City = Convert.ToString(sqlDataReader["CityName_1"]);
                //        schoolInfoModel.Country = Convert.ToString(sqlDataReader["CountryName_1"]);
                //        schoolInfoModel.POBox = Convert.ToString(sqlDataReader["Pobox"]);
                //        schoolInfoModel.Phone = Convert.ToString(sqlDataReader["Telephone1"]);
                //        schoolInfoModel.Fax = Convert.ToString(sqlDataReader["Fax1"]);
                //        schoolInfoModel.EmailId = Convert.ToString(sqlDataReader["Email"]);
                //        schoolInfoModel.Website = Convert.ToString(sqlDataReader["WebSite"]);
                //    }
                //}
                //sqlConnection.Close();

                sqlConnection = new SqlConnection(connectionString);
                SqlDataAdapter da = new SqlDataAdapter("HR_GetBasicSchoolInformation", sqlConnection);
                //da.SelectCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                //da.SelectCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                //da.SelectCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                //da.SelectCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
                //da.SelectCommand.Parameters.AddWithValue("@search", search);

                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //da.SelectCommand.Parameters.Add(TotalCount);

                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //DataSet ds = new DataSet();
                da.Fill(ds);
                // return ds;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ds;
        }

        public SchoolInformation GetFormatedSchollInformation(SchoolInformation schoolInfo)
        {
            if (!string.IsNullOrEmpty(schoolInfo.Phone))
            {
                schoolInfo.Phone = "Tel: " + schoolInfo.Phone;
            }
            if (!string.IsNullOrEmpty(schoolInfo.Phone_2))
            {
                schoolInfo.Phone_2 = "هاتف:" + schoolInfo.Phone_2;
            }
            if (!string.IsNullOrEmpty(schoolInfo.Fax))
            {
                schoolInfo.Phone += " / Fax: " + schoolInfo.Fax;
            }
            if (!string.IsNullOrEmpty(schoolInfo.Fax_2))
            {
                schoolInfo.Phone_2 += " / Fax: " + schoolInfo.Fax_2;
            }
            if (!string.IsNullOrEmpty(schoolInfo.POBox))
            {
                schoolInfo.POBox_2 = "ص.ب.: " + schoolInfo.POBox;
            }
            if (!string.IsNullOrEmpty(schoolInfo.POBox))
            {
                schoolInfo.POBox = "P.O.Box:" + schoolInfo.POBox;
            }
            schoolInfo.Logo = schoolInfo.ReportLogo;
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            return schoolInfo;
        }

    }
}
