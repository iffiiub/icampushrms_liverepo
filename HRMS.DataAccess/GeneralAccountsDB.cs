﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;


namespace HRMS.DataAccess
{
    public class GeneralAccountsDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        DataAccess.GeneralDB.DataHelper datahelper;
        public List<GeneralAccounts> GetGeneralAccountList(int? CategoryID,int? AllwanceType,int? CycleID, bool ShowMissingOnly)
        {
            try
            {
                List<GeneralAccounts> generalAccountsList = new List<GeneralAccounts>();
                GeneralAccounts generalAccountModel = null;
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_GetGeneralAccountDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                
                sqlCommand.Parameters.AddWithValue("@CategoryID", CategoryID);
                sqlCommand.Parameters.AddWithValue("@AllwanceType", AllwanceType);
                sqlCommand.Parameters.AddWithValue("@ShowMissingOnly", ShowMissingOnly);
                sqlCommand.Parameters.AddWithValue("@CycleId",CycleID);                
                DataSet objDS = new DataSet();
                SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
                objDA.Fill(objDS);
                sqlConnection.Close();
                int i = 0;
                 if (objDS.Tables[0].Rows.Count > 0)
                 {
                     DataTable payAddition = objDS.Tables[0];

                     foreach (DataRow dr in payAddition.Rows)
                     {
                         generalAccountModel = new GeneralAccounts();
                         generalAccountModel.id = dr["Id"].ToString()==""?0: Convert.ToInt32(dr["Id"].ToString());
                         generalAccountModel.payCategories = dr["CategoryName"].ToString();
                         generalAccountModel.allowanceName = dr["AllowanceName"].ToString();
                         generalAccountModel.payCategoriesId = (dr["CategoryId"].ToString() == "" ? 0 : Convert.ToInt32(dr["CategoryId"].ToString()));
                         generalAccountModel.allowanceId = (dr["AllowanceId"].ToString() == "" ? 0 : Convert.ToInt32(dr["AllowanceId"].ToString()));
                         generalAccountModel.accountCode = dr["AccountCode"].ToString();
                         generalAccountModel.vacationAccountCode = dr["VacctionAccountCode"].ToString();                        
                         generalAccountModel.allowanceType = dr["Type"].ToString() == "" ? 0 : Convert.ToInt32(dr["Type"].ToString());
                         generalAccountModel.accountCodeId= dr["AccountcodeID"].ToString() == "" ? 0 : Convert.ToInt32(dr["AccountcodeID"].ToString());
                         generalAccountModel.vacationAccountCodeId = dr["VacctionAccountCodeID"].ToString() == "" ? 0 : Convert.ToInt32(dr["VacctionAccountCodeID"].ToString());
                        generalAccountModel.autoID = i++;
                        generalAccountsList.Add(generalAccountModel);
                     }
                 }
                return generalAccountsList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }
        
        public IEnumerable<PickList> GetAccountCode()
        {
            datahelper = new GeneralDB.DataHelper();
            List<PickList> AccountCodeList = new List<PickList>();
            DataTable dt = datahelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>(), "Hr_Stp_Select_VacationCode");
            PickList pickmodel;
            foreach (DataRow dr in dt.Rows)
            {
                pickmodel = new PickList();
                pickmodel.id =Convert.ToInt32(dr["id"].ToString());
                pickmodel.text = dr["Vacationcode"].ToString();
                pickmodel.selectedValue = dr["code"].ToString();
                AccountCodeList.Add(pickmodel);
            }
            return AccountCodeList.AsEnumerable();
        }
        public IEnumerable<PickList> GetPayCategories()
        {
            datahelper = new GeneralDB.DataHelper();
            string query = "SELECT * FROM GEN_PayCategories";
            List<PickList> PayCategoriesList = new List<PickList>();
            DataTable dt = datahelper.ExcuteCommandText(query);
            PickList pickmodel;
            foreach (DataRow dr in dt.Rows)
            {
                pickmodel = new PickList();
                pickmodel.id = Convert.ToInt32( dr["CategoryID"].ToString());
                pickmodel.text = dr["CategoryName_1"].ToString();
                PayCategoriesList.Add(pickmodel);
            }
            return PayCategoriesList.AsEnumerable();
        }

        public OperationDetails AddUpdateAccountCode(GeneralAccounts generalAccountModel, int type, int operationType)
        {
            try
            {
                OperationDetails oprationDetails = new OperationDetails();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_AddUpdateAccountCode", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@type", type);
                sqlCommand.Parameters.AddWithValue("@id", generalAccountModel.id);
                sqlCommand.Parameters.AddWithValue("@operationType", operationType);
                sqlCommand.Parameters.AddWithValue("@AccountCodeId", generalAccountModel.accountCodeId);
                sqlCommand.Parameters.AddWithValue("@CategoryId", generalAccountModel.payCategoriesId);
                sqlCommand.Parameters.AddWithValue("@AllowanceId", generalAccountModel.allowanceId);
                sqlCommand.Parameters.AddWithValue("@VacationAccountCodeId", generalAccountModel.vacationAccountCodeId);

                SqlParameter OperationValue = new SqlParameter("@OperationValue", SqlDbType.VarChar, 1000);
                OperationValue.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationValue);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                sqlReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                oprationDetails.InsertedRowId = Convert.ToInt32(OperationValue.Value);
                oprationDetails.Message = OperationMessage.Value.ToString();
                if (oprationDetails.Message == "Success")
                {
                    oprationDetails.CssClass = "success";
                    oprationDetails.Message = "Account code updated successfully";
                }
                else
                {
                    oprationDetails.CssClass = "error";
                    oprationDetails.Message = "Error while updating account code!";
                }
                oprationDetails.Success = true;
                return oprationDetails;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
        }

        public OperationDetails DeleteAccountDetails(int id,int type)
        {
            try
            {
                OperationDetails oprationDetails = new OperationDetails();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_AddUpdateAccountCode", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@type", type);
                sqlCommand.Parameters.AddWithValue("@id",id);
                sqlCommand.Parameters.AddWithValue("@operationType",3);
                
                GeneralAccounts generalAccountModel = new GeneralAccounts();
                sqlCommand.Parameters.AddWithValue("@AccountCodeId", generalAccountModel.accountCodeId);
                sqlCommand.Parameters.AddWithValue("@CategoryId", generalAccountModel.payCategoriesId);
                sqlCommand.Parameters.AddWithValue("@AllowanceId", generalAccountModel.allowanceId);
                sqlCommand.Parameters.AddWithValue("@VacationAccountCodeId", generalAccountModel.vacationAccountCodeId);

                SqlParameter OperationValue = new SqlParameter("@OperationValue", SqlDbType.VarChar, 1000);
                OperationValue.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationValue);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                sqlReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                oprationDetails.InsertedRowId = Convert.ToInt32(OperationValue.Value);
                oprationDetails.Message = OperationMessage.Value.ToString();
                if (oprationDetails.Message == "Success")
                {
                    oprationDetails.CssClass = "success";
                    oprationDetails.Message = "Account code delete successfully";
                }
                else
                {
                    oprationDetails.CssClass = "error";
                    oprationDetails.Message = "Error while deleting account code!";
                }
                oprationDetails.Success = true;
                return oprationDetails;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
        }

    }
}
