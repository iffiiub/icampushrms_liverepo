﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data;
namespace HRMS.DataAccess
{
    class EmployeeLoginDB
    {
        public string connectionString { get; set; }
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        /// <summary>
        /// Add Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public string AddEmployeeLogin(EmployeeLoginModel employeeLoginModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_EmployeeLogin", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeLoginModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Status", employeeLoginModel.Status);
                sqlCommand.Parameters.AddWithValue("@LoginDate", employeeLoginModel.LoginDate);
                sqlCommand.Parameters.AddWithValue("@LogoutDate", employeeLoginModel.LogoutDate);
                sqlCommand.Parameters.AddWithValue("@IpLogin", employeeLoginModel.IpLogin);                
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeLoginModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", employeeLoginModel.ModifiedOn);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Update Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public string UpdateEmployeeLogin(EmployeeLoginModel employeeLoginModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeLoginModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Status", employeeLoginModel.Status);
                sqlCommand.Parameters.AddWithValue("@LoginDate", employeeLoginModel.LoginDate);
                sqlCommand.Parameters.AddWithValue("@LogoutDate", employeeLoginModel.LogoutDate);
                sqlCommand.Parameters.AddWithValue("@IpLogin", employeeLoginModel.IpLogin);    
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeLoginModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", employeeLoginModel.ModifiedOn);
                sqlCommand.Parameters.AddWithValue("@EmployeeLoginId", employeeLoginModel.EmployeeLoginId);
                
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        /// <summary>
        /// Delete Active users details
        /// </summary>
        /// <param name="activeuserlistModel"></param>
        /// <returns></returns>
        public string DeleteEmployeeLogin(EmployeeLoginModel employeeLoginModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_EmployeeLogin", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeLoginId", employeeLoginModel.EmployeeLoginId);                

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        /// <summary>
        /// Get All Active Users
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<EmployeeLoginModel> GetEmployeeLogin(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder)
        {
            List<EmployeeLoginModel> employeeLoginModelList = null;
            try
            {
                employeeLoginModelList = new List<EmployeeLoginModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_EmployeeLogin", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);


                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeLoginModel employeeLoginModel;
                    while (sqlDataReader.Read())
                    {
                        employeeLoginModel = new EmployeeLoginModel();
                        
                        employeeLoginModel.EmployeeLoginId = Convert.ToInt32(sqlDataReader["EmployeeLoginId"].ToString());
                        employeeLoginModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        employeeLoginModel.Status = Convert.ToInt32(sqlDataReader["Status"]);
                        employeeLoginModel.LoginDate = Convert.ToDateTime(sqlDataReader["LoginDate"]);
                        employeeLoginModel.LogoutDate = Convert.ToDateTime(sqlDataReader["LogoutDate"]);                        
                        employeeLoginModel.IpLogin = Convert.ToString(sqlDataReader["IpLogin"].ToString());
                        employeeLoginModel.ModifiedBy = Convert.ToInt32(sqlDataReader["ModifiedBy"].ToString());
                        employeeLoginModel.ModifiedOn = Convert.ToDateTime(sqlDataReader["ModifiedOn"]);
                        employeeLoginModelList.Add(employeeLoginModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeLoginModelList;
        }


    }
}
