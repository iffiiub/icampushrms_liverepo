﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class EmployeeCredentialDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Add Employee Credentials
        /// </summary>
        /// <param name="employeeCredentialModel"></param>
        /// <returns></returns>
        public string AddEmployeeCredential(EmployeeCredentialModel employeeCredentialModel)
        {
            string Message = "";
            try
            {
                int userTypeId = new EmployeeDB().GetEmployeedetailsById(employeeCredentialModel.EmployeeId).usertypeid;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_EmployeeCredential", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeCredentialModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Username", employeeCredentialModel.Username);                
                sqlCommand.Parameters.AddWithValue("@Password", employeeCredentialModel.Password);
                sqlCommand.Parameters.AddWithValue("@IsEnable", employeeCredentialModel.IsEnable);
                sqlCommand.Parameters.AddWithValue("@Createdby", employeeCredentialModel.Createdby);
                sqlCommand.Parameters.AddWithValue("@usertypeid", userTypeId);                
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Update Employee Credentials
        /// </summary>
        /// <param name="employeeCredentialModel"></param>
        /// <returns></returns>
        public string UpdateEmployeeCredential(EmployeeCredentialModel employeeCredentialModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_EmployeeCredential", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeCredentialModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Username", employeeCredentialModel.Username);                
                sqlCommand.Parameters.AddWithValue("@Password", employeeCredentialModel.Password);
                sqlCommand.Parameters.AddWithValue("@IsEnable", employeeCredentialModel.IsEnable);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", employeeCredentialModel.ModifiedBy);                

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Delete Employee Credentials
        /// </summary>
        /// <param name="employeeCredentialModel"></param>
        /// <returns></returns>
        public string DeleteEmployeeCredential(EmployeeCredentialModel employeeCredentialModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_EmployeeCredential", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeCredentialId", employeeCredentialModel.UserId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Get all employee credentials details
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<EmployeeCredentialModel> GetAllEmployeeCredentials()
        {
            List<EmployeeCredentialModel> employeeCredentialList = null;
            try
            {
                employeeCredentialList = new List<EmployeeCredentialModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_EmployeeCredential", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                              
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeCredentialModel employeeCredentialModel;
                    while (sqlDataReader.Read())
                    {
                        employeeCredentialModel = new EmployeeCredentialModel();
                        employeeCredentialModel.UserId = Convert.ToInt32(sqlDataReader["UserID"].ToString());
                        employeeCredentialModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        employeeCredentialModel.Username = Convert.ToString(sqlDataReader["Username"]).Trim();
                        employeeCredentialModel.Password = Convert.ToString(sqlDataReader["Password"]).Trim();
                        employeeCredentialModel.IsEnable = Convert.ToBoolean(sqlDataReader["IsEnable"]);
                        employeeCredentialModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : Convert.ToString(sqlDataReader["EmployeeAlternativeID"]).Trim();
                        employeeCredentialModel.employeeName = sqlDataReader["EmployeeName"].ToString().Trim();
                        employeeCredentialList.Add(employeeCredentialModel);
                    }
                }
                sqlConnection.Close();                
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeCredentialList;
        }

        public DataSet GetAllEmployeeCredentialsDatasSet(int id)
        {
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UserCredentialsExport", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }
        public EmployeeCredentialModel GetEmployeeCredential(int id)
        {
            EmployeeCredentialModel employeecredentialmodel = new EmployeeCredentialModel();

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeCredentialById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@UserId", id);



                // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                // TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        employeecredentialmodel = new EmployeeCredentialModel();

                        employeecredentialmodel.UserId = Convert.ToInt32(sqlDataReader["UserId"].ToString());
                        employeecredentialmodel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"]);
                        employeecredentialmodel.Username = Convert.ToString(sqlDataReader["Username"]);
                        //employeecredentialmodel.NormalPassword = Convert.ToString(sqlDataReader["NormalPassword"]);
                        employeecredentialmodel.Password = Convert.ToString(sqlDataReader["Password"]);
                        employeecredentialmodel.IsEnable = Convert.ToBoolean(sqlDataReader["IsEnable"]);
                        employeecredentialmodel.employeeName = sqlDataReader["EmployeeName"].ToString();
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return employeecredentialmodel;
        }

        public DataTable GetEmployeeCredentialsByUserName(string userName)
        {
            EmployeeCredentialModel objEmployeeModel = new EmployeeCredentialModel();
            try
            {
                DataTable dtEmployeeCredentials = new DataTable();
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeCredentialByUserName", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeUserName", userName);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);// sqlCommand.ExecuteReader();
                sqlDa.Fill(dtEmployeeCredentials);
                return dtEmployeeCredentials;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public bool CheckEmployeeOtherLogin(string userName)
        {
            bool isAlreadyLoggedIn = false;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Check_HR_EmployeeOtherLogin", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeUserName", userName);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    isAlreadyLoggedIn = true;
                }
                return isAlreadyLoggedIn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public string UpdateEmployeePassword(EmployeeCredentialModel objEmployeeCredentialModel)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_Update_HR_EmployeePassword", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeId", objEmployeeCredentialModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Password", objEmployeeCredentialModel.Password);
                sqlCommand.Parameters.AddWithValue("@modifiedBy", objEmployeeCredentialModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", 2);
                SqlParameter InsertionID = new SqlParameter("@OperationId", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while updating password.", exception);               
            }
            finally
            {

            }
            return op.Message;
        }
        public int CheckOldPass(string OldPass, string EmployeeId)
        {
            string Message = "";
            int checkcount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_Update_HR_EmployeePassword", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", Convert.ToInt32(EmployeeId));
                sqlCommand.Parameters.AddWithValue("@Password", OldPass);
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", 1);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.VarChar, 1000);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlParameter OpSuccess = new SqlParameter("@OpSuccess", SqlDbType.VarChar, 1000);
                OpSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpSuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    checkcount = 1;
                }
                else
                {
                    checkcount = 0;
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return checkcount;
        }
    
    }
}
