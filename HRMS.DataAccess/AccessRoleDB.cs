﻿
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class AccessRoleDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<UserGroupModel> GetAllAccessRoles()
        {
            List<UserGroupModel> roleList = new List<UserGroupModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllAccessRoles", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    UserGroupModel roleModel;
                    while (sqlDataReader.Read())
                    {
                        roleModel = new UserGroupModel();
                        roleModel.UserGroupId = Convert.ToInt32(sqlDataReader["UserGroupId"].ToString());
                        roleModel.UserGroupName = Convert.ToString(sqlDataReader["GroupName"]);
                        roleList.Add(roleModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return roleList;
        }      
    }
}
