﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class CityDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<CityModel> GetAllCityByStateId(string StateId)
        {
            List<CityModel> cityList = new List<CityModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllCityByStateId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@StateId", int.Parse(StateId));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CityModel cityModel;
                    while (sqlDataReader.Read())
                    {
                        cityModel = new CityModel();
                        cityModel.CityId = Convert.ToInt32(sqlDataReader["CityID"].ToString());
                        cityModel.CityName = Convert.ToString(sqlDataReader["CityName_1"]);
                        cityList.Add(cityModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return cityList;
        }

        public List<CityModel> GetCityByCountryId(string countryId)
        {
            List<CityModel> cityList = new List<CityModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetCityByCountryId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@countryId", int.Parse(countryId));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CityModel cityModel;
                    while (sqlDataReader.Read())
                    {
                        cityModel = new CityModel();
                        cityModel.CityId = Convert.ToInt32(sqlDataReader["CityID"].ToString());
                        cityModel.CityName = Convert.ToString(sqlDataReader["CityName_1"]);
                        cityList.Add(cityModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return cityList;
        }

        public List<CityModel> GetAllCity()
        {
            List<CityModel> cityList = new List<CityModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllCity", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

              
               
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CityModel cityModel;
                    while (sqlDataReader.Read())
                    {
                        cityModel = new CityModel();
                        cityModel.CityId = Convert.ToInt32(sqlDataReader["CityID"].ToString());
                        cityModel.CityName = Convert.ToString(sqlDataReader["CityName_1"]);
                        cityList.Add(cityModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return cityList;
        }

        public List<BirthLocationModel> GetBirthPlaceByCountryId(string countryId)
        {
            List<BirthLocationModel> BirthLocationList = new List<BirthLocationModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetBirthPlaceByCountryId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@countryId", int.Parse(countryId));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    BirthLocationModel BirthLocationModel;
                    while (sqlDataReader.Read())
                    {
                        BirthLocationModel = new BirthLocationModel();
                        BirthLocationModel.BirthPlaceID = Convert.ToInt32(sqlDataReader["BirthPlaceID"].ToString());
                        BirthLocationModel.BirthPlaceName_1 = Convert.ToString(sqlDataReader["BirthPlacename_1"]);
                        BirthLocationList.Add(BirthLocationModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return BirthLocationList;
        }


    }
}
