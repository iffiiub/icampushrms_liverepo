﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class DailyAttendanceViewDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        public DataAccess.GeneralDB.DataHelper dataHelper;
        //public List<DailyAttendanceViewModel> GetDailyAttendanceViewList(string FromDate, string ToDate, int EmployeeId)
        //{
        //    SqlParameter[] parameters =
        //            {    
        //                new SqlParameter("@attFrom",FromDate),
        //                new SqlParameter("@attTo",ToDate)
        //                //new SqlParameter("@aIntEmployeeID",EmployeeId),
        //                //new SqlParameter("@statusId",-1),
        //                //new SqlParameter("@Proceed",-1)
        //            };
        //    // Create a list to hold the DailyAttendanceView		
        //    List<DailyAttendanceViewModel> DailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();
        //    try
        //    {

        //        SqlHelper.CommandTimeout = 1000;
        //        using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetAttRecordsBydate", parameters))
        //        {
        //            // Check if the reader returned any rows
        //            if (reader.HasRows)
        //            {

        //                // While the reader has rows we loop through them,
        //                // create new PassportModel, and insert them into our list
        //                DailyAttendanceViewModel DailyAttendanceViewModel;
        //                while (reader.Read())
        //                {
        //                    DailyAttendanceViewModel = new DailyAttendanceViewModel();
        //                    DailyAttendanceViewModel.EmployeeName = reader["HRName"] == DBNull.Value ? "" : reader["HRName"].ToString();
        //                    DailyAttendanceViewModel.AttDate = reader["AttDate"] == DBNull.Value ? "" : reader["AttDate"].ToString();
        //                    DailyAttendanceViewModel.HRID = Convert.ToInt32(reader["HRID"] == DBNull.Value ? "0" : reader["HRID"].ToString());
        //                    DailyAttendanceViewModel.HRAltID = Convert.ToInt32(reader["HRAltID"] == DBNull.Value ? "0" : reader["HRAltID"].ToString());
        //                    DailyAttendanceViewModel.HrHireDate = reader["HrHireDate"] == DBNull.Value ? "" : reader["HrHireDate"].ToString();
        //                    DailyAttendanceViewModel.HrInductionDate = reader["HrInductionDate"] == DBNull.Value ? "" : reader["HrInductionDate"].ToString();
        //                    DailyAttendanceViewModel.DeptID = Convert.ToInt32(reader["DepID"] == DBNull.Value ? "0" : reader["DepID"].ToString());
        //                    DailyAttendanceViewModel.DepName = reader["DepName"] == DBNull.Value ? "" : reader["DepName"].ToString();
        //                    DailyAttendanceViewModel.InTime = reader["InTime"] == DBNull.Value ? "" : reader["InTime"].ToString();
        //                    DailyAttendanceViewModel.OutTime = reader["OutTime"] == DBNull.Value ? "" : reader["OutTime"].ToString();
        //                    DailyAttendanceViewModel.HolidayName = reader["HoldayName"] == DBNull.Value ? "" : reader["HoldayName"].ToString();
        //                    DailyAttendanceViewModel.HolidayDate = reader["HoldayDate"] == DBNull.Value ? "" : reader["HoldayDate"].ToString();
        //                    DailyAttendanceViewModel.LateMinutes = Convert.ToInt32(reader["lateMinute"] == DBNull.Value ? "0" : reader["LateMinute"].ToString());
        //                    DailyAttendanceViewModel.EarlyMinutes = Convert.ToInt32(reader["earlyMinute"] == DBNull.Value ? "0" : reader["earlyMinute"].ToString());
        //                    // DailyAttendanceViewModel.DepName = reader["DepName"] == DBNull.Value ? "" : reader["DepName"].ToString());
        //                    DailyAttendanceViewModel.Status = reader["AttStatus"] == DBNull.Value ? "" : reader["AttStatus"].ToString();
        //                    DailyAttendanceViewModel.StatusID = Convert.ToInt32(reader["AttStatusID"] == DBNull.Value ? "-1" : reader["AttStatusID"].ToString());

        //                    DailyAttendanceViewModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"] == DBNull.Value ? "0" : reader["PayEmployeeAbsentID"].ToString());
        //                    DailyAttendanceViewModel.PayEmployeeLateID = Convert.ToInt32(reader["PayEmployeeLateID"] == DBNull.Value ? "0" : reader["PayEmployeeLateID"].ToString());
        //                    DailyAttendanceViewModel.absentExcused = reader["absentExcused"] == DBNull.Value ? false : Convert.ToBoolean(reader["absentExcused"]);
        //                    DailyAttendanceViewModel.LateExcused = reader["LateExcused"] == DBNull.Value ? false : Convert.ToBoolean(reader["LateExcused"]);
        //                    DailyAttendanceViewModel.earlyExcused = reader["earlyExcused"] == DBNull.Value ? false : Convert.ToBoolean(reader["earlyExcused"]);
        //                    DailyAttendanceViewModel.EarlyMinutes = reader["earlyMinute"] == DBNull.Value ? 0 : Convert.ToInt32(reader["earlyMinute"]);
        //                    DailyAttendanceViewModel.LateMinutes = reader["lateMinute"] == DBNull.Value ? 0 : Convert.ToInt32(reader["lateMinute"]);
        //                    DailyAttendanceViewModel.Proceed = reader["Proceed"] == DBNull.Value ? false : Convert.ToBoolean(reader["Proceed"]);
        //                    DailyAttendanceViewModel.IsActive = reader["HRIsactive"] == DBNull.Value ? false : Convert.ToBoolean(reader["HRIsactive"]);
        //                    DailyAttendanceViewModel.SectionID = reader["SectionID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SectionID"]);
        //                    DailyAttendanceViewModel.SectionName = reader["SectionName"] == DBNull.Value ? "" : reader["SectionName"].ToString();

        //                    DailyAttendanceViewModelList.Add(DailyAttendanceViewModel);
        //                }
        //            }
        //        }
        //        return DailyAttendanceViewModelList;
        //    }
        //    catch (Exception exception)
        //    {
        //        // return new OperationDetails(false, "Error : while fetching DailyAttendanceViewModelList.", exception);
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}


        public List<DailyAttendanceViewModel> GetDailyAttendanceViewList(string FromDate, string ToDate, int? EmployeeId, int? statusId = null, bool? Proceed = null,
            bool? active = null, int? aIntDepartmentID = null, int? aIntSectionID = null, int? aIntShiftID = null, int? UserId = null, int? companyId = null)
        {
            List<DailyAttendanceViewModel> DailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_uspGetAttRecordsBydate", sqlConnection);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@attFrom", CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@attTo", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@aVarEmployeeIds", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@statusId", statusId);

                sqlCommand.Parameters.AddWithValue("@Proceed", (Proceed));

                sqlCommand.Parameters.AddWithValue("@active", (active));

                sqlCommand.Parameters.AddWithValue("@aIntDepartmentID", aIntDepartmentID);
                sqlCommand.Parameters.AddWithValue("@aIntSectionID", aIntSectionID);
                sqlCommand.Parameters.AddWithValue("@aIntShiftID", aIntShiftID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@companyId", companyId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        DailyAttendanceViewModel DailyAttendanceViewModel = new DailyAttendanceViewModel();
                        DailyAttendanceViewModel.EmployeeName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                        DailyAttendanceViewModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                        DailyAttendanceViewModel.AttDay = DateTime.Parse(sqlDataReader["AttDate"].ToString()).ToString("dddd"); //Task#9561 2019-02-17
                        DailyAttendanceViewModel.HRID = Convert.ToInt32(sqlDataReader["HRID"] == DBNull.Value ? "0" : sqlDataReader["HRID"].ToString());
                        DailyAttendanceViewModel.HRAltID = sqlDataReader["HRAltID"] == DBNull.Value ? "" : sqlDataReader["HRAltID"].ToString();
                        DailyAttendanceViewModel.HrHireDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HrHireDate"].ToString());
                        DailyAttendanceViewModel.HrInductionDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HrInductionDate"].ToString());
                        DailyAttendanceViewModel.DeptID = Convert.ToInt32(sqlDataReader["DepID"] == DBNull.Value ? "0" : sqlDataReader["DepID"].ToString());
                        DailyAttendanceViewModel.DepName = sqlDataReader["DepName"] == DBNull.Value ? "" : sqlDataReader["DepName"].ToString();
                        DailyAttendanceViewModel.InTime = sqlDataReader["InTime"] == DBNull.Value ? "" : sqlDataReader["InTime"].ToString();
                        DailyAttendanceViewModel.OutTime = sqlDataReader["OutTime"] == DBNull.Value ? "" : sqlDataReader["OutTime"].ToString();
                        DailyAttendanceViewModel.HolidayName = sqlDataReader["HoldayName"] == DBNull.Value ? "" : sqlDataReader["HoldayName"].ToString();
                        DailyAttendanceViewModel.HolidayDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HoldayDate"].ToString());
                        DailyAttendanceViewModel.LateMinutes = Convert.ToInt32(sqlDataReader["lateMinute"] == DBNull.Value ? "0" : sqlDataReader["LateMinute"].ToString());
                        DailyAttendanceViewModel.EarlyMinutes = Convert.ToInt32(sqlDataReader["earlyMinute"] == DBNull.Value ? "0" : sqlDataReader["earlyMinute"].ToString());
                        // DailyAttendanceViewModel.DepName = sqlDataReader["DepName"] == DBNull.Value ? "" : sqlDataReader["DepName"].ToString());
                        DailyAttendanceViewModel.Status = sqlDataReader["AttStatus"] == DBNull.Value ? "" : sqlDataReader["AttStatus"].ToString();
                        DailyAttendanceViewModel.StatusID = Convert.ToInt32(sqlDataReader["AttStatusID"] == DBNull.Value ? "-1" : sqlDataReader["AttStatusID"].ToString());

                        DailyAttendanceViewModel.PayEmployeeAbsentID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentID"] == DBNull.Value ? "0" : sqlDataReader["PayEmployeeAbsentID"].ToString());
                        DailyAttendanceViewModel.PayEmployeeLateID = Convert.ToInt32(sqlDataReader["PayEmployeeLateID"] == DBNull.Value ? "0" : sqlDataReader["PayEmployeeLateID"].ToString());
                        DailyAttendanceViewModel.absentExcused = sqlDataReader["absentExcused"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["absentExcused"]);
                        DailyAttendanceViewModel.LateExcused = sqlDataReader["LateExcused"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["LateExcused"]);
                        DailyAttendanceViewModel.earlyExcused = sqlDataReader["earlyExcused"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["earlyExcused"]);
                        DailyAttendanceViewModel.EarlyMinutes = sqlDataReader["earlyMinute"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["earlyMinute"]);
                        DailyAttendanceViewModel.LateMinutes = sqlDataReader["lateMinute"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["lateMinute"]);
                        DailyAttendanceViewModel.Proceed = sqlDataReader["Proceed"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["Proceed"]);
                        DailyAttendanceViewModel.IsActive = sqlDataReader["HRIsactive"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["HRIsactive"]);
                        DailyAttendanceViewModel.SectionID = sqlDataReader["SectionID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["SectionID"]);
                        DailyAttendanceViewModel.SectionName = sqlDataReader["SectionName"] == DBNull.Value ? "" : sqlDataReader["SectionName"].ToString();

                        DailyAttendanceViewModel.totalHrs = sqlDataReader["TotalHrs"] == DBNull.Value ? "" : sqlDataReader["TotalHrs"].ToString();
                        DailyAttendanceViewModel.shiftHrs = sqlDataReader["ShiftHrs"] == DBNull.Value ? "" : sqlDataReader["ShiftHrs"].ToString();
                        DailyAttendanceViewModel.overHrs = sqlDataReader["OverHrs"] == DBNull.Value ? "" : sqlDataReader["OverHrs"].ToString();
                        DailyAttendanceViewModel.shortHrs = sqlDataReader["ShortHrs"] == DBNull.Value ? "" : sqlDataReader["ShortHrs"].ToString();

                        DailyAttendanceViewModelList.Add(DailyAttendanceViewModel);
                    }

                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                if (!CheckViewIsPresent(new AttendenceSetupDB().GetMachinSettings().TableName))
                {
                    return DailyAttendanceViewModelList;
                }
                throw exception;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DailyAttendanceViewModelList;
        }

        public Task<List<DailyAttendanceViewModel>> GetDailyAttendanceViewListAsync(string FromDate, string ToDate, int? EmployeeId, int? statusId = null, bool? Proceed = null,
       bool? active = null, int? aIntDepartmentID = null, int? aIntSectionID = null, int? aIntShiftID = null, int? UserId = null, int? companyId=null)
        {
            return Task.Run(() =>
            {
                List<DailyAttendanceViewModel> DailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();
                try
                {
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();

                    sqlCommand = new SqlCommand("HR_uspGetAttRecordsBydate", sqlConnection);
                    sqlCommand.CommandTimeout = 9000;
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@attFrom", CommonDB.SetCulturedDate(FromDate));
                    sqlCommand.Parameters.AddWithValue("@attTo", CommonDB.SetCulturedDate(ToDate));
                    sqlCommand.Parameters.AddWithValue("@aVarEmployeeIds", EmployeeId);
                    sqlCommand.Parameters.AddWithValue("@statusId", statusId);

                    sqlCommand.Parameters.AddWithValue("@Proceed", (Proceed));

                    sqlCommand.Parameters.AddWithValue("@active", (active));

                    sqlCommand.Parameters.AddWithValue("@aIntDepartmentID", aIntDepartmentID);
                    sqlCommand.Parameters.AddWithValue("@aIntSectionID", aIntSectionID);
                    sqlCommand.Parameters.AddWithValue("@aIntShiftID", aIntShiftID);
                    sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                    sqlCommand.Parameters.AddWithValue("@companyId", companyId);
                    SqlDataAdapter sda = new SqlDataAdapter(sqlCommand);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    foreach (DataRow sqlDataReader in ds.Tables[0].Rows)
                    {
                        DailyAttendanceViewModel DailyAttendanceViewModel = new DailyAttendanceViewModel();
                        DailyAttendanceViewModel.EmployeeName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                        DailyAttendanceViewModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                        DailyAttendanceViewModel.HRID = Convert.ToInt32(sqlDataReader["HRID"] == DBNull.Value ? "0" : sqlDataReader["HRID"].ToString());
                        DailyAttendanceViewModel.HRAltID = sqlDataReader["HRAltID"] == DBNull.Value ? "" : sqlDataReader["HRAltID"].ToString();
                        DailyAttendanceViewModel.HrHireDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HrHireDate"].ToString());
                        DailyAttendanceViewModel.HrInductionDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HrInductionDate"].ToString());
                        DailyAttendanceViewModel.DeptID = Convert.ToInt32(sqlDataReader["DepID"] == DBNull.Value ? "0" : sqlDataReader["DepID"].ToString());
                        DailyAttendanceViewModel.DepName = sqlDataReader["DepName"] == DBNull.Value ? "" : sqlDataReader["DepName"].ToString();
                        DailyAttendanceViewModel.InTime = sqlDataReader["InTime"] == DBNull.Value ? "" : sqlDataReader["InTime"].ToString();
                        DailyAttendanceViewModel.OutTime = sqlDataReader["OutTime"] == DBNull.Value ? "" : sqlDataReader["OutTime"].ToString();
                        DailyAttendanceViewModel.HolidayName = sqlDataReader["HoldayName"] == DBNull.Value ? "" : sqlDataReader["HoldayName"].ToString();
                        DailyAttendanceViewModel.HolidayDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HoldayDate"].ToString());
                        DailyAttendanceViewModel.LateMinutes = Convert.ToInt32(sqlDataReader["lateMinute"] == DBNull.Value ? "0" : sqlDataReader["LateMinute"].ToString());
                        DailyAttendanceViewModel.EarlyMinutes = Convert.ToInt32(sqlDataReader["earlyMinute"] == DBNull.Value ? "0" : sqlDataReader["earlyMinute"].ToString());
                        // DailyAttendanceViewModel.DepName = sqlDataReader["DepName"] == DBNull.Value ? "" : sqlDataReader["DepName"].ToString());
                        DailyAttendanceViewModel.Status = sqlDataReader["AttStatus"] == DBNull.Value ? "" : sqlDataReader["AttStatus"].ToString();
                        DailyAttendanceViewModel.StatusID = Convert.ToInt32(sqlDataReader["AttStatusID"] == DBNull.Value ? "-1" : sqlDataReader["AttStatusID"].ToString());

                        DailyAttendanceViewModel.PayEmployeeAbsentID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentID"] == DBNull.Value ? "0" : sqlDataReader["PayEmployeeAbsentID"].ToString());
                        DailyAttendanceViewModel.PayEmployeeLateID = Convert.ToInt32(sqlDataReader["PayEmployeeLateID"] == DBNull.Value ? "0" : sqlDataReader["PayEmployeeLateID"].ToString());
                        DailyAttendanceViewModel.absentExcused = sqlDataReader["absentExcused"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["absentExcused"]);
                        DailyAttendanceViewModel.LateExcused = sqlDataReader["LateExcused"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["LateExcused"]);
                        DailyAttendanceViewModel.earlyExcused = sqlDataReader["earlyExcused"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["earlyExcused"]);
                        DailyAttendanceViewModel.EarlyMinutes = sqlDataReader["earlyMinute"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["earlyMinute"]);
                        DailyAttendanceViewModel.LateMinutes = sqlDataReader["lateMinute"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["lateMinute"]);
                        DailyAttendanceViewModel.Proceed = sqlDataReader["Proceed"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["Proceed"]);
                        DailyAttendanceViewModel.IsActive = sqlDataReader["HRIsactive"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["HRIsactive"]);
                        DailyAttendanceViewModel.SectionID = sqlDataReader["SectionID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["SectionID"]);
                        DailyAttendanceViewModel.SectionName = sqlDataReader["SectionName"] == DBNull.Value ? "" : sqlDataReader["SectionName"].ToString();

                        DailyAttendanceViewModel.totalHrs = sqlDataReader["TotalHrs"] == DBNull.Value ? "" : sqlDataReader["TotalHrs"].ToString();
                        DailyAttendanceViewModel.shiftHrs = sqlDataReader["ShiftHrs"] == DBNull.Value ? "" : sqlDataReader["ShiftHrs"].ToString();
                        DailyAttendanceViewModel.overHrs = sqlDataReader["OverHrs"] == DBNull.Value ? "" : sqlDataReader["OverHrs"].ToString();
                        DailyAttendanceViewModel.shortHrs = sqlDataReader["ShortHrs"] == DBNull.Value ? "" : sqlDataReader["ShortHrs"].ToString();

                        DailyAttendanceViewModelList.Add(DailyAttendanceViewModel);
                    }

                }
                catch (Exception exception)
                {
                    if (!CheckViewIsPresent(new AttendenceSetupDB().GetMachinSettings().TableName))
                    {
                        return DailyAttendanceViewModelList;
                    }
                    throw exception;
                }
                finally
                {

                }
                return DailyAttendanceViewModelList;
            });
        }
      
        public List<DailyAttendanceExportModel> GetDailyAttendanceExportList(string FromDate, string ToDate, int? EmployeeId, int? statusId = null, bool? Proceed = null,
           bool? active = null, int? aIntDepartmentID = null, int? aIntSectionID = null, int? aIntShiftID = null, int? UserId = null, int? companyId = null)
        {
            List<DailyAttendanceExportModel> DailyAttendanceViewModelList = new List<DailyAttendanceExportModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_uspGetAttRecordsBydate", sqlConnection);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@attFrom", CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@attTo", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@aVarEmployeeIds", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@statusId", statusId);
                sqlCommand.Parameters.AddWithValue("@Proceed", (Proceed));
                sqlCommand.Parameters.AddWithValue("@active", (active));
                sqlCommand.Parameters.AddWithValue("@aIntDepartmentID", aIntDepartmentID);
                sqlCommand.Parameters.AddWithValue("@aIntSectionID", aIntSectionID);
                sqlCommand.Parameters.AddWithValue("@aIntShiftID", aIntShiftID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@companyId", companyId);
                sqlCommand.Parameters.AddWithValue("@SuperviserID", UserId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        DailyAttendanceExportModel DailyAttendanceViewModel = new DailyAttendanceExportModel();
                        DailyAttendanceViewModel.EmployeeName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                        DailyAttendanceViewModel.AttDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AttDate"].ToString());
                        //DailyAttendanceViewModel.AttDate = DateTime.Parse(sqlDataReader["AttDate"].ToString()).ToString("dddd"); 
                        DailyAttendanceViewModel.EmployeeID = Convert.ToInt32(sqlDataReader["HRID"] == DBNull.Value ? "0" : sqlDataReader["HRID"].ToString());
                        DailyAttendanceViewModel.AlternateEmployeeID = sqlDataReader["HRAltID"] == DBNull.Value ? "" : sqlDataReader["HRAltID"].ToString();
                        DailyAttendanceViewModel.EmployeeName = sqlDataReader["HRName"] == DBNull.Value ? "" : sqlDataReader["HRName"].ToString();
                        DailyAttendanceViewModel.DepartmentName = sqlDataReader["DepName"] == DBNull.Value ? "" : sqlDataReader["DepName"].ToString();
                        DailyAttendanceViewModel.InTime = sqlDataReader["InTime"] == DBNull.Value ? "" : sqlDataReader["InTime"].ToString();
                        DailyAttendanceViewModel.OutTime = sqlDataReader["OutTime"] == DBNull.Value ? "" : sqlDataReader["OutTime"].ToString();
                        DailyAttendanceViewModel.HolidayName = sqlDataReader["HoldayName"] == DBNull.Value ? "" : sqlDataReader["HoldayName"].ToString();
                        DailyAttendanceViewModel.Status = sqlDataReader["AttStatus"] == DBNull.Value ? "" : sqlDataReader["AttStatus"].ToString();
                        DailyAttendanceViewModel.StatusID = Convert.ToInt32(sqlDataReader["AttStatusID"] == DBNull.Value ? "-1" : sqlDataReader["AttStatusID"].ToString());
                        DailyAttendanceViewModel.EarlyMinutes = sqlDataReader["earlyMinute"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["earlyMinute"]);
                        DailyAttendanceViewModel.LateMinutes = sqlDataReader["lateMinute"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["lateMinute"]);
                        DailyAttendanceViewModel.TotalHrs = sqlDataReader["TotalHrs"] == DBNull.Value ? "" : sqlDataReader["TotalHrs"].ToString();
                        DailyAttendanceViewModel.ShiftHrs = sqlDataReader["ShiftHrs"] == DBNull.Value ? "" : sqlDataReader["ShiftHrs"].ToString();
                        DailyAttendanceViewModelList.Add(DailyAttendanceViewModel);
                    }

                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                if (!CheckViewIsPresent(new AttendenceSetupDB().GetMachinSettings().TableName))
                {
                    return DailyAttendanceViewModelList;
                }
                throw exception;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DailyAttendanceViewModelList;
        }

        public List<DailyAttendanceViewModel> sortedList(List<DailyAttendanceViewModel> DailyAttendanceViewModelList)
        {
            DailyAttendanceViewModelList = DailyAttendanceViewModelList.GroupBy(x => new { x.HRID }).Select(y => y.First()).ToList();

            return DailyAttendanceViewModelList;
        }

        public bool CheckViewIsPresent(string viewName)
        {
            try
            {
                string query = "IF EXISTS(select * FROM sys.views where name = '" + viewName + "')" +
                               "begin select * FROM sys.views where name = '" + viewName + "' End";

                dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText(query);

                if (dt.Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }

        public DataTable ExportInDataTable(List<DailyAttendanceViewModel> objDailyAttendanceViewModelList)
        {
            try
            {
                DataTable table = new DataTable();
                table.Columns.Add("Emp Id", typeof(string));
                table.Columns.Add("Employee Name", typeof(string));
                table.Columns.Add("Date", typeof(string));
                table.Columns.Add("Day", typeof(string));//Task#9561
                table.Columns.Add("Status", typeof(string));
                table.Columns.Add("Department", typeof(string));
                table.Columns.Add("Section", typeof(string));
                table.Columns.Add("In Time", typeof(string));
                table.Columns.Add("Out Time", typeof(string));
                table.Columns.Add("Total Hrs", typeof(string));               
                table.Columns.Add("Shift Hrs", typeof(string));
                table.Columns.Add("Mins Late", typeof(string));
                table.Columns.Add("Mins Left Early", typeof(string));
                table.Columns.Add("Total", typeof(string));
                DataRow dr;
                foreach (var item in objDailyAttendanceViewModelList)
                {
                    dr = table.NewRow();
                    dr["Emp Id"] = item.HRAltID;
                    dr["Employee Name"] = item.EmployeeName;
                    dr["Date"] = item.AttDate;
                    dr["Day"] = item.AttDay;//Task#9561
                    dr["Status"] = item.Status;
                    dr["Department"] = item.DepName;
                    dr["Section"] = item.SectionName;
                    dr["In Time"] = item.InTime;
                    dr["Out Time"] = item.OutTime;
                    dr["Total Hrs"] = item.totalHrs;                  
                    dr["Shift Hrs"] = item.shiftHrs;
                    dr["Mins Late"] = Convert.ToString(item.LateMinutes);
                    dr["Mins Left Early"] = Convert.ToString(item.EarlyMinutes);
                    dr["Total"] = Convert.ToString(item.LateMinutes + item.EarlyMinutes);
                    table.Rows.Add(dr);
                }
                return table;
            }
            catch
            {
                return new DataTable();
            }
        }

    }
}
