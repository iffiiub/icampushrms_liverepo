﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class OtherAddressDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        private GeneralDB.DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<OtherAddressModel> GetAllOtherAddressList(int EmployeeId)
        {
            List<OtherAddressModel> otherAddressList = null;
            try
            {
                otherAddressList = new List<OtherAddressModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetAllOtherAddress", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    OtherAddressModel otherAddress;
                    while (sqlDataReader.Read())
                    {
                        otherAddress = new OtherAddressModel();
                        otherAddress.AddressInfoId = Convert.ToInt32(sqlDataReader["AddressInfoId"].ToString());
                        otherAddress.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        otherAddress.ApartmentNo = Convert.ToString(sqlDataReader["ApartmentNo"]);
                        otherAddress.Address = Convert.ToString(sqlDataReader["AddressName"]);                        
                        otherAddress.AddressTypeName = Convert.ToString(sqlDataReader["AddressType"]);
                        otherAddress.PoBox = Convert.ToString(sqlDataReader["PoBox"]);
                        otherAddress.Street = Convert.ToString(sqlDataReader["Street"]);
                        otherAddress.PoBox = Convert.ToString(sqlDataReader["PoBox"]);
                        otherAddress.CountryID = sqlDataReader["CountryID"].ToString()==""?0:Convert.ToInt32(sqlDataReader["CountryID"]);
                        otherAddress.StateID = sqlDataReader["StateID"].ToString()==""?0:Convert.ToInt32(sqlDataReader["StateID"]);
                        otherAddress.CityID = sqlDataReader["CityID"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["CityID"]);
                        otherAddress.AreaId = sqlDataReader["AreaId"].ToString()==""?0:Convert.ToInt32(sqlDataReader["AreaId"]);
                        otherAddress.CountryName = Convert.ToString(sqlDataReader["CountryName"]);
                        otherAddress.StateName = Convert.ToString(sqlDataReader["StateName"]);
                        otherAddress.CityName = Convert.ToString(sqlDataReader["CityName"]);
                        otherAddress.AreaName = Convert.ToString(sqlDataReader["AreaName"]);
                        otherAddressList.Add(otherAddress);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return otherAddressList;
        }


        public DataSet GetAllOtherAddressDataSet(int EmployeeId)
        {
            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_Stp_GetAllOtherAddressExport", sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        /// <summary>
        /// Insert/Update OtherAddressModel
        /// </summary>
        /// <param name="otherAddressModel">OtherAddressModel model</param>
        /// <returns></returns>
        public OperationDetails UpdateOtherAddress(OtherAddressModel otherAddressModel)
        {
            string Message = string.Empty;
            string strNationality = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateEmployeeOtherAddress", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@AddressInfoId", otherAddressModel.AddressInfoId);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", otherAddressModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@AddressName", otherAddressModel.Address);
                sqlCommand.Parameters.AddWithValue("@AddressType", otherAddressModel.AddressType);
                sqlCommand.Parameters.AddWithValue("@AddressLine1", otherAddressModel.AddressLine1);
                sqlCommand.Parameters.AddWithValue("@AddressLine2", otherAddressModel.AddressLine2);
                sqlCommand.Parameters.AddWithValue("@CountryID", otherAddressModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@StateID", otherAddressModel.StateID);             
                sqlCommand.Parameters.AddWithValue("@CityID", otherAddressModel.CityID);
                sqlCommand.Parameters.AddWithValue("@PostCode", otherAddressModel.PostCode);
                sqlCommand.Parameters.AddWithValue("@ApartmentNo", otherAddressModel.ApartmentNo);
                sqlCommand.Parameters.AddWithValue("@Street", otherAddressModel.Street);
                sqlCommand.Parameters.AddWithValue("@PoBox", otherAddressModel.PoBox);                 
                sqlCommand.Parameters.AddWithValue("@AreaId",otherAddressModel.AreaId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);
        }

        public OtherAddressModel GetOtherAddress(int AddressId)
        {
            OtherAddressModel objOtherAddressModel = new OtherAddressModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_EmployeeOtherAddressById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                
                sqlCommand.Parameters.AddWithValue("@OtherAddressId", AddressId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        objOtherAddressModel.AddressInfoId = Convert.ToInt32(sqlDataReader["AddressInfoId"].ToString());
                        objOtherAddressModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        objOtherAddressModel.Address = Convert.ToString(sqlDataReader["AddressName"]);
                        objOtherAddressModel.AddressType = Convert.ToInt32(sqlDataReader["AddressType"]);
                        objOtherAddressModel.ApartmentNo = Convert.ToString(sqlDataReader["ApartmentNo"]);
                        objOtherAddressModel.AddressLine1 = Convert.ToString(sqlDataReader["AddressLine"]);
                        objOtherAddressModel.AddressLine2 = Convert.ToString(sqlDataReader["AddressLine2"]);
                        objOtherAddressModel.Street = Convert.ToString(sqlDataReader["Street"]);
                        objOtherAddressModel.PoBox = Convert.ToString(sqlDataReader["PoBox"]);
                        objOtherAddressModel.PostCode = Convert.ToString(sqlDataReader["PostCode"]);  
                        objOtherAddressModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"]);
                        objOtherAddressModel.StateID = Convert.ToInt32(sqlDataReader["StateID"]);
                        objOtherAddressModel.CityID = Convert.ToInt32(sqlDataReader["CityID"]);
                        objOtherAddressModel.AreaId = Convert.ToInt32(sqlDataReader["AreaId"]);

                        objOtherAddressModel.CountryName = Convert.ToString(sqlDataReader["CountryName"]);
                        objOtherAddressModel.StateName = Convert.ToString(sqlDataReader["StateName"]);
                        objOtherAddressModel.CityName = Convert.ToString(sqlDataReader["CityName"]);
                        objOtherAddressModel.AreaName = Convert.ToString(sqlDataReader["AreaName"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return objOtherAddressModel;
        }

        public OperationDetails DeleteOtherAddressById(int ID)
        {
            try
            {
                dataHelper = new GeneralDB.DataHelper();
                string query = "delete from HR_EmployeeOtherAddress where OtherAddressId=" + ID;
                if (dataHelper.SimpleCommandText(query))
                    return new OperationDetails(true, "Record deleted successfully", null);
                else
                    return new OperationDetails(false, "Technical error has occurred", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
