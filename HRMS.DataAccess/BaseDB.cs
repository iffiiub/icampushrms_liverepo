﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    /// <summary>
    /// Common base class contains the properties realated with SQL database operations
    /// </summary>
    public class BaseDB
    {
        protected SqlConnection sqlConnection { get; set; }
        protected SqlCommand sqlCommand { get; set; }
        protected SqlDataReader sqlReader { get; set; }

        protected string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        protected string connectionStringFileDB = ConfigurationManager.ConnectionStrings["Fileshrmsconnection"].ConnectionString;
    }
}
