﻿using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class UserDB : DBHelper
    {
        /// <summary>
        /// Returns user details by given email and password
        /// </summary>
        /// <param name="email">pass email value</param>
        /// <param name="password">pass password value</param>
        /// <returns>usermodel</returns>
        public UserModel AuthenticateUser(LoginViewModel loginModel)
        {
            UserModel user = null;
            try
            {
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                user = new UserModel();
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@Username","" + loginModel.Username + "") ,
                      new SqlParameter("@Password", "" +loginModel.Password + ""),
                      new SqlParameter("@IsMicrosoftAuthVerified", loginModel.IsMicrosoftAuthVerified),
                       Output,OutputMessage
                    };

                string permissions = string.Empty, viewOnlyPages = string.Empty, addOnlyPages = string.Empty, updateOnlyPages = string.Empty, deleteOnlyPages = string.Empty, otherPermission = string.Empty;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_CheckUserCredentials", parameters))
                {
                    // Check if the reader returned any rows 
                    if (reader.HasRows)
                    {

                        user.UserRoles = new List<string>();
                        while (reader.Read())
                        {
                            user.UserId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            user.Username = Convert.ToString(reader["Username"]);
                            user.IsActive = Convert.ToBoolean(reader["IsEnable"]);
                            user.Password = Convert.ToString(reader["Password"]);
                            user.CompanyId = Convert.ToInt32(reader["CompanyId"]);
                            permissions += reader["Permissions"].ToString() + ",";
                            viewOnlyPages += reader["ViewPagesSubCategoryIDs"].ToString() + ",";
                            addOnlyPages += reader["AddPagesSubCategoryIDs"].ToString() + ",";
                            updateOnlyPages += reader["UpdatePagesSubCategoryIDs"].ToString() + ",";
                            deleteOnlyPages += reader["DeletePagesSubCategoryIDs"].ToString() + ",";
                            otherPermission += reader["OtherPermissionIDs"].ToString() + ",";
                            user.UserRoles.Add(reader["RoleName"].ToString());
                        }
                        user.Permission = GetUniqueIds(permissions);
                        user.ViewPagesSubCategoryIDs = GetUniqueIds(viewOnlyPages);
                        user.AddPagesSubCategoryIDs = GetUniqueIds(addOnlyPages);
                        user.UpdatePagesSubCategoryIDs = GetUniqueIds(updateOnlyPages);
                        user.DeletePagesSubCategoryIDs = GetUniqueIds(deleteOnlyPages);
                        user.OtherPermissionIDs = GetUniqueIds(otherPermission);
                    }
                }

                int output = Convert.ToInt32(Output.Value.ToString());
                if (output > 0)
                {
                    user.IsLoginSuccess = true;
                }
                else
                {
                    user.UserId = -1;
                    user.IsActive = false;
                    user.IsLoginSuccess = false;
                    user.LoginMessage = OutputMessage.Value.ToString();
                }

            }
            catch (Exception exception)
            {
                //throw new Exception("Association Id doesnot match");
                throw exception;
            }
            return user;
        }
        private string GetUniqueIds(string input)
        {
            int temp = 0;
            var uniqueIds = input.Split(',').Where(s => !string.IsNullOrWhiteSpace(s) && int.TryParse(s, out temp)).Select(s => Convert.ToInt32(s)).Distinct().OrderBy(s => s).ToList();
            return string.Join(",", uniqueIds);
        }
        public UserModel GetUserPermissionById(int EmployeeId)
        {
            UserModel user = null;
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@EmployeeId",EmployeeId)
                    };

                string permissions = string.Empty, viewOnlyPages = string.Empty, addOnlyPages = string.Empty, updateOnlyPages = string.Empty, deleteOnlyPages = string.Empty, otherPermission = string.Empty;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetUserAllPermissionById", parameters))
                {
                    // Check if the reader returned any rows 
                    if (reader.HasRows)
                    {
                        user = new UserModel();
                        user.UserRoles = new List<string>();
                        while (reader.Read())
                        {
                            user.UserId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            user.Username = Convert.ToString(reader["Username"]);
                            user.IsActive = Convert.ToBoolean(reader["IsEnable"]);
                            user.Password = Convert.ToString(reader["Password"]);
                            user.CompanyId = Convert.ToInt32(reader["CompanyId"]);
                            permissions += reader["Permissions"].ToString() + ",";
                            viewOnlyPages += reader["ViewPagesSubCategoryIDs"].ToString() + ",";
                            addOnlyPages += reader["AddPagesSubCategoryIDs"].ToString() + ",";
                            updateOnlyPages += reader["UpdatePagesSubCategoryIDs"].ToString() + ",";
                            deleteOnlyPages += reader["DeletePagesSubCategoryIDs"].ToString() + ",";
                            otherPermission += reader["OtherPermissionIDs"].ToString() + ",";
                            user.UserRoles.Add(reader["RoleName"].ToString());
                        }
                        user.Permission = GetUniqueIds(permissions);
                        user.ViewPagesSubCategoryIDs = GetUniqueIds(viewOnlyPages);
                        user.AddPagesSubCategoryIDs = GetUniqueIds(addOnlyPages);
                        user.UpdatePagesSubCategoryIDs = GetUniqueIds(updateOnlyPages);
                        user.DeletePagesSubCategoryIDs = GetUniqueIds(deleteOnlyPages);
                        user.OtherPermissionIDs = GetUniqueIds(otherPermission);
                    }
                }
            }
            catch (Exception exception)
            {
                //throw new Exception("Association Id doesnot match");
                throw;
            }
            return user;
        }

    }
}
