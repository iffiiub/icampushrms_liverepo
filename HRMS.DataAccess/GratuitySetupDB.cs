﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class GratuitySetupDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public DataHelper dataHelper { get; set; }
        public string AddGratuitySetting(string SalaryAllowanceIds, int isStandard, decimal Percentage)
        {
            string Message = "";

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("DELETE FROM HR_GratuitySetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("INSERT INTO HR_GratuitySetting (SalaryAllowancesID,Percentage,IsStandard) VALUES ('" + SalaryAllowanceIds + "'," + Percentage + "," + isStandard + ")", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                if (sqlDataReader.RecordsAffected > 0)
                {
                    Message = "success";
                }
                else
                {
                    Message = "Fail To Insert Record";
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;


        }

        public List<SalaryPackageAllowances> GetGratuitySetting()
        {
            string AllowanceIds = "";
            decimal percent = 0;
            bool IsStandard = false;
            try
            {
                String Query = "SELECT SalaryAllowancesID,Percentage,IsStandard FROM HR_GratuitySetting";
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Query))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {
                            AllowanceIds = reader["SalaryAllowancesID"].ToString();
                            percent = Convert.ToDecimal(reader["Percentage"].ToString());
                            IsStandard = Convert.ToBoolean(reader["IsStandard"]);


                        }
                    }
                }

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
            List<SalaryPackageAllowances> lstAllowances = new List<SalaryPackageAllowances>();
            try
            {
                String Query = "SELECT PaySalaryAllowanceID,PaySalaryAllowanceName_1 FROM GEN_PaySalaryAllowance WHERE PaySalaryAllowanceID IN (" + AllowanceIds + ") ";
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Query))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        SalaryPackageAllowances Allowance;
                        while (reader.Read())
                        {
                            Allowance = new SalaryPackageAllowances();
                            Allowance.id = Convert.ToInt32(reader["PaySalaryAllowanceID"]);
                            Allowance.PaySalaryAllowanceName = reader["PaySalaryAllowanceName_1"].ToString();
                            Allowance.AmountInPercent = percent;
                            Allowance.isSelected = IsStandard;
                            lstAllowances.Add(Allowance);

                        }
                    }
                }
            }
            catch (Exception exception)
            {

            }
            return lstAllowances;
        }

        public OperationDetails CalcGratuity(string EmployeeIds, string ToDate, string EOS)
        {
            string Message = string.Empty;
            int iResult = 0;
            int ActiveRecords = 0;

            OperationDetails op = new OperationDetails();
            try
            {
                // connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                string Query = GetGratuityTemplateSettings().SpName;
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LastDate", CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeIds);
                sqlCommand.Parameters.AddWithValue("@EOSType", EOS);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                op.Message = "Gratuity Calulated Successfully";
                op.Success = true;
                op.CssClass = "success";


            }
            catch (Exception exception)
            {
                op.Message = "Eroor While Calculating Gratuity";
                op.Success = false;
                op.CssClass = "error";
                throw exception;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }



            return op; ;

        }

        public OperationDetails SaveFinalSettlement(EmployeeGratuityModel EmpGrat, string EmployeeIds)
        {
            string Message = string.Empty;
            int iResult = 0;
            int ActiveRecords = 0;

            OperationDetails op = new OperationDetails();
            try
            {
                // connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                string Query = "Hr_Stp_AddGratutityFinalSettlment";
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmpIds", EmployeeIds);
                sqlCommand.Parameters.AddWithValue("@GratuityBeforeNEAmount", EmpGrat.GratuityBeforeNEAmount);
                sqlCommand.Parameters.AddWithValue("@ESOthersAmount", EmpGrat.ESOthersAmount);
                sqlCommand.Parameters.AddWithValue("@CompensatoryLeaveAmount", EmpGrat.CompensatoryLeaveAmount);
                sqlCommand.Parameters.AddWithValue("@GratuityAlreadyPaidAmount", EmpGrat.GratuityAlreadyPaidAmount);
                sqlCommand.Parameters.AddWithValue("@ESOthersDeductionAmount", EmpGrat.ESOthersDeductionAmount);
                sqlCommand.Parameters.AddWithValue("@TaxInclude", EmpGrat.isTaxInclude);
                sqlCommand.Parameters.AddWithValue("@LastWorkingDays", EmpGrat.LastWorkingDays);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                op.Message = "Gratuity Calulated Successfully";
                op.Success = true;
                op.CssClass = "success";


            }
            catch (Exception exception)
            {
                op.Message = "Eroor While Calculating Gratuity";
                op.Success = false;
                op.CssClass = "error";
                throw exception;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }



            return op; ;

        }

        public OperationDetails DeleteGratuityWithFinalSettlement(int EmployeeId)
        {
            string Message = string.Empty;
            int iResult = 0;
            int ActiveRecords = 0;

            OperationDetails op = new OperationDetails();
            try
            {
                // connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                string Query = "Hr_Stp_DeleteGratuityAndFinalSettlment";
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        iResult = Convert.ToInt32(sqlDataReader["AffectedRows"].ToString());
                    }
                }
                if (iResult > 0)
                {
                    op.Message = "Gratuity and Final Settlement deleted successfully";
                    op.Success = true;
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = "Eroor while deleting Gratuity and Final Settlement details";
                    op.Success = false;
                    op.CssClass = "error";
                }


            }
            catch (Exception exception)
            {
                op.Message = "Eroor while deleting Gratuity and Final Settlement details";
                op.Success = false;
                op.CssClass = "error";
                throw exception;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }



            return op; ;

        }
        public List<EmployeeGratuityModel> GetFinalSettlmentDetails()
        {
            List<EmployeeGratuityModel> finalSettlmentList = new List<EmployeeGratuityModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFinalSettlmentList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    EmployeeGratuityModel empGratuityModel;
                    while (reader.Read())
                    {
                        empGratuityModel = new EmployeeGratuityModel();
                        empGratuityModel.GratuityFSID = Convert.ToInt32(reader["GratuityFSID"].ToString());
                        empGratuityModel.EmployeeId = Convert.ToInt32(reader["EmployeeID"].ToString());
                        empGratuityModel.EmpAlternativeID = reader["EmployeeAltID"].ToString();
                        empGratuityModel.EmployeeName = reader["EmployeeName"].ToString();
                        empGratuityModel.GratuityBeforeNEAmount = Convert.ToDecimal(reader["GratuityBeforeNEAmount"].ToString());                        
                        empGratuityModel.ESOthersAmount = Convert.ToDecimal(reader["ESOthersAmount"].ToString());
                        empGratuityModel.CompensatoryLeaveAmount = Convert.ToDecimal(reader["CompensatoryLeaveAmount"].ToString());
                        empGratuityModel.GratuityAlreadyPaidAmount = Convert.ToDecimal(reader["GratuityAlreadyPaidAmount"].ToString());
                        empGratuityModel.ESOthersDeductionAmount = Convert.ToDecimal(reader["ESOthersDeductionAmount"].ToString());

                        finalSettlmentList.Add(empGratuityModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return finalSettlmentList;
        }

        public EmployeeGratuityModel GetFinalSettlmentDetailsbyEmployee(int GratuityFSID)
        {
            EmployeeGratuityModel EmpfinalSettlment = new EmployeeGratuityModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFinalSettlmentListByEmployee", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@GratuityFSID", GratuityFSID);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EmpfinalSettlment = new EmployeeGratuityModel();
                        EmpfinalSettlment.GratuityFSID = Convert.ToInt32(reader["GratuityFSID"].ToString());
                        EmpfinalSettlment.EmpAlternativeID = reader["EmployeeAltID"].ToString();
                        EmpfinalSettlment.EmployeeId = Convert.ToInt32(reader["EmployeeID"].ToString());
                        EmpfinalSettlment.EmployeeName = reader["EmployeeName"].ToString();
                        EmpfinalSettlment.LastWorkingDays = Convert.ToInt32(string.IsNullOrEmpty(reader["LastWorkingDays"].ToString()) ? "0" : reader["LastWorkingDays"].ToString());
                        EmpfinalSettlment.GratuityBeforeNEAmount = Convert.ToDecimal(reader["GratuityBeforeNEAmount"].ToString());                        
                        EmpfinalSettlment.ESOthersAmount = Convert.ToDecimal(reader["ESOthersAmount"].ToString());
                        EmpfinalSettlment.CompensatoryLeaveAmount = Convert.ToDecimal(reader["CompensatoryLeaveAmount"].ToString());
                        EmpfinalSettlment.GratuityAlreadyPaidAmount = Convert.ToDecimal(reader["GratuityAlreadyPaidAmount"].ToString());
                        EmpfinalSettlment.ESOthersDeductionAmount = Convert.ToDecimal(reader["ESOthersDeductionAmount"].ToString());
                        EmpfinalSettlment.EOSType = Convert.ToInt32(reader["EOSType"].ToString() == "Resigned" ? 0 : 1);
                        EmpfinalSettlment.ToDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["LastDate"].ToString());
                        EmpfinalSettlment.isTaxInclude = Convert.ToBoolean(reader["TaxInclude"].ToString());
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return EmpfinalSettlment;
        }
        public string DeleteAllowanceFromSetting(string AllowanceId)
        {
            string Message = "";
            string Allowances = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT SalaryAllowancesID FROM HR_GratuitySetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    // While the reader has rows we loop through them,
                    // create new PassportModel, and insert them into our list

                    while (sqlDataReader.Read())
                    {
                        Allowances = sqlDataReader["SalaryAllowancesID"].ToString();
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            try
            {
                Allowances = Allowances.Replace(AllowanceId + ",", "");
                Allowances = Allowances.Replace("," + AllowanceId, "");
                Allowances = Allowances.Replace(AllowanceId, "");
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Update HR_GratuitySetting SET SalaryAllowancesID='" + Allowances + "' ", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                if (sqlDataReader.RecordsAffected > 0)
                {
                    Message = "success";
                }
                else
                {
                    Message = "Fail Update";

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;


        }

        public bool CheckGratuitySetup()
        {
            int GratCount = 0;
            bool isGratuitySetupAvl = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT COUNT(*) AS GratCount FROM HR_GratuitySetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        GratCount = Convert.ToInt32(sqlDataReader["GratCount"].ToString());
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (GratCount > 0)
            {
                isGratuitySetupAvl = true;
            }
            return isGratuitySetupAvl;
        }

        public string GetEmployeenamesNotHavingSalary(string EmployeeIDs)
        {
            string EmployeeNames = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeNamesNotHavingSalaryAllowances", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeIds", EmployeeIDs);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        EmployeeNames = sqlDataReader["EmployeeNames"].ToString();
                    }
                }

                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeNames;
        }

        public GratuityTemplateSettings GetGratuityTemplateSettings()
        {
            dataHelper = new DataHelper();
            GratuityTemplateSettings gratuityTemplateSettings;
            DataTable Dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>(), "Hr_Stp_GetGratuityTemplateSettings");
            if (Dt.Rows.Count > 0)
            {
                gratuityTemplateSettings = new GratuityTemplateSettings()
                {
                    GratuityTemplateSettingID = Convert.ToInt32(Dt.Rows[0]["GratuityTemplateSettingID"].ToString()),
                    SpName = Dt.Rows[0]["SpName"].ToString()
                };
            }
            else
            {
                gratuityTemplateSettings = new GratuityTemplateSettings();
            }
            return gratuityTemplateSettings;
        }
    }
}
