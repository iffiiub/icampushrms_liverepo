﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class CountryDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<CountryModel> GetAllContries()
        {
            List<CountryModel> countryList = new List<CountryModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllCountries", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CountryModel countryModel;
                    while (sqlDataReader.Read())
                    {
                        countryModel = new CountryModel();
                        countryModel.CountryId = Convert.ToInt32(sqlDataReader["CountryID"].ToString());
                        countryModel.CountryName = Convert.ToString(sqlDataReader["CountryName_1"]);
                        countryList.Add(countryModel);
                    }
                }
            }
            catch (Exception exception)
            {
               throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return countryList;            
        }

        public CountryModel GetCountryByID(int CountryID)
        {
            CountryModel CountryModel = new CountryModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_CountryByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CountryID", CountryID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        CountryModel.CountryId = Convert.ToInt32(sqlDataReader["CountryID"].ToString());
                        CountryModel.CountryName = sqlDataReader["CountryName_1"] == DBNull.Value ? "" : sqlDataReader["CountryName_1"].ToString();

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return CountryModel;
        }
    }
}
