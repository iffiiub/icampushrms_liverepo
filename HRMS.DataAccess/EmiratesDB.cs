﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class EmiratesDB : DBHelper
    {
        public OperationDetails InsertEmirates(EmiratesModel EmiratesModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@DocumentNo",EmiratesModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", EmiratesModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", EmiratesModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(EmiratesModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(EmiratesModel.ExpiryDate)) ,
                      new SqlParameter("@Note",EmiratesModel.Note ) ,
                      new SqlParameter("@IsPrimary", EmiratesModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", EmiratesModel.EmployeeId),
                      new SqlParameter("@IsDeleted", EmiratesModel.IsDeleted),
                      new SqlParameter("@EmiratesFile", EmiratesModel.EmiratesFile)
                    };
                int EmiratesId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_Emirates", parameters));
                return new OperationDetails(true, "Emirates saved successfully.", null, EmiratesId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Emirates.", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateEmirates(EmiratesModel EmiratesModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@DocEmirates",EmiratesModel.DocEmirates),
                      new SqlParameter("@DocumentNo",EmiratesModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", EmiratesModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", EmiratesModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", EmiratesModel.IssueDate),
                      new SqlParameter("@ExpiryDate", EmiratesModel.ExpiryDate ) ,
                      new SqlParameter("@Note",EmiratesModel.Note ) ,
                      new SqlParameter("@IsPrimary", EmiratesModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", EmiratesModel.EmployeeId),
                      new SqlParameter("@IsDeleted", EmiratesModel.IsDeleted),
                      new SqlParameter("@EmiratesFile", EmiratesModel.EmiratesFile)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Emirates", parameters);
                return new OperationDetails(true, "Emirates updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Emirates.", exception);
                throw;
            }
            finally
            {

            }
        }

        public EmiratesModel EmiratesById(int EmiratesId)
        {
            try
            {
                EmiratesModel EmiratesModel = new EmiratesModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_EmiratesByID", new SqlParameter("@EmiratesID", EmiratesId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            EmiratesModel.DocEmirates = Convert.ToInt32(reader["DocEmirates"].ToString());
                            EmiratesModel.DocumentNo = reader["DocumentNo"].ToString();
                            EmiratesModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            EmiratesModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            EmiratesModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            EmiratesModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            EmiratesModel.Note = reader["Note"].ToString();
                            EmiratesModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            EmiratesModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            EmiratesModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            EmiratesModel.EmiratesFile = reader["EmiratesFile"].ToString();
                        }
                    }
                }
                return EmiratesModel;
                // Finally, we return Emirates

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Emirates.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteEmiratesById(int EmiratesId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@EmiratesID",EmiratesId) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_EmiratesByID", parameters);
                return new OperationDetails(true, "Emirates deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Emirates.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<EmiratesModel> GetEmiratesListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder,string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {    
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the Emirates	
            List<EmiratesModel> EmiratesModelList = new List<EmiratesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_EmiratesWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        EmiratesModel EmiratesModel;
                        while (reader.Read())
                        {
                            EmiratesModel = new EmiratesModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            EmiratesModel.DocEmirates = Convert.ToInt32(reader["DocEmirates"].ToString());
                            EmiratesModel.DocumentNo = reader["DocumentNo"].ToString();
                            EmiratesModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            EmiratesModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            EmiratesModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            EmiratesModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            EmiratesModel.Note = reader["Note"].ToString();
                            EmiratesModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            EmiratesModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            EmiratesModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            EmiratesModel.EmiratesFile = reader["EmiratesFile"].ToString();
                            EmiratesModelList.Add(EmiratesModel);
                        }
                    }
                }
                // Finally, we return our list of EmiratesModelList
                totalCount = TotalCount;
                return EmiratesModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Emirates List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
