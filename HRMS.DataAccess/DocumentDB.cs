﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;
using HRMS.Entities;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using System.Web;

namespace HRMS.DataAccess
{
    public class DocumentDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public DataAccess.GeneralDB.DataHelper dataHelper;
        public List<DocumentTypeModel> GetDocumentTypeList()
        {
            List<DocumentTypeModel> docTypeList = null;
            try
            {
                docTypeList = new List<DocumentTypeModel>();
                DocumentTypeModel docTypeObj;
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("SELECT * FROM  GEN_PayDocumentType");
                foreach (DataRow dr in dt.Rows)
                {
                    docTypeObj = new DocumentTypeModel();
                    docTypeObj.docTypeId = Convert.ToInt32(dr["PayDocumenttypeID"].ToString());
                    docTypeObj.docTypeName = dr["PayDocumentTypeDescription_1"].ToString();
                    docTypeObj.IsActive = Convert.ToBoolean(dr["IsActive"] == DBNull.Value ? "false" : dr["IsActive"]);
                    docTypeList.Add(docTypeObj);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return docTypeList.OrderBy(x => x.docTypeName).ToList();
        }

        public List<string> GetDocumentsettingDays()
        {
            List<string> expiryDays = null;
            try
            {
                expiryDays = new List<string>();
                DocumentTypeModel docTypeObj;
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("SELECT * FROM HR_DocumentSettings");
                foreach (DataRow dr in dt.Rows)
                {
                    expiryDays.Add(dr["DocumentsAboutToExpireDays"].ToString());
                    expiryDays.Add(dr["DocumentsExpiredDays"].ToString());
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return expiryDays;
        }

        public List<DocumentsModel> GetDocumentList(int docTyprid, string mode, int days, bool isPrimary, int UserId)
        {
            List<DocumentsModel> documentList = new List<DocumentsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetDocumentList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                sqlCommand.Parameters.AddWithValue("@docTypeId", docTyprid);
                sqlCommand.Parameters.AddWithValue("@days", days);
                sqlCommand.Parameters.AddWithValue("@isPrimary", isPrimary);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    DocumentsModel documentModel;
                    while (reader.Read())
                    {
                        documentModel = new DocumentsModel();
                        documentModel.EmployeeId = reader["EmployeeId"].ToString();
                        documentModel.EmployeeName = reader["EmployeeName"].ToString();
                        documentModel.DocTypeId = Convert.ToInt32(reader["DocTypeId"].ToString());
                        documentModel.DocType = reader["DocType"].ToString();
                        documentModel.DocNo = reader["DocumentNo"].ToString();
                        documentModel.IssueDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                        documentModel.ExpiryDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                        documentModel.IssueCountry = reader["IssueCountry"].ToString();
                        documentModel.IssuePlace = reader["IssuePlace"].ToString();
                        documentModel.FilePath = reader["FilePath"].ToString();
                        documentList.Add(documentModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return documentList;
        }

        public OperationDetails AddEditDocument(DocumentsModel documentsModel, int tranMode)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayDocumentCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", tranMode);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", documentsModel.EmpId);
                sqlCommand.Parameters.AddWithValue("@PayDocumentId", documentsModel.DocId);
                sqlCommand.Parameters.AddWithValue("@PayDocumentTypeID", documentsModel.DocTypeId);
                sqlCommand.Parameters.AddWithValue("@PayDocumentNumber", documentsModel.DocNo);
                sqlCommand.Parameters.AddWithValue("@IssueCityID", documentsModel.IssueCityID);
                sqlCommand.Parameters.AddWithValue("@IssueDate", CommonDB.SetCulturedDate(documentsModel.IssueDate));
                sqlCommand.Parameters.AddWithValue("@ExpireDate", CommonDB.SetCulturedDate(documentsModel.ExpiryDate));
                sqlCommand.Parameters.AddWithValue("@IsPrimary", documentsModel.IsPrimary);
                sqlCommand.Parameters.AddWithValue("@MotherName", documentsModel.MotherName);
                sqlCommand.Parameters.AddWithValue("@IsPassportWithEmployee", documentsModel.IsPassportWithEmployee);
                sqlCommand.Parameters.AddWithValue("@TakenDate", CommonDB.SetCulturedDate(documentsModel.TakenDate));
                sqlCommand.Parameters.AddWithValue("@Reason", documentsModel.Reason);
                sqlCommand.Parameters.AddWithValue("@MustReturn", documentsModel.MustReturn);
                sqlCommand.Parameters.AddWithValue("@ReturnDate", CommonDB.SetCulturedDate(documentsModel.ReturnDate));
                sqlCommand.Parameters.AddWithValue("@SponsorID", documentsModel.SponsorID);
                sqlCommand.Parameters.AddWithValue("@Note", documentsModel.Note);
                sqlCommand.Parameters.AddWithValue("@HRContractTypeID", documentsModel.HRContractTypeID);
                sqlCommand.Parameters.AddWithValue("@IssueCountryID", documentsModel.IssueCountryID);
                sqlCommand.Parameters.AddWithValue("@MOLTitleId", documentsModel.MOLTitleId);
                sqlCommand.Parameters.AddWithValue("@VAcc1", CommonDB.SetCulturedDate(documentsModel.VAcc1));
                sqlCommand.Parameters.AddWithValue("@VAcc2", CommonDB.SetCulturedDate(documentsModel.VAcc2));
                sqlCommand.Parameters.AddWithValue("@VAcc3", CommonDB.SetCulturedDate(documentsModel.VAcc3));
                sqlCommand.Parameters.AddWithValue("@FileName", documentsModel.DocumentFileName);
                sqlCommand.Parameters.AddWithValue("@Filepath", documentsModel.DocumentFile);
                sqlCommand.Parameters.AddWithValue("@DocumentFile", documentsModel.ActualDocumentFile);
                sqlCommand.Parameters.AddWithValue("@FileContentType", documentsModel.FileContentType);
                sqlCommand.Parameters.AddWithValue("@UidNo", documentsModel.UidNo);
                sqlCommand.Parameters.AddWithValue("@Remarks", documentsModel.Remarks);
                sqlCommand.Parameters.AddWithValue("@PersonalNo", documentsModel.PersonalNumber);
                sqlCommand.Parameters.AddWithValue("@PermitCancelled", documentsModel.permitCancelled);
                sqlCommand.Parameters.AddWithValue("@CancellationDate", CommonDB.SetCulturedDate(documentsModel.Cancellationdate));
                sqlCommand.Parameters.AddWithValue("@IsSponsoredBySchool", documentsModel.IsSponsoredBySchool);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@output";
                param.DbType = DbType.Int32;
                param.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(param);

                sqlCommand.ExecuteReader();
                int MunicipalityId = sqlCommand.Parameters["@output"].Value == DBNull.Value || sqlCommand.Parameters["@output"].Value == "" ? 0 : Convert.ToInt32(sqlCommand.Parameters["@output"].Value.ToString());
                sqlConnection.Close();
                if (MunicipalityId > 0)
                {
                    if (tranMode == 1)
                    {
                        op = new OperationDetails(true, "Document saved successfully.", null, MunicipalityId);
                    }
                    else if (tranMode == 2)
                    {
                        op = new OperationDetails(true, "Document updated successfully.", null, MunicipalityId);
                    }
                    else
                    {
                        op = new OperationDetails(true, "Document deleted successfully.", null, MunicipalityId);
                    }
                }
                else
                {
                    if (tranMode == 1)
                    {
                        op = new OperationDetails(false, "Error occurred while saving municipality.", null, MunicipalityId);
                    }
                    else if (tranMode == 2)
                    {
                        op = new OperationDetails(true, "Error occurred while updating municipality.", null, MunicipalityId);
                    }
                    else
                    {
                        op = new OperationDetails(true, "Error occurred while deleting municipality.", null, MunicipalityId);
                    }
                }
                return op;
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Municipality.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public List<DocumentsModel> GetDocumentList(int EmployeeId, int PayDocumentTypeId)
        {
            try
            {
                List<DocumentsModel> lstDocumentList = new List<DocumentsModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeDocuments", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@PayDocumentTypeID", PayDocumentTypeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DocumentsModel documentModel;
                    while (sqlDataReader.Read())
                    {
                        documentModel = new DocumentsModel();
                        documentModel.DocId = Convert.ToInt32(sqlDataReader["PayDocumentID"]);
                        documentModel.DocNo = Convert.ToString(sqlDataReader["PayDocumentNumber"]);
                        documentModel.IssueDate = sqlDataReader["IssueDate"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["IssueDate"]).ToString("dd/MM/yyyy");
                        documentModel.IssuePlace = Convert.ToString(sqlDataReader["IssuePlaceName"]);
                        documentModel.ExpiryDate = sqlDataReader["ExpireDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ExpireDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ExpireDate"]).ToString("dd/MM/yyyy"); ;
                        documentModel.IsPrimary = Convert.ToBoolean(sqlDataReader["IsPrimary"]);
                        documentModel.MotherName = Convert.ToString(sqlDataReader["MotherName"]);
                        documentModel.IsPassportWithEmployee = sqlDataReader["IsPassportWithEmployee"] == DBNull.Value || Convert.ToString(sqlDataReader["IsPassportWithEmployee"]) == "" ? false : Convert.ToBoolean(sqlDataReader["IsPassportWithEmployee"]);
                        documentModel.TakenDate = sqlDataReader["TakenDate"] == DBNull.Value || Convert.ToString(sqlDataReader["TakenDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["TakenDate"]).ToString("dd/MM/yyyy"); ;
                        documentModel.Reason = Convert.ToString(sqlDataReader["Reason"]);
                        documentModel.MustReturn = sqlDataReader["MustReturn"] == DBNull.Value || Convert.ToString(sqlDataReader["MustReturn"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["MustReturn"]);
                        documentModel.ReturnDate = sqlDataReader["ReturnDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ReturnDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ReturnDate"]).ToString("dd/MM/yyyy"); ;
                        documentModel.SponsorID = sqlDataReader["SponsorID"] == DBNull.Value || Convert.ToString(sqlDataReader["SponsorID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["SponsorID"]);
                        documentModel.SponsorName = Convert.ToString(sqlDataReader["SponsorName"]);
                        documentModel.Note = Convert.ToString(sqlDataReader["Note"]);
                        documentModel.HRContractTypeID = sqlDataReader["HRContractTypeID"] == DBNull.Value || Convert.ToString(sqlDataReader["HRContractTypeID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["HRContractTypeID"]);
                        documentModel.IssueCountryID = sqlDataReader["IssueCountryID"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueCountryID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["IssueCountryID"]);
                        documentModel.IssueCountry = Convert.ToString(sqlDataReader["IssueCountry"]);
                        documentModel.MOLTitleId = sqlDataReader["MolTitleId"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["MolTitleId"]);
                        documentModel.MOLTitle = sqlDataReader["MOlTitle"].ToString();
                        documentModel.VAcc1 = sqlDataReader["VAcc1"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc1"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc1"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc2 = sqlDataReader["VAcc2"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc2"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc2"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc3 = sqlDataReader["VAcc3"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc3"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc3"]).ToString("dd/MM/yyyy");
                        documentModel.UidNo = Convert.ToString(sqlDataReader["UidNo"]);
                        documentModel.SponsorPassportNumber = Convert.ToString(sqlDataReader["SponsorPassportNumber"]);
                        documentModel.DocumentFile = Convert.ToString(sqlDataReader["FilePath"]);
                        documentModel.ActualDocumentFile = sqlDataReader["DocumentFile"] == DBNull.Value ? null : (byte[])sqlDataReader["DocumentFile"];
                        lstDocumentList.Add(documentModel);
                    }
                }
                sqlConnection.Close();
                return lstDocumentList;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public DocumentsModel GetDocumentByDocId(int DocId)
        {
            try
            {
                DocumentsModel documentModel = new DocumentsModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDocumentByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aIntPayDocumentID", DocId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        documentModel.DocId = sqlDataReader["PayDocumentID"] == DBNull.Value || Convert.ToString(sqlDataReader["PayDocumentID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["PayDocumentID"]);
                        documentModel.DocNo = Convert.ToString(sqlDataReader["PayDocumentNumber"]);
                        documentModel.DocTypeId = sqlDataReader["PayDocumentTypeID"] == DBNull.Value || Convert.ToString(sqlDataReader["PayDocumentTypeID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["PayDocumentTypeID"]);
                        documentModel.IssueDate = sqlDataReader["IssueDate"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["IssueDate"]).ToString("dd/MM/yyyy");
                        documentModel.IssueCityID = sqlDataReader["IssuePlaceID"] == DBNull.Value || Convert.ToString(sqlDataReader["IssuePlaceID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["IssuePlaceID"]);
                        documentModel.IssuePlace = Convert.ToString(sqlDataReader["IssuePlaceName"]);
                        documentModel.ExpiryDate = sqlDataReader["ExpireDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ExpireDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ExpireDate"]).ToString("dd/MM/yyyy");
                        documentModel.IsPrimary = Convert.ToBoolean(sqlDataReader["IsPrimary"]);
                        documentModel.MotherName = Convert.ToString(sqlDataReader["MotherName"]);
                        documentModel.IsPassportWithEmployee = sqlDataReader["IsPassportWithEmployee"] == DBNull.Value || Convert.ToString(sqlDataReader["IsPassportWithEmployee"]) == "" ? false : Convert.ToBoolean(sqlDataReader["IsPassportWithEmployee"]);
                        documentModel.TakenDate = sqlDataReader["TakenDate"] == DBNull.Value || Convert.ToString(sqlDataReader["TakenDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["TakenDate"]).ToString("dd/MM/yyyy");
                        documentModel.Reason = Convert.ToString(sqlDataReader["Reason"]);
                        documentModel.MustReturn = sqlDataReader["MustReturn"] == DBNull.Value || Convert.ToString(sqlDataReader["MustReturn"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["MustReturn"]);
                        documentModel.ReturnDate = sqlDataReader["ReturnDate"] == DBNull.Value || Convert.ToString(sqlDataReader["ReturnDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["ReturnDate"]).ToString("dd/MM/yyyy");
                        documentModel.SponsorID = sqlDataReader["SponsorID"] == DBNull.Value || Convert.ToString(sqlDataReader["SponsorID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["SponsorID"]);
                        documentModel.SponsorName = Convert.ToString(sqlDataReader["SponsorName"]);
                        documentModel.Note = Convert.ToString(sqlDataReader["Note"]);
                        documentModel.HRContractTypeID = sqlDataReader["HRContractTypeID"] == DBNull.Value || Convert.ToString(sqlDataReader["HRContractTypeID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["HRContractTypeID"]);
                        documentModel.IssueCountryID = sqlDataReader["IssueCountryID"] == DBNull.Value || Convert.ToString(sqlDataReader["IssueCountryID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["IssueCountryID"]);
                        documentModel.IssueCountry = Convert.ToString(sqlDataReader["IssueCountry"]);
                        documentModel.MOLTitleId = sqlDataReader["MolTitleId"].ToString() == "" ? 0 : Convert.ToInt32(sqlDataReader["MolTitleId"]);
                        documentModel.VAcc1 = sqlDataReader["VAcc1"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc1"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc1"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc2 = sqlDataReader["VAcc2"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc2"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc2"]).ToString("dd/MM/yyyy");
                        documentModel.VAcc3 = sqlDataReader["VAcc3"] == DBNull.Value || Convert.ToString(sqlDataReader["VAcc3"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["VAcc3"]).ToString("dd/MM/yyyy");
                        documentModel.DocumentFile = Convert.ToString(sqlDataReader["FilePath"]);
                        documentModel.UidNo = Convert.ToString(sqlDataReader["UidNo"]);
                        documentModel.Remarks = Convert.ToString(sqlDataReader["Remarks"]);
                        documentModel.permitCancelled = Convert.ToBoolean(sqlDataReader["PermitCancelled"]);
                        documentModel.Cancellationdate = sqlDataReader["CancellationDate"] == DBNull.Value || Convert.ToString(sqlDataReader["CancellationDate"]) == "" ? "" : Convert.ToDateTime(sqlDataReader["CancellationDate"]).ToString("dd/MM/yyyy");
                        documentModel.PersonalNumber = Convert.ToString(sqlDataReader["PersonalNumber"]);
                        documentModel.MOLTitle = Convert.ToString(sqlDataReader["MOLTitle"]);
                        documentModel.IsSponsoredBySchool = Convert.ToBoolean(sqlDataReader["SponsoredBySchool"].ToString());
                        documentModel.ActualDocumentFile = sqlDataReader["DocumentFile"] == DBNull.Value ? null : (byte[])sqlDataReader["DocumentFile"];
                        documentModel.DocumentFileName = sqlDataReader["ImageName"] == DBNull.Value || sqlDataReader["ImageName"].ToString() == "" ? "" : sqlDataReader["ImageName"].ToString();
                        documentModel.FileContentType = sqlDataReader["FileContentType"] == DBNull.Value || sqlDataReader["FileContentType"].ToString() == "" ? "" : sqlDataReader["FileContentType"].ToString();
                    }
                }
                sqlConnection.Close();
                return documentModel;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public List<SponsorModel> GetSponsorList(bool? IsActive)
        {
            List<SponsorModel> lstSponsorList = new List<SponsorModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_AllSponsor", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (IsActive != null)
                    sqlCommand.Parameters.AddWithValue("@IsActive", IsActive);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    SponsorModel sponsorModel;
                    while (sqlDataReader.Read())
                    {
                        sponsorModel = new SponsorModel();
                        sponsorModel.SponsorID = Convert.ToInt32(sqlDataReader["SponsorID"]);
                        sponsorModel.SponsorName_1 = Convert.ToString(sqlDataReader["SponsorName_1"]);
                        sponsorModel.SponsorName_2 = Convert.ToString(sqlDataReader["SponsorName_2"]);
                        sponsorModel.SponsorName_3 = Convert.ToString(sqlDataReader["SponsorName_3"]);
                        sponsorModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        lstSponsorList.Add(sponsorModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
            return lstSponsorList;
        }

        public bool isPrimaryDocument(int EmpId, int DocTypeId, int DocId)
        {
            bool isPrimary = false;
            int PayDocumentId = 0;
            try
            {
                DocumentsModel documentModel = new DocumentsModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select PayDocumentId from HR_PayDocument where IsPrimary=1 and PayDocumentTypeID=@DocTypeid and EmployeeID=@EmpId and isdeleted=0", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@DocTypeid", DocTypeId);
                sqlCommand.Parameters.AddWithValue("@EmpId", EmpId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    isPrimary = true;
                    while (sqlDataReader.Read())
                    {
                        PayDocumentId = Convert.ToInt32(sqlDataReader["PayDocumentId"]);
                        if (PayDocumentId == DocId)
                        {
                            isPrimary = false;
                        }
                    }

                }
                sqlConnection.Close();
                return isPrimary;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public List<DocumentDaySettingModel> GetDocumentDaySetting()
        {
            try
            {
                List<DocumentDaySettingModel> documentDaySettingModelList = new List<DocumentDaySettingModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_StpGetDocumentDaysSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DocumentDaySettingModel documentDayModel;
                    while (sqlDataReader.Read())
                    {
                        documentDayModel = new DocumentDaySettingModel();
                        documentDayModel.DocumentTypeId = Convert.ToInt32(sqlDataReader["PayDocumentTypeID"]);
                        documentDayModel.DocumentTypeName = Convert.ToString(sqlDataReader["PayDocumentTypeDescription_1"]);
                        documentDayModel.DocumentsAboutToExpireDays = sqlDataReader["DocumentsAboutToExpireDays"] == DBNull.Value || Convert.ToString(sqlDataReader["DocumentsAboutToExpireDays"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["DocumentsAboutToExpireDays"]);
                        documentDayModel.DocumentExpiredDays = sqlDataReader["DocumentsExpiredDays"] == DBNull.Value || Convert.ToString(sqlDataReader["DocumentsExpiredDays"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["DocumentsExpiredDays"]);
                        documentDaySettingModelList.Add(documentDayModel);
                    }
                }
                sqlConnection.Close();
                return documentDaySettingModelList;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateDocumentDaysSetting(DocumentDaySettingModel documentDaySettingModel)
        {
            dataHelper = new DataHelper();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@DocumentTypeId", documentDaySettingModel.DocumentTypeId));
            sqlParmeterList.Add(new SqlParameter("@DocumentsAboutToExpireDays", documentDaySettingModel.DocumentsAboutToExpireDays));
            sqlParmeterList.Add(new SqlParameter("@DocumentExpriedDays", documentDaySettingModel.DocumentExpiredDays));
            if (dataHelper.ExcutestoredProcedure(sqlParmeterList, "Hr_Stp_CURD_HR_DocumentSettings"))
            {
                return new OperationDetails() { Message = "Document days setting updated successfully", Success = true };
            }
            else
            {
                return new OperationDetails() { Message = "Technical error has occurred", Success = false };
            }
        }

        public OperationDetails CURDSponsor(SponsorModel sponsorModel, int Mode)
        {
            dataHelper = new DataHelper();
            List<SqlParameter> sqlParmeterList = new List<SqlParameter>();
            sqlParmeterList.Add(new SqlParameter("@SponsorId", sponsorModel.SponsorID));
            sqlParmeterList.Add(new SqlParameter("@SponsorName_1", sponsorModel.SponsorName_1));
            sqlParmeterList.Add(new SqlParameter("@SponsorName_2", sponsorModel.SponsorName_2));
            sqlParmeterList.Add(new SqlParameter("@SponsorName_3", sponsorModel.SponsorName_3));
            sqlParmeterList.Add(new SqlParameter("@IsActive", sponsorModel.IsActive));
            sqlParmeterList.Add(new SqlParameter("@Mode", Mode));
            if (dataHelper.ExcutestoredProcedure(sqlParmeterList, "Hr_Stp_CURDSponsor"))
            {
                if (Mode == 1)
                    return new OperationDetails() { Message = "Sponsor added successfully", Success = true, CssClass = "success" };
                else
                {
                    if (Mode == 2)
                        return new OperationDetails() { Message = "Sponsor updated successfully", Success = true, CssClass = "success" };
                    else
                        return new OperationDetails() { Message = "Delete sponsor successfully", Success = true, CssClass = "success" };
                }
            }
            else
            {
                return new OperationDetails() { Message = "Technical error has occurred", Success = false, CssClass = "error" };
            }
        }

        public List<DocumentTypeModel> GetDocumentTypeListIsActive()
        {
            List<DocumentTypeModel> docTypeList = null;
            try
            {
                docTypeList = new List<DocumentTypeModel>();
                DocumentTypeModel docTypeObj;
                DataHelper dataHelper = new DataHelper();
                DataTable dt = dataHelper.ExcuteCommandText("SELECT * FROM  GEN_PayDocumentType where IsActive=1");
                foreach (DataRow dr in dt.Rows)
                {
                    docTypeObj = new DocumentTypeModel();
                    docTypeObj.docTypeId = Convert.ToInt32(dr["PayDocumenttypeID"].ToString());
                    docTypeObj.docTypeName = dr["PayDocumentTypeDescription_1"].ToString();
                    docTypeList.Add(docTypeObj);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return docTypeList.OrderBy(x => x.docTypeName).ToList();
        }

        public List<DocumentsModel> GetAllDocumentsForDownload(string EmpID, string PayDocumentID, bool isPrimary)
        {
            List<DocumentsModel> documentModelList = new List<DocumentsModel>();
            DocumentsModel documentModel = new DocumentsModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DownloadDocuments", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmpID", EmpID);
                sqlCommand.Parameters.AddWithValue("@PayDocumentID", PayDocumentID);
                sqlCommand.Parameters.AddWithValue("@isPrimary", isPrimary);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        documentModel = new DocumentsModel();
                        documentModel.ActualDocumentFile = sqlDataReader["DocumentFile"] == DBNull.Value ? null : (byte[])sqlDataReader["DocumentFile"];
                        documentModel.DocumentFileName = sqlDataReader["ImageName"] == DBNull.Value || sqlDataReader["ImageName"].ToString() == "" ? "" : sqlDataReader["ImageName"].ToString();
                        documentModel.FileContentType = sqlDataReader["FileContentType"] == DBNull.Value || sqlDataReader["FileContentType"].ToString() == "" ? "" : sqlDataReader["FileContentType"].ToString();
                        documentModelList.Add(documentModel);
                    }
                }
                sqlConnection.Close();
                return documentModelList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {

            }
        }

        public int GetSchoolCompanyId()
        {
            int SchoolCompanyId = 0;
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_checkSchoolOrgnization", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                int? isCompanyId = (int?)sqlCommand.ExecuteScalar();
                if (isCompanyId != null)
                {
                    SchoolCompanyId = Convert.ToInt32(isCompanyId);
                }
                else
                {
                    SchoolCompanyId = 0;
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return SchoolCompanyId;
        }

        public EmployeeDocumentModel GetEmployeeDocumentById(int DocId)
        {
            EmployeeDocumentModel employeeDocumentModel = new EmployeeDocumentModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeDocument", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DocId", DocId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        employeeDocumentModel.DocumentFile = sqlDataReader["DocumentFile"] == DBNull.Value ? null : (byte[])sqlDataReader["DocumentFile"];
                        employeeDocumentModel.FileContentType = sqlDataReader["FileContentType"] == DBNull.Value ? "" : sqlDataReader["FileContentType"].ToString();
                        employeeDocumentModel.ImageName = sqlDataReader["ImageName"] == DBNull.Value ? "" : sqlDataReader["ImageName"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
            return employeeDocumentModel;
        }
       
    }
}
