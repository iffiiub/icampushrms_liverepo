﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class WPSDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<WPSTemplateSettingsModel> GetAllWPSTemplates()
        {
            List<WPSTemplateSettingsModel> list = new List<WPSTemplateSettingsModel>();
            WPSTemplateSettingsModel WPSTemplateSettingsModel = new WPSTemplateSettingsModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetAllWPSTemplates", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        WPSTemplateSettingsModel = new WPSTemplateSettingsModel();

                        WPSTemplateSettingsModel.TemplateID = Convert.ToInt32(sqlReader["WPSTemplateSettingsID"].ToString());
                        WPSTemplateSettingsModel.TemplateName = Convert.ToString(sqlReader["TemplateName"]);
                        WPSTemplateSettingsModel.SPName = Convert.ToString(sqlReader["SPName"]);
                        WPSTemplateSettingsModel.Type = Convert.ToString(sqlReader["Type"]);
                        WPSTemplateSettingsModel.FileName = Convert.ToString(sqlReader["FileName"]);
                        list.Add(WPSTemplateSettingsModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }
        public WPSTemplateSettingsModel GetWPSTemplatesById(int templateId)
        {
            WPSTemplateSettingsModel WPSTemplateSettingsModel = new WPSTemplateSettingsModel();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetWPSTemplatesById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TemplateId", templateId);

                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        WPSTemplateSettingsModel = new WPSTemplateSettingsModel();

                        WPSTemplateSettingsModel.TemplateID = Convert.ToInt32(sqlReader["WPSTemplateSettingsID"].ToString());
                        WPSTemplateSettingsModel.TemplateName = Convert.ToString(sqlReader["TemplateName"]);
                        WPSTemplateSettingsModel.SPName = Convert.ToString(sqlReader["SPName"]);
                        WPSTemplateSettingsModel.Type = Convert.ToString(sqlReader["Type"]);
                        WPSTemplateSettingsModel.FileName = Convert.ToString(sqlReader["FileName"]);
                        WPSTemplateSettingsModel.ExcelTableStartIndex = Convert.ToInt32(sqlReader["ExcelTemplateStartIndex"].ToString() == "" ? "1" : sqlReader["ExcelTemplateStartIndex"].ToString());
                        WPSTemplateSettingsModel.MergeHeadersExcelCellsCount = Convert.ToInt32(sqlReader["MergeHeadersExcelCellsCount"] == DBNull.Value ? 0 : sqlReader["MergeHeadersExcelCellsCount"]);
                        WPSTemplateSettingsModel.ExcelColumnNameRowNo = Convert.ToInt32(sqlReader["ExcelColumnNameRowNo"] == DBNull.Value ? 0 : sqlReader["ExcelColumnNameRowNo"]);
                        WPSTemplateSettingsModel.IsSpecificSIFFileFormat = Convert.ToBoolean(sqlReader["IsSpecificSIFFileFormat"]);
                        WPSTemplateSettingsModel.IsFixedTemplate = Convert.ToBoolean(sqlReader["IsFixedTemplate"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return WPSTemplateSettingsModel;
        }
        public DataTable GetDynamicWPSData(int? categoryID, string companyId, int? GroupById, bool? IsPreview, int templateId, string cycleId, string salMonth, string salYear, string dateFrom, string dateTo, string bankIds, string userId)
        {
            DataTable dt = new DataTable();
            bool IsHideExtraFilter = false;
            try
            {
                IsHideExtraFilter = GetIsHideExtraFilter();
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(WPSTemplateSettingsModel.SPName, connectionString);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSalaryMonth", salMonth);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSalaryYear", salYear);
                da.SelectCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                da.SelectCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                da.SelectCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                da.SelectCommand.Parameters.AddWithValue("@aNvrUserID", userId);
                da.SelectCommand.Parameters.AddWithValue("@PayCategory", categoryID == null ? "" : categoryID.ToString());
                if (!IsHideExtraFilter)
                {
                    da.SelectCommand.Parameters.AddWithValue("@CompanyId", companyId);
                    da.SelectCommand.Parameters.AddWithValue("@GroupById", GroupById);
                    da.SelectCommand.Parameters.AddWithValue("@IsPreview", IsPreview);
                }

                da.Fill(dt);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dt;
        }

        public DataTable GetNonWPSData(int templateId, string cycleId, string monthDays, string dateFrom, string dateTo, string bankIds,
            string establishNumber, string bankCodeEmployer, string scrDateFrom, string scrDateTo, string count, string sum, string userId)
        {
            DataTable dt = new DataTable();
            try
            {
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(WPSTemplateSettingsModel.SPName, connectionString);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                da.SelectCommand.Parameters.AddWithValue("@aNvrMonthDays", monthDays);
                da.SelectCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                da.SelectCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                da.SelectCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                da.SelectCommand.Parameters.AddWithValue("@aNvrEstablishNumber", establishNumber);
                da.SelectCommand.Parameters.AddWithValue("@aNvrBankCodeEmployer", bankCodeEmployer);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSCRFromDate", scrDateFrom);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSCRToDate", scrDateTo);
                da.SelectCommand.Parameters.AddWithValue("@aNvrCount", count);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSum", sum);
                da.SelectCommand.Parameters.AddWithValue("@aNvrUserID", userId);
                da.Fill(dt);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dt;
        }

        public List<Template_WPS> GetTemplateData_WPS(int? categoryID, string companyId, int? GroupById, bool? IsPreview, int templateId, string cycleId, string salMonth, string salYear, string dateFrom, string dateTo, string bankIds, string userId)
        {
            List<Template_WPS> list = new List<Template_WPS>();
            Template_WPS objTemplate1 = new Template_WPS();
            bool IsHideExtraFilter = false;
            try
            {
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);
                IsHideExtraFilter = GetIsHideExtraFilter();
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(WPSTemplateSettingsModel.SPName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryMonth", salMonth);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryYear", salYear);
                sqlCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                sqlCommand.Parameters.AddWithValue("@aNvrUserID", userId);
                sqlCommand.Parameters.AddWithValue("@PayCategory", categoryID);
                if (!IsHideExtraFilter)
                {
                    sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                    sqlCommand.Parameters.AddWithValue("@GroupById", GroupById);
                    sqlCommand.Parameters.AddWithValue("@IsPreview", IsPreview);
                }
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        objTemplate1 = new Template_WPS();

                        objTemplate1.EmpId = Convert.ToString(sqlReader["EmpId"].ToString());
                        objTemplate1.Employee_Name = Convert.ToString(sqlReader["Employee_Name"]);
                        objTemplate1.Employee_Type = Convert.ToString(sqlReader["Employee_Type"]);
                        if (objTemplate1.Employee_Type == "0")
                        {
                            objTemplate1.Employee_Type = "";
                        }
                        if ((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue)
                        {
                            objTemplate1.GroupByColumn = Convert.ToString(sqlReader["GroupByColumn"]);
                        }
                        objTemplate1.Salary = Convert.ToString(sqlReader["Salary"]);
                        objTemplate1.VariablePay = Convert.ToString(sqlReader["VariablePay"].ToString());
                        objTemplate1.AccountNo = Convert.ToString(sqlReader["AccountNo"]);
                        objTemplate1.AGENT_BANK_RTN_CODE = Convert.ToString(sqlReader["AGENT_BANK_RTN_CODE"]);
                        objTemplate1.MOL_PERSONID = Convert.ToString(sqlReader["MOL_PERSONID"].ToString());
                        objTemplate1.Sal_Month = Convert.ToString(sqlReader["Sal_Month"]);
                        objTemplate1.Sal_Year = Convert.ToString(sqlReader["Sal_Year"]);
                        objTemplate1.FromDate = Convert.ToString(sqlReader["FromDate"].ToString());
                        objTemplate1.ToDate = Convert.ToString(sqlReader["ToDate"]);
                        objTemplate1.Leave = Convert.ToString(sqlReader["Leave"]);

                        list.Add(objTemplate1);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public List<Template_NonWPS> GetTemplateData_NonWPS(int? categoryID, string companyId, int? GroupById, bool? IsPreview, int templateId, string cycleId, string monthDays, string dateFrom, string dateTo, string bankIds,
            string establishNumber, string bankCodeEmployer, string scrDateFrom, string scrDateTo, string count, string sum, string userId)
        {
            List<Template_NonWPS> list = new List<Template_NonWPS>();
            Template_NonWPS objTemplate1 = new Template_NonWPS();
            bool IsHideExtraFilter = false;
            try
            {
                IsHideExtraFilter = GetIsHideExtraFilter();
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(WPSTemplateSettingsModel.SPName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                sqlCommand.Parameters.AddWithValue("@aNvrMonthDays", monthDays);
                sqlCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                sqlCommand.Parameters.AddWithValue("@aNvrEstablishNumber", establishNumber);
                sqlCommand.Parameters.AddWithValue("@aNvrBankCodeEmployer", bankCodeEmployer);
                sqlCommand.Parameters.AddWithValue("@aNvrSCRFromDate", scrDateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrSCRToDate", scrDateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrCount", count);
                sqlCommand.Parameters.AddWithValue("@aNvrSum", sum);
                sqlCommand.Parameters.AddWithValue("@aNvrUserID", userId);
                sqlCommand.Parameters.AddWithValue("@PayCategory", categoryID);
                if (!IsHideExtraFilter)
                {
                    sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                    sqlCommand.Parameters.AddWithValue("@GroupById", GroupById);
                    sqlCommand.Parameters.AddWithValue("@IsPreview", IsPreview);
                }
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        objTemplate1 = new Template_NonWPS();

                        objTemplate1.BankCode = Convert.ToString(sqlReader["Bank Code"].ToString());
                        objTemplate1.TranType = Convert.ToString(sqlReader["Tran TYPE"]);
                        objTemplate1.BeneficiaryName = Convert.ToString(sqlReader["Beneficary_Name"]);
                        objTemplate1.BeneficiaryACIBAN = Convert.ToString(sqlReader["Beneficiary A/c IBAN"]);
                        objTemplate1.AmountInAED = Convert.ToString(sqlReader["Amount in AED"].ToString());
                        objTemplate1.PaymentDetails = Convert.ToString(sqlReader["Payment Details"]);
                        if ((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue)
                        {
                            objTemplate1.GroupByColumn = Convert.ToString(sqlReader["GroupByColumn"]);
                        }
                        list.Add(objTemplate1);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }


        public List<Template_NonWPSCBD> GetTemplateData_NonWPSCBD(int? categoryID, string companyId, int? GroupById, bool? IsPreview, int templateId, string cycleId, string salMonth, string salYear, string dateFrom, string dateTo, string bankIds, string userId)
        {
            List<Template_NonWPSCBD> list = new List<Template_NonWPSCBD>();
            Template_NonWPSCBD objTemplate1 = new Template_NonWPSCBD();
            bool IsHideExtraFilter = false;
            try
            {
                IsHideExtraFilter = GetIsHideExtraFilter();
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(WPSTemplateSettingsModel.SPName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryMonth", salMonth);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryYear", salYear);
                sqlCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                sqlCommand.Parameters.AddWithValue("@aNvrUserID", userId);
                sqlCommand.Parameters.AddWithValue("@PayCategory", categoryID);
                if (!IsHideExtraFilter)
                {
                    sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                    sqlCommand.Parameters.AddWithValue("@GroupById", GroupById);
                    sqlCommand.Parameters.AddWithValue("@IsPreview", IsPreview);
                }
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        objTemplate1 = new Template_NonWPSCBD();

                        objTemplate1.EmpId = Convert.ToString(sqlReader["EmpId"].ToString());
                        objTemplate1.Employee_Name = Convert.ToString(sqlReader["Employee_Name"]);
                        objTemplate1.BankAccount = Convert.ToString(sqlReader["AccountNo"]);
                        objTemplate1.Salary = Convert.ToString(sqlReader["Salary"]);
                        if ((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue)
                        {
                            objTemplate1.GroupByColumn = Convert.ToString(sqlReader["GroupByColumn"]);
                        }
                        list.Add(objTemplate1);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public DataTable GetTemplateData_ExternalFileData(int? categoryID, string companyId, int? GroupById, bool? IsPreview, int templateId, string cycleId, string salMonth, string salYear, string dateFrom, string dateTo, string bankIds, string userId)
        {
            DataTable dt = new DataTable();
            bool IsHideExtraFilter = false;
            try
            {
                IsHideExtraFilter = GetIsHideExtraFilter();
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(WPSTemplateSettingsModel.SPName, connectionString);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                da.SelectCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSalaryMonth", salMonth);
                da.SelectCommand.Parameters.AddWithValue("@aNvrSalaryYear", salYear);
                da.SelectCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                da.SelectCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                da.SelectCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                da.SelectCommand.Parameters.AddWithValue("@aNvrUserID", userId);
                da.SelectCommand.Parameters.AddWithValue("@PayCategory", categoryID);
                if (!IsHideExtraFilter)
                {
                    da.SelectCommand.Parameters.AddWithValue("@CompanyId", companyId);
                    da.SelectCommand.Parameters.AddWithValue("@GroupById", GroupById);
                    da.SelectCommand.Parameters.AddWithValue("@IsPreview", IsPreview);
                }
                da.Fill(dt);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dt;
        }

        /*
        public List<Template1> GetWPSData_Template1(int templateId, string cycleId, string salMonth, string salYear, string dateFrom, string dateTo, string bankIds, string userId)
        {
            List<Template1> list = new List<Template1>();
            Template1 objTemplate1 = new Template1();            
            try
            {
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(WPSTemplateSettingsModel.SPName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryMonth", salMonth);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryYear", salYear);
                sqlCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                sqlCommand.Parameters.AddWithValue("@aNvrUserID", userId);

                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        objTemplate1 = new Template1();

                        objTemplate1.EmpId = Convert.ToString(sqlReader["EmpId"].ToString());
                        objTemplate1.Employee_Name = Convert.ToString(sqlReader["Employee_Name"]);
                        objTemplate1.Salary = Convert.ToString(sqlReader["Salary"]);
                        objTemplate1.VariablePay = Convert.ToString(sqlReader["VariablePay"].ToString());
                        objTemplate1.AccountNo = Convert.ToString(sqlReader["AccountNo"]);
                        objTemplate1.AGENT_BANK_RTN_CODE = Convert.ToString(sqlReader["AGENT_BANK_RTN_CODE"]);
                        objTemplate1.MOL_PERSONID = Convert.ToString(sqlReader["MOL_PERSONID"].ToString());
                        objTemplate1.Sal_Month = Convert.ToString(sqlReader["Sal_Month"]);
                        objTemplate1.Sal_Year = Convert.ToString(sqlReader["Sal_Year"]);
                        objTemplate1.FromDate = Convert.ToString(sqlReader["FromDate"].ToString());
                        objTemplate1.ToDate = Convert.ToString(sqlReader["ToDate"]);
                        objTemplate1.Leave = Convert.ToString(sqlReader["Leave"]);

                        list.Add(objTemplate1);
                    }
                }
                sqlConnection.Close();                
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public List<Template1> GetWPSData_Template2(int templateId, string cycleId, string monthDays, string dateFrom, string dateTo, string bankIds,
            string establishNumber, string bankCodeEmployer, string scrDateFrom, string scrDateTo, string count, string sum, string userId)
        {
            List<Template1> list = new List<Template1>();
            Template1 objTemplate1 = new Template1();
            try
            {
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand(WPSTemplateSettingsModel.SPName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                sqlCommand.Parameters.AddWithValue("@aNvrMonthDays", monthDays);
                sqlCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                sqlCommand.Parameters.AddWithValue("@aNvrEstablishNumber", establishNumber);
                sqlCommand.Parameters.AddWithValue("@aNvrBankCodeEmployer", bankCodeEmployer);
                sqlCommand.Parameters.AddWithValue("@aNvrSCRFromDate", scrDateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrSCRToDate", scrDateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrCount", count);
                sqlCommand.Parameters.AddWithValue("@aNvrSum", sum);
                sqlCommand.Parameters.AddWithValue("@aNvrUserID", userId);

                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        objTemplate1 = new Template1();

                        objTemplate1.EmpId = Convert.ToString(sqlReader["EmpId"].ToString());
                        objTemplate1.Employee_Name = Convert.ToString(sqlReader["Employee_Name"]);
                        objTemplate1.Salary = Convert.ToString(sqlReader["Salary"]);
                        objTemplate1.VariablePay = Convert.ToString(sqlReader["VariablePay"].ToString());
                        objTemplate1.AccountNo = Convert.ToString(sqlReader["AccountNo"]);
                        objTemplate1.AGENT_BANK_RTN_CODE = Convert.ToString(sqlReader["AGENT_BANK_RTN_CODE"]);
                        objTemplate1.MOL_PERSONID = Convert.ToString(sqlReader["MOL_PERSONID"].ToString());
                        objTemplate1.Sal_Month = Convert.ToString(sqlReader["Sal_Month"]);
                        objTemplate1.Sal_Year = Convert.ToString(sqlReader["Sal_Year"]);
                        objTemplate1.FromDate = Convert.ToString(sqlReader["FromDate"].ToString());
                        objTemplate1.ToDate = Convert.ToString(sqlReader["ToDate"]);
                        objTemplate1.Leave = Convert.ToString(sqlReader["Leave"]);

                        list.Add(objTemplate1);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public List<Template3> GetWPSData_Template3(int templateId, string cycleId, string salMonth, string salYear, string dateFrom, string dateTo, string bankIds, string userId)
        {
            List<Template3> list = new List<Template3>();
            Template3 objTemplate3 = new Template3();
            try
            {
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand(WPSTemplateSettingsModel.SPName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aNvrCycleID", cycleId);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryMonth", salMonth);
                sqlCommand.Parameters.AddWithValue("@aNvrSalaryYear", salYear);
                sqlCommand.Parameters.AddWithValue("@aNvrDateFrom", dateFrom);
                sqlCommand.Parameters.AddWithValue("@aNvrDateTo", dateTo);
                sqlCommand.Parameters.AddWithValue("@aNvrBankIds", bankIds);
                sqlCommand.Parameters.AddWithValue("@aNvrUserID", userId);

                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        objTemplate3 = new Template3();

                        objTemplate3.EmployeeID = Convert.ToString(sqlReader["EmployeeID"]);
                        objTemplate3.EmpName = Convert.ToString(sqlReader["EmpName"]);
                        objTemplate3.Salary = Convert.ToString(sqlReader["Salary"]);
                        objTemplate3.addition = Convert.ToString(sqlReader["addition"].ToString());
                        objTemplate3.BankName_1 = Convert.ToString(sqlReader["BankName_1"]);
                        objTemplate3.AccountNumber = Convert.ToString(sqlReader["AccountNumber"]);
                        objTemplate3.RtnCode = Convert.ToString(sqlReader["RtnCode"].ToString());
                        objTemplate3.MOLPersonalID = Convert.ToString(sqlReader["MOLPersonalID"]);

                        list.Add(objTemplate3);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }
        */

        public object isPDFEnabled(int templateId)
        {
            bool PDFEnabled = false;
            bool SifEnabled = false;
            try
            {
                //To get sp name
                WPSTemplateSettingsModel WPSTemplateSettingsModel = GetWPSTemplatesById(templateId);

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select IsPdfReport, IsSifReport from HR_WPSTemplateSettings where WPSTemplateSettingsID=@TemplateId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@TemplateId", templateId);
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        PDFEnabled = Convert.ToBoolean(sqlReader["IsPdfReport"].ToString());
                        SifEnabled = Convert.ToBoolean(sqlReader["IsSifReport"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return new { PDFEnabled = PDFEnabled, SifEnabled = SifEnabled };
        }

        public bool GetIsHideExtraFilter()
        {
            bool IsHideExtraFilter = false;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetIsHideExtraFilter", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                IsHideExtraFilter = Convert.ToBoolean(sqlCommand.ExecuteScalar());
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsHideExtraFilter;
        }
    }
}
