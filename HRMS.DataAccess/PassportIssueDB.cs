﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class PassportIssueDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PassportIssueModel> GetAllPassportIssueBy()
        {
            List<PassportIssueModel> passportIssueList = new List<PassportIssueModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllPassportIssueBy", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PassportIssueModel passportModel;
                    while (sqlDataReader.Read())
                    {
                        passportModel = new PassportIssueModel();
                        passportModel.PassportIssueId = Convert.ToInt32(sqlDataReader["PassportIssueId"].ToString());
                        passportModel.PassportIssueName = Convert.ToString(sqlDataReader["PassportIssueName"]);
                        passportIssueList.Add(passportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return passportIssueList;
        }
    }
}
