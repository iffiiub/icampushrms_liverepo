﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace HRMS.DataAccess
{
    public class ReportBuilderDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<ReportBuilderFieldsModel> GetReportFieldsData(string searchValue = "")
        {
            List<ReportBuilderFieldsModel> list = new List<ReportBuilderFieldsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetReportFieldsData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SearchField", searchValue);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ReportBuilderFieldsModel model;
                    while (sqlDataReader.Read())
                    {
                        model = new ReportBuilderFieldsModel();
                        model.PayReportBuilderFieldsID = Convert.ToInt32(sqlDataReader["PayReportBuilderFieldsID"].ToString());
                        model.description = Convert.ToString(sqlDataReader["description"]);
                        model.sqlname = Convert.ToString(sqlDataReader["sqlname"]);
                        model.sqlID = Convert.ToString(sqlDataReader["sqlID"]);
                        list.Add(model);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public ReportBuilderFieldsModel GetReportFieldsDataById(int payReportBuilderFieldsID)
        {
            ReportBuilderFieldsModel model = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetReportFieldsDataById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayReportBuilderFieldsID", payReportBuilderFieldsID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        model = new ReportBuilderFieldsModel();
                        model.PayReportBuilderFieldsID = Convert.ToInt32(sqlDataReader["PayReportBuilderFieldsID"].ToString());
                        model.description = Convert.ToString(sqlDataReader["description"]);
                        model.sqlname = Convert.ToString(sqlDataReader["sqlname"]);
                        model.sqlID = Convert.ToString(sqlDataReader["sqlID"]);
                        model.sqlTable = Convert.ToString(sqlDataReader["sqlTable"]);
                        model.sqlTableDescription = Convert.ToString(sqlDataReader["sqlTableDescription"]);
                        model.sqlTableID = Convert.ToString(sqlDataReader["sqlTableID"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return model;
        }

        public DataTable GetWhereConditionValues(int payReportBuilderFieldsID, out string textField, out string valueField)
        {
            DataTable dt = new DataTable();
            textField = valueField = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                SqlDataAdapter da = new SqlDataAdapter("HR_stp_GetWhereConditionValues", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@PayReportBuilderFieldsID", payReportBuilderFieldsID);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    if (ds.Tables.Count > 1)
                    {
                        textField = ds.Tables[1].Rows[0]["TextField"].ToString();
                        valueField = ds.Tables[1].Rows[0]["ValueField"].ToString();
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dt;
        }

        public OperationDetails InsertPayReportBuilderWhere(PayReportBuilderWhereModel payReportBuilderWhereModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_InsertPayReportBuilderWhere", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayReportBuilderWhere", payReportBuilderWhereModel.PayReportBuilderWhere);
                sqlCommand.Parameters.AddWithValue("@whereSQL", payReportBuilderWhereModel.whereSQL);
                sqlCommand.Parameters.AddWithValue("@Eqal", payReportBuilderWhereModel.Eqal);
                sqlCommand.Parameters.AddWithValue("@UserID", payReportBuilderWhereModel.UserID);

                SqlParameter OperationValue = new SqlParameter("@OperationValue", SqlDbType.VarChar, 1000);
                OperationValue.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationValue);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                sqlReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                payReportBuilderWhereModel.PayReportBuilderWhereID = Convert.ToInt32(OperationValue.Value);
                Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, payReportBuilderWhereModel.PayReportBuilderWhereID);
        }

        public OperationDetails DeletePayReportBuilderWhere(int userID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_DeletePayReportBuilderWhere", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", userID);

                int result = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                if (result > 0)
                    Message = "success";
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null);
        }

        public OperationDetails DeletePayReportBuilderWhereById(int payReportBuilderWhereID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_DeletePayReportBuilderWhereById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayReportBuilderWhereID", payReportBuilderWhereID);

                int result = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                if (result > 0)
                    Message = "success";
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null);
        }

        public OperationDetails InsertUpdate_PayReportBuilderSavedReports(ReportBuilderModel reportBuilderModel, int currentUserID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_InsertUpdate_PayReportBuilderSavedReports", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ReportID", reportBuilderModel.PayReportBuilderSavedReportsID);
                sqlCommand.Parameters.AddWithValue("@ReportName", reportBuilderModel.ReportName);
                sqlCommand.Parameters.AddWithValue("@Fields", reportBuilderModel.SelectedFields);
                sqlCommand.Parameters.AddWithValue("@OrderBy", reportBuilderModel.OrderBy);
                sqlCommand.Parameters.AddWithValue("@GroupBy", reportBuilderModel.GroupBy);
                sqlCommand.Parameters.AddWithValue("@PayReportBuilderSavedReportsWhere", reportBuilderModel.PayReportBuilderSavedReportsWhere);
                sqlCommand.Parameters.AddWithValue("@WhereDescription", reportBuilderModel.WhereDescription);
                sqlCommand.Parameters.AddWithValue("@UserID", reportBuilderModel.UserID);
                sqlCommand.Parameters.AddWithValue("@CurrentUserID", currentUserID);

                SqlParameter OperationValue = new SqlParameter("@OperationValue", SqlDbType.VarChar, 1000);
                OperationValue.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationValue);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                reportBuilderModel.PayReportBuilderSavedReportsID = Convert.ToInt32(OperationValue.Value);
                Message = OperationMessage.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return new OperationDetails(true, Message, null, reportBuilderModel.PayReportBuilderSavedReportsID);
        }

        public List<ReportBuilderModel> GetAllSavedReports()
        {
            List<ReportBuilderModel> list = new List<ReportBuilderModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllSavedReports", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ReportBuilderModel model;
                    while (sqlDataReader.Read())
                    {
                        model = new ReportBuilderModel();
                        model.PayReportBuilderSavedReportsID = Convert.ToInt32(sqlDataReader["PayReportBuilderSavedReportsID"].ToString());
                        model.ReportName = Convert.ToString(sqlDataReader["ReportName"]);
                        model.SelectedFields = Convert.ToString(sqlDataReader["ReportFields"]);
                        model.WhereDescription = Convert.ToString(sqlDataReader["WhereDescription"]);
                        model.OrderBy = Convert.ToString(sqlDataReader["OrderBy"]) != "" ? Convert.ToInt32(sqlDataReader["OrderBy"]) : 0;
                        model.GroupBy = Convert.ToString(sqlDataReader["GroupBy"]) != "" ? Convert.ToInt32(sqlDataReader["GroupBy"]) : 0;
                        model.UserID = Convert.ToString(sqlDataReader["UserID"]) != "" ? Convert.ToInt32(sqlDataReader["UserID"]) : 0;
                        list.Add(model);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public ReportBuilderModel GetSavedReportById(int payReportBuilderSavedReportsID)
        {
            ReportBuilderModel model = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetSavedReportById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayReportBuilderSavedReportsID", payReportBuilderSavedReportsID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        model = new ReportBuilderModel();
                        model.PayReportBuilderSavedReportsID = Convert.ToInt32(sqlDataReader["PayReportBuilderSavedReportsID"].ToString());
                        model.ReportName = Convert.ToString(sqlDataReader["ReportName"]);
                        model.SelectedFields = Convert.ToString(sqlDataReader["ReportFields"]);
                        model.PayReportBuilderSavedReportsWhere = Convert.ToString(sqlDataReader["PayReportBuilderSavedReportsWhere"]);
                        model.WhereDescription = Convert.ToString(sqlDataReader["WhereDescription"]);
                        model.OrderBy = Convert.ToString(sqlDataReader["OrderBy"]) != "" ? Convert.ToInt32(sqlDataReader["OrderBy"]) : 0;
                        model.GroupBy = Convert.ToString(sqlDataReader["GroupBy"]) != "" ? Convert.ToInt32(sqlDataReader["GroupBy"]) : 0;
                        model.UserID = Convert.ToString(sqlDataReader["UserID"]) != "" ? Convert.ToInt32(sqlDataReader["UserID"]) : 0;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return model;
        }

        public OperationDetails DeletePayReportBuilderSavedReportsById(int payReportBuilderSavedReportsID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_DeletePayReportBuilderSavedReportsById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayReportBuilderSavedReportsID", payReportBuilderSavedReportsID);

                int result = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                if (result > 0)
                    Message = "success";
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null);
        }

        public List<ReportBuilderModel> GetAllSavedReportsByUser(int UserID)
        {
            List<ReportBuilderModel> list = new List<ReportBuilderModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllSavedReportsByUser", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", UserID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ReportBuilderModel model;
                    while (sqlDataReader.Read())
                    {
                        model = new ReportBuilderModel();
                        model.PayReportBuilderSavedReportsID = Convert.ToInt32(sqlDataReader["PayReportBuilderSavedReportsID"].ToString());
                        model.ReportName = Convert.ToString(sqlDataReader["ReportName"]);
                        model.SelectedFields = Convert.ToString(sqlDataReader["ReportFields"]);
                        model.WhereDescription = Convert.ToString(sqlDataReader["WhereDescription"]);
                        model.OrderBy = Convert.ToString(sqlDataReader["OrderBy"]) != "" ? Convert.ToInt32(sqlDataReader["OrderBy"]) : 0;
                        model.GroupBy = Convert.ToString(sqlDataReader["GroupBy"]) != "" ? Convert.ToInt32(sqlDataReader["GroupBy"]) : 0;
                        model.UserID = Convert.ToString(sqlDataReader["UserID"]) != "" ? Convert.ToInt32(sqlDataReader["UserID"]) : 0;
                        list.Add(model);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        public DataTable GetReportData(int Mode, string selectedFields, int orderBy, int groupBy, int ReportId, string SqlWhereCondition, int Tmode, int IsReportTemplate, string EmployeeID, int UserId, out string groupBySqlID, out string orderBySqlID)
        {
            DataTable dt = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                SqlDataAdapter da = new SqlDataAdapter("HR_stp_VW_HR_PayViewReportbuilder", sqlConnection);
                da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@Mode", Mode);
                da.SelectCommand.Parameters.AddWithValue("@TMode", Tmode);
                da.SelectCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                da.SelectCommand.Parameters.AddWithValue("@IsReportTemplate", IsReportTemplate);
                da.SelectCommand.Parameters.AddWithValue("@Fields", selectedFields);
                da.SelectCommand.Parameters.AddWithValue("@OrderBy", orderBy);
                da.SelectCommand.Parameters.AddWithValue("@GroupBy", groupBy);
                da.SelectCommand.Parameters.AddWithValue("@SqlWhereCondition", SqlWhereCondition);
                da.SelectCommand.Parameters.AddWithValue("@ReportId", ReportId);
                da.SelectCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlParameter GroupBySqlID = new SqlParameter("@GroupBySqlID", SqlDbType.VarChar, 250);
                GroupBySqlID.Direction = ParameterDirection.Output;
                da.SelectCommand.Parameters.Add(GroupBySqlID);
                SqlParameter OrderBySqlID = new SqlParameter("@OrderBySqlID", SqlDbType.VarChar, 250);
                OrderBySqlID.Direction = ParameterDirection.Output;
                da.SelectCommand.Parameters.Add(OrderBySqlID);                
                da.SelectCommand.CommandTimeout = 9000;
                da.Fill(dt);
                groupBySqlID = GroupBySqlID.Value.ToString();
                orderBySqlID = OrderBySqlID.Value.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dt;
        }

    }
}
