﻿using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class UserTypeDB : DBHelper
    {
        /// <summary>
        /// Returns user details by given email and password
        /// </summary>
        /// <param name="email">pass email value</param>
        /// <param name="password">pass password value</param>
        /// <returns>usermodel</returns>
        public List<UserTypeModel> GetAllUser(int companyid)
        {
            UserTypeModel usertype = null;
            var userTypeList = new List<UserTypeModel>();
            try
            {

                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@CompanyId" , companyid)  
                     
                    };


                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetAllUserTypes", parameters))
                {
                    // Check if the reader returned any rows 
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            usertype = new UserTypeModel();
                            usertype.UserTypeID = Convert.ToInt32(reader["UserTypeID"].ToString());
                            usertype.UserTypeName = Convert.ToString(reader["UserTypeName"]);
                            userTypeList.Add(usertype);

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //throw new Exception("Association Id doesnot match");
                throw;
            }
            return userTypeList;
        }


        /// <summary>
        /// Returns user types 1 and 5
        /// </summary>
        /// <returns>usermodel</returns>
        public List<UserTypeModel> GetUserTypesForEmployee()
        {
            UserTypeModel usertype = null;
            var userTypeList = new List<UserTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetUserTypesForEmployee"))
                {
                    // Check if the reader returned any rows 
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            usertype = new UserTypeModel();
                            usertype.UserTypeID = Convert.ToInt32(reader["UserTypeID"].ToString());
                            usertype.UserTypeName = Convert.ToString(reader["UserTypeName"]);
                            userTypeList.Add(usertype);

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //throw new Exception("Association Id doesnot match");
                throw;
            }
            return userTypeList;
        }
    }
}
