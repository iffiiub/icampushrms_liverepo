﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class PayLateTypeDB 
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        public List<PayLateTypeModel> GetPayLateTypeList()
        {
            List<PayLateTypeModel> payLateTypeList= null;
            try
            {
                payLateTypeList = new List<PayLateTypeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayLateType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
              

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PayLateTypeModel payLateTypeModel;
                    while (sqlDataReader.Read())
                    {
                        payLateTypeModel = new PayLateTypeModel();
                        payLateTypeModel.PayLateTypeID = Convert.ToInt32(sqlDataReader["PayLateTypeID"].ToString());
                        payLateTypeModel.PayLateTypeName_1 = sqlDataReader["PayLateTypeName_1"].ToString();
                        payLateTypeModel.PayLateTypeName_2 = sqlDataReader["PayLateTypeName_2"].ToString();
                        payLateTypeModel.PayLateTypeName_3 = sqlDataReader["PayLateTypeName_3"].ToString();                        
                        payLateTypeList.Add(payLateTypeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payLateTypeList;
        }

    }
}
