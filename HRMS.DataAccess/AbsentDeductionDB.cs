﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class AbsentDeductionDB : DBHelper
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<AbsentsForCurrentMonthModel> GetAbsentForCurrentMonth(string Month, string Year, string StartDate, string EndDate, bool IsConfirmed, int? UserId)
        {
            try
            {
                bool isDeductionRulesEnable = new DeductionDB().GetDeductionRuleSetting().AppliedDeductionRules;
                List<AbsentsForCurrentMonthModel> objAbsentsForCurrentMonthModelList = new List<AbsentsForCurrentMonthModel>();
                int FilterCondition = 0;
                FilterCondition = GetFilterCondition();
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (isDeductionRulesEnable)
                {
                    sqlCommand = new SqlCommand("HR_Stp_GetAbsentWithDeductionRules", sqlConnection);
                }
                else
                {
                    sqlCommand = new SqlCommand("HR_uspGetAbsentsForCurrentMonth", sqlConnection);
                }

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                if (FilterCondition == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@aFromDate", CommonDB.SetCulturedDate(StartDate));
                    sqlCommand.Parameters.AddWithValue("@aToDate", CommonDB.SetCulturedDate(EndDate));

                }
                else
                {
                    var startDate = new DateTime(Convert.ToInt32(Year), Convert.ToInt32(Month), 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    sqlCommand.Parameters.AddWithValue("@aFromDate", startDate);
                    sqlCommand.Parameters.AddWithValue("@aToDate", endDate);
                }

                sqlCommand.Parameters.AddWithValue("@IsConfirmed", IsConfirmed);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandTimeout = 9000;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        AbsentsForCurrentMonthModel absentsForCurrentMonthModel = new AbsentsForCurrentMonthModel();

                        absentsForCurrentMonthModel.AbsentID = reader["AbsentID"].ToString();
                        absentsForCurrentMonthModel.Days = Convert.ToInt32(reader["Days"].ToString());
                        absentsForCurrentMonthModel.FromDate = Convert.ToDateTime(reader["FromDate"].ToString());
                        absentsForCurrentMonthModel.ToDate = Convert.ToDateTime(reader["ToDate"].ToString());
                        absentsForCurrentMonthModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                        absentsForCurrentMonthModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(reader["PayEmployeeAbsentHeaderID"].ToString());
                        absentsForCurrentMonthModel.Amount = Convert.ToDecimal(reader["Amount"]);
                        absentsForCurrentMonthModel.EmployeeName = reader["EmployeeName"].ToString();
                        absentsForCurrentMonthModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());

                        absentsForCurrentMonthModel.TotalDedSalary = reader["TotalDedSalary"].ToString();
                        absentsForCurrentMonthModel.AbsentTypeName = reader["AbsentTypeName"].ToString();
                        absentsForCurrentMonthModel.IsConfirmed = Convert.ToBoolean(reader["IsConfirmed"].ToString());
                        absentsForCurrentMonthModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"].ToString());
                        if (reader["StatusID"] != DBNull.Value)
                            absentsForCurrentMonthModel.StatusID = Convert.ToInt32(reader["StatusID"].ToString());
                        absentsForCurrentMonthModel.CycleID = reader["CycleID"] == DBNull.Value || Convert.ToString(reader["CycleID"]) == "" ? 0 : Convert.ToInt32(reader["CycleID"].ToString());
                        absentsForCurrentMonthModel.NoOfNullCycleCount = reader["NoOfNullCycleRecords"] == DBNull.Value || Convert.ToString(reader["NoOfNullCycleRecords"]) == "" ? 0 : Convert.ToInt32(reader["NoOfNullCycleRecords"].ToString());
                        objAbsentsForCurrentMonthModelList.Add(absentsForCurrentMonthModel);
                    }
                }
                sqlConnection.Close();

                return objAbsentsForCurrentMonthModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }

        public List<AbsentsForCurrentMonthExcusedModel> GetAbsentsForCurrentMonthExcused(int? userId, string Month, string Year, string StartDate, string EndDate)
        {

            try
            {
                List<AbsentsForCurrentMonthExcusedModel> objAbsentsForCurrentMonthExcusedModelList = new List<AbsentsForCurrentMonthExcusedModel>();
                int FilterCondition = 0;
                FilterCondition = GetFilterCondition();
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetAbsentsForCurrentMonthExcused", sqlConnection);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                if (FilterCondition == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@aFromDate", CommonDB.SetCulturedDate(StartDate));
                    sqlCommand.Parameters.AddWithValue("@aToDate", CommonDB.SetCulturedDate(EndDate));
                    sqlCommand.Parameters.AddWithValue("@UserId", userId);
                }
                else
                {
                    var startDate = new DateTime(Convert.ToInt32(Year), Convert.ToInt32(Month), 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    sqlCommand.Parameters.AddWithValue("@aFromDate", startDate);
                    sqlCommand.Parameters.AddWithValue("@aToDate", endDate);
                    sqlCommand.Parameters.AddWithValue("@UserId", userId);

                }
                SqlDataReader reader = sqlCommand.ExecuteReader();
                // Check if the reader returned any rows
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        AbsentsForCurrentMonthExcusedModel absentsForCurrentMonthExcusedModel = new AbsentsForCurrentMonthExcusedModel();
                        absentsForCurrentMonthExcusedModel.AbsentID = reader["AbsentID"].ToString();
                        absentsForCurrentMonthExcusedModel.Days = Convert.ToInt32(reader["Days"].ToString());
                        absentsForCurrentMonthExcusedModel.FromDate = Convert.ToDateTime(reader["FromDate"].ToString());
                        absentsForCurrentMonthExcusedModel.ToDate = Convert.ToDateTime(reader["ToDate"].ToString());
                        absentsForCurrentMonthExcusedModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                        absentsForCurrentMonthExcusedModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(reader["PayEmployeeAbsentHeaderID"].ToString());
                        //absentsForCurrentMonthExcusedModel.Amount = Convert.ToDecimal(reader["Amount"]);
                        absentsForCurrentMonthExcusedModel.EmployeeName = reader["EmployeeName"].ToString();
                        absentsForCurrentMonthExcusedModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
                        //absentsForCurrentMonthExcusedModel.TotalDedSalary = reader["TotalDedSalary"].ToString();
                        absentsForCurrentMonthExcusedModel.AbsentTypeName = reader["AbsentTypeName"].ToString();
                        absentsForCurrentMonthExcusedModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"].ToString());
                        absentsForCurrentMonthExcusedModel.StatusID = reader["StatusID"] == DBNull.Value || Convert.ToString(reader["StatusID"]) == "" ? 0 : Convert.ToInt32(reader["StatusID"].ToString());
                        absentsForCurrentMonthExcusedModel.CycleID = reader["CycleID"] == DBNull.Value || Convert.ToString(reader["CycleID"]) == "" ? 0 : Convert.ToInt32(reader["CycleID"].ToString());
                        objAbsentsForCurrentMonthExcusedModelList.Add(absentsForCurrentMonthExcusedModel);
                    }
                }

                return objAbsentsForCurrentMonthExcusedModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
        }

        public OperationDetails ConfirmEmployeeForAbsentDeductedStatus(int EmployeeId, int AbsentHeaderID, string EffectiveDate)
        {
            string Message = "";
            int rowCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspConfirmEmployeeForAbsentDeductedStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@aIntPayEmployeeAbsentHeaderID", AbsentHeaderID);
                sqlCommand.Parameters.AddWithValue("@astrEffectiveDate", CommonDB.SetCulturedDate(EffectiveDate));
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                rowCount = Convert.ToInt32(OperationId.Value);
                Message = "success";
                //SqlParameter[] parameters =
                //    {
                //      new SqlParameter("@aSntEmployeeID",EmployeeId) ,
                //      new SqlParameter("@aIntPayEmployeeAbsentHeaderID", AbsentHeaderID) ,
                //      new SqlParameter("@astrEffectiveDate", CommonDB.SetCulturedDate(EffectiveDate)) ,
                //       new SqlParameter("@output", ParameterDirection.Output)
                //      };
                //var op = SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                //                     "HR_uspConfirmEmployeeForAbsentDeductedStatus", parameters);
                return new OperationDetails(true, "Employee Absent Deducted Status is confirmed successfully.", null, rowCount);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while confirming Employee Absent Deducted Status.", exception);
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

       
        public List<PayAbsentTypeModel> GetPayAbsentType()
        {
            try
            {

                List<PayAbsentTypeModel> objPayAbsentTypeModelList = new List<PayAbsentTypeModel>();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_PayAbsentType"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            PayAbsentTypeModel PayAbsentTypeModel = new PayAbsentTypeModel();
                            PayAbsentTypeModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
                            PayAbsentTypeModel.PayAbsentTypeName_1 = reader["PayAbsentTypeName_1"].ToString();
                            PayAbsentTypeModel.PayAbsentTypeName_2 = reader["PayAbsentTypeName_2"].ToString();
                            PayAbsentTypeModel.PayAbsentTypeName_3 = reader["PayAbsentTypeName_3"].ToString();
                            PayAbsentTypeModel.ISDeductable = Convert.ToBoolean(reader["ISDeductable"] == DBNull.Value ? "false" : reader["ISDeductable"]);
                            PayAbsentTypeModel.AbsentPaidTypeId = Convert.ToInt32(reader["AbsentPaidTypeId"].ToString());
                            PayAbsentTypeModel.AnnualLeave = Convert.ToBoolean(reader["AnnualLeave"] == DBNull.Value ? "false" : reader["AnnualLeave"]);
                            objPayAbsentTypeModelList.Add(PayAbsentTypeModel);
                        }
                    }
                }
                return objPayAbsentTypeModelList;
                // Finally, we return Labour

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour.", exception);
                throw;
            }
            finally
            {

            }
        }
        
        public DeductionSetting GetDeductionSetting()
        {
            dataHelper = new DataHelper();
            string query = "SELECT * FROM  HR_DeductSetting";
            DataTable dt = dataHelper.ExcuteCommandText(query);
            DeductionSetting deductionSetting = new DeductionSetting();
            foreach (DataRow dr in dt.Rows)
            {
                deductionSetting.AbsentDeductionTypeId = dr["AbsentDeductionTypeID"].ToString() == "" ? 1 : Convert.ToInt32(dr["AbsentDeductionTypeID"].ToString());
                deductionSetting.FilterDateFormat = dr["FilterDateFormat"].ToString() == "" ? 0 : Convert.ToInt32(dr["FilterDateFormat"].ToString());
                deductionSetting.PostAbsentRecords = dr["PostAbsentRecords"].ToString() == "" ? false : Convert.ToBoolean(dr["PostAbsentRecords"]);
            }
            return deductionSetting;
        }
        public int GetFilterCondition()
        {
            int FilterCondition = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT TOP 1 FilterDateFormat  FROM  HR_DeductSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        FilterCondition = Convert.ToInt32(reader["FilterDateFormat"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return FilterCondition;
        }
        
        public int GetNullcycleCountBetweenDates(string Month, string Year, string StartDate, string EndDate)
        {
            int Count = 0;

            int FilterCondition = 0;
            FilterCondition = GetFilterCondition();

            string Query = "Hr_stp_GetNumberOfNullCycleRecords";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                if (FilterCondition == 1)
                {
                    DateTime dt1 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dt2 = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    sqlCommand.Parameters.AddWithValue("@StartDate", dt1);
                    sqlCommand.Parameters.AddWithValue("@EndDate", dt2);

                }
                else
                {
                    var startDate = new DateTime(Convert.ToInt32(Year), Convert.ToInt32(Month), 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    sqlCommand.Parameters.AddWithValue("@StartDate", startDate);
                    sqlCommand.Parameters.AddWithValue("@EndDate", endDate);
                }


                SqlParameter output = new SqlParameter("@NullCycleCount", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Count = Convert.ToInt32(output.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Count;
        }

        public string GetNullAcYearsBetweenDates(string Month, string Year, string StartDate, string EndDate)
        {
            string AcRecords = string.Empty;

            int FilterCondition = 0;
            FilterCondition = GetFilterCondition();

            string Query = "Hr_stp_GetNullAcademicYearsinDeduction";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                if (FilterCondition == 1)
                {
                    DateTime dt1 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dt2 = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    sqlCommand.Parameters.AddWithValue("@StartDate", dt1);
                    sqlCommand.Parameters.AddWithValue("@EndDate", dt2);

                }
                else
                {
                    var startDate = new DateTime(Convert.ToInt32(Year), Convert.ToInt32(Month), 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    sqlCommand.Parameters.AddWithValue("@StartDate", startDate);
                    sqlCommand.Parameters.AddWithValue("@EndDate", endDate);
                }

                sqlCommand.Parameters.AddWithValue("@Mode", 1);
                SqlParameter output = new SqlParameter("@NullAcademicRecords", SqlDbType.NVarChar);
                output.Size = 400;
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                AcRecords = Convert.ToString(output.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return AcRecords;
        }

        public OperationDetails PayDeductionAbsentCrud(PayDeductionModel payDeductionModel, int operationId,int PayEmployeeAbsentID)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayDeductionCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationId);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", payDeductionModel.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@aTntDeductionTypeID", payDeductionModel.DeductionTypeID);
                //sqlCommand.Parameters.AddWithValue("@aDttEffectiveDate", (DateTime.ParseExact(payDeductionModel.EffectiveDate, format, null)).ToString(sqlDateFormat));
                sqlCommand.Parameters.AddWithValue("@aDttEffectiveDate", CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate));
                sqlCommand.Parameters.AddWithValue("@aNumPaidCycle", payDeductionModel.PaidCycle.ToString());
                sqlCommand.Parameters.AddWithValue("@aBitIsInstallment", payDeductionModel.IsInstallment);
                sqlCommand.Parameters.AddWithValue("@aNumAmount", payDeductionModel.Amount);
                sqlCommand.Parameters.AddWithValue("@aNvrComments", payDeductionModel.Comments);
                sqlCommand.Parameters.AddWithValue("@aIntEmployeeID", payDeductionModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aNvrRefNumber", payDeductionModel.RefNumber);
                //sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", (DateTime.ParseExact(payDeductionModel.EffectiveDate, format, null)).ToString(sqlDateFormat));
                sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate));
                sqlCommand.Parameters.AddWithValue("@aIntPvId", payDeductionModel.PvId);
                sqlCommand.Parameters.AddWithValue("@DeductionType",1);
                sqlCommand.Parameters.AddWithValue("@PrimaryId", PayEmployeeAbsentID);
                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                id = Convert.ToInt32(output.Value);
                //}
                sqlConnection.Close();




            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public OperationDetails PayDeductionDetailsAbsentCrud(PayDeductionDetailModel payDeductionDetailModel, int operationId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_xspPayDeductionDetailCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationId);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionDetailID", payDeductionDetailModel.PayDeductionDetailID);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", payDeductionDetailModel.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@aDttDeductionDate", CommonDB.SetCulturedDate(payDeductionDetailModel.DeductionDate));
                //sqlCommand.Parameters.AddWithValue("@aDttDeductionDate", (DateTime.ParseExact(payDeductionDetailModel.DeductionDate, format, null)).ToString(sqlDateFormat));
                sqlCommand.Parameters.AddWithValue("@aNumAmount", payDeductionDetailModel.Amount);
                sqlCommand.Parameters.AddWithValue("@aNvrComments", payDeductionDetailModel.Comments);
                sqlCommand.Parameters.AddWithValue("@aBitIsActive", payDeductionDetailModel.IsActive);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                id = Convert.ToInt32(output.Value);
                //}
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return new OperationDetails(true, Message, null, id);
        }
    }
}
