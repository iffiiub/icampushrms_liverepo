﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class JobDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;




        public string InsertUpdateJobTitle(JobTitle ObjJobTitle)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_InsertUpdateJobTitle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@JobTitleID", ObjJobTitle.JobTitleID);
                sqlCommand.Parameters.AddWithValue("@JobTitleName_1", ObjJobTitle.JobTitleName_1);
                sqlCommand.Parameters.AddWithValue("@JobTitleName_2", ObjJobTitle.JobTitleName_2);
                sqlCommand.Parameters.AddWithValue("@JobTitleName_3", ObjJobTitle.JobTitleName_3);
                sqlCommand.Parameters.AddWithValue("@JobTitleShortName_1", ObjJobTitle.JobTitleShortName_1);
                sqlCommand.Parameters.AddWithValue("@JobTitleShortName_2", ObjJobTitle.JobTitleShortName_2);
                sqlCommand.Parameters.AddWithValue("@JobTitleShortName_3", ObjJobTitle.JobTitleShortName_3);

                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                //Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public int DeleteJobTitle(int JobTitleID)
        {
            int numberOfRecords = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DeleteJobTitle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@JobTitleID", JobTitleID);


                //SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                //OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                numberOfRecords = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                //Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return numberOfRecords;
        }


        public List<JobTitle> GetAllJobTitle()
        {
            List<JobTitle> JobTitleList = new List<JobTitle>();
            int temp = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllJobTitle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    JobTitle ObjJobTitle;
                    while (sqlDataReader.Read())
                    {
                        ObjJobTitle = new JobTitle();
                        ObjJobTitle.JobTitleID = int.Parse(sqlDataReader["JobTitleID"].ToString());
                        ObjJobTitle.JobTitleName_1 = sqlDataReader["Job Title Name_1"].ToString().Trim();
                        ObjJobTitle.JobTitleName_2 = sqlDataReader["Job Title Name_2"].ToString().Trim();
                        ObjJobTitle.JobTitleShortName_1 = sqlDataReader["Job Title ShortName_1"].ToString().Trim();
                        ObjJobTitle.JobTitleShortName_2 = sqlDataReader["Job Title ShortName_2"].ToString().Trim();
                        JobTitleList.Add(ObjJobTitle);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return JobTitleList;
        }


        public DataSet GetAllJobTitleDatasSet()
        {

            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            string Query = "SELECT [JobTitleName_1] AS [Job Title (En)],[JobTitleName_2] AS [Job Title (Ar)],[JobTitleName_3] AS [Job Title (Fr)]" +
                           ",[JobTitleShortName_1] AS [Short Code (En)],[JobTitleShortName_2] AS [Short Code (Ar)],[JobTitleShortName_3] AS [Short Code (Fr)]" +
                           "FROM [dbo].HR_JobTitle Where JobTitleID > 0 order by [Job Title (EN)] asc";
            SqlDataAdapter da = new SqlDataAdapter(Query, sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;

        }
        public List<JobTitle> GetJobTitleList()
        {
            List<JobTitle> JobTitleList = null;

            try
            {
                JobTitleList = new List<JobTitle>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetJobTitleList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                //  sqlCommand.Parameters.AddWithValue("@ID", Id);




                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                  //  if (sqlDataReader.Read())
                   //{
                        JobTitle ObjJobTitle;
                        while (sqlDataReader.Read())
                        {


                            ObjJobTitle = new JobTitle();
                            ObjJobTitle.JobTitleID = int.Parse(sqlDataReader["JobTitleID"].ToString());
                            ObjJobTitle.JobTitleName_1 = sqlDataReader["JobTitleName_1"].ToString();
                            //ObjJobTitle.JobTitleName_2 = sqlDataReader["JobTitleName_2"].ToString();
                            //ObjJobTitle.JobTitleName_3 = sqlDataReader["JobTitleName_3"].ToString();
                            //ObjJobTitle.JobTitleShortName_1 = sqlDataReader["JobTitleShortName_1"].ToString();
                            //ObjJobTitle.JobTitleShortName_2 = sqlDataReader["JobTitleShortName_1"].ToString();
                            //ObjJobTitle.JobTitleShortName_3 = sqlDataReader["JobTitleShortName_1"].ToString();

                            JobTitleList.Add(ObjJobTitle);

                        }
                    }
                //}
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return JobTitleList;

        }

        public JobTitle GetJobTitleById(int Id)
        {

            JobTitle ObjJobTitle = new JobTitle();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetAllJobTitleByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@ID", Id);




                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {


                        ObjJobTitle.JobTitleID = int.Parse(sqlDataReader["JobTitleID"].ToString());
                        ObjJobTitle.JobTitleName_1 = sqlDataReader["JobTitleName_1"].ToString();
                        ObjJobTitle.JobTitleName_2 = sqlDataReader["JobTitleName_2"].ToString();
                        ObjJobTitle.JobTitleName_3 = sqlDataReader["JobTitleName_3"].ToString();
                        ObjJobTitle.JobTitleShortName_1 = sqlDataReader["JobTitleShortName_1"].ToString();
                        ObjJobTitle.JobTitleShortName_2 = sqlDataReader["JobTitleShortName_2"].ToString();
                        ObjJobTitle.JobTitleShortName_3 = sqlDataReader["JobTitleShortName_3"].ToString();


                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ObjJobTitle;

        }


        public List<ContractTerm> GetContractTermList()
        {
            List<ContractTerm> ContractTermList = null;

            try
            {
                ContractTermList = new List<ContractTerm>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllHRContractType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                //  sqlCommand.Parameters.AddWithValue("@ID", Id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    {
                        ContractTerm ObjContractTerm;
                        while (sqlDataReader.Read())
                        {
                            ObjContractTerm = new ContractTerm();
                            ObjContractTerm.EmployeeContractTermsID = int.Parse(sqlDataReader["HRContractTypeID"].ToString());
                            ObjContractTerm.ContarctTermName = sqlDataReader["HRContractTypeName_1"].ToString();
                            ContractTermList.Add(ObjContractTerm);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ContractTermList;

        }



    }
}
