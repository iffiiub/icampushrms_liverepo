﻿using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class SendMailDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        GeneralDB.DataHelper dataHelper;


        public IEnumerable<SendMailLogModel> GetPaySendMailLogData(int? cycleID)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@CycleId", cycleID));
                parameters.Add(new SqlParameter("@Mode", 2));
                List<SendMailLogModel> logList = new List<SendMailLogModel>();
                SendMailLogModel logModel;
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameters, "HR_stp_GetSendMailLog");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        logModel = new SendMailLogModel();
                        logModel.paySendEmailLogID = Convert.ToInt32(row["PaySendEmailLogID"].ToString());
                        logModel.employeeID = Convert.ToInt32(row["EmployeeID"].ToString());
                        logModel.cycleId = Convert.ToInt32(row["cycleID"].ToString());
                        logModel.employeeEmailId = row["EmployeeEmailID"].ToString();
                        logModel.payslipLink = row["PaySlipLink"].ToString();
                        logModel.sendFlag = Convert.ToBoolean(row["SendFlag"].ToString());
                        logList.Add(logModel);
                    }
                }

                return logList;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public SendMailLogModel GetPaySendMailLogById(int PaySendLogId)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PaySendEmailLogID", PaySendLogId));
                parameters.Add(new SqlParameter("@Mode", 1));
                List<SendMailLogModel> logList = new List<SendMailLogModel>();
                SendMailLogModel logModel = new SendMailLogModel();
                DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameters, "HR_stp_GetSendMailLog");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        logModel.paySendEmailLogID = Convert.ToInt32(row["PaySendEmailLogID"].ToString());
                        logModel.employeeID = Convert.ToInt32(row["EmployeeID"].ToString());
                        logModel.cycleId = Convert.ToInt32(row["cycleID"].ToString());
                        logModel.employeeEmailId = row["EmployeeEmailID"].ToString();
                        logModel.payslipLink = row["PaySlipLink"].ToString();
                        logModel.sendFlag = Convert.ToBoolean(row["SendFlag"].ToString());
                    }
                }

                return logModel;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails SaveAndUpdateModel(SendMailLogModel model, int mode)
        {
            try
            {
                dataHelper = new GeneralDB.DataHelper();
                sqlCommand = new SqlCommand("HR_stp_InsertUpdateSendMailLog");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Mode", mode);
                sqlCommand.Parameters.AddWithValue("@PaySendEmailLogID", model.paySendEmailLogID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", model.employeeID);
                sqlCommand.Parameters.AddWithValue("@CycleId", model.cycleId);
                sqlCommand.Parameters.AddWithValue("@SendFlag", model.sendFlag);
                sqlCommand.Parameters.AddWithValue("@EmployeeEmailID", model.employeeEmailId);
                sqlCommand.Parameters.AddWithValue("@PaySlipLink", model.payslipLink);
                return dataHelper.SaveModel(sqlCommand);
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public int GetCountOfMailByCycleId(string CycleIds)
        {
            using (sqlConnection = new SqlConnection(connectionString))
            {
                using (sqlCommand = new SqlCommand())
                {
                    try
                    {
                        int count = 0;
                        sqlConnection.Open();
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = "SELECT COUNT(DISTINCT EmployeeId) AS Count FROM HR_PaySendEmailLog  WHERE CycleID IN (" + CycleIds + ")";
                        sqlReader = sqlCommand.ExecuteReader();
                        if (sqlReader.HasRows)
                        {
                            while (sqlReader.Read())
                            {
                                count = Convert.ToInt32(sqlReader["Count"]);
                            }
                        }
                        return count;
                    }
                    catch (Exception e)
                    {
                        return 0;
                    }
                    finally
                    {
                        if (sqlReader != null)
                        {
                            sqlReader.Close();
                        }
                        if (sqlConnection != null)
                        {
                            sqlConnection.Close();
                        }
                    }
                }
            }

        }

        public DataSet GetAllSendEmailListByCycleId(int CycleId)
        {
            DataSet _sendEmailList=new DataSet();
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_stp_InsertUpdateSendMailLog", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Mode", 3);
                sqlCommand.Parameters.AddWithValue("@CycleId", CycleId);
                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = sqlCommand;
                mySqlDataAdapter.Fill(_sendEmailList);


                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return _sendEmailList;
        }

    }
}
