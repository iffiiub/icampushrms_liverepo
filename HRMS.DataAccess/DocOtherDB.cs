﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class DocOtherDB : DBHelper
    {
        public OperationDetails InsertDocOther(DocOtherModel DocOtherModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {

                        new SqlParameter("@DocumentNo",DocOtherModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", DocOtherModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", DocOtherModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(DocOtherModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(DocOtherModel.ExpiryDate)) ,
                      new SqlParameter("@Note",DocOtherModel.Note ) ,
                      new SqlParameter("@IsPrimary", DocOtherModel.IsPrimary ),
                      new SqlParameter("@DocOtherFile", DocOtherModel.DocOtherFile ),
                      new SqlParameter("@EmployeeId", DocOtherModel.EmployeeId)
                    };
                int DocVisaId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_DocOther", parameters));
                return new OperationDetails(true, "Doc Other saved successfully.", null, DocVisaId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Doc Other .", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateDocOther(DocOtherModel DocOtherModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@DocOtherId",DocOtherModel.DocOtherId),
                      new SqlParameter("@DocumentNo",DocOtherModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", DocOtherModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", DocOtherModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(DocOtherModel.IssueDate)),
                      new SqlParameter("@ExpiryDate", GeneralDB.CommonDB.SetCulturedDate(DocOtherModel.ExpiryDate)) ,
                      new SqlParameter("@Note",DocOtherModel.Note ) ,
                      new SqlParameter("@IsPrimary", DocOtherModel.IsPrimary ),
                      new SqlParameter("@DocOtherFile", DocOtherModel.DocOtherFile ) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_DocOther", parameters);
                return new OperationDetails(true, "Doc Other   updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Doc Other  .", exception);
                throw;
            }
            finally
            {

            }
        }

        public DocOtherModel DocOtherById(int DocOtherId)
        {
            try
            {
                DocOtherModel DocOtherModel = new DocOtherModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_DocOtherByID", new SqlParameter("@DocOtherId", DocOtherId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            DocOtherModel.DocOtherId = Convert.ToInt32(reader["DocOtherId"].ToString());
                            DocOtherModel.DocumentNo = reader["DocumentNo"].ToString();
                            DocOtherModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            DocOtherModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            DocOtherModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                            DocOtherModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            DocOtherModel.Note = reader["Note"].ToString();
                            DocOtherModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            DocOtherModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            DocOtherModel.DocOtherFile = Convert.ToString(reader["DocOtherFile"]);
                            DocOtherModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());

                        }
                    }
                }
                return DocOtherModel;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteDocOtheryID(int DocOtherId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@DocOtherId",DocOtherId)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_DocOther", parameters);
                return new OperationDetails(true, "Doc Other Document deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Doc Other Document.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<DocOtherModel> GetDocOtherListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder, string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the LabourContract		
            List<DocOtherModel> DocOtherModelList = new List<DocOtherModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_DocOtherWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        DocOtherModel DocOtherModel;
                        while (reader.Read())
                        {
                            DocOtherModel = new DocOtherModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            DocOtherModel.DocOtherId = Convert.ToInt32(reader["DocOtherId"]);
                            DocOtherModel.DocumentNo = reader["DocumentNo"].ToString();
                            DocOtherModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            DocOtherModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            DocOtherModel.IssueDate = Convert.ToString(reader["IssueDate"].ToString());
                            DocOtherModel.ExpiryDate = Convert.ToString(reader["ExpiryDate"].ToString());
                            DocOtherModel.Note = reader["Note"].ToString();
                            DocOtherModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            DocOtherModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            DocOtherModel.DocOtherFile = Convert.ToString(reader["DocOtherFile"]);
                            DocOtherModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            DocOtherModelList.Add(DocOtherModel);
                        }
                    }
                }
                // Finally, we return our list of DocOtherModelList
                totalCount = TotalCount;
                return DocOtherModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching LabourContract List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
