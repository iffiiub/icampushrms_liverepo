﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class EmployeeTrainingDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        DataAccess.GeneralDB.DataHelper datahelper;
        public OperationDetails InsertEmployeeTraining(EmployeeTrainingModel EmployeeTrainingModel)
        {
            try
            {
                int NoofDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();

                SqlParameter[] parameters =
                    {

                    new SqlParameter("@CourseTitle",EmployeeTrainingModel.CourseTitle) ,
                    new SqlParameter("@StartDate", CommonDB.SetCulturedDate(EmployeeTrainingModel.StartDate)) ,
                    new SqlParameter("@EndDate",  CommonDB.SetCulturedDate(EmployeeTrainingModel.EndDate)),
                    new SqlParameter("@Cost",Math.Round(Convert.ToDecimal(EmployeeTrainingModel.Cost),NoofDecimalPlaces,MidpointRounding.AwayFromZero).ToString()),
                    new SqlParameter("@Deductable",EmployeeTrainingModel.Deductable) ,
                    new SqlParameter("@Trainee", EmployeeTrainingModel.Trainee) ,
                    new SqlParameter("@EmployeeID", EmployeeTrainingModel.EmployeeID),
                    new SqlParameter("@Location", EmployeeTrainingModel.Location),
                    new SqlParameter("@Allowance",Math.Round(EmployeeTrainingModel.Allowance,NoofDecimalPlaces,MidpointRounding.AwayFromZero)) ,
                    new SqlParameter("@Results", EmployeeTrainingModel.Results) ,
                    new SqlParameter("@Note", EmployeeTrainingModel.Note),
                    new SqlParameter("@CityID", EmployeeTrainingModel.CityId),
                    new SqlParameter("@CountryID",EmployeeTrainingModel.CountryId) ,
                    new SqlParameter("@isComplete", EmployeeTrainingModel.isComplete) ,
                    new SqlParameter("@Total",Math.Round(EmployeeTrainingModel.Total,NoofDecimalPlaces,MidpointRounding.AwayFromZero)),
                    new SqlParameter("@startTime", EmployeeTrainingModel.StartTime),
                    new SqlParameter("@endTime", EmployeeTrainingModel.EndTime),
                     };
                int EmployeeTrainingID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_EmployeeTraining", parameters));

                return new OperationDetails(true, "Employee Training saved successfully.", null, EmployeeTrainingID);
            }


            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Employee Training .", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateEmployeeTraining(EmployeeTrainingModel EmployeeTrainingModel)
        {

            try
            {
                int NoofDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
                SqlParameter[] parameter =
                    {
                        new SqlParameter("@EmployeeTrainingID",EmployeeTrainingModel.EmployeeTrainingID) ,
                        new SqlParameter("@CourseTitle",EmployeeTrainingModel.CourseTitle) ,
                        new SqlParameter("@StartDate", CommonDB.SetCulturedDate(EmployeeTrainingModel.StartDate)),
                        new SqlParameter("@EndDate",  CommonDB.SetCulturedDate(EmployeeTrainingModel.EndDate)),
                        new SqlParameter("@Cost",Math.Round(Convert.ToDecimal(EmployeeTrainingModel.Cost),NoofDecimalPlaces,MidpointRounding.AwayFromZero).ToString()),
                        new SqlParameter("@Deductable",EmployeeTrainingModel.Deductable) ,
                        new SqlParameter("@Trainee", EmployeeTrainingModel.Trainee) ,
                        new SqlParameter("@EmployeeID", EmployeeTrainingModel.EmployeeID),
                        new SqlParameter("@Location", EmployeeTrainingModel.Location),
                        new SqlParameter("@Allowance",Math.Round(EmployeeTrainingModel.Allowance,NoofDecimalPlaces,MidpointRounding.AwayFromZero)) ,
                        new SqlParameter("@Results", EmployeeTrainingModel.Results) ,
                        new SqlParameter("@Note", EmployeeTrainingModel.Note),
                        new SqlParameter("@CityID", EmployeeTrainingModel.CityId),
                        new SqlParameter("@CountryID",EmployeeTrainingModel.CountryId) ,
                        new SqlParameter("@isComplete", EmployeeTrainingModel.isComplete) ,
                        new SqlParameter("@Total",Math.Round(EmployeeTrainingModel.Total,NoofDecimalPlaces,MidpointRounding.AwayFromZero)),
                        new SqlParameter("@startTime", EmployeeTrainingModel.StartTime),
                        new SqlParameter("@endTime", EmployeeTrainingModel.EndTime),
                                       };
                SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                         "stp_Update_HR_EmployeeTraining", parameter);

                //DeleteProfessionalDevelopment(EmployeeTrainingModel.EmployeeTrainingID);


                return new OperationDetails(true, "Employee Training Updated successfully.", null);




            }


            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while Updating Employee Training .", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteProfessionalDevelopment(int TrainingID)
        {

            try
            {

                SqlParameter[] parameter =
                    {
                        new SqlParameter("@TrainingID", TrainingID)



                                       };
                int ProfessionalDevelopmentID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                       "stp_Delete_ProfessionalDevelopment", parameter));


                return new OperationDetails(true, "Professional Development Deleted successfully.", null, ProfessionalDevelopmentID);

            }


            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while Deleting Professional Development .", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteEmployeeTraining(int EmployeeTrainingID)
        {

            try
            {

                SqlParameter[] parameter =
                    {
                        new SqlParameter("@EmployeeTrainingID", EmployeeTrainingID)



                                       };
                int ProfessionalDevelopmentID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                       "stp_Delete_HR_EmployeTraining", parameter));


                return new OperationDetails(true, "Employee Training Deleted successfully.", null, EmployeeTrainingID);

            }


            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while Deleting Employee Training .", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails BulkCRUDEmployeeTrainingProfessional(List<EmployeeProfessionalTrainingModel> tainingUpdateModel, int employeeTrainingId)
        {
            string query = "";
            string nIds = "";
            int count = 0;
            datahelper = new DataHelper();
            try
            {
                if (tainingUpdateModel != null)
                {
                    if (tainingUpdateModel.Count() > 0)
                    {
                        query = "Delete from HR_ProfessionalDevelopment where TrainingId = " + employeeTrainingId;
                        datahelper.SimpleCommandText(query);
                        query = "";
                        foreach (var item in tainingUpdateModel.Where(x => x.ProfessionalTrainingID == 0))
                        {
                            if (count == 0)
                                //nIds = nIds + "(" + item.EmployeeID + "," + employeeTrainingId + ",0,1,'" + DateTime.Now.ToString() + "',null)";
                                nIds = nIds + "(" + item.EmployeeID + "," + employeeTrainingId + ",0,1,GETDATE(),null)";
                            else
                                //nIds = nIds + ",(" + item.EmployeeID + "," + employeeTrainingId + ",0,1,'" + DateTime.Now.ToString() + "',null)";
                                nIds = nIds + ",(" + item.EmployeeID + "," + employeeTrainingId + ",0,1,GETDATE(),null)";
                                count++;
                        }

                        query = "insert into HR_ProfessionalDevelopment values" + nIds;
                        datahelper.SimpleCommandText(query);
                    }
                }
                return new OperationDetails(true, "Employee Training Deleted successfully.", null, 0);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while Deleting Employee Training .", exception);
                //throw;
            }
            finally
            {

            }
        }


        public List<EmployeeProfessionalTrainingModel> GetEmployeeGridList(int CompanyId, int trainingId, int DepartmentId)
        {
            List<EmployeeProfessionalTrainingModel> EmployeeTrainingModelList = null;

            try
            {
                EmployeeTrainingModelList = new List<EmployeeProfessionalTrainingModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Get_EmployeeGridForTraining", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyId);
                sqlCommand.Parameters.AddWithValue("@TrainingID", trainingId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", DepartmentId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeProfessionalTrainingModel employeeTrainingModel;
                    while (sqlDataReader.Read())
                    {
                        employeeTrainingModel = new EmployeeProfessionalTrainingModel();
                        employeeTrainingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeTrainingModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeTrainingModel.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        employeeTrainingModel.IsPresent = Convert.ToBoolean(sqlDataReader["IsPresent"].ToString() == "1" ? true : false);
                        employeeTrainingModel.ProfessionalTrainingID = Convert.ToInt32(sqlDataReader["ProfessionalDevelopmentId"].ToString());

                        EmployeeTrainingModelList.Add(employeeTrainingModel);
                    }
                }
                sqlConnection.Close();
                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeTrainingModelList.Where(x => x.EmployeeID != 0).ToList();
        }

        public List<EmployeeTrainingModel> GetOnGoingTrainingList(int EmployeeID)
        {
            List<EmployeeTrainingModel> EmployeeTrainingModelList = null;

            try
            {
                EmployeeTrainingModelList = new List<EmployeeTrainingModel>();
                Employee Employee = new Entities.Employee();


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_OnGOingTrainingList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeTrainingModel EmployeeTrainingModel;
                    while (sqlDataReader.Read())
                    {
                        EmployeeTrainingModel = new EmployeeTrainingModel();

                        EmployeeTrainingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        EmployeeTrainingModel.Fullname = sqlDataReader["FullName"].ToString();
                        EmployeeTrainingModel.GenderID = Convert.ToInt32(sqlDataReader["Gender"] == DBNull.Value ? "0" : sqlDataReader["Gender"].ToString());
                        EmployeeTrainingModel.EmployeeAlternativeID = Convert.ToInt32(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlternativeID"].ToString());
                        EmployeeTrainingModel.isAssign = Convert.ToBoolean(sqlDataReader["isAssign"]);
                        EmployeeTrainingModel.isComplete = Convert.ToBoolean(sqlDataReader["isComplete"]);
                        EmployeeTrainingModel.EmployeeTrainingID = Convert.ToInt32(sqlDataReader["TrainingID"] == DBNull.Value ? "0" : sqlDataReader["TrainingID"].ToString());
                        EmployeeTrainingModel.CourseTitleName = sqlDataReader["HRCourseName_1"].ToString();
                        EmployeeTrainingModel.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        EmployeeTrainingModel.EndDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EndDate"].ToString());
                        EmployeeTrainingModel.Trainee = sqlDataReader["TrainerName"].ToString();

                        EmployeeTrainingModelList.Add(EmployeeTrainingModel);
                    }
                }
                sqlConnection.Close();
                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeTrainingModelList;
        }

        public List<EmployeeTrainingModel> GetHistoryTrainingList(int EmployeeID)
        {
            List<EmployeeTrainingModel> EmployeeTrainingModelList = null;

            try
            {
                EmployeeTrainingModelList = new List<EmployeeTrainingModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HistoryTrainingList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeTrainingModel EmployeeTrainingModel;
                    while (sqlDataReader.Read())
                    {

                        EmployeeTrainingModel = new EmployeeTrainingModel();

                        EmployeeTrainingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        EmployeeTrainingModel.Fullname = sqlDataReader["FullName"].ToString();
                        EmployeeTrainingModel.GenderID = Convert.ToInt32(sqlDataReader["Gender"] == DBNull.Value ? "0" : sqlDataReader["Gender"].ToString());
                        EmployeeTrainingModel.EmployeeAlternativeID = Convert.ToInt32(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeAlternativeID"].ToString());
                        EmployeeTrainingModel.isAssign = Convert.ToBoolean(sqlDataReader["isAssign"]);
                        EmployeeTrainingModel.isComplete = Convert.ToBoolean(sqlDataReader["isComplete"]);
                        EmployeeTrainingModel.EmployeeTrainingID = Convert.ToInt32(sqlDataReader["TrainingID"] == DBNull.Value ? "0" : sqlDataReader["TrainingID"].ToString());
                        EmployeeTrainingModel.CourseTitleName = sqlDataReader["HRCourseName_1"].ToString();
                        EmployeeTrainingModel.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        EmployeeTrainingModel.EndDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EndDate"].ToString());
                        EmployeeTrainingModel.Trainee = sqlDataReader["TrainerName"].ToString();

                        EmployeeTrainingModelList.Add(EmployeeTrainingModel);
                    }
                }
                sqlConnection.Close();
                //          totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeTrainingModelList;
        }

        public List<EmployeeTrainingModel> GetTrainingDataList()
        {
            List<EmployeeTrainingModel> EmployeeTrainingModelList = EmployeeTrainingModelList = new List<EmployeeTrainingModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetTrainingData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeTrainingModel EmployeeTrainingModel;
                    while (sqlDataReader.Read())
                    {
                        EmployeeTrainingModel = new EmployeeTrainingModel();
                        EmployeeTrainingModel.EmployeeTrainingID = Convert.ToInt32(sqlDataReader["EmployeeTrainingID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeTrainingID"].ToString());
                        EmployeeTrainingModel.CourseTitleName = sqlDataReader["CourseTitle"].ToString();
                        EmployeeTrainingModel.Cost = Convert.ToString(sqlDataReader["Cost"] == DBNull.Value || sqlDataReader["Cost"].ToString() == "" ? "0" : sqlDataReader["Cost"].ToString());
                        EmployeeTrainingModel.Deductable = Convert.ToBoolean(sqlDataReader["Deductable"]);
                        EmployeeTrainingModel.Trainee = sqlDataReader["Trainee"].ToString();
                        EmployeeTrainingModel.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        EmployeeTrainingModel.EndDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EndDate"].ToString());
                        EmployeeTrainingModel.StartTime = sqlDataReader["StartTime"].ToString();
                        EmployeeTrainingModel.EndTime = sqlDataReader["EndTime"].ToString();
                        EmployeeTrainingModel.Location = sqlDataReader["Location"].ToString();
                        EmployeeTrainingModel.CityName = sqlDataReader["City"].ToString();
                        EmployeeTrainingModel.CountryName = sqlDataReader["Country"].ToString();
                        EmployeeTrainingModel.Allowance = Convert.ToDouble(sqlDataReader["Allowance"] == DBNull.Value ? "0" : sqlDataReader["Allowance"].ToString());
                        EmployeeTrainingModel.Results = Convert.ToInt32(sqlDataReader["Results"] == DBNull.Value ? "0" : sqlDataReader["Results"].ToString());
                        EmployeeTrainingModel.Note = Convert.ToString(sqlDataReader["Note"]);
                        EmployeeTrainingModel.isComplete = Convert.ToBoolean(sqlDataReader["isComplete"]);
                        EmployeeTrainingModel.Total = Convert.ToDecimal(sqlDataReader["total"] == DBNull.Value ? "0" : sqlDataReader["total"].ToString());
                        EmployeeTrainingModelList.Add(EmployeeTrainingModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeTrainingModelList;
        }

        public List<EmployeeTrainingModel> GetAllCourse()
        {
            List<EmployeeTrainingModel> EmployeeTrainingList = new List<EmployeeTrainingModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_GetAllCourses", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeTrainingModel objEmployeeTrainingModel;
                    while (sqlDataReader.Read())
                    {
                        objEmployeeTrainingModel = new EmployeeTrainingModel();
                        objEmployeeTrainingModel.CourseTitle = Convert.ToInt32(sqlDataReader["HRCourseID"] == DBNull.Value ? "0" : sqlDataReader["HRCourseID"].ToString());
                        objEmployeeTrainingModel.CourseTitleName = Convert.ToString(sqlDataReader["HRCourseName_1"]);
                        EmployeeTrainingList.Add(objEmployeeTrainingModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return EmployeeTrainingList;
        }

        public EmployeeTrainingModel GetEmployeeTrainingById(int ID)
        {
            EmployeeTrainingModel objEmployeeTrainingModel = new EmployeeTrainingModel();
            try
            {
                string AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeTrainingById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeTrainingID", ID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {

                        objEmployeeTrainingModel.EmployeeTrainingID = Convert.ToInt32(sqlDataReader["EmployeeTrainingID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeTrainingID"].ToString());
                        objEmployeeTrainingModel.CourseTitle = Convert.ToInt32(sqlDataReader["CourseTitle"] == DBNull.Value ? "0" : sqlDataReader["CourseTitle"].ToString());
                        objEmployeeTrainingModel.Cost = Convert.ToDecimal(sqlDataReader["Cost"] == DBNull.Value || sqlDataReader["Cost"].ToString() == "" ? "0" : sqlDataReader["Cost"].ToString()).ToString(AmountFormat);
                        objEmployeeTrainingModel.Deductable = Convert.ToBoolean(sqlDataReader["Deductable"]);
                        objEmployeeTrainingModel.Trainee = sqlDataReader["Trainee"].ToString();
                        objEmployeeTrainingModel.StartDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        objEmployeeTrainingModel.EndDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EndDate"].ToString());
                        //objEmployeeTrainingModel.StartTime = sqlDataReader["StartTime"].ToString() == "" ? " " : sqlDataReader["StartTime"].ToString();
                        //objEmployeeTrainingModel.EndTime = sqlDataReader["EndTime"].ToString() == "" ? " " : sqlDataReader["EndTime"].ToString();
                        objEmployeeTrainingModel.Location = sqlDataReader["Location"].ToString();
                        objEmployeeTrainingModel.CityId = Convert.ToInt32(sqlDataReader["CityID"] == DBNull.Value ? "0" : sqlDataReader["CityID"].ToString());
                        objEmployeeTrainingModel.CountryId = Convert.ToInt32(sqlDataReader["CountryID"] == DBNull.Value ? "0" : sqlDataReader["CountryID"].ToString());
                        objEmployeeTrainingModel.CityName = sqlDataReader["CityName"].ToString();
                        objEmployeeTrainingModel.CountryName = sqlDataReader["CountryName"].ToString();
                        objEmployeeTrainingModel.Allowance = Convert.ToDouble(sqlDataReader["Allowance"] == DBNull.Value ? "0" : sqlDataReader["Allowance"].ToString());
                        objEmployeeTrainingModel.Results = Convert.ToInt32(sqlDataReader["Results"] == DBNull.Value ? "0" : sqlDataReader["Results"].ToString());
                        objEmployeeTrainingModel.Note = Convert.ToString(sqlDataReader["Note"]);
                        objEmployeeTrainingModel.isComplete = Convert.ToBoolean(sqlDataReader["isComplete"]);
                        objEmployeeTrainingModel.Total = Convert.ToDecimal(sqlDataReader["total"] == DBNull.Value ? "0" : sqlDataReader["total"].ToString());
                        //2019-02-13 , Task#9521 related bugs 
                        objEmployeeTrainingModel.HRCourseName_1 = sqlDataReader["HRCourseName_1"].ToString();
                        try
                        {
                            if (sqlDataReader["startTime"].ToString() != "")
                            {
                                TimeSpan time = TimeSpan.Parse(sqlDataReader["startTime"].ToString());
                                DateTime temptime = DateTime.Today.Add(time);
                                objEmployeeTrainingModel.StartTime = temptime.ToString("hh:mm tt");
                            }
                            if (sqlDataReader["endTime"].ToString() != "")
                            {
                                TimeSpan time = TimeSpan.Parse(sqlDataReader["endTime"].ToString());
                                DateTime temptime = DateTime.Today.Add(time);
                                objEmployeeTrainingModel.EndTime = temptime.ToString("hh:mm tt");
                            }

                        }
                        catch { }

                    }

                }
                sqlConnection.Close();
                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;

            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objEmployeeTrainingModel;

        }


        public OperationDetails UpdateEmployeeTrainingByEmployeeId(string EmployeeID, int EmployeeTrainingId)
        {
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_UpdateEmployeeTrainingByEmployeeId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@TrainingID", EmployeeTrainingId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(false, " ", null);
        }


    }
}

