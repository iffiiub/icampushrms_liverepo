﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace HRMS.DataAccess
{
    public class DepartmentEmployeeDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        //public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        ///// <summary>
        ///// Add Active users details
        ///// </summary>
        ///// <param name="activeuserlistModel"></param>
        ///// <returns></returns>
        //public string AddDepartment(DepartmentModel DepartmentModel)
        //{
        //    string Message = "";
        //    try
        //    {
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_Add_HR_Department", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        sqlCommand.Parameters.AddWithValue("@CompanyId", DepartmentModel.CompanyId);

        //        sqlCommand.Parameters.AddWithValue("@DepartmentName_1", DepartmentModel.DepartmentName_1);
        //        sqlCommand.Parameters.AddWithValue("@DepartmentName_2", DepartmentModel.DepartmentName_2);
        //        sqlCommand.Parameters.AddWithValue("@DepartmentName_3", DepartmentModel.DepartmentName_3);
        //        sqlCommand.Parameters.AddWithValue("@Description", DepartmentModel.Description);

        //        sqlCommand.Parameters.AddWithValue("@StartDate", DepartmentModel.StartDate);
        //        sqlCommand.Parameters.AddWithValue("@CreatedBy", DepartmentModel.CreatedBy);


        //        SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
        //        OperationMessage.Direction = ParameterDirection.Output;
        //        sqlCommand.Parameters.Add(OperationMessage);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        sqlConnection.Close();

        //        Message = OperationMessage.Value.ToString();

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return Message;
        //}

        ///// <summary>
        ///// Update Active users details
        ///// </summary>
        ///// <param name="activeuserlistModel"></param>
        ///// <returns></returns>
        //public string UpdateDepartment(DepartmentModel DepartmentModel)
        //{
        //    string Message = "";
        //    try
        //    {
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_Update_HR_Department", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        sqlCommand.Parameters.AddWithValue("@DepartmentId", DepartmentModel.DepartmentId);
        //        sqlCommand.Parameters.AddWithValue("@DepartmentName_1", DepartmentModel.DepartmentName_1);
        //        sqlCommand.Parameters.AddWithValue("@DepartmentName_2", DepartmentModel.DepartmentName_2);
        //        sqlCommand.Parameters.AddWithValue("@DepartmentName_3", DepartmentModel.DepartmentName_3);
        //        sqlCommand.Parameters.AddWithValue("@Description", DepartmentModel.Description);
        //        sqlCommand.Parameters.AddWithValue("@CreatedBy", DepartmentModel.CreatedBy);
        //        sqlCommand.Parameters.AddWithValue("@ModifiedBy", DepartmentModel.ModifiedBy);

        //        SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
        //        OperationMessage.Direction = ParameterDirection.Output;
        //        sqlCommand.Parameters.Add(OperationMessage);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        sqlConnection.Close();

        //        Message = OperationMessage.Value.ToString();

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return Message;
        //}


        ///// <summary>
        ///// Delete Active users details
        ///// </summary>
        ///// <param name="activeuserlistModel"></param>
        ///// <returns></returns>
        //public string DeleteDepartment(int id)
        //{
        //    string Message = "";
        //    try
        //    {
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_Delete_HR_Department", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        sqlCommand.Parameters.AddWithValue("@DepartmentID", id);

        //        SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
        //        OperationMessage.Direction = ParameterDirection.Output;
        //        sqlCommand.Parameters.Add(OperationMessage);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        sqlConnection.Close();

        //        Message = OperationMessage.Value.ToString();

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return Message;
        //}


        //public DepartmentModel GetDepartment(int id)
        //{
        //    DepartmentModel DepartmentModel = new DepartmentModel();

        //    try
        //    {

        //        connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_Get_DepartmentById", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        sqlCommand.Parameters.AddWithValue("@DepartmentID", id);



        //        // SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
        //        // TotalCount.Direction = ParameterDirection.Output;
        //        //sqlCommand.Parameters.Add(TotalCount);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {

        //            if (sqlDataReader.Read())
        //            {
        //                DepartmentModel = new DepartmentModel();

        //                DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
        //                DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
        //                DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
        //                DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
        //                DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
        //                DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);


        //                //DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
        //                //DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"]);
        //                //DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
        //                //DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
        //                //DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
        //                //DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);
        //                //DepartmentModel.StartDate = Convert.ToDateTime(sqlDataReader["StartDate"].ToString());
        //                //DepartmentModel.CreatedBy = Convert.ToInt32(sqlDataReader["CreatedBy"].ToString());

        //            }
        //        }
        //        sqlConnection.Close();



        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }

        //    return DepartmentModel;
        //}

        ///// <summary>
        ///// Get All Active Users
        ///// </summary>
        ///// <param name="pageNumber"></param>
        ///// <param name="numberOfRecords"></param>
        ///// <param name="sortColumn"></param>
        ///// <param name="sortOrder"></param>
        ///// <returns></returns>


        public List<DepartmentModel> GetAllDepartment(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, out int totalCount)
        {
            List<DepartmentModel> DepartmentModelList = null;
            try
            {
                DepartmentModelList = new List<DepartmentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Department", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);


                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DepartmentModel DepartmentModel;
                    while (sqlDataReader.Read())
                    {
                        DepartmentModel = new DepartmentModel();

                        DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        DepartmentModel.DepartmentName_2 = Convert.ToString(sqlDataReader["DepartmentName_2"]);
                        DepartmentModel.DepartmentName_3 = Convert.ToString(sqlDataReader["DepartmentName_3"]);
                        DepartmentModel.Description = Convert.ToString(sqlDataReader["Description"]);

                        //if (sqlDataReader["StartDate"])

                        if (!string.IsNullOrEmpty(sqlDataReader["StartDate"].ToString()))
                        {
                            DepartmentModel.StartDate = sqlDataReader["StartDate"] == DBNull.Value ? DateTime.Now.ToString("MM/dd/yyyy") : Convert.ToDateTime(Convert.ToString(sqlDataReader["StartDate"])).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            DepartmentModel.StartDate = DateTime.Now.ToString("MM/dd/yyyy");
                        }

                        // DepartmentModel.CreatedBy =Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) ;

                        DepartmentModelList.Add(DepartmentModel);
                    }
                }
                sqlConnection.Close();
                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DepartmentModelList;
        }
        
        public List<DepartmentModel> GetDepartmentByCompanyId(int companyid)
        {
            List<DepartmentModel> DepartmentModelList = new List<DepartmentModel>();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_DepartmentByCompanyId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyid);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    DepartmentModel DepartmentModel;
                    while (sqlDataReader.Read())
                    {
                        DepartmentModel = new DepartmentModel();
                        DepartmentModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        DepartmentModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"].ToString());
                        DepartmentModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        DepartmentModelList.Add(DepartmentModel);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();

                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();

                }
            }
            return DepartmentModelList;
        }

        public List<EmployeeDetails> GetEmployeeByDepartmentId(int departmentid, int CompanyId)    
        {
            List<EmployeeDetails> EmployeeDetailsList = new List<EmployeeDetails>();            
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeByDepartmentId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentID", departmentid);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)                               
                {                 
                    EmployeeDetails EmployeeDetails;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDetails = new EmployeeDetails();
                        EmployeeDetails.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeId"]).ToString();
                        EmployeeDetails.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDetails.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"].ToString());
                        EmployeeDetails.SurName_1 = Convert.ToString(sqlDataReader["Surname_1"].ToString());
                        EmployeeDetails.FullName = Convert.ToString(sqlDataReader["FullName"].ToString());
                        EmployeeDetails.isLeader = Convert.ToInt32(sqlDataReader["isLeader"].ToString());
                        EmployeeDetails.isActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString() == "" ? "false" : sqlDataReader["IsActive"].ToString());
                        //EmployeeDetails.FullName = "(" + EmployeeDetails.EmployeeAlternativeID + ") " + EmployeeDetails.FirstName_1 + " " + EmployeeDetails.SurName_1;
                        EmployeeDetailsList.Add(EmployeeDetails);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null) 
                {
                    sqlReader.Close();

                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();

                }
            }
            return EmployeeDetailsList.Where(x=>x.EmployeeID != 0).ToList();
        
        
        }

        
        
        public List<EmployeeDetails> GetEmployeeNotInDepartmentId(int departmentid, int CompanyId, int userId)
        {
            //departmentid = 1;

            List<EmployeeDetails> EmployeeDetailsList = new List<EmployeeDetails>();

            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_EmployeeNotInDepartmentId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DepartmentID", departmentid);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    EmployeeDetails EmployeeDetails;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDetails = new EmployeeDetails();
                        EmployeeDetails.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDetails.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"].ToString());
                        EmployeeDetails.SurName_1 = Convert.ToString(sqlDataReader["Surname_1"].ToString());
                        EmployeeDetails.FullName = Convert.ToString(sqlDataReader["FullName"].ToString());
                        EmployeeDetails.isActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString() == "" ? "false" : sqlDataReader["IsActive"].ToString());
                        EmployeeDetailsList.Add(EmployeeDetails);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();

                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();

                }
            }
            return EmployeeDetailsList.Where(x=>x.EmployeeID != 0).ToList();


        }

        public string UpdateDepartmentEmployeeByEmployeeId(int departmentid, string EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_DepartmentEmployeeByEmployeeId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DepartmentID", departmentid);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        public string DeleteDepartmentEmployeeByEmployeeId(string EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_DepartmentEmployeeByEmployeeId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

              
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Insert Employee Department
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public string InsertEmployeeDepartment(int DepartmentID, int EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Department_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();


                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Remove Employee From Department
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public string RemoveEmployeeDepartment(int DepartmentID, int EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Remove_HR_Department_Employee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Update Employee Department Leader
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public string UpdateDepartmentEmployeeLeader(int DepartmentID, int EmployeeID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_Department_Employee_Leader", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }
    }
}
