﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class PayEmployeeAbsentHeaderDB : DBHelper
    {
        public OperationDetails InsertPayEmployeeAbsentHeader(PayEmployeeAbsentHeaderModel objPayEmployeeAbsentHeaderModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aSntEmployeeID", objPayEmployeeAbsentHeaderModel.EmployeeID) ,
                      new SqlParameter("@aSntPayAbsentTypeID", objPayEmployeeAbsentHeaderModel.PayAbsentTypeID) ,
                      new SqlParameter("@aDttEntryDate", objPayEmployeeAbsentHeaderModel.EntryDate),
                      new SqlParameter("@aBitActive", objPayEmployeeAbsentHeaderModel.Active )
                    };
                int AbsentId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayEmployeeAbsentHeaderCUD  ", parameters));
                return new OperationDetails(true, "Absent Header saved successfully.", null, AbsentId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Absent Header.", exception);
                //throw;
            }
            finally
            {

            }
        }
    }
}
