﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class LabourContractDB : DBHelper
    {
        public OperationDetails InsertLabourContract(LabourContractModel LabourContractModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@DocumentNo",LabourContractModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", LabourContractModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", LabourContractModel.IssuePlace) ,
                      new SqlParameter("@IssueDate", GeneralDB.CommonDB.SetCulturedDate(LabourContractModel.IssueDate)),
                      new SqlParameter("@ExpiryDate",  GeneralDB.CommonDB.SetCulturedDate(LabourContractModel.ExpiryDate )) ,
                      new SqlParameter("@Note",LabourContractModel.Note ) ,
                      new SqlParameter("@IsPrimary", LabourContractModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", LabourContractModel.EmployeeId),
                      new SqlParameter("@IsDeleted", LabourContractModel.IsDeleted) ,
                      new SqlParameter("@SponsorName",LabourContractModel.SponsorName) ,
                      new SqlParameter("@ContractType", LabourContractModel.ContractType),
                      new SqlParameter("@LabourContractFile", LabourContractModel.LabourContractFile)
                    };
                int LabourContractId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_LabourContract", parameters));
                return new OperationDetails(true, "Labour Contract saved successfully.", null, LabourContractId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Labour Contract.", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateLabourContract(LabourContractModel LabourContractModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@DocLabourContract",LabourContractModel.DocLabourContract),
                      new SqlParameter("@DocumentNo",LabourContractModel.DocumentNo) ,
                      new SqlParameter("@IssueCountry", LabourContractModel.IssueCountry) ,
                      new SqlParameter("@IssuePlace", LabourContractModel.IssuePlace) ,
                      new SqlParameter("@IssueDate",  GeneralDB.CommonDB.SetCulturedDate(LabourContractModel.IssueDate)),
                      new SqlParameter("@ExpiryDate",  GeneralDB.CommonDB.SetCulturedDate(LabourContractModel.ExpiryDate )) ,
                      new SqlParameter("@Note",LabourContractModel.Note ) ,
                      new SqlParameter("@IsPrimary", LabourContractModel.IsPrimary ),
                      new SqlParameter("@EmployeeId", LabourContractModel.EmployeeId),
                      new SqlParameter("@IsDeleted", LabourContractModel.IsDeleted) ,
                      new SqlParameter("@SponsorName",LabourContractModel.SponsorName) ,
                      new SqlParameter("@ContractType", LabourContractModel.ContractType),
                      new SqlParameter("@LabourContractFile", LabourContractModel.LabourContractFile)
                      
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_LabourContract", parameters);
                return new OperationDetails(true, "Labour Contract updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Labour Contract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public LabourContractModel LabourContractById(int LabourContractId)
        {
            try
            {
                LabourContractModel LabourContractModel = new LabourContractModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_LabourContractByID", new SqlParameter("@LabourContractID", LabourContractId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            LabourContractModel.DocLabourContract = Convert.ToInt32(reader["DocLabourContract"].ToString());
                            LabourContractModel.DocumentNo = reader["DocumentNo"].ToString();
                            LabourContractModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            LabourContractModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            LabourContractModel.IssueDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString()) ;
                            LabourContractModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            LabourContractModel.Note = reader["Note"].ToString() ;
                            LabourContractModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString()) ;
                            LabourContractModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]) ;
                            LabourContractModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString() );
                            LabourContractModel.SponsorName = reader["SponsorName"].ToString() ;
                            LabourContractModel.ContractType =reader["ContractType"]==DBNull.Value ? 0 : Convert.ToInt32(reader["ContractType"].ToString());
                            LabourContractModel.LabourContractFile = reader["LabourContractFile"].ToString();
                        }
                    }
                }
                return LabourContractModel;
                // Finally, we return Labour Contract

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Labour Contract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteLabourContractById(int LabourContractId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@LabourContractID",LabourContractId) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_LabourContractByID", parameters);
                return new OperationDetails(true, "Labour deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting LabourContract.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<LabourContractModel> GetLabourContractListWithPaging(int offset, int rowCount, string sortColumn, string sortOrder,string empid, out int totalCount)
        {
            int TotalCount = 0;
            SqlParameter[] parameters =
                    {    
                        new SqlParameter("@OffsetRows",offset),
                      new SqlParameter("@FetchRows",rowCount),
                      new SqlParameter("@SortColumn",sortColumn),
                      new SqlParameter("@SortOrder",sortOrder),
                      new SqlParameter("@EmpID",empid)
                    };
            // Create a list to hold the LabourContract		
            List<LabourContractModel> LabourContractModelList = new List<LabourContractModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_LabourContractWithPaging", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        LabourContractModel LabourContractModel;
                        while (reader.Read())
                        {
                            LabourContractModel = new LabourContractModel();
                            TotalCount = reader["TotalCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCount"]);
                            LabourContractModel.DocLabourContract = Convert.ToInt32(reader["DocLabourContract"].ToString());
                            LabourContractModel.DocumentNo = reader["DocumentNo"].ToString();
                            LabourContractModel.IssueCountry = Convert.ToInt32(reader["IssueCountry"].ToString());
                            LabourContractModel.IssuePlace = Convert.ToInt32(reader["IssuePlace"].ToString());
                            LabourContractModel.IssueDate = reader["IssueDate"] == DBNull.Value ? "" : Convert.ToString(reader["IssueDate"].ToString()); 
                            LabourContractModel.ExpiryDate =reader["ExpiryDate"]== DBNull.Value? "" : Convert.ToString(reader["ExpiryDate"].ToString());
                            LabourContractModel.Note = reader["Note"].ToString();
                            LabourContractModel.IsPrimary = Convert.ToBoolean(reader["IsPrimary"].ToString());
                            LabourContractModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                            LabourContractModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            LabourContractModel.SponsorName = reader["SponsorName"].ToString();
                            LabourContractModel.ContractType =reader["ContractType"]==DBNull.Value ? 0 : Convert.ToInt32(reader["ContractType"].ToString());
                            LabourContractModel.LabourContractFile = reader["LabourContractFile"].ToString();
                            LabourContractModelList.Add(LabourContractModel);
                        }
                    }
                }
                // Finally, we return our list of LabourContractModelList
                totalCount = TotalCount;
                return LabourContractModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching LabourContract List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
