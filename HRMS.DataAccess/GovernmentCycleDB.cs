﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class GovernmentCycleDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get all Government Cycle
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<GovernmentCycleModel> GetAllGovernmentCycle()  
        {
            List<GovernmentCycleModel> governmentCycleList = null;
            try
            {
                governmentCycleList = new List<GovernmentCycleModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetGovernmentCycle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;      

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    GovernmentCycleModel governmentCycle;
                    while (sqlDataReader.Read())
                    {
                        governmentCycle = new GovernmentCycleModel();

                        governmentCycle.GovernmentCycleID = Convert.ToInt32(sqlDataReader["GovernmentCycleID"].ToString());
                        governmentCycle.GovernmentCycleName_1 = sqlDataReader["Government Cycle Name1"] == DBNull.Value ? "" : sqlDataReader["Government Cycle Name1"].ToString().Trim();
                        governmentCycle.GovernmentCycleName_2 = sqlDataReader["Government Cycle Name2"] == DBNull.Value ? "" : sqlDataReader["Government Cycle Name2"].ToString().Trim();
                        governmentCycle.GovernmentCycleName_3 = sqlDataReader["Government Cycle Name3"] == DBNull.Value ? "" : sqlDataReader["Government Cycle Name3"].ToString().Trim();
                       
                        governmentCycleList.Add(governmentCycle);
                    }
                }               
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return governmentCycleList;
        }

        public DataSet GetGovernmentCycleDatasSet()
        {
            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            string query = "SELECT GovernmentCycleName_1 AS[Government Cycle Name (En)], GovernmentCycleName_2 AS[Government Cycle Name (Ar)], GovernmentCycleName_3 AS[Government Cycle Name (Fr)] FROM[dbo].[GEN_GovernmentCycle] where GovernmentCycleID > 0 order by [Government Cycle Name (En)] asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataSet ds = new DataSet();
            da.Fill(ds);
         
            return ds;

        }


        public GovernmentCycleModel GetGovernmentCycle(int id)
        {
            GovernmentCycleModel governmentCycle = new GovernmentCycleModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetGovernmentCycleByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@GovernmentCycleID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        governmentCycle = new GovernmentCycleModel();

                        governmentCycle.GovernmentCycleID = Convert.ToInt32(sqlDataReader["GovernmentCycleID"].ToString());
                        governmentCycle.GovernmentCycleName_1 = sqlDataReader["GovernmentCycleName_1"] == DBNull.Value ? "" : sqlDataReader["GovernmentCycleName_1"].ToString();
                        governmentCycle.GovernmentCycleName_2 = sqlDataReader["GovernmentCycleName_2"] == DBNull.Value ? "" : sqlDataReader["GovernmentCycleName_2"].ToString();
                        governmentCycle.GovernmentCycleName_3 = sqlDataReader["GovernmentCycleName_3"] == DBNull.Value ? "" : sqlDataReader["GovernmentCycleName_3"].ToString();                       
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return governmentCycle;
        }

        /// <summary>
        /// Add/Update/Delete GovernmentCycle
        /// </summary>
        /// <param name="governmentCycle"></param>
        /// <param name="operationType"></param>
        /// <returns></returns>
        public string AddUpdateDeleteGovernmentCycle(GovernmentCycleModel governmentCycle, int operationType)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGovernmentCycleCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationType);
                sqlCommand.Parameters.AddWithValue("@GovernmentCycleName_1", governmentCycle.GovernmentCycleName_1);
                sqlCommand.Parameters.AddWithValue("@GovernmentCycleName_2", governmentCycle.GovernmentCycleName_2);
                sqlCommand.Parameters.AddWithValue("@GovernmentCycleName_3", governmentCycle.GovernmentCycleName_3);

                

                if (operationType == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@GovernmentCycleID",0);
                }
                else if (operationType == 2)
                {
                    sqlCommand.Parameters.AddWithValue("@GovernmentCycleID", governmentCycle.GovernmentCycleID);
                }
                else if (operationType == 3)
                {
                    sqlCommand.Parameters.AddWithValue("GovernmentCycleID", governmentCycle.GovernmentCycleID);
                }

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                //throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }
    }
}
