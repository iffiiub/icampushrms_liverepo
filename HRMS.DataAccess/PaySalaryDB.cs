﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class PaySalaryDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public string format = "dd-MM-yyyy";
        public string sqlDateFormat = "MM-dd-yyyy hh:mm:ss";


        public OperationDetails PaySalaryCRUD(PaySalaryModel paySalaryModel, int OperationId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPaySalaryCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", OperationId);
                sqlCommand.Parameters.AddWithValue("@aIntPaySalaryID", paySalaryModel.PaySalaryID);
                sqlCommand.Parameters.AddWithValue("@aSntPaySalaryAllowanceID", paySalaryModel.PaySalaryAllowanceID);
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeId", paySalaryModel.EmployeeId);
                //sqlCommand.Parameters.AddWithValue("@aFltAmount", paySalaryModel.Amount);
                sqlCommand.Parameters.AddWithValue("@aBitIsActive", paySalaryModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@aBitAddition", paySalaryModel.Addition);
                sqlCommand.Parameters.AddWithValue("@aNvrNote", paySalaryModel.Note);
                sqlCommand.Parameters.AddWithValue("@aDttTrDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(paySalaryModel.TrDate));
                sqlCommand.Parameters.AddWithValue("@aAllowanceEndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(paySalaryModel.AllowanceEndDate));
                sqlCommand.Parameters.AddWithValue("@aIntCurrency", paySalaryModel.Currency);
                sqlCommand.Parameters.AddWithValue("@aBitIsFixed", paySalaryModel.IsFixed);
                sqlCommand.Parameters.AddWithValue("@aFltAmount1", paySalaryModel.Amount1);

                SqlParameter AmountVal = new SqlParameter("@aFltAmount", SqlDbType.Float);
                AmountVal.SqlValue = paySalaryModel.Amount;
                AmountVal.Precision = 2;
                sqlCommand.Parameters.Add(AmountVal);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();


                if (OperationId == 1)
                {
                    return new OperationDetails(true, "Salary details saved successfully.", null, id);
                }
                else
                {
                    return new OperationDetails(true, "Salary details deleted successfully.", null, id);
                }
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while saving salary details.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public List<PaySalaryModel> GetEmployeeSalaryById(int employeeId)
        {
            PaySalaryModel paySalary = null;
            List<PaySalaryModel> paySalaryList = new List<PaySalaryModel>();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPaySalary", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeId", employeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        paySalary = new PaySalaryModel();
                        paySalary.PaySalaryID = Convert.ToInt32(sqlDataReader["PaySalaryID"].ToString());
                        paySalary.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"]);
                        paySalary.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        paySalary.Amount = (float)(Convert.ToDecimal(sqlDataReader["Amount"]));
                        paySalary.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        paySalary.Addition = Convert.ToBoolean(sqlDataReader["Addition"]);
                        paySalary.Note = sqlDataReader["Note"].ToString();
                        paySalary.TrDate = sqlDataReader["TrDate"] == DBNull.Value ? "" : Convert.ToDateTime(sqlDataReader["TrDate"]).ToString("dd/MM/yyyy");
                        paySalary.Currency = Convert.ToInt32(sqlDataReader["Currency"]);
                        paySalary.IsFixed = Convert.ToBoolean(sqlDataReader["IsFixed"]);
                        paySalary.Amount1 = Convert.ToInt32(sqlDataReader["Amount1"]);
                        paySalary.PaySalaryAllowanceName_1 = sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        paySalary.LastPaidDate = sqlDataReader["LastPaidDate"].ToString();
                        paySalary.AllowanceEndDate = sqlDataReader["AllowanceEndDate"].ToString();
                        paySalaryList.Add(paySalary);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySalaryList;
        }

        public decimal GetEmployeeTotalSalary(int EmployeeId)
        {
            decimal TotalSalary = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Select TotalSalary from HR_EmployeeSalaryGrade WHERE EmployeeId=@EmployeeId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {

                        TotalSalary = Convert.ToDecimal(sqlDataReader["TotalSalary"]);

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return TotalSalary;

        }

        public OperationDetails UpdateTotalSalaryFromEmployeeSalaryGrade(PaySalaryModel paySalaryModel)
        {
            decimal TotalSalary = GetEmployeeTotalSalary(paySalaryModel.EmployeeId);
            OperationDetails op = new OperationDetails();

            string Message = "";
            int id = 0;

            if (TotalSalary > 0)
            {
                try
                {
                    List<PaySalaryModel> lstEmployeeSalDetails = GetEmployeeSalaryById(paySalaryModel.EmployeeId);

                    TotalSalary = (decimal)(lstEmployeeSalDetails.Where(x => x.IsActive == true)).Sum(x => x.Amount);
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    sqlCommand = new SqlCommand("Update HR_EmployeeSalaryGrade SET TotalSalary=@TotalSalary WHERE EmployeeId=@EmployeeId ", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@TotalSalary", TotalSalary);
                    sqlCommand.Parameters.AddWithValue("@EmployeeId", paySalaryModel.EmployeeId);
                    int noOfRowAffetcted = 0;
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    noOfRowAffetcted = sqlDataReader.RecordsAffected;
                    sqlConnection.Close();


                    if (noOfRowAffetcted > 0)
                    {
                        //  return new OperationDetails(true, "Salary details saved successfully.", null, id);
                        op.Success = true;
                        op.Message = "Total Salary Updated Successfully";

                    }
                    else
                    {
                        // return new OperationDetails(true, "Salary details deleted successfully.", null, id);
                        op.Success = true;
                        op.Message = "Unable to Update Total Salary";
                    }
                }
                catch (Exception exception)
                {
                    return new OperationDetails(false, "Error : while saving salary details.", exception);
                    //throw;
                }
                finally
                {

                }

            }
            return op;
        }

        public bool isSalaryAllowanceAdded(int EmployeeId)
        {
            int EmployeeCount = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select COUNT(EmployeeId) As EmpCount from HR_PaySalary WHERE EmployeeID = @EmployeeID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {

                        EmployeeCount = Convert.ToInt32(sqlDataReader["EmpCount"]);

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (EmployeeCount > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public OperationDetails UpdatePaySalaryStatus(PaySalaryModel PaySalaryModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdatePaySalaryStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PaySalaryID", PaySalaryModel.PaySalaryID);
                sqlCommand.Parameters.AddWithValue("@isActive", PaySalaryModel.IsActive);




                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, PaySalaryModel.PaySalaryID);
        }

        public List<PaySalaryAllowance> GetAllPaySalaryAllowanceList(int addition)
        {
            List<PaySalaryAllowance> paySalaryAllowanceList = null;
            try
            {
                paySalaryAllowanceList = new List<PaySalaryAllowance>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAll_HR_PaySalaryAllowanceInformation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@addition", addition);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySalaryAllowance paySalaryAllowance;
                    while (sqlDataReader.Read())
                    {
                        paySalaryAllowance = new PaySalaryAllowance();

                        paySalaryAllowance.PaySalaryAllowanceID = Convert.ToInt32(sqlDataReader["PaySalaryAllowanceID"].ToString());
                        paySalaryAllowance.PaySalaryAllowanceName_1 = sqlDataReader["PaySalaryAllowanceName_1"] == DBNull.Value ? "" : sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        paySalaryAllowanceList.Add(paySalaryAllowance);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return paySalaryAllowanceList;
        }

        public IEnumerable<PaySalaryAllowance> GetAllPaySalaryListByParameter(int active)
        {
            GeneralDB.DataHelper datahelper = new GeneralDB.DataHelper();
            string query = "select * from GEN_PaySalaryAllowance where IsActive=" + active;
            DataTable dt = datahelper.ExcuteCommandText(query);
            List<PaySalaryAllowance> salaryAllowanceList = new List<PaySalaryAllowance>();
            PaySalaryAllowance salarymodel;
            foreach (DataRow dr in dt.Rows)
            {
                salarymodel = new PaySalaryAllowance();
                salarymodel.PaySalaryAllowanceID = Convert.ToInt32(dr["PaySalaryAllowanceID"].ToString());
                salarymodel.PaySalaryAllowanceName_1 = dr["PaySalaryAllowanceName_1"].ToString();
                salaryAllowanceList.Add(salarymodel);
            }
            return salaryAllowanceList.AsEnumerable();
        }

        public OperationDetails InsertSalaryPackage(List<SalaryPackageAllowances> lstSalAllowances, SalaryPackageModel packageDetails)
        {
            OperationDetails op = new OperationDetails();
            int SalaryPackageId = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_InsertSalaryPackage", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aTransactionType", 1);
                sqlCommand.Parameters.AddWithValue("@aSalaryPackageName", packageDetails.SalaryPackgeName);
                sqlCommand.Parameters.AddWithValue("@aSalaryPackageId", 0);

                if (packageDetails.IsEnabled == null)
                {
                    sqlCommand.Parameters.AddWithValue("@aIsEnabled", 1);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@aIsEnabled", packageDetails.IsEnabled);
                }

                SqlParameter output = new SqlParameter("@aInsertedSalaryPackageId", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                sqlConnection.Close();


                SalaryPackageId = Convert.ToInt32(output.Value);


            }
            catch (Exception exception)
            {
                op.Message = exception.Message;
                op.Success = false;
                op.Exception = exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (SalaryPackageId != 0)
            {
                foreach (var item in lstSalAllowances)
                {
                    try
                    {
                        string InsertQuery = " INSERT INTO HR_SalaryPackageAllowances (SalaryAllowanceId,SalaryPercentage,SalaryPackagesId)" +
                                             " VALUES(@SalaryAllowanceId,@SalaryPercentage,@SalaryPackagesId)";
                        sqlConnection = new SqlConnection(connectionString);
                        sqlConnection.Open();
                        sqlCommand = new SqlCommand(InsertQuery, sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.Text;

                        sqlCommand.Parameters.AddWithValue("@SalaryAllowanceId", item.id);
                        sqlCommand.Parameters.AddWithValue("@SalaryPercentage", item.AmountInPercent);
                        sqlCommand.Parameters.AddWithValue("@SalaryPackagesId", SalaryPackageId);
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        sqlConnection.Close();
                        op.Message = "Success";
                        op.Success = true;
                        op.Exception = null;
                        op.InsertedRowId = SalaryPackageId;

                    }
                    catch (Exception exception)
                    {
                        op.Message = exception.Message;
                        op.Success = false;
                        op.Exception = exception;
                        //throw;
                    }
                    finally
                    {
                        if (sqlReader != null)
                        {
                            sqlReader.Close();
                        }
                        if (sqlConnection != null)
                        {
                            sqlConnection.Close();
                        }

                    }
                }
            }
            return op;
        }


        public OperationDetails DeleteSalaryPackageDetails(int PackageID)
        {
            OperationDetails op = new OperationDetails();


            try
            {
                string InsertQuery = "delete from HR_SalaryPackageAllowances where SalaryPackagesID=@PackageID";
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(InsertQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@PackageID", PackageID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Message = "Success";
                op.Success = true;
                op.Exception = null;

            }
            catch (Exception exception)
            {
                op.Message = exception.Message;
                op.Success = false;
                op.Exception = exception;
                //throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }


            return op;
        }


        public OperationDetails UpdateSalaryPackage(List<SalaryPackageAllowances> lstSalAllowances, SalaryPackageModel packageDetails, int PackageID, int transactionType)
        {
            OperationDetails op = new OperationDetails();
            int Count = 0;
            if (packageDetails.IsEnabled == false || transactionType == 3)
            {
                try
                {
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    sqlCommand = new SqlCommand("select Count(EmployeeId) as EmployeeCount  from HR_EmployeeSalaryGrade Where SalaryPackageId=@SaalryPackageID", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.Text;
                    sqlCommand.Parameters.AddWithValue("@SaalryPackageID", PackageID);

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            Count = Convert.ToInt32(sqlDataReader["EmployeeCount"]);
                        }
                    }
                    sqlConnection.Close();
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }

                }
            }
            op = DeleteSalaryPackageDetails(PackageID);
            int SalaryPackageId = 0;
            if (Count == 0)
            {
                try
                {
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    sqlCommand = new SqlCommand("HR_stp_InsertSalaryPackage", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@aTransactionType", transactionType);
                    sqlCommand.Parameters.AddWithValue("@aSalaryPackageName", packageDetails.SalaryPackgeName == null ? string.Empty : packageDetails.SalaryPackgeName);
                    sqlCommand.Parameters.AddWithValue("@aSalaryPackageId", PackageID);

                    if (packageDetails.IsEnabled == null)
                    {
                        sqlCommand.Parameters.AddWithValue("@aIsEnabled", 1);
                    }
                    else
                    {
                        sqlCommand.Parameters.AddWithValue("@aIsEnabled", packageDetails.IsEnabled);
                    }

                    SqlParameter output = new SqlParameter("@aInsertedSalaryPackageId", SqlDbType.Int);
                    output.Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add(output);

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    sqlConnection.Close();


                    SalaryPackageId = Convert.ToInt32(output.Value);
                    op.Message = "Success";
                    op.Success = true;
                    op.Exception = null;

                }
                catch (Exception exception)
                {
                    op.Message = exception.Message;
                    op.Success = false;
                    op.Exception = exception;
                    //throw;
                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }

                }
                if (transactionType != 3)
                {
                    if (op.Success)
                    {
                        foreach (var item in lstSalAllowances)
                        {
                            try
                            {
                                string InsertQuery = " INSERT INTO HR_SalaryPackageAllowances (SalaryAllowanceId,SalaryPercentage,SalaryPackagesId)" +
                                                     " VALUES(@SalaryAllowanceId,@SalaryPercentage,@SalaryPackagesId)";
                                sqlConnection = new SqlConnection(connectionString);
                                sqlConnection.Open();
                                sqlCommand = new SqlCommand(InsertQuery, sqlConnection);
                                sqlCommand.CommandType = System.Data.CommandType.Text;

                                sqlCommand.Parameters.AddWithValue("@SalaryAllowanceId", item.id);
                                sqlCommand.Parameters.AddWithValue("@SalaryPercentage", item.AmountInPercent);
                                sqlCommand.Parameters.AddWithValue("@SalaryPackagesId", PackageID);
                                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                                sqlConnection.Close();
                                op.Message = "Success";
                                op.Success = true;
                                op.Exception = null;
                                op.InsertedRowId = SalaryPackageId;

                            }
                            catch (Exception exception)
                            {
                                op.Message = exception.Message;
                                op.Success = false;
                                op.Exception = exception;
                                //throw;
                            }
                            finally
                            {
                                if (sqlReader != null)
                                {
                                    sqlReader.Close();
                                }
                                if (sqlConnection != null)
                                {
                                    sqlConnection.Close();
                                }

                            }
                        }
                    }
                }

            }
            else
            {
                op.Message = "warning";
                op.Success = false;
            }

            return op;
        }

        public List<SalaryPackageModel> GetAllSalaryPackagesList()
        {
            List<SalaryPackageModel> salaryPackagesList = null;
            try
            {
                salaryPackagesList = new List<SalaryPackageModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT SalaryPackagesId,SalaryPackageName,IsEnabled FROM HR_SalaryPackages", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    SalaryPackageModel salPackage;
                    while (sqlDataReader.Read())
                    {
                        salPackage = new SalaryPackageModel();

                        salPackage.SalaryPackagesID = Convert.ToInt32(sqlDataReader["SalaryPackagesId"]);
                        salPackage.SalaryPackgeName = sqlDataReader["SalaryPackageName"].ToString();
                        salPackage.IsEnabled = Convert.ToBoolean(sqlDataReader["IsEnabled"]);
                        salaryPackagesList.Add(salPackage);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return salaryPackagesList;
        }

        public List<SalaryPackageAllowances> GetPackageDetailsByPackageId(int? PackageId)
        {
            List<SalaryPackageAllowances> packageDetailsList = null;
            try
            {
                packageDetailsList = new List<SalaryPackageAllowances>();

                string SelectQuery = " SELECT SPA.SalaryAllowanceId,SPA.SalaryPercentage,PSA.PaySalaryAllowanceName_1 FROM HR_SalaryPackageAllowances AS SPA INNER JOIN" +
                                     " GEN_PaySalaryAllowance AS PSA ON PSA.PaySalaryAllowanceID = SPA.SalaryAllowanceId WHERE SPA.SalaryPackagesID =@PackageId";
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(SelectQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@PackageId", PackageId == null ? 0 : PackageId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    SalaryPackageAllowances salPackageDetails;
                    while (sqlDataReader.Read())
                    {
                        salPackageDetails = new SalaryPackageAllowances();

                        salPackageDetails.SalaryAllowanceId = (short)sqlDataReader["SalaryAllowanceId"];
                        salPackageDetails.PaySalaryAllowanceName = sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        salPackageDetails.AmountInPercent = Convert.ToDecimal(sqlDataReader["SalaryPercentage"]);
                        packageDetailsList.Add(salPackageDetails);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return packageDetailsList;
        }



        public SalaryPackageModel GetSalaryPackageDetails(int? PackageId)
        {
            SalaryPackageModel salaryPack = null;
            try
            {
                salaryPack = new SalaryPackageModel();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT SalaryPackagesId,SalaryPackageName,IsEnabled FROM HR_SalaryPackages Where SalaryPackagesId=@PackageID", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@PackageID", PackageId == null ? 0 : PackageId);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {


                        salaryPack.SalaryPackagesID = Convert.ToInt32(sqlDataReader["SalaryPackagesId"]);
                        salaryPack.SalaryPackgeName = sqlDataReader["SalaryPackageName"].ToString();
                        salaryPack.IsEnabled = Convert.ToBoolean(sqlDataReader["IsEnabled"]);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            if (salaryPack.SalaryPackgeName != "")
            {
                salaryPack.lstPackAllowances = GetPackageDetailsByPackageId(PackageId);
            }

            return salaryPack;
        }

        public List<SalaryPackageAllowances> GetEmpPackageDetailsByEmployeeId(int EmployeeID)
        {
            List<SalaryPackageAllowances> packageDetailsList = null;
            try
            {
                packageDetailsList = new List<SalaryPackageAllowances>();

                //string SelectQuery = "SELECT PaySalaryAllowanceName_1, SPA.SalaryPercentage,PS.Amount FROM HR_SalaryPackageAllowances AS SPA "+
                //                     " INNER JOIN GEN_PaySalaryAllowance AS PSA ON SPA.SalaryAllowanceId = PSA.PaySalaryAllowanceID"+
                //                     " INNER JOIN HR_PaySalary AS PS ON PS.PaySalaryAllowanceID = PSA.PaySalaryAllowanceID WHERE PS.EmployeeID ="+EmployeeID;

                string SelectQuery = "SELECT PaySalaryAllowanceName_1, SPA.SalaryPercentage,PS.Amount FROM HR_SalaryPackageAllowances AS SPA" +
                               " INNER JOIN GEN_PaySalaryAllowance AS PSA ON SPA.SalaryAllowanceId = PSA.PaySalaryAllowanceID INNER JOIN HR_PaySalary AS PS" +
                               " ON PS.PaySalaryAllowanceID = PSA.PaySalaryAllowanceID  INNER JOIN HR_EmployeeSalaryGrade AS ESG ON ESG.SalaryPackageId =" +
                               " SPA.SalaryPackagesID AND ESG.EmployeeId = " + EmployeeID + "  WHERE PS.EmployeeID =" + EmployeeID;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(SelectQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    SalaryPackageAllowances salPackageDetails;
                    while (sqlDataReader.Read())
                    {
                        salPackageDetails = new SalaryPackageAllowances();
                        salPackageDetails.PaySalaryAllowanceName = sqlDataReader["PaySalaryAllowanceName_1"].ToString();
                        salPackageDetails.AmountInPercent = Convert.ToDecimal(sqlDataReader["SalaryPercentage"]);
                        salPackageDetails.ActualAmount = Convert.ToDecimal(sqlDataReader["Amount"]);
                        salPackageDetails.ActualAmount = Math.Round(salPackageDetails.ActualAmount, 2, MidpointRounding.ToEven);
                        packageDetailsList.Add(salPackageDetails);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return packageDetailsList;
        }

        public bool isToInsertSalaryPackageDetails(int EmployeeId, int? SalaryPackageId, decimal TotalSalary)
        {
            bool isDeleted = false;
            int? aSalaryPackId = 0;
            decimal aTotalSalary = 0;
            if (SalaryPackageId != 0)
            {
                try
                {
                    string SelectQuery = "Select SalaryPackageId,TotalSalary from [dbo].[HR_EmployeeSalaryGrade] WHERE EmployeeId=" + EmployeeId;
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    sqlCommand = new SqlCommand(SelectQuery, sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.Text;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    if (sqlDataReader.HasRows)
                    {

                        while (sqlDataReader.Read())
                        {

                            aSalaryPackId = sqlDataReader["SalaryPackageId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["SalaryPackageId"]);
                            aTotalSalary = Convert.ToDecimal(sqlDataReader["TotalSalary"]);


                        }
                    }
                    sqlConnection.Close();
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }

                }
                if (aTotalSalary != TotalSalary || aSalaryPackId != SalaryPackageId)
                {
                    try
                    {
                        string DeleteQuery = "DELETE FROM HR_PAySalary WHERE EmployeeId=" + EmployeeId;
                        sqlConnection = new SqlConnection(connectionString);
                        sqlConnection.Open();
                        sqlCommand = new SqlCommand(DeleteQuery, sqlConnection);
                        sqlCommand.CommandType = System.Data.CommandType.Text;

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        int NoOfRows = sqlDataReader.RecordsAffected;
                        // if (NoOfRows > 0)
                        //  {
                        isDeleted = true;
                        // }
                        sqlConnection.Close();
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    finally
                    {
                        if (sqlReader != null)
                        {
                            sqlReader.Close();
                        }
                        if (sqlConnection != null)
                        {
                            sqlConnection.Close();
                        }

                    }
                }
            }

            return isDeleted;

        }

        public bool isSalaryGradeInfoEnabled()
        {
            bool isEnabled = false;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT SalaryGradingSettingID,SalaryGradingTabName,IsEnabled  FROM HR_SalaryGradingSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {

                        isEnabled = Convert.ToBoolean(sqlDataReader["IsEnabled"]);

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return isEnabled;

        }

        public OperationDetails SavePayrollConfirmationRequestForPaySalary(PaySalaryModel paySalaryModel, int RequestEmployeeId, int Mode)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_PayrollRequestForPaySalary", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PaySalaryId", paySalaryModel.PaySalaryID);
                sqlCommand.Parameters.AddWithValue("@PaySalaryAllowanceId", paySalaryModel.PaySalaryAllowanceID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", paySalaryModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@Amount", paySalaryModel.Amount);
                sqlCommand.Parameters.AddWithValue("@IsActive", paySalaryModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@Addition", paySalaryModel.Addition);
                sqlCommand.Parameters.AddWithValue("@Note", paySalaryModel.Note);
                sqlCommand.Parameters.AddWithValue("@TrDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(paySalaryModel.TrDate));
                sqlCommand.Parameters.AddWithValue("@Currency", paySalaryModel.Currency);
                sqlCommand.Parameters.AddWithValue("@IsFixed", paySalaryModel.IsFixed);
                sqlCommand.Parameters.AddWithValue("@Amount1", paySalaryModel.Amount1);
                sqlCommand.Parameters.AddWithValue("@RequestEmployeeId", RequestEmployeeId);
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                if (paySalaryModel.PaySalaryID == 1)
                {
                    return new OperationDetails(true, "Salary details saved successfully.", null, id);
                }
                else
                {
                    return new OperationDetails(true, "Salary details deleted successfully.", null, id);
                }
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while saving salary details.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public int GetCountOfPaySalaryByCycleId(string CycleIds)
        {
            using (sqlConnection = new SqlConnection(connectionString))
            {
                using (sqlCommand = new SqlCommand())
                {
                    try
                    {
                        int count = 0;
                        sqlConnection.Open();
                        sqlCommand.Connection = sqlConnection; 
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = "SELECT COUNT(DISTINCT EmployeeId) Count FROM Hr_PayActiveSalary WHERE Cycle IN (" + CycleIds + ")";
                        sqlReader = sqlCommand.ExecuteReader();
                        if (sqlReader.HasRows)
                        {
                            while (sqlReader.Read())
                            {
                                count = Convert.ToInt32(sqlReader["Count"]);
                            }
                        }
                        return count;
                    }
                    catch
                    {
                        return 0;
                    }
                    finally
                    {
                        if (sqlReader != null)
                        {
                            sqlReader.Close();
                        }
                        if (sqlConnection != null)
                        {
                            sqlConnection.Close();
                        }
                    }
                }
            }

        }

    }
}
