﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class LateDeductionDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<LateDeductionModel> GetLateDeductionList(LateDeductionModel objLateDeduction)
        {
            List<LateDeductionModel> lateDeductionList = null;
            try
            {
                lateDeductionList = new List<LateDeductionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetLateDeductionOfEmployees", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aDttFromdate", CommonDB.SetCulturedDate(objLateDeduction.FromDate));
                sqlCommand.Parameters.AddWithValue("@aDttTodate", CommonDB.SetCulturedDate(objLateDeduction.ToDate));
                sqlCommand.Parameters.AddWithValue("@aIntUserID", objLateDeduction.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aBitShoAllRecords", objLateDeduction.MinimumSetup);
                sqlCommand.Parameters.AddWithValue("@aBitIsConfirmed", objLateDeduction.IsConfirmed);
                sqlCommand.Parameters.AddWithValue("@IsDeducted", objLateDeduction.isDeducted);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LateDeductionModel lateDeductionModel;
                    while (sqlDataReader.Read())
                    {
                        lateDeductionModel = new LateDeductionModel();
                        lateDeductionModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        lateDeductionModel.EmployeeAlterNativeId = sqlDataReader["EmployeeAlternativeID"].ToString();
                        lateDeductionModel.Amount = Convert.ToDouble(sqlDataReader["Amount"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        lateDeductionModel.LateMinutes = sqlDataReader["LateMinutes"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["LateMinutes"]);
                        lateDeductionModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        lateDeductionModel.DeductedMinutes = sqlDataReader["DeductedMinutes"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeductedMinutes"]);
                        lateDeductionModel.AllMinutes = sqlDataReader["AllMinutes"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AllMinutes"]);
                        lateDeductionModel.LateMinutesToDed = sqlDataReader["LateMinutesToDed"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["LateMinutesToDed"]);
                        lateDeductionModel.IsConfirmed = Convert.ToBoolean(sqlDataReader["IsConfirmed"]);
                        lateDeductionModel.IsGenerated = Convert.ToBoolean(sqlDataReader["IsGenerated"]);
                        lateDeductionModel.GenerateDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["GenerateDate"].ToString());
                        lateDeductionModel.EffectiveDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EffectiveDate"].ToString());
                        lateDeductionList.Add(lateDeductionModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lateDeductionList;
        }

        public List<LateDeductionModel> GetDeductableEmployeeList(LateDeductionModel objLateDeduction,int? userId)
        {
            List<LateDeductionModel> lateDeductionList = null;
            try
            {
                bool isDeductionRulesEnable = new DeductionDB().GetDeductionRuleSetting().AppliedDeductionRules;
                lateDeductionList = new List<LateDeductionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (isDeductionRulesEnable)
                {
                    if (objLateDeduction.DeductionType == 1)
                    {
                        sqlCommand = new SqlCommand("HR_Stp_GetLateDeductionWithAplliedRules", sqlConnection);
                    }
                    else
                    {
                        sqlCommand = new SqlCommand("HR_Stp_GetEarlyDeductionWithAplliedRules", sqlConnection);
                    }
                    
                }
                else
                {
                    sqlCommand = new SqlCommand("HR_uspGetDeductableEmployeeList", sqlConnection);
                }

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                DateTime dt1 = DateTime.ParseExact(objLateDeduction.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dt2 = DateTime.ParseExact(objLateDeduction.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@aDttFromdate", dt1);
                sqlCommand.Parameters.AddWithValue("@aDttTodate", dt2);
                sqlCommand.Parameters.AddWithValue("@aDeductionType", objLateDeduction.DeductionType);
                sqlCommand.Parameters.AddWithValue("@aIsExcused", objLateDeduction.excused);
                sqlCommand.Parameters.AddWithValue("@UserId",userId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LateDeductionModel lateDeductionModel;
                    while (sqlDataReader.Read())
                    {
                        lateDeductionModel = new LateDeductionModel();
                        lateDeductionModel.EmployeeAlterNativeId = sqlDataReader["EmployeeAlternativeID"].ToString();
                        lateDeductionModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        lateDeductionModel.EmployeeName = Convert.ToString(sqlDataReader["employee_name"]);
                        lateDeductionModel.AttendanceDate = Convert.ToDateTime(sqlDataReader["Attendancedate"].ToString()).ToString("dd/MM/yyyy"); ;
                        lateDeductionModel.Time = sqlDataReader["Time"].ToString();
                        lateDeductionModel.DeductableMinutes = Convert.ToInt32(sqlDataReader["DeductableMinutes"].ToString());
                        lateDeductionModel.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString()==""?"0": sqlDataReader["LateMinutes"].ToString());
                        lateDeductionModel.Amount = Convert.ToDouble(sqlDataReader["Amount"].ToString());
                        lateDeductionModel.excused = Convert.ToBoolean(sqlDataReader["excused"] == DBNull.Value ? false : sqlDataReader["excused"]);
                        lateDeductionModel.deduct = Convert.ToBoolean(sqlDataReader["deduct"] == DBNull.Value ? false : sqlDataReader["deduct"]);
                        lateDeductionModel.PayEmployeeLateID = Convert.ToInt32(sqlDataReader["PayEmployeeLateID"].ToString());
                        if (isDeductionRulesEnable)
                        {
                            lateDeductionModel.DelayToOtherWorkers = Convert.ToBoolean(sqlDataReader["DelayToOtherWorkers"] == DBNull.Value ? false : sqlDataReader["DelayToOtherWorkers"]);
                            lateDeductionModel.CountLateInCycle = Convert.ToInt32(sqlDataReader["CountLateInCycle"].ToString() == "" ? "0" : sqlDataReader["CountLateInCycle"].ToString());
                            lateDeductionModel.CycleID = Convert.ToInt32(sqlDataReader["CycleID"].ToString() == "" ? "0" : sqlDataReader["CycleID"].ToString());
                            lateDeductionModel.DeductionPercent = Convert.ToDouble(sqlDataReader["PercentageDeduction"].ToString() == "" ? "0" : sqlDataReader["PercentageDeduction"].ToString());
                        }
                        lateDeductionModel.LateReason = sqlDataReader["LateReason"].ToString();
                        lateDeductionList.Add(lateDeductionModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lateDeductionList;
        }

        public double GetDeductableEmployeeWithCauseDelay(int PayEmployeeLateId,int EmployeeID, int LateMinutes, int CycleID,bool isDelayCaused,int CountLateInCycle)
        {
            double amount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAmountForCauseDelay", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayEmployeeLateId", PayEmployeeLateId); 
                sqlCommand.Parameters.AddWithValue("@EmployeeID",EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CycleID", CycleID);
                sqlCommand.Parameters.AddWithValue("@LateMinutes", LateMinutes);
                sqlCommand.Parameters.AddWithValue("@isDelayCaused", isDelayCaused);
                sqlCommand.Parameters.AddWithValue("@CountLateInCycle",CountLateInCycle); 
                SqlParameter Amount = new SqlParameter("@Amount", SqlDbType.Float);
                Amount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Amount);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                amount =Convert.ToDouble(Amount.Value);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return amount;
        }

        public int GetStatusCount(string EmployeeLateIds, int deductionmode)
        {
            int count = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                if (deductionmode == 1)
                {
                    sqlCommand = new SqlCommand(" select Count (*) as count from HR_PayEmployeeLate where islateexcused=1and PayEmployeeLateID in   (" + EmployeeLateIds + ") ", sqlConnection);
                }
                else
                {
                    sqlCommand = new SqlCommand(" select Count (*) as count from HR_PayEmployeeLate where isearlyexcused=1  and PayEmployeeLateID in  (" + EmployeeLateIds + ") ", sqlConnection);
                }
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        count = Convert.ToInt32(sqlDataReader["count"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return count;
        }

        public OperationDetails SetEmployeeLateDeductedStatus(LateDeductionModel lateDeductionModel)
        {
            string Message = "";
            int rowCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspSetEmployeeLateDeductedStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", lateDeductionModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aDttFromdate", CommonDB.SetCulturedDate(lateDeductionModel.FromDate));
                sqlCommand.Parameters.AddWithValue("@aDttTodate", CommonDB.SetCulturedDate(lateDeductionModel.ToDate));
                sqlCommand.Parameters.AddWithValue("@aEffectiveDate", CommonDB.SetCulturedDate(lateDeductionModel.EffectiveDate));


                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                rowCount = Convert.ToInt32(OperationId.Value);
                Message = "success";

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, rowCount);
        }


        public OperationDetails ConfirmEmployeeForLateDeductedStatus(LateDeductionModel lateDeductionModel)
        {
            string Message = "";
            int rowCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspConfirmEmployeeForLateDeductedStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", lateDeductionModel.EmployeeID);
                //sqlCommand.Parameters.AddWithValue("@aDttFromdate", (DateTime.ParseExact(lateDeductionModel.FromDate, format, null)).ToString(sqlDateFormat));
                //sqlCommand.Parameters.AddWithValue("@aDttTodate", (DateTime.ParseExact(lateDeductionModel.ToDate, format, null)).ToString(sqlDateFormat));
                sqlCommand.Parameters.AddWithValue("@aDttFromdate", CommonDB.SetCulturedDate(lateDeductionModel.FromDate));
                sqlCommand.Parameters.AddWithValue("@aDttTodate", CommonDB.SetCulturedDate(lateDeductionModel.ToDate));
                //sqlCommand.Parameters.AddWithValue("@aEffectiveDate", lateDeductionModel.EffectiveDate);

                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                rowCount = Convert.ToInt32(OperationId.Value);
                Message = "success";

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, rowCount);
        }
        
        public OperationDetails UpdateDeuctedStatus(int payEmployeeLateID, bool deductionStatus, int deductionType, bool isExcused)
        {
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            if (deductionType == 1)
            {
                UpdateQuery = "Update HR_PayEmployeeLate set isLateDeduct=@aIsDeduct,isLateExcused=@aIsExcused where PayEmployeelateID=" + payEmployeeLateID;
            }
            else
            {
                UpdateQuery = "Update HR_PayEmployeeLate set isEarlyDeduct=@aIsDeduct,isEarlyExcused=@aIsExcused where PayEmployeelateID=" + payEmployeeLateID;
            }
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@aIsDeduct", deductionStatus);
                sqlCommand.Parameters.AddWithValue("@aIsExcused", isExcused);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.CssClass = "success";
                op.Message = "Excused staus updated successfully";
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while updating excused status!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public OperationDetails UpdateExcusedStatus(int payEmployeeLateID, bool excusedStatus, int deductionType, bool deductedStatus)
        {
            string Message = "";
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            if (deductionType == 1)
            {
                UpdateQuery = "Update HR_PayEmployeeLate set isLateExcused=@aIsExcused,isLateDeduct=@aIsLateDeduct where PayEmployeelateID=" + payEmployeeLateID;
            }
            else
            {
                UpdateQuery = "Update HR_PayEmployeeLate set isEarlyExcused=@aIsExcused,isEarlyDeduct=@aIsLateDeduct where PayEmployeelateID=" + payEmployeeLateID;
            }
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@aIsExcused", excusedStatus);
                sqlCommand.Parameters.AddWithValue("@aIsLateDeduct", deductedStatus);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.CssClass = "success";
                op.Message = "Excused staus updated successfully";
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while updating excused status!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public OperationDetails UpdateExcusedStatusForAllEmployee(string payEmployeeLateID, bool excusedStatus, int deductionType, bool deductedStatus)
        {
            string Message = "";
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            if (deductionType == 1)
            {
                UpdateQuery = "Update HR_PayEmployeeLate set isLateExcused=@aIsExcused,isLateDeduct=@aIsDeduct where PayEmployeelateID in (" + payEmployeeLateID + ")";
            }
            else
            {
                UpdateQuery = "Update HR_PayEmployeeLate set isEarlyExcused=@aIsExcused,isEarlyDeduct=@aIsDeduct where PayEmployeelateID in (" + payEmployeeLateID + ")";
            }
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@aIsExcused", excusedStatus);
                sqlCommand.Parameters.AddWithValue("@aIsDeduct", deductedStatus);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.CssClass = "success";
                op.Message = "Excused staus updated successfully";
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while updating excused status!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public OperationDetails ConfirmDeductionStatus(string payEmployeeLateIDs, string GenerateDate, int DeductionType)
        {
            string Message = "";
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            if (DeductionType == 1)
            {
                UpdateQuery = "update HR_PayEmployeeLate set IsConfirmedLateIn=1,GenerateDateLateIn=@Genearatedate,ConfirmedDateLateIn=GETDATE() where PayEmployeeLateID in (" + payEmployeeLateIDs + ")";
            }
            else
            {
                UpdateQuery = "update HR_PayEmployeeLate set IsConfirmedEarlyOut=2,GenerateDateEarlyOut=@Genearatedate,ConfirmedDateEarlyOut=GETDATE() where PayEmployeeLateID in (" + payEmployeeLateIDs + ")";
            }
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                DateTime dt1 = DateTime.ParseExact(GenerateDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@Genearatedate", dt1);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.CssClass = "success";
                op.Message = "Late deduction status confirmed successfully";
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while confirming late deduction!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public OperationDetails AddMultipleDeduction(int deductionType, string effectiveDatetxt, string refNotxt, int cycleId, List<multipleDeduction> multipleDeductionDetails)
        {
            OperationDetails oDetails = new OperationDetails();

            int rowCount = 1;
            try
            {
                DataTable multipleDeduction = new DataTable();
                multipleDeduction.Columns.Add("RowId", typeof(int));
                multipleDeduction.Columns.Add("DeductionType", typeof(int));
                multipleDeduction.Columns.Add("EffectiveDate", typeof(DateTime));
                multipleDeduction.Columns.Add("paidCycle", typeof(int));
                multipleDeduction.Columns.Add("isInstallment", typeof(bool));
                multipleDeduction.Columns.Add("Amount", typeof(decimal));
                multipleDeduction.Columns.Add("installMent", typeof(decimal));
                multipleDeduction.Columns.Add("comments", typeof(string));
                multipleDeduction.Columns.Add("EmployeeId", typeof(int));
                multipleDeduction.Columns.Add("RefNumber", typeof(string));
                multipleDeduction.Columns.Add("CycleId", typeof(int));
                multipleDeduction.Columns.Add("PayEmployeeLateID", typeof(int));

                DataTable multipleDeductionDetail = new DataTable();
                multipleDeductionDetail.Columns.Add("PayDeductionDetailId", typeof(int));
                multipleDeductionDetail.Columns.Add("PayDeductionId", typeof(int));
                multipleDeductionDetail.Columns.Add("DeductionDate", typeof(DateTime));
                multipleDeductionDetail.Columns.Add("Amount", typeof(decimal));
                multipleDeductionDetail.Columns.Add("Comments", typeof(string));
                multipleDeductionDetail.Columns.Add("Active", typeof(bool));
                multipleDeductionDetail.Columns.Add("employeeId", typeof(int));
                multipleDeductionDetail.Columns.Add("IsProcessed", typeof(bool));
                multipleDeductionDetail.Columns.Add("IsWaived", typeof(int));
                multipleDeductionDetail.Columns.Add("rowNum", typeof(int));
                foreach (var item in multipleDeductionDetails)
                {
                    multipleDeduction.Rows.Add(
                        rowCount,
                        deductionType,
                        DataAccess.GeneralDB.CommonDB.SetCulturedDate(effectiveDatetxt),
                        item.cycle,
                        item.cycle > 1 ? true : false,
                        item.amount * item.cycle,
                        item.amount,
                        item.comments,
                        item.employeeId,
                        item.comments,
                        cycleId,
                        item.PayEmployeeLateId
                        );
                    for (int i = 1; i <= item.cycle; i++)
                    {
                        multipleDeductionDetail.Rows.Add(
                            0,
                            0,
                            DataAccess.GeneralDB.CommonDB.SetCulturedDate(effectiveDatetxt),
                            item.amount,
                            item.comments,
                            true,
                            item.employeeId,
                            false,
                            false,
                            rowCount
                            );
                    }
                    rowCount++;
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddMultipleLateDeduction", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MultipleDeduction", multipleDeduction);
                sqlCommand.Parameters.AddWithValue("@MultipleDeductionDetails", multipleDeductionDetail);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                oDetails.CssClass = "success";
                oDetails.Message = "Excused staus updated successfully";
                sqlConnection.Close();

                oDetails.Success = true;
                oDetails.Message = "Record save successfully";
                oDetails.CssClass = "success";
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public int GetCycleFromDate(string GenerateDate)
        {
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            UpdateQuery = "select   dbo.Hr_Fn_GetCycleDetails(@Genearatedate,'0') As PayCycleID";

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                DateTime dt1 = DateTime.ParseExact(GenerateDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@Genearatedate", dt1);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        id = Convert.ToInt32(sqlDataReader["PayCycleID"] == DBNull.Value ? 0 : sqlDataReader["PayCycleID"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while confirming late deduction!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return id;
        }

        public int GetDeductionTypeIdForLate()
        {
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            UpdateQuery = "select top 1 LateDeductionTypeID from HR_LateDeductionSettings";

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        id = Convert.ToInt32(sqlDataReader["LateDeductionTypeID"] == DBNull.Value ? 0 : sqlDataReader["LateDeductionTypeID"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while confirming late deduction!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return id;
        }

        public int GetDeductionTypeIdForEarly()
        {
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            UpdateQuery = "select top 1 EarlyDeductionTypeID from HR_LateDeductionSettings";

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        id = Convert.ToInt32(sqlDataReader["EarlyDeductionTypeID"] == DBNull.Value ? 0 : sqlDataReader["EarlyDeductionTypeID"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.CssClass = "error";
                op.Message = "Error occurred while confirming late deduction!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return id;
        }

        public LateDeductionSetting GetLateDeductionSetting()
        {
            LateDeductionSetting lateDeductionSetting = null;
            try
            {
                lateDeductionSetting = new LateDeductionSetting();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLateDeductionSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        lateDeductionSetting = new LateDeductionSetting();
                        lateDeductionSetting.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        lateDeductionSetting.DeductionPercent = Convert.ToDecimal(sqlDataReader["DeductionPercent"].ToString());
                        lateDeductionSetting.AbsentTypeID = Convert.ToInt32(sqlDataReader["AbsentTypeID"].ToString());                        
                        lateDeductionSetting.MinutesPerMonth = Convert.ToInt32(sqlDataReader["MinutesPerMonth"].ToString());
                        lateDeductionSetting.DigitAfterDecimalForLate = Convert.ToInt32(sqlDataReader["DigitAfterDecimalForLate"].ToString()==""?"0": sqlDataReader["DigitAfterDecimalForLate"].ToString());
                       
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lateDeductionSetting;
        }

        public int GetNullcycleCountBetweenDates(string StartDate,string EndDate)
        {
            int Count = 0;
            string Query = "Hr_stp_GetNumberOfNullCycleRecords";            
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                DateTime dt1 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dt2 = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@StartDate", dt1);
                sqlCommand.Parameters.AddWithValue("@EndDate", dt2);
                SqlParameter output = new SqlParameter("@NullCycleCount", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Count = Convert.ToInt32(output.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Count;
        }

        public string GetNullAcYearsBetweenDates(string StartDate, string EndDate,int deductionType)
        {
            string AcRecords = string.Empty;

            string Query = "Hr_stp_GetNullAcademicYearsinDeduction";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(Query, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                
                DateTime dt1 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dt2 = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                sqlCommand.Parameters.AddWithValue("@StartDate", dt1);
                sqlCommand.Parameters.AddWithValue("@EndDate", dt2);
                if (deductionType == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@Mode", 2);
                }
                else {
                    sqlCommand.Parameters.AddWithValue("@Mode", 3);
                }
                SqlParameter output = new SqlParameter("@NullAcademicRecords", SqlDbType.NVarChar);
                output.Size = 400;
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                AcRecords = Convert.ToString(output.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return AcRecords;
        }

        public OperationDetails UpdateLateReason(int payEmployeeLateID, string LateReason)
        {
            string Message = "";
            int id = 0;
            string UpdateQuery = "";
            OperationDetails op = new OperationDetails();
            UpdateQuery = "update HR_PayEmployeeLate set LateREason=@LateReason where PayEmployeeLateID ="+ payEmployeeLateID;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(UpdateQuery, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;                
                sqlCommand.Parameters.AddWithValue("@LateReason", LateReason);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.CssClass = "success";
                op.Message = "Late reason updated successfully";
                op.Success = true;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.CssClass = "error";
                op.Message = "Error occurred while updating late reason!";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public LateDeductionModel GetLateDeductionByPayLateID(int payEmployeeLateID)
        {
            LateDeductionModel objLateDeduction = new LateDeductionModel();
            try
            {
                
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeLateDetailsById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayEmployeeLateTypeId", payEmployeeLateID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objLateDeduction = new LateDeductionModel();
                        objLateDeduction.PayEmployeeLateID = Convert.ToInt32(sqlDataReader["PayEmployeeLateID"].ToString());
                        objLateDeduction.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        objLateDeduction.AttendanceDate = sqlDataReader["PayEmployeeLateDate"].ToString() == "" ? "" : Convert.ToDateTime(sqlDataReader["PayEmployeeLateDate"]).ToString("dd/MM/yyyy");
                        objLateDeduction.excused = Convert.ToBoolean(sqlDataReader["IsExcused"].ToString());
                        objLateDeduction.LateReason = sqlDataReader["LateReason"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objLateDeduction;
        }
    }
}
