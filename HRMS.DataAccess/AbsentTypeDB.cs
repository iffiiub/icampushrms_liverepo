﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class AbsentTypeDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        public DataAccess.GeneralDB.DataHelper dataHelper;
        public List<AbsentTypeModel> GetAbsentTypeList()
        {
            // Create a list to hold the AbsentType		
            List<AbsentTypeModel> AbsentTypeModelList = new List<AbsentTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayAbsentType"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        AbsentTypeModel AbsentTypeModel;
                        while (reader.Read())
                        {
                            AbsentTypeModel = new AbsentTypeModel();
                            AbsentTypeModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
                            AbsentTypeModel.PayAbsentTypeName_1 = reader["PayAbsentTypeName_1"].ToString();
                            AbsentTypeModel.PayAbsentTypeName_2 = reader["PayAbsentTypeName_2"].ToString();
                            AbsentTypeModel.PayAbsentTypeName_3 = reader["PayAbsentTypeName_3"].ToString();
                            AbsentTypeModel.ISDeductable = Convert.ToBoolean(reader["ISDeductable"].ToString() == "" ? "false" : reader["ISDeductable"].ToString());
                            AbsentTypeModel.PaidType = Convert.ToInt32(reader["AbsentPaidTypeId"].ToString());
                            AbsentTypeModel.AnnualLeave = Convert.ToBoolean(reader["AnnualLeave"].ToString() == "" ? "false" : reader["AnnualLeave"].ToString());
                            AbsentTypeModel.PaidTypeName = reader["AbsentPaidTypeName"].ToString();
                            AbsentTypeModel.DeductAdjacentOffdays = Convert.ToBoolean(reader["AdjacentOffdays"].ToString() == "" ? "false" : reader["AdjacentOffdays"].ToString());
                            AbsentTypeModel.DeductEnClosedOffdays = Convert.ToBoolean(reader["EnclosedOffdays"].ToString() == "" ? "false" : reader["EnclosedOffdays"].ToString());
                            AbsentTypeModel.DeductAdjAcentHoliday = Convert.ToBoolean(reader["AdjacentHolidays"].ToString() == "" ? "false" : reader["AdjacentHolidays"].ToString());
                            AbsentTypeModelList.Add(AbsentTypeModel);
                        }
                    }
                }
                return AbsentTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentTypeModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public AbsentTypeModel GetAbsentType(int id)
        {
            // Create a list to hold the AbsentType		
            AbsentTypeModel AbsentType = new AbsentTypeModel(); ;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntPayAbsentTypeID",id)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayAbsentTypeByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,

                        while (reader.Read())
                        {
                            AbsentType.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
                            AbsentType.PayAbsentTypeName_1 = reader["PayAbsentTypeName_1"].ToString();
                            AbsentType.PayAbsentTypeName_2 = reader["PayAbsentTypeName_2"].ToString();
                            AbsentType.PayAbsentTypeName_3 = reader["PayAbsentTypeName_3"].ToString();
                            AbsentType.ISDeductable = Convert.ToBoolean(reader["ISDeductable"].ToString() == "" ? "false" : reader["ISDeductable"].ToString());
                            AbsentType.PaidType = Convert.ToInt32(reader["AbsentPaidTypeId"].ToString());
                            AbsentType.PaidTypeName = reader["AbsentPaidTypeName"].ToString();
                            AbsentType.AnnualLeave = Convert.ToBoolean(reader["AnnualLeave"].ToString() == "" ? "false" : reader["AnnualLeave"].ToString());
                            AbsentType.DeductAdjacentOffdays = Convert.ToBoolean(reader["AdjacentOffdays"].ToString() == "" ? "false" : reader["AdjacentOffdays"].ToString());
                            AbsentType.DeductEnClosedOffdays = Convert.ToBoolean(reader["EnclosedOffdays"].ToString() == "" ? "false" : reader["EnclosedOffdays"].ToString());
                            AbsentType.DeductAdjAcentHoliday = Convert.ToBoolean(reader["AdjacentHolidays"].ToString() == "" ? "false" : reader["AdjacentHolidays"].ToString());
                        }
                    }
                }
                return AbsentType;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentTypeModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddEditAbsentType(AbsentTypeModel objAbsentType,int tranMode)
        {
            OperationDetails op = new OperationDetails();
            if (objAbsentType.ISDeductable==false)
            {
                objAbsentType.DeductAdjAcentHoliday = false;
                objAbsentType.DeductAdjacentOffdays = false;
                objAbsentType.DeductEnClosedOffdays = false;
            }

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayAbsentTypeCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values                
                sqlCommand.Parameters.AddWithValue("@aTntTranMode",tranMode) ;
                sqlCommand.Parameters.AddWithValue("@PayAbsentTypeID", objAbsentType.PayAbsentTypeID);
                sqlCommand.Parameters.AddWithValue("@PayAbsentTypeName_1",objAbsentType.PayAbsentTypeName_1);
                sqlCommand.Parameters.AddWithValue("@PayAbsentTypeName_2", objAbsentType.PayAbsentTypeName_2);
                sqlCommand.Parameters.AddWithValue("@PayAbsentTypeName_3", objAbsentType.PayAbsentTypeName_3);
                sqlCommand.Parameters.AddWithValue("@IsDeductible", objAbsentType.ISDeductable);
                sqlCommand.Parameters.AddWithValue("@LeaveTypeID", objAbsentType.PaidType);
                sqlCommand.Parameters.AddWithValue("@AnnualLeave", objAbsentType.AnnualLeave);
                sqlCommand.Parameters.AddWithValue("@DeductAdjacentOffdays", objAbsentType.DeductAdjacentOffdays);
                sqlCommand.Parameters.AddWithValue("@DeductEnClosedOffdays", objAbsentType.DeductEnClosedOffdays);
                sqlCommand.Parameters.AddWithValue("@DeductAdjacentHoliday", objAbsentType.DeductAdjAcentHoliday);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    if (tranMode == 1)
                    {
                        op.Message = "Absent type saved successfully.";
                    }
                    else if (tranMode == 2)
                    {
                        op.Message = "Absent type updated successfully.";
                    }
                    else {
                        op.Message = "Absent type deleted successfully.";
                    }
                    
                    op.CssClass = "success";
                }
                else
                {
                    if (tranMode == 1)
                    {
                        op.Message = "Error occured while saving absent type";
                    }
                    else if (tranMode == 2)
                    {
                        op.Message = "Error occurred while updating absent type";
                    }
                    else
                    {
                        op.Message = "Error occurred while deleting absent type";
                    }
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while performing operation on absent type";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }
    }
}
