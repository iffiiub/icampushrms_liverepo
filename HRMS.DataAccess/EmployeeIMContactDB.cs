﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class EmployeeIMContactDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public OperationDetails InsertEmployeeIMContact(EmployeeIMContactModel EmployeeIMContactModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@EmployeeId", EmployeeIMContactModel.EmployeeId) ,
                       new SqlParameter("@IMContact", EmployeeIMContactModel.IMContact) ,
                      new SqlParameter("@IMContactType", EmployeeIMContactModel .IMContactType) 
                      
                    };
                int MaritialStausId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_HR_EmployeeIMContactModel", parameters));
                return new OperationDetails(true, "Employee IM Contact inserted successfully.", null, MaritialStausId);
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while inserting MaritialStaus.", exception);
                throw;
            }
            finally
            {

            }
        }

    }
}
