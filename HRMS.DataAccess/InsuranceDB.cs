﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class InsuranceDB : DBHelper
    {
        DataAccess.GeneralDB.DataHelper dataHelper;
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        //public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        public OperationDetails InsertInsurance(InsuranceModel InsuranceModel)
        {
            try
            {
                int aTntTranMode = 0;
                if (InsuranceModel.InsuranceID != null && InsuranceModel.InsuranceID != 0)
                {
                    aTntTranMode = 2;
                }
                else
                {
                    aTntTranMode = 1;

                }

                SqlParameter[] parameters =
                    {

                    new SqlParameter("@aTntTranMode",aTntTranMode) ,
                    new SqlParameter("@aIntInsuranceID ",InsuranceModel.InsuranceID) ,
                    new SqlParameter("@aNvrPolicyNumber", InsuranceModel.PolicyNumber) ,
                    new SqlParameter("@aNvrCardNumber", InsuranceModel.CardNumber) ,
                    new SqlParameter("@aDttIssueDate", CommonDB.SetCulturedDate(InsuranceModel.IssueDate)),
                    new SqlParameter("@aDttExpiryDate ", CommonDB.SetCulturedDate(InsuranceModel.ExpiryDate)),
                    new SqlParameter("@aNvrDeduction",InsuranceModel.Deduction ) ,
                    new SqlParameter("@aSntInsuranceType",InsuranceModel.InsuranceType) ,
                    new SqlParameter("@InsuranceEligibility",InsuranceModel.InsuranceEligibility) ,
                    new SqlParameter("@CatAmountID",InsuranceModel.CatAmountID) ,
                    new SqlParameter("@aDttCancellationDate", CommonDB.SetCulturedDate(InsuranceModel.CancellationDate) ),
                    new SqlParameter("@aBitReturned",InsuranceModel.Returned),
                    new SqlParameter("@aSntTeacherID",InsuranceModel.EmployeeID),
                    new SqlParameter("@aNumTotalAmount",InsuranceModel.TotalAmount ) ,
                    new SqlParameter("@aIntAddDeduction",InsuranceModel.AddDeduction ) ,
                    new SqlParameter("@aIntInsuredBySchool", InsuranceModel.InsuredBySchool ),
                    new SqlParameter("@aNumTotalAmountBySchool",InsuranceModel.TotalAmountBySchool),
                    new SqlParameter("@aNvrNote",InsuranceModel.Note),
                    new SqlParameter("@aBitDeduct",InsuranceModel.Deduct),
                    new SqlParameter("@output", 0)

                    };
                parameters[18].Direction = ParameterDirection.Output;
                Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                   "HR_InsuranceCUD", parameters));
                int InsuranceID = Convert.ToInt32(parameters[17].Value);
                return new OperationDetails(true, ((InsuranceModel.InsuranceID == 0) ? "Insurance information save successfully" : "Insurance information save successfully"), null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public InsuranceModel GetInsuranceList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            InsuranceModel InsuranceModel = new InsuranceModel();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_GetInsurance", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {
                            InsuranceModel.InsuranceID = Convert.ToInt32(reader["InsuranceID"].ToString());
                            InsuranceModel.PolicyNumber = reader["PolicyNumber"].ToString();
                            InsuranceModel.CardNumber = reader["CardNumber"].ToString();
                            InsuranceModel.IssueDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["IssueDate"].ToString());
                            InsuranceModel.ExpiryDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["ExpiryDate"].ToString());
                            InsuranceModel.Deduction = reader["Deduction"].ToString();
                            InsuranceModel.CancellationDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["CancellationDate"].ToString());
                            InsuranceModel.InsuranceType = Convert.ToInt32(reader["InsuranceType"].ToString());
                            InsuranceModel.Returned = Convert.ToBoolean(reader["Returned"].ToString());
                            InsuranceModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            InsuranceModel.TotalAmount = Convert.ToDecimal(reader["TotalAmount"] == DBNull.Value || reader["TotalAmount"].ToString() == "" ? "0" : reader["TotalAmount"].ToString());
                            InsuranceModel.AddDeduction = Convert.ToBoolean(reader["AddDeduction"].ToString());
                            //  InsuranceModel.InsuredBySchool = Convert.ToDecimal(reader["InsuredBySchool"].ToString());
                            InsuranceModel.TotalAmountBySchool = Convert.ToDecimal(reader["TotalAmountBySchool"].ToString() == "" || reader["TotalAmountBySchool"] == DBNull.Value ? "0" : reader["TotalAmountBySchool"].ToString());
                            InsuranceModel.Note = reader["Note"].ToString();
                            InsuranceModel.Deduct = Convert.ToBoolean(reader["Deduct"].ToString());
                        }
                    }
                }
                return InsuranceModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteInsuranceById(int InsuranceID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntInsuranceID",InsuranceID) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_InsuranceCUD", parameters);
                return new OperationDetails(true, "Record deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error hass occurred", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PickList> GetInsuranceTypeList()
        {

            // Create a list to hold the Absent		
            List<PickList> objPickList = new List<PickList>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetHRInsurancetype"))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            objPickList.Add(new PickList() { id = Convert.ToInt32(reader["HRInsuranceTypeID"].ToString()), text = Convert.ToString(reader["InsuranceTypeName_1"]) });
                        }
                    }
                }
                // Check if the reader returned any rows

               
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
            return objPickList;
        }

        public List<PickList> GetInsuranceEligibilityList()
        {

            // Create a list to hold the Absent		
            List<PickList> objPickList = new List<PickList>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_GetInsuranceEligibilityList"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        while (reader.Read())
                        {
                            objPickList.Add(new PickList() { id = Convert.ToInt32(reader["InsuranceEligibilityId"].ToString()), text = Convert.ToString(reader["InsuranceEligibilityName_1"]) });
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return objPickList;

        }
        public List<InsuranceCategoryAmount> GetAmountByInsuranceCategoryId(int insuranceCategoryId)
        {

            SqlParameter[] parameters =
                   {
                        new SqlParameter("@InsuranceCategory",insuranceCategoryId)
                    };
            // Create a list to hold the Absent		
            List<InsuranceCategoryAmount> insuranceCategoryAmounts = new List<InsuranceCategoryAmount>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_GetAmountByInsuranceCategoryId", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        InsuranceCategoryAmount insuranceCategoryAmount;
                        while (reader.Read())
                        {
                            insuranceCategoryAmount = new InsuranceCategoryAmount();
                            insuranceCategoryAmount.CatAmountID = Convert.ToInt32(reader["CatAmountID"].ToString());
                            insuranceCategoryAmount.ACYearID = Convert.ToInt32(reader["ACYearID"].ToString());
                            insuranceCategoryAmount.Amount = Convert.ToDecimal(reader["Amount"].ToString());
                            insuranceCategoryAmount.HRInsuranceTypeID = Convert.ToInt32(reader["HRInsuranceTypeID"].ToString());
                            insuranceCategoryAmounts.Add(insuranceCategoryAmount);
                        }
                    }
                }
                return insuranceCategoryAmounts;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }
        public List<RelationModel> GetRelationTypeList()
        {

            // Create a list to hold the Absent		
            List<RelationModel> RelationTypeModelList = new List<RelationModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetRelationName"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        RelationModel RelationModel;
                        while (reader.Read())
                        {
                            RelationModel = new RelationModel();
                            RelationModel.RelationId = Convert.ToInt32(reader["ID"].ToString());
                            RelationModel.RelationName = reader["Name"].ToString();



                            //AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());


                            RelationTypeModelList.Add(RelationModel);
                        }
                    }
                }
                return RelationTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertInsdependence(InsdependenceModel InsdependenceModel)
        {
            try
            {

                SqlParameter[] parameters =
                    {

                                     new SqlParameter("@aTntTranMode",InsdependenceModel.TranMode) ,
                      new SqlParameter("@aIntInsuranceDependenceID ",InsdependenceModel.InsuranceDependenceID) ,
                      new SqlParameter("@aNvrInsuranceDependenceName_1", InsdependenceModel.InsuranceDependenceName_1) ,
                      new SqlParameter("@aNvrInsuranceDependenceName_2", InsdependenceModel.InsuranceDependenceName_2) ,
                       new SqlParameter("@aTntRelation", InsdependenceModel.Relation) ,
                      new SqlParameter("@aNvrCardNumber ", InsdependenceModel.CardNumber),
                      new SqlParameter("@aSntTeacherID",InsdependenceModel.EmployeeID) ,
                      new SqlParameter("@aNumAmount",InsdependenceModel.Amount) ,
                      new SqlParameter("@aIntInsuredBySchool", InsdependenceModel.InsuredBySchool),
                      new SqlParameter("@aNvrNote",InsdependenceModel.Note),

                    new SqlParameter("@output", 0)


                    };
                parameters[10].Direction = ParameterDirection.Output;
                int InsuranceID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspInsdependenceCUD", parameters));
                return new OperationDetails(true, (InsdependenceModel.TranMode == 1 ? "Insurance dependant saved successfully" : "Insurance dependant saved successfully"), null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public InsdependenceModel GetInsdependenceByID(int InsuranceDependenceID)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@InsuranceDependenceID",InsuranceDependenceID)
                    };
            // Create a list to hold the Absent		
            List<InsdependenceModel> InsdependenceList = new List<InsdependenceModel>();
            try
            {
                InsdependenceModel InsdependenceModel = new InsdependenceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetInsdependenceByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {

                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {

                            InsdependenceModel.InsuranceDependenceID = Convert.ToInt32(reader["InsuranceDependenceID"].ToString());
                            InsdependenceModel.InsuranceDependenceName_1 = reader["InsuranceDependenceName_1"].ToString();

                            InsdependenceModel.Relation = Convert.ToInt32(reader["Relation"].ToString());
                            InsdependenceModel.CardNumber = reader["CardNumber"].ToString();
                            InsdependenceModel.Note = reader["Note"].ToString();




                        }
                    }
                }
                return InsdependenceModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }


        public List<InsdependenceModel> GetInsdependenceList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            List<InsdependenceModel> InsdependenceList = new List<InsdependenceModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetInsdependence", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        InsdependenceModel InsdependenceModel;
                        while (reader.Read())
                        {
                            InsdependenceModel = new InsdependenceModel();
                            InsdependenceModel.InsuranceDependenceID = Convert.ToInt32(reader["InsuranceDependenceID"].ToString());
                            InsdependenceModel.Name = reader["InsuranceDependenceName_1"].ToString();
                            InsdependenceModel.RelationName = reader["RelationName"].ToString();
                            InsdependenceModel.Relation = Convert.ToInt32(reader["Relation"].ToString());
                            InsdependenceModel.CardNumber = reader["CardNumber"].ToString();
                            InsdependenceModel.Note = reader["Note"].ToString();



                            InsdependenceList.Add(InsdependenceModel);
                        }
                    }
                }
                return InsdependenceList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteInsdependenceById(int InsuranceDependenceID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntInsuranceDependenceID",InsuranceDependenceID) ,
                      new SqlParameter("@aTntTranMode",3),
                       new SqlParameter("@output", 0)
                    };
                parameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspInsdependenceCUD", parameters);
                return new OperationDetails(true, "Record deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<InsuranceCategoryAmount> GetInsuranceCategoryAmountPaging()
        {
            List<InsuranceCategoryAmount> insuranceCategoryAmounts = new List<InsuranceCategoryAmount>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetInsuranceCategoryAmountList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader reader = sqlCommand.ExecuteReader();

                // Check if the reader returned any rows
                if (reader.HasRows)
                {
                    // While the reader has rows we loop through them,
                    // create new PassportModel, and insert them into our list
                    InsuranceCategoryAmount insuranceCategoryAmount;
                    while (reader.Read())
                    {
                        insuranceCategoryAmount = new InsuranceCategoryAmount();
                        insuranceCategoryAmount.CatAmountID = Convert.ToInt32(reader["CatAmountID"].ToString());
                        insuranceCategoryAmount.HRInsuranceTypeID = Convert.ToInt32(reader["HRInsuranceTypeID"].ToString());
                        insuranceCategoryAmount.ACYearID = Convert.ToInt32(reader["ACYearID"].ToString());
                        insuranceCategoryAmount.Amount = Convert.ToDecimal(reader["Amount"].ToString());
                        insuranceCategoryAmount.InsuranceTypeName = reader["InsuranceTypeName"].ToString().Trim();
                        insuranceCategoryAmount.AcademicYear = reader["AcademicYear"].ToString().Trim();
                        insuranceCategoryAmounts.Add(insuranceCategoryAmount);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModelList.", exception);
                throw;
            }
            finally
            {

            }
            return insuranceCategoryAmounts;
        }

        public bool CheckInsuranceCategoryAmountExists(int insuranceTypeId, int academicYearId, decimal amount)
        {
            bool IsExistsSalary = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CheckInsuranceCategoryAmountExists", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@InsuranceTypeId", insuranceTypeId);
                sqlCommand.Parameters.AddWithValue("@AcademicYearId", academicYearId);
                sqlCommand.Parameters.AddWithValue("@Amount", amount);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        IsExistsSalary = Convert.ToBoolean(reader["IsExists"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return IsExistsSalary;
        }

        public OperationDetails InsertInsuranceCategoryAmount(List<InsuranceCategoryAmount> insuranceAmountList, int userId)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("HRInsuranceTypeID", typeof(Int32));
            dt.Columns.Add("ACYearID", typeof(Int32));
            dt.Columns.Add("Amount", typeof(Int32));

            foreach (var item in insuranceAmountList)
            {
                dt.Rows.Add(item.HRInsuranceTypeID, item.ACYearID, item.Amount);
            }
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@InsuranceCategoryAmountTableType", dt));
            paramList.Add(new SqlParameter("@UserId", userId));
            if (dataHelper.ExcutestoredProcedure(paramList, "Hr_Stp_InsertInsuranceCategoryAmount"))
            {
                return new OperationDetails() { Message = "Records updated successfully.", Success = true };
            }
            else
            {
                return new OperationDetails() { Message = "Technical error has occurred.", Success = false };
            }
        }

        public InsuranceCategoryAmount GetInsuranceCategoryAmountById(int id)
        {
            InsuranceCategoryAmount insuranceCategoryAmount = null;

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetInsuranceCategoryAmountById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Id", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        insuranceCategoryAmount = new InsuranceCategoryAmount();
                        insuranceCategoryAmount.CatAmountID = Convert.ToInt32(sqlDataReader["CatAmountID"].ToString());
                        insuranceCategoryAmount.InsuranceTypeName = Convert.ToString(sqlDataReader["InsuranceTypeName"].ToString());
                        insuranceCategoryAmount.AcademicYear = Convert.ToString(sqlDataReader["AcademicYear"].ToString());
                        insuranceCategoryAmount.HRInsuranceTypeID = Convert.ToInt32(sqlDataReader["HRInsuranceTypeID"]);
                        insuranceCategoryAmount.ACYearID = Convert.ToInt32(sqlDataReader["ACYearID"]);
                        insuranceCategoryAmount.Amount = Convert.ToDecimal(sqlDataReader["Amount"]);
                    }

                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return insuranceCategoryAmount;
        }

        public bool DeleteInsuranceCategoryAmount(int id)
        {
            bool IsExistsSalary = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DeleteInsuraceCategoryAmount", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Id", id);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        IsExistsSalary = Convert.ToBoolean(reader["Result"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return IsExistsSalary;
        }



        //public List<AbsentModel> GetInsuranceList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, out int totalCount, int empid = 0)
        //{








        //    List<AbsentModel> AbsentModelList = null;
        //    try
        //    {
        //        AbsentModelList = new List<AbsentModel>();

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("HR_uspGetPayEmployeeAbsentPaging", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
        //        sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
        //        sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
        //        sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);

        //        SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
        //        TotalCount.Direction = ParameterDirection.Output;
        //        sqlCommand.Parameters.Add(TotalCount);

        //        sqlCommand.Parameters.AddWithValue("@empid", empid);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {
        //            LocationModel locationModel;

        //            AbsentModel AbsentModel;

        //            while (sqlDataReader.Read())
        //            {






        //                AbsentModel = new AbsentModel();
        //                AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentID"].ToString());
        //                AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentHeaderID"].ToString());
        //                AbsentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
        //                AbsentModel.PayEmployeeAbsentFromDate = sqlDataReader["PayEmployeeAbsentFromDate"].ToString();
        //                AbsentModel.PayEmployeeAbsentToDate = sqlDataReader["PayEmployeeAbsentToDate"].ToString();

        //                //AbsentModel.PayAbsentTypeID = Convert.ToInt32(sqlDataReader["PayAbsentTypeID"].ToString());
        //                // AbsentModel.PayAbsentTypeName = sqlDataReader["PayAbsentTypeName"].ToString();
        //                AbsentModel.LeaveTypeID = Convert.ToInt32(sqlDataReader["LeaveTypeID"].ToString());
        //                AbsentModel.LeaveTypeName = sqlDataReader["LeaveTypeName"].ToString();
        //                AbsentModel.EmployeeName = sqlDataReader["EmployeeName"].ToString();
        //                AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(sqlDataReader["IsMedicalCertificateProvided"].ToString());
        //                AbsentModel.IsDeducted = Convert.ToBoolean(sqlDataReader["IsDeducted"].ToString());

        //                AbsentModelList.Add(AbsentModel);








        //            }
        //        }
        //        sqlConnection.Close();

        //        totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return AbsentModelList;
        //}







    }
}
