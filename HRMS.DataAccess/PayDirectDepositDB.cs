﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class PayDirectDepositDB
    {

        private SqlConnection sqlConnection { get; set; }
        private SqlCommand sqlCommand { get; set; }
        private SqlDataReader sqlReader { get; set; }
        private DataHelper dataHelper { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public string format = "dd-MM-yyyy";
        public string sqlDateFormat = "MM-dd-yyyy hh:mm:ss";

        public OperationDetails InsertPayDirectDeposit(PayDirectDepositModel payDirectDepositModel, int OperationId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayDirectdepositCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", OperationId);
                sqlCommand.Parameters.AddWithValue("@aIntDirectDepositID", payDirectDepositModel.DirectDepositID);
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", payDirectDepositModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aNvrAccountNumber", payDirectDepositModel.AccountNumber);
                sqlCommand.Parameters.AddWithValue("@aIntBankID", payDirectDepositModel.BankID);
                sqlCommand.Parameters.AddWithValue("@aIntAccountTypeID", payDirectDepositModel.AccountTypeID);
                sqlCommand.Parameters.AddWithValue("@aNvrEmpName", payDirectDepositModel.EmpName);
                sqlCommand.Parameters.AddWithValue("@aIntCategoryID", payDirectDepositModel.CategoryID);
                sqlCommand.Parameters.AddWithValue("@aIntBranchID", payDirectDepositModel.BranchID);
                sqlCommand.Parameters.AddWithValue("@aNumAmountBlocked", payDirectDepositModel.AmountBlocked);
                sqlCommand.Parameters.AddWithValue("@aBitFirstSalaryBlocked", payDirectDepositModel.FirstSalaryBlocked);
                sqlCommand.Parameters.AddWithValue("@IsAccountNoUseAsIBAN", payDirectDepositModel.IsAccountNoUseAsIBAN);                //var date = payDirectDepositModel.AccountDate.Split('/').Select(x => Convert.ToInt32(x)).ToList();
                sqlCommand.Parameters.AddWithValue("@aDttAccountDate", GeneralDB.CommonDB.SetCulturedDate(payDirectDepositModel.AccountDate));
                DateTime? dtCancel = null;
                if (payDirectDepositModel.CancellationDate != null)
                    sqlCommand.Parameters.AddWithValue("@aDttCancellationDate", GeneralDB.CommonDB.SetCulturedDate(payDirectDepositModel.CancellationDate));

                sqlCommand.Parameters.AddWithValue("@aBitactive", payDirectDepositModel.Active);
                sqlCommand.Parameters.AddWithValue("@aNvrIBAN", payDirectDepositModel.UniAccountNumber);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();


                if (OperationId == 1)
                {
                    return new OperationDetails(true, "Pay direct deposite details saved successfully.", null, id);
                }
                if (OperationId == 2)
                {
                    return new OperationDetails(true, "Pay direct deposite details Updated successfully.", null, id);
                }
                else
                {
                    return new OperationDetails(true, "Pay direct deposite details deleted successfully.", null, id);
                }



            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while saving Pay direct deposite details.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public int GetBankDetailIsCount(int id)
        {
            int BankDetailIsCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_BankInformationValidation", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@aIntDirectDepositID", id);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                BankDetailIsCount = (int)sqlCommand.ExecuteScalar();                              
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return BankDetailIsCount;
        }

        public List<PayDirectDepositModel> GetPayDirectDepositFullInformation(int EmployeeId)
        {
            List<PayDirectDepositModel> payDirectDepositList = null;
            try
            {
                payDirectDepositList = new List<PayDirectDepositModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDirectDepositFullInformation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayDirectDepositModel payDirectDepositModel;
                    while (sqlDataReader.Read())
                    {
                        payDirectDepositModel = new PayDirectDepositModel();

                        payDirectDepositModel.DirectDepositID = Convert.ToInt32(sqlDataReader["DirectDepositID"].ToString());
                        payDirectDepositModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        payDirectDepositModel.BankID = Convert.ToInt32(sqlDataReader["BankID"].ToString() == "" ? "0" : sqlDataReader["BankID"].ToString());
                        payDirectDepositModel.AccountTypeID = Convert.ToInt32(sqlDataReader["AccountTypeID"]);
                        payDirectDepositModel.AccountNumber = Convert.ToString(sqlDataReader["AccountNumber"]);
                        payDirectDepositModel.EmpName = Convert.ToString(sqlDataReader["EmpName"]);
                        payDirectDepositModel.CategoryID = Convert.ToInt32(sqlDataReader["CategoryID"]);
                        payDirectDepositModel.BranchID = Convert.ToInt32(sqlDataReader["BranchID"]);
                        payDirectDepositModel.AmountBlocked = sqlDataReader["AmountBlocked"] == DBNull.Value ? 0 : Convert.ToDecimal(sqlDataReader["AmountBlocked"]);
                        payDirectDepositModel.FirstSalaryBlocked = sqlDataReader["AmountBlocked"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["FirstSalaryBlocked"]);
                        payDirectDepositModel.AccountDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AccountDate"].ToString());
                        payDirectDepositModel.CancellationDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["CancellationDate"].ToString());
                        payDirectDepositModel.Active = sqlDataReader["active"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["active"]);
                        payDirectDepositModel.UniAccountNumber = Convert.ToString(sqlDataReader["UniAccountNumber"]);
                        payDirectDepositModel.IsAccountNoUseAsIBAN = Convert.ToBoolean(sqlDataReader["IsAccountNoUseAsIBAN"]);
                        payDirectDepositModel.payBankModel.BankName_1 = Convert.ToString(sqlDataReader["BankName_1"]);
                        payDirectDepositModel.payBankModel.BankName_2 = Convert.ToString(sqlDataReader["BankName_2"]);
                        payDirectDepositModel.payBankModel.BankName_3 = Convert.ToString(sqlDataReader["BankName_3"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_1 = Convert.ToString(sqlDataReader["BranchName_1"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_2 = Convert.ToString(sqlDataReader["BranchName_2"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_3 = Convert.ToString(sqlDataReader["BranchName_3"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_1 = Convert.ToString(sqlDataReader["AccountTypeName_1"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_2 = Convert.ToString(sqlDataReader["AccountTypeName_2"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_3 = Convert.ToString(sqlDataReader["AccountTypeName_3"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_1 = Convert.ToString(sqlDataReader["CategoryName_1"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_2 = Convert.ToString(sqlDataReader["CategoryName_2"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_3 = Convert.ToString(sqlDataReader["CategoryName_3"]);

                        payDirectDepositList.Add(payDirectDepositModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDirectDepositList;
        }

        public PayDirectDepositModel GetPayDirectDepositFullInformationforEmployee(int EmployeeId)
        {
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel(); ;
            try
            {


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDirectDepositFullInformationforEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        payDirectDepositModel.DirectDepositID = Convert.ToInt32(sqlDataReader["DirectDepositID"].ToString());
                        payDirectDepositModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        payDirectDepositModel.BankID = Convert.ToInt32(sqlDataReader["BankID"]);
                        payDirectDepositModel.AccountTypeID = Convert.ToInt32(sqlDataReader["AccountTypeID"]);
                        payDirectDepositModel.AccountNumber = Convert.ToString(sqlDataReader["AccountNumber"]);
                        payDirectDepositModel.EmpName = Convert.ToString(sqlDataReader["EmpName"]);
                        payDirectDepositModel.CategoryID = Convert.ToInt32(sqlDataReader["CategoryID"]);
                        payDirectDepositModel.BranchID = Convert.ToInt32(sqlDataReader["BranchID"]);
                        payDirectDepositModel.AmountBlocked = sqlDataReader["AmountBlocked"] == DBNull.Value ? 0 : Convert.ToDecimal(sqlDataReader["AmountBlocked"]);
                        payDirectDepositModel.FirstSalaryBlocked = sqlDataReader["AmountBlocked"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["FirstSalaryBlocked"]);
                        payDirectDepositModel.AccountDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AccountDate"].ToString());
                        payDirectDepositModel.CancellationDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["CancellationDate"].ToString());
                        payDirectDepositModel.Active = sqlDataReader["active"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["active"]);
                        payDirectDepositModel.UniAccountNumber = Convert.ToString(sqlDataReader["UniAccountNumber"]);
                        payDirectDepositModel.IsAccountNoUseAsIBAN = Convert.ToBoolean(sqlDataReader["IsAccountNoUseAsIBAN"]);
                        payDirectDepositModel.payBankModel.BankName_1 = Convert.ToString(sqlDataReader["BankName_1"]);
                        payDirectDepositModel.payBankModel.BankName_2 = Convert.ToString(sqlDataReader["BankName_2"]);
                        payDirectDepositModel.payBankModel.BankName_3 = Convert.ToString(sqlDataReader["BankName_3"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_1 = Convert.ToString(sqlDataReader["BranchName_1"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_2 = Convert.ToString(sqlDataReader["BranchName_2"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_3 = Convert.ToString(sqlDataReader["BranchName_3"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_1 = Convert.ToString(sqlDataReader["AccountTypeName_1"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_2 = Convert.ToString(sqlDataReader["AccountTypeName_2"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_3 = Convert.ToString(sqlDataReader["AccountTypeName_3"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_1 = Convert.ToString(sqlDataReader["CategoryName_1"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_2 = Convert.ToString(sqlDataReader["CategoryName_2"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_3 = Convert.ToString(sqlDataReader["CategoryName_3"]);
                    }


                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDirectDepositModel;
        }

        public PayDirectDepositModel GetPayDirectDepositByID(int DirectDepositID)
        {
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDirectDepositById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aSntDirectDepositID", DirectDepositID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        payDirectDepositModel.DirectDepositID = Convert.ToInt32(sqlDataReader["DirectDepositID"].ToString());
                        payDirectDepositModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        payDirectDepositModel.BankID = Convert.ToInt32(sqlDataReader["BankID"].ToString() == "" ? "0" : sqlDataReader["BankID"].ToString());
                        payDirectDepositModel.AccountTypeID = Convert.ToInt32(sqlDataReader["AccountTypeID"]);
                        payDirectDepositModel.AccountNumber = Convert.ToString(sqlDataReader["AccountNumber"]);
                        payDirectDepositModel.EmpName = Convert.ToString(sqlDataReader["EmpName"]);
                        payDirectDepositModel.CategoryID = Convert.ToInt32(sqlDataReader["CategoryID"]);
                        payDirectDepositModel.BranchID = sqlDataReader["BranchID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["BranchID"]);
                        payDirectDepositModel.AmountBlocked = sqlDataReader["AmountBlocked"] == DBNull.Value ? 0 : Convert.ToDecimal(sqlDataReader["AmountBlocked"]);
                        payDirectDepositModel.FirstSalaryBlocked = sqlDataReader["FirstSalaryBlocked"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["FirstSalaryBlocked"]);
                        payDirectDepositModel.AccountDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["AccountDate"].ToString());
                        payDirectDepositModel.CancellationDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["CancellationDate"].ToString());
                        payDirectDepositModel.IsAccountNoUseAsIBAN = Convert.ToBoolean(sqlDataReader["IsAccountNoUseAsIBAN"]);
                        payDirectDepositModel.Active = sqlDataReader["active"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["active"]);
                        payDirectDepositModel.UniAccountNumber = Convert.ToString(sqlDataReader["UniAccountNumber"]);
                        payDirectDepositModel.payBankModel.BankName_1 = Convert.ToString(sqlDataReader["BankName_1"]);
                        payDirectDepositModel.payBankModel.BankName_2 = Convert.ToString(sqlDataReader["BankName_2"]);
                        payDirectDepositModel.payBankModel.BankName_3 = Convert.ToString(sqlDataReader["BankName_3"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_1 = Convert.ToString(sqlDataReader["BranchName_1"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_2 = Convert.ToString(sqlDataReader["BranchName_2"]);
                        payDirectDepositModel.payBankBranchModel.BranchName_3 = Convert.ToString(sqlDataReader["BranchName_3"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_1 = Convert.ToString(sqlDataReader["AccountTypeName_1"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_2 = Convert.ToString(sqlDataReader["AccountTypeName_2"]);
                        payDirectDepositModel.payAccountTypesModel.AccountTypeName_3 = Convert.ToString(sqlDataReader["AccountTypeName_3"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_1 = Convert.ToString(sqlDataReader["CategoryName_1"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_2 = Convert.ToString(sqlDataReader["CategoryName_2"]);
                        payDirectDepositModel.payCategoriesModel.CategoryName_3 = Convert.ToString(sqlDataReader["CategoryName_3"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDirectDepositModel;
        }

        public bool CheckUni_BanAccountNumber(int DirectDepositID, int EmployeeeId, string UniAccountNumber)
        {
            dataHelper = new DataHelper();           
            string sqlQuery = "SELECT DD.UniAccountNumber, DD.EmployeeId, DD.DirectDepositID from HR_PayDirectdeposit DD INNER JOIN HR_EmployeeDetail ED ON ED.EmployeeID = DD.EmployeeID AND ED.isActive = 1 WHERE RTRIM(LTRIM(LOWER(DD.UniAccountNumber))) = '" + UniAccountNumber.Trim().ToLower() + "' ";
            bool retunVal = false;
            DataTable dt = dataHelper.ExcuteCommandText(sqlQuery);
            if (DirectDepositID == 0)
            {
                if (dt.Rows.Count > 0)
                    retunVal = true;

            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["EmployeeId"].ToString() == EmployeeeId.ToString() && dr["DirectDepositID"].ToString() == DirectDepositID.ToString())
                    {

                    }
                    else
                        retunVal = true;
                }
            }

            return retunVal;
        }

    }

}
