﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class TitleDB
    {
        DataAccess.GeneralDB.DataHelper dataHelper;

        public List<TitleModel> GetTitleList()
        {
            List<TitleModel> titleList = new List<TitleModel>();
            TitleModel titleModel;
            dataHelper = new DataHelper();
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(new List<SqlParameter>(), "Hr_Stp_GetAllTitle");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    titleModel = new TitleModel();
                    titleModel.TitleId = Convert.ToInt32(dr["TitleID"].ToString());
                    titleModel.TitleName = dr["TitleName_1"].ToString();
                    titleList.Add(titleModel);
                }
            }
            return titleList;
        }
    }
}
