﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HRMS.DataAccess.GeneralDB
{
    public class HRQualificationDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<HRQualificationModel> GetAllHRQualification()
        {
            List<HRQualificationModel> HRQualificationList = null;
            try
            {
                HRQualificationList = new List<HRQualificationModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_HRQualification", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HRQualificationModel HRQualificationModel;
                    while (sqlDataReader.Read())
                    {
                        HRQualificationModel = new HRQualificationModel();

                        HRQualificationModel.HRQualificationID = Convert.ToInt32(sqlDataReader["HRQualificationID"].ToString());
                        HRQualificationModel.HRQualificationName_1 = sqlDataReader["HRQualificationName_1"] == DBNull.Value ? "" : sqlDataReader["HRQualificationName_1"].ToString();
                        HRQualificationList.Add(HRQualificationModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return HRQualificationList.OrderBy(x=>x.HRQualificationName_1).ToList();
        }

        public OperationDetails InsertHRQualification(HRQualificationModel HRQualificationModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@HRQualificationName_1",HRQualificationModel.HRQualificationName_1) ,
                      new SqlParameter("@HRQualificationName_2", HRQualificationModel.HRQualificationName_2) ,
                      new SqlParameter("@HRQualificationName_3", HRQualificationModel.HRQualificationName_3)
                    };
                int HRQualificationId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_GEN_HRQualification", parameters));
                if (HRQualificationId > 0)
                {
                    op.Success = true;
                    op.CssClass = "success";
                    op.Message = "Subject details saved successfully.";
                }
                else
                {
                    op.Success = false;
                    op.CssClass = "error";
                    op.Message = "Error : while inserting subject details.";
                }                
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while inserting subject details.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {

            }
            return op;
        }

        public List<HRQualificationModel> GetQualifications()
        {
            List<HRQualificationModel> HRQualificationModelList = new List<HRQualificationModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetQualifications", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    HRQualificationModel qualificationModel;
                    while (reader.Read())
                    {
                        qualificationModel = new HRQualificationModel();

                        qualificationModel.HRQualificationID= Convert.ToInt32(reader["HRDegreeID"].ToString());
                        qualificationModel.HRQualificationName_1 = reader["HRDegreeName_1"].ToString() == "" ? "" : reader["HRDegreeName_1"].ToString();
                        qualificationModel.HRQualificationName_2 = reader["HRDegreeName_2"].ToString() == "" ? "" : reader["HRDegreeName_2"].ToString();
                        qualificationModel.HRQualificationName_3 = reader["HRDegreeName_3"].ToString() == "" ? "" : reader["HRDegreeName_3"].ToString();
                        qualificationModel.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        qualificationModel.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                        HRQualificationModelList.Add(qualificationModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
            return HRQualificationModelList;
        }

        public HRQualificationModel GetQualificationsById(int HRDegreeID)
        {
            HRQualificationModel qualificationModel = new HRQualificationModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetQualificationsById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@HRDegreeID", HRDegreeID);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        qualificationModel.HRQualificationID = Convert.ToInt32(reader["HRDegreeID"].ToString());
                        qualificationModel.HRQualificationName_1 = reader["HRDegreeName_1"].ToString() == "" ? "" : reader["HRDegreeName_1"].ToString();
                        qualificationModel.HRQualificationName_2 = reader["HRDegreeName_2"].ToString() == "" ? "" : reader["HRDegreeName_2"].ToString();
                        qualificationModel.HRQualificationName_3 = reader["HRDegreeName_3"].ToString() == "" ? "" : reader["HRDegreeName_3"].ToString();
                        qualificationModel.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        qualificationModel.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                    }
                }
                sqlConnection.Close();
                return qualificationModel;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddEditQualifications(HRQualificationModel qualificationModel, int trnMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateQualificationDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", trnMode);
                sqlCommand.Parameters.AddWithValue("@HRDegreeID", qualificationModel.HRQualificationID);
                sqlCommand.Parameters.AddWithValue("@HRDegreeName_1", qualificationModel.HRQualificationName_1);
                sqlCommand.Parameters.AddWithValue("@HRDegreeName_2", qualificationModel.HRQualificationName_2);
                sqlCommand.Parameters.AddWithValue("@HRDegreeName_3", qualificationModel.HRQualificationName_3);
                sqlCommand.Parameters.AddWithValue("@IsActive", qualificationModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@IsDeleted", qualificationModel.IsDeleted);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting qualification details.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }

        public List<HRQualificationModel> GetQualificationSubjects()
        {
            List<HRQualificationModel> HRQualificationModelList = new List<HRQualificationModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetQualificationSubjects", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    HRQualificationModel qualificationModel;
                    while (reader.Read())
                    {
                        qualificationModel = new HRQualificationModel();

                        qualificationModel.HRQualificationID = Convert.ToInt32(reader["HRQualificationID"].ToString());
                        qualificationModel.HRQualificationName_1 = reader["HRQualificationName_1"].ToString() == "" ? "" : reader["HRQualificationName_1"].ToString();
                        qualificationModel.HRQualificationName_2 = reader["HRQualificationName_2"].ToString() == "" ? "" : reader["HRQualificationName_2"].ToString();
                        qualificationModel.HRQualificationName_3 = reader["HRQualificationName_3"].ToString() == "" ? "" : reader["HRQualificationName_3"].ToString();
                        qualificationModel.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        qualificationModel.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                        HRQualificationModelList.Add(qualificationModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
            return HRQualificationModelList;
        }

        public HRQualificationModel GetQualificationSubjectById(int HRQualificationID)
        {
            HRQualificationModel qualificationModel = new HRQualificationModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetQualificationSubjectById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@HRQualificationID", HRQualificationID);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        qualificationModel.HRQualificationID = Convert.ToInt32(reader["HRQualificationID"].ToString());
                        qualificationModel.HRQualificationName_1 = reader["HRQualificationName_1"].ToString() == "" ? "" : reader["HRQualificationName_1"].ToString();
                        qualificationModel.HRQualificationName_2 = reader["HRQualificationName_2"].ToString() == "" ? "" : reader["HRQualificationName_2"].ToString();
                        qualificationModel.HRQualificationName_3 = reader["HRQualificationName_3"].ToString() == "" ? "" : reader["HRQualificationName_3"].ToString();
                        qualificationModel.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        qualificationModel.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                    }
                }
                sqlConnection.Close();
                return qualificationModel;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddEditQualificationSubject(HRQualificationModel qualificationModel, int trnMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateSubjectDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", trnMode);
                sqlCommand.Parameters.AddWithValue("@HRQualificationID", qualificationModel.HRQualificationID);
                sqlCommand.Parameters.AddWithValue("@HRQualificationName_1", qualificationModel.HRQualificationName_1);
                sqlCommand.Parameters.AddWithValue("@HRQualificationName_2", qualificationModel.HRQualificationName_2);
                sqlCommand.Parameters.AddWithValue("@HRQualificationName_3", qualificationModel.HRQualificationName_3);
                sqlCommand.Parameters.AddWithValue("@IsActive", qualificationModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@IsDeleted", qualificationModel.IsDeleted);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting qualification subject details.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }
    }
}
