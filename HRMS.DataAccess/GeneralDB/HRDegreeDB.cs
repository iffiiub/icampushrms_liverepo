﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HRMS.DataAccess.GeneralDB
{
    public class HRDegreeDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<HRDegreeModel> GetAllHRDegree()
        {
            List<HRDegreeModel> HRDegreeList = null;
            try
            {
                HRDegreeList = new List<HRDegreeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_HRDegree", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HRDegreeModel HRDegreeModel;
                    while (sqlDataReader.Read())
                    {
                        HRDegreeModel = new HRDegreeModel();

                        HRDegreeModel.HRDegreeID = Convert.ToInt32(sqlDataReader["HRDegreeID"].ToString());
                        HRDegreeModel.HRDegreeName_1 = sqlDataReader["HRDegreeName_1"] == DBNull.Value ? "" : sqlDataReader["HRDegreeName_1"].ToString();
                        HRDegreeList.Add(HRDegreeModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return HRDegreeList.OrderBy(x=>x.HRDegreeName_1).ToList();
        }

        public OperationDetails InsertHRDegree(HRDegreeModel HRDegreeModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@HRDegreeName_1",HRDegreeModel.HRDegreeName_1) ,
                      new SqlParameter("@HRDegreeName_2", HRDegreeModel.HRDegreeName_2) ,
                      new SqlParameter("@HRDegreeName_3", HRDegreeModel.HRDegreeName_3)
                    };
                int HRDegreeId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_GEN_HRDegree", parameters));
                if (HRDegreeId>0)
                {
                    op.Success = true;
                    op.CssClass = "success";
                    op.Message = "Qualification details saved successfully.";
                }
                else
                {
                    op.Success = false;
                    op.CssClass = "error";
                    op.Message = "Error : while inserting qualification details.";
                }               
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while inserting qualification details.";
                op.CssClass = "error";
                op.Exception = exception;
                //throw;
            }
            finally
            {

            }
            return op;
        }
    }
}
