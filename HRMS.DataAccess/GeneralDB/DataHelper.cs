﻿using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.GeneralDB
{

    public class DataHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public DataTable ExcuteCommandText(string commandText)
        {
            DataTable dt = new DataTable();
            try
            {
                //To get sp name
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(commandText, connectionString);
                da.Fill(dt);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return dt;
        }

        public bool SimpleCommandText(string queryString)
        {
            bool returnVal = false;
            try
            {
                //To get sp name
                using (SqlConnection connection = new SqlConnection(
               connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                returnVal = true;
            }
            catch (Exception exception)
            {
                returnVal = false;
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return returnVal;
        }

        public DataTable ExcuteStoredProcedureWithParameter(List<SqlParameter> parameters, string procedureName)
        {
            List<SqlParameter> pList = new List<SqlParameter>();

            DataTable dt = new DataTable();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, procedureName, parameters.ToArray()))
                {
                    //if (reader.HasRows)
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
            return dt;
        }

        public OperationDetails SaveModel(SqlCommand command)
        {
            int oprtationID = 0;
            string message = "";
            try
            {
                sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = connectionString;
                sqlConnection.Open();

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                command.Parameters.Add(OperationId);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                command.Parameters.Add(OperationMessage);
                command.Connection = sqlConnection;
                SqlDataReader sqlDataReader = command.ExecuteReader();
                oprtationID = Convert.ToInt32(OperationId.Value);
                message = OperationMessage.Value.ToString();

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return new OperationDetails(true, message, null, oprtationID);
        }

        public bool ExcutestoredProcedure(List<SqlParameter> parameters, string procedureName)
        {
            bool returnVal = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(procedureName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                foreach (var item in parameters)
                {
                    sqlCommand.Parameters.Add(item);
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                returnVal = true;
            }
            catch (Exception exception)
            {
                throw exception;
                returnVal = false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return returnVal;
        }
    }
}
