﻿using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.GeneralDB
{
    public class BankInformationDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public List<PayBanksModel> GetBanksList()
        {
            SqlParameter[] parameters =
                    {

                    };
            // Create a list to hold the Absent		
            List<PayBanksModel> banksList = new List<PayBanksModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetBanks", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PayBanksModel payBanksModel;
                        while (reader.Read())
                        {
                            payBanksModel = new PayBanksModel();
                            payBanksModel.BankID = Convert.ToInt32(reader["BankID"].ToString());
                            payBanksModel.BankName_1 = Convert.ToString(reader["BankName_1"].ToString());
                            payBanksModel.BankName_2 = Convert.ToString(reader["BankName_2"].ToString());
                            payBanksModel.BankName_3 = Convert.ToString(reader["BankName_3"].ToString());
                            payBanksModel.tr_Date = reader["tr_Date"].ToString();
                            payBanksModel.acc_code = Convert.ToString(reader["acc_code"].ToString());
                            payBanksModel.RtnCode = Convert.ToString(reader["RtnCode"].ToString());
                            banksList.Add(payBanksModel);
                        }
                    }
                }
                return banksList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public PayBanksModel GetBankdetailsById(int BankId)
        {
            SqlParameter[] parameters =
                    {
                      new SqlParameter("@BankId",BankId)
                    };

            PayBanksModel payBanksModel = new PayBanksModel();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetBankdetailsByid", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            payBanksModel = new PayBanksModel();
                            payBanksModel.BankID = Convert.ToInt32(reader["BankID"].ToString());
                            payBanksModel.BankName_1 = Convert.ToString(reader["BankName_1"].ToString());
                            payBanksModel.BankName_2 = Convert.ToString(reader["BankName_2"].ToString());
                            payBanksModel.BankName_3 = Convert.ToString(reader["BankName_3"].ToString());
                            payBanksModel.BankGroupID = Convert.ToInt32(reader["BankGroupID"].ToString() == "" ? "0" : reader["BankGroupID"].ToString());
                            payBanksModel.BankGroupName = Convert.ToString(reader["BankGroupName_1"]);
                            payBanksModel.tr_Date = reader["tr_Date"].ToString() == "" ? "" : Convert.ToDateTime(reader["tr_Date"].ToString()).ToString("dd/MM/yyyy");
                            payBanksModel.acc_code = Convert.ToString(reader["acc_code"].ToString());
                            payBanksModel.RtnCode = Convert.ToString(reader["RtnCode"].ToString());
                            payBanksModel.SwiftCode = Convert.ToString(reader["SwiftCode"].ToString());
                            payBanksModel.CashBank = Convert.ToBoolean(reader["CashBank"].ToString());
                            payBanksModel.PayBanksModeID = Convert.ToInt32(reader["PayBanksModeID"] == DBNull.Value ? 0 : reader["PayBanksModeID"]);

                        }
                    }
                }
                return payBanksModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PayBanksModel> GetBankListPaging()
        {
            List<PayBanksModel> BankList = new List<PayBanksModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetBankDetailsWithPaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    PayBanksModel payBank;
                    while (reader.Read())
                    {
                        payBank = new PayBanksModel();
                        payBank.BankID = Convert.ToInt32(reader["BankID"].ToString());
                        payBank.BankName_1 = reader["BankName_1"].ToString().Trim();
                        payBank.BankName_2 = reader["BankName_2"].ToString().Trim();
                        payBank.BankName_3 = reader["BankName_3"].ToString().Trim();
                        payBank.acc_code = reader["acc_code"].ToString().Trim();
                        payBank.RtnCode = reader["RtnCode"].ToString().Trim();
                        payBank.SwiftCode = reader["SwiftCode"].ToString().Trim();
                        payBank.BankGroupName = reader["BankGroup"].ToString();
                        payBank.CashBank =Convert.ToBoolean(reader["CashBank"].ToString());
                        BankList.Add(payBank);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModelList.", exception);
                throw;
            }
            finally
            {

            }
            return BankList;
        }

        public OperationDetails AddEditBank(PayBanksModel payBank, int trnMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdatebankDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", trnMode);
                sqlCommand.Parameters.AddWithValue("@BankID", payBank.BankID);
                sqlCommand.Parameters.AddWithValue("@BankName_1", payBank.BankName_1);
                sqlCommand.Parameters.AddWithValue("@BankName_2", payBank.BankName_2);
                sqlCommand.Parameters.AddWithValue("@BankName_3", payBank.BankName_3);
                sqlCommand.Parameters.AddWithValue("@BankGroupID", payBank.BankGroupID);
                sqlCommand.Parameters.AddWithValue("@acc_code", payBank.acc_code);
                sqlCommand.Parameters.AddWithValue("@RtnCode ", payBank.RtnCode);
                sqlCommand.Parameters.AddWithValue("@SwiftCode ", payBank.SwiftCode);
                sqlCommand.Parameters.AddWithValue("@CashBank ", payBank.CashBank);
                sqlCommand.Parameters.AddWithValue("@PayBanksModeID ", payBank.PayBanksModeID);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting Academic.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }
        public List<PayBankBranchModel> GetBankBranchesList()
        {
            SqlParameter[] parameters =
                    {

                    };
            // Create a list to hold the Absent		
            List<PayBankBranchModel> payBankBranchList = new List<PayBankBranchModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GEN_uspGetBankBranches", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PayBankBranchModel payBankBranchModel;
                        while (reader.Read())
                        {
                            payBankBranchModel = new PayBankBranchModel();
                            payBankBranchModel.BranchID = Convert.ToInt32(reader["BranchID"].ToString());
                            payBankBranchModel.BranchName_1 = Convert.ToString(reader["BranchName_1"].ToString());
                            payBankBranchModel.BranchName_2 = Convert.ToString(reader["BranchName_2"].ToString());
                            payBankBranchModel.BranchName_3 = Convert.ToString(reader["BranchName_3"].ToString());

                            payBankBranchList.Add(payBankBranchModel);
                        }
                    }
                }
                return payBankBranchList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PayCategoriesModel> GetPayCategoriesList()
        {
            SqlParameter[] parameters =
                    {

                    };
            // Create a list to hold the Absent		
            List<PayCategoriesModel> payCategoriesList = new List<PayCategoriesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GEN_uspGetPayCategories", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PayCategoriesModel payCategoriesModel;
                        while (reader.Read())
                        {
                            payCategoriesModel = new PayCategoriesModel();
                            payCategoriesModel.CategoryID = Convert.ToInt32(reader["CategoryID"].ToString());
                            payCategoriesModel.CategoryName_1 = Convert.ToString(reader["CategoryName_1"].ToString());
                            payCategoriesModel.CategoryName_2 = Convert.ToString(reader["CategoryName_2"].ToString());
                            payCategoriesModel.CategoryName_3 = Convert.ToString(reader["CategoryName_3"].ToString());
                            payCategoriesModel.seq = Convert.ToInt32(reader["seq"]);                            
                            payCategoriesModel.isDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                            payCategoriesList.Add(payCategoriesModel);
                        }
                    }
                }
                return payCategoriesList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public PayCategoriesModel GetPayCategoryDetailsByID(int PayCategoryID)
        {            
            SqlParameter[] parameters =
                   {
                     new SqlParameter("@PayCategoryID",PayCategoryID)
                    };
            // Create a list to hold the Absent		
            PayCategoriesModel payCategoriesModel = new PayCategoriesModel();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_GetPayCaegoryDetailsByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list                       
                        while (reader.Read())
                        {
                            payCategoriesModel = new PayCategoriesModel();
                            payCategoriesModel.CategoryID = Convert.ToInt32(reader["CategoryID"].ToString());
                            payCategoriesModel.CategoryName_1 = Convert.ToString(reader["CategoryName_1"].ToString());
                            payCategoriesModel.CategoryName_2 = Convert.ToString(reader["CategoryName_2"].ToString());
                            payCategoriesModel.CategoryName_3 = Convert.ToString(reader["CategoryName_3"].ToString());
                            payCategoriesModel.seq = Convert.ToInt32(reader["seq"]);                           
                            payCategoriesModel.isDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());                           
                        }
                    }
                }
                return payCategoriesModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PayAccountTypesModel> GetPayAccountTypesList()
        {
            SqlParameter[] parameters =
                    {

                    };
            // Create a list to hold the Absent		
            List<PayAccountTypesModel> payAccountTypesList = new List<PayAccountTypesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GEN_uspGetPayAccountTypes", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        PayAccountTypesModel payAccountTypesModel;
                        while (reader.Read())
                        {
                            payAccountTypesModel = new PayAccountTypesModel();
                            payAccountTypesModel.AccountTypeID = Convert.ToInt32(reader["AccountTypeID"].ToString());
                            payAccountTypesModel.AccountTypeName_1 = Convert.ToString(reader["AccountTypeName_1"].ToString());
                            payAccountTypesModel.AccountTypeName_2 = Convert.ToString(reader["AccountTypeName_2"].ToString());
                            payAccountTypesModel.AccountTypeName_3 = Convert.ToString(reader["AccountTypeName_3"].ToString());
                            payAccountTypesModel.tr_date = Convert.ToString(reader["tr_date"]);
                            payAccountTypesList.Add(payAccountTypesModel);
                        }
                    }
                }
                return payAccountTypesList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public int GetIBanLength()
        {
            int IBanLength = 0;
            try
            {
                string query = "SELECT IBANNumberLength FROM dbo.HR_PayBanksSettings";
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, query))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IBanLength = Convert.ToInt32(reader["IBANNumberLength"].ToString() == "" ? "0" : reader["IBANNumberLength"].ToString());
                        }
                    }
                }
                return IBanLength;
            }
            catch (Exception exception)
            {
                return IBanLength;
            }
            finally
            {

            }
        }

        public List<PayBankGroup> GetBankgroupList()
        {
            List<PayBankGroup> PayBankGroupsList = new List<PayBankGroup>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPayBankGroupsDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    PayBankGroup payBankGroup;
                    while (reader.Read())
                    {
                        payBankGroup = new PayBankGroup();
                        payBankGroup.BankGroupID = Convert.ToInt32(reader["BankGroupID"].ToString());
                        payBankGroup.BankGroupName_1 = reader["BankGroupName_1"].ToString().Trim();
                        payBankGroup.BankGroupName_2 = reader["BankGroupName_2"].ToString().Trim();
                        payBankGroup.BankGroupName_3 = reader["BankGroupName_3"].ToString().Trim();
                        payBankGroup.SalaryPayableAccountCode = reader["SalaryPayableAccountCode"].ToString().Trim();
                        payBankGroup.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        payBankGroup.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                        PayBankGroupsList.Add(payBankGroup);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                // return new OperationDetails(false, "Error : while fetching AcademicModelList.", exception);
                throw;
            }
            finally
            {

            }
            return PayBankGroupsList;
        }

        public OperationDetails AddEditBankGroup(PayBankGroup payBankGroup, int trnMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateBankGroupDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", trnMode);
                sqlCommand.Parameters.AddWithValue("@BankGroupID", payBankGroup.BankGroupID);
                sqlCommand.Parameters.AddWithValue("@BankGroupName_1", payBankGroup.BankGroupName_1);
                sqlCommand.Parameters.AddWithValue("@BankGroupName_2", payBankGroup.BankGroupName_2);
                sqlCommand.Parameters.AddWithValue("@BankGroupName_3", payBankGroup.BankGroupName_2);
                sqlCommand.Parameters.AddWithValue("@SalaryPayableAccountCode", payBankGroup.SalaryPayableAccountCode);
                sqlCommand.Parameters.AddWithValue("@IsActive", payBankGroup.IsActive);
                sqlCommand.Parameters.AddWithValue("@IsDeleted", payBankGroup.IsDeleted);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting bank group details.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }

        public PayBankGroup GetBankGroupDetailsById(int BankGroupId)
        {
            SqlParameter[] parameters =
                    {
                      new SqlParameter("@BankGroupId",BankGroupId)
                    };

            PayBankGroup payBankGroup = new PayBankGroup();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetBankGroupDetailsById", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            payBankGroup = new PayBankGroup();
                            payBankGroup.BankGroupID = Convert.ToInt32(reader["BankGroupID"].ToString());
                            payBankGroup.BankGroupName_1 = reader["BankGroupName_1"].ToString().Trim();
                            payBankGroup.BankGroupName_2 = reader["BankGroupName_2"].ToString().Trim();
                            payBankGroup.BankGroupName_3 = reader["BankGroupName_3"].ToString().Trim();
                            payBankGroup.SalaryPayableAccountCode = reader["SalaryPayableAccountCode"].ToString().Trim();
                            payBankGroup.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                            payBankGroup.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());

                        }
                    }
                }
                return payBankGroup;
            }
            catch (Exception ex)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PayBanksModeModel> GetPayBanksModeList()
        {
            SqlParameter[] parameters =
                    {

                    };
            // Create a list to hold the Absent		
            List<PayBanksModeModel> bankModeList = new List<PayBanksModeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_STP_GetBankMode", parameters))
                {
                    if (reader.HasRows)
                    {
                        PayBanksModeModel payBanksModeModel;
                        while (reader.Read())
                        {
                            payBanksModeModel = new PayBanksModeModel();
                            payBanksModeModel.PayBanksModeID = Convert.ToInt32(reader["PayBanksModeID"].ToString());
                            payBanksModeModel.PayBanksModeName_1 = Convert.ToString(reader["PayBanksModeName_1"].ToString() == "" ? "" : reader["PayBanksModeName_1"].ToString());
                            payBanksModeModel.PayBanksModeName_2 = Convert.ToString(reader["PayBanksModeName_2"].ToString() == "" ? "" : reader["PayBanksModeName_2"].ToString());
                            payBanksModeModel.PayBanksModeName_3 = Convert.ToString(reader["PayBanksModeName_3"].ToString() == "" ? "" : reader["PayBanksModeName_3"].ToString());
                            payBanksModeModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value ? 0 : reader["IsActive"]);
                            bankModeList.Add(payBanksModeModel);
                        }
                    }
                }
                return bankModeList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
