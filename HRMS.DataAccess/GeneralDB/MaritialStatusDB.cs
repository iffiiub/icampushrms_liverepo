﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess.GeneralDB
{
    public class MaritialStatusDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        /// <summary>
        /// Get All Maritial Status
        /// </summary>
        /// <returns></returns>
        public List<MaritialStausModel> getAllMaritialStatus()
        {
            List<MaritialStausModel> maritialStausModelList = null;
            try
            {
                maritialStausModelList = new List<MaritialStausModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_AllMaritialStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    MaritialStausModel maritialStausModel;
                    while (sqlDataReader.Read())
                    {
                        maritialStausModel = new MaritialStausModel();

                        maritialStausModel.MaritalStatusID = Convert.ToInt32(sqlDataReader["MaritalStatusID"].ToString());
                        maritialStausModel.MaritalStatusName_1 = sqlDataReader["MaritalStatusName_1"] == DBNull.Value ? "" : sqlDataReader["MaritalStatusName_1"].ToString();                        
                        maritialStausModelList.Add(maritialStausModel);

                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return maritialStausModelList;

        }

        /// <summary>
        /// Get MAritial Status Detail By ID
        /// </summary>
        /// <param name="MaritalStatusID"></param>
        /// <returns></returns>
        public MaritialStausModel GetMaritalStatusByID(int MaritalStatusID)
        {
            MaritialStausModel maritialStausModel = new MaritialStausModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_MaritialStatusByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MaritalStatusID", MaritalStatusID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        maritialStausModel.MaritalStatusID = Convert.ToInt32(sqlDataReader["MaritalStatusID"].ToString());
                        maritialStausModel.MaritalStatusName_1 = sqlDataReader["MaritalStatusName_1"] == DBNull.Value ? "" : sqlDataReader["MaritalStatusName_1"].ToString();
                        

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return maritialStausModel;
        }

        public OperationDetails InsertMaritalStatus(MaritialStausModel MaritialStausModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@MaritalStatusName_1", MaritialStausModel.MaritalStatusName_1) ,
                       new SqlParameter("@MaritalStatusName_2", MaritialStausModel.MaritalStatusName_2) ,
                      new SqlParameter("@MaritalStatusName_3", MaritialStausModel .MaritalStatusName_3) 
                      
                    };
                int MaritialStausId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_GEN_MaritialStaus", parameters));
                return new OperationDetails(true, "MaritalStatus inserted successfully.", null, MaritialStausId);
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while inserting MaritialStaus.", exception);
                throw;
            }
            finally
            {

            }
        }

    }
}
