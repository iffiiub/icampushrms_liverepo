﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;

namespace HRMS.DataAccess.GeneralDB
{
    public class LanguageDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        /// <summary>
        /// Gel All Languages
        /// </summary>
        /// <returns></returns>
        public List<LanguageModel> GetAllLanguages()
        {
            List<LanguageModel> languageModelList = null;
            try
            {
                languageModelList = new List<LanguageModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_AllLanguages", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    LanguageModel languageModel;
                    while (sqlDataReader.Read())
                    {
                        languageModel = new LanguageModel();

                        languageModel.LanguageID = Convert.ToInt32(sqlDataReader["LanguageID"].ToString());
                        languageModel.LanguageName_1 = sqlDataReader["LanguageName_1"] == DBNull.Value ? "" : sqlDataReader["LanguageName_1"].ToString();
                        languageModel.LanguageShortName_1 = sqlDataReader["LanguageShortName_1"] == DBNull.Value ? "" : sqlDataReader["LanguageShortName_1"].ToString();
                        languageModelList.Add(languageModel);

                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return languageModelList;
            
        }

        /// <summary>
        /// Get Language Detail By LanguageID
        /// </summary>
        /// <param name="GenderID"></param>
        /// <returns></returns>
        public LanguageModel GetLanguageByID(int LanguageID)
        {
            LanguageModel languageModel = new LanguageModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_LanguageByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LanguageID", LanguageID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        languageModel.LanguageID = Convert.ToInt32(sqlDataReader["LanguageID"].ToString());
                        languageModel.LanguageName_1 = sqlDataReader["LanguageName_1"] == DBNull.Value ? "" : sqlDataReader["LanguageName_1"].ToString();
                        languageModel.LanguageShortName_1 = sqlDataReader["LanguageShortName_1"] == DBNull.Value ? "" : sqlDataReader["LanguageShortName_1"].ToString();

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return languageModel;
        }
    }
}
