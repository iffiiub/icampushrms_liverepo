﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using System.Data;


namespace HRMS.DataAccess.GeneralDB
{
    public class GenderDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get All Genders
        /// </summary>
        /// <returns></returns>
        public List<GenderModel> GetAllGenders()
        {
            List<GenderModel> genderModelList = null;
            try
            {
                genderModelList = new List<GenderModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_AllGenders", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    GenderModel genderModel;
                    while (sqlDataReader.Read())
                    {
                        genderModel = new GenderModel();

                        genderModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
                        genderModel.GenderName_1 = sqlDataReader["GenderName_1"] == DBNull.Value ? "" : sqlDataReader["GenderName_1"].ToString();
                        genderModel.GenderImagePath = sqlDataReader["GenderImagePath"] == DBNull.Value ? "" : sqlDataReader["GenderImagePath"].ToString();
                        genderModelList.Add(genderModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return genderModelList;
        }

        /// <summary>
        /// Get Gender Details by GenderID
        /// </summary>
        /// <param name="GenderID"></param>
        /// <returns></returns>
        public GenderModel GetGenderByID(int GenderID)
        {
            GenderModel genderModel = new GenderModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_GenderByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@GenderID", GenderID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {


                        genderModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
                        genderModel.GenderName_1 = sqlDataReader["GenderName_1"] == DBNull.Value ? "" : sqlDataReader["GenderName_1"].ToString();
                        genderModel.GenderImagePath = sqlDataReader["GenderImagePath"] == DBNull.Value ? "" : sqlDataReader["GenderImagePath"].ToString();

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return genderModel;
        }

        public List<Types> GetTypes(string SP)
        {
            List<Types> TypeList = new List<Types>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(SP, sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        Types objType = new Types();
                        objType.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objType.Text = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();
                        TypeList.Add(objType);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return TypeList;
        }

        public void AddType(Types ObjTypes, string SP)
        {

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(SP, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Name", ObjTypes.Text);
                if (ObjTypes.CityId != null && ObjTypes.CityId != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@CityId", ObjTypes.CityId);
                }
                if (SP == "stp_Add_GEN_MOLTitleName" || SP == "stp_Add_GEN_MOETitleName")
                {
                    sqlCommand.Parameters.AddWithValue("@FrName", ObjTypes.TextFrench);
                    sqlCommand.Parameters.AddWithValue("@ArName", ObjTypes.TextArabic);
                }
                if (SP == "stp_Gen_InsertStateForCountry")
                {                   
                    if (ObjTypes.CountryId != null && ObjTypes.CountryId != 0)
                    {
                        if (sqlCommand.Parameters.Contains("@CityId"))
                        {
                            sqlCommand.Parameters.Remove("@CityId");
                        }

                        sqlCommand.Parameters.AddWithValue("@CountryId", ObjTypes.CountryId);
                    }
                }

                if (SP == "stp_Gen_InsertCityForState")
                {

                    if (ObjTypes.CountryId != null && ObjTypes.CountryId != 0)
                    {
                        if (sqlCommand.Parameters.Contains("@CityId"))
                        {
                            sqlCommand.Parameters.Remove("@CityId");
                        }

                        sqlCommand.Parameters.AddWithValue("@CountryId", ObjTypes.CountryId);
                        sqlCommand.Parameters.AddWithValue("@StateID", ObjTypes.StateId);
                    }
                }
                if (SP == "stp_Gen_InsertCityForCountry")
                {
                    if (ObjTypes.TextArabic != null || ObjTypes.TextArabic != "" && ObjTypes.CityId != 0)
                    {
                        sqlCommand.Parameters.AddWithValue("@NameAR", ObjTypes.TextArabic);
                    }
                    if (ObjTypes.TextFrench != null || ObjTypes.TextFrench != "" && ObjTypes.CityId != 0)
                    {
                        sqlCommand.Parameters.AddWithValue("@NameFN", ObjTypes.TextFrench);
                    }
                    if (ObjTypes.CountryId != null && ObjTypes.CountryId != 0)
                    {
                        if (sqlCommand.Parameters.Contains("@CityId"))
                        {
                            sqlCommand.Parameters.Remove("@CityId");
                        }

                        sqlCommand.Parameters.AddWithValue("@CountryId", ObjTypes.CountryId);
                    }
                }
                if (SP == "stp_Gen_InsertBirthplaceForCountry")
                {

                    if (ObjTypes.CountryId != null && ObjTypes.CountryId != 0)
                    {
                        if (sqlCommand.Parameters.Contains("@CityId"))
                        {
                            sqlCommand.Parameters.Remove("@CityId");
                        }
                        sqlCommand.Parameters.AddWithValue("@FrName", ObjTypes.TextFrench);
                        sqlCommand.Parameters.AddWithValue("@ArName", ObjTypes.TextArabic);
                        sqlCommand.Parameters.AddWithValue("@CountryID", ObjTypes.CountryId);
                    }
                }
                if (SP == "stp_Add_Gen_LanguageName" || SP == "stp_Gen_InsertCityForState" || SP == "stp_Gen_InsertAreaForCity" || SP == "stp_Add_GEN_EmployeeSection"
                    || SP == "stp_Add_HR_EmploymentMode" || SP == "stp_Add_HR_JobCategoryName" || SP == "stp_Add_BankName" || SP == "stp_Add_BranchName"
                    || SP == "stp_Add_PayAccountTypesName" || SP == "Hr_Stp_Add_HR_JobTitleName" || SP == "stp_Add_MaritalStatusName" || SP == "stp_Add_ReligionName")
                {
                    sqlCommand.Parameters.AddWithValue("@FrName", ObjTypes.TextFrench);
                    sqlCommand.Parameters.AddWithValue("@ArName", ObjTypes.TextArabic);
                }
                if (SP == "Hr_Stp_Gen_InsertDepartment")
                {
                    sqlCommand.Parameters.AddWithValue("@companyId", ObjTypes.companyId);
                }
                if (SP == "stp_Add_HR_PositionName")
                {
                    sqlCommand.Parameters.AddWithValue("@positionTitle_2", ObjTypes.TextArabic);
                    sqlCommand.Parameters.AddWithValue("@positionTitle_3", ObjTypes.TextFrench);
                }
                if (SP == "stp_Add_GEN_EmployeeSection")
                {
                    sqlCommand.Parameters.AddWithValue("@DepartmentId", ObjTypes.DepartmentId);
                }
                if(SP == "stp_Add_HR_InsuranceEligibility")
                {
                    sqlCommand.Parameters.AddWithValue("@UserId", ObjTypes.CreatedBy);
                }
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

        }


    }
}
