﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;


namespace HRMS.DataAccess.GeneralDB
{
    public class IssuePlaceDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get All Genders
        /// </summary>
        /// <returns></returns>
        public List<IssuePlaceModel> GetAllIssuePlace()
        {
            List<IssuePlaceModel> IssuePlaceList = null;
            try
            {
                IssuePlaceList = new List<IssuePlaceModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_AllIssuePlace", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;   

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    IssuePlaceModel IssuePlaceModel;
                    while (sqlDataReader.Read())
                    {
                        IssuePlaceModel = new IssuePlaceModel();

                        IssuePlaceModel.IssuePlaceID = Convert.ToInt32(sqlDataReader["IssuePlaceID"].ToString());
                        IssuePlaceModel.IssuePlaceName = sqlDataReader["IssuePlaceName"] == DBNull.Value ? "" : sqlDataReader["IssuePlaceName"].ToString();
                          IssuePlaceList.Add(IssuePlaceModel);
                    }
                }
                sqlConnection.Close();

                

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IssuePlaceList;
        }

        public IssuePlaceModel GetIssuePlaceByID(int IssuePlaceId)
        {
            IssuePlaceModel IssuePlaceModel = new IssuePlaceModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_IssuePlaceByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@IssuePlaceID", IssuePlaceId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        IssuePlaceModel.IssuePlaceID = Convert.ToInt32(sqlDataReader["IssuePlaceID"].ToString());
                        IssuePlaceModel.IssuePlaceName = sqlDataReader["IssuePlaceName"] == DBNull.Value ? "" : sqlDataReader["IssuePlaceName"].ToString();

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IssuePlaceModel;
        }
    }
}
