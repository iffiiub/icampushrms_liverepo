﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;


namespace HRMS.DataAccess.GeneralDB
{
    public class StatusDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        ///// <summary>
        ///// Get All Genders
        ///// </summary>
        ///// <returns></returns>
        //public List<StatusModel> GetAllGenders()
        //{
        //    List<StatusModel> StatusModelList = null;
        //    try
        //    {
        //        StatusModelList = new List<StatusModel>();

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_GEN_Get_AllGenders", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;   

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {
        //            StatusModel StatusModel;
        //            while (sqlDataReader.Read())
        //            {
        //                StatusModel = new StatusModel();

        //                StatusModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
        //                StatusModel.GenderName_1 = sqlDataReader["GenderName_1"] == DBNull.Value ? "" : sqlDataReader["GenderName_1"].ToString();
        //                StatusModel.GenderImagePath = sqlDataReader["GenderImagePath"] == DBNull.Value ? "" : sqlDataReader["GenderImagePath"].ToString();
        //                StatusModelList.Add(StatusModel);
        //            }
        //        }
        //        sqlConnection.Close();

                

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return StatusModelList;
        //}

        ///// <summary>
        ///// Get Gender Details by GenderID
        ///// </summary>
        ///// <param name="GenderID"></param>
        ///// <returns></returns>
        //public StatusModel GetGenderByID(int GenderID)
        //{
        //    StatusModel StatusModel = new StatusModel();
        //    try
        //    {
                
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_GEN_Get_GenderByID", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //        sqlCommand.Parameters.AddWithValue("@GenderID", GenderID);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {
                    
        //            while (sqlDataReader.Read())
        //            {
                        

        //                StatusModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
        //                StatusModel.GenderName_1 = sqlDataReader["GenderName_1"] == DBNull.Value ? "" : sqlDataReader["GenderName_1"].ToString();
        //                StatusModel.GenderImagePath = sqlDataReader["GenderImagePath"] == DBNull.Value ? "" : sqlDataReader["GenderImagePath"].ToString();
                        
        //            }
        //        }
        //        sqlConnection.Close();



        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return StatusModel;
        //}

        //public List<Types> GetTypes(string SP)
        //{
        //    List<Types> TypeList = new List<Types>();
        //    try
        //    {

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand(SP, sqlConnection);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {

        //            while (sqlDataReader.Read())
        //            {

        //                Types objType = new Types();
        //                objType.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
        //                objType.Text = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();

        //                TypeList.Add(objType);
        //            }
        //        }
        //        sqlConnection.Close();



        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return TypeList;


        //}

        //public OperationDetails AddCourse(CourseModel CourseModel)
        //{
        //    try
        //    {
        //        SqlParameter[] parameters =
        //            {    
        //              new SqlParameter("@CourseName", CourseModel.CourseName) ,
                        
                      
        //            };
        //        int CourseId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_GEN_Course", parameters));
        //        return new OperationDetails(true, "Course inserted successfully.", null, CourseId);
        //    }
        //    catch (Exception exception)
        //    {
        //        // return new OperationDetails(false, "Error : while inserting MaritialStaus.", exception);
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}


        public List<StatusModel> GetAllStatus()
        {
            List<StatusModel> StatusModelList = null;
            try
            {
                StatusModelList = new List<StatusModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_Status", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    StatusModel StatusModel;
                    while (sqlDataReader.Read())
                    {
                        StatusModel = new StatusModel();





                        StatusModel.StatusID = Convert.ToInt32(sqlDataReader["StatusID"] == DBNull.Value ? "0" : sqlDataReader["StatusID"].ToString());
                        StatusModel.StatusName = sqlDataReader["StatusName"] == DBNull.Value ? "" : sqlDataReader["StatusName"].ToString();
                         StatusModelList.Add(StatusModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return StatusModelList;
        }
    }
}
