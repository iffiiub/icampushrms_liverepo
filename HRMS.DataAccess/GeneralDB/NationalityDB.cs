﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;

namespace HRMS.DataAccess.GeneralDB
{
    public class NationalityDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get All Nationalities details
        /// </summary>
        /// <returns></returns>
        public List<NationalityModel> getAllNationalities()
        {
            List<NationalityModel> nationalityModelList = null;
            try
            {
                nationalityModelList = new List<NationalityModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_AllNationalities", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    NationalityModel nationalityModel;
                    while (sqlDataReader.Read())
                    {
                        nationalityModel = new NationalityModel();

                        nationalityModel.NationalityID = Convert.ToInt32(sqlDataReader["NationalityID"].ToString());
                        nationalityModel.NationalityName_1 = sqlDataReader["NationalityName_1"] == DBNull.Value ? "" : sqlDataReader["NationalityName_1"].ToString();
                        nationalityModelList.Add(nationalityModel);

                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nationalityModelList;

        }

        /// <summary>
        /// Get Nationality By NationalityID
        /// </summary>
        /// <param name="MaritalStatusID"></param>
        /// <returns></returns>
        public NationalityModel GetNationalityByID(int NationalityID)
        {
            NationalityModel nationalityModel = new NationalityModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_NationalityByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@NationalityID", NationalityID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        nationalityModel.NationalityID = Convert.ToInt32(sqlDataReader["NationalityID"].ToString());
                        nationalityModel.NationalityName_1 = sqlDataReader["NationalityName_1"] == DBNull.Value ? "" : sqlDataReader["NationalityName_1"].ToString();

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nationalityModel;
        }
        public List<NationalityCountModel> GetEmployeeNationalityCount(int companyId)
        {
            List<NationalityCountModel> nationalityCountModelList = new List<NationalityCountModel>();
            NationalityCountModel nationalityCountModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeCountByNationality", sqlConnection);                
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        nationalityCountModel = new NationalityCountModel();
                        nationalityCountModel.NationalityCount = Convert.ToInt32(sqlDataReader["TotalEmployee"].ToString());
                        nationalityCountModel.NationalityId = Convert.ToInt32(sqlDataReader["CountryId"].ToString());
                        nationalityCountModel.NationalityName = sqlDataReader["CountryName"].ToString();
                        nationalityCountModelList.Add(nationalityCountModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nationalityCountModelList;
        }

        public List<NationalityCountModel> GetEmployeeNationalityCountWithFilters(int? departmentId, int? MOLTitleID, int? MOETitleID,int UserId)
        {
            List<NationalityCountModel> nationalityCountModelList = new List<NationalityCountModel>();
            NationalityCountModel nationalityCountModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeCountByNationalityWithFilters", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", departmentId);
                sqlCommand.Parameters.AddWithValue("@MolTitleID", MOLTitleID);
                sqlCommand.Parameters.AddWithValue("@MoeTitleID", MOETitleID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        nationalityCountModel = new NationalityCountModel();
                        nationalityCountModel.NationalityCount = Convert.ToInt32(sqlDataReader["TotalEmployee"].ToString());
                        nationalityCountModel.NationalityId = Convert.ToInt32(sqlDataReader["CountryId"].ToString());
                        nationalityCountModel.NationalityName = sqlDataReader["CountryName"].ToString();
                        nationalityCountModelList.Add(nationalityCountModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nationalityCountModelList;
        }
        public List<NationalityTypeModel> GetNationalityTypeList()
        {
            List<NationalityTypeModel> nationalityTypeModelList = new List<NationalityTypeModel>();
            NationalityTypeModel nationalityTypeModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetNationalityTypes", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        nationalityTypeModel = new NationalityTypeModel();
                        nationalityTypeModel.NationalityTypeId = Convert.ToInt32(sqlDataReader["NationalityTypeId"].ToString());
                        nationalityTypeModel.NationalityType = sqlDataReader["NationalityType"].ToString();
                        nationalityTypeModel.NationalityTypeDesc = sqlDataReader["NationalityTypeDesc"].ToString();
                        nationalityTypeModelList.Add(nationalityTypeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nationalityTypeModelList;
        }

    }
}
