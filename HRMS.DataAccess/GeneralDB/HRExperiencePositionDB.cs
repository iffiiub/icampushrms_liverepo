﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HRMS.DataAccess.GeneralDB
{
    public class HRExperiencePositionDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<HRExperiencePositionModel> GetAllHRExperiencePosition()
        {
            List<HRExperiencePositionModel> HRExperiencePositionList = null;
            try
            {
                HRExperiencePositionList = new List<HRExperiencePositionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_HRExperiencePosition", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HRExperiencePositionModel HRExperiencePositionModel;
                    while (sqlDataReader.Read())
                    {
                        HRExperiencePositionModel = new HRExperiencePositionModel();

                        HRExperiencePositionModel.HRExperiencePositionID = Convert.ToInt32(sqlDataReader["HRExperiencePositionID"].ToString());
                        HRExperiencePositionModel.HRExperiencePositionName_1 = sqlDataReader["HRExperiencePositionName_1"] == DBNull.Value ? "" : sqlDataReader["HRExperiencePositionName_1"].ToString();
                        HRExperiencePositionList.Add(HRExperiencePositionModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return HRExperiencePositionList;
        }

        public OperationDetails InsertHRExperiencePosition(HRExperiencePositionModel HRExperiencePositionModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@HRExperiencePositionName_1",HRExperiencePositionModel.HRExperiencePositionName_1) ,
                      new SqlParameter("@HRExperiencePositionName_2", HRExperiencePositionModel.HRExperiencePositionName_2) ,
                      new SqlParameter("@HRExperiencePositionName_3", HRExperiencePositionModel.HRExperiencePositionName_3)
                    };
                int HRExperiencePositionId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_GEN_HRExperiencePosition", parameters));
                return new OperationDetails(true, "HRExperiencePosition saved successfully.", null, HRExperiencePositionId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting HRExperiencePosition.", exception);
                //throw;
            }
            finally
            {

            }
        }
    }
}
