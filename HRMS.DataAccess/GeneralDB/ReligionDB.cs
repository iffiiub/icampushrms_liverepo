﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;


namespace HRMS.DataAccess.GeneralDB
{
    public class ReligionDB : DBHelper
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get All Genders
        /// </summary>
        /// <returns></returns>
        public List<ReligionModel> GetAllReligion()
        {
            List<ReligionModel> ReligionList = null;
            try
            {
                ReligionList = new List<ReligionModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_Religion", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;   

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ReligionModel ReligionModel;
                    while (sqlDataReader.Read())
                    {
                        ReligionModel = new ReligionModel();

                        ReligionModel.ReligionID = Convert.ToInt32(sqlDataReader["ReligionID"].ToString());
                        ReligionModel.ReligionName_1 = sqlDataReader["ReligionName_1"] == DBNull.Value ? "" : sqlDataReader["ReligionName_1"].ToString();
                          ReligionList.Add(ReligionModel);
                    }
                }
                sqlConnection.Close();

                

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ReligionList;
        }

        public OperationDetails InsertReligion(ReligionModel ReligionModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@ReligionName_1",ReligionModel.ReligionName_1) ,
                      new SqlParameter("@ReligionName_2", ReligionModel.ReligionName_2) ,
                      new SqlParameter("@ReligionName_3", ReligionModel.ReligionName_3) ,
                      new SqlParameter("@ReligionCategoryID", ReligionModel.ReligionCategoryID),
                      new SqlParameter("@isIslam", ReligionModel.isIslam ) ,
                      new SqlParameter("@isChristian",ReligionModel.isChristian) 
                    };
                int ReligionId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_GEN_Religion", parameters));
                return new OperationDetails(true, "Religion saved successfully.", null, ReligionId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Religion.", exception);
                //throw;
            }
            finally
            {

            }
        }
    }
}
