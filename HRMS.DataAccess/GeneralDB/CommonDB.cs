﻿using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace HRMS.DataAccess.GeneralDB
{
    public class CommonDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }

        GeneralDB.DataHelper dataHelper;
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public Dictionary<string, int> weekDays { get; set; }
        public Dictionary<int, string> weekDaysArabic { get; set; }
        public Dictionary<int, string> AbsentStatus;
        public Dictionary<int, string> WorkEmailFirstOption;
        public Dictionary<int, string> WorkEmailSecondOption;
        public Dictionary<int, string> WorkEmailThirdOption;
        public Dictionary<int, string> PaidType;
        private Dictionary<int, string> MonthsInArabic { get; set; }
        private Dictionary<int, string> monthsInEnglish { get; set; }
        public static string HRMSDateFormat
        {
            get { return ConfigurationManager.AppSettings["HRMSDateFormat"]; }
        }
        public CommonDB()
        {
            weekDays = new Dictionary<string, int>();
            weekDays.Add("Tuesday", 1);
            weekDays.Add("Wednesday", 2);
            weekDays.Add("Thursday", 3);
            weekDays.Add("Friday", 4);
            weekDays.Add("Saturday", 5);
            weekDays.Add("Sunday", 6);
            weekDays.Add("Monday", 7);

            weekDaysArabic = new Dictionary<int, string>();
            weekDaysArabic.Add(1, "يَوم الثلاثاء");
            weekDaysArabic.Add(2, "يَوم الأربعاء");
            weekDaysArabic.Add(3, "يَوم الخميس");
            weekDaysArabic.Add(4, "يَوم الجمعة");
            weekDaysArabic.Add(5, "يَوم السبت");
            weekDaysArabic.Add(6, "يَوم الأحَد");
            weekDaysArabic.Add(7, "يَوم الإثنين");

            AbsentStatus = new Dictionary<int, string>();
            AbsentStatus.Add(0, "Didn't Arrive");
            AbsentStatus.Add(1, "On Time");
            AbsentStatus.Add(2, "Absent");
            AbsentStatus.Add(3, "Late");
            AbsentStatus.Add(4, "Early");
            AbsentStatus.Add(5, "Hoilday");
            AbsentStatus.Add(6, "Vacation");
            AbsentStatus.Add(7, "Off Day");
            AbsentStatus.Add(8, "Late&Early");
            AbsentStatus.Add(9, "Didn't Checkout");
            AbsentStatus.Add(10, "Extra Day");
            AbsentStatus.Add(11, "Didn't Sign In");
            AbsentStatus.Add(12, "Late & Didn't Sign Out");

            WorkEmailFirstOption = new Dictionary<int, string>();
            WorkEmailFirstOption.Add(0, "");
            WorkEmailFirstOption.Add(1, "FirstName_1");
            WorkEmailFirstOption.Add(2, "LEFT(FirstName_1,1)");


            WorkEmailSecondOption = new Dictionary<int, string>();
            WorkEmailSecondOption.Add(0, "");
            WorkEmailSecondOption.Add(1, ".");
            WorkEmailSecondOption.Add(2, "_");

            WorkEmailThirdOption = new Dictionary<int, string>();
            WorkEmailThirdOption.Add(0, "");
            WorkEmailThirdOption.Add(1, "Surname_1");
            WorkEmailThirdOption.Add(2, "LEFT(Surname_1,1)");
            WorkEmailThirdOption.Add(3, "Surname_1+EmployeeAlternativeId");

            PaidType = new Dictionary<int, string>();
            PaidType.Add(1, "Not Paid");
            PaidType.Add(2, "Paid");
            PaidType.Add(3, "Half Paid");
        }

        public static string GetFormattedDate_DDMMYYYY(string sDate)
        {
            DateTime dateValue;
            try
            {
                if (string.IsNullOrEmpty(sDate))
                {
                    return "";
                }
                else
                {
                    if (DateTime.TryParse(sDate, out dateValue))
                    {
                        return (dateValue.ToString(HRMSDateFormat, CultureInfo.CreateSpecificCulture("en-GB")));
                    }
                    else
                        return (DateTime.Parse(sDate, CultureInfo.InvariantCulture).ToString(HRMSDateFormat, CultureInfo.CreateSpecificCulture("en-GB")));

                }
            }
            catch
            {
                return (DateTime.Parse(sDate).ToString(HRMSDateFormat, CultureInfo.CreateSpecificCulture("en-GB")));

            }
        }

        public static object SetCulturedDate(string sDate)
        {
            try
            {
                if (string.IsNullOrEmpty(sDate))
                {
                    return DBNull.Value;
                }
                else
                {
                    return DateTime.Parse(sDate, new CultureInfo("en-GB"));
                }
            }
            catch (Exception e)
            {
                return DBNull.Value;
            }
        }

        public static int GetUserId(UserContextViewModel model)
        {
            if (model.UserRolls.Contains("admin") || model.UserRolls.Contains("Admin"))
                return 0;
            else
                return model.UserId;
        }

        public static List<PickList> GetApplicableType()
        {
            List<PickList> list = new List<PickList>()
            {
                new PickList() {id=0,text="Days" },
                new PickList() {id=1,text="Months" }
            };
            return list;
        }

        public static List<PickList> GetTimeSpans()
        {
            List<PickList> list = new List<PickList>()
            {
                new PickList() {id=0,text="Days" },
                new PickList() {id=1,text="Months" },
                new PickList() {id=2,text="year" }
            };
            return list;
        }

        public static List<PickList> GetAccumulateRoundingType()
        {
            List<PickList> list = new List<PickList>()
            {
                new PickList() {id=-1,text="No rounding" },
                new PickList() {id=0,text="Default" },
                new PickList() {id=1,text="Round Up" },
                new PickList() {id=2,text="Round Down" }
            };
            return list;
        }

        public static List<PickList> GetDisplayLeaveBalanceType()
        {
            List<PickList> list = new List<PickList>()
            {
                new PickList() {id=0,text="1 Digit After Decimal" },
                new PickList() {id=1,text="2 Digits After Decimal" },
                new PickList() {id=2,text="3 Digits After Decimal" }
            };
            return list;
        }

        public static List<PickList> GetBoolDDL()
        {
            List<PickList> list = new List<PickList>()
            {
                new PickList() {id=0,text="No",dataFeild1="false" },
                new PickList() {id=1,text="Yes",dataFeild1="true" }
            };
            return list;
        }

        private static string[] _ones =
         {
            "Zero",
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine"
         };

        private static string[] _teens =
        {
            "Ten",
            "Eleven",
            "Twelve",
            "Thirteen",
            "Fourteen",
            "Fifteen",
            "Sixteen",
            "Seventeen",
            "Eighteen",
            "Nineteen"
        };

        private static string[] _tens =
        {
            "",
            "Ten",
            "Twenty",
            "Thirty",
            "Forty",
            "Fifty",
            "Sixty",
            "Seventy",
            "Eighty",
            "Ninety"
        };

        // US Nnumbering:
        private static string[] _thousands =
        {
            "",
            "Thousand",
            "Lac",
            "Million",
            "Billion",
            "Trillion",
            "Quadrillion"
        };

        /// <summary>
        /// Converts a numeric value to words suitable for the portion of
        /// a check that writes out the amount.
        /// </summary>
        /// <param name="value">Value to be converted</param>
        /// <returns></returns>
        public static string ConverttoWord(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            // Use StringBuilder to build result
            StringBuilder builder = new StringBuilder();
            // Convert integer portion of value to string
            digits = ((long)value).ToString();
            // Traverse characters in reverse order
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                // Determine if ones, tens, or hundreds column
                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            // First digit in number (last in loop)
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            // This digit is part of "teen" value
                            temp = String.Format("{0} ", _teens[ndigit]);
                            // Skip tens position
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            // Any non-zero digit
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            // This digit is zero. If digit in tens and hundreds
                            // column are also zero, don't show "thousands"
                            temp = String.Empty;
                            // Test for non-zero digit in this grouping
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        // Show "thousands" if non-zero in grouping
                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : ", ");
                            }
                            // Indicate non-zero digit encountered
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? "-" : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            // Capitalize first letter
            return String.Format("{0}{1}",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }

        public static string DecimalToWord(decimal amount)
        {
            string orignalAmount = "";
            orignalAmount = NumberToWords(Convert.ToInt32(amount));
            if (amount.ToString().Split('.').Count() > 1)
            {
                orignalAmount = orignalAmount + " And " + NumberToWords(Convert.ToInt32(amount.ToString().Split('.')[1]));
            }
            return orignalAmount;
        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000) + " Lacs ";
                number %= 100000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)

                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }
            return words;
        }

        public static bool ExecuteQueryString(string query)
        {
            DataAccess.GeneralDB.DataHelper dataHelper = new DataHelper();

            return dataHelper.SimpleCommandText(query);

        }

        public static object GetPropertyValue(object src, string propName)
        {

            if (propName.Contains("."))//complex type nested
            {
                var temp = propName.Split(new char[] { '.' }, 2);
                return GetPropertyValue(GetPropertyValue(src, temp[0]), temp[1]);
            }
            else
            {
                var prop = src.GetType().GetProperty(propName);
                return prop != null ? prop.GetValue(src, null) : null;
            }
        }

        public static string FindBroserName(string browserName)
        {
            try
            {
                if (browserName.ToLower().Contains("chrome"))
                    return "Chrome";
                if (browserName.ToLower().Contains("firefox"))
                    return "Mozila Firfox";
                if (browserName.ToLower().Contains("internetexplorer"))
                    return "Internet Explorer";

            }
            catch
            {

            }
            return browserName;
        }

        public static int CalculateDayDifference(DateTime fromdate, DateTime todate)
        {
            return Convert.ToInt32((todate - fromdate).TotalDays);
        }

        public static int FindDayBetweenDates(DateTime fromDate, DateTime toDate, string dayString)
        {
            int daysdiffrence = (int)((toDate - fromDate).TotalDays) + 1;
            DateTime currentDate = DateTime.Now;
            int dayCounter = 0;
            for (int i = 0; i <= daysdiffrence; i++)
            {
                currentDate = fromDate.AddDays(i);
                dayCounter = (currentDate.DayOfWeek.ToString().ToLower() == dayString.ToLower() ? dayCounter + 1 : dayCounter);
            }
            return dayCounter;
        }
        public static bool CheckDayInDate(DateTime currentDate, string dayString)
        {
            return (currentDate.DayOfWeek.ToString().ToLower() == dayString.ToLower() ? true : false);
        }

        public static string RemoveSpecialCharacter(string inputString, string specialCharacter)
        {
            if (specialCharacter.Length > 0)
            {
                List<string> specialCharacterList = specialCharacter.Split(',').ToList();
                foreach (string item in specialCharacterList)
                {
                    inputString = inputString.Replace(item, "");
                }
            }
            return inputString;
        }

        public static Dictionary<int, string> GetSortingList()
        {
            Dictionary<int, string> sortingList = new Dictionary<int, string>();
            sortingList.Add(1, "Employee Name Asc");
            sortingList.Add(2, "Employee Name Desc");
            sortingList.Add(3, "Employee ID Asc");
            sortingList.Add(4, "Employee ID Desc");
            return sortingList;
        }
        public static string FilterString(string str)
        {
            string[] stringArray = new string[] { "N/A" };
            if (stringArray.Contains(str))
                return "";
            else
                return str;
        }

        public List<EmployeeHireDateInformation> GetEmpoyeeHireDateIsLessThanDate(List<EmployeeHireDateInformation> employeeList, DateTime date)
        {
            List<EmployeeHireDateInformation> employeeSortedList = new List<EmployeeHireDateInformation>();
            EmployeeDB employeeDb = new EmployeeDB();
            foreach (var item in employeeList)
            {
                if (!employeeDb.checkHireDateIsGreater(date, item.HireDate))
                {
                    employeeSortedList.Add(item);
                }
            }
            return employeeSortedList;
        }

        public List<ValidationSetting> GetValidationSetting()
        {
            DataHelper objHelper = new DataHelper();
            DataTable dt = objHelper.ExcuteCommandText("Select * from HR_ValidationSetting");
            List<ValidationSetting> lstValidationSetting = new List<ValidationSetting>();
            ValidationSetting objSetting;
            foreach (DataRow row in dt.Rows)
            {
                objSetting = new ValidationSetting();
                objSetting.ValidationSettingID = Convert.ToInt32(row["ValidationSettingId"].ToString());
                objSetting.ValidationKey = row["ValidationKey"].ToString();
                objSetting.IsMandatory = Convert.ToBoolean(row["IsMandatory"].ToString() == "" ? "false" : row["IsMandatory"].ToString());
                lstValidationSetting.Add(objSetting);
            }
            return lstValidationSetting;
        }

        public OperationDetails UpdateValidationSetting(int ValidationSettingID, bool isChecked)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateValidationSetting", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values                
                sqlCommand.Parameters.AddWithValue("@ValidationSettingId", ValidationSettingID);
                sqlCommand.Parameters.AddWithValue("@IsChecked", isChecked);
                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Validation setting updated successfully";
                }
                else
                {
                    op.Message = "Error while updating validation setting";
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while updating validation setting ";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public ValidationSetting GetValidationSettingByName(string validationKey)
        {
            DataHelper objHelper = new DataHelper();
            DataTable dt = objHelper.ExcuteCommandText("select * from Hr_ValidationSetting where ValidationKey like '" + validationKey + "'");
            ValidationSetting objSetting = new ValidationSetting();
            foreach (DataRow row in dt.Rows)
            {
                objSetting.ValidationSettingID = Convert.ToInt32(row["ValidationSettingId"].ToString());
                objSetting.ValidationKey = row["ValidationKey"].ToString();
                objSetting.IsMandatory = Convert.ToBoolean(row["IsMandatory"].ToString() == "" ? "false" : row["IsMandatory"].ToString());
            }
            return objSetting;
        }

        public OperationDetails UpdateSMTPSetting(SMTPConfig objSMTP, int OpType)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateSMTPConfig", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values                
                sqlCommand.Parameters.AddWithValue("@FromEmailAddress", objSMTP.FromEmailAddress);
                sqlCommand.Parameters.AddWithValue("@FromDisplayName", objSMTP.FromDisplayName);
                sqlCommand.Parameters.AddWithValue("@ReplyMail", objSMTP.ReplyEmail);
                sqlCommand.Parameters.AddWithValue("@SMTPServer", objSMTP.SMTPServer);
                sqlCommand.Parameters.AddWithValue("@PortNumber", objSMTP.PortNumber);
                sqlCommand.Parameters.AddWithValue("@SecureConnection", objSMTP.SecureConnection);
                sqlCommand.Parameters.AddWithValue("@Username", objSMTP.Username);
                sqlCommand.Parameters.AddWithValue("@password", objSMTP.Password);
                sqlCommand.Parameters.AddWithValue("@OpType", OpType);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                op.Success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (op.Success)
                {
                    if (OpType == 1)
                    {
                        op.CssClass = "success";
                        op.Message = "SMTP cofiguration updated successfully";
                    }
                    else
                    {

                        op.CssClass = "success";
                        op.Message = "SMTP cofiguration deleted successfully";
                    }
                }
                else
                {
                    op.Message = "Error while updating SMTP cofiguration";
                    op.CssClass = "error";
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                op.Message = "Error while updating SMTP cofiguration";
                op.Success = false;
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return op;
        }

        public SMTPConfig GetSMTPConfig()
        {
            SMTPConfig objSMTPConfig = new SMTPConfig();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSmtpConfiguration", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        objSMTPConfig = new SMTPConfig();
                        objSMTPConfig.FromEmailAddress = sqlDataReader["FromEmailAddress"] == DBNull.Value ? "" : sqlDataReader["FromEmailAddress"].ToString();
                        objSMTPConfig.FromDisplayName = sqlDataReader["FromDisplayName"] == DBNull.Value ? "" : sqlDataReader["FromDisplayName"].ToString();
                        objSMTPConfig.ReplyEmail = sqlDataReader["ReplyEmail"] == DBNull.Value ? "" : sqlDataReader["ReplyEmail"].ToString();
                        objSMTPConfig.SMTPServer = sqlDataReader["SMTPServer"] == DBNull.Value ? "" : sqlDataReader["SMTPServer"].ToString();
                        objSMTPConfig.PortNumber = Convert.ToInt32(sqlDataReader["PortNumber"] == DBNull.Value ? "0" : sqlDataReader["PortNumber"].ToString());
                        objSMTPConfig.Username = sqlDataReader["Username"] == DBNull.Value ? "" : sqlDataReader["Username"].ToString();
                        objSMTPConfig.Password = sqlDataReader["Password"] == DBNull.Value ? "" : sqlDataReader["Password"].ToString();
                        objSMTPConfig.SecureConnection = Convert.ToBoolean(sqlDataReader["SecureConnection"] == DBNull.Value ? "false" : sqlDataReader["SecureConnection"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return objSMTPConfig;
        }

        public string GetHRMSVersion()
        {
            string HRMSVersion = "";
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_HRVersion", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        HRMSVersion = sqlDataReader["HRVersion"] == DBNull.Value ? "" : sqlDataReader["HRVersion"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return HRMSVersion;
        }


        public Dictionary<int, string> GetEnglishMonth()
        {
            monthsInEnglish = new Dictionary<int, string>();
            monthsInEnglish.Add(1, "January");
            monthsInEnglish.Add(2, "February");
            monthsInEnglish.Add(3, "March");
            monthsInEnglish.Add(4, "April");
            monthsInEnglish.Add(5, "May");
            monthsInEnglish.Add(6, "June");
            monthsInEnglish.Add(7, "July");
            monthsInEnglish.Add(8, "August");
            monthsInEnglish.Add(9, "September");
            monthsInEnglish.Add(10, "October");
            monthsInEnglish.Add(11, "November");
            monthsInEnglish.Add(12, "December");
            return monthsInEnglish;
        }
        public Dictionary<int, string> GetArabicMonth()
        {
            MonthsInArabic = new Dictionary<int, string>();
            MonthsInArabic.Add(1, "كانون الثاني");
            MonthsInArabic.Add(2, "شباط");
            MonthsInArabic.Add(3, "آذار");
            MonthsInArabic.Add(4, "نيسان");
            MonthsInArabic.Add(5, "أيار");
            MonthsInArabic.Add(6, "حزيران");
            MonthsInArabic.Add(7, "تموز");
            MonthsInArabic.Add(8, "آب");
            MonthsInArabic.Add(9, "أيلول");
            MonthsInArabic.Add(10, "تشرين الأول");
            MonthsInArabic.Add(11, "تشرين الثاني");
            MonthsInArabic.Add(12, "كانون الأول");
            return MonthsInArabic;
        }
        //*** F7
        public SqlDataReader ExecuteProcedure(string ProcedureName, SqlParameter[] sqlParameters)
        {
            SqlDataReader reader;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(ProcedureName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                foreach (SqlParameter Param in sqlParameters)
                {
                    sqlCommand.Parameters.Add(Param);
                }


                reader = sqlCommand.ExecuteReader();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                //if (sqlReader != null)
                //{
                //    sqlReader.Close();
                //}
                //if (sqlConnection != null)
                //{
                //    sqlConnection.Close();
                //}
            }

            return reader;
        }
        public List<XMLLanguages> GetLanguageList(int? LanguageID)
        {

            List<XMLLanguages> PDRPLanguageList = new List<XMLLanguages>();
            XMLLanguages PDRPLanguage;

            try
            {

                SqlParameter[] sqlParameters = null;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlDataReader reader;
                string ProcedureName = "HR_stp_PDRPGetLanguageList";
                sqlCommand = new SqlCommand(ProcedureName, sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (LanguageID != null)
                {
                    sqlParameters = new SqlParameter[1];

                    SqlParameter sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = "@LanguageID";
                    sqlParameter.Value = LanguageID;
                    sqlParameters[0] = sqlParameter;
                    foreach (SqlParameter Param in sqlParameters)
                    {
                        sqlCommand.Parameters.Add(Param);
                    }

                }






                reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        PDRPLanguage = new XMLLanguages();

                        PDRPLanguage.LanguageID = Convert.ToInt32(reader["LanguageID"] == DBNull.Value ? 0 : reader["LanguageID"]);
                        PDRPLanguage.LanguageName = Convert.ToString(reader["LanguageName"] == DBNull.Value ? "" : reader["LanguageName"]);
                        PDRPLanguage.LanguageFaFlag = Convert.ToString(reader["LanguageFaFlag"] == DBNull.Value ? "" : reader["LanguageFaFlag"]);
                        PDRPLanguage.LanguageName2 = Convert.ToString(reader["LanguageName2"] == DBNull.Value ? "" : reader["LanguageName2"]);
                        PDRPLanguage.LanguageName3 = Convert.ToString(reader["LanguageName3"] == DBNull.Value ? "" : reader["LanguageName3"]);
                        PDRPLanguage.LanguageShortName = Convert.ToString(reader["LanguageShortName"] == DBNull.Value ? "" : reader["LanguageShortName"]);
                        PDRPLanguage.LanguageCulture = Convert.ToString(reader["LanguageCulture"] == DBNull.Value ? "" : reader["LanguageCulture"]);

                        PDRPLanguageList.Add(PDRPLanguage);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return PDRPLanguageList;
        }
        //public PDRPLanguage PDRPLoadLanguage(int LanguageID)
        //{

        //    PDRPLanguage PDRPLanguage = new PDRPLanguage();
        //    try
        //    {


        //        SqlParameter[] sqlParameters = new SqlParameter[1];

        //        SqlParameter sqlParameter = new SqlParameter();
        //        sqlParameter.ParameterName = "@LanguageID";
        //        sqlParameter.Value = LanguageID;

        //        SqlDataReader reader;

        //        sqlParameters[0] = sqlParameter;
        //        reader = ExecuteProcedure("HR_stp_PDRPGetLanguageList", sqlParameters);

        //        if (reader.HasRows)
        //        {
        //            while (reader.Read())
        //            {


        //                PDRPLanguage.LanguageID = Convert.ToInt32(reader["LanguageID"] == DBNull.Value ? 0 : reader["LanguageID"]);
        //                PDRPLanguage.LanguageName = Convert.ToString(reader["LanguageName"] == DBNull.Value ? "" : reader["LanguageName"]);
        //                PDRPLanguage.LanguageFaFlag = Convert.ToString(reader["LanguageFaFlag"] == DBNull.Value ? "" : reader["LanguageFaFlag"]);
        //                PDRPLanguage.LanguageName2 = Convert.ToString(reader["LanguageName2"] == DBNull.Value ? "" : reader["LanguageName2"]);
        //                PDRPLanguage.LanguageName3 = Convert.ToString(reader["LanguageName3"] == DBNull.Value ? "" : reader["LanguageName3"]);
        //                PDRPLanguage.LanguageShortName = Convert.ToString(reader["LanguageShortName"] == DBNull.Value ? "" : reader["LanguageShortName"]);

        //            }
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }
        //    }
        //    return PDRPLanguage;
        //}

        public HrSettings GetHrSettings()
        {
            HrSettings objHrSettingsModel = new HrSettings();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetHrSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        objHrSettingsModel = new HrSettings();
                        objHrSettingsModel.HrVersion = sqlDataReader["HrVersion"] == DBNull.Value ? "" : sqlDataReader["HrVersion"].ToString();
                        objHrSettingsModel.IsMicrosoftAuthEnabled = sqlDataReader["IsMicrosoftAuthEnabled"] == DBNull.Value ? false : bool.Parse(sqlDataReader["IsMicrosoftAuthEnabled"].ToString());

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return objHrSettingsModel;
        }

        
    }
}

