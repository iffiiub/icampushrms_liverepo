﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HRMS.DataAccess.GeneralDB
{
    public class HRInstituteDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<HRInstituteModel> GetAllHRInstitute()
        {
            List<HRInstituteModel> HRInstituteList = null;
            try
            {
                HRInstituteList = new List<HRInstituteModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_HRInstitute", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HRInstituteModel HRInstituteModel;
                    while (sqlDataReader.Read())
                    {
                        HRInstituteModel = new HRInstituteModel();

                        HRInstituteModel.HRInstituteID = Convert.ToInt32(sqlDataReader["HRInstituteID"].ToString());
                        HRInstituteModel.HRInstituteName_1 = sqlDataReader["HRInstituteName_1"] == DBNull.Value ? "" : sqlDataReader["HRInstituteName_1"].ToString();
                        HRInstituteList.Add(HRInstituteModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return HRInstituteList.OrderBy(x=>x.HRInstituteName_1).ToList();
        }

        public OperationDetails InsertHRInstitute(HRInstituteModel HRInstituteModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@HRInstituteName_1",HRInstituteModel.HRInstituteName_1) ,
                      new SqlParameter("@HRInstituteName_2", HRInstituteModel.HRInstituteName_2) ,
                      new SqlParameter("@HRInstituteName_3", HRInstituteModel.HRInstituteName_3)
                    };
                int HRInstituteId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_GEN_HRInstitute", parameters));
                return new OperationDetails(true, "HRInstitute saved successfully.", null, HRInstituteId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting HRInstitute.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public List<HRInstituteModel> GetAcademicInstitutes()
        {
            List<HRInstituteModel> AcademicInstituteList = new List<HRInstituteModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetAcademicInstitutes", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    HRInstituteModel academicInstitute;
                    while (reader.Read())
                    {
                        academicInstitute = new HRInstituteModel();
                        academicInstitute.HRInstituteID = Convert.ToInt32(reader["HRInstituteID"].ToString());
                        academicInstitute.HRInstituteName_1 = reader["HRInstituteName_1"].ToString() == "" ? "" : reader["HRInstituteName_1"].ToString();
                        academicInstitute.HRInstituteName_2 = reader["HRInstituteName_2"].ToString() == "" ? "" : reader["HRInstituteName_2"].ToString();
                        academicInstitute.HRInstituteName_3 = reader["HRInstituteName_3"].ToString() == "" ? "" : reader["HRInstituteName_3"].ToString();
                        academicInstitute.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        academicInstitute.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                        AcademicInstituteList.Add(academicInstitute);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
            return AcademicInstituteList;
        }

        public HRInstituteModel GetAcademicInstitutesById(int InstituteID)
        {
            HRInstituteModel academicInstitute = new HRInstituteModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetAcademicInstitutesById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@InstituteID", InstituteID);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        academicInstitute.HRInstituteID = Convert.ToInt32(reader["HRInstituteID"].ToString());
                        academicInstitute.HRInstituteName_1 = reader["HRInstituteName_1"].ToString() == "" ? "" : reader["HRInstituteName_1"].ToString();
                        academicInstitute.HRInstituteName_2 = reader["HRInstituteName_2"].ToString() == "" ? "" : reader["HRInstituteName_2"].ToString();
                        academicInstitute.HRInstituteName_3 = reader["HRInstituteName_3"].ToString() == "" ? "" : reader["HRInstituteName_3"].ToString();
                        academicInstitute.IsDeleted = reader["IsDeleted"].ToString() == "" ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
                        academicInstitute.IsActive = reader["IsActive"].ToString() == "" ? false : Convert.ToBoolean(reader["IsActive"].ToString());
                    }
                }
                sqlConnection.Close();
                return academicInstitute;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddEditAcademicInstitute(HRInstituteModel academicInstituteModel, int trnMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateAcademicInstitutesDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", trnMode);
                sqlCommand.Parameters.AddWithValue("@HRInstituteID", academicInstituteModel.HRInstituteID);
                sqlCommand.Parameters.AddWithValue("@HRInstituteName_1", academicInstituteModel.HRInstituteName_1);
                sqlCommand.Parameters.AddWithValue("@HRInstituteName_2", academicInstituteModel.HRInstituteName_2);
                sqlCommand.Parameters.AddWithValue("@HRInstituteName_3", academicInstituteModel.HRInstituteName_3);
                sqlCommand.Parameters.AddWithValue("@IsActive", academicInstituteModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@IsDeleted", academicInstituteModel.IsDeleted);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting academic institute details.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }
    }
}
