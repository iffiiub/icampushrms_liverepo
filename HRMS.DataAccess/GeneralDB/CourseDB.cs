﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;


namespace HRMS.DataAccess.GeneralDB
{
    public class CourseDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        ///// <summary>
        ///// Get All Genders
        ///// </summary>
        ///// <returns></returns>
        //public List<GenderModel> GetAllGenders()
        //{
        //    List<GenderModel> genderModelList = null;
        //    try
        //    {
        //        genderModelList = new List<GenderModel>();

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_GEN_Get_AllGenders", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;   

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {
        //            GenderModel genderModel;
        //            while (sqlDataReader.Read())
        //            {
        //                genderModel = new GenderModel();

        //                genderModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
        //                genderModel.GenderName_1 = sqlDataReader["GenderName_1"] == DBNull.Value ? "" : sqlDataReader["GenderName_1"].ToString();
        //                genderModel.GenderImagePath = sqlDataReader["GenderImagePath"] == DBNull.Value ? "" : sqlDataReader["GenderImagePath"].ToString();
        //                genderModelList.Add(genderModel);
        //            }
        //        }
        //        sqlConnection.Close();

                

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return genderModelList;
        //}

        ///// <summary>
        ///// Get Gender Details by GenderID
        ///// </summary>
        ///// <param name="GenderID"></param>
        ///// <returns></returns>
        //public GenderModel GetGenderByID(int GenderID)
        //{
        //    GenderModel genderModel = new GenderModel();
        //    try
        //    {
                
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_GEN_Get_GenderByID", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //        sqlCommand.Parameters.AddWithValue("@GenderID", GenderID);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {
                    
        //            while (sqlDataReader.Read())
        //            {
                        

        //                genderModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
        //                genderModel.GenderName_1 = sqlDataReader["GenderName_1"] == DBNull.Value ? "" : sqlDataReader["GenderName_1"].ToString();
        //                genderModel.GenderImagePath = sqlDataReader["GenderImagePath"] == DBNull.Value ? "" : sqlDataReader["GenderImagePath"].ToString();
                        
        //            }
        //        }
        //        sqlConnection.Close();



        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return genderModel;
        //}

        //public List<Types> GetTypes(string SP)
        //{
        //    List<Types> TypeList = new List<Types>();
        //    try
        //    {

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand(SP, sqlConnection);
        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //        if (sqlDataReader.HasRows)
        //        {

        //            while (sqlDataReader.Read())
        //            {

        //                Types objType = new Types();
        //                objType.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
        //                objType.Text = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();

        //                TypeList.Add(objType);
        //            }
        //        }
        //        sqlConnection.Close();



        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return TypeList;


        //}

        public OperationDetails AddCourse(CourseModel CourseModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@CourseName", CourseModel.CourseName) ,
                        
                      
                    };
                int CourseId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "stp_Add_GEN_Course", parameters));
                return new OperationDetails(true, "Course inserted successfully.", null, CourseId);
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while inserting MaritialStaus.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
