﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.General;
using HRMS.Entities;


namespace HRMS.DataAccess.GeneralDB
{
    public class HRContractTypeDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get All Genders
        /// </summary>
        /// <returns></returns>
        public List<HRContractTypeModel> GetAllHRContractType()
        {
            List<HRContractTypeModel> HRContractTypeList = null;
            try
            {
                HRContractTypeList = new List<HRContractTypeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetAllHRContractType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HRContractTypeModel HRContractTypeModel;
                    while (sqlDataReader.Read())
                    {
                        HRContractTypeModel = new HRContractTypeModel();

                        HRContractTypeModel.HRContractTypeID = Convert.ToInt32(sqlDataReader["HRContractTypeID"].ToString());
                        HRContractTypeModel.HRContractTypeName_1 = sqlDataReader["HRContractTypeName_1"] == DBNull.Value ? "" : sqlDataReader["HRContractTypeName_1"].ToString();
                        HRContractTypeList.Add(HRContractTypeModel);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return HRContractTypeList;
        }

        /// <summary>
        /// Get contract type by id
        /// </summary>
        /// <param name="ContractTypeId"></param>
        /// <returns></returns>
        public HRContractTypeModel GetContractTypeByID(int ContractTypeId)
        {
            HRContractTypeModel HRContractTypeModel = new HRContractTypeModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GEN_Get_ContractTypeByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ContractTypeId", ContractTypeId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        HRContractTypeModel.HRContractTypeID = Convert.ToInt32(sqlDataReader["HRContractTypeID"].ToString());
                        HRContractTypeModel.HRContractTypeName_1 = sqlDataReader["HRContractTypeName_1"] == DBNull.Value ? "" : sqlDataReader["HRContractTypeName_1"].ToString();
                        HRContractTypeModel.HRContractTypeName_2 = sqlDataReader["HRContractTypeName_2"] == DBNull.Value ? "" : sqlDataReader["HRContractTypeName_2"].ToString();
                        HRContractTypeModel.HRContractTypeName_3 = sqlDataReader["HRContractTypeName_3"] == DBNull.Value ? "" : sqlDataReader["HRContractTypeName_3"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return HRContractTypeModel;
        }
    }
}
