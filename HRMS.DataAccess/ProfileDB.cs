﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using HRMS.Entities.ViewModel;

namespace HRMS.DataAccess
{
    public class ProfileDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        /// <summary>
        /// Add Profile Details
        /// </summary>
        /// <param name="profilemodel"></param>
        /// <returns></returns>
        public OperationDetails AddProfile(ProfileModel profilemodel)
        {
            string Message = "";
            int insertedRowId = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_Profile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyId", profilemodel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@ProfileName", profilemodel.ProfileName);
                sqlCommand.Parameters.AddWithValue("@SessionTimeout", profilemodel.SessionTimeout);

                sqlCommand.Parameters.AddWithValue("@CreatedBy", profilemodel.CreatedBy);
                //sqlCommand.Parameters.AddWithValue("@CreatedOn", profilemodel.CreatedOn);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();


                Message = OperationMessage.Value.ToString();
                insertedRowId = Convert.ToInt32(OperationId.Value);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, insertedRowId);
        }


        /// <summary>
        /// Update Profile Details
        /// </summary>
        /// <param name="profilemodel"></param>
        /// <returns></returns>
        public OperationDetails UpdateProfile(ProfileModel profilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_Profile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ProfileId", profilemodel.ProfileId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", profilemodel.CompanyId);
                sqlCommand.Parameters.AddWithValue("@ProfileName", profilemodel.ProfileName);
                sqlCommand.Parameters.AddWithValue("@SessionTimeout", profilemodel.SessionTimeout);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", profilemodel.ModifiedBy);




                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, profilemodel.ProfileId);
        }

        /// <summary>
        /// Delete Profile Details
        /// </summary>
        /// <param name="profilemodel"></param>
        /// <returns></returns>
        public string DeleteProfile(ProfileModel profilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_Profile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ProfileId", profilemodel.ProfileId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Get All Profiles 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<ProfileModel> GetAllProfiles(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder,int CompanyId, out int totalCount)
        {
           
            List<ProfileModel> profileModelList = null;
            try
            {
                profileModelList = new List<ProfileModel>();
                
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_HR_Profile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);

                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);
                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ProfileModel profileModel;
                    while (sqlDataReader.Read())
                    {
                        profileModel = new ProfileModel();

                        profileModel.ProfileId = Convert.ToInt32(sqlDataReader["ProfileId"].ToString());
                        profileModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        profileModel.ProfileName = Convert.ToString(sqlDataReader["ProfileName"]);
                        profileModel.SessionTimeout = Convert.ToInt32(sqlDataReader["SessionTimeout"]);
                        profileModelList.Add(profileModel);
                    }
                }
                sqlConnection.Close();

                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return profileModelList;
        }


        /// <summary>
        /// Add ProfileDaysAccess Details
        /// </summary>
        /// <param name="profilemodel"></param>
        /// <returns></returns>
        public string AddProfileDaysAccess(ProfileDaysAccessModel profilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Add_HR_ProfileDaysAcess", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ProfileId", profilemodel.ProfileId);
                sqlCommand.Parameters.AddWithValue("@No_Of_Days", profilemodel.No_Of_Days);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", profilemodel.CreatedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        public string DeleteProfileDaysAccess(ProfileDaysAccessModel profilemodel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_ProfileDaysAcess", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ProfileId", profilemodel.ProfileId);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }


        public List<ProfileDaysAccessModel> GetAllProfilesDaysAccess()
        {
            List<ProfileDaysAccessModel> profileModelList = null;
            try
            {
                profileModelList = new List<ProfileDaysAccessModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_Profile", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ProfileDaysAccessModel profileModel;
                    while (sqlDataReader.Read())
                    {
                        profileModel = new ProfileDaysAccessModel();

                        profileModel.ProfileId = Convert.ToInt32(sqlDataReader["ProfileId"].ToString());
                        profileModel.No_Of_Days = Convert.ToInt32(sqlDataReader["No_Of_Days"].ToString());
                        profileModelList.Add(profileModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return profileModelList;
        }

        /// <summary>
        /// Get All Profile Days Access Details By Profile Id
        /// </summary>
        /// <param name="ProfileId"></param>
        /// <returns></returns>
        public List<ProfileDaysAccessModel> GetAllProfilesDaysAccessByProfileId(int ProfileId)
        {
            List<ProfileDaysAccessModel> profileModelList = null;
            try
            {
                profileModelList = new List<ProfileDaysAccessModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_ProfileDaysAcessByProfileId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ProfileId", ProfileId);

                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ProfileDaysAccessModel profileModel;
                    while (sqlDataReader.Read())
                    {
                        profileModel = new ProfileDaysAccessModel();

                        profileModel.ProfileId = Convert.ToInt32(sqlDataReader["ProfileId"].ToString());
                        profileModel.No_Of_Days = Convert.ToInt32(sqlDataReader["No_Of_Days"].ToString());
                        profileModelList.Add(profileModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return profileModelList;
        }

        /// <summary>
        /// Get Profiles By Profile Id
        /// </summary>
        /// <param name="ProfileId"></param>
        /// <returns></returns>
        public ProfileModel Get_ProfileByProfileId(int ProfileId)
        {
            ProfileModel profileModel = new ProfileModel();
            List<ProfileModel> profileModelList = null;
            try
            {
                profileModelList = new List<ProfileModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_HR_ProfileByProfileId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ProfileId", ProfileId);


                //SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                //TotalCount.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {


                        profileModel.ProfileId = Convert.ToInt32(sqlDataReader["ProfileId"].ToString());
                        profileModel.CompanyId = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        profileModel.ProfileName = Convert.ToString(sqlDataReader["ProfileName"]);
                        profileModel.SessionTimeout = Convert.ToInt32(sqlDataReader["SessionTimeout"]);

                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return profileModel;
        }


    }
}
