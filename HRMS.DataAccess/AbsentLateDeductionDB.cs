﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class AbsentLateDeductionDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

    

        #region Late Deduction
        /// <summary>
        /// Get All Late Deduction of Employee
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<LateDeductionEmployeeModel> GetLateDeductionOfEmployee(DateTime? FromDate = null, DateTime? ToDate = null, int UserId = 0, bool ShowAllRecords = false, bool? IsConfirmed = null, bool? IsDeducted = null)
        {
            List<LateDeductionEmployeeModel> LateDeductionEmployeeModelList = null;
            try
            {
                LateDeductionEmployeeModelList = new List<LateDeductionEmployeeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetLateDeductionOfEmployees", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aDttFromdate", FromDate);
                sqlCommand.Parameters.AddWithValue("@aDttTodate", ToDate);
                sqlCommand.Parameters.AddWithValue("@aIntUserID", UserId);
                sqlCommand.Parameters.AddWithValue("@aBitShoAllRecords", ShowAllRecords);
                sqlCommand.Parameters.AddWithValue("@aBitIsConfirmed", IsConfirmed);
                sqlCommand.Parameters.AddWithValue("@IsDeducted", IsDeducted);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    LateDeductionEmployeeModel lateDeductionEmployeeModel;
                    while (sqlDataReader.Read())
                    {
                        lateDeductionEmployeeModel = new LateDeductionEmployeeModel();

                        lateDeductionEmployeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        lateDeductionEmployeeModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"].ToString());
                        lateDeductionEmployeeModel.LateMinutes = Convert.ToDecimal(sqlDataReader["LateMinutes"].ToString());
                        lateDeductionEmployeeModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"].ToString());
                        lateDeductionEmployeeModel.DeductedMinutes = Convert.ToDecimal(sqlDataReader["DeductedMinutes"].ToString());

                        lateDeductionEmployeeModel.AllMinutes = Convert.ToDecimal(sqlDataReader["AllMinutes"].ToString());
                        lateDeductionEmployeeModel.TotalSalary = Convert.ToDecimal(sqlDataReader["TotalSalary"].ToString());
                        lateDeductionEmployeeModel.LateMinutesToDed = Convert.ToInt32(sqlDataReader["LateMinutesToDed"].ToString());
                        lateDeductionEmployeeModel.DeductionPercent = Convert.ToDecimal(sqlDataReader["DeductionPercent"].ToString());

                        lateDeductionEmployeeModel.IsConfirmed = Convert.ToBoolean(sqlDataReader["IsConfirmed"].ToString());
                        lateDeductionEmployeeModel.IsGenerated = Convert.ToBoolean(sqlDataReader["IsGenerated"].ToString());

                        lateDeductionEmployeeModel.GenerateDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["GenerateDate"].ToString());
                        lateDeductionEmployeeModel.ConfirmedDAte = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["ConfirmedDAte"].ToString());
                        lateDeductionEmployeeModel.EffectiveDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EffectiveDate"].ToString());
                                                
                        LateDeductionEmployeeModelList.Add(lateDeductionEmployeeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return LateDeductionEmployeeModelList;
        }

        public LateDeduction GetAttendanceSetup()
        {
            LateDeduction lateDeduction = new LateDeduction();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT isnull(LateMinutes, 0) LateMinutes, isnull(DeductionPercent, 0) DeductionPercent  from HR_LateDeductionSettings;", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        lateDeduction = new LateDeduction();

                        lateDeduction.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        lateDeduction.DeductionPercent = Convert.ToDecimal(sqlDataReader["DeductionPercent"].ToString());                        
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return lateDeduction;
        }

        public string SetEmployeeDeductionStatus(int EmployeeId, DateTime FromDate, DateTime ToDate, DateTime EffectiveDate)
        {
            string iResult = string.Empty;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspSetEmployeeLateDeductedStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@aFromDate", FromDate);
                sqlCommand.Parameters.AddWithValue("@aToDate", ToDate);
                sqlCommand.Parameters.AddWithValue("@aEffectiveDate", EffectiveDate);

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                iResult = OperationMessage.Value.ToString();
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return iResult;
        }




        public string SetEmployeeLeaveTypeStatusData(int PayAbsentTypeID, int PayEmployeeAbsentID)
        {
            string iResult = string.Empty;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Update_HR_PayEmployeeAbsent_AbsentType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayAbsentTypeID ", PayAbsentTypeID);
                sqlCommand.Parameters.AddWithValue("@PayEmployeeAbsentID ", PayEmployeeAbsentID);
            
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                iResult ="Success";
            }
            catch (Exception ex)
            {
                throw ex;
                iResult = "Failed";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return iResult;
        }





        public string SaveRecord(string EmployeeId, DateTime FromDate, DateTime ToDate)
        {
            string iResult = string.Empty;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspConfirmEmployeeForLateDeductedStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@aDttFromdate", FromDate);
                sqlCommand.Parameters.AddWithValue("@aDttTodate", ToDate);                

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                iResult = OperationMessage.Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return iResult;
        }
        #endregion
    }
}
