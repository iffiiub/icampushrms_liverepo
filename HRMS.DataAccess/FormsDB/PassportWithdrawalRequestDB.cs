﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.DataAccess.FormsDB;
using System.Globalization;

namespace HRMS.DataAccess.FormsDB
{
    public class PassportWithdrawalRequestDB : FormsDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PickList> GetPassportRequestsReasonList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetPassportRequestsReasonList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["PassportReasonID"].ToString()), text = Convert.ToString(sqlDataReader["PassportReasonName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public RequestFormsProcessModel SavePassportWithdrawalRequest(PassportWithdrawalRequestModel objPassportWithdrawalRequestModel, int modifiedBy)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePassportWithdrawalRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objPassportWithdrawalRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PassportReasonID", objPassportWithdrawalRequestModel.PassportReasonID);
                sqlCommand.Parameters.AddWithValue("@WithdrawalDate", DateTime.ParseExact(objPassportWithdrawalRequestModel.WithdrawalDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@PassportReturnDate", DateTime.ParseExact(objPassportWithdrawalRequestModel.PassportReturnDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@OtherReason", objPassportWithdrawalRequestModel.OtherReason);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objPassportWithdrawalRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@Comments", objPassportWithdrawalRequestModel.Comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModel;
        }

        public int UpdatePassportWithdrawalRequest(PassportWithdrawalRequestModel objPassportWithdrawalRequestModel)
        {
            int output = 0;
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdatePassportWithdrawalRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", objPassportWithdrawalRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@PassportReasonID", objPassportWithdrawalRequestModel.PassportReasonID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objPassportWithdrawalRequestModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@WithdrawalDate", DateTime.ParseExact(objPassportWithdrawalRequestModel.WithdrawalDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@PassportReturnDate", DateTime.ParseExact(objPassportWithdrawalRequestModel.PassportReturnDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@OtherReason", objPassportWithdrawalRequestModel.OtherReason);               
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", objPassportWithdrawalRequestModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", objPassportWithdrawalRequestModel.Comments);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
               
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public PassportWithdrawalRequestModel GetPassportWithdrawalRequest(int ID, int formProcessID)
        {
            PassportWithdrawalRequestModel objPassportWithdrawalRequestModel = new PassportWithdrawalRequestModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetPassportWithdrawalRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPassportWithdrawalRequestModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objPassportWithdrawalRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        objPassportWithdrawalRequestModel.PassportReasonID = Convert.ToInt32(sqlDataReader["PassportReasonID"].ToString());
                        objPassportWithdrawalRequestModel.PassportReasonName = Convert.ToString(sqlDataReader["PassportReasonName"]);
                        objPassportWithdrawalRequestModel.OtherReason = Convert.ToString(sqlDataReader["OtherReason"]);
                        objPassportWithdrawalRequestModel.WithdrawalDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["WithdrawalDate"].ToString());
                        objPassportWithdrawalRequestModel.PassportReturnDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PassportReturnDate"].ToString());
                        objPassportWithdrawalRequestModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"].ToString());
                        objPassportWithdrawalRequestModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString());
                        objPassportWithdrawalRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objPassportWithdrawalRequestModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"].ToString());
                        objPassportWithdrawalRequestModel.Comments = sqlDataReader["Comments"].ToString();
                    }
                }
                sqlConnection.Close();
                objPassportWithdrawalRequestModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPassportWithdrawalRequestModel;
        }

        #region Passport Withdrawal Status Report
        public List<PassportWithdrawalStatusReportModel> GetPassportWithdrawalStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<PassportWithdrawalStatusReportModel> passportWithdrawalStatusReportList = null;
            try
            {
                passportWithdrawalStatusReportList = new List<PassportWithdrawalStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PassportWithdrawalStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PassportWithdrawalStatusReportModel passportWithdrawalStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        passportWithdrawalStatusReportModel = new PassportWithdrawalStatusReportModel();
                        passportWithdrawalStatusReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        passportWithdrawalStatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        passportWithdrawalStatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        passportWithdrawalStatusReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        passportWithdrawalStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        passportWithdrawalStatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        passportWithdrawalStatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        passportWithdrawalStatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        passportWithdrawalStatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        passportWithdrawalStatusReportModel.RequestReason = Convert.ToString(sqlDataReader["RequestReason"] == DBNull.Value ? "" : sqlDataReader["RequestReason"]);
                        passportWithdrawalStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        passportWithdrawalStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        passportWithdrawalStatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        passportWithdrawalStatusReportList.Add(passportWithdrawalStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return passportWithdrawalStatusReportList;
        }
        #endregion

    }
}
