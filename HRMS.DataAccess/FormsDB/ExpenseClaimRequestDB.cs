﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.DataAccess.FormsDB;
using System.Globalization;

namespace HRMS.DataAccess.FormsDB
{
    public class ExpenseClaimRequestDB : FormsDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PickList> GetExpenseClaimTypeList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExpenseClaimTypeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["ClaimTypeID"].ToString()), text = Convert.ToString(sqlDataReader["ClaimTypeName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetCurrencyForAmountList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetCurrencyForAmountList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["CurrencyID"].ToString()), text = Convert.ToString(sqlDataReader["CurrencyName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public RequestFormsProcessModel SaveExpenseClaimRequest(ExpenseClaimRequestModel objExpenseClaimRequestModel, int modifiedBy)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                DataTable allFiles = ConvertFilesListToTable(objExpenseClaimRequestModel.AllFormsFilesModelList);
                DataTable expenseClaimDetail = ConvertExpenseClaimListToTable(objExpenseClaimRequestModel.ExpenseClaimRequestDetailList, objExpenseClaimRequestModel.ID);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveExpenseClaimRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objExpenseClaimRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@ExpenseClaimRequestDetail", expenseClaimDetail);
                sqlCommand.Parameters.AddWithValue("@AllFiles", allFiles);
                sqlCommand.Parameters.AddWithValue("@Comments", objExpenseClaimRequestModel.Comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModel;
        }

        public OperationDetails UpdateExpenseClaimRequest(ExpenseClaimRequestModel objExpenseClaimRequestModel,string DeletedClaimDetailIDs, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable allFiles = ConvertFilesListToTable(objExpenseClaimRequestModel.AllFormsFilesModelList);
                DataTable expenseClaimDetail = ConvertExpenseClaimListToTable(objExpenseClaimRequestModel.ExpenseClaimRequestDetailList, objExpenseClaimRequestModel.ID);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateExpenseClaimRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", objExpenseClaimRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objExpenseClaimRequestModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@DeletedClaimDetailIDs", DeletedClaimDetailIDs);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@ExpenseClaimRequestDetail", expenseClaimDetail);
                sqlCommand.Parameters.AddWithValue("@AllFiles", allFiles);
                sqlCommand.Parameters.AddWithValue("@Comments", objExpenseClaimRequestModel.Comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public DataTable ConvertExpenseClaimListToTable(List<ExpenseClaimRequestDetailModel> objExpenseClaimRequestDetailModel, int ID)
        {
            int i = 0;

            DataTable expensesTable = new DataTable();
            expensesTable.Columns.Add("ClaimDetailID", System.Type.GetType("System.Int16"));
            expensesTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
            expensesTable.Columns.Add("FormFileIDName");
            expensesTable.Columns.Add("Date", System.Type.GetType("System.DateTime"));
            expensesTable.Columns.Add("ClaimTypeID", System.Type.GetType("System.Int16"));
            expensesTable.Columns.Add("Description");
            expensesTable.Columns.Add("CurrencyID", System.Type.GetType("System.Int16"));
            expensesTable.Columns.Add("ExchangeRate", System.Type.GetType("System.Decimal"));
            expensesTable.Columns.Add("Amount", System.Type.GetType("System.Decimal"));
            expensesTable.Columns.Add("TotalAmountAED", System.Type.GetType("System.Decimal"));
            expensesTable.Columns.Add("ExpClaimFileID", System.Type.GetType("System.Int16"));


            foreach (ExpenseClaimRequestDetailModel expenses in objExpenseClaimRequestDetailModel)
            {
                i++;
                expensesTable.Rows.Add(expenses.ClaimDetailID,
                                        ID,
                                        expenses.RowID,
                                        DateTime.ParseExact(expenses.Date, HRMSDateFormat, CultureInfo.InvariantCulture),
                                        expenses.ClaimTypeID,
                                        expenses.Description,
                                        expenses.CurrencyID,
                                        expenses.ExchangeRate,
                                        expenses.Amount,
                                        expenses.TotalAmountAED,
                                        expenses.ExpClaimFileID
                                     );
            }

            return expensesTable;
        }

        public ExpenseClaimRequestModel GetExpenseClaimRequest(int ID, int formProcessID)
        {
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExpenseClaimRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objExpenseClaimRequestModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExpenseClaimRequestModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"].ToString());
                        objExpenseClaimRequestModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString());
                        objExpenseClaimRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objExpenseClaimRequestModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : sqlDataReader["CreatedOn"].ToString());
                    }
                }
                sqlConnection.Close();
                objExpenseClaimRequestModel.ExpenseClaimRequestDetailList = GetExpenseClaimRequestDetails(objExpenseClaimRequestModel.ID);
                objExpenseClaimRequestModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                foreach (ExpenseClaimRequestDetailModel objExpenseClaimRequestDetailModel in objExpenseClaimRequestModel.ExpenseClaimRequestDetailList)
                {
                    if (objExpenseClaimRequestDetailModel.ExpClaimFileID > 0)
                    {
                        allFormsFilesModel = new AllFormsFilesModel();
                        allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objExpenseClaimRequestDetailModel.ExpClaimFileID);
                        allFormsFilesModel.FormFileIDName = objExpenseClaimRequestDetailModel.ClaimDetailID.ToString();
                        allFormsFilesModelList.Add(allFormsFilesModel);
                    }
                }
                objExpenseClaimRequestModel.AllFormsFilesModelList = allFormsFilesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExpenseClaimRequestModel;
        }

        public List<ExpenseClaimRequestDetailModel> GetExpenseClaimRequestDetails(int ID)
        {
            List<ExpenseClaimRequestDetailModel> objExpenseClaimRequestDetailModelList = new List<ExpenseClaimRequestDetailModel>();
            string AmountFormat = new PayrollDB().GetAmountFormat();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExpenseClaimRequestDetail", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExpenseClaimRequestDetailModel objExpenseClaimRequestDetailModel;
                    while (sqlDataReader.Read())
                    {
                        objExpenseClaimRequestDetailModel = new ExpenseClaimRequestDetailModel();
                        objExpenseClaimRequestDetailModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExpenseClaimRequestDetailModel.ClaimDetailID = Convert.ToInt32(sqlDataReader["ClaimDetailID"].ToString());
                        objExpenseClaimRequestDetailModel.ClaimTypeID = Convert.ToInt32(sqlDataReader["ClaimTypeID"].ToString());
                        objExpenseClaimRequestDetailModel.Date = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["Date"].ToString());
                        objExpenseClaimRequestDetailModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        objExpenseClaimRequestDetailModel.CurrencyID = Convert.ToInt32(sqlDataReader["CurrencyID"].ToString());
                        objExpenseClaimRequestDetailModel.ExchangeRate = Convert.ToDecimal(sqlDataReader["ExchangeRate"]).ToString("0.0000");
                        objExpenseClaimRequestDetailModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"]).ToString(AmountFormat);
                        objExpenseClaimRequestDetailModel.TotalAmountAED = Convert.ToDecimal(sqlDataReader["TotalAmountAED"]).ToString(AmountFormat);
                        objExpenseClaimRequestDetailModel.ExpClaimFileID = Convert.ToInt32(sqlDataReader["ExpClaimFileID"].ToString());
                        objExpenseClaimRequestDetailModelList.Add(objExpenseClaimRequestDetailModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExpenseClaimRequestDetailModelList;
        }

        public int PushFinalExpenseClaimPayAddition(int FormProcessID)
        {
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            int output = 0;
            try
            {               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_PushFinalExpenseClaimPayAddition", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);                
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        #region Expense Claim Status Report
        public List<ExpenseClaimStatusReportModel> GetExpenseClaimStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int userId)
        {
            List<ExpenseClaimStatusReportModel> listStatusReportModel = null;
            try
            {
                listStatusReportModel = new List<ExpenseClaimStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetExpenseClaimStatusReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@CurrentStatusID", CurrentStatusID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ExpenseClaimStatusReportModel StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        StatusReportModel = new ExpenseClaimStatusReportModel();
                        StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);
                        StatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        StatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        StatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        StatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        StatusReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        StatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        StatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        StatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        StatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        StatusReportModel.TotalExpenseAmount = Convert.ToDecimal(sqlDataReader["TotalExpenseAmount"] == DBNull.Value ? "0" : sqlDataReader["TotalExpenseAmount"].ToString());
                        StatusReportModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"] == DBNull.Value ? "0" : sqlDataReader["Amount"].ToString());
                        StatusReportModel.ProjectName = Convert.ToString(sqlDataReader["ProjectName"] == DBNull.Value ? "" : sqlDataReader["ProjectName"]);
                        listStatusReportModel.Add(StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listStatusReportModel;
        }
        #endregion
    }
}
