﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class LeaveRejoingDB : FormsDB
    {
        public List<LeaveRequestDetailModel> GetLeavesReJoiningList( int RequesterEmployeeID)
        {
            List<LeaveRequestDetailModel> objLeaveRequestDetailModelList = new List<LeaveRequestDetailModel>();
            LeaveRequestDetailModel objLeaveRequestDetailModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeavesReJoiningList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", RequesterEmployeeID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        objLeaveRequestDetailModel = new LeaveRequestDetailModel();
                        objLeaveRequestDetailModel.ID = Convert.ToInt16(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        objLeaveRequestDetailModel.FormProcessID = Convert.ToInt16(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        objLeaveRequestDetailModel.RequestID = Convert.ToInt16(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString()); 
                        objLeaveRequestDetailModel.IsHalfDayLeave = Convert.ToBoolean(reader["HalfDayLeave"]);
                        objLeaveRequestDetailModel.LeaveStartDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["FromDate"].ToString());
                        objLeaveRequestDetailModel.LeaveEndDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ToDate"].ToString()); 
                        objLeaveRequestDetailModel.TotalDaysLeave = Convert.ToString(reader["TotalDaysLeave"]);
                        objLeaveRequestDetailModel.CreatedOn = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["CreatedOn"].ToString());
                        objLeaveRequestDetailModelList.Add(objLeaveRequestDetailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objLeaveRequestDetailModelList;
        }

        public List<LeaveRequestDetailModel> GetLeaveRequestDetail(int LeaveID)
        {
            List<LeaveRequestDetailModel> objLeaveRequestDetailModelList = new List<LeaveRequestDetailModel>();
            LeaveRequestDetailModel objLeaveRequestDetailModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeaveRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LeaveID", LeaveID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        objLeaveRequestDetailModel = new LeaveRequestDetailModel();
                        objLeaveRequestDetailModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        objLeaveRequestDetailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        objLeaveRequestDetailModel.IsHalfDayLeave = Convert.ToBoolean(reader["HalfDayLeave"]);
                        objLeaveRequestDetailModel.LeaveStartDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["FromDate"].ToString());
                        objLeaveRequestDetailModel.LeaveEndDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ToDate"].ToString());
                        objLeaveRequestDetailModel.TotalDaysLeave = Convert.ToString(reader["TotalDaysLeave"]);
                        objLeaveRequestDetailModel.InCountry = Convert.ToBoolean(reader["InCountry"]);
                        objLeaveRequestDetailModel.OutCountry = Convert.ToBoolean(reader["OutCountry"]);
                        objLeaveRequestDetailModel.Destination = Convert.ToInt16(reader["CountryID"] == DBNull.Value ? "0" : reader["CountryID"].ToString());
                        objLeaveRequestDetailModel.VacationTypeID = Convert.ToInt16(reader["VacationTypeID"] == DBNull.Value ? "0" : reader["VacationTypeID"].ToString());
                        objLeaveRequestDetailModel.ContactInfo_1 = Convert.ToString(reader["ContactInfo1"]);
                        objLeaveRequestDetailModel.ContactInfo_2 = Convert.ToString(reader["ContactInfo2"]);
                        objLeaveRequestDetailModel.ContactInfo_3 = Convert.ToString(reader["ContactInfo3"]);
                        objLeaveRequestDetailModel.CreatedOn = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["CreatedOn"].ToString());
                        objLeaveRequestDetailModel.AvailableLeaveDays = Convert.ToString(reader["AvailableLeaveDays"]);
                        objLeaveRequestDetailModel.LapseDate = reader["LapsingDate"] != null ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["LapsingDate"].ToString()) : "";
                        objLeaveRequestDetailModel.NewExtendedLapseDate = reader["ExtendedLapsingDate"] != null ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExtendedLapsingDate"].ToString()) : "";
                        objLeaveRequestDetailModel.ExtendedLapsed = Convert.ToString(reader["ExtendedLapsingDays"]);
                        objLeaveRequestDetailModel.ToBeLapsed = Convert.ToString(reader["LapsingDays"]);
                        objLeaveRequestDetailModel.RemainingLeaveDays = Convert.ToString(reader["RemainingDays"]);
                        objLeaveRequestDetailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        objLeaveRequestDetailModelList.Add(objLeaveRequestDetailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objLeaveRequestDetailModelList;
        }

        public LeaveRejoiningModel GetEmployeeLeavesReJoining(int ID)
        {
            LeaveRejoiningModel objLeaveRejoiningModel=new LeaveRejoiningModel();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeLeavesReJoining", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        objLeaveRejoiningModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        objLeaveRejoiningModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        objLeaveRejoiningModel.ID_LeaveRequest = Convert.ToInt32(reader["ID_LeaveRequest"] == DBNull.Value ? "0" : reader["ID_LeaveRequest"].ToString());
                        objLeaveRejoiningModel.RequestDate = DateTime.Parse(reader["RequestDate"].ToString()).ToString(HRMSDateFormat);
                        objLeaveRejoiningModel.ReJoiningDate = DateTime.Parse(reader["ReJoiningDate"].ToString()).ToString(HRMSDateFormat);
                        objLeaveRejoiningModel.LeavesToAdjust = Convert.ToInt32(reader["LeavesToAdjust"].ToString());
                        objLeaveRejoiningModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"].ToString());
                        objLeaveRejoiningModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                        objLeaveRejoiningModel.CreatedOn = DateTime.Parse(reader["CreatedOn"].ToString()).ToString(HRMSDateFormat);
                        objLeaveRejoiningModel.LeaveRequestFormRequestID = string.IsNullOrEmpty(Convert.ToString(reader["LeaveRequestFormRequestID"])) ? (int?)null : int.Parse(reader["LeaveRequestFormRequestID"].ToString());
                       // HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["CreatedOn"].ToString());
                        objLeaveRejoiningModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
          
            return objLeaveRejoiningModel;
        }

        public int CheckEmployeeLeavesReJoiningExist(int LeaveID)
        {
            int ID = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckEmployeeLeavesReJoiningExist", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID_LeaveRequest", LeaveID);
                ID = Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ID;
        }
        public RequestFormsProcessModel SaveLeaveRejoiningRequest(LeaveRejoiningModel objLeaveRejoiningModel, int createdBy)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveLeaveRejoiningRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objLeaveRejoiningModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ID_LeaveRequest", objLeaveRejoiningModel.ID_LeaveRequest);
                sqlCommand.Parameters.AddWithValue("@RequestDate", DateTime.ParseExact(objLeaveRejoiningModel.RequestDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@ReJoiningDate", DateTime.ParseExact(objLeaveRejoiningModel.ReJoiningDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@LeavesToAdjust", objLeaveRejoiningModel.LeavesToAdjust);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", createdBy);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objLeaveRejoiningModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objLeaveRejoiningModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@Comments", objLeaveRejoiningModel.Comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModel;
        }

        public OperationDetails UpdateLeaveRejoiningRequest(LeaveRejoiningModel objLeaveRejoiningModel, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateLeaveRejoiningRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", objLeaveRejoiningModel.ID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objLeaveRejoiningModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestDate", DateTime.ParseExact(objLeaveRejoiningModel.RequestDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@ReJoiningDate", DateTime.ParseExact(objLeaveRejoiningModel.ReJoiningDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@LeavesToAdjust", objLeaveRejoiningModel.LeavesToAdjust);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objLeaveRejoiningModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", objLeaveRejoiningModel.Comments);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

       // public OperationDetails SaveCreditDebitLeavesReJoiningEmployee(int formProcessID,int LeaveToAdjust, int VacationType,int EmployeeID, string StartDate, string EndDate)
        public OperationDetails SaveCreditDebitLeavesReJoiningEmployee(int formProcessID, int LeaveToAdjust, int approverID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveCreditDebitLeavesReJoiningEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@LeaveToAdjust", LeaveToAdjust);
                sqlCommand.Parameters.AddWithValue("@ApproverID", approverID);
                //sqlCommand.Parameters.AddWithValue("@VacationType", VacationType);
                //sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                //sqlCommand.Parameters.AddWithValue("@StartDate", DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                //sqlCommand.Parameters.AddWithValue("@EndDate", DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                SqlParameter OperationMessage = new SqlParameter("@Output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch 
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
    }

}
