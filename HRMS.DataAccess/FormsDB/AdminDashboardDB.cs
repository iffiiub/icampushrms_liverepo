﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class AdminDashboardDB: FormsDB
    {
        public int CheckProbationConfirmationIsCompletedForSelectedEmployee(int employeeId)
        {
            int output = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckProbationConfirmationIsCompletedForSelectedEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public OperationDetails UpdateProbationCompletionDateManual(int employeeId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateProbationCompletionDateManual", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter OperationMessage = new SqlParameter("@Output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                operationDetails.InsertedRowId = Convert.ToInt32(sqlCommand.Parameters["@Output"].Value);
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.CssClass = "success";
                operationDetails.Message = "Probation Completion date is updated successfully";

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public OperationDetails IsRequestAndSupervisorIsPresentForSelectedEmployee(int employeeId)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckRequestAndSupervisorIsPresentForSelectedEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;          
        }

        public int InitializeProbationConfirmationRequest(int employeeId)
        {
            int result = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_EmployeeConfirmationRequestInitialize", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeId);
                sqlCommand.Parameters.AddWithValue("@IsManual", true);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        result = Convert.ToInt32(sqlDataReader["Result"] == DBNull.Value ? "0" : sqlDataReader["Result"].ToString());
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return result;           
        }

        public List<PickList> GetRequestList(int reqStatusID, int? formId)
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllRequestList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", reqStatusID);
                sqlCommand.Parameters.AddWithValue("@FormId", formId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["RequestID"].ToString()), text = Convert.ToString(sqlDataReader["RequestID"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public string GetPendingApproverEmployeeByRequestId(int requestId)
        {
            string result = string.Empty;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetPendingApproverEmployeeByRequestId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        result = Convert.ToString(sqlDataReader["GroupName"].ToString()) ;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return result;
        }

        public OperationDetails UpdateRequestApprovarEmpoyee(int requestId, int employeeId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateRequestApprovarEmpoyee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);               
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
    }
}
