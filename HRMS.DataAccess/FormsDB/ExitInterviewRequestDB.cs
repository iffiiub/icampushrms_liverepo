﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class ExitInterviewRequestDB : FormsDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
     
        #region Option List...
        public List<ExitInterviewReasonOptionModel> GetExitInterviewReasonOptionList()
        {
            List<ExitInterviewReasonOptionModel> objExitInterviewReasonOptionList = new List<ExitInterviewReasonOptionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewReasonOptionList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewReasonOptionModel objExitInterviewReasonOptionModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewReasonOptionModel = new ExitInterviewReasonOptionModel();
                        objExitInterviewReasonOptionModel.PrimaryReasonID = Convert.ToInt32(sqlDataReader["PrimaryReasonID"].ToString());
                        objExitInterviewReasonOptionModel.PrimaryReasonName = Convert.ToString(sqlDataReader["PrimaryReasonName"]);
                        objExitInterviewReasonOptionList.Add(objExitInterviewReasonOptionModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewReasonOptionList;
        }
        public List<ExitInterviewSupervisorOptionModel> GetExitInterviewSupervisorOptionList()
        {
            List<ExitInterviewSupervisorOptionModel> objExitInterviewSupervisorOptionList = new List<ExitInterviewSupervisorOptionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewSupervisorOptionList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewSupervisorOptionModel objExitInterviewSupervisorOptionModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewSupervisorOptionModel = new ExitInterviewSupervisorOptionModel();
                        objExitInterviewSupervisorOptionModel.SupOptionID = Convert.ToInt32(sqlDataReader["SupOptionID"].ToString());
                        objExitInterviewSupervisorOptionModel.SupOptionName = Convert.ToString(sqlDataReader["SupOptionName"]);
                        objExitInterviewSupervisorOptionList.Add(objExitInterviewSupervisorOptionModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewSupervisorOptionList;
        }

        public List<ExitInterviewRatingOptionModel> GetExitInterviewRatingOptionList()
        {
            List<ExitInterviewRatingOptionModel> objExitInterviewRatingOptionList = new List<ExitInterviewRatingOptionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewRatingOptionList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewRatingOptionModel objExitInterviewRatingOptionModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewRatingOptionModel = new ExitInterviewRatingOptionModel();
                        objExitInterviewRatingOptionModel.RatingOptionID = Convert.ToInt32(sqlDataReader["RatingOptionID"].ToString());
                        objExitInterviewRatingOptionModel.RatingOptionName = Convert.ToString(sqlDataReader["RatingOptionName"]);
                        objExitInterviewRatingOptionList.Add(objExitInterviewRatingOptionModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewRatingOptionList;
        }

        public List<ExitInterviewJobOptionModel> GetExitInterviewJobOptionsList()
        {
            List<ExitInterviewJobOptionModel> objExitInterviewJobOptionList = new List<ExitInterviewJobOptionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewJobOptionsList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewJobOptionModel objExitInterviewJobOptionModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewJobOptionModel = new ExitInterviewJobOptionModel();
                        objExitInterviewJobOptionModel.JobOptionID = Convert.ToInt32(sqlDataReader["JobOptionID"].ToString());
                        objExitInterviewJobOptionModel.JobOptionName = Convert.ToString(sqlDataReader["JobOptionName"]);
                        objExitInterviewJobOptionList.Add(objExitInterviewJobOptionModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewJobOptionList;
        }


        public List<ExitInterviewServiceOptionModel> GetExitInterviewServiceOptionsList()
        {
            List<ExitInterviewServiceOptionModel> objExitInterviewServiceOptionList = new List<ExitInterviewServiceOptionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewServiceOptionsList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewServiceOptionModel objExitInterviewServiceOptionModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewServiceOptionModel = new ExitInterviewServiceOptionModel();
                        objExitInterviewServiceOptionModel.ServiceOptionID = Convert.ToInt32(sqlDataReader["ServiceOptionID"].ToString());
                        objExitInterviewServiceOptionModel.ServiceOptionName = Convert.ToString(sqlDataReader["ServiceOptionName"]);
                        objExitInterviewServiceOptionList.Add(objExitInterviewServiceOptionModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewServiceOptionList;
        }
        #endregion


        public ExitInterviewRequestModel GetExitInterviewRequest(int FormProcessID)
        {
            ExitInterviewRequestModel objExitInterviewRequestModel = new ExitInterviewRequestModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewRequestModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExitInterviewRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        objExitInterviewRequestModel.OtherOptionsToStay = Convert.ToString(sqlDataReader["OtherOptionsToStay"]);
                        objExitInterviewRequestModel.OtherBenefitsOffer = Convert.ToString(sqlDataReader["OtherBenefitsOffer"]);
                        objExitInterviewRequestModel.JobLeastInterest = Convert.ToString(sqlDataReader["JobLeastInterest"]);
                        objExitInterviewRequestModel.JobMostInterest = Convert.ToString(sqlDataReader["JobMostInterest"]);
                        objExitInterviewRequestModel.NewJobGoodCompany = Convert.ToString(sqlDataReader["NewJobGoodCompany"]);
                        objExitInterviewRequestModel.NewSalaryAndPosition = Convert.ToString(sqlDataReader["NewSalaryAndPosition"]);
                        objExitInterviewRequestModel.GroupRecommendation = Convert.ToString(sqlDataReader["GroupRecommendation"]);
                        objExitInterviewRequestModel.ShareOtherThoughts = Convert.ToString(sqlDataReader["ShareOtherThoughts"]);
                        objExitInterviewRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objExitInterviewRequestModel.EmployeeFullName = Convert.ToString(sqlDataReader["EmployeeFullName"]);
                        objExitInterviewRequestModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        objExitInterviewRequestModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        objExitInterviewRequestModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"]);
                        objExitInterviewRequestModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"].ToString());
                        objExitInterviewRequestModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        objExitInterviewRequestModel.EmailID = Convert.ToString(sqlDataReader["WorkEmail"]);
                        objExitInterviewRequestModel.MobileNo = Convert.ToString(sqlDataReader["MobileNumberHome"]);
                        objExitInterviewRequestModel.DefaultNationalityID = Convert.ToInt32(sqlDataReader["DefaultNationalityID"].ToString());
                        objExitInterviewRequestModel.NationalityName = Convert.ToString(sqlDataReader["NationalityName"]);
                        objExitInterviewRequestModel.SuperviserID = Convert.ToInt32(sqlDataReader["SuperviserID"].ToString());
                        objExitInterviewRequestModel.SupervisorFullName = Convert.ToString(sqlDataReader["SupervisorFullName"]);
                        objExitInterviewRequestModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"]);
                        objExitInterviewRequestModel.CreatedOn = sqlDataReader["CreatedOn"]==DBNull.Value?DateTime.Today: Convert.ToDateTime(sqlDataReader["CreatedOn"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewRequestModel;
        }


        #region Option Detail List
        public List<ExitInterviewReasonDetailsModel> GetExitInterviewReasonDetailList(int ID)
        {
            List<ExitInterviewReasonDetailsModel> objExitInterviewReasonDetailList = new List<ExitInterviewReasonDetailsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewReasonOptionDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewReasonDetailsModel objExitInterviewReasonDetailsModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewReasonDetailsModel = new ExitInterviewReasonDetailsModel();
                        objExitInterviewReasonDetailsModel.ReasonDetailID = Convert.ToInt32(sqlDataReader["ReasonDetailID"].ToString());
                        objExitInterviewReasonDetailsModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExitInterviewReasonDetailsModel.PrimaryReasonID = Convert.ToInt32(sqlDataReader["PrimaryReasonID"].ToString());
                        objExitInterviewReasonDetailList.Add(objExitInterviewReasonDetailsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewReasonDetailList;
        }

        public List<ExitInterviewSupervisorDetailsModel> GetExitInterviewSupervisorDetailList(int ID)
        {
            List<ExitInterviewSupervisorDetailsModel> objExitInterviewSupervisorDetailList = new List<ExitInterviewSupervisorDetailsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewSupervisorOptionDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewSupervisorDetailsModel objExitInterviewSupervisorDetailsModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewSupervisorDetailsModel = new ExitInterviewSupervisorDetailsModel();
                        objExitInterviewSupervisorDetailsModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExitInterviewSupervisorDetailsModel.SupervisorDetailID = Convert.ToInt32(sqlDataReader["SupervisorDetailID"].ToString());
                        objExitInterviewSupervisorDetailsModel.RatingOptionID = Convert.ToInt32(sqlDataReader["RatingOptionID"].ToString());
                        objExitInterviewSupervisorDetailsModel.SupOptionID = Convert.ToInt32(sqlDataReader["SupOptionID"].ToString());
                        objExitInterviewSupervisorDetailList.Add(objExitInterviewSupervisorDetailsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewSupervisorDetailList;
        }

        public List<ExitInterviewJobDetailsModel> GetExitInterviewJobDetailList(int ID)
        {
            List<ExitInterviewJobDetailsModel> objExitInterviewJobDetailList = new List<ExitInterviewJobDetailsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewJobOptionDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewJobDetailsModel objExitInterviewJobDetailsModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewJobDetailsModel = new ExitInterviewJobDetailsModel();
                        objExitInterviewJobDetailsModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExitInterviewJobDetailsModel.JobDetailID = Convert.ToInt32(sqlDataReader["JobDetailID"].ToString());
                        objExitInterviewJobDetailsModel.RatingOptionID = Convert.ToInt32(sqlDataReader["RatingOptionID"].ToString());
                        objExitInterviewJobDetailsModel.JobOptionID = Convert.ToInt32(sqlDataReader["JobOptionID"].ToString());
                        objExitInterviewJobDetailList.Add(objExitInterviewJobDetailsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewJobDetailList;
        }

        public List<ExitInterviewServiceDetailsModel> GetExitInterviewServiceDetailList(int ID)
        {
            List<ExitInterviewServiceDetailsModel> objExitInterviewServiceDetailList = new List<ExitInterviewServiceDetailsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewServiceOptionDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ExitInterviewServiceDetailsModel objExitInterviewServiceDetailsModel;
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewServiceDetailsModel = new ExitInterviewServiceDetailsModel();
                        objExitInterviewServiceDetailsModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objExitInterviewServiceDetailsModel.ServiceDetailID = Convert.ToInt32(sqlDataReader["ServiceDetailID"].ToString());
                        objExitInterviewServiceDetailsModel.RatingOptionID = Convert.ToInt32(sqlDataReader["RatingOptionID"].ToString());
                        objExitInterviewServiceDetailsModel.ServiceOptionID = Convert.ToInt32(sqlDataReader["ServiceOptionID"].ToString());
                        objExitInterviewServiceDetailList.Add(objExitInterviewServiceDetailsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewServiceDetailList;
        }
        #endregion

        public OperationDetails SaveExitInterviewRequest(ExitInterviewRequestModel objExitInterviewRequestModel, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable supervisorTable = new DataTable();
                supervisorTable.Columns.Add("DetailID");
                supervisorTable.Columns.Add("ID");
                supervisorTable.Columns.Add("OptionID");
                supervisorTable.Columns.Add("RatingID");

                foreach (ExitInterviewSupervisorDetailsModel supervisorDetails in objExitInterviewRequestModel.ExitInterviewSupervisorDetailsList)
                {
                    supervisorTable.Rows.Add(
                        supervisorDetails.SupervisorDetailID,
                        supervisorDetails.ID,
                        supervisorDetails.SupOptionID,
                        supervisorDetails.RatingOptionID
                        );
                }

                DataTable jobTable = new DataTable();
                jobTable.Columns.Add("DetailID");
                jobTable.Columns.Add("ID");
                jobTable.Columns.Add("OptionID");
                jobTable.Columns.Add("RatingID");
                foreach (ExitInterviewJobDetailsModel jobDetails in objExitInterviewRequestModel.ExitInterviewJobDetailsList)
                {
                    jobTable.Rows.Add(
                        jobDetails.JobDetailID,
                        jobDetails.ID,
                        jobDetails.JobOptionID,
                        jobDetails.RatingOptionID
                        );
                }

                DataTable serviceTable = new DataTable();
                serviceTable.Columns.Add("DetailID");
                serviceTable.Columns.Add("ID");
                serviceTable.Columns.Add("OptionID");
                serviceTable.Columns.Add("RatingID");
                foreach (ExitInterviewServiceDetailsModel serviceDetails in objExitInterviewRequestModel.ExitInterviewServiceDetailsList)
                {
                    serviceTable.Rows.Add(
                        serviceDetails.ServiceDetailID,
                        serviceDetails.ID,
                        serviceDetails.ServiceOptionID,
                        serviceDetails.RatingOptionID
                        );
                }

                DataTable reasonTable = new DataTable();
                reasonTable.Columns.Add("ReasonDetailID");
                reasonTable.Columns.Add("ID");
                reasonTable.Columns.Add("PrimaryReasonID");
                foreach (ExitInterviewReasonDetailsModel reasonDetails in objExitInterviewRequestModel.ExitInterviewReasonDetailsList)
                {
                    reasonTable.Rows.Add(
                        reasonDetails.ReasonDetailID,
                        reasonDetails.ID,
                        reasonDetails.PrimaryReasonID
                        );
                }


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveExitInterviewRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@formProcessID", objExitInterviewRequestModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objExitInterviewRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objExitInterviewRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@OtherOptionsToStay", objExitInterviewRequestModel.OtherOptionsToStay);
                sqlCommand.Parameters.AddWithValue("@OtherBenefitsOffer", objExitInterviewRequestModel.OtherBenefitsOffer);
                sqlCommand.Parameters.AddWithValue("@JobLeastInterest", objExitInterviewRequestModel.JobLeastInterest);
                sqlCommand.Parameters.AddWithValue("@JobMostInterest", objExitInterviewRequestModel.JobMostInterest);
                sqlCommand.Parameters.AddWithValue("@NewJobGoodCompany", objExitInterviewRequestModel.NewJobGoodCompany);
                sqlCommand.Parameters.AddWithValue("@NewSalaryAndPosition", objExitInterviewRequestModel.NewSalaryAndPosition);
                sqlCommand.Parameters.AddWithValue("@GroupRecommendation", objExitInterviewRequestModel.GroupRecommendation);
                sqlCommand.Parameters.AddWithValue("@ShareOtherThoughts", objExitInterviewRequestModel.ShareOtherThoughts);
                sqlCommand.Parameters.AddWithValue("@ExitInterviewReasonDetail", reasonTable);
                sqlCommand.Parameters.AddWithValue("@ExitInterviewSupervisorDetail", supervisorTable);

                sqlCommand.Parameters.AddWithValue("@ExitInterviewJobDetail", jobTable);
                sqlCommand.Parameters.AddWithValue("@ExitInterviewServiceDetail", serviceTable);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Request updated successfully";
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails CreateExitInterviewRequest(SeparationRequestFormModel separationRequestFormModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {              
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CreateExitInterviewRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", separationRequestFormModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", separationRequestFormModel.CompanyID);
                //sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", separationRequestFormModel.RequesterEmployeeID);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully";
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }     

        public ExitInterviewRequestModel GetExitInterviewEmployeeDetails(int formProcessID)
        {
            ExitInterviewRequestModel objExitInterviewRequestModel = new ExitInterviewRequestModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetExitInterviewEmployeeDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objExitInterviewRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        objExitInterviewRequestModel.EmployeeFullName = Convert.ToString(sqlDataReader["EmployeeFullName"]);
                        objExitInterviewRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objExitInterviewRequestModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        objExitInterviewRequestModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        objExitInterviewRequestModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"]);
                        objExitInterviewRequestModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"].ToString());
                        objExitInterviewRequestModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        objExitInterviewRequestModel.EmailID = Convert.ToString(sqlDataReader["WorkEmail"]);
                        objExitInterviewRequestModel.MobileNo = Convert.ToString(sqlDataReader["MobileNumberHome"]);
                        objExitInterviewRequestModel.DefaultNationalityID = Convert.ToInt32(sqlDataReader["DefaultNationalityID"].ToString());
                        objExitInterviewRequestModel.NationalityName = Convert.ToString(sqlDataReader["NationalityName"]);
                        objExitInterviewRequestModel.SuperviserID = Convert.ToInt32(sqlDataReader["SuperviserID"].ToString());
                        objExitInterviewRequestModel.SupervisorFullName = Convert.ToString(sqlDataReader["SupervisorFullName"]);
                        objExitInterviewRequestModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"]);
                        objExitInterviewRequestModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objExitInterviewRequestModel;
        }

        #region Exit Interview Status Report
        public List<ExitInterviewStatusReportModel> GetExitInterviewStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<ExitInterviewStatusReportModel> exitInterviewStatusReportList = null;
            try
            {
                exitInterviewStatusReportList = new List<ExitInterviewStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_ExitInterviewStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ExitInterviewStatusReportModel exitInterviewStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        exitInterviewStatusReportModel = new ExitInterviewStatusReportModel();
                        exitInterviewStatusReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        exitInterviewStatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        exitInterviewStatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        exitInterviewStatusReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        exitInterviewStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        exitInterviewStatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        exitInterviewStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        exitInterviewStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        exitInterviewStatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        exitInterviewStatusReportList.Add(exitInterviewStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return exitInterviewStatusReportList;
        }
        #endregion
    }
}
