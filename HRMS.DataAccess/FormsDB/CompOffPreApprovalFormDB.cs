﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities.Forms;
using System.Data;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Globalization;

namespace HRMS.DataAccess.FormsDB
{
    public class CompOffPreApprovalFormDB : FormsDB
    {
        public List<RequestFormsProcessModel> SaveForm(CompOffPreApprovalModel compOffPreApprovalModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveCompOffPreApproval", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@employeeId", compOffPreApprovalModel.EmployeeID);
                //sqlCommand.Parameters.AddWithValue("@workingDate",DateTime.ParseExact(compOffPreApprovalModel.WorkingDate, HRMSDateFormat, CultureInfo.InvariantCulture));  
                sqlCommand.Parameters.AddWithValue("@workingDate", GeneralDB.CommonDB.SetCulturedDate(compOffPreApprovalModel.WorkingDate));
                sqlCommand.Parameters.AddWithValue("@CreatedBy", compOffPreApprovalModel.CreatedBy);              
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", compOffPreApprovalModel.ModifiedBy);              
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", compOffPreApprovalModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", compOffPreApprovalModel.Comments);
                sqlCommand.Parameters.AddWithValue("@CompanyId", compOffPreApprovalModel.CompanyID);              
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }
        public CompOffPreApprovalModel GetFormDetails(int formProcessID)
        {           
            CompOffPreApprovalModel compOffPreApprovalModel = new CompOffPreApprovalModel();
            compOffPreApprovalModel.FormProcessID = formProcessID;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetCompOffPreApprovalRequestFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();               

                if (reader.HasRows)
                {                  
                    while (reader.Read())
                    {
                        compOffPreApprovalModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        compOffPreApprovalModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        compOffPreApprovalModel.RequestStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        compOffPreApprovalModel.WorkingDate= DateTime.Parse(reader["WorkingDate"] == DBNull.Value ? "" : reader["WorkingDate"].ToString()).ToString(HRMSDateFormat);
                        compOffPreApprovalModel.AdditionalWorkingHours = decimal.Parse(reader["AdditionalHours"] == DBNull.Value ? "0" : reader["AdditionalHours"].ToString());
                        compOffPreApprovalModel.TimeSheetFileID = int.Parse(reader["TimeSheetFileID"] == DBNull.Value ? "0" : reader["TimeSheetFileID"].ToString());
                        compOffPreApprovalModel.TimeSheetFileName = Convert.ToString(reader["TimeSheetFileName"] == DBNull.Value ? "" : reader["TimeSheetFileName"].ToString());
                        compOffPreApprovalModel.RequesterEmployeeID = int.Parse(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        compOffPreApprovalModel.CompanyID = int.Parse(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        compOffPreApprovalModel.Comments = Convert.ToString(reader["Comments"] == DBNull.Value ? "" : reader["Comments"].ToString());
                        compOffPreApprovalModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.MinValue.ToString() : reader["CreatedOn"].ToString());
                        compOffPreApprovalModel.RequestID = Convert.ToInt32(reader["RequestId"] == DBNull.Value ? "0" : reader["RequestId"].ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
               
                List<AllFormsFilesModel> allfiles = new List<AllFormsFilesModel>();
                allfiles.Add(allFormsFilesDB.GetAllFormsFiles(compOffPreApprovalModel.TimeSheetFileID));
                compOffPreApprovalModel.AllFormsFilesModelList = allfiles;
                compOffPreApprovalModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                compOffPreApprovalModel.ApproverComments = GetPendingFormsApproval(formProcessID).Comments;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return compOffPreApprovalModel;
        }
        public int UpdateForm(CompOffPreApprovalModel compOffPreApprovalModel,bool isHrTeam)
        {
            var requestFormsProcessModel = new RequestFormsProcessModel();
            var result = 0;
            try
            {             
                DataTable filestable = ConvertFilesListToTable(compOffPreApprovalModel.AllFormsFilesModelList);               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateCompOffPreApprovalWorkingHoursAndTimeSheet", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@workingDate", DateTime.ParseExact(compOffPreApprovalModel.WorkingDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@isHrTeam", isHrTeam);
                sqlCommand.Parameters.AddWithValue("@formInstanceId", compOffPreApprovalModel.ID);               
                sqlCommand.Parameters.AddWithValue("@additionalHours", compOffPreApprovalModel.AdditionalWorkingHours);
                sqlCommand.Parameters.AddWithValue("@timeSheetFileID", compOffPreApprovalModel.TimeSheetFileID);               
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", compOffPreApprovalModel.ModifiedBy);               
                sqlCommand.Parameters.AddWithValue("@FormProcessId", compOffPreApprovalModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@Comments", compOffPreApprovalModel.Comments);               
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                result = (int)Output.Value;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["result"] == DBNull.Value ? "0" : reader["result"].ToString());
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return result;
        }

        public OperationDetails SaveEligibleCompOffHours(int formProcessID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveEligibleCompOffHours", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessID);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public List<RequestFormsApproveModel> GetAllFormsApproval(int formProcessID)
        {
            List<RequestFormsApproveModel> allFormsApprovalList = new List<RequestFormsApproveModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllRequestFormsApprovalByProcessId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                        FormsWorkflowGroupModel formsWorkflowGroupModel = new FormsWorkflowGroupModel();
                        requestFormsApproveModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproveModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproveModel.ApproverEmployeeID = Convert.ToInt32(reader["ApproverEmployeeID"] == DBNull.Value ? "0" : reader["ApproverEmployeeID"].ToString());
                        requestFormsApproveModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        requestFormsApproveModel.Comments = reader["Comments"].ToString();
                        formsWorkflowGroupModel.IsITGroup = bool.Parse(reader["IsITGroup"] == DBNull.Value ? "0" : reader["IsITGroup"].ToString());
                        formsWorkflowGroupModel.CompOffHRGroup = bool.Parse(reader["CompOffHRGroup"] == DBNull.Value ? "0" : reader["CompOffHRGroup"].ToString());
                        requestFormsApproveModel.FormsWorkflowGroupModel = formsWorkflowGroupModel;
                        allFormsApprovalList.Add(requestFormsApproveModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return allFormsApprovalList;
        }

        #region Compensatory Status Report
        public List<CompensatoryStatusReportModel> GetCompensatoryStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int userId)
        {
            List<CompensatoryStatusReportModel> listStatusReportModel = null;
            try
            {
                listStatusReportModel = new List<CompensatoryStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetCompensatoryStatusReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@CurrentStatusID", CurrentStatusID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CompensatoryStatusReportModel StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        StatusReportModel = new CompensatoryStatusReportModel();
                        StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);
                        StatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        StatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        StatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        StatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        StatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        StatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        StatusReportModel.AttendanceDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["AttendanceDate"] == DBNull.Value ? "" : sqlDataReader["AttendanceDate"]));
                        StatusReportModel.CompensatoryHours = Convert.ToDecimal(sqlDataReader["CompensatoryHours"] == DBNull.Value ? "0" : sqlDataReader["CompensatoryHours"].ToString());
                        StatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        StatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        StatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        listStatusReportModel.Add(StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listStatusReportModel;
        }
        #endregion

        #region CompOff Balance Report
        public List<CompOffBalanceReportModel> GetCompOffBalanceReport(int? RequestID, int? CompanyID, int? DepartmentID, string FromDate, string ToDate, int userId)
        {
            List<CompOffBalanceReportModel> listStatusReportModel = null;
            try
            {
                listStatusReportModel = new List<CompOffBalanceReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetCompOffBalanceExpiryReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);               
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CompOffBalanceReportModel StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        StatusReportModel = new CompOffBalanceReportModel();
                        StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);                      
                        StatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        StatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        StatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        StatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        StatusReportModel.JoiningDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["JoiningDate"] == DBNull.Value ? "" : sqlDataReader["JoiningDate"]));
                        StatusReportModel.CompOffBalance = Convert.ToDecimal(sqlDataReader["CompOffBalance"] == DBNull.Value ? "0" : sqlDataReader["CompOffBalance"].ToString());
                        StatusReportModel.Email = Convert.ToString(sqlDataReader["Email"] == DBNull.Value ? "" : sqlDataReader["Email"]);
                        StatusReportModel.ApprovedDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ApprovedDate"] == DBNull.Value ? "" : sqlDataReader["ApprovedDate"]));
                        StatusReportModel.ExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ExpiryDate"] == DBNull.Value ? "" : sqlDataReader["ExpiryDate"]));
                        listStatusReportModel.Add(StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listStatusReportModel;
        }
        #endregion

        #region Check Comp Off load validation for Edit/Update from All Request
        public CompOffPreApprovalModel CheckCompOffLeaveRequestLoadValidation(int? employeeId, int? id)
        {
            CompOffPreApprovalModel compOffPreApprovalModel = new CompOffPreApprovalModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AllRequestCompOffLeaveRequestLoadValidation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        compOffPreApprovalModel.IsValid = Convert.ToBoolean(reader["IsValid"] == DBNull.Value ? "0" : reader["IsValid"].ToString());
                        compOffPreApprovalModel.ValidationMessage = Convert.ToString(reader["ValidationMessage"] == DBNull.Value ? "" : reader["ValidationMessage"].ToString());
                    }
                }
                sqlConnection.Close();              
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return compOffPreApprovalModel;
        }
        #endregion
    }
}
