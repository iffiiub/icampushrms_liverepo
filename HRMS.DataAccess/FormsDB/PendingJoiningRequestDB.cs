﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;
using System.Globalization;

namespace HRMS.DataAccess.FormsDB
{
    public class PendingJoiningRequestDB :BaseDB
    {
        public List<PendingJoiningRequestModel> GetAllPendingRequest(int? EmployeeId, int? CategoryId, int? companyId, int UserId)
        {
            List<PendingJoiningRequestModel> joiningList = null;
            try
            {
                joiningList = new List<PendingJoiningRequestModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPendingJoiningRequests", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@CategoryId", CategoryId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@UserID", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                int i = 0;
                if (sqlDataReader.HasRows)
                {
                    PendingJoiningRequestModel joiningModel;
                    while (sqlDataReader.Read())
                    {
                        joiningModel = new PendingJoiningRequestModel();
                        joiningModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        joiningModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        joiningModel.EmployeeName = sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString();
                        //joiningModel.Name = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();
                        joiningModel.DepartmentId = Convert.ToInt32(sqlDataReader["DepartmentID"] == DBNull.Value?"0": sqlDataReader["DepartmentID"].ToString());
                        joiningModel.DepartmentName = sqlDataReader["DepartmentName_1"] == DBNull.Value ? "" : sqlDataReader["DepartmentName_1"].ToString();
                        joiningModel.UserTypeId = Convert.ToInt32(sqlDataReader["UserTypeID"] == DBNull.Value ? "0" : sqlDataReader["UserTypeID"].ToString());
                        joiningModel.UserTypeName = sqlDataReader["usertypename"] == DBNull.Value ? "" : sqlDataReader["usertypename"].ToString();
                        joiningModel.JoiningDate= HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HireDate"].ToString());
                        joiningModel.Organization = sqlDataReader["Organization"] == DBNull.Value ? "" : sqlDataReader["Organization"].ToString();
                        joiningModel.Designation = sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"].ToString();
                        joiningModel.autoID = i++;
                        //academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        joiningList.Add(joiningModel);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return joiningList;

        }
        public List<Employee> GetAllPendingJoiningListEmployee(int UserID,int CompanyID)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllJoiningPendingEmployees", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FullName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        #region Employee Joining Status Report
        public List<EmployeeJoiningStatusReportModel> GetEmployeeJoiningStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<EmployeeJoiningStatusReportModel> employeeJoiningStatusReportList = null;
            try
            {
                employeeJoiningStatusReportList = new List<EmployeeJoiningStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_EmployeeJoiningStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeJoiningStatusReportModel employeeJoiningStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        employeeJoiningStatusReportModel = new EmployeeJoiningStatusReportModel();
                        employeeJoiningStatusReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        employeeJoiningStatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        employeeJoiningStatusReportModel.Requester = Convert.ToString(sqlDataReader["Requester"] == DBNull.Value ? "" : sqlDataReader["Requester"]);
                        employeeJoiningStatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        employeeJoiningStatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        employeeJoiningStatusReportModel.RequesterID = Convert.ToString(sqlDataReader["RequesterID"] == DBNull.Value ? "" : sqlDataReader["RequesterID"]);
                        employeeJoiningStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeJoiningStatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        employeeJoiningStatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        employeeJoiningStatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        employeeJoiningStatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        employeeJoiningStatusReportModel.ExpectedJoiningDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ExpectedJoiningDate"] == DBNull.Value ? "" : sqlDataReader["ExpectedJoiningDate"]));
                        employeeJoiningStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        employeeJoiningStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);                       
                        employeeJoiningStatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        employeeJoiningStatusReportList.Add(employeeJoiningStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeJoiningStatusReportList;
        }
        #endregion
    }
}
