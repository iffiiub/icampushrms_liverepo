﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess.FormsDB
{
    public class EmployeeProfileCreationFormDB : FormsDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PickList> GetRequestList(int reqStatusID)
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetR1FormRequestList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", reqStatusID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["RequestID"].ToString()), text = Convert.ToString(sqlDataReader["RequestName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public EmployeeProfileCreationFormModel GetRequestDetailList(int reqStatusID, int? requestID)
        {
            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetR1FormRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", reqStatusID);
                sqlCommand.Parameters.AddWithValue("@RequestID", requestID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmployeeProfileCreationFormModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"].ToString());
                        objEmployeeProfileCreationFormModel.EmploymentModeId = Convert.ToInt32(sqlDataReader["EmploymentModeId"].ToString());
                        objEmployeeProfileCreationFormModel.UserTypeID = Convert.ToInt32(sqlDataReader["UserTypeID"].ToString());
                        objEmployeeProfileCreationFormModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objEmployeeProfileCreationFormModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        objEmployeeProfileCreationFormModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        objEmployeeProfileCreationFormModel.EmployeeJobCategoryID = Convert.ToInt32(sqlDataReader["EmployeeJobCategoryID"].ToString());
                        objEmployeeProfileCreationFormModel.ProjectData = Convert.ToString(sqlDataReader["ProjectData"]);
                        objEmployeeProfileCreationFormModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"].ToString());
                        objEmployeeProfileCreationFormModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        objEmployeeProfileCreationFormModel.JobGradeID = Convert.ToInt32(sqlDataReader["JobGradeID"].ToString());
                        objEmployeeProfileCreationFormModel.ContractStatus = Convert.ToString(sqlDataReader["ContractStatus"]);
                        objEmployeeProfileCreationFormModel.FamilySpouse = Convert.ToString(sqlDataReader["FamilySpouse"]);
                        objEmployeeProfileCreationFormModel.AnnualAirTicket = Convert.ToBoolean(sqlDataReader["AnnualAirTicket"]);
                        objEmployeeProfileCreationFormModel.AirfareFrequencyID = Convert.ToInt32(sqlDataReader["AirfareFrequencyID"].ToString());
                        objEmployeeProfileCreationFormModel.AirfareClassID = Convert.ToInt32(sqlDataReader["AirfareClassID"].ToString());
                        objEmployeeProfileCreationFormModel.HealthInsurance = Convert.ToBoolean(sqlDataReader["HealthInsurance"].ToString());
                        objEmployeeProfileCreationFormModel.LifeInsurance = Convert.ToBoolean(sqlDataReader["LifeInsurance"].ToString());
                        objEmployeeProfileCreationFormModel.DivisionID = Convert.ToInt32(sqlDataReader["DivisionID"].ToString());
                        objEmployeeProfileCreationFormModel.RecCategoryID = Convert.ToInt32(sqlDataReader["RecCategoryID"].ToString());
                        //objEmployeeProfileCreationFormModel.PDRPFormID = Convert.ToInt32(sqlDataReader["PDRPFormID"].ToString());
                        objEmployeeProfileCreationFormModel.PDRPFormID = !string.IsNullOrEmpty(sqlDataReader["PDRPFormID"].ToString()) ? Convert.ToInt32(sqlDataReader["PDRPFormID"].ToString()) : (int?)null;
                        objEmployeeProfileCreationFormModel.InsuranceCategory = Convert.ToInt32(sqlDataReader["InsuranceCategory"].ToString());
                        objEmployeeProfileCreationFormModel.InsuranceEligibility = Convert.ToInt32(sqlDataReader["InsuranceEligibility"].ToString());
                        objEmployeeProfileCreationFormModel.Accommodation = Convert.ToBoolean(sqlDataReader["Accommodation"].ToString());
                        objEmployeeProfileCreationFormModel.AccommodationType = Convert.ToInt32(sqlDataReader["AccommodationType"].ToString());

                    }
                }

                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (objEmployeeProfileCreationFormModel.OfferLetterFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objEmployeeProfileCreationFormModel.OfferLetterFileID);
                    allFormsFilesModel.FormFileIDName = "OfferLetterFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                if (objEmployeeProfileCreationFormModel.InterviewFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objEmployeeProfileCreationFormModel.InterviewFileID);
                    allFormsFilesModel.FormFileIDName = "InterviewFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                objEmployeeProfileCreationFormModel.AllFormsFilesModelList = allFormsFilesModelList;
                objEmployeeProfileCreationFormModel.ProfileAssignAssetsModelList = GetProfileAssignAssetsDetails(objEmployeeProfileCreationFormModel.ID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objEmployeeProfileCreationFormModel;
        }

        public EmployeeProfileCreationFormModel GetProfileCreationFormRequestDetails(int reqStatusID, int formProcessID)
        {
            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeProfileCreationFormRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", reqStatusID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmployeeProfileCreationFormModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        //  objEmployeeProfileCreationFormModel.FormId = Convert.ToInt32(sqlDataReader["FormId"].ToString());
                        objEmployeeProfileCreationFormModel.EmployeeCreationRequestId = Convert.ToInt32(sqlDataReader["EmployeeCreationRequestId"].ToString());
                        objEmployeeProfileCreationFormModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"].ToString());
                        objEmployeeProfileCreationFormModel.EmploymentModeId = Convert.ToInt32(sqlDataReader["EmploymentModeId"].ToString());
                        objEmployeeProfileCreationFormModel.UserTypeID = Convert.ToInt32(sqlDataReader["UserTypeID"].ToString());
                        objEmployeeProfileCreationFormModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objEmployeeProfileCreationFormModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        objEmployeeProfileCreationFormModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        objEmployeeProfileCreationFormModel.EmployeeJobCategoryID = Convert.ToInt32(sqlDataReader["EmployeeJobCategoryID"].ToString());
                        objEmployeeProfileCreationFormModel.ProjectData = Convert.ToString(sqlDataReader["ProjectData"]);
                        objEmployeeProfileCreationFormModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"].ToString());
                        objEmployeeProfileCreationFormModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        objEmployeeProfileCreationFormModel.JobGradeID = Convert.ToInt32(sqlDataReader["JobGradeID"].ToString());
                        objEmployeeProfileCreationFormModel.ContractStatus = Convert.ToString(sqlDataReader["ContractStatus"]);
                        objEmployeeProfileCreationFormModel.FamilySpouse = Convert.ToString(sqlDataReader["FamilySpouse"]);
                        objEmployeeProfileCreationFormModel.AnnualAirTicket = Convert.ToBoolean(sqlDataReader["AnnualAirTicket"]);
                        objEmployeeProfileCreationFormModel.AirfareFrequencyID = Convert.ToInt32(sqlDataReader["AirfareFrequencyID"].ToString());
                        objEmployeeProfileCreationFormModel.AirfareClassID = Convert.ToInt32(sqlDataReader["AirfareClassID"].ToString());
                        objEmployeeProfileCreationFormModel.HealthInsurance = Convert.ToBoolean(sqlDataReader["HealthInsurance"].ToString());
                        objEmployeeProfileCreationFormModel.LifeInsurance = Convert.ToBoolean(sqlDataReader["LifeInsurance"].ToString());
                        objEmployeeProfileCreationFormModel.DivisionID = Convert.ToInt32(sqlDataReader["DivisionID"].ToString());
                        objEmployeeProfileCreationFormModel.FirstName = Convert.ToString(sqlDataReader["FirstName"]);
                        objEmployeeProfileCreationFormModel.MiddleName = Convert.ToString(sqlDataReader["MiddleName"]);
                        objEmployeeProfileCreationFormModel.SurName = Convert.ToString(sqlDataReader["SurName"]);
                        objEmployeeProfileCreationFormModel.JoiningDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["JoiningDate"].ToString());
                        objEmployeeProfileCreationFormModel.SalaryBasisID = Convert.ToInt32(sqlDataReader["SalaryBasisID"].ToString());
                        objEmployeeProfileCreationFormModel.CostCenter = Convert.ToString(sqlDataReader["CostCenter"]);
                        objEmployeeProfileCreationFormModel.CostCenterCode = Convert.ToString(sqlDataReader["CostCenterCode"]);
                        objEmployeeProfileCreationFormModel.LocationCode = Convert.ToString(sqlDataReader["LocationCode"]);
                        objEmployeeProfileCreationFormModel.OfficeLocation = Convert.ToString(sqlDataReader["OfficeLocation"]);
                        objEmployeeProfileCreationFormModel.ProbationPeriod = Convert.ToInt32(sqlDataReader["ProbationPeriod"].ToString());
                        objEmployeeProfileCreationFormModel.SupervisorID = Convert.ToInt32(sqlDataReader["SupervisorID"].ToString());
                        objEmployeeProfileCreationFormModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"].ToString());
                        objEmployeeProfileCreationFormModel.CityID = Convert.ToInt32(sqlDataReader["CityID"].ToString());
                        objEmployeeProfileCreationFormModel.AirportListID = Convert.ToInt32(sqlDataReader["AirportListID"].ToString());
                        objEmployeeProfileCreationFormModel.LeaveEntitleDaysID = Convert.ToInt32(sqlDataReader["LeaveEntitleDaysID"].ToString());
                        objEmployeeProfileCreationFormModel.LeaveEntitleTypeID = Convert.ToInt32(sqlDataReader["LeaveEntitleTypeID"].ToString());
                        objEmployeeProfileCreationFormModel.NationalityID = Convert.ToString(sqlDataReader["NationalityID"]);
                        objEmployeeProfileCreationFormModel.DefaultNationalityID = Convert.ToInt32(sqlDataReader["DefaultNationalityID"].ToString());
                        objEmployeeProfileCreationFormModel.PassportNo = Convert.ToString(sqlDataReader["PassportNo"]);
                        objEmployeeProfileCreationFormModel.PassportIssueDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PassportIssueDate"].ToString());
                        objEmployeeProfileCreationFormModel.PassportExpiryDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PassportExpiryDate"].ToString());
                        objEmployeeProfileCreationFormModel.DOB = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DOB"].ToString());
                        objEmployeeProfileCreationFormModel.BirthCountryID = Convert.ToInt32(sqlDataReader["BirthCountryID"].ToString());
                        objEmployeeProfileCreationFormModel.BirthCityID = Convert.ToInt32(sqlDataReader["BirthCityID"].ToString());
                        objEmployeeProfileCreationFormModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
                        objEmployeeProfileCreationFormModel.ReligionID = Convert.ToInt32(sqlDataReader["ReligionID"].ToString());
                        objEmployeeProfileCreationFormModel.LanguageID = Convert.ToInt32(sqlDataReader["LanguageID"].ToString());
                        objEmployeeProfileCreationFormModel.MotherName = Convert.ToString(sqlDataReader["MotherName"]);
                        objEmployeeProfileCreationFormModel.FatherName = Convert.ToString(sqlDataReader["FatherName"]);
                        objEmployeeProfileCreationFormModel.MobileNumber = Convert.ToString(sqlDataReader["MobileNumber"]);
                        objEmployeeProfileCreationFormModel.LandlinePhone = Convert.ToString(sqlDataReader["LandlinePhone"]);
                        objEmployeeProfileCreationFormModel.Extension = Convert.ToString(sqlDataReader["Extension"]);
                        objEmployeeProfileCreationFormModel.InterviewFileID = Convert.ToInt32(sqlDataReader["InterviewFileID"].ToString());
                        objEmployeeProfileCreationFormModel.OfferLetterFileID = Convert.ToInt32(sqlDataReader["OfferLetterFileID"].ToString());
                        objEmployeeProfileCreationFormModel.WorkEmailID = Convert.ToString(sqlDataReader["WorkEmailID"]);
                        objEmployeeProfileCreationFormModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"].ToString());
                        objEmployeeProfileCreationFormModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"].ToString());
                        objEmployeeProfileCreationFormModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString());
                        objEmployeeProfileCreationFormModel.NoticedPeriod = Convert.ToInt32(sqlDataReader["NoticedPeriod"].ToString());
                        objEmployeeProfileCreationFormModel.HRContractTypeID = Convert.ToInt32(sqlDataReader["HRContractTypeID"].ToString());
                        objEmployeeProfileCreationFormModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        objEmployeeProfileCreationFormModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : sqlDataReader["CreatedOn"].ToString());
                        objEmployeeProfileCreationFormModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        objEmployeeProfileCreationFormModel.JobStatusID = Convert.ToInt32(sqlDataReader["JobStatusID"].ToString());
                        objEmployeeProfileCreationFormModel.RecCategoryID = Convert.ToInt32(sqlDataReader["RecCategoryID"].ToString());
                        objEmployeeProfileCreationFormModel.MaritalStatusID = Convert.ToInt16(sqlDataReader["MaritalStatusID"].ToString());
                        objEmployeeProfileCreationFormModel.PDRPFormID = Convert.ToInt32(sqlDataReader["PDRPFormID"].ToString());
                        objEmployeeProfileCreationFormModel.InsuranceCategory = Convert.ToInt32(sqlDataReader["InsuranceCategory"].ToString());
                        objEmployeeProfileCreationFormModel.InsuranceEligibility = Convert.ToInt32(sqlDataReader["InsuranceEligibility"].ToString());
                        objEmployeeProfileCreationFormModel.Accommodation = Convert.ToBoolean(sqlDataReader["Accommodation"].ToString());
                        objEmployeeProfileCreationFormModel.AccommodationType = Convert.ToInt32(sqlDataReader["AccommodationType"].ToString());
                        objEmployeeProfileCreationFormModel.IsReleased = Convert.ToBoolean(sqlDataReader["IsReleased"].ToString());
                        objEmployeeProfileCreationFormModel.R1ReleasedOn = !string.IsNullOrEmpty(sqlDataReader["R1ReleasedOn"].ToString()) ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["R1ReleasedOn"].ToString()) : "";
                        objEmployeeProfileCreationFormModel.R1ReleasedBy = !string.IsNullOrEmpty(sqlDataReader["R1ReleasedBy"].ToString()) ? Convert.ToInt32(sqlDataReader["R1ReleasedBy"].ToString()) : (int?)null;
                        objEmployeeProfileCreationFormModel.R1ReleasedByName = Convert.ToString(sqlDataReader["R1ReleasedByName"]);
                        objEmployeeProfileCreationFormModel.R1ReleaseComments = Convert.ToString(sqlDataReader["R1ReleaseComments"]);
                        objEmployeeProfileCreationFormModel.R1ReleaserCompanyName = Convert.ToString(sqlDataReader["R1ReleaserCompanyName"]);


                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (objEmployeeProfileCreationFormModel.OfferLetterFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objEmployeeProfileCreationFormModel.OfferLetterFileID);
                    allFormsFilesModel.FormFileIDName = "OfferLetterFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                if (objEmployeeProfileCreationFormModel.InterviewFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objEmployeeProfileCreationFormModel.InterviewFileID);
                    allFormsFilesModel.FormFileIDName = "InterviewFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                objEmployeeProfileCreationFormModel.AllFormsFilesModelList = allFormsFilesModelList;
                objEmployeeProfileCreationFormModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                objEmployeeProfileCreationFormModel.ProfileAssignAssetsModelList = GetProfileAssignAssetsDetails(objEmployeeProfileCreationFormModel.ID);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objEmployeeProfileCreationFormModel;
        }

        public EmployeeProfileCreationFormModel GetEmployeeProfileCreationLogDetail(int ID)
        {
            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeProfileCreationLogDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmployeeProfileCreationFormModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        objEmployeeProfileCreationFormModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"].ToString());
                        objEmployeeProfileCreationFormModel.EmploymentModeId = Convert.ToInt32(sqlDataReader["EmploymentModeId"].ToString());
                        objEmployeeProfileCreationFormModel.UserTypeID = Convert.ToInt32(sqlDataReader["UserTypeID"].ToString());
                        objEmployeeProfileCreationFormModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        objEmployeeProfileCreationFormModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        objEmployeeProfileCreationFormModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"].ToString());
                        objEmployeeProfileCreationFormModel.EmployeeJobCategoryID = Convert.ToInt32(sqlDataReader["EmployeeJobCategoryID"].ToString());
                        objEmployeeProfileCreationFormModel.ProjectData = Convert.ToString(sqlDataReader["ProjectData"]);
                        objEmployeeProfileCreationFormModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"]);
                        objEmployeeProfileCreationFormModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        objEmployeeProfileCreationFormModel.JobGradeID = Convert.ToInt32(sqlDataReader["JobGradeID"].ToString());
                        objEmployeeProfileCreationFormModel.ContractStatus = Convert.ToString(sqlDataReader["ContractStatus"]);
                        objEmployeeProfileCreationFormModel.FamilySpouse = Convert.ToString(sqlDataReader["FamilySpouse"]);
                        objEmployeeProfileCreationFormModel.AnnualAirTicket = Convert.ToBoolean(sqlDataReader["AnnualAirTicket"]);
                        objEmployeeProfileCreationFormModel.AirfareFrequencyID = Convert.ToInt32(sqlDataReader["AirfareFrequencyID"].ToString());
                        objEmployeeProfileCreationFormModel.AirfareClassID = Convert.ToInt32(sqlDataReader["AirfareClassID"].ToString());
                        objEmployeeProfileCreationFormModel.HealthInsurance = Convert.ToBoolean(sqlDataReader["HealthInsurance"].ToString());
                        objEmployeeProfileCreationFormModel.LifeInsurance = Convert.ToBoolean(sqlDataReader["LifeInsurance"].ToString());
                        objEmployeeProfileCreationFormModel.DivisionID = Convert.ToInt32(sqlDataReader["DivisionID"].ToString());

                        objEmployeeProfileCreationFormModel.FirstName = Convert.ToString(sqlDataReader["FirstName"]);
                        objEmployeeProfileCreationFormModel.MiddleName = Convert.ToString(sqlDataReader["MiddleName"]);
                        objEmployeeProfileCreationFormModel.SurName = Convert.ToString(sqlDataReader["SurName"]);
                        objEmployeeProfileCreationFormModel.JoiningDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["JoiningDate"].ToString());
                        objEmployeeProfileCreationFormModel.SalaryBasisID = Convert.ToInt32(sqlDataReader["SalaryBasisID"].ToString());
                        objEmployeeProfileCreationFormModel.CostCenter = Convert.ToString(sqlDataReader["CostCenter"]);
                        objEmployeeProfileCreationFormModel.CostCenterCode = Convert.ToString(sqlDataReader["CostCenterCode"]);
                        objEmployeeProfileCreationFormModel.LocationCode = Convert.ToString(sqlDataReader["LocationCode"]);
                        objEmployeeProfileCreationFormModel.OfficeLocation = Convert.ToString(sqlDataReader["OfficeLocation"]);
                        objEmployeeProfileCreationFormModel.ProbationPeriod = Convert.ToInt32(sqlDataReader["ProbationPeriod"].ToString());
                        objEmployeeProfileCreationFormModel.SupervisorID = Convert.ToInt32(sqlDataReader["SupervisorID"].ToString());
                        objEmployeeProfileCreationFormModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"].ToString());
                        objEmployeeProfileCreationFormModel.CityID = Convert.ToInt32(sqlDataReader["CityID"].ToString());
                        objEmployeeProfileCreationFormModel.AirportListID = Convert.ToInt32(sqlDataReader["AirportListID"].ToString());
                        objEmployeeProfileCreationFormModel.LeaveEntitleDaysID = Convert.ToInt32(sqlDataReader["LeaveEntitleDaysID"].ToString());
                        objEmployeeProfileCreationFormModel.LeaveEntitleTypeID = Convert.ToInt32(sqlDataReader["LeaveEntitleTypeID"].ToString());
                        objEmployeeProfileCreationFormModel.NationalityID = Convert.ToString(sqlDataReader["NationalityID"]);
                        objEmployeeProfileCreationFormModel.DefaultNationalityID = Convert.ToInt32(sqlDataReader["DefaultNationalityID"].ToString());
                        objEmployeeProfileCreationFormModel.PassportNo = Convert.ToString(sqlDataReader["PassportNo"]);
                        objEmployeeProfileCreationFormModel.PassportIssueDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PassportIssueDate"].ToString());
                        objEmployeeProfileCreationFormModel.PassportExpiryDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PassportExpiryDate"].ToString());
                        objEmployeeProfileCreationFormModel.DOB = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DOB"].ToString());
                        objEmployeeProfileCreationFormModel.BirthCountryID = Convert.ToInt32(sqlDataReader["BirthCountryID"].ToString());
                        objEmployeeProfileCreationFormModel.BirthCityID = Convert.ToInt32(sqlDataReader["BirthCityID"].ToString());
                        objEmployeeProfileCreationFormModel.GenderID = Convert.ToInt32(sqlDataReader["GenderID"].ToString());
                        objEmployeeProfileCreationFormModel.ReligionID = Convert.ToInt32(sqlDataReader["ReligionID"].ToString());
                        objEmployeeProfileCreationFormModel.LanguageID = Convert.ToInt32(sqlDataReader["LanguageID"].ToString());
                        objEmployeeProfileCreationFormModel.MotherName = Convert.ToString(sqlDataReader["MotherName"]);
                        objEmployeeProfileCreationFormModel.FatherName = Convert.ToString(sqlDataReader["FatherName"]);
                        objEmployeeProfileCreationFormModel.MobileNumber = Convert.ToString(sqlDataReader["MobileNumber"]);
                        objEmployeeProfileCreationFormModel.LandlinePhone = Convert.ToString(sqlDataReader["LandlinePhone"]);
                        objEmployeeProfileCreationFormModel.Extension = Convert.ToString(sqlDataReader["Extension"]);
                        objEmployeeProfileCreationFormModel.WorkEmailID = Convert.ToString(sqlDataReader["WorkEmailID"]);
                        objEmployeeProfileCreationFormModel.NoticedPeriod = Convert.ToInt32(sqlDataReader["NoticedPeriod"].ToString());
                        objEmployeeProfileCreationFormModel.HRContractTypeID = Convert.ToInt32(sqlDataReader["HRContractTypeID"].ToString());

                    }
                }

                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (objEmployeeProfileCreationFormModel.OfferLetterFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objEmployeeProfileCreationFormModel.OfferLetterFileID);
                    allFormsFilesModel.FormFileIDName = "OfferLetterFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                if (objEmployeeProfileCreationFormModel.InterviewFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(objEmployeeProfileCreationFormModel.InterviewFileID);
                    allFormsFilesModel.FormFileIDName = "InterviewFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                objEmployeeProfileCreationFormModel.AllFormsFilesModelList = allFormsFilesModelList;
                objEmployeeProfileCreationFormModel.ProfileAssignAssetsModelList = GetProfileAssignAssetsDetails(objEmployeeProfileCreationFormModel.ID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objEmployeeProfileCreationFormModel;
        }

        public RequestFormsProcessModel SaveEmployeeProfileCreationForm(EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel, int modifiedBy)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                DataTable allFiles = ConvertFilesListToTable(objEmployeeProfileCreationFormModel.AllFormsFilesModelList);

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveEmployeeProfileCreationForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", objEmployeeProfileCreationFormModel.RequestID);
                sqlCommand.Parameters.AddWithValue("@FormId", objEmployeeProfileCreationFormModel.FormId);
                sqlCommand.Parameters.AddWithValue("@FirstName", objEmployeeProfileCreationFormModel.FirstName);
                sqlCommand.Parameters.AddWithValue("@MiddleName", objEmployeeProfileCreationFormModel.MiddleName);
                sqlCommand.Parameters.AddWithValue("@SurName", objEmployeeProfileCreationFormModel.SurName);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", DateTime.ParseExact(objEmployeeProfileCreationFormModel.JoiningDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@SalaryBasisID", objEmployeeProfileCreationFormModel.SalaryBasisID);
                sqlCommand.Parameters.AddWithValue("@CostCenter", objEmployeeProfileCreationFormModel.CostCenter);
                sqlCommand.Parameters.AddWithValue("@CostCenterCode", objEmployeeProfileCreationFormModel.CostCenterCode);
                sqlCommand.Parameters.AddWithValue("@LocationCode", objEmployeeProfileCreationFormModel.LocationCode);
                sqlCommand.Parameters.AddWithValue("@OfficeLocation", objEmployeeProfileCreationFormModel.OfficeLocation);
                sqlCommand.Parameters.AddWithValue("@ProbationPeriod", objEmployeeProfileCreationFormModel.ProbationPeriod);
                sqlCommand.Parameters.AddWithValue("@SupervisorID", objEmployeeProfileCreationFormModel.SupervisorID);
                sqlCommand.Parameters.AddWithValue("@CountryID", objEmployeeProfileCreationFormModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@CityID", objEmployeeProfileCreationFormModel.CityID);
                sqlCommand.Parameters.AddWithValue("@AirportListID", objEmployeeProfileCreationFormModel.AirportListID);
                sqlCommand.Parameters.AddWithValue("@LeaveEntitleDaysID", objEmployeeProfileCreationFormModel.LeaveEntitleDaysID);
                sqlCommand.Parameters.AddWithValue("@LeaveEntitleTypeID", objEmployeeProfileCreationFormModel.LeaveEntitleTypeID);
                sqlCommand.Parameters.AddWithValue("@NationalityID", objEmployeeProfileCreationFormModel.NationalityID);
                sqlCommand.Parameters.AddWithValue("@DefaultNationalityID", objEmployeeProfileCreationFormModel.DefaultNationalityID);
                sqlCommand.Parameters.AddWithValue("@PassportNo", objEmployeeProfileCreationFormModel.PassportNo);
                sqlCommand.Parameters.AddWithValue("@PassportIssueDate", DateTime.ParseExact(objEmployeeProfileCreationFormModel.PassportIssueDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@PassportExpiryDate", DateTime.ParseExact(objEmployeeProfileCreationFormModel.PassportExpiryDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@DOB", objEmployeeProfileCreationFormModel.DOB != null ? DateTime.ParseExact(objEmployeeProfileCreationFormModel.DOB,HRMSDateFormat, CultureInfo.InvariantCulture) : (object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@BirthCountryID", objEmployeeProfileCreationFormModel.BirthCountryID);
                sqlCommand.Parameters.AddWithValue("@BirthCityID", objEmployeeProfileCreationFormModel.BirthCityID);
                sqlCommand.Parameters.AddWithValue("@GenderID", objEmployeeProfileCreationFormModel.GenderID);
                sqlCommand.Parameters.AddWithValue("@ReligionID", objEmployeeProfileCreationFormModel.ReligionID);
                sqlCommand.Parameters.AddWithValue("@LanguageID", objEmployeeProfileCreationFormModel.LanguageID);
                sqlCommand.Parameters.AddWithValue("@MotherName", objEmployeeProfileCreationFormModel.MotherName);
                sqlCommand.Parameters.AddWithValue("@FatherName", objEmployeeProfileCreationFormModel.FatherName);
                sqlCommand.Parameters.AddWithValue("@MobileNumber", objEmployeeProfileCreationFormModel.MobileNumber);
                sqlCommand.Parameters.AddWithValue("@LandlinePhone", objEmployeeProfileCreationFormModel.LandlinePhone);
                sqlCommand.Parameters.AddWithValue("@MaritalStatusID", objEmployeeProfileCreationFormModel.MaritalStatusID);
                sqlCommand.Parameters.AddWithValue("@Extension", objEmployeeProfileCreationFormModel.Extension);
                sqlCommand.Parameters.AddWithValue("@InterviewFileID", objEmployeeProfileCreationFormModel.InterviewFileID);
                sqlCommand.Parameters.AddWithValue("@OfferLetterFileID", objEmployeeProfileCreationFormModel.OfferLetterFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", objEmployeeProfileCreationFormModel.ManPowerFileID);
                sqlCommand.Parameters.AddWithValue("@WorkEmailID", objEmployeeProfileCreationFormModel.WorkEmailID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objEmployeeProfileCreationFormModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@NoticedPeriod", objEmployeeProfileCreationFormModel.NoticedPeriod);
                sqlCommand.Parameters.AddWithValue("@HRContractTypeID", objEmployeeProfileCreationFormModel.HRContractTypeID);
                sqlCommand.Parameters.AddWithValue("@Comments", objEmployeeProfileCreationFormModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", allFiles);
                //Employee Details
                sqlCommand.Parameters.AddWithValue("@EmployeeAlternativeID", objEmployeeProfileCreationFormModel.EmployeeAlternativeID);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", objEmployeeProfileCreationFormModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", objEmployeeProfileCreationFormModel.EmploymentModeId);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", objEmployeeProfileCreationFormModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", objEmployeeProfileCreationFormModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", objEmployeeProfileCreationFormModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", objEmployeeProfileCreationFormModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", objEmployeeProfileCreationFormModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", objEmployeeProfileCreationFormModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", objEmployeeProfileCreationFormModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", objEmployeeProfileCreationFormModel.ProjectData);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", objEmployeeProfileCreationFormModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@InsuranceCategory", objEmployeeProfileCreationFormModel.InsuranceCategory);
                sqlCommand.Parameters.AddWithValue("@InsuranceEligibility", objEmployeeProfileCreationFormModel.InsuranceEligibility);
                sqlCommand.Parameters.AddWithValue("@Accommodation", objEmployeeProfileCreationFormModel.Accommodation);
                sqlCommand.Parameters.AddWithValue("@AccommodationType", objEmployeeProfileCreationFormModel.AccommodationType);
                sqlCommand.Parameters.AddWithValue("@PositionTitle", objEmployeeProfileCreationFormModel.PositionTitle);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", objEmployeeProfileCreationFormModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@JobGradeID", objEmployeeProfileCreationFormModel.JobGradeID);
                sqlCommand.Parameters.AddWithValue("@DivisionID", objEmployeeProfileCreationFormModel.DivisionID);
                sqlCommand.Parameters.AddWithValue("@PositionID", objEmployeeProfileCreationFormModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@JobStatusID", objEmployeeProfileCreationFormModel.JobStatusID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", objEmployeeProfileCreationFormModel.RecCategoryID > 0 ? objEmployeeProfileCreationFormModel.RecCategoryID : (object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@PDRPFormID", objEmployeeProfileCreationFormModel.PDRPFormID > 0 ? objEmployeeProfileCreationFormModel.PDRPFormID : (object)DBNull.Value);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModel;
        }

        public OperationDetails UpdateEmployeeProfileCreationForm(EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable allFiles = ConvertFilesListToTable(objEmployeeProfileCreationFormModel.AllFormsFilesModelList);

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateEmployeeProfileCreationForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", objEmployeeProfileCreationFormModel.ID);
                sqlCommand.Parameters.AddWithValue("@FormId", objEmployeeProfileCreationFormModel.FormId);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objEmployeeProfileCreationFormModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@FirstName", objEmployeeProfileCreationFormModel.FirstName);
                sqlCommand.Parameters.AddWithValue("@MiddleName", objEmployeeProfileCreationFormModel.MiddleName);
                sqlCommand.Parameters.AddWithValue("@SurName", objEmployeeProfileCreationFormModel.SurName);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", DateTime.ParseExact(objEmployeeProfileCreationFormModel.JoiningDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@SalaryBasisID", objEmployeeProfileCreationFormModel.SalaryBasisID);
                sqlCommand.Parameters.AddWithValue("@CostCenter", objEmployeeProfileCreationFormModel.CostCenter);
                sqlCommand.Parameters.AddWithValue("@CostCenterCode", objEmployeeProfileCreationFormModel.CostCenterCode);
                sqlCommand.Parameters.AddWithValue("@LocationCode", objEmployeeProfileCreationFormModel.LocationCode);
                sqlCommand.Parameters.AddWithValue("@OfficeLocation", objEmployeeProfileCreationFormModel.OfficeLocation);
                sqlCommand.Parameters.AddWithValue("@ProbationPeriod", objEmployeeProfileCreationFormModel.ProbationPeriod);
                sqlCommand.Parameters.AddWithValue("@SupervisorID", objEmployeeProfileCreationFormModel.SupervisorID);
                sqlCommand.Parameters.AddWithValue("@CountryID", objEmployeeProfileCreationFormModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@CityID", objEmployeeProfileCreationFormModel.CityID);
                sqlCommand.Parameters.AddWithValue("@AirportListID", objEmployeeProfileCreationFormModel.AirportListID);
                sqlCommand.Parameters.AddWithValue("@LeaveEntitleDaysID", objEmployeeProfileCreationFormModel.LeaveEntitleDaysID);
                sqlCommand.Parameters.AddWithValue("@LeaveEntitleTypeID", objEmployeeProfileCreationFormModel.LeaveEntitleTypeID);
                sqlCommand.Parameters.AddWithValue("@NationalityID", objEmployeeProfileCreationFormModel.NationalityID);
                sqlCommand.Parameters.AddWithValue("@DefaultNationalityID", objEmployeeProfileCreationFormModel.DefaultNationalityID);
                sqlCommand.Parameters.AddWithValue("@PassportNo", objEmployeeProfileCreationFormModel.PassportNo);
                sqlCommand.Parameters.AddWithValue("@PassportIssueDate", DateTime.ParseExact(objEmployeeProfileCreationFormModel.PassportIssueDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@PassportExpiryDate", DateTime.ParseExact(objEmployeeProfileCreationFormModel.PassportExpiryDate,HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@DOB", objEmployeeProfileCreationFormModel.DOB != null ? DateTime.ParseExact(objEmployeeProfileCreationFormModel.DOB,HRMSDateFormat, CultureInfo.InvariantCulture) : (object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@BirthCountryID", objEmployeeProfileCreationFormModel.BirthCountryID);
                sqlCommand.Parameters.AddWithValue("@BirthCityID", objEmployeeProfileCreationFormModel.BirthCityID);
                sqlCommand.Parameters.AddWithValue("@GenderID", objEmployeeProfileCreationFormModel.GenderID);
                sqlCommand.Parameters.AddWithValue("@ReligionID", objEmployeeProfileCreationFormModel.ReligionID);
                sqlCommand.Parameters.AddWithValue("@LanguageID", objEmployeeProfileCreationFormModel.LanguageID);
                sqlCommand.Parameters.AddWithValue("@MotherName", objEmployeeProfileCreationFormModel.MotherName);
                sqlCommand.Parameters.AddWithValue("@FatherName", objEmployeeProfileCreationFormModel.FatherName);
                sqlCommand.Parameters.AddWithValue("@MobileNumber", objEmployeeProfileCreationFormModel.MobileNumber);
                sqlCommand.Parameters.AddWithValue("@LandlinePhone", objEmployeeProfileCreationFormModel.LandlinePhone);
                sqlCommand.Parameters.AddWithValue("@MaritalStatusID", objEmployeeProfileCreationFormModel.MaritalStatusID);
                sqlCommand.Parameters.AddWithValue("@Extension", objEmployeeProfileCreationFormModel.Extension);
                sqlCommand.Parameters.AddWithValue("@InterviewFileID", objEmployeeProfileCreationFormModel.InterviewFileID);
                sqlCommand.Parameters.AddWithValue("@OfferLetterFileID", objEmployeeProfileCreationFormModel.OfferLetterFileID);
                sqlCommand.Parameters.AddWithValue("@WorkEmailID", objEmployeeProfileCreationFormModel.WorkEmailID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objEmployeeProfileCreationFormModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@NoticedPeriod", objEmployeeProfileCreationFormModel.NoticedPeriod);
                sqlCommand.Parameters.AddWithValue("@HRContractTypeID", objEmployeeProfileCreationFormModel.HRContractTypeID);
                sqlCommand.Parameters.AddWithValue("@Comments", objEmployeeProfileCreationFormModel.Comments);
                //Employee Details
                sqlCommand.Parameters.AddWithValue("@EmployeeAlternativeID", objEmployeeProfileCreationFormModel.EmployeeAlternativeID);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", objEmployeeProfileCreationFormModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", objEmployeeProfileCreationFormModel.EmploymentModeId);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", objEmployeeProfileCreationFormModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", objEmployeeProfileCreationFormModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", objEmployeeProfileCreationFormModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", objEmployeeProfileCreationFormModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", objEmployeeProfileCreationFormModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", objEmployeeProfileCreationFormModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", objEmployeeProfileCreationFormModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", objEmployeeProfileCreationFormModel.ProjectData);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", objEmployeeProfileCreationFormModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@PositionTitle", objEmployeeProfileCreationFormModel.PositionTitle);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", objEmployeeProfileCreationFormModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@JobGradeID", objEmployeeProfileCreationFormModel.JobGradeID);
                sqlCommand.Parameters.AddWithValue("@DivisionID", objEmployeeProfileCreationFormModel.DivisionID);
                sqlCommand.Parameters.AddWithValue("@PositionID", objEmployeeProfileCreationFormModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@JobStatusID", objEmployeeProfileCreationFormModel.JobStatusID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", objEmployeeProfileCreationFormModel.RecCategoryID > 0 ? objEmployeeProfileCreationFormModel.RecCategoryID : (object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@PDRPFormID", objEmployeeProfileCreationFormModel.PDRPFormID > 0 ? objEmployeeProfileCreationFormModel.PDRPFormID : (object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@AllFiles", allFiles);
                sqlCommand.Parameters.AddWithValue("@InsuranceCategory", objEmployeeProfileCreationFormModel.InsuranceCategory);
                sqlCommand.Parameters.AddWithValue("@InsuranceEligibility", objEmployeeProfileCreationFormModel.InsuranceEligibility);
                sqlCommand.Parameters.AddWithValue("@Accommodation", objEmployeeProfileCreationFormModel.Accommodation);
                sqlCommand.Parameters.AddWithValue("@AccommodationType", objEmployeeProfileCreationFormModel.AccommodationType);
                //IsUpdateDetailMode is 1 for Re initialization & Update Details, for Update by IT OR Hr Group should set to 0
                sqlCommand.Parameters.AddWithValue("@IsUpdateDetailMode", objEmployeeProfileCreationFormModel.IsUpdateDetailMode); 
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails UpdateEmployeeIdToEmployeeProfileCreationForm(int ID, int EmployeeID, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateEmployeeIdToEmployeeProfileCreationForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails UpdateLineMangerOfEmployee(int EmployeeID, int LineMangerId, int modifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateLineMangerOfEmployee", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@LineMangerId", LineMangerId);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public OperationDetails UpdateProfileAssignAssets(List<ProfileAssignAssetsModel> ProfileAssignAssetsModelList, int Id)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("AssetsID");
                resultTable.Columns.Add("AssetsTypeID");
                resultTable.Columns.Add("ModelName");
                resultTable.Columns.Add("ModelNumber");
                resultTable.Columns.Add("SerialNumber");
                resultTable.Columns.Add("IssueDate", System.Type.GetType("System.DateTime"));
                foreach (ProfileAssignAssetsModel profileAssignAssets in ProfileAssignAssetsModelList)
                {
                    resultTable.Rows.Add(
                        profileAssignAssets.AssetsID,
                        profileAssignAssets.AssetsTypeID,
                        profileAssignAssets.ModelName,
                        profileAssignAssets.ModelNumber,
                        profileAssignAssets.SerialNumber,
                        DateTime.ParseExact(profileAssignAssets.IssueDate,HRMSDateFormat, CultureInfo.InvariantCulture)
                        );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateProfileAssignAssets", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", Id);
                sqlCommand.Parameters.AddWithValue("@AssetsDetailList", resultTable);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public List<ProfileAssignAssetsModel> GetProfileAssignAssetsDetails(int ID)
        {
            List<ProfileAssignAssetsModel> objProfileAssignAssetsModelList = new List<ProfileAssignAssetsModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetProfileAssignAssetsDetails", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ProfileAssignAssetsModel objProfileAssignAssetsModel;
                    while (sqlDataReader.Read())
                    {
                        objProfileAssignAssetsModel = new ProfileAssignAssetsModel();
                        objProfileAssignAssetsModel.AssetsID = Convert.ToInt32(sqlDataReader["AssetsID"].ToString());
                        objProfileAssignAssetsModel.AssetsTypeID = Convert.ToInt32(sqlDataReader["AssetsTypeID"].ToString());
                        objProfileAssignAssetsModel.ModelName = Convert.ToString(sqlDataReader["ModelName"]);
                        objProfileAssignAssetsModel.ModelNumber = Convert.ToString(sqlDataReader["ModelNumber"]);
                        objProfileAssignAssetsModel.SerialNumber = Convert.ToString(sqlDataReader["SerialNumber"]);
                        objProfileAssignAssetsModel.IssueDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["IssueDate"].ToString());
                        objProfileAssignAssetsModelList.Add(objProfileAssignAssetsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objProfileAssignAssetsModelList;
        }

        public List<PickList> GetSubCategory()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetSubCategoryList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["SubCategoryID"].ToString()), text = Convert.ToString(sqlDataReader["SubCategoryName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetJobGradeList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetJobGradeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["JobGradeID"].ToString()), text = Convert.ToString(sqlDataReader["JobGradeName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetAirPortByCountryId(int countryId)
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAirPortByCountryId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);
                sqlCommand.Parameters.AddWithValue("@CountryID", countryId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["AirportListID"].ToString()), text = Convert.ToString(sqlDataReader["AirportName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetDivisionList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetDivisionList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["DivisionID"].ToString()), text = Convert.ToString(sqlDataReader["DivisionName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetSalaryBasisFrequencyList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetSalaryBasisFrequencyList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["SalaryBasisID"].ToString()), text = Convert.ToString(sqlDataReader["SalaryBasisName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public OperationDetails IsEmployeeAlternativeIDExists(string employeeAlternativeID, int profileRequestInstanceID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_IsEmployeeAlternativeIDExists", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeAlternativeID", employeeAlternativeID);
                sqlCommand.Parameters.AddWithValue("@ProfileRequestInstanceID", profileRequestInstanceID);
               // sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", userID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 1000);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }

        public List<PickList> GetLeaveEntitlementDaysList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeaveEntitlementDaysList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["LeaveEntitleDaysID"].ToString()), text = Convert.ToString(sqlDataReader["LeaveEntitleDays"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetLeaveEntitlementTypeList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetLeaveEntitlementTypeList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["LeaveEntitleTypeID"].ToString()), text = Convert.ToString(sqlDataReader["LeaveEntitlePeriod"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetAssetTypesList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAssetTypesList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["AssetsTypeID"].ToString()), text = Convert.ToString(sqlDataReader["AssetsTypeName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public List<PickList> GetJobStatusList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_Gen_JobStatusList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["JobStatusID"].ToString()), text = Convert.ToString(sqlDataReader["JobStatusName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }
        public List<PickList> GetPDRPFormsList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PDRPGetPerformanceGroups", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["PerformanceGroupID"].ToString()), text = Convert.ToString(sqlDataReader["PerformanceGroupName"]) });
                    }
                }
                objPickList.Insert(0, new PickList() { id = 0, text = "N/A" });
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }
        public List<PickList> GetRecruitCategoryList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetRecruitCategoryList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["RecCategoryID"].ToString()), text = Convert.ToString(sqlDataReader["RecCategoryName"]) });
                    }
                }
              //  objPickList.Insert(0, new PickList() { id = 0, text = "N/A" });
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        #region Employee Profile Status Report
        public List<EmployeeProfileStatusReportModel> GetEmployeeProfileStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<EmployeeProfileStatusReportModel> employeeProfileStatusReportList = null;
            try
            {
                employeeProfileStatusReportList = new List<EmployeeProfileStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_EmployeeProfileStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeProfileStatusReportModel employeeProfileStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        employeeProfileStatusReportModel = new EmployeeProfileStatusReportModel();
                        employeeProfileStatusReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        employeeProfileStatusReportModel.R1ReferenceNo = Convert.ToInt32(sqlDataReader["R1ReferenceNo"] == DBNull.Value ? "" : sqlDataReader["R1ReferenceNo"]);
                        employeeProfileStatusReportModel.Requester = Convert.ToString(sqlDataReader["Requester"] == DBNull.Value ? "" : sqlDataReader["Requester"]);
                        employeeProfileStatusReportModel.RequestDate = CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        employeeProfileStatusReportModel.EmployeeProfile = Convert.ToInt32(sqlDataReader["EmployeeProfile"] == DBNull.Value ? "" : sqlDataReader["EmployeeProfile"]);
                        employeeProfileStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeProfileStatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        employeeProfileStatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        employeeProfileStatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        employeeProfileStatusReportModel.ExpectedJoiningDate = CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ExpectedJoiningDate"] == DBNull.Value ? "" : sqlDataReader["ExpectedJoiningDate"]));
                        employeeProfileStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        employeeProfileStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        employeeProfileStatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        employeeProfileStatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "" : sqlDataReader["FormProcessID"]);
                        employeeProfileStatusReportList.Add(employeeProfileStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeProfileStatusReportList;
        }
        #endregion

        #region Employee Master Report
        public List<EmployeeMasterReportModel> GetEmployeeMasterReportData(int? companyId, int? employeeId, int? departmentId, bool? isActive, int userId)
        {
            List<EmployeeMasterReportModel> employeeMasterReportList = null;
            try
            {
                employeeMasterReportList = new List<EmployeeMasterReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_EmployeeMasterReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@IsActive", isActive);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeMasterReportModel employeeMasterReportModel;
                    while (sqlDataReader.Read())
                    {
                        employeeMasterReportModel = new EmployeeMasterReportModel();
                        employeeMasterReportModel.BusinessUnit = Convert.ToString(sqlDataReader["BusinessUnit"] == DBNull.Value ? "" : sqlDataReader["BusinessUnit"]);
                        employeeMasterReportModel.OperatingUnitID = Convert.ToString(sqlDataReader["OperatingUnitID"] == DBNull.Value ? "" : sqlDataReader["OperatingUnitID"]);
                        employeeMasterReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        employeeMasterReportModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        employeeMasterReportModel.MiddleName = Convert.ToString(sqlDataReader["MiddleName"] == DBNull.Value ? "" : sqlDataReader["MiddleName"]);
                        employeeMasterReportModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        employeeMasterReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeMasterReportModel.EmployeeType = Convert.ToString(sqlDataReader["EmployeeType"] == DBNull.Value ? "" : sqlDataReader["EmployeeType"]);
                        employeeMasterReportModel.ExpectedJoiningDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ExpectedJoiningDate"] == DBNull.Value ? "" : sqlDataReader["ExpectedJoiningDate"]));
                       // employeeMasterReportModel.ExpectedJoiningDate = Convert.ToString(sqlDataReader["ExpectedJoiningDate"] == DBNull.Value ? "" : sqlDataReader["ExpectedJoiningDate"]);
                        employeeMasterReportModel.HireDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["HireDate"] == DBNull.Value ? "" : sqlDataReader["HireDate"]));
                        employeeMasterReportModel.ProbationMonths = Convert.ToString(sqlDataReader["ProbationMonths"] == DBNull.Value ? "" : sqlDataReader["ProbationMonths"]);
                        employeeMasterReportModel.NoticedPeriod = Convert.ToString(sqlDataReader["NoticedPeriod"] == DBNull.Value ? "" : sqlDataReader["NoticedPeriod"]);
                        employeeMasterReportModel.Religion = Convert.ToString(sqlDataReader["Religion"] == DBNull.Value ? "" : sqlDataReader["Religion"]);
                        employeeMasterReportModel.DOB = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DOB"] == DBNull.Value ? "" : sqlDataReader["DOB"]));
                        employeeMasterReportModel.BirthCity = Convert.ToString(sqlDataReader["BirthCity"] == DBNull.Value ? "" : sqlDataReader["BirthCity"]);
                        employeeMasterReportModel.BirthCountry = Convert.ToString(sqlDataReader["BirthCountry"] == DBNull.Value ? "" : sqlDataReader["BirthCountry"]);
                        employeeMasterReportModel.Citizenship = Convert.ToString(sqlDataReader["Citizenship"] == DBNull.Value ? "" : sqlDataReader["Citizenship"]);
                        employeeMasterReportModel.MaritalStatus = Convert.ToString(sqlDataReader["MaritalStatus"] == DBNull.Value ? "" : sqlDataReader["MaritalStatus"]);
                        employeeMasterReportModel.Gender = Convert.ToString(sqlDataReader["Gender"] == DBNull.Value ? "" : sqlDataReader["Gender"]);
                        employeeMasterReportModel.BloodGroup = Convert.ToString(sqlDataReader["BloodGroup"] == DBNull.Value ? "" : sqlDataReader["BloodGroup"]);
                        employeeMasterReportModel.MotherName = Convert.ToString(sqlDataReader["MotherName"] == DBNull.Value ? "" : sqlDataReader["MotherName"]);
                        employeeMasterReportModel.PositionOfferLetter = Convert.ToString(sqlDataReader["PositionOfferLetter"] == DBNull.Value ? "" : sqlDataReader["PositionOfferLetter"]);
                        employeeMasterReportModel.PositionLabourContract = Convert.ToString(sqlDataReader["PositionLabourContract"] == DBNull.Value ? "" : sqlDataReader["PositionLabourContract"]);
                        employeeMasterReportModel.Email = Convert.ToString(sqlDataReader["Email"] == DBNull.Value ? "" : sqlDataReader["Email"]);
                        employeeMasterReportModel.UserName = Convert.ToString(sqlDataReader["UserName"] == DBNull.Value ? "" : sqlDataReader["UserName"]);
                        employeeMasterReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        employeeMasterReportModel.DesignationTitle = Convert.ToString(sqlDataReader["DesignationTitle"] == DBNull.Value ? "" : sqlDataReader["DesignationTitle"]);
                        employeeMasterReportModel.Location = Convert.ToString(sqlDataReader["Location"] == DBNull.Value ? "" : sqlDataReader["Location"]);
                        employeeMasterReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        employeeMasterReportModel.SubCategory = Convert.ToString(sqlDataReader["SubCategory"] == DBNull.Value ? "" : sqlDataReader["SubCategory"]);
                        employeeMasterReportModel.SalaryBasis = Convert.ToString(sqlDataReader["SalaryBasis"] == DBNull.Value ? "" : sqlDataReader["SalaryBasis"]);
                        employeeMasterReportModel.FirstLineManagerOracleNo = Convert.ToString(sqlDataReader["FirstLineManagerOracleNo"] == DBNull.Value ? "" : sqlDataReader["FirstLineManagerOracleNo"]);
                        employeeMasterReportModel.FirstLineManagerName = Convert.ToString(sqlDataReader["FirstLineManagerName"] == DBNull.Value ? "" : sqlDataReader["FirstLineManagerName"]);
                        employeeMasterReportModel.FirstLineManagerEmail = Convert.ToString(sqlDataReader["FirstLineManagerEmail"] == DBNull.Value ? "" : sqlDataReader["FirstLineManagerEmail"]);
                        employeeMasterReportModel.FirstLineManagerAD = Convert.ToString(sqlDataReader["FirstLineManagerAD"] == DBNull.Value ? "" : sqlDataReader["FirstLineManagerAD"]);
                        employeeMasterReportModel.SponsorName = Convert.ToString(sqlDataReader["SponsorName"] == DBNull.Value ? "" : sqlDataReader["SponsorName"]);
                        employeeMasterReportModel.IBAN = Convert.ToString(sqlDataReader["IBAN"] == DBNull.Value ? "" : sqlDataReader["IBAN"]);
                        employeeMasterReportModel.BankName = Convert.ToString(sqlDataReader["BankName"] == DBNull.Value ? "" : sqlDataReader["BankName"]);
                        employeeMasterReportModel.AnnualLeaveEntitlement = Convert.ToString(sqlDataReader["AnnualLeaveEntitlement"] == DBNull.Value ? "" : sqlDataReader["AnnualLeaveEntitlement"]);
                        employeeMasterReportModel.AnnualLeaveCalculation = Convert.ToString(sqlDataReader["AnnualLeaveCalculation"] == DBNull.Value ? "" : sqlDataReader["AnnualLeaveCalculation"]);
                        employeeMasterReportModel.TicketEntitlement = Convert.ToString(sqlDataReader["TicketEntitlement"] == DBNull.Value ? "" : sqlDataReader["TicketEntitlement"]);
                        employeeMasterReportModel.TicketEntitlementFamily = Convert.ToString(sqlDataReader["TicketEntitlementFamily"] == DBNull.Value ? "" : sqlDataReader["TicketEntitlementFamily"]);
                        employeeMasterReportModel.TicketCapedAmount = Convert.ToString(sqlDataReader["TicketCapedAmount"] == DBNull.Value ? "" : sqlDataReader["TicketCapedAmount"]);
                        employeeMasterReportModel.TicketClass = Convert.ToString(sqlDataReader["TicketClass"] == DBNull.Value ? "" : sqlDataReader["TicketClass"]);
                        employeeMasterReportModel.TicketFrequency = Convert.ToString(sqlDataReader["TicketFrequency"] == DBNull.Value ? "" : sqlDataReader["TicketFrequency"]);
                        employeeMasterReportModel.HomeTownForTicket = Convert.ToString(sqlDataReader["HomeTownForTicket"] == DBNull.Value ? "" : sqlDataReader["HomeTownForTicket"]);
                        employeeMasterReportModel.MedicalInsurance = Convert.ToString(sqlDataReader["MedicalInsurance"] == DBNull.Value ? "" : sqlDataReader["MedicalInsurance"]);
                        employeeMasterReportModel.MedicalInsuranceFamily = Convert.ToString(sqlDataReader["MedicalInsuranceFamily"] == DBNull.Value ? "" : sqlDataReader["MedicalInsuranceFamily"]);
                        employeeMasterReportModel.ContactNumber = Convert.ToString(sqlDataReader["ContactNumber"] == DBNull.Value ? "" : sqlDataReader["ContactNumber"]);
                        employeeMasterReportModel.PassportNo = Convert.ToString(sqlDataReader["PassportNo"] == DBNull.Value ? "" : sqlDataReader["PassportNo"]);
                        employeeMasterReportModel.PassportExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["PassportExpiryDate"] == DBNull.Value ? "" : sqlDataReader["PassportExpiryDate"]));
                        employeeMasterReportModel.PassportCustody = Convert.ToString(sqlDataReader["PassportCustody"] == DBNull.Value ? "" : sqlDataReader["PassportCustody"]);
                        employeeMasterReportModel.VisaNumber = Convert.ToString(sqlDataReader["VisaNumber"] == DBNull.Value ? "" : sqlDataReader["VisaNumber"]);
                        employeeMasterReportModel.VisaExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["VisaExpiryDate"] == DBNull.Value ? "" : sqlDataReader["VisaExpiryDate"]));
                        employeeMasterReportModel.EmiratesIDNumber = Convert.ToString(sqlDataReader["EmiratesIDNumber"] == DBNull.Value ? "" : sqlDataReader["EmiratesIDNumber"]);
                        employeeMasterReportModel.EmiratesIDExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["EmiratesIDExpiryDate"] == DBNull.Value ? "" : sqlDataReader["EmiratesIDExpiryDate"]));
                        employeeMasterReportModel.LabourCardNumber = Convert.ToString(sqlDataReader["LabourCardNumber"] == DBNull.Value ? "" : sqlDataReader["LabourCardNumber"]);
                        employeeMasterReportModel.LabourCardExpiryDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["LabourCardExpiryDate"] == DBNull.Value ? "" : sqlDataReader["LabourCardExpiryDate"]));
                        employeeMasterReportModel.SecondLineManagerName = Convert.ToString(sqlDataReader["SecondLineManagerName"] == DBNull.Value ? "" : sqlDataReader["SecondLineManagerName"]);
                        employeeMasterReportModel.SecondLineManager = Convert.ToString(sqlDataReader["SecondLineManager"] == DBNull.Value ? "" : sqlDataReader["SecondLineManager"]);
                        employeeMasterReportModel.HODName = Convert.ToString(sqlDataReader["HODName"] == DBNull.Value ? "" : sqlDataReader["HODName"]);
                        employeeMasterReportModel.HODOracleNumber = Convert.ToString(sqlDataReader["HODOracleNumber"] == DBNull.Value ? "" : sqlDataReader["HODOracleNumber"]);
                        employeeMasterReportModel.BUHeadName = Convert.ToString(sqlDataReader["BUHeadName"] == DBNull.Value ? "" : sqlDataReader["BUHeadName"]);
                        employeeMasterReportModel.BUHeadOracleNumber = Convert.ToString(sqlDataReader["BUHeadOracleNumber"] == DBNull.Value ? "" : sqlDataReader["BUHeadOracleNumber"]);
                        employeeMasterReportList.Add(employeeMasterReportModel);

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeMasterReportList;
        }
        #endregion

    }
}
