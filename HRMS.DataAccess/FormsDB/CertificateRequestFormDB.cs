﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;
using System.Globalization;

namespace HRMS.DataAccess.FormsDB
{
    public class CertificateRequestFormDB : FormsDB
    {
        public List<CerficateRequestFormModel> GetAllCertificateTypes()
        {
            List<CerficateRequestFormModel> certificateList = null;
            try
            {
                certificateList = new List<CerficateRequestFormModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllCertificateTypesForms", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CerficateRequestFormModel certificateModel;
                    while (sqlDataReader.Read())
                    {
                        certificateModel = new CerficateRequestFormModel();

                        certificateModel.CertificateTypeID = Convert.ToInt16(sqlDataReader["CertificateTypeID"] == DBNull.Value ? "0" : sqlDataReader["CertificateTypeID"].ToString());
                        certificateModel.CertificateTypeName = Convert.ToString(sqlDataReader["CertificateTypeName_1"] == DBNull.Value ? "" : sqlDataReader["CertificateTypeName_1"]);
                        certificateList.Add(certificateModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return certificateList;
        }
        public List<CerficateRequestFormModel> GetAllCertificateReason()
        {
            List<CerficateRequestFormModel> certificateList = null;
            try
            {
                certificateList = new List<CerficateRequestFormModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllCertificateReasonForms", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CerficateRequestFormModel certificateModel;
                    while (sqlDataReader.Read())
                    {
                        certificateModel = new CerficateRequestFormModel();

                        certificateModel.CertificateReasonID = Convert.ToInt16(sqlDataReader["CertificateReasonID"] == DBNull.Value ? "0" : sqlDataReader["CertificateReasonID"].ToString());
                        certificateModel.CertificateReasonName = Convert.ToString(sqlDataReader["CertificateReasonName_1"] == DBNull.Value ? "" : sqlDataReader["CertificateReasonName_1"]);
                        certificateList.Add(certificateModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return certificateList;
        }
        public AllFormsFilesModel GetEmployeeDocumentById(int DocId)
        {
            AllFormsFilesModel allformsfilesModel = new AllFormsFilesModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeDocument", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@DocId", DocId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        allformsfilesModel.FileID = Convert.ToInt32(sqlDataReader["PayDocumentID"] == DBNull.Value ? "0" : sqlDataReader["PayDocumentID"].ToString());
                        allformsfilesModel.DocumentFile = sqlDataReader["DocumentFile"] == DBNull.Value ? null : (byte[])sqlDataReader["DocumentFile"];
                        allformsfilesModel.FileContentType = sqlDataReader["FileContentType"] == DBNull.Value ? "" : sqlDataReader["FileContentType"].ToString();
                        allformsfilesModel.FileName = sqlDataReader["ImageName"] == DBNull.Value ? "" : sqlDataReader["ImageName"].ToString();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
            return allformsfilesModel;
        }
        public CerficateRequestFormModel GetFileDetails(int UserId)
        {
            CerficateRequestFormModel certificateModel = new CerficateRequestFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_GetFileDetialsFromPayDocument", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    certificateModel.PassportFileID = 0;
                    certificateModel.VisaFileID = 0;
                    while (sqlDataReader.Read())
                    {
                        certificateModel.EmployeeID = int.Parse(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        certificateModel.DocumentTypeId = int.Parse(sqlDataReader["PayDocumentTypeID"] == DBNull.Value ? "0" : sqlDataReader["PayDocumentTypeID"].ToString());
                        certificateModel.PassportFileID = int.Parse(sqlDataReader["PayDocumentTypeID"] == DBNull.Value ? "0" : sqlDataReader["PayDocumentTypeID"].ToString() == "2" ? sqlDataReader["PayDocumentID"].ToString() : certificateModel.PassportFileID.ToString());
                        certificateModel.VisaFileID = int.Parse(sqlDataReader["PayDocumentTypeID"] == DBNull.Value ? "0" : sqlDataReader["PayDocumentTypeID"].ToString() == "1" ? sqlDataReader["PayDocumentID"].ToString() : certificateModel.VisaFileID.ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                CertificateRequestFormDB requestDB = new CertificateRequestFormDB();

                if (certificateModel.VisaFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = requestDB.GetEmployeeDocumentById(certificateModel.VisaFileID);
                    allFormsFilesModel.FormFileIDName = "VisaFileID";
                    if (allFormsFilesModel.DocumentFile != null)
                    {
                        allFormsFilesModelList.Add(allFormsFilesModel);
                    }


                }
                if (certificateModel.PassportFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = requestDB.GetEmployeeDocumentById(certificateModel.PassportFileID);
                    allFormsFilesModel.FormFileIDName = "PassportFileID";

                    if (allFormsFilesModel.DocumentFile != null)
                    {
                        allFormsFilesModelList.Add(allFormsFilesModel);
                    }

                }
                
                certificateModel.AllFormsFilesModelList = allFormsFilesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return certificateModel;
        }
        public AllFormsFilesModel GetAllFormsFiles(int fileID)
        {
            AllFormsFilesModel allFormsFilesModel = new AllFormsFilesModel();
            try
            {
                sqlConnection = new SqlConnection(connectionStringFileDB);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllFormsFilesByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FileID", fileID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        allFormsFilesModel = new AllFormsFilesModel();
                        allFormsFilesModel.FileID = Convert.ToInt32(reader["FileID"] == DBNull.Value ? "0" : reader["FileID"].ToString());
                        allFormsFilesModel.FileName = Convert.ToString(reader["FileName"].ToString());
                        allFormsFilesModel.DocumentFile = reader["DocumentFile"] == DBNull.Value ? null : (byte[])reader["DocumentFile"];
                        allFormsFilesModel.FileContentType = Convert.ToString(reader["FileContentType"].ToString());

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return allFormsFilesModel;
        }
        public FormsUniqueKeyModel GetFormID(int CertificateTypeId)
        {
            FormsUniqueKeyModel uniqueKeyModel = new FormsUniqueKeyModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFormIdFromUniqueKey", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CertificateTypeId", CertificateTypeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        uniqueKeyModel.FormID = Convert.ToInt16(sqlDataReader["formid"] == DBNull.Value ? "0" : sqlDataReader["formid"].ToString());
                        uniqueKeyModel.FormName = Convert.ToString(sqlDataReader["FormName"] == DBNull.Value ? "" : sqlDataReader["FormName"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
            return uniqueKeyModel;
        }
        public List<RequestFormsProcessModel> SaveForm(CerficateRequestFormModel certificateRequestModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {               
                DataTable filestable = ConvertFilesListToTable(certificateRequestModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveCertificateRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", certificateRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@CertificateTypeID", certificateRequestModel.CertificateTypeID);
                sqlCommand.Parameters.AddWithValue("@CertificateReasonID", certificateRequestModel.CertificateReasonID);
                sqlCommand.Parameters.AddWithValue("@RequestPurpose", certificateRequestModel.RequestPurpose);
                sqlCommand.Parameters.AddWithValue("@PassportFileID", certificateRequestModel.PassportFileID);
                sqlCommand.Parameters.AddWithValue("@VisaFileID", certificateRequestModel.VisaFileID);
                sqlCommand.Parameters.AddWithValue("@BankNameAddress", certificateRequestModel.BankNameAddress);
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", certificateRequestModel.ReqStatusID);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", certificateRequestModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CreatedOn", certificateRequestModel.CreatedOn);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", certificateRequestModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", certificateRequestModel.ModifiedOn);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", certificateRequestModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", certificateRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@CompanyId", certificateRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }
        public CerficateRequestFormModel GetForm(int formProcessID, int userID)
        {
            CerficateRequestFormModel certificateRequestModel = new CerficateRequestFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFormCertificateRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        certificateRequestModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        certificateRequestModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        certificateRequestModel.CertificateTypeID = Convert.ToInt16(reader["CertificateTypeID"] == DBNull.Value ? "0" : reader["CertificateTypeID"].ToString());
                        certificateRequestModel.CertificateTypeName = Convert.ToString(reader["CertificateTypeName_1"]);
                        certificateRequestModel.CertificateReasonID = Convert.ToInt16(reader["CertificateReasonID"] == DBNull.Value ? "0" : reader["CertificateReasonID"].ToString());
                        certificateRequestModel.CertificateReasonName = Convert.ToString(reader["CertificateReasonName_1"]);
                        certificateRequestModel.RequestPurpose = Convert.ToString(reader["RequestPurpose"]);
                        certificateRequestModel.BankNameAddress = Convert.ToString(reader["BankNameAddress"]);
                        certificateRequestModel.PassportFileID = int.Parse(reader["PassportFileID"] == DBNull.Value ? "0" : reader["PassportFileID"].ToString());
                        certificateRequestModel.VisaFileID = int.Parse(reader["VisaFileID"] == DBNull.Value ? "0" : reader["VisaFileID"].ToString());
                        certificateRequestModel.BankNameAddress = Convert.ToString(reader["BankNameAddress"]);
                        certificateRequestModel.Comments = Convert.ToString(reader["Comments"]);
                        certificateRequestModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        certificateRequestModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        certificateRequestModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());                     
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (certificateRequestModel.VisaFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(certificateRequestModel.VisaFileID);
                    allFormsFilesModel.FormFileIDName = "VisaFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                if (certificateRequestModel.PassportFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(certificateRequestModel.PassportFileID);
                    allFormsFilesModel.FormFileIDName = "PassportFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                certificateRequestModel.AllFormsFilesModelList = allFormsFilesModelList;
                certificateRequestModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return certificateRequestModel;
        }
        public CerficateRequestFormModel GetFormDetails(int formProcessID)
        {
            CerficateRequestFormModel certificateRequesModel = new CerficateRequestFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetCertificateRequestFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        certificateRequesModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        certificateRequesModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        certificateRequesModel.CertificateTypeID = Convert.ToInt16(reader["CertificateTypeID"] == DBNull.Value ? "0" : reader["CertificateTypeID"].ToString());
                        certificateRequesModel.CertificateTypeName = Convert.ToString(reader["CertificateTypeName_1"]);
                        certificateRequesModel.CertificateReasonID = Convert.ToInt16(reader["CertificateReasonID"] == DBNull.Value ? "0" : reader["CertificateReasonID"].ToString());
                        certificateRequesModel.CertificateReasonName = Convert.ToString(reader["CertificateReasonName_1"]);
                        certificateRequesModel.RequestPurpose = Convert.ToString(reader["RequestPurpose"]);
                        certificateRequesModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        certificateRequesModel.BankNameAddress = Convert.ToString(reader["BankNameAddress"]);
                        certificateRequesModel.PassportFileID = int.Parse(reader["PassportFileID"] == DBNull.Value ? "0" : reader["PassportFileID"].ToString());
                        certificateRequesModel.VisaFileID = int.Parse(reader["VisaFileID"] == DBNull.Value ? "0" : reader["VisaFileID"].ToString());
                        certificateRequesModel.BankNameAddress = Convert.ToString(reader["BankNameAddress"]);                       
                        certificateRequesModel.VisaFileName = reader["VisaFileName"] == DBNull.Value ? "" : reader["VisaFileName"].ToString();
                        certificateRequesModel.PassportFileName = reader["PassportFileName"] == DBNull.Value ? "" : reader["PassportFileName"].ToString();
                        certificateRequesModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());                       
                        certificateRequesModel.RequestStatusName = Convert.ToString(reader["ReqStatusName"]);
                        certificateRequesModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                        certificateRequesModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
                    }
                }
                sqlConnection.Close();
                certificateRequesModel.AllFormsFilesModelList = allFormsFilesModelList;
                certificateRequesModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return certificateRequesModel;
        }
        public int UpdateForm(CerficateRequestFormModel certificateRequestModel)
        {
            int output = 0;                       
            try
            {
                DataTable filestable = ConvertFilesListToTable(certificateRequestModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateCertificateRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", certificateRequestModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", certificateRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@CertificateReasonID", certificateRequestModel.CertificateReasonID);
                sqlCommand.Parameters.AddWithValue("@CertificateTypeID", certificateRequestModel.CertificateTypeID);
                sqlCommand.Parameters.AddWithValue("@RequestPurpose", certificateRequestModel.RequestPurpose);
                sqlCommand.Parameters.AddWithValue("@PassportFileID", certificateRequestModel.PassportFileID);
                sqlCommand.Parameters.AddWithValue("@VisaFileID", certificateRequestModel.VisaFileID);
                sqlCommand.Parameters.AddWithValue("@BankNameAddress", certificateRequestModel.BankNameAddress);                              
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", certificateRequestModel.ModifiedBy);                              
                sqlCommand.Parameters.AddWithValue("@Comments", certificateRequestModel.Comments);               
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();              
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;              
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        #region Certificate Request Status Report
        public List<CertificateRequestStatusReportModel> GetCertificateRequestStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<CertificateRequestStatusReportModel> certificateRequestStatusList = null;
            try
            {
                certificateRequestStatusList = new List<CertificateRequestStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CertificateRequestStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@Userid", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CertificateRequestStatusReportModel certificateRequestStatusModel;
                    while (sqlDataReader.Read())
                    {
                        certificateRequestStatusModel = new CertificateRequestStatusReportModel();
                        certificateRequestStatusModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        certificateRequestStatusModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        certificateRequestStatusModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        certificateRequestStatusModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        certificateRequestStatusModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        certificateRequestStatusModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        certificateRequestStatusModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        certificateRequestStatusModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        certificateRequestStatusModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        certificateRequestStatusModel.RequestedCertificateType = Convert.ToString(sqlDataReader["RequestedCertificateType"] == DBNull.Value ? "" : sqlDataReader["RequestedCertificateType"]);
                        certificateRequestStatusModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        certificateRequestStatusModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        certificateRequestStatusModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        certificateRequestStatusList.Add(certificateRequestStatusModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return certificateRequestStatusList;
        }
        #endregion
    }
}
