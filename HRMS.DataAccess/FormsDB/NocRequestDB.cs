﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class NocRequestDB : FormsDB
    {
        public List<PickList> GetAllNocRequestReason()
        {
            List<PickList> lstPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllNocRequestReason", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PickList objPickList;
                    while (sqlDataReader.Read())
                    {
                        objPickList = new PickList();
                        objPickList.id = Convert.ToInt32(sqlDataReader["NOCReasonID"].ToString());
                        objPickList.text = Convert.ToString(sqlDataReader["NOCReasonName_1"]);
                        lstPickList.Add(objPickList);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstPickList;
        }     

        public List<RequestFormsProcessModel> SaveNocRequest(NocRequestModel nocRequestModel)
        {
            FormsDB formsDB = new FormsDB();
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                DataTable filestable = formsDB.ConvertFilesListToTable(nocRequestModel.certificateRequest.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveNocRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", nocRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@NOCReasonID", nocRequestModel.NOCReasonID);
                sqlCommand.Parameters.AddWithValue("@OtherReason", nocRequestModel.OtherReason);
                sqlCommand.Parameters.AddWithValue("@CertificateAddressee", nocRequestModel.CertificateAddressee);
                sqlCommand.Parameters.AddWithValue("@CountryID", nocRequestModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@NOCLanguage", nocRequestModel.NOCLanguage);
                sqlCommand.Parameters.AddWithValue("@FreeZoneVisa", nocRequestModel.FreeZoneVisa);
                sqlCommand.Parameters.AddWithValue("@RequiredDate", CommonDB.SetCulturedDate(nocRequestModel.RequiredDate));
                sqlCommand.Parameters.AddWithValue("@DateOfTravel", CommonDB.SetCulturedDate(nocRequestModel.DateOfTravel));
                sqlCommand.Parameters.AddWithValue("@Comments", nocRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@PurposeOfVisit", nocRequestModel.PurposeOfVisit);
                sqlCommand.Parameters.AddWithValue("@TravelExpenseBorneBy", nocRequestModel.TravelExpenseBorneBy);
                sqlCommand.Parameters.AddWithValue("@PassportFileID", nocRequestModel.certificateRequest.PassportFileID);
                sqlCommand.Parameters.AddWithValue("@VisaFileID", nocRequestModel.certificateRequest.VisaFileID);
                sqlCommand.Parameters.AddWithValue("@ApprovedBusinessTravelFormId", nocRequestModel.ApprovedBusinessTravelFormId);
                sqlCommand.Parameters.AddWithValue("@SalaryDetail", nocRequestModel.SalaryDetail);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();               
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }

        public OperationDetails UpdateNocRequest(NocRequestModel nocRequestModel, int FormProcessID)
        {
            FormsDB formsDB = new FormsDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable filestable = formsDB.ConvertFilesListToTable(nocRequestModel.certificateRequest.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateNocRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", nocRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", nocRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@NOCReasonID", nocRequestModel.NOCReasonID);
                sqlCommand.Parameters.AddWithValue("@OtherReason", nocRequestModel.OtherReason);
                sqlCommand.Parameters.AddWithValue("@CertificateAddressee", nocRequestModel.CertificateAddressee);
                sqlCommand.Parameters.AddWithValue("@CountryID", nocRequestModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@NOCLanguage", nocRequestModel.NOCLanguage);
                sqlCommand.Parameters.AddWithValue("@FreeZoneVisa", nocRequestModel.FreeZoneVisa);
                sqlCommand.Parameters.AddWithValue("@RequiredDate", CommonDB.SetCulturedDate(nocRequestModel.RequiredDate));
                sqlCommand.Parameters.AddWithValue("@DateOfTravel", CommonDB.SetCulturedDate(nocRequestModel.DateOfTravel));
                sqlCommand.Parameters.AddWithValue("@Comments", nocRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@PurposeOfVisit", nocRequestModel.PurposeOfVisit);
                sqlCommand.Parameters.AddWithValue("@TravelExpenseBorneBy", nocRequestModel.TravelExpenseBorneBy);
                sqlCommand.Parameters.AddWithValue("@PassportFileID", nocRequestModel.certificateRequest.PassportFileID);
                sqlCommand.Parameters.AddWithValue("@VisaFileID", nocRequestModel.certificateRequest.VisaFileID);
                sqlCommand.Parameters.AddWithValue("@ApprovedBusinessTravelFormId", nocRequestModel.ApprovedBusinessTravelFormId);
                sqlCommand.Parameters.AddWithValue("@SalaryDetail", nocRequestModel.SalaryDetail);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.Int, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;               
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;               
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }

        public NocRequestModel GetNocRequestDetail(int formProcessID)
        {
            NocRequestModel nocRequestModel = new NocRequestModel();
            nocRequestModel.certificateRequest = new CerficateRequestFormModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetNocRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        nocRequestModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        nocRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        nocRequestModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"].ToString());
                        nocRequestModel.NOCReasonID = Convert.ToInt32(sqlDataReader["NOCReasonID"].ToString());
                        nocRequestModel.OtherReason = Convert.ToString(sqlDataReader["OtherReason"]);
                        nocRequestModel.CertificateAddressee = Convert.ToString(sqlDataReader["CertificateAddressee"]);
                        nocRequestModel.NOCLanguage = Convert.ToString(sqlDataReader["NOCLanguage"]);
                        nocRequestModel.CountryID = Convert.ToInt32(sqlDataReader["CountryID"].ToString());
                        nocRequestModel.FreeZoneVisa = Convert.ToBoolean(sqlDataReader["FreeZoneVisa"].ToString());
                        nocRequestModel.RequiredDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequiredDate"]));
                        nocRequestModel.DateOfTravel = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DateOfTravel"]));
                        nocRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        nocRequestModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        nocRequestModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"]);
                        nocRequestModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"]);
                        nocRequestModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"]);
                        nocRequestModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                        nocRequestModel.certificateRequest.PassportFileID = int.Parse(sqlDataReader["PassportFileID"] == DBNull.Value ? "0" : sqlDataReader["PassportFileID"].ToString());
                        nocRequestModel.certificateRequest.VisaFileID = int.Parse(sqlDataReader["VisaFileID"] == DBNull.Value ? "0" : sqlDataReader["VisaFileID"].ToString());
                        nocRequestModel.certificateRequest.VisaFileName = sqlDataReader["VisaFileName"] == DBNull.Value ? "" : sqlDataReader["VisaFileName"].ToString();
                        nocRequestModel.certificateRequest.PassportFileName = sqlDataReader["PassportFileName"] == DBNull.Value ? "" : sqlDataReader["PassportFileName"].ToString();
                        nocRequestModel.ApprovedBusinessTravelFormId = int.Parse(sqlDataReader["ApprovedBusinessTravelFormId"] == DBNull.Value ? "0" : sqlDataReader["ApprovedBusinessTravelFormId"].ToString());
                        nocRequestModel.ApprovedBusinessTravelFormName = sqlDataReader["ApprovedBusinessTravelFormName"] == DBNull.Value ? "" : sqlDataReader["ApprovedBusinessTravelFormName"].ToString();
                        nocRequestModel.PurposeOfVisit = Convert.ToInt16(sqlDataReader["PurposeOfVisit"] == DBNull.Value ? "0" : sqlDataReader["PurposeOfVisit"].ToString());
                        nocRequestModel.TravelExpenseBorneBy = Convert.ToInt16(sqlDataReader["TravelExpenseBorneBy"] == DBNull.Value ? "0" : sqlDataReader["TravelExpenseBorneBy"].ToString());
                        nocRequestModel.SalaryDetail = Convert.ToString(sqlDataReader["SalaryDetail"] == DBNull.Value ? "" : sqlDataReader["SalaryDetail"].ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (nocRequestModel.certificateRequest.VisaFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(nocRequestModel.certificateRequest.VisaFileID);
                    allFormsFilesModel.FormFileIDName = "VisaFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                if (nocRequestModel.certificateRequest.PassportFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(nocRequestModel.certificateRequest.PassportFileID);
                    allFormsFilesModel.FormFileIDName = "PassportFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                if (nocRequestModel.ApprovedBusinessTravelFormId > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(nocRequestModel.ApprovedBusinessTravelFormId);
                    allFormsFilesModel.FormFileIDName = "ApprovedBusinessTravelFormId";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                nocRequestModel.certificateRequest.AllFormsFilesModelList = allFormsFilesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nocRequestModel;
        }

        #region NOC Status Report
        public List<NOCStatusReportModel> GetNOCStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<NOCStatusReportModel> nocRequestStatusList = null;
            try
            {
                nocRequestStatusList = new List<NOCStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_NOCStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    NOCStatusReportModel nocRequestStatusModel;
                    while (sqlDataReader.Read())
                    {
                        nocRequestStatusModel = new NOCStatusReportModel();
                        nocRequestStatusModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        nocRequestStatusModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        nocRequestStatusModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        nocRequestStatusModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        nocRequestStatusModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        nocRequestStatusModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        nocRequestStatusModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        nocRequestStatusModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        nocRequestStatusModel.NOCLanguage = Convert.ToString(sqlDataReader["NOCLanguage"] == DBNull.Value ? "" : sqlDataReader["NOCLanguage"]);
                        nocRequestStatusModel.AddressTo = Convert.ToString(sqlDataReader["AddressTo"] == DBNull.Value ? "" : sqlDataReader["AddressTo"]);
                        nocRequestStatusModel.RequestReason = Convert.ToString(sqlDataReader["RequestReason"] == DBNull.Value ? "" : sqlDataReader["RequestReason"]);
                        nocRequestStatusModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        nocRequestStatusModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        nocRequestStatusModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        nocRequestStatusList.Add(nocRequestStatusModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return nocRequestStatusList;
        }
        #endregion

    }
}
