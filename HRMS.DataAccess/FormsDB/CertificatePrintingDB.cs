﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class CertificatePrintingDB : FormsDB
    {
        public List<CertificatePrintingModel> GetCertificatePrintingScreenData(int? organizationId, int? employeeId)
        {
            List<CertificatePrintingModel> certificatePrintingList = null;
            try
            {
                certificatePrintingList = new List<CertificatePrintingModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CertificatePrintingScreenData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@OrganizationId", organizationId ?? 0);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId ?? 0);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CertificatePrintingModel certificatePrintingModel;
                    while (sqlDataReader.Read())
                    {
                        certificatePrintingModel = new CertificatePrintingModel();
                        certificatePrintingModel.ComapanyName = Convert.ToString(sqlDataReader["ComapanyName"] == DBNull.Value ? "" : sqlDataReader["ComapanyName"].ToString());
                        certificatePrintingModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"]);
                        certificatePrintingModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        certificatePrintingModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        certificatePrintingModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        certificatePrintingModel.CertificateAddress = Convert.ToString(sqlDataReader["CertificateAddress"] == DBNull.Value ? "" : sqlDataReader["CertificateAddress"]);
                        certificatePrintingModel.DocumentType = Convert.ToString(sqlDataReader["DocumentType"] == DBNull.Value ? "" : sqlDataReader["DocumentType"]);
                        certificatePrintingModel.CertificateType = Convert.ToString(sqlDataReader["CertificateType"] == DBNull.Value ? "" : sqlDataReader["CertificateType"]);
                        certificatePrintingModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "" : sqlDataReader["FormProcessID"]);
                        certificatePrintingModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusID"]);
                        certificatePrintingModel.FormId = Convert.ToInt32(sqlDataReader["FormId"] == DBNull.Value ? "0" : sqlDataReader["FormId"]);
                        certificatePrintingModel.TemplateId = Convert.ToInt32(sqlDataReader["TemplateId"] == DBNull.Value ? "0" : sqlDataReader["TemplateId"]);
                        certificatePrintingModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"]);
                        certificatePrintingModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"]);
                        certificatePrintingList.Add(certificatePrintingModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return certificatePrintingList;
        }

        public PrintCertificateEmployeeInfoModel GetNOCPersonalEmployeeInfoData(int id)
        {
            PrintCertificateEmployeeInfoModel printCertificateEmployeeInfoModel = new PrintCertificateEmployeeInfoModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_NOCEmployeeInfoData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        printCertificateEmployeeInfoModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        printCertificateEmployeeInfoModel.EntityName = Convert.ToString(sqlDataReader["EntityName"] == DBNull.Value ? "" : sqlDataReader["EntityName"].ToString());
                        printCertificateEmployeeInfoModel.OrganizationCode = Convert.ToString(sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeTitle = Convert.ToString(sqlDataReader["EmployeeTitle"] == DBNull.Value ? "" : sqlDataReader["EmployeeTitle"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString());
                        printCertificateEmployeeInfoModel.Nationality = Convert.ToString(sqlDataReader["Nationality"] == DBNull.Value ? "" : sqlDataReader["Nationality"].ToString());
                        printCertificateEmployeeInfoModel.PassportNo = Convert.ToString(sqlDataReader["PassportNo"] == DBNull.Value ? "" : sqlDataReader["PassportNo"].ToString());
                        printCertificateEmployeeInfoModel.MOLDesignation = Convert.ToString(sqlDataReader["MOLDesignation"] == DBNull.Value ? "" : sqlDataReader["MOLDesignation"].ToString());
                        printCertificateEmployeeInfoModel.HireDate = Convert.ToString(sqlDataReader["HireDate"] == DBNull.Value ? "" : sqlDataReader["HireDate"].ToString());
                        printCertificateEmployeeInfoModel.Title1 = Convert.ToString(sqlDataReader["Title1"] == DBNull.Value ? "" : sqlDataReader["Title1"].ToString());
                        printCertificateEmployeeInfoModel.Title2 = Convert.ToString(sqlDataReader["Title2"] == DBNull.Value ? "" : sqlDataReader["Title2"].ToString());
                        printCertificateEmployeeInfoModel.Title3 = Convert.ToString(sqlDataReader["Title3"] == DBNull.Value ? "" : sqlDataReader["Title3"].ToString());
                        printCertificateEmployeeInfoModel.Title4 = Convert.ToString(sqlDataReader["Title4"] == DBNull.Value ? "" : sqlDataReader["Title4"].ToString());
                        printCertificateEmployeeInfoModel.TotalSalary = Convert.ToString(sqlDataReader["TotalSalary"] == DBNull.Value ? "0.0" : sqlDataReader["TotalSalary"].ToString());
                        printCertificateEmployeeInfoModel.TravelDate = Convert.ToString(sqlDataReader["TravelDate"] == DBNull.Value ? "" : sqlDataReader["TravelDate"].ToString());
                        printCertificateEmployeeInfoModel.TodaysDate = Convert.ToString(sqlDataReader["TodaysDate"] == DBNull.Value ? "" : sqlDataReader["TodaysDate"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return printCertificateEmployeeInfoModel;
        }

        public PrintCertificateEmployeeInfoModel GetNOCBusinessEmployeeInfoData(int id)
        {
            PrintCertificateEmployeeInfoModel printCertificateEmployeeInfoModel = new PrintCertificateEmployeeInfoModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_NOCEmployeeInfoData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        printCertificateEmployeeInfoModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        printCertificateEmployeeInfoModel.EntityName = Convert.ToString(sqlDataReader["EntityName"] == DBNull.Value ? "" : sqlDataReader["EntityName"].ToString());
                        printCertificateEmployeeInfoModel.OrganizationCode = Convert.ToString(sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeTitle = Convert.ToString(sqlDataReader["EmployeeTitle"] == DBNull.Value ? "" : sqlDataReader["EmployeeTitle"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString());
                        printCertificateEmployeeInfoModel.Nationality = Convert.ToString(sqlDataReader["Nationality"] == DBNull.Value ? "" : sqlDataReader["Nationality"].ToString());
                        printCertificateEmployeeInfoModel.PassportNo = Convert.ToString(sqlDataReader["PassportNo"] == DBNull.Value ? "" : sqlDataReader["PassportNo"].ToString());
                        printCertificateEmployeeInfoModel.MOLDesignation = Convert.ToString(sqlDataReader["MOLDesignation"] == DBNull.Value ? "" : sqlDataReader["MOLDesignation"].ToString());
                        printCertificateEmployeeInfoModel.HireDate = Convert.ToString(sqlDataReader["HireDate"] == DBNull.Value ? "" : sqlDataReader["HireDate"].ToString());
                        printCertificateEmployeeInfoModel.Title1 = Convert.ToString(sqlDataReader["Title1"] == DBNull.Value ? "" : sqlDataReader["Title1"].ToString());
                        printCertificateEmployeeInfoModel.Title2 = Convert.ToString(sqlDataReader["Title2"] == DBNull.Value ? "" : sqlDataReader["Title2"].ToString());
                        printCertificateEmployeeInfoModel.Title3 = Convert.ToString(sqlDataReader["Title3"] == DBNull.Value ? "" : sqlDataReader["Title3"].ToString());
                        printCertificateEmployeeInfoModel.Title4 = Convert.ToString(sqlDataReader["Title4"] == DBNull.Value ? "" : sqlDataReader["Title4"].ToString());
                        printCertificateEmployeeInfoModel.TotalSalary = Convert.ToString(sqlDataReader["TotalSalary"] == DBNull.Value ? "0.0" : sqlDataReader["TotalSalary"].ToString());
                        printCertificateEmployeeInfoModel.TravelDate = Convert.ToString(sqlDataReader["TravelDate"] == DBNull.Value ? "" : sqlDataReader["TravelDate"].ToString());
                        printCertificateEmployeeInfoModel.TodaysDate = Convert.ToString(sqlDataReader["TodaysDate"] == DBNull.Value ? "" : sqlDataReader["TodaysDate"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return printCertificateEmployeeInfoModel;
        }

        public PrintCertificateEmployeeInfoModel GetSalaryTransferLetterData(int id)
        {
            PrintCertificateEmployeeInfoModel printCertificateEmployeeInfoModel = new PrintCertificateEmployeeInfoModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetSalaryTransferLetterData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        printCertificateEmployeeInfoModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        printCertificateEmployeeInfoModel.OrganizationCode = Convert.ToString(sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString());
                        printCertificateEmployeeInfoModel.TodaysDate = Convert.ToString(sqlDataReader["TodaysDate"] == DBNull.Value ? "" : sqlDataReader["TodaysDate"].ToString());
                        printCertificateEmployeeInfoModel.BankName = Convert.ToString(sqlDataReader["BankName"] == DBNull.Value ? "" : sqlDataReader["BankName"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeTitle = Convert.ToString(sqlDataReader["EmployeeTitle"] == DBNull.Value ? "" : sqlDataReader["EmployeeTitle"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString());
                        printCertificateEmployeeInfoModel.HireDate = Convert.ToString(sqlDataReader["HireDate"] == DBNull.Value ? "" : sqlDataReader["HireDate"].ToString());
                        printCertificateEmployeeInfoModel.EmploymentStatus = Convert.ToString(sqlDataReader["EmploymentStatus"] == DBNull.Value ? "" : sqlDataReader["EmploymentStatus"].ToString());
                        printCertificateEmployeeInfoModel.LinkHRDesignation = Convert.ToString(sqlDataReader["LinkHRDesignation"] == DBNull.Value ? "" : sqlDataReader["LinkHRDesignation"].ToString());
                        printCertificateEmployeeInfoModel.GrossSalary = Convert.ToString(sqlDataReader["GrossSalary"] == DBNull.Value ? "" : sqlDataReader["GrossSalary"].ToString());
                        //printCertificateEmployeeInfoModel.SalaryInText = Convert.ToString(sqlDataReader["SalaryInText"] == DBNull.Value ? "" : sqlDataReader["SalaryInText"].ToString());
                        printCertificateEmployeeInfoModel.IBAN = Convert.ToString(sqlDataReader["IBAN"] == DBNull.Value ? "" : sqlDataReader["IBAN"].ToString());
                        printCertificateEmployeeInfoModel.MOLBUName = Convert.ToString(sqlDataReader["MOLBUName"] == DBNull.Value ? "" : sqlDataReader["MOLBUName"].ToString());
                        printCertificateEmployeeInfoModel.Title1 = Convert.ToString(sqlDataReader["Title1"] == DBNull.Value ? "" : sqlDataReader["Title1"].ToString());
                        printCertificateEmployeeInfoModel.Title2 = Convert.ToString(sqlDataReader["Title2"] == DBNull.Value ? "" : sqlDataReader["Title2"].ToString());
                        printCertificateEmployeeInfoModel.Title3 = Convert.ToString(sqlDataReader["Title3"] == DBNull.Value ? "" : sqlDataReader["Title3"].ToString());
                        printCertificateEmployeeInfoModel.Title4 = Convert.ToString(sqlDataReader["Title4"] == DBNull.Value ? "" : sqlDataReader["Title4"].ToString());
                        printCertificateEmployeeInfoModel.SalaryAmount = Convert.ToDecimal(sqlDataReader["SalaryAmount"] == DBNull.Value ? "" : sqlDataReader["SalaryAmount"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return printCertificateEmployeeInfoModel;
        }

        public PrintCertificateEmployeeInfoModel GetSalaryCertificateStandardData(int id)
        {
            CertificateSalaryModel salarycertificateModel = new CertificateSalaryModel();
            PrintCertificateEmployeeInfoModel printCertificateEmployeeInfoModel = new PrintCertificateEmployeeInfoModel();
            List<CertificateSalaryModel> salaryCertificateList;
            try
            {
                salaryCertificateList = new List<CertificateSalaryModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetSalaryCertificateStandardData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    //while (sqlDataReader.Read())
                    //{ }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        printCertificateEmployeeInfoModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        printCertificateEmployeeInfoModel.OrganizationCode = Convert.ToString(sqlDataReader["OrganizationCode"] == DBNull.Value ? "" : sqlDataReader["OrganizationCode"].ToString());
                        printCertificateEmployeeInfoModel.TodaysDate = Convert.ToString(sqlDataReader["TodaysDate"] == DBNull.Value ? "" : sqlDataReader["TodaysDate"].ToString());
                        printCertificateEmployeeInfoModel.BankName = Convert.ToString(sqlDataReader["BankName"] == DBNull.Value ? "" : sqlDataReader["BankName"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeTitle = Convert.ToString(sqlDataReader["EmployeeTitle"] == DBNull.Value ? "" : sqlDataReader["EmployeeTitle"].ToString());
                        printCertificateEmployeeInfoModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString());
                        printCertificateEmployeeInfoModel.Purpose = Convert.ToString(sqlDataReader["Purpose"] == DBNull.Value ? "" : sqlDataReader["Purpose"].ToString());
                        printCertificateEmployeeInfoModel.Nationality = Convert.ToString(sqlDataReader["Nationality"] == DBNull.Value ? "" : sqlDataReader["Nationality"].ToString());
                        printCertificateEmployeeInfoModel.PassportNo = Convert.ToString(sqlDataReader["PassportNo"] == DBNull.Value ? "" : sqlDataReader["PassportNo"].ToString());
                        printCertificateEmployeeInfoModel.LinkHRDesignation = Convert.ToString(sqlDataReader["LinkHRDesignation"] == DBNull.Value ? "" : sqlDataReader["LinkHRDesignation"].ToString());
                        printCertificateEmployeeInfoModel.HireDate = Convert.ToString(sqlDataReader["HireDate"] == DBNull.Value ? "" : sqlDataReader["HireDate"].ToString());
                        printCertificateEmployeeInfoModel.MOLBUName = Convert.ToString(sqlDataReader["MOLBUName"] == DBNull.Value ? "" : sqlDataReader["MOLBUName"].ToString());
                        printCertificateEmployeeInfoModel.Title1 = Convert.ToString(sqlDataReader["Title1"] == DBNull.Value ? "" : sqlDataReader["Title1"].ToString());
                        printCertificateEmployeeInfoModel.Title2 = Convert.ToString(sqlDataReader["Title2"] == DBNull.Value ? "" : sqlDataReader["Title2"].ToString());
                        printCertificateEmployeeInfoModel.Title3 = Convert.ToString(sqlDataReader["Title3"] == DBNull.Value ? "" : sqlDataReader["Title3"].ToString());
                        printCertificateEmployeeInfoModel.Title4 = Convert.ToString(sqlDataReader["Title4"] == DBNull.Value ? "" : sqlDataReader["Title4"].ToString());
                        printCertificateEmployeeInfoModel.TotalSalary = Convert.ToString(sqlDataReader["TotalSalary"] == DBNull.Value ? "0.0" : sqlDataReader["TotalSalary"].ToString());
                    }
                    sqlDataReader.NextResult();
                    while (sqlDataReader.Read())
                    {
                        salarycertificateModel = new CertificateSalaryModel();
                        salarycertificateModel.SalaryType = Convert.ToString(sqlDataReader["SalaryType"] == DBNull.Value ? "" : sqlDataReader["SalaryType"].ToString());
                        salarycertificateModel.AmountLabel = Convert.ToString(sqlDataReader["AmountLabel"] == DBNull.Value ? "" : sqlDataReader["AmountLabel"].ToString());
                        salarycertificateModel.Amount = Convert.ToDouble(sqlDataReader["Amount"] == DBNull.Value ? "" : sqlDataReader["Amount"].ToString());
                        salaryCertificateList.Add(salarycertificateModel);

                    }
                }
                printCertificateEmployeeInfoModel.SalryCertificateList = salaryCertificateList;
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return printCertificateEmployeeInfoModel;
        }
    }
}
