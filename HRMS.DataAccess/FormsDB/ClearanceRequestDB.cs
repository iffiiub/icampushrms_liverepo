﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class ClearanceRequestDB : FormsDB
    {
        public ClearanceRequestModel GetClearanceRequestDetails(int EmployeeID)
        {
            ClearanceRequestModel clearanceRequestModel = new ClearanceRequestModel(); ;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetClearanceRequestDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        clearanceRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        clearanceRequestModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        clearanceRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        clearanceRequestModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        clearanceRequestModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"]);
                        clearanceRequestModel.ProjectName = Convert.ToString(sqlDataReader["ProjectName"]);
                        clearanceRequestModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        clearanceRequestModel.JobGradeName = Convert.ToString(sqlDataReader["JobGradeName"]);
                        clearanceRequestModel.MobileNumber = Convert.ToString(sqlDataReader["MobileNumber"]);
                        clearanceRequestModel.LineManager = Convert.ToString(sqlDataReader["LineManager"]);
                        clearanceRequestModel.NationalityName = Convert.ToString(sqlDataReader["NationalityName"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return clearanceRequestModel;
        }

        public List<ClearanceRequestOptionsModel> GetClearanceRequestOptionDetails(int CompanyId, int UserId, int EmployeeID)
        {
            List<ClearanceRequestOptionsModel> listclearanceRequestOptions = new List<ClearanceRequestOptionsModel>();
            ClearanceRequestOptionsModel clearanceRequestOptionsModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetClearanceRequestOptionDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        clearanceRequestOptionsModel = new ClearanceRequestOptionsModel();
                        clearanceRequestOptionsModel.ClearanceOptionID = Convert.ToInt32(sqlDataReader["ClearanceOptionID"].ToString());
                        clearanceRequestOptionsModel.GroupName = Convert.ToString(sqlDataReader["GroupName"]);
                        clearanceRequestOptionsModel.GroupID = Convert.ToInt32(sqlDataReader["GroupID"].ToString());
                        clearanceRequestOptionsModel.ClearanceOptionName_1 = Convert.ToString(sqlDataReader["ClearanceOptionName_1"]);
                        clearanceRequestOptionsModel.IsExistInGroup = Convert.ToBoolean(sqlDataReader["IsExistInGroup"]);
                        listclearanceRequestOptions.Add(clearanceRequestOptionsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listclearanceRequestOptions;
        }

        public List<RequestFormsProcessModel> SaveForm(ClearanceRequestModel objClearanceRequestModel)
        {
            RequestFormsProcessModel requestFormsProcessModel;
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_SaveClearanceRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objClearanceRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@UserID", objClearanceRequestModel.UserID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objClearanceRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@Comments", objClearanceRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@ClearanceRequestDetail", ConvertOptionListToTable(objClearanceRequestModel.ClearanceRequestOptionsModel));
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }

        public int UpdateForm(List<ClearanceRequestOptionsModel> clearanceRequestOptionsModel, int formprocessid, int UserId, int ID, string Comments)
        {
            int output = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateClearanceRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formprocessid);
                sqlCommand.Parameters.AddWithValue("@UserID", UserId);
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@Comments", Comments);
                sqlCommand.Parameters.AddWithValue("@ClearanceRequestDetail", ConvertOptionListToTable(clearanceRequestOptionsModel));
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
        public DataTable ConvertOptionListToTable(List<ClearanceRequestOptionsModel> clearanceRequestOptionsModel)
        {

            DataTable filesTable = new DataTable();
            filesTable.Columns.Add("ClearanceDetailID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("ClearanceOptionID", System.Type.GetType("System.Int16"));
            filesTable.Columns.Add("ClearanceMark", System.Type.GetType("System.Boolean"));
            filesTable.Columns.Add("Amount", System.Type.GetType("System.Decimal"));
            filesTable.Columns.Add("Remarks", System.Type.GetType("System.String"));

            foreach (ClearanceRequestOptionsModel frm in clearanceRequestOptionsModel)
            {
                filesTable.Rows.Add(
                                     frm.ClearanceDetailID,
                                     frm.ClearanceOptionID,
                                     frm.ClearanceMark,
                                     frm.Amount,
                                     frm.Remarks
                                     );
            }

            return filesTable;
        }

        public ClearanceRequestModel GetForm(int formProcessID)
        {
            ClearanceRequestModel clearanceRequestModel = new ClearanceRequestModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetClearanceRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        clearanceRequestModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        clearanceRequestModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"].ToString());
                        clearanceRequestModel.ReqStatusID = Convert.ToInt16(sqlDataReader["ReqStatusID"].ToString());
                        clearanceRequestModel.RequesterEmployeeID = Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString());
                        clearanceRequestModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        clearanceRequestModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        clearanceRequestModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        clearanceRequestModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        clearanceRequestModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        clearanceRequestModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"]);
                        clearanceRequestModel.ProjectName = Convert.ToString(sqlDataReader["ProjectName"]);
                        clearanceRequestModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        clearanceRequestModel.JobGradeName = Convert.ToString(sqlDataReader["JobGradeName"]);
                        clearanceRequestModel.MobileNumber = Convert.ToString(sqlDataReader["MobileNumber"]);
                        clearanceRequestModel.LineManager = Convert.ToString(sqlDataReader["LineManager"]);
                        clearanceRequestModel.NationalityName = Convert.ToString(sqlDataReader["NationalityName"]);
                        clearanceRequestModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"].ToString());
                    }
                }
                sqlConnection.Close();
                clearanceRequestModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return clearanceRequestModel;
        }

        public List<ClearanceRequestOptionsModel> GetClearanceAssignedOption(int CompanyId, int UserId, int EmployeeID, int ID, int mode)
        {
            List<ClearanceRequestOptionsModel> listclearanceRequestOptions = new List<ClearanceRequestOptionsModel>();
            ClearanceRequestOptionsModel clearanceRequestOptionsModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetClearanceAssignedOption", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        clearanceRequestOptionsModel = new ClearanceRequestOptionsModel();
                        clearanceRequestOptionsModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        clearanceRequestOptionsModel.ClearanceDetailID = Convert.ToInt32(sqlDataReader["ClearanceDetailID"].ToString());
                        clearanceRequestOptionsModel.ClearanceOptionID = Convert.ToInt32(sqlDataReader["ClearanceOptionID"].ToString());
                        clearanceRequestOptionsModel.GroupName = Convert.ToString(sqlDataReader["GroupName"]);
                        clearanceRequestOptionsModel.GroupID = Convert.ToInt32(sqlDataReader["GroupID"].ToString());
                        clearanceRequestOptionsModel.ClearanceOptionName_1 = Convert.ToString(sqlDataReader["ClearanceOptionName_1"]);
                        clearanceRequestOptionsModel.Remarks = Convert.ToString(sqlDataReader["Remarks"]);
                        if (sqlDataReader["Amount"] == DBNull.Value)
                            clearanceRequestOptionsModel.Amount = null;
                        else
                            clearanceRequestOptionsModel.Amount = Convert.ToDecimal(sqlDataReader["Amount"].ToString());                       
                        if (sqlDataReader["ClearanceMark"] == DBNull.Value)
                            clearanceRequestOptionsModel.ClearanceMark = null;
                        else
                            clearanceRequestOptionsModel.ClearanceMark = Convert.ToBoolean(sqlDataReader["ClearanceMark"]);                      
                        clearanceRequestOptionsModel.IsExistInGroup = Convert.ToBoolean(sqlDataReader["IsExistInGroup"]);
                        listclearanceRequestOptions.Add(clearanceRequestOptionsModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listclearanceRequestOptions;
        }

        public bool IsExistsClearanceRequest(int EmployeeID)
        {
            bool IsExistsRequest = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_IsExistsClearanceRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        IsExistsRequest = Convert.ToBoolean(sqlDataReader["IsExistsRequest"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IsExistsRequest;
        }

        public int UpdateSaveForm(List<ClearanceRequestOptionsModel> clearanceRequestOptionsModel, int formprocessid, int UserId, int ID, string Comments)
        {
            int output = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateSaveClearanceRequestDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formprocessid);
                sqlCommand.Parameters.AddWithValue("@UserID", UserId);
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                sqlCommand.Parameters.AddWithValue("@Comments", Comments);
                sqlCommand.Parameters.AddWithValue("@ClearanceRequestDetail", ConvertOptionListToTable(clearanceRequestOptionsModel));
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        #region End Of Term Status Report
        public List<EndOfTermStatusReportModel> GetEndOfTermStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<EndOfTermStatusReportModel> endOfTermStatusList = null;
            try
            {
                endOfTermStatusList = new List<EndOfTermStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_EndOfTermStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EndOfTermStatusReportModel endOfTermStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        endOfTermStatusReportModel = new EndOfTermStatusReportModel();
                        endOfTermStatusReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        endOfTermStatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        endOfTermStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        endOfTermStatusReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        endOfTermStatusReportModel.RequestedBy = Convert.ToInt32(sqlDataReader["RequestedBy"] == DBNull.Value ? "0" : sqlDataReader["RequestedBy"].ToString());
                        endOfTermStatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        endOfTermStatusReportModel.ResignationNumber = Convert.ToInt32(sqlDataReader["ResignationNumber"] == DBNull.Value ? "0" : sqlDataReader["ResignationNumber"].ToString());
                        endOfTermStatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        endOfTermStatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        endOfTermStatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        endOfTermStatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        endOfTermStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        endOfTermStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        endOfTermStatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        endOfTermStatusList.Add(endOfTermStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return endOfTermStatusList;
        }
        #endregion

    }
}
