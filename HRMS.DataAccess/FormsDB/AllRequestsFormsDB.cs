﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class AllRequestsFormsDB: BaseDB
    {
        public List<AllRequestsListViewModel> GetAllRequestsList(int userID, int? requestId, int? formID, int? requestStatusId)
        {
            List<AllRequestsListViewModel> allrequestFormsList = new List<AllRequestsListViewModel>();
            AllRequestsListViewModel requestViewModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllRequestsFormsList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@UserId", userID);
                sqlCommand.Parameters.AddWithValue("@RequestID", requestId);
                sqlCommand.Parameters.AddWithValue("@RequestFormID", formID);
                sqlCommand.Parameters.AddWithValue("@RequestStatusId", requestStatusId);

                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        requestViewModel = new AllRequestsListViewModel();

                        requestViewModel.FormApprovalID = Convert.ToInt32(reader["FormApprovalID"] == DBNull.Value ? "0" : reader["FormApprovalID"].ToString());
                        requestViewModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestViewModel.FormID = Convert.ToInt16(reader["FormID"] == DBNull.Value ? "0" : reader["FormID"].ToString());
                        requestViewModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestViewModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        requestViewModel.ModifiedOn = reader["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(reader["ModifiedOn"].ToString()) : (DateTime?)null;
                        requestViewModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
                        requestViewModel.ModifiedBy = Convert.ToInt32(reader["ModifiedBy"] == DBNull.Value ? "0" : reader["ModifiedBy"].ToString());
                        requestViewModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        requestViewModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestViewModel.ModifiedEmployeeName = Convert.ToString(reader["ModifiedEmployeeName"]);
                        requestViewModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                        requestViewModel.CompletedRejectedStatus = Convert.ToString(reader["CompletedRejectedStatus"]);
                        requestViewModel.RequesterCompanyName = Convert.ToString(reader["RequesterCompanyName"]);
                        requestViewModel.ParentFormID = Convert.ToInt16(reader["ParentFormID"] == DBNull.Value ? "0" : reader["ParentFormID"].ToString());
                        requestViewModel.ApproverEmployeeName = reader["ApproverEmployeeName"].ToString();
                        requestViewModel.FormName = reader["FormName"].ToString();
                        requestViewModel.Status = reader["Status"].ToString();
                        requestViewModel.ControllerName = reader["ControllerName"].ToString();
                        allrequestFormsList.Add(requestViewModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return allrequestFormsList;
        }
    }
}
