﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class ReleaseR1RequestDB:FormsDB
    {
        public List<ReleaseR1RequestModel> GetEmployeeDetailOfR1Release(int? R1RequestID)
        {
            List<ReleaseR1RequestModel> listReleaseR1RequestModel = new List<ReleaseR1RequestModel>();
            ReleaseR1RequestModel releaseR1RequestModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeDetailOfR1Release", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@R1RequestID", R1RequestID!=null? R1RequestID:(object)DBNull.Value);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        releaseR1RequestModel = new ReleaseR1RequestModel();                        
                        releaseR1RequestModel.EmployeeProfileCreationID = Convert.ToInt32(sqlDataReader["EmployeeProfileCreationID"].ToString());
                        releaseR1RequestModel.R1RequestID = Convert.ToInt32(sqlDataReader["R1RequestID"].ToString());
                        releaseR1RequestModel.ProfileRequestID = Convert.ToInt32(sqlDataReader["ProfileRequestID"].ToString());
                        releaseR1RequestModel.ProfileRequestStatusID = Convert.ToInt32(sqlDataReader["ProfileRequestStatusID"].ToString());
                        releaseR1RequestModel.ProfileRequestStatus = releaseR1RequestModel.ProfileRequestStatusID == 1 ? "Pending" : "Completed";
                        releaseR1RequestModel.EmployeeID = !string.IsNullOrEmpty(sqlDataReader["EmployeeID"].ToString())? Convert.ToInt32(sqlDataReader["EmployeeID"].ToString()):(int?)null;
                        releaseR1RequestModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        releaseR1RequestModel.FormID = Convert.ToInt32(sqlDataReader["FormID"].ToString());
                        releaseR1RequestModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"].ToString());
                        releaseR1RequestModel.ProfileFormProcessID = Convert.ToInt32(sqlDataReader["ProfileFormProcessID"].ToString());
                        releaseR1RequestModel.IsReleased = Convert.ToBoolean(sqlDataReader["IsReleased"].ToString());
                        releaseR1RequestModel.Status = Convert.ToString(sqlDataReader["Status"]);
                        releaseR1RequestModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"].ToString());
                        releaseR1RequestModel.R1ReleasedOn = !string.IsNullOrEmpty(sqlDataReader["R1ReleasedOn"].ToString()) ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["R1ReleasedOn"].ToString()) : "";
                        releaseR1RequestModel.R1ReleasedBy = !string.IsNullOrEmpty(sqlDataReader["R1ReleasedBy"].ToString()) ? Convert.ToInt32(sqlDataReader["R1ReleasedBy"].ToString()) : (int?)null;
                        releaseR1RequestModel.R1ReleasedByName = Convert.ToString(sqlDataReader["R1ReleasedByName"]);
                        releaseR1RequestModel.R1ReleaseComments = Convert.ToString(sqlDataReader["R1ReleaseComments"]);
                        releaseR1RequestModel.R1ReleaserCompanyName = Convert.ToString(sqlDataReader["R1ReleaserCompanyName"]);
                        releaseR1RequestModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        listReleaseR1RequestModel.Add(releaseR1RequestModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listReleaseR1RequestModel;
        }

        public int UpdateReleaseR1Request(int profileCreationID, string r1Releasecomments, int userID)
        {
            int InsertedRowId = 0;
            try
            {               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateReleaseR1Request", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ProfileCreationID", profileCreationID);
                sqlCommand.Parameters.AddWithValue("@R1ReleaseComments", r1Releasecomments);
                sqlCommand.Parameters.AddWithValue("@R1ReleasedBy", userID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                InsertedRowId = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : -1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return InsertedRowId;
        }
    }
}
