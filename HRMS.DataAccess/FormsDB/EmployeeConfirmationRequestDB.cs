﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class EmployeeConfirmationRequestDB : FormsDB
    {
        public List<PickList> GetAllEvaluationType()
        {
            List<PickList> lstPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllEvaluationType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PickList objPickList;
                    while (sqlDataReader.Read())
                    {
                        objPickList = new PickList();
                        objPickList.id = Convert.ToInt32(sqlDataReader["TypeEvaluationID"].ToString());
                        objPickList.text = Convert.ToString(sqlDataReader["TypeEvaluationName_1"]);
                        lstPickList.Add(objPickList);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstPickList;
        }

        public List<RatingScaleModel> GetAllRatingScale()
        {
            List<RatingScaleModel> lstRatingScaleModel = new List<RatingScaleModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllRatingScale", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    RatingScaleModel objRatingScaleModel;
                    while (sqlDataReader.Read())
                    {
                        objRatingScaleModel = new RatingScaleModel();
                        objRatingScaleModel.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        objRatingScaleModel.RatingScaleName_1 = Convert.ToString(sqlDataReader["RatingScaleName_1"]);
                        objRatingScaleModel.RatingScaleCode = Convert.ToString(sqlDataReader["RatingScaleCode"]);
                        objRatingScaleModel.RatingScaleDetail = Convert.ToString(sqlDataReader["RatingScaleDetail"]);
                        objRatingScaleModel.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        lstRatingScaleModel.Add(objRatingScaleModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstRatingScaleModel;
        }

        public List<ConfirmationJobEvaluationModel> GetAllJobEvaluation(int formProcessID, bool RequestInitialize)
        {
            List<ConfirmationJobEvaluationModel> lstJobEvaluation = new List<ConfirmationJobEvaluationModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllJobEvaluation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestInitialize", RequestInitialize);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ConfirmationJobEvaluationModel objJobEvaluation;
                    while (sqlDataReader.Read())
                    {
                        objJobEvaluation = new ConfirmationJobEvaluationModel();
                        objJobEvaluation.JobEvaluationID = Convert.ToInt32(sqlDataReader["JobEvaluationID"].ToString());
                        objJobEvaluation.JobEvaluationName_1 = Convert.ToString(sqlDataReader["JobEvaluationName_1"]);
                        objJobEvaluation.JobEvaluationDetail = Convert.ToString(sqlDataReader["JobEvaluationDetail"]);
                        objJobEvaluation.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        objJobEvaluation.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        lstJobEvaluation.Add(objJobEvaluation);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstJobEvaluation;
        }

        public List<ConfirmationCodeOfConductModel> GetAllCodeOfConduct(int formProcessID, bool RequestInitialize)
        {
            List<ConfirmationCodeOfConductModel> lstCodeOfConduct = new List<ConfirmationCodeOfConductModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllConfirmationCodeOfConduct", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestInitialize", RequestInitialize);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ConfirmationCodeOfConductModel objCodeOfConduct;
                    while (sqlDataReader.Read())
                    {
                        objCodeOfConduct = new ConfirmationCodeOfConductModel();
                        objCodeOfConduct.CodeOfConductID = Convert.ToInt32(sqlDataReader["CodeOfConductID"].ToString());
                        objCodeOfConduct.CodeOfConductName_1 = Convert.ToString(sqlDataReader["CodeOfConductName_1"]);
                        objCodeOfConduct.CodeOfConductDetail = Convert.ToString(sqlDataReader["CodeOfConductDetail"]);
                        objCodeOfConduct.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        objCodeOfConduct.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        lstCodeOfConduct.Add(objCodeOfConduct);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstCodeOfConduct;
        }

        public List<ConfirmationOthersOptionModel> GetAllConfirmationOthersOption(int formProcessID, bool RequestInitialize)
        {
            List<ConfirmationOthersOptionModel> lstOthersOption = new List<ConfirmationOthersOptionModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllConfirmationOthersOption", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestInitialize", RequestInitialize);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ConfirmationOthersOptionModel objOthersOption;
                    while (sqlDataReader.Read())
                    {
                        objOthersOption = new ConfirmationOthersOptionModel();
                        objOthersOption.OthersOptionID = Convert.ToInt32(sqlDataReader["OthersOptionID"].ToString());
                        objOthersOption.OthersOptionName_1 = Convert.ToString(sqlDataReader["OthersOptionName_1"]);
                        objOthersOption.OthersOptionDetail = Convert.ToString(sqlDataReader["OthersOptionDetail"]);
                        objOthersOption.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        objOthersOption.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        lstOthersOption.Add(objOthersOption);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstOthersOption;
        }

        public ConfirmationDetailModel GetConfirmationDetailByID(int formProcessID, int userID)
        {
            ConfirmationDetailModel confirmationDetailModel = new ConfirmationDetailModel(); ;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetConfirmationDetailByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        confirmationDetailModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        confirmationDetailModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        confirmationDetailModel.TypeEvaluationID = Convert.ToInt32(sqlDataReader["TypeEvaluationID"].ToString());
                        confirmationDetailModel.ExtendedProbationPeriod = Convert.ToInt32(sqlDataReader["ExtendedProbationPeriod"].ToString());
                        confirmationDetailModel.FinalRatingScaleID = Convert.ToInt32(sqlDataReader["FinalRatingScaleID"].ToString());
                        confirmationDetailModel.ImprovePerformanceDetail = Convert.ToString(sqlDataReader["ImprovePerformanceDetail"]);
                        confirmationDetailModel.TrainingExpDetail = Convert.ToString(sqlDataReader["TrainingExpDetail"]);
                        confirmationDetailModel.OfferLetterFileID = Convert.ToInt32(sqlDataReader["OfferLetterFileID"].ToString());
                        confirmationDetailModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"].ToString());
                        confirmationDetailModel.RequesterEmployeeID = sqlDataReader["RequesterEmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString());
                        confirmationDetailModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        confirmationDetailModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        confirmationDetailModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        confirmationDetailModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        confirmationDetailModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        confirmationDetailModel.Email = Convert.ToString(sqlDataReader["WorkEmail"]);
                        confirmationDetailModel.TypeEvaluationName_1 = Convert.ToString(sqlDataReader["TypeEvaluationName_1"]);
                        confirmationDetailModel.LineManagerName = Convert.ToString(sqlDataReader["LineManagerName"]);
                        confirmationDetailModel.LineManagerID = sqlDataReader["LineManagerID"].ToString();
                        confirmationDetailModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        confirmationDetailModel.RequestID = int.Parse(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        confirmationDetailModel.ProbationPeriod = Convert.ToString(sqlDataReader["ProbationPeriod"]);
                        confirmationDetailModel.ProbationEndDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ProbationEndDate"]));
                        confirmationDetailModel.ProjectName = Convert.ToString(sqlDataReader["ProjectName"]);
                        confirmationDetailModel.JobGradeName_1 = Convert.ToString(sqlDataReader["JobGradeName_1"]);
                        confirmationDetailModel.ManagerDesignation = Convert.ToString(sqlDataReader["ManagerDesignation"]);
                        confirmationDetailModel.RequestInitialize = Convert.ToBoolean(sqlDataReader["RequestInitialize"].ToString());
                        confirmationDetailModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"].ToString());
                        confirmationDetailModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return confirmationDetailModel;
        }

        public int UpdateConfirmationRequest(ConfirmationDetailModel confirmationDetail, int UserId)
        {
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            int output = 0;
            try
            {
                DataTable filestable = ConvertFilesListToTable(confirmationDetail.AllFormsFilesModelList);
                DataTable jobEvalTable = new DataTable();
                jobEvalTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                jobEvalTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                jobEvalTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable codeConductTable = new DataTable();
                codeConductTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                codeConductTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                codeConductTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable otherOptionTable = new DataTable();
                otherOptionTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                otherOptionTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                otherOptionTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                if (!confirmationDetail.RequestInitialize)
                {
                    foreach (var jobEval in confirmationDetail.lstJobEvalModel)
                    {
                        jobEvalTable.Rows.Add(confirmationDetail.ID,
                                             jobEval.JobEvaluationID,
                                             jobEval.RatingScaleID
                                             );
                    }

                    foreach (var codeConduct in confirmationDetail.lstConductCode)
                    {
                        codeConductTable.Rows.Add(confirmationDetail.ID,
                                             codeConduct.CodeOfConductID,
                                             codeConduct.RatingScaleID
                                             );
                    }

                    foreach (var otherOption in confirmationDetail.lstOtherOption)
                    {
                        otherOptionTable.Rows.Add(confirmationDetail.ID,
                                             otherOption.OthersOptionID,
                                             otherOption.RatingScaleID
                                             );
                    }
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateEmployeeConfirmationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", confirmationDetail.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", confirmationDetail.ID);
                sqlCommand.Parameters.AddWithValue("@TypeEvaluationID", confirmationDetail.TypeEvaluationID);
                sqlCommand.Parameters.AddWithValue("@ExtendedProbationPeriod", confirmationDetail.ExtendedProbationPeriod);
                sqlCommand.Parameters.AddWithValue("@FinalRatingScaleID", confirmationDetail.FinalRatingScaleID);
                sqlCommand.Parameters.AddWithValue("@ImprovePerformanceDetail", confirmationDetail.ImprovePerformanceDetail);
                sqlCommand.Parameters.AddWithValue("@TrainingExpDetail", confirmationDetail.TrainingExpDetail);
                sqlCommand.Parameters.AddWithValue("@OfferLetterFileID", confirmationDetail.OfferLetterFileID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", confirmationDetail.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@Comments", confirmationDetail.Comments);
                //  sqlCommand.Parameters.AddWithValue("@RequestInitialize", confirmationDetail.RequestInitialize);
                sqlCommand.Parameters.AddWithValue("@lstJobEvalModel", jobEvalTable);
                sqlCommand.Parameters.AddWithValue("@lstConductCode", codeConductTable);
                sqlCommand.Parameters.AddWithValue("@lstOtherOption", otherOptionTable);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public int ReInitializeConfirmationRequest(ConfirmationDetailModel confirmationDetail, int UserId)
        {
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            int output = 0;
            try
            {
                DataTable filestable = ConvertFilesListToTable(confirmationDetail.AllFormsFilesModelList);
                DataTable jobEvalTable = new DataTable();
                jobEvalTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                jobEvalTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                jobEvalTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable codeConductTable = new DataTable();
                codeConductTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                codeConductTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                codeConductTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable otherOptionTable = new DataTable();
                otherOptionTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                otherOptionTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                otherOptionTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                if (confirmationDetail.ID>0)
                {
                    foreach (var jobEval in confirmationDetail.lstJobEvalModel)
                    {
                        jobEvalTable.Rows.Add(confirmationDetail.ID,
                                             jobEval.JobEvaluationID,
                                             jobEval.RatingScaleID
                                             );
                    }

                    foreach (var codeConduct in confirmationDetail.lstConductCode)
                    {
                        codeConductTable.Rows.Add(confirmationDetail.ID,
                                             codeConduct.CodeOfConductID,
                                             codeConduct.RatingScaleID
                                             );
                    }

                    foreach (var otherOption in confirmationDetail.lstOtherOption)
                    {
                        otherOptionTable.Rows.Add(confirmationDetail.ID,
                                             otherOption.OthersOptionID,
                                             otherOption.RatingScaleID
                                             );
                    }
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_ReInitializeConfirmationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", confirmationDetail.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", confirmationDetail.ID);
                sqlCommand.Parameters.AddWithValue("@TypeEvaluationID", confirmationDetail.TypeEvaluationID);
                sqlCommand.Parameters.AddWithValue("@ExtendedProbationPeriod", confirmationDetail.ExtendedProbationPeriod);
                sqlCommand.Parameters.AddWithValue("@FinalRatingScaleID", confirmationDetail.FinalRatingScaleID);
                sqlCommand.Parameters.AddWithValue("@ImprovePerformanceDetail", confirmationDetail.ImprovePerformanceDetail);
                sqlCommand.Parameters.AddWithValue("@TrainingExpDetail", confirmationDetail.TrainingExpDetail);
                sqlCommand.Parameters.AddWithValue("@OfferLetterFileID", confirmationDetail.OfferLetterFileID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", confirmationDetail.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@Comments", confirmationDetail.Comments);
                //  sqlCommand.Parameters.AddWithValue("@RequestInitialize", confirmationDetail.RequestInitialize);
                sqlCommand.Parameters.AddWithValue("@lstJobEvalModel", jobEvalTable);
                sqlCommand.Parameters.AddWithValue("@lstConductCode", codeConductTable);
                sqlCommand.Parameters.AddWithValue("@lstOtherOption", otherOptionTable);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public int UpdateProbationCompletionDate(int FormProcessID)
        {           
            int output = 0;
            try
            {               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateProbationCompletionDate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);                
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        #region Probation Status Report
        public List<ProbationStatusReportModel> GetProbationStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int ProbationPeriodTypeID, int userId)
        {
            List<ProbationStatusReportModel> listStatusReportModel = null;
            try
            {
                listStatusReportModel = new List<ProbationStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetProbationStatusReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@CurrentStatusID", CurrentStatusID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@ProbationPeriodTypeID", ProbationPeriodTypeID);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    ProbationStatusReportModel StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        StatusReportModel = new ProbationStatusReportModel();
                        StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);
                        StatusReportModel.Requester = Convert.ToString(sqlDataReader["Requester"] == DBNull.Value ? "" : sqlDataReader["Requester"]);
                        StatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        StatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        StatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        StatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        StatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        StatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        StatusReportModel.TypeOfEvaluation = Convert.ToString(sqlDataReader["TypeOfEvaluation"] == DBNull.Value ? "" : sqlDataReader["TypeOfEvaluation"]);
                        StatusReportModel.JoiningDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["JoiningDate"] == DBNull.Value ? "" : sqlDataReader["JoiningDate"]));
                        StatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        StatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        StatusReportModel.ProbationCompletionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ProbationCompletionDate"] == DBNull.Value ? "" : sqlDataReader["ProbationCompletionDate"]));
                        StatusReportModel.LineManager = Convert.ToString(sqlDataReader["LineManager"] == DBNull.Value ? "" : sqlDataReader["LineManager"]);
                        StatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        StatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        StatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        StatusReportModel.ProbationPeriodType = Convert.ToString(sqlDataReader["ProbationPeriodType"] == DBNull.Value ? "" : sqlDataReader["ProbationPeriodType"]);
                        listStatusReportModel.Add(StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listStatusReportModel;
        }
        #endregion
    }
}
