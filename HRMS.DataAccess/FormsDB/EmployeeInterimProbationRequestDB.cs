﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HRMS.Entities.Forms.InterimProbationRequestModel;

namespace HRMS.DataAccess.FormsDB
{
    public class EmployeeInterimProbationRequestDB: FormsDB
    {
        public InterimProbationRequestModel GetInterimProbationDetailsById(int formProcessID, int userID)
        {
            InterimProbationRequestModel interimProbationModel = new InterimProbationRequestModel(); ;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetInterimProbationDetailsById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        interimProbationModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        interimProbationModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        interimProbationModel.TypeEvaluationID = Convert.ToInt32(sqlDataReader["TypeEvaluationID"].ToString());
                        interimProbationModel.FinalRatingScaleID = Convert.ToInt32(sqlDataReader["FinalRatingScaleID"].ToString());
                        interimProbationModel.EmployeeConfirmedDetails = Convert.ToString(sqlDataReader["EmployeeConfirmedDetails"]);
                        interimProbationModel.LineManagerComments = Convert.ToString(sqlDataReader["LineManagerComments"]);
                        interimProbationModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"].ToString());
                        interimProbationModel.RequesterEmployeeID = sqlDataReader["RequesterEmployeeID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["RequesterEmployeeID"].ToString());
                        interimProbationModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        interimProbationModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        interimProbationModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        interimProbationModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]);
                        interimProbationModel.DepartmentName_1 = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        interimProbationModel.Email = Convert.ToString(sqlDataReader["WorkEmail"]);
                        interimProbationModel.TypeEvaluationName_1 = Convert.ToString(sqlDataReader["TypeEvaluationName_1"]);
                        interimProbationModel.LineManagerName = Convert.ToString(sqlDataReader["LineManagerName"]);
                        interimProbationModel.LineManagerID = sqlDataReader["LineManagerID"].ToString();
                        interimProbationModel.PositionTitle = Convert.ToString(sqlDataReader["PositionTitle"]);
                        interimProbationModel.RequestID = int.Parse(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        interimProbationModel.ProbationPeriod = Convert.ToString(sqlDataReader["ProbationPeriod"]);
                        interimProbationModel.ProbationEndDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ProbationEndDate"]));
                        interimProbationModel.ProjectName = Convert.ToString(sqlDataReader["ProjectName"]);
                        interimProbationModel.JobGradeName_1 = Convert.ToString(sqlDataReader["JobGradeName_1"]);
                        interimProbationModel.ManagerDesignation = Convert.ToString(sqlDataReader["ManagerDesignation"]);
                        interimProbationModel.RequestInitialize = Convert.ToBoolean(sqlDataReader["RequestInitialize"].ToString());
                        interimProbationModel.CreatedOn = Convert.ToDateTime(sqlDataReader["CreatedOn"].ToString());
                        interimProbationModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return interimProbationModel;
        }

        public List<PickList> GetAllInterimProbationEvaluationType()
        {
            List<PickList> lstPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllInterimProbationEvaluationType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PickList objPickList;
                    while (sqlDataReader.Read())
                    {
                        objPickList = new PickList();
                        objPickList.id = Convert.ToInt32(sqlDataReader["TypeEvaluationID"].ToString());
                        objPickList.text = Convert.ToString(sqlDataReader["TypeEvaluationName_1"]);
                        lstPickList.Add(objPickList);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstPickList;
        }

        public List<InterimProbationRatingScaleModel> GetAllInterimProbationRatingScale()
        {
            List<InterimProbationRatingScaleModel> lstRatingScaleModel = new List<InterimProbationRatingScaleModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllInterimProbationRatingScale", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    InterimProbationRatingScaleModel objRatingScaleModel;
                    while (sqlDataReader.Read())
                    {
                        objRatingScaleModel = new InterimProbationRatingScaleModel();
                        objRatingScaleModel.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        objRatingScaleModel.RatingScaleName_1 = Convert.ToString(sqlDataReader["RatingScaleName_1"]);
                        objRatingScaleModel.RatingScaleCode = Convert.ToString(sqlDataReader["RatingScaleCode"]);
                        objRatingScaleModel.RatingScaleDetail = Convert.ToString(sqlDataReader["RatingScaleDetail"]);
                        objRatingScaleModel.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        lstRatingScaleModel.Add(objRatingScaleModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstRatingScaleModel;
        }

        public List<InterimProbationJobEvaluationModel> GetAllInterimProbationJobEvaluation(int formProcessID, bool RequestInitialize)
        {
            List<InterimProbationJobEvaluationModel> lstJobEvaluation = new List<InterimProbationJobEvaluationModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllInterimProbationJobEvaluation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestInitialize", RequestInitialize);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    InterimProbationJobEvaluationModel objJobEvaluation;
                    while (sqlDataReader.Read())
                    {
                        objJobEvaluation = new InterimProbationJobEvaluationModel();
                        objJobEvaluation.JobEvaluationID = Convert.ToInt32(sqlDataReader["JobEvaluationID"].ToString());
                        objJobEvaluation.JobEvaluationName_1 = Convert.ToString(sqlDataReader["JobEvaluationName_1"]);
                        objJobEvaluation.JobEvaluationDetail = Convert.ToString(sqlDataReader["JobEvaluationDetail"]);
                        objJobEvaluation.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        objJobEvaluation.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        lstJobEvaluation.Add(objJobEvaluation);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstJobEvaluation;
        }

        public List<InterimProbationCodeOfConductModel> GetAllInterimProbationCodeOfConduct(int formProcessID, bool RequestInitialize)
        {
            List<InterimProbationCodeOfConductModel> lstCodeOfConduct = new List<InterimProbationCodeOfConductModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllInterimProbationCodeOfConduct", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestInitialize", RequestInitialize);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    InterimProbationCodeOfConductModel objCodeOfConduct;
                    while (sqlDataReader.Read())
                    {
                        objCodeOfConduct = new InterimProbationCodeOfConductModel();
                        objCodeOfConduct.CodeOfConductID = Convert.ToInt32(sqlDataReader["CodeOfConductID"].ToString());
                        objCodeOfConduct.CodeOfConductName_1 = Convert.ToString(sqlDataReader["CodeOfConductName_1"]);
                        objCodeOfConduct.CodeOfConductDetail = Convert.ToString(sqlDataReader["CodeOfConductDetail"]);
                        objCodeOfConduct.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        objCodeOfConduct.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        lstCodeOfConduct.Add(objCodeOfConduct);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstCodeOfConduct;
        }

        public List<InterimProbationOthersModel> GetAllInterimProbationOthersOption(int formProcessID, bool RequestInitialize)
        {
            List<InterimProbationOthersModel> lstOthersOption = new List<InterimProbationOthersModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllInterimprobationOthersOption", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@formProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@RequestInitialize", RequestInitialize);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    InterimProbationOthersModel objOthersOption;
                    while (sqlDataReader.Read())
                    {
                        objOthersOption = new InterimProbationOthersModel();
                        objOthersOption.OthersOptionID = Convert.ToInt32(sqlDataReader["OthersOptionID"].ToString());
                        objOthersOption.OthersOptionName_1 = Convert.ToString(sqlDataReader["OthersOptionName_1"]);
                        objOthersOption.OthersOptionDetail = Convert.ToString(sqlDataReader["OthersOptionDetail"]);
                        objOthersOption.Sequence = Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        objOthersOption.RatingScaleID = Convert.ToInt32(sqlDataReader["RatingScaleID"].ToString());
                        lstOthersOption.Add(objOthersOption);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return lstOthersOption;
        }

        public int UpdateEmployeeInterimProbationRequest(InterimProbationRequestModel interimProbationRequestModelData, int UserId)
        {
            int output = 0;
            try
            {
                DataTable jobEvalTable = new DataTable();
                jobEvalTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                jobEvalTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                jobEvalTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable codeConductTable = new DataTable();
                codeConductTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                codeConductTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                codeConductTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable otherOptionTable = new DataTable();
                otherOptionTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                otherOptionTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                otherOptionTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                if (!interimProbationRequestModelData.RequestInitialize)
                {
                    foreach (var jobEval in interimProbationRequestModelData.lstJobEvalModel)
                    {
                        jobEvalTable.Rows.Add(interimProbationRequestModelData.ID,
                                             jobEval.JobEvaluationID,
                                             jobEval.RatingScaleID
                                             );
                    }

                    foreach (var codeConduct in interimProbationRequestModelData.lstConductCode)
                    {
                        codeConductTable.Rows.Add(interimProbationRequestModelData.ID,
                                             codeConduct.CodeOfConductID,
                                             codeConduct.RatingScaleID
                                             );
                    }

                    foreach (var otherOption in interimProbationRequestModelData.lstOtherOption)
                    {
                        otherOptionTable.Rows.Add(interimProbationRequestModelData.ID,
                                             otherOption.OthersOptionID,
                                             otherOption.RatingScaleID
                                             );
                    }
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateInterimProbationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", interimProbationRequestModelData.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", interimProbationRequestModelData.ID);
                sqlCommand.Parameters.AddWithValue("@TypeEvaluationID", interimProbationRequestModelData.TypeEvaluationID);
                sqlCommand.Parameters.AddWithValue("@FinalRatingScaleID", interimProbationRequestModelData.FinalRatingScaleID);
                sqlCommand.Parameters.AddWithValue("@EmployeeConfirmedDetails", interimProbationRequestModelData.EmployeeConfirmedDetails);
                sqlCommand.Parameters.AddWithValue("@LineManagerComments", interimProbationRequestModelData.LineManagerComments);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", interimProbationRequestModelData.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@Comments", interimProbationRequestModelData.Comments);
                //  sqlCommand.Parameters.AddWithValue("@RequestInitialize", confirmationDetail.RequestInitialize);
                sqlCommand.Parameters.AddWithValue("@lstJobEvalModel", jobEvalTable);
                sqlCommand.Parameters.AddWithValue("@lstConductCode", codeConductTable);
                sqlCommand.Parameters.AddWithValue("@lstOtherOption", otherOptionTable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public int ReInitializeEmployeeInterimProbationRequest(InterimProbationRequestModel interimProbationRequestModelData, int UserId)
        {
            int output = 0;
            try
            {
                DataTable jobEvalTable = new DataTable();
                jobEvalTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                jobEvalTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                jobEvalTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable codeConductTable = new DataTable();
                codeConductTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                codeConductTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                codeConductTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                DataTable otherOptionTable = new DataTable();
                otherOptionTable.Columns.Add("ID", System.Type.GetType("System.Int32"));
                otherOptionTable.Columns.Add("OptionID", System.Type.GetType("System.Int16"));
                otherOptionTable.Columns.Add("RatingScaleID", System.Type.GetType("System.Int16"));

                if (interimProbationRequestModelData.ID > 0)
                {
                    foreach (var jobEval in interimProbationRequestModelData.lstJobEvalModel)
                    {
                        jobEvalTable.Rows.Add(interimProbationRequestModelData.ID,
                                             jobEval.JobEvaluationID,
                                             jobEval.RatingScaleID
                                             );
                    }

                    foreach (var codeConduct in interimProbationRequestModelData.lstConductCode)
                    {
                        codeConductTable.Rows.Add(interimProbationRequestModelData.ID,
                                             codeConduct.CodeOfConductID,
                                             codeConduct.RatingScaleID
                                             );
                    }

                    foreach (var otherOption in interimProbationRequestModelData.lstOtherOption)
                    {
                        otherOptionTable.Rows.Add(interimProbationRequestModelData.ID,
                                             otherOption.OthersOptionID,
                                             otherOption.RatingScaleID
                                             );
                    }
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_ReInitializeEmployeeInterimProbationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", interimProbationRequestModelData.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", interimProbationRequestModelData.ID);
                sqlCommand.Parameters.AddWithValue("@TypeEvaluationID", interimProbationRequestModelData.TypeEvaluationID);
                sqlCommand.Parameters.AddWithValue("@FinalRatingScaleID", interimProbationRequestModelData.FinalRatingScaleID);
                sqlCommand.Parameters.AddWithValue("@EmployeeConfirmedDetails", interimProbationRequestModelData.EmployeeConfirmedDetails);
                sqlCommand.Parameters.AddWithValue("@LineManagerComments", interimProbationRequestModelData.LineManagerComments);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", interimProbationRequestModelData.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.Parameters.AddWithValue("@Comments", interimProbationRequestModelData.Comments);
                //  sqlCommand.Parameters.AddWithValue("@RequestInitialize", confirmationDetail.RequestInitialize);
                sqlCommand.Parameters.AddWithValue("@lstJobEvalModel", jobEvalTable);
                sqlCommand.Parameters.AddWithValue("@lstConductCode", codeConductTable);
                sqlCommand.Parameters.AddWithValue("@lstOtherOption", otherOptionTable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public int UpdateProbationCompletionDate(int FormProcessID)
        {
            int output = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateProbationCompletionDate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
    }
}
