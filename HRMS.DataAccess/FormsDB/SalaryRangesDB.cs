﻿using System;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Threading.Tasks;
using System.Data;
namespace HRMS.DataAccess.FormsDB
{
    public class SalaryRangesDB : BaseDB
    {
        DataAccess.GeneralDB.DataHelper dataHelper;

        public List<SalaryRangesModel> GetAllSalaryRanges(int? CompanyID, int? PositionID, int? userId)
        {
            List<SalaryRangesModel> salaryrangeList = null;
            try
            {
                salaryrangeList = new List<SalaryRangesModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllSalaryRanges", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@PositionID", PositionID);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                int i = 0;
                if (sqlDataReader.HasRows)
                {
                    SalaryRangesModel salaryrangeModel;
                    while (sqlDataReader.Read())
                    {
                        salaryrangeModel = new SalaryRangesModel();
                        salaryrangeModel.SalaryRangesID = Convert.ToInt32(sqlDataReader["SalaryRangesID"].ToString());
                        salaryrangeModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        salaryrangeModel.CompanyName = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();
                        salaryrangeModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"].ToString());
                        salaryrangeModel.PostionName = sqlDataReader["PositionTitle"] == DBNull.Value ? "" : sqlDataReader["PositionTitle"].ToString();
                        salaryrangeModel.MinSalary = Convert.ToInt32(sqlDataReader["MinSalary"] == DBNull.Value ? "0" : sqlDataReader["MinSalary"].ToString());
                        salaryrangeModel.MaxSalary = Convert.ToInt32(sqlDataReader["MaxSalary"] == DBNull.Value ? "" : sqlDataReader["MaxSalary"].ToString());
                        salaryrangeModel.autoID = i++;
                        //academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        salaryrangeList.Add(salaryrangeModel);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return salaryrangeList;

        }
        public OperationDetails UpdateSalaryRanges(SalaryRangesModel salaryRangesModel)
        {
            OperationDetails oprationDetails = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SalaryRanges_Update", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SalaryRangesID", salaryRangesModel.SalaryRangesID);
                sqlCommand.Parameters.AddWithValue("@CompanyId", salaryRangesModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@PositiionId", salaryRangesModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@MinSalary", salaryRangesModel.MinSalary);
                sqlCommand.Parameters.AddWithValue("@MaxSalary", salaryRangesModel.MaxSalary);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", salaryRangesModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", salaryRangesModel.ModifiedOn);
                sqlCommand.Parameters.AddWithValue("@IsDeleted", salaryRangesModel.IsDeleted);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.Message = "Salary range details updated successfully.";
            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = "Technical error has occurred.";
                //oprationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;

        }
        public OperationDetails DeleteSalaryrange(int SalaryRangesID)
        {
            OperationDetails oprationDetails = new OperationDetails();
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@SalaryRangesID",SalaryRangesID)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Hr_Stp_DeleteSalaryRange", parameters);
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.Message = " record deleted.";

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = "Technical error has occurred";
                //oprationDetails.InsertedRowId = -1;
            }
            finally
            {

            }

            return oprationDetails;
        }
        public OperationDetails UpdatMultipleSalaryRange(List<SalaryRangesModel> salaryList, SalaryRangesModel salaryRangeModel)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SalaryRangesID", typeof(Int32));
            dt.Columns.Add("MinSalary", typeof(Int32));
            dt.Columns.Add("MaxSalary", typeof(Int32));

            foreach (var item in salaryList)
            {
                dt.Rows.Add(item.SalaryRangesID, item.MinSalary, item.MaxSalary);
            }
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@MultipleSalary", dt));
            paramList.Add(new SqlParameter("@ModifiedBy", salaryRangeModel.ModifiedBy));
            paramList.Add(new SqlParameter("@ModifiedOn", salaryRangeModel.ModifiedOn));
            if (dataHelper.ExcutestoredProcedure(paramList, "Hr_Stp_UpdateMultipleSalaryRange"))
            {
                return new OperationDetails() { Message = "Records updated successfully.", Success = true };
            }
            else
            {
                return new OperationDetails() { Message = "Technical error has occurred.", Success = false };
            }
        }
        public OperationDetails InsertMultipleSalaryRange(List<SalaryRangesModel> salaryListinsert, SalaryRangesModel salaryRangeModel)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CompanyID", typeof(Int32));
            dt.Columns.Add("PositionID", typeof(Int32));
            dt.Columns.Add("MinSalary", typeof(Int32));
            dt.Columns.Add("MaxSalary", typeof(Int32));

            foreach (var item in salaryListinsert)
            {
                dt.Rows.Add(item.CompanyID, item.PositionID, item.MinSalary, item.MaxSalary);
            }
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@AddMultipleSalary", dt));
            paramList.Add(new SqlParameter("@CreatedBy", salaryRangeModel.CreatedBy));
            paramList.Add(new SqlParameter("@CreatedOn", salaryRangeModel.CreatedOn));
            paramList.Add(new SqlParameter("@ModifiedBy", salaryRangeModel.ModifiedBy));
            paramList.Add(new SqlParameter("@ModifiedOn", salaryRangeModel.ModifiedOn));
            paramList.Add(new SqlParameter("@IsDeleted", salaryRangeModel.IsDeleted));
            if (dataHelper.ExcutestoredProcedure(paramList, "Hr_Stp_InsertMultipleSalaryRange"))
            {
                return new OperationDetails() { Message = "Records updated successfully.", Success = true };
            }
            else
            {
                return new OperationDetails() { Message = "Technical error has occurred.", Success = false };
            }
        }
        public SalaryRangesModel GetSalaryRangeById(int SalaryRangesID)
        {
            SqlParameter[] parameters =
                    {
                      new SqlParameter("@SalaryRangesID",SalaryRangesID)
                    };

            SalaryRangesModel salaryRangesModel = new SalaryRangesModel();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetAllSalaryrangesById", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            salaryRangesModel = new SalaryRangesModel();
                            salaryRangesModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"].ToString());
                            salaryRangesModel.CompanyID = Convert.ToInt32(reader["CompanyID"].ToString());
                            salaryRangesModel.PositionID = Convert.ToInt32(reader["PositionID"].ToString());
                            salaryRangesModel.MinSalary = Convert.ToInt32(reader["MinSalary"].ToString());
                            salaryRangesModel.MaxSalary = Convert.ToInt32(reader["MaxSalary"].ToString());


                        }
                    }
                }
                return salaryRangesModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<SalaryRangesModel> GetSalaryRanges(int companyID, int? positionID)
        {
            List<SalaryRangesModel> listSalaryRangesModel = new List<SalaryRangesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetSalaryRanges", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@CompanyID", companyID);
                if (positionID != null)
                    sqlCommand.Parameters.AddWithValue("@PositionID", positionID);
                else
                    sqlCommand.Parameters.AddWithValue("@PositionID", DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    SalaryRangesModel salaryRangesModel;
                    while (reader.Read())
                    {
                        salaryRangesModel = new SalaryRangesModel();
                        salaryRangesModel.SalaryRangesID = Convert.ToInt16(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        salaryRangesModel.SalaryRanges = reader["SalaryRanges"].ToString();
                        listSalaryRangesModel.Add(salaryRangesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return listSalaryRangesModel;
        }

        public bool CheckSalaryRangesRecordExists(int CompanyID, int PositionID, int OriginalMinSalary, int OriginalMaxSalary)
        {
            bool IsExistsSalary = false;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CheckSalaryRangesRecordExists", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@PositionID", PositionID);
                sqlCommand.Parameters.AddWithValue("@OriginalMinSalary", OriginalMinSalary);
                sqlCommand.Parameters.AddWithValue("@OriginalMaxSalary", OriginalMaxSalary);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        IsExistsSalary = Convert.ToBoolean(reader["IsExists"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return IsExistsSalary;
        }
    }
}
