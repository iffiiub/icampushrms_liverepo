﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class SeparationRequestFormDB : FormsDB
    {
        public List<SeparationRequestTypeModel> GetAllSeparationRequestTypes(bool? isActive)
        {

            List<SeparationRequestTypeModel> separationRequestTypeModelList = new List<SeparationRequestTypeModel>();
            SeparationRequestTypeModel separationRequestTypeModel;
            //  RequestFormsProcessModel requestFormsProcessModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllSeparationRequestTypes", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (isActive != null)
                    sqlCommand.Parameters.AddWithValue("@IsActive", isActive);
                else
                    sqlCommand.Parameters.AddWithValue("@IsActive", DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        separationRequestTypeModel = new SeparationRequestTypeModel();
                        separationRequestTypeModel.SeparationTypeID = Convert.ToInt16(reader["SeparationTypeID"] == DBNull.Value ? "0" : reader["SeparationTypeID"].ToString());
                        separationRequestTypeModel.SeparationTypeName_1 = Convert.ToString(reader["SeparationTypeName_1"]);
                        separationRequestTypeModel.SeparationTypeName_2 = Convert.ToString(reader["SeparationTypeName_2"]);
                        separationRequestTypeModel.SeparationTypeName_3 = Convert.ToString(reader["SeparationTypeName_3"]);
                        separationRequestTypeModel.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                        separationRequestTypeModelList.Add(separationRequestTypeModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return separationRequestTypeModelList;
        }

        public SeparationRequestFormModel GetForm(int? formProcessID, int? employeeID, int userID)
        {

            SeparationRequestFormModel separationRequestFormModel = null;
           // SeparationRequestTypeModel separationRequestTypeModel = new SeparationRequestTypeModel();
            //  RequestFormsProcessModel requestFormsProcessModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetSeparationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                if (formProcessID != null)
                    sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                else
                    sqlCommand.Parameters.AddWithValue("@FormProcessID", DBNull.Value);
                if (employeeID != null)
                    sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@EmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        separationRequestFormModel = new SeparationRequestFormModel();
                        separationRequestFormModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                        separationRequestFormModel.Nationality = Convert.ToString(reader["NationalityName"]);
                        separationRequestFormModel.Company = Convert.ToString(reader["CompanyName"]);
                        separationRequestFormModel.CompanyID = Convert.ToInt32(reader["CompanyID"]);
                        separationRequestFormModel.Department = Convert.ToString(reader["DepartmentName"]);
                        separationRequestFormModel.Designation = Convert.ToString(reader["Designation"]);                     
                        separationRequestFormModel.LineManagerID = Convert.ToString(reader["LineManagerID"].ToString());
                        if (separationRequestFormModel.LineManagerID != "0")
                            separationRequestFormModel.LineManager = Convert.ToString(reader["LineManager"]);
                        else
                            separationRequestFormModel.LineManagerID = "";
                        separationRequestFormModel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                        separationRequestFormModel.DOJ = !string.IsNullOrEmpty(reader["DOJ"].ToString()) ? Convert.ToDateTime(reader["DOJ"].ToString()).ToString(HRMSDateFormat) : string.Empty;
                        separationRequestFormModel.EmployeeName = "(" + Convert.ToString(reader["EmployeeID"]) + ") " + Convert.ToString(reader["EmployeeName"]);
                        if (formProcessID > 0)
                        {
                            separationRequestFormModel.DateOfSeparation = Convert.ToDateTime(reader["DateOfSeparation"].ToString()).ToString(HRMSDateFormat);
                            separationRequestFormModel.LastWorkingDay = Convert.ToDateTime(reader["LastWorkingDay"].ToString()).ToString(HRMSDateFormat);
                            separationRequestFormModel.NoticedPeriodDays = Convert.ToInt32(reader["NoticedPeriodDays"].ToString());
                            separationRequestFormModel.SeparationTypeID = Convert.ToInt16(reader["SeparationTypeID"].ToString());
                            separationRequestFormModel.InitiateHiringReplacement = Convert.ToBoolean(reader["InitiateHiringReplacement"].ToString());
                            separationRequestFormModel.SupportingDocFileID = Convert.ToInt32(reader["SupportingDocFileID"].ToString());
                            separationRequestFormModel.Comments = Convert.ToString(reader["Comments"]);
                            separationRequestFormModel.ReasonForSeparation = Convert.ToString(reader["ReasonForSeparation"]);
                            separationRequestFormModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"]);
                            separationRequestFormModel.ID = Convert.ToInt32(reader["ID"].ToString());
                            separationRequestFormModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"].ToString());
                            separationRequestFormModel.RequestID = Convert.ToInt32(reader["RequestID"].ToString());
                            separationRequestFormModel.SeparationTypeName_1 = Convert.ToString(reader["SeparationTypeName_1"]);
                            //separationRequestFormModel.separationRequestType = separationRequestTypeModel;
                            separationRequestFormModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"].ToString());
                            separationRequestFormModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
                        }
                    }
                    // separationRequestFormModel.Add(separationRequestTypeModel);
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return separationRequestFormModel;
        }

        public RequestFormsProcessModel SaveForm(SeparationRequestFormModel objSeparationRequestFormModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            try
            {

                DataTable filestable = ConvertFilesListToTable(objSeparationRequestFormModel.AllFormsFilesModelList);
                DataTable dt = new DataTable();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveSeparationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", objSeparationRequestFormModel.ID > 0 ? (object)objSeparationRequestFormModel.ID : DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objSeparationRequestFormModel.EmployeeID > 0 ? (object)objSeparationRequestFormModel.EmployeeID : DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objSeparationRequestFormModel.FormProcessID > 0 ? (object)objSeparationRequestFormModel.FormProcessID : DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@DateofSeparation", DateTime.ParseExact(objSeparationRequestFormModel.DateOfSeparation, HRMSDateFormat, CultureInfo.InvariantCulture).Date);
                sqlCommand.Parameters.AddWithValue("@LastWorkingDay", DateTime.ParseExact(objSeparationRequestFormModel.LastWorkingDay, HRMSDateFormat, CultureInfo.InvariantCulture).Date);
                sqlCommand.Parameters.AddWithValue("@NoticedPeriodDays", objSeparationRequestFormModel.NoticedPeriodDays);
                sqlCommand.Parameters.AddWithValue("@SeparationTypeID", objSeparationRequestFormModel.SeparationTypeID);
                sqlCommand.Parameters.AddWithValue("@InitiateHiringReplacement", objSeparationRequestFormModel.InitiateHiringReplacement);
                sqlCommand.Parameters.AddWithValue("@SupportingDocFileID", objSeparationRequestFormModel.SupportingDocFileID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", objSeparationRequestFormModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objSeparationRequestFormModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@ReasonForSeparation", objSeparationRequestFormModel.ReasonForSeparation);
                sqlCommand.Parameters.AddWithValue("@Comments", objSeparationRequestFormModel.Comments);
                sqlCommand.Parameters.AddWithValue("@CompanyID", objSeparationRequestFormModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());

                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                // sqlCommand.Parameters.AddWithValue("@ModifiedBy", leaveRequestModel.ModifiedBy);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModel;
        }

        public int UpdateForm(SeparationRequestFormModel objSeparationRequestFormModel)
        {
            // RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            int output = 0;
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            try
            {

                DataTable filestable = ConvertFilesListToTable(objSeparationRequestFormModel.AllFormsFilesModelList);
                DataTable dt = new DataTable();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveSeparationRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", objSeparationRequestFormModel.ID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objSeparationRequestFormModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@SupportingDocFileID", objSeparationRequestFormModel.SupportingDocFileID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", objSeparationRequestFormModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@Comments", objSeparationRequestFormModel.Comments);               
                sqlCommand.Parameters.AddWithValue("@DateofSeparation", DateTime.ParseExact(objSeparationRequestFormModel.DateOfSeparation, HRMSDateFormat, CultureInfo.InvariantCulture).Date);
                sqlCommand.Parameters.AddWithValue("@LastWorkingDay", DateTime.ParseExact(objSeparationRequestFormModel.LastWorkingDay, HRMSDateFormat, CultureInfo.InvariantCulture).Date);
                sqlCommand.Parameters.AddWithValue("@NoticedPeriodDays", objSeparationRequestFormModel.NoticedPeriodDays);
                sqlCommand.Parameters.AddWithValue("@SeparationTypeID", objSeparationRequestFormModel.SeparationTypeID);
                sqlCommand.Parameters.AddWithValue("@InitiateHiringReplacement", objSeparationRequestFormModel.InitiateHiringReplacement);                       
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objSeparationRequestFormModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@ReasonForSeparation", objSeparationRequestFormModel.ReasonForSeparation);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;

                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                // sqlCommand.Parameters.AddWithValue("@ModifiedBy", leaveRequestModel.ModifiedBy);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }

        public List<Employee> GetAllEmployeeForSeparationRequest(bool? isActive, int UserId)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllEmployeeForSeparationRequest", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@IsActive", isActive);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FullName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<Employee> GetAllSeparationRequestedEmployee(int UserID)
        {
            List<Employee> employeeList = null;
            try
            {
                employeeList = new List<Employee>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllSeparationRequestEmployees", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    Employee employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new Employee();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        employeeModel.FullName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        employeeList.Add(employeeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList;
        }

        public List<SeparationRequestFormModel> GetEmployeeSeparationList(int ReqStatus, int? employeeId, int? separationTypeID, int userID)
        {
            List<SeparationRequestFormModel> objSeparationRequesList = new List<SeparationRequestFormModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeSeparationList", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeId);
                sqlCommand.Parameters.AddWithValue("@SeparationTypeID", separationTypeID);
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", ReqStatus);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    SeparationRequestFormModel objSeparationRequestFormModel;
                    while (sqlDataReader.Read())
                    {
                        objSeparationRequestFormModel = new SeparationRequestFormModel();
                        objSeparationRequestFormModel.ID = Convert.ToInt32(sqlDataReader["ID"] == DBNull.Value ? "0" : sqlDataReader["ID"].ToString());
                        objSeparationRequestFormModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        objSeparationRequestFormModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"]);
                        objSeparationRequestFormModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        objSeparationRequestFormModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        objSeparationRequestFormModel.DateOfSeparation = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateofSeparation"].ToString()); 
                        objSeparationRequestFormModel.SeparationTypeID = Convert.ToInt16(sqlDataReader["SeparationTypeID"].ToString());
                        objSeparationRequestFormModel.SeparationTypeName_1 = Convert.ToString(sqlDataReader["SeparationTypeName_1"]);
                        objSeparationRequestFormModel.LastWorkingDay = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["LastWorkingDay"].ToString());
                        objSeparationRequestFormModel.ExitInterviewFormProcessID = Convert.ToInt32(sqlDataReader["ExitInterviewFormProcessID"] == DBNull.Value ? "0" : sqlDataReader["ExitInterviewFormProcessID"].ToString());
                        objSeparationRequestFormModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        objSeparationRequestFormModel.ClearanceFormProcessID = Convert.ToInt32(sqlDataReader["ClearanceFormProcessID"] == DBNull.Value ? "0" : sqlDataReader["ClearanceFormProcessID"].ToString());
                        objSeparationRequestFormModel.ClearanceRequestStatus = Convert.ToInt32(sqlDataReader["ClearanceRequestStatus"] == DBNull.Value ? "0" : sqlDataReader["ClearanceRequestStatus"].ToString());
                        objSeparationRequestFormModel.ClearanceRequestID = Convert.ToInt32(sqlDataReader["ClearanceRequestID"] == DBNull.Value ? "0" : sqlDataReader["ClearanceRequestID"].ToString());
                        objSeparationRequestFormModel.ExitInterviewReqStatusID = Convert.ToInt32(sqlDataReader["ExitInterviewReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["ExitInterviewReqStatusID"].ToString());
                        objSeparationRequestFormModel.RequestDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["CreatedOn"].ToString());                       
                        objSeparationRequestFormModel.CanOpenRequest = Convert.ToBoolean(sqlDataReader["CanOpenRequest"]);
                        objSeparationRequestFormModel.ExitInterviewRequestID = Convert.ToInt32(sqlDataReader["ExitInterviewRequestID"]);
                        objSeparationRequesList.Add(objSeparationRequestFormModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objSeparationRequesList;
        }

        #region Separation Status Report
        public List<SeparationStatusReportModel> GetSeparationStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo, int userId)
        {
            List<SeparationStatusReportModel> separationStatusReportList = null;
            try
            {
                separationStatusReportList = new List<SeparationStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SeparationStatusReportData", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestId", requestId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@DepartmentId", departmentId);
                sqlCommand.Parameters.AddWithValue("@RequestStatsId", requestStatusId);
                sqlCommand.Parameters.AddWithValue("@DateFrom", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateFrom));
                sqlCommand.Parameters.AddWithValue("@DateTo", DataAccess.GeneralDB.CommonDB.SetCulturedDate(dateTo));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    SeparationStatusReportModel separationStatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        separationStatusReportModel = new SeparationStatusReportModel();
                        separationStatusReportModel.BUName = Convert.ToString(sqlDataReader["BUName"] == DBNull.Value ? "" : sqlDataReader["BUName"]);
                        separationStatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        separationStatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        separationStatusReportModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);
                        separationStatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        separationStatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        separationStatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        separationStatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        separationStatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        separationStatusReportModel.SeparationType = Convert.ToString(sqlDataReader["SeparationType"] == DBNull.Value ? "" : sqlDataReader["SeparationType"]);
                        separationStatusReportModel.DateOfResignation = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["DateOfResignation"] == DBNull.Value ? "" : sqlDataReader["DateOfResignation"]));
                        separationStatusReportModel.LastWorkingDay = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["LastWorkingDay"] == DBNull.Value ? "" : sqlDataReader["LastWorkingDay"]));
                        separationStatusReportModel.ReasonOfResignation = Convert.ToString(sqlDataReader["ReasonOfResignation"] == DBNull.Value ? "" : sqlDataReader["ReasonOfResignation"]);
                        separationStatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        separationStatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        separationStatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        separationStatusReportList.Add(separationStatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return separationStatusReportList;
        }
        #endregion

    }
}
