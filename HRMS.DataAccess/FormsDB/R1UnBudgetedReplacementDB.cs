﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class R1UnBudgetedReplacementDB : FormsDB
    {
        public List<RequestFormsProcessModel> SaveForm(R1UnBudgetedReplacementModel hiringR1ReplacementModel)
        {
            RequestFormsProcessModel requestFormsProcessModel;
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                DataTable filestable = ConvertFilesListToTable(hiringR1ReplacementModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_SaveHiringR1UnBudgetedReplacementDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", hiringR1ReplacementModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", hiringR1ReplacementModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@ReplacedEmployeeID", hiringR1ReplacementModel.ReplacedEmployeeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", hiringR1ReplacementModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", hiringR1ReplacementModel.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", hiringR1ReplacementModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", hiringR1ReplacementModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", hiringR1ReplacementModel.ProjectData);
                if (hiringR1ReplacementModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", hiringR1ReplacementModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@PositionLocation", hiringR1ReplacementModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", hiringR1ReplacementModel.HeadCount);
                if (hiringR1ReplacementModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", hiringR1ReplacementModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (hiringR1ReplacementModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", hiringR1ReplacementModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", hiringR1ReplacementModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", hiringR1ReplacementModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", hiringR1ReplacementModel.RecruitFromID);
                sqlCommand.Parameters.AddWithValue("@PositionID", hiringR1ReplacementModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@SalaryRangesID", hiringR1ReplacementModel.SalaryRangesID);
                sqlCommand.Parameters.AddWithValue("@ActualBudget", hiringR1ReplacementModel.ActualBudget);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", hiringR1ReplacementModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", hiringR1ReplacementModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", hiringR1ReplacementModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", hiringR1ReplacementModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", hiringR1ReplacementModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", hiringR1ReplacementModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", hiringR1ReplacementModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", hiringR1ReplacementModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", hiringR1ReplacementModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", hiringR1ReplacementModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", hiringR1ReplacementModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", hiringR1ReplacementModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", hiringR1ReplacementModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", hiringR1ReplacementModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", hiringR1ReplacementModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", hiringR1ReplacementModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", hiringR1ReplacementModel.ManPowerFileID);
                if (hiringR1ReplacementModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", hiringR1ReplacementModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", hiringR1ReplacementModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", hiringR1ReplacementModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", hiringR1ReplacementModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", hiringR1ReplacementModel.Comments);
                sqlCommand.Parameters.AddWithValue("@SalaryRanges", hiringR1ReplacementModel.SalaryRanges);
                sqlCommand.Parameters.AddWithValue("@IsReplacementRecord", hiringR1ReplacementModel.IsReplacementRecord);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }
        public R1UnBudgetedReplacementModel GetForm(int formProcessID, int userID)
        {
            R1UnBudgetedReplacementModel hiringR1ReplacementModel = new R1UnBudgetedReplacementModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHiringR1UnBudgetedReplacementForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        hiringR1ReplacementModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        hiringR1ReplacementModel.ReplacedEmployeeID = Convert.ToInt32(reader["ReplacedEmployeeID"] == DBNull.Value ? "0" : reader["ReplacedEmployeeID"].ToString());
                        hiringR1ReplacementModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        hiringR1ReplacementModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        hiringR1ReplacementModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        hiringR1ReplacementModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        hiringR1ReplacementModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                        hiringR1ReplacementModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        hiringR1ReplacementModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                        hiringR1ReplacementModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                        hiringR1ReplacementModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                        hiringR1ReplacementModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                        hiringR1ReplacementModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                        hiringR1ReplacementModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                        hiringR1ReplacementModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                        hiringR1ReplacementModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                        hiringR1ReplacementModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                        hiringR1ReplacementModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                        hiringR1ReplacementModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        hiringR1ReplacementModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        hiringR1ReplacementModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        hiringR1ReplacementModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                        hiringR1ReplacementModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                        hiringR1ReplacementModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                        hiringR1ReplacementModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                        hiringR1ReplacementModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                        hiringR1ReplacementModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                        hiringR1ReplacementModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                        hiringR1ReplacementModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                        hiringR1ReplacementModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                        hiringR1ReplacementModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                        hiringR1ReplacementModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                        hiringR1ReplacementModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                        hiringR1ReplacementModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                        hiringR1ReplacementModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                        hiringR1ReplacementModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                        hiringR1ReplacementModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                        hiringR1ReplacementModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                        hiringR1ReplacementModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                        hiringR1ReplacementModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        hiringR1ReplacementModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"] == DBNull.Value ? "0" : reader["CreatedBy"].ToString());
                        hiringR1ReplacementModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["CreatedOn"].ToString());
                        hiringR1ReplacementModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        hiringR1ReplacementModel.Comments = Convert.ToString(reader["Comments"]);
                        hiringR1ReplacementModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        hiringR1ReplacementModel.RequestInitialize = Convert.ToBoolean(reader["RequestInitialize"] == DBNull.Value ? "0" : reader["RequestInitialize"].ToString());
                        hiringR1ReplacementModel.SalaryRanges = Convert.ToString(reader["SalaryRanges"]);
                        hiringR1ReplacementModel.RecCategoryID = Convert.ToInt16(reader["RecCategoryID"] == DBNull.Value ? "0" : reader["RecCategoryID"].ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (hiringR1ReplacementModel.JobDescriptionFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(hiringR1ReplacementModel.JobDescriptionFileID);
                    allFormsFilesModel.FormFileIDName = "JobDescriptionFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                if (hiringR1ReplacementModel.ManPowerFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(hiringR1ReplacementModel.ManPowerFileID);
                    allFormsFilesModel.FormFileIDName = "ManPowerFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                if (hiringR1ReplacementModel.OrgChartFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(hiringR1ReplacementModel.OrgChartFileID);
                    allFormsFilesModel.FormFileIDName = "OrgChartFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                hiringR1ReplacementModel.AllFormsFilesModelList = allFormsFilesModelList;

                hiringR1ReplacementModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return hiringR1ReplacementModel;
        }
        public int UpdateForm(R1UnBudgetedReplacementModel objHiringR1ReplacementModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            int output = 0;
            try
            {
                DataTable filestable = ConvertFilesListToTable(objHiringR1ReplacementModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateHiringR1UnBudgetedReplacement", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objHiringR1ReplacementModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ReplacedEmployeeID", objHiringR1ReplacementModel.ReplacedEmployeeID);
                sqlCommand.Parameters.AddWithValue("@ID", objHiringR1ReplacementModel.ID);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", objHiringR1ReplacementModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", objHiringR1ReplacementModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", objHiringR1ReplacementModel.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", objHiringR1ReplacementModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", objHiringR1ReplacementModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", objHiringR1ReplacementModel.ProjectData);
                if (objHiringR1ReplacementModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", objHiringR1ReplacementModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@PositionLocation", objHiringR1ReplacementModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", objHiringR1ReplacementModel.HeadCount);
                if (objHiringR1ReplacementModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", objHiringR1ReplacementModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (objHiringR1ReplacementModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", objHiringR1ReplacementModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", objHiringR1ReplacementModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", objHiringR1ReplacementModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", objHiringR1ReplacementModel.RecruitFromID);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", objHiringR1ReplacementModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", objHiringR1ReplacementModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", objHiringR1ReplacementModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", objHiringR1ReplacementModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", objHiringR1ReplacementModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", objHiringR1ReplacementModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", objHiringR1ReplacementModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", objHiringR1ReplacementModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", objHiringR1ReplacementModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", objHiringR1ReplacementModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", objHiringR1ReplacementModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", objHiringR1ReplacementModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", objHiringR1ReplacementModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", objHiringR1ReplacementModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", objHiringR1ReplacementModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", objHiringR1ReplacementModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", objHiringR1ReplacementModel.ManPowerFileID);
                if (objHiringR1ReplacementModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", objHiringR1ReplacementModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", objHiringR1ReplacementModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", objHiringR1ReplacementModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objHiringR1ReplacementModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", objHiringR1ReplacementModel.Comments);
                sqlCommand.Parameters.AddWithValue("@SalaryRanges", objHiringR1ReplacementModel.SalaryRanges);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
        public HiringR1UnBudgetedReplacementViewModel GetFormDetails(int formProcessID)
        {
            HiringR1UnBudgetedReplacementViewModel hiringR1ReplacementViewModel = new HiringR1UnBudgetedReplacementViewModel();
            R1UnBudgetedReplacementModel hiringR1ReplacementModel = new R1UnBudgetedReplacementModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHiringR1UnBudgetedReplacementFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        hiringR1ReplacementModel.ReplacementEmployee = Convert.ToString(reader["ReplacementEmployee"]);
                        hiringR1ReplacementModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        hiringR1ReplacementModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        hiringR1ReplacementModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        hiringR1ReplacementModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        hiringR1ReplacementModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        hiringR1ReplacementModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                        hiringR1ReplacementModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        hiringR1ReplacementModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                        hiringR1ReplacementModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                        hiringR1ReplacementModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                        hiringR1ReplacementModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                        hiringR1ReplacementModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                        hiringR1ReplacementModel.JobGradeName = Convert.ToString(reader["JobGradeName"]);
                        hiringR1ReplacementModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                        hiringR1ReplacementModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                        hiringR1ReplacementModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                        hiringR1ReplacementModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                        hiringR1ReplacementModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                        hiringR1ReplacementModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        hiringR1ReplacementModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        hiringR1ReplacementModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        hiringR1ReplacementModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                        hiringR1ReplacementModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                        hiringR1ReplacementModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                        hiringR1ReplacementModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                        hiringR1ReplacementModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                        hiringR1ReplacementModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                        hiringR1ReplacementModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                        hiringR1ReplacementModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                        hiringR1ReplacementModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                        hiringR1ReplacementModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                        hiringR1ReplacementModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                        hiringR1ReplacementModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                        hiringR1ReplacementModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                        hiringR1ReplacementModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                        hiringR1ReplacementModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                        hiringR1ReplacementModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                        hiringR1ReplacementModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                        hiringR1ReplacementModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                        hiringR1ReplacementModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        hiringR1ReplacementModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        hiringR1ReplacementModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
                        hiringR1ReplacementViewModel.RequestID = int.Parse(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        hiringR1ReplacementViewModel.AcademicYearName = Convert.ToString(reader["AcademicYearName"]);
                        hiringR1ReplacementViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        hiringR1ReplacementViewModel.EmploymentModeName = Convert.ToString(reader["EmploymentModeName"]);
                        hiringR1ReplacementViewModel.EmployeeJobCategoryName = Convert.ToString(reader["EmployeeJobCategoryName"]);
                        hiringR1ReplacementViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                        hiringR1ReplacementViewModel.HMEmployeeName = Convert.ToString(reader["HMEmployeeName"]);
                        hiringR1ReplacementViewModel.ReportingEmployeeName = Convert.ToString(reader["ReportingEmployeeName"]);
                        hiringR1ReplacementViewModel.SourceName = Convert.ToString(reader["SourceName"]);
                        hiringR1ReplacementViewModel.RecruitFromName = Convert.ToString(reader["RecruitFromName"]);
                        hiringR1ReplacementViewModel.SalaryRangeName = Convert.ToString(reader["SalaryRangeName"]);
                        hiringR1ReplacementViewModel.AirfareFrequencyName = Convert.ToString(reader["AirfareFrequencyName"]);
                        hiringR1ReplacementViewModel.AirfareClassName = Convert.ToString(reader["AirfareClassName"]);
                        hiringR1ReplacementViewModel.JobDescriptionFileName = Convert.ToString(reader["JobDescriptionFileName"]);
                        hiringR1ReplacementViewModel.OrgChartFileName = Convert.ToString(reader["OrgChartFileName"]);
                        hiringR1ReplacementViewModel.ManPowerFileName = Convert.ToString(reader["ManPowerFileName"]);
                        hiringR1ReplacementViewModel.DivisionName = Convert.ToString(reader["DivisionName"]);
                        hiringR1ReplacementViewModel.RequestStatusName = Convert.ToString(reader["ReqStatusName"]);
                        hiringR1ReplacementViewModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                        hiringR1ReplacementViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                        hiringR1ReplacementViewModel.UserTypeName = Convert.ToString(reader["UserTypeName"]);
                        hiringR1ReplacementModel.SalaryRanges = Convert.ToString(reader["SalaryRanges"]);
                        hiringR1ReplacementModel.RecCategoryID = Convert.ToInt16(reader["RecCategoryID"] == DBNull.Value ? "0" : reader["RecCategoryID"].ToString());
                        hiringR1ReplacementViewModel.RecCategoryName = Convert.ToString(reader["RecCategoryName"]);
                    }
                }
                sqlConnection.Close();
                hiringR1ReplacementModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                hiringR1ReplacementViewModel.R1UnBudgetedReplacementModel = hiringR1ReplacementModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return hiringR1ReplacementViewModel;
        }
        public R1UnBudgetedReplacementModel GetR1ReplacementDetails(int? EmployeeID)
        {
            R1UnBudgetedReplacementModel hiringR1ReplacementModel = new R1UnBudgetedReplacementModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetR1ReplacementDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        hiringR1ReplacementModel.IsReplacementRecord = Convert.ToBoolean(reader["IsReplacementRecord"].ToString());
                        hiringR1ReplacementModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        hiringR1ReplacementModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        hiringR1ReplacementModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        hiringR1ReplacementModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        hiringR1ReplacementModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        hiringR1ReplacementModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        hiringR1ReplacementModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        hiringR1ReplacementModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        if (hiringR1ReplacementModel.IsReplacementRecord)
                        {
                            hiringR1ReplacementModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                            hiringR1ReplacementModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                            hiringR1ReplacementModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                            hiringR1ReplacementModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                            hiringR1ReplacementModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                            hiringR1ReplacementModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                            hiringR1ReplacementModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                            hiringR1ReplacementModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                            hiringR1ReplacementModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                            hiringR1ReplacementModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                            hiringR1ReplacementModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                            hiringR1ReplacementModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                            hiringR1ReplacementModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                            hiringR1ReplacementModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                            hiringR1ReplacementModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                            hiringR1ReplacementModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                            hiringR1ReplacementModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                            hiringR1ReplacementModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                            hiringR1ReplacementModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                            hiringR1ReplacementModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                            hiringR1ReplacementModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                            hiringR1ReplacementModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                            hiringR1ReplacementModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                            hiringR1ReplacementModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                            hiringR1ReplacementModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                            hiringR1ReplacementModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                            hiringR1ReplacementModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                            hiringR1ReplacementModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                            hiringR1ReplacementModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                            hiringR1ReplacementModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                            hiringR1ReplacementModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                            hiringR1ReplacementModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                            hiringR1ReplacementModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        }
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

                if (hiringR1ReplacementModel.JobDescriptionFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(hiringR1ReplacementModel.JobDescriptionFileID);
                    allFormsFilesModel.FormFileIDName = "JobDescriptionFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }

                if (hiringR1ReplacementModel.OrgChartFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(hiringR1ReplacementModel.OrgChartFileID);
                    allFormsFilesModel.FormFileIDName = "OrgChartFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }

                if (hiringR1ReplacementModel.ManPowerFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(hiringR1ReplacementModel.ManPowerFileID);
                    allFormsFilesModel.FormFileIDName = "ManPowerFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }

                hiringR1ReplacementModel.AllFormsFilesModelList = allFormsFilesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return hiringR1ReplacementModel;
        }       
        public RequestFormsProcessModel UpdateRequestInitialize(R1UnBudgetedReplacementModel objHiringR1ReplacementModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = null;

            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                DataTable filestable = ConvertFilesListToTable(objHiringR1ReplacementModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateR1UnBudgetedReplacement", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", objHiringR1ReplacementModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", objHiringR1ReplacementModel.ID);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", objHiringR1ReplacementModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", objHiringR1ReplacementModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", objHiringR1ReplacementModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", objHiringR1ReplacementModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", objHiringR1ReplacementModel.ProjectData);
                if (objHiringR1ReplacementModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", objHiringR1ReplacementModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@PositionLocation", objHiringR1ReplacementModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", objHiringR1ReplacementModel.HeadCount);
                if (objHiringR1ReplacementModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", objHiringR1ReplacementModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (objHiringR1ReplacementModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", objHiringR1ReplacementModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", objHiringR1ReplacementModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", objHiringR1ReplacementModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", objHiringR1ReplacementModel.RecruitFromID);
                sqlCommand.Parameters.AddWithValue("@SalaryRangesID", objHiringR1ReplacementModel.SalaryRangesID);
                sqlCommand.Parameters.AddWithValue("@ActualBudget", objHiringR1ReplacementModel.ActualBudget);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", objHiringR1ReplacementModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", objHiringR1ReplacementModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", objHiringR1ReplacementModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", objHiringR1ReplacementModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", objHiringR1ReplacementModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", objHiringR1ReplacementModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", objHiringR1ReplacementModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", objHiringR1ReplacementModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", objHiringR1ReplacementModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", objHiringR1ReplacementModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", objHiringR1ReplacementModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", objHiringR1ReplacementModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", objHiringR1ReplacementModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", objHiringR1ReplacementModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", objHiringR1ReplacementModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", objHiringR1ReplacementModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", objHiringR1ReplacementModel.ManPowerFileID);
                if (objHiringR1ReplacementModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", objHiringR1ReplacementModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", objHiringR1ReplacementModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", objHiringR1ReplacementModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", objHiringR1ReplacementModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", objHiringR1ReplacementModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        //  requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModel;
        }
    }
}
