﻿using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
   public class PublicHolidayLeaveManagerDB: BaseDB
    {
        public List<PublicHolidayLeaveManagerModel> GetPublicHolidayLeaveList(int? userID, int holidayID, int? departmentID, int? companyID)
        {
            List<PublicHolidayLeaveManagerModel> publicHolidayLeaveManagerModelList = new List<PublicHolidayLeaveManagerModel>();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPublicHolidayLeaveManager", sqlConnection);
                if (holidayID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@HolidayID", holidayID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@HolidayID", DBNull.Value));
                if (departmentID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@DepartmentID", departmentID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@DepartmentID", DBNull.Value));
                if (companyID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", companyID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", DBNull.Value));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", userID));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 300;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();              
                if (sqlDataReader.HasRows)
                {
                    PublicHolidayLeaveManagerModel publicHolidayLeaveManagerModel;
                    while (sqlDataReader.Read())
                    {
                        publicHolidayLeaveManagerModel = new PublicHolidayLeaveManagerModel();
                        publicHolidayLeaveManagerModel.LeaveRequestID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        publicHolidayLeaveManagerModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        publicHolidayLeaveManagerModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"].ToString());
                        publicHolidayLeaveManagerModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"].ToString());
                        publicHolidayLeaveManagerModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"].ToString());
                        publicHolidayLeaveManagerModel.FromDate = !string.IsNullOrEmpty(sqlDataReader["FromDate"].ToString()) ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["FromDate"].ToString()) : "";
                        publicHolidayLeaveManagerModel.ToDate = !string.IsNullOrEmpty(sqlDataReader["ToDate"].ToString()) ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["ToDate"].ToString()) : "";
                        publicHolidayLeaveManagerModel.LapsingDate = !string.IsNullOrEmpty(sqlDataReader["LapsingDate"].ToString()) ? HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["LapsingDate"].ToString()) : "";
                        publicHolidayLeaveManagerModel.WeekEnds = Convert.ToString(sqlDataReader["WeekEnds"].ToString());
                        publicHolidayLeaveManagerModel.TotalLeaveDays = decimal.Parse(sqlDataReader["TotalLeaveDays"].ToString());
                        publicHolidayLeaveManagerModel.LeaveDaysFromAccruedDays = decimal.Parse(sqlDataReader["LeaveDaysFromAccruedDays"].ToString());
                        publicHolidayLeaveManagerModel.LeaveDaysFromLapsingDays = decimal.Parse(sqlDataReader["LeaveDaysFromLapsingDays"].ToString());
                        publicHolidayLeaveManagerModel.HolidayLeaveDays = decimal.Parse(sqlDataReader["HolidayLeaveDays"].ToString());
                        publicHolidayLeaveManagerModel.NoOfWeekEndDays = decimal.Parse(sqlDataReader["NoOfWeekEndDays"].ToString());                       
                        publicHolidayLeaveManagerModel.HolidayID = Convert.ToInt32(sqlDataReader["HolidayID"].ToString());
                        publicHolidayLeaveManagerModel.PublicHolidayLeaveCreditBackID = sqlDataReader["PublicHolidayLeaveCreditBackID"]!=DBNull.Value?Convert.ToInt32(sqlDataReader["PublicHolidayLeaveCreditBackID"].ToString()):(int?)null;
                        publicHolidayLeaveManagerModel.CreditBackDays = publicHolidayLeaveManagerModel.PublicHolidayLeaveCreditBackID>0? publicHolidayLeaveManagerModel.HolidayLeaveDays :publicHolidayLeaveManagerModel.HolidayLeaveDays - publicHolidayLeaveManagerModel.NoOfWeekEndDays;
                        publicHolidayLeaveManagerModel.CreatedBy = sqlDataReader["CreatedBy"] != DBNull.Value ? Convert.ToInt32(sqlDataReader["CreatedBy"].ToString()) : (int?)null;
                        publicHolidayLeaveManagerModel.CreatedOn = sqlDataReader["CreatedOn"] != DBNull.Value ? DateTime.Parse(sqlDataReader["CreatedOn"].ToString()) : (DateTime?)null;
                        publicHolidayLeaveManagerModelList.Add(publicHolidayLeaveManagerModel);
                    }
                }


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return publicHolidayLeaveManagerModelList;
        }

        public OperationDetails Save(int holidayID, List<PublicHolidayLeaveManagerModel> publicHolidayLeaveManagerList, int userId)
        {

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("LeaveRequestID");             
                resultTable.Columns.Add("CreditBackDays");
               
                foreach (PublicHolidayLeaveManagerModel obj in publicHolidayLeaveManagerList)
                {
                    resultTable.Rows.Add(obj.LeaveRequestID, obj.HolidayLeaveDays);
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SavePublicHolidayLeaveCreditBack", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@HolidayID", holidayID);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", userId);
                sqlCommand.Parameters.AddWithValue("@PublicHolidayCreditBack", resultTable);
                SqlParameter output = new SqlParameter("@Output", SqlDbType.Int, 1000);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                SqlParameter outputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 1000);
                outputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(outputMessage);               
                sqlCommand.ExecuteNonQuery();
                int outputValue = Convert.ToInt32(output.Value.ToString());
                sqlConnection.Close();
                operationDetails.CssClass = outputValue > 0 ? "success" : "error";             
                operationDetails.Success = outputValue > 0 ? true : false;                
                operationDetails.Message = outputValue > 0 ? "Saved successfully": outputMessage.Value.ToString();
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred " + ex.Message;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
    }
}
