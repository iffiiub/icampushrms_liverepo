﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
   public class FormsUniqueKeyDB:BaseDB
    {
        public List<FormsUniqueKeyModel> GetAllFormsUniqueKey(int formID)
        {
            List<FormsUniqueKeyModel> formsUniqueKeyList = new List<FormsUniqueKeyModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllFormsUniqueKey", sqlConnection);
                if (formID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@FormID", formID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@FormID", DBNull.Value));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    FormsUniqueKeyModel formsUniqueKeyModel;
                    while (sqlDataReader.Read())
                    {
                        formsUniqueKeyModel = new FormsUniqueKeyModel();
                        formsUniqueKeyModel.FormID = Convert.ToInt16(sqlDataReader["FormID"].ToString());
                        formsUniqueKeyModel.FormName = Convert.ToString(sqlDataReader["FormName"]);
                        formsUniqueKeyModel.Description = Convert.ToString(sqlDataReader["FormDescription"]);
                        formsUniqueKeyModel.ControllerName = Convert.ToString(sqlDataReader["ControllerName"]);
                        formsUniqueKeyModel.CompanyType = Convert.ToString(sqlDataReader["CompanyType"]);
                        formsUniqueKeyModel.TableName = Convert.ToString(sqlDataReader["TableName"]);                       
                        formsUniqueKeyModel.ParentFormID = sqlDataReader["ParentFormID"]!=DBNull.Value? Convert.ToInt16(sqlDataReader["ParentFormID"].ToString()):(Int16?)null;
                        formsUniqueKeyModel.CertificateTypeID = sqlDataReader["CertificateTypeID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["CertificateTypeID"].ToString()) : (Int16?)null;
                        formsUniqueKeyModel.CertificateTypeID = sqlDataReader["LeaveTypeID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["LeaveTypeID"].ToString()) : (Int16?)null;
                        formsUniqueKeyModel.CertificateTypeID = sqlDataReader["SeparationTypeID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["SeparationTypeID"].ToString()) : (Int16?)null;
                        formsUniqueKeyList.Add(formsUniqueKeyModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return formsUniqueKeyList;
        }


        public List<FormsUniqueKeyModel> GetAllFormsUniqueKeyByUser(int userID)
        {
            List<FormsUniqueKeyModel> formsUniqueKeyList = new List<FormsUniqueKeyModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllFormsUniqueKeyByUser", sqlConnection);               
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", userID));               
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    FormsUniqueKeyModel formsUniqueKeyModel;
                    while (sqlDataReader.Read())
                    {
                        formsUniqueKeyModel = new FormsUniqueKeyModel();
                        formsUniqueKeyModel.FormID = Convert.ToInt16(sqlDataReader["FormID"].ToString());
                        formsUniqueKeyModel.FormName = Convert.ToString(sqlDataReader["FormName"]);
                        formsUniqueKeyModel.Description = Convert.ToString(sqlDataReader["FormDescription"]);
                        formsUniqueKeyModel.ControllerName = Convert.ToString(sqlDataReader["ControllerName"]);
                        formsUniqueKeyModel.ParentFormID = sqlDataReader["ParentFormID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["ParentFormID"].ToString()) : (Int16?)null;
                        formsUniqueKeyList.Add(formsUniqueKeyModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return formsUniqueKeyList;
        }

        public FormsUniqueKeyModel GetFormsUniqueKey(Int16?formID, Int16? separationTypeID, Int16? leaveTypeID, Int16? certificateTypeID )
        {
            FormsUniqueKeyModel formsUniqueKeyModel = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFormsUniqueKey", sqlConnection);
                if (formID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@FormID", formID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@FormID", DBNull.Value));
                if (separationTypeID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@SeparationTypeID", separationTypeID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@SeparationTypeID", DBNull.Value));
                if (certificateTypeID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@CertificateTypeID", certificateTypeID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@CertificateTypeID", DBNull.Value));
                if (leaveTypeID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@LeaveTypeID", leaveTypeID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@LeaveTypeID", DBNull.Value));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    //FormsUniqueKeyModel formsUniqueKeyModel;
                    while (sqlDataReader.Read())
                    {
                        formsUniqueKeyModel = new FormsUniqueKeyModel();
                        formsUniqueKeyModel.FormID = Convert.ToInt16(sqlDataReader["FormID"].ToString());
                        formsUniqueKeyModel.FormName = Convert.ToString(sqlDataReader["FormName"]);
                        formsUniqueKeyModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        formsUniqueKeyModel.ControllerName = Convert.ToString(sqlDataReader["ControllerName"]);
                        formsUniqueKeyModel.CompanyType = Convert.ToString(sqlDataReader["CompanyType"]);
                        formsUniqueKeyModel.TableName = Convert.ToString(sqlDataReader["TableName"]);                       
                        formsUniqueKeyModel.ParentFormID = sqlDataReader["ParentFormID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["ParentFormID"].ToString()) : (Int16?)null;
                        formsUniqueKeyModel.CertificateTypeID = sqlDataReader["CertificateTypeID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["CertificateTypeID"].ToString()) : (Int16?)null;
                        formsUniqueKeyModel.CertificateTypeID = sqlDataReader["LeaveTypeID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["LeaveTypeID"].ToString()) : (Int16?)null;
                        formsUniqueKeyModel.CertificateTypeID = sqlDataReader["SeparationTypeID"] != DBNull.Value ? Convert.ToInt16(sqlDataReader["SeparationTypeID"].ToString()) : (Int16?)null;
                        //formsUniqueKeyList.Add(formsUniqueKeyModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return formsUniqueKeyModel;
        }

    }
}
