﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class FormsWorkflowDB : BaseDB
    {    
        public OperationDetails SaveAll(FormsWorkflowModelSaver formsWorkflow)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                DataTable resultTable = new DataTable();
                resultTable.Columns.Add("WorkflowID", System.Type.GetType("System.Int32"));                  
                resultTable.Columns.Add("PriorityListID", System.Type.GetType("System.Int16"));
                resultTable.Columns.Add("FinalApprovalNotification", System.Type.GetType("System.Boolean"));
                resultTable.Columns.Add("BaseGroupID", System.Type.GetType("System.Int16"));

                foreach (FormsWorkflowModel frm in formsWorkflow.FormsWorkflowList)
                {
                    resultTable.Rows.Add(frm.WorkflowID,
                                         frm.PriorityListID,                                        
                                         frm.FinalApprovalNotification,
                                         frm.BaseGroupID
                                         );
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_InsertUpdateFormsWorkflow", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormsWorkflowList", resultTable);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", formsWorkflow.ModifiedBy);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlParameter Output = new SqlParameter("@output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);              
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                operationDetails.Success = true;
                if (Convert.ToInt32(Output.Value) == -1)
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while updating workflow.";
                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    operationDetails.Message = "Workflow updated successfully.";
                }


            }
            catch(Exception ex)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Technical error has occurred .";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public OperationDetails UpdateFormsWorkflow(FormsWorkflowModel formsWorkflow)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateFormsWorkflow", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@WorkflowID", formsWorkflow.WorkflowID);
               // sqlCommand.Parameters.AddWithValue("@GroupID", formsWorkflow.GroupID);
                sqlCommand.Parameters.AddWithValue("@PriorityListID", formsWorkflow.PriorityListID);
                sqlCommand.Parameters.AddWithValue("@FinalApprovalNotification", formsWorkflow.FinalApprovalNotification);
                sqlCommand.Parameters.AddWithValue("@BaseGroupID", formsWorkflow.BaseGroupID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", formsWorkflow.ModifiedBy);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlParameter Output = new SqlParameter("@output", SqlDbType.Int, 1000);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                if (Convert.ToInt32(Output.Value) == -1)
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while updating Workflow.";
                }
                else if (Convert.ToInt32(Output.Value) >= 1)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    operationDetails.Message = "Workflow updated successfully.";
                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = OperationMessage.Value.ToString(); 
                }
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.CssClass = "error";
                operationDetails.Message = "Technical error has occurred."+exception.Message;
                //oprationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public object IsPendingApprovalExists(int groupID, int formID, int companyTypeID, int PriorityListID,int UpdatedPriorityListID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_IsPendingApprovalExistsForWorkflow", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;             
                sqlCommand.Parameters.AddWithValue("@GroupID", groupID);
                sqlCommand.Parameters.AddWithValue("@FormID", formID);
                sqlCommand.Parameters.AddWithValue("@CompanyTypeID", companyTypeID);
                sqlCommand.Parameters.AddWithValue("@PriorityListID", PriorityListID);
                sqlCommand.Parameters.AddWithValue("@UpdatedPriorityListID", UpdatedPriorityListID);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);

                SqlParameter InitiatorIdentity = new SqlParameter("@InitiatorIdentity", SqlDbType.NVarChar, 4000);
                InitiatorIdentity.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InitiatorIdentity);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();
                op.InitiatorIdentity = InitiatorIdentity.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }

        public List<FormsWorkflowModel> GetAllFormsWorkflow(Int16 companyId, Int16? formID)
        {         
            List<FormsWorkflowModel> formsWorkFlowList = new List<FormsWorkflowModel>();         
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetFormsWorkflowDetails", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyId", companyId));
                if (formID > 0)
                    sqlCommand.Parameters.Add(new SqlParameter("@FormID", formID));
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@FormID", DBNull.Value));
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FormsWorkflowModel formsWorkflowModel;
                    while (sqlDataReader.Read())
                    {
                        formsWorkflowModel = new FormsWorkflowModel();
                        formsWorkflowModel.WorkflowID = Convert.ToInt32(sqlDataReader["WorkflowID"].ToString());
                        formsWorkflowModel.FormID = Convert.ToInt16(sqlDataReader["FormID"].ToString());
                        formsWorkflowModel.GroupID = Convert.ToInt16(sqlDataReader["GroupID"].ToString());
                        formsWorkflowModel.PriorityListID = Convert.ToInt16(sqlDataReader["PriorityListID"] == DBNull.Value ? "0" : sqlDataReader["PriorityListID"].ToString());
                        formsWorkflowModel.FinalApprovalNotification = Convert.ToBoolean(sqlDataReader["FinalApprovalNotification"]);
                        formsWorkflowModel.SchoolBasedGroup = bool.Parse(sqlDataReader["SchoolBasedGroup"] == DBNull.Value ? "1" : sqlDataReader["SchoolBasedGroup"].ToString());
                        formsWorkflowModel.BaseGroupID = Convert.ToInt16(sqlDataReader["BaseGroupID"].ToString());
                        formsWorkflowModel.GroupName = Convert.ToString(sqlDataReader["GroupName"].ToString());
                        formsWorkflowModel.FormName = Convert.ToString(sqlDataReader["FormName"].ToString());
                        formsWorkflowModel.PriorityListName = Convert.ToString(sqlDataReader["PriorityListName"].ToString());
                        formsWorkflowModel.BaseGroupName = Convert.ToString(sqlDataReader["BaseGroupName"].ToString());
                        formsWorkflowModel.CompanyTypeID = Convert.ToInt16(sqlDataReader["CompanyTypeID"].ToString());


                        formsWorkFlowList.Add(formsWorkflowModel);
                    }
                }

          
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
             return formsWorkFlowList;
        }

        public List<FormsWorkflowGroupModel> GetAllFormsWorkflowGroups(Int16? groupID, bool? schoolBasedGroup, bool? isActive)
        {
            List<FormsWorkflowGroupModel> groupsList = null;
            //FormsWorkflowGroupModel groups = new FormsWorkflowGroupModel();
            try
            {
                groupsList = new List<FormsWorkflowGroupModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllFormsWorkFlowGroup", sqlConnection);
                if (groupID != null)
                    sqlCommand.Parameters.AddWithValue("@GroupID", groupID);
                else
                    sqlCommand.Parameters.AddWithValue("@GroupID", DBNull.Value);
                if (schoolBasedGroup != null)
                    sqlCommand.Parameters.AddWithValue("@SchoolBasedGroup", schoolBasedGroup);
                else
                    sqlCommand.Parameters.AddWithValue("@SchoolBasedGroup", DBNull.Value);
                if (isActive != null)
                    sqlCommand.Parameters.AddWithValue("@IsActive", isActive);
                else
                    sqlCommand.Parameters.AddWithValue("@IsActive", DBNull.Value);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FormsWorkflowGroupModel formModel;
                    while (sqlDataReader.Read())
                    {
                        formModel = new FormsWorkflowGroupModel();
                        formModel.GroupID = Convert.ToInt16(sqlDataReader["GroupID"].ToString());
                        formModel.GroupName = sqlDataReader["GroupName"] == DBNull.Value ? "" : sqlDataReader["GroupName"].ToString();
                        formModel.IsITGroup = bool.Parse(sqlDataReader["IsITGroup"] == DBNull.Value ? "0" : sqlDataReader["IsITGroup"].ToString());
                        formModel.SchoolBasedGroup = bool.Parse(sqlDataReader["SchoolBasedGroup"] == DBNull.Value ? "0" : sqlDataReader["SchoolBasedGroup"].ToString());
                        //academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        groupsList.Add(formModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return groupsList;
        }
        public List<FormsWorkflowPriorityModel> GetAllWorkflowPriorityList(Int16? priorityListID)
        {
            List<FormsWorkflowPriorityModel> priorityList = null;
            //FormsWorkflowGroupModel groups = new FormsWorkflowGroupModel();
            try
            {
                priorityList = new List<FormsWorkflowPriorityModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllWorkflowPriorityList", sqlConnection);
                if (priorityListID != null)
                    sqlCommand.Parameters.AddWithValue("@PriorityListID", priorityListID);
                else
                    sqlCommand.Parameters.AddWithValue("@PriorityListID", DBNull.Value);
                
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FormsWorkflowPriorityModel formModel;
                    while (sqlDataReader.Read())
                    {
                        formModel = new FormsWorkflowPriorityModel();
                        formModel.PriorityListID = Convert.ToInt16(sqlDataReader["PriorityListID"].ToString());
                        formModel.PriorityListName = sqlDataReader["PriorityListName"] == DBNull.Value ? "" : sqlDataReader["PriorityListName"].ToString();

                        priorityList.Add(formModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return priorityList;
        }

        public OperationDetails MarkToCompletePendingApprovals(int groupID, int formID, int companyTypeID,int PriorityListID, int UpdatedPriorityListID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_MarkToCompletePendingApprovals", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@GroupID", groupID);
                sqlCommand.Parameters.AddWithValue("@FormID", formID);
                sqlCommand.Parameters.AddWithValue("@CompanyTypeID", companyTypeID);
                sqlCommand.Parameters.AddWithValue("@PriorityListID", PriorityListID);
                sqlCommand.Parameters.AddWithValue("@UpdatedPriorityListID", UpdatedPriorityListID);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
              
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();               

            }
            catch (Exception exception)
            {
                op.InsertedRowId = -1;
                op.Message = "Technical error has occurred.";
                return op;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
    }
}
