﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class RecruitR1BudgetedDB : FormsDB
    {
        public List<RequestFormsProcessModel> SaveForm(RecruitR1BudgetedModel recruitR1BudgetedModel)
        {
            RequestFormsProcessModel requestFormsProcessModel;
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                DataTable filestable = ConvertFilesListToTable(recruitR1BudgetedModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_SaveHiringR1BudgetedForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", recruitR1BudgetedModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", recruitR1BudgetedModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", recruitR1BudgetedModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", recruitR1BudgetedModel.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", recruitR1BudgetedModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", recruitR1BudgetedModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", recruitR1BudgetedModel.ProjectData);
                if (recruitR1BudgetedModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", recruitR1BudgetedModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@PositionLocation", recruitR1BudgetedModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", recruitR1BudgetedModel.HeadCount);
                if (recruitR1BudgetedModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", recruitR1BudgetedModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (recruitR1BudgetedModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", recruitR1BudgetedModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", recruitR1BudgetedModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", recruitR1BudgetedModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", recruitR1BudgetedModel.RecruitFromID);
                sqlCommand.Parameters.AddWithValue("@PositionID", recruitR1BudgetedModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@SalaryRangesID", recruitR1BudgetedModel.SalaryRangesID);
                sqlCommand.Parameters.AddWithValue("@SalaryRanges", recruitR1BudgetedModel.SalaryRanges);
                sqlCommand.Parameters.AddWithValue("@ActualBudget", recruitR1BudgetedModel.ActualBudget);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", recruitR1BudgetedModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", recruitR1BudgetedModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", recruitR1BudgetedModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", recruitR1BudgetedModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", recruitR1BudgetedModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", recruitR1BudgetedModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", recruitR1BudgetedModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", recruitR1BudgetedModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", recruitR1BudgetedModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", recruitR1BudgetedModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", recruitR1BudgetedModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", recruitR1BudgetedModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", recruitR1BudgetedModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", recruitR1BudgetedModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", recruitR1BudgetedModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", recruitR1BudgetedModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", recruitR1BudgetedModel.ManPowerFileID);
                if (recruitR1BudgetedModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", recruitR1BudgetedModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", recruitR1BudgetedModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", recruitR1BudgetedModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", recruitR1BudgetedModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", recruitR1BudgetedModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }
        public RecruitR1BudgetedModel GetForm(int formProcessID, int userID)
        {
            RecruitR1BudgetedModel recruitR1BudgetedModel = new RecruitR1BudgetedModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHiringR1BudgetedForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        recruitR1BudgetedModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        recruitR1BudgetedModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        recruitR1BudgetedModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        recruitR1BudgetedModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        recruitR1BudgetedModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        recruitR1BudgetedModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                        recruitR1BudgetedModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        recruitR1BudgetedModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                        recruitR1BudgetedModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                        recruitR1BudgetedModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                        recruitR1BudgetedModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                        recruitR1BudgetedModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                        recruitR1BudgetedModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                        //Convert.ToInt32(reader["JobGradeID"] == DBNull.Value ? "0" : reader["JobGradeID"].ToString());
                        recruitR1BudgetedModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                        recruitR1BudgetedModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                        recruitR1BudgetedModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                        recruitR1BudgetedModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                        recruitR1BudgetedModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        recruitR1BudgetedModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        recruitR1BudgetedModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        recruitR1BudgetedModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                        recruitR1BudgetedModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                        recruitR1BudgetedModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                        recruitR1BudgetedModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                        recruitR1BudgetedModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                        recruitR1BudgetedModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                        recruitR1BudgetedModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                        recruitR1BudgetedModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                        recruitR1BudgetedModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                        recruitR1BudgetedModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                        recruitR1BudgetedModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                        recruitR1BudgetedModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                        recruitR1BudgetedModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                        recruitR1BudgetedModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                        recruitR1BudgetedModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                        recruitR1BudgetedModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                        recruitR1BudgetedModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                        recruitR1BudgetedModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                        //int.Parse(reader["DivisionID"] == DBNull.Value ? "0" : reader["DivisionID"].ToString());
                        recruitR1BudgetedModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        recruitR1BudgetedModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"] == DBNull.Value ? "0" : reader["CreatedBy"].ToString());
                        recruitR1BudgetedModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["CreatedOn"].ToString());
                        recruitR1BudgetedModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        recruitR1BudgetedModel.Comments = Convert.ToString(reader["Comments"]);
                        recruitR1BudgetedModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        recruitR1BudgetedModel.SalaryRanges = Convert.ToString(reader["SalaryRanges"]);
                        recruitR1BudgetedModel.RecCategoryID = Convert.ToInt16(reader["RecCategoryID"] == DBNull.Value ? "0" : reader["RecCategoryID"].ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (recruitR1BudgetedModel.JobDescriptionFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(recruitR1BudgetedModel.JobDescriptionFileID);
                    allFormsFilesModel.FormFileIDName = "JobDescriptionFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                if (recruitR1BudgetedModel.ManPowerFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(recruitR1BudgetedModel.ManPowerFileID);
                    allFormsFilesModel.FormFileIDName = "ManPowerFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                if (recruitR1BudgetedModel.OrgChartFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(recruitR1BudgetedModel.OrgChartFileID);
                    allFormsFilesModel.FormFileIDName = "OrgChartFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                recruitR1BudgetedModel.AllFormsFilesModelList = allFormsFilesModelList;

                recruitR1BudgetedModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return recruitR1BudgetedModel;
        }
        public int UpdateForm(RecruitR1BudgetedModel recruitR1BudgetedModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            int output = 0;
            try
            {
                DataTable filestable = ConvertFilesListToTable(recruitR1BudgetedModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateHiringR1BudgetedForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", recruitR1BudgetedModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", recruitR1BudgetedModel.ID);
                //sqlCommand.Parameters.AddWithValue("@CompanyIDs", recruitR1BudgetedModel.CompanyIDs);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", recruitR1BudgetedModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", recruitR1BudgetedModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", recruitR1BudgetedModel.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", recruitR1BudgetedModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", recruitR1BudgetedModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", recruitR1BudgetedModel.ProjectData);
                if (recruitR1BudgetedModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", recruitR1BudgetedModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@PositionLocation", recruitR1BudgetedModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", recruitR1BudgetedModel.HeadCount);
                if (recruitR1BudgetedModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", recruitR1BudgetedModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (recruitR1BudgetedModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", recruitR1BudgetedModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", recruitR1BudgetedModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", recruitR1BudgetedModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", recruitR1BudgetedModel.RecruitFromID);
                //sqlCommand.Parameters.AddWithValue("@PositionID", recruitR1BudgetedModel.PositionID);
                // sqlCommand.Parameters.AddWithValue("@SalaryRangesID", recruitR1BudgetedModel.SalaryRangesID);
                // sqlCommand.Parameters.AddWithValue("@ActualBudget", recruitR1BudgetedModel.ActualBudget);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", recruitR1BudgetedModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", recruitR1BudgetedModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", recruitR1BudgetedModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", recruitR1BudgetedModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", recruitR1BudgetedModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", recruitR1BudgetedModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", recruitR1BudgetedModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", recruitR1BudgetedModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", recruitR1BudgetedModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", recruitR1BudgetedModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", recruitR1BudgetedModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", recruitR1BudgetedModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", recruitR1BudgetedModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", recruitR1BudgetedModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", recruitR1BudgetedModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", recruitR1BudgetedModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", recruitR1BudgetedModel.ManPowerFileID);
                if (recruitR1BudgetedModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", recruitR1BudgetedModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", recruitR1BudgetedModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", recruitR1BudgetedModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", recruitR1BudgetedModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", recruitR1BudgetedModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                sqlCommand.Parameters.AddWithValue("@SalaryRanges", recruitR1BudgetedModel.SalaryRanges);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
        public RecruitR1BudgetedViewModel GetFormDetails(int formProcessID)
        {
            RecruitR1BudgetedViewModel recruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            RecruitR1BudgetedModel recruitR1BudgetedModel = new RecruitR1BudgetedModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHiringR1BudgetedFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        recruitR1BudgetedModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        recruitR1BudgetedModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        recruitR1BudgetedModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        recruitR1BudgetedModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        recruitR1BudgetedModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        recruitR1BudgetedModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                        recruitR1BudgetedModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        recruitR1BudgetedModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                        recruitR1BudgetedModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                        recruitR1BudgetedModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                        recruitR1BudgetedModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                        recruitR1BudgetedModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                        recruitR1BudgetedModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                        recruitR1BudgetedModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                        recruitR1BudgetedModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                        recruitR1BudgetedModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                        recruitR1BudgetedModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                        recruitR1BudgetedModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        recruitR1BudgetedModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        recruitR1BudgetedModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        recruitR1BudgetedModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                        recruitR1BudgetedModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                        recruitR1BudgetedModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                        recruitR1BudgetedModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                        recruitR1BudgetedModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                        recruitR1BudgetedModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                        recruitR1BudgetedModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                        recruitR1BudgetedModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                        recruitR1BudgetedModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                        recruitR1BudgetedModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                        recruitR1BudgetedModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                        recruitR1BudgetedModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                        recruitR1BudgetedModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                        recruitR1BudgetedModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                        recruitR1BudgetedModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                        recruitR1BudgetedModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                        recruitR1BudgetedModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                        recruitR1BudgetedModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                        recruitR1BudgetedModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        recruitR1BudgetedModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        recruitR1BudgetedViewModel.RequestID = int.Parse(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        recruitR1BudgetedViewModel.AcademicYearName = Convert.ToString(reader["AcademicYearName"]);
                        recruitR1BudgetedViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        recruitR1BudgetedViewModel.EmploymentModeName = Convert.ToString(reader["EmploymentModeName"]);
                        recruitR1BudgetedViewModel.EmployeeJobCategoryName = Convert.ToString(reader["EmployeeJobCategoryName"]);
                        recruitR1BudgetedViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                        recruitR1BudgetedViewModel.HMEmployeeName = Convert.ToString(reader["HMEmployeeName"]);
                        recruitR1BudgetedViewModel.ReportingEmployeeName = Convert.ToString(reader["ReportingEmployeeName"]);
                        recruitR1BudgetedViewModel.SourceName = Convert.ToString(reader["SourceName"]);
                        recruitR1BudgetedViewModel.RecruitFromName = Convert.ToString(reader["RecruitFromName"]);
                        recruitR1BudgetedViewModel.SalaryRangeName = Convert.ToString(reader["SalaryRangeName"]);
                        recruitR1BudgetedViewModel.AirfareFrequencyName = Convert.ToString(reader["AirfareFrequencyName"]);
                        recruitR1BudgetedViewModel.AirfareClassName = Convert.ToString(reader["AirfareClassName"]);
                        recruitR1BudgetedViewModel.JobDescriptionFileName = Convert.ToString(reader["JobDescriptionFileName"]);
                        recruitR1BudgetedViewModel.OrgChartFileName = Convert.ToString(reader["OrgChartFileName"]);
                        recruitR1BudgetedViewModel.ManPowerFileName = Convert.ToString(reader["ManPowerFileName"]);
                        recruitR1BudgetedViewModel.DivisionName = Convert.ToString(reader["DivisionName"]);
                        recruitR1BudgetedViewModel.RequestStatusName = Convert.ToString(reader["ReqStatusName"]);
                        recruitR1BudgetedViewModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                        recruitR1BudgetedViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                        recruitR1BudgetedViewModel.UserTypeName = Convert.ToString(reader["UserTypeName"]);
                        recruitR1BudgetedModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"] == DBNull.Value ? "0" : reader["CreatedBy"].ToString());
                        recruitR1BudgetedModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["CreatedOn"].ToString());
                        recruitR1BudgetedModel.SalaryRanges = Convert.ToString(reader["SalaryRanges"]);
                        recruitR1BudgetedModel.RecCategoryID = Convert.ToInt16(reader["RecCategoryID"] == DBNull.Value ? "0" : reader["RecCategoryID"].ToString());
                        recruitR1BudgetedViewModel.RecCategoryName = Convert.ToString(reader["RecCategoryName"]);
                    }
                }
                sqlConnection.Close();
                recruitR1BudgetedModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                recruitR1BudgetedViewModel.RecruitR1BudgetedModel = recruitR1BudgetedModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return recruitR1BudgetedViewModel;
        }

        #region R1 Status Report
        public List<R1StatusReportModel> GetAllR1StatusReportDetails(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int userId)
        {
            List<R1StatusReportModel> listR1StatusReportModel = null;
            try
            {               
                listR1StatusReportModel = new List<R1StatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetR1StatusReportDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@CurrentStatusID", CurrentStatusID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    R1StatusReportModel r1StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        r1StatusReportModel = new R1StatusReportModel();
                        r1StatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        r1StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);
                        r1StatusReportModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName"] == DBNull.Value ? "" : sqlDataReader["DepartmentName"]);
                        r1StatusReportModel.Project = Convert.ToString(sqlDataReader["Project"] == DBNull.Value ? "" : sqlDataReader["Project"]);
                        r1StatusReportModel.Requester = Convert.ToString(sqlDataReader["Requester"] == DBNull.Value ? "" : sqlDataReader["Requester"]);
                        r1StatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        r1StatusReportModel.Position = Convert.ToString(sqlDataReader["Position"] == DBNull.Value ? "" : sqlDataReader["Position"]);
                        r1StatusReportModel.SalaryRanges = Convert.ToString(sqlDataReader["SalaryRanges"] == DBNull.Value ? "" : sqlDataReader["SalaryRanges"]);
                        r1StatusReportModel.ReplacementEmployee = Convert.ToString(sqlDataReader["ReplacementEmployee"] == DBNull.Value ? "" : sqlDataReader["ReplacementEmployee"]);
                        r1StatusReportModel.HeadCount = Convert.ToInt32(sqlDataReader["HeadCount"] == DBNull.Value ? "0" : sqlDataReader["HeadCount"].ToString());
                        r1StatusReportModel.ReqStatusID = Convert.ToInt32(sqlDataReader["ReqStatusID"] == DBNull.Value ? "0" : sqlDataReader["ReqStatusID"].ToString());
                        r1StatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        r1StatusReportModel.IsBudgeted = Convert.ToString(sqlDataReader["IsBudgeted"] == DBNull.Value ? "" : sqlDataReader["IsBudgeted"]);
                        r1StatusReportModel.IsReplacement = Convert.ToString(sqlDataReader["IsReplacement"] == DBNull.Value ? "" : sqlDataReader["IsReplacement"]);
                        r1StatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        r1StatusReportModel.EmployeeJobCategory = Convert.ToString(sqlDataReader["EmployeeJobCategory"] == DBNull.Value ? "" : sqlDataReader["EmployeeJobCategory"]);
                        r1StatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        r1StatusReportModel.UrlString = Convert.ToString(sqlDataReader["UrlString"] == DBNull.Value ? "" : sqlDataReader["UrlString"]);
                        listR1StatusReportModel.Add(r1StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listR1StatusReportModel;
        }

        #endregion     

    }
}
