﻿using System;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Threading.Tasks;
using System.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class CompanyBasedHeirarchyDB : BaseDB
    {
        DataAccess.GeneralDB.DataHelper dataHelper;
        public List<FormsWorkflowGroupModel> GetAllGroups(bool? schoolBasedGroup)
        {
            List<FormsWorkflowGroupModel> groupsList = null;
            //FormsWorkflowGroupModel groups = new FormsWorkflowGroupModel();
            try
            {
                groupsList = new List<FormsWorkflowGroupModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GellAllGroups", sqlConnection);
                if (schoolBasedGroup != null)
                    sqlCommand.Parameters.AddWithValue("@SchoolBasedGroup", schoolBasedGroup);
                else
                    sqlCommand.Parameters.AddWithValue("@SchoolBasedGroup", DBNull.Value);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FormsWorkflowGroupModel formModel;
                    while (sqlDataReader.Read())
                    {
                        formModel = new FormsWorkflowGroupModel();

                        formModel.GroupID = Convert.ToInt16(sqlDataReader["GroupID"].ToString());
                        formModel.GroupName = sqlDataReader["GroupName"] == DBNull.Value ? "" : sqlDataReader["GroupName"].ToString();
                        //academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        groupsList.Add(formModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return groupsList;
        }
        public List<CompanyBasedHierarchyModel> GetAllCompanyGroups(int? CompanyID, Int16? GroupID, int? userId)
        {
            List<CompanyBasedHierarchyModel> companyList = null;
            try
            {
                companyList = new List<CompanyBasedHierarchyModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CompanyGroup_list", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@GroupID", GroupID);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                int i = 0;
                if (sqlDataReader.HasRows)
                {
                    CompanyBasedHierarchyModel companyModel;
                    while (sqlDataReader.Read())
                    {
                        companyModel = new CompanyBasedHierarchyModel();
                        companyModel.CBHierarchyID = Convert.ToInt32(sqlDataReader["CBHierarchyID"].ToString());
                        companyModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        companyModel.Name = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();
                        companyModel.GroupID = Convert.ToInt16(sqlDataReader["GroupID"].ToString());
                        companyModel.GroupName = sqlDataReader["GroupName"] == DBNull.Value ? "" : sqlDataReader["GroupName"].ToString();
                        companyModel.EmployeeID = Convert.ToInt16(sqlDataReader["EmployeeID"] == DBNull.Value ? "0" : sqlDataReader["EmployeeID"].ToString());
                        companyModel.EmployeeName = sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString();
                        companyModel.autoID = i++;
                        //academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        companyList.Add(companyModel);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return companyList;

        }
        public DataSet GetAllCompanyGroupsDataset(int? companyId, Int16? groupId)
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("Hr_Stp_CompanyGroup_list", sqlConnection);
            if (companyId.HasValue && companyId.Value > 0)
                da.SelectCommand.Parameters.AddWithValue("@CompanyID", companyId);
            if (groupId.HasValue && groupId.Value > 0)
                da.SelectCommand.Parameters.AddWithValue("@GroupID", groupId);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Columns.Contains("CBHierarchyID"))
                    ds.Tables[0].Columns.Remove("CBHierarchyID");
                if (ds.Tables[0].Columns.Contains("CompanyID"))
                    ds.Tables[0].Columns.Remove("CompanyID");
                if (ds.Tables[0].Columns.Contains("GroupID"))
                    ds.Tables[0].Columns.Remove("GroupID");
                if (ds.Tables[0].Columns.Contains("EmployeeID"))
                    ds.Tables[0].Columns.Remove("EmployeeID");
                for (int columnIndex = 0; columnIndex < ds.Tables[0].Columns.Count; columnIndex++)
                {
                    if (ds.Tables[0].Columns[columnIndex].ColumnName == "Name")
                        ds.Tables[0].Columns[columnIndex].ColumnName = "Organization Name";
                    else if (ds.Tables[0].Columns[columnIndex].ColumnName == "GroupName")
                        ds.Tables[0].Columns[columnIndex].ColumnName = "Approver Group";
                    else if (ds.Tables[0].Columns[columnIndex].ColumnName == "EmployeeName")
                        ds.Tables[0].Columns[columnIndex].ColumnName = "Approver";
                }
            }
            return ds;

        }

        public List<CompanyBasedHierarchyModel> GetAllCompanyGroupsByCompanyId(int CmpId, Int16 GrpId)
        {
            List<CompanyBasedHierarchyModel> companyList = null;
            try
            {
                companyList = new List<CompanyBasedHierarchyModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CompanyGroup_list_CompanyId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CmpId", CmpId);
                if (GrpId > 0)
                    //sqlCommand.Parameters.Add(new SqlParameter("@FormID", formID));
                    sqlCommand.Parameters.AddWithValue("@GrpId", GrpId);
                else
                    sqlCommand.Parameters.Add(new SqlParameter("@GrpId", DBNull.Value));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    CompanyBasedHierarchyModel companyModel;
                    while (sqlDataReader.Read())
                    {
                        companyModel = new CompanyBasedHierarchyModel();
                        companyModel.CompanyID = Convert.ToInt32(sqlDataReader["CompanyId"].ToString());
                        companyModel.Name = sqlDataReader["Name"] == DBNull.Value ? "" : sqlDataReader["Name"].ToString();
                        companyModel.GroupID = Convert.ToInt16(sqlDataReader["GroupID"].ToString());
                        companyModel.GroupName = sqlDataReader["GroupName"] == DBNull.Value ? "" : sqlDataReader["GroupName"].ToString();
                        //academicYearModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"].ToString());
                        companyList.Add(companyModel);

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return companyList;

        }
        public List<EmployeeDetailsModel> GetAllEmployee()
        {
            List<EmployeeDetailsModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeDetailsModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Get_AllActiveEmployees", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeDetailsModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeDetailsModel();

                        employeeModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        //employeeModel.FullName = Convert.ToString(sqlDataReader["FirstName"] + " " + sqlDataReader["LastName"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["Name"]);
                        //employeeModel.SurName_1 = Convert.ToString(sqlDataReader["LastName"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();

                // employeeList = employeeList.OrderBy(x => x.FirstName_1).ToList();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeID != 0).ToList();
        }
        public CompanyBasedHierarchyModel GetCompanyBasedHeirarchyById(int CBHierarchyID)
        {
            SqlParameter[] parameters =
                    {
                      new SqlParameter("@CBHierarchyID",CBHierarchyID)
                    };

            CompanyBasedHierarchyModel companyBasedHierarchyModel = new CompanyBasedHierarchyModel();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetCompanyBasedHeirachyByid", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            companyBasedHierarchyModel = new CompanyBasedHierarchyModel();
                            companyBasedHierarchyModel.CBHierarchyID = Convert.ToInt32(reader["CBHierarchyID"].ToString());
                            companyBasedHierarchyModel.CompanyID = Convert.ToInt32(reader["CompanyID"].ToString());
                            companyBasedHierarchyModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? 0 : reader["EmployeeID"]);
                            companyBasedHierarchyModel.GroupID = Convert.ToInt16(reader["GroupID"].ToString());


                        }
                    }
                }
                return companyBasedHierarchyModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }
        public OperationDetails UpdateCompanybasedHeirachy(CompanyBasedHierarchyModel companyBasedHierarchyModel)
        {
            OperationDetails oprationDetails = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_CompanyBasedHeirarchy_CUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CBHierarchyID", companyBasedHierarchyModel.CBHierarchyID);
                sqlCommand.Parameters.AddWithValue("@CompanyId", companyBasedHierarchyModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@GroupId", companyBasedHierarchyModel.GroupID);
                sqlCommand.Parameters.AddWithValue("@EmployeeId", companyBasedHierarchyModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", companyBasedHierarchyModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", companyBasedHierarchyModel.ModifiedOn);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.Message = "Approver details saved successfully.";
            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = "Technical error has occurred." + exception.Message;
                //oprationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;

        }
        public OperationDetails DeleteCompanyDetails(int CBHierarchyID)
        {
            OperationDetails oprationDetails = new OperationDetails();
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@CBHierarchyID",CBHierarchyID)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_CompanyBasedHeirarchy_Delete", parameters);
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.Message = "Approver record deleted.";

            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;
                //oprationDetails.InsertedRowId = -1;
            }
            finally
            {

            }

            return oprationDetails;
        }
        public OperationDetails UpdatMultipleEmployees(List<CompanyBasedHierarchyModel> heirarchyList, CompanyBasedHierarchyModel heirarchyModel)
        {
            OperationDetails oprationDetails = new OperationDetails();
            DataTable dt = new DataTable();
            dt.Columns.Add("CBHierarchyID", typeof(Int32));
            dt.Columns.Add("EmployeeID", typeof(Int32));
            foreach (var item in heirarchyList)
            {
                dt.Rows.Add(item.CBHierarchyID, item.EmployeeID);
            }

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_UpdateMultipleCompanyHeirarchy", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MultipleEmployee", dt);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", heirarchyModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", heirarchyModel.ModifiedOn);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                oprationDetails.Success = true;
                oprationDetails.CssClass = "success";
                oprationDetails.Message = "Approver details saved successfully.";
            }
            catch (Exception exception)
            {
                oprationDetails.Success = false;
                oprationDetails.CssClass = "error";
                oprationDetails.Message = exception.Message;
                //oprationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return oprationDetails;
        }

    }
}
