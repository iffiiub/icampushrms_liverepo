﻿using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
   public class CompanyTypeDB: BaseDB
    {
        public List<CompanyTypeModel> GetAllCompanyType()
        {
            List<CompanyTypeModel> companyTypeList = new List<CompanyTypeModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT CompanyTypeID,CompanyTypeName FROM HR_CompanyType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    CompanyTypeModel companyTypeModel;
                    while (sqlDataReader.Read())
                    {
                        companyTypeModel = new CompanyTypeModel();
                        companyTypeModel.CompanyTypeID = Convert.ToInt16(sqlDataReader["CompanyTypeID"].ToString());
                        companyTypeModel.CompanyTypeName = Convert.ToString(sqlDataReader["CompanyTypeName"]);
                        companyTypeList.Add(companyTypeModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return companyTypeList;
        }

    }
}
