﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;
using System.Globalization;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class FormLeaveRequestDB : FormsDB
    {
        public List<RequestFormsProcessModel> SaveForm(LeaveRequestModel leaveRequestModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                int i = 0;
                DataTable filestable = ConvertFilesListToTable(leaveRequestModel.AllFormsFilesModelList);
                DataTable dt = new DataTable();
                dt.Columns.Add("ID", System.Type.GetType("System.Int16"));
                dt.Columns.Add("TaskAssignEmpID", typeof(Int32));
                dt.Columns.Add("TaskDetail");
                foreach (EmployeeTaskHandoverModel et in leaveRequestModel.EmployeeTaskHandoverList)
                {
                    // if (leaveRequestModel.EmployeeTaskHandoverList[i].TaskAssignEmpID >= 0)
                    if (et.TaskAssignEmpID >= 0)
                    {
                        i++;
                        dt.Rows.Add(i,
                            et.TaskAssignEmpID,
                            et.TaskDetail);
                    }
                }
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_SaveEmployeeLeaveRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", leaveRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@TaskHandOver", dt);
                sqlCommand.Parameters.AddWithValue("@CompanyId", leaveRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", leaveRequestModel.VacationTypeID);
                sqlCommand.Parameters.AddWithValue("@HalfDayLeave", leaveRequestModel.HalfDayLeave);
                sqlCommand.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(leaveRequestModel.FromDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(leaveRequestModel.ToDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@InCountry", leaveRequestModel.InCountry);
                sqlCommand.Parameters.AddWithValue("@OutCountry", leaveRequestModel.OutCountry);
                sqlCommand.Parameters.AddWithValue("@CountryID", leaveRequestModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@ContactInfo1", leaveRequestModel.ContactInfo1);
                sqlCommand.Parameters.AddWithValue("@ContactInfo2", leaveRequestModel.ContactInfo2);
                sqlCommand.Parameters.AddWithValue("@ContactInfo3", leaveRequestModel.ContactInfo3);
                sqlCommand.Parameters.AddWithValue("@MadicalCerFileID", leaveRequestModel.MadicalCerFileID);
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", leaveRequestModel.ReqStatusID);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", leaveRequestModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CreatedOn", leaveRequestModel.CreatedOn);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", leaveRequestModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", leaveRequestModel.ModifiedOn);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", leaveRequestModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", leaveRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AvailableLeaveDays", leaveRequestModel.AvailableLeaveDays);
                sqlCommand.Parameters.AddWithValue("@LapsingDays", leaveRequestModel.LapsingDays);
                sqlCommand.Parameters.AddWithValue("@ExtendedLapsingDays", leaveRequestModel.ExtendedLapsingDays);
                if (leaveRequestModel.LapsingDate != null)
                    sqlCommand.Parameters.AddWithValue("@LapsingDate", leaveRequestModel.LapsingDate);
                else
                    sqlCommand.Parameters.AddWithValue("@LapsingDate", DBNull.Value);
                if (leaveRequestModel.ExtendedLapsingDate != null)
                    sqlCommand.Parameters.AddWithValue("@ExtendedLapsingDate", leaveRequestModel.ExtendedLapsingDate);
                else
                    sqlCommand.Parameters.AddWithValue("@ExtendedLapsingDate", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@RemainingDays", leaveRequestModel.RemainingDays);
                sqlCommand.Parameters.AddWithValue("@RequestedDays", leaveRequestModel.RequestedDays);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                requestFormsProcessModelList = null;
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }
        public int UpdateForm(LeaveRequestModel leaveRequestModel)
        {
            //  RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            //string message = "";
            int output = 0;
            try
            {
                int i = 0;


                DataTable filestable = ConvertFilesListToTable(leaveRequestModel.AllFormsFilesModelList);
                DataTable dt = new DataTable();
                dt.Columns.Add("ID", System.Type.GetType("System.Int16"));
                dt.Columns.Add("TaskAssignEmpID", typeof(Int32));
                dt.Columns.Add("TaskDetail");
                foreach (EmployeeTaskHandoverModel et in leaveRequestModel.EmployeeTaskHandoverList)
                {
                    if (leaveRequestModel.EmployeeTaskHandoverList[i].TaskAssignEmpID >= 0)
                    {
                        i++;
                        dt.Rows.Add(et.TaskHandOverID,
                            et.TaskAssignEmpID,
                            et.TaskDetail);
                    }
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateEmployeeLeaveRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ID", leaveRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", leaveRequestModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@TaskHandOver", dt);
                sqlCommand.Parameters.AddWithValue("@VacationTypeID", leaveRequestModel.VacationTypeID);
                sqlCommand.Parameters.AddWithValue("@HalfDayLeave", leaveRequestModel.HalfDayLeave);
                sqlCommand.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(leaveRequestModel.FromDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(leaveRequestModel.ToDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@InCountry", leaveRequestModel.InCountry);
                sqlCommand.Parameters.AddWithValue("@OutCountry", leaveRequestModel.OutCountry);
                sqlCommand.Parameters.AddWithValue("@CountryID", leaveRequestModel.CountryID);
                sqlCommand.Parameters.AddWithValue("@ContactInfo1", leaveRequestModel.ContactInfo1);
                sqlCommand.Parameters.AddWithValue("@ContactInfo2", leaveRequestModel.ContactInfo2);
                sqlCommand.Parameters.AddWithValue("@ContactInfo3", leaveRequestModel.ContactInfo3);
                sqlCommand.Parameters.AddWithValue("@MadicalCerFileID", leaveRequestModel.MadicalCerFileID);
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", leaveRequestModel.ReqStatusID);
                sqlCommand.Parameters.AddWithValue("@Comments", leaveRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", leaveRequestModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", leaveRequestModel.ModifiedOn);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", leaveRequestModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@AvailableLeaveDays", leaveRequestModel.AvailableLeaveDays);
                sqlCommand.Parameters.AddWithValue("@LapsingDays", leaveRequestModel.LapsingDays);
                sqlCommand.Parameters.AddWithValue("@ExtendedLapsingDays", leaveRequestModel.ExtendedLapsingDays);
                if (leaveRequestModel.LapsingDate != null)
                    sqlCommand.Parameters.AddWithValue("@LapsingDate", leaveRequestModel.LapsingDate);
                else
                    sqlCommand.Parameters.AddWithValue("@LapsingDate", DBNull.Value);
                if (leaveRequestModel.ExtendedLapsingDate != null)
                    sqlCommand.Parameters.AddWithValue("@ExtendedLapsingDate", leaveRequestModel.ExtendedLapsingDate);
                else
                    sqlCommand.Parameters.AddWithValue("@ExtendedLapsingDate", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@RemainingDays", leaveRequestModel.RemainingDays);
                sqlCommand.Parameters.AddWithValue("@RequestedDays", leaveRequestModel.RequestedDays);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
        public int UpdateLeaveRequest(LeaveRequestModel leaveRequestModel)
        {
            int output = 0;
            try
            {
                int i = 0;
                DataTable dt = new DataTable();
                dt.Columns.Add("ID", System.Type.GetType("System.Int16"));
                dt.Columns.Add("TaskAssignEmpID", typeof(Int32));
                dt.Columns.Add("TaskDetail");
                foreach (EmployeeTaskHandoverModel et in leaveRequestModel.EmployeeTaskHandoverList)
                {
                   // if (leaveRequestModel.EmployeeTaskHandoverList[i].TaskAssignEmpID >= 0)
                   if(et.TaskAssignEmpID>0)
                    {
                      //  i++;
                        dt.Rows.Add(et.TaskHandOverID,
                            et.TaskAssignEmpID,
                            et.TaskDetail);
                    }
                }

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateLeaveRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormInstanceID", leaveRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(leaveRequestModel.FromDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(leaveRequestModel.ToDate, HRMSDateFormat, CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@TaskHandOver", dt);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
        public LeaveRequestModel GetForm(int formProcessID, int userID)
        {
            LeaveRequestModel leaveRequestModel = new LeaveRequestModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFormLeaveRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        leaveRequestModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        leaveRequestModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        leaveRequestModel.VacationTypeID = Convert.ToInt16(reader["VacationTypeID"] == DBNull.Value ? "0" : reader["VacationTypeID"].ToString());
                        leaveRequestModel.Name = Convert.ToString(reader["Name"]);
                        leaveRequestModel.HalfDayLeave = bool.Parse(reader["HalfDayLeave"] == DBNull.Value ? "0" : reader["HalfDayLeave"].ToString());
                        leaveRequestModel.FromDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["FromDate"].ToString());
                        leaveRequestModel.ToDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ToDate"].ToString());
                        leaveRequestModel.InCountry = bool.Parse(reader["InCountry"] == DBNull.Value ? "0" : reader["InCountry"].ToString());
                        leaveRequestModel.OutCountry = bool.Parse(reader["OutCountry"] == DBNull.Value ? "0" : reader["OutCountry"].ToString());
                        leaveRequestModel.CountryID = Convert.ToInt16(reader["CountryID"] == DBNull.Value ? "0" : reader["CountryID"].ToString());
                        leaveRequestModel.CountryName = Convert.ToString(reader["CountryName_1"]);
                        leaveRequestModel.ContactInfo1 = Convert.ToString(reader["ContactInfo1"]);
                        leaveRequestModel.ContactInfo2 = Convert.ToString(reader["ContactInfo2"]);
                        leaveRequestModel.ContactInfo3 = Convert.ToString(reader["ContactInfo3"]);
                        leaveRequestModel.Comments = Convert.ToString(reader["Comments"]);
                        leaveRequestModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        leaveRequestModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        leaveRequestModel.AvailableLeaveDays = Convert.ToDecimal(reader["AvailableLeaveDays"] == DBNull.Value ? "0" : reader["AvailableLeaveDays"].ToString());
                        leaveRequestModel.LapsingDays = Convert.ToDecimal(reader["LapsingDays"] == DBNull.Value ? "0" : reader["LapsingDays"].ToString());
                        leaveRequestModel.ExtendedLapsingDays = Convert.ToDecimal(reader["ExtendedLapsingDays"] == DBNull.Value ? "0" : reader["ExtendedLapsingDays"].ToString());
                        leaveRequestModel.RemainingDays = Convert.ToDecimal(reader["RemainingDays"] == DBNull.Value ? "0" : reader["RemainingDays"].ToString());
                        leaveRequestModel.RequestedDays = Convert.ToDecimal(reader["RequestedDays"] == DBNull.Value ? "0" : reader["RequestedDays"].ToString());
                        leaveRequestModel.LapsingDate = reader["LapsingDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse(reader["LapsingDate"].ToString());
                        leaveRequestModel.ExtendedLapsingDate = reader["ExtendedLapsingDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse(reader["ExtendedLapsingDate"].ToString());
                        leaveRequestModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
                        leaveRequestModel.MadicalCerFileID = Convert.ToInt32(reader["MadicalCerFileID"].ToString());
                        leaveRequestModel.CancelLeaveRequest = bool.Parse(reader["CancelLeaveRequest"] == DBNull.Value ? "0" : reader["CancelLeaveRequest"].ToString());
                        leaveRequestModel.CancelComments = Convert.ToString(reader["CancelComments"]);
                        leaveRequestModel.ModifiedByName = Convert.ToString(reader["ModifiedByName"]);
                        leaveRequestModel.ModifiedOn =  Convert.ToDateTime(reader["ModifiedOn"].ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (leaveRequestModel.MadicalCerFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(leaveRequestModel.MadicalCerFileID);
                    allFormsFilesModel.FormFileIDName = "leaveRequestModel.MadicalCerFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }

                leaveRequestModel.AllFormsFilesModelList = allFormsFilesModelList;

                leaveRequestModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return leaveRequestModel;
        }
        public List<EmployeeTaskHandoverModel> GetTaskDetial(int ID)
        {
            List<EmployeeTaskHandoverModel> taskList = null;
            try
            {
                taskList = new List<EmployeeTaskHandoverModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetTaskhandOverList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", ID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeTaskHandoverModel taskModel;
                    while (sqlDataReader.Read())
                    {
                        taskModel = new EmployeeTaskHandoverModel();
                        taskModel.ID = Convert.ToInt32(sqlDataReader["ID"].ToString());
                        taskModel.TaskHandOverID = Convert.ToInt32(sqlDataReader["TaskHandOverID"] == DBNull.Value ? "0" : sqlDataReader["TaskHandOverID"].ToString());
                        taskModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"] == DBNull.Value ? "0" : sqlDataReader["EmployeeId"].ToString());
                        taskModel.FirstName = Convert.ToString(sqlDataReader["FirstName"] == DBNull.Value ? "" : sqlDataReader["FirstName"]);
                        taskModel.LastName = Convert.ToString(sqlDataReader["LastName"] == DBNull.Value ? "" : sqlDataReader["LastName"]);
                        taskModel.EmployeeAlternativeID = Convert.ToString(sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"]);                     
                        var companyShortName = Convert.ToString(sqlDataReader["CompanyShortName"] == DBNull.Value ? "" : (" - " + sqlDataReader["CompanyShortName"]));
                        taskModel.FullName = "(" + taskModel.EmployeeAlternativeID + ") " + taskModel.FirstName + " " + taskModel.MiddleName + " " + taskModel.LastName + companyShortName;
                       // taskModel.FullName = "(" + taskModel.EmployeeAlternativeID + ") " + taskModel.FirstName + " " + taskModel.LastName;
                        taskModel.TaskAssignEmpID = Convert.ToInt32(sqlDataReader["TaskAssignEmpID"] == DBNull.Value ? "0" : sqlDataReader["TaskAssignEmpID"].ToString());
                        taskModel.TaskDetail = sqlDataReader["TaskDetail"] == DBNull.Value ? "" : sqlDataReader["TaskDetail"].ToString();
                        taskList.Add(taskModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return taskList;
        }
        public FormLeaverequestviewModel GetFormDetails(int formProcessID)
        {
            FormLeaverequestviewModel leaveRequestViewModel = new FormLeaverequestviewModel();
            LeaveRequestModel leaverequestmodel = new LeaveRequestModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFormLeaveRequestDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        leaveRequestViewModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        leaveRequestViewModel.ReqStatusID = Convert.ToInt16((reader["ReqStatusID"]).ToString());
                        leaveRequestViewModel.VacationTypeID = Convert.ToInt16(reader["VacationTypeID"] == DBNull.Value ? "0" : reader["VacationTypeID"].ToString());
                        leaveRequestViewModel.Name = Convert.ToString(reader["Name"]);
                        leaveRequestViewModel.HalfDayLeave = bool.Parse(reader["HalfDayLeave"] == DBNull.Value ? "0" : reader["HalfDayLeave"].ToString());
                        leaveRequestViewModel.FromDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["FromDate"].ToString());
                        leaveRequestViewModel.ToDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ToDate"].ToString());
                        leaveRequestViewModel.InCountry = bool.Parse(reader["InCountry"] == DBNull.Value ? "0" : reader["InCountry"].ToString());
                        leaveRequestViewModel.OutCountry = bool.Parse(reader["OutCountry"] == DBNull.Value ? "0" : reader["OutCountry"].ToString());
                        leaveRequestViewModel.CountryID = Convert.ToInt16(reader["CountryID"] == DBNull.Value ? "0" : reader["CountryID"].ToString());
                        leaveRequestViewModel.CountryName = Convert.ToString(reader["CountryName_1"]);
                        leaveRequestViewModel.ContactInfo1 = Convert.ToString(reader["ContactInfo1"]);
                        leaveRequestViewModel.ContactInfo2 = Convert.ToString(reader["ContactInfo2"]);
                        leaveRequestViewModel.ContactInfo3 = Convert.ToString(reader["ContactInfo3"]);
                        leaveRequestViewModel.RequestID = int.Parse(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        leaveRequestViewModel.AvailableLeaveDays = Convert.ToDecimal(reader["AvailableLeaveDays"] == DBNull.Value ? "0" : reader["AvailableLeaveDays"].ToString());
                        leaveRequestViewModel.LapsingDays = Convert.ToDecimal(reader["LapsingDays"] == DBNull.Value ? "0" : reader["LapsingDays"].ToString());
                        leaveRequestViewModel.ExtendedLapsingDays = Convert.ToDecimal(reader["ExtendedLapsingDays"] == DBNull.Value ? "0" : reader["ExtendedLapsingDays"].ToString());
                        leaveRequestViewModel.RemainingDays = reader["RemainingDays"] == DBNull.Value ? "0" : reader["RemainingDays"].ToString();
                        leaveRequestViewModel.LapsingDate = string.IsNullOrEmpty(reader["LapsingDate"].ToString()) ? string.Empty : HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["LapsingDate"].ToString());
                        leaveRequestViewModel.ExtendedLapsingDate = string.IsNullOrEmpty(reader["ExtendedLapsingDate"].ToString()) ? string.Empty : HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["ExtendedLapsingDate"].ToString());
                        leaveRequestViewModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"]);
                        leaveRequestViewModel.MadicalCerFileID = Convert.ToInt32(reader["MadicalCerFileID"]);
                        leaveRequestViewModel.RequestedDays = Convert.ToString(reader["RequestedDays"]);
                        leaveRequestViewModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
                        //Pooja's Changes
                        leaveRequestViewModel.CancelLeaveRequest = Convert.ToBoolean(reader["CancelLeaveRequest"]);
                        leaveRequestViewModel.ModifiedBy = Convert.ToInt32(reader["ModifiedBy"]);
                        leaveRequestViewModel.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
                        leaveRequestViewModel.EmployeeName = reader["EmployeeName"].ToString();
                        leaveRequestViewModel.CancelComments = reader["CancelComments"].ToString();
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (leaveRequestViewModel.MadicalCerFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(leaveRequestViewModel.MadicalCerFileID);
                    allFormsFilesModel.FormFileIDName = "leaveRequestModel.MadicalCerFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }

                leaveRequestViewModel.AllFormsFilesModelList = allFormsFilesModelList;
                leaverequestmodel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                leaveRequestViewModel.leaverequestmodel = leaverequestmodel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return leaveRequestViewModel;
        }
        public List<EmployeeTaskHandoverModel> GetAllEmployeeByDepartment(int UserId)
        {
            List<EmployeeTaskHandoverModel> employeeList = null;
            try
            {
                employeeList = new List<EmployeeTaskHandoverModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_StpGetEmployeeBasedOnLoginEmployeeDepartment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    EmployeeTaskHandoverModel employeeModel;
                    while (sqlDataReader.Read())
                    {
                        employeeModel = new EmployeeTaskHandoverModel();

                        employeeModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeId"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        employeeModel.FullName = Convert.ToString(sqlDataReader["FirstName"] + " " + sqlDataReader["LastName"]);
                        employeeModel.FirstName = Convert.ToString(sqlDataReader["FirstName"]);
                        employeeModel.LastName = Convert.ToString(sqlDataReader["LastName"]);
                        employeeList.Add(employeeModel);

                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeList.Where(x => x.EmployeeId != 0).ToList();
        }

        public ValidateProbationModel GetEmployeeProbationDetails(int employeeID)
        {
            ValidateProbationModel obj = new ValidateProbationModel();
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",employeeID),
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetEmployeeProbationDetails", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            obj.IsEmployeeUnderProbation = Convert.ToBoolean(reader["IsEmployeeUnderProbation"].ToString());
                            obj.IsProbationCompletedBeforeProbationPeriod = Convert.ToBoolean(reader["IsProbationCompletedBeforeProbationPeriod"].ToString());
                            obj.IsProbationConfirmationPending = Convert.ToBoolean(reader["IsProbationConfirmationPending"].ToString());
                            
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return obj;
        }

    


    public int SaveFinalLeaveApproval(int FormProcessID)
        {                       
            int output = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveFinalLeaveApproval", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                //sqlCommand.Parameters.AddWithValue("@TotalAppliedLeave", TotalAppliedLeave);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                //output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;
                output = 1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return output;
        }

        public bool IsPendingLeaveExists(int employeeID,int leaveRequestID)
        {
            bool IsExists = false;
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",employeeID),
                         new SqlParameter("@LeaveRequestID",leaveRequestID)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_IsPendingLeaveRequestExists", parameters))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IsExists = Convert.ToBoolean(reader["IsExists"].ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return IsExists;
        }

        public OperationDetails RejectForm(RequestFormsApproveModel requestFormsApproveModel)
        {
            OperationDetails op = new OperationDetails(); 
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_RejectLeaveRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", requestFormsApproveModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ApproverEmployeeID", requestFormsApproveModel.ApproverEmployeeID);
                sqlCommand.Parameters.AddWithValue("@IsCancelLeave",0);
                sqlCommand.Parameters.AddWithValue("@Comments", requestFormsApproveModel.Comments);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }      
        public OperationDetails CancelLeaveRequest(int? formProcessId, string cancelComments, int userId)
        {
            OperationDetails operationDetails = new OperationDetails();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_RejectLeaveRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessId);
                sqlCommand.Parameters.AddWithValue("@Comments", cancelComments);
                sqlCommand.Parameters.AddWithValue("@IsCancelLeave", 1);
                sqlCommand.Parameters.AddWithValue("@ApproverEmployeeID", userId);
                SqlParameter OperationOutput = new SqlParameter("@Output", SqlDbType.Int);
                OperationOutput.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationOutput);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar,1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = Convert.ToString(OperationMessage.Value);
                //"Leave Request is cancelled successfully";
                operationDetails.InsertedRowId = Convert.ToInt32(OperationOutput.Value);

            }
            catch(Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.InsertedRowId = -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        #region Leave Request Status Report
        public List<LeaveRequestStatusReportModel> GetLeaveRequestStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int userId)
        {
            List<LeaveRequestStatusReportModel> listStatusReportModel = null;
            try
            {
                listStatusReportModel = new List<LeaveRequestStatusReportModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetLeaveRequestStatusReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@CurrentStatusID", CurrentStatusID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LeaveRequestStatusReportModel StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        StatusReportModel = new LeaveRequestStatusReportModel();
                        StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);                      
                        StatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        StatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        StatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"].ToString());
                        StatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        StatusReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        StatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);                       
                        StatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        StatusReportModel.LeaveType = Convert.ToString(sqlDataReader["LeaveType"] == DBNull.Value ? "" : sqlDataReader["LeaveType"]);
                        StatusReportModel.LeaveDateFrom = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["LeaveDateFrom"] == DBNull.Value ? "" : sqlDataReader["LeaveDateFrom"]));
                        StatusReportModel.LeaveDateTo = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["LeaveDateTo"] == DBNull.Value ? "" : sqlDataReader["LeaveDateTo"]));
                        StatusReportModel.RequestedDays = Convert.ToDecimal(sqlDataReader["RequestedDays"] == DBNull.Value ? "0" : sqlDataReader["RequestedDays"]);
                        StatusReportModel.BeforeAvailableLeaveDays = Convert.ToDecimal(sqlDataReader["BeforeAvailableLeaveDays"] == DBNull.Value ? "0" : sqlDataReader["BeforeAvailableLeaveDays"]);
                        StatusReportModel.AfterAvailableLeaveDays = Convert.ToDecimal(sqlDataReader["AfterAvailableLeaveDays"] == DBNull.Value ? "0" : sqlDataReader["AfterAvailableLeaveDays"]);
                        StatusReportModel.PublicHolidays = Convert.ToDecimal(sqlDataReader["PublicHolidays"] == DBNull.Value ? "0" : sqlDataReader["PublicHolidays"]);
                        StatusReportModel.LeaveAdjusted = Convert.ToDecimal(sqlDataReader["LeaveAdjusted"] == DBNull.Value ? "0" : sqlDataReader["LeaveAdjusted"]);
                        StatusReportModel.TotalDays = Convert.ToDecimal(sqlDataReader["TotalDays"] == DBNull.Value ? "0" : sqlDataReader["TotalDays"]);
                        StatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        StatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        StatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        listStatusReportModel.Add(StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listStatusReportModel;
        }
        #endregion
    }
}
