﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class RecruitR1UnBudgetedDB : FormsDB
    {
        public List<RequestFormsProcessModel> SaveForm(RecruitR1UnBudgetedModel recruitR1BudgetedModel)
        {
            RequestFormsProcessModel requestFormsProcessModel;
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                DataTable filestable = ConvertFilesListToTable(recruitR1BudgetedModel.AllFormsFilesModelList);             
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_SaveHiringR1UnBudgetedForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CompanyID", recruitR1BudgetedModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", recruitR1BudgetedModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", recruitR1BudgetedModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", recruitR1BudgetedModel.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", recruitR1BudgetedModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", recruitR1BudgetedModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", recruitR1BudgetedModel.ProjectData);
                if (recruitR1BudgetedModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", recruitR1BudgetedModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@PositionLocation", recruitR1BudgetedModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", recruitR1BudgetedModel.HeadCount);
                if (recruitR1BudgetedModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", recruitR1BudgetedModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (recruitR1BudgetedModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", recruitR1BudgetedModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", recruitR1BudgetedModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", recruitR1BudgetedModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", recruitR1BudgetedModel.RecruitFromID);
                sqlCommand.Parameters.AddWithValue("@PositionID", recruitR1BudgetedModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@SalaryRangesID", recruitR1BudgetedModel.SalaryRangesID);
                sqlCommand.Parameters.AddWithValue("@SalaryRanges", recruitR1BudgetedModel.SalaryRanges);
                sqlCommand.Parameters.AddWithValue("@ActualBudget", recruitR1BudgetedModel.ActualBudget);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", recruitR1BudgetedModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", recruitR1BudgetedModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", recruitR1BudgetedModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", recruitR1BudgetedModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", recruitR1BudgetedModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", recruitR1BudgetedModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", recruitR1BudgetedModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", recruitR1BudgetedModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", recruitR1BudgetedModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", recruitR1BudgetedModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", recruitR1BudgetedModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", recruitR1BudgetedModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", recruitR1BudgetedModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", recruitR1BudgetedModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", recruitR1BudgetedModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", recruitR1BudgetedModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", recruitR1BudgetedModel.ManPowerFileID);
                if (recruitR1BudgetedModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", recruitR1BudgetedModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", recruitR1BudgetedModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", recruitR1BudgetedModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", recruitR1BudgetedModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", recruitR1BudgetedModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);             
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return requestFormsProcessModelList;
        }
        public RecruitR1UnBudgetedModel GetForm(int formProcessID, int userID)
        {
            RecruitR1UnBudgetedModel recruitR1UnBudgetedModel = new RecruitR1UnBudgetedModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHiringR1UnBudgetedForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recruitR1UnBudgetedModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        recruitR1UnBudgetedModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        recruitR1UnBudgetedModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        recruitR1UnBudgetedModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        recruitR1UnBudgetedModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        recruitR1UnBudgetedModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                        recruitR1UnBudgetedModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        recruitR1UnBudgetedModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                        recruitR1UnBudgetedModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                        recruitR1UnBudgetedModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                        recruitR1UnBudgetedModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                        recruitR1UnBudgetedModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                        recruitR1UnBudgetedModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                        recruitR1UnBudgetedModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                        recruitR1UnBudgetedModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                        recruitR1UnBudgetedModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                        recruitR1UnBudgetedModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                        recruitR1UnBudgetedModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        recruitR1UnBudgetedModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        recruitR1UnBudgetedModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        recruitR1UnBudgetedModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                        recruitR1UnBudgetedModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                        recruitR1UnBudgetedModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                        recruitR1UnBudgetedModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                        recruitR1UnBudgetedModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                        recruitR1UnBudgetedModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                        recruitR1UnBudgetedModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                        recruitR1UnBudgetedModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                        recruitR1UnBudgetedModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                        recruitR1UnBudgetedModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                        recruitR1UnBudgetedModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                        recruitR1UnBudgetedModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                        recruitR1UnBudgetedModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                        recruitR1UnBudgetedModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                        recruitR1UnBudgetedModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                        recruitR1UnBudgetedModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                        recruitR1UnBudgetedModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                        recruitR1UnBudgetedModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                        recruitR1UnBudgetedModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        recruitR1UnBudgetedModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"] == DBNull.Value ? "0" : reader["CreatedBy"].ToString());
                        recruitR1UnBudgetedModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["CreatedOn"].ToString());
                        recruitR1UnBudgetedModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        recruitR1UnBudgetedModel.Comments = Convert.ToString(reader["Comments"]);
                        recruitR1UnBudgetedModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        recruitR1UnBudgetedModel.SalaryRanges = Convert.ToString(reader["SalaryRanges"]);
                        recruitR1UnBudgetedModel.RecCategoryID = Convert.ToInt16(reader["RecCategoryID"] == DBNull.Value ? "0" : reader["RecCategoryID"].ToString());
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (recruitR1UnBudgetedModel.JobDescriptionFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(recruitR1UnBudgetedModel.JobDescriptionFileID);
                    allFormsFilesModel.FormFileIDName = "JobDescriptionFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                if (recruitR1UnBudgetedModel.ManPowerFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(recruitR1UnBudgetedModel.ManPowerFileID);
                    allFormsFilesModel.FormFileIDName = "ManPowerFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                if (recruitR1UnBudgetedModel.OrgChartFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(recruitR1UnBudgetedModel.OrgChartFileID);
                    allFormsFilesModel.FormFileIDName = "OrgChartFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                recruitR1UnBudgetedModel.AllFormsFilesModelList = allFormsFilesModelList;

                recruitR1UnBudgetedModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return recruitR1UnBudgetedModel;
        }
        public int UpdateForm(RecruitR1UnBudgetedModel recruitR1BudgetedModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            int output = 0;
            try
            {
                DataTable filestable = ConvertFilesListToTable(recruitR1BudgetedModel.AllFormsFilesModelList);
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();

                sqlCommand = new SqlCommand("HR_Stp_UpdateHiringR1UnBudgetedForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", recruitR1BudgetedModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ID", recruitR1BudgetedModel.ID);
                sqlCommand.Parameters.AddWithValue("@EmploymentModeId", recruitR1BudgetedModel.EmploymentModeID);
                sqlCommand.Parameters.AddWithValue("@UserTypeID", recruitR1BudgetedModel.UserTypeID);
                sqlCommand.Parameters.AddWithValue("@RecCategoryID", recruitR1BudgetedModel.RecCategoryID);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", recruitR1BudgetedModel.EmployeeJobCategoryID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", recruitR1BudgetedModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@ProjectData", recruitR1BudgetedModel.ProjectData);
                if (recruitR1BudgetedModel.HMEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", recruitR1BudgetedModel.HMEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@HMEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@PositionLocation", recruitR1BudgetedModel.PositionLocation);
                sqlCommand.Parameters.AddWithValue("@HeadCount", recruitR1BudgetedModel.HeadCount);
                if (recruitR1BudgetedModel.JobGradeID != null)
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", recruitR1BudgetedModel.JobGradeID);
                else
                    sqlCommand.Parameters.AddWithValue("@JobGradeID", DBNull.Value);
                if (recruitR1BudgetedModel.ReportingEmployeeID != null)
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", recruitR1BudgetedModel.ReportingEmployeeID);
                else
                    sqlCommand.Parameters.AddWithValue("@ReportingEmployeeID", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@MinExpRequired", recruitR1BudgetedModel.MinExpRequired);
                sqlCommand.Parameters.AddWithValue("@SourceID", recruitR1BudgetedModel.SourceID);
                sqlCommand.Parameters.AddWithValue("@RecruitFromID", recruitR1BudgetedModel.RecruitFromID);
                sqlCommand.Parameters.AddWithValue("@VehicleToolTrade", recruitR1BudgetedModel.VehicleToolTrade);
                sqlCommand.Parameters.AddWithValue("@ContractStatus", recruitR1BudgetedModel.ContractStatus);
                sqlCommand.Parameters.AddWithValue("@FamilySpouse", recruitR1BudgetedModel.FamilySpouse);
                sqlCommand.Parameters.AddWithValue("@AnnualAirTicket", recruitR1BudgetedModel.AnnualAirTicket);
                sqlCommand.Parameters.AddWithValue("@AirfareFrequencyID", recruitR1BudgetedModel.AirfareFrequencyID);
                sqlCommand.Parameters.AddWithValue("@AirfareClassID", recruitR1BudgetedModel.AirfareClassID);
                sqlCommand.Parameters.AddWithValue("@HealthInsurance", recruitR1BudgetedModel.HealthInsurance);
                sqlCommand.Parameters.AddWithValue("@LifeInsurance", recruitR1BudgetedModel.LifeInsurance);
                sqlCommand.Parameters.AddWithValue("@SalikTag", recruitR1BudgetedModel.SalikTag);
                sqlCommand.Parameters.AddWithValue("@SalikAmount", recruitR1BudgetedModel.SalikAmount);
                sqlCommand.Parameters.AddWithValue("@PetrolCard", recruitR1BudgetedModel.PetrolCard);
                sqlCommand.Parameters.AddWithValue("@PetrolCardAmount", recruitR1BudgetedModel.PetrolCardAmount);
                sqlCommand.Parameters.AddWithValue("@ParkingCard", recruitR1BudgetedModel.ParkingCard);
                sqlCommand.Parameters.AddWithValue("@ParkingAreas", recruitR1BudgetedModel.ParkingAreas);
                sqlCommand.Parameters.AddWithValue("@JobDescriptionFileID", recruitR1BudgetedModel.JobDescriptionFileID);
                sqlCommand.Parameters.AddWithValue("@OrgChartFileID", recruitR1BudgetedModel.OrgChartFileID);
                sqlCommand.Parameters.AddWithValue("@ManPowerFileID", recruitR1BudgetedModel.ManPowerFileID);
                if (recruitR1BudgetedModel.DivisionID != null)
                    sqlCommand.Parameters.AddWithValue("@DivisionID", recruitR1BudgetedModel.DivisionID);
                else
                    sqlCommand.Parameters.AddWithValue("@DivisionID", DBNull.Value);

                sqlCommand.Parameters.AddWithValue("@ModifiedBy", recruitR1BudgetedModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@AcademicYearID", recruitR1BudgetedModel.AcademicYearID);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", recruitR1BudgetedModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", recruitR1BudgetedModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                sqlCommand.Parameters.AddWithValue("@SalaryRanges", recruitR1BudgetedModel.SalaryRanges);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return output;
        }
        public RecruitR1UnBudgetedViewModel GetFormDetails(int formProcessID)
        {
            RecruitR1UnBudgetedViewModel recruitR1UnBudgetedViewModel = new RecruitR1UnBudgetedViewModel();
            RecruitR1UnBudgetedModel recruitR1UnBudgetedModel = new RecruitR1UnBudgetedModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHiringR1UnBudgetedFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        recruitR1UnBudgetedModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        recruitR1UnBudgetedModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        recruitR1UnBudgetedModel.AcademicYearID = Convert.ToInt16(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                        recruitR1UnBudgetedModel.EmploymentModeID = Convert.ToInt32(reader["EmploymentModeID"] == DBNull.Value ? "0" : reader["EmploymentModeID"].ToString());
                        recruitR1UnBudgetedModel.UserTypeID = Convert.ToInt32(reader["UserTypeID"] == DBNull.Value ? "0" : reader["UserTypeID"].ToString());
                        recruitR1UnBudgetedModel.EmployeeJobCategoryID = Convert.ToInt32(reader["EmployeeJobCategoryID"] == DBNull.Value ? "0" : reader["EmployeeJobCategoryID"].ToString());
                        recruitR1UnBudgetedModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                        recruitR1UnBudgetedModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                        recruitR1UnBudgetedModel.HMEmployeeID = Convert.ToInt32(reader["HMEmployeeID"] == DBNull.Value ? "0" : reader["HMEmployeeID"].ToString());
                        recruitR1UnBudgetedModel.HMDesignation = Convert.ToString(reader["HMDesignation"]);
                        recruitR1UnBudgetedModel.PositionLocation = Convert.ToString(reader["PositionLocation"]);
                        recruitR1UnBudgetedModel.HeadCount = Convert.ToInt32(reader["HeadCount"] == DBNull.Value ? "0" : reader["HeadCount"].ToString());
                        recruitR1UnBudgetedModel.JobGradeID = (reader["JobGradeID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["JobGradeID"].ToString());
                        recruitR1UnBudgetedModel.ReportingEmployeeID = Convert.ToInt32(reader["ReportingEmployeeID"] == DBNull.Value ? "0" : reader["ReportingEmployeeID"].ToString());
                        recruitR1UnBudgetedModel.MinExpRequired = Convert.ToString(reader["MinExpRequired"]);
                        recruitR1UnBudgetedModel.SourceID = Convert.ToInt32(reader["SourceID"] == DBNull.Value ? "0" : reader["SourceID"].ToString());
                        recruitR1UnBudgetedModel.RecruitFromID = Convert.ToInt16(reader["RecruitFromID"] == DBNull.Value ? "0" : reader["RecruitFromID"].ToString());
                        recruitR1UnBudgetedModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                        recruitR1UnBudgetedModel.SalaryRangesID = Convert.ToInt32(reader["SalaryRangesID"] == DBNull.Value ? "0" : reader["SalaryRangesID"].ToString());
                        recruitR1UnBudgetedModel.ActualBudget = Convert.ToDecimal(reader["ActualBudget"] == DBNull.Value ? "0" : reader["ActualBudget"].ToString());
                        recruitR1UnBudgetedModel.VehicleToolTrade = bool.Parse(reader["VehicleToolTrade"] == DBNull.Value ? "0" : reader["VehicleToolTrade"].ToString());
                        recruitR1UnBudgetedModel.ContractStatus = Convert.ToString(reader["ContractStatus"]);
                        recruitR1UnBudgetedModel.FamilySpouse = Convert.ToString(reader["FamilySpouse"]);
                        recruitR1UnBudgetedModel.AnnualAirTicket = bool.Parse(reader["AnnualAirTicket"] == DBNull.Value ? "0" : reader["AnnualAirTicket"].ToString());
                        recruitR1UnBudgetedModel.AirfareFrequencyID = Int16.Parse(reader["AirfareFrequencyID"] == DBNull.Value ? "0" : reader["AirfareFrequencyID"].ToString());
                        recruitR1UnBudgetedModel.AirfareClassID = int.Parse(reader["AirfareClassID"] == DBNull.Value ? "0" : reader["AirfareClassID"].ToString());
                        recruitR1UnBudgetedModel.HealthInsurance = bool.Parse(reader["HealthInsurance"] == DBNull.Value ? "0" : reader["HealthInsurance"].ToString());
                        recruitR1UnBudgetedModel.LifeInsurance = bool.Parse(reader["LifeInsurance"] == DBNull.Value ? "0" : reader["LifeInsurance"].ToString());
                        recruitR1UnBudgetedModel.SalikTag = bool.Parse(reader["SalikTag"] == DBNull.Value ? "0" : reader["SalikTag"].ToString());
                        recruitR1UnBudgetedModel.SalikAmount = decimal.Parse(reader["SalikAmount"] == DBNull.Value ? "0" : reader["SalikAmount"].ToString());
                        recruitR1UnBudgetedModel.PetrolCard = bool.Parse(reader["PetrolCard"] == DBNull.Value ? "0" : reader["PetrolCard"].ToString());
                        recruitR1UnBudgetedModel.PetrolCardAmount = decimal.Parse(reader["PetrolCardAmount"] == DBNull.Value ? "0" : reader["PetrolCardAmount"].ToString());
                        recruitR1UnBudgetedModel.ParkingCard = bool.Parse(reader["ParkingCard"] == DBNull.Value ? "0" : reader["ParkingCard"].ToString());
                        recruitR1UnBudgetedModel.ParkingAreas = Convert.ToString(reader["ParkingAreas"]);
                        recruitR1UnBudgetedModel.JobDescriptionFileID = int.Parse(reader["JobDescriptionFileID"] == DBNull.Value ? "0" : reader["JobDescriptionFileID"].ToString()); ;
                        recruitR1UnBudgetedModel.OrgChartFileID = int.Parse(reader["OrgChartFileID"] == DBNull.Value ? "0" : reader["OrgChartFileID"].ToString()); ;
                        recruitR1UnBudgetedModel.ManPowerFileID = int.Parse(reader["ManPowerFileID"] == DBNull.Value ? "0" : reader["ManPowerFileID"].ToString()); ;
                        recruitR1UnBudgetedModel.DivisionID = (reader["DivisionID"] == DBNull.Value) ? (int?)null : Convert.ToInt32(reader["DivisionID"].ToString());
                        recruitR1UnBudgetedModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        recruitR1UnBudgetedModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        recruitR1UnBudgetedModel.RequestID = int.Parse(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        recruitR1UnBudgetedViewModel.AcademicYearName = Convert.ToString(reader["AcademicYearName"]);
                        recruitR1UnBudgetedViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                        recruitR1UnBudgetedViewModel.EmploymentModeName = Convert.ToString(reader["EmploymentModeName"]);
                        recruitR1UnBudgetedViewModel.EmployeeJobCategoryName = Convert.ToString(reader["EmployeeJobCategoryName"]);
                        recruitR1UnBudgetedViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                        recruitR1UnBudgetedViewModel.HMEmployeeName = Convert.ToString(reader["HMEmployeeName"]);
                        recruitR1UnBudgetedViewModel.ReportingEmployeeName = Convert.ToString(reader["ReportingEmployeeName"]);
                        recruitR1UnBudgetedViewModel.SourceName = Convert.ToString(reader["SourceName"]);
                        recruitR1UnBudgetedViewModel.RecruitFromName = Convert.ToString(reader["RecruitFromName"]);
                        recruitR1UnBudgetedViewModel.SalaryRangeName = Convert.ToString(reader["SalaryRangeName"]);
                        recruitR1UnBudgetedViewModel.AirfareFrequencyName = Convert.ToString(reader["AirfareFrequencyName"]);
                        recruitR1UnBudgetedViewModel.AirfareClassName = Convert.ToString(reader["AirfareClassName"]);
                        recruitR1UnBudgetedViewModel.JobDescriptionFileName = Convert.ToString(reader["JobDescriptionFileName"]);
                        recruitR1UnBudgetedViewModel.OrgChartFileName = Convert.ToString(reader["OrgChartFileName"]);
                        recruitR1UnBudgetedViewModel.ManPowerFileName = Convert.ToString(reader["ManPowerFileName"]);
                        recruitR1UnBudgetedViewModel.DivisionName = Convert.ToString(reader["DivisionName"]);
                        recruitR1UnBudgetedViewModel.RequestStatusName = Convert.ToString(reader["ReqStatusName"]);
                        recruitR1UnBudgetedViewModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                        recruitR1UnBudgetedViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                        recruitR1UnBudgetedViewModel.UserTypeName = Convert.ToString(reader["UserTypeName"]);
                        recruitR1UnBudgetedModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"] == DBNull.Value ? "0" : reader["CreatedBy"].ToString());
                        recruitR1UnBudgetedModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["CreatedOn"].ToString());
                        recruitR1UnBudgetedModel.SalaryRanges = Convert.ToString(reader["SalaryRanges"]);
                        recruitR1UnBudgetedModel.RecCategoryID = Convert.ToInt16(reader["RecCategoryID"] == DBNull.Value ? "0" : reader["RecCategoryID"].ToString());
                        recruitR1UnBudgetedViewModel.RecCategoryName = Convert.ToString(reader["RecCategoryName"]);
                    }
                }
                sqlConnection.Close();
                recruitR1UnBudgetedModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
                recruitR1UnBudgetedViewModel.RecruitR1UnBudgetedModel = recruitR1UnBudgetedModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return recruitR1UnBudgetedViewModel;
        }

    }
}
