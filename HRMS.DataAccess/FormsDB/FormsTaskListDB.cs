﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class FormsTaskListDB:BaseDB
    {
        public List<FormsTaskListViewModel> GetAllRequestFormsApprovalsByUser(int userID)
        {
            List<FormsTaskListViewModel> requestFormsApproveModelList = new List<FormsTaskListViewModel>();
            FormsTaskListViewModel formsTaskListViewModel;
          //  RequestFormsProcessModel requestFormsProcessModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllRequestFormsApprovalByUser", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@UserID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        formsTaskListViewModel = new FormsTaskListViewModel();

                       // formsTaskListViewModel.FormApprovalID = Convert.ToInt32(reader["FormApprovalID"] == DBNull.Value ? "0" : reader["FormApprovalID"].ToString());
                        formsTaskListViewModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        formsTaskListViewModel.FormID = Convert.ToInt16(reader["FormID"] == DBNull.Value ? "0" : reader["FormID"].ToString());
                        formsTaskListViewModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        formsTaskListViewModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        formsTaskListViewModel.ModifiedOn = reader["ModifiedOn"]!=DBNull.Value?Convert.ToDateTime(reader["ModifiedOn"].ToString()):(DateTime?)null;
                        formsTaskListViewModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
                        formsTaskListViewModel.ModifiedBy = Convert.ToInt32(reader["ModifiedBy"] == DBNull.Value ? "0" : reader["ModifiedBy"].ToString());
                        formsTaskListViewModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        formsTaskListViewModel.GroupName = Convert.ToString(reader["GroupName"]);
                        formsTaskListViewModel.ModifiedEmployeeName = Convert.ToString(reader["ModifiedEmployeeName"]);
                        formsTaskListViewModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                        formsTaskListViewModel.CompletedRejectedStatus = Convert.ToString(reader["CompletedRejectedStatus"]);
                        formsTaskListViewModel.RequesterCompanyName = Convert.ToString(reader["RequesterCompanyName"]);
                        formsTaskListViewModel.ParentFormID = Convert.ToInt16(reader["ParentFormID"] == DBNull.Value ? "0" : reader["ParentFormID"].ToString());

                       ////*** F4 Danny Commented sue to changes
                       // formsTaskListViewModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                       // formsTaskListViewModel.EmployeeName = Convert.ToString(reader["EmployeeName"]);
                       // formsTaskListViewModel.EmployeeCompanyName = Convert.ToString(reader["EmployeeCompanyName"]); 

                        //*** F5 Danny
                        formsTaskListViewModel.PDRP_EmployeeCompanyID = Convert.ToInt32(reader["PDRP_EmployeeCompanyID"] == DBNull.Value ? "0" : reader["PDRP_EmployeeCompanyID"].ToString());
                        formsTaskListViewModel.PDRP_EmployeeCompanyName = Convert.ToString(reader["PDRP_EmployeeCompanyName"] == DBNull.Value ? "0" : reader["PDRP_EmployeeCompanyName"].ToString());
                        formsTaskListViewModel.PDRP_EmployeeID = Convert.ToInt32(reader["PDRP_EmployeeID"] == DBNull.Value ? "0" : reader["PDRP_EmployeeID"].ToString());
                        formsTaskListViewModel.PDRP_EmployeeName = Convert.ToString(reader["PDRP_EmployeeName"] == DBNull.Value ? "0" : reader["PDRP_EmployeeName"].ToString());
                        formsTaskListViewModel.PDRP_AcademicYearID = Convert.ToInt32(reader["PDRP_AcademicYearID"] == DBNull.Value ? "0" : reader["PDRP_AcademicYearID"].ToString());
                        formsTaskListViewModel.PDRP_AcademicYearName = Convert.ToString(reader["PDRP_AcademicYearName"] == DBNull.Value ? "0" : reader["PDRP_AcademicYearName"].ToString());
                        formsTaskListViewModel.PDRP_ProcessName = Convert.ToString(reader["PDRP_ProcessName"] == DBNull.Value ? "0" : reader["PDRP_ProcessName"].ToString());


                        requestFormsApproveModelList.Add(formsTaskListViewModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproveModelList;
        }
    }
}
