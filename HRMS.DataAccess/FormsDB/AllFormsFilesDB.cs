﻿using HRMS.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess.FormsDB
{
    public class AllFormsFilesDB:BaseDB
    {
        public int DeleteAllFormsFilesByID(int fileID)
        {

            int output = 0;

            try
            {
                sqlConnection = new SqlConnection(connectionStringFileDB);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DeleteAllFormsFilesByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FileID", fileID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                //SqlParameter OpMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar,100);               
                //OpMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OpMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return output;
        }
        public AllFormsFilesModel GetAllFormsFiles(int fileID)
        {
            AllFormsFilesModel allFormsFilesModel = new AllFormsFilesModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAllFormsFilesByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FileID", fileID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        allFormsFilesModel = new AllFormsFilesModel();
                        allFormsFilesModel.FileID = Convert.ToInt32(reader["FileID"] == DBNull.Value ? "0" : reader["FileID"].ToString());
                        allFormsFilesModel.FileName = Convert.ToString(reader["FileName"].ToString());
                        allFormsFilesModel.DocumentFile = reader["DocumentFile"] == DBNull.Value ? null : (byte[])reader["DocumentFile"];
                        allFormsFilesModel.FileContentType = Convert.ToString(reader["FileContentType"].ToString());

                        // allFormsFilesModelList.Add(allFormsFilesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return allFormsFilesModel;
        }

        public int DeleteAllFormsFile(int fileID, int formProcessID, string fileIDFieldName, int modifiedBy)
        {

            int output = 0;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DeleteAllFormsFiles", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@FileIDFieldName", fileIDFieldName);
                sqlCommand.Parameters.AddWithValue("@FileID", fileID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                //SqlParameter OpMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar,100);               
                //OpMessage.Direction = ParameterDirection.Output;
                //sqlCommand.Parameters.Add(OpMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                output = Output.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(Output.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return output;
        }
    }
}
