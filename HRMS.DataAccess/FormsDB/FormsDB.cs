﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HRMS.Entities;
using HRMS.Entities.Forms;
using System.Data.SqlClient;
using System.Data;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.PDRP;
using System.Configuration;

namespace HRMS.DataAccess.FormsDB
{
    /// <summary>
    /// Common class for all forms ,contains SQl Connection related  properties from BaseDB and common methods
    /// for all forms
    /// </summary> 
    public class FormsDB : BaseDB
    {
        public static string HRMSDateFormat
        {
            get { return ConfigurationManager.AppSettings["HRMSDateFormat"]; }
        }

        /// <summary>
        /// Get the request id with approver emails
        /// </summary>
        /// <param name="formProcessIDs"></param>
        /// <returns></returns>
        public List<RequestFormsApproverEmailModel> GetApproverEmailList(string formProcessIDs)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetNextApproverDetailsByFormProcessIDs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessIDs", formProcessIDs);
              //  sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", requesterEmployeeID!=null? requesterEmployeeID:(object)DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    RequestFormsApproverEmailModel requestFormsApproverEmailModel;
                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();                     
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestFormsApproverEmailModel.IsRequester = bool.Parse(Convert.ToString(reader["IsRequester"]));
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }

        /// <summary>
        /// Get the request id with approver emails
        /// </summary>
        /// <param name="formProcessIDs"></param>
        /// <returns></returns>
        public List<RequestFormsApproverEmailModel> GetApproverEmailList(string formProcessIDs, int? requesterEmployeeID)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetNextApproverDetailsByFormProcessIDs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessIDs", formProcessIDs);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", requesterEmployeeID!=null? requesterEmployeeID:(object)DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    RequestFormsApproverEmailModel requestFormsApproverEmailModel;
                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();
                        requestFormsApproverEmailModel.EmployeeFullName = reader["EmployeeFullName"].ToString();                        
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestFormsApproverEmailModel.IsRequester = bool.Parse(Convert.ToString(reader["IsRequester"]));
                        requestFormsApproverEmailModel.IsApprover = bool.Parse(Convert.ToString(reader["IsApprover"]));
                        requestFormsApproverEmailModel.IsNextApprover = bool.Parse(Convert.ToString(reader["IsNextApprover"]));
                        requestFormsApproverEmailModel.BUName = Convert.ToString(reader["BUName"]);
                        requestFormsApproverEmailModel.BUShortName_1 = Convert.ToString(reader["BUShortName_1"]);                        
                        requestFormsApproverEmailModel.FormName = Convert.ToString(reader["FormName"]);
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }    

        /// <summary>
        /// Get Next Approver details with Email for a request
        /// </summary>
        /// <param name="formProcessID"></param>
        /// <returns></returns>
        public RequestFormsApproverEmailModel GetApproverEmail(int formProcessID)
        {
            RequestFormsApproverEmailModel requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetNextApproverDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();                  
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.IsRequester = bool.Parse(Convert.ToString(reader["IsRequester"]));
                        requestFormsApproverEmailModel.IsApprover = bool.Parse(Convert.ToString(reader["IsApprover"]));
                        requestFormsApproverEmailModel.IsNextApprover = bool.Parse(Convert.ToString(reader["IsNextApprover"]));
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(Convert.ToString(reader["GroupID"]));
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);

                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModel;
        }
        public List<RequestFormsApproverEmailModel> GetApproverEmailList(int formProcessID)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            RequestFormsApproverEmailModel requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetNextApproverDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();
                        requestFormsApproverEmailModel.EmployeeFullName = reader["EmployeeFullName"].ToString();
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.IsRequester = bool.Parse(Convert.ToString(reader["IsRequester"]));
                        requestFormsApproverEmailModel.IsApprover = bool.Parse(Convert.ToString(reader["IsApprover"]));
                        requestFormsApproverEmailModel.IsNextApprover = bool.Parse(Convert.ToString(reader["IsNextApprover"]));
                        requestFormsApproverEmailModel.GroupID = Convert.ToInt16(Convert.ToString(reader["GroupID"]));
                        requestFormsApproverEmailModel.GroupName = Convert.ToString(reader["GroupName"]);
                        requestFormsApproverEmailModel.BUName = Convert.ToString(reader["BUName"]);
                        requestFormsApproverEmailModel.BUShortName_1 = Convert.ToString(reader["BUShortName_1"]);
                        requestFormsApproverEmailModel.FormName = Convert.ToString(reader["FormName"]);
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }
        /// <summary>
        /// Get All Approval records for a request 
        /// </summary>
        /// <param name="formProcessID"></param>
        /// <returns></returns>
        public List<RequestFormsApproveModel> GetAllRequestFormsApprovals(int formProcessID)
        {
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllRequestFormsApprovals", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    RequestFormsApproveModel requestFormsApproveModel;
                    while (reader.Read())
                    {
                        requestFormsApproveModel = new RequestFormsApproveModel();
                        requestFormsApproveModel.FormApprovalID = Convert.ToInt32(reader["FormApprovalID"] == DBNull.Value ? "0" : reader["FormApprovalID"].ToString());
                        requestFormsApproveModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproveModel.ApproverEmployeeID = Convert.ToInt32(reader["ApproverEmployeeID"] == DBNull.Value ? "0" : reader["ApproverEmployeeID"].ToString());
                        requestFormsApproveModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        requestFormsApproveModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproveModel.Comments = Convert.ToString(reader["Comments"]);
                        requestFormsApproveModel.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"] == DBNull.Value ? DateTime.Now.Date.ToString() : reader["ModifiedOn"].ToString());
                        requestFormsApproveModel.ApproverEmployeeName = Convert.ToString(reader["ApproverEmployeeName"]);
                        requestFormsApproveModel.GroupName = Convert.ToString(reader["GroupName"]);
                        // requestFormsApproveModel.ModifiedOn = DateTime.Parse(CommonDB.GetFormattedDate_DDMMYYYY(reader["ModifiedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["ModifiedOn"].ToString()));
                        requestFormsApproveModelList.Add(requestFormsApproveModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproveModelList;
        }
        /// <summary>
        /// Gets the current/present pending approval for a request 
        /// </summary>
        /// <param name="formProcessID"></param>
        /// <returns></returns>
        public RequestFormsApproveModel GetPendingFormsApproval(int formProcessID)
        {
            RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
            FormsWorkflowGroupModel formsWorkflowGroupModel = new FormsWorkflowGroupModel();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetPendingFormsApproval", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        requestFormsApproveModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproveModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproveModel.ApproverEmployeeID = Convert.ToInt32(reader["ApproverEmployeeID"] == DBNull.Value ? "0" : reader["ApproverEmployeeID"].ToString());
                        requestFormsApproveModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        requestFormsApproveModel.Comments = reader["Comments"].ToString();
                        formsWorkflowGroupModel.IsITGroup = bool.Parse(reader["IsITGroup"] == DBNull.Value ? "0" : reader["IsITGroup"].ToString());
                        formsWorkflowGroupModel.CompOffHRGroup = bool.Parse(reader["CompOffHRGroup"] == DBNull.Value ? "0" : reader["CompOffHRGroup"].ToString());
                        formsWorkflowGroupModel.IsHrGroup = bool.Parse(reader["IsHrGroup"] == DBNull.Value ? "0" : reader["IsHrGroup"].ToString());
                        requestFormsApproveModel.FormsWorkflowGroupModel = formsWorkflowGroupModel;
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproveModel;
        }

        public bool IsValidFormRequest(string controllerName, int formProcessID)
        {
            bool isValid = false;
            try
            {


                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckValidFormRequest", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                sqlCommand.Parameters.AddWithValue("@ControllerName", controllerName);
               
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                isValid = Convert.ToInt32(Output.Value.ToString()) > 0 ? true : false;               
                string message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return isValid;
        }

        /// <summary>
        /// Appoving request 
        /// </summary>
        /// <param name="requestFormsApproveModel"></param>
        /// <returns></returns>
        public OperationDetails ApproveRequestForm(RequestFormsApproveModel requestFormsApproveModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_ApproveRequestForm", sqlConnection);               
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", requestFormsApproveModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ApproverEmployeeID", requestFormsApproveModel.ApproverEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", requestFormsApproveModel.Comments);              
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 1000);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
        /// <summary>
        /// Rejecting request
        /// </summary>
        /// <param name="requestFormsApproveModel"></param>
        /// <returns></returns>
        public OperationDetails RejectRequestForm(RequestFormsApproveModel requestFormsApproveModel)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_RejectRequestForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", requestFormsApproveModel.FormProcessID);
                sqlCommand.Parameters.AddWithValue("@ApproverEmployeeID", requestFormsApproveModel.ApproverEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", requestFormsApproveModel.Comments);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }


        /// <summary>
        /// Will check whether its final approval/rejection and get email details of
        /// employee who belongs to the group with final email notification
        /// </summary>
        /// <param name="formProcessID"></param>
        /// <returns></returns>
        public List<RequestFormsApproverEmailModel> GetFinalNotificationEmail(int formProcessID)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            RequestFormsApproverEmailModel requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetRequestFormsFinalNotificationEmail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {

                        requestFormsApproverEmailModel = new RequestFormsApproverEmailModel();
                        requestFormsApproverEmailModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproverEmailModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproverEmailModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        requestFormsApproverEmailModel.EmployeeName = reader["EmployeeName"].ToString();
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.IsRequester =Convert.ToBoolean(reader["IsRequester"]);
                        requestFormsApproverEmailModel.WorkEmail = reader["WorkEmail"].ToString();
                        requestFormsApproverEmailModel.IsRequester = Convert.ToBoolean(reader["IsRequester"]);
                        requestFormsApproverEmailModel.BUName = Convert.ToString(reader["BUName"]);
                        requestFormsApproverEmailModel.BUShortName_1 = Convert.ToString(reader["BUShortName_1"]);
                        requestFormsApproverEmailModel.FormName = Convert.ToString(reader["FormName"]);
                        requestFormsApproverEmailModelList.Add(requestFormsApproverEmailModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproverEmailModelList;
        }

        public DataTable ConvertFilesListToTable(List<AllFormsFilesModel> allFormsFilesModelList)
        {
            int i = 0;

            DataTable filesTable = new DataTable();
            //resultTable.Columns.Add("WorkflowID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("ID", System.Type.GetType("System.Int16"));
            filesTable.Columns.Add("FileID", System.Type.GetType("System.Int32"));
            filesTable.Columns.Add("FileName");
            filesTable.Columns.Add("DocumentFile", System.Type.GetType("System.Byte[]"));
            filesTable.Columns.Add("FileContentType");
            filesTable.Columns.Add("FormFileIDName");

            foreach (AllFormsFilesModel frm in allFormsFilesModelList)
            {
                i++;
                filesTable.Rows.Add(i,
                                     frm.FileID,
                                     frm.FileName,
                                     frm.DocumentFile,
                                     frm.FileContentType,
                                     frm.FormFileIDName
                                     );
            }

            return filesTable;
        }
     
        public List<RequestFormsApproveModel> GetAllRequestFormsApprovalsByUser(int userID)
        {
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            RequestFormsApproveModel requestFormsApproveModel;
            RequestFormsProcessModel requestFormsProcessModel;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllRequestFormsApprovalByUser", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", userID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        requestFormsApproveModel = new RequestFormsApproveModel();
                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsApproveModel.FormApprovalID = Convert.ToInt32(reader["FormApprovalID"] == DBNull.Value ? "0" : reader["FormApprovalID"].ToString());
                        requestFormsApproveModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsProcessModel.FormID = Convert.ToInt16(reader["FormID"] == DBNull.Value ? "0" : reader["FormID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        requestFormsApproveModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        requestFormsApproveModel.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"].ToString());
                        requestFormsApproveModel.Comments = reader["Comments"].ToString();
                        requestFormsApproveModel.RequestFormsProcessModel = requestFormsProcessModel;
                        requestFormsApproveModelList.Add(requestFormsApproveModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproveModelList;
        }

        public OperationDetails IsWorkFlowExists(Int16 formID, string companyIDs, int userID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckWorkflowExists", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormID", formID);
                sqlCommand.Parameters.AddWithValue("@CompanyIDs", companyIDs);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", userID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }

        public OperationDetails CheckFormLoadValidation(int formID, int companyId, int userID)
        {
            OperationDetails op = new OperationDetails();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckFormLoadValidation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormID", formID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", companyId);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", userID);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlParameter OutputMessage = new SqlParameter("@OperationMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OutputMessage);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                op.InsertedRowId = Convert.ToInt32(Output.Value.ToString());
                op.Message = OutputMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }

        public RequestFormsProcessModel GetRequestFormsProcess(int formProcessID)
        {
            RequestFormsProcessModel objRequestFormsProcessModel = new RequestFormsProcessModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetRequestFormsProcess", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objRequestFormsProcessModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"].ToString());
                        objRequestFormsProcessModel.RequestID = Convert.ToInt32(reader["RequestID"].ToString());
                        objRequestFormsProcessModel.FormID = Convert.ToInt16(reader["FormID"].ToString());
                        objRequestFormsProcessModel.FormInstanceID = Convert.ToInt32(reader["FormInstanceID"].ToString());
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objRequestFormsProcessModel;
        }
        /// <summary>
        /// This method intents to get all the details from hiring tables for an employee,
        /// can modify the procedure and method to get required values
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public RecruitR1BudgetedViewModel GetRecruitFormDetails(int? employeeID)
        {
            RecruitR1BudgetedViewModel objRecruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            RecruitR1BudgetedModel objRecruitR1BudgetedModel = new RecruitR1BudgetedModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetRecruitFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objRecruitR1BudgetedModel.ID = Convert.ToInt32(reader["ID"].ToString());
                        objRecruitR1BudgetedModel.ProjectData = reader["ProjectData"].ToString();
                        objRecruitR1BudgetedViewModel.JobGradeName = reader["JobGradeName_1"].ToString();



                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objRecruitR1BudgetedViewModel;
        }

        public List<PickList> GetEmployeeAirfareFrequencyList()
        {
            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeAirfareFrequencyList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@SystemLanguageId", 1);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["AirfareFrequencyID"].ToString()), text = Convert.ToString(sqlDataReader["AirfareFrequencyName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;

        }
        public List<PickList> GetEmployeeAirfareClassesList()
        {

            List<PickList> objPickList = new List<PickList>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetEmployeeAirfareClassesList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objPickList.Add(new PickList() { id = Convert.ToInt32(sqlDataReader["AirfareClassId"].ToString()), text = Convert.ToString(sqlDataReader["AirfareClassName"]) });
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objPickList;
        }

        public RequesterInfoViewModel GetRequesterInformation(int employeeID, bool? isAddMode, int? formProcessId)
        {
            RequesterInfoViewModel objRequesterInfoViewModel = new RequesterInfoViewModel();
            RecruitR1BudgetedViewModel objRecruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeInformationWithR1Details", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID);
                sqlCommand.Parameters.AddWithValue("@IsAddMode", isAddMode.HasValue? isAddMode:(object)DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@FormProcessId", formProcessId.HasValue? formProcessId:(object)DBNull.Value);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (formProcessId == 0 || !formProcessId.HasValue)
                        {
                            objRequesterInfoViewModel.RequesterID = Convert.ToInt32(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                            objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["EmployeeAlternativeID"]);
                        
                            objRequesterInfoViewModel.RequesterName = Convert.ToString(reader["EmployeeName"]);
                            objRequesterInfoViewModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                            objRequesterInfoViewModel.CompanyName = Convert.ToString(reader["CompanyName"]);
                            objRequesterInfoViewModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? "0" : reader["SuperviserID"].ToString());
                            objRequesterInfoViewModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                            objRequesterInfoViewModel.SuperviserID = Convert.ToInt32(reader["SuperviserID"] == DBNull.Value ? "0" : reader["SuperviserID"].ToString());
                            objRequesterInfoViewModel.SuperviserName = Convert.ToString(reader["SuperviserName"]);
                            objRequesterInfoViewModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            objRequesterInfoViewModel.PositionTitle = Convert.ToString(reader["PositionTitle"]);
                            objRequesterInfoViewModel.DepartmentID = Convert.ToInt32(reader["DepartmentID"] == DBNull.Value ? "0" : reader["DepartmentID"].ToString());
                            objRequesterInfoViewModel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            objRequesterInfoViewModel.NationalityID = Convert.ToInt32(reader["NationalityID"] == DBNull.Value ? "0" : reader["NationalityID"].ToString());
                            objRequesterInfoViewModel.NationalityName = Convert.ToString(reader["NationalityName"]);
                            objRequesterInfoViewModel.WorkEmail = Convert.ToString(reader["WorkEmail"]);
                            objRequesterInfoViewModel.PersonalEmail = Convert.ToString(reader["PersonalEmail"]);
                            objRequesterInfoViewModel.PassportNo = Convert.ToString(reader["PassportNo"]);
                            objRequesterInfoViewModel.PassportIssueDate = reader["PassportIssueDate"] != DBNull.Value ? Convert.ToDateTime(reader["PassportIssueDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.PassportExpireDate = reader["PassportExpireDate"] != DBNull.Value ? Convert.ToDateTime(reader["PassportExpireDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.VisaNo = Convert.ToString(reader["VisaNo"]);
                            objRequesterInfoViewModel.VisaIssueDate = reader["VisaIssueDate"] != DBNull.Value ? Convert.ToDateTime(reader["VisaIssueDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.VisaExpireDate = reader["VisaExpireDate"] != DBNull.Value ? Convert.ToDateTime(reader["VisaExpireDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.ProjectData = Convert.ToString(reader["ProjectData"]);
                            objRequesterInfoViewModel.JobGrade = Convert.ToString(reader["JobGradeName_1"]);
                            objRequesterInfoViewModel.HireDate = reader["HireDate"] != DBNull.Value ? Convert.ToDateTime(reader["HireDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.BirthDate = reader["BirthDate"] != DBNull.Value ? Convert.ToDateTime(reader["BirthDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.AcademicYearID = Convert.ToInt32(reader["AcademicYearID"] == DBNull.Value ? "0" : reader["AcademicYearID"].ToString());
                            objRequesterInfoViewModel.AcademicYearName = Convert.ToString(reader["AcademicYearName"] == DBNull.Value ? "" : reader["AcademicYearName"].ToString());
                        }
                        else
                        {
                            objRequesterInfoViewModel.RequesterAlternativeID = Convert.ToString(reader["RequesterAlternativeID"]);
                            objRequesterInfoViewModel.RequestID = reader["RequestID"].ToString();
                            objRequesterInfoViewModel.FormID = Convert.ToInt16(reader["FormID"]);
                            objRequesterInfoViewModel.FormInstanceID = Convert.ToInt32(reader["FormInstanceID"]);
                            objRequesterInfoViewModel.RequesterName = reader["RequesterName"].ToString();
                            objRequesterInfoViewModel.RequestDate = reader["RequestDate"] != DBNull.Value ? Convert.ToDateTime(reader["RequestDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.CompanyName = reader["Company"].ToString();
                            objRequesterInfoViewModel.DepartmentName = reader["Department"].ToString();
                            objRequesterInfoViewModel.PositionTitle = reader["Designation"].ToString();
                            objRequesterInfoViewModel.ProjectData = reader["Project"].ToString();
                            objRequesterInfoViewModel.WorkEmail = reader["RequesterEmail"].ToString();
                            objRequesterInfoViewModel.JobGrade = reader["JobGrade"].ToString();
                            objRequesterInfoViewModel.SuperviserName = reader["LineManager"].ToString();
                            objRequesterInfoViewModel.NationalityName = reader["Nationality"].ToString();
                            objRequesterInfoViewModel.BirthDate = reader["DateOfBirth"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfBirth"]) : (DateTime?)null;
                            objRequesterInfoViewModel.HireDate = reader["DateOfJoining"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfJoining"]) : (DateTime?)null;
                            objRequesterInfoViewModel.PassportNo = reader["PassportNo"].ToString();
                            objRequesterInfoViewModel.PassportIssueDate = reader["PassportIssueDate"] != DBNull.Value ? Convert.ToDateTime(reader["PassportIssueDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.PassportExpireDate = reader["PassportExpiryDate"] != DBNull.Value ? Convert.ToDateTime(reader["PassportExpiryDate"]) : (DateTime?)null;
                            objRequesterInfoViewModel.VisaNo = reader["VisaNo"].ToString();
                            objRequesterInfoViewModel.VisaExpireDate = reader["VisaExpiryDate"] != DBNull.Value ? Convert.ToDateTime(reader["VisaExpiryDate"]) : (DateTime?)null;                          
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            //objRecruitR1BudgetedViewModel.RecruitR1BudgetedModel = objRecruitR1BudgetedModel;
            return objRequesterInfoViewModel;
        }

        

        public List<RequestFormsApproveModel> GetAllRequestFormsApprovalHistory(int formProcessID)
        {
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetAllRequestFormsApprovalHistory", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();


                if (reader.HasRows)
                {
                    RequestFormsApproveModel requestFormsApproveModel;
                    while (reader.Read())
                    {
                        requestFormsApproveModel = new RequestFormsApproveModel();
                        requestFormsApproveModel.FormApprovalID = Convert.ToInt32(reader["FormApprovalID"] == DBNull.Value ? "0" : reader["FormApprovalID"].ToString());
                        requestFormsApproveModel.FormProcessID = Convert.ToInt32(reader["FormProcessID"] == DBNull.Value ? "0" : reader["FormProcessID"].ToString());
                        requestFormsApproveModel.ApproverEmployeeID = Convert.ToInt32(reader["ApproverEmployeeID"] == DBNull.Value ? "0" : reader["ApproverEmployeeID"].ToString());
                        requestFormsApproveModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        requestFormsApproveModel.GroupID = Convert.ToInt16(reader["GroupID"] == DBNull.Value ? "0" : reader["GroupID"].ToString());
                        requestFormsApproveModel.Comments = Convert.ToString(reader["Comments"]);
                        requestFormsApproveModel.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"] == DBNull.Value ? DateTime.Now.Date.ToString() : reader["ModifiedOn"].ToString());
                        requestFormsApproveModel.ApproverEmployeeName = Convert.ToString(reader["ApproverEmployeeName"]);
                        requestFormsApproveModel.GroupName = Convert.ToString(reader["GroupName"]);
                        // requestFormsApproveModel.ModifiedOn = DateTime.Parse(CommonDB.GetFormattedDate_DDMMYYYY(reader["ModifiedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["ModifiedOn"].ToString()));
                        requestFormsApproveModelList.Add(requestFormsApproveModel);
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsApproveModelList;
        }
       

    }
}