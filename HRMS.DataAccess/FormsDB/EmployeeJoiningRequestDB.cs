﻿using System;
using HRMS.Entities.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HRMS.Entities;
using System.Data;

namespace HRMS.DataAccess.FormsDB
{
    public class EmployeeJoiningRequestDB : FormsDB
    {
        public FormsEmployeeJoiningModel GetDetails(int id)
        {
            FormsEmployeeJoiningModel joiningRequesModel = new FormsEmployeeJoiningModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetPendingJoiningRequestFormByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", id);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        joiningRequesModel.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        joiningRequesModel.EmployeeName = sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"].ToString();
                        joiningRequesModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeAlternativeID"].ToString();
                        joiningRequesModel.JoiningDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HireDate"].ToString());
                        joiningRequesModel.OfferLetterFileID = int.Parse(sqlDataReader["OfferLetterFileID"] == DBNull.Value ? "0" : sqlDataReader["OfferLetterFileID"].ToString()); ;
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (joiningRequesModel.OfferLetterFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(joiningRequesModel.OfferLetterFileID);
                    allFormsFilesModel.FormFileIDName = "joiningRequesModel.OfferLetterFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                joiningRequesModel.AllFormsFilesModelList = allFormsFilesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return joiningRequesModel;
        }
        public List<RequestFormsProcessModel> SaveForm(FormsEmployeeJoiningModel joiningRequestModel)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();

            try
            {
                DataTable filestable = ConvertFilesListToTable(joiningRequestModel.AllFormsFilesModelList);

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_SaveEmployeeJoiningForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", joiningRequestModel.EmployeeId);
                sqlCommand.Parameters.AddWithValue("@CompanyId", joiningRequestModel.CompanyID);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", Convert.ToDateTime(joiningRequestModel.JoiningDate));
                sqlCommand.Parameters.AddWithValue("@OfferLetterFileID", joiningRequestModel.OfferLetterFileID);
                sqlCommand.Parameters.AddWithValue("@ReqStatusID", joiningRequestModel.ReqStatusID);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", joiningRequestModel.CreatedBy);
                sqlCommand.Parameters.AddWithValue("@CreatedOn", joiningRequestModel.CreatedOn);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", joiningRequestModel.ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@ModifiedOn", joiningRequestModel.ModifiedOn);
                sqlCommand.Parameters.AddWithValue("@RequesterEmployeeID", joiningRequestModel.RequesterEmployeeID);
                sqlCommand.Parameters.AddWithValue("@Comments", joiningRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        requestFormsProcessModel = new RequestFormsProcessModel();
                        requestFormsProcessModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        requestFormsProcessModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        requestFormsProcessModelList.Add(requestFormsProcessModel);
                    }
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return requestFormsProcessModelList;
        }
        public OperationDetails UpdateForm(FormsEmployeeJoiningModel joiningRequestModel, int FormProcessID, int ModifiedBy)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            try
            {
                DataTable filestable = ConvertFilesListToTable(joiningRequestModel.AllFormsFilesModelList);

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateEmployeeJoiningForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", joiningRequestModel.ID);
                sqlCommand.Parameters.AddWithValue("@FormProcessID", FormProcessID);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", Convert.ToDateTime(joiningRequestModel.JoiningDate));
                sqlCommand.Parameters.AddWithValue("@OfferLetterFileID", joiningRequestModel.OfferLetterFileID);
                sqlCommand.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                sqlCommand.Parameters.AddWithValue("@Comments", joiningRequestModel.Comments);
                sqlCommand.Parameters.AddWithValue("@AllFiles", filestable);
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Output);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                operationDetails.Success = true;
                operationDetails.Message = "Updated successfully";
                operationDetails.CssClass = "success";
            }
            catch (Exception exception)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "error";
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return operationDetails;
        }
        public FormsEmployeeJoiningModel GetForm(int formProcessID, int FormInstanceID)
        {
            FormsEmployeeJoiningModel joiningRequesModel = new FormsEmployeeJoiningModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFormEmployeeJoining", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormInstanceID", FormInstanceID);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        joiningRequesModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        joiningRequesModel.EmployeeId = Convert.ToInt16(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        joiningRequesModel.EmployeeName = Convert.ToString(reader["EmployeeName"]);
                        joiningRequesModel.JoiningDate = Convert.ToDateTime(reader["JoiningDate"].ToString()).ToString(HRMSDateFormat);
                        joiningRequesModel.OfferLetterFileID = int.Parse(reader["OfferLetterFileID"] == DBNull.Value ? "0" : reader["OfferLetterFileID"].ToString());
                        joiningRequesModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        joiningRequesModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        joiningRequesModel.CompanyID = Convert.ToInt32(reader["CompanyID"] == DBNull.Value ? "0" : reader["CompanyID"].ToString());
                        joiningRequesModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"] == DBNull.Value ? "0" : reader["CreatedBy"].ToString());
                        joiningRequesModel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"] == DBNull.Value ? DateTime.Now.ToString() : reader["CreatedOn"].ToString());
                        joiningRequesModel.EmployeeAlternativeID = reader["EmployeeAlternativeID"] == DBNull.Value ? "" : reader["EmployeeAlternativeID"].ToString();
                    }
                }
                sqlConnection.Close();
                AllFormsFilesModel allFormsFilesModel;
                AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                if (joiningRequesModel.OfferLetterFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(joiningRequesModel.OfferLetterFileID);
                    allFormsFilesModel.FormFileIDName = "OfferLetterFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                joiningRequesModel.AllFormsFilesModelList = allFormsFilesModelList;
                joiningRequesModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return joiningRequesModel;
        }
        public FormsEmployeeJoiningModel GetFormDetails(int formProcessID)
        {
            FormsEmployeeJoiningModel joiningRequesModel = new FormsEmployeeJoiningModel();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeJoiningFormDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FormProcessID", formProcessID);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        joiningRequesModel.ID = Convert.ToInt32(reader["ID"] == DBNull.Value ? "0" : reader["ID"].ToString());
                        joiningRequesModel.ReqStatusID = Convert.ToInt16(reader["ReqStatusID"] == DBNull.Value ? "0" : reader["ReqStatusID"].ToString());
                        joiningRequesModel.EmployeeId = Convert.ToInt16(reader["EmployeeID"] == DBNull.Value ? "0" : reader["EmployeeID"].ToString());
                        joiningRequesModel.JoiningDate = Convert.ToDateTime(reader["JoiningDate"].ToString()).ToString(HRMSDateFormat);
                        joiningRequesModel.EmployeeName = Convert.ToString(reader["EmployeeName"]);
                        joiningRequesModel.OfferLetterFileID = int.Parse(reader["OfferLetterFileID"] == DBNull.Value ? "0" : reader["OfferLetterFileID"].ToString());
                        joiningRequesModel.OfferletterFileName = reader["OfferletterFileName"] == DBNull.Value ? "" : reader["OfferletterFileName"].ToString();
                        joiningRequesModel.RequesterEmployeeID = Convert.ToInt32(reader["RequesterEmployeeID"] == DBNull.Value ? "0" : reader["RequesterEmployeeID"].ToString());
                        joiningRequesModel.RequestID = Convert.ToInt32(reader["RequestID"] == DBNull.Value ? "0" : reader["RequestID"].ToString());
                        joiningRequesModel.RequestStatusName = Convert.ToString(reader["ReqStatusName"]);
                        joiningRequesModel.RequesterEmployeeName = Convert.ToString(reader["RequesterEmployeeName"]);
                    }
                }
                sqlConnection.Close();
                joiningRequesModel.AllFormsFilesModelList = allFormsFilesModelList;
                joiningRequesModel.RequestFormsApproveModelList = GetAllRequestFormsApprovals(formProcessID);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return joiningRequesModel;
        }
        public int UpdateFlag(int employeeId, DateTime joiningDate)
        {           
            try
            {              
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateIsPayActiveEmployeeeJoiningForm", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlCommand.Parameters.AddWithValue("@JoiningDate", joiningDate);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return employeeId;
        }

        #region Leave Rejoining Status Report
        public List<LeaveRejoiningStatusReport> GetLeaveRejoiningStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int userId)
        {
            List<LeaveRejoiningStatusReport> listStatusReportModel = null;
            try
            {
                listStatusReportModel = new List<LeaveRejoiningStatusReport>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetLeaveRejoiningStatusReport", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RequestID", RequestID);
                sqlCommand.Parameters.AddWithValue("@CompanyID", CompanyID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
                sqlCommand.Parameters.AddWithValue("@CurrentStatusID", CurrentStatusID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@ToDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    LeaveRejoiningStatusReport StatusReportModel;
                    while (sqlDataReader.Read())
                    {
                        StatusReportModel = new LeaveRejoiningStatusReport();
                        StatusReportModel.BU_Name = Convert.ToString(sqlDataReader["BU_Name"] == DBNull.Value ? "" : sqlDataReader["BU_Name"]);
                        StatusReportModel.RequestID = Convert.ToInt32(sqlDataReader["RequestID"] == DBNull.Value ? "0" : sqlDataReader["RequestID"].ToString());
                        StatusReportModel.RequestDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["RequestDate"] == DBNull.Value ? "" : sqlDataReader["RequestDate"]));
                        StatusReportModel.EmployeeID = Convert.ToString(sqlDataReader["EmployeeID"] == DBNull.Value ? "" : sqlDataReader["EmployeeID"]);
                        StatusReportModel.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"] == DBNull.Value ? "" : sqlDataReader["EmployeeName"]);
                        StatusReportModel.Department = Convert.ToString(sqlDataReader["Department"] == DBNull.Value ? "" : sqlDataReader["Department"]);
                        StatusReportModel.Designation = Convert.ToString(sqlDataReader["Designation"] == DBNull.Value ? "" : sqlDataReader["Designation"]);
                        StatusReportModel.Category = Convert.ToString(sqlDataReader["Category"] == DBNull.Value ? "" : sqlDataReader["Category"]);
                        StatusReportModel.LeaveType = Convert.ToString(sqlDataReader["LeaveType"] == DBNull.Value ? "" : sqlDataReader["LeaveType"]);
                        StatusReportModel.LeaveDateFrom = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["LeaveDateFrom"] == DBNull.Value ? "" : sqlDataReader["LeaveDateFrom"]));
                        StatusReportModel.LeaveDateTo = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["LeaveDateTo"] == DBNull.Value ? "" : sqlDataReader["LeaveDateTo"]));
                        StatusReportModel.RequestedDays = Convert.ToDecimal(sqlDataReader["RequestedDays"] == DBNull.Value ? "0" : sqlDataReader["RequestedDays"]);
                        StatusReportModel.ReJoiningDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(Convert.ToString(sqlDataReader["ReJoiningDate"] == DBNull.Value ? "" : sqlDataReader["ReJoiningDate"]));
                        StatusReportModel.LeavesToAdjust = Convert.ToInt32(sqlDataReader["LeavesToAdjust"] == DBNull.Value ? "0" : sqlDataReader["LeavesToAdjust"].ToString());
                        StatusReportModel.AfterAvailableLeaveDays = Convert.ToDecimal(sqlDataReader["AfterAvailableLeaveDays"] == DBNull.Value ? "0" : sqlDataReader["AfterAvailableLeaveDays"]);
                        StatusReportModel.CurrentStatus = Convert.ToString(sqlDataReader["CurrentStatus"] == DBNull.Value ? "" : sqlDataReader["CurrentStatus"]);
                        StatusReportModel.CurrentApprover = Convert.ToString(sqlDataReader["CurrentApprover"] == DBNull.Value ? "" : sqlDataReader["CurrentApprover"]);
                        StatusReportModel.FormProcessID = Convert.ToInt32(sqlDataReader["FormProcessID"] == DBNull.Value ? "0" : sqlDataReader["FormProcessID"].ToString());
                        listStatusReportModel.Add(StatusReportModel);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return listStatusReportModel;
        }
        #endregion
    }
}
