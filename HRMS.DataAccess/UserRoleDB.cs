﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using HRMS.Entities.General;

namespace HRMS.DataAccess
{
    public class UserRoleDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails InsertUserRole(UserRoleModel UserRoleModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",1) ,
                        new SqlParameter("@UserRoleName",UserRoleModel.UserRoleName),
                        new SqlParameter("@Permissions", UserRoleModel.Permissions),
                        new SqlParameter("@ViewPagesSubCategoryIDs", UserRoleModel.ViewPagesSubCategoryIDs),
                        new SqlParameter("@AddPagesSubCategoryIDs", UserRoleModel.AddPagesSubCategoryIDs),
                        new SqlParameter("@UpdatePagesSubCategoryIDs", UserRoleModel.UpdatePagesSubCategoryIDs),
                        new SqlParameter("@DeletePagesSubCategoryIDs", UserRoleModel.DeletePagesSubCategoryIDs),
                        new SqlParameter("@OtherPermissionIds", UserRoleModel.OtherPermissionIds)
                    };
                int UserRoleId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspUserRoleCUD  ", parameters));
                return new OperationDetails(true, "UserRole saved successfully.", null, UserRoleId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting UserRole.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateUserRole(UserRoleModel UserRoleModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",2) ,
                        new SqlParameter("@UserRoleId",UserRoleModel.UserRoleId) ,
                        new SqlParameter("@UserRoleName",UserRoleModel.UserRoleName) ,
                        new SqlParameter("@Permissions", UserRoleModel.Permissions) ,
                        new SqlParameter("@ViewPagesSubCategoryIDs", UserRoleModel.ViewPagesSubCategoryIDs),
                        new SqlParameter("@AddPagesSubCategoryIDs", UserRoleModel.AddPagesSubCategoryIDs),
                        new SqlParameter("@UpdatePagesSubCategoryIDs", UserRoleModel.UpdatePagesSubCategoryIDs),
                        new SqlParameter("@DeletePagesSubCategoryIDs", UserRoleModel.DeletePagesSubCategoryIDs),
                        new SqlParameter("@OtherPermissionIds", UserRoleModel.OtherPermissionIds)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspUserRoleCUD", parameters);
                return new OperationDetails(true, "User role updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating UserRole.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteUserRoleById(int UserRoleId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@UserRoleId",UserRoleId) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspUserRoleCUD", parameters);
                return new OperationDetails(true, "UserRole deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "This user role already assigned to user group, you cannot delete..", null);
                //throw;
            }
            finally
            {

            }
        }

        public List<UserRoleModel> GetUserRoleList()
        {
            // Create a list to hold the UserRole		
            List<UserRoleModel> UserRoleModelList = new List<UserRoleModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetUserRole"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        UserRoleModel UserRoleModel;
                        while (reader.Read())
                        {
                            UserRoleModel = new UserRoleModel();
                            UserRoleModel.UserRoleId = Convert.ToInt32(reader["UserRoleId"].ToString());
                            UserRoleModel.UserRoleName = reader["UserRoleName"].ToString();
                            UserRoleModel.Permissions = reader["Permissions"].ToString();
                            UserRoleModel.EmployeeJobCategoryID = string.IsNullOrEmpty(reader["EmployeeJobCategoryID"].ToString()) ? (Int16?)null : Convert.ToInt16(reader["EmployeeJobCategoryID"].ToString());
                            UserRoleModelList.Add(UserRoleModel);
                        }
                    }
                }
                return UserRoleModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching UserRoleModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get All Users roles
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<UserRoleModel> GetAllRoles_old()
        {
            List<UserRoleModel> userRoleModelList = null;
            try
            {
                userRoleModelList = new List<UserRoleModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetUserRoles", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    UserRoleModel userRoleModel;
                    while (sqlDataReader.Read())
                    {
                        userRoleModel = new UserRoleModel();
                        userRoleModel.UserRoleId = Convert.ToInt32(sqlDataReader["UserRoleID"].ToString());
                        userRoleModel.UserRoleName = sqlDataReader["UserRoleName"].ToString().Trim();
                        userRoleModel.Permissions = sqlDataReader["Permissions"].ToString().Trim();
                        userRoleModelList.Add(userRoleModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return userRoleModelList;
        }

        public UserRoleModel UserRoleById(int UserRoleId)
        {
            try
            {
                UserRoleModel UserRoleModel = new UserRoleModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetUserRoleByID", new SqlParameter("@aIntUserRoleID", UserRoleId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            UserRoleModel = new UserRoleModel();
                            UserRoleModel.UserRoleId = Convert.ToInt32(reader["UserRoleId"].ToString());
                            UserRoleModel.UserRoleName = reader["UserRoleName"].ToString();
                            UserRoleModel.Permissions = reader["Permissions"].ToString();
                            UserRoleModel.ViewPagesSubCategoryIDs = reader["ViewPagesSubCategoryIDs"].ToString();
                            UserRoleModel.AddPagesSubCategoryIDs = reader["AddPagesSubCategoryIDs"].ToString();
                            UserRoleModel.UpdatePagesSubCategoryIDs = reader["UpdatePagesSubCategoryIDs"].ToString();
                            UserRoleModel.DeletePagesSubCategoryIDs = reader["DeletePagesSubCategoryIDs"].ToString();
                            UserRoleModel.OtherPermissionIds = reader["OtherPermissionIds"].ToString();
                        }
                    }
                }
                return UserRoleModel;
                // Finally, we return UserRoleModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching UserRoleModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<OtherPermissions> GetOtherPermission()
        {
            try
            {
                List<OtherPermissions> otherPermissionsList = new List<OtherPermissions>();
                OtherPermissions otherPermissions = null;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_GetOtherPermission"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            otherPermissions = new OtherPermissions();
                            otherPermissions.MainNavigationSubCategoryId = Convert.ToInt32(reader["MainNavigationSubCategoryId"].ToString());
                            otherPermissions.OtherPermissionId = Convert.ToInt32(reader["OtherPermissionId"].ToString());
                            otherPermissions.OtherPermissionName = reader["OtherPermissionName"].ToString();
                            otherPermissionsList.Add(otherPermissions);
                        }
                    }
                }
                return otherPermissionsList;
                // Finally, we return UserRoleModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching UserRoleModel.", exception);
                throw;
            }
            finally
            {

            }
        }


        //************New User Role Functionality************//

        public List<UserRoleModel> GetAllRoles()
        {
            List<UserRoleModel> userRoleModelList = null;
            try
            {
                userRoleModelList = new List<UserRoleModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetUserRoles", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    UserRoleModel userRoleModel;
                    while (sqlDataReader.Read())
                    {
                        userRoleModel = new UserRoleModel();
                        userRoleModel.UserRoleId = Convert.ToInt32(sqlDataReader["UserRoleID"].ToString());
                        userRoleModel.UserRoleName = sqlDataReader["UserRoleName"].ToString().Trim();
                        userRoleModel.Permissions = sqlDataReader["Permissions"].ToString().Trim();
                        userRoleModel.EmployeeJobCategoryID = string.IsNullOrEmpty(sqlDataReader["EmployeeJobCategoryID"].ToString())?(Int16?)null: Convert.ToInt16(sqlDataReader["EmployeeJobCategoryID"].ToString());
                        userRoleModel.EmployeeJobCategoryName =sqlDataReader["EmployeeJobCategoryName"]==DBNull.Value ?"-" : sqlDataReader["EmployeeJobCategoryName"].ToString().Trim();
                        userRoleModelList.Add(userRoleModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return userRoleModelList;
        }

        public List<UserRolePermissions> GetPermissionDetailsByNavigation(int moduleID, int UserRoleId, bool showOnlyAssigned)
        {
            List<UserRolePermissions> lstUserRolePermissions = new List<UserRolePermissions>();
            UserRolePermissions objUserRolePermissions =null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetUserRolePermissions", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                // Passing values
                sqlCommand.Parameters.AddWithValue("@ModuleID", moduleID);
                sqlCommand.Parameters.AddWithValue("@UserRoleID", UserRoleId);
                sqlCommand.Parameters.AddWithValue("@showOnlyAssigned", showOnlyAssigned);
                DataSet objDS = new DataSet();
                SqlDataAdapter objDA = new SqlDataAdapter(sqlCommand);
                objDA.Fill(objDS);
                sqlConnection.Close();
                if (objDS.Tables[0].Rows.Count > 0)
                {
                   
                    for (int i = 0; i < objDS.Tables[0].Rows.Count; i++)
                    {
                        objUserRolePermissions = new UserRolePermissions();
                        objUserRolePermissions.ModuleID = int.Parse(objDS.Tables[0].Rows[i]["ModuleID"].ToString());
                        objUserRolePermissions.ModuleName = objDS.Tables[0].Rows[i]["ModuleName"].ToString();
                        objUserRolePermissions.ActNavigationID = Convert.ToInt32(objDS.Tables[0].Rows[i]["ActNavigationID"].ToString());
                      
                        if (objDS.Tables[1].Rows.Count > 0)
                        {                            
                            for (int j = 0; j < objDS.Tables[1].Rows.Count; j++)
                            {                               
                                if (objDS.Tables[1].Rows[j]["ModuleID"].ToString().Trim() == objDS.Tables[0].Rows[i]["ModuleID"].ToString().Trim())
                                {                                   
                                    objUserRolePermissions.IsFullPermssion = Convert.ToBoolean(objDS.Tables[1].Rows[j]["IsFullPermission"].ToString() == "0" ? "false" : "true");
                                    if (objDS.Tables[1].Rows[j]["IsViewPagePermission"].ToString() != "")
                                    {
                                        objUserRolePermissions.IsViewPermssion = Convert.ToBoolean(objDS.Tables[1].Rows[j]["IsViewPagePermission"].ToString() == "0" ? "false" : "true");
                                    }
                                    if (objDS.Tables[1].Rows[j]["IsAddPagePermission"].ToString() != "")
                                    {
                                        objUserRolePermissions.IsAddPermssion = Convert.ToBoolean(objDS.Tables[1].Rows[j]["IsAddPagePermission"].ToString() == "0" ? "false" : "true");
                                    }
                                    if (objDS.Tables[1].Rows[j]["IsUpdatePagePermission"].ToString() != "")
                                    {
                                        objUserRolePermissions.IsUpdatePermssion = Convert.ToBoolean(objDS.Tables[1].Rows[j]["IsUpdatePagePermission"].ToString() == "0" ? "false" : "true");
                                    }
                                    if (objDS.Tables[1].Rows[j]["IsDeletePagePermission"].ToString() != "")
                                    {
                                        objUserRolePermissions.IsDeletePermssion = Convert.ToBoolean(objDS.Tables[1].Rows[j]["IsDeletePagePermission"].ToString() == "0" ? "false" : "true");
                                    }                                 
                                }                             
                            }
                        }
                        if (objDS.Tables[2].Rows.Count > 0)
                        {
                            objUserRolePermissions.lstOtherPermissions = new List<OtherNavPermissions>();
                            for (int k = 0; k < objDS.Tables[2].Rows.Count; k++)
                            {                                
                                if (objDS.Tables[2].Rows[k]["ModuleID"].ToString().Trim() == objDS.Tables[0].Rows[i]["ModuleID"].ToString().Trim())
                                {
                                    OtherNavPermissions objOtherNavPermissions = new OtherNavPermissions();
                                    objOtherNavPermissions.AssociatedNavigationId = Convert.ToInt32(objDS.Tables[2].Rows[k]["ActNavigationID"].ToString());
                                    if (objDS.Tables[2].Rows[k]["OtherPermissionID"].ToString() != "")
                                    {
                                        objOtherNavPermissions.OtherPermissionId = Convert.ToInt32(objDS.Tables[2].Rows[k]["OtherPermissionID"].ToString());
                                        objOtherNavPermissions.OtherPermissionName = Convert.ToString(objDS.Tables[2].Rows[k]["OtherPermissionName"].ToString());
                                        objOtherNavPermissions.IsPermitted = Convert.ToBoolean(objDS.Tables[2].Rows[k]["IsPermitted"].ToString() == "0" ? "false" : "true");
                                        objUserRolePermissions.lstOtherPermissions.Add(objOtherNavPermissions);
                                    }
                                }                               
                            }
                        }
                        lstUserRolePermissions.Add(objUserRolePermissions);
                    }
                   
                }                             

            }
            catch (Exception ex)
            {
                throw ex;

            }
            return lstUserRolePermissions;
        }

        public OperationDetails UpdateUserRolePermission(DataTable dt, int UserRoleID, int NavigationID, bool IsFullPermiited,
            bool? IsViewPermitted, bool? IsAddPermitted, bool? IsUpdatePermitted, bool? IsDeletePermitted, int? ParentNavID)
        {
            OperationDetails obj = new OperationDetails();
            try
            {
                string AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_UpdateUserRoleDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@OtherPermissionDetails", dt);
                sqlCommand.Parameters.AddWithValue("@UserRoleId", UserRoleID);
                sqlCommand.Parameters.AddWithValue("@NavigationId", NavigationID);
                sqlCommand.Parameters.AddWithValue("@IsFullPermiited", IsFullPermiited);
                sqlCommand.Parameters.AddWithValue("@IsViewPermitted", IsViewPermitted);
                sqlCommand.Parameters.AddWithValue("@IsAddPermitted", IsAddPermitted);
                sqlCommand.Parameters.AddWithValue("@IsUpdatePermitted", IsUpdatePermitted);
                sqlCommand.Parameters.AddWithValue("@IsDeletePermitted", IsDeletePermitted);
                sqlCommand.Parameters.AddWithValue("@ParentNavID", ParentNavID);
                SqlParameter IsSuccess = new SqlParameter("@IsSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                int result = sqlCommand.ExecuteNonQuery();

                if (Convert.ToBoolean(IsSuccess.Value.ToString()))
                {
                    obj.Message = "User role updated successfully.";
                    obj.Success = true;
                    obj.CssClass = "success";
                }
                else
                {
                    obj.Message = "Error while updating user role details.";
                    obj.Success = false;
                    obj.CssClass = "error";
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                obj.Message = "Technical Error while updating user role details.";
                obj.Success = false;
                obj.CssClass = "error";
                obj.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return obj;
        }

        public OperationDetails AddUserRole(UserRoleModel objUserRoleModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            try
            {
                string AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_AddUserRole", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@UserRoleName", objUserRoleModel.UserRoleName);
                sqlCommand.Parameters.AddWithValue("@EmployeeJobCategoryID", objUserRoleModel.EmployeeJobCategoryID);
                SqlParameter IsSuccess = new SqlParameter("@IsSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                int result = sqlCommand.ExecuteNonQuery();

                if (Convert.ToBoolean(IsSuccess.Value.ToString()))
                {
                    objOperationDetails.Message = "User role added successfully.";
                    objOperationDetails.Success = true;
                    objOperationDetails.CssClass = "success";
                }
                else
                {
                    objOperationDetails.Message = "Error while adding user role.";
                    objOperationDetails.Success = false;
                    objOperationDetails.CssClass = "error";
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                objOperationDetails.Message = "Technical Error while adding user role.";
                objOperationDetails.Success = false;
                objOperationDetails.CssClass = "error";
                objOperationDetails.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return objOperationDetails;
        }

        //////////////////////////////////////


    }
}
