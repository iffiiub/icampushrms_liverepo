﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.DataAccess
{
    public class MOLTitleDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Get all MOL Title
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<MOLTitleModel> GetAllMOLTitle(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, out int totalCount)
        {
            List<MOLTitleModel> MOLTitleList = null;
            try
            {
                MOLTitleList = new List<MOLTitleModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetMOLTitle", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
                sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
                sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
                sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);

                SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
                TotalCount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(TotalCount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    MOLTitleModel MOLTitle;
                    while (sqlDataReader.Read())
                    {
                        MOLTitle = new MOLTitleModel();

                        MOLTitle.MOLTitleID = Convert.ToInt32(sqlDataReader["MOLTitleID"].ToString());
                        MOLTitle.MOLTitleName_1 = sqlDataReader["MOLTitleName_1"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_1"].ToString();
                        MOLTitle.MOLTitleName_2 = sqlDataReader["MOLTitleName_2"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_2"].ToString();
                        MOLTitle.MOLTitleName_3 = sqlDataReader["MOLTitleName_3"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_3"].ToString();


                        MOLTitleList.Add(MOLTitle);
                    }
                }
                sqlConnection.Close();

                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return MOLTitleList;
        }

        public List<MOLTitleModel> GetAllMOLTitleList()
        {
            List<MOLTitleModel> MOLTitleList = null;
            try
            {
                MOLTitleList = new List<MOLTitleModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetMOLTitleList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    MOLTitleModel MOLTitle;
                    while (sqlDataReader.Read())
                    {
                        MOLTitle = new MOLTitleModel();

                        MOLTitle.MOLTitleID = Convert.ToInt32(sqlDataReader["MOLTitleID"].ToString());
                        MOLTitle.MOLTitleName_1 = sqlDataReader["MOLTitleName_1"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_1"].ToString();
                        MOLTitle.MOLTitleName_2 = sqlDataReader["MOLTitleName_2"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_2"].ToString();
                        MOLTitle.MOLTitleName_3 = sqlDataReader["MOLTitleName_3"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_3"].ToString();


                        MOLTitleList.Add(MOLTitle);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return MOLTitleList;
        }


        public MOLTitleModel GetMOLTitle(int id)
        {
            MOLTitleModel MOLTitle = new MOLTitleModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_GetMOLTitleByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MOLTitleID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    if (sqlDataReader.Read())
                    {
                        MOLTitle = new MOLTitleModel();

                        MOLTitle.MOLTitleID = Convert.ToInt32(sqlDataReader["MOLTitleID"].ToString());
                        MOLTitle.MOLTitleName_1 = sqlDataReader["MOLTitleName_1"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_1"].ToString();
                        MOLTitle.MOLTitleName_2 = sqlDataReader["MOLTitleName_2"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_2"].ToString();
                        MOLTitle.MOLTitleName_3 = sqlDataReader["MOLTitleName_3"] == DBNull.Value ? "" : sqlDataReader["MOLTitleName_3"].ToString();

                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return MOLTitle;
        }

        /// <summary>
        /// Add/Update/Delete MOL Title
        /// </summary>
        /// <param name="MOLTitle"></param>
        /// <param name="operationType"></param>
        /// <returns></returns>
        public OperationDetails AddUpdateDeleteMOLTitle(MOLTitleModel MOLTitle, int operationType)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_stp_MOLTitleCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@MolTitleEn", MOLTitle.MOLTitleName_1);
                sqlCommand.Parameters.AddWithValue("@MolTitleAr", MOLTitle.MOLTitleName_2);
                sqlCommand.Parameters.AddWithValue("@MolTitleFr", MOLTitle.MOLTitleName_3);
                sqlCommand.Parameters.AddWithValue("@OperationType", operationType);
                SqlParameter isSuccess = new SqlParameter("@Suceess", SqlDbType.Bit);
                isSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(isSuccess);

                if (operationType == 1)
                {
                    sqlCommand.Parameters.AddWithValue("@MOLTitleID", 0);
                }
                else if (operationType == 2)
                {
                    sqlCommand.Parameters.AddWithValue("@MOLTitleID", MOLTitle.MOLTitleID);
                }
                else if (operationType == 3)
                {
                    sqlCommand.Parameters.AddWithValue("MOLTitleID", MOLTitle.MOLTitleID);
                }

                SqlParameter OperationMessage = new SqlParameter("@output", SqlDbType.NVarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Message = OperationMessage.Value.ToString();
                op.Success = Convert.ToBoolean(isSuccess.Value.ToString());

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }
    }
}
