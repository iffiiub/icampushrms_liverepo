﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Configuration;
using System.Globalization;
namespace HRMS.DataAccess
{
    public class InternalEmploymentDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        /// <summary>
        /// Add past employment details
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails AddInternalEmployment(InternalEmploymentModel internalEmploymentModel)
        {
            string Message = "";
            int InternalEmploymentID = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertInternalEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", internalEmploymentModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@LocationID", internalEmploymentModel.LocationID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", internalEmploymentModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@PositionID", internalEmploymentModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(internalEmploymentModel.StartDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(internalEmploymentModel.EndDate));
                sqlCommand.Parameters.AddWithValue("@Comments", internalEmploymentModel.Comments == null ? "" : internalEmploymentModel.Comments);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", internalEmploymentModel.CreatedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();
                InternalEmploymentID = Convert.ToInt32(OperationId.Value);


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, InternalEmploymentID);
        }

        /// <summary>
        /// Update Past Employment
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails UpdateInternalEmployment(InternalEmploymentModel internalEmploymentModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateInternalEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@InternalEmploymentID", internalEmploymentModel.InternalEmploymentID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", internalEmploymentModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@LocationID", internalEmploymentModel.LocationID);
                sqlCommand.Parameters.AddWithValue("@DepartmentID", internalEmploymentModel.DepartmentID);
                sqlCommand.Parameters.AddWithValue("@PositionID", internalEmploymentModel.PositionID);
                sqlCommand.Parameters.AddWithValue("@StartDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(internalEmploymentModel.StartDate));
                sqlCommand.Parameters.AddWithValue("@EndDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(internalEmploymentModel.EndDate));
                sqlCommand.Parameters.AddWithValue("@Comments", internalEmploymentModel.Comments);
                sqlCommand.Parameters.AddWithValue("ModifiedBy", internalEmploymentModel.ModifiedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, internalEmploymentModel.InternalEmploymentID);
        }

        /// <summary>
        /// Delete Delete PastEmployment
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails DeleteInternalEmployment(int InternalEmploymentID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Delete_HR_InternalEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@InternalEmploymentID", InternalEmploymentID);
                //sqlCommand.Parameters.AddWithValue("@EmployeeID", pastEmploymentModel.EmployeeID);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, InternalEmploymentID);
        }


        /// <summary>
        /// Get Past Employment Details
        /// </summary>
        /// <param name="PastEmploymentID"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public InternalEmploymentModel GetInternalEmploymentByID(int PastEmploymentID)
        {
            InternalEmploymentModel internalEmploymentModel = new InternalEmploymentModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_Get_InternalEmploymentbyID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@InternalEmploymentID", PastEmploymentID);           
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        //DateTime.ParseExact(Convert.ToString(sqlDataReader["StartDate"]), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        internalEmploymentModel.InternalEmploymentID = Convert.ToInt32(sqlDataReader["InternalEmploymentID"].ToString());
                        internalEmploymentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        internalEmploymentModel.LocationID = Convert.ToInt32(sqlDataReader["LocationID"]);
                        internalEmploymentModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"]);
                        internalEmploymentModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"]);
                        internalEmploymentModel.StartDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["StartDate"].ToString());
                        internalEmploymentModel.EndDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EndDate"].ToString());
                        internalEmploymentModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        internalEmploymentModel.PositionName = Convert.ToString(sqlDataReader["PositionTitle"]);
                        internalEmploymentModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        //internalEmploymentModel.LocationName = Convert.ToString(sqlDataReader["LocationName"]);
                        internalEmploymentModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"]);
                        internalEmploymentModel.SurName_1 = Convert.ToString(sqlDataReader["SurName_1"]);
                    }
                }
                sqlConnection.Close();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return internalEmploymentModel;
        }


        /// <summary>
        /// Get Past Employment Details by Employee ID with sorting ang paging crieteria
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="totalCount"></param>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public List<InternalEmploymentModel> GetInternalEmploymentList(int EmployeeID)
        {
            List<InternalEmploymentModel> InternalEmploymentList = null;
            try
            {
                InternalEmploymentList = new List<InternalEmploymentModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetInternalEmployment", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    InternalEmploymentModel internalEmploymentModel;
                    while (sqlDataReader.Read())
                    {
                        internalEmploymentModel = new InternalEmploymentModel();
                        internalEmploymentModel.InternalEmploymentID = Convert.ToInt32(sqlDataReader["InternalEmploymentID"].ToString());
                        internalEmploymentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        internalEmploymentModel.LocationID = Convert.ToInt32(sqlDataReader["LocationID"]);
                        internalEmploymentModel.DepartmentID = Convert.ToInt32(sqlDataReader["DepartmentID"]);
                        internalEmploymentModel.PositionID = Convert.ToInt32(sqlDataReader["PositionID"]);
                        internalEmploymentModel.StartDate = Convert.ToString(sqlDataReader["StartDate"]);
                        internalEmploymentModel.EndDate = Convert.ToString(sqlDataReader["EndDate"]);
                        internalEmploymentModel.DepartmentName = Convert.ToString(sqlDataReader["DepartmentName_1"]);
                        internalEmploymentModel.PositionName = Convert.ToString(sqlDataReader["PositionTitle"]);
                        internalEmploymentModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        //internalEmploymentModel.LocationName = Convert.ToString(sqlDataReader["LocationName"]);
                        internalEmploymentModel.FirstName_1 = Convert.ToString(sqlDataReader["FirstName_1"]);
                        internalEmploymentModel.SurName_1 = Convert.ToString(sqlDataReader["SurName_1"]);

                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();

                        InternalEmploymentList.Add(internalEmploymentModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return InternalEmploymentList;
        }

        /// <summary>
        /// Get Past Employment Documents by PastEmploymentID
        /// </summary>
        /// <param name="PastEmploymentID"></param>
        /// <returns></returns>
        public List<InternalEmploymentModel> GetInternalEmploymentDocs(int InternalEmploymentId)
        {
            List<InternalEmploymentModel> internalEmploymentList = null;
            try
            {
                internalEmploymentList = new List<InternalEmploymentModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetInternalEmploymentDocs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@InternalEmploymentId", InternalEmploymentId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    InternalEmploymentModel internalEmploymentModel;
                    while (sqlDataReader.Read())
                    {
                        internalEmploymentModel = new InternalEmploymentModel();
                        internalEmploymentModel.AttatchmentId = Convert.ToInt32(sqlDataReader["AttatchmentID"].ToString());
                        internalEmploymentModel.InternalEmploymentID = Convert.ToInt32(sqlDataReader["InternalEmploymentID"].ToString());
                        internalEmploymentModel.FileName = Convert.ToString(sqlDataReader["FileName"].ToString());
                        //employeeModel.EmailId = Convert.ToString(sqlDataReader["EmailId"]);
                        internalEmploymentModel.Description = Convert.ToString(sqlDataReader["Description"]);
                        internalEmploymentModel.FileSize = Convert.ToString(sqlDataReader["FileSize"]);
                        internalEmploymentModel.FileType = Convert.ToString(sqlDataReader["FileType"]);
                        internalEmploymentModel.FilePath = Convert.ToString(sqlDataReader["FilePath"]);

                        //employeeModel.PhoneNo = sqlDataReader["PhoneNo"].ToString();

                        internalEmploymentList.Add(internalEmploymentModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return internalEmploymentList;
        }

        /// <summary>
        /// Insert Past Employment Docs
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public string InsertInternalEmploymentDocs(InternalEmploymentModel internalEmploymentModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertInternalEmploymentDocs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@InternalEmploymentID", internalEmploymentModel.InternalEmploymentID);
                sqlCommand.Parameters.AddWithValue("@FileName", internalEmploymentModel.FileName);
                sqlCommand.Parameters.AddWithValue("@Description", internalEmploymentModel.Description);
                sqlCommand.Parameters.AddWithValue("@FileSize", internalEmploymentModel.FileSize);
                sqlCommand.Parameters.AddWithValue("@FileType", internalEmploymentModel.FileType);
                sqlCommand.Parameters.AddWithValue("@FilePath", internalEmploymentModel.FilePath);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", internalEmploymentModel.CreatedBy);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        /// <summary>
        /// Delete Past Employment Docs By PastEmploymentID
        /// </summary>
        /// <param name="pastEmploymentModel"></param>
        /// <returns></returns>
        public OperationDetails DeleteInternalEmploymentDocs(int AttatchmentID)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_DeleteInternalEmploymentDocs", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AttatchmentID", AttatchmentID);


                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();



            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, AttatchmentID);
        }

        public DataSet GetInternalEmployeeDataset(int employeeId, bool addCurrentPosition)
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("Hr_Stp_GetInternalEmploymentData", sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@EmployeeID", employeeId);
            da.SelectCommand.Parameters.AddWithValue("@AddCurrentPosition", addCurrentPosition);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
    }
}
