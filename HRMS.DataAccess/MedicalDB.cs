﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.DataAccess
{
    public class MedicalDB : DBHelper
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        //public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;


        public OperationDetails InsertMedicalMain(MedicalMainModel MedicalMainModel)
        {
            try
            {
                int aTntTranMode = 0;
                if (MedicalMainModel.EmployeeMedicalMainID != null && MedicalMainModel.EmployeeMedicalMainID != 0)
                {
                    aTntTranMode = 2;
                }
                else
                {
                    aTntTranMode = 1;

                }




                SqlParameter[] parameters =
                    {


                        new SqlParameter("@aTntTranMode",aTntTranMode) ,
                      new SqlParameter("@EmployeeMedicalMainID ",MedicalMainModel.EmployeeMedicalMainID) ,
                      new SqlParameter("@EmployeeID", MedicalMainModel.EmployeeID) ,
                      new SqlParameter("@Height", MedicalMainModel.Height) ,
                       new SqlParameter("@Weight", MedicalMainModel.Weight) ,
                      new SqlParameter("@Blood ", MedicalMainModel.Blood),
                      new SqlParameter("@RightEye",MedicalMainModel.RightEye ) ,
                      new SqlParameter("@LeftEye",MedicalMainModel.LeftEye ) ,

                    new SqlParameter("@output", 0)


                    };
                parameters[8].Direction = ParameterDirection.Output;
                 Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_MedicalMainCUD", parameters));
                int InsuranceID = Convert.ToInt32(parameters[8].Value);
                return new OperationDetails(true, "Medical information save successfully", null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertMedicalHistory(MedicalHistoryModel MedicalHistoryModel)

        {
            try
            {
                int aTntTranMode = 0;
                if (MedicalHistoryModel.EmployeeHealthHistoryID != null && MedicalHistoryModel.EmployeeHealthHistoryID != 0)
                {
                    aTntTranMode = 2;
                }
                else
                {
                    aTntTranMode = 1;

                }




                SqlParameter[] parameters =
                    {


                        new SqlParameter("@aTntTranMode",aTntTranMode) ,
                      new SqlParameter("@EmployeeHealthHistoryID ",MedicalHistoryModel.EmployeeHealthHistoryID) ,
                      new SqlParameter("@EmployeeID", MedicalHistoryModel.EmployeeID) ,
                      new SqlParameter("@EmployeeHealthHistoryDate", CommonDB.SetCulturedDate(MedicalHistoryModel.EmployeeHealthHistoryDate)),
                       new SqlParameter("@Description", MedicalHistoryModel.Description) ,
                      new SqlParameter("@Action", MedicalHistoryModel.Action),
                      new SqlParameter("@Note",MedicalHistoryModel.Note ) ,

                    new SqlParameter("@output", 0)


                    };
                parameters[7].Direction = ParameterDirection.Output;
                int InsuranceID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_MedicalHistoryCUD", parameters));
                return new OperationDetails(true, "Medical history information save successfully", null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertMedicalVaccination(MedicalVaccinationModel MedicalVaccinationModel)
        {
            try
            {
                int aTntTranMode = 0;
                if (MedicalVaccinationModel.EmployeeVaccinationID != null && MedicalVaccinationModel.EmployeeVaccinationID != 0)
                {
                    aTntTranMode = 2;
                }
                else
                {
                    aTntTranMode = 1;

                }




                SqlParameter[] parameters =
                    {



                        new SqlParameter("@aTntTranMode",aTntTranMode) ,
                      new SqlParameter("@EmployeeVaccinationID",MedicalVaccinationModel.EmployeeVaccinationID) ,
                      new SqlParameter("@EmployeeID", MedicalVaccinationModel.EmployeeID) ,
                      new SqlParameter("@EmployeeVaccinationDate", CommonDB.SetCulturedDate(MedicalVaccinationModel.EmployeeVaccinationDate)) ,
                       new SqlParameter("@Vaccination", MedicalVaccinationModel.Vaccination) ,
                      new SqlParameter("@Administrated ", MedicalVaccinationModel.Administrated),
                      new SqlParameter("@Note",MedicalVaccinationModel.Note) ,

                    new SqlParameter("@output", 0)


                    };
                parameters[7].Direction = ParameterDirection.Output;
                int InsuranceID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_MedicalVaccinationCUD", parameters));
                return new OperationDetails(true, "Medical vaccination information save successfully", null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertMedicalAlert(MedicalMainModel MedicalMainModel)
        {
            try
            {
                int aTntTranMode = 0;
                if (MedicalMainModel.EmployeeMedicalAlertID != null && MedicalMainModel.EmployeeMedicalAlertID != 0)
                {
                    aTntTranMode = 2;
                }
                else
                {
                    aTntTranMode = 1;

                }




                SqlParameter[] parameters =
                    {


                        new SqlParameter("@aTntTranMode",aTntTranMode) ,
                      new SqlParameter("@EmployeeMedicalAlertID ",MedicalMainModel.EmployeeMedicalAlertID) ,
                      new SqlParameter("@EmployeeID", MedicalMainModel.EmployeeID) ,
                       new SqlParameter("@MedicalAlert",MedicalMainModel.MedicalAlert) ,
                      new SqlParameter("@MedicalAlertAr", MedicalMainModel.MedicalAlertAr) ,
                    new SqlParameter("@output", 0)


                    };
                parameters[5].Direction = ParameterDirection.Output;
                int InsuranceID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_MedicalAlertCUD", parameters));
                return new OperationDetails(true, "Medical alert save successfully", null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }





        public List<MedicalHistoryModel> GetMedicalHistoryModelList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            List<MedicalHistoryModel> MedicalHistoryModelList = new List<MedicalHistoryModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetEmployeeHealthHistory", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        MedicalHistoryModel MedicalHistoryModel;
                        while (reader.Read())
                        {

                            MedicalHistoryModel = new MedicalHistoryModel();
                            MedicalHistoryModel.EmployeeHealthHistoryID = Convert.ToInt32(reader["EmployeeHealthHistoryID"].ToString());
                            MedicalHistoryModel.EmployeeHealthHistoryDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["EmployeeHealthHistoryDate"].ToString());
                            MedicalHistoryModel.Description = reader["Description"].ToString();
                            MedicalHistoryModel.Action = reader["Action"].ToString();

                            MedicalHistoryModel.Note = reader["Note"].ToString();





                            MedicalHistoryModelList.Add(MedicalHistoryModel);
                        }
                    }
                }
                return MedicalHistoryModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<MedicalVaccinationModel> GetMedicalVaccinationModelList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            List<MedicalVaccinationModel> MedicalVaccinationModelList = new List<MedicalVaccinationModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetEmployeeVaccination", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        MedicalVaccinationModel MedicalVaccinationModel;
                        while (reader.Read())
                        {

                            MedicalVaccinationModel = new MedicalVaccinationModel();
                            MedicalVaccinationModel.EmployeeVaccinationID = Convert.ToInt32(reader["EmployeeVaccinationID"].ToString());
                            MedicalVaccinationModel.EmployeeVaccinationDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["EmployeeVaccinationDate"].ToString());
                            MedicalVaccinationModel.Vaccination = Convert.ToInt32(reader["Vaccination"].ToString());
                            MedicalVaccinationModel.VaccinationName = reader["VaccinationName"].ToString();
                            MedicalVaccinationModel.Administrated = Convert.ToBoolean(reader["Administrated"].ToString());

                            MedicalVaccinationModel.Note = reader["Note"].ToString();






                            MedicalVaccinationModelList.Add(MedicalVaccinationModel);
                        }
                    }
                }
                return MedicalVaccinationModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }



        public MedicalVaccinationModel GetMedicalVaccinationModelByID(int EmployeeVaccinationID)
        {


            SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeVaccinationID",EmployeeVaccinationID)
                    };
            // Create a list to hold the Absent		

            try
            {
                MedicalVaccinationModel MedicalVaccinationModel = new MedicalVaccinationModel();

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetEmployeeVaccinationByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {


                            MedicalVaccinationModel.EmployeeVaccinationID = Convert.ToInt32(reader["EmployeeVaccinationID"].ToString());
                            MedicalVaccinationModel.EmployeeVaccinationDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["EmployeeVaccinationDate"].ToString());
                            MedicalVaccinationModel.Vaccination = Convert.ToInt32(reader["Vaccination"].ToString());
                            MedicalVaccinationModel.VaccinationName = reader["VaccinationName"].ToString();
                            MedicalVaccinationModel.Administrated = Convert.ToBoolean(reader["Administrated"].ToString());

                            MedicalVaccinationModel.Note = reader["Note"].ToString();







                        }
                    }
                }
                return MedicalVaccinationModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public MedicalHistoryModel GetMedicalHistoryModelByID(int EmployeeHealthHistoryID)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeHealthHistoryID",EmployeeHealthHistoryID)
                    };
            // Create a list to hold the Absent		

            try
            {
                MedicalHistoryModel MedicalHistoryModel = new MedicalHistoryModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetEmployeeHealthHistoryByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {


                            MedicalHistoryModel.EmployeeHealthHistoryID = Convert.ToInt32(reader["EmployeeHealthHistoryID"].ToString());
                            MedicalHistoryModel.EmployeeHealthHistoryDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["EmployeeHealthHistoryDate"].ToString());
                            MedicalHistoryModel.Description = reader["Description"].ToString();
                            MedicalHistoryModel.Action = reader["Action"].ToString();

                            MedicalHistoryModel.Note = reader["Note"].ToString();






                        }
                    }
                }
                return MedicalHistoryModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public MedicalMainModel GetMedicalMainByEmployee(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            MedicalMainModel MedicalMainModel = new MedicalMainModel();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_GetEmployeeMedicalMain", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {




                            MedicalMainModel.EmployeeMedicalMainID = Convert.ToInt32(reader["EmployeeMedicalMainID"].ToString());
                            MedicalMainModel.Height = Convert.ToDouble(reader["Height"].ToString());
                            MedicalMainModel.Weight = Convert.ToDouble(reader["Weight"].ToString());
                            MedicalMainModel.Blood = Convert.ToInt32(reader["Blood"].ToString());
                            MedicalMainModel.RightEye = reader["RightEye"].ToString();
                            MedicalMainModel.LeftEye = reader["LeftEye"].ToString();
                            MedicalMainModel.Alert1 = reader["Alert1"].ToString();
                            MedicalMainModel.Alerta1 = reader["Alerta1"].ToString();



                        }
                    }
                }
                return MedicalMainModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }



        public List<MedicalMainModel> GetMedicalAlertsByEmployee(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            List<MedicalMainModel> MedicalMainModelList = new List<MedicalMainModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetMedicalAlertByEmployee", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        MedicalMainModel MedicalMainModel;
                        while (reader.Read())
                        {
                            MedicalMainModel = new MedicalMainModel();
                            MedicalMainModel.EmployeeMedicalAlertID = Convert.ToInt32(reader["EmployeeMedicalAlertID"].ToString());
                            MedicalMainModel.MedicalAlert = reader["MedicalAlert"].ToString();
                            MedicalMainModel.MedicalAlertAr = reader["MedicalAlertAr"].ToString();
                            MedicalMainModelList.Add(MedicalMainModel);
                        }
                    }
                }
                return MedicalMainModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public MedicalMainModel GetMedicalAlertsByID(int EmployeeMedicalAlertID)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeMedicalAlertID",EmployeeMedicalAlertID)
                    };
            // Create a list to hold the Absent		

            try
            {
                MedicalMainModel MedicalMainModel = new MedicalMainModel();

                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetMedicalAlertByID", parameters))
                {


                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {



                            MedicalMainModel.EmployeeMedicalAlertID = Convert.ToInt32(reader["EmployeeMedicalAlertID"].ToString());

                            MedicalMainModel.MedicalAlert = reader["MedicalAlert"].ToString();
                            MedicalMainModel.MedicalAlertAr = reader["MedicalAlertAr"].ToString();


                        }
                    }
                }
                return MedicalMainModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<BloodTypeModel> GetBloodTypeList()
        {

            // Create a list to hold the Absent		
            List<BloodTypeModel> BloodTypeModelList = new List<BloodTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetHRBloodtype"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        InsuranceTypeModel InsuranceTypeModel;
                        while (reader.Read())
                        {
                            BloodTypeModel BloodTypeModel = new BloodTypeModel();
                            BloodTypeModel.HRBloodID = Convert.ToInt32(reader["HRBloodID"].ToString());
                            BloodTypeModel.HRBloodName_1 = reader["HRBloodName_1"].ToString();
                            BloodTypeModel.HRBloodName_2 = reader["HRBloodName_2"].ToString();
                            BloodTypeModel.HRBloodName_3 = reader["HRBloodName_3"].ToString();


                            //AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());


                            BloodTypeModelList.Add(BloodTypeModel);
                        }
                    }
                }
                return BloodTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }


        public List<VaccinationTypeModel> GetVaccinationTypeList()
        {

            // Create a list to hold the Absent		
            List<VaccinationTypeModel> VaccinationTypeModelList = new List<VaccinationTypeModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetHRVaccinationtype"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        VaccinationTypeModel VaccinationTypeModel;
                        while (reader.Read())
                        {
                            VaccinationTypeModel = new VaccinationTypeModel();
                            VaccinationTypeModel.HRVaccinationID = Convert.ToInt32(reader["HRVaccinationID"].ToString());
                            VaccinationTypeModel.HRVaccinationName_1 = reader["HRVaccinationName_1"].ToString();
                            VaccinationTypeModel.HRVaccinationName_2 = reader["HRVaccinationName_2"].ToString();
                            VaccinationTypeModel.HRVaccinationName_3 = reader["HRVaccinationName_3"].ToString();


                            //AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());


                            VaccinationTypeModelList.Add(VaccinationTypeModel);
                        }
                    }
                }
                return VaccinationTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<RelationModel> GetRelationTypeList()
        {

            // Create a list to hold the Absent		
            List<RelationModel> RelationTypeModelList = new List<RelationModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_GetRelationName"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        RelationModel RelationModel;
                        while (reader.Read())
                        {
                            RelationModel = new RelationModel();
                            RelationModel.RelationId = Convert.ToInt32(reader["ID"].ToString());
                            RelationModel.RelationName = reader["Name"].ToString();



                            //AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());


                            RelationTypeModelList.Add(RelationModel);
                        }
                    }
                }
                return RelationTypeModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }



        public OperationDetails DeleteHealthHistory(int EmployeeHealthHistoryID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",3) ,
                      new SqlParameter("@EmployeeHealthHistoryID ",EmployeeHealthHistoryID) ,
                        new SqlParameter("@output", 0)


                    };
                parameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_MedicalHistoryCUD", parameters);
                return new OperationDetails(true, "Health History deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Health History.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteMedicalVaccination(int EmployeeVaccinationID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@aTntTranMode",3) ,
                      new SqlParameter("@EmployeeVaccinationID",EmployeeVaccinationID) ,
                      new SqlParameter("@output", 0)


                    };
                parameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_MedicalVaccinationCUD", parameters);
                return new OperationDetails(true, "Medical Vaccination deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Medical Vaccination.", exception);
                throw;
            }
            finally
            {

            }
        }


        public OperationDetails DeleteMedicalAlert(int EmployeeMedicalAlertID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",3) ,
                      new SqlParameter("@EmployeeMedicalAlertID ",EmployeeMedicalAlertID) ,
                        new SqlParameter("@output", 0)


                    };
                parameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_MedicalAlertCUD", parameters);
                return new OperationDetails(true, "Medical Alert deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Medical Alert.", exception);
                throw;
            }
            finally
            {

            }
        }


        //===========================
        public OperationDetails DeleteInsuranceById(int InsuranceID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntInsuranceID",InsuranceID) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_MedicalMainCUD", parameters);
                return new OperationDetails(true, "Insurance deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Insurance.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertInsdependence(InsdependenceModel InsdependenceModel)
        {
            try
            {

                SqlParameter[] parameters =
                    {

                                     new SqlParameter("@aTntTranMode",InsdependenceModel.TranMode) ,
                      new SqlParameter("@aIntInsuranceDependenceID ",InsdependenceModel.InsuranceDependenceID) ,
                      new SqlParameter("@aNvrInsuranceDependenceName_1", InsdependenceModel.InsuranceDependenceName_1) ,
                      new SqlParameter("@aNvrInsuranceDependenceName_2", InsdependenceModel.InsuranceDependenceName_2) ,
                       new SqlParameter("@aTntRelation", InsdependenceModel.Relation) ,
                      new SqlParameter("@aNvrCardNumber ", InsdependenceModel.CardNumber),
                      new SqlParameter("@aSntTeacherID",InsdependenceModel.EmployeeID) ,
                      new SqlParameter("@aNumAmount",InsdependenceModel.Amount) ,
                      new SqlParameter("@aIntInsuredBySchool", InsdependenceModel.InsuredBySchool),
                      new SqlParameter("@aNvrNote",InsdependenceModel.Note),

                    new SqlParameter("@output", 0)


                    };
                parameters[10].Direction = ParameterDirection.Output;
                int InsuranceID = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspInsdependenceCUD", parameters));
                return new OperationDetails(true, "Insurance Dependant saved successfully.", null, InsuranceID);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Insurance Dependant.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public InsdependenceModel GetInsdependenceByID(int InsuranceDependenceID)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@InsuranceDependenceID",InsuranceDependenceID)
                    };
            // Create a list to hold the Absent		
            List<InsdependenceModel> InsdependenceList = new List<InsdependenceModel>();
            try
            {
                InsdependenceModel InsdependenceModel = new InsdependenceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetInsdependenceByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {

                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list

                        while (reader.Read())
                        {

                            InsdependenceModel.InsuranceDependenceID = Convert.ToInt32(reader["InsuranceDependenceID"].ToString());
                            InsdependenceModel.InsuranceDependenceName_1 = reader["InsuranceDependenceName_1"].ToString();

                            InsdependenceModel.Relation = Convert.ToInt32(reader["Relation"].ToString());
                            InsdependenceModel.CardNumber = reader["CardNumber"].ToString();
                            InsdependenceModel.Note = reader["Note"].ToString();




                        }
                    }
                }
                return InsdependenceModel;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }


        public List<InsdependenceModel> GetInsdependenceList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            List<InsdependenceModel> InsdependenceList = new List<InsdependenceModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetInsdependence", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        InsdependenceModel InsdependenceModel;
                        while (reader.Read())
                        {
                            InsdependenceModel = new InsdependenceModel();
                            InsdependenceModel.InsuranceDependenceID = Convert.ToInt32(reader["InsuranceDependenceID"].ToString());
                            InsdependenceModel.Name = reader["InsuranceDependenceName_1"].ToString();
                            InsdependenceModel.RelationName = reader["RelationName"].ToString();
                            InsdependenceModel.Relation = Convert.ToInt32(reader["Relation"].ToString());
                            InsdependenceModel.CardNumber = reader["CardNumber"].ToString();
                            InsdependenceModel.Note = reader["Note"].ToString();



                            InsdependenceList.Add(InsdependenceModel);
                        }
                    }
                }
                return InsdependenceList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteInsdependenceById(int InsuranceDependenceID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntInsuranceDependenceID",InsuranceDependenceID) ,
                      new SqlParameter("@aTntTranMode",3),
                       new SqlParameter("@output", 0)
                    };
                parameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspInsdependenceCUD", parameters);
                return new OperationDetails(true, "Insurance Dependant deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Insurance Dependant.", exception);
                throw;
            }
            finally
            {

            }
        }





        //public List<AbsentModel> GetInsuranceList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, out int totalCount, int empid = 0)
        //{








        //    List<AbsentModel> AbsentModelList = null;
        //    try
        //    {
        //        AbsentModelList = new List<AbsentModel>();

        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("HR_uspGetPayEmployeeAbsentPaging", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
        //        sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
        //        sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
        //        sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);

        //        SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
        //        TotalCount.Direction = ParameterDirection.Output;
        //        sqlCommand.Parameters.Add(TotalCount);

        //        sqlCommand.Parameters.AddWithValue("@empid", empid);

        //        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

        //        if (sqlDataReader.HasRows)
        //        {
        //            LocationModel locationModel;

        //            AbsentModel AbsentModel;

        //            while (sqlDataReader.Read())
        //            {






        //                AbsentModel = new AbsentModel();
        //                AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentID"].ToString());
        //                AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentHeaderID"].ToString());
        //                AbsentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
        //                AbsentModel.PayEmployeeAbsentFromDate = sqlDataReader["PayEmployeeAbsentFromDate"].ToString();
        //                AbsentModel.PayEmployeeAbsentToDate = sqlDataReader["PayEmployeeAbsentToDate"].ToString();

        //                //AbsentModel.PayAbsentTypeID = Convert.ToInt32(sqlDataReader["PayAbsentTypeID"].ToString());
        //                // AbsentModel.PayAbsentTypeName = sqlDataReader["PayAbsentTypeName"].ToString();
        //                AbsentModel.LeaveTypeID = Convert.ToInt32(sqlDataReader["LeaveTypeID"].ToString());
        //                AbsentModel.LeaveTypeName = sqlDataReader["LeaveTypeName"].ToString();
        //                AbsentModel.EmployeeName = sqlDataReader["EmployeeName"].ToString();
        //                AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(sqlDataReader["IsMedicalCertificateProvided"].ToString());
        //                AbsentModel.IsDeducted = Convert.ToBoolean(sqlDataReader["IsDeducted"].ToString());

        //                AbsentModelList.Add(AbsentModel);








        //            }
        //        }
        //        sqlConnection.Close();

        //        totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;

        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }

        //    }
        //    return AbsentModelList;
        //}







    }
}
