﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using HRMS.Entities.General;

namespace HRMS.DataAccess
{
    public class GeneralControlDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public string SaveApplicationLogo(string logo, int mode)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_SaveApplicationLogo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Logo", logo);
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();
                if (Message == "Success")
                {
                    Message = "Logo is saved successfully.";
                }

            }
            catch (Exception exception)
            {
                Message = "Error ! while inserting logo.";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Message;
        }

        public string GetApplicationLogo(int mode)
        {
            string logoName = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetApplicationLogo", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                logoName = (string)sqlCommand.ExecuteScalar();
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return logoName;
        }
        public DocumentRemindersModel GetDocumentReminder()
        {
            try
            {
                DocumentRemindersModel documentReminderModel = new DocumentRemindersModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetDocumentReminders", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        documentReminderModel.DocReminderID = sqlDataReader["DocReminderID"] == DBNull.Value || Convert.ToString(sqlDataReader["DocReminderID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["DocReminderID"]);
                        documentReminderModel.ExpiredNumberOfDays = sqlDataReader["ExpiredNumberOfDays"] == DBNull.Value || Convert.ToString(sqlDataReader["ExpiredNumberOfDays"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["ExpiredNumberOfDays"]);
                        documentReminderModel.ExpiringNumberOfDays = sqlDataReader["ExpiringNumberOfDays"] == DBNull.Value || Convert.ToString(sqlDataReader["ExpiringNumberOfDays"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["ExpiringNumberOfDays"]);
                        documentReminderModel.MonthlyDay = sqlDataReader["MonthlyDay"] == DBNull.Value || Convert.ToString(sqlDataReader["MonthlyDay"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["MonthlyDay"]);
                        documentReminderModel.AboutToExpire = Convert.ToBoolean(sqlDataReader["AboutToExpire"]);
                        documentReminderModel.AlreadyExpire = Convert.ToBoolean(sqlDataReader["AlreadyExpire"]);
                        documentReminderModel.FrequencyType = Convert.ToString(sqlDataReader["FrequencyType"]);
                        documentReminderModel.WeeklyDayName = Convert.ToString(sqlDataReader["WeeklyDayName"]);
                        documentReminderModel.RecieverEmployeeIDs = Convert.ToString(sqlDataReader["RecieverEmployeeIDs"]);
                    }
                }
                sqlConnection.Close();
                return documentReminderModel;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }
        public OperationDetails UpdateDocumentReminder(DocumentRemindersModel documentRemindersModel, int userId)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateDocumentReminders", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@DocReminderID", documentRemindersModel.DocReminderID);
                sqlCommand.Parameters.AddWithValue("@AboutToExpire", documentRemindersModel.AboutToExpire);
                sqlCommand.Parameters.AddWithValue("@ExpiringNumberOfDays", documentRemindersModel.ExpiringNumberOfDays ?? 0);
                sqlCommand.Parameters.AddWithValue("@AlreadyExpire", documentRemindersModel.AlreadyExpire);
                sqlCommand.Parameters.AddWithValue("@ExpiredNumberOfDays", documentRemindersModel.ExpiredNumberOfDays ?? 0);
                sqlCommand.Parameters.AddWithValue("@FrequencyType", documentRemindersModel.FrequencyType);
                sqlCommand.Parameters.AddWithValue("@WeeklyDayName", documentRemindersModel.WeeklyDayName ?? "");
                sqlCommand.Parameters.AddWithValue("@MonthlyDay", documentRemindersModel.MonthlyDay);
                sqlCommand.Parameters.AddWithValue("@RecieverEmployeeIDs", documentRemindersModel.RecieverEmployeeIDs);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                // Insert Spoken Languages
                //sqlConnection.Open();                
                sqlDataReader.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);

        }
        public AttendanceRemindersModel GetAttendanceReminder()
        {
            try
            {
                AttendanceRemindersModel attendanceRemindersModel = new AttendanceRemindersModel(); ;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetAttendanceReminders", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        attendanceRemindersModel.AttReminderID = sqlDataReader["AttReminderID"] == DBNull.Value || Convert.ToString(sqlDataReader["AttReminderID"]) == "" ? 0 : Convert.ToInt32(sqlDataReader["AttReminderID"]);
                        attendanceRemindersModel.EnableReminder = Convert.ToBoolean(sqlDataReader["EnableReminder"]);
                        attendanceRemindersModel.AttendanceStatusIDs = Convert.ToString(sqlDataReader["AttendanceStatusIDs"]);
                        attendanceRemindersModel.RecieverEmployeeIDs = Convert.ToString(sqlDataReader["RecieverEmployeeIDs"]);
                    }
                }
                sqlConnection.Close();
                return attendanceRemindersModel;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {

            }
        }
        public OperationDetails UpdateAttendanceReminder(AttendanceRemindersModel attendanceRemindersModel, int userId)
        {
            string Message = string.Empty;
            int iResult = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_InsertUpdateAttendanceReminders", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@AttReminderID", attendanceRemindersModel.AttReminderID);
                sqlCommand.Parameters.AddWithValue("@AttendanceStatusIDs", attendanceRemindersModel.AttendanceStatusIDs);
                sqlCommand.Parameters.AddWithValue("@RecieverEmployeeIDs", attendanceRemindersModel.RecieverEmployeeIDs);
                sqlCommand.Parameters.AddWithValue("@EnableReminder", attendanceRemindersModel.EnableReminder);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);

                SqlParameter OperationId = new SqlParameter("@OperationId", SqlDbType.Int);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Message = OperationMessage.Value.ToString();
                iResult = Convert.ToInt32(OperationId.Value);

                // Insert Spoken Languages
                //sqlConnection.Open();                
                sqlDataReader.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return new OperationDetails(true, Message, null, iResult);

        }
        public List<TrainingCompetencyDetailModel> GetTrainingCompetencies()
        {
            List<TrainingCompetencyDetailModel> trainingCompetencies = new List<TrainingCompetencyDetailModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetTrainingCompetencies", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    TrainingCompetencyDetailModel model;
                    while (reader.Read())
                    {
                        model = new TrainingCompetencyDetailModel();
                        model.CompetencyID = Convert.ToInt32(reader["CompetencyID"].ToString());
                        model.TrainingID = Convert.ToInt32(reader["TrainingID"].ToString());
                        model.TrainingName = reader["TrainingName"].ToString() == "" ? "" : reader["TrainingName"].ToString();
                        model.CompetencyTitle = reader["CompetencyTitle"].ToString() == "" ? "" : reader["CompetencyTitle"].ToString();
                        trainingCompetencies.Add(model);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
            return trainingCompetencies;
        }

        public TrainingCompetencyDetailModel GetTrainingCompetencyById(int id)
        {
            TrainingCompetencyDetailModel model = new TrainingCompetencyDetailModel();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetTrainingCompetencyById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TrainingID", id);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        model = new TrainingCompetencyDetailModel();
                        model.CompetencyID = Convert.ToInt32(reader["CompetencyID"].ToString());
                        model.TrainingID = Convert.ToInt32(reader["TrainingID"].ToString());
                        model.TrainingName = reader["TrainingName"].ToString() == "" ? "" : reader["TrainingName"].ToString();
                        model.CompetencyTitle = reader["CompetencyTitle"].ToString() == "" ? "" : reader["CompetencyTitle"].ToString();
                        model.CompetencyIDs = reader["CompetencyIDs"].ToString() == "" ? "" : reader["CompetencyIDs"].ToString();
                    }
                }
                sqlConnection.Close();
                return model;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddEditTrainingCompetency(TrainingCompetencyDetailModel trainingCompetencyModel, int tranMode)
        {
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateTrainingCompetencies", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@TrainingID", trainingCompetencyModel.TrainingID);
                sqlCommand.Parameters.AddWithValue("@CompetencyIDs", trainingCompetencyModel.CompetencyIDs ?? string.Empty);
                sqlCommand.Parameters.AddWithValue("@TranMode", tranMode);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;
                if (success)
                {
                    op.CssClass = "success";
                    if (OperationMessage == "InsertSuccess")
                        OperationMessage = "Record saved successfully.";
                    else if (OperationMessage == "UpdateSuccess")
                        OperationMessage = "Training Competency updated successfully.";
                }
                else
                {
                    if (OperationMessage == "Duplicate")
                        OperationMessage = "Training title is already exists.";
                    else if (OperationMessage == "AlreadyReferenced")
                        OperationMessage = "Training title is used in development plan, you cannot delete it.";
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting qualification subject details.", exception);
                //throw;
            }
            finally
            {

            }
            return op;
        }
    }
}
