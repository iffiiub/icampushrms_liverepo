﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class WorkexperienceDB : DBHelper
    {
        public OperationDetails InsertWorkexperience(WorkexperienceModel WorkexperienceModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aNvrEmployerName_1", WorkexperienceModel.EmployerName_1) ,
                      new SqlParameter("@aNvrEmployerName_2", WorkexperienceModel.EmployerName_2) ,
                      new SqlParameter("@aNvrEmployerName_3", WorkexperienceModel.EmployerName_3),
                      new SqlParameter("@aDttWorkStartDate", WorkexperienceModel.WorkStartDate ) ,
                      new SqlParameter("@aDttWorkEndDate",WorkexperienceModel.WorkEndDate ) ,
                      new SqlParameter("@aSntHRExperiencePositionID", WorkexperienceModel.HRExperiencePositionID ),
                      new SqlParameter("@aNvrWorkAddress",WorkexperienceModel.WorkAddress),
                      new SqlParameter("@aSntCityID",WorkexperienceModel.CityID),
                      new SqlParameter("@aSntCountryID",WorkexperienceModel.CountryID) ,
                      new SqlParameter("@aSntEmployeeID", WorkexperienceModel.EmployeeID) ,
                      new SqlParameter("@aSntExperienceMonths", WorkexperienceModel.ExperienceMonths)
                    };
                int WorkexperienceId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspWorkexperienceCUD  ", parameters));
                return new OperationDetails(true, "Workexperience saved successfully.", null, WorkexperienceId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Workexperience.", exception);
                //throw;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateWorkexperience(WorkexperienceModel WorkexperienceModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                         new SqlParameter("@aTntTranMode",2) ,
                         new SqlParameter("@aIntWorkexperienceID",WorkexperienceModel.WorkexperienceID) ,
                      new SqlParameter("@aNvrEmployerName_1", WorkexperienceModel.EmployerName_1) ,
                      new SqlParameter("@aNvrEmployerName_2", WorkexperienceModel.EmployerName_2) ,
                      new SqlParameter("@aNvrEmployerName_3", WorkexperienceModel.EmployerName_3),
                      new SqlParameter("@aDttWorkStartDate", WorkexperienceModel.WorkStartDate ) ,
                      new SqlParameter("@aDttWorkEndDate",WorkexperienceModel.WorkEndDate ) ,
                      new SqlParameter("@aSntHRExperiencePositionID", WorkexperienceModel.HRExperiencePositionID ),
                      new SqlParameter("@aNvrWorkAddress",WorkexperienceModel.WorkAddress),
                      new SqlParameter("@aSntCityID",WorkexperienceModel.CityID),
                      new SqlParameter("@aSntCountryID",WorkexperienceModel.CountryID) ,
                      new SqlParameter("@aSntEmployeeID", WorkexperienceModel.EmployeeID) ,
                      new SqlParameter("@aSntExperienceMonths", WorkexperienceModel.ExperienceMonths)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspWorkexperienceCUD", parameters);
                return new OperationDetails(true, "Workexperience updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Workexperience.", exception);
                throw;
            }
            finally
            {

            }
        }


        public OperationDetails DeleteWorkexperienceById(int WorkexperienceId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@aIntWorkexperienceID",WorkexperienceId) ,
                      new SqlParameter("@aTntTranMode",3) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspWorkexperienceCUD", parameters);
                return new OperationDetails(true, "Workexperience deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Workexperience.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<WorkexperienceModel> GetWorkexperienceList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {    
                        new SqlParameter("@aSntEmployeeID",EmployeeId)
                    };
            // Create a list to hold the Workexperience		
            List<WorkexperienceModel> WorkexperienceModelList = new List<WorkexperienceModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetWorkexperience", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        WorkexperienceModel WorkexperienceModel;
                        while (reader.Read())
                        {
                            WorkexperienceModel = new WorkexperienceModel();
                            WorkexperienceModel.WorkexperienceID = Convert.ToInt32(reader["WorkexperienceID"].ToString());
                            WorkexperienceModel.EmployerName_1 = reader["EmployerName_1"].ToString();
                            WorkexperienceModel.EmployerName_2 = reader["EmployerName_2"].ToString();
                            WorkexperienceModel.EmployerName_3 = reader["EmployerName_3"].ToString();
                            WorkexperienceModel.WorkStartDate = reader["WorkStartDate"].ToString();
                            WorkexperienceModel.WorkEndDate = reader["WorkEndDate"].ToString();
                            WorkexperienceModel.HRExperiencePositionID = Convert.ToInt32(reader["HRExperiencePositionID"].ToString());
                            WorkexperienceModel.WorkAddress = reader["WorkAddress"].ToString();
                            WorkexperienceModel.CityID = Convert.ToInt32(reader["CityID"].ToString());
                            WorkexperienceModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                            WorkexperienceModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            WorkexperienceModel.ExperienceMonths = Convert.ToInt32(reader["ExperienceMonths"].ToString());

                            WorkexperienceModelList.Add(WorkexperienceModel);
                        }
                    }
                }
                return WorkexperienceModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching WorkexperienceModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public WorkexperienceModel WorkexperienceById(int WorkexperienceId)
        {
            try
            {
                WorkexperienceModel WorkexperienceModel = new WorkexperienceModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetWorkexperienceByID", new SqlParameter("@aIntWorkexperienceID", WorkexperienceId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            WorkexperienceModel.WorkexperienceID = Convert.ToInt32(reader["WorkexperienceID"].ToString());
                            WorkexperienceModel.EmployerName_1 = reader["EmployerName_1"].ToString();
                            WorkexperienceModel.EmployerName_2 = reader["EmployerName_2"].ToString();
                            WorkexperienceModel.EmployerName_3 = reader["EmployerName_3"].ToString();
                            WorkexperienceModel.WorkStartDate = reader["WorkStartDate"].ToString();
                            WorkexperienceModel.WorkEndDate = reader["WorkEndDate"].ToString();
                            WorkexperienceModel.HRExperiencePositionID = Convert.ToInt32(reader["HRExperiencePositionID"].ToString());
                            WorkexperienceModel.WorkAddress = reader["WorkAddress"].ToString();
                            WorkexperienceModel.CityID = Convert.ToInt32(reader["CityID"].ToString());
                            WorkexperienceModel.CountryID = Convert.ToInt32(reader["CountryID"].ToString());
                            WorkexperienceModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            WorkexperienceModel.ExperienceMonths = Convert.ToInt32(reader["ExperienceMonths"].ToString());
                        }
                    }
                }
                return WorkexperienceModel;
                // Finally, we return WorkexperienceModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching WorkexperienceModel.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
