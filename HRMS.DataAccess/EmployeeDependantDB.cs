﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class EmployeeDependantDB : DBHelper
    {

        #region CURD Operation EmployeeDependantModule
        /// <summary>
        /// Insert Employee Dependant into EmployeeDependant table
        /// Created By : Pradnesh Raut
        /// Created Date : 19 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails InsertEmployeeDependant(EmployeeDependantModel employeeDependantModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@FirstName",employeeDependantModel.FirstName) ,
                      new SqlParameter("@EmployeeId",employeeDependantModel.EmployeeId) ,
                      new SqlParameter("@MiddleName", employeeDependantModel.MiddleName) ,
                      new SqlParameter("@LastName", employeeDependantModel.LastName) ,
                      new SqlParameter("@Relationship", employeeDependantModel.Relationship),
                      new SqlParameter("@Address1", employeeDependantModel.Address1) ,
                      new SqlParameter("@Address2",employeeDependantModel.Address2)  ,
                      new SqlParameter("@City", employeeDependantModel.City) ,
                      new SqlParameter("@City_Town", employeeDependantModel.City_Town),
                      new SqlParameter("@Country", employeeDependantModel.Country) ,
                      new SqlParameter("@Phone1",employeeDependantModel.Phone1) ,
                      new SqlParameter("@Phone2", employeeDependantModel.Phone2),
                      new SqlParameter("@CreatedBy", employeeDependantModel.CreatedBy),
                      new SqlParameter("@StudentId", employeeDependantModel.StudentId)
                    };

                int DependantId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                  "stp_Add_HR_Employee_Dependant", parameters));
                return new OperationDetails(true, "Children is added to dependant successfully.", null, DependantId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while employee dependant Position.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public OperationDetails DeleteEmployeeDependant(int studentId)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("DELETE FROM HR_Employee_Dependant Where StudentId=@StudentId", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                sqlCommand.Parameters.AddWithValue("@StudentId", studentId);

                int rowsAffected = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                if (rowsAffected > 0)
                {
                    op.Message = "Children deleted from dependant successfully";
                    op.Success = true;
                }
                else
                {
                    op.Message = "No rows to delete";
                    op.Success = false;
                }


            }
            catch (Exception exception)
            {
                // throw exception;
                op.Message = "Failed to delete children from dependant";
                op.Success = false;
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        /// <summary>
        /// update Employee Dependant into EmployeeDependant table
        /// Created By : Pradnesh Raut
        /// Created Date : 19 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails UpdateEmployeeDependant(EmployeeDependantModel employeeDependantModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                        new SqlParameter("@DependantId",employeeDependantModel.DependantId) ,
                        new SqlParameter("@EmployeeId",employeeDependantModel.EmployeeId) ,
                       new SqlParameter("@FirstName",employeeDependantModel.FirstName) ,
                      new SqlParameter("@MiddleName", employeeDependantModel.MiddleName) ,
                      new SqlParameter("@LastName", employeeDependantModel.LastName) ,
                      new SqlParameter("@Relationship", employeeDependantModel.Relationship),
                      new SqlParameter("@Address1", employeeDependantModel.Address1) ,
                      new SqlParameter("@Address2",employeeDependantModel.Address2)  ,
                      new SqlParameter("@City", employeeDependantModel.City) ,
                      new SqlParameter("@City_Town", employeeDependantModel.City_Town),
                      new SqlParameter("@Country", employeeDependantModel.Country) ,
                      new SqlParameter("@Phone1",employeeDependantModel.Phone1) ,
                      new SqlParameter("@Phone2", employeeDependantModel.Phone2),
                      new SqlParameter("@ModifiedBy", employeeDependantModel.ModifiedBy)
                      
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Employee_Dependant", parameters);
                return new OperationDetails(true, "Employee dependant updated successfully.", null);
            }
            catch (Exception exception)
            {
                 return new OperationDetails(false, "Error : while updating employee dependant.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all Employee Dependant from the EmployeeDependant table
        /// Created By : Pradnesh Raut
        /// Created Date : 19 April 2015
        /// </summary>
        /// <returns>List of Employee Dependant</returns>
        public List<EmployeeDependantModel> GetEmployeeDependantList()
        {
            // Create a list to hold the EmployeeDependant		
            List<EmployeeDependantModel> employeeDependantModelList = new List<EmployeeDependantModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Employee_DependantList"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new EmployeeDependant, and insert them into our list
                        EmployeeDependantModel employeeDependantModel;
                        while (reader.Read())
                        {
                            employeeDependantModel = new EmployeeDependantModel();
                            employeeDependantModel.DependantId = Convert.ToInt32(reader["DependantId"].ToString());
                            employeeDependantModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            employeeDependantModel.FirstName = reader["FirstName"].ToString();
                            employeeDependantModel.MiddleName = reader["MiddleName"].ToString();
                            employeeDependantModel.LastName = reader["LastName"].ToString();
                            employeeDependantModel.Relationship = reader["Relationship"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Relationship"]);
                            employeeDependantModel.Address1 = reader["Address1"].ToString();
                            employeeDependantModel.Address2 = reader["Address2"].ToString();
                            employeeDependantModel.City = reader["City"].ToString();
                            employeeDependantModel.City_Town = reader["City_Town"].ToString();
                            employeeDependantModel.Country = Convert.ToInt32(reader["Country"].ToString());
                            employeeDependantModel.Phone1 = reader["Phone1"].ToString();
                            employeeDependantModel.Phone2 = reader["Phone2"].ToString();
                            employeeDependantModel.CreatedOn = reader["CreatedOn"].ToString();
                            employeeDependantModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                            employeeDependantModelList.Add(employeeDependantModel);
                        }
                    }
                }
                // Finally, we return our list of EmployeeDependant
                return employeeDependantModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching EmployeeDependant List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get Employee Dependant By id from the EmployeeDependant table
        /// Created By : Pradnesh Raut
        /// Created Date : 19 April 2015
        /// </summary>
        /// <returns>Employee Dependant</returns>
        public EmployeeDependantModel GetEmployeeDependantByDependantId(int DependantId)
        {
            try
            {
                EmployeeDependantModel employeeDependantModel = new EmployeeDependantModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Employee_DependantByDependantId", new SqlParameter("@DependantId", DependantId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            employeeDependantModel.DependantId = Convert.ToInt32(reader["DependantId"].ToString());
                            employeeDependantModel.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
                            employeeDependantModel.FirstName = reader["FirstName"].ToString();
                            employeeDependantModel.MiddleName = reader["MiddleName"].ToString();
                            employeeDependantModel.LastName = reader["LastName"].ToString();
                            employeeDependantModel.Relationship = reader["Relationship"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Relationship"]);
                            employeeDependantModel.Address1 = reader["Address1"].ToString();
                            employeeDependantModel.Address2 = reader["Address2"].ToString();
                            employeeDependantModel.City = reader["City"].ToString();
                            employeeDependantModel.City_Town = reader["City_Town"].ToString();
                            employeeDependantModel.Country = Convert.ToInt32(reader["Country"].ToString());
                            employeeDependantModel.Phone1 = reader["Phone1"].ToString();
                            employeeDependantModel.Phone2 = reader["Phone2"].ToString();
                            employeeDependantModel.CreatedOn = reader["CreatedOn"].ToString();
                            employeeDependantModel.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());
                            employeeDependantModel.RelationshipName = reader["RelationshipName"].ToString();
                            employeeDependantModel.CountryName = reader["CountryName"].ToString();
                        }
                    }
                }
                return employeeDependantModel;
                // Finally, we return employeeDependant

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching employeeDependant.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Delete Employee Dependent from the EmployeeDependant table
        /// Created By : Pradnesh Raut
        /// Created Date : 19 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails DeleteEmployeeDependantByDependentId(int DependentId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@DependentId",DependentId) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_Employee_DependantByDependantId", parameters);
                return new OperationDetails(true, "Dependent deleted successfully", null);

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while deleting Dependent.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteEmployeeDependant(int DependentId,int UserId)
        {
            try
            {
                SqlParameter[] parameters =
                    {    
                      new SqlParameter("@DependentId",DependentId) ,
                      new SqlParameter("@UserId",UserId) 
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "STP_HR_DeleteEmployeeDependent", parameters);
                return new OperationDetails(true, "Dependent deleted successfully", null);

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while deleting Dependent.", exception);
                throw;
            }
            finally
            {

            }
        }
        

        #endregion
    }
}
