﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class HR_PaySundriesDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails InsertHR_PaySundries(HR_PaySundriesModel HR_PaySundriesModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aSntEmployeeID", HR_PaySundriesModel.EmployeeID) ,
                      new SqlParameter("@aDttPaySundriesDate",GeneralDB.CommonDB.SetCulturedDate(HR_PaySundriesModel.PaySundriesDate)),
                      new SqlParameter("@aIntHRSundryID", HR_PaySundriesModel.HRSundryID ) ,
                      new SqlParameter("@aFltamount",HR_PaySundriesModel.Amount ) ,
                      new SqlParameter("@aIntacyear", HR_PaySundriesModel.AcYear ),
                      new SqlParameter("@aIntPayBatchID",HR_PaySundriesModel.PayBatchID),
                      new SqlParameter("@aIntgenerate",HR_PaySundriesModel.generate)
                    };
                int HR_PaySundriesId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPaySundriesCUD  ", parameters));
                return new OperationDetails(true, "Pay sundry save successfully", null, HR_PaySundriesId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateHR_PaySundries(HR_PaySundriesModel HR_PaySundriesModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@aTntTranMode",2) ,
                         new SqlParameter("@aIntPaySundriesID", HR_PaySundriesModel.PaySundriesID) ,
                      new SqlParameter("@aSntEmployeeID", HR_PaySundriesModel.EmployeeID) ,
                      new SqlParameter("@aDttPaySundriesDate", GeneralDB.CommonDB.SetCulturedDate(HR_PaySundriesModel.PaySundriesDate)),
                      new SqlParameter("@aIntHRSundryID", HR_PaySundriesModel.HRSundryID ) ,
                      new SqlParameter("@aFltamount",HR_PaySundriesModel.Amount ) ,
                      new SqlParameter("@aIntacyear", HR_PaySundriesModel.AcYear ),
                      new SqlParameter("@aIntPayBatchID",HR_PaySundriesModel.PayBatchID),
                      new SqlParameter("@aIntgenerate",HR_PaySundriesModel.generate)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspPaySundriesCUD", parameters);
                return new OperationDetails(true, "Pay sundries updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteHR_PaySundriesById(int HR_PaySundriesId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntPaySundriesID", HR_PaySundriesId) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspPaySundriesCUD", parameters);
                return new OperationDetails(true, "Pay sundries deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<HR_PaySundriesModel> GetHR_PaySundriesList_Sort(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder, int BatchId, int SundriesId, int AcademicYearId, string BankListIds, out int totalCount)
        {
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            sqlCommand = new SqlCommand("HR_uspGetPaySundries", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            sqlCommand.Parameters.AddWithValue("@OffsetRows", pageNumber);
            sqlCommand.Parameters.AddWithValue("@FetchRows", numberOfRecords);
            sqlCommand.Parameters.AddWithValue("@SortColumn", sortColumn);
            sqlCommand.Parameters.AddWithValue("@SortOrder", sortOrder);
            sqlCommand.Parameters.AddWithValue("@aSntBatchID", BatchId);
            sqlCommand.Parameters.AddWithValue("@aSntSundriesID", SundriesId);
            sqlCommand.Parameters.AddWithValue("@aSntAcademicYearID", AcademicYearId);
            sqlCommand.Parameters.AddWithValue("@BankListIds", BankListIds);

            SqlParameter TotalCount = new SqlParameter("@count", SqlDbType.Int);
            TotalCount.Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add(TotalCount);

            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            try
            {
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HR_PaySundriesModel HR_PaySundriesModel;
                    while (sqlDataReader.Read())
                    {
                        HR_PaySundriesModel = new HR_PaySundriesModel();
                        HR_PaySundriesModel.EmpName = sqlDataReader["EmployeeName"].ToString() ?? "";
                        HR_PaySundriesModel.PaySundriesDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PaySundriesDate"].ToString());
                        HR_PaySundriesModel.SundryName = sqlDataReader["Sundry"].ToString() ?? "";
                        HR_PaySundriesModel.PayBatchName = sqlDataReader["Batch"].ToString() ?? "";
                        HR_PaySundriesModel.AcademicYear = sqlDataReader["AcademicYear"].ToString() ?? "";
                        HR_PaySundriesModel.Amount = Convert.ToDecimal(sqlDataReader["amount"].ToString() ?? "0.0");

                        HR_PaySundriesModel.PaySundriesID = Convert.ToInt32(sqlDataReader["PaySundriesID"].ToString() ?? "0");
                        HR_PaySundriesModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString() ?? "0");
                        HR_PaySundriesModel.HRSundryID = Convert.ToInt32(sqlDataReader["HRSundryID"].ToString() ?? "0");
                        HR_PaySundriesModel.AcYear = Convert.ToInt32(sqlDataReader["acyear"].ToString() ?? "0");
                        HR_PaySundriesModel.PayBatchID = Convert.ToInt32(sqlDataReader["PayBatchID"].ToString() ?? "0");
                        HR_PaySundriesModel.generate = Convert.ToInt32(sqlDataReader["generate"].ToString() ?? "0");

                        HR_PaySundriesModelList.Add(HR_PaySundriesModel);
                    }
                }
                sqlConnection.Close();
                totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return HR_PaySundriesModelList;
        }

        public List<HR_PaySundriesModel> GetHR_PaySundriesList(int BatchId, int SundriesId, int AcademicYearId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntBatchID",BatchId),
                        new SqlParameter("@aSntSundriesID",SundriesId),
                        new SqlParameter("@aSntAcademicYearID",AcademicYearId)
                    };
            // Create a list to hold the HR_PaySundries		
            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPaySundriesOfBatch", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        HR_PaySundriesModel HR_PaySundriesModel;
                        while (reader.Read())
                        {
                            HR_PaySundriesModel = new HR_PaySundriesModel();
                            HR_PaySundriesModel.PaySundriesID = Convert.ToInt32(reader["PaySundriesID"].ToString() ?? "0");
                            HR_PaySundriesModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString() ?? "0");
                            HR_PaySundriesModel.PaySundriesDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["PaySundriesDate"].ToString());
                            HR_PaySundriesModel.HRSundryID = Convert.ToInt32(reader["HRSundryID"].ToString() ?? "0");
                            HR_PaySundriesModel.AcYear = Convert.ToInt32(reader["acyear"].ToString() ?? "0");
                            HR_PaySundriesModel.PayBatchID = Convert.ToInt32(reader["PayBatchID"].ToString() ?? "0");
                            HR_PaySundriesModel.generate = Convert.ToInt32(reader["generate"].ToString() ?? "0");
                            HR_PaySundriesModel.Amount = Convert.ToDecimal(reader["amount"].ToString() ?? "0.0");

                            EmployeeDB objEmployeeDB = new EmployeeDB();
                            var empobj = objEmployeeDB.GetEmployee(HR_PaySundriesModel.EmployeeID);
                            HR_PaySundriesModel.EmpName = empobj.FirstName + " " + empobj.LastName;

                            HR_PaySundriesModelList.Add(HR_PaySundriesModel);
                        }
                    }
                }
                return HR_PaySundriesModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching HR_PaySundriesModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public HR_PaySundriesModel HR_PaySundriesById(int HR_PaySundriesId)
        {
            try
            {
                HR_PaySundriesModel HR_PaySundriesModel = new HR_PaySundriesModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPaySundriesByID", new SqlParameter("@aIntPaySundriesID", HR_PaySundriesId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            HR_PaySundriesModel.PaySundriesID = Convert.ToInt32(reader["PaySundriesID"].ToString());
                            HR_PaySundriesModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            HR_PaySundriesModel.PaySundriesDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["PaySundriesDate"].ToString());
                            HR_PaySundriesModel.HRSundryID = Convert.ToInt32(reader["HRSundryID"].ToString());
                            HR_PaySundriesModel.AcYear = Convert.ToInt32(reader["acyear"].ToString());
                            HR_PaySundriesModel.PayBatchID = Convert.ToInt32(reader["PayBatchID"].ToString());
                            HR_PaySundriesModel.generate = Convert.ToInt32(reader["generate"].ToString());
                            HR_PaySundriesModel.Amount = Convert.ToDecimal(reader["amount"].ToString());
                        }
                    }
                }
                return HR_PaySundriesModel;
                // Finally, we return HR_PaySundriesModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching HR_PaySundriesModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<HR_PaySundriesModel> GetHR_PaySundriesListForReport(int BatchId, int SundriesId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aIntBatchID",BatchId),
                        new SqlParameter("@aIntHRSundryID",SundriesId)
                    };
            // Create a list to hold the HR_PaySundries		
            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPaySundriesOfBatchForReport", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        HR_PaySundriesModel HR_PaySundriesModel;
                        while (reader.Read())
                        {
                            HR_PaySundriesModel = new HR_PaySundriesModel();
                            HR_PaySundriesModel.Amount = Convert.ToDecimal(reader["Amount"].ToString() ?? "0.0");
                            HR_PaySundriesModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString() ?? "0");
                            HR_PaySundriesModel.SundryName = reader["SundryName"].ToString() ?? "";
                            HR_PaySundriesModel.PayBatchName = reader["PayBatchName"].ToString() ?? "";
                            HR_PaySundriesModel.AccountNumber = reader["AccountNumber"].ToString() ?? "";
                            HR_PaySundriesModel.EmpName = reader["EmpName"].ToString() ?? "";
                            HR_PaySundriesModel.BankID = reader["BankID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["BankID"]);
                            HR_PaySundriesModel.BankName = reader["BankName"].ToString() ?? "";

                            HR_PaySundriesModelList.Add(HR_PaySundriesModel);
                        }
                    }
                }
                return HR_PaySundriesModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching HR_PaySundriesModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<HR_PaySundriesModel> GetHR_PaySundriesListForWPS(int BatchId, int SundriesId, int AcademicYearId, string BankListIds = "")
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aIntBatchID",BatchId),
                        new SqlParameter("@aIntHRSundryID",SundriesId),
                        new SqlParameter("@aIntAcademicYearId",AcademicYearId),
                        new SqlParameter("@BankListIds",BankListIds)
                    };
            // Create a list to hold the HR_PaySundries		
            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_SundriesInternalBanklist", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        HR_PaySundriesModel HR_PaySundriesModel;
                        while (reader.Read())
                        {
                            HR_PaySundriesModel = new HR_PaySundriesModel();
                            HR_PaySundriesModel.Amount = Convert.ToDecimal(reader["Amount"].ToString());
                            HR_PaySundriesModel.EmployeeID = reader["EmployeeID"].ToString() != "" ? Convert.ToInt32(reader["EmployeeID"].ToString()) : 0;
                            HR_PaySundriesModel.AccountNumber = reader["AccountNumber"].ToString();
                            HR_PaySundriesModel.EmpName = reader["EmpName"].ToString();
                            HR_PaySundriesModel.BankID = reader["BankID"].ToString() != "" ? Convert.ToInt32(reader["BankID"].ToString()) : 0;
                            HR_PaySundriesModel.BankName = reader["BankName"].ToString();
                            HR_PaySundriesModel.PayBatchName = reader["BatchName"].ToString();
                            HR_PaySundriesModel.SundryName = Convert.ToString(reader["HRSundryName_1"] == DBNull.Value ? "" : reader["HRSundryName_1"]);
                            HR_PaySundriesModelList.Add(HR_PaySundriesModel);
                        }
                    }
                }
                return HR_PaySundriesModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching HR_PaySundriesModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public DataTable ExternalExcelExport(int? BatchId, int? SundriesId, int? AcademicYearId, string BankListIds = "")
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aIntBatchID",BatchId),
                        new SqlParameter("@aIntHRSundryID",SundriesId),
                        new SqlParameter("@aIntAcademicYearId",AcademicYearId),
                        new SqlParameter("@BankListIds",BankListIds)
                    };
           	
            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            DataTable dt = new DataTable();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_SundriesExternalBanklist", parameters))
                {
                    dt.Load(reader);                   
                }
                return dt;
            }
            catch (Exception exception)
            {                
                throw;
            }
            finally
            {

            }
        }

        public DataTable NBDExternalExcelExport(int? BatchId, int? SundriesId, int? AcademicYearId, string BankListIds = "")
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntBatchID",BatchId),
                        new SqlParameter("@aSntSundriesID",SundriesId),
                        new SqlParameter("@aSntAcademicYearID",AcademicYearId),
                        new SqlParameter("@BankListIds",BankListIds)
                    };

            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            DataTable dt = new DataTable();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_SundriesExternalNBDBanklist", parameters))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception exception)
            {
                throw;
            }
            finally
            {

            }
        }
        public OperationDetails GetHR_PaySundriesListForGenerateJV(int BatchId, int UserId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aBatch",BatchId),
                        new SqlParameter("@aUserID",UserId)
                    };
                int output = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                        "HR_uspPassSundryJVs  ", parameters));

                return new OperationDetails(true, "JV passed successfully.", null, output);
            }

            catch (Exception exception)
            {
                return new OperationDetails(false, exception.Message, null, 0);
                // throw;
            }
            finally
            {

            }
        }

        public List<HR_PaySundriesModel> GetPaySundriesByDate(DateTime FromDate, DateTime ToDate)
        {
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            sqlCommand = new SqlCommand("Hr_Stp_GetSundriesByDate", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@FromDate", FromDate);
            sqlCommand.Parameters.AddWithValue("@ToDate", ToDate);
            List<HR_PaySundriesModel> HR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            try
            {
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HR_PaySundriesModel HR_PaySundriesModel;
                    while (sqlDataReader.Read())
                    {
                        HR_PaySundriesModel = new HR_PaySundriesModel();
                        HR_PaySundriesModel.EmpName = sqlDataReader["EmployeeName"].ToString() ?? "";
                        HR_PaySundriesModel.PaySundriesDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PaySundriesDate"].ToString());
                        HR_PaySundriesModel.SundryName = sqlDataReader["Sundry"].ToString() ?? "";
                        HR_PaySundriesModel.PayBatchName = sqlDataReader["Batch"].ToString() ?? "";
                        HR_PaySundriesModel.AcademicYear = sqlDataReader["AcademicYear"].ToString() ?? "";
                        HR_PaySundriesModel.Amount = Convert.ToDecimal(sqlDataReader["amount"].ToString() ?? "0.0");
                        HR_PaySundriesModel.PaySundriesID = Convert.ToInt32(sqlDataReader["PaySundriesID"].ToString() ?? "0");
                        HR_PaySundriesModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString() ?? "0");
                        HR_PaySundriesModel.HRSundryID = Convert.ToInt32(sqlDataReader["HRSundryID"].ToString() ?? "0");
                        HR_PaySundriesModel.AcYear = Convert.ToInt32(sqlDataReader["acyear"].ToString() ?? "0");
                        HR_PaySundriesModel.PayBatchID = Convert.ToInt32(sqlDataReader["PayBatchID"].ToString() ?? "0");
                        HR_PaySundriesModel.generate = Convert.ToInt32(sqlDataReader["generate"].ToString() ?? "0");
                        HR_PaySundriesModelList.Add(HR_PaySundriesModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return HR_PaySundriesModelList;
        }

        public OperationDetails SavePayrollConfirmationRequestForSundry(HR_PaySundriesModel paySundryModel, int RequestEmployeeId, int Mode)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_PayrollRequestForPaySundries", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PaySundriesID", paySundryModel.PaySundriesID);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", paySundryModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PaySundriesDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(paySundryModel.PaySundriesDate));
                sqlCommand.Parameters.AddWithValue("@HRSundryID", paySundryModel.HRSundryID);
                sqlCommand.Parameters.AddWithValue("@Amount", paySundryModel.Amount);
                sqlCommand.Parameters.AddWithValue("@AcYear", paySundryModel.AcYear);
                sqlCommand.Parameters.AddWithValue("@PayBatchID", paySundryModel.PayBatchID);
                sqlCommand.Parameters.AddWithValue("@generate", paySundryModel.generate);
                sqlCommand.Parameters.AddWithValue("@RequestEmployeeId", @RequestEmployeeId);
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                return new OperationDetails(true, "Pay sundries modification request send for confirmation", null, id);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
            }
            finally
            {

            }
        }

        public List<PaySundryType> GetPaySundriesType()
        {
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            sqlCommand = new SqlCommand("Hr_Stp_GetPaySundriesType", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;          
            List<PaySundryType> PaySundryTypesList = new List<PaySundryType>();
            try
            {
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PaySundryType paySundryType;
                    while (sqlDataReader.Read())
                    {
                        paySundryType = new PaySundryType();
                        paySundryType.PaySundryID = Convert.ToInt32(sqlDataReader["HRSundryID"].ToString());
                        paySundryType.SundryName_1 = sqlDataReader["HRSundryName_1"].ToString();
                        paySundryType.SundryName_2 = sqlDataReader["HRSundryName_2"].ToString();
                        paySundryType.SundryName_3 = sqlDataReader["HRSundryName_3"].ToString();
                        paySundryType.AccountCode = sqlDataReader["AccountCode"].ToString();
                        paySundryType.IsAccommodation =Convert.ToBoolean(sqlDataReader["IsAccommodation"].ToString());
                        paySundryType.IsFurniture = Convert.ToBoolean(sqlDataReader["IsFurniture"].ToString());
                        paySundryType.IsDeleted = Convert.ToBoolean(sqlDataReader["IsDeleted"].ToString());
                        PaySundryTypesList.Add(paySundryType);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return PaySundryTypesList;
        }

        public PaySundryType GetPaySundryTypeID(int SundryID)
        {
            SqlParameter[] parameters =
                   {
                     new SqlParameter("@PaySundryTypeID",SundryID)
                    };
            // Create a list to hold the Absent		
            PaySundryType paySundryType = new PaySundryType();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_PaySundryTypeByID", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list                       
                        while (reader.Read())
                        {
                            paySundryType = new PaySundryType();
                            paySundryType.PaySundryID = Convert.ToInt32(reader["HRSundryID"].ToString());
                            paySundryType.SundryName_1 = reader["HRSundryName_1"].ToString();
                            paySundryType.SundryName_2 = reader["HRSundryName_2"].ToString();
                            paySundryType.SundryName_3 = reader["HRSundryName_3"].ToString();
                            paySundryType.AccountCode = reader["AccountCode"].ToString();
                            paySundryType.IsAccommodation = Convert.ToBoolean(reader["IsAccommodation"].ToString());
                            paySundryType.IsFurniture = Convert.ToBoolean(reader["IsFurniture"].ToString());
                        }
                    }
                }
                return paySundryType;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdatePaySundrytypeDetails(PaySundryType objSundryType, int Mode)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_PaySundryCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);
                sqlCommand.Parameters.AddWithValue("@HRSundryID", objSundryType.PaySundryID);
                sqlCommand.Parameters.AddWithValue("@HRSundryName_1", objSundryType.SundryName_1);
                sqlCommand.Parameters.AddWithValue("@HRSundryName_2", objSundryType.SundryName_2);
                sqlCommand.Parameters.AddWithValue("@HRSundryName_3", objSundryType.SundryName_3);
                sqlCommand.Parameters.AddWithValue("@AccountCode", objSundryType.AccountCode);
                sqlCommand.Parameters.AddWithValue("@IsAccommodation", objSundryType.IsAccommodation);
                sqlCommand.Parameters.AddWithValue("@IsFurniture", objSundryType.IsFurniture);                
                SqlParameter IsSuccess = new SqlParameter("@isSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                sqlCommand.ExecuteReader();
                op.Success = Convert.ToBoolean(IsSuccess.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

        public OperationDetails CheckIfAlreadySundrySelected(int type,int SundryId)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_CheckIfAccOrFurnitureSundryPresent", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Type", type);
                sqlCommand.Parameters.AddWithValue("@SundryId", SundryId);
                SqlParameter IsSuccess = new SqlParameter("@isSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                sqlCommand.ExecuteReader();
                op.Success = Convert.ToBoolean(IsSuccess.Value);
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return op;
        }

    }
}
