﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace HRMS.DataAccess
{
    public class PASITAxDB : DBHelper
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        DataAccess.GeneralDB.DataHelper dataHelper;
        //public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PASITaxModel> GetPASITaxList()
        {

            List<PASITaxModel> PASITaxModelList = null;
            try
            {
                int decimalCount = 2;
                decimalCount = new PayrollDB().GetDigitAfterDecimal();
                PASITaxModelList = new List<PASITaxModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetTaxDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PASITaxModel PASITaxModel;
                    while (sqlDataReader.Read())
                    {
                        PASITaxModel = new PASITaxModel();
                        PASITaxModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        PASITaxModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        PASITaxModel.FullName = sqlDataReader["FullName"].ToString();
                        PASITaxModel.Amount = Math.Round(Convert.ToDecimal(sqlDataReader["Amount"].ToString()), decimalCount);
                        PASITaxModel.PASI_TAX = Math.Round(Convert.ToDecimal(sqlDataReader["TaxAmount"].ToString()), decimalCount);
                        PASITaxModel.PasiPercentage = Convert.ToDecimal(sqlDataReader["Percentage"].ToString());
                        PASITaxModelList.Add(PASITaxModel);
                    }
                }
                sqlConnection.Close();

                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0; 

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PASITaxModelList;
        }

        public List<PASITaxModel> GetJordanPASITaxList()
        {

            List<PASITaxModel> PASITaxModelList = null;
            try
            {
                int decimalCount = 2;
                decimalCount = new PayrollDB().GetDigitAfterDecimal();
                PASITaxModelList = new List<PASITaxModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetJordanTaxDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PASITaxModel PASITaxModel;
                    while (sqlDataReader.Read())
                    {
                        PASITaxModel = new PASITaxModel();
                        PASITaxModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        PASITaxModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        PASITaxModel.FullName = sqlDataReader["FullName"].ToString();
                        PASITaxModel.Amount = Math.Round(Convert.ToDecimal(sqlDataReader["Amount"].ToString()), decimalCount);
                        PASITaxModel.PASI_TAX = Math.Round(Convert.ToDecimal(sqlDataReader["TaxAmount"].ToString()), decimalCount);
                        PASITaxModel.PasiPercentage = Convert.ToDecimal(sqlDataReader["Percentage"].ToString());
                        PASITaxModelList.Add(PASITaxModel);
                    }
                }
                sqlConnection.Close();

                //totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0; 

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PASITaxModelList;
        }


        public int CheckPASIExistOnCycleID(int cycleid, int Deductiontype = 0)
        {

            SqlParameter[] parameters =
                    {
                        new SqlParameter("@cycleid",cycleid) ,
                      new SqlParameter("@Deductiontype",Deductiontype)

                    };




            try
            {
                int TotalCount = 0;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "CheckPASIExistOnCycleID"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            TotalCount = Convert.ToInt32(reader["TotalCount"].ToString());

                        }
                    }
                }
                return TotalCount;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }


        public int GetPasiDeductionTypeID(ref bool isPasi)
        {
            try
            {
                int DeductionTypeID = 0;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetPasiDeductionTypeID"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            DeductionTypeID = Convert.ToInt32(reader["DeductionTypeID"].ToString());
                            isPasi = Convert.ToBoolean(reader["IsPasi"]);
                        }
                    }
                }
                return DeductionTypeID;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }


        public OperationDetails DeletePASIOnCycleID(int cycleid, int Deductiontype = 0)
        {

            SqlParameter[] parameters =
                    {
                        new SqlParameter("@cycleid",cycleid) ,
                      new SqlParameter("@Deductiontype",Deductiontype)

                    };




            try
            {
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "DeletePASIOnCycleID", parameters);
                return new OperationDetails(true, "PASI tax deleted successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting PASI tax.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails InsertPASITAXintoPayDeduct(PASITaxModel PASITaxModel, int cycleid, int Deductiontype, string EffectiveDate, string cyclename, bool IsPasi)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode", 1),
                        new SqlParameter("@aTntDeductionTypeID", Deductiontype),
                        new SqlParameter("@aNumPaidCycle", 1),
                        new SqlParameter("@CycleID", cycleid),
                        new SqlParameter("@aBitIsInstallment", 0),
                        new SqlParameter("@aNumAmount", PASITaxModel.PASI_TAX),
                        new SqlParameter("@aNumInstallment", 1),
                        new SqlParameter("@aNvrComments", string.Format("" + (IsPasi ? "Pasi" : "Tax") + " for {0} .", cyclename)),
                        new SqlParameter("@aIntEmployeeID", PASITaxModel.EmployeeID),
                        new SqlParameter("@aNvrRefNumber", string.Format("" + (IsPasi ? "Pasi" : "Tax") + " for {0} .", cyclename)),
                        new SqlParameter("@aDttEffectiveDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate( EffectiveDate)),
                        new SqlParameter ("@aDttTransactionDate", DateTime.Now),
                        new SqlParameter ("@aIntPvId", DBNull.Value),
                        new SqlParameter ("@output", 0)
                    };

                parameters[13].Direction = ParameterDirection.Output;
                int AbsentId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_xspPayDeductionCUD ", parameters));
                return new OperationDetails(true, "Tax saved successfully.", null, AbsentId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred .", exception);
            }
            finally
            {

            }
        }


        public OperationDetails CheackPayDetailIsPresent(int cycleID, int deductionTypeID, int EmployeeID)
        {
            string query = "Select * from HR_PayDeduction where CycleID =" + cycleID + " and DeductionTypeID = " + deductionTypeID + " and EmployeeID =" + EmployeeID;
            OperationDetails details = new OperationDetails();
            try
            {
                dataHelper = new GeneralDB.DataHelper();
                DataTable table = dataHelper.ExcuteCommandText(query);


                if (table.Rows.Count > 0)
                {
                    details.Success = false;
                    details.Message = "Record is already present";
                }
                else
                {
                    details.Success = true;
                    details.Message = "Record is not present";
                }
                return details;
            }
            catch
            {
                return details;

            }
        }

        public OperationDetails UpdatePasiTax(List<PASITaxModel> List, string effectiveDate, int cycleId)
        {
            bool isPasi = false;
            int DeductionTypeid = GetPasiDeductionTypeID(ref isPasi);
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int count = 1;
            try
            {
                DataTable pasiTaxTable = new DataTable();
                pasiTaxTable.Columns.Add("EmployeeID", typeof(int));
                pasiTaxTable.Columns.Add("Amount", typeof(decimal));
                pasiTaxTable.Columns.Add("RowId", typeof(int));
                foreach (var item in List)
                {
                    pasiTaxTable.Rows.Add(item.EmployeeID, item.PASI_TAX, count++);
                }
                List<SqlParameter> parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("@PasiTaxTable", pasiTaxTable));
                parameterList.Add(new SqlParameter("@effectiveDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(effectiveDate)));
                parameterList.Add(new SqlParameter("@cycleId", cycleId));
                parameterList.Add(new SqlParameter("@deductionTypeId", DeductionTypeid));
                bool result = dataHelper.ExcutestoredProcedure(parameterList, "Hr_Stp_UpdateTax");
                oDetails.Success = true;
                oDetails.Message = "Records reposted successfully.";
                oDetails.CssClass = "success";
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "Technical error has occured";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public TaxSetting GetPasiTaxtSetting()
        {
            dataHelper = new GeneralDB.DataHelper();
            List<SqlParameter> sqlParameterList = new List<SqlParameter>();
            sqlParameterList.Add(new SqlParameter("@Mode", 1));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(sqlParameterList, "Hr_Stp_CURDTaxSetting");
            TaxSetting pasiTaxSetting = new TaxSetting();
            foreach (DataRow dr in dt.Rows)
            {
                pasiTaxSetting.TaxForAll = Convert.ToBoolean(dr["TaxForAll"]);
                pasiTaxSetting.TaxForEmployee = Convert.ToBoolean(dr["TaxForEmployee"]);
                pasiTaxSetting.TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"].ToString());
                pasiTaxSetting.EnableJordanTax = Convert.ToBoolean(dr["EnableJordanTax"]);
                pasiTaxSetting.HideTaxPercentage = Convert.ToBoolean(dr["HideTaxPercentage"] !=DBNull.Value? dr["HideTaxPercentage"] :"0");
            }
            return pasiTaxSetting;
        }
        public OperationDetails SavePasiTaxSetting(bool PasiTaxForEmployee, bool PasiTaxForAll, decimal PasiTaxPercentage)
        {
            List<SqlParameter> sqlParameterList = new List<SqlParameter>();
            sqlParameterList.Add(new SqlParameter("@TaxForEmployee", PasiTaxForEmployee));
            sqlParameterList.Add(new SqlParameter("@TaxForAll", PasiTaxForAll));
            sqlParameterList.Add(new SqlParameter("@TaxPercentage", PasiTaxPercentage));
            sqlParameterList.Add(new SqlParameter("@Mode", 2));
            dataHelper = new GeneralDB.DataHelper();
            OperationDetails operationDetails = new OperationDetails();
            if (dataHelper.ExcutestoredProcedure(sqlParameterList, "Hr_Stp_CURDTaxSetting"))
            {
                operationDetails.Success = true;
                operationDetails.Message = "Tax setting updated successfully";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return operationDetails;
        }
        public OperationDetails SavePasiTaxPerSetting(int employeeId, decimal pasiPercentage,decimal taxAmount)
        {
            List<SqlParameter> sqlParameterList = new List<SqlParameter>();
            sqlParameterList.Add(new SqlParameter("@EmployeeId", employeeId));
            sqlParameterList.Add(new SqlParameter("@Percentage", pasiPercentage));
            sqlParameterList.Add(new SqlParameter("@TaxAmount", taxAmount));
            dataHelper = new GeneralDB.DataHelper();
            OperationDetails operationDetails = new OperationDetails();
            if (dataHelper.ExcutestoredProcedure(sqlParameterList, "Hr_Stp_UpdateTaxPerEmployee"))
            {
                operationDetails.Success = true;
                operationDetails.Message = "Tax per employee setting updated successfully";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return operationDetails;
        }

    }
}
