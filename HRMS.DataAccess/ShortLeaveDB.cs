﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class ShortLeaveDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public List<ShortLeaveModel> GetShortLeaveList(int empid = 0, int UserID = 0, string FromDate = "", string ToDate = "")
        {
            List<ShortLeaveModel> ShortLeaveModelList = null;
            try
            {               
                ShortLeaveModelList = new List<ShortLeaveModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeavePaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(FromDate))
                {
                    sqlCommand.Parameters.AddWithValue("@fromDate", CommonDB.SetCulturedDate(FromDate));
                }
                if (!string.IsNullOrEmpty(ToDate))
                {
                    sqlCommand.Parameters.AddWithValue("@toDate", CommonDB.SetCulturedDate(ToDate));
                }
                if (empid != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@empid", empid);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@empid", null);
                }                                
                sqlCommand.Parameters.AddWithValue("@UserId", UserID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    ShortLeaveModel shortLeaveModel;
                    while (sqlDataReader.Read())
                    {
                        shortLeaveModel = new ShortLeaveModel();
                        shortLeaveModel.ShortLeaveID = Convert.ToInt32(sqlDataReader["ShortLeaveID"].ToString());
                        shortLeaveModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        shortLeaveModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        shortLeaveModel.LeaveDate = sqlDataReader["LeaveDate"].ToString();
                        shortLeaveModel.LeaveTime = sqlDataReader["LeaveTime"].ToString();
                        shortLeaveModel.ReturnTime = sqlDataReader["ReturnTime"].ToString();
                        shortLeaveModel.ActualLeaveTime = sqlDataReader["ActualLeaveTime"].ToString();
                        shortLeaveModel.ActualReturnTime = sqlDataReader["ActualReturnTime"].ToString();
                        shortLeaveModel.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        shortLeaveModel.EarlyMinutes = Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString());
                        shortLeaveModel.Comments = Convert.ToString(sqlDataReader["Comments"].ToString());
                        ShortLeaveModelList.Add(shortLeaveModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }          
            return ShortLeaveModelList;
        }
        public ShortLeaveModel GetShortLeaveListById(int ShortLeaveID)
        {
            ShortLeaveModel shortLeaveModel = new ShortLeaveModel();             
            try
            {
               
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetShortLeavePagingById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", ShortLeaveID);               
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();               
                if (sqlDataReader.HasRows)
                {                    
                    while (sqlDataReader.Read())
                    {                        
                        shortLeaveModel.ShortLeaveID = Convert.ToInt32(sqlDataReader["ShortLeaveID"].ToString());
                        shortLeaveModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        shortLeaveModel.EmployeeAlternativeID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        shortLeaveModel.FullName = sqlDataReader["FullName"].ToString();
                        shortLeaveModel.LeaveDate = sqlDataReader["LeaveDate"].ToString();
                        shortLeaveModel.LeaveTime = sqlDataReader["LeaveTime"].ToString();
                        shortLeaveModel.ReturnTime = sqlDataReader["ReturnTime"].ToString();
                        shortLeaveModel.ActualLeaveTime = sqlDataReader["ActualLeaveTime"].ToString();
                        shortLeaveModel.ActualReturnTime = sqlDataReader["ActualReturnTime"].ToString();
                        shortLeaveModel.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        shortLeaveModel.EarlyMinutes = Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString());
                        shortLeaveModel.Comments = Convert.ToString(sqlDataReader["Comments"].ToString());
                        shortLeaveModel.ReturnNextDay = Convert.ToBoolean(sqlDataReader["ReturnNextDay"]==DBNull.Value?0: sqlDataReader["ReturnNextDay"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return shortLeaveModel;
        }
        public OperationDetails AddShortLeave(ShortLeaveModel objShortLeaveModel)
        {
            string Message = "";
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", objShortLeaveModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@LeaveDate", DateTime.ParseExact(objShortLeaveModel.LeaveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@LeaveTime", objShortLeaveModel.LeaveTime.ToString());
                sqlCommand.Parameters.AddWithValue("@ReturnTime", objShortLeaveModel.ReturnTime);
                sqlCommand.Parameters.AddWithValue("@ActualLeaveTime", objShortLeaveModel.ActualLeaveTime);
                sqlCommand.Parameters.AddWithValue("@ActualReturnTime", objShortLeaveModel.ActualReturnTime);
                sqlCommand.Parameters.AddWithValue("@LateMinutes", objShortLeaveModel.LateMinutes);
                sqlCommand.Parameters.AddWithValue("@EarlyMinutes", objShortLeaveModel.EarlyMinutes);
                sqlCommand.Parameters.AddWithValue("@Comments", objShortLeaveModel.Comments);
                sqlCommand.Parameters.AddWithValue("@ReturnNextDay", objShortLeaveModel.ReturnNextDay);
                sqlCommand.Parameters.AddWithValue("@Operation", "Save");                        
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", objShortLeaveModel.ShortLeaveID);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave record saved successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while inserting short leave record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while inserting short leave record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public OperationDetails EditShortLeave(ShortLeaveModel objShortLeaveModel)
        {
            string Message = "";
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;                
                sqlCommand.Parameters.AddWithValue("@LeaveDate", DateTime.ParseExact(objShortLeaveModel.LeaveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@LeaveTime", objShortLeaveModel.LeaveTime);
                sqlCommand.Parameters.AddWithValue("@ReturnTime", objShortLeaveModel.ReturnTime);
                sqlCommand.Parameters.AddWithValue("@ActualLeaveTime", objShortLeaveModel.ActualLeaveTime);
                sqlCommand.Parameters.AddWithValue("@ActualReturnTime", objShortLeaveModel.ActualReturnTime);
                sqlCommand.Parameters.AddWithValue("@LateMinutes", objShortLeaveModel.LateMinutes);
                sqlCommand.Parameters.AddWithValue("@EarlyMinutes", objShortLeaveModel.EarlyMinutes);
                sqlCommand.Parameters.AddWithValue("@Operation", "Edit");
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", objShortLeaveModel.ShortLeaveID);
                sqlCommand.Parameters.AddWithValue("@Comments", objShortLeaveModel.Comments);
                sqlCommand.Parameters.AddWithValue("@ReturnNextDay", objShortLeaveModel.ReturnNextDay);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave record updated successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while updating short leave record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while updating short leave record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }
        public OperationDetails DeleteShortLeave(int id)
        {
            string Message = "";
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddShortLeaveCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;              
                sqlCommand.Parameters.AddWithValue("@Operation", "Delete");
                sqlCommand.Parameters.AddWithValue("@ShortLeaveID", id);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Short leave record deleted successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while deleting short leave record.";
                }
            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while deleting short leave record.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;


        }

        public DataSet ShortLeaveExport(int UserID, int empid = 0, string startDate = "", string endDate = "")
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_STP_GetShortLeavePaging", sqlConnection);
            da.SelectCommand.Parameters.AddWithValue("@UserId", UserID);
            if (!string.IsNullOrEmpty(startDate))
            {
                da.SelectCommand.Parameters.AddWithValue("@fromDate", CommonDB.SetCulturedDate(startDate));
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                da.SelectCommand.Parameters.AddWithValue("@toDate", CommonDB.SetCulturedDate(endDate));
            }
            if (empid != 0)
            {
                da.SelectCommand.Parameters.AddWithValue("@empid", empid);
            }
            else
            {
                da.SelectCommand.Parameters.AddWithValue("@empid", null);
            }
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            da.Fill(ds);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(1);
            ds.Tables[0].Columns["EmployeeID"].ColumnName = "Emp ID";
            ds.Tables[0].Columns["EmployeeName"].ColumnName = "Employee Name";
            ds.Tables[0].Columns["LeaveDate"].ColumnName = "Leave Date";
            ds.Tables[0].Columns["LeaveTime"].ColumnName = "Leave Time";
            ds.Tables[0].Columns["ReturnTime"].ColumnName = "Return Time";
            ds.Tables[0].Columns["ActualLeaveTime"].ColumnName = "Actual Leave Time";
            ds.Tables[0].Columns["ActualReturnTime"].ColumnName = "Actual Return Time";
            ds.Tables[0].Columns["LateMinutes"].ColumnName = "Late Minutes";
            ds.Tables[0].Columns["EarlyMinutes"].ColumnName = "Early Minutes";
            return ds;

        }

    }
}
