﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;

namespace HRMS.DataAccess
{
    public class JobCategoryDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<JobCategoryModel> GetJobCategory()
        {
            List<JobCategoryModel> JobCategoryList = null;
            try
            {
                JobCategoryList = new List<JobCategoryModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Select_Gen_JobCategoryList", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;



                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    JobCategoryModel JobCategoryModel;
                    while (sqlDataReader.Read())
                    {
                        JobCategoryModel = new JobCategoryModel();

                        JobCategoryModel.EmployeeJobCategoryID = Convert.ToInt32(sqlDataReader["EmployeeJobCategoryID"].ToString());
                        JobCategoryModel.EmployeeJobCategoryName_1 = Convert.ToString(sqlDataReader["EmployeeJobCategoryName_1"]);
                        JobCategoryModel.EmployeeJobCategoryName_2 = Convert.ToString(sqlDataReader["EmployeeJobCategoryName_2"]);
                        JobCategoryModel.EmployeeJobCategoryName_3 = Convert.ToString(sqlDataReader["EmployeeJobCategoryName_3"]);
                        JobCategoryList.Add(JobCategoryModel);
                    }
                }
                sqlConnection.Close();


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return JobCategoryList;
        }
    }
}
