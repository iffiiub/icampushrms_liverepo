﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class AbsentLateInEarlyOutDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public OperationDetails InsertAbsentLateInEarlyOut(AbsentLateInEarlyOutModel AbsentLateInEarlyOutModel)
        {
            try
            {               
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aSntEmployeeID",AbsentLateInEarlyOutModel.EmployeeID) ,
                      new SqlParameter("@aDttPayEmployeeLateDate", DateTime.ParseExact(AbsentLateInEarlyOutModel.PayEmployeeLateDate,"dd/MM/yyyy",CultureInfo.InvariantCulture)),
                      new SqlParameter("@aBitIsExcused", AbsentLateInEarlyOutModel.IsExcused),
                      new SqlParameter("@aDttInTime",Convert.ToDateTime(AbsentLateInEarlyOutModel.InTime)) ,
                      new SqlParameter("@aDttOutTime",Convert.ToDateTime(AbsentLateInEarlyOutModel.OutTime)) ,
                      new SqlParameter("@aSntLateMinutes", AbsentLateInEarlyOutModel.LateMinutes ),
                      new SqlParameter("@aSntEarlyMinutes",AbsentLateInEarlyOutModel.EarlyMinutes),
                      new SqlParameter("@aBitIsDeducted",AbsentLateInEarlyOutModel.IsDeducted),
                      new SqlParameter("@aStatusId",AbsentLateInEarlyOutModel.statusID),
                      new SqlParameter("@aIsManual",AbsentLateInEarlyOutModel.IsManual),
                      new SqlParameter("@LateReason",AbsentLateInEarlyOutModel.LateReason)
                    };
                int AbsentLateInEarlyOutId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayEmployeeLateCUD", parameters));
                return new OperationDetails(true, "Late In Early Out record saved successfully.", null, AbsentLateInEarlyOutId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Late In Early Out Record.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateAbsentLateInEarlyOut(AbsentLateInEarlyOutModel AbsentLateInEarlyOutModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aTntTranMode",2) ,
                        new SqlParameter("@aSntEmployeeID",AbsentLateInEarlyOutModel.EmployeeID) ,
                        new SqlParameter("@aIntPayEmployeeLateID",AbsentLateInEarlyOutModel.PayEmployeeLateID) ,
                        new SqlParameter("@aDttPayEmployeeLateDate",CommonDB.SetCulturedDate(AbsentLateInEarlyOutModel.PayEmployeeLateDate)),
                        new SqlParameter("@aBitIsExcused", AbsentLateInEarlyOutModel.IsExcused),
                        new SqlParameter("@aDttInTime", AbsentLateInEarlyOutModel.InTime ) ,
                        new SqlParameter("@aDttOutTime",AbsentLateInEarlyOutModel.OutTime ) ,
                        new SqlParameter("@aSntLateMinutes", AbsentLateInEarlyOutModel.LateMinutes ),
                        new SqlParameter("@aSntEarlyMinutes",AbsentLateInEarlyOutModel.EarlyMinutes),
                        new SqlParameter("@aBitIsDeducted",AbsentLateInEarlyOutModel.IsDeducted),
                        new SqlParameter("@LateReason",AbsentLateInEarlyOutModel.LateReason)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspPayEmployeeLateCUD", parameters);
                return new OperationDetails(true, "Late In Early Out record updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Late In Early Out record.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteAbsentLateInEarlyOutById(int AbsentLateInEarlyOutId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntPayEmployeeLateID",AbsentLateInEarlyOutId) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspPayEmployeeLateCUD", parameters);
                return new OperationDetails(true, "AbsentLateInEarlyOut deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting AbsentLateInEarlyOut.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AbsentLateInEarlyOutModel> GetAbsentLateInEarlyOutListByID(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntEmployeeID",EmployeeId)
                    };
            // Create a list to hold the AbsentLateInEarlyOut		
            List<AbsentLateInEarlyOutModel> AbsentLateInEarlyOutModelList = new List<AbsentLateInEarlyOutModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayEmployeeLate", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        AbsentLateInEarlyOutModel AbsentLateInEarlyOutModel;
                        while (reader.Read())
                        {
                            AbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
                            AbsentLateInEarlyOutModel.PayEmployeeLateID = Convert.ToInt32(reader["PayEmployeeLateID"].ToString());
                            AbsentLateInEarlyOutModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            AbsentLateInEarlyOutModel.PayEmployeeLateDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeLateDate"].ToString());
                            AbsentLateInEarlyOutModel.PayLateTypeID = Convert.ToInt32(reader["PayLateTypeID"].ToString());
                            AbsentLateInEarlyOutModel.IsExcused = Convert.ToBoolean(reader["IsExcused"].ToString());
                            AbsentLateInEarlyOutModel.InTime = reader["InTime"].ToString();
                            AbsentLateInEarlyOutModel.OutTime = reader["OutTime"].ToString();
                            AbsentLateInEarlyOutModel.LateMinutes = Convert.ToInt32(reader["LateMinutes"].ToString());
                            AbsentLateInEarlyOutModel.EarlyMinutes = Convert.ToInt32(reader["EarlyMinutes"].ToString());
                            AbsentLateInEarlyOutModel.IsDeducted = Convert.ToBoolean(reader["IsDeducted"].ToString());

                            AbsentLateInEarlyOutModelList.Add(AbsentLateInEarlyOutModel);
                        }
                    }
                }
                return AbsentLateInEarlyOutModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentLateInEarlyOutModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AbsentLateInEarlyOutModel> GetAbsentLateInEarlyOutList(int empid = 0, string FromDate = "", string ToDate = "")
        {
            List<AbsentLateInEarlyOutModel> AbsentLateInEarlyOutModelList = null;
            try
            {
                AbsentLateInEarlyOutModelList = new List<AbsentLateInEarlyOutModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayEmployeeLatePaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@empid", empid);
                sqlCommand.Parameters.AddWithValue("@fromDate", FromDate);
                sqlCommand.Parameters.AddWithValue("@toDate", ToDate);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AbsentLateInEarlyOutModel AbsentLateInEarlyOutModel;
                    while (sqlDataReader.Read())
                    {
                        AbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
                        AbsentLateInEarlyOutModel.PayEmployeeLateID = Convert.ToInt32(sqlDataReader["PayEmployeeLateID"].ToString());
                        AbsentLateInEarlyOutModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        AbsentLateInEarlyOutModel.PayEmployeeLateDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PayEmployeeLateDate"].ToString());
                        AbsentLateInEarlyOutModel.PayLateTypeID = Convert.ToInt32(sqlDataReader["PayLateTypeID"].ToString());
                        AbsentLateInEarlyOutModel.IsExcused = Convert.ToBoolean(sqlDataReader["IsExcused"].ToString());
                        AbsentLateInEarlyOutModel.InTime = sqlDataReader["InTime"].ToString();
                        AbsentLateInEarlyOutModel.OutTime = sqlDataReader["OutTime"].ToString();
                        AbsentLateInEarlyOutModel.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        AbsentLateInEarlyOutModel.EarlyMinutes = Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString());
                        AbsentLateInEarlyOutModel.IsDeducted = Convert.ToBoolean(sqlDataReader["IsDeducted"].ToString());
                        AbsentLateInEarlyOutModel.Status= sqlDataReader["PayLateType"].ToString();
                        AbsentLateInEarlyOutModel.LateReason = sqlDataReader["LateReason"].ToString();
                        AbsentLateInEarlyOutModelList.Add(AbsentLateInEarlyOutModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return AbsentLateInEarlyOutModelList;
        }

        public AbsentLateInEarlyOutModel AbsentLateInEarlyOutById(int AbsentLateInEarlyOutId)
        {
            try
            {
                AbsentLateInEarlyOutModel AbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
                DateTime inTime = DateTime.Now;
                DateTime outTime = DateTime.Now;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayEmployeeLateByID ", new SqlParameter("@aIntPayEmployeeLateID", AbsentLateInEarlyOutId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            AbsentLateInEarlyOutModel.PayEmployeeLateID = Convert.ToInt32(reader["PayEmployeeLateID"].ToString());
                            AbsentLateInEarlyOutModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            AbsentLateInEarlyOutModel.PayEmployeeLateDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeLateDate"].ToString());
                            AbsentLateInEarlyOutModel.PayLateTypeID = Convert.ToInt32(reader["PayLateTypeID"].ToString());
                            AbsentLateInEarlyOutModel.IsExcused = Convert.ToBoolean(reader["IsExcused"].ToString());

                            try
                            {
                                TimeSpan time = TimeSpan.Parse(reader["InTime"].ToString());
                                DateTime temptime = DateTime.Today.Add(time);
                                AbsentLateInEarlyOutModel.InTime = temptime.ToString("hh:mm tt");
                            }
                            catch
                            {
                                AbsentLateInEarlyOutModel.InTime = reader["InTime"].ToString();
                            }
                            try
                            {
                                TimeSpan time = TimeSpan.Parse(reader["OutTime"].ToString());
                                DateTime temptime = DateTime.Today.Add(time);
                                AbsentLateInEarlyOutModel.OutTime = temptime.ToString("hh:mm tt");
                            }
                            catch
                            {
                                AbsentLateInEarlyOutModel.OutTime = reader["OutTime"].ToString();
                            }

                            //AbsentLateInEarlyOutModel.InTime = time.ToString("hh:mm tt");

                            //AbsentLateInEarlyOutModel.OutTime = reader["OutTime"].ToString();
                            AbsentLateInEarlyOutModel.LateMinutes = Convert.ToInt32(reader["LateMinutes"].ToString());
                            AbsentLateInEarlyOutModel.EarlyMinutes = Convert.ToInt32(reader["EarlyMinutes"].ToString());
                            AbsentLateInEarlyOutModel.IsDeducted = Convert.ToBoolean(reader["IsDeducted"].ToString());
                            AbsentLateInEarlyOutModel.LateReason = Convert.ToString(reader["LateReason"].ToString());

                        }
                    }
                }
                return AbsentLateInEarlyOutModel;
                // Finally, we return AbsentLateInEarlyOutModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentLateInEarlyOutModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AbsentLateInEarlyOutModel> GetAbsentLateInEarlyWithEmployeeIds(string Empids, string FromDate = "", string ToDate = "")
        {
            List<AbsentLateInEarlyOutModel> AbsentLateInEarlyOutModelList = null;
            try
            {
                AbsentLateInEarlyOutModelList = new List<AbsentLateInEarlyOutModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_StpGetPayEmployeeLateWithEmpIds", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Empids", Empids);
                sqlCommand.Parameters.AddWithValue("@fromDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(FromDate));
                sqlCommand.Parameters.AddWithValue("@toDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(ToDate));
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AbsentLateInEarlyOutModel AbsentLateInEarlyOutModel;
                    while (sqlDataReader.Read())
                    {
                        AbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
                        AbsentLateInEarlyOutModel.PayEmployeeLateID = Convert.ToInt32(sqlDataReader["PayEmployeeLateID"].ToString());
                        AbsentLateInEarlyOutModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        AbsentLateInEarlyOutModel.PayEmployeeLateDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PayEmployeeLateDate"].ToString());
                        AbsentLateInEarlyOutModel.PayLateTypeID = Convert.ToInt32(sqlDataReader["PayLateTypeID"].ToString());
                        AbsentLateInEarlyOutModel.IsExcused = Convert.ToBoolean(sqlDataReader["IsExcused"].ToString());
                        AbsentLateInEarlyOutModel.InTime = sqlDataReader["InTime"].ToString();
                        AbsentLateInEarlyOutModel.OutTime = sqlDataReader["OutTime"].ToString();
                        AbsentLateInEarlyOutModel.LateMinutes = Convert.ToInt32(sqlDataReader["LateMinutes"].ToString());
                        AbsentLateInEarlyOutModel.EarlyMinutes = Convert.ToInt32(sqlDataReader["EarlyMinutes"].ToString());
                        AbsentLateInEarlyOutModel.IsDeducted = Convert.ToBoolean(sqlDataReader["IsDeducted"].ToString());
                        AbsentLateInEarlyOutModelList.Add(AbsentLateInEarlyOutModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return AbsentLateInEarlyOutModelList;
        }
    }
}
