﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;


namespace HRMS.DataAccess
{
    public class PerformanceRatingDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        DataAccess.GeneralDB.DataHelper datahelper;
        public List<PerformanceRatingModel> GetPerformanceRatingList(bool ShhowActiveOnly, int? AcadamicYearId)
        {
            List<PerformanceRatingModel> PerformanceList = new List<PerformanceRatingModel>();
            try
            {
                PerformanceRatingModel objPerformanceRating = null;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPerformanceRatingWithPaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ShowActiveOnly", ShhowActiveOnly);
                sqlCommand.Parameters.AddWithValue("@AcadamicYearId", AcadamicYearId == 0 ? null : AcadamicYearId);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objPerformanceRating = new PerformanceRatingModel();
                        objPerformanceRating.EmployeeId = Convert.ToInt32(reader["EmployeeID"].ToString());
                        objPerformanceRating.PerformanceRatingID = reader["PerformanceRatingId"].ToString() == "" ? 0 : Convert.ToInt32(reader["PerformanceRatingId"].ToString());
                        objPerformanceRating.AcademicYearId = reader["AcademicYearId"].ToString() == "" ? 0 : Convert.ToInt32(reader["AcademicYearId"].ToString());
                        objPerformanceRating.AcademicYear = reader["AcademicYear"].ToString();
                        objPerformanceRating.EmployeeAltId = reader["EmployeeAlternativeID"].ToString();
                        objPerformanceRating.EmployeeName = reader["EmployeeName"].ToString();
                        objPerformanceRating.Note = reader["Note"].ToString();
                        objPerformanceRating.Rate = reader["Rate"].ToString() == "" ? 0 : Convert.ToDecimal(reader["Rate"].ToString());
                        PerformanceList.Add(objPerformanceRating);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {

            }
            return PerformanceList;
        }

        public OperationDetails AddUpdatePerformanceRate(int PerformanceRateId, int EmployeeID, decimal rate, int AcYearID, string Note, int tranMode)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddUpdateEmployeePerformanceRate", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aTntTranMode", tranMode);
                sqlCommand.Parameters.AddWithValue("@Rate", rate);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@PerformanceRateId", PerformanceRateId);
                sqlCommand.Parameters.AddWithValue("@AcademicYearId", AcYearID);
                sqlCommand.Parameters.AddWithValue("@Note", Note);
                SqlParameter InsertionID = new SqlParameter("@insertionid", SqlDbType.Int);
                InsertionID.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(InsertionID);
                SqlParameter OpMessage = new SqlParameter("@OutMessage", SqlDbType.NVarChar);
                OpMessage.Size = 400;
                OpMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OpMessage);

                SqlParameter Opsuccess = new SqlParameter("@OpSuccess", SqlDbType.Bit);
                Opsuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Opsuccess);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                affectedId = InsertionID.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(InsertionID.Value.ToString()) : 0;
                OperationMessage = OpMessage.Value.ToString();
                success = Opsuccess.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(Opsuccess.Value.ToString()) : false;

                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while inserting Academic.", exception);
                //throw;
            }
            finally
            {
                sqlConnection.Close();
            }
            return op;

        }

        public OperationDetails CheckEmployeeExistence(string EmployeeIDs, int AcYearID)
        {
            int affectedId = 0;
            string OperationMessage = "";
            bool success = false;
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_CheckEmployeeExistInPerformanceRating", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeIDs", EmployeeIDs);
                sqlCommand.Parameters.AddWithValue("@AcademicYearId", AcYearID);

                SqlParameter isEmployeePresent = new SqlParameter("@isEmployeePresent", SqlDbType.Bit);

                isEmployeePresent.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(isEmployeePresent);

                SqlDataReader reader = sqlCommand.ExecuteReader();

                success = isEmployeePresent.Value.ToString() != DBNull.Value.ToString() ? Convert.ToBoolean(isEmployeePresent.Value.ToString()) : false;

                if (success)
                {
                    op.CssClass = "success";
                }
                else
                {
                    op.CssClass = "error";
                }
                op.Success = success;
                op.Message = OperationMessage;
                op.InsertedRowId = affectedId;
            }
            catch (Exception exception)
            {
                op = new OperationDetails(false, "Error : while cheking existence", exception);
                //throw;
            }
            finally
            {
                sqlConnection.Close();
            }
            return op;

        }

        public PerformanceRatingModel GetPerformanceRatingById(int id)
        {
            PerformanceRatingModel PerformanceRating = new PerformanceRatingModel();
            return PerformanceRating;
        }
    }
}
