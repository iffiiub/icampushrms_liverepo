﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class PayCycleDB
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public List<PayCycleModel> GetAllPayCycle()
        {
            List<PayCycleModel> list = new List<PayCycleModel>();
            PayCycleModel PayCycleModel = new PayCycleModel();

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetAllPayCycles", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        PayCycleModel = new PayCycleModel();

                        PayCycleModel.PayCycleID = Convert.ToInt32(sqlDataReader["PayCycleID"].ToString());
                        PayCycleModel.PayCycleName_1 = Convert.ToString(sqlDataReader["PayCycleName_1"]);
                        PayCycleModel.DateFrom = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateFrom"].ToString());
                        PayCycleModel.DateTo = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateTo"].ToString());
                        PayCycleModel.active = Convert.ToBoolean(sqlDataReader["Active"]);
                        list.Add(PayCycleModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return list;
        }

        public PayCycleModel GetPayCycleById(int cycleId)
        {
            PayCycleModel PayCycleModel = new PayCycleModel();
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_GetPayCycleById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@CycleId", cycleId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        PayCycleModel = new PayCycleModel();

                        PayCycleModel.PayCycleID = Convert.ToInt32(sqlDataReader["PayCycleID"].ToString());
                        PayCycleModel.PayCycleName_1 = Convert.ToString(sqlDataReader["PayCycleName_1"]);
                        PayCycleModel.DateFrom = Convert.ToString(sqlDataReader["DateFrom"]);
                        PayCycleModel.DateTo = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateTo"].ToString());
                        PayCycleModel.DateFrom = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DateFrom"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return PayCycleModel;
        }
    }
}
