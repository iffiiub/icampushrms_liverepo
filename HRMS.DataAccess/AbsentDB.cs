﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using Microsoft.ApplicationBlocks.Data;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class AbsentDB : DBHelper
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        //public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails InsertAbsent(AbsentModel AbsentModel)
        {
            try
            {
                SqlParameter InTime, OutTime;
                if (AbsentModel.StatusID == 2)
                {
                    InTime = new SqlParameter("@SignIn", "00:00");
                    OutTime = new SqlParameter("@SignOut", "00:00");
                }
                else
                {
                    if (AbsentModel.StatusID == 9 || AbsentModel.StatusID == 12)
                    {
                        InTime = new SqlParameter("@SignIn", Convert.ToDateTime(AbsentModel.InTime).ToString("HH:mm"));
                        OutTime = new SqlParameter("@SignOut", "00:00");
                    }
                    else
                    {
                        InTime = new SqlParameter("@SignIn", "00:00");
                        OutTime = new SqlParameter("@SignOut", Convert.ToDateTime(AbsentModel.OutTime).ToString("HH:mm"));
                    }
                }
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aIntPayEmployeeAbsentHeaderID",AbsentModel.PayEmployeeAbsentHeaderID) ,
                      new SqlParameter("@aSntEmployeeID", AbsentModel.EmployeeID) ,
                      new SqlParameter("@aDttPayEmployeeAbsentFromDate",CommonDB.SetCulturedDate(AbsentModel.PayEmployeeAbsentFromDate)),
                      new SqlParameter("@aDttPayEmployeeAbsentToDate",CommonDB.SetCulturedDate(AbsentModel.PayEmployeeAbsentToDate)),
                      new SqlParameter("@aSntPayAbsentTypeID", AbsentModel.PayAbsentTypeID),
                      new SqlParameter("@aBitIsMedicalCertificateProvided",AbsentModel.IsMedicalCertificateProvided ) ,
                      new SqlParameter("@aBitIsDeducted", AbsentModel.IsDeducted ),
                      InTime,
                      OutTime,
                      new SqlParameter("@aStatusID",AbsentModel.StatusID ),
                      new SqlParameter("@Comments", AbsentModel.Comments ),
                    };
                int AbsentId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayEmployeeAbsentCUD", parameters));
                return new OperationDetails(true, "Absent saved successfully.", null, AbsentId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Absent.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails AddMedCertificateStatus(int MedCertId, bool IsMedCert)
        {

            string Message = "";
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_AddMedCertificateStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MedCertId", MedCertId);
                sqlCommand.Parameters.AddWithValue("@IsMedCert", IsMedCert);
                SqlParameter OperationId = new SqlParameter("@output", SqlDbType.Bit);
                OperationId.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                op.Success = Convert.ToBoolean(OperationId.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Employee medical certificate status is updated successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while confirming employee medical certificate status.";
                }


            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while confirming employee medical certificate status.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }
        public OperationDetails InsertAbsentHeader(AbsentModel AbsentModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aTntTranMode",1) ,
                      new SqlParameter("@aIntPayEmployeeAbsentHeaderID",AbsentModel.PayEmployeeAbsentHeaderID) ,
                      new SqlParameter("@aSntEmployeeID", AbsentModel.EmployeeID) ,
                      new SqlParameter("@aSntPayAbsentTypeID", AbsentModel.PayAbsentTypeID),
                      new SqlParameter("@aDttEntryDate", CommonDB.SetCulturedDate(AbsentModel.EntryDate) ) ,
                      new SqlParameter("@aBitActive",AbsentModel.isActive ) ,
                    };
                int AbsentHeaderId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspPayEmployeeAbsentHeaderCUD  ", parameters));
                return new OperationDetails(true, "Absent Header saved successfully.", null, AbsentHeaderId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Absent Header.", exception);
                //throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateAbsent(AbsentModel AbsentModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aTntTranMode",2) ,
                      new SqlParameter("@aIntPayEmployeeAbsentID",AbsentModel.PayEmployeeAbsentID) ,
                      new SqlParameter("@aIntPayEmployeeAbsentHeaderID",AbsentModel.PayEmployeeAbsentHeaderID) ,
                      new SqlParameter("@aSntEmployeeID", AbsentModel.EmployeeID) ,
                      new SqlParameter("@aDttPayEmployeeAbsentFromDate", CommonDB.SetCulturedDate(AbsentModel.PayEmployeeAbsentFromDate)) ,
                      new SqlParameter("@aDttPayEmployeeAbsentToDate", CommonDB.SetCulturedDate(AbsentModel.PayEmployeeAbsentToDate)) ,
                      new SqlParameter("@aSntPayAbsentTypeID", AbsentModel.PayAbsentTypeID),
                      new SqlParameter("@aBitIsMedicalCertificateProvided",AbsentModel.IsMedicalCertificateProvided ) ,
                      new SqlParameter("@aBitIsDeducted", AbsentModel.IsDeducted ),
                      new SqlParameter("@Comments", AbsentModel.Comments )
                     // new SqlParameter("@SignIn",AbsentModel.InTime),
                    //  new SqlParameter("@SignOut",AbsentModel.OutTime)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "HR_uspPayEmployeeAbsentCUD", parameters);
                return new OperationDetails(true, "Absent updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Absent.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateLeavetypeAbsent(int AbsentPaidTypeID, int PayEmployeeAbsentID)
        {
            try
            {
                SqlParameter[] parameters =
                    {

                         new SqlParameter("@AbsentPaidTypeID",AbsentPaidTypeID) ,
                      new SqlParameter("@PayEmployeeAbsentID",PayEmployeeAbsentID)

                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_UpdateEmployeeAbsentLeaveType", parameters);
                return new OperationDetails(true, "Leave Type updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Leave Type.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteAbsentById(int AbsentId)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@aIntPayEmployeeAbsentID",AbsentId) ,
                      new SqlParameter("@aTntTranMode",3)
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "HR_uspPayEmployeeAbsentCUD", parameters);
                return new OperationDetails(true, "Absent deleted successfully", null);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Absent.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails DeleteMultipleAbsentIds(string PayEmployeeAbsentIds)
        {
            string Message = "";
            OperationDetails op = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_DeleteMultiplePayEmployeeAbsent", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayEmployeeAbsentIds", PayEmployeeAbsentIds);
                SqlParameter IsSuccess = new SqlParameter("@Success", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                op.Success = Convert.ToBoolean(IsSuccess.Value);
                if (op.Success)
                {
                    op.CssClass = "success";
                    op.Message = "Selected absent record(s) delete successfully.";
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "Error : while deleting selectes Absent record(s)";
                }


            }
            catch (Exception exception)
            {
                op.Success = false;
                op.Message = "Error : while confirming employee medical certificate status.";
                op.CssClass = "error";
                op.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return op;
        }

        public List<AbsentModel> GetAbsentList(int EmployeeId)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntEmployeeID",EmployeeId)
                    };
            // Create a list to hold the Absent		
            List<AbsentModel> AbsentModelList = new List<AbsentModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayEmployeeAbsent", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        AbsentModel AbsentModel;
                        while (reader.Read())
                        {
                            AbsentModel = new AbsentModel();
                            AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"].ToString());
                            AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(reader["PayEmployeeAbsentHeaderID"].ToString());
                            AbsentModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());

                            /*
                            *These line is comment becuse sp not contain column "PayEmployeeAbsentFromDate", "PayEmployeeAbsentToDate"
                            *Sp Contains column 
                            */



                            //AbsentModel.PayEmployeeAbsentFromDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentFromDate"].ToString());
                            //AbsentModel.PayEmployeeAbsentToDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentToDate"].ToString());

                            AbsentModel.PayEmployeeAbsentFromDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentDate"].ToString());

                            //AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
                            AbsentModel.AbsentPaidTypeID = Convert.ToInt32(reader["AbsentPaidTypeID"].ToString());
                            AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(reader["IsMedicalCertificateProvided"].ToString());
                            AbsentModel.IsDeducted = Convert.ToBoolean(reader["IsDeducted"].ToString());

                            AbsentModelList.Add(AbsentModel);
                        }
                    }
                }
                return AbsentModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AbsentModel> GetAbsentListWithinDates(int EmployeeId, DateTime absentDate)
        {
            SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntEmployeeID",EmployeeId),
                        new SqlParameter("@absentDate",absentDate)
                    };
            // Create a list to hold the Absent		
            List<AbsentModel> AbsentModelList = new List<AbsentModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_Stp_GetPayEmployeeAbsentByDate", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new PassportModel, and insert them into our list
                        AbsentModel AbsentModel;
                        while (reader.Read())
                        {
                            AbsentModel = new AbsentModel();
                            AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"].ToString());
                            AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(reader["PayEmployeeAbsentHeaderID"].ToString());
                            AbsentModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());

                            /*
                            *These line is comment becuse sp not contain column "PayEmployeeAbsentFromDate", "PayEmployeeAbsentToDate"
                            *Sp Contains column 
                            */



                            //AbsentModel.PayEmployeeAbsentFromDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentFromDate"].ToString());
                            //AbsentModel.PayEmployeeAbsentToDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentToDate"].ToString());

                            AbsentModel.PayEmployeeAbsentFromDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentDate"].ToString());

                            //AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
                            AbsentModel.AbsentPaidTypeID = Convert.ToInt32(reader["AbsentPaidTypeID"].ToString());
                            AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(reader["IsMedicalCertificateProvided"].ToString());
                            AbsentModel.IsDeducted = Convert.ToBoolean(reader["IsDeducted"].ToString());

                            AbsentModelList.Add(AbsentModel);
                        }
                    }
                }
                return AbsentModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<AbsentModel> GetAbsentList(string startDate, string endDate, int UserID, int empid = 0, int deptID = 0, int sectionID = 0)
        {
            List<AbsentModel> AbsentModelList = null;
            try
            {
                AbsentModelList = new List<AbsentModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayEmployeeAbsentPaging", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", UserID);
                if (!string.IsNullOrEmpty(startDate))
                {
                    sqlCommand.Parameters.AddWithValue("@StartDate", DateTime.ParseExact(startDate.Replace("-","/"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    sqlCommand.Parameters.AddWithValue("@EndDate", DateTime.ParseExact(endDate.Replace("-", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                }
                if (empid != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@empid", empid);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@empid", null);
                }
                if (deptID != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@DeptID", deptID);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@DeptID", null);
                }
                if (sectionID != 0)
                {
                    sqlCommand.Parameters.AddWithValue("@SectionID", sectionID);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@SectionID", null);
                }
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    AbsentModel AbsentModel;
                    while (sqlDataReader.Read())
                    {
                        AbsentModel = new AbsentModel();
                        AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentID"].ToString());
                        AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(sqlDataReader["PayEmployeeAbsentHeaderID"].ToString());
                        AbsentModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        AbsentModel.EmployeeAltID = Convert.ToString(sqlDataReader["EmpAltID"].ToString());
                        AbsentModel.PayEmployeeAbsentFromDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PayEmployeeAbsentFromDate"].ToString());
                        AbsentModel.PayEmployeeAbsentToDate = CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["PayEmployeeAbsentToDate"].ToString());
                        AbsentModel.PayAbsentTypeID = Convert.ToInt32(sqlDataReader["PayAbsentTypeID"].ToString());
                        // AbsentModel.PayAbsentTypeName = sqlDataReader["PayAbsentTypeName"].ToString();
                        AbsentModel.AbsentPaidTypeID = Convert.ToInt32(sqlDataReader["AbsentPaidTypeID"].ToString());
                        AbsentModel.AbsentPaidTypeName = sqlDataReader["AbsentPaidTypeName"].ToString();
                        AbsentModel.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(sqlDataReader["IsMedicalCertificateProvided"].ToString());
                        AbsentModel.IsDeducted = Convert.ToBoolean(sqlDataReader["IsDeducted"].ToString());
                        AbsentModel.AbsenceReason = sqlDataReader["AbsenceReason"].ToString();
                        AbsentModel.IsConfirmed = Convert.ToBoolean(sqlDataReader["IsConfirmed"].ToString());
                        AbsentModelList.Add(AbsentModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return AbsentModelList;
        }

        //public List<AbsentModel> GetAbsentList(int pageNumber, int numberOfRecords, string sortColumn, string sortOrder)
        //{





        //    SqlParameter[] parameters =
        //            {    


        //                new SqlParameter("@OffsetRows", pageNumber),
        // new SqlParameter("@FetchRows", numberOfRecords),
        //     new SqlParameter("@SortColumn", sortColumn),
        //    new SqlParameter("@SortOrder", sortOrder)



        //};



        //    // Create a list to hold the Absent		
        //    List<AbsentModel> AbsentModelList = new List<AbsentModel>();
        //    try
        //    {
        //        using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayEmployeeAbsentPaging", parameters))
        //        {
        //            // Check if the reader returned any rows
        //            if (reader.HasRows)
        //            {
        //                // While the reader has rows we loop through them,
        //                // create new PassportModel, and insert them into our list
        //                AbsentModel AbsentModel;
        //                while (reader.Read())
        //                {
        //                    AbsentModel = new AbsentModel();
        //                    AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"].ToString());
        //                    AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(reader["PayEmployeeAbsentHeaderID"].ToString());
        //                    AbsentModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
        //                    AbsentModel.PayEmployeeAbsentDate = reader["PayEmployeeAbsentDate"].ToString();
        //                    AbsentModel.PayAbsentTypeID = Convert.ToInt32(reader["PayAbsentTypeID"].ToString());
        //                    AbsentModel.PayAbsentTypeName = reader["PayAbsentTypeName"].ToString();
        //                    AbsentModel.LeaveTypeID = Convert.ToInt32(reader["LeaveTypeID"].ToString());
        //                    AbsentModel.LeaveTypeName = reader["LeaveTypeName"].ToString();
        //                    AbsentModel.EmployeeName = reader["EmployeeName"].ToString();
        //                    AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(reader["IsMedicalCertificateProvided"].ToString());
        //                    AbsentModel.IsDeducted = Convert.ToBoolean(reader["IsDeducted"].ToString());

        //                    AbsentModelList.Add(AbsentModel);
        //                }
        //            }
        //        }

        //       // totalCount = TotalCount.Value.ToString() != DBNull.Value.ToString() ? Convert.ToInt32(TotalCount.Value.ToString()) : 0;
        //        return AbsentModelList;
        //    }
        //    catch (Exception exception)
        //    {
        //        // return new OperationDetails(false, "Error : while fetching AbsentModelList.", exception);
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        public int GetAbsentType()
        {
            try
            {
                int AbsentTypeID = 0;
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "GetAttendanceType"))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            AbsentTypeID = Convert.ToInt32(reader["AbsentTypeID"].ToString());

                        }
                    }
                }
                return AbsentTypeID;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }


        public int CheckEmployeeAbsentByDate(int EmployeeID, string AbsentDate)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeID) ,
                      new SqlParameter("@aDttPayEmployeeAbsentDate",CommonDB.SetCulturedDate(AbsentDate))

                    };
                int Count = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspCheckPayEmployeeAbsentExistdByDate  ", parameters));
                return Count;
            }
            catch (Exception exception)
            {
                return 0;
                //throw;
            }
            finally
            {

            }
        }

        public int CheckEmployeeLateByDate(int EmployeeID, string LateDate)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@aSntTeacherID",EmployeeID) ,
                      new SqlParameter("@aDttPayEmployeeLateDate",CommonDB.SetCulturedDate(LateDate))

                    };
                int Count = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "HR_uspCheckPayEmployeeLateExistdByDate  ", parameters));
                return Count;
            }
            catch (Exception exception)
            {
                return 0;
                //throw;
            }
            finally
            {

            }
        }

        public AbsentModel AbsentById(int AbsentId)
        {
            try
            {
                AbsentModel AbsentModel = new AbsentModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "HR_uspGetPayEmployeeAbsentByID", new SqlParameter("@aIntPayEmployeeAbsentID", AbsentId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AbsentModel.PayEmployeeAbsentID = Convert.ToInt32(reader["PayEmployeeAbsentID"].ToString());
                            AbsentModel.PayEmployeeAbsentHeaderID = Convert.ToInt32(reader["PayEmployeeAbsentHeaderID"].ToString());
                            AbsentModel.EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString());
                            AbsentModel.PayEmployeeAbsentFromDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentFromDate"].ToString());
                            AbsentModel.PayEmployeeAbsentToDate = CommonDB.GetFormattedDate_DDMMYYYY(reader["PayEmployeeAbsentToDate"].ToString());
                            AbsentModel.AbsentPaidTypeID = Convert.ToInt32(reader["AbsentPaidTypeID"].ToString());
                            AbsentModel.IsMedicalCertificateProvided = Convert.ToBoolean(reader["IsMedicalCertificateProvided"].ToString());
                            AbsentModel.IsDeducted = Convert.ToBoolean(reader["IsDeducted"].ToString());
                            AbsentModel.PayAbsentTypeID = (reader["PayAbsentTypeID"].ToString() == "" ? (Convert.ToInt32(reader["PayAbsentTypeID"].ToString())) : (Convert.ToInt32(reader["PayAbsentTypeID"].ToString())));
                            AbsentModel.Comments = reader["Comments"].ToString();
                        }
                    }
                }
                return AbsentModel;
                // Finally, we return AbsentModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching AbsentModel.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateAbsentType(int AbsentTypeID, int PayEmployeeAbsentID)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@AbsentTypeID",AbsentTypeID) ,
                      new SqlParameter("@PayEmployeeAbsentID",PayEmployeeAbsentID)

                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_UpdateEmployeeAbsentType", parameters);
                return new OperationDetails(true, "Leave Type updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Leave Type.", exception);
                throw;
            }
            finally
            {

            }
        }

        public OperationDetails UpdateMultipleAbsentType(string payEmployeeAbsentID, int absentTypeID)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@AbsentTypeID",absentTypeID) ,
                      new SqlParameter("@PayEmployeeAbsentIDs",payEmployeeAbsentID)

                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "Hr_stp_UpdateMultipleEmployeeAbsentType", parameters);
                op.Success = true;
                op.CssClass = "success";
                op.Message = "Absent Type updated successfully";
                return op;
            }
            catch (Exception ex)
            {
                op.Success = false;
                op.Message = "Error : while updating Absent Type.";
                op.CssClass = "error";
                op.Exception = ex;
                return op;
            }
            finally
            {

            }
        }

        //***************************************** Export to excel ***********************************************************************************
        public DataSet GetEmployeeDataSet(int UserID, int empid = 0, string startDate = "", string endDate = "")
        {
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_STP_GetPayEmployeeAbsentExport", sqlConnection);
            da.SelectCommand.Parameters.AddWithValue("@UserId", UserID);
            if (!string.IsNullOrEmpty(startDate))
            {
                da.SelectCommand.Parameters.AddWithValue("@StartDate", DateTime.ParseExact(startDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                da.SelectCommand.Parameters.AddWithValue("@EndDate", DateTime.ParseExact(endDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            if (empid != 0)
            {
                da.SelectCommand.Parameters.AddWithValue("@empid", empid);
            }
            else
            {
                da.SelectCommand.Parameters.AddWithValue("@empid", null);
            }
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }


    }
}
