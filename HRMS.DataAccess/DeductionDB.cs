﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Configuration;
using System.Data;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class DeductionDB
    {

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        GeneralDB.DataHelper dataHelper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<PayDeductionModel> GetAllPayDeductionList(int EmployeeId)
        {
            List<PayDeductionModel> payDeductionModelList = null;
            try
            {
                payDeductionModelList = new List<PayDeductionModel>();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDeductionNew", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aIntEmployeeID", EmployeeId);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayDeductionModel payDeductionModel;
                    while (sqlDataReader.Read())
                    {
                        payDeductionModel = new PayDeductionModel();
                        payDeductionModel.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        payDeductionModel.DeductionTypeID = Convert.ToInt32(sqlDataReader["DeductionTypeID"].ToString());
                        payDeductionModel.EffectiveDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EffectiveDate"].ToString());
                        payDeductionModel.PaidCycle = Convert.ToInt32(sqlDataReader["PaidCycle"]);
                        payDeductionModel.IsInstallment = Convert.ToBoolean(sqlDataReader["IsInstallment"]);
                        payDeductionModel.Amount = Convert.ToDouble(sqlDataReader["Amount"]);
                        payDeductionModel.Installment = Convert.ToDouble(sqlDataReader["Installment"].ToString());
                        payDeductionModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        payDeductionModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        payDeductionModel.RefNumber = Convert.ToString(sqlDataReader["RefNumber"]);
                        payDeductionModel.TransactionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["TransactionDate"].ToString());
                        payDeductionModel.PvId = Convert.ToInt32(sqlDataReader["PvId"]);
                        payDeductionModel.GenerateDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["GenerateDate"].ToString());
                        payDeductionModel.DeductionType = Convert.ToString(sqlDataReader["DeductionTypeName"]);
                        payDeductionModel.AmountLeft = Convert.ToDouble(sqlDataReader["AmountLeft"]);
                        payDeductionModel.IsProcessed = CheckIfDeductionIsProcessed(payDeductionModel.PayDeductionID);
                        payDeductionModel.IsFamilyBalanceDeduction = Convert.ToBoolean(sqlDataReader["IsFamilyBalanceDeduction"]);
                        payDeductionModelList.Add(payDeductionModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDeductionModelList;
        }

        public DataSet GetAllPayDeductionDataset(int EmployeeId)
        {
            sqlConnection = new SqlConnection(connectionString);
            DataTable dt = new DataTable();
            string query = "SELECT ED.FirstName_1 + ' ' + ED.Surname_1 as [Employee Name],CONVERT(VARCHAR(10), PD.EffectiveDate, 101)  AS [Effective Date], " +
                           "  CASE WHEN PD.IsInstallment=0  THEN 'No' WHEN PD.IsInstallment=1 THEN 'Yes' END As [Is Installment],IsNull (PD.Amount, 0) As Amount,IsNull (PD.Installment, 0) As Installment, " +
                           "PD.PaidCycle As [Paid Cycle],IsNull (PD.RefNumber, '') As [Ref. Number]," +
                           "PDT.DeductionTypeName_1 AS [Deduction Type Name]" +
                           "FROM  HR_PayDeduction  PD  inner join GEN_PayDeductionType PDT on PD.DeductionTypeID = PDT.DeductionTypeID " +
                           "inner join HR_EmployeeDetail ED on ed.EmployeeId = PD.EmployeeId   WHERE PD.EmployeeID = @aIntEmployeeID " +
                           "ORDER BY PD.EffectiveDate DESC";
            SqlDataAdapter da = new SqlDataAdapter(query, sqlConnection);

            da.SelectCommand.Parameters.AddWithValue("@aIntEmployeeID", EmployeeId);



            da.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;
        }
        public List<PayDeductionTypeModel> GetAllPayDeductionType()
        {
            List<PayDeductionTypeModel> payDeductionTypeModelList = null;
            try
            {
                payDeductionTypeModelList = new List<PayDeductionTypeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDeductionType", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayDeductionTypeModel payDeductionTypeModel;
                    while (sqlDataReader.Read())
                    {
                        payDeductionTypeModel = new PayDeductionTypeModel();

                        payDeductionTypeModel.DeductionTypeID = Convert.ToInt32(sqlDataReader["DeductionTypeID"].ToString());
                        payDeductionTypeModel.DeductionTypeName_1 = Convert.ToString(sqlDataReader["DeductionTypeName_1"].ToString());
                        payDeductionTypeModel.DeductionTypeName_2 = Convert.ToString(sqlDataReader["DeductionTypeName_2"]);
                        payDeductionTypeModel.DeductionTypeName_3 = Convert.ToString(sqlDataReader["DeductionTypeName_3"]);
                        payDeductionTypeModel.DeductionTypeShortName_1 = Convert.ToString(sqlDataReader["DeductionTypeShortName_1"]);
                        payDeductionTypeModel.DeductionTypeShortName_2 = Convert.ToString(sqlDataReader["DeductionTypeShortName_2"]);
                        payDeductionTypeModel.DeductionTypeShortName_3 = Convert.ToString(sqlDataReader["DeductionTypeShortName_3"]);
                        //payDeductionTypeModel.SortOrder = sqlReader["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(sqlReader["SortOrder"].ToString());
                        payDeductionTypeModel.TransactionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["TransactionDate"].ToString());
                        payDeductionTypeModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        //payDeductionTypeModel.Sequence = sqlReader["Sequence"] == DBNull.Value ? 0 : Convert.ToInt32(sqlReader["Sequence"].ToString()); 
                        payDeductionTypeModel.acc_code = Convert.ToString(sqlDataReader["acc_code"]);
                        //payDeductionTypeModel.IsExcludeInJV = Convert.ToBoolean(sqlDataReader["IsExcludeInJV"]);
                        payDeductionTypeModel.IsFamilyBalanceDeduction = Convert.ToBoolean(sqlDataReader["FamilyBalanceDeduction"].ToString() == "" ? "false" : sqlDataReader["FamilyBalanceDeduction"].ToString());
                        payDeductionTypeModelList.Add(payDeductionTypeModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDeductionTypeModelList;
        }

        /// <summary>
        /// To get the deduction type list for pagination
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<PayDeductionTypeModel> GetPayDeductionTypeListBySearch()
        {
            List<PayDeductionTypeModel> payDeductionTypeModelList = null;
            try
            {
                payDeductionTypeModelList = new List<PayDeductionTypeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetPayDeductionTypeListBySearch", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    PayDeductionTypeModel payDeductionTypeModel;
                    while (sqlDataReader.Read())
                    {
                        payDeductionTypeModel = new PayDeductionTypeModel();

                        payDeductionTypeModel.DeductionTypeID = Convert.ToInt32(sqlDataReader["DeductionTypeID"].ToString());
                        payDeductionTypeModel.DeductionTypeName_1 = Convert.ToString(sqlDataReader["DeductionTypeName_1"]).Trim();
                        payDeductionTypeModel.DeductionTypeName_2 = Convert.ToString(sqlDataReader["DeductionTypeName_2"]).Trim();
                        payDeductionTypeModel.DeductionTypeName_3 = Convert.ToString(sqlDataReader["DeductionTypeName_3"]).Trim();
                        payDeductionTypeModel.DeductionTypeShortName_1 = Convert.ToString(sqlDataReader["DeductionTypeShortName_1"]).Trim();
                        payDeductionTypeModel.DeductionTypeShortName_2 = Convert.ToString(sqlDataReader["DeductionTypeShortName_2"]).Trim();
                        payDeductionTypeModel.DeductionTypeShortName_3 = Convert.ToString(sqlDataReader["DeductionTypeShortName_3"]).Trim();
                        payDeductionTypeModel.TransactionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["TransactionDate"].ToString());
                        payDeductionTypeModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        payDeductionTypeModel.Sequence = sqlDataReader["Sequence"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        payDeductionTypeModel.acc_code = Convert.ToString(sqlDataReader["acc_code"]).Trim();
                        //payDeductionTypeModel.IsExcludeInJV = Convert.ToBoolean(sqlDataReader["IsExcludeInJV"]);
                        payDeductionTypeModelList.Add(payDeductionTypeModel);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDeductionTypeModelList;
        }


        //List<PayDeductionModel> payDeductionModelList
        public OperationDetails PayDeductionCrud(PayDeductionModel payDeductionModel, int operationId)
        {
            OperationDetails operationDetails = new OperationDetails();
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayDeductionCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationId);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", payDeductionModel.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@aTntDeductionTypeID", payDeductionModel.DeductionTypeID);
                sqlCommand.Parameters.AddWithValue("@aDttEffectiveDate", GeneralDB.CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate));
                sqlCommand.Parameters.AddWithValue("@aNumPaidCycle", payDeductionModel.PaidCycle);
                sqlCommand.Parameters.AddWithValue("@aBitIsInstallment", payDeductionModel.IsInstallment);
                sqlCommand.Parameters.AddWithValue("@aNumAmount", payDeductionModel.Amount);
                sqlCommand.Parameters.AddWithValue("@aNvrComments", payDeductionModel.Comments);
                sqlCommand.Parameters.AddWithValue("@aIntEmployeeID", payDeductionModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aNvrRefNumber", payDeductionModel.RefNumber);

                sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", GeneralDB.CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate));
                sqlCommand.Parameters.AddWithValue("@aIntPvId", payDeductionModel.PvId);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                id = Convert.ToInt32(output.Value);
                //}
                sqlConnection.Close();

                operationDetails.Success = true;
                operationDetails.InsertedRowId = id;
                if (payDeductionModel.PayDeductionID == 0)
                    operationDetails.Message = "Pay Deduction Added Successfully.";
                else
                    operationDetails.Message = "Pay Deduction Updated Successfully.";


            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
                operationDetails.InsertedRowId = id;
                operationDetails.Exception = ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public DataTable GetEmployeeFullSalary(int EmployeeId, int mode)
        {
            SqlConnection objConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString);
            DataTable dt = new DataTable();
            try
            {

                //objConnection.Close();
                objConnection.Open();
                SqlCommand objCommand = new SqlCommand("HR_uspEmployeeFullSalary", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.Parameters.AddWithValue("@aSntEmployeeID", EmployeeId);
                objCommand.Parameters.AddWithValue("@mode", mode);
                SqlDataAdapter dap = new SqlDataAdapter(objCommand);
                dap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                objConnection.Close();
                ex.Message.ToString();
                return dt;
            }
            finally
            {
                objConnection.Close();
            }
        }

        public OperationDetails PayDeductionDetailsCrud(PayDeductionDetailModel payDeductionDetailModel, int operationId)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_xspPayDeductionDetailCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //foreach(PayDeductionModel payDeductionModel in payDeductionModelList)
                //{
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationId);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionDetailID", payDeductionDetailModel.PayDeductionDetailID);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", payDeductionDetailModel.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@aDttDeductionDate", GeneralDB.CommonDB.SetCulturedDate(payDeductionDetailModel.DeductionDate));
                sqlCommand.Parameters.AddWithValue("@aNumAmount", payDeductionDetailModel.Amount);
                sqlCommand.Parameters.AddWithValue("@aNvrComments", payDeductionDetailModel.Comments);
                sqlCommand.Parameters.AddWithValue("@aBitIsActive", payDeductionDetailModel.IsActive);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                id = Convert.ToInt32(output.Value);
                //}
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public OperationDetails StudentFOSTBDetailsCrud(StudentFOSTBDetails studentFOSTBDetails,int mode)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_StudentFOSTBDetailsCrud", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;               
                sqlCommand.Parameters.AddWithValue("@PayDeductionID", studentFOSTBDetails.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@StudentID", studentFOSTBDetails.StudentID);
                sqlCommand.Parameters.AddWithValue("@DeductionStdID", studentFOSTBDetails.DeductionStdID);
                sqlCommand.Parameters.AddWithValue("@DeductionDate", GeneralDB.CommonDB.SetCulturedDate(studentFOSTBDetails.DeductionDate));
                sqlCommand.Parameters.AddWithValue("@Amount", studentFOSTBDetails.Amount);
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Message = "success";
                id = Convert.ToInt32(output.Value);               
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, id);
        }

        public OperationDetails PayDeductionTypeCrud(PayDeductionTypeModel payDeductionTypeModel, int operationId)
        {
            string Message = "";
            int id = 0;
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspPayDeductionTypeCUD", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aTntTranMode", operationId);
                sqlCommand.Parameters.AddWithValue("@aTntDeductionTypeID", payDeductionTypeModel.DeductionTypeID);
                sqlCommand.Parameters.AddWithValue("@aNvrDeductionTypeName_1", payDeductionTypeModel.DeductionTypeName_1);
                sqlCommand.Parameters.AddWithValue("@aNvrDeductionTypeName_2", payDeductionTypeModel.DeductionTypeName_2);
                sqlCommand.Parameters.AddWithValue("@aNvrDeductionTypeName_3", payDeductionTypeModel.DeductionTypeName_3);
                sqlCommand.Parameters.AddWithValue("@aNvrDeductionTypeShortName_1", payDeductionTypeModel.DeductionTypeShortName_1);
                sqlCommand.Parameters.AddWithValue("@aNvrDeductionTypeShortName_2", payDeductionTypeModel.DeductionTypeShortName_2);
                sqlCommand.Parameters.AddWithValue("@aNvrDeductionTypeShortName_3", payDeductionTypeModel.DeductionTypeShortName_3);
                sqlCommand.Parameters.AddWithValue("@aDttTransactionDate", GeneralDB.CommonDB.SetCulturedDate(payDeductionTypeModel.TransactionDate));
                sqlCommand.Parameters.AddWithValue("@aBitIsActive", payDeductionTypeModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@aIntSequence", payDeductionTypeModel.Sequence);
                sqlCommand.Parameters.AddWithValue("@aNvracc_code", payDeductionTypeModel.acc_code);
                sqlCommand.Parameters.AddWithValue("@aBitIsExcludeInJV", payDeductionTypeModel.IsExcludeInJV);
                sqlCommand.Parameters.AddWithValue("@VacationAccountCode", payDeductionTypeModel.VacationAccountCode);
                sqlCommand.Parameters.AddWithValue("@aBitFamilyBalanceDeduction", payDeductionTypeModel.IsFamilyBalanceDeduction);

                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                SqlParameter OutPutMessage = new SqlParameter("@OutPutMessage", SqlDbType.NVarChar, 100);
                output.Direction = ParameterDirection.Output;
                OutPutMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);
                sqlCommand.Parameters.Add(OutPutMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (operationId == 1 || operationId == 2)
                {
                    operationDetails.InsertedRowId = Convert.ToInt32(output.Value);
                    operationDetails.Message = "success";
                    operationDetails.CssClass = "success";
                }
                if (operationId == 3)
                {
                    if (OutPutMessage.Value.ToString() == "error")
                    {
                        operationDetails.Success = false;
                        operationDetails.CssClass = "error";
                        operationDetails.Message = "You are not able to delete this deduction type as its already assign to employees.";
                    }
                    else
                    {
                        operationDetails.InsertedRowId = Convert.ToInt32(output.Value);
                        operationDetails.Message = "Deduction type deleted successfully";
                        operationDetails.CssClass = "success";
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public List<PayDeductionDetailModel> GetAllPayDeductionDetailsList(int PayDeductionId)
        {
            List<PayDeductionDetailModel> payDeductionDetailModelList = null;
            try
            {
                payDeductionDetailModelList = new List<PayDeductionDetailModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDeductionDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", PayDeductionId);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayDeductionDetailModel payDeductionDetailModel;
                    while (sqlDataReader.Read())
                    {
                        payDeductionDetailModel = new PayDeductionDetailModel();
                        payDeductionDetailModel.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        payDeductionDetailModel.PayDeductionDetailID = Convert.ToInt32(sqlDataReader["PayDeductionDetailID"].ToString());
                        payDeductionDetailModel.Amount = Convert.ToDouble(sqlDataReader["Amount"]);
                        payDeductionDetailModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        payDeductionDetailModel.DeductionDate = sqlDataReader["DeductionDate"].ToString();
                        payDeductionDetailModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        payDeductionDetailModel.IsProcessed = Convert.ToBoolean(sqlDataReader["IsProcessed"]);
                        payDeductionDetailModel.IsWaived = Convert.ToBoolean(sqlDataReader["IsWaived"]);
                        payDeductionDetailModelList.Add(payDeductionDetailModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDeductionDetailModelList;
        }


        public PayDeductionTypeModel GetDeductionById(int id)
        {
            PayDeductionTypeModel payDeductionTypeModel = null;

            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_GetPayDeductionById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@deductionTypeID", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        payDeductionTypeModel = new PayDeductionTypeModel();
                        payDeductionTypeModel.DeductionTypeID = Convert.ToInt32(sqlDataReader["DeductionTypeID"].ToString());
                        payDeductionTypeModel.DeductionTypeName_1 = Convert.ToString(sqlDataReader["DeductionTypeName_1"].ToString());
                        payDeductionTypeModel.DeductionTypeName_2 = Convert.ToString(sqlDataReader["DeductionTypeName_2"]);
                        payDeductionTypeModel.DeductionTypeName_3 = Convert.ToString(sqlDataReader["DeductionTypeName_3"]);
                        payDeductionTypeModel.DeductionTypeShortName_1 = Convert.ToString(sqlDataReader["DeductionTypeShortName_1"]);
                        payDeductionTypeModel.DeductionTypeShortName_2 = Convert.ToString(sqlDataReader["DeductionTypeShortName_2"]);
                        payDeductionTypeModel.DeductionTypeShortName_3 = Convert.ToString(sqlDataReader["DeductionTypeShortName_3"]);
                        payDeductionTypeModel.TransactionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["TransactionDate"].ToString());
                        payDeductionTypeModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        payDeductionTypeModel.Sequence = sqlDataReader["Sequence"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Sequence"].ToString());
                        payDeductionTypeModel.acc_code = Convert.ToString(sqlDataReader["acc_code"]);
                        payDeductionTypeModel.VacationAccountCode = Convert.ToString(sqlDataReader["VacationAccountCode"]);
                        payDeductionTypeModel.IsExcludeInJV = sqlDataReader["IsExcludeInJV"] == DBNull.Value ? false : Convert.ToBoolean(sqlDataReader["IsExcludeInJV"]);
                        payDeductionTypeModel.IsFamilyBalanceDeduction = Convert.ToBoolean(sqlDataReader["FamilyBalanceDeduction"].ToString() == "" ? "false" : sqlDataReader["FamilyBalanceDeduction"].ToString());
                    }

                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return payDeductionTypeModel;
        }

        public OperationDetails updateInstallmentStatus(PayDeductionModel payDeductionModel)
        {
            string Message = "";
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdatePayDeductionInstallmentStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayDeductionID", payDeductionModel.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@isInstallment", payDeductionModel.IsInstallment);

                SqlParameter OperationMessage = new SqlParameter("@OperationMessage", SqlDbType.VarChar, 1000);
                OperationMessage.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(OperationMessage);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();

                Message = OperationMessage.Value.ToString();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return new OperationDetails(true, Message, null, payDeductionModel.PayDeductionID);
        }

        public List<PayDeductionTypeModel> GetDashboardFlashForDeductions()
        {
            List<PayDeductionTypeModel> payDeductionTypeModelList = null;
            try
            {
                payDeductionTypeModelList = new List<PayDeductionTypeModel>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("DashboardFlashForPayRollDeductions", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    PayDeductionTypeModel payDeductionTypeModel;
                    while (sqlDataReader.Read())
                    {
                        payDeductionTypeModel = new PayDeductionTypeModel();

                        payDeductionTypeModel.DeductionTypeID = Convert.ToInt32(sqlDataReader["DeductionTypeID"] == null ? "0" : sqlDataReader["DeductionTypeID"].ToString());
                        payDeductionTypeModel.DeductionTypeName_1 = Convert.ToString(sqlDataReader["DeductionTypeName_1"] == null ? "" : sqlDataReader["DeductionTypeName_1"].ToString());
                        payDeductionTypeModel.CumulativeDeduction = Convert.ToDecimal(sqlDataReader["CummulativeAmount"] == null ? 0.0 : sqlDataReader["CummulativeAmount"]);
                        payDeductionTypeModelList.Add(payDeductionTypeModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return payDeductionTypeModelList;
        }

        /// <summary>
        /// To get by id
        /// </summary>
        /// <param name="PayDeductionID"></param>
        /// <returns></returns>
        public PayDeductionModel GetPayDeductionByID(int PayDeductionID)
        {
            PayDeductionModel payDeductionModel = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_uspGetPayDeductionByID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", PayDeductionID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        payDeductionModel = new PayDeductionModel();
                        payDeductionModel.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        payDeductionModel.DeductionTypeID = Convert.ToInt32(sqlDataReader["DeductionTypeID"].ToString());
                        payDeductionModel.EffectiveDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["EffectiveDate"].ToString());
                        payDeductionModel.PaidCycle = Convert.ToInt32(sqlDataReader["PaidCycle"]);
                        payDeductionModel.IsInstallment = Convert.ToBoolean(sqlDataReader["IsInstallment"]);
                        payDeductionModel.Amount = Convert.ToDouble(sqlDataReader["Amount"]);
                        payDeductionModel.Installment = Convert.ToDouble(sqlDataReader["Installment"].ToString());
                        payDeductionModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        payDeductionModel.EmployeeID = Convert.ToInt32(sqlDataReader["EmployeeID"]);
                        payDeductionModel.RefNumber = Convert.ToString(sqlDataReader["RefNumber"]);
                        payDeductionModel.TransactionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["TransactionDate"].ToString());
                        payDeductionModel.PvId = Convert.ToInt32(sqlDataReader["PvId"]);
                        payDeductionModel.GenerateDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["GenerateDate"].ToString());
                        payDeductionModel.DeductionType = Convert.ToString(sqlDataReader["DeductionTypeName"]);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return payDeductionModel;
        }

        public OperationDetails UpdateDeductionDetailIsActive(int id, bool status)
        {
            OperationDetails obj;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("UpdateDeductionDetailIsActive", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionDetailID", id);
                sqlCommand.Parameters.AddWithValue("@aBitIsActive", status);
                int result = sqlCommand.ExecuteNonQuery();
                obj = new OperationDetails();
                if (result > 0)
                {
                    obj.Message = "success";
                    obj.Success = true;
                }
                else
                {
                    obj.Message = "error";
                    obj.Success = false;
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return obj;
        }

        //public OperationDetails UpdateDeductionDetails(PayDeductionDetailModel model)
        //{
        //    OperationDetails obj=new OperationDetails();
        //    try
        //    {
        //        string AmountFormat = new PayrollDB().GetAmountFormat();
        //        sqlConnection = new SqlConnection(connectionString);
        //        sqlConnection.Open();
        //        sqlCommand = new SqlCommand("stp_HR_UpdateDeductionDetails", sqlConnection);
        //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //        sqlCommand.Parameters.AddWithValue("@PayDeductionDetailID", model.PayDeductionDetailID);
        //        sqlCommand.Parameters.AddWithValue("@PayDeductionID", model.PayDeductionID);
        //        sqlCommand.Parameters.AddWithValue("@DeductionDate", GeneralDB.CommonDB.SetCulturedDate(model.DeductionDate));
        //        sqlCommand.Parameters.AddWithValue("@Amount", model.Amount);
        //        sqlCommand.Parameters.AddWithValue("@Comments", model.Comments);
        //        sqlCommand.Parameters.AddWithValue("@IsActive", model.IsActive);

        //        SqlParameter IsSuccess = new SqlParameter("@IsSuccess", SqlDbType.Bit);
        //        IsSuccess.Direction = ParameterDirection.Output;
        //        sqlCommand.Parameters.Add(IsSuccess);
        //        int result = sqlCommand.ExecuteNonQuery();

        //        if (Convert.ToBoolean(IsSuccess.Value.ToString()))
        //        {
        //            obj.Message = "Pay deduction installment updated successfully.";
        //            obj.Success = true;
        //            obj.CssClass = "success";
        //        }
        //        else
        //        {
        //            obj.Message = "Error while updating pay deduction installment.";
        //            obj.Success = false;
        //            obj.CssClass = "error";
        //        }
        //        sqlConnection.Close();
        //    }
        //    catch (Exception exception)
        //    {
        //        obj.Message = "Technical Error while updating pay deduction installment.";
        //        obj.Success = false;
        //        obj.CssClass = "error";
        //        obj.Exception = exception;
        //    }
        //    finally
        //    {
        //        if (sqlReader != null)
        //        {
        //            sqlReader.Close();
        //        }
        //        if (sqlConnection != null)
        //        {
        //            sqlConnection.Close();
        //        }
        //    }
        //    return obj;
        //}

        public OperationDetails UpdateDeductionDetails(DataTable dt, int PayDeductionID,int mode)
        {
            OperationDetails obj = new OperationDetails();
            try
            {
                string AmountFormat = new PayrollDB().GetAmountFormat();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_UpdateDeductionDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Hr_MultipleDeductionDetails", dt);
                sqlCommand.Parameters.AddWithValue("@PayDeductionID", PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                SqlParameter IsSuccess = new SqlParameter("@IsSuccess", SqlDbType.Bit);
                IsSuccess.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsSuccess);
                int result = sqlCommand.ExecuteNonQuery();

                if (Convert.ToBoolean(IsSuccess.Value.ToString()))
                {
                    obj.Message = "Pay deduction installment updated successfully.";
                    obj.Success = true;
                    obj.CssClass = "success";
                }
                else
                {
                    obj.Message = "Error while updating pay deduction installment.";
                    obj.Success = false;
                    obj.CssClass = "error";
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                obj.Message = "Technical Error while updating pay deduction installment.";
                obj.Success = false;
                obj.CssClass = "error";
                obj.Exception = exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return obj;
        }

        public PayDeductionDetailModel GetDeductionDetailById(int payDeductionDetailID)
        {
            PayDeductionDetailModel payDeductionDetailModel = null;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_HR_GetPayDeductionDetailById", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayDeductionDetailID", payDeductionDetailID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        payDeductionDetailModel = new PayDeductionDetailModel();
                        payDeductionDetailModel.PayDeductionDetailID = Convert.ToInt32(sqlDataReader["PayDeductionDetailID"].ToString());
                        payDeductionDetailModel.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        payDeductionDetailModel.DeductionDate = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["DeductionDate"].ToString());
                        payDeductionDetailModel.Comments = Convert.ToString(sqlDataReader["Comments"]);
                        payDeductionDetailModel.Amount = Convert.ToDouble(sqlDataReader["Amount"] == DBNull.Value ? 0 : sqlDataReader["Amount"]);
                        payDeductionDetailModel.IsActive = Convert.ToBoolean(sqlDataReader["IsActive"]);
                        payDeductionDetailModel.TotalAmount = Convert.ToDouble(sqlDataReader["TotalAmount"] == DBNull.Value ? 0 : sqlDataReader["TotalAmount"]);
                        payDeductionDetailModel.AmountLeft = Convert.ToDouble(sqlDataReader["AmountLeft"] == DBNull.Value ? 0 : sqlDataReader["AmountLeft"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return payDeductionDetailModel;
        }

        public PayDeductionAmountsSummation GetPayDeductionAmountsSummation(int payDeductionID)
        {
            PayDeductionAmountsSummation payDeductionDetailModel = null;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("usp_GetPayDeductionAmountsSummation", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayDeductionID", payDeductionID);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        payDeductionDetailModel = new PayDeductionAmountsSummation();
                        payDeductionDetailModel.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        payDeductionDetailModel.Amount = Convert.ToDouble(sqlDataReader["Amount"] == DBNull.Value ? 0 : sqlDataReader["Amount"]);
                        payDeductionDetailModel.InstallmentTotalAmount = Convert.ToDouble(sqlDataReader["InstallmentTotalAmount"] == DBNull.Value ? 0 : sqlDataReader["InstallmentTotalAmount"]);
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }

            return payDeductionDetailModel;
        }

        public IEnumerable<PickList> GetPayDeductionDddlList()
        {
            GeneralDB.DataHelper dataHelper = new GeneralDB.DataHelper();
            DataTable dt = dataHelper.ExcuteCommandText("select * from GEN_PayDeductionType");
            List<PickList> list = new List<PickList>();
            PickList model;
            foreach (DataRow dr in dt.Rows)
            {
                model = new PickList();
                model.id = Convert.ToInt32(dr["DeductionTypeID"].ToString());
                model.text = dr["DeductionTypeName_1"].ToString();
                list.Add(model);
            }
            return list;
        }

        public OperationDetails AddMultipleDeduction(int deductionType, string effectiveDatetxt, string refNotxt, string descriptiontxt, List<multipleDeduction> multipleDeductionDetails, int RequestEmployeeId, bool Permission)
        {
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int rowCount = 1;
            try
            {
                DataTable multipleDeduction = new DataTable();
                multipleDeduction.Columns.Add("RowId", typeof(int));
                multipleDeduction.Columns.Add("DeductionType", typeof(int));
                multipleDeduction.Columns.Add("EffectiveDate", typeof(DateTime));
                multipleDeduction.Columns.Add("paidCycle", typeof(int));
                multipleDeduction.Columns.Add("isInstallment", typeof(bool));
                multipleDeduction.Columns.Add("Amount", typeof(decimal));
                multipleDeduction.Columns.Add("installMent", typeof(decimal));
                multipleDeduction.Columns.Add("comments", typeof(string));
                multipleDeduction.Columns.Add("EmployeeId", typeof(int));
                multipleDeduction.Columns.Add("RefNumber", typeof(string));
                multipleDeduction.Columns.Add("CycleId", typeof(int));

                DataTable multipleDeductionDetail = new DataTable();
                multipleDeductionDetail.Columns.Add("PayDeductionDetailId", typeof(int));
                multipleDeductionDetail.Columns.Add("PayDeductionId", typeof(int));
                multipleDeductionDetail.Columns.Add("DeductionDate", typeof(DateTime));
                multipleDeductionDetail.Columns.Add("Amount", typeof(decimal));
                multipleDeductionDetail.Columns.Add("Comments", typeof(string));
                multipleDeductionDetail.Columns.Add("Active", typeof(bool));
                multipleDeductionDetail.Columns.Add("employeeId", typeof(int));
                multipleDeductionDetail.Columns.Add("IsProcessed", typeof(bool));
                multipleDeductionDetail.Columns.Add("IsWaived", typeof(int));
                multipleDeductionDetail.Columns.Add("rowNum", typeof(int));
                foreach (var item in multipleDeductionDetails)
                {
                    multipleDeduction.Rows.Add(
                        rowCount,
                        deductionType,
                        DataAccess.GeneralDB.CommonDB.SetCulturedDate(effectiveDatetxt),
                        item.cycle,
                        item.cycle > 1 ? true : false,
                        item.amount,
                        Math.Round(item.amount / item.cycle, 3, MidpointRounding.AwayFromZero),
                        descriptiontxt,
                        item.employeeId,
                        refNotxt,
                        null
                        );
                    decimal splitAmount = 0;
                    if (item.cycle > 0)
                    {
                        splitAmount = item.amount / item.cycle;
                    }
                    else
                    {
                        splitAmount = item.amount;
                    }
                    for (int i = 0; i < item.cycle; i++)
                    {
                        DateTime dt = new DateTime();
                        if (i == 0)
                        {
                            dt = DateTime.ParseExact(effectiveDatetxt, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            dt = DateTime.ParseExact(effectiveDatetxt, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddMonths(i);
                        }
                        multipleDeductionDetail.Rows.Add(
                            0,
                            0,
                            dt,
                            splitAmount,
                            descriptiontxt,
                            true,
                            item.employeeId,
                            false,
                            false,
                            rowCount
                            );
                    }
                    rowCount++;
                }
                List<SqlParameter> parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("@MultipleDeduction", multipleDeduction));
                parameterList.Add(new SqlParameter("@MultipleDeductionDetails", multipleDeductionDetail));
                parameterList.Add(new SqlParameter("@RequestEmployeeId", RequestEmployeeId));
                parameterList.Add(new SqlParameter("@Permission", Permission));
                bool result = dataHelper.ExcutestoredProcedure(parameterList, "HR_Stp_AddMultipleDeduction");
                oDetails.Success = true;
                oDetails.Message = "Record save successfully";
                oDetails.CssClass = "success";
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public OperationDetails GetCycleIDbyDate(DateTime date, out int cycleID)
        {
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            cycleID = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select [dbo].[Hr_Fn_GetCycleDetails](@date,0) as CycleID", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                sqlCommand.Parameters.AddWithValue("@date", date);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        cycleID = Convert.ToInt32(sqlDataReader["CycleID"] == DBNull.Value ? "0" : sqlDataReader["CycleID"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public List<DeductionRule> GetDeductionRules(int mode)
        {
            List<DeductionRule> DeductionRuleList = null;
            try
            {
                DeductionRuleList = new List<DeductionRule>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_GetDeductionRules", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Mode", mode);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionRule deductionRuleModel;
                    while (sqlDataReader.Read())
                    {
                        deductionRuleModel = new DeductionRule();
                        deductionRuleModel.DeductionRuleID = Convert.ToInt32(sqlDataReader["DeductionRuleID"].ToString());
                        deductionRuleModel.DeductionRuleDesc = sqlDataReader["DeductionRuleDescription"].ToString();
                        deductionRuleModel.FirstTimeDeduction = Convert.ToDouble(sqlDataReader["FirstTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["FirstTimeDeduction"].ToString());
                        deductionRuleModel.SecondTimeDeduction = Convert.ToDouble(sqlDataReader["SecondTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["SecondTimeDeduction"].ToString());
                        deductionRuleModel.ThirdTimeDeduction = Convert.ToDouble(sqlDataReader["ThirdTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["ThirdTimeDeduction"].ToString());
                        deductionRuleModel.FourthTimeDeduction = Convert.ToDouble(sqlDataReader["FourthTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["FourthTimeDeduction"].ToString());
                        deductionRuleModel.StartMinute = Convert.ToInt32(sqlDataReader["StartMinute"].ToString() == "" ? "0" : sqlDataReader["StartMinute"].ToString());
                        deductionRuleModel.EndMinute = Convert.ToInt32(sqlDataReader["EndMinute"].ToString() == "" ? "0" : sqlDataReader["EndMinute"].ToString());
                        deductionRuleModel.isAbsentRule = Convert.ToBoolean(sqlDataReader["IsAbsentRule"].ToString() == "" ? "false" : sqlDataReader["IsAbsentRule"].ToString());
                        deductionRuleModel.isEarlyRule = Convert.ToBoolean(sqlDataReader["IsEarlyOutRule"].ToString() == "" ? "false" : sqlDataReader["IsEarlyOutRule"].ToString());
                        deductionRuleModel.DelayToOtherWorkers = Convert.ToBoolean(sqlDataReader["DelayToOtherWorkers"].ToString() == "" ? "false" : sqlDataReader["DelayToOtherWorkers"].ToString());
                        DeductionRuleList.Add(deductionRuleModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DeductionRuleList;
        }

        public List<DeductionRule> GetGeneralDeductionRules()
        {
            List<DeductionRule> DeductionRuleList = null;
            try
            {
                DeductionRuleList = new List<DeductionRule>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetGeneralDeductionRules", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    DeductionRule deductionRuleModel;
                    while (sqlDataReader.Read())
                    {
                        deductionRuleModel = new DeductionRule();
                        deductionRuleModel.DeductionRuleID = Convert.ToInt32(sqlDataReader["DeductionRulesID"].ToString());
                        deductionRuleModel.DeductionRuleDesc = sqlDataReader["DeductionRuleDescription"].ToString();
                        deductionRuleModel.FirstTimeDeduction = Convert.ToDouble(sqlDataReader["FirstTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["FirstTimeDeduction"].ToString());
                        deductionRuleModel.SecondTimeDeduction = Convert.ToDouble(sqlDataReader["SecondTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["SecondTimeDeduction"].ToString());
                        deductionRuleModel.ThirdTimeDeduction = Convert.ToDouble(sqlDataReader["ThirdTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["ThirdTimeDeduction"].ToString());
                        deductionRuleModel.FourthTimeDeduction = Convert.ToDouble(sqlDataReader["FourthTimeDeduction"] == DBNull.Value ? "0" : sqlDataReader["FourthTimeDeduction"].ToString());
                        DeductionRuleList.Add(deductionRuleModel);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return DeductionRuleList;
        }

        public decimal GetAmountForDeductionRule(int EmployeeId, int DeductionRuleId, string DeductionDate)
        {
            decimal Amount = 0;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_CalulateAmountAsPerDeductionRule", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeId);
                sqlCommand.Parameters.AddWithValue("@DeductionRuleId", DeductionRuleId);
                sqlCommand.Parameters.AddWithValue("@DeductionDate", DateTime.ParseExact(DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                SqlParameter DeductionAmount = new SqlParameter("@DeductionAmount", SqlDbType.Float);
                DeductionAmount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(DeductionAmount);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Amount = Convert.ToDecimal(DeductionAmount.Value.ToString() == "" ? "0" : DeductionAmount.Value.ToString());
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return Amount;
        }

        public OperationDetails AddMultipleGeneralDeductions(List<EmployeeGeneralDeduction> lstEmployeeDeductions, string DeductionDate, int userId)
        {
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int rowCount = 1;
            try
            {
                DataTable multipleDeduction = new DataTable();

                multipleDeduction.Columns.Add("RowId", typeof(int));
                multipleDeduction.Columns.Add("EmployeeGeneralDeductionID", typeof(int));
                multipleDeduction.Columns.Add("DeductionRuleId", typeof(int));
                multipleDeduction.Columns.Add("EmployeeID", typeof(int));
                multipleDeduction.Columns.Add("DeductionDate", typeof(DateTime));
                multipleDeduction.Columns.Add("DeductionAmount", typeof(decimal));

                foreach (var item in lstEmployeeDeductions)
                {
                    multipleDeduction.Rows.Add(
                        rowCount,
                        null,
                        item.DeductionRuleId,
                        item.EmployeeId,
                        null,
                        item.DeductionAmount
                        );
                    rowCount++;
                }
                List<SqlParameter> parameterList = new List<SqlParameter>();

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_AddMultipleGeneralDeduction", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MultipleDeduction", multipleDeduction);
                sqlCommand.Parameters.AddWithValue("@DeductionDate", DateTime.ParseExact(DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                SqlParameter Success = new SqlParameter("@isInserted", SqlDbType.Bit);
                Success.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Success);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                bool isInserted = Convert.ToBoolean(Success.Value.ToString() == "" ? "false" : Success.Value.ToString());
                if (isInserted)
                {
                    oDetails.Success = true;
                    oDetails.Message = "Deductions saved successfully";
                    oDetails.CssClass = "success";
                }
                else
                {
                    oDetails.Success = false;
                    oDetails.Message = "Error while saving deductions";
                    oDetails.CssClass = "error";
                }

            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public List<EmployeeGeneralDeduction> GetEmployeeGeneralDeductions(int? EmployeeID, int? DeductionTypeID, int? UserId, string FromDate, string Todate, bool isConfirmed)
        {
            List<EmployeeGeneralDeduction> Deductions = null;
            try
            {
                Deductions = new List<EmployeeGeneralDeduction>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeGeneralDeductions", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@DeductiontypeID", DeductionTypeID);
                sqlCommand.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(Todate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@isConfirmed", isConfirmed);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeGeneralDeduction EmployeeDeduction;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDeduction = new EmployeeGeneralDeduction();
                        EmployeeDeduction.EmployeeGeneralDeductionID = Convert.ToInt32(sqlDataReader["EmployeeGeneralDeductionID"].ToString());
                        EmployeeDeduction.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDeduction.EmployeeAltID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        EmployeeDeduction.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        EmployeeDeduction.DeductionDate = Convert.ToDateTime(sqlDataReader["DeductionDate"].ToString()).ToString("dd/MM/yyyy");
                        EmployeeDeduction.DeductionRuleId = Convert.ToInt32(sqlDataReader["DeductionRuleID"].ToString());
                        EmployeeDeduction.DeductionRuleDesc = sqlDataReader["DeductionRuleDescription"].ToString();
                        EmployeeDeduction.DeductionAmount = Convert.ToDecimal(sqlDataReader["DeductionAmount"].ToString() == "" ? "0" : sqlDataReader["DeductionAmount"].ToString());
                        EmployeeDeduction.isConfirmed = Convert.ToBoolean(sqlDataReader["IsConfirmed"].ToString() == "" ? "false" : sqlDataReader["IsConfirmed"].ToString());
                        Deductions.Add(EmployeeDeduction);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Deductions;
        }

        public EmployeeGeneralDeduction GetEmployeeGeneralDeductionDetails(int EmployeeGeneralDeductionID)
        {
            EmployeeGeneralDeduction Deduction = null;
            try
            {
                Deduction = new EmployeeGeneralDeduction();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeGeneralDeductionDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeGeneralDeductionId", EmployeeGeneralDeductionID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeGeneralDeduction EmployeeDeduction;
                    while (sqlDataReader.Read())
                    {
                        Deduction.EmployeeGeneralDeductionID = Convert.ToInt32(sqlDataReader["EmployeeGeneralDeductionID"].ToString());
                        Deduction.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        Deduction.EmployeeAltID = sqlDataReader["EmployeeAlternativeID"].ToString();
                        Deduction.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        Deduction.DeductionDate = Convert.ToDateTime(sqlDataReader["DeductionDate"].ToString()).ToString("dd/MM/yyyy");
                        Deduction.DeductionRuleId = Convert.ToInt32(sqlDataReader["DeductionRuleID"].ToString());
                        Deduction.DeductionRuleDesc = sqlDataReader["DeductionRuleDescription"].ToString();
                        Deduction.DeductionAmount = Convert.ToDecimal(sqlDataReader["DeductionAmount"].ToString() == "" ? "0" : sqlDataReader["DeductionAmount"].ToString());
                        Deduction.isConfirmed = Convert.ToBoolean(sqlDataReader["IsConfirmed"].ToString() == "" ? "false" : sqlDataReader["IsConfirmed"].ToString());

                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Deduction;
        }

        public OperationDetails UpdateGeneralDeductions(EmployeeGeneralDeduction EmployeeDeduction, int userId)
        {
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int rowCount = 1;
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_UpdateEmployeeGeneralDeductions", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeGeneralDeductionID", EmployeeDeduction.EmployeeGeneralDeductionID);
                sqlCommand.Parameters.AddWithValue("@Amount", EmployeeDeduction.DeductionAmount);
                sqlCommand.Parameters.AddWithValue("@DeductionRuleID", EmployeeDeduction.DeductionRuleId);
                sqlCommand.Parameters.AddWithValue("@DeductionDate", DateTime.ParseExact(EmployeeDeduction.DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                SqlParameter Success = new SqlParameter("@isUpdated", SqlDbType.Bit);
                Success.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Success);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                bool isUpdated = Convert.ToBoolean(Success.Value.ToString() == "" ? "false" : Success.Value.ToString());
                if (isUpdated)
                {
                    oDetails.Success = true;
                    oDetails.Message = "Deductions updated successfully";
                    oDetails.CssClass = "success";
                }
                else
                {
                    oDetails.Success = false;
                    oDetails.Message = "Error while savin deductions";
                    oDetails.CssClass = "error";
                }

            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public OperationDetails DeleteGeneralDeductions(int EmployeeGeneralDeductionID)
        {
            OperationDetails oDetails = new OperationDetails();
            try
            {

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Update HR_PayEmployeeGeneralDeductions SET IsDeleted=1 WHERE EmployeeGeneralDeductionID=" + EmployeeGeneralDeductionID + "", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                oDetails.Success = true;
                oDetails.Message = "Deductions updated successfully";
                oDetails.CssClass = "success";
            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public List<EmployeeGeneralDeduction> GetSelectedEmployeeGeneralDeductions(string GeneralDeductionIds)
        {
            List<EmployeeGeneralDeduction> Deductions = null;
            try
            {
                Deductions = new List<EmployeeGeneralDeduction>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("SELECT * from HR_PayEmployeeGeneralDeductions WHERE EmployeeGeneralDeductionID IN (" + GeneralDeductionIds + ")", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    EmployeeGeneralDeduction EmployeeDeduction;
                    while (sqlDataReader.Read())
                    {
                        EmployeeDeduction = new EmployeeGeneralDeduction();
                        EmployeeDeduction.EmployeeGeneralDeductionID = Convert.ToInt32(sqlDataReader["EmployeeGeneralDeductionID"].ToString());
                        EmployeeDeduction.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmployeeDeduction.DeductionDate = Convert.ToDateTime(sqlDataReader["DeductionDate"].ToString()).ToString("dd/MM/yyyy");
                        EmployeeDeduction.DeductionRuleId = Convert.ToInt32(sqlDataReader["DeductionRuleID"].ToString());
                        EmployeeDeduction.DeductionAmount = Convert.ToDecimal(sqlDataReader["DeductionAmount"].ToString() == "" ? "0" : sqlDataReader["DeductionAmount"].ToString());
                        EmployeeDeduction.isConfirmed = Convert.ToBoolean(sqlDataReader["IsConfirmed"].ToString() == "" ? "false" : sqlDataReader["IsConfirmed"].ToString());
                        Deductions.Add(EmployeeDeduction);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return Deductions;
        }

        public OperationDetails ConfirmedMultipleGeneralDeductions(List<EmployeeGeneralDeduction> lstEmployeeDeductions, string GenerateDate, int CycleId)
        {
            OperationDetails oDetails = new OperationDetails();
            dataHelper = new GeneralDB.DataHelper();
            int rowCount = 1;
            try
            {
                DataTable multipleDeduction = new DataTable();
                multipleDeduction.Columns.Add("RowId", typeof(int));
                multipleDeduction.Columns.Add("EmployeeGeneralDeductionID", typeof(int));
                multipleDeduction.Columns.Add("DeductionRuleId", typeof(int));
                multipleDeduction.Columns.Add("EmployeeID", typeof(int));
                multipleDeduction.Columns.Add("DeductionDate", typeof(DateTime));
                multipleDeduction.Columns.Add("DeductionAmount", typeof(decimal));

                foreach (var item in lstEmployeeDeductions)
                {
                    multipleDeduction.Rows.Add(
                        rowCount,
                        item.EmployeeGeneralDeductionID,
                        item.DeductionRuleId,
                        item.EmployeeId,
                        DateTime.ParseExact(item.DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                        item.DeductionAmount
                        );
                    rowCount++;
                }
                List<SqlParameter> parameterList = new List<SqlParameter>();

                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_Stp_UpdateGeneralDeductionConfirmStatus", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@MultipleDeduction", multipleDeduction);
                sqlCommand.Parameters.AddWithValue("@GenerateDate", DateTime.ParseExact(GenerateDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                sqlCommand.Parameters.AddWithValue("@GenerateCycleID", CycleId);
                SqlParameter Success = new SqlParameter("@isInserted", SqlDbType.Bit);
                Success.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(Success);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                bool isInserted = Convert.ToBoolean(Success.Value.ToString() == "" ? "false" : Success.Value.ToString());
                if (isInserted)
                {
                    oDetails.Success = true;
                    oDetails.Message = "Deductions posted successfully";
                    oDetails.CssClass = "success";
                }
                else
                {
                    oDetails.Success = false;
                    oDetails.Message = "Error while posting deductions";
                    oDetails.CssClass = "error";
                }

            }
            catch (Exception e)
            {
                oDetails.Success = false;
                oDetails.Message = "technical error has occurred";
                oDetails.CssClass = "error";
            }
            return oDetails;
        }

        public OperationDetails SavePayrollConfirmationRequestForDeduction(PayDeductionModel payDeductionModel, PayDeductionDetailModel payDeductionDetailModel, DataTable dt, int RequestEmployeeId, int Mode)
        {
            string Message = "";
            int id = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_PayrollRequestForPayDeduction", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayDeductionID", payDeductionModel.PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@DeductionTypeID", payDeductionModel.DeductionTypeID);
                sqlCommand.Parameters.AddWithValue("@EffectiveDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate));
                sqlCommand.Parameters.AddWithValue("@PaidCycle", payDeductionModel.PaidCycle);
                sqlCommand.Parameters.AddWithValue("@IsInstallment", payDeductionModel.IsInstallment);
                sqlCommand.Parameters.AddWithValue("@Amount", payDeductionModel.Amount);
                sqlCommand.Parameters.AddWithValue("@Installment", payDeductionModel.Installment);
                sqlCommand.Parameters.AddWithValue("@Comments", payDeductionModel.Comments);
                sqlCommand.Parameters.AddWithValue("@EmployeeID", payDeductionModel.EmployeeID);
                sqlCommand.Parameters.AddWithValue("@RefNumber", payDeductionModel.RefNumber);
                sqlCommand.Parameters.AddWithValue("@TransactionDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate));
                sqlCommand.Parameters.AddWithValue("@PvId", payDeductionModel.PvId);
                sqlCommand.Parameters.AddWithValue("@GenerateDate", DateTime.Now);

                sqlCommand.Parameters.AddWithValue("@DeductionDate", DataAccess.GeneralDB.CommonDB.SetCulturedDate(payDeductionDetailModel.DeductionDate));
                sqlCommand.Parameters.AddWithValue("@PayDeductionDetailID", payDeductionDetailModel.PayDeductionDetailID);
                sqlCommand.Parameters.AddWithValue("@DetailAmount", payDeductionDetailModel.Amount);
                sqlCommand.Parameters.AddWithValue("@DetailComments", payDeductionModel.Comments);
                sqlCommand.Parameters.AddWithValue("@DetailActive", payDeductionDetailModel.IsActive);
                sqlCommand.Parameters.AddWithValue("@RequestEmployeeId", RequestEmployeeId);
                if (dt.Rows.Count > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@HR_MultipleDeductionDetails", dt));
                }
                sqlCommand.Parameters.AddWithValue("@Mode", Mode);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                sqlConnection.Close();
                return new OperationDetails(true, "Pay deduction modification request send for confirmation.", null, id);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Technical error has occurred", exception);
            }
            finally
            {

            }
        }
        
        public RuleDeductionSetting GetDeductionRuleSetting()
        {
            RuleDeductionSetting ruleDeductionSetting = null;
            try
            {
                ruleDeductionSetting = new RuleDeductionSetting();
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select AppliedDeductionRules,AbsentCountForCalculation from HR_DeductionRulesSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        ruleDeductionSetting = new RuleDeductionSetting();
                        ruleDeductionSetting.AppliedDeductionRules = Convert.ToBoolean(sqlDataReader["AppliedDeductionRules"].ToString() == null ? "false" : sqlDataReader["AppliedDeductionRules"].ToString());
                        ruleDeductionSetting.AbsentCountForCalculation = Convert.ToInt32(sqlDataReader["AbsentCountForCalculation"].ToString() == null ? "0" : sqlDataReader["AbsentCountForCalculation"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return ruleDeductionSetting;
        }

        public EmployeeSalaryDetails GetEmployeeSalaryDetails(int EmployeeID, string ApplicableDate)
        {
            EmployeeSalaryDetails objEmpSalaryDetails = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetEmployeeSalaryDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                if (!string.IsNullOrEmpty(ApplicableDate))
                {
                    DateTime Date = DateTime.ParseExact(ApplicableDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    sqlCommand.Parameters.AddWithValue("@aDate", Date);
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        objEmpSalaryDetails = new EmployeeSalaryDetails();
                        objEmpSalaryDetails.FullSalary = Convert.ToDouble(sqlDataReader["FullSalary"].ToString() == null ? "0" : sqlDataReader["FullSalary"].ToString());
                        objEmpSalaryDetails.PerDaySalary = Convert.ToDouble(sqlDataReader["PerDaySalary"].ToString() == null ? "0" : sqlDataReader["PerDaySalary"].ToString());
                        objEmpSalaryDetails.PerHourSalary = Convert.ToDouble(sqlDataReader["PerHourSalary"].ToString() == null ? "0" : sqlDataReader["PerHourSalary"].ToString());
                        objEmpSalaryDetails.PerMinuteSalary = Convert.ToDouble(sqlDataReader["PerMinuteSalary"].ToString() == null ? "0" : sqlDataReader["PerMinuteSalary"].ToString());
                        objEmpSalaryDetails.Salary_DA = Convert.ToDouble(sqlDataReader["Salary_DA"].ToString() == null ? "0" : sqlDataReader["Salary_DA"].ToString());
                        objEmpSalaryDetails.PerDaySalary_DA = Convert.ToDouble(sqlDataReader["PerDaySalary_DA"].ToString() == null ? "0" : sqlDataReader["PerDaySalary_DA"].ToString());
                        objEmpSalaryDetails.PerHourSalary_DA = Convert.ToDouble(sqlDataReader["PerHourSalary_DA"].ToString() == null ? "0" : sqlDataReader["PerHourSalary_DA"].ToString());
                        objEmpSalaryDetails.PerMinuteSalary_DA = Convert.ToDouble(sqlDataReader["PerMinuteSalary_DA"].ToString() == null ? "0" : sqlDataReader["PerMinuteSalary_DA"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return objEmpSalaryDetails;
        }

        public OperationDetails DeletePayDeductionDetail(int DeductionDetailID)
        {
            OperationDetails operationDetails = new OperationDetails();
            int DeletedRowsCount = 0;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_DeleteDeductionDetail", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PayDeductionDetailId", DeductionDetailID);
                SqlParameter output = new SqlParameter("@output", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(output);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DeletedRowsCount = Convert.ToInt32(output.Value);

                sqlConnection.Close();

                if (DeletedRowsCount > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Deduction installment deleted successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {

                    operationDetails.Success = false;
                    operationDetails.Message = "Error while deleting deduction installment.";
                    operationDetails.CssClass = "error";
                }


            }
            catch
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred.";
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return operationDetails;
        }

        public bool CheckAmountCycleChange()
        {
            return true;
        }

        public DataSet GetInstallmentDeductionDetail(int? EmployeeID, int? InstallmentPayDeductionId)
        {
            DataSet ds = new DataSet();
            try
            {

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_STP_GetEmployeeInstalmentDeductionReport", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@aIntEmployeeID", EmployeeID);
                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", InstallmentPayDeductionId);
                sqlCommand.CommandTimeout = 9000;
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = sqlCommand;

                mySqlDataAdapter.Fill(ds);

                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
            return ds;
        }

        public bool CheckPaidCycleValidation(int PayDeductionID, int Paidcycles, double Amount, out bool IsPaidAmountValid)
        {
            bool IsPaidCycleValid = false;
            IsPaidAmountValid = false;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_ValidatePaidDeduction", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayDeductionID", PayDeductionID);
                sqlCommand.Parameters.AddWithValue("@Amount", Amount);
                sqlCommand.Parameters.AddWithValue("@Paidcycles", Paidcycles);
                SqlParameter IsPaidCycle = new SqlParameter("@IsPaidCyclesValid", SqlDbType.Bit);
                IsPaidCycle.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsPaidCycle);
                SqlParameter IsPaidAmount = new SqlParameter("@IsPaidAmountVald", SqlDbType.Bit);
                IsPaidAmount.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsPaidAmount);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                IsPaidCycleValid = Convert.ToBoolean(IsPaidCycle.Value);
                IsPaidAmountValid = Convert.ToBoolean(IsPaidAmount.Value);
                //}
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                IsPaidCycleValid = false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IsPaidCycleValid;
        }

        public bool CheckIfDeductionIsProcessed(int PayDeductionID)
        {
            bool IsProcessedAll = false;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_stp_CheckIfDeductionIsProcessed", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PayDeductionID", PayDeductionID);

                SqlParameter IsProcessed = new SqlParameter("@IsProcessed", SqlDbType.Bit);
                IsProcessed.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(IsProcessed);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                IsProcessedAll = Convert.ToBoolean(IsProcessed.Value);

                //}
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                IsProcessedAll = false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IsProcessedAll;

        }

        public bool CheckIfDeductionTypeExistForOst()
        {
            bool IsExistOSTDedType = false;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("select * from GEN_PayDeductionType Where FamilyBalanceDeduction=1", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    IsExistOSTDedType = true;
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return IsExistOSTDedType;

        }

        public DeductionSetting GetDeductionSetting()
        {
            DeductionSetting deductionSetting = new DeductionSetting();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetDeductionSettings", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {

                        deductionSetting.AbsentDeductionTypeId = Convert.ToInt32(sqlDataReader["AbsentDeductionTypeID"].ToString() == "" ? "0" : sqlDataReader["AbsentDeductionTypeID"].ToString());
                        deductionSetting.LateDeductionTypeId = Convert.ToInt32(sqlDataReader["LateDeductionTypeID"].ToString() == "" ? "0" : sqlDataReader["LateDeductionTypeID"].ToString());
                        deductionSetting.EarlyDeductionTypeId = Convert.ToInt32(sqlDataReader["EarlyDeductionTypeID"].ToString() == "" ? "0" : sqlDataReader["EarlyDeductionTypeID"].ToString());
                        deductionSetting.GeneralDeductionTypeId = Convert.ToInt32(sqlDataReader["GeneralDeductionTypeID"].ToString() == "" ? "0" : sqlDataReader["GeneralDeductionTypeID"].ToString());
                        deductionSetting.TaxPayDeductionTypeId = Convert.ToInt32(sqlDataReader["TaxDeductionTypeID"].ToString() == "" ? "0" : sqlDataReader["TaxDeductionTypeID"].ToString());
                        deductionSetting.FamilyBalanceDeductionTypeID = Convert.ToInt32(sqlDataReader["FamilyBalanceDeductionTypeID"].ToString() == "" ? "0" : sqlDataReader["FamilyBalanceDeductionTypeID"].ToString());
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return deductionSetting;
        }

        public List<FamilyDetails> GetFamilyOutstandingDetails(int? EmployeeId,int UserId)
        {
            List<FamilyDetails> FamilyOstDetails = null;
            try
            {
                FamilyOstDetails = new List<FamilyDetails>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetFamilyOutStandingBalance", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);
                if (EmployeeId != null)
                {
                    sqlCommand.Parameters.AddWithValue("@EmployeeID", EmployeeId);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@EmployeeID", null);
                }

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 9000;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    FamilyDetails EmpFamilyDetails;
                    while (sqlDataReader.Read())
                    {
                        EmpFamilyDetails = new FamilyDetails();
                        EmpFamilyDetails.FamilyID = Convert.ToInt32(sqlDataReader["family_id"].ToString());
                        EmpFamilyDetails.EmployeeId = Convert.ToInt32(sqlDataReader["EmployeeID"].ToString());
                        EmpFamilyDetails.EmpAltId = sqlDataReader["EmployeeAlternativeID"].ToString();
                        EmpFamilyDetails.EmployeeName = sqlDataReader["EmployeeName"].ToString();
                        EmpFamilyDetails.FamilyName_1 = sqlDataReader["family_name"].ToString();
                        EmpFamilyDetails.FatherName_1 = sqlDataReader["father_name"].ToString();
                        EmpFamilyDetails.OutstandingBalance = Convert.ToDouble(sqlDataReader["Balance"].ToString() == "" ? "0" : sqlDataReader["Balance"].ToString());
                        EmpFamilyDetails.IsOSTDeducted = Convert.ToBoolean(sqlDataReader["IsDeducted"].ToString() == "0" ? 0 : 1);
                        EmpFamilyDetails.DeductedAmount = Convert.ToDouble(sqlDataReader["DeductedAmount"].ToString() == "" ? "0" : sqlDataReader["DeductedAmount"].ToString());
                        FamilyOstDetails.Add(EmpFamilyDetails);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return FamilyOstDetails;
        }

        public List<StudentFOSTBDetails> GetStudentDetailsByFamilyId(int? FamilyID)
        {
            List<StudentFOSTBDetails> _StudentFOSTBDetails = null;
            try
            {
                _StudentFOSTBDetails = new List<StudentFOSTBDetails>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetStudentDetailsByFamilyId", sqlConnection);
                if (FamilyID != null)
                {
                    sqlCommand.Parameters.AddWithValue("@FamilyID", FamilyID);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("@FamilyID", null);
                }

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    StudentFOSTBDetails objStudentFOSTBDetails;
                    while (sqlDataReader.Read())
                    {
                        objStudentFOSTBDetails = new StudentFOSTBDetails();
                        objStudentFOSTBDetails.FamilyID = Convert.ToInt32(sqlDataReader["FamilyID"].ToString());
                        objStudentFOSTBDetails.StudentID = Convert.ToInt32(sqlDataReader["StudentID"].ToString());
                        objStudentFOSTBDetails.StudentFullName = sqlDataReader["StudentFullName"].ToString();
                        objStudentFOSTBDetails.IsRegistered = Convert.ToBoolean(sqlDataReader["IsRegistered"].ToString());
                        _StudentFOSTBDetails.Add(objStudentFOSTBDetails);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return _StudentFOSTBDetails;
        }

        public List<StudentFOSTBDetails> GetFOSTBDetailsListByDeductionId(int PayDeductionId,string DeductionDate, int mode)
        {
            List<StudentFOSTBDetails> studentFOSTBDetailsList = null;
            try
            {
                studentFOSTBDetailsList = new List<StudentFOSTBDetails>();

                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("HR_StpGetFOSTBalanceDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@aIntPayDeductionID", PayDeductionId);
                sqlCommand.Parameters.AddWithValue("@mode", mode);
                if (!string.IsNullOrEmpty(DeductionDate))
                {
                    sqlCommand.Parameters.AddWithValue("@DeductionDate", GeneralDB.CommonDB.SetCulturedDate(DeductionDate));
                }                              

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    StudentFOSTBDetails studentFOSTBDetails;
                    while (sqlDataReader.Read())
                    {
                        studentFOSTBDetails = new StudentFOSTBDetails();
                        studentFOSTBDetails.PayDeductionID = Convert.ToInt32(sqlDataReader["PayDeductionID"].ToString());
                        studentFOSTBDetails.DeductionStdID = sqlDataReader["DeductionStdID"]==DBNull.Value?0:Convert.ToInt32(sqlDataReader["DeductionStdID"].ToString());
                        studentFOSTBDetails.StudentID = sqlDataReader["StudentID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["StudentID"].ToString());
                        studentFOSTBDetails.Amount = Convert.ToDecimal(sqlDataReader["Amount"].ToString());
                        studentFOSTBDetails.DeductionDate = sqlDataReader["DeductionDate"].ToString();
                        studentFOSTBDetails.StudentFullName = sqlDataReader["StudentFullName"].ToString();
                        studentFOSTBDetails.IsRegistered = Convert.ToBoolean(sqlDataReader["IsRegistered"].ToString());
                        studentFOSTBDetails.IsProcessed = Convert.ToBoolean(sqlDataReader["IsProcessed"].ToString());
                        studentFOSTBDetailsList.Add(studentFOSTBDetails);
                    }
                }
                sqlConnection.Close();

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return studentFOSTBDetailsList;
        }

        public int CheckWaivedInstallments(string arrWaived, int PayDeductionId)
        {
            int CountOfWaived = 0;

            try
            {                
                connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_WaivedDeductionDetails", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@deductionTypeID", PayDeductionId);
                sqlCommand.Parameters.AddWithValue("@arrWaived", arrWaived);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    if (sqlDataReader.Read())
                    {
                        CountOfWaived = Convert.ToInt32(sqlDataReader["CountOfWaived"].ToString());
                    }

                }
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }

            }
            return CountOfWaived;
        }
    }
}
