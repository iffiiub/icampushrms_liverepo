﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace HRMS.DataAccess
{
    public class PositionDB : DBHelper
    {
        /// <summary>
        /// Insert Position into Position table
        /// Created By : Pradnesh Raut
        /// Created Date : 13 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        /// 

        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
        public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;

        public OperationDetails InsertPosition(PositionModel positionModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@PositionCode",(positionModel.PositionCode == null?"":positionModel.PositionCode)) ,
                      new SqlParameter("@PositionTitle", positionModel.PositionTitle) ,
                      new SqlParameter("@PositionTitle_2", (positionModel.PositionTitle_2 == null?"":positionModel.PositionTitle_2)) ,
                      new SqlParameter("@PositionTitle_3", (positionModel.PositionTitle_3 == null?"":positionModel.PositionTitle_3)) ,
                      new SqlParameter("@IsActive", positionModel.IsActive),
                      //Naresh 2020-03-18 CompanyId added and DepartmentId removed
                      //new SqlParameter("@DepartmentID", positionModel.DepartmentID) ,                    
                      new SqlParameter("@CompanyID", positionModel.CompanyID),
                      new SqlParameter("@Reg_TempID",positionModel.Reg_TempID) ,
                      new SqlParameter("@EmploymentTypeID", (positionModel.EmploymentTypeID == null?0:positionModel.EmploymentTypeID)),
                      new SqlParameter("@JobFamilyID", positionModel.JobFamilyID),
                      
                      new SqlParameter("@IsDeleted", positionModel.IsDeleted),
                      new SqlParameter("@IsAcademic", positionModel.IsAcademic)
                    };
                int PositionId = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure,
                                    "stp_Add_HR_Position", parameters));
                return new OperationDetails(true, "Position saved successfully.", null, PositionId);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while inserting Position.", exception);
                //throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// update Position into Position table
        /// Created By : Pradnesh Raut
        /// Created Date : 13 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails UpdatePosition(PositionModel positionModel)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@PositionID",positionModel.PositionID),
                      new SqlParameter("@PositionCode",(positionModel.PositionCode == null?"":positionModel.PositionCode)) ,
                      new SqlParameter("@PositionTitle", positionModel.PositionTitle) ,
                      new SqlParameter("@PositionTitle_2", (positionModel.PositionTitle_2 == null?"":positionModel.PositionTitle_2)) ,
                      new SqlParameter("@PositionTitle_3", (positionModel.PositionTitle_3 == null?"":positionModel.PositionTitle_3)) ,

                      new SqlParameter("@IsActive", positionModel.IsActive),
                     
                //new SqlParameter("@Available",positionModel.Available == null ? positionModel.Available : positionModel.Available.Replace('-','/')) ,
                //new SqlParameter("@EndDate", positionModel.EndDate == null ? positionModel.EndDate : positionModel.EndDate.Replace('-','/')),
                //new SqlParameter("@IncumbentsNumber", positionModel.IncumbentsNumber),
                    //new SqlParameter("@DepartmentID", positionModel.DepartmentID) ,
                    //Naresh 2020-03-18 CompanyId added
                      new SqlParameter("@CompanyID", positionModel.CompanyID),
                      new SqlParameter("@Reg_TempID", positionModel.Reg_TempID) ,
                      new SqlParameter("@EmploymentTypeID", (positionModel.EmploymentTypeID == null?0:positionModel.EmploymentTypeID)),
                      new SqlParameter("@JobFamilyID", positionModel.JobFamilyID),
                      new SqlParameter("@IsDeleted", positionModel.IsDeleted),
                      new SqlParameter("@IsAcademic", positionModel.IsAcademic)


                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_HR_Position", parameters);
                return new OperationDetails(true, "Position updated successfully", null);
            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while updating Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get all Position from the Position table
        /// Created By : Pradnesh Raut
        /// Created Date : 13 April 2015
        /// </summary>
        /// <returns>List of position</returns>
        public List<PositionModel> GetPositionList(int? companyId=null,int? userId=null)
        {
            // Create a list to hold the position		
            List<PositionModel> positionModelList = new List<PositionModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_Position", new SqlParameter("@CompanyId", companyId), new SqlParameter("@UserId", userId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        PositionModel positionModel;
                        while (reader.Read())
                        {
                            positionModel = new PositionModel();
                            positionModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            positionModel.PositionCode = Convert.ToString(reader["PositionCode"] == DBNull.Value ? "" : reader["PositionCode"].ToString());
                            positionModel.PositionTitle = Convert.ToString(reader["PositionTitle"] == DBNull.Value ? "" : reader["PositionTitle"].ToString());
                            positionModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value || reader["IsActive"].ToString() == "" ? 0 : reader["IsActive"]);
                            //*** Naresh 2020-03-19 DepartmentId removed and added companyId
                            //positionModel.DepartmentID = reader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DepartmentID"]);
                            positionModel.CompanyID = reader["CompanyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CompanyID"]);
                            positionModel.Reg_TempID = reader["Reg_TempID"] == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(reader["Reg_TempID"]).Trim()) ? 0 : Convert.ToInt32(reader["Reg_TempID"]);
                            positionModel.EmploymentTypeID = reader["EmploymentTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmploymentTypeID"]);
                            positionModel.JobFamilyID = reader["JobFamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["JobFamilyID"]);
                            positionModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? 0 : reader["IsDeleted"]);
                            positionModel.IsAcademic = Convert.ToBoolean(reader["IsAcademic"] == DBNull.Value ? 0 : reader["IsAcademic"]);
                            positionModelList.Add(positionModel);
                        }
                    }
                }
                // Finally, we return our list of position
                return positionModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching position List.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get Position By id from the Position table
        /// Created By : Pradnesh Raut
        /// Created Date : 13 April 2015
        /// </summary>
        /// <returns>Position</returns>
        public PositionModel GetPositionById(int PositionId)
        {
            try
            {
                PositionModel positionModel = new PositionModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_HR_PositionByPositionID", new SqlParameter("@PositionID", PositionId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            positionModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            positionModel.PositionCode = Convert.ToString(reader["PositionCode"] == DBNull.Value ? "" : reader["PositionCode"].ToString());
                            positionModel.PositionTitle = Convert.ToString(reader["PositionTitle"] == DBNull.Value ? "" : reader["PositionTitle"].ToString());
                            positionModel.PositionTitle_2 = Convert.ToString(reader["PositionTitle_2"] == DBNull.Value ? "" : reader["PositionTitle_2"].ToString());
                            positionModel.PositionTitle_3 = Convert.ToString(reader["PositionTitle_3"] == DBNull.Value ? "" : reader["PositionTitle_3"].ToString());
                            positionModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value || reader["IsActive"].ToString() == "" ? 0 : reader["IsActive"]);
                            //Naresh 2020-03-18 CompanyId added and DepartmentId removed
                            //positionModel.DepartmentID = reader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DepartmentID"]);
                            positionModel.CompanyID = reader["CompanyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CompanyID"]);
                            positionModel.CompanyName = Convert.ToString(reader["CompanyName"] == DBNull.Value ? "" : reader["CompanyName"].ToString());
                            positionModel.Reg_TempID = reader["Reg_TempID"] == DBNull.Value ? 0 : (reader["Reg_TempID"].ToString().Trim() == "" ? 0 : Convert.ToInt32(reader["Reg_TempID"]));
                            positionModel.EmploymentTypeID = reader["EmploymentTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmploymentTypeID"]);
                            positionModel.JobFamilyID = reader["JobFamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["JobFamilyID"]);
                            positionModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? 0 : reader["IsDeleted"]);
                            positionModel.IsAcademic = Convert.ToBoolean(reader["IsAcademic"] == DBNull.Value ? 0 : reader["IsAcademic"]);

                        }
                    }
                }
                return positionModel;
                // Finally, we return Position

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching Position.", exception);
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Delete Position from the Position table
        /// Created By : Pradnesh Raut
        /// Created Date : 13 April 2015
        /// </summary>
        /// <returns>OperationDetails</returns>
        public OperationDetails DeletePositionById(int PositionId)
        {
            int records = 0;
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@PositionID",PositionId)
                    };
                records = SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_HR_PositionByPositionID", parameters);
                return new OperationDetails(true, "Position deleted successfully", null, records);

            }
            catch (Exception exception)
            {
                return new OperationDetails(false, "Error : while deleting Position.", exception, records);
                throw;
            }
            finally
            {

            }
        }


        /// <summary>
        /// Get Position With Paging from the Position table
        /// Created By : Pradnesh Raut
        /// Created Date : 13 April 2015
        /// </summary>
        /// <returns>List of position With Paging</returns>
        public List<PositionModel> GetPositionListWithPaging(int? companyId,int? userId)
        {


            var parameters = new List<SqlParameter>();
            if (companyId.HasValue && companyId.Value > 0)
                parameters.Add(new SqlParameter("@CompanyId", companyId));
            parameters.Add(new SqlParameter("@UserId", userId));
            // Create a list to hold the position		
            List<PositionModel> positionModelList = new List<PositionModel>();
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Select_HR_PositionWithPaging", parameters.ToArray()))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        PositionModel positionModel;
                        while (reader.Read())
                        {
                            positionModel = new PositionModel();
                            positionModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            positionModel.PositionCode = Convert.ToString(reader["PositionCode"] == DBNull.Value ? "" : reader["PositionCode"].ToString().Trim());
                            positionModel.PositionTitle = Convert.ToString(reader["PositionTitle"] == DBNull.Value ? "" : reader["PositionTitle"].ToString().Trim());
                            positionModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value || reader["IsActive"].ToString() == "" ? 0 : Convert.ToInt32(reader["IsActive"]));
                            //Naresh 2020-03-18 CompanyId added and DepartmentId removed
                            //positionModel.DepartmentID = reader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DepartmentID"]);
                            positionModel.CompanyID = reader["CompanyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CompanyID"]);
                            positionModel.Reg_TempID = reader["Reg_TempID"] == DBNull.Value || string.IsNullOrEmpty(reader["Reg_TempID"].ToString().Trim()) ? 0 : Convert.ToInt32(reader["Reg_TempID"]);
                            positionModel.EmploymentTypeID = reader["EmploymentTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmploymentTypeID"]);
                            positionModel.JobFamilyID = reader["JobFamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["JobFamilyID"]);
                            positionModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? 0 : reader["IsDeleted"]);
                            positionModel.IsAcademic = Convert.ToBoolean(reader["IsAcademic"] == DBNull.Value ? 0 : reader["IsAcademic"]);
                            positionModel.PositionTitle_2 = Convert.ToString(reader["PositionTitle_2"] == DBNull.Value ? "" : reader["PositionTitle_2"].ToString().Trim());
                            positionModel.PositionTitle_3 = Convert.ToString(reader["PositionTitle_3"] == DBNull.Value ? "" : reader["PositionTitle_3"].ToString().Trim());
                            //positionModel.departmentName = Convert.ToString(reader["DepartmentName_1"] == DBNull.Value ? "" : reader["DepartmentName_1"].ToString().Trim());
                            positionModel.CompanyName = Convert.ToString(reader["CompanyName"] == DBNull.Value ? "" : reader["CompanyName"].ToString().Trim());
                            positionModelList.Add(positionModel);
                        }
                    }
                }
                // Finally, we return our list of position               
                return positionModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching position List.", exception);
                throw;
            }
            finally
            {

            }
        }

        public DataSet GetPositionDatasSet(int? companyId)
        {
            //HR_stp_EmployeeCredentialExport
            sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("HR_stp_PositionExport", sqlConnection);
            da.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            if (companyId.HasValue && companyId.Value > 0)
                da.SelectCommand.Parameters.AddWithValue("@CompanyId", companyId);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }

        public List<PositionModel> GetPositionListByVacationType(int VacationTypeId)
        {
            // Create a list to hold the position		
            List<PositionModel> positionModelList = new List<PositionModel>();
            try
            {
                SqlParameter[] parameters =
                   {
                      new SqlParameter("@VacationTypeID",VacationTypeId)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetPositionByVacationType", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        PositionModel positionModel;
                        while (reader.Read())
                        {
                            positionModel = new PositionModel();
                            positionModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            positionModel.PositionCode = Convert.ToString(reader["PositionCode"] == DBNull.Value ? "" : reader["PositionCode"].ToString());
                            positionModel.PositionTitle = Convert.ToString(reader["PositionTitle"] == DBNull.Value ? "" : reader["PositionTitle"].ToString());
                            positionModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value || reader["IsActive"].ToString() == "" ? 0 : reader["IsActive"]);
                            //*** Naresh 2020-03-19 DepartmentId removed and added companyId
                            //positionModel.DepartmentID = reader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DepartmentID"]);
                            positionModel.CompanyID = reader["CompanyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CompanyID"]);
                            positionModel.Reg_TempID = reader["Reg_TempID"] == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(reader["Reg_TempID"]).Trim()) ? 0 : Convert.ToInt32(reader["Reg_TempID"]);
                            positionModel.EmploymentTypeID = reader["EmploymentTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmploymentTypeID"]);
                            positionModel.JobFamilyID = reader["JobFamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["JobFamilyID"]);
                            positionModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? 0 : reader["IsDeleted"]);
                            positionModel.IsAcademic = Convert.ToBoolean(reader["IsAcademic"] == DBNull.Value ? 0 : reader["IsAcademic"]);
                            positionModelList.Add(positionModel);
                        }
                    }
                }
                // Finally, we return our list of position
                return positionModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching position List.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PositionModel> GetExistingPositionInAccrual(string PositionIds, int VacationTypeId)
        {
            // Create a list to hold the position		
            List<PositionModel> positionModelList = new List<PositionModel>();
            try
            {
                SqlParameter[] parameters =
                   {
                      new SqlParameter("@PositionIds",PositionIds),
                      new SqlParameter("@VacationTypeID",VacationTypeId)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetExistingPositionsInAccrualLeave", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        PositionModel positionModel;
                        while (reader.Read())
                        {
                            positionModel = new PositionModel();
                            positionModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            positionModel.PositionCode = Convert.ToString(reader["PositionCode"] == DBNull.Value ? "" : reader["PositionCode"].ToString());
                            positionModel.PositionTitle = Convert.ToString(reader["PositionTitle"] == DBNull.Value ? "" : reader["PositionTitle"].ToString());
                            positionModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value || reader["IsActive"].ToString() == "" ? 0 : reader["IsActive"]);
                            //*** Naresh 2020-03-19 DepartmentId removed and added companyId
                            //positionModel.DepartmentID = reader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DepartmentID"]);
                            positionModel.CompanyID = reader["CompanyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CompanyID"]);
                            positionModel.Reg_TempID = reader["Reg_TempID"] == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(reader["Reg_TempID"]).Trim()) ? 0 : Convert.ToInt32(reader["Reg_TempID"]);
                            positionModel.EmploymentTypeID = reader["EmploymentTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmploymentTypeID"]);
                            positionModel.JobFamilyID = reader["JobFamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["JobFamilyID"]);
                            positionModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? 0 : reader["IsDeleted"]);
                            positionModel.IsAcademic = Convert.ToBoolean(reader["IsAcademic"] == DBNull.Value ? 0 : reader["IsAcademic"]);
                            positionModelList.Add(positionModel);
                        }
                    }
                }
                // Finally, we return our list of position
                return positionModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching position List.", exception);
                throw;
            }
            finally
            {

            }
        }

        public List<PositionModel> GetPositionBasedOnSalaryRanges(string companyIDs)
        {
            // Create a list to hold the position		
            List<PositionModel> positionModelList = new List<PositionModel>();
            try
            {
                SqlParameter[] parameters =
                   {
                      new SqlParameter("@CompanyIDs",companyIDs)
                    };
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Hr_Stp_GetExistingPositionsInAccrualLeave", parameters))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        // While the reader has rows we loop through them,
                        // create new Position, and insert them into our list
                        PositionModel positionModel;
                        while (reader.Read())
                        {
                            positionModel = new PositionModel();
                            positionModel.PositionID = Convert.ToInt32(reader["PositionID"] == DBNull.Value ? "0" : reader["PositionID"].ToString());
                            positionModel.PositionCode = Convert.ToString(reader["PositionCode"] == DBNull.Value ? "" : reader["PositionCode"].ToString());
                            positionModel.PositionTitle = Convert.ToString(reader["PositionTitle"] == DBNull.Value ? "" : reader["PositionTitle"].ToString());
                            positionModel.IsActive = Convert.ToBoolean(reader["IsActive"] == DBNull.Value || reader["IsActive"].ToString() == "" ? 0 : reader["IsActive"]);
                            //*** Naresh 2020-03-19 DepartmentId removed and added companyId
                            //positionModel.DepartmentID = reader["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DepartmentID"]);
                            positionModel.CompanyID = reader["CompanyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CompanyID"]);
                            positionModel.Reg_TempID = reader["Reg_TempID"] == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(reader["Reg_TempID"]).Trim()) ? 0 : Convert.ToInt32(reader["Reg_TempID"]);
                            positionModel.EmploymentTypeID = reader["EmploymentTypeID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EmploymentTypeID"]);
                            positionModel.JobFamilyID = reader["JobFamilyID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["JobFamilyID"]);
                            positionModel.IsDeleted = Convert.ToBoolean(reader["IsDeleted"] == DBNull.Value ? 0 : reader["IsDeleted"]);
                            positionModel.IsAcademic = Convert.ToBoolean(reader["IsAcademic"] == DBNull.Value ? 0 : reader["IsAcademic"]);
                            positionModelList.Add(positionModel);
                        }
                    }
                }
                // Finally, we return our list of position
                return positionModelList;
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching position List.", exception);
                throw;
            }
            finally
            {

            }
        }
    }
}
