﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.Entities;
using HRMS.Entities.General;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Globalization;

namespace HRMS.DataAccess
{
    public class HolidayDB : DBHelper
    {
        public SqlConnection sqlConnection { get; set; }
        public SqlCommand sqlCommand { get; set; }
        public SqlDataReader sqlReader { get; set; }
      //  public string connectionString = ConfigurationManager.ConnectionStrings["hrmsconnection"].ConnectionString;
        public OperationDetails InsertHoliday(HolidayModel HolidayModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@HolidayName",HolidayModel.HolidayName) ,
                      new SqlParameter("@From",GeneralDB.CommonDB.SetCulturedDate( HolidayModel.From)),
                      new SqlParameter("@to", GeneralDB.CommonDB.SetCulturedDate(HolidayModel.to)),
                      new SqlParameter("@HolidayName_2", HolidayModel.HolidayName_2) ,
                      new SqlParameter("@HolidayName_3", HolidayModel.HolidayName_3),
                       new SqlParameter("@CompanyIDs", HolidayModel.CompanyIDs),
                        new SqlParameter("@UserID", HolidayModel.CreatedBy),
                         Output,OutputMessage
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                  "stp_Add_GEN_Holiday", parameters);
                int output = Convert.ToInt32(Output.Value.ToString());
                //new OperationDetails()
                // return new OperationDetails(output > 0 ? true : false, OutputMessage.Value.ToString(), null, output);
                operationDetails.CssClass = output > 0 ? "success" : "error";
                operationDetails.Message = OutputMessage.Value.ToString();
                operationDetails.Success = output > 0 ? true : false;
                return operationDetails;
            }
            catch (Exception exception)
            {
                operationDetails.Message = "Error : while updating Holiday.";
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                return operationDetails;
                //return new OperationDetails(false, "Error : while updating Holiday.", exception);
                throw exception;
            }
            finally
            {

            }
        }


        public OperationDetails UpdateHoliday(HolidayModel HolidayModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.Output;
                SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
                OutputMessage.Direction = ParameterDirection.Output;
                SqlParameter[] parameters =
                    {
                         new SqlParameter("@HolidayID",HolidayModel.HolidayID) ,
                          new SqlParameter("@HolidayName",HolidayModel.HolidayName) ,
                      new SqlParameter("@From",GeneralDB.CommonDB.SetCulturedDate(HolidayModel.From)),
                      new SqlParameter("@to",GeneralDB.CommonDB.SetCulturedDate(HolidayModel.to)),
                      new SqlParameter("@HolidayName_2", HolidayModel.HolidayName_2) ,
                      new SqlParameter("@HolidayName_3", HolidayModel.HolidayName_3),
                        new SqlParameter("@CompanyID", HolidayModel.CompanyId),
                             new SqlParameter("@UserID", HolidayModel.ModifiedBy),
                        Output,OutputMessage
                    };


                // SqlDataReader reader = sqlCommand.ExecuteReader();           

                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure,
                                   "stp_Update_GEN_Holiday", parameters);
                int output = Convert.ToInt32(Output.Value.ToString());
                //new OperationDetails()
                // return new OperationDetails(output > 0 ? true : false, OutputMessage.Value.ToString(), null, output);
                operationDetails.CssClass = output > 0 ? "success" : "error";
                operationDetails.Message = OutputMessage.Value.ToString();
                operationDetails.Success = output > 0 ? true : false;
                return operationDetails;
            }
            catch (Exception exception)
            {
                operationDetails.Message = "Error : while updating Holiday.";
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                return operationDetails;
                //return new OperationDetails(false, "Error : while updating Holiday.", exception);
                throw exception;
            }
            finally
            {

            }
        }


        public OperationDetails DeleteHolidayById(int HolidayId, int userID)
        {
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.Output;
            OperationDetails operationDetails = new OperationDetails();
            SqlParameter OutputMessage = new SqlParameter("@OutputMessage", SqlDbType.NVarChar, 500);
            OutputMessage.Direction = ParameterDirection.Output;
            try
            {
                SqlParameter[] parameters =
                    {
                      new SqlParameter("@HolidayID",HolidayId),
                        new SqlParameter("@UserID",userID),Output,OutputMessage
                    };
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "stp_Delete_GEN_HolidayByHolidayID", parameters);
                int output = Convert.ToInt32(Output.Value.ToString());
                operationDetails.CssClass = output > 0 ? "success" : "error";
                operationDetails.Message = OutputMessage.Value.ToString();
                operationDetails.Success = output > 0 ? true : false;
                return operationDetails;

            }
            catch (Exception exception)
            {               
                operationDetails.Message = "Error : while deleting Holiday.";
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
            
                return operationDetails;
                throw exception;
            }
            finally
            {

            }
        }

        public List<HolidayModel> GetHolidayList(int?companyID,int? holidayID, int userId)
        {
            // Create a list to hold the Holiday		
            List<HolidayModel> HolidayModelList = new List<HolidayModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("stp_Get_GEN_Holiday", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", companyID==null?(object)DBNull.Value: companyID));
                sqlCommand.Parameters.Add(new SqlParameter("@HolidayID", holidayID == null ? (object)DBNull.Value : holidayID));
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HolidayModel holidayModel;
                    while (sqlDataReader.Read())
                    {
                        holidayModel = new HolidayModel();
                        holidayModel.HolidayID = Convert.ToInt32(sqlDataReader["HolidayID"].ToString());
                        holidayModel.HolidayName = sqlDataReader["HolidayName"].ToString().Trim();
                        holidayModel.HolidayName_2 = Convert.ToString(sqlDataReader["HolidayName_2"]).Trim();
                        holidayModel.HolidayName_3 = Convert.ToString(sqlDataReader["HolidayName_3"]).Trim();
                        holidayModel.From = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["From"].ToString());
                        holidayModel.to = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["to"].ToString());
                        holidayModel.CompanyId = string.IsNullOrEmpty(sqlDataReader["CompanyID"].ToString()) ? 0 : Convert.ToInt32(sqlDataReader["CompanyID"].ToString());
                        holidayModel.CompanyName = Convert.ToString(sqlDataReader["CompanyName"]).Trim();
                        HolidayModelList.Add(holidayModel);
                    }


                }
            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching HolidayModelList.", exception);
                throw exception;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return HolidayModelList;
        }

        public HolidayModel HolidayById(int HolidayId)
        {
            try
            {
                HolidayModel HolidayModel = new HolidayModel();
                using (SqlDataReader reader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "stp_Get_GEN_HolidayByHolidayID", new SqlParameter("@HolidayID", HolidayId)))
                {
                    // Check if the reader returned any rows
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            HolidayModel = new HolidayModel();
                            HolidayModel.HolidayID = Convert.ToInt32(reader["HolidayID"].ToString());
                            HolidayModel.HolidayName = reader["HolidayName"].ToString();
                            HolidayModel.HolidayName_2 = Convert.ToString(reader["HolidayName_2"]);
                            HolidayModel.HolidayName_3 = Convert.ToString(reader["HolidayName_3"]);
                            HolidayModel.From = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["From"].ToString());
                            HolidayModel.to = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(reader["to"].ToString());
                            HolidayModel.CompanyId = string.IsNullOrEmpty(reader["CompanyID"].ToString()) ? 0 : Convert.ToInt32(reader["CompanyID"].ToString());
                            HolidayModel.IsUsed =  bool.Parse(reader["IsUsed"].ToString()); 
                        }
                    }
                }
                return HolidayModel;
                // Finally, we return HolidayModel

            }
            catch (Exception exception)
            {
                // return new OperationDetails(false, "Error : while fetching HolidayModel.", exception);
                throw exception;
            }
            finally
            {


            }
        }

        public List<HolidayModel> GetHolidayDates(int holidayID)
        {
            // Create a list to hold the Holiday		
            List<HolidayModel> HolidayModelList = new List<HolidayModel>();
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Hr_Stp_GetHolidayDates", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@HolidayID", holidayID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    HolidayModel holidayModel;
                    while (sqlDataReader.Read())
                    {
                        holidayModel = new HolidayModel();
                        //  holidayModel.HolidayID = Convert.ToInt32(sqlDataReader["HolidayID"].ToString());
                        holidayModel.HolidayName = sqlDataReader["HolidayName"].ToString().Trim();
                        holidayModel.From = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["HolidayDate"].ToString());
                        // holidayModel.to = GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(sqlDataReader["to"].ToString());
                        HolidayModelList.Add(holidayModel);
                    }


                }
            }
            catch (Exception ex)
            {
                // return new OperationDetails(false, "Error : while fetching HolidayModelList.", exception);
                throw ex;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

            return HolidayModelList;
        }
    }
}
