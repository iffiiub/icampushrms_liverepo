﻿CostCenterDetails = [];
ModifiedCostCenterDetails = [];

$(document).ready(function () {
    var PayCategoryID = $("#CategoryID").val();
    GetAllInstallments(PayCategoryID);
});

function GetAllInstallments(id) {
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/GeneralControl/GetCostCenterDetails?PayCategoryId=' + id,
        success: function (data) {
            CostCenterDetails = data.list;
            ModifiedCostCenterDetails = data.list;
        },
        error: function (data) { }
    });
}


function EditCostCenterPercentage(e) {

    var id = $(e).closest("tr").attr('data-rowId');
    $("#tbl_CostCenterDetails tbody tr").each(function (i, item) {
        var EditedId = $(this).attr('data-rowId');
        if (id == EditedId) {
            $.each((CostCenterDetails), function (k, item1) {
                if (item1.DimensionValueID == id) {
                    $(item).html('<td class="col-sm-5">' + item1.DimensionName + ' - ' + item1.DimensionValueName + '</td>' +
                           '<td class="col-sm-4"><div class="col-sm-8 padding-0"><input type="textbox" class="form-control positiveOnly" id="Percentage' + item1.DimensionValueID + '" Value="' + parseFloat(item1.Percentage).toFixed(2) + '"></div></td>' +
                          '<td class="center-text-align col-md-3">' +
                           '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>' +
                       '</td>');
                }
            });
        }
    });
    bindPercentageOnly('.positiveOnly');
}

function CancelNewRow(e) {
    var id = $(e).closest("tr").attr('data-rowId');
    $("#tbl_CostCenterDetails tbody tr").each(function (i, item) {
        var EditedId = $(this).attr('data-rowId');
        if (id == EditedId) {
            $.each((CostCenterDetails), function (k, item1) {
                if (item1.DimensionValueID == id) {
                    $(item).html('<td>' + item1.DimensionName + ' - ' + item1.DimensionValueName + '</td>' +
                           '<td>' + parseFloat(item1.Percentage).toFixed(2) + '%</td>' +
                           '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" data-id="' + item1.DimensionValueID + '" onclick="EditCostCenterPercentage(this)" title="Edit"><i class="fa fa-pencil"></i> </a>' +
                           '</td>');
                }
            });
        }
    });
}

function saveRow(e) {

    var DimensionValueID = $(e).closest("tr").attr('data-rowId');
    var TotalPercentage = 0;
    for (var i = 0; i < ModifiedCostCenterDetails.length; i++) {
        if (ModifiedCostCenterDetails[i].DimensionValueID != DimensionValueID) {
            TotalPercentage = TotalPercentage + parseFloat(ModifiedCostCenterDetails[i].Percentage)
        }

    }
    TotalPercentage = TotalPercentage + parseFloat($("#Percentage" + parseFloat(DimensionValueID)).val())
    if (TotalPercentage <= 100) {
        var DimensionValueID = $(e).closest("tr").attr('data-rowId');
        for (var i = 0; i < ModifiedCostCenterDetails.length; i++) {
            if (ModifiedCostCenterDetails[i].DimensionValueID == parseFloat(DimensionValueID)) {
                ModifiedCostCenterDetails[i].Percentage = $("#Percentage" + parseFloat(DimensionValueID)).val();
                $("#tbl_CostCenterDetails tbody tr").each(function (i, item) {
                    var EditedId = $(this).attr('data-rowId');
                    if (DimensionValueID == EditedId) {

                        $.each((ModifiedCostCenterDetails), function (k, item1) {
                            if (item1.DimensionValueID == DimensionValueID) {
                                $(item).html('<td>' + item1.DimensionName + ' - ' + item1.DimensionValueName + '</td>' +
                                      '<td>' + parseFloat(item1.Percentage).toFixed(2) + '</td>' +
                                      '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditCostCenterPercentage(this)" data-id=' + item1.DimensionValueID + ' title="Edit"><i class="fa fa-pencil"></i> </a></td>');
                            }
                        });
                    }
                });
                break;
            }
        }
    }
    else {
        ShowMessage("error", "Percentage cannot exceed more than 100%.");
    }


}


function SavePayCategoryDetails() {    
    var CategoryID = $("#CategoryID").val();
    var CategoryName_1 = $("#CategoryName_1").val();
    var CategoryName_2 = $("#CategoryName_2").val();
    var CategoryName_3 = $("#CategoryName_3").val();
    var TotalPercentage = 0;
    for (var i = 0; i < ModifiedCostCenterDetails.length; i++) {
        TotalPercentage = TotalPercentage + parseFloat(ModifiedCostCenterDetails[i].Percentage)
    }
    $("#CostCenters").val(JSON.stringify(ModifiedCostCenterDetails));
    if ($("#isCostCenterValid").val() == "False")
    {
        TotalPercentage = 100;
    }
    var $form = $('form#frmAddEditPayCategory');
    if ($form.valid()) {
        //Ajax call here       
        if (TotalPercentage == 100) {
            $form.submit();
        }
        else {
            ShowMessage("error", "Percentage should be equal to 100%.");
        }
    }

}