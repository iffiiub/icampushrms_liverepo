﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridPoliceClearance();
    }
});

var oTableChannel;

function getGridPoliceClearance() {
    oTableChannel = $('#tbl_PoliceClearanceList').dataTable({
        "sAjaxSource": "/PoliceClearance/GetPoliceClearanceCertList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document Name/No", 'width': '20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", "bSortable": false, 'width': '20%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PoliceClearance/GetPoliceClearanceCertList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_PoliceClearanceList");
        }
    });
}

$("#btn_ExportToExcel").click(function () { ExportToExcelEmirates(); });
$("#btn_ExportToPdf").click(function () { ExportToPdfEmirates(); });

function AddPoliceCleranceCert() {
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/PoliceClearance/AddPoliceClearanceCert");
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add PoliceClearance Cert.');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditPoliceCleranceCert(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/PoliceClearance/EditPoliceClearanceCert/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit PoliceClearance Cert.');

}


function DeletePoliceCleranceCert(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/PoliceClearance/DeletePoliceClearanceCert',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridPoliceClearance();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewPoliceClearanceCert(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/PoliceClearance/ViewPoliceClearanceCert/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View PoliceClearance Cert.');

}

function ExportToExcelEmirates() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Emirates/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdfEmirates() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Emirates/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function CheckPrimary(e,id,docId) {
    if ($(e).is(":checked")) {
        $.ajax({
            type: 'POST',
            data: { DocTypeId: id, DocId: docId },
            url: '/Document/CheckPrimary',
            success: function (data) {
                if (data.result == 'success')
                    //ShowMessage(data.result, data.resultMessage);
                    $("#warningMessage").removeClass("hidden");
            },
            error: function (data) { }
        });

    }
    else {
        $("#warningMessage").addClass("hidden");
    }
    
}