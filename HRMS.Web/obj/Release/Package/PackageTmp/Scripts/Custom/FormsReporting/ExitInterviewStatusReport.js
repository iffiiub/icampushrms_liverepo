﻿$(document).ready(function () {
    loadExitInterviewStatusReportGrid();
});

var oTableChannel;

function loadExitInterviewStatusReportGrid() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();

    oTableChannel = $('#tblExitInterviewStatusReportGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetExitInterviewStatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "requestId", "value": requestId },
                { "name": "companyId", "value": companyId },
                { "name": "employeeId", "value": employeeId },
                { "name": "departmentId", "value": departmentId },
                { "name": "requestStatusId", "value": requestStatusId },
                { "name": "dateFrom", "value": dateFrom },
                { "name": "dateTo", "value": dateTo }
            );
        },
        "aoColumns": [
            { "mData": "BUName", "sTitle": "BU Name", 'sWidth': '15%' },
            { "mData": "DepartmentName", "sTitle": "Department", 'sWidth': '15%' },
            { "mData": "RequestID", "sTitle": "Request No", 'sWidth': '10%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '10%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '15%' },
            { "mData": "RequestDate", "sTitle": "Request Date", 'sWidth': '10%' },
            { "mData": "CurrentStatus", "sTitle": "Current Status", 'sWidth': '10%' },
            { "mData": "CurrentApprover", "sTitle": "Pending With", 'sWidth': '15%' }                  
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

$('#btnSearch').on('click', function () {
    loadExitInterviewStatusReportGrid();
});

$("#RequestID").on('keypress', function (e) {
    if (e.which == 13) {
        loadExitInterviewStatusReportGrid();
    }
});

function ExportToExcel() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();
    var recordCount = $("#tblExitInterviewStatusReportGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelExitInterviewStatusReport?requestId=" + requestId + "&companyId=" + companyId + "&employeeId=" + employeeId + "&departmentId=" + departmentId + "&requestStatusId=" + requestStatusId + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo);
    }
    else
        ShowMessage("warning", "No data for export!");
}
