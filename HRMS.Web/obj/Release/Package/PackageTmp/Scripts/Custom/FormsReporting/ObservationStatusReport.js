﻿$(document).ready(function () {

    $('#btnSearch').on('click', function () {
        loadObservationStatusReportGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            loadObservationStatusReportGrid();
        }
    });
});

var oTableChannel;

function loadObservationStatusReportGrid() {
    var companyId = $("#ddlOrganization").val();
    var departmentId = $("#ddlDepartment").val();
    var statusId = $("#ddlStatus").val();
    var fromDate = $("#DateFrom").val();
    var toDate = $("#DateTo").val(); 


    oTableChannel = $('#tblObservationStatusGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetObservationtatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "companyId", "value": companyId },
                { "name": "departmentId", "value": departmentId },
                { "name": "statusId", "value": statusId },
                { "name": "fromDate", "value": fromDate },
                { "name": "toDate", "value": toDate }
            );
        },
        "aoColumns": [
            { "mData": "OrganizationName", "sTitle": "BU Name", 'sWidth': '10%' },
            { "mData": "Department", "sTitle": "Department", 'sWidth': '5%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '5%' },
            { "mData": "IsObservationProcessInitiated", "sTitle": "Is Observation Process Initiated", 'sWidth': '5%' },
            { "mData": "Year", "sTitle": "Year", 'sWidth': '10%' },
            { "mData": "TotalScore", "sTitle": "Total Score", 'sWidth': '5%' },
            { "mData": "TotalRating", "sTitle": "Total Rating", 'sWidth': '5%' },
            { "mData": "ObservationVisitDate", "sTitle": "Observation Visit Date", 'sWidth': '10%' },
            { "mData": "PreObservationRequest", "sTitle": "Pre-Observation Request", 'sWidth': '5%' },
            { "mData": "PostObservationRequest", "sTitle": "Post-Observation Request", 'sWidth': '5%' },
            { "mData": "ObservationRequest", "sTitle": "Observation Request", 'sWidth': '5%' },
            { "mData": "ObservationSignOffDate", "sTitle": "Observation Sign Off Date", 'sWidth': '10%' },
            { "mData": "CurrentStatus", "sTitle": "Current Status", 'sWidth': '5%' },
            { "mData": "CurrentApprover", "sTitle": "Current Approver", 'sWidth': '5%' }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function ExportToExcel() {
    var companyId = $("#ddlOrganization").val();
    var departmentId = $("#ddlDepartment").val();
    var statusId = $("#ddlStatus").val();
    var fromDate = $("#DateFrom").val();
    var toDate = $("#DateTo").val();
    var recordCount = $("#tblObservationStatusGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelObservationStatusReport?companyId=" + companyId + "&departmentId=" + departmentId + "&statusId=" + statusId + "&fromDate=" + fromDate + "&toDate=" + toDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}