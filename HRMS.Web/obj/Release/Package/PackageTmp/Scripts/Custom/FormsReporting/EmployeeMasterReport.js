﻿$(document).ready(function () {
    loadEmployeeMasterReportGrid();
});

var oTableChannel;

function loadEmployeeMasterReportGrid() {
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    if ($("#ddlIsActive").val() == '')
        var isActive = null;
    else
        var isActive = $("#ddlIsActive").val() == "1"; 

    oTableChannel = $('#tblEmployeeMasterReportGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetEmployeeMasterReportData",
        "fnServerParams": function (aoData) {
            aoData.push(             
                { "name": "companyId", "value": companyId },
                { "name": "employeeId", "value": employeeId },
                { "name": "departmentId", "value": departmentId },
                { "name": "isActive", "value": isActive }
            );
        },
        "aoColumns": [
            { "mData": "BusinessUnit", "sTitle": "Business Unit"},
            { "mData": "OperatingUnitID", "sTitle": "Operating Unit ID"},
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID" },
            { "mData": "FirstName", "sTitle": "First Name"},
            { "mData": "MiddleName", "sTitle": "Middle Name"},
            { "mData": "LastName", "sTitle": "Last Name"},
            { "mData": "EmployeeName", "sTitle": "Employee Name"},
            { "mData": "EmployeeType", "sTitle": "Employee Type"},            
            { "mData": "ExpectedJoiningDate", "sTitle": "Expected Joining Date"},
            { "mData": "HireDate", "sTitle": "Hire Date"},
            { "mData": "ProbationMonths", "sTitle": "Probation Months"},
            { "mData": "NoticedPeriod", "sTitle": "Notice Period"},
            { "mData": "Religion", "sTitle": "Religion"},
            { "mData": "DOB", "sTitle": "DOB"},
            { "mData": "BirthCity", "sTitle": "Birth City"},
            { "mData": "BirthCountry", "sTitle": "Birth Country"},
            { "mData": "Citizenship", "sTitle": "Citizenship"},
            { "mData": "MaritalStatus", "sTitle": "Marital Status"},
            { "mData": "Gender", "sTitle": "Gender"},
            { "mData": "BloodGroup", "sTitle": "Blood Group"},
            { "mData": "MotherName", "sTitle": "Mother Name"},
            { "mData": "PositionOfferLetter", "sTitle": "Position Offer Letter"},
            { "mData": "PositionLabourContract", "sTitle": "Position Labour Contract"},
            { "mData": "Email", "sTitle": "Email"},
            { "mData": "UserName", "sTitle": "User Name"},
            { "mData": "DepartmentName", "sTitle": "Department"},
            { "mData": "DesignationTitle", "sTitle": "Designation Title"},
            { "mData": "Location", "sTitle": "Location", 'sWidth':'5%'},
            { "mData": "Category", "sTitle": "Category"},
            { "mData": "SubCategory", "sTitle": "SubCategory"},
            { "mData": "SalaryBasis", "sTitle": "Salary Basis"},
            //{ "mData": "FirstLineManagerOracleNo", "sTitle": "First Line Manager Oracle No"},
            //{ "mData": "FirstLineManagerName", "sTitle": "First Line Manager Name" },
            //{ "mData": "FirstLineManagerEmail", "sTitle": "First Line Manager Email"},
            //{ "mData": "FirstLineManagerAD", "sTitle": "First Line Manager AD"},
            { "mData": "SponsorName", "sTitle": "Sponsor Name"},
            { "mData": "IBAN", "sTitle": "IBAN"},
            { "mData": "BankName", "sTitle": "Bank Name"},
            { "mData": "AnnualLeaveEntitlement", "sTitle": "Annual Leave Entitlement"},
            { "mData": "AnnualLeaveCalculation", "sTitle": "Annual Leave Calculation"},
            { "mData": "TicketEntitlement", "sTitle": "Ticket Entitlement"},
            { "mData": "TicketEntitlementFamily", "sTitle": "Ticket Entitlement Family"},
            { "mData": "TicketCapedAmount", "sTitle": "Ticket Caped Amount"},
            { "mData": "TicketClass", "sTitle": "Ticket Class"},
            { "mData": "TicketFrequency", "sTitle": "Ticket Frequency"},
            { "mData": "HomeTownForTicket", "sTitle": "Home Town For Ticket"},
            { "mData": "MedicalInsurance", "sTitle": "Medical Insurance"},
            { "mData": "MedicalInsuranceFamily", "sTitle": "Medical Insurance Family"},
            { "mData": "ContactNumber", "sTitle": "Contact Number"},
            { "mData": "PassportNo", "sTitle": "Passport No"},
            { "mData": "PassportExpiryDate", "sTitle": "Passport Expiry Date"},
            { "mData": "PassportCustody", "sTitle": "Passport Custody"},
            { "mData": "VisaNumber", "sTitle": "Visa Number"},
            { "mData": "VisaExpiryDate", "sTitle": "Visa Expiry Date"},
            { "mData": "EmiratesIDNumber", "sTitle": "Emirates ID Number"},
            { "mData": "EmiratesIDExpiryDate", "sTitle": "Emirates ID Expiry Date"},
            { "mData": "LabourCardNumber", "sTitle": "Labour Card Number"},
            { "mData": "LabourCardExpiryDate", "sTitle": "Labour Card Expiry Date"},
            { "mData": "SecondLineManagerName", "sTitle": "Second Line Manager Name"},
            { "mData": "SecondLineManager", "sTitle": "Second Line Manager ID" },
            { "mData": "HODName", "sTitle": "HOD Name"},
            { "mData": "HODOracleNumber", "sTitle": "HOD ID"},
            { "mData": "BUHeadName", "sTitle": "BU Head Name"},
            { "mData": "BUHeadOracleNumber", "sTitle": "BU Head ID"}
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
      //  "scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

$('#btnSearch').on('click', function () {
    loadEmployeeMasterReportGrid();
});

$("#RequestID").on('keypress', function (e) {
    if (e.which == 13) {
        loadEmployeeMasterReportGrid();
    }
});

function ExportToExcel() {
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var isActive = $("#ddlIsActive").val();
    var recordCount = $("#tblEmployeeMasterReportGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelEmployeeMasterReport?companyId=" + companyId + "&employeeId=" + employeeId + "&departmentId=" + departmentId + "&isActive=" + isActive);
    }
    else
        ShowMessage("warning", "No data for export!");
}