﻿$(document).ready(function () {
    getGrid();
});

var oTableChannel;

function getGrid() {
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();

    oTableChannel = $('#tblExpenseClaimStatusReportGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/ExpenseClaimStatusReportGrid",
        "fnServerParams": function (aoData) {
            aoData.push(
                        { "name": "RequestID", "value": RequestID },
                        { "name": "CompanyID", "value": CompanyID },
                        { "name": "DepartmentID", "value": DepartmentID },
                        { "name": "CurrentStatusID", "value": CurrentStatusID },
                        { "name": "FromDate", "value": FromDate },
                        { "name": "ToDate", "value": ToDate }
                        );
        },
        "aoColumns": [
                         { "mData": "BU_Name", "sTitle": "BU Name" },
                         { "mData": "Department", "sTitle": "Department" },
                         { "mData": "RequestID", "sTitle": "Request ID" },
                         { "mData": "RequestDate", "sTitle": "Request Date" },
                         { "mData": "EmployeeID", "sTitle": "Employee ID" },
                         { "mData": "EmployeeName", "sTitle": "Employee Name" },                        
                         { "mData": "Designation", "sTitle": "Designation" },
                         { "mData": "ProjectName", "sTitle": "Project Name" },
                         { "mData": "TotalExpenseAmount", "sTitle": "Total Expense" },
                         { "mData": "Amount", "sTitle": "Amount" },                                               
                         { "mData": "CurrentStatus", "sTitle": "Current Status" },
                         { "mData": "CurrentApprover", "sTitle": "Current Approver" }
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

$('#btnSearch').on('click', function () {
    getGrid();
});

$("#RequestID").on('keypress', function (e) {
    if (e.which == 13) {
        getGrid();
    }
});

function ExportToExcel() {
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    var RecordCount = $("#tblExpenseClaimStatusReportGrid").DataTable().rows("tr").data().length;

    if (RecordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelExpenseClaimStatusReport?RequestID=" + RequestID + "&CompanyID=" + CompanyID +
            "&DepartmentID=" + DepartmentID + "&CurrentStatusID=" + CurrentStatusID + "&FromDate=" + FromDate + "&ToDate=" + ToDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}
