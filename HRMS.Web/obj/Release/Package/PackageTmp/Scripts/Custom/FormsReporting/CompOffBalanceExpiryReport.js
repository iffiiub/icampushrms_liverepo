﻿$(document).ready(function () {
    getGrid();  
});

var oTableChannel;

function getGrid() {    
    var RequestID=$("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();

    oTableChannel = $('#tblCompOffBalanceGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetCompOffBalanceReportGrid",
        "fnServerParams": function (aoData) {
            aoData.push(
                        { "name": "RequestID", "value": RequestID },
                        { "name": "CompanyID", "value": CompanyID },
                        { "name": "DepartmentID", "value": DepartmentID },                       
                        { "name": "FromDate", "value": FromDate },
                        { "name": "ToDate", "value": ToDate }
                        );
        },
        "aoColumns": [
                         { "mData": "BU_Name", "sTitle": "BU Name", 'width': '8%' },                        
                         { "mData": "EmployeeID", "sTitle": "Employee ID", 'width': '8%' },
                         { "mData": "EmployeeName", "sTitle": "Employee Name", 'width': '8%' },
                         { "mData": "DepartmentName", "sTitle": "Department", 'width': '8%' },
                         { "mData": "Designation", "sTitle": "Designation", 'width': '8%' },
                         { "mData": "JoiningDate", "sTitle": "Joining Date", 'width': '8%' },
                         { "mData": "Email", "sTitle": "Email", 'width': '8%' },
                         { "mData": "ApprovedDate", "sTitle": "Fully Approved Date", 'width': '5%' },
                         { "mData": "CompOffBalance", "sTitle": "Comp-Off Balance (Hours)", 'width': '9%' },
                         { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '5%' }
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {          
        }
    });
}

$('#btnSearch').on('click', function () {
    getGrid();
});

$("#RequestID").on('keypress', function (e) {
    if (e.which == 13) {
        getGrid();
    }
});

function ExportToExcel() {    
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();  
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    var RecordCount = $("#tblCompOffBalanceGrid").DataTable().rows("tr").data().length;

    if (RecordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelCompOffBalanceReport?RequestID=" + RequestID + "&CompanyID=" + CompanyID +
            "&DepartmentID=" + DepartmentID + "&FromDate=" + FromDate + "&ToDate=" + ToDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}