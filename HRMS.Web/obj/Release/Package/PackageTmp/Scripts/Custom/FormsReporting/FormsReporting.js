﻿function ViewDetails(id,ControllerName) {
    var url = "/" + ControllerName + "/SetFormProcessSesssion";
    $.ajax({
        type: 'POST',
        url: url,
        data: { id: id },
        dataType: 'json',
        success: function (result) {
            if (result.id != null)
                location.href = "/" + ControllerName + "/ViewDetails";
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
}