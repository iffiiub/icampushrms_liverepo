﻿$(document).ready(function () {
    loadEmployeeProfileStatusReportGrid();
});

var oTableChannel;

function loadEmployeeProfileStatusReportGrid() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();

    oTableChannel = $('#tblEmployeeProfileStatusReportGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetEmployeeProfileStatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "requestId", "value": requestId },
                { "name": "companyId", "value": companyId },
                { "name": "employeeId", "value": employeeId },
                { "name": "departmentId", "value": departmentId },
                { "name": "requestStatusId", "value": requestStatusId },
                { "name": "dateFrom", "value": dateFrom },
                { "name": "dateTo", "value": dateTo }
            );
        },
        "aoColumns": [
            { "mData": "BUName", "sTitle": "BU Name", 'sWidth': '10%' },
            { "mData": "R1ReferenceNo", "sTitle": "R1 Reference No", 'sWidth': '10%' },
            { "mData": "Requester", "sTitle": "Requester", 'sWidth': '10%' },
            { "mData": "RequestDate", "sTitle": "Request Date", 'sWidth': '5%' },
            { "mData": "EmployeeProfile", "sTitle": "Profile Request ID", 'sWidth': '5%' },
            { "mData": "EmployeeID", "sTitle": "Employee ID", 'sWidth': '10%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "DepartmentName", "sTitle": "Department", 'sWidth': '10%' },
            { "mData": "Project", "sTitle": "Project", 'sWidth': '5%' },
            { "mData": "Designation", "sTitle": "Designation", 'sWidth': '10%' },
            { "mData": "ExpectedJoiningDate", "sTitle": "Expected Joining Date", 'sWidth': '10%' },
            { "mData": "CurrentStatus", "sTitle": "Current Status", 'sWidth': '10%' },
            { "mData": "CurrentApprover", "sTitle": "Current Approver", 'sWidth': '10%' }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

$('#btnSearch').on('click', function () {
    loadEmployeeProfileStatusReportGrid();
});

$("#RequestID").on('keypress', function (e) {
    if (e.which == 13) {
        loadEmployeeProfileStatusReportGrid();
    }
});

function ExportToExcel() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();
    var recordCount = $("#tblEmployeeProfileStatusReportGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelEmployeeProfileStatusReport?requestId=" + requestId + "&companyId=" + companyId + "&employeeId=" + employeeId + "&departmentId=" + departmentId + "&requestStatusId=" + requestStatusId + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo);
    }
    else
        ShowMessage("warning", "No data for export!");
}