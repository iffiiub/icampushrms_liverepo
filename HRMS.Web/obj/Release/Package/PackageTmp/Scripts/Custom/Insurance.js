﻿$(document).ready(function () {
    if ($("#EmployeeID").val() != "" && $("#EmployeeID").val() != 0) {
        var id = $("#EmployeeID").val();
        LoadEmployeeDetails(id);
        $("#hdnEmployee_ID").val(id);
        getGridInsdependence(id);
    }

    $("form#frmAddInsurance").submit(function () {
        if (parseInt($("#EmployeeID").val()) > 0)
            return true;
        else {
            ShowMessage("error", "Please select employee first");
            return false;
        }
    })
});

$("#btn_addDependencies").click(function () {
    if ($("#EmployeeID").val() != "" && $("#EmployeeID").val() !== undefined && $("#EmployeeID").val() != "0") {
        var EmployeeId = $("#EmployeeID").val();
        $("#ActiveEmployee").val(EmployeeId);
        AddDependencies();
    }
    else {
        ShowMessage("error", "Please select employee.")
    }
});

var oTableChannel;

$("#TotalAmount").change(function () {
    CalculateDeduction();
});

$("#TotalAmountBySchool").change(function () {
    CalculateDeduction();
});

function CalculateDeduction() {
    var Total = $("#TotalAmount").val();
    var AmountBySchool = $("#TotalAmountBySchool").val();
    // alert(Total + ' ' + AmountBySchool)
    if (!isNaN(parseInt(Total, 10)) && !isNaN(parseInt(AmountBySchool, 10))) {
        var AmountBySchool = parseInt(AmountBySchool, 10);
        var Total = parseInt(Total, 10);
        if (AmountBySchool > 0 && Total > AmountBySchool) {

            var DeductionAmount = Total - AmountBySchool;
            $("#Deduction").val(DeductionAmount);
        }
    }
    else {
        DeductionAmount = 0.0;
        $("#Deduction").val(DeductionAmount);

    }
}

function EditInsuranceDependence(id) {
    //var buttonName = id > 0 ? "Update": "Save";
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Insurance/EditInsdependence/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Insurance Dependent');
    });

}

function getGridInsdependence(empid) {
    oTableChannel = $('#tbl_InsuranceDependentList').dataTable({
        "sAjaxSource": "/Insurance/GetInsdependenceList",
        "aoColumns": [
        { "mData": "Name", "sTitle": "Name", "width": "30%" },
        { "mData": "RelationName", "sTitle": "Relation Name", "width": "15%" },
        { "mData": "CardNumber", "sTitle": "Card Number", "width": "20%" },
        { "mData": "Note", "sTitle": "Note", "bSortable": false, "width": "20%" },
        { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", "bSortable": false, "width": "15%" }
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "ajax": "/Insurance/GetInsdependenceList",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {

        },
        "bSortCellsTop": true

    });
}

function AddDependencies() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Insurance/AddInsdependence", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add  Insurance Dependent');

    });
}

function EditAbsent(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/EditAbsent/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Edit Absent');
}

function DeleteInsuranceDependence(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Insurance/DeleteInsdependence',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully.");
                var id = $("#hdnEmployee_ID").val();
                getGridInsdependence(id);
            },
            error: function (data) { }
        });
    });
}