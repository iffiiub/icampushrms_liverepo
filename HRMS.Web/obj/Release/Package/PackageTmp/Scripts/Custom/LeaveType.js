﻿$(document).ready(function () {


    getGrid();
    $("#btn_add").click(function () {
        EditChannel(0);
    });


});

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_LeaveType').dataTable({
        "sAjaxSource": "/LeaveType/GetLeaveTypeList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "search", "value": searchval });
        },
        "aoColumns": [

            { "mData": "LeaveTypeId", "bVisible": false, "sTitle": "Leave Type Id" },
            { "mData": "LeaveTypeName", "sTitle": "Name" },
            { "mData": "NoOfDays", "sTitle": "No Of Days" },
            { "mData": "RepeatedFor", "sTitle": "Repeated<br/>For" },
            { "mData": "ApplicableAfterDigits", "sTitle": "Applicable<br/>After" },
            { "mData": "ApplicableAfterTypeID", "sTitle": "Applicable<br/>After Type" },
            { "mData": "GenderName_1", "sTitle": "Gender" },
        { "mData": "ReligionName_1", "sTitle": "Religion" },
         { "mData": "NationalityName_1", "sTitle": "Nationality" },
        // { "mData": "AnnualLeave", "sTitle": "Annual<br/>Leave" },
        //{ "mData": "LifetimeLeave", "sTitle": "Lifetime<br/>Leave" },
        //{ "mData": "MonthlySplit", "sTitle": "Monthly<br/>Split" },
        { "mData": "Action", "sTitle": "Action", "bSortable": false ,"sClass":"ClsAction"}
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/LeaveType/GetLeaveTypeList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_LeaveType");
        },
        "bSortCellsTop": true
    });
}

function AddLeaveType() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/LeaveType/AddLeaveType");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Leave Type');
}


function EditChannel(id) {
    var buttonName = id > 0 ? "Update": "Submit";

    $("#modal_Loader").load("/LeaveType/Edit/" + id, function () {
        $("#myModal").modal("show");
        if (id == 0) {

            $("#modal_heading").text('Add Leave Type');
        }
        else {

            $("#modal_heading").text('Edit Leave Type');

        }
        //$("#form0").find('input[type="submit"]').val(buttonName);
    });
}


function Start() {
    $.blockUI();
}

function SucessLeaveType(arg) {
    $.unblockUI();
    ShowMessage(arg.responseJSON.result, arg.responseJSON.resultMessage);
    $("#myModal").modal("hide");
    getGrid();
}



function EditLeaveType(id) {
    var buttonName = id > 0 ? "Update": "Submit";

    //location.href = "/LeaveType/EditLeaveType/" + id;
    $("#modal_Loader").load("/LeaveType/Edit/" + id, function () {
    $("#myModal").modal("show");
    $("#modal_heading").text('Edit Leave Type');
    //$("#form0").find('input[type="submit"]').val(buttonName);
    });
}


function DeleteLeaveType(id) {
    
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/LeaveType/DeleteLeaveType',
            success: function (data) {
                getGrid();
                ShowMessage("success", data.resultMessage);
            },
            error: function (data) {
                ShowMessage("error", data.resultMessage);
            }
        });
    });
}
//}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewLeaveType(id) {
    //location.href = "/LeaveType/ViewLeaveType/" + id;
    $("#modal_Loader").load("/LeaveType/ViewLeaveType/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Leave Type Details');

}


//function ExportToExcel() {
//    if (oTableChannel.fnGetData().length > 0) {
//        window.location.assign("/Position/ExportToExcel");
//    }
//    else ShowMessage("warning", "No data for export!");
//}

//function ExportToPdf() {
//    if (oTableChannel.fnGetData().length > 0) {
//        window.location.assign("/Position/ExportToPdf");
//    }
//    else ShowMessage("warning", "No data for export!");
//}