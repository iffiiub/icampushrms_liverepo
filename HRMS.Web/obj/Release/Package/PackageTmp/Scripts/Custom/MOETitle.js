﻿$(document).ready(function () {
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));
    getGrid();

    $("#btn_add").click(function () {
        EditChannel(0);
    });

    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);
    $("#btnTextClear").click(function () {
        $('#txtSearch').val(""); // $('#txtSearch').val() == "";
        getGrid();
    });
    $("#btnTextSearch").click(function () {

        getGrid();
    });

});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_company').dataTable({
        "sAjaxSource": "/MOETitle/GetMOETitleList",
        "aoColumns": [

            //{ "mData": "MOETitleID", "bVisible": false, "sTitle": "MOETitleID" },
            { "mData": "MOETitleName_1", "bSortable": true, "sTitle": "MOE Title (En)", "width": "26%" },
           { "mData": "MOETitleName_2", "bSortable": true, "sTitle": "MOE Title (Ar)", "width": "20%" },
          // { "mData": "MOETitleName_3", "bVisible": false, "sTitle": "MOE Title Name (FR)" },
           { "mData": "MOETitleShortName_1", "sTitle": "Short Code (En)", "width": "20%" },
           { "mData": "MOETitleShortName_2", "sTitle": "Short Code (Ar)", "width": "20%" },
           //{ "mData": "MOETitleShortName_3", "bVisible": false, "sTitle": "Short Code (FR)" },
           { "mData": "Actions", "sTitle": "Actions" ,"bSortable": false,"width":"14%"}
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/MOETitle/GetMOETitleList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var buttonName = id > 0 ? "Update": "Submit";
   
    $("#modal_Loader").load("/MOETitle/Edit/" + id, function () {
        if (id == 0) {
            $("#modal_heading").text('Add MOE Title');
        }
        if (id != 0 && id != null) {
            $("#modal_heading").text('Edit MOE Title');
        }
        $("#myModal").modal("show");
        //$("#form0").find('input[type="submit"]').val(buttonName);
    });
   
}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/MOETitle/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}



//}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/MOETitle/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/MOETitle/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}