﻿$(document).ready(function () {
    getGrid();
    $("#btn_add").click(function () {
        //alert($("#Empid").val());
        window.location.href = '/Employee/EditInternalEmployment?Id=0&EmployeeID=' + $("#Empid").val();
    });
    $("#btn_BackProfile").click(function () { window.location.href = "/ViewEmployeeProfile/Index/" + $('#Empid').val(); });


});


var oTableChannel;

function getGrid() {

    oTableChannel = $('#tbl_InternalEmployment').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/Employee/GetInternalEmploymentList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": $("#Empid").val() });
        },
        "aoColumns": [
            { "mData": "Actions", "sTitle": "Actions", "sWidth": "10%" },
            { "mData": "InternalEmploymentID", "bVisible": false, "sTitle": "InternalEmploymentID" },
            { "mData": "FirstName", "sTitle": "First Name" },
            { "mData": "LastName", "sTitle": "Last Name" },
            { "mData": "StartDate", "sTitle": "Start Date" },
            { "mData": "EndDate", "sTitle": "End Date" },
            { "mData": "Department", "sTitle": "Department" },
            { "mData": "Position", "sTitle": "Position" },
            { "mData": "Location", "sTitle": "Location" }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/Employee/GetInternalEmploymentList",
        "aLengthMenu": [[10, 25, 50, 100, 150, 200, 250, 300, 350], [10, 25, 50, 100, 150, 200, 250, 300, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_InternalEmployment");
        }
    });
}

function EditChannel(id, employeeid) {

    window.location.href = '/Employee/EditInternalEmployment?Id=' + id + '&EmployeeID=' + employeeid;

}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Employee/DeleteInternalEmployment',
            success: function (data) {
                getGrid();
                ShowMessage(data.result, data.resultMessage);
            },
            error: function (data) {
                ShowMessage(data.result, data.resultMessage);
            }
        });
    });
}
