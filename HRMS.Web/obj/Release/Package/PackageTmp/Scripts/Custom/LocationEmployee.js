﻿$(document).ready(function () {   
    GetLocationEmployee();
    
 //   $("#EmployeeLocationDetailsDiv").load("/Location/LocationDetails");
});

function GetLocationEmployee() {
   var locid= $("#LocationID").val()
   if (locid != null)
   {
       var comID = $("#LocationCompanyID").val()
        
       $("#EmployeeLocationDiv").load("/Location/GetEmployeeByLocationId?LocationID=" + locid + "&CompanyID=" + comID);
        }
    
}
 

    $(function () {
        $("#CountryID").change(function () {
           
            var selectedItem = $(this).val();
            var ddlStates = $("#StateID");
            var statesProgress = $("#states-loading-progress");
            statesProgress.show();
            $.ajax({
                cache: false,
                type: "GET",
                url: "/Location/GetStatesByCountryId",
                data: { "Id": selectedItem },
                success: function (data) {
                    ddlStates.html('');
                    ddlStates.append($('<option></option>').val('0').html('Select State'));
                    $.each(data, function (id, option) {
                        ddlStates.append($('<option></option>').val(option.id).html(option.name));
                    });
                    statesProgress.hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $.MessageBox({
                        buttonDone: "Ok",
                        message: "Failed to retrieve states.",
                    });
                    statesProgress.hide();
                }
            });
        });


        $("#StateID").change(function () {
            var selectedItem = $(this).val();
            var ddlCity = $("#CityID");
            var statesProgress = $("#states-loading-progress");
            statesProgress.show();
            $.ajax({
                cache: false,
                type: "GET",
                url: "/Location/GetCityByStateId",
                data: { "Id": selectedItem },
                success: function (data) {
                    ddlCity.html('');
                    ddlCity.append($('<option></option>').val('0').html('Select City'));
                    $.each(data, function (id, option) {
                        ddlCity.append($('<option></option>').val(option.id).html(option.name));
                    });
                    statesProgress.hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $.MessageBox({
                        buttonDone: "Ok",
                        message: "Failed to retrieve Cities.",
                    });
                    statesProgress.hide();
                }
            });
        });
    });



  



function AddEmployeeToDepartment(empId) {
    var depit = $("#departmentList").val();
    $.ajax({
        type: 'POST',
        data: { departmentid: depit, EmployeeID: empId },
        url: '/DepartmentEmployee/UpdateDepartmentEmployeeByEmployeeId',
        success: function (data) {
            $("#Location").load("/DepartmentEmployee/getAllEmployeesNotInDepartment?departmentid=" + depit + "&CompanyID=" + 0, function () {
                $("#DepartmentEmployeesList").load("/DepartmentEmployee/GetEmployeeByDepartmentId?DepartmentID=" + depit + "&CompanyID=" + 0);
            });
        },
        error: function (data) { }

    });



}







