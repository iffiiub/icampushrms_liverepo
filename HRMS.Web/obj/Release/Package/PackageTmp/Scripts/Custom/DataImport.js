﻿$(document).ready(function () {

});

$(document).on("click", "#fu_ExcelFile", function (event) {
    if ($("#ddlImportType").val() == "" || $("#ddlImportType").val() == undefined) {
        ShowMessage("error", "Please, Select data import type");
        return false;
    }
});

$(document).on("change", "#fu_ExcelFile", function (event) {
    var validExtensions = ['xls', 'xlsx'];
    var file = this.files[0];
    fileName = file.name;
    size = file.size;
    type = file.type;
    $("#DataImport").empty();
    var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
    $("#txtuploadedMsgAdd_ExcelFile").css("color", "red");
    if ($("#ddlImportType").val() == "" || $("#ddlImportType").val() == undefined) {
        ShowMessage("error", "Please, Select data import type");
        return;
    }
    if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
        if (size <= 2097152) {
            $("#txtuploadedMsgAdd_ExcelFile").text("");
            $("#txtuploadedMsgAdd_ExcelFile").hide();
            $("#divBrowse_ExcelFile").hide();
            $("#btnRemove_ExcelFile").show();
            $("#file_caption_name_ExcelFile").show();
            $("#file_caption_id_ExcelFile").html(fileName.substring(0, 15));
            var fileJD = URL.createObjectURL(event.target.files[0]);
            $('#divPreview_ExcelFile').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');

            PreviewImportExcel();
        }
        else {
            $("#txtuploadedMsgAdd_ExcelFile").show();
            $("#txtuploadedMsgAdd_ExcelFile").text("Maximum 2MB file size is allowed to upload.");
        }
    }
    else {
        $("#txtuploadedMsgAdd_ExcelFile").show();
        $("#txtuploadedMsgAdd_ExcelFile").text("Only xls and xlsx extension files are allowed to upload.");
    }
});

$(document).on("change", "#ddlImportType", function () {

    $("#DataImport").empty();
    RemoveDocument();
});

$(document).on("click", "#btnDownloadTemplate", function () {
    if ($("#ddlImportType").val() == undefined || $("#ddlImportType").val() == "") {
        ShowMessage("error", "Please select data import type.");
        return;
    }
    if ($("#ddlImportType").val() != "") {
        $.ajax({
            type: "POST",
            data: { DataImportTypeId: $("#ddlImportType").val() },
            datatype: "json",
            url: '/DataImport/GetDataImportFileTemplatePath',
            success: function (data) {
                window.location.assign('/DataImport/DownloadExcel?FileName=' + data.FileName + '&DataImportTypeId=' + $("#ddlImportType").val())
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }
});

function RemoveDocument() {
    $("#btnRemove_ExcelFile").hide();
    $("#divBrowse_ExcelFile").show();
    $("#file_caption_name_ExcelFile").hide();
    $("#file_caption_id_ExcelFile").html('');
    $("#txtuploadedMsgAdd_ExcelFile").text("");
    $("#fu_ExcelFile").val("");
    $("#divPreview_ExcelFile").html("");
    $("#DataImport").empty();
}

function PreviewImportExcel() {
    var importExcelFile;
    if ($("#fu_ExcelFile").get(0).files.length > 0) {
        importExcelFile = $("#fu_ExcelFile").get(0).files[0];
    }
    else {
        ShowMessage("error", "Please, Upload file");
        return;
    }


    var formData = new FormData();
    formData.append("file", importExcelFile);
    formData.append("DataImportTypeId", $("#ddlImportType").val());
    pageLoaderFrame();
    $.ajax({
        contentType: false,
        processData: false,
        type: "POST",
        data: formData,
        datatype: "json",
        url: '../DataImport/UploadFile',
        success: function (data) {
            if (data.result == undefined) {
                $("#DataImport").html(data);
                var tblPayAdditionImportExcel = new $('#tblPayAdditionImportExcel').dataTable({
                    columnDefs: [
                        { width: "15%", targets: [0, 3] },
                        { width: "20%", targets: [2] },
                        { width: "25%", targets: [1, 4] }
                    ],
                    destroy: true

                });
                hideLoaderFrame();
            }
            else {
                ShowMessage("error", data.Message);
                hideLoaderFrame();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

$(document).on("click", "#btnSaveImportExcel", function () {
    if ($("#IsValidData").val() == "False") {
        ShowMessage("error", "The highlighted data is in incorrect format, Please re-upload file after correct the data format.");
        return;
    }

    if ($("#ddlImportType").val() == "" || $("#ddlImportType").val() == undefined) {
        ShowMessage("error", "Please select data import type.");
        return;
    }

    var ImportExcelData = new Array();
    if ($("#ddlImportType").val() == 1) {
        var rows = $('#tblPayAdditionImportExcel').DataTable().rows().nodes();
        if (rows.length > 0) {
            $(rows).each(function (i, item) {
                var PaySalaryAllowanceID = $(item).attr("PaySalaryAllowanceId");
                var EmployeeID = $(item).attr("EmployeeId");
                var AdditionDate = $(item).attr("AdditionDate");
                var Amount = $(item).attr("Amount");
                var Description = $(item).attr("Description");

                var PayAdditionDetails = {};
                if (PaySalaryAllowanceID != undefined && PaySalaryAllowanceID > 0 && EmployeeID != undefined && EmployeeID > 0 && AdditionDate != undefined && AdditionDate != "" &&
                    Amount != undefined && Amount != "") {
                    PayAdditionDetails.PaySalaryAllowanceID = PaySalaryAllowanceID;
                    PayAdditionDetails.EmployeeID = EmployeeID;
                    PayAdditionDetails.AdditionDate = AdditionDate.toString();
                    PayAdditionDetails.Amount = Amount.toString();
                    PayAdditionDetails.Description = Description;
                    ImportExcelData.push(PayAdditionDetails);
                }
            });
        }
        else {
            ShowMessage("error", "Please upload file data before save.");
            return;
        }

        $.ajax({
            type: "POST",
            data: { PayAdditions: JSON.stringify(ImportExcelData), DataImportID: $("#DataImportId").val() },
            datatype: "json",
            url: '/DataImport/ImportExcelToPayAddition',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    RemoveDocument();
                }
                else {
                    ShowMessage("error", data.Message);
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }

});