﻿
var sundriesJson = [];
var callBackCount = 0;
$(document).ready(function () {
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    GetAllPayBatch($("#ddl_AcademicYear").val(), "ddl_PayBatch", "Seleted");

    $(document).on("change", "#AcYear", function () {
        if (parseInt($(this).val()) > 0)
            GetAllPayBatch($(this).val(), "PayBatchID", "Seleted");
        else
            GetAllPayBatch(0, "PayBatchID", "All");
    })
});

$("#ddl_AcademicYear").change(function () {
    if (parseInt($(this).val()) > 0)
        GetAllPayBatch($(this).val(), "ddl_PayBatch", "Seleted");
    else
        GetAllPayBatch(0, "ddl_PayBatch", "All");
});

$("#btn_addHR_PaySundries").click(function () { AddHR_PaySundries(); });
var oTableChannel;

function LoadId() {

    $.each($('#tbl_HR_PaySundriesList > tbody tr'), function (ind, item) {
        $(item).attr({ 'data-id': $(item).find('.btnEditRow').attr('id') });
    });
    removeSpan('#tbl_HR_PaySundriesList');

}

function AddPayBatch() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/PaySundries/AddPayBatch");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Pay Batch');
}

function AddHRSundry() {

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/PaySundries/AddHRSundry/", function () {
        bindSelectpicker('.selectpickerddl');
        $("#modal_heading").text('Add Pay Sundry');
        $('#myModal').modal({ backdrop: 'static' });
    });

    //$("#modal_Loader").html("");
    //$("#modal_Loader").load("/PaySundries/AddHRSundry");
    //$("#myModal").modal("show");
    //$("#modal_heading").text('Add HR Sundry');
    //bindSelectpicker('.selectpickerddl');
}

function AddHR_PaySundries() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/PaySundries/AddHR_PaySundries", function () {
        $("#AcYear").change();
    });
    $("#myModal").modal("show");
    $(".modal-dialog").attr("style", "width:750px;");
    $("#modal_heading").text('Add Pay Sundries');
}

function AddSundries(e) {   
    var currentRow = $(e).closest('tr');
    var empId = $(currentRow).find('#ddlEmployeeID').val();
    var sundryDate = $(currentRow).find('#txtPaySundriesDate').val();
    var amount = $(currentRow).find('#txtAmount').val();
    var index = (sundriesJson.length + 1);
    if (parseInt(empId) > 0) {
        if (sundryDate != "") {
            if (parseFloat(amount) > 0) {
                var insertAt = $('#tblSundries > tbody > tr').length - 1;
                $('#tblSundries > tbody tr:eq(' + insertAt + ')').before('<tr class="newTr">' + currentRow.html() + '</tr>');
                ResetTable(insertAt, empId, sundryDate, amount, index);
                addAndUpdateJson(empId, sundryDate, amount, index, 'Add');
            } else
                ShowMessage("error", "Please enter amount greater than zero");
        } else
            ShowMessage("error", "Please enter valid date");
    } else
        ShowMessage("error", "Please select employee");
}

function ResetTable(rowNo, empid, sundryDate, amount, index) {
    var actionString = '<a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditSundries(this)" title="Edit">'
                            + '<i class="fa fa-pencil"></i>'
                        + '</a>'
                        + '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteSundries(this)" title="Delete">'
                            + '<i class="fa fa-times"></i>'
                        + '</a> <input class="hidIndex" type="hidden" value="' + index + '">';
    var currentRow = $('#tblSundries > tbody tr:eq(' + rowNo + ')');
    $(currentRow).find('#ddlEmployeeID').val(empid);
    $(currentRow).find('#txtPaySundriesDate').val(sundryDate);
    $(currentRow).find('td:eq(3)').html(actionString);
    $(currentRow).find('#txtAmount').val(amount);
    $(currentRow).find('td select, td input').prop('disabled', true);
    var resetRow = $('#tblSundries > tbody tr:eq(' + (rowNo + 1) + ')');
    $(resetRow).find('#ddlEmployeeID').val('');
    $(resetRow).find('#txtPaySundriesDate').val('');
    $(resetRow).find('#txtAmount').val('');
}

function EditSundries(e) {
    var currentRow = $(e).closest('tr');
    if ($(e).find('.fa-pencil').length) {
        $(e).removeClass('btn-success').addClass('btn-info').find('.fa-pencil').removeClass('fa-pencil').addClass('fa-check');
        $(currentRow).find('td select, td input').prop('disabled', false);
        bindDatePicker($(currentRow).find('#txtPaySundriesDate'));
    } else {
        var empId = $(currentRow).find('#ddlEmployeeID').val();
        var sundryDate = $(currentRow).find('#txtPaySundriesDate').val();
        var amount = $(currentRow).find('#txtAmount').val();
        var index = $(currentRow).find('.hidIndex').val();
        if (parseInt(empId) > 0) {
            if (sundryDate != "") {
                if (parseFloat(amount) > 0) {
                    $(e).removeClass('btn-info').addClass('btn-success').find('.fa-check').removeClass('fa-check').addClass('fa-pencil');
                    $(currentRow).find('td select, td input').prop('disabled', true);
                    addAndUpdateJson(empId, sundryDate, amount, index, 'Edit');
                } else
                    ShowMessage("error", "Please enter amount greater than zero");
            } else
                ShowMessage("error", "Please enter valid date");
        } else
            ShowMessage("error", "Please select employee");
    }
}

function DeleteSundries(e) {
    var currentRow = $(e).closest('tr');
    var empId = $(currentRow).find('#ddlEmployeeID').val();
    var sundryDate = $(currentRow).find('#txtPaySundriesDate').val();
    var amount = $(currentRow).find('#txtAmount').val();
    var index = $(currentRow).find('.hidIndex').val();
    addAndUpdateJson(empId, sundryDate, amount, index, 'Delete');
    $(e).closest('tr').remove();

}

function addAndUpdateJson(empid, sundryDate, amount, index, mode) {
    if (mode == "Add") {
        sundriesJson.push({
            EmployeeID: empid,
            PaySundriesDate: sundryDate,
            Amount: amount,
            PaySundriesID: index
        });
    } else if (mode == "Delete") {
        $.each(sundriesJson, function (ind, item) {
            if (item.PaySundriesID == index) {
                sundriesJson.splice(ind, 1);
            }
        });
    } else if (mode == "Edit") {
        $.each(sundriesJson, function (ind, item) {
            if (item.PaySundriesID == index) {
                sundriesJson[ind].EmployeeID = empid;
                sundriesJson[ind].PaySundriesDate = sundryDate;
                sundriesJson[ind].Amount = amount;
            }
        });
    }
}

function DeleteHR_PaySundries(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        pageLoadingFrame("show");
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/PaySundries/DeleteHR_PaySundries',
            success: function (data) {
                if (data.Success) {
                    if (data.CssClass == "Request") {                      
                        ShowMessage("information", data.Message);
                    } else {                       
                        ShowMessage("success", data.Message);
                    }
                    getGrid();
                }
                else {
                    pageLoadingFrame("hide");
                    ShowMessage('error', data.Message);
                }
            },
            error: function (data) { pageLoadingFrame("hide"); }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewHR_PaySundries(id) {
    $("#modal_Loader").load("/PaySundries/ViewHR_PaySundries/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View HR_PaySundries Details');

}

function NBDExternalExcel() {
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    var SundryId = $("#ddl_HRSundry").val();
    if (SundryId == "") SundryId = 0;
    var AcademicYear = $("#ddl_AcademicYear").val();
    if (AcademicYear == "") AcademicYear = 0;
    var PayBatch = $("#ddl_PayBatch").val();
    if (PayBatch == "") PayBatch = 0;
    window.location.assign("/PaySundries/NBDExternalExcelExport?BatchId=" + PayBatch + "&SundriesId=" + SundryId + "&AcademicYearId=" + AcademicYear + "&BankListIds=" + BankListIds);
}

function InternalExcel() {
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    var SundryId = $("#ddl_HRSundry").val();
    if (SundryId == "") SundryId = 0;
    var AcademicYear = $("#ddl_AcademicYear").val();
    if (AcademicYear == "") AcademicYear = 0;
    var PayBatch = $("#ddl_PayBatch").val();
    if (PayBatch == "") PayBatch = 0;
    window.location.assign("/PaySundries/ExportToExcelSundries?BatchId=" + PayBatch + "&SundriesId=" + SundryId + "&AcademicYearId=" + AcademicYear + "&BankListIds=" + BankListIds);
}

function ExternalExcel() {
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    var SundryId = $("#ddl_HRSundry").val();
    if (SundryId == "") SundryId = 0;
    var AcademicYear = $("#ddl_AcademicYear").val();
    if (AcademicYear == "") AcademicYear = 0;
    var PayBatch = $("#ddl_PayBatch").val();
    if (PayBatch == "") PayBatch = 0;
    window.location.assign("/PaySundries/ExternalExcelExport?BatchId=" + PayBatch + "&SundriesId=" + SundryId + "&AcademicYearId=" + AcademicYear + "&BankListIds=" + BankListIds);

}

function ExportToPdfReport() {
    var SundryId = $("#ddl_HRSundry").val();
    var AcademicYear = $("#ddl_AcademicYear").val();
    var PayBatch = $("#ddl_PayBatch").val();
    window.location.assign("/PaySundries/ExportToPdfReport?BatchId=" + PayBatch + "&SundriesId=" + SundryId);
}

function ExportToPdfGenerateJV() {
    pageLoadingFrame("show");
    var SundryId = $("#ddl_HRSundry").val();
    var PayBatch = $("#ddl_PayBatch").val();
    var AcademicYear = $("#ddl_AcademicYear").val();

    if (PayBatch != "") {
        if (AcademicYear != "") {
            $.ajax({
                url: "/PaySundries/GenerateJV?BatchId=" + PayBatch + "&AcademicYear=" + AcademicYear,
                success: function (data) {
                    if (data.Success == true) {
                        pageLoadingFrame("hide");
                        ShowMessage("success", "JV generated successfully");
                        location.reload();
                    }
                    else {
                        pageLoadingFrame("hide");
                        ShowMessage("error", "Technical error has occurred");
                    }
                },
                error: function (data) { }
            });
        } else {
            pageLoadingFrame("hide");
            ShowMessage("error", "Please select academic year");
        }
    } else {
        pageLoadingFrame("hide");
        ShowMessage("error", "Please select pay batch");
    }

}

function PrintLetterToPdf() {
    var SundryId = $("#ddl_HRSundry").val();
    var AcademicYear = $("#ddl_AcademicYear").val();
    var PayBatch = $("#ddl_PayBatch").val();
    window.location.assign("/PaySundries/PrintLetterToPdf?BatchId=" + PayBatch + "&SundriesId=" + SundryId);
}

function SendLetter() {
    pageLoadingFrame("show");
    var SundryId = $("#ddl_HRSundry").val();
    if (SundryId == "") SundryId = 0;
    var AcademicYear = $("#ddl_AcademicYear").val();
    if (AcademicYear == "") AcademicYear = 0;
    var PayBatch = $("#ddl_PayBatch").val();
    if (PayBatch == "") PayBatch = 0;
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    $.ajax({
        type: 'POST',
        url: "/PaySundries/SendLetter?BatchId=" + PayBatch + "&SundriesId=" + SundryId + "&AcademicYearId=" + AcademicYear + "&BankListIds=" + BankListIds,
        success: function (data) {
            pageLoadingFrame("hide");
            ShowMessage(data.CssClass, data.Message);
        },
        error: function (data) {
            pageLoadingFrame("hide");
            ShowMessage("warning", "Error");
        }
    });
}

function onFailure(xhr, status, error) {

    console.log("xhr", xhr);
    console.log("status", status);

    // TODO: make me pretty
    ShowMessage("warning", "Error");
}

function onSuccess(data, status, xhr) {

    console.log("data", data);
    console.log("status", status);
    console.log("xhr", xhr);
    if (data.Success == true) {
        $("#myModal").modal("hide");
        getGridHR_PaySundries();
        ShowMessage("success", "Success");
        //else if (data.InitiatorIdentity = "btnSubmitAndNew") {
        //    alert(data.Message);
        //    // $("#myModal").modal("hide");
        //    getGrid();
        //    // timer = setInterval(AddPosition,1000);
        //}
    }
    else {
        ShowMessage("warning", "Error");
    }

}

function ValidateForm() {
}

function GetAllPayBatch(academicYearId, ddlBatch, Mode) {
    var url = "";
    if (Mode == "All")
        url = "/PaySundries/GetAllPayBatch?academicYearId=" + academicYearId + "&mode=All"
    else {
        if (Mode == "Seleted")
            url = "/PaySundries/GetAllPayBatch?academicYearId=" + academicYearId + "&mode=Selected"
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        success: function (data) {
            data = $.map(data, function (item, a) {
                return "<option value=" + item.Value + ">" + item.Text + "</option>";
            });
            $("#" + ddlBatch).html(data.join(""));
            RefreshSelect($("#" + ddlBatch));
        }
    });
}

function GeneratePaySundriesReport() {
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    var SundryId = $("#ddl_HRSundry").val();
    if (SundryId == "") SundryId = 0;
    var AcademicYear = $("#ddl_AcademicYear").val();
    if (AcademicYear == "") AcademicYear = 0;
    var PayBatch = $("#ddl_PayBatch").val();
    if (PayBatch == "") PayBatch = 0;
    window.open("/PaySundries/PaySundriesReportingViewer?BatchId=" + PayBatch + "&SundriesId=" + SundryId + "&AcademicYearId=" + AcademicYear + "&BankListIds=" + BankListIds, "_blank");
}

function PrintLetter() {
    var SundryId = $("#ddl_HRSundry").val();
    if (SundryId == "") SundryId = 0;
    var AcademicYear = $("#ddl_AcademicYear").val();
    if (AcademicYear == "") AcademicYear = 0;
    var PayBatch = $("#ddl_PayBatch").val();
    if (PayBatch == "") PayBatch = 0;
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    window.open("/PaySundries/PaySundriesLetterReportingViewer?BatchId=" + PayBatch + "&SundriesId=" + SundryId + "&AcademicYearId=" + AcademicYear + "&BankListIds=" + BankListIds, "_blank");
}

function EditRow(e) {
    var td = $(e).parent('td');
    $(e).toggle();
    $(td).find('.btnDeleteRow').toggle();
    $(td).find('.btnRowRefresh').toggle();
};

function RefreshRow(e) {
    $(e).toggle();
    $tr = $(e).closest("tr");
    $td = $(e).closest("td");
    $tr.find('td').each(function (ind, cell) {
        $(cell).data('editorVisible', false);
    });
    $tr.find("td").eq(0).html($(e).attr("data-Date"));
    $tr.find("td").eq(1).html($(e).attr("data-Employee"));
    $tr.find("td").eq(2).html($(e).attr("data-Sundry"));
    $tr.find("td").eq(3).html($(e).attr("data-Batch"));
    $tr.find("td").eq(4).html($(e).attr("data-AcademicYear"));
    $tr.find("td").eq(5).html($(e).attr("data-Amount"));
    $($td).find('.btnDeleteRow').show();
    $($td).find('.btnEditRow').show();
}

function Add(title, id, dropdownId) {
    $("div.tooltip").hide();
    $("#hdnDropDownReloadId").val(dropdownId);
    $("#hdnDropDownId").val(id);
    $("#modal_Loader").load("/PaySundries/AddBankName/");
    $("#myModal").modal("show");
    $("#modal_heading").text(title);
}

function getGrid() {
    pageLoadingFrame("show");
    var SundryId = $("#ddl_HRSundry").val();
    var AcademicYear = $("#ddl_AcademicYear").val();
    var PayBatch = $("#ddl_PayBatch").val();
    var BankListIds = getSelectedIds('.ddlBankList').join(',');
    oTableChannel = $('#tbl_HR_PaySundriesList').dataTable({
        "aoColumns": [
           { "mData": "PaySundriesDate", "sTitle": "Pay Sundries Date", "sType": "date-uk", 'width': '12%' },
           { "mData": "EmployeeName", "sTitle": "Employee", 'width': '17%' },
           { "mData": "Sundry", "sTitle": "Sundry", 'width': '17%' },
           { "mData": "Batch", "sTitle": "Batch", 'width': '17%' },
           { "mData": "AcademicYear", "sTitle": "Academic Year", 'width': '12%' },
           { "mData": "Amount", "sTitle": "Amount", 'width': '12%' },
           { "mData": "Actions", "sTitle": "Action", 'width': '12%' }
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "BatchId", "value": PayBatch }, { "name": "SundriesId", "value": SundryId }, { "name": "AcademicYearId", "value": AcademicYear }, { "name": "BankListIds", "value": BankListIds });
        },
        "processing": false,
        "serverSide": false,
        "bLengthChange": true,
        "iDisplayLength": 10,
        "ajax": "/PaySundries/GetHR_PaySundriesList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "initComplete": function (settings, json) {
            pageLoadingFrame("hide");
        },
        "fnDrawCallback": function () {
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var type = "Edit";
    var buttonName = "Update";
    var ModalHeadingLabel = "Edit Pay Sundries";
    $("#modal_Loader3").load("/PaySundries/Edit/" + id, function () {
        bindSelectpicker('.selectpickerddl');
        $("#myModal3").modal("show");
        $("#modal_heading3").text(ModalHeadingLabel);
        $("#form0").find('input[type="submit"]').val(buttonName);
        //$("#myModal3 .modal-dialog").attr("style", "width:550px;");
    });
}