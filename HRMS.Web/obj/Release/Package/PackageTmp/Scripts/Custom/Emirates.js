﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridEmirates();
    }
});

var oTableChannel;

function getGridEmirates() {
    oTableChannel = $('#tbl_EmiratesList').dataTable({
        "sAjaxSource": "/Emirates/GetEmiratesList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document Name/No", 'width': '20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", "bSortable": false, 'width': '20%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Emirates/GetEmiratesList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_EmiratesList");
        }
    });
}

$("#btn_ExportToExcel").click(function () { ExportToExcelEmirates(); });
$("#btn_ExportToPdf").click(function () { ExportToPdfEmirates(); });

function AddEmirates(DocTypeName) {
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/Emirates/AddEmirates");
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add ' + DocTypeName + '');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditEmirates(id, DocTypeName) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/Emirates/EditEmirates/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit ' + DocTypeName + '');

}


function DeleteEmirates(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Emirates/DeleteEmirates',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridEmirates();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewEmirates(id, DocTypeName) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Emirates/ViewEmirates/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View ' + DocTypeName + ' Details');

}

function ExportToExcelEmirates() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Emirates/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdfEmirates() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Emirates/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}