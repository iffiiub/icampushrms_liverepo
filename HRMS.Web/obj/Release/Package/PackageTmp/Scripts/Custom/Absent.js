﻿var EmployeeAbsentIDs = [];
$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    //getGridAbsent($("#hdnEmployee").val());
    var isDateSelected = $("#isDateSelected").val();
    if (isDateSelected == "true") {
        var d1 = $("#txtStartDate").val();
        var d2 = $("#txtToDate").val();
        getGridAbsentFilterByDate(d1, d2);
    }
    else {
        getGridAbsent($("#EmployeeID").val());
    }
});
//$("#btnTextSearch").click(function () {

//    getGridAbsent($("#hdnEmployee").val());
//});

$("#ddl_Month,#ddl_year").change(function () {
    var month = parseInt($("#ddl_Month").val());
    var year = parseInt($("#ddl_year").val());
    var d1 = "01/" + formatDateOrMonth(month.toString()) + "/" + formatDateOrMonth(year.toString());
    var d2 =formatDateOrMonth( lastday(year, month).toString() )+ "/" +formatDateOrMonth( month.toString()) + "/" +formatDateOrMonth( year.toString());
    $("#txtStartDate").val(d1);
     $("#txtToDate").val(d2);
});

function formatDateOrMonth(raw)
{
    if (raw.length == 1) {
        raw = "0" + raw;
    }
    return raw;
}

$("#btnGetAbsentRecords").click(function () {
    var d1 = $("#txtStartDate").val();
    var d2 = $("#txtToDate").val();
    $("#isDateSelected").val("true")
    getGridAbsentFilterByDate(d1, d2);
});

$("#btnGetAbsentRecords_monthwise").click(function () {
    var month = parseInt( $("#ddl_Month").val());
    var year = parseInt($("#ddl_year").val());
    var d1 = "01/" + month.toString() + "/" + year.toString();
    var d2 = lastday(year, month) + "/" + month.toString() + "/" + year.toString();
    $("#isDateSelected").val("true");
    //alert("date1 : "+d1 + "date 2 : " + d2);
    getGridAbsentFilterByDate(d1, d2);
});

var lastday = function (y, m)
{
return  new Date(y, m, 0).getDate();
}

$("#btn_addAbsent").click(function () {

    AddAbsent();
});

$("#btn_deleteMultipleAbsent").click(function () {
    DeleteMultipleAbsent();
});
var oTableChannel;
function getGridAbsent(empid) {
    $("#tbl_AbsentList").empty();
    var startDate = $("#txtStartDate").val();
    var endDate = $("#txtToDate").val();
    if ($("#ChkGetAllEmployee").is(":checked")) {
        empid = 0;
    }
    var checkAllColumn = "";
    checkAllColumn = "<input type='checkbox' id = 'check_all' name = 'check_all' data-toggle='tooltip' data-placement='top' data-original-title='Check all' onclick='CheckAll()'/>";
    oTableChannel = $('#tbl_AbsentList').dataTable({
        "sAjaxSource": "/Absent/GetAbsentList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid }, { "name": "startDate", "value": startDate }, { "name": "endDate", "value": endDate });
        },
        "aoColumns": [
            {
                "mData": "CheckRow", "sTitle": checkAllColumn, "sClass": "center",
                "bSortable": false, "sWidth": "3%"
            },
            { "mData": "EmployeeAltID", "sTitle": "Employee Id", "bSortable": true, "sWidth": "8%" },
            { "mData": "PayEmployeeAbsentID", "bVisible": false },
            { "mData": "EmployeeName", "sTitle": "Emp. Name", "sWidth": "15%" },
            { "mData": "PayEmployeeAbsentFromDate", "sTitle": "Absent From", "sType": "date-uk", "sWidth": "10%" },
            { "mData": "PayEmployeeAbsentToDate", "sTitle": "Absent To", "sType": "date-uk", "sWidth": "10%" },
            { "mData": "LeaveTypeName", "sTitle": "Paid Type", "sWidth": "10%" },
            { "mData": "AbsentTypeName", "sTitle": "Absent Type", "bSortable": false, "sWidth": "15%" },
            { "mData": "AbsentStatus", "sTitle": "Status", "sWidth": "10%" },
            { "mData": "IsMedicalCertificateProvided", "sTitle": "Med. Cert.", "bSortable": false, "sWidth": "5%" },
             { "mData": "Action", "sTitle": "Action", "sClass": "center-text-align", "bSortable": false, "sWidth": "14%" },
             { "mData": "IsConfirmed", "bVisible": false, "sClass": "hidden" },

        ],
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 10, 25, 50, 100, 1000], [10, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var AbsentTypeID = aData["AbsentTypeID"];
            var select = $(nRow).find("select");
            $(select).val(AbsentTypeID).attr("selected", "selected");
            var IsConfirmed = aData["IsConfirmed"];
            if (IsConfirmed == 1) {
                $('td', nRow).addClass('highlightRow');
            }
        },
        "fnDrawCallback": function () {
            $('#tbl_AbsentList .selectpickerddl').selectpicker();
            removeSpan("#tbl_AbsentList");
        }
    });

}


function getGridAbsentFilterByDate(StartDate, EndDate) {
    var searchval = $("#txtSearch").val();
    var empId = $("#ddl_EmployeeList").val();
    var deptID = $("#ddl_DeptList").val();
    var SectionID = $("#ddl_SectionList").val();
    var checkAllColumn = "";
    checkAllColumn = "<input type='checkbox' id = 'check_all' name = 'check_all' data-toggle='tooltip' data-placement='top' data-original-title='Check all' onclick='CheckAll()'/>";

    oTableChannel = $('#tbl_AbsentList').dataTable({
        "sAjaxSource": "/Absent/GetAbsentList",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "search", "value": searchval },
                { "name": "empid", "value": empId },
                { "name": "deptID", "value": deptID },
                { "name": "EMpSectionID", "value": SectionID },
                { "name": "startDate", "value": StartDate },
                { "name": "endDate", "value": EndDate }
                );
        },
        "aoColumns": [
           {
               "mData": "CheckRow", "sTitle": checkAllColumn, "sClass": "center",
               "bSortable": false, "sWidth": "3%"
           },
            { "mData": "EmployeeAltID", "sTitle": "Employee Id", "bSortable": true, "sWidth": "8%" },
            { "mData": "PayEmployeeAbsentID", "bVisible": false },
            { "mData": "EmployeeName", "sTitle": "Emp. Name", "sWidth": "15%" },
            { "mData": "PayEmployeeAbsentFromDate", "sTitle": "Absent From", "sType": "date-uk", "sWidth": "10%" },
            { "mData": "PayEmployeeAbsentToDate", "sTitle": "Absent To", "sType": "date-uk", "sWidth": "10%" },
            { "mData": "LeaveTypeName", "sTitle": "Paid Type", "sWidth": "10%" },
            { "mData": "AbsentTypeName", "sTitle": "Absent Type", "bSortable": false, "sWidth": "15%" },
            { "mData": "AbsentStatus", "sTitle": "Status", "sWidth": "10%" },
            { "mData": "IsMedicalCertificateProvided", "sTitle": "Med. Cert.", "bSortable": false, "sWidth": "5%" },
             { "mData": "Action", "sTitle": "Action", "sClass": "center-text-align", "bSortable": false, "sWidth": "14%" },
             { "mData": "IsConfirmed", "bVisible": false, "sClass": "hidden" },
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 10, 25, 50, 100, 1000], [10, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var AbsentTypeID = aData["AbsentTypeID"];
            var select = $(nRow).find("select");
            $(select).val(AbsentTypeID).attr("selected", "selected");
            var IsConfirmed = aData["IsConfirmed"];
            if (IsConfirmed == 1) {
                $('td', nRow).addClass('highlightRow');
            }
        },
        "fnDrawCallback": function () {
            $('#tbl_AbsentList .selectpickerddl').selectpicker();
            removeSpan("#tbl_AbsentList");
        }
    });
}

function AddAbsent() {

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/AddAbsent", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add Absent');
    });

}

function EditAbsent(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/EditAbsent/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Absent');
    });
}

function DeleteAbsent(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        //Logic to delete the item
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Absent/DeleteAbsent',
            success: function (data) {
                ShowMessage("success", "Absent deleted successfully.")
                var id = $("#hdnEmployee").val();
                var isDateSelected = $("#isDateSelected").val();
                if (isDateSelected == "true") {
                    var d1 = $("#txtStartDate").val();
                    var d2 = $("#txtToDate").val();
                    getGridAbsentFilterByDate(d1, d2);
                }
                else {
                    getGridAbsent($("#EmployeeID").val());
                }
            },
            error: function (data) { }
        });
    }).fail(function () { });
}


function DeleteMultipleAbsent() {
    if (EmployeeAbsentIDs.length > 0) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "" + EmployeeAbsentIDs.length + " selected Record(s) going to be delete, are you sure you want to proceed?." }).done(function () {
            //Logic to delete the item
            $.ajax({
                type: 'POST',
                data: { PayEmployeeAbsentIDs: JSON.stringify(EmployeeAbsentIDs) },
                url: '/Absent/DeleteMultipleAbsentRecords',
                success: function (data) {
                    ShowMessage(data.CssClass, data.Message)
                    if (data.Success) {
                        EmployeeAbsentIDs = [];
                        var id = $("#hdnEmployee").val();
                        var isDateSelected = $("#isDateSelected").val();
                        if (isDateSelected == "true") {
                            var d1 = $("#txtStartDate").val();
                            var d2 = $("#txtToDate").val();
                            getGridAbsentFilterByDate(d1, d2);
                        }
                        else {
                            getGridAbsent($("#EmployeeID").val());
                        }
                    }
                },
                error: function (data) { }
            });
        }).fail(function () { });
    } else {
        ShowMessage("error", "Please selected at least one record.")
    }

}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewAbsent(id) {
    $("#modal_Loader").load("/Absent/ViewAbsent/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Absent Details');

}

function addToAbsentIdCollection(PayEmployeAbsentID) {
    var isChecked = $("#Chk_" + PayEmployeAbsentID).is(":checked")
    if (isChecked) {
        EmployeeAbsentIDs.push(PayEmployeAbsentID)
    }
    else {
        //EmployeeAbsentIDs.splice($.inArray(PayEmployeAbsentID, EmployeeAbsentIDs), 1);
        EmployeeAbsentIDs = $.grep(EmployeeAbsentIDs, function (value) {
            return value != PayEmployeAbsentID;
        });
    }

}

function UpdateSelectedAbsentType() {
    if (EmployeeAbsentIDs.length > 0) {
        var AbsentTypeID = $("#ddl_AbsentTypeList").val();

        if (AbsentTypeID > 0) {
            $.ajax({
                type: 'POST',
                data: { PayEmployeeAbsentIDs: JSON.stringify(EmployeeAbsentIDs), AbsentTypeID: AbsentTypeID },
                url: '/Absent/UpdateMultipleAbsentType',
                success: function (data) {
                    ShowMessage(data.CssClass, data.Message);
                    var d1 = $("#txtStartDate").val();
                    var d2 = $("#txtToDate").val();
                    getGridAbsentFilterByDate(d1, d2);
                },
                error: function (data) { }
            });
        } else {
            ShowMessage("error", "Please select absent type.")
        }

    }
    else {
        ShowMessage("error", "Please select at least one record.")
    }

}

function CheckEmployeeInLate(selectedEmpId, fromDate, toDate, callBack) {

    $.ajax({
        url: "/Absent/CheckEmployeeInLate",
        data: { EmpIds: selectedEmpId, FromDate: fromDate, ToDate: toDate },
        dataType: "json",
        type: "GET",
        success: function (data) {
            callBack(data.isPresent)
        },
        error: function () {
        }
    })
}

function CheckAll() {
    if ($("#check_all").is(':checked')) {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".ChkRowCheck").closest('tr').addClass("selected");
        var table = $('#tbl_AbsentList').DataTable();
        var rowData = table.rows().data();
        //alert(rowData);
        $(rowData).each(function (i, item) {
            var checkBox = item.CheckRow;
            if (!$(checkBox).is(':disabled')) {
                EmployeeAbsentIDs.push(item.PayEmployeeAbsentID)
            }
        });

    }
    else {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".ChkRowCheck").closest('tr').removeClass("selected");
        EmployeeAbsentIDs = [];
    }

}

function UpdateMedCertId(e, MedCertId) {
    var IsMedCert = $(e).is(":checked");
    $.ajax({
        url: "/Absent/AddMedCertificateStatus",
        data: { MedCertId: MedCertId, IsMedCert: IsMedCert },
        dataType: "json",
        type: "POST",
        success: function (data) {
            ShowMessage(data.CssClass, data.Message);
        },
        error: function () {
        }
    });
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        var startDate = $("#txtStartDate").val();
        var endDate = $("#txtToDate").val();
        var empId = $("#ddl_EmpList").val();
        window.location.assign("/Absent/ExportToExcel?empId=" + empId + "&startDate=" + startDate + "&endDate=" + endDate);
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        var startDate = $("#txtStartDate").val();
        var endDate = $("#txtToDate").val();
        var empId = $("#ddl_EmpList").val();
        window.location.assign("/Absent/ExportToPdf?empId=" + empId + "&startDate=" + startDate + "&endDate=" + endDate);
    }
    else ShowMessage("warning", "No data for export!");
}
