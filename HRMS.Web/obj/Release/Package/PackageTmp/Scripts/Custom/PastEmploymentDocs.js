﻿$(document).ready(function () {
     GetPastEmployment();
    //$("#btn_BackProfile1").click(function () {
    //    alert($('#Empid').val());
    //    window.location.href = "/ViewEmployeeProfile/Index/" + $('#Empid').val();
    //});

});

function GetPastEmployment() {
    var locid = $("#PastEmploymentID").val()
    
    if (locid != null) {
        //alert(locid);
        $("#PastemployeeDocs").load("/PastEmployment/GetPastEmploymentDocsList/" + $("#PastEmploymentID").val());
    }

}


var oTableChannel;

function getGrid(locid) {
    //alert(locid);
    if (locid != null) {
        oTableChannel = $('#tbl_PastEmploymentDocs').dataTable({
            "dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
            "language": {
                "lengthMenu": "Records per page _MENU_ ",
            },
            "sAjaxSource": "/PastEmployment/GetPastEmploymentDocsList",
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "PastEmploymentID", "value": locid });
            },
            "aoColumns": [
                
                { "mData": "PastEmploymentID", "bVisible": false, "sTitle": "PastEmploymentID" },
                { "mData": "FileName", "sTitle": "FileName" },
                { "mData": "Description", "sTitle": "Description" },
                { "mData": "Size", "sTitle": "Size" },
                { "mData": "Type", "sTitle": "Type" },
                { "mData": "Actions", "sTitle": "Actions" }

            ],
            "processing": true,
            "serverSide": true,
            "ajax": "/PastEmployment/GetPastEmploymentDocsList",
            "aLengthMenu": [[10, 25, 50, 100, 100,125], [10, 25, 50, 100,100, "All"]],
            //"iDisplayLength": 10,
            "bDestroy": true,
            "bFilter": false,
            "bInfo": true,
            "bSortCellsTop": true
        });
    }
}
