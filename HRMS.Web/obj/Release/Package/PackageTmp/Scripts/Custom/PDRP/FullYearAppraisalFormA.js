﻿var selectedValues = [];
var Competencies1 = [];
var Competencies2 = [];

$(document).ready(function () {
    $(document).on("click", "#EMFullbtnSave", function () {
        $("#FormSubmit").val("No");
        SavePage();

    });

    $(document).on("click", "#EMFullbtnSubmit", function () {
        $("#FormSubmit").val("Yes");
        SavePage();

    });

    $(document).on("click", "#LMFullbtnSubmit", function () {
        $("#FormSubmit").val("YesLM");
        SavePage();

    });
    $(document).on("click", "#LM2FullbtnSubmit", function () {
        $("#FormSubmit").val("YesLM2");
        SavePage();

    });
    $(document).on("click", "#BUFullbtnSubmit", function () {
        $("#FormSubmit").val("YesBU");
        SavePage();

    });

    $(document).on("click", "#EM2FullbtnSubmit", function () {
        $("#FormSubmit").val("Yes2");
        SavePage();

    });
    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });

    FullYearControls();

    $(".wid180").on('change', function () {
       
        var totalScore = 0;
        $("#tblBusinessTargets #ProficiencyScore").each(function () {
            var item_Weight = $(this).closest('tr').find('#item_Weight').val();
            var Score = $(this).closest('tr').find('#item_Score');
            if (parseInt(item_Weight) > 0) {
                var sc = parseInt($(this).val()) * parseInt(item_Weight);
                Score.val(sc);
                totalScore = parseInt(totalScore) + parseInt(sc);

            }

        });



        $("#BusinessTargetsTota").val(totalScore);
        $("#divTotalScore").html(totalScore);
        $("#TotalScore1").val(totalScore);
        $("#divTotalScore1").html(totalScore);

        OverallAppraisalScore();
    });

    $(".wid181").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblBehavioralCompetenciesFormB #ProficiencyScore").each(function () {

            if (parseInt($(this).val()) > 0) {
                var sc = parseInt($(this).val());               
                totalScore = parseInt(totalScore) + parseInt(sc);
                cnt++;
            }

        });


        $("#BehavioralTotalScore").val(totalScore / 5);
        $("#divBehavioralTotalScore").html(totalScore / 5);

        OverallAppraisalScore();

    });
    $(".wid182").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblProfessionalCompetenciesFormB #ProficiencyScore").each(function () {

            if (parseInt($(this).val()) > 0) {
                var sc = parseInt($(this).val());
                totalScore = parseInt(totalScore) + parseInt(sc);
                cnt++;
            }

        });


        $("#ProfessionalTotalScore").val(totalScore / 5);
        $("#divProfessionalTotalScore").html(totalScore / 5);

        OverallAppraisalScore();
    });
});

function SavePage() {
    var Errormsg = "";
    delete (selectedValues)
    delete (Competencies1)
    delete (Competencies2)




    //delete (selectedValues);
    var Tweight = 0;
    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var businesstargetid = row.data("businesstargetid");
        var businessTargetDetails = row.find('.businesstarget').val();
        var accomplishments = row.find('.accomplishments').val();
        var weight = row.find('.weight').val();
        var wid180 = row.find('.wid180').val();
        var Score = row.find('.Score').val();
        if (weight != "") {
            Tweight = parseInt(Tweight) + parseInt(weight);
        }
        if ((businessTargetDetails != "") && (weight != "")) {
            if (Score == 0 || Score == "") {
                Errormsg = "Please Select Rating for all business target"

            }
            else {
                selectedValues.push({ businesstargetid: businesstargetid, businessTargetDetails: businessTargetDetails, Accomplishments: accomplishments, weight: weight, Rating: wid180 });
            }
        }
    });
    if (Tweight > 100) {
        ShowMessage("error", "Invalid Total KPI.");
    }
    else {
        var grade = $("#txtGrade").val();
        if (grade == '' || grade == undefined) {
            $("#txtGrade-validationMsg").css("color", "red");
            $("#txtGrade-validationMsg").text("This field is mandatory");
            $("#txtGrade-validationMsg").show();
        }
        else {
            $("#txtGrade-validationMsg").text("");
            $("#txtGrade-validationMsg").hide();
            $(".clsProfessionalCompetencies").each(function () {
                var row = $(this);
                var wid182 = row.find('.wid182').val();
                var vNo = row.find('td:eq(0)').text();              
                Competencies1.push({ No: parseInt(vNo), Score: parseInt(wid182) })
            });


            $(".clsBehavioralCompetencies").each(function () {
                var row = $(this);
                var wid181 = row.find('.wid181').val();
                var vvNo = row.find('td:eq(0)').text();
                Competencies2.push({ No: vvNo, Score: wid181 })
            });

            if ($("#OverallAppraisalScore").val() == 0) {
                Errormsg = Errormsg + "</br>Overall Score cannot be zero!"
            }
            if (Errormsg != "") {
                ShowMessage("error", Errormsg);
            }
            else {

                if (selectedValues.length > 0) {


                    $.ajax({
                        url: "/PDRP/AnnualAppraisal/SaveFullYearAppraisalFormData",
                        type: 'POST',
                        data: { model: JSON.stringify(getFullYearAppraisalModelData()) },
                        success: function (result) {
                            ShowMessage(result.CssClass, result.Message);
                            selectedValues = [];
                            if (result.Success) {
                                setTimeout(function () { BackToTaskList(); }, 1000);
                            }
                        }
                    });
                }
                else
                    ShowMessage("error", "Please add business target details.");
            }
        }
    }

    
}
function getFullYearAppraisalModelData() {

    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinessTargetsComments: $("#businessTargetComments").val(),
        CoreCompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        JsonString1: JSON.stringify(Competencies1),
        JsonString2: JSON.stringify(Competencies2),
        ID: $("#AnnualGoalSettingFormAId").val(),
        PerformanceGroupId: $("#PerformanceGroupId").val(),
        JobGrade: $("#txtGrade").val(),
        FormState: $("#FormState").val(),
        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds: $("#TrainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val(),
        MyThreeKeyAchivements: $("#MyThreeKeyAchivements").val(),
        MyTwoDevelopmentAreas: $("#MyTwoDevelopmentAreas").val(),
        EmployeeCommentsonFullYearPerformance: $("#EmployeeCommentsonFullYearPerformance").val(),
        Strengths: $("#Strengths").val(),
        FullTrainingNeeds: $("#FullTrainingNeeds").val(),
        LineManagerCommentsonFullYearPerformance: $("#LineManagerCommentsonFullYearPerformance").val(),
        BusinessTargetsTota: $("#BusinessTargetsTota").val(),
        ProfessionalCompetenciesAVG: $("#ProfessionalTotalScore").val(),
        BehavioralCompetenciesAVG: $("#BehavioralTotalScore").val(),
        CompetenciesAVG: $("#BehavioralTotalScore1").val(),
        OverallAppraisalScore: $("#OverallAppraisalScore").val(),
        FormSubmit: $("#FormSubmit").val(),
        RequestId: $("#RequestId").val()

    };

    return businessTargetModel;
}

function OverallAppraisalScore() {
    var ddlDropInsYearID = $("#YearId").val();
    var performanceGroupId = $("#PerformanceGroupId").val();
    var scorevalues = [];
    
    scorevalues.push($("#BusinessTargetsTota").val());
    var s = (parseInt($("#ProfessionalTotalScore").val()) + parseInt($("#BehavioralTotalScore").val()) / 2)

    $("#BehavioralTotalScore1").val(s);
    $("#divBehavioralTotalScore1").html(s);
    scorevalues.push(Math.round(s));
    


    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, FormulaYearID: ddlDropInsYearID, PerformanceGroupId: performanceGroupId },
        traditional: true,
        url: '/PDRP/AnnualAppraisal/CalculateDropInScore',
        success: function (data) {

            $("#OverallScoreFormula").val(data.Formula);
            $("#OverallAppraisalScore").val(data.OverallScore);
            $("#divOverallScore").text(data.OverallScore);

        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred" + data);
            globalFunctions.onFailure(data, xhr, status);
        }
    });

}
//function getMidYearAppraisalModelData() {
//    var businessTargetModel = {
//        EmployeeId: $("#EmployeeID").val(),
//        BusinessTargetsComments: $("#businessTargetComments").val(),
//        CoreCompetenciesComments: $("#CompetenciesComments").val(),
//        YearId: $("#YearId").val(),
//        CompanyId: $("#CompanyId").val(),
//        JsonString: JSON.stringify(selectedValues),
//        ID: $("#MidYearAppraisalFormModelId").val(),
//        PerformanceGroupId: $("#PerformanceGroupId").val(),
//        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
//        TrainingNeeds: $("#trainingNeeds").val(),
//        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
//        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val()
//    };
//    return businessTargetModel;
//}
function FullYearControls() {
    var PeriodId = $('#PeriodId').val();
    if (PeriodId == 2) {
        $('#item_Score').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_BusinessTargetDetails').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_Weight').bind('keypress', function (e) {
            e.preventDefault();
        });
    }
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}