﻿var selectedValues = [];
var isSelectedValues = false;
var appliedSelections = [];
var isFilter = false;
$(document).ready(function () {
    loadGoalSettingData();

    $('#btnSearch').on('click', function () {
        loadGoalSettingData();
        isSelectedValues = false;
        isFilter = true;    
    });

    $(document).on("click", "#btnApplyToAll", function () {
        $(".YNClass").not("[disabled]").each(function () {
             if ($(this).is(':checked')) {
                 isSelectedValues = true;
                 var startDate = $(this).closest('tr').find('.settingGolasStartDate');
                 var vProcessName = $(this).closest('tr').find('.ProcessName');
                 if (startDate.val() == '' || startDate.val() == undefined)
                     var dueDate = $(this).closest('tr').find('.settingGolasDueDate');
                 if ((startDate.val() == '' || startDate.val() == undefined) && (dueDate.val() == '' || dueDate.val() == undefined)) {
                     if ($("#SettingGoalsStartDate").val() == '')
                         ShowMessage("error", "Please select Setting Goals Start date.");
                     else if ($("#SettingGoalsDueDate").val() == '')
                         ShowMessage("error", "Please select Setting Goals Due date.");
                     else {
                         
                         startDate.val($("#SettingGoalsStartDate").val());
                         dueDate.val($("#SettingGoalsDueDate").val());
                         vProcessName.val($("#txtProcessName").val());
                         appliedSelections.push({ employeeId: $(this).data("employeeid"), companyId: $(this).data("companyid"), ProcessName: vProcessName });
                     }
                 }
             }
        });
        if (!isSelectedValues)
            ShowMessage("error", "Please seelct at least one checkbox");
        else
            isSelectedValues = false;
           
    });

    $(document).on("click", "#btnSubmit", function () {
        $(".YNClass").not("[disabled]").each(function () {
            if ($(this).is(':checked')) {
                var performceGroupId = $(this).closest('tr').find('.performcacegroup').val();
                var employeeId = $(this).data("employeeid");
                var yearId = $("#ddlKPIYear").val();
                var startdate = $(this).closest('tr').find('.settingGolasStartDate').val();
                var duedate = $(this).closest('tr').find('.settingGolasDueDate').val();
                var vProcessName = $(this).closest('tr').find('.ProcessName').val();
                
                selectedValues.push({ performanceGroupId: performceGroupId, EmployeeID: employeeId, YearId: yearId, SettingGoalsStartDate: startdate, SettingGoalsDueDate: duedate, ProcessName:vProcessName });
            }
        });

        $.ajax({
            url: "/PDRP/GoalSetting/SaveGoalSettingsData",
            type: 'POST',
            data: { jsonString: JSON.stringify(selectedValues) },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                loadGoalSettingData();
                selectedValues = [];
                appliedSelections = [];
                isSelectedValues = false;
                isFilter = false;
            }
        });
    });

    $(document).on("click", "#btnCancel", function() {
        $("#SettingGoalsStartDate, #SettingGoalsDueDate").val("");
        $(".YNClass").each(function () {
            if ($(this).is(':checked')) {
                $(this).prop('checked', false);
                $(this).closest('tr').find('.settingGolasStartDate').val("");
                $(this).closest('tr').find('.settingGolasDueDate').val("");
            }
        });
    });

    $(document).on("change", "#ddlKPIYear", function () {
        loadGoalSettingData();
        isSelectedValues = false;
        isFilter = true;
    });


});

function loadGoalSettingData() {
        $.ajax({
            type: 'POST',
            data: { companyId: $("#ddlCompany").val(), yearId: $("#ddlKPIYear").val() },
            traditional: true,
            url: '/PDRP/GoalSetting/GetGoalSettingDataList',
            success: function (data) {
                $("#divGoalSettings").html(data);
                $('#tblGoalSettingGrid').DataTable({
                    "aoColumnDefs": [
                        { "sWidth": "10%", "aTargets": [0, 1, 2, 4, 5] },
                        { "sWidth": "15%", "aTargets": [3, 6, 7] }
                    ],
                    "fnDrawCallback": function () {
                        bindSelectpicker('#tblGoalSettingGrid .selectpickerddl');
                    },
                });
                if (isFilter) {
                    var companyId = appliedSelections[0].companyId;
                    if (companyId == $("#ddlCompany").val()) {
                        $.each(appliedSelections, function (key, value) {
                            var row = $('tr[data-employeeid="' + value["employeeId"] + '"]');
                            row.find(".YNClass").prop("checked", true);
                            row.find(".settingGolasStartDate").val($("#SettingGoalsStartDate").val());
                            row.find(".settingGolasDueDate").val($("#SettingGoalsDueDate").val());
                        });
                    }
                }
            },
            error: function (data, xhr, status) {
                ShowMessage("error", "Some technical error occurred");
                globalFunctions.onFailure(data, xhr, status);
            }
        });
}