﻿var selectedValues = [];
function SavePage()
{
    delete (selectedValues);
    var Tweight = 0;
    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var businessTargetId = row.data("businesstargetid");
        var businessTargetDetails = row.find('.businesstarget').val();
        var weight = row.find('.weight').val();
        if (weight != "") {
            Tweight = parseInt(Tweight) + parseInt(weight);
        }

        if ((businessTargetDetails != "") && (weight != ""))
            selectedValues.push({ businessTargetId: businessTargetId, businessTargetDetails: businessTargetDetails, weight: weight });
    });
    if (Tweight > 100) {
        ShowMessage("error", "Invalid Total KPI.");
    }
    else {
       
        var grade = $("#txtGrade").val();
        if (grade == '' || grade == undefined) {
            $("#txtGrade-validationMsg").css("color", "red");
            $("#txtGrade-validationMsg").text("This field is mandatory");
            $("#txtGrade-validationMsg").show();
        }
        else {
            $("#txtGrade-validationMsg").text("");
            $("#txtGrade-validationMsg").hide();

            if (selectedValues.length > 0) {
                $.ajax({
                    url: "/PDRP/AnnualAppraisal/SaveAnnualMidFormBData",
                    type: 'POST',
                    data: { model: JSON.stringify(getBusinessTargetModeldata()) },
                    success: function (result) {
                        ShowMessage(result.CssClass, result.Message);
                        selectedValues = [];
                        if (result.Success) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    }
                });
            }
            else
                ShowMessage("error", "Please add business target details.");
        }
    }
}
$(document).ready(function () {
    $(document).on("click", "#LMMidbtnSave", function () {
        SavePage();

    });
    $(document).on("click", "#LMMidbtnSubmit", function () {
        $("#FormState").val('Mid Year LMS')
       
        
        var annualGoalSettingFormAId = $("#AnnualGoalSettingFormAId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 36;
        var goalSettingId = $("#GoalSettingId").val();
        if ($("#isSaved").val() == "True") {
            SavePage();
        }
        else
            ShowMessage("error", "Please save business target details first.");


    });
    $(document).on("click", "#LMMidbtnSign", function () {
        $("#FormState").val('Mid Year EM')
        SavePage();

    });


    FullYearControls();

    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });

    $("#divBusinessTargets").on('change', '.weight', function () {
        var calculated_total_sum = 0;

        $("#tblBusinessTargets .weight").each(function () {

            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                calculated_total_sum += parseFloat(get_textbox_value);
            }

        });
        if (calculated_total_sum > 100) {
            $("#txtTotalWeightage").css({ "border-color": "Red", "border-width": "thick" });
        }
        else {
            $("#txtTotalWeightage").css({ "border-color": "#d5d5d5", "border-width": "thin" });
        }
        $("#txtTotalWeightage").val(calculated_total_sum);
    });

    $(".wid180").on('change', function () {

        var totalScore = 0;
        $("#tblBusinessTargets #ProficiencyScore").each(function () {
            var item_Weight = $(this).closest('tr').find('#item_Weight').val();
            var Score = $(this).closest('tr').find('#item_Score');
            if (parseInt(item_Weight) > 0) {
                var sc = parseInt($(this).val()) * parseInt(item_Weight);
                Score.val(sc);
                totalScore = parseInt(totalScore) + parseInt(sc);

            }

        });

        alert(0);

        $("#BusinessTargetsTota").val(totalScore);
        $("#divTotalScore").html(totalScore);
        $("#TotalScore1").val(totalScore);
        $("#divTotalScore1").html(totalScore);

        OverallAppraisalScore();
    });

    $(".wid181").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblBehavioralCompetenciesFormB #ProficiencyScore").each(function () {

            if (parseInt($(this).val()) > 0) {
                var sc = parseInt($(this).val());
                totalScore = parseInt(totalScore) + parseInt(sc);
                cnt++;
            }

        });


        $("#BehavioralTotalScore").val(totalScore / 5);
        $("#divBehavioralTotalScore").html(totalScore / 5);

        OverallAppraisalScore();

    });
    $(".wid182").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblProfessionalCompetenciesFormB #ProficiencyScore").each(function () {

            if (parseInt($(this).val()) > 0) {
                var sc = parseInt($(this).val());
                totalScore = parseInt(totalScore) + parseInt(sc);
                cnt++;
            }

        });


        $("#ProfessionalTotalScore").val(totalScore / 5);
        $("#divProfessionalTotalScore").html(totalScore / 5);

        OverallAppraisalScore();
    });

});

function getBusinessTargetModeldata() {
    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinesstargetComments: $("#businessTargetComments").val(),
        CompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),  
        JsonString: JSON.stringify(selectedValues),
        ID: $("#AnnualGoalSettingFormBId").val(),
        JobGrade: $("#txtGrade").val(),
        FormState: $("#FormState").val(),

        //PerformanceGroupId: $("#PerformanceGroupId").val(),

        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds: $("#TrainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val(),
    };
    return businessTargetModel;
}
function FullYearControls() {
    var PeriodId = $('#PeriodId').val();
    if (PeriodId == 2) {
        $('#item_Score').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_BusinessTargetDetails').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_Weight').bind('keypress', function (e) {
            e.preventDefault();
        });
    }
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}