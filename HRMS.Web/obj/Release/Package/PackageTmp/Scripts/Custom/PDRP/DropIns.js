﻿

function BackToTaskList() {
    location.href = "/PDRP/DropIns/DropInManage";
}

function getGrid() {

    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }
   
    var employeeStatus = $("#employeeStatusddl").val();
    var ddlDropInsYearID = $("#ddlKPIYear").val();
    var ddlDropInsYearTxt = $("#ddlKPIYear option:selected").text();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
   
    if (ddlDropInsYearID=="")
    {
        ddlDropInsYearID=0
    }
    //var IncludeInactive = $("#includeInActiveChk").is(':checked');
    //var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        "sAjaxSource": "/PDRP/DropIns/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt,
        "aoColumns": [
           

            { "mData": "EmployeeAlternativeID", "sTitle": "Emp ID", "width": "6%", "sType": "Int", "className": "center-text-align" },
            { "mData": "FirstName", "sTitle": "First Name", "width": "15%" },
            { "mData": "LastName", "sTitle": "Last Name", "width": "15%" },
            { "mData": "Gender",  "bVisible": false,"sTitle": "Gender", "sWidth": "10%" },
            //{ "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            { "mData": "PositionName", "sTitle": "Position Name", "sWidth": "13%" },
            { "mData": "Department", "sTitle": "Department Name", "sWidth": "13%" },
            //{ "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            //{ "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            //{ "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            //{ "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            { "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" },
            { "mData": "ViewDropIns", "bVisible": false, "sTitle": "View DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "CreateDropIns", "bVisible": false, "sTitle": "Create DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "Action", "sTitle": "Actions", "width": "12%", "bSortable": false, "className": "center-text-align" },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/DropIns/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () { },
        "language": { "loadingRecords": "<span class='loader'></span>" }
    });
    
}

$(document).ready(function () {
   
    $('#DropInEmployeeModel_VisitDATE').bind('keypress', function (e) {
        e.preventDefault();
    });
    $('#DropInEmployeeModel_VisitTime').bind('keypress', function (e) {
        e.preventDefault();
    });
    getGrid();
    
    $(".wid180").on('change', function () {       
        var scorevalues = [];
        $("#tblDropInProficiencyLevel #ProficiencyScore").each(function () {           
            var get_textbox_value = $(this).val();
            if (get_textbox_value > 0) {
                scorevalues.push(get_textbox_value);
            }
        });
        CalculateDropInScore(scorevalues);        
    });

    $("#ddlKPIYear").on('change', function () {
        getGrid();
    });
    
   

    $("#tbtnSave").on('click', function () {
        $("#isAddMode").val("2");
        $("#DropInsForm").submit();
    });
    $("#tbtnSubmit").on('click', function () {
        $("#isAddMode").val("3");
        $("#DropInsForm").submit();
    });

    $("#btnSubmit").on('click', function () {
        $("#isAddMode").val("3");
    });
    $("#btnSave").on('click', function () {
        $("#isAddMode").val("2");
    });
   
    var ReqStatusID = $("#ReqStatusID").val();    
    if (ReqStatusID == 4) {
        $(".btnSave").hide();
        $(".btn-submit").hide();
        
    }

    var RequesterID = $("#DropInEmployeeModel_RequesterID").val();
    var SuperviserID = $("#DropInEmployeeModel_SuperviserID").val();
    if (RequesterID != SuperviserID) {
       
        $("#TopButtons").html("");      

    }

    $(".selectpicker").selectpicker("refresh");
    //if ($('#hdVisitDATE').val() == "") {
    //    $('#DropInEmployeeModel_VisitDATE').val("");
    //}
    //else
    //{
    //    $('#DropInEmployeeModel_VisitDATE').val($('#hdVisitDATE').val());

    //}
    //if ($('#hdVisitTime').val() == "") {
    //    $('#DropInEmployeeModel_VisitTime').val("");
    //    //alert($('#hdVisitTime').val());
    //}
    //else {
    //    $('#DropInEmployeeModel_VisitTime').val($('#hdVisitTime').val());

    //}
});


function CalculateDropInScore(scorevalues) {
   
    var ddlDropInsYearID = $("#DropInEmployeeModel_DropinYearID").val();
   
    $.ajax({        
        type: 'POST',
        data: { Scorevalues: scorevalues, FormulaYearID: ddlDropInsYearID },
        traditional: true,
        url: '/PDRP/DropIns/CalculateDropInScore',
        success: function (data) {
            $("#ProficiencyScoreFormula").val(data.Formula);
            $("#ProficiencyScoreTotal").val(data.Total);
            $("#divProficiencyScoreTotal").text(data.Total);
            
            $("#ProficiencyScoreOverall").val(data.OverallScore);
            $("#divProficiencyScoreOverall").text(data.OverallScore);
            
        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}


function ViewDeductionDetails(TeacherID, TeacherName, DropInYearID, DropInYear) {
    //InstallmentPayDeductionId = PayDeductionId;
    $("#modal_Loader3").load("/PDRP/DropIns/ViewAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYearID, function () {
        $("#myModal3").modal("show");
        $(".modal-dialog").attr("style", "width:750px;");
        $("#modal_heading3").html("<i class='fa fa-user'></i> " + TeacherName + "'s Drop Ins - For: " + DropInYear);
        $(".close").removeClass('hidden');
        getAllDropInsListGrid(TeacherID, DropInYearID);
    });
}
function getAllDropInsListGrid(TeacherID, DropInYear) {

    //alert(TeacherID);
    oTableChannel = $('#tbl_AllDropIns').dataTable({
        "sAjaxSource": "/PDRP/DropIns/GetAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear,
        "aoColumns": [


            { "mData": "ID", "sTitle": "ID", "bVisible": false, "width": "6%", "className": "center-text-align" },
            { "mData": "VisitDATE", "sTitle": "Visit Date", "width": "30%" },
            { "mData": "Total", "sTitle": "Total", "width": "30%" },
            { "mData": "OverAllScore", "sTitle": "Over All Score", "sWidth": "10%" },
            //{ "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            //{ "mData": "PositionName", "sTitle": "Position Name", "sWidth": "13%" },
            //{ "mData": "Department", "sTitle": "Department Name", "sWidth": "13%" },
            //{ "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            //{ "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            //{ "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            //{ "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            //{ "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "ViewDropIns", "bVisible": true, "sTitle": "View DropIns", "width": "5%", "className": "center-text-align" },
            //{ "mData": "CreateDropIns", "bVisible": true, "sTitle": "Create DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "Action", "sTitle": "Actions", "width": "24%", "bSortable": false, "className": "center-text-align" },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/DropIns/GetAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () { },
        "language": { "loadingRecords": "<span class='loader'></span>" }
    });

}

function ViewDropIns(PDRPDropInID, TeacherID, DropInYear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { PDRPDropInID: PDRPDropInID, isAddMode: 2, EMployeeID: TeacherID, DropInYear: DropInYear },
        url: '/PDRP/DropIns/SetDropInsActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.open("/PDRP/DropIns/DropInForm/");
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
function CreateDropIn(id, dropinyear) {
    ////*** Block Dropin creation if Annual appraisal is Started for this employee
     //$.ajax({
     //   dataType: 'json',
     //   type: 'POST',
     //   data: { EMployeeID: id, DropInYear: dropinyear,isAddMode:1 },
     //   url: '/PDRP/DropIns/ValidatingAnnualAppraisal',
     //   success: function (data) {
     //       if (data.result.length > 0) {
     //           ShowMessage(data.result, data.resultMessage);
     //       }
     //       else
     //       {
                //*** isAddMode
                //*** 0 = View
                //*** 1 = Add
                //*** 2 = Edit
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    data: { EMployeeID: id, DropInYear: dropinyear,isAddMode:1 },
                    url: '/PDRP/DropIns/SetDropInsActiveEmployeeIDSesson',
                    success: function (data) {
                        if (data.success == true) {
                            window.location.href = "/PDRP/DropIns/DropInForm/";
                        }
                    },
                    error: function (data) {
                        ShowMessage("error", "Some technical error occurred");
                    }
                });
    //        }
    //    },
    //    error: function (data) {
    //        ShowMessage("error", "Some technical error occurred");
    //    }
    //});
    
}







function ExportToExcel() {
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToExcel?employeeStatus=" + employeeStatus);
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToPdf?employeeStatus=" + employeeStatus);
    }
    else ShowMessage("warning", "No data for export!");
}