﻿var selectedValues = [];
function SavePage() {
    delete (selectedValues);
    var Tweight = 0;
    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var kpiId = row.data("kpiid");
        var businessTargetDetails = row.find('.kpibusinesstarget').val();
        var weight = row.find('.weight').val();
        if (weight != "") {
            Tweight = parseInt(Tweight) + parseInt(weight);
        }

        if ((businessTargetDetails != "") && (weight != ""))
            selectedValues.push({ kpiId: kpiId, businessTargetDetails: businessTargetDetails, weight: weight });
    });
    if (Tweight > 100) {
        ShowMessage("error", "Invalid Total KPI.");
    }
    else {
        var grade = $("#txtGrade").val();
        if (grade == '' || grade == undefined) {
            $("#txtGrade-validationMsg").css("color", "red");
            $("#txtGrade-validationMsg").text("This field is mandatory");
            $("#txtGrade-validationMsg").show();
        }
        else {
            $("#txtGrade-validationMsg").text("");
            $("#txtGrade-validationMsg").hide();
            if (selectedValues.length > 0) {
               
                $.ajax({
                    url: "/PDRP/GoalSetting/SaveAnnualGoalSettingsFormBData",
                    type: 'POST',
                    data: { model: JSON.stringify(getBusinessTargetModeldata()) },
                    success: function (result) {
                        ShowMessage(result.CssClass, result.Message);
                        selectedValues = [];
                        if (result.Success) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    }
                });
            }
            else
                ShowMessage("error", "Please add business target details.");
        }
    }
}
$(document).ready(function () {
    $(document).on("click", "#prebtnSave", function () {
        
        SavePage();
    });

    $(document).on("click", ".btnSubmit", function () {
        SavePage();
        var approvalGoalSettingFormBId = $("#AnnualGoalSettingFormBId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 37;
        var goalSettingId = $("#GoalSettingId").val();
        if ($("#isSaved").val() == "True") {
            $.ajax({
                url: "/PDRP/GoalSetting/SaveFormApprovalForLineManagerFormB",
                type: 'POST',
                data: { id: approvalGoalSettingFormBId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                }
            });
        }
        else
            ShowMessage("error", "Please save business target details first.");
    });

    $(document).on("click", ".btnLMSubmit", function () {
        SavePage();
        var approvalGoalSettingFormBId = $("#AnnualGoalSettingFormBId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 37;
        var requestId = $("#RequestId").val();
        $.ajax({
            url: "/PDRP/GoalSetting/SaveFormApprovalForEmployeeSignOffForms",
            type: 'POST',
            data: { id: approvalGoalSettingFormBId, employeeId: employeeId, formId: formId, performanceGroupId: 2, requestId: requestId },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                selectedValues = [];
                if (result.Success) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            }
        });
    });

    $(document).on("click", ".btnEmpSignOffSubmit", function () {
        var approvalGoalSettingFormBId = $("#AnnualGoalSettingFormBId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 37;
        var requestId = $("#RequestId").val();
        var goalSettingId = $("#GoalSettingId").val();
        $.ajax({
            url: "/PDRP/GoalSetting/SubmitEmployeeSignOffForms",
            type: 'POST',
            data: { id: approvalGoalSettingFormBId, employeeId: employeeId, formId: formId, performanceGroupId: 2, requestId: requestId, goalSettingId: goalSettingId },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                selectedValues = [];
                if (result.Success) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            }
        });
    });

    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });



    $("#divKPIFormB").on('change', '.weight', function () {
        var calculated_total_sum = 0;

        $("#tblKPIFormB .weight").each(function () {

            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                calculated_total_sum += parseFloat(get_textbox_value);
            }

        });
        if (calculated_total_sum > 100) {
            $("#txtTotalWeightage").css({ "border-color": "Red", "border-width": "thick" });
        }
        else {
            $("#txtTotalWeightage").css({ "border-color": "#d5d5d5", "border-width": "thin" });
        }
        $("#txtTotalWeightage").val(calculated_total_sum);
    });
});

function getBusinessTargetModeldata() {
    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        KPIsComments: $("#kpiComments").val(),
        CompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        ID: $("#AnnualGoalSettingFormBId").val(),
        JobGrade: $("#txtGrade").val()
    };
    return businessTargetModel;
}