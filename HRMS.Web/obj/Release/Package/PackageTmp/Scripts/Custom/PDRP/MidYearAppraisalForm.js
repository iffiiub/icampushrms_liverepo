﻿var selectedValues = [];
var Competencies1 = [];
var Competencies2 = [];

function SaveForm()
{
    var Errormsg = "";
    delete (selectedValues)
    delete (Competencies1)
    delete (Competencies2)

    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var businesstargetid = row.data("businesstargetid");
        var businessTargetDetails = row.find('.businesstarget').val();
        var accomplishments = row.find('.accomplishments').val();
        var weight = row.find('.weight').val();
        var wid180 = row.find('.wid180').val();
        var Score = row.find('.Score').val();

        if ((businessTargetDetails != "") && (weight != "")) {
            if (Score == 0 || Score == "") {
                Errormsg = "Please Select Rating for all business target"

            }
            else {
                selectedValues.push({ businesstargetid: businesstargetid, businessTargetDetails: businessTargetDetails, Accomplishments: accomplishments, weight: weight, Rating: wid180 });
            }
        }
    });


    $(".clsProfessionalCompetencies").each(function () {

        var row = $(this);
        var wid182 = row.find('.wid182').val();
        var vNo = row.find('td:eq(0)').text();


        Competencies1.push({ No: parseInt(vNo), Score: parseInt(wid182) })

    });
    

    $(".clsBehavioralCompetencies").each(function () {
        var row = $(this);
        var wid181 = row.find('.wid181').val();
        var vvNo = row.find('td:eq(0)').text();


        Competencies2.push({ No: vvNo, Score: wid181 })

    });




    if ($("#OverallAppraisalScore").val() == 0) {
        Errormsg = Errormsg + "</br>Overall Score cannot be zero!"
    }
    if (Errormsg != "") {
        ShowMessage("error", Errormsg);
    }
    else {
        if (selectedValues.length > 0) {
            $.ajax({
                url: "/PDRP/AnnualAppraisal/SaveFullYearAppraisalFormData",
                type: 'POST',
                data: { model: JSON.stringify(getFullYearAppraisalModelData()) },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                }
            });
        }
        else
            ShowMessage("error", "Please add business target details.");
    }
}

$(document).ready(function () {
    $(document).on("click", "#prebtnSave", function () {
        $(".businesstargetandweightsection").each(function () {
            var row = $(this);
            var businesstargetid = row.data("businesstargetid");
            var businessTargetDetails = row.find('.businesstarget').val();
            var weight = row.find('.weight').val();
            if ((businessTargetDetails != "") && (weight != ""))
                selectedValues.push({ businesstargetid: businesstargetid, businessTargetDetails: businessTargetDetails, weight: weight });
        });

        if (selectedValues.length > 0) {
            $.ajax({
                url: "/PDRP/AnnualAppraisal/SaveMidYearAppraisalFormData",
                type: 'POST',
                data: { model: JSON.stringify(getMidYearAppraisalModelData()) },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                }
            });
        }
        else
            ShowMessage("error", "Please add business target details.");
    });

    $(document).on("click", ".btnSubmit", function () {
        var midYearAppraisalFormModelId = $("#MidYearAppraisalFormModelId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 40;
        var requestId = $("#RequestId").val();
        var performanceGroupId = $("#PerformanceGroupId").val();
        var annualAppraisalProcessId = $("#AnnualAppraisalProcessId").val();
        if ($("#isSaved").val() == "True") {
            $.ajax({
                url: "/PDRP/GoalSetting/SaveFormApprovalForEmployeeSignOffForms",
                type: 'POST',
                data: { id: midYearAppraisalFormModelId, employeeId: employeeId, formId: formId, performanceGroupId: performanceGroupId, requestId: requestId, annualAppraisalProcessId: annualAppraisalProcessId },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                }
            });
        }
        else
            ShowMessage("error", "Please save business target details first.");
    });

    //*** Danny 24/09/2019
    $(document).on("click", "#preFullbtnSave", function () {
        $("#FormSubmit").val("No");
        SaveForm();
    });
    $(document).on("click", "#preFullbtnSubmit", function () {
        $("#FormSubmit").val("YES");
        SaveForm();
       
    });

    FullYearControls();
    
    //////$(".wid180").on('change', function () {

    //////    var totalScore = 0;
    //////    $("#tblBusinessTargets #ProficiencyScore").each(function () {
    //////        var item_Weight = $(this).closest('tr').find('#item_Weight').val();
    //////        var Score = $(this).closest('tr').find('#item_Score');
    //////        if (parseInt(item_Weight) > 0) {
    //////            var sc = parseInt($(this).val()) * parseInt(item_Weight);
    //////            Score.val(sc);
    //////            totalScore = parseInt(totalScore) + parseInt(sc);
               
    //////        }

    //////    });
       
        

    //////    $("#BusinessTargetsTota").val(totalScore);
    //////    $("#divTotalScore").html(totalScore);
    //////    $("#TotalScore1").val(totalScore);
    //////    $("#divTotalScore1").html(totalScore);

    //////    OverallAppraisalScore();
    //////});

    //////$(".wid181").on('change', function () {

    //////    var totalScore = 0;
    //////    var cnt = 0;
    //////    $("#tblBehavioralCompetenciesFormB #ProficiencyScore").each(function () {
           
    //////        if (parseInt($(this).val()) > 0) {
    //////            var sc = parseInt($(this).val());               
    //////            totalScore = parseInt(totalScore) + parseInt(sc);
    //////            cnt++;
    //////        }

    //////    });


    //////    $("#BehavioralTotalScore").val(totalScore / 5);
    //////    $("#divBehavioralTotalScore").html(totalScore / 5);
        
    //////    OverallAppraisalScore();

    //////});
    //////$(".wid182").on('change', function () {

    //////    var totalScore = 0;
    //////    var cnt = 0;
    //////    $("#tblProfessionalCompetenciesFormB #ProficiencyScore").each(function () {

    //////        if (parseInt($(this).val()) > 0) {
    //////            var sc = parseInt($(this).val());
    //////            totalScore = parseInt(totalScore) + parseInt(sc);
    //////            cnt++;
    //////        }

    //////    });


    //////    $("#ProfessionalTotalScore").val(totalScore / 5);
    //////    $("#divProfessionalTotalScore").html(totalScore / 5);
      
    //////    OverallAppraisalScore();
    //////});
});

function getFullYearAppraisalModelData() {
   
    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinessTargetsComments: $("#businessTargetComments").val(),
        CoreCompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        JsonString1: JSON.stringify(Competencies1),
        JsonString2: JSON.stringify(Competencies2),
        ID: $("#MidYearAppraisalFormModelId").val(),
        PerformanceGroupId: $("#PerformanceGroupId").val(),
        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds: $("#trainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val(),
        MyThreeKeyAchivements: $("#MyThreeKeyAchivements").val(),
        MyTwoDevelopmentAreas: $("#MyTwoDevelopmentAreas").val(),
        EmployeeCommentsonFullYearPerformance: $("#EmployeeCommentsonFullYearPerformance").val(),
        Strengths: $("#Strengths").val(),
        FullTrainingNeeds: $("#FullTrainingNeeds").val(),
        LineManagerCommentsonFullYearPerformance: $("#LineManagerCommentsonFullYearPerformance").val(),
        BusinessTargetsTota: $("#BusinessTargetsTota").val(),
        ProfessionalCompetenciesAVG: $("#ProfessionalTotalScore").val(),
        BehavioralCompetenciesAVG: $("#BehavioralTotalScore").val(),
        CompetenciesAVG: $("#BehavioralTotalScore1").val(),
        OverallAppraisalScore: $("#OverallAppraisalScore").val(),
        FormSubmit: $("#FormSubmit").val(),
        RequestId: $("#RequestId").val()

    };
    
    return businessTargetModel;
}

function OverallAppraisalScore()
{
    var ddlDropInsYearID = $("#YearId").val();
    var performanceGroupId = $("#PerformanceGroupId").val();
    var scorevalues = [];
    alert($("#BusinessTargetsTota").val());
    scorevalues.push($("#BusinessTargetsTota").val());
    var s = (parseInt($("#ProfessionalTotalScore").val()) + parseInt($("#BehavioralTotalScore").val()) / 2)

    $("#BehavioralTotalScore1").val(s);
    $("#divBehavioralTotalScore1").html(s);
    scorevalues.push(Math.round(s));
   

   
    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, FormulaYearID: ddlDropInsYearID, PerformanceGroupId: performanceGroupId },
        traditional: true,
        url: '/PDRP/AnnualAppraisal/CalculateDropInScore',
        success: function (data) {
            
            $("#OverallScoreFormula").val(data.Formula);
            $("#OverallAppraisalScore").val(data.OverallScore);
            $("#divOverallScore").text(data.OverallScore);
          
        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });

}
function getMidYearAppraisalModelData() {
    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinessTargetsComments: $("#businessTargetComments").val(),
        CoreCompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        ID: $("#MidYearAppraisalFormModelId").val(),
        PerformanceGroupId: $("#PerformanceGroupId").val(),
        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds:  $("#trainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val()
    };
    return businessTargetModel;
}

function FullYearControls() {
    var PeriodId = $('#PeriodId').val();
    if (PeriodId == 2) {
        $('#item_Score').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_BusinessTargetDetails').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_Weight').bind('keypress', function (e) {
            e.preventDefault();
        });
    }
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}
