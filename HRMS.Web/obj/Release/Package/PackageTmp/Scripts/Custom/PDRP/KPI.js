﻿$(document).ready(function () {
   
//===========================================================================
    $("#divYearlyKPIs").on('input', '.weightage', function () {
        var calculated_total_sum = 0;
        
        $("#tblKPIs .weightage").each(function () {
            
            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                calculated_total_sum += parseFloat(get_textbox_value);
            }
            
        });
        if (calculated_total_sum > 100)
        {
            $("#txtTotalWeightage").css({ "border-color": "Red","border-width": "thick" });
        }
        else
        {
            $("#txtTotalWeightage").css({ "border-color": "#d5d5d5", "border-width": "thin" });
        }
        $("#txtTotalWeightage").val(calculated_total_sum);
    });

   
    //================================================================================

    $("#btn_saveAll").on('click', function () {
       
        $("#frmKPIs").submit();

    });

//=====================================================================================

    $("#ddlKPIYear").on('change', function () {
        
        GetKPI();
        
    });
    
    $("#ddlKPICompany").on('change', function () {
        GetKPI();

    });

});

function GetKPI()
{
    pageLoaderFrame();
    var vCompanyID = $('#ddlKPICompany').val();

    var vkpiyear = $('#ddlKPIYear').val();
    //if (vkpiyear == "") {
    //    var currentTime = new Date();
    //    var year = currentTime.getFullYear()
    //    vkpiyear = year;
    //}


    $('#KPIYear').val(vkpiyear);
    if (vCompanyID == '')
    {
        vCompanyID = 0;
    }
    
    $.ajax({
        url: '/PDRP/KPIs/GetYearlyKPIs',
        data: { kpiyear: vkpiyear, CompanyID: vCompanyID },
        type: 'GET',
        success: function (data) {
            $("#divYearlyKPIs").html(data);



        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);
        }
    });
    hideLoaderFrame();
}