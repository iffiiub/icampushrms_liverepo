﻿var selectedValues = [];
var isSelectedValues = false;
var appliedSelections = [];
var isFilter = false;

$(document).ready(function () {
    loadAnnualAppraisalProcessData();

    $('#btnSearch').on('click', function () {
        loadAnnualAppraisalProcessData();
        isSelectedValues = false;
        isFilter = true;    
    });

    $(document).on("click", "#btnApplyToAll", function () {
        delete (appliedSelections);

        $(".YNClass").each(function () {
           
            if ($(this).is(':checked')) {
                
                isSelectedValues = true;
                var startDate = $(this).closest('tr').find('.evaluationStartDate');
                var MidYearStat = $(this).closest('tr').find("td").eq(3).text();
                var periodId = $("#ddlPeriod").val();
                if (startDate.val() == '' || startDate.val() == undefined)
                    var dueDate = $(this).closest('tr').find('.evaluationDueDate');
                if ((startDate.val() == '' || startDate.val() == undefined) && (dueDate.val() == '' || dueDate.val() == undefined)) {
                    if ($("#EvaluationStartDate").val() == '')
                        ShowMessage("error", "Please select Setting Goals Start date.");
                    else if ($("#EvaluationDueDate").val() == '')
                        ShowMessage("error", "Please select Setting Goals Due date.");
                    else {
                        //--*** Danny 22/09/2019
                        //alert($(this).data("employeeid"));
                        //alert(MidYearStat);
                        //alert(periodId);
                        
                        if (periodId == 2 && MidYearStat=='Yes')
                        {
                            startDate.val($("#EvaluationStartDate").val());
                            dueDate.val($("#EvaluationDueDate").val());
                            appliedSelections.push({ employeeId: $(this).data("employeeid"), companyId: $(this).data("companyid") });
                        }
                        else if (periodId == 1 && MidYearStat == 'No')
                        {
                           
                            startDate.val($("#EvaluationStartDate").val());
                            dueDate.val($("#EvaluationDueDate").val());
                            appliedSelections.push({ employeeId: $(this).data("employeeid"), companyId: $(this).data("companyid") });
                        }
                        else
                        {
                            ShowMessage("error", "Invalid Selection.");
                        }
                    }
                }
            }
        });
        if (!isSelectedValues) 
            ShowMessage("error", "Please seelct at least one checkbox");
        else
            isSelectedValues = false;

    });

    $(document).on("click", "#btnSubmit", function () {
        var employees = " ";
        var isMidYearAppraisalSubmitted;
        var periodId;
        delete (selectedValues);
        $(".YNClass").each(function () {
            if ($(this).is(':checked')) {
                
                //--*** Danny 22/09/2019
                var performceGroupId = $(this).closest('tr').find('.performcacegroup').val();
                var employeeId = $(this).data("employeeid");
                var yearId = $("#ddlKPIYear").val();
                var startdate = $(this).closest('tr').find('.evaluationStartDate').val();
                var duedate = $(this).closest('tr').find('.evaluationDueDate').val();
                periodId = $("#ddlPeriod").val();
                isMidYearAppraisalSubmitted = $(this).closest('tr').data("ismidyearappraisalsubmitted");
                var employeeName = $(this).closest('tr').find("td").eq(5).text();
                var vProcessName = $('#ProcessName').val();
                
                employees = employees + employeeName + ",";
                if (startdate != "" || duedate != "") {
                    selectedValues.push({ performanceGroupId: performceGroupId, EmployeeID: employeeId, YearId: yearId, EvaluationStartDate: startdate, EvaluationDueDate: duedate, periodId: periodId, ProcessName: vProcessName });
                }
            }
        });
        
        if (periodId == 2 && isMidYearAppraisalSubmitted == "False")
            ShowMessage("error", "Please submit Mid year Appraisal Process first for employees :" + employees.slice(0, -1));
        else {
            $.ajax({
                url: "/PDRP/AnnualAppraisal/SaveAnnualAppraisalProcessData",
                type: 'POST',
                data: { jsonString: JSON.stringify(selectedValues) },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    loadAnnualAppraisalProcessData();
                    selectedValues = [];
                }
            });
        }      
    });

    $(document).on("change", "#ddlKPIYear", function () {
        loadAnnualAppraisalProcessData();
        isSelectedValues = false;
        isFilter = true;
    });

});

function loadAnnualAppraisalProcessData() {

    var ddlDropInsYearID = $("#ddlKPIYear").val();
    if (ddlDropInsYearID == "") {
        ddlDropInsYearID = 0
    }
    $.ajax({
        type: 'POST',
        data: { companyId: $("#ddlCompany").val(), yearId: ddlDropInsYearID, isFormDCategory: $("#FormDCategory").is(":checked") },
        traditional: true,
        url: '/PDRP/AnnualAppraisal/GetAnnualAppraisalProcessList',
        success: function (data) {
            $("#divAnnualAppraisalSettings").html(data);
            $('#tblAnnualAppraisalSettingGrid').DataTable({
                "aoColumnDefs": [
                    { "sWidth": "5%", "aTargets": [0] },
                    { "sWidth": "10%", "aTargets": [1, 2, 3, 4, 6, 7, 8, 9] },
                    { "sWidth": "15%", "aTargets": [5] }
                ],
                "fnDrawCallback": function () {
                    bindSelectpicker('#tblAnnualAppraisalSettingGrid .selectpickerddl');
                },
            });
            if (isFilter) {
                var companyId = appliedSelections[0].companyId;
                if (companyId == $("#ddlCompany").val()) {
                    $.each(appliedSelections, function (key, value) {
                        var row = $('tr[data-employeeid="' + value["employeeId"] + '"]');
                        row.find(".YNClass").prop("checked", true);
                        row.find(".evaluationStartDate").val($("#EvaluationStartDate").val());
                        row.find(".evaluationDueDate").val($("#EvaluationDueDate").val());
                    });
                }
            }
        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}