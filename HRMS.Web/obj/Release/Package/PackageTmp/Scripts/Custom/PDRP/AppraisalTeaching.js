﻿
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}
function ViewObservation(id) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        url: '/PDRP/Observations/SetViewObservationSesson',
        success: function (data) {
            if (data.success == true) {


                window.open("/PDRP/Observations/ObservationEvaluation/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
function DateAdd(source) {
    var closesetRow = $(source).closest('tr');
    var vtxtEvlDate = $(closesetRow).find('#txtEvlDate').val();
    var vtxtEvlDueDate = $(closesetRow).find('#txtEvlDueDate').val();
    var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')
    
    if (vtxtEvlDate.length > 0 && vtxtEvlDueDate.length > 0) {
        vchkEmployeeId.prop('checked', true);
        var employeeId = $(closesetRow).addClass('IsEdit');
    }


}
function ViewDeductionDetails(TeacherID, TeacherName, DropInYearID, DropInYear) {
    //InstallmentPayDeductionId = PayDeductionId;
    $("#modal_Loader3").load("/PDRP/DropIns/ViewAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYearID, function () {
        $("#myModal3").modal("show");
        $(".modal-dialog").attr("style", "width:750px;");
        $("#modal_heading3").html("<i class='fa fa-user'></i> " + TeacherName + "'s Drop Ins - For: " + DropInYear);
        $(".close").removeClass('hidden');
        getTop3DropInsListGrid(TeacherID, DropInYearID);
    });
}

function SelectAll() {



    var table = $("#tbl_employee").DataTable();
    var UpdatedTr1 = table.$(".odd");
    var UpdatedTr2 = table.$(".even");
    $(UpdatedTr1).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            chbox.prop('checked', false);

        }
        else {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', true);
            }

        }
        ChkChange(this);

    });
    $(UpdatedTr2).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            chbox.prop('checked', false);

        }
        else {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', true);
            }

        }
        ChkChange(this);

    });

}
function ChkChange(source) {

    var closesetRow = $(source).closest('tr');
    var vdatepicker = $(closesetRow).find('.datepicker').val();
    //var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if ($(source).prop("checked") == true) {
        if (vdatepicker.length > 0) {

            var employeeId = $(closesetRow).addClass('IsEdit');
        }
    }
    else if ($(source).prop("checked") == false) {
        var employeeId = $(closesetRow).removeClass('IsEdit');
    }


}
function getTop3DropInsListGrid(TeacherID, DropInYear) {

    //alert(TeacherID);
    oTableChannel = $('#tbl_AllDropIns').dataTable({
        "sAjaxSource": "/PDRP/DropIns/GetTop3DropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear,
        "aoColumns": [


            { "mData": "ID", "sTitle": "ID", "bVisible": false, "width": "6%", "className": "center-text-align" },
            { "mData": "VisitDATE", "sTitle": "Visit Date", "width": "30%" },
            { "mData": "Total", "sTitle": "Total", "width": "30%" },
            { "mData": "OverAllScore", "sTitle": "Over All Score", "sWidth": "10%" },
            //{ "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            //{ "mData": "PositionName", "sTitle": "Position Name", "sWidth": "13%" },
            //{ "mData": "Department", "sTitle": "Department Name", "sWidth": "13%" },
            //{ "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            //{ "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            //{ "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            //{ "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            //{ "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "ViewDropIns", "bVisible": true, "sTitle": "View DropIns", "width": "5%", "className": "center-text-align" },
            //{ "mData": "CreateDropIns", "bVisible": true, "sTitle": "Create DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "Action", "sTitle": "Actions", "width": "24%", "bSortable": false, "className": "center-text-align" },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/DropIns/GetTop3DropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () { },
        "language": { "loadingRecords": "<span class='loader'></span>" }
    });

}

function ViewDropIns(PDRPDropInID, TeacherID, DropInYear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { PDRPDropInID: PDRPDropInID, isAddMode: 2, EMployeeID: TeacherID, DropInYear: DropInYear },
        url: '/PDRP/DropIns/SetDropInsActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {               
                window.open("/PDRP/DropIns/DropInForm/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}

$('#TalentAreaImage').bind('change', function () {
    //if (jQuery('#TalentAreaImage').val() > 0) {
    //    var imgeUrl = '/Areas/PDRP/images/' + jQuery('#TalentAreaImage').val() + '.jpg';
    //    jQuery('#imageHolder').html('<img class="block-full-width" src="' + imgeUrl + '" />');
    //}
    //else {
    //    jQuery('#imageHolder').html('');
    //}
});

$(document).ready(function () {

    $('.KPIScoreT').bind('keydown', function (e) {
       
        e.preventDefault();
    });

    $('#txtVisitDate').datepicker({
        changeMonth: false,
        changeYear: false,
        maxDate: new Date('2020-3-26'),
        todayHighlight: true,
        autoclose: true
    });
    //$(".datepicker").datepicker("destroy");
    //$(".datepicker").datepicker({
    //    changeMonth: true,
    //    changeYear: false

    //});
    $('#txtVisitTime').datepicker({
        changeMonth: false,
        changeYear: false,
        maxDate: new Date('26/3/2026'),
        todayHighlight: true,
        autoclose: true
        
    });
    //$('#txtVisitDatee').datepicker({
    //    changeMonth: false,
    //    changeYear: false,
    //    maxDate: new Date('2020-3-26')
    //});
    //$("datepicker").datepicker({
    //    changeMonth: false,
    //    changeYear: false,
    //    dateFormat: "dd/mm/yy"

    //});

    //$(".datepicker").datepicker({ dateFormat: "dd/mm/yy" });
    $(".wid180").on('change', function () {
        
        var scorevalues = [];
        
        $("#tblKPIs #KPIRatings").each(function () {

            var get_textbox_value = $(this).val();
            //if (get_textbox_value > 0) {
                scorevalues.push(get_textbox_value);
            //}
        });
        var weightages = [];
        $("#tblKPIs #KPIWeightage").each(function () {

            var get_textbox_value = $(this).val();
            //if (get_textbox_value > 0) {
                weightages.push(get_textbox_value);
            //}
        });

        CalculateKPIScore(scorevalues,weightages);

    });

    $("#tbtnSave").on('click', function () {
        //var FormMode1=$("#FormMode1").val();
        $("#FormMode").val(1);
        //if ($("#FormMode1").val() == 6) {
        //    $("#FormMode").val(parceint(FormMode1+1));
        //}
        //else if ($("#FormMode1").val() == 4) {
        //    $("#FormMode").val(5);
        //}
        //else if ($("#FormMode1").val() == 2) {
        //    $("#FormMode").val(3);
        //}
        //else {
        //    $("#FormMode").val(1);
        //}
        
        $("#AppraisalTeachingForm").submit();
    });
    $("#tbtnSubmit").on('click', function () {
     //var FormMode1=$("#FormMode1").val();
        $("#FormMode").val(2);
        //if ($("#FormMode1").val() == 6) {
        //    $("#FormMode").val(8);
        //}
        //else if ($("#FormMode1").val() == 4) {
        //    $("#FormMode").val(6);
        //}
        //else if ($("#FormMode1").val() == 2) {
        //    $("#FormMode").val(4);  
        //}
        //else {
        //    $("#FormMode").val(2);
        //}
        $("#AppraisalTeachingForm").submit();
    });
    $("#tbtnSignOff").on('click', function () {
        //var FormMode1=$("#FormMode1").val();
        $("#FormMode").val(4);
        //if ($("#FormMode1").val() == 6) {
        //    $("#FormMode").val(8);
        //}
        //else if ($("#FormMode1").val() == 4) {
        //    $("#FormMode").val(6);
        //}
        //else if ($("#FormMode1").val() == 2) {
        //    $("#FormMode").val(4);  
        //}
        //else {
        //    $("#FormMode").val(2);
        //}
        $("#AppraisalTeachingForm").submit();
    });

    $("#btnSave").on('click', function () {
        $("#FormMode").val(1);
        // if ($("#FormMode1").val() == 6) {
        //    $("#FormMode").val(7);
        // }
        // else if ($("#FormMode1").val() == 4) {
        //    $("#FormMode").val(5);
        //}
        //else if ($("#FormMode1").val() == 2)
        //{
        //    $("#FormMode").val(3);
        //}
        //else
        //{
        //    $("#FormMode").val(1);
        //}
        
    });


   

    $("#btnSubmit").on('click', function () {
     $("#FormMode").val(2);
        //if ($("#FormMode1").val() == 6) {
        //    $("#FormMode").val(8);
        //}
        //else if ($("#FormMode1").val() == 4) {
        //    $("#FormMode").val(6);
        //}
        //else if ($("#FormMode1").val() == 2) {
        //    $("#FormMode").val(4);
        //}
        //else {
        //    $("#FormMode").val(2);
        //}
    });
    $("#btnSignOff").on('click', function () {
        $("#FormMode").val(4);
        //if ($("#FormMode1").val() == 6) {
        //    $("#FormMode").val(8);
        //}
        //else if ($("#FormMode1").val() == 4) {
        //    $("#FormMode").val(6);
        //}
        //else if ($("#FormMode1").val() == 2) {
        //    $("#FormMode").val(4);
        //}
        //else {
        //    $("#FormMode").val(2);
        //}
    });
    $("#btnCancel").on('click', function () {
        $("#FormMode").val(10);
        
    });

});
function GetScoreSlab(thisyearscore)
{

    $.ajax({
        type: 'POST',
        data: { ThisYearScore: thisyearscore},
        traditional: true,
        url: '/PDRP/AppraisalTeaching/GetScoreSlab',
        success: function (data) {
            $("#ThisYearScoreDescription").val(data.ScoreSlab);
            $("#AppraisalTeachingEmployeeModel_ThisYearScoreDescription").val(data.ScoreSlab);          

        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}
function CalculateKPIScore(scorevalues, weightages) {
   
    var ddlDropInsYearID = $("#AppraisalTeachingEmployeeModel_AppraisalTeachingYearID").val();
    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, Weightages: weightages, FormulaYearID: ddlDropInsYearID },
        traditional: true,
        url: '/PDRP/AppraisalTeaching/CalculateKPIScore',
        success: function (data) {
            $("#KPIFormula").val(data.Formula);
            $("#KPIScore").val(data.Total);
            
            $("#divTotalKPIScore").html(data.OverallScore);
            $("#TotalKPIScore").val(data.OverallScore);
            $("#tTotalKPIScore").val(data.OverallScore);
            var drop = $("#DropInScoreOverall").val();
            var observ = $("#ObservEvlScoreOverall").val();
           
            observ = parseFloat(observ) + parseFloat(drop) + parseFloat(data.OverallScore);
           

            
            $("#TopThisYearScore").val(parseFloat(observ));
            $("#ThisYearScore").val(parseFloat(observ));

            $.each(data.ItemTotal, function (index, element) {
               
                //alert(element.toFixed(2));
                //var formElements = $('#tblKPIs #KPIScore'+(index+1)).val();
                //alert(formElements);
                $('#tblKPIs #KPIScore' + (index + 1)).val(element.toFixed(2));
               
            });
           
            GetScoreSlab($("#ThisYearScore").val());

        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}
$("#ddlPDRPYears").on('change', function () {

    getEmployeeGrid();

});

$("#ddlKPICompany").on('change', function () {
    getEmployeeGrid(); 

});
function ApplytoallSelections() {
    var VisitDate = $('#txtVisitDate').val().trim();
    var VisitTime = $('#txtVisitTime').val().trim();
    var Ch = 0;
    if (VisitDate.length > 0 && VisitTime.length > 0) {
        var table = $("#tbl_employee").DataTable();
        var UpdatedTr1 = table.$(".odd");
        var UpdatedTr2 = table.$(".even");
        $(UpdatedTr1).each(function () {
            var row = $(this);
            console.log(row);
            if (row.find('input[type="checkbox"]').is(':checked') &&
               row.find('.datepicker').val().length <= 0) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);
                $(this).find('.EvlDate').val(VisitTime);
                Ch = 1;
            }
        });
        $(UpdatedTr2).each(function () {
            var row = $(this);
            console.log(row);
            if (row.find('input[type="checkbox"]').is(':checked') &&
               row.find('.datepicker').val().length <= 0) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);
                $(this).find('.EvlDate').val(VisitTime);
                Ch = 1;
            }
        });

        if (Ch == 0) {
            ShowMessage("error", "Please select at least one record.");
        }
    }
    else {
        ShowMessage("error", "Please enter valid evaluation date & evaluation due date");
    }
}
function SetObservationVisitDate(e) {

    //AllEditMode = 0;
    var table = $("#tbl_employee").DataTable();
    var vYearId = $("#ddlPDRPYears").val();
    var ddlYearTxt = $("#ddlPDRPYears option:selected").text();


    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var updatedData = [];
        $(UpdatedTr).each(function () {

            var vEmployeeId = $(this).find('#EmployeeID').val();
            var vVisitDate = $(this).find('.datepicker').val() ;
            var vPreObservationDueDate = $(this).find('.EvlDate').val();
            //var autoID = $(itm).find('td:eq(8)').text();
            //var EmpId = ($(itm).find("#ddlApprover_" + autoID)).val();
            
            updatedData.push({

                EmployeeID: vEmployeeId,
                VisitDate: vVisitDate,
                PreObservationDate: vPreObservationDueDate
            });

        });

        //console.log(updatedData);
        //$.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save " + updatedData.length + " records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/AppraisalTeaching/SetAppraisalVisitDateMulty",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'SelectedTeachersList': updatedData, 'YearId': vYearId }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        getEmployeeGrid();
                    }
                    else {
                        ShowMessage("error", data.Message);

                    }
                    hideLoaderFrame();
                }
            })
        //});
    }
    else {
        ShowMessage("error", "Please select at least one record");
        hideLoaderFrame();
    }
}
function getEmployeeGrid() {

    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }

    var employeeStatus = $("#employeeStatusddl").val();
    var ddlDropInsYearID = $("#ddlPDRPYears").val();
    var ddlDropInsYearTxt = $("#ddlPDRPYears option:selected").text();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    var vCompanyID = $('#ddlKPICompany').val();
    if (ddlDropInsYearID == "") {
        ddlDropInsYearID = 0
    }
    if (vCompanyID == "") {
        vCompanyID = 0
    }
    //var IncludeInactive = $("#includeInActiveChk").is(':checked');
    //var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        "sAjaxSource": "/PDRP/AppraisalTeaching/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&AppraisalYear=" + ddlDropInsYearID + "&AppraisalYearTxt=" + ddlDropInsYearTxt + "&CompanyID=" + vCompanyID,
        "aoColumns": [
             { "mData": "Default", "bVisible": false },
            { "mData": "Select", "bVisible": true, "sTitle": "", "sTitle": "<input type=checkbox id=chkSelevctAll onclick=SelectAll()>", "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "Observation", "bVisible": true, "sTitle": "Lesson Observation Done?", "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "DropInsCnt3", "bVisible": true, "sTitle": "3 DropIns Done?", "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "EmployeeAlternativeID", "bVisible": true, "sTitle": "Emp ID", "width": "6%", "sType": "Int", "className": "center-text-align" },
            { "mData": "FirstName", "sTitle": "First Name", "width": "15%" },
            { "mData": "LastName", "sTitle": "Last Name", "width": "15%" },
            { "mData": "StartDate", "sTitle": "Evaluation Date", "bSortable": true },
            { "mData": "DueDate", "sTitle": "Evaluation Due Date", "bSortable": true },
            //{ "mData": "VisitTime", "sTitle": "Visit Time", "width": "15%" },
            //{ "mData": "PreObservationDueDate", "sTitle": "Pre-Observation Due Date", "bSortable": true },
            { "mData": "EmployeeId", "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "Action", "sTitle": "Action", "bSortable": true },
            { "mData": "KPICount", "bVisible": false},
            { "mData": "DropInCount", "sTitle": "DropIns", "bVisible": false },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/AppraisalTeaching/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&AppraisalYear=" + ddlDropInsYearID + "&AppraisalYearTxt=" + ddlDropInsYearTxt + "&CompanyID=" + vCompanyID,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            $(".datepicker").datepicker("destroy");
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: false,
                autoclose: true
            }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });
            //KPICheck(); //*** Removed On Request
        },
        "language": { "loadingRecords": "<span class='loader'></span>" }
    });

   

}
function KPICheck()
{
   
    var vKPICount = $(".KPICount").text();
    //alert(vKPICount);
    if (vKPICount != "") {
        if (vKPICount == 0) {
            //ShowMessage("error", "KPI is not Set for the current year.</br> You cannot process Appraisal for this year");

            $("#lblKPI").text("KPIs not set for selected year.");
           

        }
        else {
           
            $("#lblKPI").text("");
        }
    }
}


