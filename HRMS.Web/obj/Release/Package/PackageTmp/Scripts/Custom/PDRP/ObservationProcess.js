﻿/// <reference path="KPI.js" />

function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}

function SelectAll()
{
   

   
    var table = $("#tbl_employee").DataTable();
    var UpdatedTr1 = table.$(".odd");
    var UpdatedTr2 = table.$(".even");
    $(UpdatedTr1).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked'))
        {
            chbox.prop('checked', false);

        }
        else
        {
            chbox.prop('checked', true);

        }
        ChkChange(this);
       
    });
    $(UpdatedTr2).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            chbox.prop('checked', false);

        }
        else {
            chbox.prop('checked', true);

        }
        ChkChange(this);
        
    });
   
}
function ViewPreObservation(id) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { ID: id},
        url: '/PDRP/Observations/SetViewPreObservationSesson',
        success: function (data) {
            if (data.success == true) {
               

                window.open("/PDRP/Observations/PreObservation/",'_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
function ViewPostObservation(id) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        url: '/PDRP/Observations/SetViewPostObservationSesson',
        success: function (data) {
            if (data.success == true) {


                window.open("/PDRP/Observations/PostObservation/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
$(document).ready(function () {
    
   
    $("#tprebtnSave").on('click', function () {
        //$("#isAddMode").val("2");
        $("#PreObservationsForm").submit();
    });
    $("#tprebtnSubmit").on('click', function () {
        $("#isAddMode").val("3");
        $("#PreObservationsForm").submit();
    });
    $("#prebtnSubmit").on('click', function () {
        $("#isAddMode").val("3");
        //$("#PreObservationsForm").submit();
    });
    $("#prebtnSave").on('click', function () {
        $("#isAddMode").val("1");
        //$("#PreObservationsForm").submit();
    });
    $("#postbtnSubmit").on('click', function () {
        $("#isAddMode").val("3");
        //$("#PreObservationsForm").submit();
    });
    $("#postbtnSave").on('click', function () {
        $("#isAddMode").val("1");
        //$("#PreObservationsForm").submit();
    });
    $("#tpostbtnSubmit").on('click', function () {
        $("#isAddMode").val("3");
        $("#PostObservationsForm").submit();
    });
    $("#tpostbtnSave").on('click', function () {
        $("#isAddMode").val("1");
        $("#PostObservationsForm").submit();
    });
   
    //$('.timePicker').timepicker({
    //    showMeridian: true,
    //    showInputs: true,
    //    minuteStep: 5
    //});
    $('#txtVisitDate').datepicker({
        changeMonth: false,
        changeYear: false,
        dateFormat: "dd/mm/yy",
        todayHighlight: true,
        autoclose: true
    });
    //$('#txtVisitDate').datepicker({
    //    changeMonth: false,
    //    changeYear: false,
    //    dateFormat: "dd/mm/yy"
    //});
    //$("datepicker").datepicker({ changeMonth: false,
    //    changeYear: false,
    //    dateFormat: "dd/mm/yy"
       
    //});
    
    //$(".datepicker").datepicker({ dateFormat: "dd/mm/yy" });




    $(".wid180").on('change', function () {
       
        var scorevalues = [];
        $("#tblObservationsProficiencyLevel #ProficiencyScore").each(function () {
           
            var get_textbox_value = $(this).val();
            if (get_textbox_value > 0) {
                scorevalues.push(get_textbox_value);
            }
        });
        CalculateScore(scorevalues);
      
    });

    //***Initiate=========================================
    $("#OBEvlbtnInitiate").on('click', function () {
        $("#isAddMode").val("101");
    });
    $("#tOBEvlbtnInitiate").on('click', function () {
        $("#isAddMode").val("101");
        $("#ObservationForm").submit();
    });

    //***Submit=========================================
    $("#OBEvlbtnSubmit").on('click', function () {
        $("#isAddMode").val("102");
    });
    $("#tOBEvlbtnSubmit").on('click', function () {
        $("#isAddMode").val("102");
        $("#ObservationForm").submit();
    });

    //***signoff=========================================
    $("#OBEvlbtnsignoff").on('click', function () {
        $("#isAddMode").val("103");
    });
    $("#tOBEvlbtnsignoff").on('click', function () {
        $("#isAddMode").val("103");
        $("#ObservationForm").submit();
    });

    //***Save=========================================
    $("#tOBEvlbtnSave").on('click', function () {      
        $("#isAddMode").val("105");
        $("#ObservationForm").submit();
    });
    $("#OBEvlbtnSave").on('click', function () {
        $("#isAddMode").val("105");       
    });
    //=========================================





    //if ($("#FormMode").val() == 0) {
    //    $("#FormMode").val("0");
    //}
    //else if ($("#FormMode").val() == 1) {
    //    $("#FormMode").val("10");
    //}
    //else if ($("#FormMode").val() == 2) {
    //    $("#FormMode").val("20");
    //}
    //else {
    //    //$("#FormMode").val("0");
    //}
    var ReqStatusID = $("#ReqStatusID").val();

    if (ReqStatusID == 4) {
        $(".btnSave").hide();
        $(".btn-submit").hide();

    }

    var FormMode = $("#FormMode").val();
   
    if (FormMode == 2) {
        $(".btnSave").hide();
        $(".btn-submit").hide();

    }
    
    
   
});
function CalculateScore(scorevalues) {
    var ddlDropInsYearID = $("#ObservationsEmployeeModel_ObservationsYearID").val();
    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, FormulaYearID: ddlDropInsYearID },
        traditional: true,
        url: '/PDRP/Observations/CalculateDropInScore',
        success: function (data) {
            $("#ProficiencyScoreFormula").val(data.Formula);
            $("#ProficiencyScoreTotal").val(data.Total);
            $("#divProficiencyScoreTotal").text(data.Total);

            $("#ProficiencyScoreOverall").val(data.OverallScore);
            $("#divProficiencyScoreOverall").text(data.OverallScore);

        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}
$("#ddlPDRPYears").on('change', function () {

    getEmployeeGrid();
   
});
function SetObservationVisitDate(e) {

    //AllEditMode = 0;
    var table = $("#tbl_employee").DataTable();
    var vYearId = $("#ddlPDRPYears").val();
    var ddlYearTxt = $("#ddlPDRPYears option:selected").text();

    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {       
        var updatedData = [];
        $(UpdatedTr).each(function () {           

            var vEmployeeId = $(this).find('#EmployeeID').val();
            //alert(vEmployeeId);
            var vVisitDate = $(this).find('.datepicker').val() + ' ' + $(this).find('.timepicker').val();            
            var vPreObservationDueDate = $(this).find('#txtPreObsProc').text();
            //var autoID = $(itm).find('td:eq(8)').text();
            //var EmpId = ($(itm).find("#ddlApprover_" + autoID)).val();
            
            updatedData.push({
               
                EmployeeID: vEmployeeId,
                VisitDate: vVisitDate,
                PreObservationDueDate: vPreObservationDueDate,
            });
            
        });
        
        //console.log(updatedData);
        //$.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save "+updatedData.length+" records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/Observations/SetObservationVisitDateMulty",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'SelectedTeachersList': updatedData, 'YearId': vYearId }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        getEmployeeGrid();
                    }
                    else {
                        ShowMessage("error", data.Message);
                       
                    }
                    hideLoaderFrame();
                }
            })
        //});
    }
    else {
        ShowMessage("error", "Please select at least one record.");
        hideLoaderFrame();
    }
}
function getEmployeeGrid() {

    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }

    var employeeStatus = $("#employeeStatusddl").val();
    var ddlDropInsYearID = $("#ddlPDRPYears").val();
    var ddlDropInsYearTxt = $("#ddlPDRPYears option:selected").text();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (ddlDropInsYearID == "") {
        ddlDropInsYearID = 0
    }
    //var IncludeInactive = $("#includeInActiveChk").is(':checked');
    //var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        "sAjaxSource": "/PDRP/Observations/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt,
        "aoColumns": [
            { "mData": "Default", "bVisible": false },

            { "mData": "Select", "bVisible": true, "sTitle": "<input type=checkbox id=chkSelevctAll onclick=SelectAll()>", "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "EmployeeAlternativeID", "bVisible": true, "sTitle": "ID", "width": "6%", "sType": "Int", "className": "center-text-align" },
            { "mData": "FirstName", "sTitle": "First Name", "width": "15%" },
            { "mData": "LastName", "sTitle": "Last Name", "width": "15%" },
            { "mData": "KPICount", "className": "hidden" },
            { "mData": "VisitDate", "sTitle": "Visit Date", "bSortable": true, "width": "15%" },
            { "mData": "VisitTime", "sTitle": "Visit Time", "width": "15%" },
            { "mData": "PreObservationDueDate", "sTitle": "Pre-Observation Due Date", "bSortable": true, "width": "15%" },             
            { "mData": "EmployeeId",  "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "Action", "sTitle": "Action", "bSortable": true },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/Observations/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            $(".tbdate").datepicker("destroy");
            $(".tbdate").datepicker({
                changeMonth: true,
                changeYear: false,               
                autoclose: true
            }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });
            
            $('.timepicker').timepicker({
                showMeridian: true,
                showInputs: true,
                minuteStep: 5,
            });
            KPICheck();
        },
        "language": { "loadingRecords": "<span class='loader'></span>" }
    });

}
function KPICheck() {
    
    var vKPICount = $(".KPICount").text();
    //alert(vKPICount);
    if (vKPICount != "") {
        if (vKPICount == 0) {
            //ShowMessage("error", "KPI is not Set for the current year.</br> You cannot process Appraisal for this year");

            $("#lblKPI").text("KPIs not set for selected year.");


        }
        else {

            $("#lblKPI").text("");
        }
    }
}
function ChkChange(source) {
    
    var closesetRow = $(source).closest('tr');
    var vdatepicker = $(closesetRow).find('.datepicker').val();
    //var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if ($(source).prop("checked") == true) {
        if (vdatepicker.length > 0) {

            var employeeId = $(closesetRow).addClass('IsEdit');
        }
    }
    else if ($(source).prop("checked") == false) {
        var employeeId = $(closesetRow).removeClass('IsEdit');
    }



    

    


}
function DateAdd(source)
{
    var closesetRow = $(source).closest('tr');
    var vdatepicker= $(closesetRow).find('.datepicker').val();
    var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if (vdatepicker.length > 0) {
        vchkEmployeeId.prop('checked', true);
        var employeeId = $(closesetRow).addClass('IsEdit');
    }

    
}

function SaveSingleEmployeeHeirarchy(source) {
    var closesetRow = $(source).closest('tr');
    var employeeId = $(closesetRow).data().employeeid;
    var updatedData = [];
    $(closesetRow).find(".manger").each(function (index, obj) {
        var groupId = $(obj).data().groupid;
        var oldMangerId = $(obj).data().managerid;
        var managerId = $("#ddlManger_" + groupId + "_" + employeeId + "").val();
        if ((oldMangerId != "" && oldMangerId != "0") || (managerId != "" && managerId != '0')) {
            updatedData.push({
                EmployeeID: employeeId,
                ManagerID: managerId,
                GroupID: groupId
            });
        }
    });
    if (updatedData.length > 0) {
        pageLoaderFrame();
        $.ajax({
            url: "/EmployeeBasedHierarchy/SaveMultipleEmployees",
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            data: JSON.stringify({ 'employeeHeirarchyList': updatedData }),
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);

                    var closesetRow = $(source).closest('tr');
                    var employeeId = $(closesetRow).data().employeeid;
                    $(closesetRow).find(".manger").each(function (index, obj) {
                        var groupId = $(obj).data().groupid;
                        $(obj).data().managerid = $("#ddlManger_" + groupId + "_" + employeeId).val();
                        var managerId = $(obj).data().managerid;
                        var managerName = managerId != "" ? $("#ddlManger_" + groupId + "_" + employeeId + " option[value='" + managerId + "']").text() : "";
                        $(obj).html(managerName);
                    });
                    $(closesetRow).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHeirarchy(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
                    //+
                    //   "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
                    $(closesetRow).removeClass("IsEdit");
                    hideLoaderFrame();
                }
                else {
                    ShowMessage("error", data.Message);
                    hideLoaderFrame();
                }
            }
        });
    }
    else {
        ShowMessage("error", "Please edit atleast one record.");
        hideLoaderFrame();
    }
}
function ApplytoallSelections()
{
    
    var VisitDate = $('#txtVisitDate').val().trim();
    var VisitTime = $('#txtVisitTime').val().trim();
    var Ch = 0;
    if (VisitDate.length > 0 && VisitTime.length > 0) {
        var table = $("#tbl_employee").DataTable();
        var UpdatedTr1 = table.$(".odd");
        var UpdatedTr2 = table.$(".even");
        $(UpdatedTr1).each(function () {
            var row = $(this);
            console.log(row);
            if (row.find('input[type="checkbox"]').prop('disabled'))
            {
                //** No Action need
            }
                //*** Removed On Request
            //else if (row.find('input[type="checkbox"]').is(':checked') &&
            //   row.find('.datepicker').val().length <= 0) {
            else if (row.find('input[type="checkbox"]').is(':checked')) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);
                
                $(this).find('.timepicker').val(VisitTime);
                Ch = 1;
            }

        });
        $(UpdatedTr2).each(function () {
            var row = $(this);
            console.log(row);
            if (row.find('input[type="checkbox"]').is(':checked') &&
               row.find('.datepicker').val().length <= 0) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);
                
                $(this).find('.timepicker').val(VisitTime);
                Ch = 1;
            }
        });
        
        if ( Ch == 0)
        {
            ShowMessage("error", "Please select at least one record.");
        }
    }
    else
    {
        ShowMessage("error", "Please select Visit Date / Visit Time.");
    }
}


function PreObservation(id, observationyear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, ObservationYear: observationyear, isAddMode: 1 },
        url: '/PDRP/Observations/SetObservationActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/Observations/PreObservation/";
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}

function PostObservation(id, observationyear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, ObservationYear: observationyear, isAddMode: 1 },
        url: '/PDRP/Observations/SetObservationActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/Observations/PostObservation/";
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
function ObservationEvaluation(id, observationyear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, ObservationYear: observationyear, isAddMode: 1 },
        url: '/PDRP/Observations/SetObservationActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/Observations/ObservationEvaluation/";
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
