﻿var hideClass = "";
var amountSetting = "";
var hidetaxpercentage = "False";
$(document).ready(function () {  
    $("#txtEffectiveDate").datepicker('setDate', new Date());
    var tax = $("#TaxAmount").val();
    hidetaxpercentage = $("#HideTaxPercentage").val();
    $("#btnSavePasiSetting").click(function () {
        savePasiSetting();
    });
    var checkAllEmployee = $("#TaxForAll").val();
    var checkForEmployee = $("#TaxForEmployee").val();
    if (checkAllEmployee == "False" && checkForEmployee == "True") {
        $("#radioAllEmployee").prop("checked", false);
        $("#radioEachEmployee").prop("checked", true);
        $("#TaxPercentage").prop("disabled", true);
        getGridPASITax(tax);
    }
    else if (checkForEmployee == "False" && checkAllEmployee == "True") {
        $("#radioAllEmployee").prop("checked", true);
        $("#radioEachEmployee").prop("checked", false);
        $("#TaxPercentage").prop("disabled", false);
        getGridPASITax(tax);
    } else {
        $("#radioJordanTax").prop("checked", true);
        $("#radioAllEmployee").prop("checked", false);
        $("#radioEachEmployee").prop("checked", false);
        $("#TaxPercentage").prop("disabled", false);
        getGridPASITax("JDTaxStatus");
    }
    bindPositiveOnly(".inputPercentage");
    bindPositiveOnly(".txtAmount");
    bindMaxAndMinSelector(".inputPercentage");
    getAmountFormateSetting(function (data) {
        amountSetting = data;
    });
    var tg = 4;
    $(document).on("keypress", ".taxAmount,.inputPercentage", function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});
$("#BtnPreview").click(function () {
    var tax = $("#TaxAmount").val();
    getGridPASITax(tax);
    $("#divPASIList").show();
});

$("#btn_Generate").click(function () {
    var Cycleid = $("#ddl_CyclesList").val();
    if (parseInt(Cycleid) > 0) {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/PASITax/GetCycleDataById?cycleId=' + $('#ddl_CyclesList').val() + '&date=' + $('#txtEffectiveDate').val(),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data) {
                    PostRecords();
                } else {
                    ShowMessage("error", "Please select valid deduction date for selected cycle");
                }
            },
            error: function (data) { }
        });
    } else { ShowMessage("error", 'Please select Pay Cycle.'); }
});

$("input[type=radio][name=taxforall]").change(function () {
    if ($("#radioEachEmployee").is(':checked')) {
        $("#TaxPercentage").prop("disabled", true);
    } else if ($("#radioJordanTax").is(':checked')) {
        $("#TaxPercentage").prop("disabled", true);
    } else {
        $("#TaxPercentage").prop("disabled", false);
    }
})

var oTableChannel;

function BindBlankData() {
    oTableChannel = $('#tbl_PASITaxList').dataTable({
        "aoColumns": [
             {
                 "mData": "CheckRow", "sTitle": "<input type='checkbox' id = 'check_all' name = 'check_all' onclick='CheckAll()'/>", "sClass": "center",
                 "bSortable": false
             },
              { "mData": "EmployeeAlternativeID", "sTitle": "Employee Id", "sClass": "EmployeeAlternativeID", "width": "5%", "sType": "Int" },
            { "mData": "EmployeeID", "sTitle": "EmployeeID", "bVisible": false, "bSortable": false, "sClass": "EmployeeID" },
            { "mData": "FullName", "sTitle": "Employee Name", "sClass": "FullName", "width": "40%" },
            { "mData": "Amount", "sTitle": "Salary Amount", "width": "20%" },
            { "mData": "Percentage", "sTitle": "Tax (%)", "sClass": hideClass, "width": "10%" },
            { "mData": "PASI_TAX", "sTitle": "Tax Amount", "width": "20%" },
            //{ "mData": "EmployeeAlternativeID", "sTitle": "EmployeeAlternativeID", "bVisible": false, "bSortable": false, "sClass": "EmployeeAlternativeID" },
            //{ "mData": "EmployeeID", "sTitle": "Employee ID", "sClass": "EmployeeID" },
            //{ "mData": "FullName", "sTitle": "Employee Name", "sClass": "FullName" },
            //{ "mData": "Amount", "sTitle": "Salary Amount", "sClass": "right-text-align" },
            //{ "mData": "PASI_TAX", "sTitle": "PASI TAX", "sClass": "right-text-align" },
        ],


        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_PASITaxList");
        }
        //"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        //    // Bold the grade for all 'A' grade browsers
        //    //console.log(aData["PayAbsentTypeID"]);
        //    var LeaveTypeID = aData["LeaveTypeID"];
        //    //console.log(nRow);
        //    var select = $(nRow).find("select");
        //    //console.log(AbsentTypeId);
        //    //console.log($(select).val());
        //    //$(select).val(AbsentTypeId);
        //    $(select).val(LeaveTypeID).attr("selected", "selected");
        //    //var AbsentID = aData[2];
        //    //console.log(AbsentID);

        //    //if (aData[4] == "A") {
        //    //    $('td:eq(4)', nRow).html('<b>A</b>');
        //    //}
        //}
    });
}

function getGridPASITax(PASIPercentage) {

    if ($("#radioAllEmployee").is(':checked')) {
        hideClass = "hidden";
    } else if ($("#radioJordanTax").is(':checked')) {
        hideClass = "hidden";
    } else {
        hideClass = ""
    }
    //if (hidetaxpercentage == "True" || hidetaxpercentage == 'True') {
    //    hideClass = "hidden";
    //}
    if (oTableChannel != undefined) {
        $('#tbl_PASITaxList').DataTable().destroy();
    }

    oTableChannel = $('#tbl_PASITaxList').dataTable({
        "sAjaxSource": "/PASITax/GetPASITaxList",
        "aoColumns": [
                         {
                             "mData": "CheckRow", "sTitle": "<input type='checkbox' id = 'check_all' name = 'check_all' onclick='CheckAll()'/>", "sClass": "center",
                             "bSortable": false, "width": "5%"
                         },
                         { "mData": "EmployeeAlternativeID", "sTitle": "Employee Id", "sClass": "EmployeeAlternativeID", "width": "5%", "sType": "Int" },
                         { "mData": "EmployeeID", "sTitle": "EmployeeID", "bVisible": false, "bSortable": false, "sClass": "EmployeeID" },
                         { "mData": "FullName", "sTitle": "Employee Name", "sClass": "FullName", "width": "40%" },
                         { "mData": "Amount", "sTitle": "Salary Amount", "width": "20%" },
                         { "mData": "Percentage", "sTitle": "Tax (%)", "sClass": hideClass, "width": "10%" },
                         { "mData": "PASI_TAX", "sTitle": "Tax Amount", "width": "20%" },
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "PASIPercentage", "value": PASIPercentage });
        },
        "processing": false,
        "serverSide": false,
        "ajax": "/PASITax/GetPASITaxList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bSortCellsTop": true,
        "order": [[3, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_PASITaxList");
            //if (hideClass == "hidden") {
            //    $("#tbl_PASITaxList").find('th:eq(4)').addClass("hidden");
            //} else {
            //    $("#tbl_PASITaxList").find('th:eq(4)').removeClass("hidden");
            //}
            if ($("#radioEachEmployee").is(':checked')) {              
                if (hidetaxpercentage == "True" || hidetaxpercentage == 'True') {
                    $('td:nth-child(5),th:nth-child(5)').hide();
                   DisplayAmountInEditMode();
                }
            }
        }


    });


}
function DisplayAmountInEditMode() {
    $tr = $('#tbl_PASITaxList tbody tr');
    $($tr).each(function (i, item) {
        var amt = $(item).find("td").eq(5).text();
        if (amt != null && amt != '' && amt != "") {
            $(item).find("td").eq(5).html("<input  onchange='saveTaxSetting(this)'  type='text' value='" + amt + "' class='form-control taxAmount' />");

        }
    });
}
function ChkRowCheckClick(obj) {
    if ($(obj).is(':checked')) {
        $(obj).closest('tr').addClass("selected");
        $(obj).attr('data-SelectState', 'true');
    }
    else {
        $('#check_all').prop('checked', false);
        $(obj).closest('tr').removeClass("selected");
        $(obj).attr('data-SelectState', 'false');
    }

}


function PostRecords() {
    var chkTaxForAll = $("#radioAllEmployee").is(":checked");
    var chkTaxForEach = $("#radioEachEmployee").is(":checked");
    var Effectivedate = $("#txtEffectiveDate").val();
    var Cycleid = $("#ddl_CyclesList").val();
    var Cyclename = $("#ddl_CyclesList option:selected").text();
    var id = "";

    var table = $('#tbl_PASITaxList').DataTable();
    rows = $('#tbl_PASITaxList tr.selected');
    if (Cycleid > 0) {
        if (rows.length == 0) {
            ShowMessage("error", 'Please select at least one Employee from the list.');
        }
        else {
            rowData = table.rows(rows).data();
            var PASITaxList = [];
            var allRow = table.rows().data();
            if ($('#check_all').is(':checked')) {
                $(allRow).each(function (i, item) {
                    if (!findItemInArray(PASITaxList, "EmployeeID", item.EmployeeID)) {
                        var curRow = {};
                        curRow.EmployeeAlternativeID = item.EmployeeAlternativeID;
                        curRow.EmployeeID = item.EmployeeID;
                        curRow.FullName = item.FullName;
                        curRow.Amount = item.Amount;
                        curRow.PASI_TAX = item.PASI_TAX;
                        curRow.Effectivedate = Effectivedate;
                        curRow.Cycleid = Cycleid;
                        curRow.Cyclename = Cyclename;
                        PASITaxList.push(curRow);
                    }
                });
            } else {
                var selectedCheckBox = table.$(".ChkRowCheck:checked", { "page": "all" });
                $(allRow).each(function (i, item) {
                    var checkboxId = $(allRow[i].CheckRow).attr('id');
                    $.each(selectedCheckBox, function (ii, itemTemp) {
                        if ($(itemTemp).attr('id') == checkboxId) {
                            if (!findItemInArray(PASITaxList, "EmployeeID", item.EmployeeID)) {
                                var curRow = {};
                                curRow.EmployeeAlternativeID = item.EmployeeAlternativeID;
                                curRow.EmployeeID = item.EmployeeID;
                                curRow.FullName = item.FullName;
                                curRow.Amount = item.Amount;
                                curRow.PASI_TAX = item.PASI_TAX;
                                curRow.Effectivedate = Effectivedate;
                                curRow.Cycleid = Cycleid;
                                curRow.Cyclename = Cyclename;
                                PASITaxList.push(curRow);
                            }
                        }
                    })
                });
            }

            if (PASITaxList.length != 0) {
                PASITaxList = JSON.stringify({ 'PASITaxList': PASITaxList });

                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    //    data: { PasitaxLists: PASITaxList,effectivedate:Effectivedate,cycleid:Cycleid,cyclename:Cyclename },
                    data: PASITaxList,
                    url: '/PASITax/PushRecords',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.success) {
                            ShowMessage("success", data.msg);// 'Records have been posted succesfully');
                            var tax = $("#TaxAmount").val();
                            if (chkTaxForAll == false && chkTaxForEach == false) {
                                getGridPASITax("JDTaxStatus");
                            }
                            else {
                                getGridPASITax(tax);
                            }
                        }
                        else {
                            if (data.msg == 'Record is exist')
                                notyConfirm("Selected employees records already posted for selected cycle.\nDo you want to regenerate the tax for this cycle?", PASITaxList);
                            else
                                ShowMessage("warning", data.msg);
                        }
                    },
                    error: function (data) { }
                });
            }
        }
    }
    else { ShowMessage("error", 'Please select Pay Cycle.'); }
}

function CheckAll() {
    if ($("#check_all").is(':checked')) {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".ChkRowCheck").closest('tr').addClass("selected");
    }
    else {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".ChkRowCheck").closest('tr').removeClass("selected");
    }
}

function AddAbsent() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/AddAbsent");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Absent');
}

function EditAbsent(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/EditAbsent/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Edit Absent');

}


function DeleteAbsent(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Absent/DeleteAbsent',
            success: function (data) {
                ShowMessage("success", "Absent deleted successfully.")
                var id = $("#hdnEmployee").val();
                getGridAbsent(id);
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewAbsent(id) {
    $("#modal_Loader").load("/Absent/ViewAbsent/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Absent Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function notyConfirm(massage, PASITaxList) {
    var chkTaxForAll = $("#radioAllEmployee").is(":checked");
    var chkTaxForEach = $("#radioEachEmployee").is(":checked");
    $.MessageBox({
        buttonDone: "Yes",
        buttonFail: "No",
        message: massage
    }).done(function () {
        pageLoaderFrame();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: PASITaxList,
            url: '/PASITax/UpdatePasiTax',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);// 'Records have been posted succesfully');
                    var tax = $("#TaxAmount").val();
                    if (chkTaxForAll == false && chkTaxForEach == false) {
                        getGridPASITax("JDTaxStatus");
                    }
                    else {
                        getGridPASITax(tax);
                    }
                }
                else {
                    ShowMessage("error", data.Message);
                }
                hideLoaderFrame();
            },
            error: function (data) { }
        });
    }).fail(function () {

    });

}

function savePasiSetting() {
    var chkTaxForAll = $("#radioAllEmployee").is(":checked");
    var chkTaxForEach = $("#radioEachEmployee").is(":checked");
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { taxForEmployee: $("#radioEachEmployee").is(":checked"), taxForAll: $("#radioAllEmployee").is(":checked"), taxPercentage: $("#TaxPercentage").val() },
        url: '/PASITax/SaveTax',
        success: function (data) {
            if (data.Success == true) {
                ShowMessage("success", data.Message);
                if (chkTaxForAll == false && chkTaxForEach == false) {
                    getGridPASITax("JDTaxStatus");
                }
                else {
                    getGridPASITax(0);
                }
            }
        },
        error: function (data) { }
    });
}

function saveTaxSetting(e) {
    // var taxPercentage = parseFloat($(e).val()).toFixed(2);
    var $row = $(e).closest('tr');
    //var taxamt = $row.find('input.taxAmount:text').val();  
    var employeeId = parseFloat($($row.find('td').eq('0').html()).attr('data-Id'));
    var employeeAlternativeId = $row.find('td').eq('1').html();
    var employeeName = $row.find('td').eq('2').html();
    var amount = $row.find('td').eq('3').html();   
    var taxAmount, taxPercentage;     
     if (hidetaxpercentage == "True" || hidetaxpercentage == 'True') {
         taxPercentage = parseFloat('0.00');
         taxAmount = parseFloat($row.find('input.taxAmount:text').val());
     }
     else {
         taxPercentage = parseFloat($(e).val()).toFixed(2);
         taxAmount = parseFloat($row.find('td').eq('5').text());

     }
    taxAmount = taxAmount.toFixed(amountSetting);
    taxPercentage = parseFloat(taxPercentage).toFixed(amountSetting);
    var rowIndex = $(e).closest('tr').index();
    var rowArray = {
        "CheckRow": "<input data-Id='" + employeeId + "' type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id='Chkcheck" + employeeId + "' name='check" + employeeId + "' />",
        "EmployeeAlternativeID": employeeAlternativeId,
        "EmployeeID": employeeId,
        "FullName": employeeName,
        "Amount": amount,
        "PASI_TAX": taxAmount,
        "Percentage": "<input onkeyup='calculateTax(this)' onchange='saveTaxSetting(this)' data-Max='100' data-Min='0' type='text' value='" + taxPercentage + "' class='form-control inputPercentage' />"
        //"<input onkeyup='calculateTaxPercentage(this)' onchange='saveTaxSetting(e)' type='text' value='" + taxAmount.toFixed(amountSetting) + "' class='form-control taxAmount' />"
        //taxAmount
    }
    oTableChannel.fnUpdate(rowArray, rowIndex);
    $(e).closest('tr').addClass('selected');
    var table = $('#tbl_PASITaxList').DataTable();
    table.row('.selected').remove().draw(false);
    if (taxAmount >= 0) {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { employeeId: employeeId, taxPercentage: taxPercentage, taxAmount: taxAmount },
            url: '/PASITax/SaveTaxPerEmployee',
            success: function (data) {
                if (data.Success == true) {
                    ShowMessage("success", data.Message);
                }
            },
            error: function (data) { }
        });

    }
}

function calculateTax(e) {
    var maxValue = parseFloat($(e).attr('data-Max'));
    var minValue = parseFloat($(e).attr('data-Min'));
    var value = parseFloat($(e).val());
    if (maxValue < value || minValue > value) {
        var valueStr = $(e).val()
        $(e).val(valueStr.substring(0, valueStr.length - 1));
    }
    if (parseFloat($(e).val()) >= 0) {
        var $row = $(e).closest('tr');
        var amount = $($row).find('td').eq('3').text();
        value = parseFloat($(e).val());
        amount = ((parseFloat(value) * parseFloat(amount)) / 100);
        $($row).find('td').eq('5').text(amount.toFixed(amountSetting));
        //$($row).find('td').eq('5').html("<input onkeyup='calculateTaxPercentage(this)' onchange='saveTaxSetting(this)' type='text' value='" + amount.toFixed(amountSetting) + "' class='form-control taxAmount' />");

    }
}
//function calculateTaxPercentage(e) {
//    var $row = $(e).closest('tr');
//    if (parseFloat($(e).val()) >= 0) {
//        var $row = $(e).closest('tr');
//        var amount = $($row).find('td').eq('3').text();
//        value = parseFloat($(e).val());
//        if (value > amount) {
//            value = amount;
//            $row.find('input.taxAmount:text').val(amount);
//        }
//        var taxper = parseFloat(value) * 100 / parseFloat(amount);
//        $($row).find('td').eq('4').html("<input onkeyup='calculateTax(this)' onchange='saveTaxSetting(e)' data-Max='100' data-Min='0' type='text' value='" + taxper.toFixed(amountSetting) + "' class='form-control inputPercentage' />");

//    }
//    else {
//        $row.find('input.taxAmount:text').val(0);
//        $row.find('input.inputPercentage:text').val(0);
//    }
//}
