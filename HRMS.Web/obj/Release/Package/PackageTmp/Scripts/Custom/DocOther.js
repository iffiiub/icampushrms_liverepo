﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridDocOther();
    }
});

var oTableChannel;

function getGridDocOther() {

    oTableChannel = $('#tbl_DocOtherList').dataTable({
        "sAjaxSource": "/DocOther/GetDocOtherList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document<br/>Name/No", 'width': '18%' },
            { "mData": "IssueCountry", "sTitle": "Issue<br/>Country", 'width': '18%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '13%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '13%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '13%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", 'width': '15%' }

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/DocOther/GetDocOtherList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_DocOtherList");
        }
    });
}

function AddDocOther() {

    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/DocOther/AddDocOther");
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add DocOther');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditDocOther(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/DocOther/EditDocOther/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit Other Document');
}

function DeleteDocOther(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/DocOther/DeleteDocOther',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridDocOther();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewDocOther(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/DocOther/ViewDocOther/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View Other Document Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/DocOther/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/DocOther/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}