﻿$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_PositionList').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        //"dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/Position/GetPositionList",
        "aoColumns": [

            { "mData": "PositionID", "bVisible": false, "sTitle": "PositionID" },
            { "mData": "PositionTitle", "sTitle": "Position Title (En)", 'width': '17%' },
            { "mData": "PositionTitle_2", "sTitle": "Position Title (Ar)", 'width': '17%' },           
            { "mData": "EmploymentTypeID", "sTitle": "Employment Type", 'width': '14%' },
            //{ "mData": "Reg_TempID", "sTitle": "Regular/Temporary", 'width': '15%' },            
            { "mData": "DepartmentName", "sTitle": "Department Name", 'width': '14%' },
            { "mData": "isAcademic", "sTitle": "SIS Access", 'width': '12%', "sClass": "text-center", "bSortable": false },
            { "mData": "IsActive", "sTitle": "Active", 'width': '12%', "sClass": "text-center", "bSortable": false },
            { "mData": "Action", "sTitle": "Actions", "bSortable": false, 'width': '14%' }

        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/Position/GetPositionList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "bAutoWidth": false,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {
            
        }
    });
}

$("#btn_add").click(function () {

    location.href = "/Position/AddPosition/";
    //EditPosition(0);
});

function AddPosition() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Position/AddPosition");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Position');
}

function EditPosition(id) {

    location.href = "/Position/EditPosition/" + id;
    //$("#modal_Loader").load("/Position/EditPosition/" + id);
    //$("#myModal").modal("show");
    //$("#modal_heading").text('Edit Position');
}

function DeletePosition(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Position/DeletePosition',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewPosition(id) {
    //location.href = "/Position/ViewPosition/" + id;
    $("#modal_Loader").load("/Position/ViewPosition/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Position Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}