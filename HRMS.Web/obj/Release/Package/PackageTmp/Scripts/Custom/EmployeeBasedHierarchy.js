﻿var AllEditMode = 0;
var AllRefreshMode = 0;

$(document).ready(function () {
    initEmployeeHierchyGrid();
    //  loadEmployeeHierchyData();
});

function initEmployeeHierchyGrid() {
    var CountWorkFlowGroup = $("#hdnCountWorkFlowGroup").val();
    var actionColumn = 3 + parseInt(CountWorkFlowGroup);
    $('#tbl_employeeHierchy').dataTable(
        {
            columnDefs: [
                { width: "5%", targets: [0] },
                { width: "15%", targets: [1] },
                { width: "10%", targets: [2, actionColumn] }
                //{ width: "15%", targets: [3, 4, 5, 6] }

            ],
            //columnDefs: [
            //     { width: "100", targets: [0] },
            //    { width: "150", targets: [1] },
            //    { width: "100", targets: [2] },
            //    { width: "100", targets: [3, 4, 5, 6, 7] }
            //],
            fixedColumns: true,
            "order": [[1, "asc"]],
            bAutoWidth: false,
            "fnDrawCallback": function () {
                if (AllEditMode == 1) {
                    pageLoaderFrame();
                    setTimeout(DispalyInEditMode, 10);
                }
                if (AllRefreshMode == 1) {
                    pageLoaderFrame();
                    setTimeout(RefreshInEditMode, 10);
                }

            },
            "bStateSave": true,
            "fnInitComplete": function (oSettings) {
                hideLoaderFrame();
            }
        }
    );
}

function loadEmployeeHierchyData() {

    var employeeId = $('#ddlEmployee').val();
    var departmentId = $('#ddlDepartment').val();

    $.ajax({
        url: '/EmployeeBasedHierarchy/LoadEmployeeHierchyGrid',
        data: { employeeId: employeeId, departmentId: departmentId },
        type: 'GET',
        success: function (data) {
            $("#employeeHierachy").html(data);
            initEmployeeHierchyGrid();


        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}

function EditAll() {
    AllRefreshMode = 0;
    AllEditMode = 1;
    pageLoaderFrame();
    setTimeout(DispalyInEditMode, 10);
}

function RefreshAll() {
    AllRefreshMode = 1;
    AllEditMode = 0;
    pageLoaderFrame();
    setTimeout(RefreshInEditMode, 10);
}
function Validate(e,managerId,groupId) {    
    var closesetRow = $(e).closest('tr');
    var employeeId = $(closesetRow).data().employeeid;
    var select = $(closesetRow).find("#ddlManger_" + groupId + "_" + employeeId);
    var id = $(select).val();   
    if ((id == null || id == undefined || id == "" || id == '' || id == '0') && managerId != 0) {
        pageLoaderFrame();
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/EmployeeBasedHierarchy/IsPendingApprovalExists',
            data: { employeeID: employeeId, groupID: groupId, managerID: managerId },
            success: function (result) {
                hideLoaderFrame();
                if (result.InsertedRowId > 0)
                {
                    ShowMessage('error', result.Message);
                    $("#ddlManger_" + groupId + "_" + employeeId + "").val(managerId);
                }
                   
                else {

                    $("#ddlManger_" + groupId + "_" + employeeId + "").val(id);  
                }
                $("#ddlManger_" + groupId + "_" + employeeId + "").selectpicker("refresh");
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
      //  $("#ddlReportingEmploye").selectpicker("refresh");
      
    }
    

}
function DispalyInEditMode() {   
    $tr = $('#tbl_employeeHierchy tbody tr');
    $($tr).each(function (i, item) {
        var employeeId = $(item).data().employeeid;
        $(item).find(".manger").each(function (index, obj) {
            var managerId = $(obj).data().managerid;
            var groupId = $(obj).data().groupid;
            if (managerId == null || managerId == undefined || managerId == "" || managerId == '')
                managerId = 0;
            if ($(obj).find(".ddlManger").length == 0) {
                $(obj).html("<select data-live-search='true' onchange='Validate(this," + managerId + "," + groupId + ")'  class='form-control ddlManger' id='ddlManger_" + groupId + "_" + employeeId + "' >" + $("#EmployeeList").text() + "  </select>");
                $("#ddlManger_" + groupId + "_" + employeeId + "").val(managerId);
                $("#ddlManger_" + groupId + "_" + employeeId + "").selectpicker();
            }
        });
        $(item).find(".action").html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="SaveSingleEmployeeHeirarchy(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
        $(item).addClass("IsEdit");
    });
    hideLoaderFrame();
}

function RefreshInEditMode() {
    var table = $("#tbl_employeeHierchy").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    $(UpdatedTr).each(function (i, item) {
        var employeeId = $(item).data().employeeid;
        $(item).find(".manger").each(function (index, obj) {
            var managerId = $(obj).data().managerid;
            var groupId = $(obj).data().groupid;
            var managerName = managerId != "" ? $("#ddlManger_" + groupId + "_" + employeeId + " option[value='" + managerId + "']").text() : "";
            $(obj).html(managerName);
        });
        $(item).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHeirarchy(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
        //+
        // "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
        $(item).removeClass("IsEdit");
    });
    hideLoaderFrame();
}

function EditHeirarchy(source) {
    $tr = $(source).closest("tr");
    var closesetRow = $(source).closest('tr');
    var employeeId = $(closesetRow).data().employeeid;
    $(closesetRow).find(".manger").each(function (index, obj) {
        var managerId = $(obj).data().managerid;
        var groupId = $(obj).data().groupid;
        if (managerId == null || managerId == undefined || managerId == "" || managerId == '')
            managerId = 0;
        $(obj).html("<select data-live-search='true' onchange='Validate(this," + managerId + "," + groupId + ")'  class='form-control ddlManger' id='ddlManger_" + groupId + "_" + employeeId + "' >" + $("#EmployeeList").text() + "  </select>");
        $("#ddlManger_" + groupId + "_" + employeeId + "").val(managerId);
        $("#ddlManger_" + groupId + "_" + employeeId + "").selectpicker();
    });
    $(closesetRow).find(".action").html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="SaveSingleEmployeeHeirarchy(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
    $($tr).addClass("IsEdit");
}

function CancelNewRow(source) {
    var closesetRow = $(source).closest('tr');
    var employeeId = $(closesetRow).data().employeeid;
    $(closesetRow).find(".manger").each(function (index, obj) {
        var managerId = $(obj).data().managerid;
        var groupId = $(obj).data().groupid;
        var managerName = managerId != "" ? $("#ddlManger_" + groupId + "_" + employeeId + " option[value='" + managerId + "']").text() : "";
        $(obj).html(managerName);
    });
    $(closesetRow).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHeirarchy(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
    //+
    //"<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
    $(closesetRow).removeClass("IsEdit");
}

function SaveSingleEmployeeHeirarchy(source) {
    var closesetRow = $(source).closest('tr');
    var employeeId = $(closesetRow).data().employeeid;
    var updatedData = [];
    $(closesetRow).find(".manger").each(function (index, obj) {
        var groupId = $(obj).data().groupid;
        var oldMangerId = $(obj).data().managerid;
        var managerId = $("#ddlManger_" + groupId + "_" + employeeId + "").val();
        if ((oldMangerId != "" && oldMangerId!="0") ||(managerId != "" && managerId != '0')) {
            updatedData.push({
                EmployeeID: employeeId,
                ManagerID: managerId,
                GroupID: groupId
            });
        }
    });
    if (updatedData.length > 0) {
        pageLoaderFrame();
        $.ajax({
            url: "/EmployeeBasedHierarchy/SaveMultipleEmployees",
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            data: JSON.stringify({ 'employeeHeirarchyList': updatedData }),
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);

                    var closesetRow = $(source).closest('tr');
                    var employeeId = $(closesetRow).data().employeeid;
                    $(closesetRow).find(".manger").each(function (index, obj) {
                        var groupId = $(obj).data().groupid;
                        $(obj).data().managerid = $("#ddlManger_" + groupId + "_" + employeeId).val();
                        var managerId = $(obj).data().managerid;
                        var managerName = managerId != "" ? $("#ddlManger_" + groupId + "_" + employeeId + " option[value='" + managerId + "']").text() : "";
                        $(obj).html(managerName);
                    });
                    $(closesetRow).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHeirarchy(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
                    //+
                     //   "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
                    $(closesetRow).removeClass("IsEdit");
                    hideLoaderFrame();
                }
                else {
                    ShowMessage("error", data.Message);
                    hideLoaderFrame();
                }
            }
        });
    }
    else {
        ShowMessage("error", "Please edit atleast one record.");
        hideLoaderFrame();
    }
}

function DeleteEmployeeHierachy(source, employeeId) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            url: '/EmployeeBasedHierarchy/DeleteEmployeeHierarchy',
            type: 'GET',
            data: { employeeId: employeeId },
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    loadEmployeeHierchyData();
                }
                else {
                    ShowMessage("error", data.Message);
                    loadEmployeeHierchyData();
                    hideLoaderFrame();
                }

            }
        });
    });
}

function SaveAllEmployeeHierarchy() {
    AllEditMode = 0;
    var table = $("#tbl_employeeHierchy").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var updatedData = [];
        $(UpdatedTr).each(function (i, item) {
            var employeeId = $(item).data().employeeid;
            $(item).find(".manger").each(function (index, obj) {
                var groupId = $(obj).data().groupid;
                var oldMangerId = $(obj).data().managerid;
                var managerId = table.$("#ddlManger_" + groupId + "_" + employeeId + "").val();              
                if ((oldMangerId != "" && oldMangerId != "0") ||(managerId != "" && managerId != '0')) {
                    updatedData.push({
                        EmployeeID: employeeId,
                        ManagerID: managerId,
                        GroupID: groupId
                    });
                }
            });
        });
        if (updatedData.length == 0) {
            ShowMessage("error", "Please edit atleast one record.");
            return;
        }
        console.log(updatedData);
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/EmployeeBasedHierarchy/SaveMultipleEmployees",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'employeeHeirarchyList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        loadEmployeeHierchyData();
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            });
        });
    }
    else {
        ShowMessage("error", "Please edit atleast one record.");
        hideLoaderFrame();
    }
}
