﻿var AllEditMode = 0;
var AllRefreshMode = 0;

$(document).ready(function () {
    var employeeId = $('#ddlEmployee').val();
    var departmentId = $('#ddlDepartment').val();
    //pageLoaderFrame();
    //var url = '/LeaveBalanceManager/LoadLeaveBalanceGrid';
    //$('#divLeaveBalance').load(url, { employeeId: employeeId, departmentId: departmentId },
    //function () {
    //    InitLoadLeaveBalance();
    //    hideLoaderFrame();
    //}

    //    );
    $(document).on("change", ".txtExtLapsingDays", function () {
        ValidateLapsingDays(this);
    });  
    $(document).on("keypress", ".decimalOnly", function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});

function InitLoadLeaveBalance() {
    // var CountWorkFlowGroup = $("#hdnCountVacationType").val();  
    // var CountWorkFlowGroup = 24;
    //var actionColumn = 3 + parseInt(CountWorkFlowGroup);
    $('#tbl_leavebalance').dataTable(
        {
            columnDefs: [
                { width: "5%", targets: [0] },
                { width: "10%", targets: [1] }
               
            ],           
            fixedColumns: true,
            "order": [[1, "asc"]],
            bAutoWidth: false,
            "fnDrawCallback": function () {
                if (AllEditMode == 1) {
                    pageLoaderFrame();
                    setTimeout(DispalyInEditMode, 10);
                    $('.showCalendar').datepicker()
                }
                if (AllRefreshMode == 1) {
                    pageLoaderFrame();
                    setTimeout(RefreshInEditMode, 10);
                }

            },
            "bStateSave": true,
            "fnInitComplete": function (oSettings) {
                hideLoaderFrame();
            }
        }
    );
}

function LoadLeaveBalance() {
    AllRefreshMode = 0; AllEditMode = 0;
    pageLoaderFrame();
    var employeeId = $('#ddlEmployee').val();
    var departmentId = $('#ddlDepartment').val();

    $.ajax({
        url: '/LeaveBalanceManager/LoadLeaveBalanceGrid',
        data: { employeeId: employeeId, departmentId: departmentId },
        type: 'GET',
        success: function (data) {
            $("#divLeaveBalance").html(data);
            InitLoadLeaveBalance();
            hideLoaderFrame();

        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);
            hideLoaderFrame();
        }
    });
}
function EditAll() {
    AllRefreshMode = 0;
    AllEditMode = 1;
    pageLoaderFrame();
    setTimeout(DispalyInEditMode, 10);
}

function RefreshAll() {
    AllRefreshMode = 1;
    AllEditMode = 0;
    pageLoaderFrame();
    setTimeout(RefreshInEditMode, 10);
}

function DispalyInEditMode() {   
    $tr = $('#tbl_leavebalance tbody tr');
    $($tr).each(function (i, item) {     
        var employeeid = $(item).data().employeeid;
        var oldlapsingdays = $(item).data().lapsingdays;
        var oldvalue = $(item).data().extendedlapsingdays;
        var oldextenddate = $(item).data().extendedlapsingdate;
        var annualvacationtypeid = $(item).data().annualvacationtypeid;
        var balance = null;
        var lapsingdays = null;
        var extlapsingdays = null;
        var extlapsingdate = null;
        $(item).find(".leavetype").each(function (index, obj) {
            balance = null;           
            var vacationtypeid = $(obj).data().vacationtypeid;
            var isaccumulated = $(obj).data().isaccumulated;
            if (vacationtypeid == null || vacationtypeid == undefined || vacationtypeid == "" || vacationtypeid == '')
                vacationtypeid = 0;
            if ($("#txtBalance_" + employeeid + "_" + vacationtypeid + "").length) {
                balance = $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").val();
            }
            else {
                balance = $(obj).text().trim();
            }
          
            if (balance != '--' && (isaccumulated == 'True' || isaccumulated == "True")) {
                $(obj).html("<input data-old='" + balance + "' type='text' value='" + balance + "' class='form-control txtInput txtBalance decimalOnly' id = 'txtBalance_" + employeeid + "_" + vacationtypeid + "'>");

            }

        });

        if ($("#txtLapsingDays_" + employeeid).length) {
            lapsingdays = $("#txtLapsingDays_" + employeeid).val();
        }
        else {
            // var oldvalue = $(closesetRow).find(".extendedlapsingdays").text().trim();
            lapsingdays = $(item).find(".lapsingdays").text().trim();
        }
        if ($("#txtExtLapsingDays_" + employeeid).length) {
            extlapsingdays = $("#txtExtLapsingDays_" + employeeid).val();
        }
        else {
           // var oldvalue = $(closesetRow).find(".extendedlapsingdays").text().trim();
            extlapsingdays = $(item).find(".extendedlapsingdays").text().trim();
        }
        if ($("#txtExtLapsingDate_" + employeeid).length) {
            extlapsingdate = $("#txtExtLapsingDate_" + employeeid).val();
        }
        else {
            // var oldvalue = $(closesetRow).find(".extendedlapsingdays").text().trim();
            extlapsingdate = $(item).find(".extendedlapsedate").text().trim();
        }
        if (extlapsingdays == null || extlapsingdays == undefined || extlapsingdays == "" || extlapsingdays == '')
            extlapsingdays = 0;
        if (extlapsingdate == null || extlapsingdate == undefined || extlapsingdate == "" || extlapsingdate == '')
            extlapsingdate = "-";
        if (lapsingdays == null || lapsingdays == undefined || lapsingdays == "" || lapsingdays == '')
            lapsingdays = 0;
        if (oldextenddate == null || oldextenddate == undefined || oldextenddate == "" || oldextenddate == '')
            oldextenddate = "-";
        if (annualvacationtypeid == null || annualvacationtypeid == undefined || annualvacationtypeid == "" || annualvacationtypeid == '')
            annualvacationtypeid = null;
        if (annualvacationtypeid != null) {
            var isactive = $(item).find(".lapsingdays").data().isactive;
            if (isactive != "False") {
                $(item).find(".lapsingdays").html("<input data-old=" + oldlapsingdays + "  type='text' value =" + lapsingdays + "  class='form-control txtInput decimalOnly txtLapsingDays' id = 'txtLapsingDays_" + employeeid + "'>");

                //if (parseFloat(lapsingdays) > 0) {
                $(item).find(".extendedlapsingdays").html("<input data-old=" + oldvalue + "  type='text' value =" + extlapsingdays + "  class='form-control txtInput decimalOnly txtExtLapsingDays' id = 'txtExtLapsingDays_" + employeeid + "'>");
                $(item).find(".extendedlapsedate").html("<input data-old=" + oldextenddate + "  type='text' value =" + extlapsingdate + "  class='form-control datepicker' id = 'txtExtLapsingDate_" + employeeid + "' readonly >");

            }
            // }
        }
            $(item).find(".action").html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="SaveSingleLeaveBalance(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
            $(item).addClass("IsEdit");
            bindDatePicker('.datepicker');
       

    });
    hideLoaderFrame();
}

function RefreshInEditMode() {
    // alert('RefreshInEditMode');
    var table = $("#tbl_leavebalance").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    $(UpdatedTr).each(function (i, item) {
        var employeeid = $(item).data().employeeid;
        $(item).find(".leavetype").each(function (index, obj) {
            var vacationtypeid = $(obj).data().vacationtypeid;
            var balance = $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").data("old");
            $(obj).html(balance);
        });
        var lapsingdays = $("#txtLapsingDays_" + employeeid).data("old");
        var extendeddays = $("#txtExtLapsingDays_" + employeeid).data("old");
        var extendeddate = $("#txtExtLapsingDate_" + employeeid).data("old");
        $(item).find(".lapsingdays").html(lapsingdays);
        $(item).find(".extendedlapsingdays").html(extendeddays);
        $(item).find(".extendedlapsedate").html(extendeddate);
        $(item).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditLeaveBalance(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
        //+
        // "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
        $(item).removeClass("IsEdit");
    });
    hideLoaderFrame();
}
function EditLeaveBalance(source) {  
    $tr = $(source).closest("tr");
    var closesetRow = $(source).closest('tr');
    var employeeid = $(closesetRow).data().employeeid;
    var lapsingdays = $(closesetRow).data().lapsingdays;
    var oldlapsingdays = $(closesetRow).find(".lapsingdays").text().trim();
    var oldvalue = $(closesetRow).find(".extendedlapsingdays").text().trim();
    var oldextdate = $(closesetRow).find(".extendedlapsedate").text().trim();
    var annualvacationtypeid = $(closesetRow).data().annualvacationtypeid;
    var extlapsingdays = 0;
    if (annualvacationtypeid == null || annualvacationtypeid == undefined || annualvacationtypeid == "" || annualvacationtypeid == '')
        annualvacationtypeid = null;
    if (oldlapsingdays == null || oldlapsingdays == undefined || oldlapsingdays == "" || oldlapsingdays == '')
        oldlapsingdays = null;
    if (oldvalue == null || oldvalue == undefined || oldvalue == "" || oldvalue == '')
        oldvalue = null;
    if (oldextdate == null || oldextdate == undefined || oldextdate == "" || oldextdate == '')
        oldextdate = "-";
    $(closesetRow).find(".leavetype").each(function (index, obj) {
        var vacationtypeid = $(obj).data().vacationtypeid;
        if (vacationtypeid == null || vacationtypeid == undefined || vacationtypeid == "" || vacationtypeid == '')
            vacationtypeid = 0;
        var balance = $(obj).html().trim();
        var isaccumulated = $(obj).data().isaccumulated;
        if (balance != '--' && (isaccumulated == 'True' || isaccumulated == "True")) {
            $(obj).html("<input data-old='" + balance + "' type='text' value='" + balance + "' class='form-control txtInput txtBalance decimalOnly' id = 'txtBalance_" + employeeid + "_" + vacationtypeid + "'>");

        }


    });   
    var isactive = $(closesetRow).find(".lapsingdays").data().isactive;
    if (lapsingdays == null || lapsingdays == undefined || lapsingdays == "" || lapsingdays == '')
        lapsingdays = 0;
    if (annualvacationtypeid != null) {
        if (isactive != "False") 
             $(closesetRow).find(".lapsingdays").html("<input data-old='" + oldlapsingdays + "' type='text' value='" + oldlapsingdays + "'  class='form-control txtInput decimalOnly txtLapsingDays' id = 'txtLapsingDays_" + employeeid + "'>");

        //Already exists extended lapsing days ,no need to change the date,only days can be edited
        if (isactive != "False") {
            if (oldvalue == null) {
                // if (parseFloat(lapsingdays) > 0)
                //{

                $(closesetRow).find(".extendedlapsingdays").html("<input data-old='0' type='text'  class='form-control txtInput decimalOnly txtExtLapsingDays' id = 'txtExtLapsingDays_" + employeeid + "'>");
                $(closesetRow).find(".extendedlapsedate").html("<input data-old='-' type='text' class='form-control datepicker' id = 'txtExtLapsingDate_" + employeeid + "' readonly>");


                //}
            }

            else {
                //  if (parseFloat(lapsingdays) > 0)
                // {
                $(closesetRow).find(".extendedlapsingdays").html("<input data-old='" + oldvalue + "' type='text' value='" + oldvalue + "' class='form-control txtInput decimalOnly txtExtLapsingDays' id = 'txtExtLapsingDays_" + employeeid + "'>");
                $(closesetRow).find(".extendedlapsedate").html("<input data-old='" + oldextdate + "' type='text' value='" + oldextdate + "' class='form-control datepicker' id = 'txtExtLapsingDate_" + employeeid + "' readonly >");

                // }

            }
        }
    }
        bindDatePicker('.datepicker');
        $(closesetRow).find(".action").html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="SaveSingleLeaveBalance(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
        $($tr).addClass("IsEdit");
    
}
function SaveSingleLeaveBalance(source) {
    var closesetRow = $(source).closest('tr');
    var employeeid = $(closesetRow).data().employeeid;
    var annualvacationtypeid = $(closesetRow).data().annualvacationtypeid;
    var updatedData = [];
    var extendeddays = null;
    var extendeddate = null;
    var balance = 0;
    var lapsingdays = 0;
    $(closesetRow).find(".leavetype").each(function (index, obj) {
        var vacationtypeid = $(obj).data().vacationtypeid;     
        var currentbalance = $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").data("old");
        var newbalance = $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").val();
      //  var currentlapsingdays = $("#txtLapsingDays_" + employeeid).data("old");
        var newlapsingdays = $("#txtLapsingDays_" + employeeid).val();
        var isaccumulated = $(obj).data().isaccumulated;
        var vtcategoryid = $(obj).data().vtcategoryid;     
        if (vacationtypeid == annualvacationtypeid) {
            extendeddays = $("#txtExtLapsingDays_" + employeeid).val();
            extendeddate = $("#txtExtLapsingDate_" + employeeid).val();
        }
        else
        {
            extendeddays = null;
            extendeddate = null;
        }
           
        if (extendeddays == null || extendeddays == undefined || extendeddays == "" || extendeddays == '')
            extendeddays = null;
        if (extendeddate == null || extendeddate == undefined || extendeddate == "" || extendeddate == '' || extendeddate == '-' || extendeddate == "-")
            extendeddate = null;
        else
            extendeddate = moment(extendeddate, 'DD/MM/YYYY').toDate();
       
        if (currentbalance != newbalance && newbalance != "" && newbalance != '' && newbalance != undefined) {
            balance = parseFloat(newbalance) - parseFloat(currentbalance);
        }
        else
            balance = 0;
        //if (currentlapsingdays == null || currentlapsingdays == "" || currentlapsingdays == '' || currentlapsingdays == undefined)
        //    currentlapsingdays = 0;
        if (newlapsingdays == null || newlapsingdays == "" || newlapsingdays == '' || newlapsingdays == undefined)
            newlapsingdays = 0;
        //if (currentlapsingdays != newlapsingdays && newlapsingdays != "" && newlapsingdays != '' && newlapsingdays != undefined) {
        //    lapsingdays = parseFloat(newlapsingdays) - parseFloat(currentlapsingdays);
        //}
        //else
        //    lapsingdays = 0;
       
        if (vacationtypeid == annualvacationtypeid) {
            if (extendeddays != null || balance > 0 || lapsingdays > 0) {
                updatedData.push({
                    EmployeeID: employeeid,
                    VacationTypeID: vacationtypeid,
                    Days: balance,
                    ExtendedDays: extendeddays,
                    ExtendedDate: extendeddate,
                    LapsingDays: newlapsingdays
                   // LapsingDays: lapsingdays
                });
            }
            // }
        }
        });
        if (updatedData.length > 0) {
            pageLoaderFrame();
            $.ajax({
                url: "/LeaveBalanceManager/UpdateLeaveBalance",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'accumulativeAllVacationList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);

                        var closesetRow = $(source).closest('tr');
                        var employeeid = $(closesetRow).data().employeeid;

                        $(closesetRow).find(".leavetype").each(function (index, obj) {
                            var vacationtypeid = $(obj).data().vacationtypeid;
                            var balance = $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").val();
                            $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").attr("old", balance);                      
                            $(obj).html(balance);
                        });
                        extendeddays = $("#txtExtLapsingDays_" + employeeid).val();
                        extendeddate = $("#txtExtLapsingDate_" + employeeid).val();
                        lapsingdays = $("#txtLapsingDays_" + employeeid).val();

                        $("#txtExtLapsingDays_" + employeeid).attr("old", extendeddays);
                        $("#txtExtLapsingDate_" + employeeid).attr("old", extendeddate);
                        $("#txtLapsingDays_" + employeeid).attr("old", lapsingdays);

                        $(closesetRow).find(".extendedlapsingdays").html(extendeddays);           
                        $(closesetRow).find(".extendedlapsedate").html(extendeddate);
                        $(closesetRow).find(".lapsingdays").html(lapsingdays);
                        $(closesetRow).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditLeaveBalance(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
                        //+
                        //   "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
                        $(closesetRow).removeClass("IsEdit");
                        hideLoaderFrame();
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            });
        }
        else {
            ShowMessage("error", "Please edit atleast one record.");
            hideLoaderFrame();
        }
   
}

function CancelNewRow(source) {
    var closesetRow = $(source).closest('tr');
    var employeeid = $(closesetRow).data().employeeid;
    $(closesetRow).find(".leavetype").each(function (index, obj) {       
        var vacationtypeid = $(obj).data().vacationtypeid;
        //$("#txtBalance_" + employeeid + "_" + vacationtypeid + "").val();
        var balance = $("#txtBalance_" + employeeid + "_" + vacationtypeid + "").data("old");
        //var managerName = managerId != "" ? $("#ddlManger_" + groupId + "_" + employeeId + " option[value='" + managerId + "']").text() : "";
        $(obj).html(balance);
    });
    var extendeddays = $("#txtExtLapsingDays_" + employeeid).data("old");
    var extendeddate = $("#txtExtLapsingDate_" + employeeid).data("old");
    var lapsingdays = $("#txtLapsingDays_" + employeeid).data("old");
    $(closesetRow).find(".extendedlapsingdays").html(extendeddays);
    $(closesetRow).find(".extendedlapsedate").html(extendeddate);
    $(closesetRow).find(".lapsingdays").html(lapsingdays);
    $(closesetRow).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditLeaveBalance(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
    //+
    //"<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
    $(closesetRow).removeClass("IsEdit");
}

function SaveAllLeaveBalance() {
    AllEditMode = 0;
    var table = $("#tbl_leavebalance").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    var extendeddays = null;
    var extendeddate = null;
    var balance = 0;
    var currentlapsingdays = null;
    var lapsingdays = 0;
    var newlapsingdays = null;
    if (UpdatedTr.length > 0) {        
        var updatedData = [];
        $(UpdatedTr).each(function (i, item) {         
            var employeeid = $(item).data().employeeid;
            var annualvacationtypeid = $(item).data().annualvacationtypeid;
            $(item).find(".leavetype").each(function (index, obj) {
                currentlapsingdays = 0;
                newlapsingdays = 0;
                var vacationtypeid = $(obj).data().vacationtypeid;
                var currentbalance = ($(obj).find("#txtBalance_" + employeeid + "_" + vacationtypeid + "")).data("old");
                if (currentbalance == "" || currentbalance == '' || currentbalance == undefined)
                    currentbalance = 0;
                var newbalance = ($(obj).find("#txtBalance_" + employeeid + "_" + vacationtypeid + "")).val();
                if (currentbalance != newbalance && newbalance != "" && newbalance != '' && newbalance != undefined) {
                    balance = parseFloat(newbalance) - parseFloat(currentbalance);
                }
                else
                    balance = 0;
                var isaccumulated = $(obj).data().isaccumulated;
                var vtcategoryid = $(obj).data().vtcategoryid;
                // if ((isaccumulated == 'True' || isaccumulated == "True") && parseFloat(vtcategoryid) == 1) {
                if (vacationtypeid == annualvacationtypeid) {
                    extendeddays = ($(item).find("#txtExtLapsingDays_" + employeeid)).val();
                    extendeddate = ($(item).find("#txtExtLapsingDate_" + employeeid)).val();
                   // currentlapsingdays = ($(item).find("#txtLapsingDays_" + employeeid)).data("old");
                    newlapsingdays = ($(item).find("#txtLapsingDays_" + employeeid)).val();
                }
                else {
                    extendeddays = null;
                    extendeddate = null;
                    currentlapsingdays = null;
                    newlapsingdays = null;
                }
              
                //if (currentlapsingdays == null || currentlapsingdays == "" || currentlapsingdays == '' || currentlapsingdays == undefined)
                   // currentlapsingdays = 0;
                if (newlapsingdays== null || newlapsingdays == "" || newlapsingdays == '' || newlapsingdays == undefined)
                    newlapsingdays = 0;
               // if (currentlapsingdays != newlapsingdays && newlapsingdays != "" && newlapsingdays != '' && newlapsingdays != undefined) {
                    //lapsingdays = parseFloat(newlapsingdays) - parseFloat(currentlapsingdays);
               // }
                //else
                   // lapsingdays = 0;

                if (extendeddays == null || extendeddays == undefined || extendeddays == "" || extendeddays == '')
                    extendeddays = null;
                if (extendeddate == null || extendeddate == undefined || extendeddate == "" || extendeddate == '' || extendeddate == '-' || extendeddate == "-")
                    extendeddate = null;
                else {
                    extendeddate = moment(extendeddate, 'DD/MM/YYYY').toDate();
                    //alert(xtdate);
                    //alert(xtdate.toISOString());
                    //extendeddate = xtdate;
                }
                if (vacationtypeid == annualvacationtypeid) {
                    if (extendeddays != null || balance > 0 || lapsingdays > 0) {
                        updatedData.push({
                            EmployeeID: employeeid,
                            VacationTypeID: vacationtypeid,
                            Days: balance,
                            ExtendedDays: extendeddays,
                            ExtendedDate: extendeddate,
                            // LapsingDays: lapsingdays
                            LapsingDays: newlapsingdays
                        });
                    }
                }

            });

        });
        if (updatedData.length == 0) {
            ShowMessage("error", "Please edit atleast one record.");
            return;
        }
       // console.log(updatedData);
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/LeaveBalanceManager/UpdateLeaveBalance",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'accumulativeAllVacationList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        LoadLeaveBalance();
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            });
        });
    }
    else {
        ShowMessage("error", "Please edit atleast one record.");
        hideLoaderFrame();
    }
}
function ValidateLapsingDays(source) {
    // alert("Validate");   
    var closesetRow = $(source).closest('tr');
    var employeeid = $(closesetRow).data().employeeid;
    var lapsingdays = $(closesetRow).data().lapsingdays;
    var oldvalue = $(closesetRow).data().extendedlapsingdays;
    if (oldvalue == null || oldvalue == undefined || oldvalue == "" || oldvalue == '')
        oldvalue = null;
    if (lapsingdays == null || lapsingdays == undefined || lapsingdays == "" || lapsingdays == '')
        lapsingdays = 0;
    var extlapsingdays = $("#txtExtLapsingDays_" + employeeid).val();

    if ($("#txtLapsingDays_" + employeeid).length) {
        lapsingdays = $("#txtLapsingDays_" + employeeid).val();
    }
    if ((oldvalue == null || oldvalue == 0) && extlapsingdays > 0) {
        var currentdate = new Date();
        currentdate.setMonth(currentdate.getMonth() + 3);
        // $(closesetRow).find(".extendedlapsedate").html(FormatDate(currentdate));
        $(closesetRow).find(".extendedlapsedate").html("<input data-old='' type='text' value='" + FormatDate(currentdate) + "' class='form-control datepicker txtExtLapsingDate' id = 'txtExtLapsingDate_" + employeeid + "'>");
        bindDatePicker('.datepicker');
    }
    if (parseFloat(extlapsingdays) == 0) {
        $(closesetRow).find(".extendedlapsedate").html("");
    }
    //if (parseFloat(extlapsingdays) > 10) {
    //    ShowMessage("error", "Extended lapse days can't be greater than 10 days!!.");
    //    $("#txtExtLapsingDays_" + employeeid).val('');
    //}else
    // if (parseFloat(extlapsingdays) > lapsingdays) {
    //    ShowMessage("error", "Extended lapse days can't be greater than lapse days!!.");
    //    $("#txtExtLapsingDays_" + employeeid).val(lapsingdays);
    //}
    //else {       
    //    //If not extended before then setting date        
    //    if ((oldvalue == null || oldvalue == 0) && extlapsingdays > 0) {
    //        var currentdate = new Date();         
    //        currentdate.setMonth(currentdate.getMonth() + 3);          
    //        // $(closesetRow).find(".extendedlapsedate").html(FormatDate(currentdate));
    //        $(closesetRow).find(".extendedlapsedate").html("<input data-old='' type='text' value='" + FormatDate(currentdate) + "' class='form-control datepicker txtExtLapsingDate' id = 'txtExtLapsingDate_" + employeeid + "'>");
    //        bindDatePicker('.datepicker');
    //    }
    //    if (parseFloat(extlapsingdays) == 0) {
    //        $(closesetRow).find(".extendedlapsedate").html("");
    //    }
    //}
}

function FormatDate(date) {  
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var date = dd + '/' + mm + '/' + yyyy;
    return date;
}
