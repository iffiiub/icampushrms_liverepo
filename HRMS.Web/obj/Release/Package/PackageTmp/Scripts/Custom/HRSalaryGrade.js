﻿

var oTableChannel;

function getGrid() {

    oTableChannel = $('#tbl_HRSalaryGrade').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/HRSalaryGrade/GetHRSalaryGradeList",
        "aoColumns": [
            
            { "mData": "salaryGradeID", "bVisible": false, "sTitle": "Id" },
            { "mData": "salaryGradeCode", "bSortable": true, "sTitle": "Code" },
            { "mData": "salaryGradeLevel", "bSortable": true, "sTitle": "Level" },
            { "mData": "description", "sTitle": "Description", "bSortable": false },
            { "mData": "monthlyHours", "sTitle": "Monthly Hours","sClass":"ClsPrice" },
            { "mData": "minHourPrice", "sTitle": "MinHour Price","sClass":"ClsPrice"},
            { "mData": "medHourPrice", "sTitle": "MedHour Price","sClass":"ClsPrice"},
            { "mData": "maxHourPrice", "sTitle": "MaxHour Price","sClass":"ClsPrice"},
            { "mData": "isActive", "sTitle": "Is Active" },
            //{ "mData": "isDeleted", "sTitle": "isDeleted" },
            { "mData": "companyId", "sTitle": "Company Id", "bVisible": false },
            { "mData": "Actions", "sTitle": "Actions", "sWidth": "10%" ,"sClass":"ClsAction","bSortable":false}

        ],
        "processing": false,
        "serverSide": true,
        "ajax": "/HRSalaryGrade/GetHRSalaryGradeList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_HRSalaryGrade");
        },
        "bSortCellsTop": true
    });
}

function EditSalaryGrade(id) {
    var buttonName = id > 0 ? "Update": "Submit";
    $("#modal_Loader").load("/HRSalaryGrade/Edit/" + id, function () { 
    $("#myModal").modal("show");
    if (id > 0)
    { $("#modal_heading").text('Edit Salary'); }
    else
    { $("#modal_heading").text('Add Salary'); }
    $("#form0").find('input[type="submit"]').val(buttonName);
    });

}


function DeleteSalaryGrade(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/HRSalaryGrade/Delete',
            success: function (data) {
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewSalaryGrade(id) {

    $("#modal_Loader").load("/HRSalaryGrade/ViewDetails/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Salary');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/HRSalaryGrade/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/HRSalaryGrade/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}