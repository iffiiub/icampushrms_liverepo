﻿$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});

$(document).ready(function () {
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"))
})

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_department').dataTable({
        //"dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/Department/GetDepartmentList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "search", "value": searchval });
        },
        "aoColumns": [

            { "mData": "DepartmentID", "bVisible": false, "bSortable": true, "sTitle": "DepartmentID" },
            { "mData": "CompanyId", "bVisible": false, "sTitle": "CompanyId" },
            { "mData": "DepartmentName_1", "bSortable": true, "sTitle": "Department Name (En)", 'width': '20%' },
            { "mData": "DepartmentName_2", "bSortable": true, "sTitle": "Department Name (Ar)", 'width': '20%' },
            { "mData": "DepartmentName_3", "bSortable": true, "sTitle": "Department Name (Fr)", 'width': '20%' },
             { "mData": "Description", "sTitle": "Description", 'width': '15%' },
            { "mData": "StartDate", "bSortable": true, "sTitle": "Start Date", 'width': '12%', "sType": "date-uk" },
            { "mData": "CreatedBy", "bVisible": false, "sTitle": "Created By", 'width': '13%' },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/Department/GetDepartmentList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[2, "asc"]],
        "fnDrawCallback": function () {
           
        }
    });
}

function EditChannel(id) {
    if (parseInt(id) == -1) {
        $("#modal_heading").text('Add Department');
    }
    else {
        $("#modal_heading").text('Edit Department');
    }
    var buttonName = id > 0 ? "Update" : "Save";

    $("#modal_Loader").load("/Department/Edit/" + id, function () {
        $("#myModal").modal("show");
        $(".modal-dialog").attr("style", "width:900px;");
        //$(this).closest(".modal").css({"overflow-y":"auto"});
        $("#DepartmentForm").find('input[type="submit"]').val(buttonName);
    });

}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Department/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Department/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Department/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

var timer;
function ViewSalaryGrade(id) {
    $("#modal_Loader").load("/Department/ViewDetails/" + id, function () {

    });
    $("#myModal").modal("show");
    $("#modal_heading").text('View Department Details');
    // getGridForDepartmentEmployee();
    //timer = setInterval(getGridForDepartmentEmployee, 1000);
}

function getGridForDepartmentEmployee() {
    var DepartmentId = $("#DepartmentId").val();
    var CompanyId = $("#DepartmentId").val();
    oTableChannel = $('#tbl_DepartmentEmployees').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/Department/getGridForDepartmentEmployee?CompanyId=" + CompanyId + "&DepartmentId=" + DepartmentId,

        "aoColumns": [

            { "mData": "EmployeeId", "bVisible": false, "sTitle": "Code" },
            { "mData": "FirstName", "sTitle": "First Name" },
            { "mData": "LastName", "sTitle": "Last Name" },
             { "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id" }

        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/Department/getGridForDepartmentEmployee?CompanyId=" + CompanyId + "&DepartmentId=" + DepartmentId,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true
    });

    clearInterval(timer);
}

 