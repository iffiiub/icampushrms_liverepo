﻿$(document).on("click", "#familyDetailsTable > tbody > tr", function () {
    $("#familyDetailsTable > tbody > tr").removeClass('highlightRow');
    if ($(this).hasClass('highlightRow')) {
        $(this).removeClass('highlightRow');
    }
    else {
        $(this).addClass('highlightRow');
    }
});

$(document).on("click", "#updateFamilyName", function () {
    var FamilyId = 0;
    var EmpId = 0;

    $("#familyDetailsTable > tbody > tr").each(function (index) {
        if ($(this).hasClass('highlightRow')) {

            FamilyId = $(this).children('td:eq(0)').text();// $(this+':eq(0)').text()
            EmpId = $("#hdnEmployeeId").val();
            // alert(FamilyId+EmpId);
        }
    });
    if (parseInt(FamilyId) > 0) {
        $("#myModal2").modal("hide");
        var msg = "This will assign selected family to this employee.Do you want to continue?.";
        $.MessageBox({
            buttonDone: "Yes",
            buttonFail: "No",
            message: msg,
        }).done(function () {

            $.ajax({

                dataType: 'json',
                type: 'POST',
                data: { EmpId: EmpId, FamilyId: FamilyId },
                url: '/ViewEmployeeProfile/UpdateFamilyId',
                success: function (data) {
                    ShowMessage(data.result, data.resultMessage);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)


                },
                error: function (data) {
                    ShowMessage("error", "Some technical error occurred");
                }
            });
        }).fail(function () {
            ShowMessage("warning", "Selected family is not assign");
        });
    }
    else {
        ShowMessage("error", "Please select family id");
    }


});

$(document).ready(function () {
    var Hostname = window.location.host;
    //alert(Hostname);
    getGrid();

    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);
    $(document).on("click", ".dropdown-toggle", function () {
        $(this).parent().addClass("open");
    });
    $('.modal-header .close').click(function () {
        getGrid();
    });

    try {
        getPastEmploymentGrid($('#hdnEmployeeId').val());
        getInternalEmploymentGrid($('#hdnEmployeeId').val());
    } catch (e) { }
    $("#tblCourseDepartment").dataTable({
        "bFilter": false,
        "fnDrawCallback": function () {
            $("#tblCourseDepartment_length").hide()
        }
    });

});

$("#btn_add").click(function () {

    EditChannel(0);
});

function ExportEmployeeRecordToPDF(id) {
    location.href = "/ViewEmployeeProfile/ExportEmployeeProfileToPdf/" + id;
}

var oTableChannel;

//function loaddiv()
//{
//    alert("s");
//    $("#ExportTo").empty();
//    $("#ExportTo").html('<b> Export As </b><button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToExcel"> <img  style="width:20px;height:20px;" <img src="http://' + Hostname + '/Content/images/excel.png" id="ImgExportToExcel"/>  Excel</button>  <button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToPdf"> <img src="http://' + Hostname + '/Content/images/pdf-icon-transparent-background.png" id="ImgExportToPDf" style="width:20px;height:20px;" />  Pdf</button>');
//}

function getGrid() {
    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined)
    {
        employeeStatus = "1";
    }
    var IncludeInactive = $("#includeInActiveChk").is(':checked');
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        "sAjaxSource": "/Employee/GetEmployeeList?Statustype=" + employeeStatus,
        "aoColumns": [
            { "mData": "Actions", "sTitle": "Actions", "width": "6%", "bSortable": false, "className": "center-text-align" },

            { "mData": "EmployeeAlternativeID", "sTitle": "ID", "width": "6%", "sType": "Int" },
            { "mData": "FirstName", "sTitle": "First Name", "width": "15%" },
            { "mData": "LastName", "sTitle": "Last Name", "width": "15%" },
            { "mData": "Gender", "sTitle": "Gender", "sWidth": "10%" },
            { "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            { "mData": "PositionName", "sTitle": "Position Name", "sWidth": "13%" },
            { "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            { "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            { "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            { "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            { "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" }

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Employee/GetEmployeeList?Statustype=" + employeeStatus,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () { },
        "language": { "loadingRecords": "<span class='loader'></span>" }
    });

}



function EditChannel(id) {

    //$("#modal_Loader").load("/Employee/Edit/" + id);
    //$("#myModal").modal("show");
    //$("#modal_heading").text('Add Employee');
    window.location.href = '/Employee/index/' + id;

}




function TerminateEmployee(id) {
    $("#modal_Loader").load("/Employee/ViewTerminateEmployee/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Terminate Employee');

}

function UpdateStatus(id, status) {


    if (status == "False") {
        var msg = "Are you sure you want to activate the employee ?";
        $.MessageBox({
            buttonDone: "Yes",
            buttonFail: "No",
            message: msg,
        }).done(function () {
            $("#modal_Loader3").html("");
            $("#modal_Loader3").load("/Employee/UpdateRejoingDate/", { EmployeeId: id }, function () {
                $("#myModal3").modal("show");
                $("#modal_heading3").text('Add Rejoining Date');
                $("#HireDate").datepicker();
            });
        }).fail(function () {
            getGrid();
        });
    }
    else {
        var msg = "Are you sure you want to deactivate the employee ?";
        $.MessageBox({
            buttonDone: "Yes",
            buttonFail: "No",
            message: msg,
        }).done(function () {
            $("#modal_Loader3").html("");
            $("#modal_Loader3").load("/Employee/AddEmployeeLeaveReason/", { EmployeeId: id, Status: "False" }, function () {
                $("#myModal3").modal("show");
                $("#modal_heading3").text('Add Employee Leave Reason');
                $("#LeftDate").datepicker();


            });

        }).fail(function () {
            getGrid();
        });
    }
}

function onUpdateRejoiningSuccess(data) {
    $("#myModal3").modal("hide");
    if (data.result == "success") {
        ShowMessage(data.result, data.resultMessage);
        getGrid();
    }
    else {
        ShowMessage("error", "Some technical error occurred");
    }
}
function onUpdateRejoiningFailure() {
    ShowMessage("error", "Some technical error occurred");
}
function onEmployeeLeaveReasonSuccess(data) {
    $("#myModal3").modal("hide");
    if (data.result == "success") {
        $.ajax({

            dataType: 'json',
            type: 'POST',
            data: { Id: data.id, status: data.status },
            url: '/Employee/UpdateStatus',
            success: function (data) {

                ShowMessage(data.result, data.resultMessage);
                getGrid();

                //$("#ExportTo").html('');
                //$("#ExportTo").html('<b> Export As </b><button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToExcel"> <img  style="width:20px;height:20px;" <img src="http://' + Hostname + '/Content/images/excel.png" id="ImgExportToExcel"/>  Excel</button>  <button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToPdf"> <img src="http://' + Hostname + '/Content/images/pdf-icon-transparent-background.png" id="ImgExportToPDf" style="width:20px;height:20px;" />  Pdf</button>');
            },
            error: function (data) {
                ShowMessage("error", "Some technical error occurred");
            }
        });
    }
    else {
        ShowMessage("error", "Some technical error occurred");
    }
}


function onEmployeeLeaveReasonFailure() {
    ShowMessage("error", "Some technical error occurred");
}


function DeleteChannel(id) {


    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Employee/Delete',
        success: function (data) {
            getGrid();
        },
        error: function (data) { }
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}


function AddDependent() {
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader2").html("");
    $("#modal_Loader2").load("/Employee/AddEmployeeDependent/" + EmployeeId);
    $("#myModal2").modal("show");
    $("#modal_heading2").text('Add Employee Dependent');
}
function ChangeFamily() {
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader2").html("");
    $("#modal_Loader2").load("/ViewEmployeeProfile/ChangeFamily/" + EmployeeId);
    $("#myModal2").modal("show");
    $("#modal_heading2").text('Link Family');
}

function CheckFamilyOutstandingBalance() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/FamilyOutstandingBalance?EmployeeId=" + EmployeeId;
}


function AddEmployeeLeaveRequest() {

    var EmployeeId = $("#hdnEmployeeId").val();

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Employee/AddEmployeeLeaveRequest/" + EmployeeId);
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Employee Leave Request');
}
// View Profile 

function AddPastEmployment(id) {

    //var EmployeeId = $("#hdnEmployeeId").val();
    //window.location.href = "/pastemployment/index/" + EmployeeId;

    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader2").html("");
    $("#modal_Loader2").load("/pastemployment/Edit?id=" + id + "&EmployeeID=" + EmployeeId);
    $("#myModal2").modal("show");
    $("#modal_heading2").text('Add Past Employment');

}

function AddInternalEmployment(id) {
    //var EmployeeId = $("#hdnEmployeeId").val();
    //window.location.href = "/employee/InternalEmploymentList/" + EmployeeId;
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader2").html("");
    $("#modal_Loader2").load("/employee/EditInternalEmployment?id=" + id + "&EmployeeID=" + EmployeeId);
    $("#myModal2").modal("show");
    $("#modal_heading2").text('Add Internal Employment');

}

function ExportToExcel() {  
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToExcel?employeeStatus=" + employeeStatus);
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToPdf?employeeStatus=" + employeeStatus);
    }
    else ShowMessage("warning", "No data for export!");
}



function AddEmegencyContact() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/EmergencyContact/Index/" + EmployeeId;
}

function AddOtherAddress() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/OtherAddress/Index/" + EmployeeId;
}



function updateEmployeeStatus(employeeid) {

    var isActive;
    var message = "";
    if ($("#" + employeeid).is(':checked')) {
        isActive = $('#' + employeeid).prop("checked");

    } else {
        isActive = $('#' + employeeid).prop("checked");
    }

    if ($("#" + employeeid).is(':checked')) {
        message = "Are you sure you want to activate the employee ?";
    }
    else {
        message = "Are you sure you want to deactivate the employee ?";
    }


    var status = isActive == "true" ? false : true

    if (isActive == true) {
        $.MessageBox({
            buttonDone: "Yes",
            buttonFail: "No",
            message: message,
        }).done(function () {
            $.ajax({

                dataType: 'json',
                type: 'POST',
                data: { Id: employeeid, status: isActive },
                url: '/Employee/UpdateEmployeeStatus',
                success: function (data) {

                    ShowMessage(data.result, data.resultMessage);
                    getGrid();

                    //$("#ExportTo").html('');
                    //$("#ExportTo").html('<b> Export As </b><button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToExcel"> <img  style="width:20px;height:20px;" <img src="http://' + Hostname + '/Content/images/excel.png" id="ImgExportToExcel"/>  Excel</button>  <button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToPdf"> <img src="http://' + Hostname + '/Content/images/pdf-icon-transparent-background.png" id="ImgExportToPDf" style="width:20px;height:20px;" />  Pdf</button>');
                },
                error: function (data) {
                    ShowMessage("error", "Some technical error occurred");
                }
            });
        }).fail(function () {
            getGrid();
        });
    }
    else {

        $.MessageBox({
            buttonDone: "Yes",
            buttonFail: "No",
            message: message,
        }).done(function () {

            $("#modal_Loader3").html("");
            $("#modal_Loader3").load("/Employee/AddEmployeeLeaveReason/", { EmployeeId: employeeid, Status: status }, function () {
                $("#myModal3").modal("show");
                $("#modal_heading3").text('Add Employee Leave Reason');
                $("#LeftDate").datepicker();
            });


        }).fail(function () {
            getGrid();
        });
    }
    //if ($("#" + employeeid).is(':checked')) {
    //    var result = confirm("Are You Sure You Want to Activate the Employee");
    //}
    //else {
    //    var result = confirm("Are You Sure You Want to Deactivate the Employee");
    //}
    //if(result){
    //$.ajax({
    //    dataType: 'json',
    //    type: 'POST',
    //    data: { "Id": employeeid, "status": isActive },
    //    url: '/Employee/UpdateEmployeeStatus',
    //    success: function (data) {

    //        ShowMessage(data.result, data.resultMessage);
    //        getGrid();
    //    },
    //    error: function (data) { }
    //});

}

function Sucess(arg) {
    $("#myModal").modal("hide");
    getGrid();
    ShowMessage(arg.result, arg.resultMessage);
}

function Add(title, id, dropdownId) {
    $("div.tooltip").hide();
    $("#hdnDropDownReloadId").val(dropdownId);
    $("#hdnDropDownId").val(id);
    $("#modal_Loader").load("/Employee/AddType/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text(title);
}

function AddChildrenToDependant(e, Studentid) {
    var isChecked = $(e).prop('checked');
    var EmployeeId = $("#hdnEmployeeId").val();
    var msg = "Are you sure you want to add child to dependant?";
    if (!isChecked) {
        msg = "Are you sure you want to remove child from dependant?";
    }
    $.MessageBox({
        buttonDone: "Yes",
        buttonFail: "No",
        message: msg,
    }).done(function () {

        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { StudentId: Studentid, EmpId: EmployeeId, isAddedtoDependant: isChecked },
            url: '/ViewEmployeeProfile/AddChildtoDependant',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            },
            error: function (data) {
                ShowMessage("error", "Some technical error occurred");
                if (!isChecked) {
                    $(e).prop('checked', true);
                }
                else {
                    $(e).prop('checked', false);
                }
            }
        });
    }).fail(function () {
        ShowMessage("warning", "Children is not updated for dependant");
        if (!isChecked) {
            $(e).prop('checked', true);
        }
        else {
            $(e).prop('checked', false);
        }
    });
}

function getPastEmploymentGrid(empId) {
    oTablePastEmploymentChannel = $('#tbl_PastEmploymentTable').dataTable({
        "sAjaxSource": "/PastEmployment/GetPastEmploymentList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": empId });
        },
        "aoColumns": [

            { "mData": "PastEmploymentID", "bVisible": false, "sTitle": "PastEmploymentID" },
            { "mData": "FirstName", "sTitle": "First Name" },
            { "mData": "LastName", "sTitle": "Last Name" },
            { "mData": "CompanyName", "sTitle": "Company Name" },
            { "mData": "StartDate", "sTitle": "Start Date", "sType": "date-uk" },
            { "mData": "EndDate", "sTitle": "End Date", "sType": "date-uk" },
            { "mData": "Department", "sTitle": "Department" },
            { "mData": "Position", "sTitle": "Position" },
            { "mData": "Role", "sTitle": "Role", "bVisible": false, },
            { "mData": "refrence", "sTitle": "Refrence" },
            { "mData": "Actions", "sTitle": "Actions" },
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/PastEmployment/GetPastEmploymentList",
        "aLengthMenu": [[10, 25, 50, 100, 1000, 10000000], [10, 25, 50, 100, 1000, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,       
        "drawCallback": function (settings) {            
            var isWithEmpPermission = $("#isWithEmpPermission").is(":checked");
            if (!isWithEmpPermission) {
                $('td:nth-child(9),th:nth-child(9)').hide();
            }
        }
    });
}


function getInternalEmploymentGrid(empId) {

    oTableInternalEmploymentChannel = $('#tbl_InternalEmploymentTable').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/ViewEmployeeProfile/GetInternalEmploymentList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": empId });
        },
        "aoColumns": [

            { "mData": "InternalEmploymentID", "bVisible": false, "sTitle": "InternalEmploymentID" },
            { "mData": "FirstName", "sTitle": "First Name" },
            { "mData": "LastName", "sTitle": "Last Name" },
            { "mData": "StartDate", "sTitle": "Start Date", "sType": "date-uk" },
            { "mData": "EndDate", "sTitle": "End Date", "sType": "date-uk" },
            { "mData": "Department", "sTitle": "Department" },
            { "mData": "Position", "sTitle": "Position" },
            //{ "mData": "Location", "sTitle": "Location" },
            { "mData": "Actions", "sTitle": "Actions", "sWidth": "10%" },
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/ViewEmployeeProfile/GetInternalEmploymentList",
        "aLengthMenu": [[10, 25, 50, 100, 150, 200, 250, 300, 350], [10, 25, 50, 100, 150, 200, 250, 300, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "drawCallback": function (settings) {
            var isWithEmpPermission = $("#isWithEmpPermission").is(":checked");
            if (!isWithEmpPermission) {
                $('td:nth-child(7),th:nth-child(7)').hide();
            }
        }
    });
}

function ExportToExcelInternalEmployee() {
    var employeeId = $("#hdnEmployeeId").val();
    if (oTableInternalEmploymentChannel.fnGetData().length > 0) {
        window.location.assign("/ViewEmployeeProfile/ExportToExcelInternalEmployee?employeeId=" + employeeId + "&addCurrentPosition=" + $("#addCurrentPosition").is(":checked"));
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdfInternalEmployee() {
    var employeeId = $("#hdnEmployeeId").val();
    if (oTableInternalEmploymentChannel.fnGetData().length > 0) {
        window.location.assign("/ViewEmployeeProfile/ExportToPdfInternalEmployee?employeeId=" + employeeId + "&addCurrentPosition=" + $("#addCurrentPosition").is(":checked"));
    }
    else ShowMessage("warning", "No data for export!");
}

function DeleteChannelPastEmployment(id, empId) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/PastEmployment/Delete',
            success: function (data) {
                getPastEmploymentGrid(empId);
                ShowMessage("success", data.resultMessage);
            },
            error: function (data) {
                ShowMessage("error", data.resultMessage);
            }
        });
    });
}

function DeleteInternalEmploymentChannel(id, empId) {

    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Employee/DeleteInternalEmployment',
            success: function (data) {
                getInternalEmploymentGrid(empId);
                ShowMessage("success", data.resultMessage);
            },
            error: function (data) {
                ShowMessage("error", data.resultMessage);
            }
        });
    });
}

