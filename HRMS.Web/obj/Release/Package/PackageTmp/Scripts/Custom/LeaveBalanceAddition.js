﻿$(document).ready(function () {
    loadLeaveBalanceAdditionGrid();
    $(document).on('click', '#btnAddLeaveBalance', function (e) {
        $("#modal_Loader").load("/LeaveBalanceAddition/AddEditLeaveBalanceForm/", function () {
            $("#myModal").modal("show");       
            $("#modal_heading").text('Add Leave Balance');
        });
    });

    $(document).on('click', '#btnSearch', function (e) {
        loadLeaveBalanceAdditionGrid();
    });

    $(document).on('change', '#ddlEmployee, #EmployeeID', function () {
        selectList = "";
        var count = 0;
        selectList = "<option value=\"\">Select Vacation Type</option>";
        var EmpID = $(this).val() == "" ? 0 : $(this).val();
        $.ajax({
            type: 'get',
            url: '/LeaveBalanceAddition/LoadVacationTypesBasedOnEmployee?employeeId=' + EmpID,
            datatype: 'Json',
            success: function (data) {
                $.each(data, function (key, value) {
                    count = count + 1;
                    selectList += '<option value=' + value.VacationTypeId + '>' + value.Name + '</option>';
                });
                if ($("#IsAddMode").val() == undefined) {
                    $("#ddlVacationType").html();
                    $("#ddlVacationType").html(selectList);
                    RefreshSelect("#ddlVacationType");
                }
                else {
                    $("#VacationTypeID").html();
                    $("#VacationTypeID").html(selectList);
                    RefreshSelect("#ddlVacationType");
                    RefreshSelect("#VacationTypeID");
                }
               
                if (count == 0) {
                    ShowMessage("error", "Selected employee is not linked with any leave type, Please assign first any leave type.");
                }
            }
        });
    });
});

function loadLeaveBalanceAdditionGrid() {
    var employeeId = $("#ddlEmployee").val();
    var vacationTypeId = $("#ddlVacationType").val();

    oTableChannel = $('#tblLeaveBalanceAddition').dataTable({
        //"dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/LeaveBalanceAddition/GetLeaveBalanceAdditionList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "employeeId", "value": employeeId });
            aoData.push({ "name": "vacationTypeId", "value": vacationTypeId });
        },
        "aoColumns": [

            { "mData": "EmployeeID", "bVisible": true, "bSortable": true, "sTitle": "EmployeeID", 'width': '10%' },
            { "mData": "EmployeeName", "bVisible": true, "bSortable": true, "sTitle": "Employee Name", 'width': '20%' },
            { "mData": "VacationTypeName", "bVisible": true, "bSortable": true, "sTitle": "Vacation Type Name", 'width': '15%' },
            { "mData": "AdditionalBalance","bVisible": true, "sTitle": "Leave Balance", 'width': '15%' },
            { "mData": "FormattedLeaveEffectiveDate", "bSortable": true, "sTitle": "Effective Date", 'width': '10%', "sType": "date-uk" },
            { "mData": "Comments", "bVisible": true, "sTitle": "Comments", 'width': '15%' },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/LeaveBalanceAddition/GetLeaveBalanceAdditionList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[2, "asc"]],
        "fnDrawCallback": function () {

        }
    });
}

function editLaveBalance(id) {
    $("#modal_Loader").load("/LeaveBalanceAddition/AddEditLeaveBalanceForm?leaveBalanceId=" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Leave Balance');
    });
}


function deleteLeaveBalance(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { leaveAdditionId: id },
            url: '/LeaveBalanceAddition/DeleteLeaveBalance',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                loadLeaveBalanceAdditionGrid();
            },
            error: function (data) { }
        });
    });
}