﻿var firstLoad = false;
var tableHtml;
var tableHtml1;
var oTableLateduduction;
var oTableUnLateduduction;
var checkBoxArray = [];
var undeuctableDdlVal = [];
var deuctableDdlVal = [];


$(document).ready(function () {
    //firstLoad = true;
    //tableHtml = $('#tbl_Deductable .tbl_data_wrap').html();
    //tableHtml1 = $('#tbl_UnDeductable .tbl_data_wrap').html();

    if ($("#hdnMonth").val() != "" && $("#hdnMonth").val() != undefined && $("#hdnYear").val() != "" && $("#hdnYear").val() != undefined) {
        var Month = $("#hdnMonth").val();
        //$("#Month option:contains(" + Month + ")").attr('selected', 'selected');
        var Year = $("#hdnYear").val();
        //$("#Year option:contains(" + Year + ")").attr('selected', 'selected');
        //$("#Year").val(CurruntYear);
        //RefreshSelect('.selectpicker');
    }
    else {
        var currentTime = new Date()
        var month = currentTime.getMonth() + 1
        var day = currentTime.getDate()
        // returns the year (four digits)
        var year = currentTime.getFullYear()
        var nextyear = currentTime.getFullYear() + 1
        var yearrange = year + "-" + nextyear
        var YEarValue = 0
        var name = "2015-2016";
        $("#Month").val(month);
        $("#Year").val(CurruntYear);

    }
    $("#Year").val(CurruntYear);

    $('#btnPreview').click();

    //GetDeductableGrid();
    ////firstLoad = true;
    //GetUnDeductableGrid();
    $("#Generate").click(function () {
        var isChecked = $("#Generate").is(":checked");
        if (isChecked) {
            $("#EffectiveDate").removeAttr('disabled');
            $("#Cycle").attr('disabled', false)
            $("#Cycle").selectpicker('refresh');
        }
        else {
            $("#EffectiveDate").attr('disabled', 'disabled');
            $("#Cycle").attr('disabled', true)
            $("#Cycle").selectpicker('refresh');
        }
    });
});

$(document).on('click', '#tbl_Deductable .Deductablecheckbox', function () {
    if ($(this).is(':checked'))
        AddToCheckArray($(this).attr('id'));
    else
        RemoveToCheckArray($(this).attr('id'));
})

function ChangeAbsentType(obj, PayEmployeeAbsentIDval, type) {
    var PayAbsentTypeIDval = $(obj).val();
    var PayEmployeeAbsentIDval = PayEmployeeAbsentIDval;
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { absenttypeId: PayAbsentTypeIDval, payemployeeAbsentId: PayEmployeeAbsentIDval },
        url: '/AbsentLateDeduction/CheckUpdateAbsentType',
        success: function (data) {
            GetDeductableGrid();
            GetUnDeductableGrid();
        },
        error: function (data) {
            $.MessageBox({
                buttonDone: "Ok",
                message: data.error,
            });
        }
    });

}



function checkall() {
    if ($('#ChkDeductCheckAll').is(':checked')) {
        $(".Deductablecheckbox").prop("checked", true);
        SelectAllCheckBox();
    }
    else {
        $(".Deductablecheckbox").prop("checked", false);
        checkBoxArray = [];
    }
}

$('#btnPreview').click(function () {   
    GetDeductableGrid();
    GetUnDeductableGrid();
    var undeuctableDdlVal = [];
    var deuctableDdlVal = [];
});

function GetDeductableGrid() {

    oTableLateduduction = $('#tbl_Deductable').dataTable({
        //"dom": 'rt<".divFooter"<"pageNo"p>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "Month", "value": $('#Month').val() },
                { "name": "Year", "value": $('#Year option:selected').text() },
                { "name": "FromDate", "value": $('#txtStartDate').val() },
                { "name": "ToDate", "value": $('#txtToDate').val() },
                { "name": "ShowGeneratedOnly", "value": $('#GeneratedDeductions').is(":checked") }                
                );
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "sAjaxSource": "/AbsentLateDeduction/GetAbsentForCurrentMonth",
        "aoColumns": [
            { "mData": "Checkboxes", "sTitle": "<input name='select_all' value='1' type='checkbox' id='ChkDeductCheckAll' onclick='checkall()'>", "bSortable": false, "sWidth": "10%", "sClass": "left-text-align" },
            { "mData": "EmployeeName", "sTitle": "Employee<br/>Name", "sWidth": "32%" },
            { "mData": "AbsentTypeName", "sTitle": "Absent Type", "bSortable": false, "sWidth": "30%", "sClass": "left-text-align" },
            { "mData": "FromDate", "sTitle": "Date", "sWidth": "10%", "bSortable": false },
            { "mData": "ToDate", "sTitle": "To<br/>Date", "sWidth": "10%", "bSortable": false, "sClass": "hidden" },
            { "mData": "Days", "sTitle": "Days", "sWidth": "10%", "sClass": "left-text-align" },
            { "mData": "Amount", "sTitle": "Amount", "sClass": "left-text-align", "sWidth": "10%" },
             { "mData": "PayEmployeeAbsentID", "bVisible": false, "visible": false, "sClass": "hidden" },
            { "mData": "EmployeeID", "bVisible": false, "sClass": "hidden" },
            { "mData": "AbsentID", "bVisible": false, "sClass": "hidden" },
            { "mData": "PayEmployeeAbsentHeaderID", "sClass": "hidden" },
            { "mData": "TotalDedSalary", "bVisible": false, "sClass": "hidden" },
            { "mData": "PayAbsentTypeID", "bVisible": false, "sClass": "hidden" },
            { "mData": "IsConfirmed", "bVisible": false, "sClass": "hidden" },
            { "mData": "Status", "sTitle": "Status", "sClass": "left-text-align", "sWidth": "10%" },
            { "mData": "CycleID", "sTitle": "CycleID", "sClass": "left-text-align", "sWidth": "10%", "sClass": "hidden" },
            { "mData": "NoOfNullCycleCount", "sTitle": "NoOfNullCycleCount", "sClass": "hidden" }
        ],

        "processing": true,
        "serverSide": false,
        'iDisplayLength': 25,
        "bPaginate": true,
        "bLengthChange": false,
        "ajax": "/AbsentLateDeduction/GetAbsentForCurrentMonth",
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Bold the grade for all 'A' grade browsers
            //console.log(aData["PayAbsentTypeID"]);
            var AbsentTypeId = aData["PayAbsentTypeID"];
            //console.log(nRow);
            var select = $(nRow).find("select");
            //console.log(AbsentTypeId);
            //console.log($(select).val());
            //$(select).val(AbsentTypeId);
            $(select).val($(AbsentTypeId).text());// .attr("selected", "selected");
            var IsConfirmed = aData["IsConfirmed"];
            if (IsConfirmed == 1) {                
                $('td', nRow).addClass('highlightRow');
            }
            //var AbsentID = aData[2];
            //console.log(AbsentID);

            //if (aData[4] == "A") {
            //    $('td:eq(4)', nRow).html('<b>A</b>');
            //}            
            $("#tbl_Deductable > thead > tr > th:eq(2)").addClass('sWidth');
            //bindSelectpicker('.selectpickerddl');
            if (aData["CycleID"] == 0) {
                $('td', nRow).css('background-color', '#eed5d5');
            }
            if (aData["NoOfNullCycleCount"] > 0) {
                if ($('#GeneratedDeductions').is(":checked") == true) {
                    $("#lblDeductionMessage").addClass("hidden");
                    $("#lblGeneratedDeduction").removeClass("hidden");
                } else {                   
                    $("#lblGeneratedDeduction").addClass("hidden");
                    $("#lblDeductionMessage").removeClass("hidden");
                    //$("#lblDeductionMessage").text("Highlighted absent records are not under any pay cycle, You cannot generate deductions.");
                    $("#warningMessageCycle").removeClass("hidden");
                }                            
            }
            else {
                if ($('#GeneratedDeductions').is(":checked") == true) {
                    $("#lblDeductionMessage").addClass("hidden");
                    $("#lblGeneratedDeduction").removeClass("hidden");
                } else {
                    $("#lblDeductionMessage").addClass("hidden");
                    $("#lblDeductionMessage").addClass("hidden");
                    $("#warningMessageCycle").addClass("hidden");
                }               
            }
        },
        "fnDrawCallback": function () {
            selectFromJson(deuctableDdlVal);
            bindSelectpicker('#tbl_Deductable .selectpickerddl');
            checkSelection();
            //$("#tbl_Deductable > tbody tr").each(function (index) {
            //    $(this).attr({
            //        'data-toggle': 'tooltip',
            //        'data-placement': 'top',
            //        'title': '',
            //        'data-original-title': 'From Date: ' + $(this).find('#lblFromDate').text() + '\nTo Date: ' + $(this).find('#lblToDate').text() + '\nStatus: ' + $(this).find('#lblStatusID').text()
            //    })
            //});

        },
        "initComplete": function (settings, json) {
            var itemValue = $('#tbl_Deductable tbody tr:first td:last').html();
            if (itemValue == "No data available in table") {
                $("#warningMessageCycle").addClass("hidden");
                $("#AcWarningMessage").addClass("hidden");
            }
            else {
                GetNullAcYearMessage();
            }
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DeductiblePageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DeductiblePageNo'));
        }
    });
}

function GetUnDeductableGrid() {
    oTableUnLateduduction = $('#tbl_UnDeductable').dataTable({
        // "dom": 'rt<".divFooter"<" pageNo"p>>',
        // "dom": 'rt<".divFooter"<" pageNo"p><"clear">>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "Month", "value": $('#Month').val() },
                { "name": "Year", "value": $('#Year option:selected').text() },
                { "name": "StartDate", "value": $('#txtStartDate').val() },
                { "name": "EndDate", "value": $('#txtToDate').val() }
                );
        },
        "sAjaxSource": "/AbsentLateDeduction/GetAbsentsForCurrentMonthExcused",
        "aoColumns": [
            { "mData": "EmployeeName", "sTitle": "Employee <br/> Name", "bSortable": true, "sWidth": "30%" },
            { "mData": "AbsentTypeName", "bSortable": false, "sTitle": "Absent <br/>Type", "sWidth": "30%" },
            { "mData": "FromDate", "sTitle": "Date", "bSortable": false },
            { "mData": "ToDate", "sTitle": "To <br/>Date", "bSortable": false, "sClass": "hidden" },
            { "mData": "PayEmployeeAbsentID", "sTitle": "PayEmployeeAbsentID", "sClass": "hidden" },
            { "mData": "EmployeeID", "sTitle": "EmployeeID", "sClass": "hidden" },
            { "mData": "AbsentID", "sTitle": "AbsentID", "sClass": "hidden" },
            { "mData": "PayEmployeeAbsentHeaderID", "sTitle": "PayEmployeeAbsentHeaderID", "sClass": "hidden" },
            { "mData": "Days", "sTitle": "Days", "sClass": "hidden" },
            { "mData": "PayAbsentTypeID", "sTitle": "PayAbsentTypeID", "sClass": "hidden" },
            { "mData": "Status", "sTitle": "Status", "sClass": "left-text-align", "sWidth": "10%" },
            { "mData": "IsConfirmed", "sTitle": "Confirmed", "sClass": "hidden" },
        ],

        "processing": false,
        "serverSide": false,
        'iDisplayLength': 25,
        "bPaginate": true,
        "bLengthChange": false,
        "ajax": "/AbsentLateDeduction/GetAbsentsForCurrentMonthExcused",
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Bold the grade for all 'A' grade browsers
            //console.log(aData["PayAbsentTypeID"]);
            var AbsentTypeId = aData["PayAbsentTypeID"];        
            //console.log(nRow);
            var select = $(nRow).find("select");
            //console.log(AbsentTypeId);
            //console.log($(select).val());
            //$(select).val(AbsentTypeId);
            $(select).val(AbsentTypeId).attr("selected", "selected");
            //var AbsentID = aData[2];
            //console.log(AbsentID);

            //if (aData[4] == "A") {
            //    $('td:eq(4)', nRow).html('<b>A</b>');
            //}
            //bindSelectpicker('.selectpickerddl');
        },
        "fnDrawCallback": function () {
            selectFromJson(undeuctableDdlVal);
            bindSelectpicker('#tbl_UnDeductable .selectpickerddl');
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('NonDeductiblePageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('NonDeductiblePageNo'));
        }
    });
}

/*Saving Functionality*/
$('#btnConfirm').click(function () {
    //Confirm();
    if ($('#Generate').is(":checked") == true) {
        SaveAbsentDeduction();
    } else { ShowMessage("error", "Please select generated checkbox"); }

});


//ABSENT DEDUCTION
function Confirm() {

    var EffectiveDate = $("#EffectiveDate").val();

    if (EffectiveDate != "") {
        var x = 0;

        var stringArray = new Array();

        var AbsentDeductionList = [];
        $("#tbl_Deductable tbody tr").each(function (i, item) {
            var id = $(this).find('.Deductablecheckbox').attr('data-rowId');
            if ($(this).find('.Deductablecheckbox').is(':checked')) {

                var curRow = {};
                curRow.EmployeeId = $(this).find('.Deductablecheckbox').attr('id');
                curRow.PayEmployeeAbsentHeaderID = $(this).find('#lblPayEmployeeAbsentHeaderID').text();
                curRow.EffectiveDate = $("#EffectiveDate").val();
                AbsentDeductionList.push(curRow);
            }

        });
        if (AbsentDeductionList.length != 0) {
            AbsentDeductionList = JSON.stringify({ 'AbsentDeductionList': AbsentDeductionList });
            console.log(AbsentDeductionList);
            $.ajax({
                url: '/AbsentLateDeduction/ConfirmEmployeeForAbsentDeductedStatus',
                type: 'POST',
                data: AbsentDeductionList,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',

                success: function (data) {
                    //alert(data.Result);
                    ShowMessage("success", "Absent Deduction Confirmed successfully.");
                    window.setTimeout(function () { location.reload() }, 1000)
                },

            });
        }
        else {
            ShowMessage("error", "Please select employees");
        }
    }
    else {
        ShowMessage("error", "Please Select  Date ");
    }
}



function SaveAbsentDeduction() {

    var EffectiveDate = $("#EffectiveDate").val();
    //&& $('#IsGenerated').is(':checked') == true
    pageLoaderFrame();
    if (EffectiveDate != "" && $('#Generate').is(':checked') == true) {
        var x = 0;

        var stringArray = new Array();

        var AbsentDeductionList = [];
        var table = $('#tbl_Deductable').DataTable();
        var allRow = table.rows().data();
        if ($("#ChkDeductCheckAll").is(':checked')) {
            $(allRow).each(function (i, item) {
                var curRow = {};
                curRow.EmployeeId = $(item.EmployeeID).text();
                curRow.PayEmployeeAbsentHeaderID = $(item.PayEmployeeAbsentHeaderID).text();
                curRow.EffectiveDate = $("#EffectiveDate").val();
                curRow.AbsentTypeName = $(item.AbsentTypeName).val();
                curRow.Days = $(item.Days).text();
                curRow.FromDate = $(item.FromDate).attr('data-FromDate');
                curRow.ToDate = $(item.ToDate).attr('data-Todate');
                curRow.Amount = $(item.Amount).text();
                curRow.PayAbsentTypeID = $(item.PayAbsentTypeID).text();
                curRow.TotalDedSalary = $(item.TotalDedSalary).text();
                curRow.PayEmployeeAbsentID = $(item.PayEmployeeAbsentID).text();
                curRow.IsConfirmed = $(item.IsConfirmed).text();
                curRow.PaidCycle = $('#Cycle').val();
                curRow.Status = $(item.Status).text();
                curRow.EmployeeName = item.EmployeeName;
                AbsentDeductionList.push(curRow);
            });
        } else {
            $(allRow).each(function (i, item) {
                var checkBoxId = $(item.Checkboxes).attr('id');
                $.each(checkBoxArray, function (ind2, item2) {
                    if (item2 == checkBoxId) {
                        var curRow = {};
                        curRow.EmployeeId = $(item.EmployeeID).text();
                        curRow.PayEmployeeAbsentHeaderID = $(item.PayEmployeeAbsentHeaderID).text();
                        curRow.EffectiveDate = $("#EffectiveDate").val();
                        curRow.AbsentTypeName = $(item.AbsentTypeName).val();
                        curRow.Days = $(item.Days).text();
                        curRow.FromDate = $(item.FromDate).attr('data-FromDate');
                        curRow.ToDate = $(item.ToDate).attr('data-Todate');
                        curRow.Amount = $(item.Amount).text();
                        curRow.PayAbsentTypeID = $(item.PayAbsentTypeID).text();
                        curRow.TotalDedSalary = $(item.TotalDedSalary).text();
                        curRow.PayEmployeeAbsentID = $(item.PayEmployeeAbsentID).text();
                        curRow.IsConfirmed = $(item.IsConfirmed).text();
                        curRow.PaidCycle = $('#Cycle').val();
                        curRow.Status = $(item.Status).text();
                        curRow.EmployeeName = item.EmployeeName;
                        AbsentDeductionList.push(curRow);
                    }
                });
            });
        }

        //$("#tbl_Deductable tbody tr").each(function (i, item) {
        //    var id = $(this).find('.Deductablecheckbox').attr('id');
        //    if ($(this).find('.Deductablecheckbox').is(':checked')) {
        //        var curRow = {};
        //        curRow.EmployeeId = $(this).find('.Deductablecheckbox').attr('id');
        //        curRow.PayEmployeeAbsentHeaderID = $(this).find('#lblPayEmployeeAbsentHeaderID').text();
        //        curRow.EffectiveDate = $("#EffectiveDate").val();
        //        curRow.AbsentTypeName = $(this).find('.ddlAbsentTypeName').val();
        //        curRow.Days = $(this).find('#lblDays').text();
        //        curRow.FromDate = $(this).find('#lblFromDate').text();
        //        curRow.ToDate = $(this).find('#lblToDate').text();
        //        curRow.Amount = $(this).find('#lblAmount').text();
        //        curRow.PayAbsentTypeID = $(this).find('#lblPayAbsentTypeID').text();
        //        curRow.TotalDedSalary = $(this).find('#lblTotalDedSalary').text();
        //        curRow.PayEmployeeAbsentID = $(this).find('#lblPayEmployeeAbsentID').text();
        //        curRow.IsConfirmed = $(this).find('#lblIsConfirmed').text();
        //        curRow.PaidCycle = $('#Cycle').val();
        //        AbsentDeductionList.push(curRow);
        //    }
        //});

        if (AbsentDeductionList.length != 0) {
            $.ajax({
                url: '/AbsentLateDeduction/ConfirmEmployeeForAbsentDeductedStatus',
                type: 'POST',
                data: JSON.stringify({ 'AbsentDeductionList': AbsentDeductionList, 'PaidCycleId': 1 }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    hideLoaderFrame();
                    ShowMessage("success", "Absent deduction saved successfully.");
                    window.setTimeout(function () { location.reload() }, 1000)
                },
            });
        }
        else {
            hideLoaderFrame();
            ShowMessage("error", "Please select employees");
        }
    }
    else {
        hideLoaderFrame();
        ShowMessage("error", "Please Select Date to confirm and Check Generate");
    }

}

function checkSelection() {
    var id = 0;
    var isPresent = false;
    $.each($("#tbl_Deductable .Deductablecheckbox"), function (ind, item) {
        id = $(item).attr("id");
        if (checkBoxArray.length > 0) {
            $.each(checkBoxArray, function (ind2, item2) {
                if (item2 == id) {
                    $(item).prop("checked", true);
                }
            });
        } else {
            $("#tbl_Deductable .Deductablecheckbox").prop("checked", false);
        }
    });

}

function AddToCheckArray(id) {
    var isPresent = false;
    $.each(checkBoxArray, function (ind, item) {
        if (item == id)
            isPresent = true;
    });
    if (!isPresent)
        checkBoxArray.push(id);
}

function RemoveToCheckArray(id) {
    checkBoxArray = $.grep(checkBoxArray, function (value) {
        return value != id;
    });
}

function SelectAllCheckBox() {
    checkBoxArray = [];
    var table = $('#tbl_Deductable').DataTable();
    var allRow = table.rows().data();
    $(allRow).each(function (i, item) {
        var itemId = $(item.Checkboxes).attr('id')
        checkBoxArray.push(itemId)
    });
}

function removeJson(jsonObj, val) {
    $.each(jsonObj, function (ind, item) {
        if (item.id == val)
            jsonObj.splice(ind, 1);
    });
}

function addJson(jsonObj, id, val) {
    jsonObj.push({ 'id': id, 'value': val });
}

function selectFromJson(json) {
    $.each(json, function (ind, item) {
        $("#" + item.id).val(item.value);
    });
}

function GetNullAcYearMessage() {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Startdate: $('#txtStartDate').val(), EndDate: $('#txtToDate').val(), Month: $('#Month').val(), Year: $('#Year option:selected').text() },
        url: '/AbsentLateDeduction/GetNullAcademicYears',
        success: function (data) {
            if (data.AcRecords != '') {
                $("#AcWarningMessage").removeClass("hidden");
                $("#lblAcYearWarnong").text(data.Message);

            }
            else {
                $("#AcWarningMessage").addClass("hidden");
            }
        }
    });

}
