﻿$(function () {

    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: 'dd/mm/yyyy',
        todayHighlight: true
    });

    $('.DOB').datepicker({
        dateFormat: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: 'dd/mm/yyyy',
        todayHighlight: true
    }).on('change', function () {
        try {
            $(this).valid();  // triggers the validation test
        }
        catch (e)
        { }
    });

    var date = new Date();
    date.setDate(date.getDate() + 1);

    $('.NextDaydatepicker').datepicker({
        dateFormat: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        startDate: date
    }).on('change', function () {
        try {
            $(this).valid();  // triggers the validation test
        }
        catch (e)
        { }
    });

    $('.CurrentDate').datepicker({
        dateFormat: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: 'dd/mm/yyyy',
        todayHighlight: true
    }).datepicker('setDate', new Date());


    $('.datepickerFrom').datepicker({
        dateFormat: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: 'dd/mm/yyyy',
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepickerTo').datepicker('setStartDate', minDate);
    }).on('show', function (selected) {
        //if (selected.date != undefined) {
        //    var minDate = new Date(selected.date.valueOf());
        //    $('.datepickerTo').datepicker('setStartDate', minDate);
        //}
    });

    $('.datepickerTo').datepicker({
        dateFormat: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: 'dd/mm/yyyy',
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepickerFrom').datepicker('setEndDate', minDate);
    }).on('show', function (selected) {   
        //if (selected.date != undefined) {
        //    var minDate = new Date(selected.date.valueOf());
        //    $('.datepickerFrom').datepicker('setEndDate', minDate);
        //}
    });
});

function getDatepickerDate(element) {
    var dateFormat = "mm/dd/yy";
    var date;
    try {
        //date = $.datepicker.parseDate(dateFormat, element.value);
        date = $(element).val();
    } catch (error) {
        date = null;
    }
    return date;
}

//$(document).ready(function () {

//    $('#StartDate').datepicker({
//        dateFormat: 'dd-mm-yy',
//        changeMonth: true,
//        changeYear: true,
//        yearRange: '-50:+50',
//        buttonImage: "../../Content/images/calender-icon.png",
//        showOn: "both",
//        buttonImageOnly: true,
//        minDate: new Date(),
//        buttonImageText: "Calendar",
//        onClose: function (selectedDate) {
//            if (selectedDate != '') {
//                $("#EndDate").datepicker("option", "minDate", selectedDate);
//            }
//        }
//    });
//    $('#EndDate').datepicker({
//        dateFormat: 'dd-mm-yy',
//        changeMonth: true,
//        changeYear: true,
//        yearRange: '-50:+50',
//        buttonImage: "../../Content/images/calender-icon.png",
//        showOn: "both",
//        buttonImageOnly: true,
//        //maxDate: new Date(),       
//        buttonImageText: "Calendar",
//        onClose: function (selectedDate) {
//            if (selectedDate != '') {
//                $("#StartDate").datepicker("option", "maxDate", selectedDate);
//            }
//        }
//    });
//});