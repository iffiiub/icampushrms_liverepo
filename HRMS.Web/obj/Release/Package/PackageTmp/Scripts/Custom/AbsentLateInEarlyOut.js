﻿$(document).ready(function () {
    if ($("#hdnEmployee").val() != "") {
        var id = $("#hdnEmployee").val();
        LoadEmployeeDetails(id);
        getGridAbsentLateInEarlyOut(id);
    }
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
});

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGridAbsentLateInEarlyOut($("#hdnEmployee").val());
});

$("#btnTextSearch").click(function () {
    getGridAbsentLateInEarlyOut($("#hdnEmployee").val());
});

$("#btn_addAbsentLateInEarlyOut").click(function () {
    if (parseInt($("#hdnEmployee").val()) > 0 && $("#hdnEmployee").val() !== undefined) {
        var EmployeeId = $("#hdnEmployee").val();
        AddAbsentLateInEarlyOut(EmployeeId);
    }
    else {
        ShowMessage("error", "Please select employee.")
    }
});

var oTableChannel;

function getGridAbsentLateInEarlyOut(empid) {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_AbsentLateInEarlyOutList').dataTable({
        "sAjaxSource": "/AbsentLateInEarlyOut/GetAbsentLateInEarlyOutList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "search", "value": searchval }, { "name": "empid", "value": empid });
        },
        "aoColumns": [
            { "mData": "PayEmployeeLateDate", "sTitle": "Late Date", "sType": "date-uk", 'width': '12%' },
            { "mData": "PayLateTypeID", "sTitle": "Type", 'width': '12%' },
            { "mData": "InTime", "sTitle": "In Time", 'width': '12%' },
            { "mData": "OutTime", "sTitle": "Out Time", 'width': '12%' },
            { "mData": "LateMinutes", "sTitle": "Late Mins", 'width': '12%' },
            { "mData": "EarlyMinutes", "sTitle": "Early Mins", 'width': '12%' },
            { "mData": "LateReason", "sTitle": "Late Reason", 'width': '20%' },
            { "mData": "Action", "sTitle": "Actions", "sClass": "ClsAction", "bSortable": false, 'width': '8%' },
        ],

        "processing": false,
        "serverSide": false,
        "ajax": "/AbsentLateInEarlyOut/GetAbsentLateInEarlyOutList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_AbsentLateInEarlyOutList");
        }
    });
}

function AddAbsentLateInEarlyOut() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/AbsentLateInEarlyOut/AddAbsentLateInEarlyOut", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add Late In Early Out');
        $('.timePicker12').timepicker({
            showMeridian: true,
            showInputs: true,
        });
        GetLateAndEarlyMinutes();
    });
}

function EditAbsentLateInEarlyOut(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/AbsentLateInEarlyOut/EditAbsentLateInEarlyOut/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Late In Early Out');
        $('#OutTime').timepicker({
            showMeridian: true,
            showInputs: true,
        });
        $('#InTime').timepicker({
            showMeridian: true,
            showInputs: true,
        });
        $('#timepicker').timepicker('setTime', $('#timepicker').val());
        $('#timepicker').timepicker('setTime', $('#timepicker').val());
    });
}

function DeleteAbsentLateInEarlyOut(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/AbsentLateInEarlyOut/DeleteAbsentLateInEarlyOut',
            success: function (data) {
                var id = $("#hdnEmployee").val();

                getGridAbsentLateInEarlyOut(id);
            },
            error: function (data) { }
        });
    }).fail(function () { });
}

function ViewChannel(id) {
    $('.tblChannel').hide();
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewAbsentLateInEarlyOut(id) {
    $("#modal_Loader").load("/AbsentLateInEarlyOut/ViewAbsentLateInEarlyOut/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View AbsentLateInEarlyOut Details');
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function GetLateAndEarlyMinutes() {
    var employeeId = $("#EmployeeID").val();
    if (parseInt(employeeId) > 0) {
        $.ajax({
            dataType: "json",
            type: "GET",
            data: { employeeId: employeeId, date: $("#PayEmployeeLateDate").val(), inTime: $("#InTime").val(), outTime: $("#OutTime").val() },
            url: '/Common/GetLateAndEarlyMinutesForEmployee',
            success: function (data) {
                $("#LateMinutes").val(data.lateMinute);
                $("#EarlyMinutes").val(data.earlyMinute);
            },
            error: function (data) { }
        });
    }
}

function CheckEmployeeInAbsent(callBack) {
    var empId = $("#EmployeeID").val();
    var fromDate = $("#PayEmployeeLateDate").val();
    var toDate = $("#PayEmployeeLateDate").val();
    $.ajax({
        url: "/AbsentLateInEarlyOut/CheckEmployeeInAbsent",
        data: { EmpID: empId, FromDate: fromDate, ToDate: toDate },
        dataType: "json",
        type: "GET",
        success: function (data) {
            callBack(data.isPresent)
        },
        error: function () {

        }

    })
}