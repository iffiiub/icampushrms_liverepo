﻿
function Start() {

}

function Sucess(arg) {
    $("#myModal").modal("hide");
    ShowMessage("success", arg.responseJSON.resultMessage);
    getGrid();
}

function ValidateForm(evt) {
    var startMonth = $("#StartMonth").val();
    var endMonth = $("#EndMonth").val();
    var weekStartDay = $("#WeekStartDay").val();
    var weekEndDay = $("#WeekEndDay").val();
    if (startMonth == endMonth)
    { ShowMessage("warning", "Start month should not be same as end month."); }
    else if (weekStartDay == weekEndDay)
    { ShowMessage("warning", "Week start day should not be same as week end day."); }
    else { $("#YearForm").submit(); }
}

var oTableChannel;

function getGrid() {

    oTableChannel = $('#tbl_year').dataTable({
        "sAjaxSource": "/Year/GetYearList",
        "aoColumns": [
            { "mData": "WeekStartDay", "sTitle": "Week Start Day","width":"22%" },
            { "mData": "WeekEndDay", "sTitle": "Week End Day", "width": "22%" },
            { "mData": "StartMonth", "sTitle": "Start Month", "width": "22%" },
            { "mData": "EndMonth", "sTitle": "End Month", "width": "22%" },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "12%" }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/Year/GetYearList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            
        },
    });
}

function EditChannel(id) {
    if (id == 0) {
        $("#modal_Loader").load("/Year/Edit/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            $("#myModal").modal("show");
            $("#modal_heading").text('Add Year');
        });
    }

    if (id != 0 && id != null) {
        $("#modal_Loader").load("/Year/Edit/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            $("#myModal").modal("show");
            $("#modal_heading").text('Edit Year');
            $("form").find('input[type="submit"]').val('Update');
        });
    }
}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Year/Delete',
            success: function (data) {
                ShowMessage("success", data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}


function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Year/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Year/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}
