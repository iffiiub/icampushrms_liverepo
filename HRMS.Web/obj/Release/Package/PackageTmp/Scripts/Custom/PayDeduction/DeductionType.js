﻿$(document).ready(function () {

    getGrid();
    //BindEmployees();
    $("#btn_add").click(function () {
        EditDeductionType(-1);
        return false;
    });
});

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});

var oTableChannel;


function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_PayDeductionTypes').dataTable({
        "sAjaxSource": "/Deduction/GetDeductionTypesList",
        "aoColumns": [

            //{ "mData": "DeductionTypeID", "bVisible": false, "sTitle": "DeductionTypeID" },
            { "mData": "DeductionTypeName_1", "bSortable": true, "sTitle": "Deduction Name(EN)", "width": "11%" },
            { "mData": "DeductionTypeName_2", "bSortable": true, "sTitle": "Deduction Name (AR)", "width": "11%" },
            { "mData": "DeductionTypeName_3", "sTitle": "Deduction Name (FR)", "width": "11%" },
            { "mData": "DeductionTypeShortName_1", "sTitle": "Short Code (EN)", "width": "11%" },
            //{ "mData": "DeductionTypeShortName_2", "sTitle": "Short Code (AR)", "width": 100 },
            //{ "mData": "DeductionTypeShortName_3", "sTitle": "Short Code (FR)", "width": 100 },          
            { "mData": "TransactionDate", "sTitle": "Transaction Date", "sType": "date-uk", "width": "11%" },
            { "mData": "Sequence", "sTitle": "Sequence", "width": "11%" },
            { "mData": "acc_code", "sTitle": "Account Code", "width": "11%" },
              { "mData": "Actions", "sTitle": "Actions", "sClass": "ClsAction", "bSortable": false, "width": "11%" }

        ],
        "processing": false,
        "serverSide": false,
        "shrinkToFit": false,
        "ajax": "/Deduction/GetDeductionTypesList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]]
    });
}

function EditDeductionType(id) {
    var buttonName = id > -1 ? "Update" : "Submit";
    $("#addPaySalaryModal_Loader").html("");


    $("#addPaySalaryModal_Loader").load("/Deduction/EditDeductionType/" + id, function () {
        bindSelectpicker(".selectpickerddl");
        $("#addPaySalaryModal").modal("show");
        if (id == -1) {
            $("#addPaySalaryModal_heading").text('Add Deduction Type');
        }
        else {
            $("#addPaySalaryModal_heading").text('Edit Deduction Type');
        }
        bindPositiveOnly(".numberOnly");
        $("#btnDeductionSubmit").click(function (e) {
            if ($("#hdnDeductionName").val().trim().length > 0) {
                if ($("#hdnDeductionName").val().trim() == $("#DeductionTypeName_1").val().trim()) {
                    $("#frmAddDeductionType").submit();
                }
                else {
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        async: false,
                        data: { deductionName: $("#DeductionTypeName_1").val() },
                        url: '/Deduction/CheckDeductionNameExist',
                        success: function (data) {
                            if (data == "true") {
                                ShowMessage("error", "Deduction type name is already exist.")
                            } else {
                                $("#frmAddDeductionType").submit();
                            }
                        },
                        error: function (data) { }
                    });
                }
            }
            else {
                $("#frmAddDeductionType").submit();
            }
        })

        $("#frmAddDeductionType").submit(function () {
            var returnType = true;
            //if (parseInt($("#acc_code").val()) > 0) {
            //    returnType = true;
            //} else {
            //    if ($("#acc_code").val().length == 0)
            //        $("span[data-valmsg-for=acc_code]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='acc_code' class=''>This field is required.</span>");
            //    else
            //        $("span[data-valmsg-for=acc_code]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='acc_code' class=''>Invalid Account code</span>");
            //    returnType = false;
            //}

            //if (parseInt($("#VacationAccountCode").val()) > 0) {
            //    returnType = true;
            //} else {
            //    if ($("#VacationAccountCode").val().length == 0)
            //        $("span[data-valmsg-for=VacationAccountCode]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='VacationAccountCode' class=''>This field is required.</span>");
            //    else
            //        $("span[data-valmsg-for=VacationAccountCode]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='VacationAccountCode' class=''>Invalid Vacation Account code</span>");
            //    returnType = false;
            //}
            return returnType;
        });
    });
}

function deductionFormSubmit() { 
    
}

function DeleteDeductionType(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete Pay Deduction?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Deduction/DeletePayDeductionType',
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

$("#btnEditCancel").on("click", function () {
    $("#DeductiontypeModal").modal("hide");
});



function CheckOSTDedTypeExist(e) {
    if ($(e).is(":checked")) {
        $.ajax({
            type: 'POST',           
            url: '/Deduction/CheckDedTypeExistForOST',
            success: function (data) {
                if (data.result == 'success')
                    //ShowMessage(data.result, data.resultMessage);
                    $("#warningMessage").removeClass("hidden");
            },
            error: function (data) { }
        });

    }
    else {
        $("#warningMessage").addClass("hidden");
    }
}

