﻿var oTableDeductionDetails;
function getGrid(PayDeductionId) {

    oTableDeductionDetails = $('#tbl_PayDeductionDetails').dataTable({
        "dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        "language": {
            "lengthMenu": "Records per page _MENU_ ",
        },
        "sAjaxSource": "/Deduction/GetPayDeductionList",

        "aoColumns": [
           
            { "mData": "PayDeductionID", "bVisible": false, "sTitle": "PayDeductionID" },
            { "mData": "DeductionTypeID", "bVisible": false, "sTitle": "DeductionTypeID" },
            { "mData": "EmployeeID", "bVisible": false, "sTitle": "EmployeeID" },
            { "mData": "RefNumber", "sTitle": "RefNumber" },
            { "mData": "DeductionType", "sTitle": "DeductionType" },
            { "mData": "EffectiveDate", "sTitle": "Date" },
            { "mData": "GenerateDate", "sTitle": "Generated Date" },
            { "mData": "PaidCycle", "sTitle": "PaidCycle" },
            { "mData": "Amount", "sTitle": "Amount" },
            { "mData": "Installment", "sTitle": "Installment" },
            { "mData": "IsInstallment", "sTitle": "IsInstallment" },
            { "mData": "TransactionDate", "sTitle": "TransactionDate" },
            { "mData": "PvId", "sTitle": "PvId" },
             { "mData": "Actions", "sTitle": "Actions" },

        ],
        "processing": false,
        "serverSide": true,
        "ajax": "/Deduction/GetPayDeductionList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "paging": false,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": employeeId });
        },
    });
}