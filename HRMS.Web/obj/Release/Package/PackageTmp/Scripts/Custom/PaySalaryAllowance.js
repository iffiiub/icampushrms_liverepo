﻿$(document).ready(function () {


    getGrid();

    $("#btn_add").click(function () {        
        EditChannel(-1);
    });

    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);

});

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});

$("#btnTextSearch").click(function () {

    getGrid();
});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_company').dataTable({         
        "sAjaxSource": "/PaySalaryAllowance/GetPaySalaryAllowanceList",
        "aoColumns": [

            { "mData": "PaySalaryAllowanceName_1", "bSortable": true, "sTitle": "Allowance Name (En)", "width": "22%" },
           { "mData": "PaySalaryAllowanceName_2", "bSortable": true, "sTitle": "Allowance Name (Ar)", "width": "22%" },
           { "mData": "DateFrom", "sTitle": "Tran. Date", "sType": "date-uk", "width": "14%" },
           { "mData": "PaySalaryAllowanceShortName_1", "sTitle": "Short Code (En)", "width": "14%" },
           { "mData": "IsIncludeInDeduction", "sTitle": "Include Ded.", "bSortable": false, "sClass": "center-text-align", "width": "14%" },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "13%" }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PaySalaryAllowance/GetPaySalaryAllowanceList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
                          
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var buttonName = id > -1 ? "Update" : "Submit";
    $("#addPaySalaryModal_Loader").load("/PaySalaryAllowance/Edit/" + id, function () {
        $("#addPaySalaryModal").modal("show");
        $("#addPaySalaryModal .modal-dialog").attr("style", "width:630px;");
        if (id > -1)
        { $("#addPaySalaryModal_heading").text('Edit Salary Allowance'); }
        else { $("#addPaySalaryModal_heading").text('Add Salary Allowance'); }
        bindPositiveOnly(".numberOnly");
        bindSelectpicker(".selectpickerddl");
        $("#btnSalaryAllowance").click(function () {
            var salaryAllowanceName = $("#PaySalaryAllowanceName_1").val().trim();
            var hiddensalaryAllowanceName = $("#hdnPaySalaryAllowanceName_1").val().trim();
            if (!(hiddensalaryAllowanceName == salaryAllowanceName)) {
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    data: { salaryAllowanceName: salaryAllowanceName },
                    url: '/PaySalaryAllowance/CheckSalaryAllowance',
                    success: function (data) {
                        if (data == false)
                            $("#frmPaySalaryAllowance").submit();
                        else
                            ShowMessage("error", "Salary allowance name is aready present, please try different name");
                    },
                    error: function (data) { }
                });
            }
            else
                $("#frmPaySalaryAllowance").submit();
        });

        $("#frmPaySalaryAllowance").submit(function () {
            var returnType = false;
            if (typeof ($("#AccountCode").val()) === "string" && $("#AccountCode").val().length > 1) {
                returnType = true;
            } else {
                if (parseInt($("#AccountCode").val()) > 0) {
                    returnType = true;
                } else {
                    if ($("#AccountCode").val().length == 0)
                        $("span[data-valmsg-for=AccountCode]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='AccountCode' class=''>This field is required.</span>");
                    else
                        $("span[data-valmsg-for=AccountCode]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='AccountCode' class=''>Invalid Account code</span>");
                    returnType = false;
                }
            }

            if (typeof ($("#VacationAccountCode").val()) === "string" && $("#VacationAccountCode").val().length > 1) {
                returnType = true;
            } else {
                if (parseInt($("#VacationAccountCode").val()) > 0) {
                    returnType = true;
                } else {
                    if ($("#VacationAccountCode").val().length == 0)
                        $("span[data-valmsg-for=VacationAccountCode]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='VacationAccountCode' class=''>This field is required.</span>");
                    else
                        $("span[data-valmsg-for=VacationAccountCode]").removeClass("field-validation-valid").addClass("field-validation-error").html("<span for='VacationAccountCode' class=''>Invalid Vacation code</span>");

                    returnType = false;
                }
            }
            return returnType;

        })
    });
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Pay category accounts will be delete with deleting salary allowance. Are you sure you want to continue.?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/PaySalaryAllowance/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function Start() {
    $.blockUI();
}

function SucessCompany(arg) {
    $.unblockUI();
    ShowMessage(arg.responseJSON.result, arg.responseJSON.resultMessage);
    //$("#myModal").modal("hide");
    $("#addPaySalaryModal").modal("hide");
    getGrid();
}

//}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/PaySalaryAllowance/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/PaySalaryAllowance/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}