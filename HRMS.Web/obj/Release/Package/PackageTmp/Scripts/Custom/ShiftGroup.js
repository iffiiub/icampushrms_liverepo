﻿

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_ShiftGroup').dataTable({
        "sAjaxSource": "/ShiftGRoup/GetShiftGroupList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "search", "value": searchval });
        },
        "aoColumns": [

            { "mData": "GroupNameID", "bVisible": false, "bSortable": true, "sTitle": "Group Name ID" },

            { "mData": "GroupNameEn", "bSortable": true, "sTitle": "Group Name (En) ", "width": "28%" },
            { "mData": "GroupNameAr", "bSortable": true, "sTitle": "Group Name (Ar)", "width": "28%" },
            { "mData": "GroupNameFr", "bSortable": true, "sTitle": "Group Name (Fr)", "width": "28%" },

            { "mData": "Head", "bVisible": false, "bSortable": true, "sTitle": "Head" },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "14%" }

        ],
        "processing": false,
        "serverSide": true,
        "ajax": "/ShiftGRoup/GetShiftGroupList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_ShiftGroup");
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var buttonName = id > -1 ? "Update": "Submit";

    $("#modal_Loader").load("/ShiftGRoup/Edit/" + id, function () {
        $("#myModal").modal("show");
        bindSelectpicker('.selectpickerddl');
        if (id > -1)
        { $("#modal_heading").text('Edit Shift Group'); }
        else { $("#modal_heading").text('Add Shift Group'); }
        //$("#ShiftGroupForm").find('input[type="submit"]').val(buttonName);
        //$(".modal-dialog").attr("style", "width:700px;");
    });
}


function Start() {

}
function Sucess(arg) {
    $("#myModal").modal("hide");
    ShowMessage(arg.responseJSON.result, arg.responseJSON.resultMessage);
    getGrid();
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/ShiftGRoup/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}


//function ExportToExcel() {
//    if (oTableChannel.fnGetData().length > 0) {
//        window.location.assign("/Department/ExportToExcel");
//    }
//    else ShowMessage("warning", "No data for export!");
//}

//function ExportToPdf() {
//    if (oTableChannel.fnGetData().length > 0) {
//        window.location.assign("/Department/ExportToPdf");
//    }
//    else ShowMessage("warning", "No data for export!");
//}

function ViewShiftGroup(id) {
    $("#modal_Loader").load("/ShiftGRoup/ViewDetails/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('View Shift Group Details');
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}



