﻿$("#btn_addAccommodationHistory").click(function () {

    if (parseInt($("#EmployeeID").val()) > 0 && $("#EmployeeID").val() !== undefined) {


        var EmployeeId = $("#EmployeeID").val();

        $("#ActiveEmployee").val(EmployeeId);
        AddAccommodationHistory();

    }
    else {
        ShowMessage("error", "Please select employee.")
    }
});

$("#btn_addMedicalHistory").click(function () {

    if ($("#EmployeeID").val() != "" && $("#EmployeeID").val() !== undefined) {


        var EmployeeId = $("#EmployeeID").val();

        $("#ActiveEmployee").val(EmployeeId);
        AddMedicalHistory();

    }
    else {
        ShowMessage("error", "Please select employee.")
    }




});

$("#btn_addMedicalalerts").click(function () {

    if ($("#EmployeeID").val() != "" && $("#EmployeeID").val() !== undefined) {


        var EmployeeId = $("#EmployeeID").val();

        $("#ActiveEmployee").val(EmployeeId);
        AddMedicalAlert();

    }
    else {
        ShowMessage("error", "Please select employee.")
    }




});

$("#btn_addMaintenanceInfo").click(function () {

    if ($("#EmployeeID").val() != "" && $("#EmployeeID").val() !== undefined) {


        var EmployeeId = $("#EmployeeID").val();

        $("#ActiveEmployee").val(EmployeeId);
        AddMaintenanceInfo();

    }
    else {
        ShowMessage("error", "Please select employee.")
    }




});

$("#btn_addGasCard").click(function () {

    if (parseInt($("#EmployeeID").val()) > 0 && $("#EmployeeID").val() !== undefined) {
        var EmployeeId = $("#EmployeeID").val();
        $("#ActiveEmployee").val(EmployeeId);
        AddGasCard();
    }
    else {
        ShowMessage("error", "Please select employee.")
    }

});

function AddAccomadation(title, id, dropdownId) {
    $("div.tooltip").hide();
    $("#hdnDropDownReloadId").val(dropdownId);
    $("#hdnDropDownId").val(id);
    $("#modal_Loader2").load("/Employee/AddType/" + id);
    $("#myModal2").modal("show");
    $("#modal_heading2").text(title);
}

function AddGasCard() {

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Allowance/AddGasCard");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Gas Card');


}

function DeleteGasCard(id) {

    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Allowance/DeleteGasCard',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                var id = $("#hdnEmployee_ID").val();


                getGridGasCard(id)
            },
            error: function (data) { }
        });
    });
}

function AddMaintenanceInfo() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Allowance/AddMaintenanceInfo");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Maintenance Info');


}

function AddAccommodationHistory() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Allowance/AddAccommodationHistory");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Accommodation History');


}

var oTableChannel;

function EditInsuranceDependence(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Insurance/EditInsdependence/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Insdependence');
        $("form").find('input[type="submit"]').val('Update');
    });
}

function getGridAccommodationHistory(empid) {
    oTableChannel = $('#tbl_AccommodationHistoryList').dataTable({
        "sAjaxSource": "/Allowance/GetAccommodationHistoryList",

        "aoColumns": [
                        { "mData": "PayEmployeeAccommodationHistoryID", "sTitle": "PayEmployeeAccommodationHistoryID", "bVisible": false },
                        { "mData": "PayEmployeeAccommodationHistoryYear", "sTitle": "Year", "sWidth": "20%" },
                        { "mData": "AccommodationHistoryAmount", "sTitle": "Amount", "sClass": "ClsPrice", "sWidth": "10%" },
                        { "mData": "Furnished", "sTitle": "Furnished", "bSortable": false, "sWidth": "10%" },
                        { "mData": "FurnishedAmount", "sTitle": "Furnished Amount", "sClass": "ClsPrice", "sWidth": "10%" },
                        { "mData": "Electricity", "sTitle": "Electricity", "bSortable": false, "sWidth": "10%" },
                        { "mData": "ElectricityAmount", "sTitle": "Electricity Amount", "sClass": "ClsPrice", "sWidth": "10%" },
                        { "mData": "Water", "sTitle": "Water", "bSortable": false, "sWidth": "10%" },
                        { "mData": "WaterAmount", "sTitle": "Water Amount", "sClass": "ClsPrice", "sWidth": "10%" },
                        { "mData": "Action", "sTitle": "Action", "bSortable": false, "sClass": "ClsAction", "sWidth": "10%" }
        ],

        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "ajax": "/Allowance/GetAccommodationHistoryList",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": false,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_AccommodationHistoryList");
        },
        "bSortCellsTop": true

    });
}

function getGridMaintenanceInfo(empid) {
    oTableChannel = $('#tbl_MaintenanceInfoList').dataTable({
        "sAjaxSource": "/Allowance/GetMaintenanceInfoList",
        "aoColumns": [
            //{ "mData": "PayEmployeeAccommodationHistoryID", "sTitle": "PayEmployeeAccommodationHistoryID", "bVisible": false },
            { "mData": "PayEmployeeAccommodationHistoryYear", "sTitle": "Year" },
            { "mData": "AccommodationHistoryAmount", "sTitle": "Amount", "sClass": "ClsPrice" },

            { "mData": "Furnished", "sTitle": "Furnished" },
            { "mData": "FurnishedAmount", "sTitle": "Furnished Amount", "sClass": "ClsPrice" },
            { "mData": "Electricity", "sTitle": "Electricity" },
            { "mData": "ElectricityAmount", "sTitle": "Electricity Amount", "sClass": "ClsPrice" },
            { "mData": "Water", "sTitle": "Water" },
            { "mData": "WaterAmount", "sTitle": "Water Amount", "sClass": "ClsPrice" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sClass": "ClsAction" }
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": true,
        "ajax": "/Allowance/GetMaintenanceInfoList",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": false,
        "fnDrawCallback": function () {
            removeSpan("#tbl_MaintenanceInfoList");
        },
        "bSortCellsTop": true

    });
}

function EditAccommodationHistory(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Allowance/EditAccommodationHistory/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Accommodation History');
        //$("form").find('input[type="submit"]').val('Update');
    });
}

function DeleteAccommodationHistory(id) {

    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Allowance/DeleteAccommodationHistory',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                var id = $("#hdnEmployee_ID").val();


                getGridAccommodationHistory(id);
            },
            error: function (data) { }
        });
    });
}

function getGridGasCard(empid) {
    oTableChannel = $('#tbl_GasCardList').dataTable({
        "sAjaxSource": "/Allowance/GetGasCardList",
        "aoColumns": [
            //{ "mData": "PayGasCardID", "sTitle": "PayGasCardID", "bVisible": false },
            { "mData": "PayGasCardValue", "sTitle": "Value", "sClass": "ClsPrice" },
            { "mData": "Company", "sTitle": "Company" },
            { "mData": "ReceivedDate", "sTitle": "ReceivedDate", "sType": "date-uk" },
            { "mData": "PayGasCardNumber", "sTitle": "Number" },
            { "mData": "PayGasCardPeriod", "sTitle": "Period" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sClass": "ClsAction" },
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "ajax": "/Allowance/GetGasCardList",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": false,
        "fnDrawCallback": function () {
            removeSpan("#tbl_GasCardList");
        },
        "bSortCellsTop": true

    });
}

function EditGasCard(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Allowance/EditGasCard/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Gas Card');
        //$("form").find('input[type="submit"]').val('Update');
    });
}

function getGridMedicalVaccination(empid) {

    oTableChannel = $('#tbl_MedicalVaccination').dataTable({
        "sAjaxSource": "/EMedical/GetMedicalVaccination",
        "aoColumns": [

   { "mData": "Action", "sTitle": "Action", "bSortable": false },

        { "mData": "EmployeeVaccinationID", "sTitle": "EmployeeVaccination", "bVisible": false },
{ "mData": "EmployeeVaccinationDate", "sTitle": "Date" },

{ "mData": "Vaccination", "sTitle": "Vaccination" },

{ "mData": "Administrated", "sTitle": "Administrated" },

{ "mData": "Note", "sTitle": "Note", "sWidth": "2%", "bSortable": false }



        ],



        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": true,
        "ajax": "/EMedical/GetMedicalVaccination",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": false,
        "fnDrawCallback": function () {
            removeSpan("#tbl_MedicalVaccination");
        },
        "bSortCellsTop": true

    });
}

function getGridMedicalAlert(empid) {

    oTableChannel = $('#tbl_MedicalMainAlerts').dataTable({
        "sAjaxSource": "/EMedical/GetMedicalAlertList",
        "aoColumns": [

   { "mData": "Action", "sTitle": "Action", "bSortable": false },

        { "mData": "EmployeeMedicalAlertID", "sTitle": "EmployeeMedicalAlertID", "bVisible": false },
{ "mData": "MedicalAlert", "sTitle": "Alert(ENG)" },

{ "mData": "MedicalAlertAr", "sTitle": "Alert(Arabic)" }


        ],



        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": true,
        "ajax": "/EMedical/GetMedicalAlertList",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": false,
        "fnDrawCallback": function () {
            removeSpan("#tbl_MedicalMainAlerts");
        },
        "bSortCellsTop": true

    });
}

function AddMedicalAlert() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/AddMedicalAlert");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Medical Alert');
}

function AddMedicalVaccination() {
    $("#addPaySalaryModal_Loader").html("");
    $("#addPaySalaryModal_Loader").load("/EMedical/AddMedicalVaccination");
    $("#addPaySalaryModal").modal("show");
    $("#addPaySalaryModal_heading").text('Add Medical Vaccination');
}

function AddMedicalHistory() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/AddMedicalHistory");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Medical History');
}

function EditMedicalVaccination(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/EditMedicalVaccination/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Medical Vaccination');
    });
}

function EditHealthHistory(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/EditMedicalHistory/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Medical History');
    });
}

function EditAbsent(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/EditAbsent/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Absent');
        $("form").find('input[type="submit"]').val('Update');
    });
}

function EditMedicalAlert(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/EditMedicalAlert/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Medical Alert');
    });
}

function DeleteHealthHistory(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EMedical/DeleteHealthHistory',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                var id = $("#hdnEmployee_ID").val();
                getGridMedicalHistory(id);
            },
            error: function (data) { }
        });
    });
}

function DeleteMedicalVaccination(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete Medical Vaccination record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EMedical/DeleteMedicalVaccination',
            success: function (data) {
                ShowMessage("success", "Medical Vaccination record deleted successfully.");
                var id = $("#hdnEmployee_ID").val();
                getGridMedicalVaccination(id);
            },
            error: function (data) { }
        });
    });
}

function TransPortationSetup() {
    var ishasCar = $("#transportationModel_HasOwnCar").is(":checked")
    if (ishasCar == true) {
        $("#transportationModel_BySchoolCar").prop('checked', false);
        $("#transportationModel_IsShuttle").prop('checked', false);
        DisabledSelect('.disableddl', false);
        $("#transportationModel_PayTransportationValue").removeAttr("disabled");
        $("#transportationModel_ModelName").removeAttr("disabled");
        $("#transportationModel_ReceivedDate").removeAttr("disabled");
        $("#transportationModel_Ecurrence").removeAttr("disabled");
        $("#transportationModel_Maintenance").removeAttr("disabled");
    }

    var isSchoolCar = $("#transportationModel_BySchoolCar").is(":checked")
    if (isSchoolCar == true) {
        DisabledSelect('.disableddl', true);
        $("#transportationModel_PayTransportationValue").attr("disabled", "disabled");
        $("#transportationModel_ModelName").attr("disabled", "disabled");
        $("#transportationModel_ReceivedDate").attr("disabled", "disabled");
        $("#transportationModel_Ecurrence").attr("disabled", "disabled");
        $("#transportationModel_Maintenance").attr("disabled", "disabled");
    }
    else {
        var IsShuttle = $("#transportationModel_IsShuttle").is(":checked")
        if (IsShuttle == false) {
            DisabledSelect('.disableddl', false);
            $("#transportationModel_PayTransportationValue").removeAttr("disabled");
            $("#transportationModel_ModelName").removeAttr("disabled");
            $("#transportationModel_ReceivedDate").removeAttr("disabled");
            $("#transportationModel_Ecurrence").removeAttr("disabled");
            $("#transportationModel_Maintenance").removeAttr("disabled");
        }
    }
    var IsShuttle = $("#transportationModel_IsShuttle").is(":checked")
    if (IsShuttle == true) {
        DisabledSelect('.disableddl', true);
        $("#transportationModel_PayTransportationValue").attr("disabled", "disabled");
        $("#transportationModel_ModelName").attr("disabled", "disabled");
        $("#transportationModel_ReceivedDate").attr("disabled", "disabled");
        $("#transportationModel_Ecurrence").attr("disabled", "disabled");
        $("#transportationModel_Maintenance").attr("disabled", "disabled");
    }
    else {
        var isSchoolCar = $("#transportationModel_BySchoolCar").is(":checked")
        if (isSchoolCar == false) {
            DisabledSelect('.disableddl', false);
            $("#transportationModel_PayTransportationValue").removeAttr("disabled");
            $("#transportationModel_ModelName").removeAttr("disabled");
            $("#transportationModel_ReceivedDate").removeAttr("disabled");
            $("#transportationModel_Ecurrence").removeAttr("disabled");
            $("#transportationModel_Maintenance").removeAttr("disabled");
        }
    }


}

function DeleteMedicalAlert(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete Medical Alert?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EMedical/DeleteMedicalAlert',
            success: function (data) {
                ShowMessage("success", "Medical History deleted successfully.");
                var id = $("#hdnEmployee_ID").val();

                getGridMedicalAlert(id);
            },
            error: function (data) { }
        });
    });
}
