﻿var hideClass = "";
var amountSetting = "";
var hidetaxpercentage = "False";
$(document).ready(function () { 
    hidetaxpercentage = $("#HideTaxPercentage").val();
    getSecurityList();
    $(document).on("keypress", ".taxAmount,.inputPercentage", function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});


$("#btn_Generate").click(function () {
    var Cycleid = $("#ddl_CyclesList").val();
    if (parseInt(Cycleid) > 0) {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/SocialSecurity/GetCycleDataById?cycleId=' + $('#ddl_CyclesList').val() + '&date=' + $('#txtEffectiveDate').val(),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data) {
                    PostRecords();
                } else {
                    ShowMessage("error", "Please select valid deduction date for selected cycle");
                }
            },
            error: function (data) { }
        });
    } else { ShowMessage("error", 'Please select Pay Cycle.'); }
});



var oTableChannel;

function getSecurityList() {    
    oTableChannel = $('#tbl_SocialSecurity').dataTable({
        "sAjaxSource": "/SocialSecurity/GetSocialSecurityList",
        "aoColumns": [
                         {
                             "mData": "CheckRow", "sTitle": "<input type='checkbox' id = 'check_all' name = 'check_all' onclick='CheckAll()'/>", "sClass": "center",
                             "bSortable": false, "width": "5%"
                         },
                         { "mData": "EmployeeAlternativeID", "sTitle": "Emp. ID", "sClass": "EmployeeAlternativeID", "width": "5%", "sType": "Int" },
                         { "mData": "EmployeeID", "sTitle": "EmployeeID", "bVisible": false, "bSortable": false, "sClass": "EmployeeID" },
                         { "mData": "FullName", "sTitle": "Employee Name", "sClass": "FullName", "width": "40%" },
                         { "mData": "Salary", "sTitle": "Salary Amount", "width": "20%" },
                         { "mData": "Percentage", "sTitle": "Social Security (%)", "sClass": hideClass, "width": "10%" },
                         { "mData": "Amount", "sTitle": "Social Security Amount", "width": "20%" },
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bSortCellsTop": true,
        "order": [[3, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_SocialSecurity");
            if (hidetaxpercentage == "True" || hidetaxpercentage == 'True') {
                $('td:nth-child(5),th:nth-child(5)').hide();
                DisplayAmountInEditMode();
            }
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('PageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('PageNo'));
        }
    });
}
function DisplayAmountInEditMode() {
    $tr = $('#tbl_SocialSecurity tbody tr');
    $($tr).each(function (i, item) {
        var amt = $(item).find("td").eq(5).text();
        if (amt != null && amt != '' && amt != "") {
            $(item).find("td").eq(5).html("<input  onchange='saveSecurityTax(this)'  type='text' value='" + amt + "' class='form-control taxAmount' />");

        }
    });
}
function ChkRowCheckClick(obj) {
    if ($(obj).is(':checked')) {
        $(obj).closest('tr').addClass("selected");
        $(obj).attr('data-SelectState', 'true');
    }
    else {
        $('#check_all').prop('checked', false);
        $(obj).closest('tr').removeClass("selected");
        $(obj).attr('data-SelectState', 'false');
    }

}

function PostRecords() {
    var Effectivedate = $("#txtEffectiveDate").val();
    var Cycleid = $("#ddl_CyclesList").val();
    var Cyclename = $("#ddl_CyclesList option:selected").text();
    var table = $('#tbl_SocialSecurity').DataTable();
    rows = $('#tbl_SocialSecurity tr.selected');
    if (Cycleid > 0) {
        if (rows.length == 0) {
            ShowMessage("error", 'Please select at least one Employee from the list.');
        }
        else {
            rowData = table.rows(rows).data();
            var SocialSecurityList = [];
            var allRow = table.rows().data();
            if ($('#check_all').is(':checked')) {
                $(allRow).each(function (i, item) {
                    if (!findItemInArray(SocialSecurityList, "EmployeeID", item.EmployeeID)) {
                        var curRow = {};
                        curRow.EmployeeAltId = item.EmployeeAlternativeID;
                        curRow.EmployeeID = item.EmployeeID;
                        curRow.EmployeeName = item.FullName;
                        curRow.Percentage = parseFloat($(item.Percentage).val()).toFixed(3);
                        curRow.Amount = item.Amount;
                        SocialSecurityList.push(curRow);
                    }
                });
            } else {
                var selectedCheckBox = table.$(".ChkRowCheck:checked", { "page": "all" });
                $(allRow).each(function (i, item) {
                    var checkboxId = $(allRow[i].CheckRow).attr('id');
                    $.each(selectedCheckBox, function (ii, itemTemp) {
                        if ($(itemTemp).attr('id') == checkboxId) {
                            if (!findItemInArray(SocialSecurityList, "EmployeeID", item.EmployeeID)) {
                                var curRow = {};
                                curRow.EmployeeAltId = item.EmployeeAlternativeID;
                                curRow.EmployeeID = item.EmployeeID;
                                curRow.EmployeeName = item.FullName;
                                curRow.Percentage = parseFloat($(item.Percentage).val()).toFixed(3);
                                curRow.Amount = item.Amount;
                                SocialSecurityList.push(curRow);
                            }
                        }
                    })
                });
            }

            if (SocialSecurityList.length != 0) {
                pageLoadingFrame("show");
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    //    data: { PasitaxLists: PASITaxList,effectivedate:Effectivedate,cycleid:Cycleid,cyclename:Cyclename },
                    data: {
                        SocialSecurityDetails: JSON.stringify(SocialSecurityList),
                        EffectiveDate: Effectivedate,
                        CycleName: Cyclename,
                        CycleID: Cycleid,
                        IsValidate: true
                    },
                    url: '/SocialSecurity/PushRecords',
                    success: function (data) {
                        pageLoadingFrame("hide");
                        if (data.CssClass == 'success') {
                            ShowMessage(data.CssClass, data.Message);
                        }
                        else if (data.CssClass == "warning") {                                                       
                            $("#modal_no_head").modal("show");
                        }
                        else {
                            ShowMessage(data.CssClass, data.Message);
                        }
                    },
                    error: function (data) { }
                });
            }
        }
    }
    else { ShowMessage("error", 'Please select Pay Cycle.'); }
}

function CheckAll() {
    if ($("#check_all").is(':checked')) {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".ChkRowCheck").closest('tr').addClass("selected");
    }
    else {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".ChkRowCheck").closest('tr').removeClass("selected");
    }
}

function saveSecurityTax(e) { 
    var $row = $(e).closest('tr');
    var taxAmount, taxPercentage;
    //taxPercentage = parseFloat($row.find('td').eq('4').text());
    if (hidetaxpercentage == "True" || hidetaxpercentage == 'True') {
        taxPercentage = parseFloat('0.00');
        taxAmount = parseFloat($row.find('input.taxAmount:text').val());
    }
    else {
        taxPercentage = parseFloat($(e).val()).toFixed(2);
        taxAmount = parseFloat($row.find('td').eq('5').text());

    }
    taxAmount = taxAmount.toFixed(amountSetting);
    taxPercentage = parseFloat(taxPercentage).toFixed(amountSetting);
  //  var taxPercentage = $row.find('input.inputPercentage:text').val();
    var ssid = parseFloat($($row.find('td').eq('0').html()).attr('data-ssid'));
    var employeeId = parseFloat($($row.find('td').eq('0').html()).attr('data-Id'));
    var employeeAlternativeId = $row.find('td').eq('1').html();
    var employeeName = $row.find('td').eq('2').html();
 //   var taxAmount = $row.find('td').eq('5').html();
    var Salamount = $row.find('td').eq('3').html();
    var rowIndex = $(e).closest('tr').index();
    var rowArray = {
        "CheckRow": "<input data-Id='" + employeeId + "' type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id='Chkcheck" + employeeId + "' name='check" + employeeId + "'/>",
        "EmployeeAlternativeID": employeeAlternativeId,
        "EmployeeID": employeeId,
        "FullName": employeeName,
        "Salary": Salamount,
        "Amount": taxAmount,
        "Percentage": "<input onkeyup='calculateTax(this)' onchange='saveSecurityTax(this)' data-Max='100' data-Min='0' type='text' value='" + taxPercentage + "' class='form-control inputPercentage'/>"
    }

    if (taxAmount >= 0) {
        pageLoadingFrame("show");
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { employeeId: employeeId, taxPercentage: taxPercentage, SSID: ssid, taxAmount: taxAmount },
            url: '/SocialSecurity/SaveTaxPerEmployee',
            success: function (data) {
                pageLoadingFrame("hide");
                ShowMessage(data.CssClass, data.Message);
                if (data.Success) {
                   getSecurityList();
                }
            },
            error: function (data) {
                pageLoadingFrame("hide");
            }
        });
    }
}

function calculateTax(e) {
    var maxValue = parseFloat($(e).attr('data-Max'));
    var minValue = parseFloat($(e).attr('data-Min'));
    var value = parseFloat($(e).val());
    if (maxValue < value || minValue > value) {
        var valueStr = $(e).val()
        $(e).val(valueStr.substring(0, valueStr.length - 1));
    }
    if (parseFloat($(e).val()) >= 0) {
        var $row = $(e).closest('tr');
        var amount = $($row).find('td').eq('3').text();
        value = parseFloat($(e).val());
        amount = ((parseFloat(value) * parseFloat(amount)) / 100);
        $($row).find('td').eq('5').text(amount.toFixed(amountSetting));
        //$($row).find('td').eq('5').html("<input onkeyup='calculateTaxPercentage(this)' onchange='saveSecurityTax(this)' type='text' value='" + amount.toFixed(amountSetting) + "' class='form-control taxAmount' />");
    }
}

function Regenerate(isAll)
{
    var url=""
    if (isAll==1) {
        url = '/SocialSecurity/PushRecords'
    }
    else {
        url = '/SocialSecurity/UpdateSocialTaxExcludingPresent'
    }

    var Effectivedate = $("#txtEffectiveDate").val();
    var Cycleid = $("#ddl_CyclesList").val();
    var Cyclename = $("#ddl_CyclesList option:selected").text();
    var table = $('#tbl_SocialSecurity').DataTable();
    rows = $('#tbl_SocialSecurity tr.selected');
    if (Cycleid > 0) {
        if (rows.length == 0) {
            ShowMessage("error", 'Please select at least one Employee from the list.');
        }
        else {
            rowData = table.rows(rows).data();
            var SocialSecurityList = [];
            var allRow = table.rows().data();
            if ($('#check_all').is(':checked')) {
                $(allRow).each(function (i, item) {
                    if (!findItemInArray(SocialSecurityList, "EmployeeID", item.EmployeeID)) {
                        var curRow = {};
                        curRow.EmployeeAltId = item.EmployeeAlternativeID;
                        curRow.EmployeeID = item.EmployeeID;
                        curRow.EmployeeName = item.FullName;
                        curRow.Percentage = parseFloat($(item.Percentage).val()).toFixed(3);
                        curRow.Amount = item.Amount;
                        SocialSecurityList.push(curRow);
                    }
                });
            } else {
                var selectedCheckBox = table.$(".ChkRowCheck:checked", { "page": "all" });
                $(allRow).each(function (i, item) {
                    var checkboxId = $(allRow[i].CheckRow).attr('id');
                    $.each(selectedCheckBox, function (ii, itemTemp) {
                        if ($(itemTemp).attr('id') == checkboxId) {
                            if (!findItemInArray(SocialSecurityList, "EmployeeID", item.EmployeeID)) {
                                var curRow = {};
                                curRow.EmployeeAltId = item.EmployeeAlternativeID;
                                curRow.EmployeeID = item.EmployeeID;
                                curRow.EmployeeName = item.FullName;
                                curRow.Percentage = parseFloat($(item.Percentage).val()).toFixed(3);
                                curRow.Amount = item.Amount;
                                SocialSecurityList.push(curRow);
                            }
                        }
                    })
                });
            }
            if (SocialSecurityList.length != 0) {
                pageLoadingFrame("show");
                $.ajax({
                    dataType: 'json',
                    type: 'POST',                 
                    data: {
                        SocialSecurityDetails: JSON.stringify(SocialSecurityList),
                        EffectiveDate: Effectivedate,
                        CycleName: Cyclename,
                        CycleID: Cycleid,
                        IsValidate: false
                    },
                    url: decodeURI(url),
                    success: function (data) {
                        pageLoadingFrame("hide");
                        $("#modal_no_head").modal("hide");
                        ShowMessage(data.CssClass, data.Message);
                    },
                    error: function (data) {
                        pageLoadingFrame("hide");
                    }
                });
            }
        }
    }
    else { ShowMessage("error", 'Please select Pay Cycle.'); }
}



