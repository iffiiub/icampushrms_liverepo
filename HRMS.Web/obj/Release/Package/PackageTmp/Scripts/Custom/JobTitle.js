﻿$(document).ready(function () {
    //alert("IN");
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));
    getGrid();
    $("#btn_add").click(function () {
        EditChannel(-1);
    });
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);
    $("#btnTextClear").click(function () {
        $('#txtSearch').val(""); // $('#txtSearch').val() == "";
        getGrid();
    });
    $("#btnTextSearch").click(function () {
        getGrid();
    });
});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/Job/GetJobTitleList" ,
        "aoColumns": [

            //{ "mData": "JobTitleID", "bVisible": false, "sTitle": "Code" },
            { "mData": "JobTitleName_1", "sTitle": "Job Title (En)", "width": "25%" },
            { "mData": "JobTitleName_2", "sTitle": "Job Title (Ar)", "width": "21%" },
            { "mData": "JobTitleShortName_1", "sTitle": "Short Code (En)", "width": "21%" },
            { "mData": "JobTitleShortName_2", "sTitle": "Short Code (Ar)", "width": "21%" },
             { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "12%" },
        ],

        "processing": true,
        "serverSide": false,
        "ajax": "/Job/GetJobTitleList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {            
                        
        }
    });
}

function EditChannel(id) {
    var buttonName = id > -1 ? "Update" : "Submit";
    $("#modal_Loader").load("/Job/AddEditJob/" + id, function () {
        $("#myModal").modal("show");

        if (id > -1) {
            $("#modal_heading").text('Edit Job Title');
        }
        else {
            $("#modal_heading").text('Add Job Title');
        }
        //$("#form0").find('input[type="submit"]').val(buttonName);
    });
}

function Sucess() {
    $("#myModal").modal("hide");
    ShowMessage("success", "Job title save successfully")
    getGrid();
}

function TerminateEmployee(id) {
    $("#modal_Loader").load("/Employee/ViewTerminateEmployee/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Terminate Employee');

}

function UpdateStatus(id, status) {
    //alert("In");
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { "Id": id, "status": status },
        url: '/Employee/UpdateStatus',
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
            getGrid();
        },
        error: function (data) { }
    });
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Job/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function AddEmployeeDependent() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Employee/AddEmployeeDependent");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Employee Dependent');
}

function AddEmployeeLeaveRequest() {
    var EmployeeId = $("#hdnEmployeeId").val();

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Employee/AddEmployeeLeaveRequest/" + EmployeeId);
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Employee Leave Request');
}

function ViewProfileChannel(id) {
    window.location.href = "/ViewEmployeeProfile/Index/" + id;
}

// View Profile 

function AddPastEmployment() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/pastemployment/index/" + EmployeeId;
}

function AddInternalEmployment() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/employee/InternalEmploymentList/" + EmployeeId;
}

function ChangePassword() {
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader").load("/Employee/ChangeEmployeePassword/" + EmployeeId);
    $("#myModal").modal("show");
    $("#modal_heading").text('Change Password');
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Job/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Job/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportEmployeeRecordToPDF(id) {

    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportEmployeeToPDF/" + id);
    }
    else ShowMessage("warning", "No data for export!");

}
//--------------------