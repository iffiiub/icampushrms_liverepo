﻿var isPositionUpdate = true;
$(document).ready(function () {

    if ($("#userGroupModel_Status").val() == 0) {
        $("#leftDate").removeClass('hidden');
        $("#leftReson").removeClass('hidden');
    }

    $('#btnSaveAll').prop('disabled', false);

    $.ajax({
        type: "GET",
        //contentType: "application/json; charset=utf-8",
        url: "/Employee/LoadBirthPlaceForCountry/" + $('#employeeDetailsModel_BirthCountryID').val(),
        dataType: "json",
        success: function (data) {
            data = $.map(data, function (item, a) {
                return "<option value=" + item.Value + ">" + item.Text + "</option>";
            });
            $("#employeeDetailsModel_BirthPlaceID").html(data.join("")); //converts array into the string
            $("#employeeDetailsModel_BirthPlaceID").val($("#hdnBirthPlaceID").val());
        },
    });


    $('.ColLanguage ul li label').each(function () {
        var lbl = $(this).text().trim();
        if (lbl.toLowerCase() == 'native') {
            $(this).find('input[type="checkbox"]').addClass('lngNative');
        }
    })
    $('.ColLanguage ul li input[type="checkbox"]').click(function () {
        var lbl = $(this).parent().text().trim();


        if (lbl.toLowerCase() == 'native') {
            if ($(this).is(':checked')) {
                $('.lngNative').prop('checked', false);
                $(this).prop('checked', true);
            }
        }

    });


    $(document).on("click", ".clickme1", function () {
        var pathname = window.location.pathname;
        var last = pathname.substring(pathname.lastIndexOf("/") + 1, pathname.length);
        var id = $(this).attr('id');
        if (last != "0") {
            $.MessageBox({
                buttonDone: "Yes",
                buttonFail: "No",
                message: "Do you want to switch Employee?<br> Note:- Save data before switch to avoid data lost"
            }).done(function () {
                window.location.href = '/Employee/index/' + id;
            }).fail(function () {
            });
        }
        else {
            var id = $(this).attr('id');
            window.location.href = '/Employee/index/' + id;
        }
    });

    $(document).on("click", ".emplistcontent", function () {
        var panelName = [];
        var pathname = window.location.pathname;
        var last = pathname.substring(pathname.lastIndexOf("/") + 1, pathname.length);
        var id = $(this).find('.clickme').attr('id');
        if (last != "0") {
            $.MessageBox({
                buttonDone: "Yes",
                buttonFail: "No",
                message: "Do you want to switch Employee?<br> Note:- Save data before switch to avoid data lost"
            }).done(function () {
                window.location.href = '/Employee/index/' + id;
            }).fail(function () {
            });
        }
        else {
            var id = $(this).find('.clickme').attr('id');
            window.location.href = '/Employee/index/' + id;
        }
    });

    $("#employeePersonalIdentityInfoModel_PassportNumber").on('input', function () {
        var passportnum = $("#employeePersonalIdentityInfoModel_PassportNumber").val();
        if (passportnum != "") {
            $(this).siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $(this).siblings("span.field-validation-error:first").removeClass('hidden');
        }
    });

    $("#employeePersonalIdentityInfoModel_PassportIssueDate").on('input', function () {
        var passportissuedate = $("#employeePersonalIdentityInfoModel_PassportIssueDate").val();
        if (passportissuedate == "" && $("#employeePersonalIdentityInfoModel_PassportExpiryDate").val() == "") {
            $("#employeePersonalIdentityInfoModel_PassportNumber").siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $("#employeePersonalIdentityInfoModel_PassportNumber").siblings("span.field-validation-error:first").removeClass('hidden');
        }
    });

    $("#employeePersonalIdentityInfoModel_PassportExpiryDate").on('input', function () {
        var passportExpdate = $("#employeePersonalIdentityInfoModel_PassportExpiryDate").val();
        if (passportExpdate == "" && $("#employeePersonalIdentityInfoModel_PassportIssueDate").val() == "") {
            $("#employeePersonalIdentityInfoModel_PassportNumber").siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $("#employeePersonalIdentityInfoModel_PassportNumber").siblings("span.field-validation-error:first").removeClass('hidden');
        }
    });

    $("#employeePersonalIdentityInfoModel_VisaNumber").on('input', function () {
        var visanum = $("#employeePersonalIdentityInfoModel_VisaNumber").val();
        if (visanum != "") {
            $(this).siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $(this).siblings("span.field-validation-error:first").removeClass('hidden');
        }
    });

    $("#employeePersonalIdentityInfoModel_VisaIssueDate").on('input', function () {

        var passportissuedate = $("#employeePersonalIdentityInfoModel_VisaIssueDate").val();
        if (passportissuedate == "" && $("#employeePersonalIdentityInfoModel_VisaExpirationDate").val() == "") {
            $("#employeePersonalIdentityInfoModel_VisaNumber").siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $("#employeePersonalIdentityInfoModel_VisaNumber").siblings("span.field-validation-error:first").removeClass('hidden');
        }

    });

    $("#employeePersonalIdentityInfoModel_VisaExpirationDate").on('input', function () {
        var passportExpdate = $("#employeePersonalIdentityInfoModel_VisaExpirationDate").val();
        if (passportExpdate == "" && $("#employeePersonalIdentityInfoModel_VisaIssueDate").val() == "") {
            $("#employeePersonalIdentityInfoModel_VisaNumber").siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $("#employeePersonalIdentityInfoModel_VisaNumber").siblings("span.field-validation-error:first").removeClass('hidden');
        }
    });

    $.each($('a[data-PermissionValid="true"]'), function (ind, item) {
        var url = $(item).attr("data-url");
        $.ajax({
            url: "/Home/CheckUrlPermission?url=" + url + "&mode=Module",
            async: false,
            success: function (data) {
                if (!data) {
                    $(item).attr("data-PermissionValid", "false");
                    $(item).addClass("cursor-disabled");
                    $(item).parent().addClass("cursor-disabled");
                }
            },
            error: function (data) { }
        });
    });

    $("#userGroupModel_Status").change(function () {
        var statusId = $("#userGroupModel_Status").val();
        if (statusId == 1) {
            $("#leftDate").addClass('hidden');
            $("#leftReson").addClass('hidden');
            $("#userGroupModel_LeftDate").siblings("span.field-validation-error:first").addClass('hidden');
            $("#userGroupModel_LeftReasonID").siblings("span.field-validation-error:first").addClass('hidden');
        }
        else {
            $("#leftDate").removeClass('hidden');
            $("#leftReson").removeClass('hidden');
            $("#userGroupModel_LeftDate").siblings("span.field-validation-error:first").removeClass('hidden');
            $("#userGroupModel_LeftReasonID").siblings("span.field-validation-error:first").removeClass('hidden');
        }
    });

    $("#employmentInformation_DepartmentID").change(function () {
        LoadEmployeeSectionByDepartment();
    });

    $("#employmentInformation_PositionID").change(function () {
        UpdateEmployeeAccrualLeaves();
    })   
});

$("#employeeDetailsModel_EmployeeAlternativeID").on('keypress',function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        ShowMessage("error", "Please enter only numeric.");
        return false;
    }
});

$("#employeeDetailsModel_EmployeeAlternativeID").on('paste', function (e) {
    if (isNaN(parseInt(e.originalEvent.clipboardData.getData('Text')))) {
        ShowMessage("error", "Please enter only numeric.");
        return false;
    }
});

function ColNationalityClick(e) {
    var chk = $(e).find("input[type='checkbox']");
    var value = chk.val();
    var txt = chk.parent('label').text().trim();
    if (chk.is(":checked")) {
        $("#employeeDetailsModel_DefaultNationalityID").append($('<option></option>').val(value).html(txt));
        //if selected remove the error
        var field = $(".ColNationality .field-validation-error");
        field.attr("class", "field-validation-valid");
        field.html("");
    }
    else
        $("#employeeDetailsModel_DefaultNationalityID option[value='" + value + "']").remove();
    RefreshSelect("#employeeDetailsModel_DefaultNationalityID");
}
function refershcoutry() {
    $(".dropNationality").multiselect('rebuild');
}

function refershLanguage() {
    $(".dropSpokenLanguage").multiselect('rebuild');
}

// Add States
function AddBirthPlace(title, id, dropdownId) {
    if ($('#employeeDetailsModel_BirthCountryID').val() != undefined && $('#employeeDetailsModel_BirthCountryID').val() != "" && $('#employeeDetailsModel_BirthCountryID').val() != "0") {
        $("#hdnDropDownReloadId").val(dropdownId);
        $("#hdnDropDownId").val(id);
        $("#modal_Loader").load("/Employee/AddBirthPlace/" + $('#employeeDetailsModel_BirthCountryID').val());
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
    }
    else {
        $.MessageBox({
            buttonDone: "Ok",
            message: 'Please select BirthCountry First',
        });
        return;
    }
}

function Add(title, id, dropdownId, obj) {
    var checkType = false;
    if (obj == undefined) {
        checkType = true;
    } else {
        if ($(obj).attr("data-PermissionValid") == "true")
            checkType = true;
        else
            checkType = false;
    }
    if (checkType) {
        $("div.tooltip").hide();
        $("#hdnDropDownReloadId").val(dropdownId);
        $("#hdnDropDownId").val(id);
        $("#modal_Loader").load("/Employee/AddType/" + id);
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
    }
}

function BirthCountryIDChange(object) {
    var selectedItem = $(object).val();
    var ddlStates = $("#employeeDetailsModel_BirthPlaceID");
    var statesProgress = $("#states-loading-progress");
    statesProgress.show();
    $.ajax({
        cache: false,
        type: "GET",
        url: "/Employee/GetBirthPlaceByCountryId",
        data: { "countryId": selectedItem },
        success: function (data) {
            ddlStates.html('');
            ddlStates.append($('<option></option>').val("0").html("Select Birth Place"));
            $.each(data, function (id, option) {
                ddlStates.append($('<option></option>').val(option.id).html(option.name));
            });
            statesProgress.hide();
            RefreshSelect("#employeeDetailsModel_BirthPlaceID");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //  alert('Failed to retrieve states.');
            statesProgress.hide();
        }
    });
}

//for add nationality dropdown
function LoadAddedNationality() {
    $jq12.MessageBox({
        buttonDone: "OK",
        buttonFail: "Cancel",
        message: "Insert nationality Name",
        input: true,
        speed: 100
    }).done(function (data) {

        $.ajax({
            cache: false,
            type: "GET",
            url: "/Employee/AddDropNationalities",
            data: { "text": data },

            success: function (data) {

                var parsed = $.parseJSON(data);
                $(".dropNationality").append("<option value='" + parsed[0].Value + "'>" + parsed[0].Text + "</option>");
                $(".dropNationality").parents('.ColNationality').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + parsed[0].Value + '>' + parsed[0].Text + '</label></a></li>');

                refershcoutry();

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: xhr.status + ". " + xhr.responseText + ". " + thrownError,
                });
            }
        });

    }).fail(function () {

    });
}

function LoadAddedLanguage() {
    $jq12.MessageBox({
        buttonDone: "OK",
        buttonFail: "Cancel",
        message: "Insert Language ",
        input: true,
        speed: 100
    }).done(function (data) {

        $.ajax({
            cache: false,
            type: "GET",
            url: "/Employee/AddDropLanguage",
            data: { "text": data },

            success: function (data) {

                var parsed = $.parseJSON(data);
                //$(".dropNationality").append("<option value='" + parsed[0].Value + "'>" + parsed[0].Text + "</option>");
                $(".dropSpokenLanguage").append("<optgroup label='" + parsed[0].Text + "'>" +
            "<option value='Speak-" + parsed[0].Value + "'>Speak</option>" +
            "<option value='Read-" + parsed[0].Value + "'>Read</option>" +
            "<option value='Write-" + parsed[0].Value + "'>Write</option>" +
            "<option value='Native-" + parsed[0].Value + "'>Native</option>" +
            "</optgroup>");

                $(".dropSpokenLanguage").parents('.ColLanguage').find('ul').append("<li class='multiselect-item multiselect-group'><label>" + parsed[0].Text + "</label></li>" +
                    "<li><a tabindex='0'><label class='checkbox'><input type='checkbox' value='Speak-" + parsed[0].Value + "'>Speak</label></a></li>" +
                "<li><a tabindex='0'><label class='checkbox'><input type='checkbox' value='Read-" + parsed[0].Value + "'> Read</label></a></li>" +
                "<li><a tabindex='0'><label class='checkbox'><input type='checkbox' value='Write-" + parsed[0].Value + "'> Write</label></a></li>" +
                "<li><a tabindex='0'><label class='checkbox'><input type='checkbox' value='Native-" + parsed[0].Value + "'> Native</label></a></li>");

                // $(".dropSpokenLanguage").parents('.ColLanguage').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + parsed[0].Value + '>' + parsed[0].Text + '</label></a></li>');
                refershLanguage();

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: xhr.status + ". " + xhr.responseText + ". " + thrownError,
                });
            }
        });



    }).fail(function () {

    });
}

function Saveall() {
    $(".panel-body").addClass("panel-body-open");
    $('.accordion .panel-body').css("display", "block");
    // $("#employeePersonalIdentityInfoModel_PassportNumber").siblings("span.field-validation-error:first").removeClass('hidden');
    // $("#employeePersonalIdentityInfoModel_VisaNumber").siblings("span.field-validation-error:first").removeClass('hidden');
    var panaelName = [];
    var HeaderName = [];

    var isPersonalValid = 0;
    var $form = $('form#FrmPersonalDEtails');
    if ($form.valid()) {
        //Ajax call here
        isPersonalValid = 1;
    }

    var arrayForLanguages = [];

    var brands = $('.dropSpokenLanguage optgroup');
    $(brands).each(function (index, brand) {
        var getRuffval = $(this).find('option').eq(0).val();
        var res = getRuffval.split("-");
        var data = res[1];

        arrayForLanguages.push({
            "LanguageId": data,
            "IsSpeak": $('.dropSpokenLanguage option[value=Speak-' + data + ']').prop('selected') == true ? true : false,
            "IsRead": $('.dropSpokenLanguage option[value=Read-' + data + ']').prop('selected') == true ? true : false,
            "IsWrite": $('.dropSpokenLanguage option[value=Write-' + data + ']').prop('selected') == true ? true : false,
            "IsNative": $('.dropSpokenLanguage option[value=Native-' + data + ']').prop('selected') == true ? true : false
        });
    });

    $.ajax({
        url: '/Employee/SaveSelectedLanguages',
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        processData: false,
        data: JSON.stringify(arrayForLanguages),
        async: false,
        success: function (data) { }
    });

    var errorCount = 0;
    //Nationality
    var nationalityIds = '';
    var nationalityIdsArray = [];
    var nations = $('.dropNationality option');
    $(nations).each(function (index, brand) {
        var data = $(this).eq(0).val();

        if ($('.dropNationality option[value=' + data + ']').prop('selected'))
            nationalityIdsArray.push(data);
    });

    nationalityIds = nationalityIdsArray.join(',');

    if (nationalityIds != "") {
        $.ajax({
            cache: false,
            url: '/Employee/SaveSelectedNationalities',
            data: { "nationalityIds": nationalityIds },
            type: 'GET',
            async: false,
            success: function (result) { },
            error: function (msg) { }
        });

        var field = $(".ColNationality .field-validation-error");
        field.attr("class", "field-validation-valid");
        field.html("");
    }
    else {
        errorCount++;
        var field = $(".ColNationality .field-validation-valid");
        field.attr("class", "field-validation-error");
        field.html("<span for='NationalityID'>Please select nationality</span>");
    }

    if ($("#employeeDetailsModel_DefaultNationalityID").val() == 0) {
        errorCount++;
        var field = $("#employeeDetailsModel_DefaultNationalityID").parent().next(".field-validation-valid");//span
        field.attr("class", "field-validation-error padding-left-item");
        field.html("<span for='employeeDetailsModel_DefaultNationalityID'>Please select default nationality</span>");
    }
    if ($("#employmentInformation_CompanyId").val() == 0) {
        errorCount++;
        var field = $("#employmentInformation_CompanyId").parent().next(".field-validation-valid");//span
        field.attr("class", "field-validation-error padding-left-item");
        field.html("<span for='employmentInformation_CompanyId'>Please select Organization</span>");
    }

    if ($("#employeeDetailsModel_NationalityIDCardNo").val() != "") {
        var idCardVal = $("#employeeDetailsModel_NationalityIDCardNo").val()
        var EmpId = $("#EmployeeId").val();

        CheckUniqueNationalityIdCardNo(EmpId, idCardVal, errorCount, function (errCtr) {
            CheckEmployeeValidation(isPersonalValid, errCtr);

        });
    }
    else {
        CheckEmployeeValidation(isPersonalValid, errorCount);
    }
}

function Begin() {
    pageLoaderFrame();
}

function AddMoreIMContact() {
    $("#myModal").modal("show");
    $("#modal_heading").text("Add IM Contact");
    $("#modal_Loader").load("/Employee/AddIMContact");
}

function AddMoreFields(type) {
    var dynamicFields = "";
    $.ajax({
        type: "GET",
        url: "/Employee/LoadFieldsForEmailIMPhone?type=" + type,
        success: function (data) {
            dynamicFields = data;

            if (type == "Email") {
                $('.dynamicEmailFields').append(dynamicFields);
            }
            else if (type == "IM") {
                $('.dynamicIMFields').append(dynamicFields);
            }
            else if (type == "Mobile") {
                $('.dynamicMobileFields').append(dynamicFields);
            }
            else if (type == "PersonalEmail") {
                $('.dynamicPersonalEmailFields').append(dynamicFields);
            }
            else if (type == "Phone") {
                $('.dynamicPhoneFields').append(dynamicFields);
            }
        },
    });
}

function RemoveEmployeeContact(EmpContactId, FieldType) {
    var EmpId = $("#hdnEmployeeId").val();
    alert(EmpId);
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: { EmpId: EmpId, id: EmpContactId, fieldType: FieldType },
        url: "/Employee/deleteMultipleDetails",
        success: function (data) {
        }
    });
}

function CountryIDchange(object) {
    var selectedItem = $(object).val();
    var ddlStates = $("#employeeAddressInfoModel_StateID");
    var statesProgress = $("#states-loading-progress");
    statesProgress.show();
    if (selectedItem != "" && selectedItem != null) {
        $.ajax({
            cache: false,
            type: "GET",
            url: "/Employee/GetStatesByCountryId",
            data: { "countryId": selectedItem },
            success: function (data) {
                ddlStates.html('');
                ddlStates.append($('<option></option>').val("0").html("Select State"));
                $.each(data, function (id, option) {
                    ddlStates.append($('<option></option>').val(option.id).html(option.name));
                });
                RefreshSelect("#employeeAddressInfoModel_StateID");
                statesProgress.hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert('Failed to retrieve states.');
                statesProgress.hide();
            }
        });
    }
    else {
        ddlStates.html('');
        ddlStates.append($('<option></option>').val("0").html("Select State"));
        statesProgress.hide();
    }
}

function StateIDchange(object) {
    // Set value for Hidden Field
    $('#hdnStateId').val($(object).val());

    var selectedItem = $(object).val();
    var ddlCity = $("#employeeAddressInfoModel_CityID");
    var statesProgress = $("#states-loading-progress");
    statesProgress.show();
    if (selectedItem != "" && selectedItem != null) {
        $.ajax({
            cache: false,
            type: "GET",
            url: "/Employee/GetCityByStateId",
            data: { "stateId": selectedItem },
            success: function (data) {
                ddlCity.html('');
                ddlCity.append($('<option></option>').val("0").html("Select City"));
                $.each(data, function (id, option) {
                    ddlCity.append($('<option></option>').val(option.id).html(option.name));
                });
                RefreshSelect("#employeeAddressInfoModel_CityID");
                statesProgress.hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //   alert('Failed to retrieve cities.');
                statesProgress.hide();
            }
        });
    }
    else {
        ddlCity.html('');
        ddlCity.append($('<option></option>').val("0").html("Select City"));
        statesProgress.hide();
    }
}

function CityIDchange(object) {
    // Set value for Hidden Field
    $('#hdnCityId').val($(object).val());

    var selectedItem = $(object).val();
    var ddlArea = $("#employeeAddressInfoModel_AreaId");
    var statesProgress = $("#states-loading-progress");
    statesProgress.show();
    if (selectedItem != "" && selectedItem != null) {
        $.ajax({
            cache: false,
            type: "GET",
            url: "/Employee/GetAreaByCityId",
            data: { "cityId": selectedItem },
            success: function (data) {
                ddlArea.html('');
                ddlArea.append($('<option></option>').val("0").html("Select Area"));
                $.each(data, function (id, option) {
                    ddlArea.append($('<option></option>').val(option.id).html(option.name));
                });
                statesProgress.hide();
                RefreshSelect("#employeeAddressInfoModel_AreaId");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //    alert('Failed to retrieve areas.');
                statesProgress.hide();
            }
        });
    }
    else {
        ddlArea.html('');
        ddlArea.append($('<option></option>').val("0").html("Select Area"));
        statesProgress.hide();
    }
}

function AreaIdchange(object) {
    // Set value for Hidden Field
    $('#hdnAreaId').val($(this).val());
}

// Add Area
function AddArea(title, id, dropdownId) {

    if ($('#employeeAddressInfoModel_CityID').val() != undefined && $('#employeeAddressInfoModel_CityID').val() != "" && $('#employeeAddressInfoModel_CityID').val() != "0") {
        $("#hdnDropDownReloadId").val(dropdownId);
        $("#hdnDropDownId").val(id);
        $("#modal_Loader").load("/Employee/AddArea/" + $('#employeeAddressInfoModel_CityID').val());
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
    }
    else {
        $.MessageBox({
            buttonDone: "Ok",
            message: 'Please select City First',
        });
        return;
    }
}

// Add States
function AddState(title, id, dropdownId) {

    if ($('#employeeAddressInfoModel_CountryID').val() != undefined && $('#employeeAddressInfoModel_CountryID').val() != "" && $('#employeeAddressInfoModel_CountryID').val() != "0") {
        $("#hdnDropDownReloadId").val(dropdownId);
        $("#hdnDropDownId").val(id);
        $("#modal_Loader").load("/Employee/AddState/" + $('#employeeAddressInfoModel_CountryID').val());
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
    }
    else {
        $.MessageBox({
            buttonDone: "Ok",
            message: 'Please select Country First',
        });
        return;
    }
}

// Add States
function AddCity(title, id, dropdownId) {

    if ($('#employeeAddressInfoModel_StateID').val() != undefined && $('#employeeAddressInfoModel_StateID').val() != "" && $('#employeeAddressInfoModel_StateID').val() != "0") {
        $("#hdnDropDownReloadId").val(dropdownId);
        $("#hdnDropDownId").val(id);
        $("#modal_Loader").load("/Employee/AddCity/?ID=" + $('#employeeAddressInfoModel_StateID').val() + "&CountryID=" + $('#employeeAddressInfoModel_CountryID').val());
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
    }
    else {
        $.MessageBox({
            buttonDone: "Ok",
            message: 'Please select State First',
        });
        return;
    }
}

function SucessEmployee(arg) {

    hideLoaderFrame();
    $('#btnSaveAll').prop('disabled', false);
    ShowMessage(arg.result, arg.resultMessage);
    if (arg.result == 'success') {
        setTimeout(function () {
            var EmployeeID = $("#EmployeeId").val();
            if (parseInt(EmployeeID) > 0) {
                var isAccrual = localStorage.getItem('isAccrualPosition');
                var isApproved = localStorage.getItem('isApproved');
                var linkedAccrualLeave = localStorage.getItem('LinkedAccrualLeave');
                var EmployeeChangePosition = localStorage.getItem('ChangedEmployeePosition');
                if (isApproved) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: { EmployeeId: EmployeeID, VacTypeId: linkedAccrualLeave, PositionId: EmployeeChangePosition, isPositionAccrual: isAccrual },
                        url: "/VacationType/UpdateEmployeeLeaveType",
                        success: function (data) {
                            ShowMessage(data.CssClass, data.Message);
                        }
                    });
                }
            }
            window.location = "/Employee/Index/" + arg.EmployeeID;
        }, 2000)
    }
}

function BackToList() {
    window.location.href = '/Employee/ListEmployee/';
}

function LoadEmployeeSectionByDepartment() {
    var DeptId = $("#employmentInformation_DepartmentID").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: { DeptId: DeptId },
        url: "/Employee/GetEmployeeSectionAsPerDepartment",
        success: function (data) {
            //    alert(JSON.stringify(data));
            data = $.map(data, function (item, a) {
                return "<option value=" + item.Value + ">" + item.Text + "</option>";
            });
            $("#employmentInformation_EmployeeSectionID").html(data.join("")); //converts array into the string
            RefreshSelect("#employmentInformation_EmployeeSectionID");
        }
    });
}

function CheckUniqueNationalityIdCardNo(EployeeId, IdCardNo, errorCount, callback) {
    $.ajax({
        type: "GET",
        data: { EmployeeId: EployeeId, NataionalityIdCardNo: IdCardNo },
        url: "/Employee/CheckUniqueNationalityId",
        success: function (data) {
            if (data.isSuccess == false) {
                errorCount++;
                try {
                    $("#employeeDetailsModel_NationalityIDCardNo").siblings("span.field-validation-error:first").removeClass('hidden');
                } catch (e) { }
                var field = $("#employeeDetailsModel_NationalityIDCardNo").next(".field-validation-valid");//span
                field.attr("class", "field-validation-error");
                field.html("<span for='#employeeDetailsModel_NationalityIDCardNo'>National ID card no. (" + IdCardNo + ") is already assign to another employee. Please add unique Id card number.</span>");
            } else {
                try {
                    var field = $("#employeeDetailsModel_NationalityIDCardNo").next(".field-validation-error");
                    field.attr("class", "field-validation-valid");
                    field.html("");
                } catch (e) { }
            }
            callback(errorCount);
        },
    });

}

function CheckEmployeeValidation(isPersonalValid, errCtr) {
    if ($("#employeePersonalIdentityInfoModel_PassportNumber").val() == "" && ($("#employeePersonalIdentityInfoModel_PassportIssueDate").val() != "" || $("#employeePersonalIdentityInfoModel_PassportExpiryDate").val() != "")) {
        errCtr++;
        try {
            $("#employeePersonalIdentityInfoModel_PassportNumber").siblings("span.field-validation-error:first").removeClass('hidden');
        } catch (e) { }
        var field = $("#employeePersonalIdentityInfoModel_PassportNumber").next(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='employeePersonalIdentityInfoModel_PassportNumber'>Please enter Passport Number</span>");
    }

    if ($("#employeePersonalIdentityInfoModel_VisaNumber").val() == "" && ($("#employeePersonalIdentityInfoModel_VisaIssueDate").val() != "" || $("#employeePersonalIdentityInfoModel_VisaExpirationDate").val() != "")) {
        errCtr++;
        try {
            $("#employeePersonalIdentityInfoModel_VisaNumber").siblings("span.field-validation-error:first").removeClass('hidden');
        } catch (e) { }
        var field = $("#employeePersonalIdentityInfoModel_VisaNumber").next(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='employeePersonalIdentityInfoModel_VisaNumber'>Please enter VISA Number</span>");
    }

    CheckUni_BanAccountNumber(errCtr, function (errCtrReturn) {
        if (isPersonalValid == 0 || errCtrReturn > 0) {
            ShowMessage("error", "Please fill mandatory fields");
        }
        else {
            var EmployeeID = $("#employeeDetailsModel_EmployeeAlternativeID").val();
            var EmployeeIDOld = $("#hdnEmpAlternateID").val();           
            if (EmployeeID != "") {
                if (EmployeeID.trim() != '0') {
                    if (EmployeeIDOld != EmployeeID) {
                        $.ajax({
                            type: 'POST',
                            data: { EmployeeID: EmployeeID },
                            url: '/Employee/IsExistsEmployeeID',
                            success: function (data) {
                                if (data > 0) {
                                    ShowMessage("error", "Employee ID is already preserve with another employee, Please enter unique Employee ID.");
                                    return false;
                                }
                                else {
                                    $('#btnSaveAll').prop('disabled', true);
                                    SubmitEmployeeForm();
                                }
                            },
                            error: function (data) { }
                        });
                    }
                    else {
                        $('#btnSaveAll').prop('disabled', true);
                        SubmitEmployeeForm();
                    }
                }
                else
                {
                    ShowMessage("error", "Employee ID can't be Zero");
                }
            }
            else {
                $('#btnSaveAll').prop('disabled', true);
                SubmitEmployeeForm();
            }
        }
    });
}

function CheckUni_BanAccountNumber(errCtr, CallBack) {
    if ($("#payDirectDepositModel_UniAccountNumber").val().trim().length > 0 && $("#hdnisBankInformationPresent").val() != "False") {
        $.ajax({
            type: 'GET',
            url: encodeURI('/Employee/CheckUni_BanAccountNo?DirectDepositID=' + $("#payDirectDepositModel_DirectDepositID").val() + '&EmployeeeId=' + $("#EmployeeId").val() + "&UniAccountNumber=" + $("#payDirectDepositModel_UniAccountNumber").val()),
            dataType: "json",
            success: function (data) {
                if (data) {
                    errCtr++;
                    var field;
                    $("#payDirectDepositModel_UniAccountNumber").siblings("span.field-validation-error:first").removeClass('hidden');
                    if ($("#payDirectDepositModel_UniAccountNumber").next(".field-validation-valid").length > 0) {
                        field = $("#payDirectDepositModel_UniAccountNumber").next(".field-validation-valid");//span
                    } else {
                        field = $("#payDirectDepositModel_UniAccountNumber").next(".field-validation-error");
                    }
                    field.attr("class", "field-validation-error");
                    field.html("<span for='UniAccountNumber'>UNI/IBAN Number already exists.</span>");
                } else {
                    if ($("#payDirectDepositModel_UniAccountNumber").val() != "") {
                        var IBanNumber = $("#payDirectDepositModel_UniAccountNumber").val();
                        var IBanLength = $("#IBanLength").val();
                        var InputLength = IBanNumber.length;
                        if (IBanLength != 0) {
                            if (InputLength != IBanLength) {
                                errCtr++;
                                try {
                                    $("#payDirectDepositModel_UniAccountNumber").siblings("span.field-validation-error:first").removeClass('hidden');
                                } catch (e) { }
                                var field;
                                $("#payDirectDepositModel_UniAccountNumber").siblings("span.field-validation-error:first").removeClass('hidden');
                                if ($("#payDirectDepositModel_UniAccountNumber").next(".field-validation-valid").length > 0) {
                                    field = $("#payDirectDepositModel_UniAccountNumber").next(".field-validation-valid");//span
                                } else {
                                    field = $("#payDirectDepositModel_UniAccountNumber").next(".field-validation-error");
                                }
                                field.attr("class", "field-validation-error");
                                field.html("<span for='payDirectDepositModel_UniAccountNumber'>UNI/IBAN Number must be " + IBanLength + " digits</span>");
                            }
                        }
                    }
                }
                CallBack(errCtr);
            }
        });
    }
    else {
        if ($("#hdnisBankInformationPresent").val() != "False")
            $("#payDirectDepositModel_UniAccountNumber").val("");
        CallBack(errCtr);
    }
}

function SubmitEmployeeForm() {
    $("#FrmPersonalDEtails").submit();
}

function UpdateEmployeeAccrualLeaves() {
    localStorage.setItem('isAccrualPosition', false);
    localStorage.setItem('isApproved', false);
    localStorage.setItem('LinkedAccrualLeave', -1);
    localStorage.setItem('ChangedEmployeePosition', -100);
    var EmployeeID = $("#EmployeeId").val();
    if (EmployeeID > 0) {
        $.ajax({
            type: 'GET',
            url: '/VacationType/CheckAccrualAvailable?EmployeeId=' + EmployeeID,
            success: function (data) {
                if (data.result == true) {
                    isAccrualPosition = false;
                    var EmpPosId = $("#hdnEmpPositionID").val();
                    var ChangePosID = $("#employmentInformation_PositionID").val();
                    if (EmpPosId != ChangePosID) {
                        $.MessageBox({
                            buttonDone: "Yes",
                            buttonFail: "No",
                            message: "Do you want to carry forward accrual balance for last position?."
                        }).done(function () {
                            var isAccrualPosition = true;
                            $.ajax({
                                type: 'POST',
                                url: '/VacationType/CheckPositionHavingAccrualLeave',
                                data: { PositionId: ChangePosID },
                                success: function (data) {
                                    if (data.result != true) {
                                        isAccrualPosition = false;
                                    }
                                    else {
                                        isAccrualPosition = true;
                                    }
                                    $("#modal_Loader3").html("");
                                    $("#modal_Loader3").load("/VacationType/UpdateAccrualLeaveTypeForPosition?PositionId=" + ChangePosID + "&isAccrualPosition=" + isAccrualPosition, function () {
                                        $("#myModal3").modal("show");
                                        $("#modal_heading3").text('Accrual Leave Type');
                                        $(".modal-dialog").attr("style", "width:700px;");
                                        bindSelectpicker('.selectpickerddl');
                                    });
                                },
                                error: function (data) { }
                            });
                        }).fail(function () {
                        });
                    }

                }

            },
            error: function (data) { }
        });


    }

}

$("#ddlContractStatus").on("change", function () {
    if ($("#ddlContractStatus").val().trim() == "Family")
    {
        $("#divContractStatus").removeClass("hidden");
        $("#ddlFamilySpouse").val(null);
        RefreshSelect("#ddlFamilySpouse");
    }       
    else {
        $("#divContractStatus").addClass("hidden");
        $("#ddlFamilySpouse").val(null);
        RefreshSelect("#ddlFamilySpouse");
    }       
});