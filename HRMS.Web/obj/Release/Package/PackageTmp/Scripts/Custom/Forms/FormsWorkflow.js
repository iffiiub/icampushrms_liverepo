﻿var priorityList;
var workflowList;
var workflowGroupList;
var workflowForms;
//list for saving data
var newworkflowList = [];
var validateData = [];
var AllEditMode = 0;
var $tr;
var oTableChannel;
var isDataIsRefresh = true;

$(document).ready(function () {
    GetData(1);
    $("#btnSearch").click(function () {
        GetData(1);
    });
    $("#ddlCompany").change(function () {
        GetData(1);
    });
});

function GetData(id) {
    var companyId = $("#ddlCompany").val();
    var formid = $("#ddlForm").val();
    if (companyId > 0) {
        if (id == 1) {
            pageLoaderFrame();
        }
        if (formid == 0) {
            formid = null;
        }
        oTableChannel = $('#tblWorkflowList').dataTable({
            "sAjaxSource": "/FormsWorkflow/GetAllFormsWorkflow",
            "aoColumns": [
                { "mData": "FormID", "bVisible": true, "sClass": "hidden" },
                { "mData": "FormName", "sTitle": "Form Name", 'width': '10%' },
                { "mData": "GroupID", "bVisible": true, "sClass": "hidden" },
                { "mData": "CompanyTypeID", "bVisible": true, "sClass": "hidden" },
                { "mData": "GroupName", "sTitle": "Group Name", 'width': '10%' },
                { "mData": "PriorityListID", "bVisible": true, "sClass": "hidden" },
                { "mData": "PriorityListName", "sTitle": "Priority", 'width': '10%' },
                { "mData": "FinalApprovalNotification", "sTitle": "Final Notification", 'width': '10%' },
                { "mData": "BaseGroupID", "bVisible": true, "sClass": "hidden" },
                { "mData": "BaseGroupName", "sTitle": "Base Group", 'width': '10%' },
                { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '10%', "sClass": "text-center" },
                { "mData": "WorkFlowID", "bVisible": true, "sClass": "hidden" },
                { "mData": "SchoolBasedGroup", "bVisible": true, "sClass": "hidden" }
            ],
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "companyId", "value": companyId }, { "name": "formID", "value": formid });
            },
            "processing": false,
            "serverSide": false,
            "ajax": "/FormsWorkflow/GetAllFormsWorkflow",
            "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
            "iDisplayLength": 10,
            "bDestroy": true,
            "bFilter": true,
            "bInfo": true,
            "bSortCellsTop": false,
            'columnDefs': [{
                                "targets": 7, // notification checkbox
                                "className": "text-center"
                          }],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            },
            "fnDrawCallback": function () {
                $('#tblWorkflowList .selectpickerddl').selectpicker();
                if (AllEditMode == 1) {                   
                    DispalyInEditMode();
                }
            },
            "bStateSave": true,           
            "fnInitComplete": function (oSettings) {
                hideLoaderFrame();
            }
        });
    }
    else {
        ShowMessage("error", "Select a company type.");
    }
}

function EditAll() {
    AllEditMode = 1;
    pageLoaderFrame();
    setTimeout(DispalyInEditMode(), 1000);    
}

function RefreshAll() {
    validateData = [];
    AllEditMode = 0;
    pageLoaderFrame();
    setTimeout(GetData(0), 500);
}

function DispalyInEditMode() {
    $tr = $('#tblWorkflowList tr ');
    $($tr).each(function (i, item) {
        var workflowid = $(item).find("td").eq(11).html();
        var prioritylistid = $(item).find("td").eq(5).html();
        var basegroupid = $(item).find("td").eq(8).html();
        var schoolbasedgroup = $(item).find("td").eq(12).html();
        var notification = false;
        $('#chk_' + workflowid + '').removeAttr("disabled");
        if ($('#chk_' + workflowid + '').is(':checked'))
            notification = true;
        else
            notification = false;
        $(item).find("td").eq(6).html("<select data-live-search='true' onchange='SetPriorityValue(this)'  class='form-control selectpickerddl ddlPriority' id='ddlPriority_" + workflowid + "' >" + $("#divPriority").text() + "  </select>");
        if (schoolbasedgroup === 'false')
            $(item).find("td").eq(9).html("<select data-live-search='true' onchange='SetBaseGroupValue(this)'  class='form-control selectpickerddl ddlBaseGroup' id='ddlBaseGroup_" + workflowid + "' >" + $("#divGroup").text() + "  </select>");

        $(item).find("td").eq(10).html('<div class="center-text-align"><button id="btnRowSave" class="tableButton"  onclick="UpdateWorkflow(' + workflowid + ',' + prioritylistid + ',' + notification + ',' + basegroupid + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + workflowid + ',' + prioritylistid + ',' + notification + ',' + basegroupid + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
        $(item).addClass("IsEdit");

        var ddlprty = $(item).find("#ddlPriority_" + workflowid);
        $(ddlprty).val(prioritylistid);
        var ddlbgrp = $(item).find("#ddlBaseGroup_" + workflowid);
        $(ddlbgrp).val(basegroupid);
        $('#tblWorkflowList .selectpickerddl').selectpicker();

    });
    hideLoaderFrame();
}

function SetPriorityValue(e) {    
    var closesetRow = $(e).closest('tr');
    var wfid = $(closesetRow).find('td:eq(11)').text();
    var frmid = $(closesetRow).find('td:eq(0)').text();
    var ctypeid = $(closesetRow).find('td:eq(3)').text();
    var grpid = $(closesetRow).find('td:eq(2)').text();
    var prtyid = $(e).val();    
    var oldprtyid = $(closesetRow).find('td:eq(5)').text();
    pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/FormsWorkflow/IsPendingApprovalExists',
        data: { groupID: grpid, formID: frmid, companyTypeID: ctypeid, PriorityListID: oldprtyid, UpdatedPriorityListID: prtyid },
        success: function (result) {
            hideLoaderFrame();
            if (result.InsertedRowId > 0) {
                if ((result.InsertedRowId > 1) && (prtyid != null || prtyid != undefined || prtyid != '0' || prtyid != '')) {
                    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "All the pending requests (" + result.InitiatorIdentity + ") will mark to complete and approval level will lock with previous priority. Do you want to proceed?" }).done(function () {
                        $.ajax({
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            data: { groupID: grpid, formID: frmid, companyTypeID: ctypeid, PriorityListID: oldprtyid, UpdatedPriorityListID: prtyid },
                            url: '/FormsWorkflow/MarkToCompletePendingApprovals',
                            success: function (res) {                               
                                if (res.InsertedRowId > 0) {
                                    ShowMessage("success", res.Message);
                                    GetData(1);
                                }
                                else {
                                    ShowMessage("error", res.Message);
                                }

                            },
                            error: function (res) {
                                ShowMessage("error", res.Message);
                            }
                        });
                    });
                }
                else {
                    ShowMessage('error', result.Message);
                }
                $(closesetRow).find("#ddlPriority_" + wfid).val(oldprtyid);
            }
            else {
                $(closesetRow).find("#ddlPriority_" + wfid).val(prtyid);
                if (prtyid === null || prtyid !== undefined || prtyid === '0' || prtyid === '') {
                    RemoveValidateData(wfid);
                }
                if (prtyid !== null && prtyid !== undefined && prtyid !== '0' && prtyid !== '') {
                    if (CheckPriorityAlreadyExists(wfid, frmid, ctypeid, prtyid)) {
                        ShowMessage("error", "Same priority already set for another group.");                       
                        $(closesetRow).find('td:eq(5)').text(oldprtyid);
                        $(closesetRow).find("#ddlPriority_" + wfid).val(oldprtyid);

                        $('#tblWorkflowList .selectpickerddl').selectpicker('refresh');                        
                        return;
                    }
                    else {
                        if (CheckPriorityExists(wfid, frmid, ctypeid, prtyid)) {
                            ShowMessage("error", "Same priority already set for another group.");
                            $(closesetRow).find('td:eq(5)').text(oldprtyid);
                            $(closesetRow).find("#ddlPriority_" + wfid).val(oldprtyid);
                            $('#tblWorkflowList .selectpickerddl').selectpicker('refresh');
                            return;
                        }                      
                    }
                }              
            }
            $('#tblWorkflowList .selectpickerddl').selectpicker('refresh');
            hideLoaderFrame();
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);           
        }
    });

}

function CheckPriorityExists(wfid, frmid, ctypeid, prtyid) {
    var isexists = false;
    $.each(validateData, function (key, val) {
        if (wfid !== val.WorkflowID && frmid == val.FormID && ctypeid == val.CompanyTypeID && prtyid == val.PriorityListID) {
            isexists = true;
            return false;
        }
    });

    validateData.push({
        WorkflowID: wfid,
        FormID: frmid,
        CompanyTypeID: ctypeid,
        PriorityListID: prtyid
    });
    if (isexists)
        return true;
    else
        return false;
}

function CheckPriorityAlreadyExists(wfid, frmid, ctypeid, prtyid) {
    var isexists = false;
    var table = $("#tblWorkflowList").DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {
        var workflowid = value.WorkflowID;
        var formid = value.FormID;
        var companytypeid = value.CompanyTypeID
        var prioritylistid = value.PriorityListID
        if (wfid !== workflowid && frmid == formid && companytypeid == ctypeid && prioritylistid == prtyid) {
            isexists = true;
            return false;
        }
    });
    if (isexists)
        return true;
    else
        return false;
}

function SetBaseGroupValue(e) {
    var closesetRow = $(e).closest('tr');
    $(closesetRow).find('td:eq(8)').text($(e).val());
}

function EditWorkflow(e, workflowid, prioritylistid, notification, basegroupid) {
    $tr = $(e).closest("tr");
    var closesetRow = $(e).closest('tr');
    var schoolbasedgroup = $(closesetRow).find("td").eq(12).html();
    $($tr).find("td").eq(6).html("<select data-live-search='true' onchange='SetPriorityValue(this)'  class='form-control selectpickerddl ddlPriority' id='ddlPriority_" + workflowid + "' >" + $("#divPriority").text() + "  </select>");
    if (notification == 1)
        $($tr).find("td").eq(7).html("<input data-toggle='tooltip' data-placement='top' data-original-title='Email notification on Final Approval'  type='checkbox' id=chk_" + workflowid + " checked >");
    else
        $($tr).find("td").eq(7).html("<input data-toggle='tooltip' data-placement='top' data-original-title='Email notification on Final Approval'  type='checkbox' id=chk_" + workflowid + ">");
    if (schoolbasedgroup === 'false')
        $($tr).find("td").eq(9).html("<select data-live-search='true' onchange='SetBaseGroupValue(this)'  class='form-control selectpickerddl ddlBaseGroup' id='ddlBaseGroup_" + workflowid + "' >" + $("#divGroup").text() + "  </select>");
    $($tr).find("td").eq(10).html('<div class="center-text-align"><button id="btnRowSave" class="tableButton"  onclick="UpdateWorkflow(' + workflowid + ',' + prioritylistid + ',' + notification + ',' + basegroupid + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + workflowid + ',' + prioritylistid + ',' + notification + ',' + basegroupid + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
    // formid,groupid,workflowid,prioritylistid,notification,basegroupid        
    $($tr).addClass("IsEdit");
    var ddlprty = $(closesetRow).find("#ddlPriority_" + workflowid);
    $(ddlprty).val(prioritylistid);
    var ddlbgrp = $(closesetRow).find("#ddlBaseGroup_" + workflowid);
    $(ddlbgrp).val(basegroupid);
    $('#tblWorkflowList .selectpickerddl').selectpicker();
}

function CancelNewRow(e, workflowid, prioritylistid, notification, basegroupid) {
    var closesetRow = $(e).closest('tr');
    $(closesetRow).find('td:eq(6)').html($("#ddlPriority_" + workflowid + " option[value='" + prioritylistid + "']").text());
    $(closesetRow).find('td:eq(9)').html($("#ddlBaseGroup_" + workflowid + " option[value='" + basegroupid + "']").text());
    $(closesetRow).find('td:eq(10)').html("<a class='btn btn-success btn-rounded btn-condensed btn-sm'   onclick='EditWorkflow(this," + workflowid + "," + prioritylistid + "," + notification + "," + basegroupid + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>");    
    $(closesetRow).find('#chk_' + workflowid + '').attr("disabled", true);
    RemoveValidateData(workflowid);  
}

function UpdateWorkflow(workflowid, prioritylistid, notification, basegroupid) {
    var table = $('#tblWorkflowList').DataTable();
    var upriorityid = $("#ddlPriority_" + workflowid).val();
    var ubasegroupid = $("#ddlBaseGroup_" + workflowid).val();
    var unotify = false;
    if (table.$('#chk_' + workflowid + '').is(':checked'))
        unotify = true;
    else
        unotify = false;
    pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { WorkflowID: workflowid, PriorityListID: upriorityid, FinalApprovalNotification: unotify, BaseGroupID: ubasegroupid },
        url: '/FormsWorkflow/UpdateFormsWorkflow',
        success: function (data) {
            ShowMessage(data.CssClass, data.Message);
            GetData(0);           
        }
    });
}

function ChangeStatus(e, workflowid, prioritylistid, notification, basegroupid) {
    pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { WorkflowID: workflowid, PriorityListID: null, FinalApprovalNotification: false, BaseGroupID: null },
        url: '/FormsWorkflow/UpdateFormsWorkflow',
        success: function (data) {
            ShowMessage(data.CssClass, data.Message);
            GetData(0);
            RemoveValidateData(workflowid);
        }
    });
}

function SaveAll(e) {
    AllEditMode = 0;
    var table = $("#tblWorkflowList").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var updatedData = [];
        $(UpdatedTr).each(function (i, itm) {
            var workflowid = $(itm).find('td:eq(11)').text();
            var priorityid = table.$("#ddlPriority_" + workflowid).val();
            var basegroupid = table.$("#ddlBaseGroup_" + workflowid).val();
            var unotify = false;
            if (table.$('#chk_' + workflowid).is(":checked"))
                unotify = true;
            else
                unotify = false;
            updatedData.push({
                WorkflowID: workflowid,
                PriorityListID: priorityid,
                FinalApprovalNotification: unotify,
                BaseGroupID: basegroupid
            });
        });       
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/FormsWorkflow/SaveFormsWorkflowDetails",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'formsWorkflowList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        GetData(0)
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            })
        });
    }
    else {
        ShowMessage("error", "Please edit at least one record.");
        hideLoaderFrame();
    }
}

function RemoveValidateData(workflowid) {
    validateData = $.grep(validateData, function (data, index) {
        return data.WorkflowID != workflowid;
    });
}
