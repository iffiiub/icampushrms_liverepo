﻿$(document).ready(function () {
    bindSelectpicker('.selectpickerddl');
    var reqstatusid = $("#hdnReqStatusID").val();

    if (reqstatusid == "3") {
        $("#EmployeeConfirmedDetails").removeAttr("disabled");
        $("#LineManagerComments").removeAttr("disabled");
    }

    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });

    $('.btnSave').click(function () {
        var formid = 43;
        var companyIds = $("#hdnCompanyID").val()
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/EmployeeInterimProbation/IsWorkFlowExists',
            data: { formID: formid, companyIDs: companyIds },
            success: function (result) {
                hideLoaderFrame();
                if (result.InsertedRowId > 0) {
                    ////Save Form
                    if (window.FormData !== undefined) {
                        var $form = $("#InterimProbationRequestFrom");
                        $.validator.unobtrusive.parse($form);
                        $form.validate();
                        var isValidDdls = isValidOptionDdl();
                        if ($form.valid() && isValidDdls) {
                            pageLoaderFrame();
                            var ID = $("#ID").val();
                            var RequestInitialize = $("#RequestInitialize").val();
                            var hdnformProcessID = $("#hdnformProcessID").val();
                            var RequesterEmployeeID = $("#RequesterEmployeeID").val();
                            var FinalRatingScaleID = $("#FinalRatingScaleID").val();
                            var TypeEvaluationID = $("#TypeEvaluationID").val();
                            var EmployeeConfirmedDetails = $("#EmployeeConfirmedDetails").val();
                            var LineManagerComments = $("#LineManagerComments").val();
                            var Comments = $("#txtComments").val();
                            var changeOfferId = $('input[type=radio][name=changeOfferId]:checked').val();

                            var AllJobEvalData = [];
                            var selectedJobEval = $('select.jobevalddl option:selected');
                            $(selectedJobEval).each(function (id, option) {
                                AllJobEvalData.push({
                                    JobEvaluationID: $(this).data('jobevalid'),
                                    RatingScaleID: option.value
                                });

                            });

                            var AllCodeConductData = [];
                            var selectedCodeConduct = $('select.codeconductddl option:selected')
                            $(selectedCodeConduct).each(function (id, option) {
                                AllCodeConductData.push({
                                    CodeOfConductID: $(this).data('codeconductid'),
                                    RatingScaleID: option.value
                                });

                            });

                            var AllOtherOptionData = [];
                            var selectedOtherOption = $('select.otheroptionddl option:selected')
                            $(selectedOtherOption).each(function (id, option) {
                                AllOtherOptionData.push({
                                    OthersOptionID: $(this).data('otheroptionid'),
                                    RatingScaleID: option.value
                                });

                            });

                            //---------------------------------------------
                            var interimProbationRequestModel = {
                                ID: ID,
                                FinalRatingScaleID: FinalRatingScaleID,
                                TypeEvaluationID: TypeEvaluationID,
                                EmployeeConfirmedDetails: EmployeeConfirmedDetails,
                                LineManagerComments: LineManagerComments,
                                Comments: Comments,
                                RequesterEmployeeID: RequesterEmployeeID,
                                RequestInitialize: RequestInitialize,
                                FormProcessID: hdnformProcessID,
                                lstJobEvalModel: AllJobEvalData,
                                lstConductCode: AllCodeConductData,
                                lstOtherOption: AllOtherOptionData
                            };
                            //---------------------------------------------    
                            
                            var formData = new FormData($form[0]);
                            formData.append("interimProbationRequestModelDetails", JSON.stringify(interimProbationRequestModel));

                            $.ajax({
                                url: '/EmployeeInterimProbation/UpdateEmployeeInterimProbationRequest',
                                type: 'POST',
                                contentType: false, // Not to set any content header  
                                processData: false, // Not to process data  
                                data: formData,
                                success: function (result) {
                                    ShowMessage(result.CssClass, result.Message);
                                    hideLoaderFrame();
                                    if (result.InsertedRowId > 0) {
                                        setTimeout(BackToTaskList(), 1000);
                                    }
                                },
                                error: function (err) {
                                    hideLoaderFrame();
                                    ShowMessage('error', err.statusText);
                                }
                            });
                        }
                    }
                }

                else {
                    ShowMessage(result.CssClass, result.Message);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });

    });

    $('.btnUpdate').click(function () {
        pageLoaderFrame();
        var formid = 43;
        var companyIds = $("#hdnCompanyID").val()
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/EmployeeInterimProbation/IsWorkFlowExists',
            data: { formID: formid, companyIDs: companyIds },
            success: function (result) {
                if (result.InsertedRowId > 0) {
                    if (window.FormData !== undefined) {
                        var $form = $("#InterimProbationRequestFrom");
                        $.validator.unobtrusive.parse($form);
                        $form.validate();
                        var isValidDdls = isValidOptionDdl();
                        if ($form.valid() && isValidDdls) {
                            var ID = $("#ID").val();
                            var RequestInitialize = $("#RequestInitialize").val();
                            var hdnformProcessID = $("#hdnformProcessID").val();
                            var RequesterEmployeeID = $("#RequesterEmployeeID").val();
                            var FinalRatingScaleID = $("#FinalRatingScaleID").val();
                            var TypeEvaluationID = $("#TypeEvaluationID").val();
                            var EmployeeConfirmedDetails = $("#EmployeeConfirmedDetails").val();
                            var LineManagerComments = $("#LineManagerComments").val();
                            var Comments = $("#txtComments").val();
                            var changeOfferId = $('input[type=radio][name=changeOfferId]:checked').val();
                            var reqstatusid = $("#hdnReqStatusID").val();

                            var AllJobEvalData = [];
                            var selectedJobEval = $('select.jobevalddl option:selected')
                            $(selectedJobEval).each(function (id, option) {
                                AllJobEvalData.push({
                                    JobEvaluationID: $(this).data('jobevalid'),
                                    RatingScaleID: option.value
                                });

                            });

                            var AllCodeConductData = [];
                            var selectedCodeConduct = $('select.codeconductddl option:selected')
                            $(selectedCodeConduct).each(function (id, option) {
                                AllCodeConductData.push({
                                    CodeOfConductID: $(this).data('codeconductid'),
                                    RatingScaleID: option.value
                                });

                            });

                            var AllOtherOptionData = [];
                            var selectedOtherOption = $('select.otheroptionddl option:selected')
                            $(selectedOtherOption).each(function (id, option) {
                                AllOtherOptionData.push({
                                    OthersOptionID: $(this).data('otheroptionid'),
                                    RatingScaleID: option.value
                                });

                            });

                            //---------------------------------------------
                            var interimProbationRequestModel = {
                                ID: ID,
                                FinalRatingScaleID: FinalRatingScaleID,
                                TypeEvaluationID: TypeEvaluationID,
                                EmployeeConfirmedDetails: EmployeeConfirmedDetails,
                                LineManagerComments: LineManagerComments,
                                Comments: Comments,
                                RequesterEmployeeID: RequesterEmployeeID,
                                RequestInitialize: RequestInitialize,
                                FormProcessID: hdnformProcessID,
                                ReqStatusID: reqstatusid,
                                lstJobEvalModel: AllJobEvalData,
                                lstConductCode: AllCodeConductData,
                                lstOtherOption: AllOtherOptionData
                            };
                            //---------------------------------------------                            
                            var formData = new FormData($form[0]);
                            formData.append("interimProbationRequestModelDetails", JSON.stringify(interimProbationRequestModel));

                            $.ajax({
                                url: '/EmployeeInterimProbation/ReInitializeEmployeeInterimProbationRequest',
                                type: 'POST',
                                contentType: false, // Not to set any content header  
                                processData: false, // Not to process data  
                                data: formData,
                                success: function (result) {
                                    ShowMessage(result.CssClass, result.Message);
                                    hideLoaderFrame();
                                    if (result.InsertedRowId > 0) {
                                        setTimeout(function () { location.href = "/EmployeeInterimProbation/Edit/" }, 1000);
                                    }
                                },
                                error: function (err) {
                                    hideLoaderFrame();
                                    ShowMessage('error', err.statusText);
                                }
                            });
                        }
                        else
                            hideLoaderFrame();
                    }
                }

                else {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });

    });

    $('.btnApprove').click(function () {
        pageLoaderFrame();
        var formprocessid = $("#hdnformProcessID").val();
        var comments = $("#txtComments").val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/EmployeeInterimProbation/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/EmployeeInterimProbation/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        }
        else
            ShowMessage("error", "Comments field is mandatory.");
    });

});

$(document).on("change", ".jobevalddl,.codeconductddl,.otheroptionddl", function () {
    validateOptionDropDown(this);
});

function validateOptionDropDown(element) {
    if ($(element).val() == "0") {
        if ($(element).parent().find('.field-validation-error').length == 0)
            $(element).parent().append('<span class=\"field-validation-error\">This field is mandatory</span>');
    }
    else if ($(element).parent().find('.field-validation-error').length > 0) {
        $(element).parent().find('.field-validation-error')[0].remove();
    }
}

function isValidOptionDdl() {
    var isValid = true;
    var RequestInitialize = $("#RequestInitialize").val();
    if (RequestInitialize.toLowerCase() == "false") {
        $(".jobevalddl").each(function (index, selectedDdl) {
            if ($(selectedDdl).val() == "0") {
                validateOptionDropDown(this);
                isValid = false;
            }
        });
        $(".codeconductddl").each(function (index, selectedDdl) {
            if ($(selectedDdl).val() == "0") {
                validateOptionDropDown(this);
                isValid = false;
            }
        });
        $(".otheroptionddl").each(function (index, selectedDdl) {
            if ($(selectedDdl).val() == "0") {
                validateOptionDropDown(this);
                isValid = false;
            }
        });
    }
    return isValid;
}
