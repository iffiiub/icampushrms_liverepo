﻿var i = 0;
var selectList = "";
var SalaryRangeData = [];
var orignalList = "";
$(document).ready(function () {
    GetData(1);
    bindSelectpicker('.selectpickerddl');
    $(document).on("click", ".close", function () {
        SalaryRangeData = [];
    });
});
var AllEditMode = 0;
var $tr;
var oTableChannel;
var isDataIsRefresh = true;

function GetData(id) {
    if (id == 1) {
        pageLoaderFrame();
    }
    isDataIsRefresh = true;
    var CompanyID = $("#ddlCompanyFilter").val();
    var PositionID = $("#ddlPositionFilter").val();
    if (CompanyID == 0) {
        CompanyID = null;
    }
    if (PositionID == 0) {
        PositionID = null;
    }
    oTableChannel = $('#tblSalaryRangesList').dataTable({
        "sAjaxSource": "/SalaryRange/GetSalaryRanges",
        "aoColumns": [
            { "mData": "CompanyId", "bVisible": true, "sClass": "hidden" },
            { "mData": "CompanyName", "sTitle": "Organization Name", 'width': '10%' },
            { "mData": "PositionID", "bVisible": true, "sClass": "hidden" },
            { "mData": "Position", "sTitle": "Position Name", 'width': '10%' },
            { "mData": "MinSalary", "sTitle": "Min Salary", 'width': '10%' },
            { "mData": "MaxSalary", "sTitle": "Max Salary", 'width': '10%' },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '10%', "sClass": "text-center" },
            { "mData": "SalaryRangesID", "bVisible": true, "sClass": "hidden" },
            { "mData": "AutoId", "bVisible": true, "sClass": "hidden" },
            { "mData": "OldMinSal", "bVisible": true, "sClass": "hidden" },
            { "mData": "OldMaxSal", "bVisible": true, "sClass": "hidden" }
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "CompanyID", "value": CompanyID }, { "name": "PositionID", "value": PositionID });
        },
        "processing": false,
        "serverSide": false,
        "ajax": "/SalaryRange/GetSalaryRanges",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var CompanyId = aData["CompanyId"];
            var select = $(nRow).find(".ddlCompany");
            $(select).val(CompanyId);// .attr("selected", "selected"); 
            var PositionId = aData["PositionID"];
            var select = $(nRow).find(".ddlPosition");
            $(select).val(PositionId);

        },
        "fnDrawCallback": function () {
            $('#tblSalaryRangesList .selectpickerddl').selectpicker();
            if (AllEditMode == 1)
                DispalyInEditMode();
            //if (AllRefreshMode == 1)
            //  RefreshInEditMode();


        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('GeneralAccountPageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('GeneralAccountPageNo'));
        },
        "fnInitComplete": function (oSettings) {
            hideLoaderFrame();
        }
    });
}

function SetValue(e) {

    var closesetRow = $(e).closest('tr');
    var select = $(closesetRow).find(".ddlCompany");
    var id = $(select).val();
    $(closesetRow).find('td:eq(0)').text(id);
    //

}

function SetValuePosition(e) {

    var closesetRow = $(e).closest('tr');
    var select = $(closesetRow).find(".ddlPosition");
    var id = $(select).val();
    $(closesetRow).find('td:eq(2)').text(id);
    //

}

function EditSalaryRange(e, CompanyID, PositionID, autoID, SalaryRangesID) {

    $tr = $(e).closest("tr");

    var closesetRow = $(e).closest('tr');

    $($tr).find("td").eq(6).html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="UpdateSalaryRanges(this,' + CompanyID + ',' + PositionID + ',' + autoID + ', ' + SalaryRangesID + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + CompanyID + ',' + PositionID + ',' + autoID + ', ' + SalaryRangesID + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
    $($tr).addClass("IsEdit");

    var minSalary = $($tr).find("td").eq(4).html();
    var maxSalary = $($tr).find("td").eq(5).html();
    $($tr).find("td").eq(4).html("<input data-Old='" + minSalary + "' type='text' value='" + minSalary + "' class='form-control txtInput txtMinSalary'>");
    $($tr).find("td").eq(5).html("<input data-Old='" + maxSalary + "' type='text' value='" + maxSalary + "' class='form-control txtInput txtMaxSalary'>");
}

function UpdateSalaryRanges(e, CompanyID, PositionID, autoID, SalaryRangesID) {
    var $tr = $(e).closest("tr");
    var minSalary = $($tr).find(".txtMinSalary").val();
    var maxSalary = $($tr).find(".txtMaxSalary").val();
    var IsExistRecordInDB = false;

    if (parseInt(maxSalary) < parseInt(minSalary)) {
        ShowMessage("error", "Max salary should be greater than min salary.");
        return;
    }
    IsExistRecordInDB = IsExistsSalaryRanges(CompanyID, PositionID, minSalary, maxSalary);
    if (IsExistRecordInDB) {
        ShowMessage("error", "Salary range is already exists please enter valid salary range.");
        return;
    }
    else {
        var SalaryRangeModel = {
            CompanyID: CompanyID,
            PositionID: PositionID,
            SalaryRangesID: SalaryRangesID,
            IsDeleted: false,
            MinSalary: $($tr).find('.txtMinSalary').val(),
            MaxSalary: $($tr).find('.txtMaxSalary').val(),
        }
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: SalaryRangeModel,
            url: '/SalaryRange/UpdateSaalryRange',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                GetData(1);
            }
        });
    }
}

function CancelNewRow(e, CompanyID, PositionID, autoID, SalaryRangesID) {

    var closesetRow = $(e).closest('tr');

    var minSalary = $(closesetRow).find(".txtMinSalary").attr("data-Old");
    var maxSalary = $(closesetRow).find(".txtMaxSalary").attr("data-Old");

    $(closesetRow).find('td:eq(4)').html(minSalary);
    $(closesetRow).find('td:eq(5)').html(maxSalary);

    $(closesetRow).find('td:eq(6)').html("<a class='btn btn-success btn-rounded btn-condensed btn-sm'   onclick='UpdateSalaryRanges(this," + CompanyID + "," + PositionID + "," + autoID + "," + SalaryRangesID + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> " +
                                        "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"changeStatus(this, " + CompanyID + "," + PositionID + "," + autoID + "," + SalaryRangesID + ")\" title='Delete'><i class='fa fa-times'></i> </a>");

}

function changeStatus(e, CompanyID, PositionID, autoID, SalaryRangesID) {

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { SalaryRangesID: SalaryRangesID },
        url: '/SalaryRange/DeleteSalaryRange',
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
            GetData(1);
        }
    });
}

function EditAll() {

    AllEditMode = 1;
    DispalyInEditMode();
}

function DispalyInEditMode() {

    $tr = $('#tblSalaryRangesList tr ');
   // var canEdit = $tr.find("td").eq(4).find('input').length > 0 ? false : true;
    pageLoaderFrame();
    $($tr).each(function (i, item) {
        if ($(item).find("td").eq(4).find('input').length == 0) {
            var CompanyID = $(item).find("td").eq(0).html();
            var PositionID = $(item).find("td").eq(2).html();
            var autoID = $(item).find("td").eq(8).html();
            var SalaryRangesID = $(item).find("td").eq(7).html();
            var minSalary = $(item).find("td").eq(4).html();
            var maxSalary = $(item).find("td").eq(5).html();
            $(item).find("td").eq(4).html("<input data-Old='" + minSalary + "' type='text' value='" + minSalary + "' class='form-control txtInput txtMinSalary'>");
            $(item).find("td").eq(5).html("<input data-Old='" + maxSalary + "' type='text' value='" + maxSalary + "' class='form-control txtInput txtMaxSalary'>");
            $(item).addClass("IsEdit");
            $(item).find("td").eq(6).html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="UpdateSalaryRanges(this,' + CompanyID + ',' + PositionID + ',' + autoID + ', ' + SalaryRangesID + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + CompanyID + ',' + PositionID + ',' + autoID + ', ' + SalaryRangesID + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
        }
    });
    hideLoaderFrame();
}

function SaveAll() {

    AllEditMode = 0;
    var table = $("#tblSalaryRangesList").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var UpdatedRowData = table.rows(UpdatedTr).data();
        var updatedData = [];
        $(UpdatedTr).each(function (i, itm) {
            var SalaryRangesID = $(itm).find("td").eq(7).html();
            var minSalary = $(itm).find("td").eq(4).find("input").val();
            var maxSalary = $(itm).find("td").eq(5).find("input").val();

            updatedData.push({
                SalaryRangesID: SalaryRangesID,
                MinSalary: minSalary,
                MaxSalary: maxSalary,
            });

        });
        console.log(updatedData);
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/SalaryRange/SaveMultipleSalaryRange",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'salaryList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        GetData(1)
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            })
        });
    }
    else {
        ShowMessage("error", "Please edit atleast one record.");
        hideLoaderFrame();
    }
}

function RefreshAll() {
    location.reload();
}

$("#btn_addMultipleSalary").click(function () {
    AddMultipleSalary();
});

function AddMultipleSalary() {
    SalaryRangeData = [];
    $("#modal_Loader").load("/SalaryRange/AddMultipleSalaryRange", function () {
        $("#modal_heading").text('Add Salary Range');
        $('#myModal').modal({ backdrop: 'static' })
        $(".modal-dialog").attr("style", "width:750px;");
        $("#myModal").modal("show");
        bindSelectpicker('.selectpickerddl');

    });
}

function SaveSingleSalary() {

    var $tr = $('#singlesalaryrange-table tr ');

    var CompanyID = $($tr).find(".ddlCompany").val();

    var PositionID = $($tr).find(".ddlPosition").val();

    var minSalary = $($tr).find(".txtMinSalary").val();
    var maxSalary = $($tr).find(".txtMaxSalary").val();
    var salaryRangesModel = {
        CompanyID: CompanyID,
        PositionID: PositionID,

        MinSalary: $($tr).find('.txtMinSalary').val(),
        MaxSalary: $($tr).find('.txtMaxSalary').val(),
    }
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: salaryRangesModel,
        url: '/SalaryRange/InsertSingleSalaryRange',
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
            $("#myModal").modal('hide');
            GetData(1);
        }
    });
}

function saveRow(e) {
    i = i + 1;
    var Cmpid = $("#ddlCompanies").val();
    var CompanyName = $("#ddlCompanies option:selected").text();
    var Posid = $("#ddlPositions").val();
    var PositionName = $("#ddlPositions option:selected").text();
    var minSalary = $("#txtMinSalary").val();
    var maxSalary = $("#txtMaxSalary").val();
    var IsExistRecord = false;
    var IsExistRecordInDB = false;

    if (!checkFieldsEmptyOrNot()) {
        if (parseInt(maxSalary) < parseInt(minSalary)) {
            ShowMessage("error", "Max salary should be greater than min salary.");
            return;
        }
        IsExistRecordInDB = IsExistsSalaryRanges(Cmpid, Posid, minSalary, maxSalary);
        if (IsExistRecordInDB) {
            ShowMessage("error", "Salary range is already exists please enter valid salary range.");
            return;
        }
        else {
            var countRec = 0;
            SalaryRangeData.forEach(function (p) {
                if (p.CompanyID === Cmpid && p.PositionID === Posid && ((p.MinSalary >= minSalary && p.MinSalary <= maxSalary) || (p.MaxSalary >= minSalary && p.MaxSalary <= maxSalary))) {
                    countRec++;
                }
            });
            if (countRec == 0) {
                IsExistRecord = true;
            }
        }
        if (IsExistRecord) {
            SalaryRangeData.push({
                "CompanyID": Cmpid,
                "CompanyName": CompanyName,
                "PositionID": Posid,
                "PositionName": PositionName,
                "MinSalary": minSalary,
                "MaxSalary": maxSalary
            });
            reloadTableNew();
        }
        else {
            ShowMessage("error", "Salary ranges are mismatching please enter correct range.");
        }
    }
    else {
        return;
    }
}

$(document).on("change", "#ddlCompanies,#ddlPositions", function () {
    var id = $(this).attr("id");
    var fieldValue = $("#" + id).val();

    if (fieldValue == "" || fieldValue == "0") {
        IsValidInputField(id, 1);
    }
    else {
        IsValidInputField(id, 2);
    }
});

$(document).on("keyup", "#txtMinSalary,#txtMaxSalary", function () {
    var id = $(this).attr("id");
    var fieldValue = $("#" + id).val();

    if (fieldValue == "" || fieldValue == "0") {
        IsValidInputField(id, 1);
    }
    else {
        IsValidInputField(id, 2);
    }
});

function checkFieldsEmptyOrNot() {
    var Cmpid = $("#ddlCompanies").val();
    var Posid = $("#ddlPositions").val();
    var minSalary = $("#txtMinSalary").val();
    var maxSalary = $("#txtMaxSalary").val();
    var IsValidAllFields = false;

    if (Cmpid == "0" || Cmpid == "") {
        IsValidInputField("ddlCompanies", 1);
        IsValidAllFields = true;
    } else {
        IsValidInputField("ddlCompanies", 2);
    }
    if (Posid == "0" || Posid == "") {
        IsValidInputField("ddlPositions", 1);
        IsValidAllFields = true;
    } else {
        IsValidInputField("ddlPositions", 2);
    }
    if (minSalary == 0) {
        IsValidInputField("txtMinSalary", 1);
        IsValidAllFields = true;
    }
    else {
        IsValidInputField("txtMinSalary", 2);
    }
    if (maxSalary == 0) {
        IsValidInputField("txtMaxSalary", 1);
        IsValidAllFields = true;
    }
    else {
        IsValidInputField("txtMaxSalary", 2);
    }

    return IsValidAllFields;
}

function IsValidInputField(id, mode) {
    if (mode == 1) {
        if ($("#" + id).parent().find('.field-validation-error').length == 0)
            $("#" + id).parent().append('<span class=\"field-validation-error\">This field is mandatory</span>');
    }
    else if (mode == 2) {
        if ($("#" + id).parent().find('.field-validation-error').length > 0)
            $("#" + id).parent().find('.field-validation-error')[0].remove();
    }
}

function reloadTableNew() {
    var k = 0;
    $("#salaryrange-table > tbody").empty();

    var j = Object.keys(SalaryRangeData).length;

    SalaryRangeData.forEach(function (p, index) {
        k = k + 1;
        if (j == 1) {

            $('#salaryrange-table > tbody').append(
           '<tr id="rowId_' + p.CompanyID + '" class="edit' + p.CompanyID + '">' +
           '<td class="col-md-1 hidden">' + p.CompanyID + '</td>' +
          '<td class="col-md-4">' + p.CompanyName + '</td>' +
          '<td class="col-md-1 hidden">' + p.PositionID + '</td>' +
           '<td class="col-md-4">' + p.PositionName + '</td>' +
           '<td class="col-md-3">' + p.MinSalary + '</td>' +
           '<td class="col-md-3">' + p.MaxSalary + '</td>' +
          '<td class="col-md-3 center-text-align padding-0">' +
          '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditSalaryRanges(' + index + ',this)" id="btnEdit" data-id=' + p.CompanyID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
          '<a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>' +
          '</td>' +
          '</tr>')

        }

        else if (k < j) {
            $('#salaryrange-table > tbody').append(
          '<tr id="rowId_' + p.CompanyID + '" class="edit' + p.CompanyID + '">' +
          '<td class="col-md-1 hidden">' + p.CompanyID + '</td>' +
         '<td class="col-md-4">' + p.CompanyName + '</td>' +
         '<td class="col-md-1 hidden">' + p.PositionID + '</td>' +
          '<td class="col-md-4">' + p.PositionName + '</td>' +
          '<td class="col-md-3">' + p.MinSalary + '</td>' +
          '<td class="col-md-3">' + p.MaxSalary + '</td>' +
         '<td class="col-md-3 center-text-align padding-0">' +
         '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditSalaryRanges(' + index + ',this)" id="btnEdit" data-id=' + p.CompanyID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
         '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(' + index + ')" title="Delete"><i class="fa fa-times"></i> </a>' +
         '</td>' +
         '</tr>')

        }
        else if (k == j) {

            $('#salaryrange-table > tbody').append(
          '<tr id="rowId_' + p.CompanyID + '" class="edit' + p.CompanyID + '">' +
          '<td class="col-md-1 hidden">' + p.CompanyID + '</td>' +
         '<td class="col-md-4">' + p.CompanyName + '</td>' +
         '<td class="col-md-1 hidden">' + p.PositionID + '</td>' +
          '<td class="col-md-4">' + p.PositionName + '</td>' +
          '<td class="col-md-3">' + p.MinSalary + '</td>' +
          '<td class="col-md-3">' + p.MaxSalary + '</td>' +
         '<td class="col-md-3 center-text-align padding-0">' +
         '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditSalaryRanges(' + index + ',this)" id="btnEdit" data-id=' + p.CompanyID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
         '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(' + index + ')" title="Delete"><i class="fa fa-times"></i> </a>' +
          '<a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>' +
          '</td>' +
          '</tr>')
        }
    });

}

function AddDataRow() {
    $("#btnAddRow" + i + "").hide();
    $("#addsalaryrangeInfo").attr("disabled", true)
    $('#salaryrange-table > tbody').append(
      '<tr>' +
        '<td class="col-md-1 hidden"></td>' +
        '<td class="col-md-4">' +
             '<div class=""><select data_CompaniesId="" class="ddlCompanies form-control selectpickerddl" data-live-search="true" id="ddlCompanies">' + $("#ddlCompany").text() + ' </select></div>' +
        '</td>' +
         '<td class="col-md-1 hidden"></td>' +
         '<td class="col-md-4">' +
             '<div class=""><select data_PositionsId="" class="ddlPositions form-control selectpickerddl" data-live-search="true" id="ddlPositions"><option value="">Select Position</option>' + $("#ddlPosition").text() + ' </select></div>' +
        '</td>' +
        '<td class="col-md-3">' +
            '<div class=""><input class="form-control txtInput txtMinSalary"  id="txtMinSalary" type="number" /></div>' +
        '</td>' +
         '<td class="col-md-3">' +
            '<div class=""><input class="form-control txtInput txtMaxSalary"  id="txtMaxSalary" type="number" /></div>' +
        '</td>' +
        '<td class="center-text-align col-md-3 padding-0">' +
            '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>' +
        '</td>' +
       '</tr>');
    bindSelectpicker(".selectpickerddl")
}

function CancelNewRow(e) {
    var item = $(e).closest('tr');
    var CompanyID = $(item).find("td").eq(0).html();
    var PositionID = $(item).find("td").eq(2).html();
    var autoID = $(item).find("td").eq(8).html();
    var SalaryRangesID = $(item).find("td").eq(7).html();
    var minSalary = $(item).find("td").eq(9).html();
    var maxSalary = $(item).find("td").eq(10).html();
    $(item).find("td").eq(4).html(minSalary);
    $(item).find("td").eq(5).html(maxSalary);
    $(item).addClass("IsEdit");
    $(item).find("td").eq(6).html('<div class="center-text-align"><a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditSalaryRange(this,' + CompanyID + ',' + PositionID + ',' + autoID + ',' + SalaryRangesID + ')" title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="changeStatus(this,' + CompanyID + ',' + PositionID + ',' + autoID + ',' + SalaryRangesID + ')" title="Delete"><i class="fa fa-times"></i> </a></div>');
    $(item).removeClass("IsEdit");    
}

function DeleteRow(id) {
    SalaryRangeData.splice(id, 1);
    reloadTableNew();
}

function SaveMultipleSalary() {
    if (Object.keys(SalaryRangeData).length == 0) {
        ShowMessage("error", "Please enter salary range ");
    }
    else {
        $.ajax({
            url: '/SalaryRange/InsertMultipleSalayRange',
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            data: JSON.stringify({ 'salaryListinsert': SalaryRangeData }),
            success: function (data) {
                $("#myModal").modal('hide');
                if (data.Success) {
                    if (data.CssClass == "Request") {
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                } else {
                    ShowMessage("error", data.Message);
                }
                GetData(1);
                SalaryRangeData = [];
            },
            error: function (xhr) {
            }
        });
    }
}

function EditSalaryRanges(RecordIndex, e) {

    var $row = $(e).parent().parent();
    var CompanyId = $row.find('td:eq(0)').text();
    var PositionId = $row.find('td:eq(2)').text();
    var minsalary = $row.find('td:eq(4)').text();
    var maxsalary = $row.find('td:eq(5)').text();

    $($row).find('td:eq(1)').html('<div class=""><select class="ddlCompanies form-control selectpickerddl" data-live-search="true" id="ddlCompanies">' + $("#ddlCompany").text() + ' </select></div>');
    $($row).find('td:eq(3)').html('<div class=""><select class="ddlPositions form-control selectpickerddl" data-live-search="true" id="ddlPositions"><option value="">Select Position</option>' + $("#ddlPosition").text() + ' </select></div>');
    $($row).find('td:eq(4)').html('<div class=""><input class="form-control txtInput txtMinSalary"  id="txtMinSalary" type="number"/></div>');
    $($row).find('td:eq(5)').html('<div class=""><input class="form-control txtInput txtMaxSalary"  id="txtMaxSalary" type="number"/></div>');
    $($row).find('td:eq(6)').html('<div class="center-text-align"><button id="btnSaveEdited" class="tableButton" onclick="saveEditedRow(' + RecordIndex + ',this,' + minsalary + ',' + maxsalary + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');

    $("#txtMinSalary").val(minsalary);
    $("#txtMaxSalary").val(maxsalary);

    var select = $row.find(".ddlCompanies");
    $(select).val(CompanyId);// .attr("selected", "selected");  

    var select = $row.find(".ddlPositions");
    $(select).val(PositionId);// .attr("selected", "selected");
    bindSelectpicker(".selectpickerddl")
    $('#salaryrange-table .selectpickerddl').selectpicker();
}

function saveEditedRow(RecordIndex, e, oldMinSal, oldMaxSal) {
    var Cmpid = $("#ddlCompanies").val();
    var CompanyName = $("#ddlCompanies option:selected").text();
    var Posid = $("#ddlPositions").val();
    var PositionName = $("#ddlPositions option:selected").text();
    var minSalary = $("#txtMinSalary").val();
    var maxSalary = $("#txtMaxSalary").val();
    var IsExistRecord = false;
    var IsExistRecordInDB = false;

    if (!checkFieldsEmptyOrNot()) {
        if (parseInt(maxSalary) < parseInt(minSalary)) {
            ShowMessage("error", "Max salary should be greater than min salary.");
            return;
        }
        IsExistRecordInDB = IsExistsSalaryRanges(Cmpid, Posid, minSalary, maxSalary);
        if (IsExistRecordInDB) {
            ShowMessage("error", "Salary range is already exists please enter valid salary range.");
            return;
        }
        else {
            var countRec = 0;
            SalaryRangeData.forEach(function (p, index) {
                if (index != RecordIndex) {
                    if (p.CompanyID === Cmpid && p.PositionID === Posid && ((p.MinSalary >= minSalary && p.MinSalary <= maxSalary) || (p.MaxSalary >= minSalary && p.MaxSalary <= maxSalary))) {
                        countRec++;
                    }
                }
            });
            if (countRec == 0) {
                IsExistRecord = true;
            }
        }
        if (IsExistRecord) {
            SalaryRangeData[RecordIndex].MinSalary = minSalary;
            SalaryRangeData[RecordIndex].MaxSalary = maxSalary;
            SalaryRangeData[RecordIndex].CompanyName = CompanyName;
            SalaryRangeData[RecordIndex].CompanyID = Cmpid;
            SalaryRangeData[RecordIndex].PositionID = Posid;
            SalaryRangeData[RecordIndex].PositionName = PositionName;
            reloadTableNew();
        } else {
            ShowMessage("error", "Salary ranges are mismatching please enter correct range.");
        }

    }
    else {
        return;
    }
}

function IsExistsSalaryRanges(Cmpid, Posid, minSalary, maxSalary) {
    var IsExistRecordInDB = false;
    $.ajax({
        url: '/SalaryRange/CheckSalaryRangesRecordExists',
        dataType: 'json',
        type: "POST",
        data: { CompanyID: Cmpid, PositionID: Posid, OriginalMinSalary: minSalary, OriginalMaxSalary: maxSalary },
        async: false,
        success: function (data) {
            IsExistRecordInDB = data.IsExistsSalary;
        },
        error: function (xhr) {
        }
    });
    return IsExistRecordInDB;
}