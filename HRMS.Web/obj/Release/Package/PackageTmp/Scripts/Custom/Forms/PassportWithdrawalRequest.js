﻿$(document).ready(function () {
    //checkFormLoadValidation($("#FormId").val(), $("#CompanyId").val(), "#lblPassportWithdrawalRequestNotification", "PassportWithdrawalRequest");
    var formprocessid = $("#FormProcessID").val();

    if (formprocessid == null || formprocessid == '' || formprocessid == '0') {
        checkFormLoadValidation($("#FormId").val(), $("#CompanyId").val(), "#lblPassportWithdrawalRequestNotification", "PassportWithdrawalRequest");
    }   
    $("#btnPassportWithdrawalRequestWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#PassportWithdrawalRequestWarningBox").remove();
    });

    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });
    var reqstatusid = $("#hdnReqStatusID").val();
    var PassportReasonID = $("#ddlPassportReason").val();
    if (reqstatusid == "3") {
        $("#WithdrawalDate").removeAttr("disabled", "disabled");
        $("#PassportReturnDate").removeAttr("disabled", "disabled");
        if (PassportReasonID == "12") {
            $("#OtherReason").removeAttr("disabled");
        }
        else {
            $("#OtherReason").val('');
            $("#OtherReason").attr("disabled", "disabled");
            $("#OtherReason").removeClass('input-validation-error');
            $(".OtherReason").text('');
        }
    }
});

$(document).on("change", "#ddlPassportReason", function () {
    if ($(this).val() == 12) {
        $("#OtherReason").removeAttr("disabled");
    }
    else {
        $("#OtherReason").val('');
        $("#OtherReason").attr("disabled", "disabled");
        $("#OtherReason").removeClass('input-validation-error');
        $(".OtherReason").text('');
    }
});

$(document).on("click", ".btnSave", function () {
    if ($("#frmPassportWithdrawalRequest").valid()) {
        pageLoaderFrame();
        $.ajax({
            dataType: 'json',
            url: '/PassportWithdrawalRequest/IsWorkFlowExists',
            data: {
                formID: $("#FormId").val(), companyIDs: $("#CompanyId").val()
            },
            success: function (result) {
                if (result.InsertedRowId > 0) {
                    SavePassportWithdrawalRequest(); //Save Form
                }
                else {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
});

$(document).on("click", ".btnUpdate", function () {
    if ($("#frmPassportWithdrawalRequest").valid()) {
        pageLoaderFrame();
        var param = {
            ID:$("#ID").val(),PassportReasonID: $("#ddlPassportReason").val(), OtherReason: $("#OtherReason").val(),
            WithdrawalDate: $("#WithdrawalDate").val(), PassportReturnDate: $("#PassportReturnDate").val(),
            ReqStatusID: $("#hdnReqStatusID").val(), Comments: $("#txtComments").val()
        };
        $.ajax({
            url: "/PassportWithdrawalRequest/UpdatePassportWithdrawalRequest",
            type: 'POST',
            data: param,
            datatype: "json",
            success: function (data) {
                if (data.InsertedRowId>0) {
                    ShowMessage("success", data.Message);
                    hideLoaderFrame();
                    BackToTaskList();
                }
                else {
                    ShowMessage("error", data.Message);
                    hideLoaderFrame();
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
});

function SavePassportWithdrawalRequest() {
    var param = {
        PassportReasonID: $("#ddlPassportReason").val(), OtherReason: $("#OtherReason").val(),
        WithdrawalDate: $("#WithdrawalDate").val(), PassportReturnDate: $("#PassportReturnDate").val(), Comments: $("#txtComments").val()
    };
    $.ajax({
        url: "/PassportWithdrawalRequest/SavePassportWithdrawalRequest",
        type: 'POST',
        data: param,
        datatype: "json",
        success: function (data) {
            if (data.Success) {
                ShowMessage("success", data.Message);
                hideLoaderFrame();
                BackToTaskList();
            }
            else {
                ShowMessage("error", data.Message);
                hideLoaderFrame();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

$(document).on("click", ".btnApprove", function () {
    pageLoaderFrame();
    var formprocessid = $("#FormProcessID").val();
    var comments = $("#txtComments").val();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/PassportWithdrawalRequest/ApproveForm',
        data: { formProcessID: formprocessid, comments: comments },
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (result.InsertedRowId > 0) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
});

$(document).on("click", ".btnReject", function () {
    var IsValidateComment = validateComments($("#txtComments"));
    if (IsValidateComment) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
            var formprocessid = $("#FormProcessID").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/PassportWithdrawalRequest/RejectForm',
                data: { formProcessID: formprocessid, comments: comments },
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (result.InsertedRowId > 0) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        });
    }
    else
        ShowMessage("error", "Comments field is mandatory.");
});

$(document).on("keypress", "#txtComments", function () {
    var maxlength = $(this).attr("maxlength");
    var currentLength = $(this).val().length;

    if (currentLength >= maxlength) {
        $('#lblCommentCharsLeft').text("0 chars left");
    } else {
        $('#lblCommentCharsLeft').text(maxlength - currentLength + " chars left");
    }
});
