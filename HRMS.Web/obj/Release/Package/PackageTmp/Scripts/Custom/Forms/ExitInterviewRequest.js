﻿$(document).ready(function () {
    $("#txtOtherOptionsToStay").attr("disabled", "disabled");
    $("#txtOtherBenefitsOffer").attr("disabled", "disabled");
    $("#txtNewJobGoodCompany").attr("disabled", "disabled");
    $("#txtGroupRecommendation").attr("disabled", "disabled");
});



$(document).on("change", ".OtherOptionsToStayIsEnable", function () {
    if ($("#rdOtherOptionsToStayEnable").prop("checked")) {
        $("#txtOtherOptionsToStay").removeAttr("disabled");
    }
    else {
        $("#txtOtherOptionsToStay").attr("disabled", "disabled");
        $("#txtOtherOptionsToStay").removeClass('input-validation-error');
        $(".OtherOptionsToStay").text('');
    }
});

$(document).on("change", ".OtherBenefitsOfferIsEnable", function () {
    if ($("#rdOtherBenefitsOfferEnable").prop("checked")) {
        $("#txtOtherBenefitsOffer").removeAttr("disabled");
    }
    else {
        $("#txtOtherBenefitsOffer").attr("disabled", "disabled");
        $("#txtOtherBenefitsOffer").removeClass('input-validation-error');
        $(".OtherBenefitsOffer").text('');
    }
});

$(document).on("change", ".NewJobGoodCompanyIsEnable", function () {
    if ($("#rdNewJobGoodCompanyEnable").prop("checked")) {
        $("#txtNewJobGoodCompany").removeAttr("disabled");
    }
    else {
        $("#txtNewJobGoodCompany").attr("disabled", "disabled");
        $("#txtNewJobGoodCompany").removeClass('input-validation-error');
        $(".NewJobGoodCompany").text('');
    }
});

$(document).on("change", ".GroupRecommendationIsEnable", function () {
    if ($("#rdGroupRecommendationEnable").prop("checked")) {
        $("#txtGroupRecommendation").removeAttr("disabled");
    }
    else {
        $("#txtGroupRecommendation").attr("disabled", "disabled");
        $("#txtGroupRecommendation").removeClass('input-validation-error');
        $(".GroupRecommendation").text('');
    }
});

$(document).on("click", ".btnSave", function () {
    if ($("#frmExitInterviewRequest").valid()) {

        var exitInterviewRequest = {
            EmployeeID: parseInt($("#EmployeeID").val()),
            OtherOptionsToStay: $("#txtOtherOptionsToStay").val(),
            OtherBenefitsOffer: $("#txtOtherBenefitsOffer").val(),
            JobLeastInterest: $("#txtJobLeastInterest").val(),
            JobMostInterest: $("#txtJobMostInterest").val(),
            NewJobGoodCompany: $("#txtNewJobGoodCompany").val(),
            NewSalaryAndPosition: $("#txtNewSalaryAndPosition").val(),
            GroupRecommendation: $("#txtGroupRecommendation").val(),
            ShareOtherThoughts: $("#txtShareOtherThoughts").val(),
            CompanyID: parseInt($("#CompanyID").val()),
            ExitInterviewReasonDetailsList: getPrimaryReason(),
            ExitInterviewSupervisorDetailsList: getSupervisorRating(),
            ExitInterviewJobDetailsList: getJobRating(),
            ExitInterviewServiceDetailsList: getServiceRating(),
            FormProcessID: parseInt($("#FormProcessID").val())
        };
        $.ajax({
            url: "/ExitInterviewRequest/SaveExitInterviewRequest",
            type: 'POST',
            data: {
                exitInterviewRequest: JSON.stringify(exitInterviewRequest)
            },
            dataType: 'json',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    hideLoaderFrame();
                    setTimeout(function () { location.href = "/SeparatedEmployeeList/Index"  }, 1000);
                    //location.reload();
                }
                else {
                    ShowMessage("error", data.Message);
                    hideLoaderFrame();
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
});

$(document).on("click", ".btnBack", function () {
    location.href = "/SeparatedEmployeeList/Index";
});

function getPrimaryReason() {
    var primaryreason = [];
    $(".primaryreason").each(function (index, element) {
        if ($(element).prop("checked")) {
            primaryreason.push({ PrimaryReasonID: $(element).data().primaryreasonid });
        }
    });
    return primaryreason;
}

function getSupervisorRating() {
    var supRating = [];
    $(".SupRatingOption").each(function (index, element) {
        if ($(element).prop("checked")) {
            supRating.push({ RatingOptionID: $(element).data().ratingoptionid, SupOptionID: $(element).data().supoptionid });
        }
    });
    return supRating;
}

function getJobRating() {
    var jobRating = [];
    $(".JobRatingOption").each(function (index, element) {
        if ($(element).prop("checked")) {
            jobRating.push({ RatingOptionID: $(element).data().ratingoptionid, JobOptionID: $(element).data().joboptionid });
        }
    });
    return jobRating;
}

function getServiceRating() {
    var serRating = [];
    $(".ServiceRatingOption").each(function (index, element) {
        if ($(element).prop("checked")) {
            serRating.push({ RatingOptionID: $(element).data().ratingoptionid, ServiceOptionID: $(element).data().serviceoptionid });
        }
    });
    return serRating;
}


