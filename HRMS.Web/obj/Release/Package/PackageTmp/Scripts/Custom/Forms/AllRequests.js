﻿$(document).ready(function () {    
    getGrid();   
});

function getGrid(EnableLoader) {
        pageLoaderFrame();
        var requestId = $("#txtRequestID").val();
        var formid = $("#ddlForm").val();
        var requestStatusId = $("#ddlRequestStatus").val();
 
        oTableChannel = $('#tblAllRequestsList').dataTable({
                "sAjaxSource": "/AllRequests/GetAllRequestsList",
                "aoColumns": [
                    { "mData": "RequestID", "sTitle": "Request ID", "bVisible": true, 'width': '10%'},
                    { "mData": "FormName", "sTitle": "Form Name", 'width': '20%' },
                    { "mData": "CreatedOn", "sTitle": "Submitted On", "bVisible": true, 'width': '10%' },
                    { "mData": "RequesterEmployeeName", "sTitle": "Requester", "bVisible": true, 'width': '20%' },
                    { "mData": "Status", "sTitle": "Status", 'width': '10%' },
                    { "mData": "ApproverEmployeeName", "sTitle": "Approver", "bVisible": true, 'width': '20%'},
                    { "mData": "Action", "sTitle": "Action", "bSortable": false, 'width': '10%', "sClass": "text-center" },                
                ],
                "fnServerParams": function (aoData) {
                    aoData.push({ "name": "requestId", "value": requestId }, { "name": "formID", "value": formid }, { "name": "requestStatusId", "value": requestStatusId});
                },
                "processing": false,
                "serverSide": false,
                "ajax": "/AllRequests/GetAllRequestsList",
                "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
                "iDisplayLength": 10,
                "bDestroy": true,
                "bFilter": true,
                "bInfo": true,
                "bSortCellsTop": false,
                'columnDefs': [ ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                },
                "fnDrawCallback": function () {                  
                },
                "bStateSave": true,             
                "fnInitComplete": function (oSettings) {
                    hideLoaderFrame();
                }
            });
}

function EditRequestForm(contollername, formprocessid) {
    var urlstring = '';   
    urlstring = '/' + contollername + '/UpdateDetails';
    $.ajax({       
        type: 'POST',
        url: '/' + contollername + '/SetEditRequestsFormProccessSession',
        data: { id: formprocessid },
        success: function (result) {
            if (result.id != null)
                location.href = urlstring;

        },
        error: function (err) {           
            ShowMessage('error', err.statusText);
        }
    });
}