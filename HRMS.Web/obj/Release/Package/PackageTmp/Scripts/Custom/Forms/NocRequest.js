﻿$(document).ready(function () {
    var formprocessid = $("#hdnformProcessID").val();
    var isShowddl = false;
    var isShowBusinessTravelFile = false;
    $("#showddls").hide();
    if ($("#NOCReasonID").val() == 1 || $("#NOCReasonID").val() == 2) {
        isShowddl = true;
        if ($("#ddlPurposeOfVisit").val() == 2 || $("#ddlPurposeOfVisit").val() == 3)
            isShowBusinessTravelFile = true;
    }
    else
        isShowBusinessTravelFile = false;
    $("#showddls").toggle(isShowddl);
    $("#ApprovedBusinessTravelUploadFile").toggle(isShowBusinessTravelFile);
    if ($("#NOCReasonID").val() != '7') {
        $("#OtherReason").attr("disabled", "disabled");
    }

    $("#NOCLanguage").val($("#hdnNOCLanguage").val());

    if (formprocessid == null || formprocessid == '' || formprocessid == '0' || formprocessid == undefined) {
        checkFormLoadValidation(11, $("#hdnCompanyId").val(), "#lblNOCRequestNotification", "NocRequest");
    }

    $("#btnNOCRequestWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#NOCRequestWarningBox").remove();
    });
    bindSelectpicker('.selectpickerddl');
    var formid = 11;

    if ($("#hdnReqStatusID").val() == 3 && $("#ID").val() > 0) {
        $(".btnApprove").remove();
        $(".btnReject").remove();
        $(".btnUpdate").prop("title", "Re-Submit");
        $('button#btnUpdate').text("Re-Submit");
        if ($("#OtherReason").val().length == 0) {
            $("#OtherReason").attr("disabled", "disabled");
        }
    }
    else if ($("#ID").val() != undefined && $("#ID").val() != null && $("#ID").val() > 0) {
        $("#NOCReasonID").attr("disabled", "disabled");
        $("#OtherReason").attr("disabled", "disabled");
        $("#CertificateAddressee").attr("disabled", "disabled");
        $("#CountryID").attr("disabled", "disabled");
        $("#NOCLanguage").attr("disabled", "disabled");
        //$("#FreeZoneVisa").attr("disabled", "disabled");
        $("#RequiredDate").attr("disabled", "disabled");
        $("#DateOfTravel").attr("disabled", "disabled");
        $("#ddlPurposeOfVisit").attr("disabled", "disabled");
        $("#ddlTravelExpenseBorneBy").attr("disabled", "disabled");
        $("#salaryDetail").attr("disabled", "disabled");
    }
    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#NocRequestFrom");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            var IsValidFiles = ValidateData();
            var FreeZoneVisa = $('#FreeZoneVisa').is(":checked");
            var RequiredDate = $("#RequiredDate").val();
            if ($form.valid() && IsValidFiles) {
                var ValidationStatus = ValidRequiredDate(FreeZoneVisa, RequiredDate);
                var companyIds = $("#hdnCompanyId").val();
                if (ValidationStatus == false) {
                    return;
                }
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: '/NocRequest/IsWorkFlowExists',
                    data: { formID: formid, companyIDs: companyIds },
                    success: function (result) {
                        if (result.InsertedRowId > 0)
                            SaveForm($form);
                        else {
                            ShowMessage(result.CssClass, result.Message);
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);

                    }
                });
            }
        }
    });

    $('.btnUpdate').click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#NocRequestFrom");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            var IsValidFiles = ValidateData();
            var FreeZoneVisa = $('#FreeZoneVisa').is(":checked");
            var RequiredDate = $("#RequiredDate").val();
            if ($form.valid() && IsValidFiles) {
                var ValidationStatus = ValidRequiredDate(FreeZoneVisa, RequiredDate);
                var companyIds = $("#hdnCompanyId").val();
                if (ValidationStatus == false) {
                    return;
                }
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: '/NocRequest/IsWorkFlowExists',
                    data: { formID: formid, companyIDs: companyIds },
                    success: function (result) {
                        if (result.InsertedRowId > 0)
                            UpdateNocRequestForm();
                        else {
                            ShowMessage(result.CssClass, result.Message);
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);

                    }
                });
            }
        }
    });

    $('.btnApprove').click(function () {
        pageLoaderFrame();
        var formprocessid = $("#hdnformProcessID").val();
        var comments = $("#txtComments").val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/NocRequest/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/NocRequest/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        } else {
            ShowMessage('error', 'Comments field is mandatory.');
            return false;
        }
    });

    $('.btnBack').click(function () {
        BackToTaskList();
    });

    $("#fu_PF").change(function () {
        var file = this.files[0];
        fileName = file.name;
        size = file.size;
        type = file.type;
        var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
        $("#txtuploadedMsgAdd_PF").css("color", "red");

        if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
            if (size <= 2097152) {
                $("#txtuploadedMsgAdd_PF").text("");
                $("#divBrowse_PF").hide();
                $("#btnRemove_PF").show();
                $("#file_caption_name_PF").show();
                $("#file_caption_id_PF").html(fileName.substring(0, 15));
                $("#chkFileUpload_PF").val("2");
                var file = URL.createObjectURL(event.target.files[0]);
                $('#divPreview_PF').append('<a href="' + file + '" target="_blank">' + event.target.files[0].name + '</a>');
                $("#hdnDeletedFileUpload_PF").val('0');

            }
            else {
                $("#txtuploadedMsgAdd_PF").text("Maximum 2MB file size is allowed to upload.");
                $("#btnUploadAdd_PF").hide();
                $("#divAddInforamtionDialog_PF").hide();
            }
        }
        else {
            $("#txtuploadedMsgAdd_PF").text("Only JPG,JPEG,PNG,GIF,PDF,XLS,XLSX,PPT,PPTX,DOC and DOCX extension files are allowed to upload.");
            $("#divAddInforamtionDialog_PF").hide();
        }
    });

    $("#fu_VF").change(function () {
        var file = this.files[0];
        fileName = file.name;
        size = file.size;
        type = file.type;
        var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
        $("#txtuploadedMsgAdd_VF").css("color", "red");

        if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
            if (size <= 2097152) {
                $("#txtuploadedMsgAdd_VF").text("");
                $("#divBrowse_VF").hide();
                $("#btnRemove_VF").show();
                $("#file_caption_name_VF").show();
                $("#file_caption_id_VF").html(fileName.substring(0, 15));
                $("#chkFileUpload_VF").val("2");
                var file = URL.createObjectURL(event.target.files[0]);
                $('#divPreview_VF').append('<a href="' + file + '" target="_blank">' + event.target.files[0].name + '</a>');
                $("#hdnDeletedFileUpload_VF").val('0');

            }
            else {
                $("#txtuploadedMsgAdd_VF").text("Maximum 2MB file size is allowed to upload.");
                $("#btnUploadAdd_VF").hide();
                $("#divAddInforamtionDialog_VF").hide();
            }
        }
        else {
            $("#txtuploadedMsgAdd_VF").text("Only JPG,JPEG,PNG,GIF,PDF,XLS,XLSX,PPT,PPTX,DOC and DOCX extension files are allowed to upload.");
            $("#divAddInforamtionDialog_VF").hide();
        }
    });


    $("#fu_BT").change(function () {
        var file = this.files[0];
        fileName = file.name;
        size = file.size;
        type = file.type;
        var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
        $("#txtuploadedMsgAdd_BT").css("color", "red");

        if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
            if (size <= 2097152) {
                $("#txtuploadedMsgAdd_BT").text("");
                $("#divBrowse_BT").hide();
                $("#btnRemove_BT").show();
                $("#file_caption_name_BT").show();
                $("#file_caption_id_BT").html(fileName.substring(0, 15));
                $("#chkFileUpload_Bt").val("2");
                var file = URL.createObjectURL(event.target.files[0]);
                $('#divPreview_BT').append('<a href="' + file + '" target="_blank">' + event.target.files[0].name + '</a>');
                $("#hdnDeletedFileUpload_BT").val('0');

            }
            else {
                $("#txtuploadedMsgAdd_BT").text("Maximum 2MB file size is allowed to upload.");
                $("#btnUploadAdd_BT").hide();
                $("#divAddInforamtionDialog_BT").hide();
            }
        }
        else {
            $("#txtuploadedMsgAdd_BT").text("Only JPG,JPEG,PNG,GIF,PDF,XLS,XLSX,PPT,PPTX,DOC and DOCX extension files are allowed to upload.");
            $("#divAddInforamtionDialog_BT").hide();
        }
    });

    $(document).on("change", "#NOCReasonID", function () {
        if ($(this).val() == 7) {
            $("#OtherReason").removeAttr("disabled");
        }
        else {
            $("#OtherReason").attr("disabled", "disabled");
        }

        if ($(this).val() == 1 || $(this).val() == 2) {
            $("#showddls").show();
            if ($("#ddlPurposeOfVisit").val() == 2 || $("#ddlPurposeOfVisit").val() == 3)
                $("#ApprovedBusinessTravelUploadFile").show();
            else
                $("#ApprovedBusinessTravelUploadFile").hide();
        }

        else {
            $("#showddls").hide();
            $("#ApprovedBusinessTravelUploadFile").hide();
        }

    });

    $(document).on("change", "#ddlPurposeOfVisit", function () {
        if ($(this).val() == "2" || $(this).val() == "3")
            $("#ApprovedBusinessTravelUploadFile").show();
        else
            $("#ApprovedBusinessTravelUploadFile").hide();
    });
});

function ValidRequiredDate(FreeZoneVisa, RequiredDate) {
    var ValidationStatus = true;
    var todayDate = new Date();
    //var TotalDays = Math.round((RequiredDate - todayDate) / (1000 * 60 * 60 * 24));
    var TotalDays = (moment($("#RequiredDate").val(), 'DD/MM/YYYY').toDate() - todayDate) / (1000 * 60 * 60 * 24);
    //if (FreeZoneVisa == true) {
    //    if (parseInt(TotalDays) < 14) {
    //        ValidationStatus = false;
    //        ShowMessage("error", "Please select the required date after two weeks for free zone visa.");
    //    }
    //}
    //else {
    //    if (parseInt(TotalDays) < 2) {
    //        ValidationStatus = false;
    //        ShowMessage("error", "Please select the required date after two days.");
    //    }
    //}
    return ValidationStatus;
}

function SaveForm($form) {
    var filePF, fileVF, fileBT;
    var FreeZoneVisa = $('#FreeZoneVisa').is(":checked");
    var RequiredDate = $("#RequiredDate").val();
    var NOCReasonID = $("#NOCReasonID").val();
    var OtherReason = $("#OtherReason").val();
    var CertificateAddressee = $("#CertificateAddressee").val();
    var CountryID = $("#CountryID").val();
    var NOCLanguage = $("#NOCLanguage").val();
    var DateOfTravel = $("#DateOfTravel").val();
    var txtComments = $("#txtComments").val();
    var UpLoadedPassID = $("#hdnFileUpload_PF").val();
    var UpLoadedVisaID = $("#hdnFileUpload_VF").val();
    var purposeOfVisit = $("#ddlPurposeOfVisit").val();
    var travelExpenseBorneBy = $("#ddlTravelExpenseBorneBy").val();
    var uploadedBTID = $("#hdnFileUpload_BT").val();
    var salaryDetail = $("#salaryDetail").val();

    var formData = new FormData();

        if ($("#fu_PF").get(0).files.length > 0) {
            filePF = $("#fu_PF").get(0).files;
            formData.append("PassportFile", filePF[0]);
        }
        else
            formData.append("PassportFileID", null);
        if ($("#fu_VF").get(0).files.length > 0) {
            fileVF = $("#fu_VF").get(0).files;
            formData.append("VisaFile", fileVF[0]);
        }
        else
            formData.append("VisaFile", null);
        if ($("#fu_BT").get(0).files.length > 0) {
            fileBT = $("#fu_BT").get(0).files;
            formData.append("ApprovedBusinessTravelFormFile", fileBT[0]);
        }
        else
            formData.append("ApprovedBusinessTravelFormFile", null);
   

    formData.append("NOCReasonID", NOCReasonID)
    formData.append("OtherReason", OtherReason);
    formData.append("CertificateAddressee", CertificateAddressee);
    formData.append("CountryID", CountryID)
    formData.append("NOCLanguage", NOCLanguage);
    formData.append("FreeZoneVisa", FreeZoneVisa);
    formData.append("RequiredDate", RequiredDate);
    formData.append("DateOfTravel", DateOfTravel);
    formData.append("Comments", txtComments);
    formData.append("PurposeOfVisit", purposeOfVisit);
    formData.append("TravelExpenseBorneBy", travelExpenseBorneBy);
    formData.append("pfileid", UpLoadedPassID);
    formData.append("vfileid", UpLoadedVisaID);
    formData.append("bfileid", uploadedBTID);
    formData.append("SalaryDetail", salaryDetail);
   
    $.ajax({
        url: '/NocRequest/SaveNocRequest',
        type: 'POST',
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData,
        success: function (result) {
            ShowMessage(result.CssClass, result.Message);
            hideLoaderFrame();
            BackToTaskList();
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

function UpdateNocRequestForm() {

    var filePF, fileVF;
    var ID = $("#ID").val();
    var FormProcessID = $("#hdnformProcessID").val();
    var FreeZoneVisa = $('#FreeZoneVisa').is(":checked");
    var RequiredDate = $("#RequiredDate").val();
    var NOCReasonID = $("#NOCReasonID").val();
    var OtherReason = $("#OtherReason").val();
    var CertificateAddressee = $("#CertificateAddressee").val();
    var CountryID = $("#CountryID").val();
    var NOCLanguage = $("#NOCLanguage").val();
    var DateOfTravel = $("#DateOfTravel").val();
    var txtComments = $("#txtComments").val();
    var ReqStatusID = $("#hdnReqStatusID").val();
    var CompanyID = $("#hdnCompanyId").val();
    var RequesterEmployeeID = $("#hdnRequesterEmployeeID").val();
    var UpLoadedPassID = $("#hdnFileUpload_PF").val();
    var UpLoadedVisaID = $("#hdnFileUpload_VF").val();
    var purposeOfVisit = $("#ddlPurposeOfVisit").val();
    var travelExpenseBorneBy = $("#ddlTravelExpenseBorneBy").val();
    var uploadedBTID = $("#hdnFileUpload_BT").val();
    var salaryDetail = $("#salaryDetail").val();

    var formData = new FormData();

    if ($("#fu_PF").get(0).files.length > 0) {
        filePF = $("#fu_PF").get(0).files;
        formData.append("PassportFile", filePF[0]);
    }
    else
        formData.append("PassportFileID", $("#hdnFileUpload_PF").val());
    if ($("#fu_VF").get(0).files.length > 0) {
        fileVF = $("#fu_VF").get(0).files;
        formData.append("VisaFile", fileVF[0]);
    }
    else
        formData.append("VisaFile", $("#hdnFileUpload_VF").val());
    if ($("#fu_BT").get(0).files.length > 0) {
        fileBT = $("#fu_BT").get(0).files;
        formData.append("ApprovedBusinessTravelFormFile", fileBT[0]);
    }
    else
        formData.append("ApprovedBusinessTravelFormFile", null);

    formData.append("ID", ID)
    formData.append("FormProcessID", FormProcessID)
    formData.append("FreeZoneVisa", FreeZoneVisa);
    formData.append("RequiredDate", RequiredDate);
    formData.append("NOCReasonID", NOCReasonID)
    formData.append("OtherReason", OtherReason);
    formData.append("CertificateAddressee", CertificateAddressee);
    formData.append("NOCLanguage", NOCLanguage);
    formData.append("DateOfTravel", DateOfTravel);
    formData.append("txtComments", txtComments);
    formData.append("ReqStatusID", ReqStatusID);
    formData.append("CompanyID", CompanyID);
    formData.append("RequesterEmployeeID", RequesterEmployeeID);
    formData.append("PurposeOfVisit", purposeOfVisit);
    formData.append("TravelExpenseBorneBy", travelExpenseBorneBy);
    formData.append("pfileid", UpLoadedPassID);
    formData.append("vfileid", UpLoadedVisaID);
    formData.append("CountryID", CountryID);
    formData.append("bfileid", uploadedBTID);
    formData.append("SalaryDetail", salaryDetail);

    $.ajax({
        url: '/NocRequest/UpdateNocRequest',
        type: 'POST',
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData,       
        success: function (result) {            
            ShowMessage(result.CssClass, result.Message);
            hideLoaderFrame();
            if (result.InsertedRowId>0) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }            
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
  
}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);

}

function ValidateData() {
    var filepf = $("#fu_PF").val();
    var filepfid = $("#hdnFileUpload_PF").val();
    var filevfid = $("#hdnFileUpload_VF").val();
    var filebtid = $("#hdnFileUpload_BT").val();
    var pffiledeleteid = $("#hdnDeletedFileUpload_PF").val();
    var vffiledeleteid = $("#hdnDeletedFileUpload_VF").val();
    var btfiledeleted = $("#hdnDeletedFileUpload_BT").val();
    var isvalid = true;
    var ispffile = true;
    var isvffile = true;
    var isbtfile = true;
    if (filepfid === '' || filepfid == '0') {
        if (filepf === '' || filepf === null || filepf === undefined) {
            isvalid = false;
            ispffile = false;
        }
    }
    if (pffiledeleteid !== '0') {
        isvalid = false;
        ispffile = false;
    }

    var filevf = $("#fu_VF").val();
    if (filevfid === '' || filevfid == '0') {
        if (filevf === '' || filevf === null || filevf === undefined) {
            isvalid = false;
            isvffile = false;
        }
    }
    if (vffiledeleteid !== '0') {
        isvalid = false;
        isvffile = false;
    }

    if ($("#ApprovedBusinessTravelUploadFile").is(":visible") && $("#ddlPurposeOfVisit").val() != 1) {
        var filebt = $("#fu_BT").val();
        if (filebtid === '' || filebtid == '0') {
            if (filebt === '' || filebt === null || filebt === undefined) {
                isvalid = false;
                isbtfile = false;
            }
        }
        if (btfiledeleted !== '0') {
            isvalid = false;
            isbtfile = false;
        }
    }

    if (isvalid)
        return true
    else {
        if (!ispffile) {
            $("#txtuploadedMsgAdd_PF").text("This field is mandatory.");
        }
        else
            $("#txtuploadedMsgAdd_PF").text("");
        if (!isvffile) {
            $("#txtuploadedMsgAdd_VF").text("This field is mandatory.");
        }
        else
            $("#txtuploadedMsgAdd_VF").text("");
        if ($("#ApprovedBusinessTravelUploadFile").is(":visible") && $("#ddlPurposeOfVisit").val() != 1) {
            if (!isbtfile) {
                $("#txtuploadedMsgAdd_BT").text("This field is mandatory.");
            }
            else
                $("#txtuploadedMsgAdd_BT").text("");
        }
        return false;
    }
}
