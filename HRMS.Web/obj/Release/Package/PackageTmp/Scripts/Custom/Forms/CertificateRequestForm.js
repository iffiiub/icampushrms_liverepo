﻿$(document).ready(function () {

    var formprocessid = $("#hdnFormProcessID").val();
    if (formprocessid == null || formprocessid == '' || formprocessid == '0') {
        checkFormLoadValidation(19, $("#hdnCompanyId").val(), "#lblCertificateRequestNotification", "CertificateRequestForm");
    }    
    $("#btnCertificateRequestWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#CertificateRequestWarningBox").remove();
    });

    bindSelectpicker(".selectpickerddl");
    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });   
    var salaryid = $("#ddlCertificateType").val();
    if (salaryid == 3) {
        $("#divCertificateReason").show();
    }
    else {       
        $("#divCertificateReason").hide();
    }

    $(document).on('change', '#ddlCertificateType', function () {
        if ($(this).val() != '') {
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                url: '/CertificateRequestForm/CheckCertificateRequestFormValidation',
                data: { certificateTypeId: $(this).val() },
                success: function (result) {
                    hideLoaderFrame();
                    if (result.InsertedRowId == 0) {
                        $('#warningMessage').show();
                        $("#lblCertificateRequestNotification").html(result.Message);
                        $(".btnSave").prop('disabled', true);
                    }
                    else {
                        $('#warningMessage').hide();
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);

                }
            });
        }      
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#frmCertificateRequestForm");
            var formprocessid = $("#hdnFormProcessID").val();
            var CertificateTypeID = $("#ddlCertificateType").val();
            $.validator.unobtrusive.parse($form);
            $form.validate();
            var IsValidFiles = ValidateData();
            if ($form.valid() && IsValidFiles) {
                pageLoaderFrame();
                if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                    $.ajax({
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: '/CertificateRequestForm/IsWorkFlowExistsByCertificate',
                        data: { CertificateTypeID: CertificateTypeID },
                        success: function (result) {
                            if (result.InsertedRowId > 0)
                                SaveForm($form);
                            else {
                                hideLoaderFrame();
                                ShowMessage(result.CssClass, result.Message);
                            }
                        },
                        error: function (err) {
                            hideLoaderFrame();
                            ShowMessage('error', err.statusText);

                        }
                    });
                }
            }
        }
    });

    $('.btnUpdate').click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#frmCertificateRequestForm");
            var formprocessid = $("#hdnFormProcessID").val();
            var CertificateTypeID = $("#ddlCertificateType").val();
            var IsValidFiles = ValidateData();
            $.validator.unobtrusive.parse($form);
            $form.validate();          
            if ($form.valid() && IsValidFiles) {
                pageLoaderFrame();
                SaveForm($form);
            }
        }
    });

    $('.btnApprove').click(function () {
        pageLoaderFrame();
        var formprocessid = $("#hdnFormProcessID").val();
        var comments = $("#txtComments").val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/CertificateRequestForm/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnFormProcessID").val();
                var comments = $("#txtComments").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/CertificateRequestForm/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        }
        else
            ShowMessage("error", "Comments field is mandatory.");
    });

    $('#txtComments').on("input", function () {
        var maxlength = $(this).attr("maxlength");
        var currentLength = $(this).val().length;
        var noofcharsleft;

        if (currentLength >= maxlength) {
            $('#lblCommentCharsLeft').text("0 chars left");
        } else {
            noofcharsleft = maxlength - currentLength;
            $('#lblCommentCharsLeft').text(noofcharsleft + " chars left");
        }
    });

});

$("#ddlCertificateType").change(function () {
    var salaryid = $("#ddlCertificateType").val();
    if (salaryid == 3) {
        $("#divCertificateReason").show();
    }
    else {
        $("#divCertificateReason").hide();
    }
});

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);

}

function ValidateData() {    
    var filepf = $("#fu_PF").val();
    var filepfid = $("#hdnFileUpload_PF").val();
    var filevfid = $("#hdnFileUpload_VF").val();
    var pffiledeleteid = $("#hdnDeletedFileUpload_PF").val();
    var vffiledeleteid = $("#hdnDeletedFileUpload_VF").val();
    var isvalid = true;
    var ispffile = true;
    var isvffile = true;
    if (filepfid === '' || filepfid == '0') {
        if (filepf === '' || filepf === null || filepf === undefined) {
            isvalid = false;
            ispffile = false;
        }
    }
    if (pffiledeleteid !== '0') {
        isvalid = false;
        ispffile = false;
    }

    var filevf = $("#fu_VF").val();
    if (filevfid === '' || filevfid == '0') {
        if (filevf === '' || filevf === null || filevf === undefined) {
            isvalid = false;
            isvffile = false;
        }
    }
    if (vffiledeleteid !== '0') {
        isvalid = false;
        isvffile = false;
    }
    if (isvalid)
        return true
    else {
        if (!ispffile) {
            $("#txtuploadedMsgAdd_PF").text("This field is mandatory.");
        }
        if (!isvffile) {
            $("#txtuploadedMsgAdd_VF").text("This field is mandatory.");
        }
        return false;
    }
}

function SaveForm($form) {    
    var filePF, fileVF;
    var CertificateId = $("#ddlCertificateType").val();
    var UpLoadedPassID = $("#hdnFileUpload_PF").val();
    var UpLoadedVisaID = $("#hdnFileUpload_VF").val();
    var CertificateType = $("#hdnCertificateType").val();
    var certificaterequestid = $("#hdnCertificateRequestID").val();
    var formprocessid = $("#hdnFormProcessID").val();
    var CertificateReason = 0;
    var formData = new FormData();

        if ($("#fu_PF").get(0).files.length > 0) {
            filePF = $("#fu_PF").get(0).files;
            formData.append("PassportFile", filePF[0]);
        }
        else
            formData.append("PassportFileID", null);
        if ($("#fu_VF").get(0).files.length > 0) {
            fileVF = $("#fu_VF").get(0).files;
            formData.append("VisaFile", fileVF[0]);
        }
        else
            formData.append("VisaFile", null);

    if (CertificateId == 3) {
        var CertificateReason = $("#ddlCertificateReason").val();
    }
    formData.append("CertificateReason", CertificateReason)
    formData.append("pfileid", UpLoadedPassID);
    formData.append("vfileid", UpLoadedVisaID);
    formData.append("Certificate", CertificateType)
    formData.append("CertificateTypeID", $("#ddlCertificateType").val())
    formData.append("CertificateRequestid", certificaterequestid);
    formData.append("FormProcessID", formprocessid);
    formData.append("hdnReqStatusID", $("#hdnReqStatusID").val());
    formData.append("BankNameAddress", $("#txtBankNameAddress").val());
    formData.append("RequestPurpose", $("#txtRequestPurpose").val());
    formData.append("Comments", $("#txtComments").val());
    $.ajax({
        url: '/CertificateRequestForm/SaveForm',
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData,
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
            else {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame
            ShowMessage('error', err.statusText);
        }
    });
}