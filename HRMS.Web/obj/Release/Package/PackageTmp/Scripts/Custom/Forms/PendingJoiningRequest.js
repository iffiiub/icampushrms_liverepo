﻿$(document).ready(function () {

    GetData(1);
});
var oTableChannel;
var isDataIsRefresh = true;

function GetData(id) {
    if (id == 1) {
        pageLoaderFrame();
    }
    isDataIsRefresh = true;
    var EmployeeId = $("#ddlEmployee").val();
    var CategoryId = $("#ddlCategory").val();
    var companyId = $("#ddlCompany").val();
    if (EmployeeId == 0) {
        EmployeeId = null;
    }
    if (CategoryId == 0) {
        CategoryId = null;
    }
    oTableChannel = $('#tblJoiningList').dataTable({
        "sAjaxSource": "/PendingJoiningRequest/GetPendingJoiningRequest",
        "aoColumns": [
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'width': '10%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'width': '15%' },
            { "mData": "DepartmentId", "bVisible": true, "sClass": "hidden" },
            { "mData": "DepartmentName", "sTitle": "Department", 'width': '15%' },
            { "mData": "Designation", "sTitle": "Designation", "bVisible": true, 'width': '10%' },
            { "mData": "Organization", "sTitle": "Organization", "bVisible": true, 'width': '15%' },
            { "mData": "CategoryId", "bVisible": true, "sClass": "hidden" },
            { "mData": "CategoryName", "sTitle": "Category", 'width': '15%' },
            { "mData": "JoiningDate", "sTitle": "Joining date", 'width': '10%' },
            { "mData": "Actions", "sTitle": "Action", "bSortable": false, 'width': '20%', "sClass": "text-center" },
            { "mData": "AutoId", "bVisible": true, "sClass": "hidden" },
            { "mData": "CompanyId", "bVisible": false, "sClass": "hidden" },
            
         ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeId", "value": EmployeeId }, { "name": "CategoryId", "value": CategoryId }, {"name":"CompanyId", "value": companyId});
        },
        "processing": false,
        "serverSide": false,
        "ajax": "/PendingJoiningRequest/GetPendingJoiningRequest",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[2, "asc"]],
        "fnDrawCallback": function () {
           
        },
        "fnInitComplete": function (oSettings) {
            hideLoaderFrame();
        }
    });
}
function ViewPendingJoiningRequestForm(id) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EmployeeID: id },
        url: '/PendingJoiningRequest/SetCurrentSessionPendingJoiningRequestForm',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/EmployeeJoiningForm/Create/";
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}