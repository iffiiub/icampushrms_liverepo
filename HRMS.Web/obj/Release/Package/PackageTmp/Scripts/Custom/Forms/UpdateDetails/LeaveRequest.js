﻿var balancedigitafterdecimal = 2;
var leaveentitletypeid = 0;
var oldfromdate = "";
var oldtodate = "";
$(document).ready(function () {
    var formid = 6;
    var formprocessid = $("#hdnFormProcessID").val();

    $("#chkHalfday").attr("disabled", 'disabled');
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $('#hdnBalanceDays').val(0);
    $('#hdnVTCategoryID').val(0);

    $("#txtToDate").datepicker({
        dateFormat: "dd/mm/yyyy",
        onSelect: function (f, d, i) {
            if (d !== i.lastVal) {
                $('#txtToDate').trigger("change");
            }
        }
    }).data("datepicker");

    //$('#txtToDate').on("change", function () {
    //    var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
    //    if (NoOfDaysDiff > 0)
    //        CalculateDays();
    //    else {
    //        $('#txtRequestedDays').val(0);
    //        GetRemainingDays();
    //    }
    //});

    $("#txtFromDate").datepicker({
        dateFormat: "dd/mm/yyyy",
        onSelect: function (f, d, i) {
            if (d !== i.lastVal) {
                $("#txtFromDate").trigger("change");
            }
        }
    }).data("datepicker");

    //$("#txtFromDate").on("change", function () {
    //    var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
    //    if (NoOfDaysDiff > 0)
    //        CalculateDays();
    //    else {
    //        //  ShowMessage("error", "From date should be less than to date.");
    //        $('#txtRequestedDays').val(0);
    //        GetRemainingDays();
    //    }
    //});

    var formprocessid = $("#hdnFormProcessID").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    var LeaveTypeID = $("#VacationTypeID").val();
    var fromdate = $("#txtFromDate").val();
    var todate = $("#txtToDate").val();

    $(':input').each(function () {
        if ($(this).prop("id") != 'ddlLeaveType') //not taking values from company id
            $(this).data('initialValue', $(this).val());
    });

    $('.btnSave').click(function () {
        SaveForm();
    });

    $('.leave-dates').on('mouseleave', function () {
        $("#txtFromDate").datepicker("setDate", $("#txtFromDate").val());
        $("#txtToDate").datepicker("setDate", $("#txtToDate").val());
    });

});

function ValidateData() {
    $("#txtFromDatelbel").css("color", "red");
    $("#txtToDatelbel").css("color", "red");
    var date1 = $("#txtFromDate").val();
    var date2 = $("#txtToDate").val();
    //for update if file already there filejdid will have id value

    if (date1 === '' || date1 === null || date1 === undefined) {
        $("#txtFromDatelbel").text("This field is mandatory.");
        return false;
    }
    if (date2 === '' || date2 === null || date2 === undefined) {
        $("#txtToDatelbel").text("This field is mandatory.");
        return false;
    }
    return true;
}

function SaveForm() {
    if (window.FormData !== undefined) {
        var fileMC;
        var HalfDay;
        var In;
        var Out;
        if (ValidateData()) {
            var $form = $("#frmEmployeeLeaveRequestForm");
            var formprocessid = $("#hdnFormProcessID").val();
            var reqstatusid = $("#hdnReqStatusID").val();
            var leavetypeid;
            leavetypeid = $("#hdnVacationTypeID").val();
            if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined)
                formprocessid = 0;
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid()) {
                var employeeid = $("#hdnRequesterEmployeeID").val();
                var fromdate = $("#txtFromDate").val();
                var todate = $("#txtToDate").val();
                var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
                var applieddays = vacation.NoOfDays;

                var actualrequestdays = $("#hdnRequestedDays").val();
                if (applieddays != actualrequestdays) {
                    ShowMessage("error", "Any modification in dates should be equal to Requested Days (" + actualrequestdays + ")");
                    //ShowMessage("error", "Requested days (" + applieddays + ") must be same as Initial Request Days(" + actualrequestdays + ")");
                    return false;
                }

                var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
                if (parseInt(NoOfDaysDiff) <= 0) {
                    ShowMessage("error", "From date must be a date less than to date .");
                    return false;
                }

                var isleaveexists = IsLeaveDatesAlreadyExists();
                if (isleaveexists) {
                    ShowMessage("error", "Requested leave days are already taken, please select another date.");
                    return true;
                }

                var TaskHandOverEmp1 = $("#ddlEmployee1").val();
                var TaskHandOverEmp2 = $("#ddlEmployee2").val();
                var TaskHandOverEmp3 = $("#ddlEmployee3").val();
                var TaskHandOverEmp4 = $("#ddlEmployee4").val();
                var TaskHandOverEmp5 = $("#ddlEmployee5").val();
                var TaskDetail1 = $("#txtTaskDetail1").val();
                var TaskDetail2 = $("#txtTaskDetail2").val();
                var TaskDetail3 = $("#txtTaskDetail3").val();
                var TaskDetail4 = $("#txtTaskDetail4").val();
                var TaskDetail5 = $("#txtTaskDetail5").val();
                var TaskHandOverID1 = $("#txtTaskDetail1").attr("taskhandoverid");
                var TaskHandOverID2 = $("#txtTaskDetail2").attr("taskhandoverid");
                var TaskHandOverID3 = $("#txtTaskDetail3").attr("taskhandoverid");
                var TaskHandOverID4 = $("#txtTaskDetail4").attr("taskhandoverid");
                var TaskHandOverID5 = $("#txtTaskDetail5").attr("taskhandoverid");

                var updatedData = [];
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp1,
                    TaskDetail: TaskDetail1,
                    TaskHandOverID: TaskHandOverID1
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp2,
                    TaskDetail: TaskDetail2,
                    TaskHandOverID: TaskHandOverID2
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp3,
                    TaskDetail: TaskDetail3,
                    TaskHandOverID: TaskHandOverID3
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp4,
                    TaskDetail: TaskDetail4,
                    TaskHandOverID: TaskHandOverID4
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp5,
                    TaskDetail: TaskDetail5,
                    TaskHandOverID: TaskHandOverID5
                });
               
                var formData = new FormData($form[0]);
                formData.append("updatedData", JSON.stringify(updatedData));
                var FromDate = $('#txtFromDate').val();
                var ToDate = $('#txtToDate').val();               

                pageLoaderFrame();
                $.ajax({
                    url: '/EmployeeLeaveRequest/UpdateLeaveRequest',
                    type: "POST",
                    contentType: false, // Not to set any content header  
                    processData: false, // Not to process data  
                    data: formData,
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.CssClass != 'error') {
                            location.reload();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame()
                        ShowMessage('error', err.statusText);
                    }
                });
            }
        }
    }
}

function IsLeaveDatesAlreadyExists() {
    var isexists = false;
    var fromdate = $("#txtFromDate").val();
    var todate = $("#txtToDate").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    var LeaveRequestID = $("#ID").val();
    $.ajax({
        // dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/IsLeaveDatesAlreadyExistsForCurrentReq',
        data: { employeeID: employeeid, fromDate: fromdate, toDate: todate, LeaveRequestID: LeaveRequestID },
        async: false,
        success: function (result) {
            if (result.Success == true) {

                isexists = result.Success;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return isexists;
}

function GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate) {
    var vacationmodel = null;
    // pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetValidLeaveRequestDates',
        data: { employeeID: employeeid, vacationTypeID: leavetypeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            vacationmodel = result;
            // hideLoaderFrame();
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
            //  hideLoaderFrame();
        }
    });
    return vacationmodel;
}