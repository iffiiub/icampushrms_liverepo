﻿$(document).ready(function () {
    loadCertificatePrintingGrid();
});

function loadCertificatePrintingGrid() {

    oTableChannel = $('#tblCertificatePrintingGrid').dataTable({
        "sAjaxSource": "/CertificatePrinting/GetCertificatePrintingScreenData",
        "fnServerParams": function (aoData) {
        },
        "aoColumns": [
            { "mData": "ComapanyName", "sTitle": "Organization", 'sWidth': '10%' },    
            { "mData": "RequestID", "sTitle": "Request No.", 'sWidth': '5%' }, 
            { "mData": "DocumentType", "sTitle": "Document Type", 'sWidth': '5%' }, 
            { "mData": "RequestDate", "sTitle": "Request Date", 'sWidth': '10%' }, 
            { "mData": "EmployeeName", "sTitle": "Requester", 'sWidth': '15%' }, 
            { "mData": "EmployeeAlternativeID", "sTitle": "Oracle No.", 'sWidth': '10%' },
            { "mData": "CertificateAddress", "sTitle": "Certificate Address", 'sWidth': '15%' },
            { "mData": "CertificateType", "sTitle": "Certificate Type", 'sWidth': '15%' },
            { "mData": "Action", "sTitle": "", 'sWidth': '10%', "sClass": "text-center" },
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 25,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function printCertificateTemplate(formId, templateId, id) {
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/CertificatePrinting/PrintCertificateTemplate',
        data: { formId: formId, templateId: templateId, id: id},
        success: function (result) {
            if (result.success === true)
                window.location.assign('/CertificatePrinting/DownloadPrintCertificate?fileName=' + result.fileName + '&downloadFileName=' + result.downloadFileName);
            else
                ShowMessage('error', "some error has occured");
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}