﻿
function validateComments(element) {
    var IsValidateComment = true;
    if ($(element).val() == '') {
        if ($(element).parent().find('.field-validation-error').length == 0)
            $(element).parent().append('<span class=\"field-validation-error\">This is mandatory field.</span>');
        IsValidateComment = false;
    }
    else if ($(element).parent().find('.field-validation-error').length > 0) {
        $(element).parent().find('.field-validation-error')[0].remove();
    }
    return IsValidateComment;
}

$(".CheckKeyReleaseComments").on('keyup paste', function () {
    if ($(this).val().length > 0) {
        if ($(this).parent().find('.field-validation-error').length > 0)
            $(this).parent().find('.field-validation-error')[0].remove();
    }
});

function checkFormLoadValidation(formId, companyId, labelId, controllerName) {
    pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        url: '/' + controllerName +'/CheckFormLoadValidation',
        data: {
            formID: formId, companyId: companyId
        },
        success: function (result) {
            hideLoaderFrame();
            if (result.InsertedRowId == 0) {
                $('#warningMessage').show();
                $(labelId).html(result.Message);
                $(".btnSave").prop('disabled', true);
            }
            else {
                $('#warningMessage').hide();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

//All Requests related

$('.btnBackToAllRequest').click(function () {
    BackToAllRequestList();
});

function BackToAllRequestList()
{
    location.href = "/AllRequests/Index/";
}