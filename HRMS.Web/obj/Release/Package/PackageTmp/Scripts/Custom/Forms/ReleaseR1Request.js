﻿$(document).ready(function () {
    initReleaseR1RequestList();
});


function initReleaseR1RequestList() {
    var ReleaseR1RequestList = $('#tblReleaseR1Request').DataTable(
        {
            "bFilter": true,
            "bInfo": true,
            "bSortCellsTop": true,
            "bStateSave": false,
            "aaSorting": [[1, 'asc']]
        }
    );
}

$(document).on("click", "#btnSearch", function () {
    pageLoaderFrame();
    LoadReleaseR1RequestGrid(true);
});

function LoadReleaseR1RequestGrid(IsStartLoader) {
    if (IsStartLoader) {
        pageLoaderFrame();
    }
    $.ajax({
        type: 'GET',
        url: '/ReleaseR1Request/GetR1ReleaseByR1RequestID',
        data: { R1RequestID: $("#ddlReleaseR1Request").val() },
        async: false,
        success: function (result) {
            hideLoaderFrame();
            $("#EmployeeReleaseGrid").html(result);
            initReleaseR1RequestList();
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

function UpdateReleaseR1Request(R1RequestID, FormProcessID, FormID) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to release this R1 Request?" }).done(function () {
        pageLoaderFrame();
        $.ajax({
            url: '/ReleaseR1Request/UpdateReleaseR1Request',
            data: { R1RequestID: R1RequestID },
            success: function (result) {
                if (result.InsertedRowId > 0) {
                    LoadReleaseR1RequestGrid(false);
                    ShowMessage('success', 'R1 Request ID ('+R1RequestID+') released successfully');
                   
                }
                else
                {
                    ShowMessage('error','Unable to release R1 Request');
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });
}

function OpenFromRequest(formprocessid,formid)
{
    $.ajax({
        url: '/ReleaseR1Request/SetFormProcessSesssion',
        data: { id: formprocessid },
        success: function (result) {
            if (result.id > 0) {
               // LoadReleaseR1RequestGrid(false);
                var urlString = ''
                if (formid === 1) {
                    urlString = '/HiringRequisitionR1BudgetedForm/ViewDetails';
                }
                else if (formid === 2) {
                    urlString = '/HiringRequisitionR1UnBudgetedForm/ViewDetails';
                }
                else if (formid === 3) {
                    urlString = '/HiringR1Replacement/ViewDetails';
                }
                else if (formid === 29) {
                    urlString = '/R1UnBudgetedReplacement/ViewDetails';
                }
                else if (formid === 4) {
                    urlString = '/EmployeeProfileCreationForm/ViewDetails';
                }
                if (urlString != '') {
                    window.open(urlString, '_blank');
                }
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}
