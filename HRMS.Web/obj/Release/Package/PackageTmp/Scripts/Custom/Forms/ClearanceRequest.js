﻿var formid = 14;
$(document).ready(function () {

    $('.DecimalOnly').keypress(function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('.YNClass').each(function () {
        $(this).attr('data-initialValue', $(this).is(":checked"));
    });

    $('.DecimalOnly').each(function () {
        $(this).attr('data-initialValue', $(this).val());
    });

    $('.Remarks').each(function () {
        $(this).attr('data-initialValue', $(this).val());
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {

            var formprocessid = $("#hdnFormProcessID").val();
            var companyIds = $("#hdnUserCompanyID").val();
            var IsValidOptionAction = true;
            var IsExistsClearanceRequest = true;
            $("table tbody tr").each(function () {
                if (($(this).find('.YNClass').prop('disabled') == false && $(this).find('.YNClass').is(":checked") == false) && ($(this).find('.NAClass').prop('disabled') == false && $(this).find('.NAClass').is(":checked") == false)) {
                    IsValidOptionAction = false;
                }
            });

            $.ajax({
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                url: '/ClearanceRequest/IsExistsClearanceRequest',
                data: { EmployeeID: $("#EmployeeID").val() },
                async: false,
                success: function (result) {
                    if (result.Success == false) {
                        ShowMessage(result.CssClass, result.Message);
                        IsExistsClearanceRequest = false;
                    }
                },
                error: function (err) {
                    ShowMessage('error', err.statusText);
                }
            });

            if (!IsExistsClearanceRequest) {
                return false;
            }

            if (IsValidOptionAction) {
                pageLoaderFrame();
                if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                    $.ajax({
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: '/ClearanceRequest/IsWorkFlowExists',
                        data: { formID: formid, companyIDs: companyIds },
                        success: function (result) {
                            if (result.InsertedRowId > 0)
                                SaveForm();
                            else {
                                ShowMessage(result.CssClass, result.Message);
                                hideLoaderFrame();
                            }
                        },
                        error: function (err) {
                            hideLoaderFrame();
                            ShowMessage('error', err.statusText);

                        }
                    });
                }
            }
            else {
                ShowMessage("error", "*All options are required.");
            }
        }
    });

    $('.btnUpdate').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var companyIds = $("#hdnUserCompanyID").val();
            var reqstatusid = $("#hdnReqStatusID").val();
            var IsValidOptionAction = true;
            $("table tbody tr").each(function () {
                if (($(this).find('.YNClass').prop('disabled') == false && $(this).find('.YNClass').is(":checked") == false) && ($(this).find('.NAClass').prop('disabled') == false && $(this).find('.NAClass').is(":checked") == false)) {
                    IsValidOptionAction = false;
                }
            });
            //var IsComPenReq = $("#IsComPenReq").val();
           
            //if (!IsValidOptionAction) {
            //    if (IsComPenReq.toLocaleLowerCase() == "true") {
            //        IsValidOptionAction = true;
            //    }
            //}

            if (IsValidOptionAction) {
                pageLoaderFrame();
                var formprocessid = $("#hdnFormProcessID").val();
                var ID = $("#ID").val();
                var Comments = $("#txtComments").val();
                var RequesterEmployeeID = $("#RequesterEmployeeID").val();
                var ClearanceRequestOptionsModel = [];
                $("table tbody tr").each(function () {
                    ClearanceRequestOptionsModel.push({
                        ID: $(this).find("td").eq(0).data("id"),
                        ClearanceDetailID: $(this).find("td").eq(0).data("optiondetailid"),
                        ClearanceOptionID: $(this).find("td").eq(0).data("optionid"),
                        ClearanceMark: $(this).find('.YNClass').is(":checked"),
                        Amount: $(this).find(".DecimalOnly").val(),
                        Remarks: $(this).find(".Remarks").val()
                    });
                });                
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/ClearanceRequest/UpdateForm',
                    data: { listclearanceRequestOptionsModel: JSON.stringify(ClearanceRequestOptionsModel), formprocessid: formprocessid, ID: ID, Comments: Comments, RequesterEmployeeID: RequesterEmployeeID, ReqStatusID: reqstatusid },
                    success: function (result) {
                        ShowMessage(result.CssClass, result.Message);
                        hideLoaderFrame();
                        location.reload();
                        //if (IsComPenReq.toLocaleLowerCase() == "true") {
                        //    setTimeout(function () { BackToTaskList(); }, 1000);
                        //}
                       
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
            else {
                ShowMessage("error", "*All options are required.");
            }
        }
    });

    $('.btnUpdateSave').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var companyIds = $("#hdnUserCompanyID").val();
            var reqstatusid = $("#hdnReqStatusID").val();
            var IsValidOptionAction = true;
            $("table tbody tr").each(function () {
                if (($(this).find('.YNClass').prop('disabled') == false && $(this).find('.YNClass').is(":checked") == false) && ($(this).find('.NAClass').prop('disabled') == false && $(this).find('.NAClass').is(":checked") == false)) {
                    IsValidOptionAction = false;
                }
            });

            if (IsValidOptionAction) {
                pageLoaderFrame();
                var formprocessid = $("#hdnFormProcessID").val();
                var ID = $("#ID").val();
                var Comments = $("#txtComments").val();
                var RequesterEmployeeID = $("#RequesterEmployeeID").val();
                var ClearanceRequestOptionsModel = [];
                $("table tbody tr").each(function () {
                    if (($(this).find('.YNClass').prop('disabled') == false && $(this).find('.NAClass').prop('disabled') == false) && ($(this).find('.YNClass').is(":checked") == true || $(this).find('.NAClass').is(":checked") == true)) {
                        ClearanceRequestOptionsModel.push({
                            ID: $(this).find("td").eq(0).data("id"),
                            ClearanceDetailID: $(this).find("td").eq(0).data("optiondetailid"),
                            ClearanceOptionID: $(this).find("td").eq(0).data("optionid"),
                            ClearanceMark: $(this).find('.YNClass').is(":checked"),
                            Amount: $(this).find(".DecimalOnly").val(),
                            Remarks: $(this).find(".Remarks").val()
                        });
                    }
                });
                console.log(ClearanceRequestOptionsModel);
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/ClearanceRequest/UpdateSaveForm',
                    data: { listclearanceRequestOptionsModel: JSON.stringify(ClearanceRequestOptionsModel), formprocessid: formprocessid, ID: ID, Comments: Comments, RequesterEmployeeID: RequesterEmployeeID, ReqStatusID: reqstatusid },
                    success: function (result) {
                        ShowMessage(result.CssClass, result.Message);
                        hideLoaderFrame();
                        if (result.InsertedRowId > 0) {
                            sessionStorage.setItem("UpdateSaveClerance", true);
                            setTimeout(function () { location.href = "/ClearanceRequest/Edit/" }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
            else {
                ShowMessage("error", "*All options are required.");
            }
        }
    });

    $('.btnApprove').click(function () {
        var formprocessid = $("#hdnFormProcessID").val();
        var comments = $("#txtComments").val();
        var ischanged = false;
        var RequiredAllOption = false;
        $("table tbody tr").each(function () {
            if (!ischanged) {

                if (($(this).find('.YNClass').prop('disabled') == false && $(this).find('.NAClass').prop('disabled') == false) && ($(this).find('.YNClass').is(":checked") == false && $(this).find('.NAClass').is(":checked") == false)) {
                    RequiredAllOption = true;                   
                }

                if ($(this).find('.YNClass').prop('disabled') == false && $(this).find('.YNClass').attr('data-initialValue') != $(this).find('.YNClass').is(":checked").toString()) {
                    ischanged = true;
                }

                if ($(this).find('.YNClass').prop('disabled') == false && $(this).find('.DecimalOnly').attr('data-initialValue') != $(this).find('.DecimalOnly').val()
                   && $(this).find('.DecimalOnly').val() != '') {
                    ischanged = true;
                }

                if ($(this).find('.YNClass').prop('disabled') == false && $(this).find('.Remarks').attr('data-initialValue') != $(this).find('.Remarks').val()
                    && $(this).find('.Remarks').val() != '') {
                    ischanged = true;
                }

            }
        });
        if (RequiredAllOption) {
            ShowMessage("error", "*All options are required.");
            return false;
        }
        else if (ischanged) {
            globalFunctions.showWarningMessage("You must save record and then Approve!!");
            return false;
        }
        else {
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/ClearanceRequest/ApproveForm',
                data: { formProcessID: formprocessid, comments: comments },
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (result.InsertedRowId > 0) {
                        sessionStorage.setItem("UpdateSaveClerance", null);
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    });

    $('.btnReject').on('click', function () {
        var comments = $("#txtComments").val();
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnFormProcessID").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/ClearanceRequest/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        }
        else
            ShowMessage("error", "Comments field is mandatory.");
    });

});

function ChangeOptionAction(element, mode) {
    if (mode == 1) {
        if ($(element).is(":checked")) {
            $(element).closest('td').next('td').find('.YNClass').prop('checked', false)
        }
        else {
            $(element).closest('td').next('td').find('.YNClass').prop('checked', true)
        }
    }
    else {
        if ($(element).is(":checked")) {
            $(element).closest('td').prev('td').find('.NAClass').prop('checked', false)
        }
        else {
            $(element).closest('td').prev('td').find('.NAClass').prop('checked', true)
        }
    }
}

function SaveForm() {
    var formprocessid = $("#hdnFormProcessID").val();
    var ClearanceRequestOptionsModel = [];

    $("table tbody tr").each(function () {
        if (($(this).find('.YNClass').prop('disabled') == false && $(this).find('.NAClass').prop('disabled') == false) && ($(this).find('.YNClass').is(":checked") == true || $(this).find('.NAClass').is(":checked") == true)) {
            ClearanceRequestOptionsModel.push({
                ClearanceOptionID: $(this).find("td").eq(0).data("optionid"),
                ClearanceMark: $(this).find('.YNClass').is(":checked"),
                Amount: $(this).find(".DecimalOnly").val(),
                Remarks: $(this).find(".Remarks").val()
            });
        }
    });
    console.log(ClearanceRequestOptionsModel);
    var clearanceRequestModel = {
        ID: $("#ID").val(),
        CompanyID: $("#CompanyID").val(),
        EmployeeID: $("#EmployeeID").val(),
        Comments: $("#txtComments").val(),
        ClearanceRequestOptionsModel: ClearanceRequestOptionsModel
    }

    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/ClearanceRequest/SaveForm',
        data: { stringClearanceRequestModel: JSON.stringify(clearanceRequestModel) },
        success: function (result) {
            ShowMessage(result.CssClass, result.Message);
            hideLoaderFrame();
            setTimeout(function () { BackToSeparationList(); }, 1000);
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });

}

function BackToSeparationList() {
    location.href = "/SeparatedEmployeeList/Index"
}

