﻿$(document).ready(function () {
    var formid = 2;
    $(':input').each(function () {
        if ($(this).prop("id") != 'ddlCompanyList') //not taking values from company id
            $(this).data('initialValue', $(this).val());

    });

    var formprocessid = $("#hdnFormProcessID").val();
    var reqstatusid = $("#hdnReqStatusID").val();
    //var hdnEditCompletedFormRequest = $("#hdnEditCompletedFormRequest").val();
    //var IsEditRequestFromAllRequests = $("#IsEditRequestFromAllRequests").val();
    //if (reqstatusid == "3" || (IsEditRequestFromAllRequests.toLocaleLowerCase() == "true" && hdnEditCompletedFormRequest.toLocaleLowerCase() == "true")) {
    if (reqstatusid == "3") {
        $(".btnApprove").remove();
        $(".btnReject").remove();
        if (reqstatusid == "3") {
            $(".btnSave").prop("title", "Re-Submit Request");
            $('button#btnSave').html("<i class='fa fa-floppy-o'></i> Re-Submit");
        }
        //else if (hdnEditCompletedFormRequest.toLocaleLowerCase() == "true") {
        //    $(".btnSave").prop("title", "Update Request");
        //    $('button#btnSave').html("<i class='fa fa-floppy-o'></i> Update");
        //}
      
        $("#txtHeadCount").attr("disabled", "disabled");
        if ($("#ddlContractStatus").val() === 'Family')
            $("#divSpouse").show();
        else
            $("#divSpouse").hide();
        if ($("#ddlSalikTag").val() === 'true')
            $("#divSalikTag").show();
        else
            $("#divSalikTag").hide();

        if ($("#ddlPetrolCard").val() === 'true')
            $("#divPetrolCard").show();
        else
            $("#divPetrolCard").hide();

        if ($("#ddlParkingCard").val() === 'true')
            $("#divParking").show();
        else
            $("#divParking").hide();
    }
    else if (formprocessid != null && formprocessid != '' && formprocessid != '0') {
        $(".btnSave").remove();
        $("#ddlSalaryRanges").val($("#hdnSalaryRanges").val());
        $("#ddlSalaryRanges").selectpicker("refresh");
        $(":checkbox[value=multiselect-all]").attr("disabled", true);
        $('ul.multiselect-container input[type=checkbox]').attr("disabled", true);
        $("#txtHeadCount").attr("disabled", "disabled");
        $("#ddlPosition").attr("disabled", "disabled");
        $("#ddlSalaryRanges").attr("disabled", "disabled");
        $("#txtActualBudget").attr("disabled", "disabled");       
        $("#ddlCompanyList").attr("disabled", "disabled");
        $("#ddlAcademicYear").attr("disabled", "disabled");
        $("#ddlEmploymentMode").attr("disabled", "disabled");
        $("#ddlUserType").attr("disabled", "disabled");
        $("#ddlJobCategoryID").attr("disabled", "disabled");
        $("#ddlDepartment").attr("disabled", "disabled");
        $("#txtProjectData").attr("disabled", "disabled");
        $("#ddlHMEmploye").attr("disabled", "disabled");
        $("#txtPositionLocation").attr("disabled", "disabled");
        $("#txtHeadCount").attr("disabled", "disabled");
        $("#ddlJobGrade").attr("disabled", "disabled");
        $("#ddlReportingEmploye").attr("disabled", "disabled");
        $("#txtMinExpRequired").attr("disabled", "disabled");
        $("#ddlSource").attr("disabled", "disabled");
        $("#ddlRecruitFrom").attr("disabled", "disabled");
        $("#DivisionID").attr("disabled", "disabled");
        $("#ddlPosition").attr("disabled", "disabled");
        $("#ddlSalaryRanges").attr("disabled", "disabled");
        $("#ddlVehicleToolTrade").attr("disabled", "disabled");
        $("#ddlContractStatus").attr("disabled", "disabled");
        $("#ddlAnnualAirTicket").attr("disabled", "disabled");
        $("#ddlAirfareFrequency").attr("disabled", "disabled");
        $("#ddlAirfareClass").attr("disabled", "disabled");
        $("#ddlHealthInsurance").attr("disabled", "disabled");
        $("#ddlLifeInsurance").attr("disabled", "disabled");
        $("#ddlSalikTag").attr("disabled", "disabled");
        $("#ddlPetrolCard").attr("disabled", "disabled");
        $("#txtPetrolCardAmount").attr("disabled", "disabled");
        $("#ddlParkingCard").attr("disabled", "disabled");
        $("#txtParkingAreas").attr("disabled", "disabled");
        $("#btnRemove_JD").attr("disabled", "disabled");
        $("#btnRemove_OC").attr("disabled", "disabled");
        $("#btnRemove_MP").attr("disabled", "disabled");
        $("#ddlFamilySpouse").attr("disabled", "disabled");
        $("#ddlJobGrade").attr("disabled", "disabled");

        if ($("#ddlContractStatus").val() === 'Family')
            $("#divSpouse").show();
        else
            $("#divSpouse").hide();
        if ($("#ddlSalikTag").val() === 'true')
            $("#divSalikTag").show();
        else
            $("#divSalikTag").hide();

        if ($("#ddlPetrolCard").val() === 'true')
            $("#divPetrolCard").show();
        else
            $("#divPetrolCard").hide();

        if ($("#ddlParkingCard").val() === 'true')
            $("#divParking").show();
        else
            $("#divParking").hide();

    }
    else {
        $("#txtActualBudget").val('');
        $("#divSpouse").hide();
        $("#divSalikTag").hide();
        $("#divPetrolCard").hide();
        $("#divParking").hide();
    }

    $('.DecimalOnly').keypress(function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var companyId = $("#ddlCompanyList").val();
            var actualbudget = 0;
            var maxbudget = 0;
            var $form = $("#frmHiringRequisitionR1UnBudgetedForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid() && ValidateData()) {
                if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                    //Save                  
                    formid = 2;
                    $.ajax({
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: '/HiringRequisitionR1UnBudgetedForm/IsWorkFlowExists',
                        data: { formID: formid, companyIDs: companyId.toString() },
                        success: function (result) {
                            hideLoaderFrame();
                            if (result.InsertedRowId > 0)
                                SaveForm($form);
                            else
                                ShowMessage(result.CssClass, result.Message);
                        },
                        error: function (err) {
                            hideLoaderFrame();
                            ShowMessage('error', err.statusText);

                        }
                    });
                }
                else // Update
                    SaveForm($form);
            }
            else {
                ValidateData();
            }
        }
    });

    $('.btnApprove').click(function () {
        var reqstatusid = $("#hdnReqStatusID").val();
        if (reqstatusid == 3) {
            var ischanged = false;
            $(':input').each(function () {
                if ($(this).prop("id") != 'ddlCompanyList' && $(this).prop("id") != 'txtComments') {
                    if ($(this).data('initialValue') != $(this).val()) {
                        ischanged = true;
                    }
                }
            });
            if (ischanged) {
                globalFunctions.showWarningMessage("Unsaved changes in form please Save and then Approve!!");
                return;
            }
        }
        pageLoaderFrame();
        var formprocessid = $("#hdnFormProcessID").val();
        var comments = $("#txtComments").val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/HiringRequisitionR1UnBudgetedForm/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {

                var formprocessid = $("#hdnFormProcessID").val();
                var comments = $("#txtComments").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/HiringRequisitionR1UnBudgetedForm/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        }
        else
            ShowMessage('error', 'Comments field is mandatory.');
    });

    $('.btnBack').click(function () {
        BackToTaskList();
    });

    $("#ddlHMEmploye").change(function () {

        $("#txtPosition").val('');
        var empid = $(this).val();
        var Url = "/Common/GetEmployeeDesignation?employeeID=" + empid;

        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: Url,
            success: function (result) {

                $("#txtPosition").val(result);
            }
        });
        //});
    });

    $("#ddlPosition").change(function () {
        // GetSalaryRanges();
    });

    //$("#ddlSalaryRanges").change(function () {
    //    var salrangeid = $("#ddlSalaryRanges").val();
    //   // GetSalaryRangeDetails(salrangeid);
    //});

    $('#ddlCompanyList').change(function (event) {

        var companyId = $('#ddlCompanyList').val();

        var Url = "/HiringRequisitionR1UnBudgetedForm/GetAllEmployeeForAdminByCompanyID?CompanyId=" + companyId;

        $.ajax({
            url: Url, success: function (result) {


                $('#ddlHMEmploye').children('option[value!=""]').remove();
                $('#ddlReportingEmploye').children('option[value!=""]').remove();

                $.each(result, function (key, value) {
                    $('#ddlHMEmploye')
                        .append($("<option> </option>")
                            .attr("value", value.EmployeeID)
                            .text(value.FullName));

                    $('#ddlReportingEmploye')
                        .append($("<option> </option>")
                            .attr("value", value.EmployeeID)
                            .text(value.FullName));
                });

                $("#ddlHMEmploye").selectpicker("refresh");
                $("#ddlReportingEmploye").selectpicker("refresh");
            }
        });
        //GetSalaryRanges();

    });

    function GetSalaryRanges() {
        var positionId = $('#ddlPosition').val();
        var companyId = $("#ddlCompanyList").val();
        $('#ddlSalaryRanges').find('option').remove();
        $('#ddlSalaryRanges')
            .append($("<option> </option>")
                .attr("value", "")
                .text("Select Salary Range"));
        if (companyId !== '' && positionId !== null && positionId !== '') {
            $.ajax({
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: { "companyID": companyId, "positionID": positionId },
                url: '/Common/GetSalaryRange',
                success: function (result) {

                    $.each(result, function (key, value) {

                        $('#ddlSalaryRanges')
                            .append($("<option> </option>")
                                .attr("value", value.SalaryRangesID)
                                .text(value.SalaryRanges));

                    });
                    $("#ddlSalaryRanges").selectpicker("refresh");
                }
            });
        }
        else {
            $("#ddlSalaryRanges").selectpicker("refresh");
        }
    }

    function GetSalaryRangeDetails(id) {
        if (id !== null && id !== '' && id != undefined) {
            $.ajax({
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                url: '/SalaryRange/GetSalaryRangeByID?salaryRangeID=' + id,
                success: function (result) {
                    $("#hdnMaximumSalary").val(result.MaxSalary);
                }
            });
        }
        else
            $("#hdnMaximumSalary").val(0);
    }

    $("#txtHeadCount").val(1);

    $("#ddlContractStatus").change(function () {
        if ($(this).val() === 'Family') {
            $("#divSpouse").show();
        } else {
            $("#ddlFamilySpouse").val('');
            $("#divSpouse").hide();
        }
    });

    $("#ddlSalikTag").change(function () {
        if ($(this).val() == "true") {
            $("#divSalikTag").show();
        } else {
            $("#txtSalikAmount").val(0);
            $("#divSalikTag").hide();

        }
    });

    $("#ddlPetrolCard").change(function () {
        if ($(this).val() == "true") {
            $("#divPetrolCard").show();
        } else {
            $("#txtPetrolCardAmount").val(0);
            $("#divPetrolCard").hide();

        }
    });

    $("#ddlParkingCard").change(function () {

        if ($(this).val() == "true") {
            $("#divParking").show();
        } else {
            $("#divParking").hide();

        }
    });


    $("#ddlAnnualAirTicket").change(function () {
        if ($(this).val() === 'false') {
            $("#ddlAirfareFrequency").val("0");
            $("#ddlAirfareClass").val("0");
            RefreshSelect('#ddlAirfareFrequency');
            RefreshSelect('#ddlAirfareClass');
            $("#ddlAirfareFrequency").attr("disabled", "disabled");
            $("#ddlAirfareClass").attr("disabled", "disabled");
        } else {
            $("#ddlAirfareFrequency").removeAttr("disabled");
            $("#ddlAirfareClass").removeAttr("disabled");
            $("#ddlAirfareFrequency").val("");
            $("#ddlAirfareClass").val("");
            RefreshSelect('#ddlAirfareFrequency');
            RefreshSelect('#ddlAirfareClass');
        }
    });

});

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);
}

function SaveForm($form) {

    var fileJD, fileOC, fileMP;
    //var companyId = $("#ddlCompanyList").val();
    var formprocessid = $("#hdnFormProcessID").val();
    var recruitr1budgetedid = $("#hdnRecruitR1BudgetedID").val();
    var deletedfileids = "";

    pageLoaderFrame();
    var formData = new FormData($form[0]);
    if ($("#fu_JD").get(0).files.length > 0) {
        fileJD = $("#fu_JD").get(0).files;
        formData.append("JobDescription", fileJD[0]);
    }
    else
        formData.append("JobDescription", null);

    if ($("#fu_OC").get(0).files.length > 0) {
        fileOC = $("#fu_OC").get(0).files;
        formData.append("OrganizationChart", fileOC[0]);
    }
    else
        formData.append("OrganizationChart", null);

    if ($("#fu_MP").get(0).files.length > 0) {
        fileMP = $("#fu_MP").get(0).files;
        formData.append("ManpowerPlan", fileMP[0]);
    }
    else
        formData.append("ManpowerPlan", null);

    //formData.append("CompanyID", companyId);
    formData.append("JobDescriptionFileID", $("#hdnFileUpload_JD").val());
    formData.append("OrgChartFileID", $("#hdnFileUpload_OC").val());
    formData.append("ManPowerFileID", $("#hdnFileUpload_MP").val());
    formData.append("RecruitR1BudgetedID", recruitr1budgetedid);
    formData.append("FormProcessID", formprocessid);

    $.ajax({
        url: '/HiringRequisitionR1UnBudgetedForm/SaveForm',
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
            else {
                setTimeout(function () { location.href = "/HiringRequisitionR1UnBudgetedForm/Edit/" }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });

}

function ValidateData() {
    $("#txtuploadedMsgAdd_OC").css("color", "red");
    $("#txtuploadedMsgAdd_JD").css("color", "red");
    $("#txtuploadedMsgAdd_MP").css("color", "red");

    var filejd = $("#fu_JD").val();
    var filempid = $("#hdnFileUpload_MP").val();
    var fileocid = $("#hdnFileUpload_OC").val();
    var filejdid = $("#hdnFileUpload_JD").val();
    //if the file is removed these hiddenfield will hold its ids
    var jdfiledeleteid = $("#hdnDeletedFileUpload_JD").val();
    var ocfiledeleteid = $("#hdnDeletedFileUpload_OC").val();
    var mpfiledeleteid = $("#hdnDeletedFileUpload_MP").val();
    var isvalid = true;
    var isjdfile = true;
    var isocfile = true;
    var ismpfile = true;
    //for update if file already there filejdid will have id value
    if (filejdid === '' || filejdid == '0') {
        if (filejd === '' || filejd === null || filejd === undefined) {
            isvalid = false;
            isjdfile = false;
        }
    }
    if (jdfiledeleteid !== '0') {
        isvalid = false;
        isjdfile = false;
    }

    var fileoc = $("#fu_OC").val();
    if (fileocid === '' || fileocid == '0') {
        if (fileoc === '' || fileoc === null || fileoc === undefined) {
            isvalid = false;
            isocfile = false;
        }
    }
    if (ocfiledeleteid !== '0') {
        isvalid = false;
        isocfile = false;
    }

    var filemp = $("#fu_MP").val();
    if (filempid === '' || filempid == '0') {
        if (filemp === '' || filemp === null || filemp === undefined) {
            isvalid = false;
            ismpfile = false;
        }
    }
    if (mpfiledeleteid !== '0') {
        isvalid = false;
        ismpfile = false;
    }

    if ($("#ddlSalikTag").val() === 'true' && $("#txtSalikAmount").val() === '') {
        $("#spSalikAmountValidator").text("This field is mandatory.");
        isvalid = false;
    }
    if ($("#ddlPetrolCard").val() === 'true' && $("#txtPetrolCardAmount").val() === '') {
        $("#spPetrolCardAmountValidator").text("This field is mandatory.");
        isvalid = false;
    }
    if (isvalid)
        return true
    else {
        if (!isjdfile) {
            $("#txtuploadedMsgAdd_JD").text("This field is mandatory.");
        }
        if (!isocfile) {
            $("#txtuploadedMsgAdd_OC").text("This field is mandatory.");
        }
        if (!ismpfile) {
            $("#txtuploadedMsgAdd_MP").text("This field is mandatory.");
        }
        return false;
    }

}

$("#txtSalikAmount").on('change keyup paste', function () {
    if ($("#txtSalikAmount").val() === '') {
        $("#spSalikAmountValidator").css("color", "red");
        $("#spSalikAmountValidator").text("This field is mandatory.");
    }
    else
        $("#spSalikAmountValidator").text("");
});
