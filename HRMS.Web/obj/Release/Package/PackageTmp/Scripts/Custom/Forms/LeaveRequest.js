﻿var balancedigitafterdecimal = 2;
var leaveentitletypeid = 0;
var oldfromdate = "";
var oldtodate = "";
var isleavetypechanged = 0;
$(document).ready(function ()
{   
    var formid = 6;
    var formprocessid = $("#hdnFormProcessID").val();    
    if (formprocessid == null || formprocessid == '' || formprocessid == '0') {
        $("#txtToDate").val("");
        $("#txtFromDate").val("");
         checkFormLoadValidation(6, $("#hdnCompanyID").val(), "#lblEmployeeLeaveRequestNotification", "EmployeeLeaveRequest");
    }   
    $("#btnEmployeeLeaveRequestWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#EmployeeLeaveReequestWarningBox").remove();
    });
    $("#chkHalfday").attr("disabled", 'disabled');
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $('#hdnBalanceDays').val(0);
    $('#hdnVTCategoryID').val(0);    


    $("#txtToDate").datepicker({
        dateFormat: "dd/mm/yyyy",
        onSelect: function (f, d, i) {
            if (d !== i.lastVal) {
                $('#txtToDate').trigger("change");
            }
        }
    }).data("datepicker");
    $('#txtToDate').on("change", function () {
        var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
        if (NoOfDaysDiff>0)
            CalculateDays();
        else {           
            $('#txtRequestedDays').val(0);
            GetRemainingDays();
        }
    });

    $("#txtFromDate").datepicker({
        dateFormat: "dd/mm/yyyy",
        onSelect: function (f, d, i) {
            if (d !== i.lastVal) {
                $("#txtFromDate").trigger("change");
            }
        }
    }).data("datepicker");
    $("#txtFromDate").on("change", function () {
        var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
        if (NoOfDaysDiff > 0)
            CalculateDays();
        else
        {
          //  ShowMessage("error", "From date should be less than to date.");
            $('#txtRequestedDays').val(0);
            GetRemainingDays();
        }
    });
    var reqstatusid = $("#hdnReqStatusID").val();
    var formprocessid = $("#hdnFormProcessID").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    var LeaveTypeID = $("#VacationTypeID").val();
    var fromdate = $("#txtFromDate").val();
    var todate = $("#txtToDate").val();
    if (LeaveTypeID > 0) {
        var vacation = GetValidLeaveRequestDates(employeeid, LeaveTypeID, fromdate, todate);
        if (vacation != null) {
            $("#txtFromDate").val(vacation.fromDate);
            oldfromdate = vacation.fromDate;
            $("#txtToDate").val(vacation.toDate);
            oldtodate = vacation.toDate;
            $('#txtRequestedDays').val(vacation.NoOfDays);
        }
    }
    //(reqstatusid != "3" && iseditcompleted == "True" && iseditallrequests == "True")
    if (formprocessid != null && formprocessid != '' && formprocessid != '0') {
        if (reqstatusid == "3") {    
            $(".btnApprove").remove();
            $(".btnReject").remove();
            if (reqstatusid == "3") {
                //$(".btnSave").prop("title", "Re-Submit");
                //$('button#btnSave').text("Re-Submit");
                $(".btnUpdate").prop("title", "Re-Submit");
                $('button#btnUpdate').text("Re-Submit");
            }
            if (formprocessid > 0 && reqstatusid == "3") {
                var LeaveBalanceInfo = GetLeaveBalanceInfoById(LeaveTypeID);
                $('#hdnVTCategoryID').val(LeaveBalanceInfo.VTCategoryID);
                balancedigitafterdecimal = LeaveBalanceInfo.BalanceDigitAfterDecimal;
                if (balancedigitafterdecimal == 0)
                    balancedigitafterdecimal = 2;
                if (LeaveBalanceInfo.RemainingLeave != null) {
                    $('#hdnBalanceDays').val(LeaveBalanceInfo.RemainingLeave);
                    $("#txtTotalAvailLeaves").val(parseFloat(LeaveBalanceInfo.RemainingLeave).toFixed(balancedigitafterdecimal));
                }
                else {
                    $("#txtTotalAvailLeaves").val('');
                    $('#hdnBalanceDays').val(0);
                }
                //if annual and accumulated leave (assuming that only one such leave type will be active for an employee) then only need to set lapse and extended lapsing details
                if (LeaveBalanceInfo.VTCategoryID == 1 && LeaveBalanceInfo.IsAccumulatedLeave) {                  
                    if (LeaveBalanceInfo.LapsingDays != null)
                        $("#txtLastYearCarriedDays").val(parseFloat(LeaveBalanceInfo.LapsingDays).toFixed(balancedigitafterdecimal));
                    else
                        $("#txtLastYearCarriedDays").val('');
                    if (LeaveBalanceInfo.ExtendedLapsingDays != null)
                        $("#txtExtendedLapsedDays").val(parseFloat(LeaveBalanceInfo.ExtendedLapsingDays).toFixed(balancedigitafterdecimal));

                    if (LeaveBalanceInfo.LapseDate != null && LeaveBalanceInfo.LapseDate.length > 0)
                        $("#txtLastYearForwardLapseDate").val(FormatDate(LeaveBalanceInfo.LapseDate));
                    if (LeaveBalanceInfo.ExtendedLapseDate != null && LeaveBalanceInfo.ExtendedLapseDate.length > 0)
                        $("#txtExtendedLapsedate").val(FormatDate(LeaveBalanceInfo.ExtendedLapseDate));
                }
                 //Sick leave
                else if(LeaveBalanceInfo.VTCategoryID == 2)
                {
                    // alert('inside'+LeaveBalanceInfo.LapsingDays); 
                    var fullpaid = parseFloat(LeaveBalanceInfo.FullPaidDays);
                    var halfpaid = parseFloat(LeaveBalanceInfo.HalfPaidDays);
                    var unpaid = parseFloat(LeaveBalanceInfo.UnPaidDays);
                    var taken = parseFloat(LeaveBalanceInfo.TakenLeave);
                    var bal = 0;
                    if (taken >= fullpaid) {
                        if (taken >= (fullpaid + halfpaid)) {
                            if (taken >= (fullpaid + halfpaid + unpaid)) {
                                bal = 0;
                            }
                            else {
                                bal = (fullpaid + halfpaid + unpaid) - taken;
                                bal = 0 - bal;

                            }
                        }
                        else {
                            bal = (fullpaid + halfpaid) - taken;
                            bal = 0 - bal;
                        }
                    }
                    else {
                        bal = fullpaid - taken;
                    }

                    $("#txtLastYearCarriedDays").val(0);
                    $("#txtExtendedLapsedDays").val(0);
                    $("#txtLastYearForwardLapseDate").val(null);
                    $("#txtExtendedLapsedate").val(null);
                    $("#txtTotalAvailLeaves").val(bal);
                }
                else {
                    $("#txtLastYearCarriedDays").val('0');
                    $("#txtExtendedLapsedDays").val('0');
                    $("#txtLastYearForwardLapseDate").val('');
                    $("#txtLastYearForwardLapseDate").val('');
                }
                GetRemainingDays();
            }
        }
        else {
            $(".btnSave").hide();
            $("#chkHalfday").attr("disabled", true);
            $("#rdoInCountry").attr("disabled", true);
            $("#rdoOutCountry").attr("disabled", true);
            $("#ddlCountry").attr("disabled", true);
            $("#txtFromDate").attr("disabled", "disabled");
            $("#txtToDate").attr("disabled", "disabled");
            $("#txtContactInfo1").attr("readonly", true);
            $("#txtContactInfo2").attr("readonly", true);
            $("#txtContactInfo3").attr("readonly", true);
            $("#divBrowse_MC").attr("disabled", "disabled");
            $("#btnRemove_MC").attr("disabled", "disabled");
            if ($("#ddlEmployee1").length)
                $("#ddlEmployee1").attr("disabled", true);
            if ($("#ddlEmployee2").length)
                $("#ddlEmployee2").attr("disabled", true);
            if ($("#ddlEmployee3").length)
                $("#ddlEmployee3").attr("disabled", true);
            if ($("#ddlEmployee4").length)
                $("#ddlEmployee4").attr("disabled", true);
            if ($("#ddlEmployee5").length)
                $("#ddlEmployee5").attr("disabled", true);
            if ($("#txtTaskDetail1").length)
                $("#txtTaskDetail1").attr("readonly", true);
            if ($("#txtTaskDetail2").length)
                $("#txtTaskDetail2").attr("readonly", true);
            if ($("#txtTaskDetail3").length)
                $("#txtTaskDetail3").attr("readonly", true);
            if ($("#txtTaskDetail4").length)
                $("#txtTaskDetail4").attr("readonly", true);
            if ($("#txtTaskDetail5").length)
                $("#txtTaskDetail5").attr("readonly", true);

        }
             

    }
    else
    {

    }  
   
    $(':input').each(function () {
        if ($(this).prop("id") != 'ddlLeaveType') //not taking values from company id
            $(this).data('initialValue', $(this).val());
    });

    $('.btnSave').click(function () {
       // alert('In Save');
        var formprocessid = $("#hdnFormProcessID").val();
        var formid = 6;
        var companyIds = $("#hdnCompanyID").val();
        if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
            $.ajax({
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                url: '/EmployeeLeaveRequest/IsWorkFlowExists',
                data: { formID: formid, companyIDs: companyIds },
                success: function (result) {
                    if (result.InsertedRowId > 0)
                        SaveForm();
                    else {
                        ShowMessage(result.CssClass, result.Message);
                       // hideLoaderFrame();
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
        else
        {
            SaveForm();
        }
    });

    $('.btnApprove').click(function () {
        pageLoaderFrame();
        var formprocessid = $("#hdnFormProcessID").val();
        var comments = $("#txtComments").val();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/EmployeeLeaveRequest/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnFormProcessID").val();
                var comments = $("#txtComments").val();

                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/EmployeeLeaveRequest/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });

            });
        }
        else
            ShowMessage('error', 'Comments field is mandatory.');
    });

    $('#btnBack').click(function () {
        location.href = "/FormsTaskList/Index/";
    });

    $('#txtComments').on("input", function () {
        var str = "Approver Comments(max 2000 characters) ";
        var maxlength = $(this).attr("maxlength");
        var currentLength = $(this).val().length;
        var noofcharsleft;

        if (currentLength >= maxlength) {
            //console.log("You have reached the maximum number of characters.");
            $('#lblComments').text(str + "0 chars left");
        } else {
            // console.log(maxlength - currentLength + " chars left");
            noofcharsleft = maxlength - currentLength;
            $('#lblComments').text(str + " " + noofcharsleft + " chars left");
        }
    });
    $('.leave-dates').on('mouseleave', function () {       
        $("#txtFromDate").datepicker("setDate", $("#txtFromDate").val());
        $("#txtToDate").datepicker("setDate", $("#txtToDate").val());
    });
    $('#txtCancelComments').on("input", function () {
        var str = "Cancel Request Comments(max 2000 characters) ";
        var maxlength = $(this).attr("maxlength");
        var currentLength = $(this).val().length;
        var noofcharsleft;

        if (currentLength >= maxlength) {
            //console.log("You have reached the maximum number of characters.");
            $('#lblCancelRequestComments').text(str + "0 chars left");
        } else {
            // console.log(maxlength - currentLength + " chars left");
            noofcharsleft = maxlength - currentLength;
            $('#lblCancelRequestComments').text(str + " " + noofcharsleft + " chars left");
        }
    });

    $('.btnCancelRequest').on('click', function () {
        var formProcessId = $("#hdnFormProcessID").val();
        var cancelComments = $("#txtCancelComments").val();
        pageLoaderFrame();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/EmployeeLeaveRequest/CancelLeaveRequest',
            data: { formProcessId: parseInt(formProcessId), cancelComments: cancelComments },
            success: function (result) {
                hideLoaderFrame();
               
                if (result.insertedRowId > 0) {
                    ShowMessage(result.result, result.resultMessage);
                    setTimeout(function () { location.reload(); }, 1000);
                }
                else {
                    ShowMessage('error', result.resultMessage);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

});

function CalculateDays() {  
    var leavetypeid = 0;
    var formprocessid = $("#hdnFormProcessID").val();
    if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined || formprocessid === '' || formprocessid ==="")
        formprocessid = 0;          
    if (formprocessid > 0)
        leavetypeid = $("#hdnVacationTypeID").val();
    else
        leavetypeid = $("#ddlLeaveType").val();
    if (leavetypeid > 0) {
        var fromdate = $("#txtFromDate").val();
        var todate = $("#txtToDate").val();
        var employeeid = $("#hdnRequesterEmployeeID").val();
        var vacationmodel;
        var NoOfDaysDiff = 0;
        var requesteddays = 0;
        var ishalfday = $("#chkHalfday").prop("checked");
        var ischanged = 0;
        if (ishalfday)
            NoOfDaysDiff = 0.5;
        else
            NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
        if (oldfromdate != fromdate)
            ischanged = 1;
        if (oldtodate != todate)
            ischanged = 1;

        if (NoOfDaysDiff > 0) {
            if (leavetypeid > 0 && !ishalfday && (ischanged == 1 || isleavetypechanged == 1) && fromdate != '' && todate!='') {
                var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
                if (vacation != null) {
                    $("#txtFromDate").val(vacation.fromDate);
                    oldfromdate = vacation.fromDate;
                    $("#txtToDate").val(vacation.toDate);
                    oldtodate = vacation.toDate;
                    requesteddays = vacation.NoOfDays;

                }
                else {
                    ShowMessage("error", "Unable to get details.");
                }

                if (requesteddays > 0) {
                    $('#txtRequestedDays').val(requesteddays);
                    GetRemainingDays();
                }
                else {
                    //ShowMessage("error", "Please select valid dates.");
                    $('#txtRequestedDays').val("");
                    GetRemainingDays();
                }

            }
            else {
                if (ishalfday) {
                    $('#txtRequestedDays').val(NoOfDaysDiff);

                }
                //else {
                //    $('#txtRequestedDays').val(0);
                //}
                GetRemainingDays();
            }

        }
        else {
            //  ShowMessage("error", "From date should be less than to date.");
            $('#txtRequestedDays').val("");
            GetRemainingDays();
        }

    }
    else {
        $('#txtRequestedDays').val(0);
        //ShowMessage("error", "Select leave type.");
    }
    isleavetypechanged = 0;
}

$("#chkHalfday").change(function () {   
    var start = $('#txtFromDate').val();
    var end = $('#txtToDate').val();

    if ($("#chkHalfday").prop("checked") == true) {
        var todayTime = new Date();
        var mm = todayTime.getMonth() + 1;
        var dd = todayTime.getDate();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        } 
        var yyyy = todayTime.getFullYear();
        var todayDate = dd + "/" + mm + "/" + yyyy;           
        $("#txtFromDate").attr("disabled", true);
        $("#txtToDate").attr("disabled", true);
        $('#txtFromDate').val(todayDate);
        $('#txtToDate').val(todayDate);
        CalculateDays();
    }
    if ($("#chkHalfday").prop("checked") == false) {
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        CalculateDays();
    }
});
$("input[name$='OutCountryRadios']").click(function () {
    $("#divDesignation").show();
    $("#rdoInCountry").prop('checked', false);
});
$("input[name$='InCountryRadios']").click(function () {
    $("#divDesignation").hide();
    $("#rdoInCountry").prop('checked', true);
    $("#rdoOutCountry").prop('checked', false);
});


function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);
}

function ValidateData() {
    $("#txtFromDatelbel").css("color", "red");
    $("#txtToDatelbel").css("color", "red");
    var date1 = $("#txtFromDate").val();
    var date2 = $("#txtToDate").val();
    //for update if file already there filejdid will have id value

    if (date1 === '' || date1 === null || date1 === undefined) {
        $("#txtFromDatelbel").text("This field is mandatory.");
        return false;
    }
    if (date2 === '' || date2 === null || date2 === undefined) {
        $("#txtToDatelbel").text("This field is mandatory.");
        return false;
    }
    return true;
}

function SaveForm() {
    if (window.FormData !== undefined) {
        var fileMC;
        var HalfDay;
        var In;
        var Out;      
        if (ValidateData()) {          
            var $form = $("#frmEmployeeLeaveRequestForm");
            var formprocessid = $("#hdnFormProcessID").val();           
            var reqstatusid = $("#hdnReqStatusID").val();
            var leavetypeid;
            if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined)
                formprocessid = 0;
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid()) {                
                if (formprocessid > 0)
                    leavetypeid = $("#hdnVacationTypeID").val();
                else
                    leavetypeid = $("#ddlLeaveType").val();
                var CurrentAnniversaryModel = GetLeavesTakenInCurrentAnniversary(leavetypeid);
                var LeaveBalanceInfo = GetLeaveBalanceInfoById(leavetypeid);
                var employeeid = $("#hdnRequesterEmployeeID").val();
                var fromdate = $("#txtFromDate").val();
                var todate = $("#txtToDate").val();
                var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
                var applieddays = vacation.NoOfDays;
                var availabledays = $("#txtTotalAvailLeaves").val();              
                if (IsPendingLeaveRequestExists())
                {                   
                    ShowMessage("error", "Another leave request is already under process, you cannot apply the leave until previous leave request not completed");
                    return false;
                }              
                //User with edit permission for pending and completed requests and request is not rejected
                if (formprocessid > 0 && reqstatusid==4) {
                  //Requested Days when leave was requested
                    var actualrequestdays = $("#hdnRequestedDays").val();
                    if(applieddays!=actualrequestdays)
                    {
                        ShowMessage("error", "Requested days (" + applieddays + ") must be same as Initial Request Days(" + actualrequestdays + ")");
                        return false;
                       
                    }
                }
                else
                {
                    var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
                    if (parseInt(NoOfDaysDiff) <= 0) {                      
                        ShowMessage("error", "From date must be a date less than to date .");
                        return false;
                    }
                  
                    if (LeaveBalanceInfo.VTCategoryID != 1 && LeaveBalanceInfo.VTCategoryID != 2)
                        {
                      if(parseFloat(LeaveBalanceInfo.RemainingLeave) < applieddays) {
                            ShowMessage("error", "You don't have sufficient leave balance.");
                            return false;
                        }
                    }
                else
                    {
                        //Annual leave
                        if (LeaveBalanceInfo.VTCategoryID == 1) {
                            if ((parseFloat(LeaveBalanceInfo.RemainingLeave) + parseFloat(LeaveBalanceInfo.LapsingDays) + parseFloat(LeaveBalanceInfo.ExtendedLapsingDays)) < applieddays) {
                                ShowMessage("error", "You don't have sufficient leave balance.");
                                return false;
                            }
                        }
                        //Sick Leave                      
                        if (LeaveBalanceInfo.VTCategoryID == 2) {
                            //if (applieddays>Math.abs(parseFloat(LeaveBalanceInfo.RemainingLeave))) {
                            if (applieddays > Math.abs(parseFloat(availabledays))) {
                                ShowMessage("error", "You don't have sufficient leave balance.");
                                return false;
                            }
                        }

                  }
                    //Sick/Annual/emergency/unpaid/compo Difference between request date and leave from date cannot be more than 10 working days
                    if (LeaveBalanceInfo.VTCategoryID == 2 || LeaveBalanceInfo.VTCategoryID == 1 || LeaveBalanceInfo.VTCategoryID == 4 || LeaveBalanceInfo.VTCategoryID == 8 || LeaveBalanceInfo.VTCategoryID == 9) {
                       // var days = ((moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate() - new Date()) / (1000 * 60 * 60 * 24)) + 1;
                        NoOfDaysDiff = GetNoOfWorkingDaysDiff();
                       if (parseInt(NoOfDaysDiff) > 10) {
                            ShowMessage("error", "From date must be a date less than 10 days from today's date .");
                            return false;
                        }
                    }
                    //No Annual Leave/Sick Leave during probation
                    if (LeaveBalanceInfo.VTCategoryID == 1 || LeaveBalanceInfo.VTCategoryID == 2) {
                        if (IsEmployeeUnderProbation()) {
                            ShowMessage('error', 'Not Eligible! You are under probation.');
                            return true;
                        }
                    }
                    //Sick Leave
                    if (CurrentAnniversaryModel.VTCategoryID == 2) {                    
                        if (IsValidSickLeave(CurrentAnniversaryModel)) {
                            return false;
                        }
                    }
                  
                    //Emergency Leave
                    if (CurrentAnniversaryModel.VTCategoryID == 4) {
                        var VacationTypeDetail = GetVacationTypeDetail(leavetypeid);
                        if (IsValidEmergencyPaidLeave(CurrentAnniversaryModel, VacationTypeDetail.TotalNoOfDays)) {
                            return false;
                        }
                    }
                    //Maternity & Hajj Leaves
                    if (CurrentAnniversaryModel.VTCategoryID == 5 || CurrentAnniversaryModel.VTCategoryID == 7) {
                        var NoOfDaysDiff = ((moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate() - new Date()) / (1000 * 60 * 60 * 24)) + 1;
                        if (NoOfDaysDiff < 0) {
                            ShowMessage("error", "You cannot apply leave for past date.");
                            return false;
                        }
                    }


                    //Compassionate & Industrial/Accident Leave Policy
                    if (CurrentAnniversaryModel.VTCategoryID == 6 || CurrentAnniversaryModel.VTCategoryID == 10) {
                        var fileId = $("#fu_MC").val();
                        if (fileId === '' || fileId === null || fileId === undefined) {
                            validateMedicalCertificate($("#txtuploadedMsgAdd_MC"), true);
                            return false;
                        }
                        else {
                            validateMedicalCertificate($("#txtuploadedMsgAdd_MC"), false);
                        }
                    }
                    if (CurrentAnniversaryModel.VTCategoryID == 8) {
                        var IsUnderProbation = IsEmployeeUnderProbation();
                        var appliedLeave = GetTotalAppliedLeaves();
                        if (IsUnderProbation) {
                            // if (appliedLeave > 5) {
                            if (appliedLeave > LeaveBalanceInfo.TotalLeaveDays) {
                                ShowMessage("error", "You can't apply more than " + LeaveBalanceInfo.TotalLeaveDays + " days leave.");
                                return true;
                            }
                        }
                        else {
                          //  var LeaveBalanceInfo = GetLeaveBalanceInfoById(leavetypeid);
                            var remainingAnualLeave = GetLeaveBalanceInfoByCatagoryId(1);
                            if (remainingAnualLeave.RemainingLeave <= 0) {
                                // if (appliedLeave > 5) {   
                                if (appliedLeave > LeaveBalanceInfo.TotalLeaveDays) {
                                    // ShowMessage("error", "You can't apply more than five days leave.");   
                                    ShowMessage("error", "You can't apply more than " + LeaveBalanceInfo.TotalLeaveDays + " days leave.");
                                    return true;
                                }
                            }
                            else {
                                ShowMessage("error", "You are not Eligible for this leave Policy.");
                                return true;
                            }
                        }
                    }
                    if (CurrentAnniversaryModel.VTCategoryID == 9) {
                        var appliedLeave = GetTotalAppliedLeaves();
                        if (IsEmployeeUnderProbation()) {
                            ShowMessage('error', 'Not Eligible! You are under probation.');
                            return true;
                        }
                        //var appliedLeave = GetTotalAppliedLeaves();                      
                        if (LeaveBalanceInfo.RemainingLeave > 0 && LeaveBalanceInfo.RemainingLeave >= appliedLeave) {
                            if (appliedLeave > LeaveBalanceInfo.TotalLeaveDays) {
                                ShowMessage("error", "You can't apply more than " + LeaveBalanceInfo.TotalLeaveDays + " days leave.");
                                return true;
                            }
                        }
                        else {
                            ShowMessage("error", "You don't have sufficient leave balance.");
                            return true;
                        }
                    }
                    if (CurrentAnniversaryModel.VTCategoryID == 7) //Hajj Leave
                        {
                        if (appliedLeave > LeaveBalanceInfo.TotalLeaveDays) {
                            ShowMessage("error", "You can't apply more than " + LeaveBalanceInfo.TotalLeaveDays + " days leave.");
                            return true;
                        }
                        }
                    var isleaveexists = IsLeaveDatesAlreadyExists();                 
                    if (isleaveexists) {
                        ShowMessage("error", "Requested leave days are already taken, please select another date.");
                        return true;

                    }

                }                             
             
                if ($("#chkHalfday").prop("checked") == true) {
                    HalfDay = true;
                }
                if ($("#chkHalfday").prop("checked") == false) {
                    HalfDay = false;

                }
                if ($("#rdoInCountry").prop('checked') == true) {
                    In = true;
                    Out = false;
                    var Country = 0;

                }
                else {
                    In = false;
                    Out = true;
                    var Country = $("#ddlCountry").val();
                }

                var formData = new FormData($form[0]);
                if ($("#fu_MC").get(0).files.length > 0) {
                    fileMC = $("#fu_MC").get(0).files;
                    formData.append("MedicalCertificate", fileMC[0]);

                }
                else
                    formData.append("MedicalCertificate", null);

                var TaskHandOverEmp1 = $("#ddlEmployee1").val();
                var TaskHandOverEmp2 = $("#ddlEmployee2").val();
                var TaskHandOverEmp3 = $("#ddlEmployee3").val();
                var TaskHandOverEmp4 = $("#ddlEmployee4").val();
                var TaskHandOverEmp5 = $("#ddlEmployee5").val();
                var TaskDetail1 = $("#txtTaskDetail1").val();
                var TaskDetail2 = $("#txtTaskDetail2").val();
                var TaskDetail3 = $("#txtTaskDetail3").val();
                var TaskDetail4 = $("#txtTaskDetail4").val();
                var TaskDetail5 = $("#txtTaskDetail5").val();
                var FromDate = $('#txtFromDate').val();
                var formprocessid = $("#hdnFormProcessID").val();
                var ToDate = $('#txtToDate').val();
               
               
                var updatedData = [];
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp1,
                    TaskDetail: TaskDetail1
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp2,
                    TaskDetail: TaskDetail2
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp3,
                    TaskDetail: TaskDetail3
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp4,
                    TaskDetail: TaskDetail4
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp5,
                    TaskDetail: TaskDetail5
                });
                formData.append("updatedData", JSON.stringify(updatedData));
                formData.append("Country", Country);
                formData.append("MedicalCertificateID", $("#hdnFileUpload_MC").val());
                formData.append("HalfDay", HalfDay);
                formData.append("In", In);
                formData.append("Out", Out);
                formData.append("FormProcessID", formprocessid);
               
                if ($("#hdn_JD").val() <= 0) {
                    formData.append("FromDate", FromDate);
                    formData.append("ToDate", ToDate)

                }               
                pageLoaderFrame();
                $.ajax({
                    url: '/EmployeeLeaveRequest/SaveForm',
                    type: "POST",
                    contentType: false, // Not to set any content header  
                    processData: false, // Not to process data  
                    data: formData,
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                        else {
                            setTimeout(function () {  BackToTaskList(); }, 1000);
                        };

                    },
                    error: function (err) {
                        hideLoaderFrame()
                        ShowMessage('error', err.statusText);
                    }
                });
            }
        }
    }
}

function IsEmployeeUnderProbation() {
    var IsEmployeeUnderProbation = false;
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/IsEmployeeUnderProbation',
        async: false,
        success: function (result) {
            if (result.Success == true) {
                //ShowMessage(result.CssClass, result.Message);
                IsEmployeeUnderProbation = result.Success;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return IsEmployeeUnderProbation;
}
function IsPendingLeaveRequestExists() {
    var ispendingleaveexists = false;
    var leaverequestid = 0;
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/IsPendingLeaveRequestExists',
        data: { leaveRequestID: leaverequestid },
        async: false,
        success: function (result) {
            if (result.Success == true) {
                //ShowMessage(result.CssClass, result.Message);
                ispendingleaveexists = result.Success;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return ispendingleaveexists;
}
function GetLeavesTakenInCurrentAnniversary(LeaveTypeID) { 
    var CurrentAnniversaryModel = [];
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetLeavesTakenInCurrentAnniversary',
        data: { LeaveTypeID: LeaveTypeID },
        async: false,
        success: function (result) {
            CurrentAnniversaryModel = result;
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return CurrentAnniversaryModel;
}

function IsOffDayBeforeOrAfterAppliedLeave(startdate, enddate) { 
    var IsOffDay = false;
    $.ajax({
        //dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/IsOffDayBeforeOrAfterAppliedLeave',
        data:{ StartDate: startdate, EndDate: startdate },
        async: false,
        success: function (result) {         
            if (result.Success == true) {
                //ShowMessage(result.CssClass, result.Message);
                IsOffDay = result.Success;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return IsOffDay;
}

function validateMedicalCertificate(element, IsValidLeaveType) {
    if (IsValidLeaveType) {      
        if ($(element).find('.field-validation-error').length == 0)
            $(element).append('<span class=\"field-validation-error\">This is mandatory field.</span>');
        ShowMessage("error", "Medical certificate is mandatory");
    }
    else if ($(element).find('.field-validation-error').length > 0) {
        $(element).find('.field-validation-error')[0].remove();
    }
}

function IsValidSickLeave(CurrentAnniversaryModel) { 
    var fileId = $("#fu_MC").val();
   // Validating on top
    //if (IsEmployeeUnderProbation()) {
    //    ShowMessage('error', 'Not Eligible! You are under probation.');
    //    return true;
    //}
    var filemcid = $("#hdnFileUpload_MC").val();
    if (fileId === '' || fileId === null || fileId === undefined || fileId === "")
        fileId = 0;
    if (filemcid === '' || filemcid === null || filemcid === undefined || filemcid==="")
        filemcid = 0;
    //
    if (fileId==0 && filemcid == 0) {
        if (GetTotalAppliedLeaves() > 1) {
            validateMedicalCertificate($("#txtuploadedMsgAdd_MC"), true);
            return true;
        }
        //if (CurrentAnniversaryModel.LeavesTakenInCurrentAnniversary >= 3) {
        if (CurrentAnniversaryModel.NoOfLeavesWithOutMedicalCertificate >= 3) {
            validateMedicalCertificate($("#txtuploadedMsgAdd_MC"), true);
            return true;
        }
        if (IsOffDayBeforeOrAfterAppliedLeave($("#txtFromDate").val(), $("#txtToDate").val())) {
            validateMedicalCertificate($("#txtuploadedMsgAdd_MC"), true);
            return true;
        }
    }
    validateMedicalCertificate($("#txtuploadedMsgAdd_MC"), false);
    return false;
}

function IsValidEmergencyPaidLeave(CurrentAnniversaryModel, TotalNoOfDaysLimit) {
    var appliedLeave = GetTotalAppliedLeaves();
    var appliedAndPreviousLeave = appliedLeave + CurrentAnniversaryModel.LeavesTakenInCurrentAcademicYear;
    if (appliedLeave > 2) {
        ShowMessage("error", "You can't apply more than two days leave.");
        return true;
    }
    if (CurrentAnniversaryModel.LeavesTakenInCurrentAcademicYear >= TotalNoOfDaysLimit) {
        ShowMessage("error", "You have already taken six Emergency leaves.");
        return true;
    }
    if (appliedAndPreviousLeave > TotalNoOfDaysLimit) {
        ShowMessage("error", "Applied leave days exceeded by maximum limit.");
        return true;
    }
    return false;
}

function GetTotalAppliedLeaves() {
    var leavedays = $("#txtRequestedDays").val();
    if ($("#chkHalfday").prop("checked") == true) {
        leavedays = 0.5;       
    }
    return leavedays;
}

function GetRemainingDays() {    
    var requesteddays = $("#txtRequestedDays").val();
    var remainingdays = 0;
    if (requesteddays == null || requesteddays == '' || requesteddays == "" || requesteddays == undefined)
        requesteddays = 0;
    var totalavailabledays = $("#txtTotalAvailLeaves").val();
    if (totalavailabledays == null || totalavailabledays == '' || totalavailabledays == "" || totalavailabledays == undefined)
        totalavailabledays = 0;
    var lapsedays = $("#txtLastYearCarriedDays").val();
    if (lapsedays == null || lapsedays == '' || lapsedays == "" || lapsedays == undefined)
        lapsedays = 0;
    var extendedlapsedays = $("#txtExtendedLapsedDays").val();
    if (extendedlapsedays == null || extendedlapsedays == '' || extendedlapsedays == "" || extendedlapsedays == undefined)
        extendedlapsedays = 0; 
    if (totalavailabledays == 0)
        remainingdays = 0;
    totalavailabledays = parseFloat(totalavailabledays);
    requesteddays = parseFloat(requesteddays);
    var vtcategoryId = $('#hdnVTCategoryID').val();
    var balancedays = $('#hdnBalanceDays').val();
    //Sick Leave
    if (vtcategoryId == 2) {
        // remainingdays = parseFloat(balancedays) - parseFloat(requesteddays);
        if (totalavailabledays < 0)
        {
            //var availablesickleavedays = 0 - totalavailabledays
            remainingdays = totalavailabledays + requesteddays;
            if (remainingdays > 0)
                remainingdays = 0 - remainingdays;
        }
        else {
            remainingdays = totalavailabledays - requesteddays;
        }
        
    }
    else if (vtcategoryId == 1) {
       // $("#txtRemainingDays").val();
        remainingdays = totalavailabledays  - requesteddays;
    }
    else {
        remainingdays = totalavailabledays - requesteddays;
    }
    $("#txtRemainingDays").val(remainingdays.toFixed(balancedigitafterdecimal));
}

$("#ddlLeaveType").on("change", function () {
    var LeaveTypeID = $("#ddlLeaveType").val();
    if (LeaveTypeID != null && LeaveTypeID != '') {
        isleavetypechanged = 1;
        var LeaveBalanceInfo = GetLeaveBalanceInfoById(LeaveTypeID);
        leaveentitletypeid = LeaveBalanceInfo.LeaveEntitleTypeID;       
        $('#hdnBalanceDays').val(LeaveBalanceInfo.RemainingLeave);
        $('#hdnVTCategoryID').val(LeaveBalanceInfo.VTCategoryID);
        if (LeaveBalanceInfo.VTCategoryID == 1)
            $("#chkHalfday").removeAttr("disabled");
        else {          
            $("#chkHalfday").prop("checked", false);
            $("#chkHalfday").prop("disabled", true);
        }         

        balancedigitafterdecimal = LeaveBalanceInfo.BalanceDigitAfterDecimal;
        if (balancedigitafterdecimal == 0)
            balancedigitafterdecimal = 2;
        if (LeaveBalanceInfo.RemainingLeave != null) {
            if (LeaveBalanceInfo.VTCategoryID == 1)
            {
                var totalavailableleaves = parseFloat(LeaveBalanceInfo.RemainingLeave) + parseFloat(LeaveBalanceInfo.LapsingDays) + parseFloat(LeaveBalanceInfo.ExtendedLapsingDays)
                $("#txtTotalAvailLeaves").val(totalavailableleaves.toFixed(balancedigitafterdecimal));
            }
            else
            $("#txtTotalAvailLeaves").val(parseFloat(LeaveBalanceInfo.RemainingLeave).toFixed(balancedigitafterdecimal));
        }
        else
            $("#txtTotalAvailLeaves").val('');
        //Sick Leave
        if (LeaveBalanceInfo.VTCategoryID == 2) {
            // alert('inside'+LeaveBalanceInfo.LapsingDays); 
            var fullpaid = parseFloat(LeaveBalanceInfo.FullPaidDays);
            var halfpaid = parseFloat(LeaveBalanceInfo.HalfPaidDays);
            var unpaid = parseFloat(LeaveBalanceInfo.UnPaidDays);
            var taken = parseFloat(LeaveBalanceInfo.TakenLeave);
            var bal = 0;
            if (taken >= fullpaid)
            {
                if(taken >= (fullpaid+halfpaid))
                {
                    if(taken >= (fullpaid+halfpaid+unpaid))
                    {
                        bal = 0;
                    }
                    else {
                        bal = (fullpaid + halfpaid+unpaid) - taken;
                        bal = 0 - bal;

                    }
                }
                else {
                    bal = (fullpaid + halfpaid) - taken;
                    bal = 0 - bal;
                }
            }
                else
            {
                bal = fullpaid - taken;
            }

            $("#txtLastYearCarriedDays").val(0);
            $("#txtExtendedLapsedDays").val(0);
            $("#txtLastYearForwardLapseDate").val(null);
            $("#txtExtendedLapsedate").val(null);
            $("#txtTotalAvailLeaves").val(bal);
        }
        //if annual and accumulated leave (assuming that only one such leave type will be active for an employee) then only need to set lapse and extended lapsing details
        if (LeaveBalanceInfo.VTCategoryID == 1 && LeaveBalanceInfo.IsAccumulatedLeave) {
            // alert('inside'+LeaveBalanceInfo.LapsingDays); 
            if (LeaveBalanceInfo.LapsingDays!=null)
                $("#txtLastYearCarriedDays").val(parseFloat(LeaveBalanceInfo.LapsingDays).toFixed(balancedigitafterdecimal));
            else
                $("#txtLastYearCarriedDays").val('');
            if (LeaveBalanceInfo.ExtendedLapsingDays != null)
                $("#txtExtendedLapsedDays").val(parseFloat(LeaveBalanceInfo.ExtendedLapsingDays).toFixed(balancedigitafterdecimal));

            if (LeaveBalanceInfo.LapseDate != null && LeaveBalanceInfo.LapseDate.length > 0)
                $("#txtLastYearForwardLapseDate").val(FormatDate(LeaveBalanceInfo.LapseDate));
            if (LeaveBalanceInfo.ExtendedLapseDate != null && LeaveBalanceInfo.ExtendedLapseDate.length > 0)
                $("#txtExtendedLapsedate").val(FormatDate(LeaveBalanceInfo.ExtendedLapseDate));


        }
        else {
            $("#txtLastYearCarriedDays").val('0');
            $("#txtExtendedLapsedDays").val('0');
            $("#txtLastYearForwardLapseDate").val('');
            $("#txtLastYearForwardLapseDate").val('');
        }

        if (LeaveBalanceInfo.VTCategoryID == 9) {
            $("#chkHalfday").prop("checked", false);
            $("#chkHalfday").attr("disabled", true);

        }
        //else {
        //    $('#chkHalfday').prop("disabled", false);
        //}
       
    }
    else {
        $("#txtTotalAvailLeaves").val('0');
        $("#txtLastYearCarriedDays").val('0');
        $("#txtExtendedLapsedDays").val('0');
        $("#txtLastYearForwardLapseDate").val('');
        $("#txtLastYearForwardLapseDate").val('');
    }
    CalculateDays();
});

function GetVacationTypeDetail(LeaveTypeID) {
    var VacationTypeDetail = [];
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetVacationTypeDetail',
        data: { LeaveTypeID: LeaveTypeID },
        async: false,
        success: function (result) {
            VacationTypeDetail = result;
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return VacationTypeDetail;
}

function GetLeaveBalanceInfoById(LeaveTypeID) {
    var LeaveBalanceInfo = [];
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetLeaveBalanceInfoById',
        data: { LeaveTypeID: LeaveTypeID, RequesterEmployeeID: $("#hdnRequesterEmployeeID").val() },
        async: false,
        success: function (LeaveBalanceInfoModel) {
            LeaveBalanceInfo = LeaveBalanceInfoModel;
            //$("#txtTotalAvailLeaves").val(LeaveBalanceInfoModel.RemainingLeave);
            //GetRemainingDays();
            //console.log(LeaveBalanceInfoModel);
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return LeaveBalanceInfo;
}

function GetLeaveBalanceInfoByCatagoryId(LeaveCategoryId) {
    var LeaveBalanceInfo = [];
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetLeaveBalanceInfoByCategoryId',
        data: { LeaveCategoryId: LeaveCategoryId, RequesterEmployeeID: $("#hdnRequesterEmployeeID").val() },
        async: false,
        success: function (LeaveBalanceInfoModel) {
            LeaveBalanceInfo = LeaveBalanceInfoModel;
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return LeaveBalanceInfo;
}

function FormatDate(dateString) {

    dateString = dateString.substr(6);
    var date = new Date(parseInt(dateString));  
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var date = dd + '/' + mm + '/' + yyyy;
    return date;
}
function FormatDateToString(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var date = dd + '/' + mm + '/' + yyyy;
    return date;
}
function IsLeaveDatesAlreadyExists()
{  
    var isexists = false;
    var fromdate=$("#txtFromDate").val();
    var todate = $("#txtToDate").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    $.ajax({
       // dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/IsLeaveDatesAlreadyExists',
        data: { employeeID: employeeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            if (result.Success == true) {         
               
                isexists = result.Success;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return isexists;
}
function GetNoOfWorkingDays() {
    var noofdays = 0;
    var fromdate = FormatDateToString(new Date())
    //alert(fromdate);
    var todate = $("#txtToDate").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    $.ajax({
        // dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetNoOfWorkingDays',
        data: { employeeID: employeeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            if (result != null) {
                //alert(result);
                noofdays = result;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    //var leavetypeid = $("#hdnRequesterEmployeeID").val();
    return noofdays;
}
function GetNoOfWorkingDaysDiff() {
    var noofdays = 0;
    var fromdate =  $("#txtFromDate").val();
    //alert(fromdate);
    var todate = FormatDateToString(new Date());
    var employeeid = $("#hdnRequesterEmployeeID").val();
    $.ajax({
        // dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetNoOfWorkingDays',
        data: { employeeID: employeeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            if (result != null) {
                //alert(result);
                noofdays = result;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    //var leavetypeid = $("#hdnRequesterEmployeeID").val();
    return noofdays;
}
function GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate) {  
    var vacationmodel = null;
   // pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetValidLeaveRequestDates',
        data: { employeeID: employeeid, vacationTypeID: leavetypeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            vacationmodel = result;
           // hideLoaderFrame();
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
           //  hideLoaderFrame();
        }
    });
    return vacationmodel;
}