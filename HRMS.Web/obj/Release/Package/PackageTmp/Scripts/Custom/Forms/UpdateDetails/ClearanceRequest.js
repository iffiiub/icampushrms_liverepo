﻿var formid = 14;
$(document).ready(function () {

    $('.DecimalOnly').keypress(function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('.YNClass').each(function () {
        $(this).attr('data-initialValue', $(this).is(":checked"));
    });

    $('.DecimalOnly').each(function () {
        $(this).attr('data-initialValue', $(this).val());
    });

    $('.Remarks').each(function () {
        $(this).attr('data-initialValue', $(this).val());
    });

    $('.btnUpdate').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var companyIds = $("#hdnUserCompanyID").val();
            var reqstatusid = $("#hdnReqStatusID").val();
            var IsValidOptionAction = true;

            pageLoaderFrame();

            var formprocessid = $("#hdnFormProcessID").val();
            var ID = $("#ID").val();
            var Comments = $("#txtComments").val();
            var RequesterEmployeeID = $("#RequesterEmployeeID").val();

            var ClearanceRequestOptionsModel = [];

            $("table tbody tr").each(function () {
                if ($(this).find(".YNClass").is(":checked") || $(this).find(".NAClass").is(":checked")) {
                    ClearanceRequestOptionsModel.push({
                        ID: $(this).find("td").eq(0).data("id"),
                        ClearanceDetailID: $(this).find("td").eq(0).data("optiondetailid"),
                        ClearanceOptionID: $(this).find("td").eq(0).data("optionid"),
                        ClearanceMark: $(this).find('.YNClass').is(":checked"),
                        Amount: $(this).find(".DecimalOnly").val(),
                        Remarks: $(this).find(".Remarks").val()
                    });
                }
            });

            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/ClearanceRequest/UpdateForm',
                data: { listclearanceRequestOptionsModel: JSON.stringify(ClearanceRequestOptionsModel), formprocessid: formprocessid, ID: ID, Comments: Comments, RequesterEmployeeID: RequesterEmployeeID, ReqStatusID: reqstatusid },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    hideLoaderFrame();
                    if (result.CssClass != 'error') {
                        location.reload();
                    }                  
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    });
});


function ChangeOptionAction(element, mode) {
    if (mode == 1) {
        if ($(element).is(":checked")) {
            $(element).closest('td').next('td').find('.YNClass').prop('checked', false)
        }
        else {
            $(element).closest('td').next('td').find('.YNClass').prop('checked', true)
        }
    }
    else {
        if ($(element).is(":checked")) {
            $(element).closest('td').prev('td').find('.NAClass').prop('checked', false)
        }
        else {
            $(element).closest('td').prev('td').find('.NAClass').prop('checked', true)
        }
    }
}


