﻿var NoOfDecimalPlaces = 0;
var totalExpense = 0;
var selectClaimTypeList = "";
var selectCurrencyList = "";
var ExpenseClaimDatatable;
var ExpenseClaimRequestDetailModel = [];// = new Array();
var FilesArray = [];
var DeletedClaimDetailIDs = "";
var DeletedClaimFileIDs = "";
var defaultCurrency = { AED: "1.0000", USD: "3.6730", EUR: "4.1100" };
$(document).ready(function () {

    if (window.location.pathname.split("/")[2].toLowerCase() != 'viewdetails') {
        checkFormLoadValidation($("#FormId").val(), $("#CompanyId").val(), "#lblExpenxeClaimNotification", "ExpenseClaimRequest");
    }
   
    NoOfDecimalPlaces = parseInt($("#NoOfDecimalPlaces").val());
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    initExpenseClaimRequestGrid();
    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });
    BindExpenseClaimTypes();
    BindCurrency();
    var reqstatusid = $("#hdnReqStatusID").val();
    if (reqstatusid == "3") {
        $(".btnApprove").remove();
        $(".btnReject").remove();
        $(".btnUpdate").prop("title", "Re-Submit");
        $('button#btnUpdate').text("Re-Submit");
    }

    $("#btnExpenseClaimWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#ExpeseClaimWarningBox").remove();
    });
});

$(document).on("change", ".Attachment", function () {
    var file = this.files[0];
    fileName = file.name;
    size = file.size;
    type = file.type;
    var currentRow = $(this).closest("tr");
    var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
    $(currentRow).find("#txtuploadedMsgAdd_Attachment").css("color", "red");

    if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
        if (size <= 2097152) {
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("");
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").hide();
            $(currentRow).find("#divBrowse_Attachment").hide();
            $(currentRow).find("#btnRemove_Attachment").show();
            $(currentRow).find("#file_caption_name_Attachment").show();
            //$("#file_caption_name_Attachment").html(fileName.substring(0, 15));
            //$("#file_caption_id_Attachment").html(fileName.substring(0, 15));
            $(currentRow).find("#chkFileUpload_Attachment").val("2");
            var fileJD = URL.createObjectURL(event.target.files[0]);
            $(currentRow).find('#divPreview_Attachment').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');
        }
        else {
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("Maximum 2MB file size is allowed to upload.");
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").show();
            $(currentRow).find("#divAddInforamtionDialog_Attachment").hide();
            $(currentRow).find("#divPreview_Attachment").empty();
        }
    }
    else {
        $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("Only JPG, JPEG, PNG, GIF, PDF, XLS, XLSX, PPT, PPTX, DOC and DOCX extension files are allowed to upload.");
        $(currentRow).find("#txtuploadedMsgAdd_Attachment").show();
        $(currentRow).find("#divAddInforamtionDialog_Attachment").hide();
    }
});


$(document).on("click", ".btnSave", function () {
    if (ExpenseClaimRequestDetailModel.length == 0) {
        ShowMessage("error", "Please add at least one record.");
        return false;
    }
    $.ajax({
        dataType: 'json',
        url: '/ExpenseClaimRequest/IsWorkFlowExists',
        data: {
            formID: $("#FormId").val(), companyIDs: $("#CompanyId").val()
        },
        success: function (result) {
            hideLoaderFrame();
            if (result.InsertedRowId > 0) {
                SaveExPenseClaimRequest(); //Save Form
            }
            else {
                ShowMessage(result.CssClass, result.Message);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });


});
function SaveExPenseClaimRequest() {
    var formData = new FormData();
    formData.append("expenseClaimRequestDetailList", JSON.stringify(ExpenseClaimRequestDetailModel));
    formData.append("Comments", $("#txtComments").val());
    if (ExpenseClaimRequestDetailModel.length == 0) {
        ShowMessage("error", "Please add at least one record.");
        return false;
    }
    for (var i = 0; i < ExpenseClaimRequestDetailModel.length; i++) {
        if (ExpenseClaimRequestDetailModel[i].FileData != null) {
            var fileName = ExpenseClaimRequestDetailModel[i].FileData.name;
            var extension = fileName.substr(fileName.lastIndexOf('.') + 1);
            fileName = ExpenseClaimRequestDetailModel[i].RowID.toString() + "." + extension;
            //formData.append("FilesArray", ExpenseClaimRequestDetailModel[i].FileData.slice(), fileName);
            formData.append("FilesArray", ExpenseClaimRequestDetailModel[i].FileData, fileName);
        }
    }
    pageLoaderFrame();
    $.ajax({
        url: "/ExpenseClaimRequest/SaveExpenseClaimRequest",
        contentType: false,
        processData: false,
        type: "POST",
        data: formData,
        datatype: "json",
        success: function (data) {
            if (data.Success) {
                ShowMessage("success", data.Message);
                hideLoaderFrame();
                BackToTaskList();
            }
            else {
                ShowMessage("error", data.Message);
                hideLoaderFrame();
            }
        },
        error: function (err) {
            ;
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

$(document).on("click", ".btnUpdate", function () {
    if ($("#tblExpenseClaimRequest > tbody > tr").data('claimtypeid') == "" && $("#tblExpenseClaimRequest > tbody > tr").data('isnewexpense') == true) {
        ShowMessage("error", "Please add at least one record.");
        return false;
    }
    $.ajax({
        dataType: 'json',
        url: '/ExpenseClaimRequest/IsWorkFlowExists',
        data: {
            formID: $("#FormId").val(), companyIDs: $("#CompanyId").val()
        },
        success: function (result) {
            hideLoaderFrame();
            if (result.InsertedRowId > 0) {
                UpdateExPenseClaimRequest(); //Update Form
            }
            else {
                ShowMessage(result.CssClass, result.Message);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
});

function UpdateExPenseClaimRequest() {
    var formData = new FormData();
    formData.append("expenseClaimRequestDetailList", JSON.stringify(ExpenseClaimRequestDetailModel));
    formData.append("FormProcessID", $("#FormProcessID").val());
    formData.append("ID", $("#ID").val());
    formData.append("Comments", $("#txtComments").val());
    formData.append("DeletedClaimDetailIDs", DeletedClaimDetailIDs);
    formData.append("DeletedClaimFileIDs", DeletedClaimFileIDs);
 
    if ($("#tblExpenseClaimRequest > tbody > tr").data('claimtypeid') == "" && $("#tblExpenseClaimRequest > tbody > tr").data('isnewexpense') == true) {
        ShowMessage("error", "Please add at least one record.");
        return false;
    }
    for (var i = 0; i < ExpenseClaimRequestDetailModel.length; i++) {
        if (ExpenseClaimRequestDetailModel[i].FileData != null) {
            var fileName = ExpenseClaimRequestDetailModel[i].FileData.name;
            var extension = fileName.substr(fileName.lastIndexOf('.') + 1);
            fileName = ExpenseClaimRequestDetailModel[i].RowID.toString() + "." + extension;
            formData.append("FilesArray", ExpenseClaimRequestDetailModel[i].FileData, fileName);
        }
    }
    pageLoaderFrame();
    $.ajax({
        url: "/ExpenseClaimRequest/UpdateExpenseClaimRequest",
        contentType: false,
        processData: false,
        type: "POST",
        data: formData,
        datatype: "json",
        success: function (data) {
            if (data.Success) {
                ShowMessage("success", data.Message);
                hideLoaderFrame();
                BackToTaskList();
            }
            else {
                ShowMessage("error", data.Message);
                hideLoaderFrame();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

$(document).on("click", ".btnApprove", function () {
    pageLoaderFrame();
    var formprocessid = $("#FormProcessID").val();
    var comments = $("#txtComments").val();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/ExpenseClaimRequest/ApproveForm',
        data: { formProcessID: formprocessid, comments: comments },
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (result.InsertedRowId > 0) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
});

$(document).on("click", ".btnReject", function () {
    var IsValidateComment = validateComments($("#txtComments"));
    if (IsValidateComment) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {

            var formprocessid = $("#FormProcessID").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/ExpenseClaimRequest/RejectForm',
                data: { formProcessID: formprocessid, comments: comments },
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (result.InsertedRowId > 0) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        });
    }
    else
        ShowMessage('error', 'Comments field is mandatory.');
});

$(document).on("click", ".btnBack", function () {
    BackToTaskList();
});

$(document).on('change', '.Currency', function () {
    var currentRow = $(this).closest("tr");
    if ($(this).val() > 0) {        
        if ($(this).val() == 1)
            $(currentRow).find("#ExchangeRate").val(defaultCurrency.AED);
        else if ($(this).val() == 2)
            $(currentRow).find("#ExchangeRate").val(defaultCurrency.USD);
        else if ($(this).val() == 3)
            $(currentRow).find("#ExchangeRate").val(defaultCurrency.EUR);
    }
    else {
        $(currentRow).find("#ExchangeRate").val("0.0000");
    }

});



//$(document).on("change", ".Currency", function () {
//    var currentRow = $(this).closest("tr");

//    if ($(this).val() > 0) {
//        $.fn.currencyconverter($(this).find("option:selected").text(), "AED", $(currentRow).find("#ExchangeRate"), function (event, e) {
//            var digit = $(this).val().split(".")[0];
//            var decimal = $(this).val().split(".")[1].substring(0, 4);
//            var exchangeValue = digit.toString() + "." + decimal.toString();
//            $(this).val(exchangeValue);
//            if ($(this).val() > 0) {
//                if ($(currentRow).find("#Amount").val().length > 0) {
//                    $(currentRow).find("#TotalAmountAED").val(($(currentRow).find("#Amount").val() * $(this).val()).toFixed(NoOfDecimalPlaces));
//                }
//            }
//        });
//    }
//    else {
//        $(currentRow).find("#ExchangeRate").val("0.0000");
//    }
//});

$(document).on("change", ".exchangerate", function () {
    var currentRow = $(this).closest("tr");
    if ($(this).val() > 0) {
        if ($(currentRow).find("#Amount").val().length > 0) {
            $(currentRow).find("#TotalAmountAED").val(($(currentRow).find("#Amount").val() * $(this).val()).toFixed(NoOfDecimalPlaces));
        }
    }
});

$(document).on("change", ".amount", function () {
    $(this).val(parseFloat($(this).val()).toFixed(NoOfDecimalPlaces));
    var currentRow = $(this).closest("tr");
    if ($(this).val() > 0) {
        if ($(currentRow).find("#ExchangeRate").val().length > 0) {
            $(currentRow).find("#TotalAmountAED").val(($(this).val() * $(currentRow).find("#ExchangeRate").val()).toFixed(NoOfDecimalPlaces));
        }
    }
    else {
        if ($(currentRow).find("#ExchangeRate").val().length > 0) {
            $(currentRow).find("#TotalAmountAED").val((0 * $(currentRow).find("#ExchangeRate").val()).toFixed(NoOfDecimalPlaces));
        }
    }
});

function initExpenseClaimRequestGrid() {
    if ($("#IsReadOnlyView").val() == "True") {
        ExpenseClaimDatatable = $('#tblExpenseClaimRequest').DataTable(
            {
                "createdRow": function (row, data, index) {
                    $(row).attr("RowID", index);
                },
                columnDefs: [
                    { width: "20%", targets: [0, 6] },
                    { width: "25%", targets: [1, 2, 3, 4, 5] },
                    { width: "30%", targets: [7] }
                ],
                "bFilter": true,
                "bInfo": true,
                "bSortCellsTop": true,
                "bStateSave": false,
                "paging": false,
                "aaSorting": [[0, 'desc']]
            }
        );
    }
    else {
        ExpenseClaimDatatable = $('#tblExpenseClaimRequest').DataTable(
            {
                "createdRow": function (row, data, index) {
                    $(row).attr("RowID", index);
                },
                columnDefs: [
                    { width: "20%", targets: [0, 6] },
                    { width: "25%", targets: [1, 2, 3, 4, 5] },
                    { width: "30%", targets: [7] },
                    {
                        width: "20%", targets: 8, 'createdCell': function (td, cellData, rowData, row, col) {
                            $(td).attr('class', 'center-text-align col-md-3 padding-0');
                        }
                    }
                ],
                "bFilter": true,
                "bInfo": true,
                "bSortCellsTop": true,
                "bStateSave": false,
                "paging": false,
                "aaSorting": [[0, 'desc']]
            }
        );
    }
    CalculateTotalExpense();
}

function CalculateTotalExpense() {
    totalExpense = 0;
    var rows = $('#tblExpenseClaimRequest > tbody tr');
    if (rows.length > 0) {
        $(rows).each(function (i, item) {
            if ($(item).attr("data-TotalAmountAED") != "" && $(item).attr("data-TotalAmountAED") != undefined) {
                totalExpense = totalExpense + parseFloat($(item).attr("data-TotalAmountAED"));
            }
        });
    }

    //totalExpense = 0;
    //for (var rowIndex = 0; rowIndex <= ExpenseClaimDatatable.rows().length; rowIndex++) {
    //    if (ExpenseClaimDatatable.cells(rowIndex, 6).data()[0].indexOf("TotalAmountAED") == -1) {
    //        totalExpense = totalExpense + parseFloat(ExpenseClaimDatatable.cells(rowIndex, 6).data()[0]);
    //    }
    //}
    $("#totalExpense").text(totalExpense.toFixed(NoOfDecimalPlaces));

}

function AddDataRow() {
    var newRow = ExpenseClaimDatatable.row.add([
        '<input type="text" class="DOB form-control"  id="Date" />',
        '<select class="form-control selectpickerddl" data-live-search="true" id="ddlClaimTypeID">' + selectClaimTypeList + '</select>',
        '<input type="text" class="form-control"  id="Description" />',
        '<select class="form-control selectpickerddl Currency" data-live-search="true" id="ddlCurrency">' + selectCurrencyList + '</select>',
        '<input type="number" class="form-control exchangerate"  id="ExchangeRate" disabled = "disabled" />',
        '<input type="number" class="form-control amount"  id="Amount" />',
        '<input type="number" class="form-control totalamount"  id="TotalAmountAED" disabled = "disabled"/>',
        '<div id="FileData">' +
        '<div class="input-group input-group-sm">' +
        '<div tabindex="-1" class="form-control file-caption  kv-fileinput-caption">' +
        '<span class="glyphicon glyphicon-file kv-caption-icon" style="display:none;" id="file_caption_name_Attachment"></span><div id="file_caption_id_Attachment"></div>' +
        '</div>' +
        '<div class="input-group-btn">' +
        '<button type="button" class="btn btn-default fileinput-remove fileinput-remove-button" style="display:none;" id="btnRemove_Attachment" onclick="RemoveDocument(this)"><i class="glyphicon glyphicon-ban-circle"></i> Remove</button>' +
        '<div class="btn btn-primary btn-file" id="divBrowse_Attachment">' +
        '<input type="file" multiple="" name="fu_Attachment" id="fu_Attachment" class="Attachment"> Browse' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<span id="txtuploadedMsgAdd_Attachment" style="margin-top: 3px;display:none;"> </span>' +
        '<div id="divPreview_Attachment">' +
        '</div>' +
        '</div>',
        '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;" /></button></div>'
    ]).draw();
    var newNode = newRow.nodes();
    $(newNode).attr("data-ClaimDetailID", 0);
    $(newNode).attr("data-Date", "");
    $(newNode).attr("data-ClaimTypeID", "");
    $(newNode).attr("data-Description", "");
    $(newNode).attr("data-CurrencyID", "");
    $(newNode).attr("data-ExchangeRate", "");
    $(newNode).attr("data-Amount", "");
    $(newNode).attr("data-TotalAmountAED", "");
    $(newNode).attr("data-ExpClaimFileID", "");
    bindDatePicker(".DOB");
    bindSelectpicker(".selectpickerddl");
}

function saveRow(source) {
    var FileData;
    var AlreadyFileExist = false;
    var currentRow = $(source).closest("tr");
    if (currentRow.find("#fu_Attachment").get(0).files.length > 0) {
        FileData = currentRow.find("#fu_Attachment").get(0).files[0];
    }
    else {
        FileData = null;
        if (currentRow.find("#divPreview_Attachment").text().trim() != "" || currentRow.find("#PreviewFile").text().trim() != "") {
            AlreadyFileExist = true;
        }
    }
    if ($(currentRow).find("#txtuploadedMsgAdd_Attachment").text().trim() != "") {
        ShowMessage('error', 'Please fill all field');
        return;
    }
    var Date = currentRow.find("#Date").val();
    var ClaimTypeID = currentRow.find("#ddlClaimTypeID").val();
    var ClaimType = currentRow.find("#ddlClaimTypeID option:selected").text();
    var Description = currentRow.find("#Description").val();
    var CurrencyID = currentRow.find("#ddlCurrency").val();
    var Currency = currentRow.find("#ddlCurrency option:selected").text();
    var ExchangeRate = currentRow.find("#ExchangeRate").val();
    var Amount = currentRow.find("#Amount").val();
    var TotalAmountAED = currentRow.find("#TotalAmountAED").val();
    var ExpClaimFileID = currentRow.find("#item_ExpClaimFileID").val();
    var ClaimDetailID = currentRow.attr("data-claimdetailid");
    if (ClaimDetailID == "" || ClaimDetailID == undefined) {
        ClaimDetailID = 0;
    }
    if (ExpClaimFileID == "" || ExpClaimFileID == undefined) {
        ExpClaimFileID = 0;
    }
    var RowID = currentRow.attr("rowid");


    var ExpClaimFile = currentRow.find('td:eq(7)').html();
    if (Date != undefined && Date != "" && ClaimTypeID != undefined && ClaimTypeID > 0 && Description != undefined && Description != "" &&
        CurrencyID != undefined && CurrencyID > 0 && ExchangeRate != undefined && ExchangeRate != "" && Amount != undefined && Amount != "" &&
        TotalAmountAED != undefined && TotalAmountAED != "" && (FileData != null || AlreadyFileExist)) {

        var newRow = ExpenseClaimDatatable.row(currentRow).data([
            Date,
            ClaimType,
            Description,
            Currency,
            ExchangeRate,
            Amount,
            TotalAmountAED,
            ExpClaimFile,
            '<div class="center-text-align">' +
            '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditRow(this)" id="btnEdit"  title="Edit"><i class="fa fa-pencil"></i> </a>' +
            '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(this)" title="Delete"><i class="fa fa-times"></i> </a>' +
            '</div>'
        ]).draw();
        var newNode = newRow.nodes();
        $(newNode).attr("data-ClaimDetailID", ClaimDetailID);
        $(newNode).attr("data-Date", Date);
        $(newNode).attr("data-ClaimTypeID", ClaimTypeID);
        $(newNode).attr("data-Description", Description);
        $(newNode).attr("data-CurrencyID", CurrencyID);
        $(newNode).attr("data-ExchangeRate", ExchangeRate);
        $(newNode).attr("data-Amount", Amount);
        $(newNode).attr("data-TotalAmountAED", TotalAmountAED);
        $(newNode).attr("data-ExpClaimFileID", ExpClaimFileID);
        var reader = new FileReader();
        var ExpenseClaimRequestDetail = {};
        var isAvailable = false;
        for (var i = 0; i < ExpenseClaimRequestDetailModel.length; i++) {
            if (ExpenseClaimRequestDetailModel[i].RowID == parseInt(RowID)) {
                isAvailable = true;
                ExpenseClaimRequestDetailModel[i].ClaimDetailID = parseInt(ClaimDetailID);
                ExpenseClaimRequestDetailModel[i].Date = Date.toString();
                ExpenseClaimRequestDetailModel[i].ClaimTypeID = parseInt(ClaimTypeID);
                ExpenseClaimRequestDetailModel[i].Description = Description.toString();
                ExpenseClaimRequestDetailModel[i].CurrencyID = parseInt(CurrencyID);
                ExpenseClaimRequestDetailModel[i].ExchangeRate = parseFloat(ExchangeRate);
                ExpenseClaimRequestDetailModel[i].Amount = parseFloat(Amount);
                ExpenseClaimRequestDetailModel[i].TotalAmountAED = parseFloat(TotalAmountAED);
                ExpenseClaimRequestDetailModel[i].ExpClaimFileID = ExpClaimFileID;
                ExpenseClaimRequestDetailModel[i].AlreadyFileExist = AlreadyFileExist;
                if (!AlreadyFileExist) {
                    ExpenseClaimRequestDetailModel[i].FileData = FileData;
                    ExpenseClaimRequestDetailModel[i].FileName = FileData.name;
                }
                break;
            }
        }
        if (!isAvailable) {
            if (AlreadyFileExist) {
                ExpenseClaimRequestDetailModel.push({
                    RowID: parseInt(RowID), ClaimDetailID: parseInt(ClaimDetailID), Date: Date.toString(), ClaimTypeID: parseInt(ClaimTypeID), Description: Description.toString(),
                    CurrencyID: parseInt(CurrencyID), ExchangeRate: ExchangeRate.toString(), Amount: Amount.toString(), TotalAmountAED: TotalAmountAED.toString(), ExpClaimFileID: parseInt(ExpClaimFileID),
                    FileData: null, FileName: '', AlreadyFileExist: AlreadyFileExist
                });
            }
            else {
                ExpenseClaimRequestDetailModel.push({
                    RowID: parseInt(RowID), ClaimDetailID: parseInt(ClaimDetailID), Date: Date.toString(), ClaimTypeID: parseInt(ClaimTypeID), Description: Description.toString(),
                    CurrencyID: parseInt(CurrencyID), ExchangeRate: ExchangeRate.toString(), Amount: Amount.toString(), TotalAmountAED: TotalAmountAED.toString(), ExpClaimFileID: parseInt(ExpClaimFileID),
                    FileData: FileData, FileName: FileData.name, AlreadyFileExist: AlreadyFileExist
                });
            }
        }
        currentRow.find("#btnRemove_Attachment").attr("disabled", "disabled");
        CalculateTotalExpense();

    }
    else {
        ShowMessage('error', 'Please fill all field');
        return;
    }
}

function EditRow(source) {
    var currentRow = $(source).closest("tr");
    var ClaimDetailID = $(currentRow).data().claimdetailid;
    var Date = $(currentRow).data().date;
    var ClaimTypeID = $(currentRow).data().claimtypeid;
    var Description = $(currentRow).data().description;
    var CurrencyID = $(currentRow).data().currencyid;
    var ExchangeRate = $(currentRow).data().exchangerate;
    var Amount = $(currentRow).data().amount;
    var TotalAmountAED = $(currentRow).data().totalamountaed;
    var ExpClaimFileID = $(currentRow).data().expclaimfileid;
    var RowID = $(currentRow).attr("rowid");
    //ExpenseClaimDatatable.cell(RowID, 0).data('<input class="DOB form-control" id="Date" name="Date" type="text" value="">').draw();
    //ExpenseClaimDatatable.cell(RowID, 1).data('<select class="form-control selectpickerddl"  data-live-search="true" id="ddlClaimTypeID" name="ClaimTypeID">' + selectClaimTypeList + '</select>').draw();
    //ExpenseClaimDatatable.cell(RowID, 2).data('<input class="form-control" id="Description" name="Description" type="text" value="">').draw();
    //ExpenseClaimDatatable.cell(RowID, 3).data('<select class="form-control Currency selectpickerddl"  data-live-search="true" id="ddlCurrency" name="CurrencyID">' + selectCurrencyList + '</select>').draw();
    //ExpenseClaimDatatable.cell(RowID, 4).data('<input class="form-control exchangerate" id="ExchangeRate" name="ExchangeRate" type="number" value=""  disabled = "disabled">').draw();
    //ExpenseClaimDatatable.cell(RowID, 5).data('<input class="form-control amount" id="Amount" name="Amount" type="number" value=""  >').draw();
    //ExpenseClaimDatatable.cell(RowID, 6).data('<input class="form-control totalamount" id="TotalAmountAED" name="TotalAmountAED" type="number" value=""  disabled = "disabled">').draw();
    //ExpenseClaimDatatable.cell(RowID, 8).data('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>').draw();
    currentRow.find('td:eq(0)').html('<input class="DOB form-control" id="Date" name="Date" type="text" value="">');
    currentRow.find('td:eq(1)').html('<select class="form-control selectpickerddl"  data-live-search="true" id="ddlClaimTypeID" name="ClaimTypeID">' + selectClaimTypeList + '</select>');
    currentRow.find('td:eq(2)').html('<input class="form-control" id="Description" name="Description" type="text" value="">');
    currentRow.find('td:eq(3)').html('<select class="form-control selectpickerddl"  data-live-search="true" id="ddlCurrency" name="CurrencyID">' + selectCurrencyList + '</select>');
    currentRow.find('td:eq(4)').html('<input class="form-control exchangerate" id="ExchangeRate" name="ExchangeRate" type="number" value=""  disabled = "disabled">');
    currentRow.find('td:eq(5)').html('<input class="form-control amount" id="Amount" name="Amount" type="number" value=""  >');
    currentRow.find('td:eq(6)').html('<input class="form-control totalamount" id="TotalAmountAED" name="TotalAmountAED" type="number" value=""  disabled = "disabled">');
    currentRow.find('td:eq(8)').html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');
    //currentRow.find('td:eq(7)').html('<div class="input-group input-group-sm">' +
    //    '<div tabindex = "-1" class="form-control file-caption  kv-fileinput-caption">' +
    //    '<span class="glyphicon glyphicon-file kv-caption-icon" style="display:none;" id="file_caption_name_Attachment"></span> <div id="file_caption_id_Attachment"></div>' +
    //    '</div >' +
    //    '<div class="input-group-btn">' +
    //    '<button type="button" class="btn btn-default fileinput-remove fileinput-remove-button" style="display:none;" id="btnRemove_Attachment" onclick="RemoveDocument(this)"><i class="glyphicon glyphicon-ban-circle"></i> Remove</button>' +
    //    '<div class="btn btn-primary btn-file" id="divBrowse_Attachment">' +
    //    '<input type="file" multiple="" name="fu_Attachment" id="fu_Attachment" class="Attachment"> Browse ' +
    //    '</div > ' +
    //    '</div>' +
    //    '</div>' +
    //    '<span id="txtuploadedMsgAdd_Attachment" style="margin-top: 3px;display:none;"> </span>' +
    //    '<div id="divPreview_Attachment">' +
    //    '</div>');
    currentRow.find('td:eq(8)').html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');

    currentRow.find('#Date').val(Date);
    currentRow.find("#ddlClaimTypeID").val(ClaimTypeID);
    currentRow.find("#Description").val(Description);
    currentRow.find("#ddlCurrency").val(CurrencyID);
    currentRow.find("#ExchangeRate").val(ExchangeRate);
    currentRow.find("#Amount").val(Amount);
    currentRow.find("#TotalAmountAED").val(TotalAmountAED);
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $(currentRow).find("#btnRemove_Attachment").removeAttr("disabled");
}

function DeleteRow(source) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to delete record?" }).done(function () {
        var currentRow = $(source).closest("tr");
        var RowID = currentRow.attr("rowid");
        var ClaimDetailID = $(currentRow).data().claimdetailid;
        var ExpclaimFileId = $(currentRow).data().expclaimfileid;
        ExpenseClaimDatatable.row(currentRow).remove().draw();
        ExpenseClaimRequestDetailModel.forEach(function (result, index) {
            if (result['RowID'] == RowID) {
                ExpenseClaimRequestDetailModel.splice(index, 1);
            }
        });
        if (ExpclaimFileId > 0) {
            if (DeletedClaimFileIDs.length > 0) {
                DeletedClaimFileIDs = DeletedClaimFileIDs + "," + ExpclaimFileId.toString();
            }
            else {
                DeletedClaimFileIDs = ExpclaimFileId.toString();
            }
        }
        if (ClaimDetailID > 0) {
            if (DeletedClaimDetailIDs.length > 0) {
                DeletedClaimDetailIDs = DeletedClaimDetailIDs + "," + ClaimDetailID.toString();
            }
            else {
                DeletedClaimDetailIDs = ClaimDetailID.toString();
            }
        }
        CalculateTotalExpense();

    });
}

function CancelNewRow(source) {
    var currentRow = $(source).closest("tr");
    ExpenseClaimDatatable.row(currentRow).remove().draw();
}

function RemoveDocument(source) {
    var currentRow = $(source).closest("tr");
    var fileid = $("#hdnFileUpload_Attachment").val();
    if (fileid == '' || fileid == null || fileid == '0') {
        $(currentRow).find("#btnRemove_Attachment").hide();
        $(currentRow).find("#divBrowse_Attachment").show();
        $(currentRow).find("#file_caption_name_Attachment").hide();
        $(currentRow).find("#file_caption_name_Attachment").html('');
        $(currentRow).find("#file_caption_id_Attachment").html('');
        $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("");
        $(currentRow).find("#fu_Attachment").val("");
        $(currentRow).find("#divPreview_Attachment").html("");
        $(currentRow).find("#hdnFileUpload_Attachment").val("0");
    }
    else {
        $(currentRow).find("#btnUploadAdd_Attachment").hide();
        $(currentRow).find("#btnRemove_Attachment").hide();
        $(currentRow).find("#divBrowse_Attachment").show();
        $(currentRow).find("#file_caption_name_Attachment").hide();
        $(currentRow).find("#file_caption_name_Attachment").html('');
        $(currentRow).find("#file_caption_id_Attachment").html('');
        $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("");
        $(currentRow).find("#divPreview_Attachment").empty();
        $(currentRow).find("#fu_Attachment").val("");
    }
}

function BindExpenseClaimTypes() {
    selectClaimTypeList = "<option value=''>Select Claim Type</option>";
    $.ajax({
        type: 'GET',
        url: '/ExpenseClaimRequest/GetExpenseClaimTypes',
        datatype: 'Json',
        success: function (data) {
            $.each(data, function (key, value) {
                selectClaimTypeList += '<option value=' + value.id + '>' + value.text + '</option>';
            });
        }
    });
}
function BindCurrency() {
    selectCurrencyList = "<option value=''>Select Currency</option>";
    $.ajax({
        type: 'GET',
        url: '/ExpenseClaimRequest/GetCurrency',
        datatype: 'Json',
        success: function (data) {
            $.each(data, function (key, value) {
                selectCurrencyList += '<option value=' + value.id + '>' + value.text + '</option>';
            });
        }
    });
}