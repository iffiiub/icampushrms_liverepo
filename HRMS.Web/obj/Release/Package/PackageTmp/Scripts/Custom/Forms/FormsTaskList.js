﻿$(document).ready(function () {
    $('.tblRequest').dataTable();
    $(".TasksListCollapse").click(function () {
        CollapseTaskList($(this).data("collapseid"));
    });
});

function ShowRequestForm(contollername, formprocessid, reqstatusid) {
    var urlstring = '';   
    if (reqstatusid == 1 || reqstatusid == 3)
        urlstring = '/' + contollername + '/Edit';
    else
        urlstring = '/' + contollername + '/ViewDetails';
    $.ajax({
        //dataType: 'json',
        type: 'POST',
        url: '/' + contollername + '/SetFormProcessSesssion',
        data: { id: formprocessid },
        success: function (result) {
            if (result.id != null)
                location.href = urlstring;

        },
        error: function (err) {
            // hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}
function EditRequestForm(contollername, formprocessid)
{
    debugger;
    var urlstring = '';
    //if (reqstatusid == 1 || reqstatusid == 3)
    urlstring = '/' + contollername + '/Edit';
    $.ajax({
        //dataType: 'json',
        type: 'POST',
        url: '/' + contollername + '/SetFormProcessSesssion',
        data: { id: formprocessid },
        success: function (result) {
            if (result.id != null)
                location.href = urlstring;

        },
        error: function (err) {
            // hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}
function CollapseTaskList(status) {
    if (status == true) {
        $(".IsMyTaskList").addClass("hidden")
        if ($(".IsMyRequest").hasClass("hidden")) {
            $(".IsMyRequest").removeClass("hidden")
            //$("#taskListLabel").text("My Tasks List");
        }
        else {           
            $(".IsMyRequest").addClass("hidden")
        }
    }
    else {
        $(".IsMyRequest").addClass("hidden")
        if ($(".IsMyTaskList").hasClass("hidden")) {           
            $(".IsMyTaskList").removeClass("hidden");
            //$("#taskListLabel").text("My Task");
        }
        else {
            $(".IsMyTaskList").addClass("hidden");
            //$("#taskListLabel").text("My Tasks List");
        }
    }
}