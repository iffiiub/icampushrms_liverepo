﻿$(document).ready(function () {
    var formid = 29;
    var formprocessid = $("#hdnFormProcessID").val();
    $("#ddlSalaryRanges").val($("#hdnSalaryRanges").val());
    $("#ddlSalaryRanges").selectpicker("refresh");

    if ($("#ddlContractStatus").val() === 'Family')
        $("#divSpouse").show();
    else
        $("#divSpouse").hide();
    if ($("#ddlSalikTag").val() === 'true')
        $("#divSalikTag").show();
    else
        $("#divSalikTag").hide();

    if ($("#ddlPetrolCard").val() === 'true')
        $("#divPetrolCard").show();
    else
        $("#divPetrolCard").hide();

    if ($("#ddlParkingCard").val() === 'true')
        $("#divParking").show();
    else
        $("#divParking").hide();


    $('.DecimalOnly').keypress(function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var companyIds = $("#ddlCompanyList").val();
            var $form = $("#frmHiringR1Replacement");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid() && ValidateData()) {
                SaveForm($form);
            }
            else {
                ValidateData();
            }
        }
    });

    $("#ddlHMEmploye").change(function () {

        $("#txtPosition").val('');
        var empid = $(this).val();
        var Url = "/Common/GetEmployeeDesignation?employeeID=" + empid;

        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: Url,
            success: function (result) {

                $("#txtPosition").val(result);
            }
        });
        //});
    });

    $('#ddlCompanyList').change(function (event) {
        var companyId = $("#ddlCompanyList").val();
        if (companyId != null || companyId !== '')
            $("#spCompanyIDValidator").text('');
        var Url = "/R1UnBudgetedReplacement/GetAllEmployeeForAdminByCompanyID?CompanyId=" + companyId;

        $.ajax({
            url: Url, success: function (result) {


                $('#ddlHMEmploye').children('option[value!=""]').remove();
                $('#ddlReportingEmploye').children('option[value!=""]').remove();

                $.each(result, function (key, value) {
                    $('#ddlHMEmploye')
                        .append($("<option> </option>")
                            .attr("value", value.EmployeeID)
                            .text(value.FullName));

                    $('#ddlReportingEmploye')
                        .append($("<option> </option>")
                            .attr("value", value.EmployeeID)
                            .text(value.FullName));
                });

                $("#ddlHMEmploye").selectpicker("refresh");
                $("#ddlReportingEmploye").selectpicker("refresh");
            }
        });
        //GetSalaryRanges();
    });

    $("#ddlContractStatus").change(function () {
        if ($(this).val() === 'Family') {
            $("#divSpouse").show();
        } else {
            $("#ddlFamilySpouse").val('');
            $("#divSpouse").hide();
        }
    });

    $("#ddlSalikTag").change(function () {
        if ($(this).val() == "true") {
            $("#divSalikTag").show();
        } else {
            $("#txtSalikAmount").val(0);
            $("#divSalikTag").hide();

        }
    });

    $("#ddlPetrolCard").change(function () {
        if ($(this).val() == "true") {
            $("#divPetrolCard").show();
        } else {
            $("#txtPetrolCardAmount").val(0);
            $("#divPetrolCard").hide();

        }
    });

    $("#ddlParkingCard").change(function () {

        if ($(this).val() == "true") {
            $("#divParking").show();
        } else {
            $("#divParking").hide();

        }
    });

    $("#ddlAnnualAirTicket").change(function () {
        if ($(this).val() === 'false') {
            $("#ddlAirfareFrequency").val("0");
            $("#ddlAirfareClass").val("0");
            RefreshSelect('#ddlAirfareFrequency');
            RefreshSelect('#ddlAirfareClass');
            $("#ddlAirfareFrequency").attr("disabled", "disabled");
            $("#ddlAirfareClass").attr("disabled", "disabled");
        } else {
            $("#ddlAirfareFrequency").removeAttr("disabled");
            $("#ddlAirfareClass").removeAttr("disabled");
            $("#ddlAirfareFrequency").val("");
            $("#ddlAirfareClass").val("");
            RefreshSelect('#ddlAirfareFrequency');
            RefreshSelect('#ddlAirfareClass');
        }
    });

});

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);
}

function SaveForm($form) {

    var fileJD, fileOC, fileMP;
    var companyIds = $("#ddlCompanyList").val();
    var formprocessid = $("#hdnFormProcessID").val();
    var R1ReplacementID = $("#hdnR1ReplacementID").val();
    var deletedfileids = "";

    pageLoaderFrame();
    var hiringR1ReplacementModel = {
        ID: $("#hdnReplacementID").val(),
        CompanyID: $("#ddlCompanyList").val(),
        EmploymentModeID: $("#ddlEmploymentMode").val(),
        ReplacedEmployeeID: $("#ddlEmployee").val(),
        UserTypeID: $("#ddlUserType").val(),
        EmployeeJobCategoryID: $("#ddlJobCategoryID").val(),
        DepartmentID: $("#ddlDepartment").val(),
        ProjectData: $("#txtProjectData").val(),
        HMEmployeeID: $("#ddlHMEmploye").val(),
        PositionLocation: $("#txtPositionLocation").val(),
        HeadCount: $("#txtHeadCount").val(),
        JobGradeID: $("#ddlJobGrade").val(),
        ReportingEmployeeID: $("#ddlReportingEmploye").val(),
        MinExpRequired: $("#txtMinExpRequired").val(),
        SourceID: $("#ddlSource").val(),
        RecruitFromID: $("#ddlRecruitFrom").val(),
        PositionID: $("#ddlPosition").val(),
        //SalaryRangesID: $("#ddlSalaryRanges").val(),
        VehicleToolTrade: $("#ddlVehicleToolTrade").val(),
        ContractStatus: $("#ddlContractStatus").val(),
        FamilySpouse: $("#ddlFamilySpouse").val(),
        AnnualAirTicket: $("#ddlAnnualAirTicket").val(),
        AirfareFrequencyID: $("#ddlAirfareFrequency").val(),
        AirfareClassID: $("#ddlAirfareClass").val(),
        HealthInsurance: $("#ddlHealthInsurance").val(),
        LifeInsurance: $("#ddlLifeInsurance").val(),
        SalikTag: $("#ddlSalikTag").val(),
        SalikAmount: $("#txtSalikAmount").val(),
        PetrolCard: $("#ddlPetrolCard").val(),
        PetrolCardAmount: $("#txtPetrolCardAmount").val(),
        ParkingCard: $("#ddlParkingCard").val(),
        ParkingAreas: $("#txtParkingAreas").val(),
        DivisionID: $("#DivisionID").val(),
        AcademicYearID: $("#ddlAcademicYear").val(),
        ActualBudget: $("#txtActualBudget").val(),
        Comments: $("#txtComments").val(),
        IsReplacementRecord: $("#IsReplacementRecord").val(),
        JobDescriptionFileID: $("#hdnJobDescriptionFileID").val(),
        OrgChartFileID: $("#hdnOrgChartFileID").val(),
        ManPowerFileID: $("#hdnManPowerFileID").val(),
        RequestInitialize: $("#hdnRequestInitialize").val(),
        ReqStatusID: $("#hdnReqStatusID").val(),
        RequesterEmployeeID: $("#hdnRequesterEmployeeID").val(),
        SalaryRanges: $("#ddlSalaryRanges").val()
    }

    var formData = new FormData($form[0]);
    if ($("#fu_JD").get(0).files.length > 0) {
        fileJD = $("#fu_JD").get(0).files;
        formData.append("JobDescription", fileJD[0]);
    }
    else
        formData.append("JobDescription", null);

    if ($("#fu_OC").get(0).files.length > 0) {
        fileOC = $("#fu_OC").get(0).files;
        formData.append("OrganizationChart", fileOC[0]);
    }
    else
        formData.append("OrganizationChart", null);

    if ($("#fu_MP").get(0).files.length > 0) {
        fileMP = $("#fu_MP").get(0).files;
        formData.append("ManpowerPlan", fileMP[0]);
    }
    else
        formData.append("ManpowerPlan", null);

    formData.append("hiringR1ReplacementModel", JSON.stringify(hiringR1ReplacementModel));
    formData.append("JobDescriptionFileID", $("#hdnJobDescriptionFileID").val());
    formData.append("OrgChartFileID", $("#hdnOrgChartFileID").val());
    formData.append("ManPowerFileID", $("#hdnManPowerFileID").val());
    formData.append("FormProcessID", formprocessid);

    $.ajax({
        url: '/R1UnBudgetedReplacement/SaveForm',
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            hideLoaderFrame();
            if (result.CssClass != 'error') {
                location.reload();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });

}

function ValidateData() {
    var filejd = $("#fu_JD").val();
    var filempid = $("#hdnFileUpload_MP").val();
    var fileocid = $("#hdnFileUpload_OC").val();
    var filejdid = $("#hdnFileUpload_JD").val();
    //if the file is removed these hiddenfield will hold its ids
    var jdfiledeleteid = $("#divPreview_JD").html().trim();
    var ocfiledeleteid = $("#divPreview_OC").html().trim();
    var mpfiledeleteid = $("#divPreview_MP").html().trim();
    var isvalid = true;
    var isjdfile = true;
    var isocfile = true;
    var ismpfile = true;

    if (jdfiledeleteid == '') {
        isvalid = false;
        isjdfile = false;
    }

    if (ocfiledeleteid == '') {
        isvalid = false;
        isocfile = false;
    }

    if (mpfiledeleteid == '') {
        isvalid = false;
        ismpfile = false;
    }

    if (isvalid)
        return true
    else {
        if (!isjdfile) {
            $("#txtuploadedMsgAdd_JD").text("This field is mandatory.");
        }
        if (!isocfile) {
            $("#txtuploadedMsgAdd_OC").text("This field is mandatory.");
        }
        if (!ismpfile) {
            $("#txtuploadedMsgAdd_MP").text("This field is mandatory.");
        }
        return false;
    }

}

function GetSalaryRanges() {
    var positionId = $('#ddlPosition').val();
    var companyId = $('#ddlCompanyList').val();
    $('#ddlSalaryRanges').find('option').remove();
    $('#ddlSalaryRanges')
        .append($("<option> </option>")
            .attr("value", "")
            .text("Select Salary Range"));
    if (companyId !== '' && positionId !== null && positionId !== '') {
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: { "companyID": companyId, "positionID": positionId },
            url: '/Common/GetSalaryRange',
            async: false,
            success: function (result) {

                $.each(result, function (key, value) {

                    $('#ddlSalaryRanges')
                        .append($("<option> </option>")
                            .attr("value", value.SalaryRangesID)
                            .text(value.SalaryRanges));

                });
                RefreshSelect('#ddlSalaryRanges');
            }
        });
    }
    else {
        RefreshSelect('#ddlSalaryRanges');
    }
}

function GetSalaryRangeDetails(id) {
    if (id !== null && id !== '' && id != undefined) {
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/SalaryRange/GetSalaryRangeByID?salaryRangeID=' + id,
            success: function (result) {
                $("#hdnMaximumSalary").val(result.MaxSalary);
            }
        });
    }
    else
        $("#hdnMaximumSalary").val(0);
}

$("#txtSalikAmount").on('change keyup paste', function () {
    if ($("#txtSalikAmount").val() === '') {
        $("#spSalikAmountValidator").css("color", "red");
        $("#spSalikAmountValidator").text("This field is mandatory.");
    }
    else
        $("#spSalikAmountValidator").text("");
});

$("#ddlEmployee").on('change', function () {
    pageLoaderFrame();
    var EmployeeID = $("#ddlEmployee").val();
    $(".field-validation-error").html('');
    if (EmployeeID == "")
        EmployeeID = null;
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: { "EmployeeID": EmployeeID },
        url: '/R1UnBudgetedReplacement/GetR1ReplacementDetails',
        success: function (result) {
            $("#IsReplacementRecord").val(result.IsReplacementRecord);
            $("#ddlCompanyList").val(result.CompanyID).change();
            $("#ddlAcademicYear").val(result.AcademicYearID);
            $("#ddlEmploymentMode").val(result.EmploymentModeID);
            $("#ddlUserType").val(result.UserTypeID);
            $("#ddlJobCategoryID").val(result.EmployeeJobCategoryID);
            $("#ddlDepartment").val(result.DepartmentID);
            $("#txtProjectData").val(result.ProjectData);
            $("#ddlHMEmploye").val(result.HMEmployeeID);
            $("#txtPosition").val(result.HMDesignation);
            $("#txtPositionLocation").val(result.PositionLocation);
            $("#txtHeadCount").val(result.HeadCount == 0 ? 1 : result.HeadCount);
            $("#ddlJobGrade").val(result.JobGradeID);
            $("#ddlReportingEmploye").val(result.ReportingEmployeeID);
            $("#txtMinExpRequired").val(result.MinExpRequired);
            $("#ddlSource").val(result.SourceID);
            $("#ddlRecruitFrom").val(result.RecruitFromID);
            $("#DivisionID").val(result.DivisionID);
            $("#ddlPosition").val(result.PositionID);
            //GetSalaryRanges();
            $("#ddlSalaryRanges").val(result.SalaryRangesID);
            $("#txtActualBudget").val(result.ActualBudget);
            //--If ActualBudget >0 then it should disabled.
            if (result.ActualBudget == '' || result.ActualBudget > 0) {
                $("#txtActualBudget").attr("disabled", "disabled");
            }
            else {
                $("#txtActualBudget").removeAttr("disabled");
            }
            $("#ddlVehicleToolTrade").val(result.VehicleToolTrade == null ? result.VehicleToolTrade : result.VehicleToolTrade.toString());
            $("#ddlContractStatus").val(result.ContractStatus);
            $("#ddlFamilySpouse").val(result.FamilySpouse);
            $("#ddlAnnualAirTicket").val(result.AnnualAirTicket == null ? result.AnnualAirTicket : result.AnnualAirTicket.toString());
            $("#ddlAirfareFrequency").val(result.AirfareFrequencyID);
            $("#ddlAirfareClass").val(result.AirfareClassID);
            $("#ddlHealthInsurance").val(result.HealthInsurance == null ? result.HealthInsurance : result.HealthInsurance.toString());
            $("#ddlLifeInsurance").val(result.LifeInsurance == null ? result.LifeInsurance : result.LifeInsurance.toString());
            $("#ddlSalikTag").val(result.SalikTag == null ? result.SalikTag : result.SalikTag.toString());
            $("#txtSalikAmount").val(result.SalikAmount);
            $("#ddlPetrolCard").val(result.PetrolCard == null ? result.PetrolCard : result.PetrolCard.toString());
            $("#txtPetrolCardAmount").val(result.PetrolCardAmount);
            $("#ddlParkingCard").val(result.ParkingCard == null ? result.ParkingCard : result.ParkingCard.toString());
            $("#txtParkingAreas").val(result.ParkingAreas);
            $('#divPreview_JD').html('');
            $('#divPreview_OC').html('');
            $('#divPreview_MP').html('');

            result.AllFormsFilesModelList.forEach(function (p) {
                if (p.FormFileIDName == 'JobDescriptionFileID') {
                    $('#divPreview_JD').append('<a onclick="OpenFilePreview(\'R1UnBudgetedReplacement\',' + p.FileID + ')">' + p.FileName + '</a>');
                    $("#hdnJobDescriptionFileID").val(p.FileID);
                }
                if (p.FormFileIDName == 'OrgChartFileID') {
                    $('#divPreview_OC').append('<a onclick="OpenFilePreview(\'R1UnBudgetedReplacement\',' + p.FileID + ')">' + p.FileName + '</a>');
                    $("#hdnOrgChartFileID").val(p.FileID);
                }
                if (p.FormFileIDName == 'ManPowerFileID') {
                    $('#divPreview_MP').append('<a onclick="OpenFilePreview(\'R1UnBudgetedReplacement\',' + p.FileID + ')">' + p.FileName + '</a>');
                    $("#hdnManPowerFileID").val(p.FileID);
                }
            });

            RefreshSelect("#ddlAcademicYear");
            RefreshSelect("#ddlEmploymentMode");
            RefreshSelect("#ddlUserType");
            RefreshSelect("#ddlJobCategoryID");
            RefreshSelect("#ddlDepartment");
            RefreshSelect("#ddlHMEmploye");
            RefreshSelect("#ddlJobGrade");
            RefreshSelect("#ddlReportingEmploye");
            RefreshSelect("#ddlSource");
            RefreshSelect("#ddlRecruitFrom");
            RefreshSelect("#DivisionID");
            RefreshSelect("#ddlVehicleToolTrade");
            RefreshSelect("#ddlContractStatus");
            RefreshSelect("#ddlFamilySpouse");
            RefreshSelect("#ddlAnnualAirTicket");
            RefreshSelect("#ddlAirfareFrequency");
            RefreshSelect("#ddlAirfareClass");
            RefreshSelect("#ddlHealthInsurance");
            RefreshSelect("#ddlLifeInsurance");
            RefreshSelect("#ddlSalikTag");
            RefreshSelect("#ddlPetrolCard");
            RefreshSelect("#ddlParkingCard");
            RefreshSelect("#ddlSalaryRanges");

            if (result.IsReplacementRecord) {
                RefreshDisabledDdl("#ddlCompanyList", true);
                RefreshDisabledDdl("#ddlPosition", true);
            }
            else {
                RefreshDisabledDdl("#ddlCompanyList", false);
                RefreshDisabledDdl("#ddlPosition", false);
            }

            if ($("#ddlSalikTag").val() === 'true')
                $("#divSalikTag").show();
            else
                $("#divSalikTag").hide();
            if ($("#ddlPetrolCard").val() === 'true')
                $("#divPetrolCard").show();
            else
                $("#divPetrolCard").hide();
            if ($("#ddlParkingCard").val() === 'true')
                $("#divParking").show();
            else
                $("#divParking").hide();

            if ($("#ddlSalaryRanges").val() != '') {
                $("#ddlSalaryRanges").attr("disabled", "disabled");
            }
            $("#txtActualBudget").attr("disabled", "disabled");

            hideLoaderFrame();
        }
    });
});

function RefreshDisabledDdl(id, mode) {
    if (mode) {
        RefreshSelect(id);
        $(id).attr("disabled", "disabled");
    }
    else {
        $(id).removeAttr("disabled");
        RefreshSelect(id);
    }
}