﻿var shiftChangejson = [];

var SelectedOption = 'ShiftDetailsByEmployee';
$(document).ready(function () {
    EmployeeIdsToTransfer = [];
    var ShiftID = sessionStorage.getItem("SelectedShift");
    if (ShiftID == null || ShiftID == 'undefined' || ShiftID == "") {
    }
    else {
        $("#ancShift_" + ShiftID).addClass('active');

        GetShiftDetails(ShiftID);

    }

    GetShiftNameList();

    $('input.timepicker').timepicker({
        minTime: '00:00:00',
        timeFormat:'h:iA',
        maxHour: 0,
        maxMinutes: 10,
        step: 5,
        code: 'A',        
        startTime: new Date(0, 0, 0, 15, 0, 0),
        disableTextInput: true,
        _twelveHourTime : true
    }).on('change', function () {
        createModificationJson(this);
    });

    $("#ShiftID_1").val(-1);
    bindIntgerOnly('.gracein');
    bindIntgerOnly('.graceout');
    bindIntgerOnly('.breakhours');

    // #column3_search is a <input type="text"> element
    $('#txtEmpSearchNotInShift').on('keyup', function () {
        var table = $('#tblEmployeesNotInShift').DataTable();
        table
            //.columns(1,2)
            .search(this.value)
            .draw();

    });

    $('#txtEmpSearchInShift').on('keyup', function () {
        var table = $('#tblEmployeesInShift').DataTable();
        table
            .columns(1)
            .search(this.value)
            .draw();
    });

    $('input:radio[name=ShiftDetailsBy]').change(function () {
        SelectedOption = this.value;
        getAssignShiftDetails();
    });

    $('#txtSectionSearchNotInShift').on('keyup', function () {
        var table = $('#tblSectionNotInShift').DataTable();
        table
            //.columns(1)
            .search(this.value)
            .draw();
    });

    $('#txtSectionSearchInShift').on('keyup', function () {
        var table = $('#tblSectionsInShift').DataTable();
        table
            .columns(1)
            .search(this.value)
            .draw();
    });

    $('#txtDepartmentSearchInShift').on('keyup', function () {
        var table = $('#tblDepartmentInShift').DataTable();
        table
            //.columns(1)
            .search(this.value)
            .draw();
    });


    $('#txtDepartmentSearchNotInShift').on('keyup', function () {
        var table = $('#tblDepartmentNotInShift').DataTable();
        table
           // .columns(1)
            .search(this.value)
            .draw();
    });

});

function createModificationJson(e) {
    var exist = false;
    var shiftDay = $(e).closest('tr').attr('data-shiftday');
    if (shiftDay == 7) {
        shiftDay = 0;
    }
    if ($(event.target).hasClass('weekend')) {
        if ($(e).is(':checked')) {
            $("#ShiftStart_" + (parseInt(shiftDay) + 1)).val('00:00')
            $("#ShiftStart_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftEnd_" + (parseInt(shiftDay) + 1)).val('00:00');
            $("#ShiftEnd_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftGraceIn_" + (parseInt(shiftDay) + 1)).val(0);
            $("#ShiftGraceIn_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftGraceOut_" + (parseInt(shiftDay) + 1)).val(0);
            $("#ShiftGraceOut_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftBreakHours_" + (parseInt(shiftDay) + 1)).val(0);
            $("#ShiftBreakHours_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#CutOffTime_" + (parseInt(shiftDay) + 1)).val('00:00');
            $("#CutOffTime_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
        }
        else {
            $("#ShiftStart_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftEnd_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftGraceIn_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftGraceOut_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftBreakHours_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#CutOffTime_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
        }
    }

    var shiftId = (parseInt($('#ShiftID').val()) > -1 ? parseInt($('#ShiftID').val()) : -1);
    var objshiftChangejson = {
        ShiftID: shiftId,
        shiftDay: parseInt(shiftDay),
        isChanged: true
    }
    if (shiftId > -1) {
        if (shiftChangejson.length == 0) {
            shiftChangejson.push(objshiftChangejson);
        } else {
            for (var i = 0; i < shiftChangejson.length; i++) {
                if (shiftChangejson[i].shiftDay == parseInt(shiftDay)) {
                    exist = true;
                    break;
                }
            }
            if (!exist)
                shiftChangejson.push(objshiftChangejson);
        }
    }
    if (shiftChangejson.length > 0)
        $("#hdnShiftChange").val(JSON.stringify(shiftChangejson))
}

function BindShiftDetails(data) {
    $("#IncludeInAttendance").prop('checked', data[0].IncludeInAttendance);
    $("#ShiftName").val(data[0].ShiftName);
    $("#ShiftID_1").val(data[0].ShiftID);
    var i = 1;
    $.each(data, function (id, option) {
        $.each(weekDaysJson, function (index, item) {
            if (item.Value == option.ShiftDay)
                i = item.bindValue;
        })
        $("#ObjShiftModelList_" + (i - 1) + "__ShiftDetailsId").val(option.ShiftDetailsId);
        if (option.IsWeekend) {
            $("#ShiftStart_" + i).val(option.ShiftStart).prop('disabled', true);
            $("#ShiftEnd_" + i).val(option.ShiftEnd).prop('disabled', true);
            $("#ShiftGraceIn_" + i).val(option.ShiftGraceIn).prop('disabled', true);
            $("#ShiftGraceOut_" + i).val(option.ShiftGraceOut).prop('disabled', true);
            $("#ShiftBreakHours_" + i).val(option.ShiftBreakHours).prop('disabled', true);
            $("#CutOffTime_" + i).val(option.CutOffTime).prop('disabled', true);
        }
        else {
            $("#ShiftStart_" + i).val(option.ShiftStart);
            $("#ShiftEnd_" + i).val(option.ShiftEnd);
            $("#ShiftGraceIn_" + i).val(option.ShiftGraceIn);
            $("#ShiftGraceOut_" + i).val(option.ShiftGraceOut);
            $("#ShiftBreakHours_" + i).val(option.ShiftBreakHours);
            $("#CutOffTime_" + i).val(option.CutOffTime);
            $("#ShiftStart_" + i).val(option.ShiftStart).prop('disabled', false);
            $("#ShiftEnd_" + i).val(option.ShiftEnd).prop('disabled', false);
            $("#ShiftGraceIn_" + i).val(option.ShiftGraceIn).prop('disabled', false);
            $("#ShiftGraceOut_" + i).val(option.ShiftGraceOut).prop('disabled', false);
            $("#ShiftBreakHours_" + i).val(option.ShiftBreakHours).prop('disabled', false);
            $("#CutOffTime_" + i).val(option.CutOffTime).prop('disabled', false);

        }
        $("#IsWeekend_" + i).prop('checked', option.IsWeekend);
        //i++;
    });
    //if (parseInt(data[0].ShiftID) > -1) {
    //    $("#GetAllEmployeesNotInShift").load("/Shift/GetAllEmployeesNotInShift/" + data[0].ShiftID);
    //    $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift/" + data[0].ShiftID);
    //}
    $('.hiddenShiftID').val(data[0].ShiftID);
}
function GetShiftDetails(ShiftId) {

    sessionStorage.setItem("SelectedShift", ShiftId);
    if ($("#hdnOldModel").val() != "") {
        var counter = 0;
        var oldData = JSON.parse($("#hdnOldModel").val());

        if (oldData[0].ShiftName != $("#ShiftName").val())
            counter++;

        var i = 1;

        $.each(oldData, function (id, option) {

            if (option.ShiftStart != $("#ShiftStart_" + i).val())
                counter++;
            if (option.ShiftEnd != $("#ShiftEnd_" + i).val())
                counter++;
            if (option.ShiftGraceIn != $("#ShiftGraceIn_" + i).val())
                counter++;
            if (option.ShiftGraceOut != $("#ShiftGraceOut_" + i).val())
                counter++;
            if (option.ShiftBreakHours != $("#ShiftBreakHours_" + i).val())
                counter++;
            if (option.IsWeekend != $("#IsWeekend_" + i).prop('checked'))
                counter++;
            if (option.CutOffTime != $("#CutOffTime_" + i).val())
                counter++;
            i++;
        });

        if (counter > 0) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save previous changes?" }).done(function () {
                pageLoaderFrame();
                $("#frmAddShift").ajaxSubmit(function (data) {
                    ShowMessage("success", data.Message);
                    $("#hdnOldModel").val("");
                    //window.location.reload();
                    $.ajax({
                        type: 'POST',
                        data: { ShiftID: ShiftId },
                        url: '/Shift/GetShiftDetails',
                        success: function (data) {
                            hideLoaderFrame();
                            BindShiftDetails(data);
                            //keep in hidden value
                            $("#hdnOldModel").val(JSON.stringify(data));
                        },
                        error: function (data) { }
                    });
                });
            }).fail(function () {
                GetAndBindShiftDetails(ShiftId);
            });
        }
        else {
            GetAndBindShiftDetails(ShiftId);
        }
    }
    else {
        GetAndBindShiftDetails(ShiftId);
    }

    SelectShift(ShiftId);

    $("#ddl_Shift").val(ShiftId);
    RefreshSelect("#ddl_Shift");
    if (SelectedOption == "ShiftDetailsByDept") {
        $("#shiftDepartmentDetails").removeClass('hidden');
        $("#shiftEmployeedetails").addClass('hidden');
        $("#shiftSectiondetails").addClass('hidden');
        if (ShiftId != "") {
            StartPanelLoader("#DepartmentNotInShiftList");
            StartPanelLoader("#ShiftDepartmentList");
            $("#DepartmentNotInShiftList").load("/Shift/GetAllDepartmentNotInShift?Id=" + ShiftId);
            $("#ShiftDepartmentList").load("/Shift/GetAllDepartmentInShift?Id=" + ShiftId);
        }
        else {
            StartPanelLoader("#DepartmentNotInShiftList");
            $("#DepartmentNotInShiftList").load("/Shift/GetAllDepartmentNotInShift?Id=-1");
        }
    }
    else if (SelectedOption == "ShiftDetailsBySection") {
        $("#shiftSectiondetails").removeClass('hidden');
        $("#shiftEmployeedetails").addClass('hidden');
        $("#shiftDepartmentDetails").addClass('hidden');
        if (ShiftId != "") {
            StartPanelLoader("#SectionNotInShiftList");
            StartPanelLoader("#ShiftSectionList");
            $("#ShiftSectionList").load("/Shift/GetAllSectionInShift?Id=" + ShiftId);
            $("#SectionNotInShiftList").load("/Shift/GetAllSectionNotInShift?Id=" + ShiftId);
        }
        else {
            StartPanelLoader("#SectionNotInShiftList");
            $("#SectionNotInShiftList").load("/Shift/GetAllSectionNotInShift?Id=-1");
        }
    }
    else if (SelectedOption == "ShiftDetailsByEmployee") {
        $("#shiftSectiondetails").addClass('hidden');
        $("#shiftEmployeedetails").removeClass('hidden');
        $("#shiftDepartmentDetails").addClass('hidden');
        if (ShiftId != "") {
            StartPanelLoader("#ShiftNonEmployeesList");
            StartPanelLoader("#ShiftEmployeesList");
            $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift?Id=" + ShiftId);
            $("#ShiftNonEmployeesList").load("/Shift/GetAllEmployeesNotInShift?Id=" + ShiftId);
        }
        else {
            StartPanelLoader("#ShiftEmployeesList");
            $("#ShiftNonEmployeesList").load("/Shift/GetAllEmployeesNotInShift?Id=-1");
        }
    }

}

function GetAndBindShiftDetails(ShiftId) {
    $.ajax({
        type: 'POST',
        data: { ShiftID: ShiftId },
        url: '/Shift/GetShiftDetails',
        async: true,
        success: function (data) {
            BindShiftDetails(data);
            //keep in hidden value
            $("#hdnOldModel").val(JSON.stringify(data));
        },
        error: function (data) { }
    });
}

function DeleteShiftID(ShiftID) {
    $.ajax({
        type: 'POST',
        data: { Id: ShiftID },
        url: '/Shift/GetAllEmployeesShiftCount',
        success: function (data) {
            var count = parseInt(data);
            if (count > 0) {
                ShowMessage("error", "Shift cannot be deleted as it has Employees in it.");
            }
            else {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are You sure You want to delete Shift?" }).done(function () {
                    $.ajax({
                        type: 'POST',
                        data: { id: ShiftID },
                        url: '/Shift/DeleteShift',
                        success: function (data) {
                            ShowMessage("success", "Shift deleted successfully");
                            GetShiftNameList();
                        },
                        error: function (data) { }
                    });
                });
            }
        },
        error: function (data) { }
    });
}

function CopyShift() {
    var key = 7;
    var s = $("#ShiftStart_" + key).val();
    for (var k = 1; k <= 7; k++) {
        $("#ShiftStart_" + k).val($("#ShiftStart_" + key).val());
        $("#ShiftEnd_" + k).val($("#ShiftEnd_" + key).val());
        $("#ShiftGraceIn_" + k).val($("#ShiftGraceIn_" + key).val());
        $("#ShiftGraceOut_" + k).val($("#ShiftGraceOut_" + key).val());
        $("#ShiftBreakHours_" + k).val($("#ShiftBreakHours_" + key).val());
        $("#CutOffTime_" + k).val($("#CutOffTime_" + key).val());
    }

}

function SaveShiftDetails() {
    $("#hdnOldModel").val("");
    var shiftId = $('#ShiftID_1').val();
    checkFuturRecord(shiftId, '[0,0]', function (a) {
        if (a.IsRecordExist == true) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "This shift change will overwrite the user shifts modification. Are you sure you want to save changes.?" }).done(function () {
                submitShiftDetails();
            }).fail(function () {

            });
        } else {
            submitShiftDetails();
        }
    });
}
function checkFuturRecord(shiftId, EmpId, callBack) {
    $("#hdnOldModel").val("");
    $.ajax({
        type: 'GET',
        data: { shiftId: shiftId, EmpIds: EmpId },
        url: '/Shift/CheckFuturRecordInAttShiftTable',
        success: function (data) {
            if (typeof callBack === "function")
                callBack(data);
        },
        error: function (data) { }
    });
}

$("#btnCancel").click(function () {
    $("#myModal").modal("hide");
});

function submitShiftDetails() {
    if ($('form#frmAddShift').valid()) {
        pageLoaderFrame();
    }
    $('form#frmAddShift').submit();
}

function onFailure(xhr, status, error) {
    hideLoaderFrame();
    console.log("xhr", xhr);
    console.log("status", status);

    ShowMessage("info", error);
}

function onSuccess(data, status, xhr, arg) {
    console.log("data", data);
    console.log("status", status);
    console.log("xhr", xhr);
    hideLoaderFrame();
    if (data.Success == true) {
        ShowMessage("success", data.Message);
        sessionStorage.setItem("SelectedShift", data.InsertedRowId);
        location.reload();
        GetShiftNameList();
    }
    else {
        ShowMessage("success", data.Message);
    }

    $('.hiddenShiftID').val(data.InsertedRowId);
}

function closeform() {
    $("#myModal").modal("hide");
}


function GetShiftNameList() {
    //$.ajax({
    //    type: 'POST',
    //    url: '/Shift/GetAllShiftNames',
    //    success: function (data) {
    StartPanelLoader("#shiftListView");
    $("#ShiftList").load("/Shift/GetAllShiftNames?isDeleteEnable=true");
    //    },
    //    error: function (data) { }
    //});
}

function AddNewShift() {
    sessionStorage.setItem("SelectedShift", null);
    location.reload();
}

function SelectShift(ShiftID) {

    $(".chkSelectShift").attr("disabled", false);
    $('a').removeClass('active');
    $("#ancShift_" + ShiftID).addClass('active');
    $("#chkSelectShift_" + ShiftID).attr("disabled", true);
}

function getAssignShiftDetails() {

    var selectedShift = $("#ddl_Shift").val();
    if (selectedShift == null || selectedShift == 'undefined' || selectedShift == "") {

    }
    else {
        $("#ancShift_" + selectedShift).addClass('active');

        sessionStorage.setItem("SelectedShift", selectedShift);
        if ($("#hdnOldModel").val() != "") {
            var counter = 0;
            var oldData = JSON.parse($("#hdnOldModel").val());

            if (oldData[0].ShiftName != $("#ShiftName").val())
                counter++;

            var i = 1;

            $.each(oldData, function (id, option) {

                if (option.ShiftStart != $("#ShiftStart_" + i).val())
                    counter++;
                if (option.ShiftEnd != $("#ShiftEnd_" + i).val())
                    counter++;
                if (option.ShiftGraceIn != $("#ShiftGraceIn_" + i).val())
                    counter++;
                if (option.ShiftGraceOut != $("#ShiftGraceOut_" + i).val())
                    counter++;
                if (option.ShiftBreakHours != $("#ShiftBreakHours_" + i).val())
                    counter++;
                if (option.IsWeekend != $("#IsWeekend_" + i).prop('checked'))
                    counter++;
                if (option.CutOffTime != $("#CutOffTime_" + i).val())
                    counter++;
                i++;
            });

            if (counter > 0) {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save previous changes?" }).done(function () {
                    pageLoaderFrame();
                    $("#frmAddShift").ajaxSubmit(function (data) {
                        ShowMessage("success", data.Message);
                        $("#hdnOldModel").val("");
                        //window.location.reload();
                        $.ajax({
                            type: 'POST',
                            data: { ShiftID: selectedShift },
                            url: '/Shift/GetShiftDetails',
                            success: function (data) {
                                hideLoaderFrame();
                                BindShiftDetails(data);
                                //keep in hidden value
                                $("#hdnOldModel").val(JSON.stringify(data));
                            },
                            error: function (data) { }
                        });
                    });
                }).fail(function () {
                    GetAndBindShiftDetails(selectedShift);
                });
            }
            else {
                GetAndBindShiftDetails(selectedShift);
            }
        }
        else {
            GetAndBindShiftDetails(selectedShift);
        }

        SelectShift(selectedShift);


        if (SelectedOption == "ShiftDetailsByDept") {
            $("#shiftDepartmentDetails").removeClass('hidden');
            $("#shiftEmployeedetails").addClass('hidden');
            $("#shiftSectiondetails").addClass('hidden');
            if (selectedShift != "") {
                StartPanelLoader("#DepartmentNotInShiftList");
                StartPanelLoader("#ShiftDepartmentList");
                $("#DepartmentNotInShiftList").load("/Shift/GetAllDepartmentNotInShift?Id=" + selectedShift);
                $("#ShiftDepartmentList").load("/Shift/GetAllDepartmentInShift?Id=" + selectedShift);
            }
            else {
                StartPanelLoader("#DepartmentNotInShiftList");
                $("#DepartmentNotInShiftList").load("/Shift/GetAllDepartmentNotInShift?Id=-1");
            }
        }
        else if (SelectedOption == "ShiftDetailsBySection") {
            $("#shiftSectiondetails").removeClass('hidden');
            $("#shiftEmployeedetails").addClass('hidden');
            $("#shiftDepartmentDetails").addClass('hidden');
            if (selectedShift != "") {
                StartPanelLoader("#SectionNotInShiftList");
                StartPanelLoader("#ShiftSectionList");
                $("#ShiftSectionList").load("/Shift/GetAllSectionInShift?Id=" + selectedShift);
                $("#SectionNotInShiftList").load("/Shift/GetAllSectionNotInShift?Id=" + selectedShift);
            }
            else {
                StartPanelLoader("#SectionNotInShiftList");
                $("#SectionNotInShiftList").load("/Shift/GetAllSectionNotInShift?Id=-1");
            }
        }
        else if (SelectedOption == "ShiftDetailsByEmployee") {
            $("#shiftSectiondetails").addClass('hidden');
            $("#shiftEmployeedetails").removeClass('hidden');
            $("#shiftDepartmentDetails").addClass('hidden');
            if (selectedShift != "") {
                StartPanelLoader("#ShiftNonEmployeesList");
                StartPanelLoader("#ShiftEmployeesList");
                $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift?Id=" + selectedShift);
                $("#ShiftNonEmployeesList").load("/Shift/GetAllEmployeesNotInShift?Id=" + selectedShift);
            }
            else {
                StartPanelLoader("#ShiftNonEmployeesList");
                $("#ShiftNonEmployeesList").load("/Shift/GetAllEmployeesNotInShift?Id=-1");
            }
        }
    }


}

// Employee
var EmployeeIdsToTransfer = [];
var EmployeeIdsToRemoveFromShift = [];

function AddEmployeeIdToShift(EmployeeID) {
    if ($("#chkSelectEmployeeToAdd_" + EmployeeID).is(":checked")) {
        EmployeeIdsToTransfer.push(
        EmployeeID
    );
    } else {
        var index = EmployeeIdsToTransfer.indexOf(EmployeeID);
        if (index > -1) {
            EmployeeIdsToTransfer.splice(index, 1);
        }
    }
    var table = $('#tblEmployeesNotInShift').DataTable();

    if (table.rows().data().length == EmployeeIdsToTransfer.length) {
        $("#tblEmployeesNotInShift-select-all").prop('checked', true);
    }
    else {
        $("#tblEmployeesNotInShift-select-all").prop('checked', false);
    }
}
function RemoveEmployeeFromShift(EmployeeID) {
    if ($("#chkSelectEmployeeToRemove_" + EmployeeID).is(":checked")) {
        EmployeeIdsToRemoveFromShift.push(
        EmployeeID
    );
    }
    else {
        var index = EmployeeIdsToRemoveFromShift.indexOf(EmployeeID);
        if (index > -1) {
            EmployeeIdsToRemoveFromShift.splice(index, 1);
        }
    }
    var table = $('#tblEmployeesInShift').DataTable();

    if (table.rows().data().length == EmployeeIdsToRemoveFromShift.length) {
        $("#tblEmployeesInShift-select-all").prop('checked', true);
    }
    else {
        $("#tblEmployeesInShift-select-all").prop('checked', false);
    }
}
var oTableChannel;
function SelectAllToRemove(e) {
    oTableChannel = $('#tblEmployeesInShift').dataTable();
    if ($(e).is(':checked')) {
        $('.chkSelectEmployeeToRemove', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".chkSelectEmployeeToRemove").closest('tr').addClass("selected");

        EmployeeIdsToRemoveFromShift = [];
        var table = $('#tblEmployeesInShift').DataTable();
        rowData = table.rows().data();
        $(rowData).each(function (i, item) {
            var curRow;
            curRow = item[0];
            EmployeeIdsToRemoveFromShift.push(
               parseInt($(curRow).attr("data-empid"))
            );
        });
    }
    else {
        $('.chkSelectEmployeeToRemove', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".chkSelectEmployeeToRemove").closest('tr').removeClass("selected");
        EmployeeIdsToRemoveFromShift = [];
    }
}

function SelectAllToAdd(e) {
    oTableChannel = $('#tblEmployeesNotInShift').dataTable();
    if ($(e).is(':checked')) {
        $('.chkSelectEmployeeToAdd', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".chkSelectEmployeeToAdd").closest('tr').addClass("selected");

        EmployeeIdsToTransfer = [];
        var table = $('#tblEmployeesNotInShift').DataTable();
        rowData = table.rows().data();
        $(rowData).each(function (i, item) {
            var curRow;
            curRow = item[0];
            EmployeeIdsToTransfer.push(
                   parseInt($(curRow).attr("data-empid"))
            );
        });
    }
    else {
        $('.chkSelectEmployeeToAdd', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".chkSelectEmployeeToAdd").closest('tr').removeClass("selected");
        EmployeeIdsToTransfer = [];
    }
}

function AddEmployeeToShift() {

    if ($("#ddl_Shift").val() == "") {
        ShowMessage("error", "Please select shift first.");
    }
    else {
        if (EmployeeIdsToTransfer.length > 0) {
            StartPanelLoader("#ShiftNonEmployeesList");
            StartPanelLoader("#ShiftEmployeesList");

            $('#btnAddEmployeeToShift').css('pointer-events', 'none');
            $('#btnRemoveShiftEmployee').css('pointer-events', 'none');

            checkFuturRecord(0, JSON.stringify(EmployeeIdsToTransfer), function (a) {
                if (a.IsRecordExist == true) {
                    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "This shift change will overwrite the user shifts modification. Are you sure you want to save changes.?" }).done(function () {

                        $.ajax({
                            type: 'POST',
                            data: { EmployeeIds: JSON.stringify(EmployeeIdsToTransfer), shiftID: $("#ddl_Shift").val() },
                            url: '/Shift/AddEmployeeToShift',
                            async: true,
                            success: function (data) {
                                StopPanelLoader("#ShiftNonEmployeesList");
                                StopPanelLoader("#ShiftEmployeesList");
                                $('#btnAddEmployeeToShift').css('pointer-events', 'visible');
                                $('#btnRemoveShiftEmployee').css('pointer-events', 'visible');
                                if (data.Success) {
                                    ShowMessage(data.CssClass, data.Message);
                                    getAssignShiftDetails();
                                    EmployeeIdsToTransfer = [];
                                }
                                else {
                                    ShowMessage("error", "error while updating shift.");
                                }
                            },
                            error: function (data) {
                                StopPanelLoader("#ShiftNonEmployeesList");
                                StopPanelLoader("#ShiftEmployeesList");
                                $('#btnAddEmployeeToShift').css('pointer-events', 'visible');
                                $('#btnRemoveShiftEmployee').css('pointer-events', 'visible');
                            }
                        });
                    }).fail(function () {
                        StopPanelLoader("#ShiftNonEmployeesList");
                        StopPanelLoader("#ShiftEmployeesList");
                        $('#btnAddEmployeeToShift').css('pointer-events', 'visible');
                        $('#btnRemoveShiftEmployee').css('pointer-events', 'visible');
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        data: { EmployeeIds: JSON.stringify(EmployeeIdsToTransfer), shiftID: $("#ddl_Shift").val() },
                        url: '/Shift/AddEmployeeToShift',
                        async: true,
                        success: function (data) {
                            StopPanelLoader("#ShiftNonEmployeesList");
                            StopPanelLoader("#ShiftEmployeesList");
                            $('#btnAddEmployeeToShift').css('pointer-events', 'visible');
                            $('#btnRemoveShiftEmployee').css('pointer-events', 'visible');
                            if (data.Success) {
                                ShowMessage(data.CssClass, data.Message);
                                getAssignShiftDetails();
                            }
                            else {
                                ShowMessage("error", "error while updating shift.");
                            }
                        },
                        error: function (data) {
                            StopPanelLoader("#ShiftNonEmployeesList");
                            StopPanelLoader("#ShiftEmployeesList");
                            $('#btnAddEmployeeToShift').css('pointer-events', 'visible');
                            $('#btnRemoveShiftEmployee').css('pointer-events', 'visible');
                        }
                    });
                }
            });
        }
        else {
            ShowMessage("error", "Please select employees to add to selected shift.");
        }
    }

}

function RemoveShiftEmployee() {
    if ($("#ddl_Shift").val() == "") {
        ShowMessage("error", "Please select shift.");
    }
    else {
        if (EmployeeIdsToRemoveFromShift.length > 0) {
            StartPanelLoader("#ShiftNonEmployeesList");
            StartPanelLoader("#ShiftEmployeesList");
            $('#btnAddEmployeeToShift').css('pointer-events', 'none');
            $('#btnRemoveShiftEmployee').css('pointer-events', 'none');
            $.ajax({
                type: 'POST',
                data: { EmployeeIds: JSON.stringify(EmployeeIdsToRemoveFromShift), shiftId: $("#ddl_Shift").val() },
                url: '/Shift/RemoveEmployeeShift',
                success: function (data) {
                    StopPanelLoader("#ShiftNonEmployeesList");
                    StopPanelLoader("#ShiftEmployeesList");
                    $('#btnAddEmployeeToShift').css('pointer-events', 'visible');
                    $('#btnRemoveShiftEmployee').css('pointer-events', 'visible');
                    if (data.Success) {
                        ShowMessage(data.CssClass, data.Message);
                        getAssignShiftDetails();
                        EmployeeIdsToRemoveFromShift = [];
                    }
                    else {
                        ShowMessage("error", "error while updating shift.");
                    }
                },
                error: function (data) {
                    StopPanelLoader("#ShiftNonEmployeesList");
                    StopPanelLoader("#ShiftEmployeesList");
                }
            });
        }
        else {
            ShowMessage("error", "Please select employees to remove from selected shift.");
        }

    }

}

//////Section
var SectionIdsToTransfer = [];
var SectionIdsToREmoveFromShift = [];
var DepartmentIdToTransfer = [];
var DepartmentIdsToRemoveFromShift = [];

function CreateSectionIdJson(EmployeeSectionID) {
    if ($("#chkSelectSectionToAdd_" + EmployeeSectionID).is(":checked")) {
        SectionIdsToTransfer.push(
        EmployeeSectionID
    );
    } else {
        var index = SectionIdsToTransfer.indexOf(EmployeeSectionID);
        if (index > -1) {
            SectionIdsToTransfer.splice(index, 1);
        }
    }
    var table = $("#tblSectionNotInShift").DataTable();
    if (table.rows().data().length == SectionIdsToTransfer.length) {
        $("#tblSectionNotInShift_Select_All").prop('checked', true);
    }
    else {
        $("#tblSectionNotInShift_Select_All").prop('checked', false);
    }
}

function RemoveSectionsFromAssignedShift(EmployeeSectionID) {
    if ($("#chkSelectSectionToRemove_" + EmployeeSectionID).is(":checked")) {
        SectionIdsToREmoveFromShift.push(
        EmployeeSectionID
    );
    }
    else {
        var index = SectionIdsToREmoveFromShift.indexOf(EmployeeSectionID);
        if (index > -1) {
            SectionIdsToREmoveFromShift.splice(index, 1);
        }
    }
    var table = $("#tblSectionsInShift").DataTable();
    if (table.rows().data().length == SectionIdsToREmoveFromShift.length) {
        $("#tblSectionInShift_Select_All").prop('checked', true);
    }
    else {
        $("#tblSectionInShift_Select_All").prop('checked', false);
    }
}

function SelectAllSectionToRemove(e) {
    oTableChannel = $('#tblSectionsInShift').dataTable();
    if ($(e).is(':checked')) {
        $('.chkSelectSectionToRemove', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".chkSelectSectionToRemove").closest('tr').addClass("selected");

        SectionIdsToREmoveFromShift = [];
        var table = $('#tblSectionsInShift').DataTable();
        rowData = table.rows().data();
        $(rowData).each(function (i, item) {
            var curRow;
            curRow = item[0];
            SectionIdsToREmoveFromShift.push(
               parseInt($(curRow).attr("data-SectionID"))
            );
        });
    }
    else {
        $('.chkSelectSectionToRemove', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".chkSelectSectionToRemove").closest('tr').removeClass("selected");
        SectionIdsToREmoveFromShift = [];
    }
}

function SelectAllSectionToAdd(e) {
    oTableChannel = $('#tblSectionNotInShift').dataTable();
    if ($(e).is(':checked')) {
        $('.chkSelectSectionToAdd', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".chkSelectSectionToAdd").closest('tr').addClass("selected");

        SectionIdsToTransfer = [];
        var table = $('#tblSectionNotInShift').DataTable();
        rowData = table.rows().data();
        $(rowData).each(function (i, item) {
            var curRow;
            curRow = item[0];
            SectionIdsToTransfer.push(
                   parseInt($(curRow).attr("data-SectionID"))
            );
        });
    }
    else {
        $('.chkSelectSectionToAdd', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".chkSelectSectionToAdd").closest('tr').removeClass("selected");
        SectionIdsToTransfer = [];
    }
}

function TransferSectionsToShift() {
    if ($("#ddl_Shift").val() == "") {
        ShowMessage("error", "Please select shift.");
    }
    else {
        if (SectionIdsToTransfer.length > 0) {
            if (SectionIdsToTransfer.length > 0) {
                $.MessageBox({ buttonDone: "Proceed", buttonFail: "Cancel", message: "<a href='#' onclick='ShowCustomShifts();' class='pull-right'>All shifts in selected section</a><br><br>You are about to change section shift, It will update all employees shift that are under this section." }).done(function () {
                    StartPanelLoader("#SectionNotInShiftList");
                    StartPanelLoader("#ShiftSectionList");
                    $('#btnAssignSectionToShift').css('pointer-events', 'none');
                    $('#btnRemoveSectionFromShift').css('pointer-events', 'none');
                    $.ajax({
                        type: 'post',
                        data: { SectionIds: JSON.stringify(SectionIdsToTransfer), shiftID: $("#ddl_Shift").val() },
                        url: '/shift/AddSectionsToShift',
                        async: true,
                        success: function (data) {
                            $('#btnAssignSectionToShift').css('pointer-events', 'visible');
                            $('#btnRemoveSectionFromShift').css('pointer-events', 'visible');
                            StopPanelLoader("#SectionNotInShiftList");
                            StopPanelLoader("#ShiftSectionList");
                            if (data.Success) {
                                ShowMessage(data.CssClass, data.Message);
                                getAssignShiftDetails();
                                SectionIdsToTransfer = [];
                            }
                            else {
                                ShowMessage("error", "error while updating shift.");
                            }
                        },
                        error: function (data) {
                            $('#btnAssignSectionToShift').css('pointer-events', 'visible');
                            $('#btnRemoveSectionFromShift').css('pointer-events', 'visible');
                            StopPanelLoader("#SectionNotInShiftList");
                            StopPanelLoader("#ShiftSectionList");
                        }
                    });
                }).fail(function () {
                    $('#btnAssignSectionToShift').css('pointer-events', 'visible');
                    $('#btnRemoveSectionFromShift').css('pointer-events', 'visible');
                    StopPanelLoader("#SectionNotInShiftList");
                    StopPanelLoader("#ShiftSectionList");
                });
            }
            else {
                ShowMessage("error", "Please select sections to transfer.");
            }
        }
        else {
            ShowMessage("error", "Please select sections to add in selected shift.");
        }

    }
}

function RollbackSectionsFromShif() {
    if ($("#ddl_Shift").val() == "") {
        ShowMessage("error", "Please select shift.");
    }
    else {
        if (SectionIdsToREmoveFromShift.length > 0) {
            StartPanelLoader("#SectionNotInShiftList");
            StartPanelLoader("#ShiftSectionList");
            $('#btnAssignSectionToShift').css('pointer-events', 'none');
            $('#btnRemoveSectionFromShift').css('pointer-events', 'none');
            $.ajax({
                type: 'POST',
                data: { SectionIDs: JSON.stringify(SectionIdsToREmoveFromShift), shiftId: $("#ddl_Shift").val() },
                url: '/Shift/RemoveSectionsFromShift',
                success: function (data) {
                    $('#btnAssignSectionToShift').css('pointer-events', 'visible');
                    $('#btnRemoveSectionFromShift').css('pointer-events', 'visible');
                    StopPanelLoader("#SectionNotInShiftList");
                    StopPanelLoader("#ShiftSectionList");
                    if (data.Success) {
                        ShowMessage(data.CssClass, data.Message);
                        getAssignShiftDetails();
                        SectionIdsToREmoveFromShift = [];
                    }
                    else {
                        ShowMessage("error", "error while updating shift.");
                    }
                },
                error: function (data) {
                    $('#btnAssignSectionToShift').css('pointer-events', 'visible');
                    $('#btnRemoveSectionFromShift').css('pointer-events', 'visible');
                    StopPanelLoader("#SectionNotInShiftList");
                    StopPanelLoader("#ShiftSectionList");
                }
            });
        }
        else {
            ShowMessage("error", "Please select sections to remove from shift.");
        }
    }


}

function ShowCustomShifts() {
    $("#messagebox_overlay").css("z-index", "-1");
    $("#modal_Loader").load("/Shift/GetEmployeeAsPerSections?SectionIds=" + JSON.stringify(SectionIdsToTransfer), function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Employees In Section');
    });

}

function SelectAllDepartmentToAdd(e) {
    oTableChannel = $('#tblDepartmentNotInShift').dataTable();
    if ($(e).is(':checked')) {
        $('.chkSelectDepartmentToAdd', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".chkSelectDepartmentToAdd").closest('tr').addClass("selected");

        DepartmentIdToTransfer = [];
        var table = $('#tblDepartmentNotInShift').DataTable();
        rowData = table.rows().data();
        $(rowData).each(function (i, item) {
            var curRow;
            curRow = item[0];
            DepartmentIdToTransfer.push(
                   parseInt($(curRow).attr("data-deptID"))
            );
        });
    }
    else {
        $('.chkSelectDepartmentToAdd', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".chkSelectDepartmentToAdd").closest('tr').removeClass("selected");
        DepartmentIdToTransfer = [];
    }
}

function SelectAllDepartmentToRemove(e) {
    oTableChannel = $('#tblDepartmentInShift').dataTable();
    if ($(e).is(':checked')) {
        $('.chkSelectDepartmentToRemove', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".chkSelectDepartmentToRemove").closest('tr').addClass("selected");

        DepartmentIdsToRemoveFromShift = [];
        var table = $('#tblDepartmentInShift').DataTable();
        rowData = table.rows().data();
        $(rowData).each(function (i, item) {
            var curRow;
            curRow = item[0];
            DepartmentIdsToRemoveFromShift.push(
               parseInt($(curRow).attr("data-deptID"))
            );
        });
    }
    else {
        $('.chkSelectDepartmentToRemove', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".chkSelectDepartmentToRemove").closest('tr').removeClass("selected");
        DepartmentIdsToRemoveFromShift = [];
    }
}

function CreateDeptIdJson(DepartmentID) {
    if ($("#chkSelectDepartmentToAdd_" + DepartmentID).is(":checked")) {
        DepartmentIdToTransfer.push(
        DepartmentID
    );
    } else {
        var index = DepartmentIdToTransfer.indexOf(DepartmentID);
        if (index > -1) {
            DepartmentIdToTransfer.splice(index, 1);
        }
    }

    var table = $("#tblDepartmentNotInShift").DataTable();
    if (table.rows().data().length == DepartmentIdToTransfer.length) {
        $("#tblDepartmentNotInShift_select_all").prop('checked', true);
    }
    else {
        $("#tblDepartmentNotInShift_select_all").prop('checked', false);
    }
}

function RemoveDepartmentsFromAssignedShift(DepartmentID) {
    if ($("#chkSelectDepartmentToRemove_" + DepartmentID).is(":checked")) {
        DepartmentIdsToRemoveFromShift.push(
        DepartmentID
    );
    }
    else {
        var index = DepartmentIdsToRemoveFromShift.indexOf(DepartmentID);
        if (index > -1) {
            DepartmentIdsToRemoveFromShift.splice(index, 1);
        }
    }

    var table = $("#tblDepartmentInShift").DataTable();
    if (table.rows().data().length == DepartmentIdsToRemoveFromShift.length) {
        $("#tblDepartmentInShift_select_all").prop('checked', true);
    }
    else {
        $("#tblDepartmentInShift_select_all").prop('checked', false);
    }
}

function TransferDepartmentsToShift() {
    if ($("#ddl_Shift").val() == "") {
        ShowMessage("error", "Please select shift.");
    }
    else {
        if (DepartmentIdToTransfer.length > 0) {
            $.MessageBox({ buttonDone: "Proceed", buttonFail: "Cancel", message: "<a href='#' onclick='ShowCustomDeptShifts();' class='pull-right'>All shifts in selected department</a><br><br>You are about to change department shift, It will update all employees shift that are under this department." }).done(function () {
                $('#btnAssignDeptToShift').css('pointer-events', 'none');
                $('#btnUnAssignDeptFromShift').css('pointer-events', 'none');
                StartPanelLoader("#DepartmentNotInShiftList");
                StartPanelLoader("#ShiftDepartmentList");
                $.ajax({
                    type: 'post',
                    data: { DeptIds: JSON.stringify(DepartmentIdToTransfer), shiftID: $("#ddl_Shift").val() },
                    url: '/shift/AddDepartmentsToShift',
                    async: true,
                    success: function (data) {
                        $('#btnAssignDeptToShift').css('pointer-events', 'visible');
                        $('#btnUnAssignDeptFromShift').css('pointer-events', 'visible');
                        StopPanelLoader("#DepartmentNotInShiftList");
                        StopPanelLoader("#ShiftDepartmentList");
                        if (data.Success) {
                            ShowMessage(data.CssClass, data.Message);
                            getAssignShiftDetails();
                            DepartmentIdToTransfer = [];
                        }
                        else {
                            ShowMessage("error", "error while updating shift.");
                        }
                    },
                    error: function (data) {
                        $('#btnAssignDeptToShift').css('pointer-events', 'visible');
                        $('#btnUnAssignDeptFromShift').css('pointer-events', 'visible');
                        StopPanelLoader("#DepartmentNotInShiftList");
                        StopPanelLoader("#ShiftDepartmentList");
                    }
                });
            }).fail(function () {

            });
        }
        else {
            ShowMessage("error", "Please select departments to transfer.");
        }


    }

}

function RollbackDeptsFromShif() {
    if ($("#ddl_Shift").val() == "") {
        ShowMessage("error", "Please select shift.");
    }
    else {
        if (DepartmentIdsToRemoveFromShift.length > 0) {
            $.ajax({
                type: 'POST',
                data: { DeptIds: JSON.stringify(DepartmentIdsToRemoveFromShift), shiftId: $("#ddl_Shift").val() },
                url: '/Shift/RemoveDepartmentFromShift',
                success: function (data) {
                    if (data.Success) {
                        ShowMessage(data.CssClass, data.Message);
                        getAssignShiftDetails();
                        DepartmentIdsToRemoveFromShift = [];
                    }
                    else {
                        ShowMessage("error", "error while updating shift.");
                    }
                },
                error: function (data) { }
            });
        }
        else {
            ShowMessage("error", "Please select department to remove from department.");
        }
    }

}

function ShowCustomDeptShifts() {
    $("#messagebox_overlay").css("z-index", "-1");
    $("#modal_Loader").load("/Shift/GetEmployeeAsPerDepts?DeptIds=" + JSON.stringify(DepartmentIdToTransfer), function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Employees In Department');
    });
}

var SelectedShift = [];
function SetShiftIdToChange(ShiftID, ShiftName) {
    var s;
    if ($("#chkSelectShift_" + ShiftID).is(":checked")) {
        SelectedShift.push({
            ShiftID: ShiftID,
            ShiftName: ShiftName
        });

        $.each($(SelectedShift), function (ind, item) {
            if (ind == 0) {
                s = item.ShiftName;
            }
            else {
                s += ', ' + item.ShiftName
            }
        });
        $("#SelectedShift").text(s);
    } else {
        s = '';
        SelectedShift.forEach(function (result, index) {
            if (result.ShiftID == ShiftID) {
                //selectList += '<option value=' + id + '>' + result.EmployeeName + '</option>';
                SelectedShift.splice(index, 1);
            }
        });
        $.each($(SelectedShift), function (ind, item) {
            if (ind == 0) {
                s = item.ShiftName;
            }
            else {
                s += ', ' + item.ShiftName
            }
        });
        $("#SelectedShift").text(s);

    }
    $("#hdnSelectedShiftsToUpdate").val(JSON.stringify(SelectedShift));
}