﻿$(document).ready(function () {
   
    getGridHoliday();
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    $("#btnTextClear").click(function () {
        $('#txtSearch').val(""); // $('#txtSearch').val() == "";
        getGridHoliday();
    });
    $("#btnTextSearch").click(function () {

        getGridHoliday();
    });



});
$("#btn_addHoliday").click(function () { AddHoliday(); });
var oTableChannel;

function getGridHoliday() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_HolidayList').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
       // "dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/Holiday/GetHolidayList" ,
        "aoColumns": [
            
            { "mData": "HolidayID", "bVisible": false, "sTitle": "HolidayID" },
            { "mData": "HolidayName", "sTitle": "Holiday Name" ,"width":"40%"},
            { "mData": "From", "sTitle": "From", "sType": "date-uk", "width": "18%" },
            { "mData": "to", "sTitle": "To", "sType": "date-uk", "width": "18%" },
            { "mData": "Action", "sTitle": "Actions" ,"bSortable":false,"width":"14%"}

        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/Holiday/GetHolidayList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {            
            $('#tbl_HolidayList').css({ 'width': '100%' });
        },
        "bSortCellsTop": true
    });
}

function AddHoliday() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Holiday/AddHoliday");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Public Holiday');
}

function EditHoliday(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Holiday/EditHoliday/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Edit Public Holiday');
   
}

function DeleteHoliday(id) {   
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Holiday/DeleteHoliday',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                getGridHoliday();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewHoliday(id) {
    $("#modal_Loader").load("/Holiday/ViewHoliday/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Holiday Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}