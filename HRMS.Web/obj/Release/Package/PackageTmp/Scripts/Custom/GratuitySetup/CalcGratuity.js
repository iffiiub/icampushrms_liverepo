﻿$(document).ready(function () {

 

    $("#btn_add").click(function () {
        EditChannel(0);
    });

    getFinalSettlementDetails();

    $.each($("#jsTreeMainDiv a"), function (index, item) {
        $(item).attr("target", "_blank");
    });


});




function getmultiselect() {
    var selectedVal = $('.salaryAllowances').multiselect('getSelected');
}


//function ShowGratuityList() {
//    var EmployeeIds = getSelectedIds('.ColNationality');
//    if (EmployeeIds.length > 0) {
//        $.ajax({
//            url: '/GratuitySetup/SaveEmployeeId',
//            type: 'GET',
//            data: { employeeid: EmployeeIds.join(',') }
//            ,
//            success: function (data) {
//                var url = '/Reporting/EmployeeGratuityListReportViewer'
//                var win = window.open(url, '_blank');
//                win.focus();
//            },
//            error: function () {
//            }
//        })
//    } else { ShowMessage("error", "Please select employee.") }

//}

function EditChannel(id) {
    var buttonName = id > 0 ? "Update" : "Save";

    $("#modal_Loader").load("/GratuitySetup/AddFinalSettlementofEmployee/?GratuityFSID=" + id, function () {
        $("#myModal").modal("show");
        bindSelectpicker('.selectpickerddl');
        if (id == 0) {

            $("#modal_heading").text('Add Final Settlement');
        }
        else {

            $("#modal_heading").text('Edit Final Settlement');

        }
        $(".modal-dialog").attr("style", "width:1000px;");
        $("#form0").find('input[type="submit"]').val(buttonName);

        $(".wizard").smartWizard({
            // This part of code can be removed FROM
            onLeaveStep: function (obj) {
                var wizard = obj.parents(".wizard");

                if (wizard.hasClass("wizard-validation")) {

                    var valid = true;

                    $('input,textarea', $(obj.attr("href"))).each(function (i, v) {
                        valid = validator.element(v) && valid;
                    });

                    if (!valid) {
                        wizard.find(".stepContainer").removeAttr("style");
                        validator.focusInvalid();
                        return false;
                    }

                }

                return true;
            },// <-- TO

            //This is important part of wizard init
            onShowStep: function (obj) {
                var wizard = obj.parents(".wizard");

                if (wizard.hasClass("show-submit")) {

                    var step_num = obj.attr('rel');
                    var step_max = obj.parents(".anchor").find("li").length;

                    if (step_num == step_max) {
                        obj.parents(".wizard").find(".actionBar .btn-primary").css("display", "block");
                    }
                    if (step_num == "2") {
                        var EmployeeIds = getSelectedIds('.multi-Select').join(',');
                        if ((EmployeeIds == "" || EmployeeIds == null) && ($("#EmployeeId").val() == "" || $("#EmployeeId").val() == 0 || $("#EmployeeId").val() == undefined)) {
                            $jq(".NextBtn").addClass("disabled")
                            customShowMessage("error", "Please select employees.", 10000, "center");
                        }
                        else {
                            $jq(".NextBtn").removeClass("disabled");
                        }
                        $.ajax({
                            url: "/GratuitySetup/CheckGratuitySetup",
                            type: "GET",
                            success: function (data) {
                                if (data.result == 'error') {
                                    $jq(".NextBtn").addClass("disabled")
                                    customShowMessage(data.result, data.resultMessage, 10000, "center");
                                }
                            },
                            error: function () {
                                $.MessageBox({
                                    buttonDone: "Ok",
                                    message: "Some error occured.",
                                });
                            }
                        });
                        $.ajax({
                            url: "/GratuitySetup/GetEmployeesNotInSalary",
                            type: "GET",
                            data: {
                                EmpIds: getSelectedIds('.multi-Select').join(',')
                            },
                            success: function (data) {
                                if (data.resultMessage != null) {
                                    customShowMessage(data.result, data.resultMessage, 10000, "center");
                                }


                            },
                            error: function () {
                                $.MessageBox({
                                    buttonDone: "Ok",
                                    message: "Some error occured.",
                                });
                            }
                        });
                    }

                }
                return true;
            }//End
        });
    });
}


function getFinalSettlementDetails() {
    var url = "/GratuitySetup/GetFinalSettlmentDetails/";
    oTableChannel = $('#tbl_GratuityList').dataTable({
        "sAjaxSource": url,
        "aoColumns": [
            { "mData": "EmployeeId", "sTitle": "Emp ID", "sWidth": "5%", "sType": "Int" },
            { "mData": "employeename", "sTitle": "Employee Name", "sWidth": "20%" },
            { "mData": "GratuityBeforeNEAmount", "sTitle": "Gratuity Before <br/> 1980 Amount", "sWidth": "13%","sClass":"hidden" },           
            { "mData": "ESOthersAmount", "sTitle": "ES Others <br/> Amount", "sWidth": "12%" },
            { "mData": "CompensatoryLeaveAmount", "sTitle": "Compensatory <br/> Leave Amount", "sWidth": "12%" },
            { "mData": "GratuityAlreadyPaidAmount", "sTitle": "Gratuity Already <br/> Paid Amount", "sWidth": "12%" },
            { "mData": "ESOthersDeductionAmount", "sTitle": "ES Others <br/> Deduction Amount", "sWidth": "12%" },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "sWidth": "12%", "sClass": "text-center" }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": url,
        "bDestroy": true,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bInfo": true,
        "aaSorting": [[1, 'asc']],
        "fnDrawCallback": function () {
        },
        "bSortCellsTop": true
    });
}

function DeleteGratuityWithSettlement(EmployeeID) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete gratuity and final settlement details?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { EmployeeId: EmployeeID },
            url: '/GratuitySetup/DeleteGratuityAndSettlement',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getFinalSettlementDetails();
            },
            error: function (data) { }
        });
    });
}
function ShowLink(e) {
    //ShowMessage('success', $(e).attr('href')); noLink
    if (!$(e).hasClass("noLink")) {
        $('#templink').attr('href', $(e).find('a').attr('href'));
        document.getElementById("templink").click();
    }
}