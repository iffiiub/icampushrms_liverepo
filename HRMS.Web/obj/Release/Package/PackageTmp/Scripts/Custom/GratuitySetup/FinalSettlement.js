﻿$(document).ready(function () {

    $(document).on('click', 'ul.multiselect-container.dropdown-menu li input[type="checkbox"]', function () {
        var multiselectContainer = $(this).closest('ul.multiselect-container.dropdown-menu');
        var $this = $(this);
        if ($this.val == "Select All") {
            if ($this.is(":checked")) {
                $('ul.multiselect-container.dropdown-menu li').find('input[type="checkbox"]').prop('checked', true);
            }
            else {
                $('ul.multiselect-container.dropdown-menu li').find('input[type="checkbox"]').prop('checked', false);
            }
        }
    });

   
    $(".saveSettlement").click(function () {
        SaveFinalSettlmentDetails();
    })

});


function SaveFinalSettlmentDetails() {
    var EmployeeIds = "";
    EmployeeIds = getSelectedIds('.multi-Select').join(',');
    var LastWorkingDays = 0;
    var TaxInclude = $("#isTaxInclude").is(":checked")
    var EmployeeId = $("#EmployeeId").val();
    var AdvanceOnGratuity = $("#GratuityAlreadyPaidAmount").val();
    var GratuityPriortoNE = 0;
    var toDate = $("#txtToDate").val();
    var EOS = $("#EOSType").val();
    var CompensatoryLeaveAmount = $("#CompensatoryLeaveAmount").val();
    var AirTicketAmount = $("#AirTicketAmount").val();
    var ESOthersAmount = $("#ESOthersAmount").val();
    var ESOthersDeductionAmount = $("#ESOthersDeductionAmount").val();
    var EmployeeGratiutyDetails = {
        LastWorkingDays: LastWorkingDays,
        isTaxInclude: TaxInclude,
        EmployeeId: EmployeeId,
        GratuityAlreadyPaidAmount: AdvanceOnGratuity,
        GratuityBeforeNEAmount: GratuityPriortoNE,
        CompensatoryLeaveAmount: CompensatoryLeaveAmount,
        AirTicketAmount: AirTicketAmount,
        ESOthersAmount: ESOthersAmount,
        ESOthersDeductionAmount: ESOthersDeductionAmount,
        EOSType: EOS,
        ToDate: toDate
    }
    $.ajax({
        url: "/GratuitySetup/SaveFinalSettlement/",
        type: "POST",
        data: {
            EmpGrat: JSON.stringify(EmployeeGratiutyDetails),
            EmployeeIds: EmployeeIds
        },
        success: function (data) {
            ShowMessage("success", "Gratuity calulated successfully.");
            $("#myModal").modal("hide");
            getFinalSettlementDetails();
        },
        error: function () {
            $.MessageBox({
                buttonDone: "Ok",
                message: "Some error occured.",
            });
        }
    });

}


