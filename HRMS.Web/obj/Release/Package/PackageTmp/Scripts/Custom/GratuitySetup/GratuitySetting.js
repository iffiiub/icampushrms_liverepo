﻿

$(document).ready(function () {   
    
    loadGratuitySettings();

});



function loadGratuitySettings() {

    oTableChannel = $('#tbl_GratuitySetup').dataTable({
        "sAjaxSource": "/GratuitySetup/GetGratuitySetupList",
        "aoColumns": [
        { "mData": "AllowanceName", "sTitle": "Allowance Name",'width':'50%' },
        { "mData": "Percentage", "sTitle": "Percentage", 'width': '18%' },
        { "mData": "IsStandered", "sTitle": "Total Amount", "sClass": "text-center", 'width': '18%' },
        { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", "bSortable": false, 'width': '14%' }
        ],
        //"fnServerParams": function (aoData) {
        //    aoData.push({ "name": "empid", "value": empid });
        //},
        "processing": true,
        "serverSide": false,
        "ajax": "/GratuitySetup/GetGratuitySetupList",
        "aLengthMenu": [[10,25, 50, 100, 1000], [10,25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"fnDrawCallback": function () {
        //    removeSpan("#tbl_InsuranceDependentList");
        //},
        "bSortCellsTop": true

    });
}




function AddGratuitySetting() {

    var selectedAllowances = [];

    $('.ColNationality ul li input[type="checkbox"]').each(function (ind,item) {
        var chk = $(item);
            var value = chk.val();
            var txt = chk.parent('label').text().trim();
            if (chk.is(":checked")) {
                selectedAllowances.push({
                    "id": value,
                    "PaySalaryAllowanceName": txt,
                                    
                });
            }
    });

    $.ajax({
        url: "/GratuitySetup/AddGratuitySetting",
        type: "POST",
        data: {
            "selectedAllowances": JSON.stringify(selectedAllowances),
            "isStandard": $("#chkisStandard").is(':checked')
          
        },
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
            loadGratuitySettings();
        },
        error: function () {
            $.MessageBox({
                buttonDone: "Ok",
                message: "Some error occured.",
            });
        }
    });

     $('.ColNationality ul li input[type="checkbox"]').each(function (ind, item) {
        var chk = $(item);
        var value = chk.val();
        var txt = chk.parent('label').text().trim();
        $(chk).attr('checked', false);
    });

    $('.multiselect-selected-text').text('None Selected');
}

function DeleteAllowance(id)
{
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {

        $.ajax({
            url: "/GratuitySetup/DeleteAllowanceFromSetting",
            type: "POST",
            data: {
                "AllowanceId": id,


            },
            success: function (data) {

                ShowMessage(data.result, data.resultMessage);
                loadGratuitySettings();
            },
            error: function () {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: "Some error occured.",
                });
            }
        });

    });
   
}

function AddPaySalaryAllowances() {
    $("#addPaySalaryModal_Loader").html("");
    $("#addPaySalaryModal_Loader").load("/PaySalaryAllowance/Edit/0");
    $("#addPaySalaryModal").modal("show");
    $("#addPaySalaryModal_heading").text('Add  Salary Allowance');
}




