﻿var oTableChannel;
$(document).ready(function () {

    $("#divTemplate2Fields").hide();

    $('#chkAllBank').click(function () {
        if ($(this).prop('checked')) {
            $("#tblBankList input[type=checkbox]").prop('checked', true);
        } else {
            $("#tblBankList input[type=checkbox]").prop('checked', false);
        }
    });

    $("#TemplateID").change(function () {
        var templateId = $(this).val();
        if ($(this).val() == 4)//Non WPS
            $("#divTemplate2Fields").show();
        else
            $("#divTemplate2Fields").hide();

           $.ajax({
            type: 'GET',
            data: { templateId: templateId},
            url: '/WPS/isPDFEnabledForWPSReport',
            success: function (data) {              
                if (data.PDFEnabled == true)
                {
                    $("#btnPDF").removeClass('hide');
                }
                else
                {
                    $("#btnPDF").addClass('hide');
                }
                if (data.SifEnabled == true) {
                    $("#btnSIF").removeClass('hide');
                }
                else {
                    $("#btnSIF").addClass('hide');
                }
                
            },
            error: function (data) {
               
            }
        });

    });

    $("#btnPreview").click(function () {

        //Reset table
        if ($("#tbl_TemplateList > tbody > tr").length > 0) {
            try {
                $("#tbl_TemplateList").dataTable().fnDestroy();
            } catch (e) {

            }
        }
        $('#tbl_TemplateList').find('thead tr th').remove();
        $('#tbl_TemplateList').find('tbody').remove();

        var bankIds = "";
        $("#tblBankList input[type=checkbox]:checked").each(function () {
            bankIds += $(this).val() + ",";
        });

        if (bankIds == "") {
            $.MessageBox({
                buttonDone: "Ok",
                message: "Please select at least one bank.",
            })
        }
        else {
            var cycleId = $("#CycleID").val();
            var templateId = $("#TemplateID").val();
            var categoryId = $("#CategoryID").val();
            var companyId = $("#ddlCompany").val() == "" ? null : $("#ddlCompany").val();
            bankIds = bankIds.substring(0, bankIds.length - 1);
            var GroupById = $("#ddlGroupBy").val();
            var establishNumber = "", bankCodeEmployer = "";
            var IsPreview = true;
            if (templateId == 4) {//Non WPS
                establishNumber = $("#EstablishNumber").val();
                bankCodeEmployer = $("#BankCodeEmployer").val();
            }
            if (cycleId == "") {
                ShowMessage("error", "Please select cycle");
            } else {
                if (templateId == "") {
                    ShowMessage("error", "Please select template");
                } else {
                    GetTemplate(bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer,categoryId);
                }
            }
        }
    });

    $("#btnExcel").click(function () {
        var bankIds = "";
        $("#tblBankList input[type=checkbox]:checked").each(function () {
            bankIds += $(this).val() + ",";
        });
        if (bankIds == "") {
            $.MessageBox({
                buttonDone: "Ok",
                message: "Please select atleast one bank",
            })
        }
        else {
            var cycleId = $("#CycleID").val();
            var templateId = $("#TemplateID").val();
            var establishNumber = "";
            var bankCodeEmployer = "";
            var categoryId = $("#CategoryID").val();
            var companyId = ($("#ddlCompany").val() == undefined || $("#ddlCompany").val() == "") ? "" : $("#ddlCompany").val();
            var GroupById = $("#ddlGroupBy").val();
            if (parseInt(cycleId) > 0) {
                if (parseInt(templateId) > 0) {
                    if (templateId == 2) {
                        establishNumber = $("#EstablishNumber").val();
                        bankCodeEmployer = $("#BankCodeEmployer").val();
                    }
                    window.location.assign("/WPS/ExportToExcel?bankIds=" + bankIds.substring(0, bankIds.length - 1) + "&companyId=" + companyId + "&GroupById=" + GroupById + "&cycleId=" + cycleId + "&templateId=" + templateId
                        + "&establishNumber=" + establishNumber + "&bankCodeEmployer=" + bankCodeEmployer + "&categoryId=" + categoryId);
                } else {
                    ShowMessage("error", "Please select template");
                }
            } else {
                ShowMessage("error", "Please select cycle");
            }
        }
    });

    $("#btnPDF").click(function () {
        var bankIds = "";
        $("#tblBankList input[type=checkbox]:checked").each(function () {
            bankIds += $(this).val() + ",";
        });
        if (bankIds == "") {
            $.MessageBox({
                buttonDone: "Ok",
                message: "Please select atleast one bank",
            })
        }
        else {
            var cycleId = $("#CycleID").val();
            var templateId = $("#TemplateID").val();
            var establishNumber = "";
            var bankCodeEmployer = "";
            var categoryId = $("#CategoryID").val();
            var companyId = ($("#ddlCompany").val() == undefined || $("#ddlCompany").val() == "") ? "" : $("#ddlCompany").val();
            var GroupById = $("#ddlGroupBy").val();
            if (parseInt(cycleId) > 0) {
                if (parseInt(templateId) > 0) {
                    if (templateId == 2) {
                        establishNumber = $("#EstablishNumber").val();
                        bankCodeEmployer = $("#BankCodeEmployer").val();
                    }
                    window.location.assign("/WPS/ExportToPDF?bankIds=" + bankIds.substring(0, bankIds.length - 1) + "&companyId=" + companyId + "&GroupById=" + GroupById+  "&cycleId=" + cycleId + "&templateId=" + templateId
                        + "&establishNumber=" + establishNumber + "&bankCodeEmployer=" + bankCodeEmployer + "&categoryId=" + categoryId);
                } else {
                    ShowMessage("error", "Please select template");
                }
            } else {
                ShowMessage("error", "Please select cycle");
            }
        }
    });

    $("#btnSIF").click(function () {
        var bankIds = "";
        $("#tblBankList input[type=checkbox]:checked").each(function () {
            bankIds += $(this).val() + ",";
        });
        if (bankIds == "") {
            $.MessageBox({
                buttonDone: "Ok",
                message: "Please select atleast one bank",
            })
        }
        else {
            var cycleId = $("#CycleID").val();
            var templateId = $("#TemplateID").val();
            var establishNumber = "";
            var bankCodeEmployer = "";
            var categoryId = $("#CategoryID").val();
            var companyId = ($("#ddlCompany").val() == undefined || $("#ddlCompany").val() == "") ? "" : $("#ddlCompany").val();
            if (parseInt(cycleId) > 0) {
                if (parseInt(templateId) > 0) {
                    if (templateId == 2) {
                        establishNumber = $("#EstablishNumber").val();
                        bankCodeEmployer = $("#BankCodeEmployer").val();
                    }
                    window.location.assign("/WPS/ExportToSIF?bankIds=" + bankIds.substring(0, bankIds.length - 1) + "&companyId=" + companyId + "&cycleId=" + cycleId + "&templateId=" + templateId
                        + "&establishNumber=" + establishNumber + "&bankCodeEmployer=" + bankCodeEmployer + "&categoryId=" + categoryId);
                } else {
                    ShowMessage("error", "Please select template");
                }
            } else {
                ShowMessage("error", "Please select cycle");
            }
        }
    });
});


//function GetTemplate(bankIds, cycleId, templateId) {
//    $.ajax({
//        type: 'GET',
//        data: { bankIds: bankIds, cycleId: cycleId, templateId: templateId },
//        url: '/WPS/PreviewWPS',
//        success: function (data) {
//            $("#tbl_TemplateList").html(data);
//        },
//        error: function (data) { }
//    });
//}

function GetTemplate(bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer,categoryId) {
    var cols = [];
    var dataVal = { bankIds: bankIds, companyId: companyId, GroupById: GroupById, IsPreview: IsPreview, cycleId: cycleId, templateId: templateId, establishNumber: establishNumber, bankCodeEmployer: bankCodeEmployer, categoryId: categoryId };
    var dataSet = [];
    if (templateId == 1 || templateId == 4 || templateId == 5) {
        pageLoaderFrame();
        cols = GetColumnsByTemplateId(templateId);
        SetPreview(cols, dataVal);
    }
    else {
        pageLoaderFrame();
        var listData = [];
        $.ajax({
            type: 'GET',
            data: { bankIds: bankIds, companyId: companyId, GroupById, IsPreview: IsPreview, cycleId: cycleId, templateId: templateId, categoryId: categoryId },
            url: '/WPS/GetColumnData',
            success: function (data) {                
                cols = JSON.parse(data);
                SetPreview(cols, dataVal);
            },
            error: function (data) {
                hideLoaderFrame();
            }
        });
    }


    //$('#tbl_TemplateList').dataTable().fnClearTable();
    //$('#tbl_TemplateList').dataTable().fnDestroy();
    //oTableChannel = $('#tbl_TemplateList').dataTable({
    //    "sAjaxSource": "/WPS/PreviewWPS",
    //    "aoColumns": cols,
    //    "fnServerParams": function (aoData) {
    //        aoData.push({ "name": "bankIds", "value": bankIds }, { "name": "cycleId", "value": cycleId }, { "name": "templateId", "value": templateId },
    //                    { "name": "establishNumber", "value": establishNumber }, { "name": "bankCodeEmployer", "value": bankCodeEmployer });
    //    },
    //    "processing": false,
    //    "serverSide": true,
    //    "ajax": "/WPS/PreviewWPS",
    //    "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
    //    "iDisplayLength": 100,
    //    "bDestroy": true,
    //    "bFilter": false,
    //    "bInfo": false,
    //    "bSortCellsTop": false,
    //    "bSort": false,
    //    "fnDrawCallback": function () {
    //        removeSpan("#tbl_TemplateList");
    //    }
    //});
}

function SetPreview(cols, dataVal) {
    var groupColumnIndex = 0;
    var columnlength = cols.length;
    $.ajax({
        type: 'GET',
        data: dataVal,// 
        url: '/WPS/GetWPSData',
        success: function (data) {
            dataSet = data;
            var IsGroupBy = ($('#ddlGroupBy').val() == undefined || $('#ddlGroupBy').val() == "") ? false : true;
            $('#tbl_TemplateList').DataTable({
                data: dataSet,
                columns: cols,
                "aLengthMenu": [[10, 20, 50, 100, 1000], [10, 20, 50, 100, "All"]],
                "iDisplayLength": 20,
                "bDestroy": true,
                "aaSorting": [],
                "fnDrawCallback": function () {
                    removeSpan("#tbl_TemplateList");
                    hideLoaderFrame();
                    if (IsGroupBy) {
                        var api = this.api();
                        var rows = api.rows({ page: 'current' }).nodes();
                        var last = null;
                        api.column(groupColumnIndex, { page: 'current' }).data().each(function (group, i) {
                            if (last != group) {
                                $(rows).eq(i).before(
                                    '<tr class="group"><td class="highlightGoupCell-lightBlue" colspan="' + columnlength + '">' + group + '</td></tr>'
                                );
                                last = group;
                            }
                        });
                    }
                }
            });
        },
        error: function (data) {
            hideLoaderFrame();
        }
    });
}

function GetColumnsByTemplateId(templateId) {
    var columns = [];
    if (templateId == 1)//WPS
    {
        columns = [
            { "mData": "EmpId", "sTitle": "EmpId" },
            { "mData": "Employee_Name", "sTitle": "Employee_Name" },
            { "mData": "Employee_Type", "sTitle": "Employee_Type" },
            { "mData": "GroupByColumn", "sTitle": "GroupByColumn", "visible": false },
            { "mData": "Salary", "sTitle": "Salary" },
            { "mData": "VariablePay", "sTitle": "VariablePay" },
            { "mData": "AccountNo", "sTitle": "AccountNo" },
            { "mData": "AGENT_BANK_RTN_CODE", "sTitle": "AGENT_BANK_RTN_CODE" },
            { "mData": "MOL_PERSONID", "sTitle": "MOL_PERSONID" },
            { "mData": "Sal_Month", "sTitle": "Sal_Month" },
            { "mData": "Sal_Year", "sTitle": "Sal_Year" },
            { "mData": "FromDate", "sTitle": "FromDate" },
            { "mData": "ToDate", "sTitle": "ToDate" },
            { "mData": "Leave", "sTitle": "Leave" }
        ];
    }
    else if (templateId == 4)//Non_WPS
    {
        columns = [
            { "mData": "BankCode", "sTitle": "Bank Code" },
            { "mData": "TranType", "sTitle": "Tran Type" },
            { "mData": "BeneficiaryName", "sTitle": "Beneficiary Name" },
            { "mData": "BeneficiaryACIBAN", "sTitle": "Beneficiary A/c IBAN" },
            { "mData": "AmountInAED", "sTitle": "Amount in AED" },
            { "mData": "PaymentDetails", "sTitle": "Payment Details" }
        ];
    }
    else if (templateId == 5)//Non_WPS_CBD
    {
        columns = [
            { "mData": "EmpId", "sTitle": "EmpId" },
            { "mData": "Employee_Name", "sTitle": "Employee_Name" },
            { "mData": "BankAccount", "sTitle": "Bank Account" },
            { "mData": "Salary", "sTitle": "Amount" }
        ];
    }
    return columns;
}