﻿var deductionData = [];
var EmployeeIds = [];
var i = 0;

$(document).ready(function () {
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".datepicker");
    checkCycleExistForDate($("#DeductionDate").val());


    $('#DeductionDate').datepicker({

    }).on('changeDate', function (selected) {
        try {
            var minDate = new Date(selected.date.valueOf());
            var dd = minDate.getDate();
            var mm = minDate.getMonth() + 1;
            var yyyy = minDate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var deductionDate = dd + '/' + mm + '/' + yyyy;
            checkCycleExistForDate(deductionDate);
        }
        catch (e)
        { }
    });


});

function SaveRow(e) {
    var index = $(e).closest('tr').children('td:eq(0)').text();
    var EmployeeID = $("#ddlEmployee_" + index).val();
    var DeductionType = $("#ddlDeductionTypeList_" + index).val();
    var EmployeeName = $("#ddlEmployee_" + index + " option:selected").text();
    var DeductionTypeName = $("#ddlDeductionTypeList_" + index + " option:selected").text();
    var Amount = $("#txtAmount_" + index + "").text();
    if (EmployeeID != "") {
        if (DeductionType != 0) {
            deductionData.push({
                "EmployeeId": EmployeeID,
                "DeductionRuleId": DeductionType,
                "DeductionAmount": Amount
            });
            EmployeeIds.push({
                "EmployeeId": EmployeeID,
            });
            var rowcount = $('#EmpGeneralDeductions-table tr').length;
            $(e).closest('tr').children('td:eq(0)').text(index);
            $(e).closest('tr').children('td:eq(1)').text(EmployeeID);
            $(e).closest('tr').children('td:eq(2)').text(EmployeeName);
            $(e).closest('tr').children('td:eq(3)').text(DeductionTypeName);
            $(e).closest('tr').children('td:eq(4)').text(Amount);
            if (rowcount == 2) {
                $(e).closest('tr').children('td:eq(5)').html('<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditDeduction(this,' + index + ')" id="btnEdit" data-id=' + i + ' title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + index + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>');
            }
            else {
                $(e).closest('tr').children('td:eq(5)').html('<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditDeduction(this,' + index + ')" id="btnEdit" data-id=' + i + ' title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(this)" title="Delete" ><i class="fa fa-times"></i> </a><a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>');
            }
        } else { ShowMessage("error", "Please select deduction type."); }

    }
    else { ShowMessage("error", "Please select employee."); }
}
function AddDataRow() {
    
    i = i + 1;
    $.ajax({
        type: 'GET',
        data: { EmpIds: JSON.stringify(EmployeeIds), SelectedEmployeeId: 0 },
        url: '/PerformanceRating/GetEmployeesNotInSelection',
        success: function (data) {
            $('#EmpGeneralDeductions-table > tbody').append(
      '<tr id="rowId_' + i + '" class="edit' + i + '">' +
      '<td class="hidden id=row_' + i + '">' + i + '</td>' +
      '<td class="hidden id=EmployeeID_' + i + '"></td>' +
     '<td class="col-md-3"><select data-live-search="true"  class="form-control selectpickerddl ddlEmployee" id="ddlEmployee_' + i + '" OnChange="CalculateAmount(this)" > ' + data + '</select></td>' +
     '<td class="col-md-4"><select data-live-search="true"  class="form-control selectpickerddl ddlDeductionTypeList" id="ddlDeductionTypeList_' + i + '" OnChange="CalculateAmount(this)" > ' + $("#DeductionTypeList").text() + '</select></td>' +
     '<td class="center-text-align"><label id="txtAmount_' + i + '" class="control-label"></label></td>' +
     '<td class="center-text-align col-md-3">' +
     '<div class="center-text-align">' +
     '<button type="button" id="btnRowSave" class="tableButton" onclick="SaveRow(this)">' +
     '<img style="width:16px;" src="/Content/images/tick.ico" />' +
     '</button>' +
     '</div>' +
     '</td>' +
     '</tr>')
            bindSelectpicker('.selectpickerddl');
            bindPositiveOnly('positiveOnly');
        },
        error: function (data) { }
    });
}

function EditDeduction(e, i) {
    
    var selectedIndex = i;
    var selectedEmployeeId = $(e).closest('tr').children('td:eq(1)').html();
    var selectedRule = $(e).closest('tr').children('td:eq(3)').html();
    var Amount = $(e).closest('tr').children('td:eq(4)').html();
    EmployeeIds = RemoveElement(EmployeeIds, 'EmployeeId', selectedEmployeeId);
    deductionData = RemoveElement(deductionData, 'EmployeeId', selectedEmployeeId);
    $.ajax({
        type: 'GET',
        data: { EmpIds: '', SelectedEmployeeId: selectedEmployeeId },
        url: '/PerformanceRating/GetEmployeesNotInSelection',
        success: function (data) {
            $(e).closest('tr').children('td:eq(0)').html(selectedIndex);
            $(e).closest('tr').children('td:eq(1)').html(selectedEmployeeId);
            $(e).closest('tr').children('td:eq(2)').html('<select data-live-search="true"  class="form-control selectpickerddl ddlEmployee" id="ddlEmployee_' + selectedIndex + '" OnChange="CalculateAmount(this)"> ' + data + '</select>');
            $(e).closest('tr').children('td:eq(3)').html('<select data-live-search="true"  class="form-control selectpickerddl ddlDeductionTypeList" id="ddlDeductionTypeList_' + selectedIndex + '" OnChange="CalculateAmount(this)" > ' + $("#DeductionTypeList").text() + '</select>');
            $(e).closest('tr').children('td:eq(4)').html('<label id="txtAmount_' + selectedIndex + '" class="control-label">' + Amount + '</label>');
            $(e).closest('tr').children('td:eq(5)').html('<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="SaveRow(this)"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');

            $("#ddlDeductionTypeList_" + selectedIndex + " option").each(function () {
                if ($(this).text() == selectedRule) {
                    $(this).attr('selected', 'selected');
                }
            });
            bindSelectpicker('.selectpickerddl');
            bindPositiveOnly('positiveOnly');
        },
        error: function (data) { }
    });

}

function DeleteRow(e) {
    var selectedEmployeeId = $(e).closest('tr').children('td:eq(0)').html();
    $(e).closest('tr').remove();
    EmployeeIds = RemoveElement(EmployeeIds, 'EmployeeId', selectedEmployeeId);
    deductionData = RemoveElement(deductionData, 'EmployeeId', selectedEmployeeId);
}

function saveEmployeeDeductions() {
    var DeductionDate = $("#DeductionDate").val();
    var rowcount = deductionData.length;
    if (rowcount > 0) {
        $.ajax({
            type: 'POST',
            data: { GeneralDeductins: JSON.stringify(deductionData), DeductionDate: DeductionDate },
            url: '/GeneralDeductions/SaveGeneralDeductions',
            success: function (data) {
                if (data.Success == true) {
                    ShowMessage(data.CssClass, data.Message);
                    $("#myModal3").modal("hide");
                    GetGrid();
                }
                else {
                    ShowMessage("error", data.Message);
                }
            },
            error: function (data) { }
        });
    }
    else {
        ShowMessage("error", "Please enter employee general deduction details in grid.");
    }
}

function CalculateAmount(e) {
    var index = $(e).closest('tr').children('td:eq(0)').text();
    var EmployeeID = $("#ddlEmployee_" + index + "").val();
    var DeductionType = $("#ddlDeductionTypeList_" + index + "").val();
    var DeductionDate = $("#DeductionDate").val();
    $.ajax({
        type: 'GET',
        data: { EmployeeId: EmployeeID, DeductionRuleId: DeductionType, DeductionDate: DeductionDate },
        url: '/GeneralDeductions/CalculateAmount',
        success: function (data) {
            if (data.Amount.indexOf('-1') >= 0) {
                ShowMessage("error", "Cycle is not generated for selected deduction date.");
            }
            else {
                $(e).closest("tr").find('td:eq(4)').html('<label id="txtAmount_' + index + '" class="control-label">' + data.Amount + '</label>');
            }
        },
        error: function (data) { }
    });
}


function CalculateAmountForEditForm(EmployeeID) {
   
    var DeductionType = $("#DeductionRuleId").val();
    var DeductionDate = $("#DeductionDate").val();
    $.ajax({
        type: 'GET',
        data: { EmployeeId: EmployeeID, DeductionRuleId: DeductionType, DeductionDate: DeductionDate },
        url: '/GeneralDeductions/CalculateAmount',
        success: function (data) {
            if (data.Amount.indexOf('-1') >= 0) {
                ShowMessage("error", "Cycle is not generated for selected deduction date.");
            }
            else {                
                $("#DeductionAmount").val(data.Amount);
            }
        },
        error: function (data) { }
    });
}

