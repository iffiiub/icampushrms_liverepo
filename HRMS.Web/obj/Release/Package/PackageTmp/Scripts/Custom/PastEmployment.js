﻿$(document).ready(function () {    
    getGrid();
    $("#btn_add").click(function () {
        
        //alert($("#Empid").val());
        window.location.href = '/PastEmployment/Edit?Id=0&EmployeeID=' + $("#Empid").val();
    });

    $("#btn_BackProfile").click(function () { window.location.href = "/ViewEmployeeProfile/Index/" + $('#Empid').val(); });

});


var oTableChannel;

function getGrid() {
    oTableChannel = $('#tbl_PastEmployment').dataTable({
        "sAjaxSource": "/PastEmployment/GetPastEmploymentList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": $("#Empid").val() });
        },
        "aoColumns": [
            { "mData": "Actions", "sTitle": "Actions" },
            { "mData": "PastEmploymentID", "bVisible": false, "sTitle": "PastEmploymentID" },
            { "mData": "FirstName", "sTitle": "First Name" },
            { "mData": "LastName", "sTitle": "Last Name" },
            { "mData": "CompanyName", "sTitle": "Company Name" },
            { "mData": "StartDate", "sTitle": "Start Date" },
            { "mData": "EndDate", "sTitle": "End Date" },
            { "mData": "Department", "sTitle": "Department" },
            { "mData": "Position", "sTitle": "Position" },
            { "mData": "Role", "sTitle": "Role" }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/PastEmployment/GetPastEmploymentList",        
        "aLengthMenu": [[10, 25, 50, 100, 1000,10000000], [10, 25, 50, 100,1000,"All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_PastEmployment");
        }
    });
}

function EditChannel(id, employeeid) {
    window.location.href = '/PastEmployment/Edit?Id=' + id + '&EmployeeID=' + employeeid;
}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/PastEmployment/Delete',
            success: function (data) {
                getGrid();
                ShowMessage(data.result, data.resultMessage)
            },
            error: function (data) {

                ShowMessage(data.result, data.resultMessage);
            }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}
