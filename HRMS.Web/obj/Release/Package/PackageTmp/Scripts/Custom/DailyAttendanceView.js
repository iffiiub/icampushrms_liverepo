﻿var newStatusId = '';
var SelectedStatus = -1;
var customData = null;
$(document).ready(function () {
    $("#ddl_PayCycle").change(function () {
        var PayCycleId = $("#ddl_PayCycle").val();
        $.ajax({
            cache: false,
            type: "GET",
            url: "/DailyAttendance/GetCyclePayInfo",
            data: { "PayCycleId": PayCycleId },
            success: function (data) {
                $("#txtFromDate").val(data.DateFrom.substring(0, data.DateFrom.length - 11));
                $("#txtToDate").val(data.DateTo.substring(0, data.DateTo.length - 11));
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });

    });
    $('#spinner').css({ opacity: 1 });

    getGridDailyAttendanceView();
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    $('#spinner').css({ opacity: 0 });
});

function AddTooltipOnAttDate() {
    $('td.AttDate').hover(function () {
        var dt = this.innerHTML;
        var dtArr = dt.split("/")
        var day = new Date(dtArr[2], dtArr[1] - 1, dtArr[0]).getDay();
        var dayNmShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        $(this).attr({
            "data-toggle": "tooltip",
            "data-placement": "top",
            "data-original-title": dayNmShort[day]
        });
    });
}

$("#BtnGetReport").click(function () {
    setTimeout(getGridDailyAttendanceView, 1000);
});

$("#BtnGetFilteredReport").click(function () {
    setTimeout(getGridDailyAttendanceView, 1000);
});

$("#btnDaily").click(function () {
    $("#txtFromDate").val($("#txtFromDate").attr('data-today'));
    $("#txtToDate").val($("#txtFromDate").attr('data-today'));
    setTimeout(getGridDailyAttendanceView, 1000);
});

$("#btnWeekly").click(function () {
    var fromdate = $("#txtToDate").val();
    var date = new Date(fromdate);
    var fromdatenew = new Date(date);
    fromdatenew.setDate(fromdatenew.getDate() - 7);
    var dd = (fromdatenew.getDate() < 10 ? '0' : '') + fromdatenew.getDate();
    var mm = ((fromdatenew.getMonth() + 1) < 10 ? '0' : '') + (fromdatenew.getMonth() + 1);
    var y = fromdatenew.getFullYear();
    var Todate = dd + '-/' + mm + '/' + y;
    $("#txtFromDate").val($("#txtFromDate").attr('data-firstDayOfWeek'));
    $("#txtToDate").val($("#txtFromDate").attr('data-lastDayOfWeek'));
    setTimeout(getGridDailyAttendanceView, 1000);
});

$("#btnMonthly").click(function () {
    var fromdate = $("#txtToDate").val();
    var date = new Date(fromdate);
    var fromdatenew = new Date(date);
    fromdatenew.setMonth(fromdatenew.getMonth() - 1);
    var dd = (fromdatenew.getDate() < 10 ? '0' : '') + fromdatenew.getDate();
    var mm = ((fromdatenew.getMonth() + 1) < 10 ? '0' : '') + (fromdatenew.getMonth() + 1);
    var y = fromdatenew.getFullYear();
    var Todate = dd + '/' + mm + '/' + y;
    $("#txtFromDate").val($("#txtFromDate").attr('data-firstDay'));
    $("#txtToDate").val($("#txtFromDate").attr('data-lastDay'));
    setTimeout(getGridDailyAttendanceView, 1000);
});

$("#btn_addDailyAttendanceView").click(function () { AddDailyAttendanceView(); });
var oTableChannel;

$("#btnPostRecords").click(function () {
    //setTimeout(PostRecords, 1000);
    PostRecords();
});

function PostRecords() {
    pageLoadingFrame("show");
    var FilterByStatus = $("#DDLFilterStatus").val();
    var showCnfMsgBox = 0;
    if (FilterByStatus == "" && SelectedStatus != -1) {
        FilterByStatus = SelectedStatus;
    }
    if (FilterByStatus != "" && SelectedStatus != -1) {
        if (FilterByStatus != SelectedStatus) {
            FilterByStatus = SelectedStatus;
        }
    }
    var id = "";
    var table = $('#tbl_DailyAttendanceViewList').DataTable();
    rows = table.$(".ChkRowCheck:checked", { "page": "all" }).closest('tr');
    var chkAllStatus = [];
    var k;

    if (rows.length == 0) {
        pageLoadingFrame("hide");
        ShowMessage("warning", 'Please select at least one Employee from the list.');
    }
    else {
        if ($("#check_all").is(':checked')) {
            if (FilterByStatus == "") {
                for (k = 0; k < rows.length; k++) {
                    chkAllStatus[k] = rows[k].children[7].innerText
                }
                var AllUniqueStatus = $.unique(chkAllStatus);
                if (AllUniqueStatus.length > 1) {
                    showCnfMsgBox = 1;
                }
            }
            rowData = table.rows().data();
        }
        else {
            rowData = table.rows(rows).data();
        }

        var listDailyAttendanceViewModel = [];
        var IsValidRecord = true;
        $(rowData).each(function (i, item) {
            var AttDate;
            var element = item.AttDate.split("/");
            AttDate = new Date(element[2], element[1] - 1, element[0]);

            var TodaysDate = new Date();
            if (moment(AttDate).isBefore(TodaysDate.setHours(0, 0, 0, 0))) {
                if (FilterByStatus != "") {
                    if (item.StatusID == FilterByStatus) {
                        var curRow = {};
                        curRow.HRAltID = item.HRAltID;
                        curRow.EmpoyeeName = item.EmpoyeeName;
                        curRow.DepName = item.DepName;
                        curRow.AttDate = item.AttDate;
                        curRow.InTime = item.InTime;
                        curRow.OutTime = item.OutTime;
                        curRow.Status = item.Status;
                        curRow.EarlyMinutes = item.EarlyMinutes;
                        curRow.LateMinutes = item.LateMinutes;
                        curRow.StatusID = item.StatusID;
                        curRow.DeptID = item.DeptID;
                        curRow.PayEmployeeAbsentID = item.PayEmployeeAbsentID;
                        curRow.PayEmployeeLateID = item.PayEmployeeLateID;
                        curRow.absentExcused = item.absentExcused;
                        curRow.LateExcused = item.LateExcused;
                        curRow.earlyExcused = item.earlyExcused;
                        curRow.IsActive = item.IsActive;
                        curRow.HRID = item.HRID;
                        listDailyAttendanceViewModel.push(curRow);
                    }
                }
                else {
                    var curRow = {};
                    curRow.HRAltID = item.HRAltID;
                    curRow.EmpoyeeName = item.EmpoyeeName;
                    curRow.DepName = item.DepName;
                    curRow.AttDate = item.AttDate;
                    curRow.InTime = item.InTime;
                    curRow.OutTime = item.OutTime;
                    curRow.Status = item.Status;
                    curRow.EarlyMinutes = item.EarlyMinutes;
                    curRow.LateMinutes = item.LateMinutes;
                    curRow.StatusID = item.StatusID;
                    curRow.DeptID = item.DeptID;
                    curRow.PayEmployeeAbsentID = item.PayEmployeeAbsentID;
                    curRow.PayEmployeeLateID = item.PayEmployeeLateID;
                    curRow.absentExcused = item.absentExcused;
                    curRow.LateExcused = item.LateExcused;
                    curRow.earlyExcused = item.earlyExcused;
                    curRow.IsActive = item.IsActive;
                    curRow.HRID = item.HRID;
                    listDailyAttendanceViewModel.push(curRow);
                }
            }
            else {
                TodaysDate = new Date();
                if (moment(new Date()).isBefore(TodaysDate.setHours(17, 0, 0, 0))) {
                    IsValidRecord = false;
                }
                else {
                    if (FilterByStatus != "") {
                        if (item.StatusID == FilterByStatus) {
                            var curRow = {};
                            curRow.HRAltID = item.HRAltID;
                            curRow.EmpoyeeName = item.EmpoyeeName;
                            curRow.DepName = item.DepName;
                            curRow.AttDate = item.AttDate;
                            curRow.InTime = item.InTime;
                            curRow.OutTime = item.OutTime;
                            curRow.Status = item.Status;
                            curRow.EarlyMinutes = item.EarlyMinutes;
                            curRow.LateMinutes = item.LateMinutes;
                            curRow.StatusID = item.StatusID;
                            curRow.DeptID = item.DeptID;
                            curRow.PayEmployeeAbsentID = item.PayEmployeeAbsentID;
                            curRow.PayEmployeeLateID = item.PayEmployeeLateID;
                            curRow.absentExcused = item.absentExcused;
                            curRow.LateExcused = item.LateExcused;
                            curRow.earlyExcused = item.earlyExcused;
                            curRow.IsActive = item.IsActive;
                            curRow.HRID = item.HRID;
                            listDailyAttendanceViewModel.push(curRow);
                        }
                    }
                    else {
                        var curRow = {};
                        curRow.HRAltID = item.HRAltID;
                        curRow.EmpoyeeName = item.EmpoyeeName;
                        curRow.DepName = item.DepName;
                        curRow.AttDate = item.AttDate;
                        curRow.InTime = item.InTime;
                        curRow.OutTime = item.OutTime;
                        curRow.Status = item.Status;
                        curRow.EarlyMinutes = item.EarlyMinutes;
                        curRow.LateMinutes = item.LateMinutes;
                        curRow.StatusID = item.StatusID;
                        curRow.DeptID = item.DeptID;
                        curRow.PayEmployeeAbsentID = item.PayEmployeeAbsentID;
                        curRow.PayEmployeeLateID = item.PayEmployeeLateID;
                        curRow.absentExcused = item.absentExcused;
                        curRow.LateExcused = item.LateExcused;
                        curRow.earlyExcused = item.earlyExcused;
                        curRow.IsActive = item.IsActive;
                        curRow.HRID = item.HRID;
                        listDailyAttendanceViewModel.push(curRow);
                    }
                }
            }
        });
        if (IsValidRecord) {
            if (listDailyAttendanceViewModel.length != 0) {
                listDailyAttendanceViewModel = JSON.stringify({ 'listDailyAttendanceViewModel': listDailyAttendanceViewModel });
                if (showCnfMsgBox == 1) {
                    $.MessageBox({ buttonDone: "Continue", buttonFail: "Cancel", message: "This action will post Absent, Didn't Sign In and Didn't Sign Out employees to the absent deduction page and Late In, Early Out and Late & Early employees to the late deduction page." }).done(function () {
                        $.ajax({
                            dataType: 'json',
                            type: 'POST',
                            data: listDailyAttendanceViewModel,
                            url: '/DailyAttendance/PushRecords',
                            contentType: 'application/json; charset=utf-8',
                            success: function (data) {
                                pageLoadingFrame("hide");
                                $("#LblRecordsNotPosted0").text("Absent Records Not Posted(" + data[0] + ")");
                                $("#LblRecordsNotPosted1").text("Late In Records Not Posted(" + data[1] + ")");
                                $("#LblRecordsNotPosted2").text("Early Out Records Not Posted(" + data[2] + ")");
                                ShowMessage("success", 'Records have been posted succesfully');
                            },
                            error: function (data) {
                                pageLoadingFrame("hide");
                            }
                        });
                    }).fail(function () {
                        pageLoadingFrame("hide");
                    });

                }
                else {
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        data: listDailyAttendanceViewModel,
                        url: '/DailyAttendance/PushRecords',
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            pageLoadingFrame("hide");
                            $("#LblRecordsNotPosted0").text("Absent Records Not Posted(" + data[0] + ")");
                            $("#LblRecordsNotPosted1").text("Late In Records Not Posted(" + data[1] + ")");
                            $("#LblRecordsNotPosted2").text("Early Out Records Not Posted(" + data[2] + ")");
                            ShowMessage("success", 'Records have been posted succesfully');
                        },
                        error: function (data) {
                            pageLoadingFrame("hide");
                        }
                    });
                }

            }
        }
        else {
            ShowMessage("error", "Sorry! You cannot post today's attendance before 17:00");
            pageLoadingFrame("hide");
        }
    }
}

function getGridDailyAttendanceView(obj) {
    pageLoadingFrame("show");
    var ddl_PayCycle = $("#ddl_PayCycle").val();
    var txtFromDate = $("#txtFromDate").val();
    var txtToDate = $("#txtToDate").val();
    var txtDepID = $("#ddl_DeptList").val();
    var txtEmpID = $("#ddl_EmpList").val();
    var txtSectionID = $("#ddl_SectionList").val();
    var s = "Records   From         " + "  " + txtFromDate + "    To  " + txtToDate;

    var checkAllColumn = "";
    checkAllColumn = "<input type='checkbox' id = 'check_all' name = 'check_all' onclick='CheckAll()'/>";
    $("#LblRecordsHeader").text(s);
    oTableChannel = $('#tbl_DailyAttendanceViewList').dataTable({
        "async": true,
        "contentType": "application/json",
        "sAjaxSource": "/LongOperations/GetDailyAttendanceViewList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FromDate", "value": txtFromDate }, { "name": "ToDate", "value": txtToDate }, { "name": "DepID", "value": txtDepID }, { "name": "empid", "value": txtEmpID }, { "name": "sectionid", "value": txtSectionID });
        },
        "aoColumns": [
                        {
                            "mData": "CheckRow", "sTitle": checkAllColumn, "sClass": "center",
                            "bSortable": false
                        },
                        { "mData": "HRAltID", "sTitle": "Emp ID", "sClass": "font-Bold", "sWidth": "2%" },
                        { "mData": "EmpoyeeName", "sTitle": "Name", "sClass": "EmpoyeeName", "sWidth": "15%" },
                        { "mData": "DepName", "sTitle": "Department", "sClass": "DepName" },
                        { "mData": "AttDate", "sTitle": "Date", "sType": "date-uk", "sClass": "AttDate", "sWidth": "15%" },
                        { "mData": "InTime", "sTitle": "In Time", "sClass": "InTime" },
                        { "mData": "OutTime", "sTitle": "Out Time", "sClass": "OutTime" },
                        { "mData": "Status", "sTitle": "Status", "sClass": "Status" },
                        { "mData": "EarlyMinutes", "sTitle": "Early Min", "sClass": "EarlyMinutes" },
                        { "mData": "LateMinutes", "sTitle": "Late Min", "sClass": "LateMinutes" },
                        { "mData": "totalHrs", "sTitle": "Total Hrs", "sClass": "TotalHrs" },
                        { "mData": "shiftHrs", "sTitle": "Shift Hrs", "sClass": "ShiftHrs" },
                        { "mData": "overHrs", "sTitle": "Over Hrs", "sClass": "OverHrs" },
                        { "mData": "shortHrs", "sTitle": "Short Hrs", "sClass": "ShortHrs" },

                        { "mData": "SectionName", "sTitle": "SectionName", "bVisible": false, "sClass": "SectionName" },
                        { "mData": "SectionID", "sTitle": "SectionName", "bVisible": false, "sClass": "SectionID" },
            //{ "mData": "HrHireDate", "sTitle": "Hire Date" },
            //{ "mData": "HrInductionDate", "sTitle": "Induction Date" },

            //{ "mData": "HolidayName", "sTitle": "Holiday Name", "bVisible": false },
            //{ "mData": "HolidayDate", "sTitle": "Holiday Date", "bVisible": false  },
                        { "mData": "StatusID", "sTitle": "StatusID", "bVisible": false, "sClass": "StatusID" },
                        { "mData": "DeptID", "sTitle": "DeptID", "bVisible": false, "sClass": "DeptID" },
                        { "mData": "Proceed", "sTitle": "Proceed", "bVisible": false, "sClass": "Proceed" },
                        { "mData": "IsActive", "sTitle": "IsActive", "bVisible": false, "sClass": "IsActive" },
                        { "mData": "HRID", "sTitle": "HRID", "bVisible": false, "sClass": "HRID" },
                        { "mData": "IsHireDateIsGreater", "sTitle": "IsHireDateIsGreater", "bVisible": false, "sClass": "Proceed" }


        ],
        "oLanguage": { sProcessing: "<img src='../Content/images/ajax-loader.gif'>" },
        "processing": false,
        "serverSide": false,
        "bLengthChange": true,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, 100, 1000], [5, 10, 25, 50, 100, "All"]],
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "columnDefs": [{
            targets: [0],
            orderData: [3, 2, 4]
        }
        ],
        "bSortCellsTop": true,
        "initComplete": function (settings, json) {
            GetAttendanceStatusCount();
            GetRecordsNotPostedCount();
            pageLoadingFrame("hide");
            AddTooltipOnAttDate();
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData["Proceed"] == 1) {
                $('td', nRow).css('background-color', '#D6DFFB');
                var AbsentTypeId = aData["PayAbsentTypeID"];
                var checkbox = $(nRow).find(".ChkRowCheck");
                $(checkbox).prop("checked", true);
                // $(checkbox).attr("disabled", true);
                $(checkbox).removeClass("ChkRowCheck");
            }
            if (aData["IsHireDateIsGreater"] == true) {
                $('td', nRow).css('background-color', '#F7CDCD');
                var checkbox = $(nRow).find(".ChkRowCheck");
                $(checkbox).attr("disabled", true);
                $(checkbox).removeClass("ChkRowCheck");
            }
        },
        "drawCallback": function (settings) {
            AddTooltipOnAttDate();
        }
    });
    customData = null;
    SelectedStatus = -1;
}

function showWaitPanel() {
    $('body').block({ message: '<h1> Please Wait...</h1>' });

}
function removeWaitPanel() {
    $('body').unblock();
}

function GetRecordsNotPostedCount() {
    $.ajax({
        dataType: 'json',
        type: 'Get',
        url: '/DailyAttendance/GetRecordsNotPostedCount',
        success: function (data) {
            $("#LblRecordsNotPosted0").text("Absent Records Not Posted(" + data[0] + ")");

            $("#LblRecordsNotPosted1").text("Late In Records Not Posted(" + data[1] + ")");

            $("#LblRecordsNotPosted2").text("Early Out Records Not Posted(" + data[2] + ")");
        },
        error: function (data) { }
    });
    $.unblockUI();
}

function GetAttendanceStatusCount() {
    var ddl_PayCycle = $("#ddl_PayCycle").val();
    var txtFromDate = $("#txtFromDate").val();
    var txtToDate = $("#txtToDate").val();
    var txtDepID = $("#ddl_DeptList").val();
    var txtEmpID = $("#ddl_EmpList").val();

    $.ajax({
        dataType: 'json',
        type: 'Get',
        data: { FromDate: txtFromDate, ToDate: txtToDate, DepID: txtDepID, empid: txtEmpID },
        url: '/DailyAttendance/GetDailyAttendanceStatusCount',
        success: function (data) {
            $('#LblCalendarDate').text(data[13]);
            for (var i = 0 ; i <= 12; i++) {
                $("#lblstatus" + i).text(data[i]);
            }
        },
        error: function (data) { }
    });
    $.unblockUI();
}

function getGridDailyAttendanceViewByStatus(statusid) {
    SelectedStatus = statusid;
    StartPanelLoader("#divEmpAtt");
    if ($("#check_all").is(':checked')) {
        $("#check_all").prop('checked', false);
    }

    if (customData == null) {
        customData = oTableChannel.fnGetData();
    }
    oTableChannel.fnDeleteRow();
    var i;
    for (i = 0; i < customData.length; i++) {
        var chkStatusId = customData[i].StatusID;
        if (chkStatusId == statusid) {
            oTableChannel.fnAddData(customData[i]);
        }
    }
    StopPanelLoader("#divEmpAtt");
}

function GetAttendanceStatusCountForStatus(statusid) {
    pageLoadingFrame("show");
    var ddl_PayCycle = $("#ddl_PayCycle").val();
    var txtFromDate = $("#txtFromDate").val();
    var txtToDate = $("#txtToDate").val();
    var txtDepID = $("#ddl_DeptList").val();
    var txtEmpID = $("#ddl_EmpList").val();
    $.ajax({
        dataType: 'json',
        type: 'Get',
        data: { FromDate: txtFromDate, ToDate: txtToDate, DepID: txtDepID, statusid: statusid, empid: txtEmpID },
        url: '/DailyAttendance/GetDailyAttendanceStatusCount',
        success: function (data) {
            for (var i = 0 ; i <= 11; i++) {
                $("#lblstatus" + i).text(data[i]);
            }
            pageLoadingFrame("hide");
        },
        error: function (data) { $.unblockUI(); }
    });

}

function AddDailyAttendanceView() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/DailyAttendance/AddDailyAttendanceView", function () {
        $('.timePicker12').timepicker({
            showMeridian: true,
            showInputs: true,
            minuteStep: 5
        });
        $("#myModal").modal("show");
        $("#modal_heading").text('Add Daily Attendance');
    });
}

function EditDailyAttendanceView(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/DailyAttendanceView/EditDailyAttendanceView/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Edit DailyAttendanceView');

}


function DeleteDailyAttendanceView(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/DailyAttendanceView/DeleteDailyAttendanceView',
            success: function (data) {
                getGridDailyAttendanceView();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewDailyAttendanceView(id) {
    $("#modal_Loader").load("/DailyAttendanceView/ViewDailyAttendanceView/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View DailyAttendanceView Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function CheckAll() {
    if ($("#check_all").is(':checked')) {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', true);
        $(".ChkRowCheck").closest('tr').addClass("selected");
    }
    else {
        $('.ChkRowCheck', oTableChannel.fnGetNodes()).prop('checked', false);
        $(".ChkRowCheck").closest('tr').removeClass("selected");
    }
}

function ChkRowCheckClick(obj) {
    if ($(obj).is(':checked')) {
        $(obj).closest('tr').addClass("selected");
    }
    else {
        $('#check_all').prop('checked', false);
        $(obj).closest('tr').removeClass("selected");
    }
}
function TableToExport(type) {
    var ddl_PayCycle = $("#ddl_PayCycle").val();
    var txtFromDate = $("#txtFromDate").val();
    var txtToDate = $("#txtToDate").val();
    var txtDepID = $("#ddl_DeptList").val();
    var txtEmpID = $("#ddl_EmpList").val();
    var txtSectionID = $("#ddl_SectionList").val();
    var url = "/DailyAttendance/PushRecordToDoc?type="
            + type + "&FromDate=" + txtFromDate + "&ToDate=" + txtToDate + "&DepID=" + txtDepID
            + "&empid=" + txtEmpID + "&sectionid=" + txtSectionID + "&statusid=" + newStatusId

    window.location.assign(encodeURI(url));
}

function GetLateAndEarlyMinutes() {
    var employeeId = $("#EmployeeID").val();
    if (parseInt(employeeId) > 0) {
        $.ajax({
            dataType: "json",
            type: "GET",
            data: { employeeId: employeeId, date: $("#PayEmployeeAbsentDate").val(), inTime: $("#InTime").val(), outTime: $("#OutTime").val() },
            url: '/Common/GetLateAndEarlyMinutesForEmployee',
            success: function (data) {
                $("#LateMinutes").val(data.lateMinute);
                $("#EarlyMinutes").val(data.earlyMinute);
                var AttendanceCategory = $("#AttendanceCategory").val();

                if (parseInt(AttendanceCategory) == 2) {
                    $(".divStatus").show();
                    if (data.lateMinute > 0 && data.earlyMinute > 0) {
                        $(".lblstatus").text("Late&Early").css("color", "#520D4E");
                    } else {
                        if (data.lateMinute > 0)
                            $(".lblstatus").text("Late").css("color", "#5bc0de");
                        else {
                            if (data.lateMinute == 0 && data.earlyMinute == 0)
                                $(".lblstatus").text("Present").css("color", "#00FF01");
                            else
                                $(".lblstatus").text("Early").css("color", "#f0ad4e");
                        }
                    }
                } else {
                    $(".divStatus").hide();
                    $("#lblstatus").val("");
                }
            },
            error: function (data) { }

        });
    }
}
