﻿$(document).ready(function () {


    if ($("#hdnEmployee").val() != "") {
        var id = $("#hdnEmployee").val();
         LoadEmployeeDetails(id);
        getGridWorkexperience(id);

    }
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
});
$("#btn_addWorkexperience").click(function () { AddWorkexperience(); });
var oTableChannel;

function getGridWorkexperience(empid) {
    oTableChannel = $('#tbl_WorkexperienceList').dataTable({      
        "sAjaxSource": "/Workexperience/GetWorkexperienceList", 
        
        "aoColumns": [
           
            { "mData": "WorkexperienceID", "bVisible": false, "sTitle": "WorkexperienceID" },
            { "mData": "EmployeeID", "sTitle": "Employee" },
            { "mData": "EmployerName_1", "sTitle": "Employer Name" },
            { "mData": "WorkStartDate", "sTitle": "Work Start Date" },
            { "mData": "WorkEndDate", "sTitle": "Work End Date" },
            { "mData": "HRExperiencePositionID", "sTitle": "Experience Position" },
            { "mData": "WorkAddress", "sTitle": "Work Address" },
            { "mData": "ExperienceMonths", "sTitle": "Experience Months" },
            { "mData": "CityID", "sTitle": "City" },
            { "mData": "CountryID", "sTitle": "Country" },
        { "mData": "Action", "sTitle": "Action" }
        ],
        "fnServerParams": function ( aoData ) {
            aoData.push({ "name": "empid", "value": empid});
        },
        "processing": true,
        "serverSide": true,
        "ajax": "/Workexperience/GetWorkexperienceList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true
});
}

function AddWorkexperience() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Workexperience/AddWorkexperience");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Workexperience');
}

function EditWorkexperience(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Workexperience/EditWorkexperience/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Edit Workexperience');

}


function DeleteWorkexperience(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Workexperience/DeleteWorkexperience',
            success: function (data) {
                getGridWorkexperience();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewWorkexperience(id) {
    $("#modal_Loader").load("/Workexperience/ViewWorkexperience/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Workexperience Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}
