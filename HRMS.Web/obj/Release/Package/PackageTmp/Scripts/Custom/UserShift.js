﻿$(document).ready(function () {
    GetData(1);
});
var $tr;
var oTableChannel;
var userShiftIdUpdated = 0;
var isDataIsRefresh = true;
function GetData(id) {    
    if (id==1) {
        pageLoaderFrame();
    }
    isDataIsRefresh = true;
    var empId = $("#ddlEmployeeList").val();
    var fromdate = $("#txtFromDate").val();
    var toDate = $("#txtToDate").val();
    oTableChannel = $('#tblUserList').dataTable({
        "aoColumns": [
           { "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId" },
           { "mData": "IsUpdated", "bVisible": false, "sTitle": "IsUpdated" },
           { "mData": "ShiftId", "bVisible": false, "sTitle": "ShiftId" },
           { "mData": "UserShiftId", "sTitle": "UserShiftId" },
           { "mData": "AttDate", "sTitle": "Date", 'width': '10%' },
           { "mData": "Day", "sTitle": "Day", 'width': '10%' },
           { "mData": "ShiftName", "sTitle": "Shift Name", 'width': '10%' },
           { "mData": "InTime", "sTitle": "In Time", 'width': '10%' },
           { "mData": "OutTime", "sTitle": "Out Time", 'width': '10%' },
           { "mData": "GreaceIn", "sTitle": "Grace In", "bSortable": false, 'width': '10%' },
           { "mData": "GreaceOut", "sTitle": "Grace Out", 'width': '10%' },
           { "mData": "OffDays", "sTitle": "Off Day", 'width': '10%' },
           { "mData": "Posted", "sTitle": "Posted", 'width': '10%' },
           { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '10%', "sClass": "text-center" }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Shift/GetAttShiftData?EmpId=" + empId + "&FromDate=" + fromdate + "&ToDate=" + toDate,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[2, "asc"]],
        "fnInitComplete": function (oSettings) {
            if (oTableChannel.fnGetData().length == 0) {
                ShowMessage('error', 'Please search daily attendance records for future dates to get attendance in user shifts');
            }
            hideLoaderFrame();
        }
    });
}

function EditAttShiftData(userShiftId, e) {
    userShiftIdUpdated = userShiftId;
    $tr = $(e).closest("tr");
    var inTime = $($tr).find("td").eq(4).html();
    var outTime = $($tr).find("td").eq(5).html();
    var graceIn = $($tr).find("td").eq(6).html();
    var graceOut = $($tr).find("td").eq(7).html();
    $($tr).find("td").eq(4).html("<input data-Old='" + inTime + "' type='text' value='" + inTime + "' class='form-control txtTimeInput txtInTime'>");
    $($tr).find("td").eq(5).html("<input data-Old='" + outTime + "' type='text' value='" + outTime + "' class='form-control txtTimeInput txtOutTime'>");
    $($tr).find("td").eq(6).html("<input data-Old='" + graceIn + "' type='text' value='" + graceIn + "' class='form-control txtMinInput txtGraceIn'>");
    $($tr).find("td").eq(7).html("<input data-Old='" + graceOut + "' type='text' value='" + graceOut + "' class='form-control txtMinInput txtGraceOut'>");
    $($tr).find("td").eq(10).html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='UpdateAttShiftData(" + userShiftId + ",this)' title='Save'><i class='fa fa-check'></i> </a>" +
        "<a class='btn btn-danger  btn-rounded btn-condensed btn-sm' onclick='CancelRow(" + userShiftId + ",this)' title='Close'><i class='fa fa-times'></i> </a>");
    $($tr).addClass("IsEdit");
    $('input.txtTimeInput').timepicker({
        minTime: '00:00:00', // 11:45:00 AM,
        maxHour: 0,
        maxMinutes: 10,
        step: 5,
        startTime: new Date(0, 0, 0, 15, 0, 0),// 3:00:00 PM - noon
        timeFormat: 'h:iA',
        disableTextInput: true
    }).on('change', function () {
        //ShowMessage("success", $('#ShiftID').length);

    });
}

function CancelRow(userShiftId, e) {
    var $tr = $(e).closest("tr");
    var inTime = $($tr).find(".txtInTime").attr("data-Old");
    var outTime = $($tr).find(".txtOutTime").attr("data-Old");
    var graceIn = $($tr).find(".txtGraceIn").attr("data-Old");
    var graceOut = $($tr).find(".txtGraceOut").attr("data-Old");
    $($tr).find("td").eq(4).html(inTime);
    $($tr).find("td").eq(5).html(outTime);
    $($tr).find("td").eq(6).html(graceIn);
    $($tr).find("td").eq(7).html(graceOut);
    $($tr).find("td").eq(10).html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAttShiftData(" + userShiftId + ",this)' title='Edit'><i class='fa fa-pencil'></i> </a>");
    $($tr).removeClass("IsEdit");
}

function UpdateAttShiftData(UserShiftId, e) {
    var $tr = $(e).closest("tr");
    var inTime = $($tr).find(".txtInTime").val();
    var outTime = $($tr).find(".txtOutTime").val();
    var graceIn = $($tr).find(".txtGraceIn").val();
    var graceOut = $($tr).find(".txtGraceOut").val();
    var AttShiftTableModel = {
        UserShiftId: UserShiftId,
        EmployeeId: 0,
        EmployeeAlternativeId: '',
        EmployeeName: '',
        Day: '',
        AttDate: '',
        ShiftId: 0,
        ShiftName: '',
        OffDays: false,
        Posted: false,
        IsUserShiftUpdated: false,
        InTime: $($tr).find('.txtInTime').val(),
        OutTime: $($tr).find('.txtOutTime').val(),
        GreaceIn: $($tr).find('.txtGraceIn').val(),
        GreaceOut: $($tr).find('.txtGraceOut').val(),
    }
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: AttShiftTableModel,
        url: '/Shift/UpdateAttShiftData',
        success: function (data) {
            if (data == true) {
                ShowMessage("success", "User Shift Update Successfully");
                $($tr).find("td").eq(4).html(inTime);
                $($tr).find("td").eq(5).html(outTime);
                $($tr).find("td").eq(6).html(graceIn);
                $($tr).find("td").eq(7).html(graceOut);
                $($tr).find("td").eq(10).html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAttShiftData(" + UserShiftId + ",this)' title='Edit'><i class='fa fa-pencil'></i> </a>");
                $($tr).removeClass("IsEdit");
            }

            else
                ShowMessage("error", "Technical error has occurred");
        },
        error: function (data) { }
    });
}

function CopyData() {  
    var table = $("#tblUserList").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var UpdatedRowData = table.rows(UpdatedTr).data();
        var ShiftIds = [];
        $(UpdatedRowData).each(function (i, itm) {
            ShiftIds.push({
                UserShiftId: itm.UserShiftId,
                ShiftId: itm.ShiftId
            });

        });
        var updatedData = [];
        $(UpdatedTr).each(function (i, item) {
            var UserShiftId = $(item).find("td").eq(0).html();
            var AttDate = $(item).find("td").eq(1).html();
            var ShiftId = ShiftIds.find(x => x.UserShiftId === parseInt(UserShiftId)).ShiftId;
            var inTime = $(item).find("td").eq(4).find("input").val();
            var outTime = $(item).find("td").eq(5).find("input").val();
            var graceIn = $(item).find("td").eq(6).find("input").val();
            var graceOut = $(item).find("td").eq(7).find("input").val();
            updatedData.push({
                UserShiftId: UserShiftId,
                ShiftId: ShiftId,
                AttDate: AttDate,
                InTime: inTime,
                OutTime: outTime,
                GreaceIn: graceIn,
                GreaceOut: graceOut
            });

        });
        console.log(updatedData);
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to copy & save for selected shift data?" }).done(function () {            
            pageLoaderFrame();
            $.ajax({
                url: "/Shift/CopyAndSaveMultipleUserShift",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'attShiftList': updatedData }),                
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        GetData(0)                        
                    }
                    else {
                        hideLoaderFrame();
                        ShowMessage("error", data.Message);
                    }
                }
            })
        });
    }
    else {
        hideLoaderFrame();
        ShowMessage("error", "Please edit atleast one record.");
    }
}

function saveAllUserShift() {
    var table = $("#tblUserList").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var UpdatedRowData = table.rows(UpdatedTr).data();
        var updatedData = [];
        $(UpdatedTr).each(function (i, itm) {
            var UserShiftId = $(itm).find("td").eq(0).html();
            var inTime = $(itm).find("td").eq(4).find("input").val();
            var outTime = $(itm).find("td").eq(5).find("input").val();
            var graceIn = $(itm).find("td").eq(6).find("input").val();
            var graceOut = $(itm).find("td").eq(7).find("input").val();
            updatedData.push({
                UserShiftId: UserShiftId,
                InTime: inTime,
                OutTime: outTime,
                GreaceIn: graceIn,
                GreaceOut: graceOut
            });

        });
        console.log(updatedData);
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all user shift data?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/Shift/SaveMultipleUserShift",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'attShiftList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        GetData(0)
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            })
        });
    }
    else {
        ShowMessage("error", "Please edit atleast one record.");
        hideLoaderFrame();
    }
}