﻿$(document).ready(function () {

    getGrid();

    $("#btn_add").click(function () {
        EditChannel(-1);
    });
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));
});

$("#btnTextClear").click(function () {
    $('#txtSearch').val("");
    getGrid();
});

$("#btnTextSearch").click(function () {

    getGrid();
});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employeecredential').dataTable({
        "sAjaxSource": "/EmployeeCredential/GetEmployeeCredentialList",       
        "aoColumns": [

            { "mData": "UserId", "bVisible": false, "sTitle": "Code" },
            { "mData": "EmployeeId", "bVisible": false, "sTitle": "Employee Id" },
            { "mData": "EmployeeAlternativeId", "sTitle": "Employee Id", "width": "10%" },
            { "mData": "EmployeeName", "sTitle": "Employee Name", "width": "23%" },
            { "mData": "Username", "sTitle": "User Name", "width": "23%" },
            { "mData": "Password", "sTitle": "Password", "bSortable": false, "width": "20%" },
            { "mData": "IsEnable", "sTitle": "Enabled", "sClass": "text-middle", "width": "10%", "bSortable": false },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "14%" }

        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/EmployeeCredential/GetEmployeeCredentialList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[3, "asc"]],
        "fnDrawCallback": function () {
           
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {

    //$("#div_employeecredential").load();
    //$("#myModal").modal("show");
    var buttonName = id > -1 ? "Update" : "Save";
    $("#modal_Loader").load("/EmployeeCredential/Edit/" + id, function () {
        $("#myModal").modal("show");
        bindSelectpicker('.selectpickerddl');
        if (id > 0)
        { $("#modal_heading").text('Edit User Account'); }
        else
        {
            $("#modal_heading").text('Add User Account');
        }
        //$("#frmSaveCredential").find('input[type="submit"]').val(buttonName);
    });

}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/EmployeeCredential/Delete',
            success: function (data) {
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/EmployeeCredential/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/EmployeeCredential/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}


function PasswordGeneration() {
    var Password = $("#EmployeeId option:selected").text();
    var res = Password.split(" ");
    var a = res[0].toString();
    if (res.count > 1)
        a += res[1].charAt(0).toString();
    $("#Password").val(a);
}

function CheckUserName() {
    var $form = $('form');
    if ($form.valid()) {

        var check = 0;
        if ($("#UserId").val() != "0" && $("#UserId").val() != "")//Edit
        {
            if ($("#Username").val() != $("#hdnUsername").val())
                check = 1;
        }
        else//New
            check = 1;

        if (check == 1) {
            var result = "";
            $.ajax({
                dataType: 'json',
                type: 'GET',
                url: '/EmployeeCredential/CheckUserName',
                data: { "username": $("#Username").val() },
                success: function (data) {
                    result = data;

                    if (result == "1") {
                        ShowMessage("error", "Username already exists. Please try another username.");
                    }
                    else {
                        $('#frmSaveCredential').submit();
                    }
                },
                error: function (msg) {
                    ShowMessage("error", "Technical error");
                }
            });
        }
        else
            $('#frmSaveCredential').submit();
    }
}