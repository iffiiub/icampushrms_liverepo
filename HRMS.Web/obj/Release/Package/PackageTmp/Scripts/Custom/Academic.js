﻿
$("#btn_addAcademic").click(function () { AddAcademic(); });
$("#btn_addCertificate").click(function () { AddCertificates(); });
var oTableChannel;

function getGridAcademic(empid) {

    oTableChannel = $('#tbl_AcademicList').dataTable({
        "sAjaxSource": "/Academic/GetAcademicList",
        "aoColumns": [

           // { "mData": "AcademicID", "bVisible": false, "sTitle": "AcademicID" },
            { "mData": "HRQualificationName_1", "sTitle": "Subject", "mDataProp": "HRQualificationName_1", 'width': '20%' },
            { "mData": "HRDegreeName_1", "sTitle": "Qualification", "mDataProp": "HRDegreeName_1", 'width': '20%' },
            { "mData": "HRInstituteName_1", "sTitle": "Institute", "mDataProp": "HRInstituteName_1", 'width': '20%' },
            { "mData": "CountryName_1", "sTitle": "Country", "mDataProp": "CountryName_1", 'width': '20%' },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, 'width': '20%' }

        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": false,
        "serverSide": false,
        "ajax": "/Academic/GetAcademicList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_AcademicList");
        }
    });
}


function getGridCertificate(empid) {

    oTableChannel = $('#tbl_CertificateList').dataTable({
        "sAjaxSource": "/Academic/GetCertificateList",
        "aoColumns": [

           // { "mData": "AcademicID", "bVisible": false, "sTitle": "AcademicID" },
            { "mData": "CertificationName", "sTitle": "Name", "mDataProp": "CertificationName" },
            { "mData": "CertificationDate", "sTitle": "Certification Date", "mDataProp": "CertificationDate", "sType": "date-uk" },
            { "mData": "NameAsInDegree", "sTitle": "Name As In Degree", "mDataProp": "NameAsInDegree" },
            { "mData": "HRInstituteName_1", "sTitle": "Institute", "mDataProp": "HRInstituteName_1" },
            { "mData": "CountryName_1", "sTitle": "Country", "mDataProp": "CountryName_1" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false }

        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_CertificateList");
        }
    });
}

function AddAcademic() {
    if (parseInt($("#hdnEmployee").val()) > 0) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/Academic/AddAcademic", function () {
            bindSelectpicker('.selectpickerddl');
            $("#modal_heading").text('Add Academic Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else {
        ShowMessage("error", "Please Select Employee.");
    }
}

function AddCertificates() {
    if (parseInt($("#hdnEmployee").val()) > 0) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/Academic/AddCertificates", function () {
            bindSelectpicker('.selectpickerddl');
            $("#modal_heading").text('Add Certificates Details');
            $('#myModal').modal({ backdrop: 'static' });
        });

    }
    else {
        ShowMessage("error", "Please Select Employee.");
    }
}

function EditAcademic(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Academic/EditAcademic/" + id, function () {
        $("#modal_heading").text('Edit Academic Details');
        $('#myModal').modal({ backdrop: 'static' });
        bindSelectpicker('.selectpickerddl');
    });

}
function EditCertificate(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Academic/EditCertificates/" + id, function () {
        $("#modal_heading").text('Edit Certificate Details');
        $('#myModal').modal({ backdrop: 'static' });
        bindSelectpicker('.selectpickerddl');
    });

}


function ViewAcademic(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Academic/ViewAcademic/" + id, function () {
        $("#modal_heading").text('View Academic Details');
        $('#myModal').modal({ backdrop: 'static' });
    });

}

function ViewCertificate(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Academic/ViewCertificateDetails/" + id, function () {
        $("#modal_heading").text('View Certificate Details');
        $('#myModal').modal({ backdrop: 'static' });
    });

}


function DeleteAcademic(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Academic/DeleteAcademic',
            success: function (data) {
                ShowMessage("success", "Academic Details deleted successfully.");

                if ($("#hdnEmployee").val() != "") {
                    var id = $("#hdnEmployee").val();
                    LoadEmployeeDetails(id);
                    //  getGridWorkexperience(id);
                    getGridAcademic(id);

                }
                else {
                    getGridAcademic();
                }
            },
            error: function (data) { }
        });
    });
}

function DeleteCertificate(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Academic/DeleteCertificate',
            success: function (data) {
                ShowMessage("success", "Certificate Details deleted successfully.");

                if ($("#hdnEmployee").val() != "") {
                    var id = $("#hdnEmployee").val();
                    LoadEmployeeDetails(id);
                    //  getGridWorkexperience(id);
                    getGridCertificate(id);

                }
                else {
                    getGridCertificate();
                }
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

/*Qualification*/
function AddSubject() {
    $("#modal_Loader1").html("");
    $("#modal_Loader1").load("/Academic/AddQualification");
    $("#myModal1").modal({ backdrop: 'static' });
    $("#modal_heading1").text("Add Subject");
}

function GetAllHRQualification() {

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: {},
        url: '/Academic/GetAllHRQualification',
        success: function (data) {

            var listB = $('#HRQualificationID');
            listB.empty();
            listB.append(
                    $('<option>', {
                        value: 0,
                        text: 'Select Subject'
                    }, '<option/>'))
            $.each(data, function (index, item) {
                listB.append(
                    $('<option>', {
                        value: item.HRQualificationID,
                        text: item.HRQualificationName_1
                    }, '<option/>'))
            });
            RefreshSelect('#HRQualificationID');
        },
        error: function (data) { }
    });
}
/*End-Qualification*/

/*Degree*/
function AddQualification() {
    $("#modal_Loader1").html("");
    $("#modal_Loader1").load("/Academic/AddDegree");
    $("#myModal1").modal({ backdrop: 'static' });
    $("#modal_heading1").text("Add Qualification");
}

function GetAllHRDegree() {

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: {},
        url: '/Academic/GetAllHRDegree',
        success: function (data) {

            var listB = $('#HRDegreeID');
            listB.empty();
            listB.append(
                    $('<option>', {
                        value: 0,
                        text: 'Select Qualification'
                    }, '<option/>'))
            $.each(data, function (index, item) {
                listB.append(
                    $('<option>', {
                        value: item.HRDegreeID,
                        text: item.HRDegreeName_1
                    }, '<option/>'))
            });
            RefreshSelect('#HRDegreeID');
        },
        error: function (data) { }
    });
}
/*End-Degree*/

/*Institute*/
function AddInstitute() {
    $("#modal_Loader1").html("");
    $("#modal_Loader1").load("/Academic/AddInstitute");
    $("#myModal1").modal({ backdrop: 'static' });
    $("#modal_heading1").text("Add Institute");
}

function GetAllHRInstitute() {

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: {},
        url: '/Academic/GetAllHRInstitute',
        success: function (data) {

            var listB = $('#HRInstituteID');
            listB.empty();
            listB.append(
                    $('<option>', {
                        value: 0,
                        text: 'Select Institute'
                    }, '<option/>'))
            $.each(data, function (index, item) {
                listB.append(
                    $('<option>', {
                        value: item.HRInstituteID,
                        text: item.HRInstituteName_1
                    }, '<option/>'))
            });
            RefreshSelect('#HRInstituteID');
        },
        error: function (data) { }
    });
}
/*End-Institute*/
function CheckRelevant(e, AcademicId) {
    var EmpId=$("#EmployeeID").val();
    if ($(e).is(":checked")) {
        $.ajax({
            type: 'POST',
            data: { EmployeeID: EmpId, AcademicId: AcademicId },
            url: '/Academic/CheckRelevant',
            success: function (data) {
                if (data.result == 'success')
                    //ShowMessage(data.result, data.resultMessage);
                    $("#warningMessage").removeClass("hidden");
            },
            error: function (data) { }
        });

    }
    else {
        $("#warningMessage").addClass("hidden");
    }

}