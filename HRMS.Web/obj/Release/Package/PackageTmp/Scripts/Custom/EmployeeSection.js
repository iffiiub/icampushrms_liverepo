﻿$(document).ready(function () {

    getGrid();
    //BindEmployees();
    $("#btn_add").click(function () {
        EditEmployeeSection(0);
    });
});


var oTableChannel;


function getGrid() {

    oTableChannel = $('#tbl_PayDeductionTypes').dataTable({
        "dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        "language": {
            "lengthMenu": "Records per page _MENU_ ",
        },
        "sAjaxSource": "/EmployeeSection/GetEmployeeSectionList",
        "aoColumns": [
            { "mData": "Actions", "sTitle": "Actions", "width": 177 },
            { "mData": "EmployeeSectionID", "bVisible": false, "sTitle": "DeductionTypeID" },
            { "mData": "EmployeeSectionName_1", "bSortable": true, "sTitle": "Section Name(EN)" },
            { "mData": "EmployeeSectionName_2", "bSortable": true, "sTitle": "Section Name (AR)" },
            { "mData": "EmployeeSectionName_3", "sTitle": "Section Name (FR)" },
            { "mData": "Head", "sTitle": "Head", "width": 100 }
        ],
        "processing": true,
        "serverSide": true,
        "shrinkToFit": false,
        "ajax": "/EmployeeSection/GetEmployeeSectionList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true
    });
}

function EditEmployeeSection(id) {
    $("#modal_Loader").load("/EmployeeSection/EditEmployeeSection/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Employee Section');
}

function DeleteEmployeeSection(id) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/EmployeeSection/DeleteEmployeeSection',
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
            getGrid();
        },
        error: function (data) { }
    });

}

$("#btnEditCancel").on("click", function () {
    $("#myModal").modal("hide");
});