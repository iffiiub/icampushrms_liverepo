﻿$jquery = jQuery.noConflict(true);
$jquery(document).ready(function () {
    $jquery('#ddlCompanyList').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '300', includeSelectAllOption: true });
    $jquery('#ddlCategoryList').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '300', includeSelectAllOption: true });

    $("#SalaryPaymentSummaryReportPreview").click(function () {
        var payCycleId = $("#ddlPayCycle").val();
        var complanyIds = getSelectedIds('.ddlCompany').join(',');
        var categoryIds = getSelectedIds('.ddlCategory').join(',');
        globalFunctions.previewCRReport('/Reporting/LoadSalaryPaymentSummeryReport?complanyIds=' + complanyIds + '&payCycleId=' + payCycleId + '&payCycleTitle=' + $("#ddlPayCycle option:selected").text() + '&categoryIds=' + categoryIds);
    });
});