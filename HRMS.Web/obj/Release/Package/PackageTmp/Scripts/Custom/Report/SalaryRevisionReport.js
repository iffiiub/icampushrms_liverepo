﻿$(document).ready(function () {
    $("#btnStatment").click(function () {      
        var employeeIds = getSelectedIds('.multiddl').join(',');
        if (employeeIds != "") {
            globalFunctions.previewCRReport('/Reporting/LoadSalaryRevisionCrReport?Empids=' + employeeIds);         
        } else {
            ShowMessage("error","Please select employee.")
        }
               
    });
});

function rebuildEmpDDl()
{    
    $jq14("#ddlEmployeeList").next().addClass('open');
    $jq14("#ddlEmployeeList").next();
    $jq14("#ddlEmployeeList").multiselect("selectAll");
    $jq14("#ddlEmployeeList").multiselect('updateButtonText');
    $jq14("#ddlEmployeeList").multiselect('rebuild');
    $jq14("#ddlEmployeeList").next().removeClass('open');
}
function IncludeIncativeEmployee(e) {
    var active = null;
    if (!$(e).is(':checked')) {
        active = true;
    }
    else {
     
    }
    
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { 'active': active },
        url: '/Common/GetEmployeeDdl',
        success: function (data) {
            $jq14("#ddlEmployeeList").empty();
            $jq14("#ddlEmployeeList").multiselect('refresh');
            var parsed = data;
            for (var i = 0; i < data.length; i++) {
                
                $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');
               
            }
            rebuildEmpDDl();
        },
        error: function (data) {
        }
    })
}


function GetIncrementedSalaryEmployee(e) {
    if ($(e).is(':checked')) {
        $("#chkisIncludeInactive").attr('checked',false);
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data:{isIncludeEmpJoinInLastMonth: $("#chkisIncludeEmpJoinInLastMonth").is(":checked")},
            url: '/Common/GetIncrementedSalaryEmployee',
            success: function (data) {
                $jq14("#ddlEmployeeList").empty();
                $jq14("#ddlEmployeeList").multiselect('refresh');
                var parsed = data;
                for (var i = 0; i < data.length; i++) {
                    $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                    $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');
                }
                rebuildEmpDDl();
            },
            error: function (data) {
            }
        })
    }
    else if ($("#chkisIncludeEmpJoinInLastMonth").is(":checked"))
    {
        GetEmployeeInJoinInLastMonth("#chkisIncludeEmpJoinInLastMonth");
    }
    else {
        var active = null;
        active = true;
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { 'active': active },
            url: '/Common/GetEmployeeDdl',
            success: function (data) {
                $jq14("#ddlEmployeeList").empty();
                $jq14("#ddlEmployeeList").multiselect('refresh');
                var parsed = data;
                for (var i = 0; i < data.length; i++) {
                    $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                    $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');

                }
                rebuildEmpDDl();
            },
            error: function (data) {
            }
        })

    }
    
}

function GetEmployeeInJoinInLastMonth(e) {
    
    if ($(e).is(':checked')) {
        $("#chkisIncludeInactive").attr('checked', false);
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { isIncludeIncSalEmp: $("#chkisGetIncrementedSalaryEmployee").is(":checked") },
            url: '/Common/GetEmployeeInJoinInLastMonth',
            success: function (data) {
                $jq14("#ddlEmployeeList").empty();
                $jq14("#ddlEmployeeList").multiselect('refresh');
                var parsed = data;
                for (var i = 0; i < data.length; i++) {
                    $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                    $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');

                }              
                rebuildEmpDDl();
            },
            error: function (data) {
            }
        })
    }
    else if ($("#chkisGetIncrementedSalaryEmployee").is(":checked"))
    {
        GetIncrementedSalaryEmployee("#chkisGetIncrementedSalaryEmployee");
    }
    else {
        var active = null;     
            active = true;
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { 'active': active },
            url: '/Common/GetEmployeeDdl',
            success: function (data) {
                $jq14("#ddlEmployeeList").empty();
                $jq14("#ddlEmployeeList").multiselect('refresh');
                var parsed = data;
                for (var i = 0; i < data.length; i++) {                    
                    $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                    $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');
                }               
                rebuildEmpDDl();
            },
            error: function (data) {
            }
        })

    }
    
}

