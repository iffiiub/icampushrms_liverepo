﻿$(document).ready(function () {
    $("#btnPreviewEmployeeDistributionSectionReport").click(function () {
        var EmployeeIds = getSelectedIds('.SectionList').join(',');
        var Url = "/Reporting/EmployeeDistributionSectionViewer?SectionIds=" + EmployeeIds;
        $("#LoadReportDiv").load(Url);
    });
});