﻿
var DeductionReport = function () {

    loadAbsentDeductionReport = function () {
        if (checkDateValidation()) {
            var text = "/Reporting/DeductionReportviewer?reportType=AbsentDeductable";
            text = urlParams(text);          
            text = text + "&SortBy=" + $("#ddlOrderBy").val();
            pageLoaderFrame();
            $('#LoadReportDiv').html('').load(encodeURI(text), function () {
                hideLoaderFrame();
            });
        } else {
            ShowMessage("error", "From Date or To Date is not selected");
        }
    },

    loadAbsentDeductionSalaryReport = function () {
        if (checkDateValidation()) {
            var text = "/Reporting/DeductionReportviewer?reportType=AbsentDeductableSalary";
            text = urlParams(text);
            text = text + "&SortBy=" + $("#ddlOrderBy").val();
            pageLoaderFrame();
            $('#LoadReportDiv').html('').load(encodeURI(text), function () {
                hideLoaderFrame();
            });
        } else {
            ShowMessage("error", "From Date or To Date is not selected");
        }
    },

    loadAbsentUnDeductionReport = function () {

        var text = "/Reporting/DeductionReportviewer?reportType=AbsentUndeductableReportViewer";
        text = text + "&startDate=" + $('#txtStartDate').val() + "&toDate=" + $('#txtToDate').val();
        //$('#reportframe').attr('src', encodeURI(text));
        pageLoaderFrame();
        $('#LoadReportDiv').html('').load(encodeURI(text), function () {
            hideLoaderFrame();
        });
    },

    loadLateDeductionReport = function () {
        var text = "/Reporting/DeductionReportviewer?reportType=LateDeductibleReportViewer";
        text = urlParams(text);
        //$('#reportframe').attr('src', encodeURI(text));
        text = text + "&SortBy=" + $("#ddlOrderBy").val();
        pageLoaderFrame();
        $('#LoadReportDiv').html('').load(encodeURI(text), function () {
            hideLoaderFrame();
        });
    },

    loadEarlyDeductionReport = function () {
        var text = "/Reporting/DeductionReportviewer?reportType=EarlyDeductibleReportViewer";
        text = urlParams(text);
        pageLoaderFrame();
        $('#LoadReportDiv').html('').load(encodeURI(text), function () {
            hideLoaderFrame();
        });
    },

    loadPayDeductionReport = function () {
        var url = "/Reporting/DeductionReportviewer?reportType=PayDeductionReport";
        var startDate = $('#txtStartDate').val();
        var toDate = $('#txtToDate').val();
        if (startDate != "")
            url = url + "&startDate=" + startDate;
        if (toDate != "") {
            url = url + "&toDate=" + toDate;
        }
        url = url + "&deductionType=" + $('#deductionddl').val();

        pageLoaderFrame();
        $('#LoadReportDiv').html('').load(encodeURI(url), function () {
            hideLoaderFrame();
        });
    },

    checkDateValidation = function () {
        if ($('#txtStartDate').val() != "" && $('#txtToDate').val() != "")
            return true;
        else
            return false;
    },

    urlParams = function (url) {
        var startDate = $('#txtStartDate').val();
        var toDate = $('#txtToDate').val();
        if (startDate != "") {
            if ($('#chkLateFrom').length > 0) {
                if ($('#chkLateFrom').prop('checked')) {
                    url = url + "&startDate=" + startDate;
                } else {

                }
            } else {
                url = url + "&startDate=" + startDate;
            }
        }

        if (toDate != "") {
            if ($('#chkLateFrom').length > 0) {
                if ($('#chkLateFrom').prop('checked')) {
                    url = url + "&toDate=" + toDate;
                } else {

                }
            } else {
                url = url + "&toDate=" + toDate;
            }
        }

        if ($('#chkConfiremed').prop('checked'))
            url = url + "&bConfirmed=" + $('#chkConfiremed').prop('checked');
        if ($('#chkGenerated').prop('checked'))
            url = url + "&bGenrated=" + $('#chkGenerated').prop('checked');
        if ($('#chkLateFrom').prop('checked'))
            url = url + "&bSelectedLateFrom=" + $('#chkLateFrom').prop('checked');
        if ($('#chkshowOnly').length > 0) {
            if ($('#chkshowOnly').prop('checked')) { }
            else
                url = url + "&bShowAllRecord=true";
        }
        return url;
    },
    inti = function () {

    };

    return {
        loadAbsentDeductionReport: loadAbsentDeductionReport,
        loadAbsentDeductionSalaryReport: loadAbsentDeductionSalaryReport,
        loadAbsentUnDeductionReport: loadAbsentUnDeductionReport,
        loadLateDeductionReport: loadLateDeductionReport,
        loadEarlyDeductionReport: loadEarlyDeductionReport,
        loadPayDeductionReport: loadPayDeductionReport
    };
}();

$(function () {
    $('#btnAbsentDeduction').click(function () {
        DeductionReport.loadAbsentDeductionReport();
    });

    $('#btnAbsentUnDeduction').click(function () {
        DeductionReport.loadAbsentUnDeductionReport();
    });

    $('#btnLateDeduction').click(function () {
        DeductionReport.loadLateDeductionReport();
    });

    $('#btnEarlyDeduction').click(function () {
        DeductionReport.loadEarlyDeductionReport();
    });

    $('#btnAbsentDeductionWithSalary').click(function () {
        DeductionReport.loadAbsentDeductionSalaryReport();
    });

    $('#chkLateFrom').click(function () {
        $('#txtStartDate').attr('disabled', !this.checked);
        $('#txtToDate').attr('disabled', !this.checked);
    });

    $('#btnPayDeduction').click(function () {
        DeductionReport.loadPayDeductionReport();
    });
});

$(document).ready(function () {

    $('#school').change(function () { // handle for drop down
        var form = $(this).closest('form');
        $(form).submit();// these fire submite event of form
    });

    try {
        $('#ddlYear').val(dateYear);
        RefreshSelect('#ddlYear');
    } catch (e) { }

    try {
        $('#ddlMonth').val(dateMonth);
        RefreshSelect('#ddlMonth');
    } catch (e) { }
});