﻿var SalaryReport = {
    checkValidation: function () {
        var check = true;
        if (!(parseInt($("#ddlEmployee").val()) > 0)) {
            ShowMessage("error", "Please select employee name");
            check = false;
        } else {

            if ($("#txtFrom").val() == "" || $("#txtTo").val() == "") {
                ShowMessage("error", "Please select date properly");
                check = false;
            }
        }
        return check;
    }
};
$(function () {
    //bindDatePicker('.txtDate')
    $("#ddlEmployee").change(function () {
        var EmployeeId = $(this).val();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { employeeId: EmployeeId },
            url: '/Reporting/GetEmployeeHireDate',
            success: function (data) {
                //$('#txtTo').datepicker().val(new Date(2017, 9, 10));
                $('#txtFrom').datepicker("setDate", data.hireData);
                //$('#txtFrom').val(data.hireData);
                //$('#txtTo').val(data.todayDate);
            },
            error: function (data) {
            }
        })
    });

    $("#btnStatment").click(function () {
        if (SalaryReport.checkValidation()) {
            var url = '/Reporting/SalaryStatementViewer?fromDate=' + $("#txtFrom").val() + '&toDate=' + $("#txtTo").val() + '&empId=' + $("#ddlEmployee").val();
            var win = window.open(url, '_blank');
            win.focus();
        }

    });
});

function ShowIncativeEmployee(e) {
    var active = null;
    if (!$(e).is(':checked'))
        active = true;
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { 'active': active },
        url: '/Common/GetEmployeeDdl',
        success: function (data) {
            $("#ddlEmployee").html("");
            $("#ddlEmployee").append($("<option     />").val
               ("").text("Select Employee"));
            $.each(data, function (ind, item) {
                $("#ddlEmployee").append($("<option></option>").val
               (item.Value).text(item.Text));
            });
            RefreshSelect("#ddlEmployee");
        },
        error: function (data) {
        }
    })
}