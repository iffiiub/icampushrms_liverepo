﻿
var AttendanceReport = function () {
    BindData = function (sourceId, destinationId, result) {
        var text = "";
        $(destinationId).html('');
        $.each(result, function (ind, item) {
            text = text + "<option value='" + item.Value + "'>" + item.Text + "</option>"
        });
        $(destinationId).html(text);
        RefreshSelect(destinationId);
    },

    loadAttendanceReport = function () {        
        $("#divStireport").hide();
        $("#divCrReport").hide();
        var chkTemplate1 = $('#chkTemplate1').prop('checked');
        var chkDailyAttTemplate1 = $('#chkDailyAttTemplate1').prop('checked');
        var pageId = $("#PageId").val().toLowerCase();
        var text = "";
        if (chkTemplate1) {
            text = "/Reporting/LoadAttendanceReportByWorkingDays?pageId=" + pageId;
            $("#divCrReport").show();
        }
        else if (chkDailyAttTemplate1) {
            text = "/Reporting/LoadAttendanceReportByDateByEmployee?pageId=" + pageId;
            $("#divCrReport").show();
        }
        else {
            text = "/Reporting/FilteredAttendanceReportViewer?pageId=" + pageId;
            $("#divStireport").show();
        }

        var StatusIds = getSelectedIds('.ddlStatusList').join(',');
        //dropdowns
        if ($('#ddlDepartmentList').val() != "" && $('#ddlDepartmentList').length > 0)
            text = text + "&depId=" + $('#ddlDepartmentList').val();
        if ($('#ddlEmployeeList').val() != "" && $('#ddlEmployeeList').length > 0)
            text = text + "&empId=" + $('#ddlEmployeeList').val();
        if ($('#ddlSectionList').val() != "" && $('#ddlSectionList').length > 0)
            text = text + "&sectionId=" + $('#ddlSectionList').val();
        if ($('#ddlShiftList').val() != "" && $('#ddlShiftList').length > 0)
            text = text + "&shiftId=" + $('#ddlShiftList').val();

        //checkfields

        if ($('#chkInactiveOnly').length > 0) {
            text = text + "&inactiveOnly=" + $('#chkInactiveOnly').prop('checked');
        } else {
            text = text + "&inactiveOnly=false";
        }

        if ($('#chkPostedOnly').length > 0) {
            if ($('#chkPostedOnly').prop('checked'))
                text = text + "&postedOnly=" + $('#chkPostedOnly').prop('checked');
        }

        if ($('#chkDetailReports').length > 0) {
            text = text + "&detailReports=" + $('#chkDetailReports').prop('checked');
        } else {
            text = text + "&detailReports=false";
        }

        if (pageId == "employeedailyattendance" || pageId == "workingdays")//Monthly Summary
        {
            text = text + "&isListReport=" + $('#chkListReport').prop('checked');
            if (pageId == "employeedailyattendance") {
                text = text + "&isEmployeePageWise=" + $('#EmployeRecordByPage').prop('checked');
            }
        }

        //dates
        if (pageId == "monthlyattendancesummary")//Monthly Summary
        {
            var month = parseInt($('#ddlMonths').val()) - 1;
            var year = $('#ddlYears').val();
            var firstDay = new Date(year, month, 1);
            var lastDay = new Date(year, month, 3);
            text = text + "&fromDate=" + firstDay.getDate() + '/' + (firstDay.getMonth() + 1) + '/' + firstDay.getFullYear();
            text = text + "&toDate=" + lastDay.getDate() + '/' + (lastDay.getMonth() + 1) + '/' + lastDay.getFullYear();
            text = text + "&month=" + $('#ddlMonths').val();
            text = text + "&year=" + $('#ddlYears').val();
        }
        else {
            text = text + "&fromDate=" + $('#txtFromDate').val();
            text = text + "&toDate=" + $('#txtToDate').val();
        }

        if (pageId == "attendancestatistics") {
            text = text + "&OrderBy=Percentage&OrderType=" + $("#ddlOrderBy").val() + "&isPercentageBasedOndays=" + $("#PercentageBasedOndays").is(":checked")
        }

        if ($("#ddlSuperVisorList").length > 0) {
            if (parseInt($("#ddlSuperVisorList").val()) > 0) {
                text = text + "&superVisorId=" + $('#ddlSuperVisorList').val();
            }
        }


        if (StatusIds.length > 0) {
            text = text + "&statusID=" + StatusIds;
        }

        //$('#reportframe').attr('src', encodeURI(text));

        if (chkTemplate1 || chkDailyAttTemplate1) {
            globalFunctions.previewCRReport(encodeURI(text));
        }
        else {
            pageLoaderFrame();
            $('#LoadReportDiv').load(encodeURI(text), function () {
                hideLoaderFrame();
            });
            if (pageId == "monthlyattendancesummary")
                $('#LoadReportDiv').addClass("Scroll-MonthlyReport");

            if (pageId == "workingdays") {
                if ($('#chkDetailReports').is(':checked'))
                    $('#LoadReportDiv').addClass("width-1500");
                else
                    $('#LoadReportDiv').removeClass("width-1500");
            }
        }

    };



    return {
        loadAttendanceReport: loadAttendanceReport,
        BindData: BindData
    };
}();

$(function () {
    $('#btnPreviewAttendanceReport').click(function () {
        AttendanceReport.loadAttendanceReport();
    });

    $('#ddlDepartmentList').change(function () {
        var deptID = $(this).val();
        var params = {};
        if (parseInt(deptID) > 0) {
            params = {
                DeptId: (parseInt(deptID) > 0 ? deptID : 0)
            };
        } else {

        }
        $.ajax({
            type: 'POST',
            url: '/Reporting/FilterSection',
            data: params,
            success: function (result) {
                AttendanceReport.BindData('#ddlDepartmentList', '#ddlSectionList', result);
            },
            error: function (msg) {
            }
        });

    });
});

function BindEmployee() {
    var params = {};
    var superVisorId = 0;
    var isActive = true;
    var employeeStatusChk = $(".employeeStatusChk:checked");

    if ($("#ddlSuperVisorList").length > 0) {
        if (parseInt($("#ddlSuperVisorList").val()) > 0) {
            superVisorId = $("#ddlSuperVisorList").val()
        }
    }

    if ($(employeeStatusChk).length > 0) {
        if ($(employeeStatusChk).attr("datatype") == "all") {
            isActive = null
        } else {
            isActive = false
        }
    }

    $.ajax({
        type: 'POST',
        url: '/Reporting/FilterEmployee',
        data: params = {
            isActive: isActive,
            superVisiorId: superVisorId
        },
        success: function (result) {
            AttendanceReport.BindData('#ddlEmployeeList', '#ddlEmployeeList', result);
        },
        error: function (msg) {
        }
    });
}

$('#chkListReport').click(function () {
    var isSelected = $('#chkListReport').prop('checked');
    if (isSelected) {
        DisabledSelect('#ddlEmployeeList', true);
        DisabledSelect('#ddlDepartmentList', true);
        DisabledSelect('#ddlSectionList', true);
        DisabledSelect('#ddlShiftList', true);
        $("#EmployeRecordByPage").removeAttr("disabled");
        $("#chkDailyAttTemplate1").removeAttr("disabled");
    }
    else {
        DisabledSelect('#ddlEmployeeList', false);
        DisabledSelect('#ddlDepartmentList', false);
        DisabledSelect('#ddlSectionList', false);
        DisabledSelect('#ddlShiftList', false);
        $("#EmployeRecordByPage").attr("disabled", "disabled");
        $("#chkDailyAttTemplate1").attr("disabled", "disabled");
        $("#EmployeRecordByPage").prop('checked', false);
        $("#chkDailyAttTemplate1").prop('checked', false);
    }
})



