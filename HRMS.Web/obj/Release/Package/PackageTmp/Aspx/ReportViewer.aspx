﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="HRMS.Web.Aspx.ReportViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-color:#fff;">
    <form id="form1" runat="server">
        <div>
            <asp:Panel EnableTheming="false" runat="server" ID="pnl">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <CR:CrystalReportViewer ID="CrViewer" runat="server"
                    AutoDataBind="True" ReportSourceID="CrystalReportSource1"
                    PrintMode="ActiveX" OnLoad="CrViewer_Load" OnUnload="CrViewer_Unload" />
                <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                </CR:CrystalReportSource>
            </asp:Panel>
        </div>
    </form>
</body>
 <script lang="JavaScript">
     let stateCheck = setInterval(() => {
         if (document.readyState === 'complete') {
             clearInterval(stateCheck);
             var pageLoading = window.parent.document.getElementsByClassName('page-loading-frame')
             if (pageLoading.length > 0) {
                 var pageLoading = pageLoading[0]
                 pageLoading.classList.add('remove')

                 setTimeout(function () {
                     pageLoading.remove();
                 }, 300);
             }
         }
     }, 100);
 </script>
</html>
