﻿
function InitDevelopmentPlanList() {
    loadDevelopmentPlanGrid();
    $('#btnSearch').on('click', function () {
        loadDevelopmentPlanGrid();
    });
}


function InitMyDevelopmentPlanList() {
    debugger;
    loadMyDevelopmentPlans();
}


function loadMyDevelopmentPlans() {
    oTableChannel = $('#tblDevelopmentPlanGrid').dataTable({
        "sAjaxSource": "/DevelopmentPlan/GetMyDevelopmentPlans",
        "fnServerParams": function (aoData) {
        },

        "aoColumns": [
            { "mData": "CompanyName", "sTitle": "Organization Name" },
            { "mData": "DepartmentName", "sTitle": "Department" },
            { "mData": "EmployeeAlternativeID", "sTitle": "Emp ID" },
            { "mData": "EmployeeName", "sTitle": "Employee Name" },
            { "mData": "ActiveDevelopmentPlan", "sTitle": "Active Development Plan" },
            { "mData": "DevelopmentPlanHistory", "sTitle": "Development Plan History" },
            { "mData": "PrintActivePlan", "sTitle": "Print Active Plan" }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function loadDevelopmentPlanGrid() {
    var CompanyId = $("#ddlCompany").val();
    var DepartmentId = $("#ddlDepartment").val();
    oTableChannel = $('#tblDevelopmentPlanGrid').dataTable({
        "sAjaxSource": "/DevelopmentPlan/GetEmployeeDevelopmentPlans",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "CompanyId", "value": CompanyId },
                { "name": "DepartmentId", "value": DepartmentId }
            );
        },

        "aoColumns": [
            { "mData": "CompanyName", "sTitle": "Organization Name" },
            { "mData": "DepartmentName", "sTitle": "Department" },
            { "mData": "EmployeeAlternativeID", "sTitle": "Emp ID" },
            { "mData": "EmployeeName", "sTitle": "Employee Name" },
            { "mData": "CreateDevelopmentPlan", "sTitle": "Create Development Plan" },
            { "mData": "ActiveDevelopmentPlan", "sTitle": "Active Development Plan" },
            { "mData": "DevelopmentPlanHistory", "sTitle": "Development Plan History" },
            { "mData": "PrintActivePlan", "sTitle": "Print Active Plan" }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}
function ViewDevelopmentPlanDetail(DevelopmentGoalId, CompanyId, EmployeeId, EmployeeAlternativeID,IsMyDevelopmentPlanScreen)
{
    DevelopmentPlanDetail(DevelopmentGoalId, CompanyId, EmployeeId, EmployeeAlternativeID);
}
function DevelopmentPlanDetail(DevelopmentGoalId, CompanyId, EmployeeId, EmployeeAlternativeID) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data:
        {
            DevelopmentGoalID: DevelopmentGoalId,
            EmployeeID: EmployeeId,
            CompanyID: CompanyId
        },
        url: '/DevelopmentPlan/SetSelectedDevelopmentPlanDetailInSession',
        success: function (data) {
            if (data.success === true) {
                //window.open("/DevelopmentPlan/DevelopmentGoalForm");
                location.href = "/DevelopmentPlan/DevelopmentGoalForm";
            }
            else {
                location.href = "/DevelopmentPlan/Index";
            }
        },
        error: function (data) {
            //  ShowMessage("error", "Some technical error occurred");
        }
    });
}
function BindDepartments(object) {
    var selectedItem = $(object).val();
    if (selectedItem == undefined || selectedItem == "")
        selectedItem = 0;
    selectedItem = parseInt(selectedItem);
    $.ajax({
        cache: false,
        type: "GET",
        url: "/DevelopmentPlan/GetDepartments",
        data: { "companyId": selectedItem },
        success: function (data) {
            $("#ddlDepartment").html('');
            $("#ddlDepartment").append($('<option></option>').val("0").html("Select Department"));
            $.each(data, function (id, option) {
                $("#ddlDepartment").append($('<option></option>').val(option.id).html(option.name));
            });
            RefreshSelect("#ddlDepartment");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //  alert('Failed to retrieve states.');
        }
    });

}

function GetDevelopmentGoalFormData(CategoryID, SectionID, SectionName, SelectedKPIORCompetency, AnnualAppraisalID, JobDesignationCompetencyID, HaveLoadedFromCategoryPage) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: {
            CategoryID: CategoryID, SectionID: SectionID, SectionName: SectionName, SelectedKPIORCompetency: SelectedKPIORCompetency,
            AnnualAppraisalID: AnnualAppraisalID, JobDesignationCompetencyID: JobDesignationCompetencyID, HaveLoadedFromCategoryPage: HaveLoadedFromCategoryPage
        },
        url: '/DevelopmentPlan/SetSelectedDevelopmentPlanDetailInSession',
        success: function (data) {
            if (data.success === true) {
                $("#ddlCategories").val("");
                //window.open("/DevelopmentPlan/DevelopmentGoalForm");
                location.href = "/DevelopmentPlan/DevelopmentGoalForm";
            }
            else {
                location.href = "/DevelopmentPlan/Index";
            }
        },
        error: function (data) {
            //  ShowMessage("error", "Some technical error occurred");
        }
    });
}

function ViewMyAllDevelopmentPlans(CompanyId, EmployeeId, AlternateEmployeeId, EmployeeName, DevelopmentPlanType)
{
    ViewAllDevelopmentPlansByEmployeeId(CompanyId, EmployeeId, AlternateEmployeeId, EmployeeName, DevelopmentPlanType, true);
}

function ViewAllDevelopmentPlans(CompanyId, EmployeeId, AlternateEmployeeId, EmployeeName, DevelopmentPlanType) {
    ViewAllDevelopmentPlansByEmployeeId(CompanyId, EmployeeId, AlternateEmployeeId, EmployeeName, DevelopmentPlanType, true);
}


function ViewAllDevelopmentPlansByEmployeeId(CompanyId, EmployeeId, AlternateEmployeeId, EmployeeName, DevelopmentPlanType, IsMyDevelopmentPlanScreen) {
    //InstallmentPayDeductionId = PayDeductionId;
    $("#myModal3").modal("show");
    $(".modal-dialog").attr("style", "width:750px;");
    $(".close").removeClass('hidden');
    var $html = $($("#divDevelopmentPlans").clone());
    $html.removeAttr("style");
    $("#modal_Loader3").html($html.get(0).outerHTML);
    $("#modal_heading3").html("<i class='fa fa-user'></i>&nbsp;Development Plans for " + "(" + AlternateEmployeeId + ") " + EmployeeName);
    $("#myModal3").find("#divDevelopmentPlans").removeAttr("id");
    $("#myModal3").find("#tblAllDevelopmentPlans").attr("id", "DevelopmentPlansGrid");
    getEmployeeDevelopmentPlans(CompanyId, EmployeeId, DevelopmentPlanType, IsMyDevelopmentPlanScreen);
}
function getEmployeeDevelopmentPlans(CompanyId, EmployeeId, DevelopmentPlanType, IsMyDevelopmentPlanScreen) {
    //tblAllDevelopmentPlans 
    oTableChannel = $("#DevelopmentPlansGrid").dataTable({
        "sAjaxSource": IsMyDevelopmentPlanScreen ? "/DevelopmentPlan/GetMyAllDevelopmentPlans" : "/DevelopmentPlan/GetAllDevelopmentPlans",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "CompanyId", "value": CompanyId },
                { "name": "EmployeeId", "value": EmployeeId },
                { "name": "DevelopmentPlanType", "value": DevelopmentPlanType },
            );
        },
        "aoColumns": [
            { "mData": "ID", "sTitle": "ID", "bVisible": false, "width": "6%", "className": "center-text-align" },
            { "mData": "GoalName", "sTitle": "Goal Name", "width": "30%" },
            { "mData": "Category", "sTitle": "Category", "width": "10%" },
            { "mData": "RequestNumber", "sTitle": "Request No.", "width": "10%" },
            { "mData": "LearningCategory", "sTitle": "Learning Category", "width": "10%", "className": "center-text-align" },
            //{ "mData": "EmployeeId", "sTitle": "EmployeeId", "width": "10%" },
            //{ "mData": "EmployeeName", "sTitle": "EmployeeName", "sWidth": "10%" },
            { "mData": "Status", "sTitle": "Status", "width": "10%" },
            { "mData": "Action", "sTitle": "Action", "width": "24%", "bSortable": false, "className": "center-text-align" },

        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });

}

function CreateDevelopmentGoal(DevelopmentGoalID, EmployeeID, AlternateEmployeeId, CompanyID, DepartmentID, PositionID) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { DevelopmentGoalID: DevelopmentGoalID, EmployeeID: EmployeeID, CompanyID: CompanyID, DepartmentID: DepartmentID, PositionID: PositionID },
        url: '/DevelopmentPlan/SetSelectedDevelopmentPlanDetailInSession',
        success: function (data) {
            if (data.success === true) {
                location.href = "/DevelopmentPlan/DevelopmentGoalPDRP";
                // window.open("/DevelopmentPlan/DevelopmentGoalPDRP");
            }
            else {
                location.href = "/DevelopmentPlan/Index";
            }
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
function GetGoalForm() {
    if ($("#ddlCategories").val() === "1") {
        LoadJobCompetencyGrid(1);
        $("#nonTeachingDiv").addClass("hide");
        $("#teachingDiv").addClass("hide");
        $("#jobCompetencieDiv").removeClass("hide");
    }
    else if ($("#ddlCategories").val() === "2") {
        LoadPDRPGrid(2);
        $("#nonTeachingDiv").removeClass("hide");
        $("#teachingDiv").addClass("hide");
        $("#jobCompetencieDiv").addClass("hide");
    }
    else if ($("#ddlCategories").val() === "3" || $("#ddlCategories").val() == "4") {
        GetDevelopmentGoalFormData(parseInt($("#ddlCategories").val()), 0, "", "");
    }

}

function LoadJobCompetencyGrid(CategoryID) {
    oTableChannel = $('#tblJobCompetenciesFormC').dataTable({
        "sAjaxSource": "/DevelopmentPlan/GetJobCompetency",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "CategoryID", "value": CategoryID },
            );
        },

        "aoColumns": [
            { "mData": "SrNo", "sTitle": "Sr No", className: "col-md-1" },
            { "mData": "Competency", "sTitle": "Job Competencies", className: "col-md-10" },
            //{ "mData": "JobDescription", "sTitle": "", className: "col-md-1"  },
            { "mData": "Add", "sTitle": "Action", className: "col-md-1" }

        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        //"aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": false,
        //"scrollX": true,
        "bSortCellsTop": false,
        "bPaginate": false,
        "ordering": false,
        "fnDrawCallback": function () {
        },
        rowCallback: function (row, data) {
            $(row).addClass('clsProfessionalCompetencies');
        }
    });
}

function LoadTeachingGrid(CategoryID) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { CategoryID: CategoryID },
        url: '/DevelopmentPlan/LoadTeachingGrid',
        success: function (data) {

            $("#teachingDiv").html(data.value1);

        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}

function LoadPDRPGrid(CategoryID) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { CategoryID: CategoryID },
        url: '/DevelopmentPlan/LoadPDRPGrid',
        success: function (data) {

            $("#nonTeachingDiv").html(data.value1);

        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}


function BrowseFile($this) {
    var file = $this.files[0];
    fileName = file.name;
    size = file.size;
    type = file.type;
    var currentRow = $this.closest("tr");
    var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
    $(currentRow).find("#txtuploadedMsgAdd_Attachment").css("color", "red");
    $(currentRow).find('#attachmentError' + 'File').addClass('hide');
    if ($.inArray(fileNameExt.toLowerCase(), validExtensions) !== -1) {
        if (size <= 2097152) {
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("");
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").hide();
            $(currentRow).find("#divBrowse_Attachment").hide();
            $(currentRow).find("#btnRemove_Attachment").show();
            $(currentRow).find("#file_caption_name_Attachment").show();
            $(currentRow).find("#file_caption_id_Attachment").html(fileName);

            $(currentRow).find("#chkFileUpload_Attachment").val("2");
            var fileJD = URL.createObjectURL(event.target.files[0]);
            $(currentRow).find('#divPreview_Attachment').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');
        }
        else {
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("Maximum 2MB file size is allowed to upload.");
            $(currentRow).find("#txtuploadedMsgAdd_Attachment").show();
            $(currentRow).find("#divAddInforamtionDialog_Attachment").hide();
            $(currentRow).find("#divPreview_Attachment").empty();
        }
    }
    else {
        $(currentRow).find("#txtuploadedMsgAdd_Attachment").text("Only JPG, JPEG, PNG, GIF, PDF, XLS, XLSX, PPT, PPTX, DOC and DOCX extension files are allowed to upload.");
        $(currentRow).find("#txtuploadedMsgAdd_Attachment").show();
        $(currentRow).find("#divAddInforamtionDialog_Attachment").hide();
    }
}
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];
function InitCreateDevelopmentPlanGoalForm() {

    $("#developmentGoalSection").InitializeValidationEvents();

    $("#btnSaveDevelopmentGoal, #btnSavebtnDevelopmentGoal").on('click', function () {
        SaveDevelopmentGoal();
    });
    $("[name='file']").change(function () {
        BrowseFile(this);
    });
    $('#StartDate').on("change", function () {
        if ($(this).val() !== $('#StartDate').attr("data-oldvalue")) {
            var date = new Date(new Date($(this).val()));
            date.setMonth(date.getMonth() + 2);
            date.setDate(date.getDate() - 1);
            $('#CompletionDate').val((date.getDate() + "/" + monthNames[date.getMonth()] + "/" + date.getFullYear()));
            $('#StartDate').attr("data-oldvalue", $('#StartDate').val());
        }
    });

    $('.datepickerc').datepicker({
        dateFormat: HRMSDateFormat,
        format: HRMSDateFormat,
        todayHighlight: true
    });

    $('.required-field').removeClass('hide');

    if ($.trim($("#kpicompetency").val()) === "") {
        $("#kpicompetency").removeAttr("readonly");
    }
    //$('#attachmentDiv [data-id="attachmentblock"]').each(function () {
    //    if ($(this).find('input#fu_Attachment').val().length > 0) {
    //        isAttachmentValid = false;
    //        return false;
    //    }
    //    if ($(this).find('textarea#note').val().length === 0) {
    //        isAttachmentValid = false;
    //        return false;
    //    }
    //});
}

function SaveDevelopmentGoal() {
    var developmentGoalAttachments = new Array();
    var goalAttachment = new Object();
    // validating attachments
    var attachmentErrors = new Array();
    var $form = $("#frmDevelopmentGoalFromSave");
    var formData = new FormData($form[0]);
    var goalActions = new Array();
    var actionDescription = new Object();
    $('#developmentgoalactions [data-id="attachmentblock"][data-isSampleView="false"] tr[data-rowtype="attachmentdatarow"]').each(function (index, attachmentRow) {
        actionDescription = new Object();
        if (index === 0) {
            if ($(this).find('textarea#actionDescription').val() === "") {
                attachmentErrors.push({ "IsError": true, "ErrorType": "note" });
                $(this).find('#actionDescriptionError').removeClass('hide');
            } else {
                $(this).find('#actionDescriptionError').addClass('hide');
            }
        }
        actionDescription.ID = parseInt($(this).attr("data-actionid"));
        actionDescription.DevelopmentGoalID = parseInt($(this).attr("data-developmentgoalid"));
        actionDescription.ActionStepDescription = $(this).find('textarea#actionDescription').val();
        if ($.trim($(this).find('textarea#actionDescription').val().length) > 0) {
            goalActions.push(actionDescription);
        }
    });
    $('#attachmentDiv [data-id="attachmentblock"][data-isSampleView="false"] tr[data-rowtype="attachmentdatarow"]').each(function (index, attachmentRow) {
        if (index === 0) {
            var attachment = $(this).find('input#fu_Attachment');
            var attachmentNote = $(this).find('textarea#note');
            if (($(attachment).val().length === 0 && parseInt($(this).attr("data-fileid")) === 0) ||
                (parseInt($(this).attr("data-fileid")) > 0 && parseInt($(this).find("#hdnDeletedFileUpload_Attachment").val()) > 0 && $(this).find('input#fu_Attachment').val().length === 0)) {
                attachmentErrors.push({ "IsError": true, "ErrorType": "file" });
                $(this).find('#attachmentError' + 'File').removeClass('hide');
            } else {
                $(this).find('#attachmentError' + 'File').addClass('hide');
            }
            if ($(attachmentNote).val().length === 0) {
                attachmentErrors.push({ "IsError": true, "ErrorType": "note" });
                $(this).find('#attachmentError' + 'Note').removeClass('hide');
            } else {
                $(this).find('#attachmentError' + 'Note').addClass('hide');
            }
        }
    });
    $.validator.unobtrusive.parse($form);
    $form.validate();
    var isFormValid = $form.valid();

    if (attachmentErrors.length > 0)
        return false;

    if (isFormValid === true && attachmentErrors.length === 0) {

        var unModifiedFileIds = new Array();
        var deletedFiles = new Array();
        $('#attachmentDiv [data-id="attachmentblock"][data-isSampleView="false"] tr[data-rowtype="attachmentdatarow"]').each(function () {
            goalAttachment = new Object();
            var attachment = $(this).find('input#fu_Attachment');
            goalAttachment.FileID = parseInt($(this).attr("data-fileid"));
            goalAttachment.ID = $(this).attr("data-goalnotedetailid");
            goalAttachment.RowID = parseInt($(this).attr("data-rowid"));
            goalAttachment.Notes = $(this).find('textarea#note').val();
            if ($(attachment).val().length > 0) {
                var fileName = attachment.get(0).files[0].name + "@@@" + parseInt($(this).attr("data-fileid"));
                formData.append("FilesArray_" + parseInt($(this).attr("data-rowid")), $(this).find('input#fu_Attachment').get(0).files[0], fileName);
            }
            else if (parseInt($(this).attr("data-fileid")) > 0 && parseInt($(this).find("#hdnDeletedFileUpload_Attachment").val()) === 0) {
                unModifiedFileIds.push(parseInt($(this).attr("data-fileid")));
            }
            developmentGoalAttachments.push(goalAttachment);
        });
        formData.append("actionSteps", JSON.stringify(goalActions));
        formData.append("UnModifiedFiles", unModifiedFileIds);
        formData.append("NoteDetails", JSON.stringify(developmentGoalAttachments));
        formData.delete("file");
        formid = 49;
        //var companyId = $("#ddlCompany").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            url: '/developmentplan/saveform',
            data: formData,
            success: function (result) {
                hideLoaderFrame();
                if (result.InsertedRowId > 0)
                    //Save Form
                    location.reload();

                else {
                    ShowMessage(result.CssClass, result.Message);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
}

function Remove(obj) {
    $(obj).parents("[data-id ='attachmentblock']").remove();
}

function AddMoreActionSteps() {
    var $attachmentDiv = $('#developmentGoalActionsSampleDiv [data-isSampleView]').clone();
    $attachmentDiv.attr("data-isSampleView", false);
    $attachmentDiv.removeClass("hide");
    $attachmentDiv.find('[data-rowtype="attachmentdatarow"]').attr("id", "rowId_" + parseInt($("#attachmentDiv").attr("data-totalrecrods")) + 1);
    $attachmentDiv.find('[data-rowtype="attachmentdatarow"]').attr("data-rowid", parseInt($("#attachmentDiv").attr("data-totalrecrods")) + 1);
    $("#developmentgoalactions").attr("data-totalrecrods", parseInt($("#developmentgoalactions").attr("data-totalrecrods")) + 1);
    $("#developmentgoalactions").append($attachmentDiv.get(0).outerHTML);
}
function AddMore() {
    var $attachmentDiv = $('#attachmentSampleDiv [data-isSampleView]').clone();
    $attachmentDiv.attr("data-isSampleView", false);
    $attachmentDiv.removeClass("hide");
    $attachmentDiv.find('[data-rowtype="attachmentdatarow"]').attr("id", "rowId_" + parseInt($("#attachmentDiv").attr("data-totalrecrods")) + 1);
    $attachmentDiv.find('[data-rowtype="attachmentdatarow"]').attr("data-rowid", parseInt($("#attachmentDiv").attr("data-totalrecrods")) + 1);
    $("#attachmentDiv").attr("data-totalrecrods", parseInt($("#attachmentDiv").attr("data-totalrecrods")) + 1);
    $("#attachmentDiv").append($attachmentDiv.get(0).outerHTML);
    $("#attachmentDiv").find("input[type='file']").each(function () {
        $(this).detach("change");
        $(this).change(function () {
            BrowseFile(this);
        });
    });
}
function RemoveDocument(ctrl, fileidfieldname, $this) {
    var $parent = $($this).parents('div[data-id="attachmentblock"]');

    var fileid = $($parent).find("#hdnFileUpload_" + ctrl).val();
    $($parent).find("#btnRemove_" + ctrl).hide();
    $($parent).find("#divBrowse_" + ctrl).show();
    $($parent).find("#file_caption_name_" + ctrl).html('');
    $($parent).find("#file_caption_name_" + ctrl).hide();
    $($parent).find("#file_caption_id_" + ctrl).html('No file choosen');
    $($parent).find("#txtuploadedMsgAdd_" + ctrl).text("");
    $($parent).find("#fu_" + ctrl).val("");
    $($parent).find("#divPreview_" + ctrl).html("");
    $($parent).find("#hdnDeletedFileUpload_" + ctrl).val(fileid);
}

(function ($) {
    $.fn.GetNumericValue = function () {
        if ($.trim($(this).val()).length === 0 || isNaN($(this).val()) === true) {
            return 0;
        }
        return parseFloat($(this).val());
    };
    $.fn.AllowNumeric = function () {

        $(this).find("input[type='text'][data-ud]").each(function () {

            $(this).on("keydown", function (e) {
                var val = $(this).val();
                if (e.which === 9 || e.which === 13 || e.which === 46 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.which === 8 || e.which === 18 || (e.which >= 35 && e.which <= 40)) {
                    if ((e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105)) {
                        val = val + String.fromCharCode(e.which);
                    }
                    if (typeof $(this).data("callbackfn") !== typeof undefined && $.isFunction(eval($(this).data("callbackfn")))) {
                        window[$(this).data("callbackfn")]($(this));
                    }
                    return true;
                }
                else {
                    return false;
                }
            });
        });

        $(this).find("input[type='text'][data-d]").each(function () {
            $(this).on("keydown", function (e) {
                if (e.which === 9 || e.which === 13 || e.which === 46 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.which === 8 || e.which === 109 || e.which === 189 || e.which === 18 || (e.which >= 35 && e.which <= 40)) {
                    return true;
                }
                else {
                    return false;
                }
            });
        });
        $(this).find("input[type='text'][data-uf]").each(function () {
            $(this).on("keydown", function (e) {
                if (((e.which === 110 || e.which === 190) && ($(this).val().toString().indexOf(".") === -1)) || e.which === 9 || e.which === 13 || e.which === 46 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.which === 8 || e.which === 109 || e.which === 18 || (e.which >= 35 && e.which <= 40)) {
                    return true;
                }
                else {
                    return false;
                }
            });
        });
        $(this).find("input[type='text'][data-f]").each(function () {
            $(this).on("keydown", function (e) {
                if (((e.which === 110 || e.which === 190) && ($(this).val().toString().indexOf(".") === -1)) || e.which === 9 || e.which === 13 || e.which === 46 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.which === 8 || e.which === 109 || e.which === 189 || e.which === 18 || (e.which >= 35 && e.which <= 40)) {
                    return true;
                }
                else {
                    return false;
                }
            });
        });
    };
    $.fn.InitializeValidationEvents = function () {
        $(this).AllowNumeric();
    };
})(jQuery);