﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridMunicipality();
    }
});

//$("#btn_addMunicipality").click(function () { AddMunicipality(); });
var oTableChannel;

function getGridMunicipality() {
    oTableChannel = $('#tbl_MunicipalityList').dataTable({
        "sAjaxSource": "/Municipality/GetMunicipalityList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document Name/No",'width':'20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%' , "sType": "date-uk"},
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", 'width': '20%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Municipality/GetMunicipalityList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "order": [[0, "asc"]],
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_MunicipalityList");
        }
    });
}

function AddMunicipality() {
    //alert($("#hdnEmployee_ID").val());
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/Municipality/AddMunicipality");
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add Municipality');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditMunicipality(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/Municipality/EditMunicipality/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit Municipality');

}


function DeleteMunicipality(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Municipality/DeleteMunicipality',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridMunicipality();
            },
            error: function (data) {
                ShowMessage("error", data.Message);
            }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewMunicipality(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Municipality/ViewMunicipality/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View Municipality Details');
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}