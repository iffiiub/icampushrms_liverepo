﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridHealth();
    }
});

var oTableChannel;

function getGridHealth() {

    oTableChannel = $('#tbl_HealthList').dataTable({
       
        "sAjaxSource": "/Health/GetHealthList",
        "aoColumns": [           
            { "mData": "DocumentNo", "sTitle": "Document Name/No", 'width': '20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%' },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%' },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", 'width': '20%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Health/GetHealthList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true
    });
}

function AddHealth() {


    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/Health/AddHealth");
                $("#myModal3").modal({backdrop: 'static'});
                $("#modal_heading3").text('Add Health');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditHealth(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/Health/EditHealth/" + id);
    $("#myModal3").modal({backdrop: 'static'});
    $("#modal_heading3").text('Edit Health');

}


function DeleteHealth(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Health/DeleteHealth',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridHealth();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewHealth(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Health/ViewHealth/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('View Health Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Health/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Health/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}