﻿$(document).ready(function () {
    bindCharsOnly('.charOnly');
    /*
    $('#AccountDate').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-50:+50',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        maxDate: new Date(),
        buttonImageText: "Calendar"
    });

    $('#CancellationDate').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-50:+50',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        maxDate: new Date(),
        buttonImageText: "Calendar"

    });
    */
});
function SaveBankDetails() {

    var errorCount = 0;
    var $form = $('form#frmBankInformation');
    if (!$form.valid()) {
        //Ajax call here
        errorCount++;
    } else {
        if ($("#UniAccountNumber").val().trim().length > 0) {
            checkUniBanAccountnumber(errorCount)
        } else {
            $("#UniAccountNumber").val("");
            submitBankInformation();
        }
    }
}

function checkUniBanAccountnumber(errorCount) {
    $.ajax({
        type: 'GET',
        url: encodeURI('/BankInformation/CheckUni_BanAccountNo?DirectDepositID=' + $("#DirectDepositID").val() + '&EmployeeeId=' + $("#EmployeeID").val() + "&UniAccountNumber=" + $("#UniAccountNumber").val()),
        dataType: "json",
        success: function (data) {

            if (data) {
                errorCount++;
                var field;
                $("#UniAccountNumber").siblings("span.field-validation-error:first").removeClass('hidden');
                if ($("#UniAccountNumber").next(".field-validation-valid").length > 0) {
                    field = $("#UniAccountNumber").next(".field-validation-valid");//span
                } else {
                    field = $("#UniAccountNumber").next(".field-validation-error");
                }
                field.attr("class", "field-validation-error");
                field.html("<span for='UniAccountNumber' class='text-danger'>UNI/IBAN Number already exists.</span>");
            } else {
                var IBanNumber = $("#UniAccountNumber").val();
                var IBanLength = $("#IBanLength").val();
                var InputLength = IBanNumber.length;
                if (IBanLength != 0) {
                    if (InputLength != IBanLength) {
                        errorCount++;
                        try {
                            $("#UniAccountNumber").siblings("span.field-validation-error:first").removeClass('hidden');
                        } catch (e) { }
                    }
                    var field;

                    if ($("#UniAccountNumber").next(".field-validation-valid").length > 0) {
                        field = $("#UniAccountNumber").next(".field-validation-valid");//span
                    } else {
                        field = $("#UniAccountNumber").next(".field-validation-error");
                    }
                    field.attr("class", "field-validation-error");
                    field.html("<span for='payDirectDepositModel_UniAccountNumber' class='text-danger'>UNI/IBAN Number must be " + IBanLength + " digits</span>");
                }
            }
            if (errorCount == 0) {
                submitBankInformation();
            }
        }
    });
}

function submitBankInformation() {
    $("#frmBankInformation").submit();
}

function cancelAddUpdateBankInfo() {
    $("#addBankInfo").show();
    // $("#bankInfoDiv").hide();
    $("#myModal").modal("hide");
}

function checkDefaultAccount111(e) {
    if ($(e).is(':checked'))
        $.ajax({
            type: 'GET',
            url: '/BankInformation/CheckDefaultAccount?EmployeeeId=' + $("#EmployeeID").val(),
            dataType: "json",
            success: function (data) {
                if (data.isPresent) {
                    $("#lblForDdefaultAccount").removeClass('hide');
                    //$("#lblForDdefaultAccount").text("After save current detail the existing bank accounts will updated to not default accounts.");
                } else {
                    $("#lblForDdefaultAccount").addClass('hide');
                }
            }
        });
    else {
        $("#lblForDdefaultAccount").addClass('hide');
    }
}