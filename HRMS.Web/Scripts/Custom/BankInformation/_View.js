﻿
function EditAccount(directpaydepositID) {
    var EmployeeId = $("#hdnEmployee").val();        
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { "id": directpaydepositID },
        url: '/BankInformation/CheckBankDetailIsAssigned',
        success: function (data) {          
            if (data.BankDetailCount > 0) {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Current bank detail is already used for employee salary, after update record it will show updated detail in payslip. Do you want to proceed?" }).done(function () {
                    $("#modal_Loader").load("/BankInformation/Edit?id=" + directpaydepositID + "&EmployeeID=" + EmployeeId, function () {
                        $("#myModal").modal("show");
                        $("#modal_heading").text('Edit Employee Bank Details');
                    });
                });
            }
            else {
                $("#modal_Loader").load("/BankInformation/Edit?id=" + directpaydepositID + "&EmployeeID=" + EmployeeId, function () {
                    $("#myModal").modal("show");
                    $("#modal_heading").text('Edit Employee Bank Details');
                });
            }
        },
        error: function (data) { }
    });
}


function DeleteAccount(directpaydepositID) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { "id": directpaydepositID },
        url: '/BankInformation/CheckBankDetailIsAssigned',
        success: function (data) {
            //alert(data.BankDetailCount);
            if (data.BankDetailCount>0) {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Current bank detail is already used for employee salary, after deleted record it will not show in payslip. Do you want to proceed?" }).done(function () {
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        data: { "id": directpaydepositID },
                        url: '/BankInformation/Delete',
                        success: function (data) {
                            ShowMessage(data.result, data.resultMessage);
                            var EmployeeId = $("#hdnEmployee").val();
                            $("#EmpBankDetails").html("");
                            $("#EmpBankDetails").load("/BankInformation/ViewDirectDepositDetails/" + EmployeeId);
                            //getGrid();
                        },
                        error: function (data) { }
                    });
                });
            }
            else {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        data: { "id": directpaydepositID },
                        url: '/BankInformation/Delete',
                        success: function (data) {
                            ShowMessage(data.result, data.resultMessage);
                            var EmployeeId = $("#hdnEmployee").val();
                            $("#EmpBankDetails").html("");
                            $("#EmpBankDetails").load("/BankInformation/ViewDirectDepositDetails/" + EmployeeId);
                            //getGrid();
                        },
                        error: function (data) { }
                    });
                });
            }
        },
        error: function (data) { }
    });    
}