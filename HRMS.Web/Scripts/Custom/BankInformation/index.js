﻿$(document).ready(function () {

    if ($("#hdnEmployee").val() != "" && $("#hdnEmployee").val() !== undefined) {
        var id = $("#hdnEmployee").val();
        LoadEmployeeDetails(id);
        $("#EmpBankDetails").html("");
        $("#EmpBankDetails").load("/BankInformation/ViewDirectDepositDetails/" + id);
        //  $("#ActiveEmployee").val(id);
    }
});

function onSuccess(arg) {
    ShowMessage(arg.result, arg.resultMessage);

    //if (arg.result.indexOf("success") > -1) {
    //    $("#bankInfoDiv").show();
    //}
    //else {
    //$("#bankInfoDiv").html();
    //$("#bankInfoDiv").hide();
    $("#myModal3").modal("hide");
    $('.modal').modal('hide');
    //  $("#EmpBankDetails").html("");
    var EmployeeId = $('#hdnEmployee').val();
    $("#EmpBankDetails").load("/BankInformation/ViewDirectDepositDetails/" + EmployeeId);
    //$("#addBankInfo").show();
    //}
}

function Add(title, id, dropdownId) {
    $("div.tooltip").hide();

    $("#hdnDropDownReloadId").val(dropdownId);
    $("#hdnDropDownId").val(id);
    $("#modal_Loader").load("/Employee/AddType/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text(title);
}


function loadAddBankInfo(id) {
    //$("#bankInfoDiv").load("/BankInformation/Edit/"+id)
}




$("#SearchEmployee").blur(function () {

    var val = $('#SearchEmployee').val();
    if (val != "") {
        $.ajax({

            // The url to which the request is to be sent.
            url: "/BankInformation/EmployeeSearch",

            // The type of request. It can be "GET" or "POST" or others.
            type: "GET",

            // The data that is to be sent to the server. It can be omitted if no data is
            // to be sent. In a "GET" request this data is appended to the url.
            data: { "search": val },

            // The function to be called when the server returns some data. The data
            // variable holds the data that was returned.
            success: function (data) {
                //if(data != null)
                //{
                var text = '';
                //$('#ULEmployeeNotInDepartment').html("");
                $(".list-group-item").remove();
                $.each(data, function (index, itemData) {

                    var Name = itemData.FirstName_1 + ' ' + itemData.SurName_1;
                    text += "<a href='javascript:void(0)' id=" + itemData.EmployeeID + " onclick='getBankInfo(" + itemData.EmployeeID + ")' class='list-group-item'>" + Name + "</a>";
                    //text += "<li class='liemployeenotindepartment'>";
                    //text += "<input type='checkbox' class='chkEmployee'  value='" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "' id='" + itemData.EmployeeID + "' />";
                    //text += "<label for='" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "'>" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "</label></li>";

                });

                $('#AllEmployees').append(text);
                //}


            },

            // Show an alert if an error occurs.
            error: function () {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: "Some error occured.",
                });
            }
        });
    }

});




function getBankInfo(employeeId) {
    //$(".list-group-item").removeClass("active");
    //$('#' + employeeId).addClass("active");     
    $("#EmpBankDetails").html("");
    $("#EmpBankDetails").load("/BankInformation/ViewDirectDepositDetails/" + employeeId);
    //$("#bankInfoDiv").load("/BankInformation/Edit?id=" + 0 + "&EmployeeID=" + employeeId);
}


function LoadAddUpdateBankInfo() {

    if (parseInt($("#hdnEmployee").val()) > 0 && $("#hdnEmployee").val() !== undefined) {
        var EmployeeId = $("#hdnEmployee").val();
        $("#modal_Loader3").load("/BankInformation/Edit?id=" + 0 + "&EmployeeID=" + EmployeeId, function () {
            $("#myModal3").modal("show");
            $("#modal_heading3").text('Add Employee Bank Details');
           bindCharsOnly(".charOnly");
        });
    }
    else {
        ShowMessage("error", "Please select employee.")
    }
}