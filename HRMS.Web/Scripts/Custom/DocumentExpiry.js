﻿var oTableChannel;
$(document).ready(function () {
    if (filterMode == "1")
        $('#btnAbouttoExpire').prop('checked', true);
    if (filterMode == "2")
        $('#btnAlreadyExpire').prop('checked', true);

    $('#btnSearchDocument').click(function () {
        var doctypeId = $("#DocmentTypeList").val();
        var days = "0";
        var mode = "0";
        if ($('#btnAlreadyExpire').is(':checked')) {
            mode = "2";
            days = parseInt($("#txtExpired").val() > 0 ? $("#txtExpired").val() : "0");
        }
        if ($('#btnAbouttoExpire').is(':checked')) {
            mode = "1";
            days = parseInt($("#txtAboutExpired").val() > 0 ? $("#txtAboutExpired").val() : "0");
        }
        var isPrimary = $("#chkisPrimary").is(":checked");
        if (mode != "0") {
            getDocumentList(doctypeId, days, mode,isPrimary);
        } else
            ShowMessage("warning", "Please select radion button");
    });

    //if (filterMode != "")
    $('#btnSearchDocument').click();
});

function getDocumentList(doctypeId, days, mode, isPrimary) {
    var url = "/Document/GetDocumentList?docTyprid=" + doctypeId + "&mode=" + mode + "&days=" + days + "&isPrimary=" + isPrimary;
    oTableChannel = $('#tbl_DocumentList').dataTable({
        "sAjaxSource": url,
        "aoColumns": [
            { "mData": "employeeId", "sTitle": "Emp ID", "sWidth": "5%", "sType": "Int" },
            { "mData": "employeename", "sTitle": "Employee Name", "sWidth": "25%" },
            { "mData": "documentNo", "sTitle": "Document Number", "sWidth": "13%" },
            { "mData": "doctype", "sTitle": "Document Type", "sWidth": "13%" },
            { "mData": "issueDate", "sTitle": "Issue Date", "sType": "date-uk", "sWidth": "10%" },
            { "mData": "expiryDate", "sTitle": "Expiry Date", "sType": "date-uk", "sWidth": "10%" },
            { "mData": "issueCountry", "sTitle": "Issue Country", "sWidth": "12%" },
            { "mData": "issuePlace", "sTitle": "Issue Place", "sWidth": "12%" }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": url,
        "bDestroy": true,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bInfo": true,
        "aaSorting": [[1, 'asc']],
        "fnDrawCallback": function () {
        },
        "bSortCellsTop": true
    });
}

