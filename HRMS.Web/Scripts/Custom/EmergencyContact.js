﻿$(document).ready(function () {

    getGrid($('#hiddenEmployeeId').val());

    $("#btn_add").click(function () {
        AddEditEmergencyContact(0);
    });

    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    $("#btn_BackProfile").click(function () { window.location.href = "/ViewEmployeeProfile/Index/" + $('#hiddenEmployeeId').val(); });
});

var oTableChannel;
function getGrid(employeeId) {

    oTableChannel = $('#tbl_EmergencyContactList').dataTable({
        "sAjaxSource": "/EmergencyContact/GetEmergencyContactList",
        "aoColumns": [
            { "mData": "ContactName", "sTitle": "Contact Name" },
            { "mData": "RelationName", "sTitle": "Relation" },
            { "mData": "WorkPhone", "sTitle": "Phone (Work)" },
            { "mData": "ResPhone", "sTitle": "Phone (Res)" },
            { "mData": "Mobile", "sTitle": "Mobile" },
            { "mData": "Actions", "sTitle": "Actions" }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/EmergencyContact/GetEmergencyContactList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "paging": true,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": employeeId });
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_EmergencyContactList");
            $("#tbl_EmergencyContactList_length").css("width", "100%");
            var divContent = getExportIcon("1", "ExportToExcel()", "ExportToPdf()");
            $("#tbl_EmergencyContactList_length").find(".pull-right").remove();
            $("#tbl_EmergencyContactList_length").append(divContent);
        }
    });
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/EmergencyContact/ExportToExcel?EmployeeId=" + $("#hiddenEmployeeId").val());
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/EmergencyContact/ExportToPdf?EmployeeId=" + $("#hiddenEmployeeId").val());
    }
    else ShowMessage("warning", "No data for export!");
}

function AddEditEmergencyContact(contactId) {
    var href = "/EmergencyContact/Contact?EmployeeId=" + $('#hiddenEmployeeId').val() + "&RefId=" + contactId;
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader").load(href);
    $("#myModal").modal("show");
    if (parseInt(contactId) > 0)
        $("#modal_heading").text('Edit Emergency Contact');
    else
        $("#modal_heading").text('Add Emergency Contact');
}

function DeleteContact(id, empId) {

    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EmergencyContact/DeleteContact',
            success: function (data) {
                ShowMessage(data.Success == true ? "success" : "error", data.Message);
                getGrid($('#hiddenEmployeeId').val());
            },
            error: function (data) {
                ShowMessage("error", data.message);
            }
        });
    });
}

