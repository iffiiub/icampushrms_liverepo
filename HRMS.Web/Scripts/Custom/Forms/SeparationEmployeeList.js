﻿$(document).ready(function () {

    intitSeparationList();
});

function intitSeparationList() {
    var SeparationDatatable = $('#tblSeparationList').DataTable(
        {
            columnDefs: [
                { width: "9%", targets: [0,2] },
                { width: "10%", targets: [3,4] },
                { width: "11%", targets: [5] },
                { width: "15%", targets: [1] }
            ],
            "bFilter": true,
            "bInfo": true,
            "bSortCellsTop": true,
            "bStateSave": false,
            "aaSorting": [[1, 'asc']]
        }
    );
}

$(document).on("click", "#btnSearch", function () {
    pageLoaderFrame();
    $.ajax({
        type: 'GET',
        url: '/SeparatedEmployeeList/GetAllSeparationRequests',
        data: {
            employeeId: $("#ddlEmployee").val(), separationTypeID: $("#ddlSeparationType").val()
        },
        success: function (result) {
            hideLoaderFrame();
            $("#EmployeeSeparationGrid").html(result);
            intitSeparationList();
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
});

function CreateEmployeeSeparationForm() {
    location.href = "/SeparationRequestForm/Create";
}

function ShowExitInterviewForm(urlstring, id, isCreate) {

    var url = '';
    var IsExistsMainNavigationPermission = IsMainNavigationPermission('/ExitInterviewRequest');
    if (IsExistsMainNavigationPermission) {

        url = '/SeparatedEmployeeList/SetExitInterviewFormProcessIDSesssion';

        $.ajax({
            //dataType: 'json',
            type: 'POST',
            url: url,
            data: { id: id },
            success: function (result) {
                if (result.id != null)
                    location.href = urlstring;

            },
            error: function (err) {
                // hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
}

function ShowClearanceForm(urlstring, id, isCreate) {

    var IsExistsMainNavigationPermission = IsMainNavigationPermission('/ClearanceRequest');
    var url = '';
    var IsExistsClearanceRequest = true;
    if (IsExistsMainNavigationPermission) {
        if (isCreate) {
            $.ajax({
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                url: '/ClearanceRequest/IsExistsClearanceRequest',
                data: { EmployeeID: id },
                async: false,
                success: function (result) {
                    if (result.Success == false) {
                        ShowMessage(result.CssClass, result.Message);
                        IsExistsClearanceRequest = false;
                    }
                },
                error: function (err) {
                    ShowMessage('error', err.statusText);
                }
            });

            if (!IsExistsClearanceRequest) {
                location.reload(true);
                return false;
            }
            else {
                url = '/SeparatedEmployeeList/SetClearanceEmployeeIDSesssion';
            }
        }
        else {
            url = '/SeparatedEmployeeList/SetClearanceInstanceIDSesssion';
        }
        $.ajax({
            //dataType: 'json',
            type: 'POST',
            url: url,
            data: { id: id },
            success: function (result) {
                if (result.id != null)
                    location.href = urlstring;
                //window.open(urlstring, '_blank');            
            },
            error: function (err) {
                // hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
}
