﻿$(document).ready(function () {
    $(document).on("keypress", ".decimalOnly", function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    
});
$(document).on('change', '#ddlCompany', function () {
    LoadDepartment('#ddlCompany');
    selectList = "";
    var count = 0;
    selectList = "<option value=\"\">Select Holiday</option>";
    var companyid = $(this).val() == "" ? 0 : $(this).val();
    if (companyid > 0) {
        pageLoaderFrame();
        $.ajax({
            type: 'get',
            url: '/PublicHolidayLeaveManager/GetHolidayListByCompany?companyID=' + companyid,
            datatype: 'Json',
            success: function (data) {
                $.each(data, function (key, value) {
                    count = count + 1;
                    selectList += '<option value=' + value.HolidayID + '>' + value.HolidayName + '</option>';
                });
               
                $("#ddlHoliday").html();
                $("#ddlHoliday").html(selectList);
                RefreshSelect("#ddlHoliday");

                if (count == 0) {
                    ShowMessage("error", "No holidays are added for selected Organization");
                }
                hideLoaderFrame();
            }

        });
    }
    else
    {
        ShowMessage("error", "Select an Organization");
    }
});
function InitGrid()
{
   // $('#tbl_PublicHolidayLeave').dataTable();

    $('#tbl_PublicHolidayLeave').dataTable(
       {
           columnDefs: [
               { width: "5%", targets: [0,2,3,4,5] }
               ,
                 { width: "10%", targets: [1] },
             { width: "12%", targets: [6] }

           ],
           fixedColumns: true,
           "order": [[1, "asc"]],
           bAutoWidth: false,
           "fnDrawCallback": function () {               

           },
           "bStateSave": true,
           "fnInitComplete": function (oSettings) {
               hideLoaderFrame();
           }
       }
   );
}
function LoadPublicHolidayLeave() {
    AllRefreshMode = 0; AllEditMode = 0; 
    var holidayid = $('#ddlHoliday').val();
    var departmentId = $('#ddlDepartment').val();
    var companyid = $('#ddlCompany').val();    
    if (holidayid > 0) {
        $('#hdnHolidayID').val(holidayid);
        pageLoaderFrame();
        $.ajax({
            url: '/PublicHolidayLeaveManager/LoadPublicHolidayLeaveGrid',
            data: { holidayID: holidayid, departmentID: departmentId, companyID: companyid },
            type: 'GET',
            success: function (data) {
                $("#divPublicHolidayLeave").html(data);
                InitGrid();
                hideLoaderFrame();

            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);
                hideLoaderFrame();
            }
        });
    }
    else
    {
        ShowMessage('error', "Please select a Holiday");
    }
}
function SaveAll() {
    var creditbacklist = [];
    //var table = $("#tbl_PublicHolidayLeave").DataTable();
    //var row = table.rows();
    // $(row).each(function (i, item) {
    var holidayid = $('#hdnHolidayID').val();
    $('#tbl_PublicHolidayLeave > tbody > tr').each(function (i, item) {
       // debugger;
        // var row = $(this);  
       // alert($(this).data("leaverequestid"));
        var leaverequestid = $(item).data().leaverequestid;
      //  alert(leaverequestid);
        var days = ($(item).find("#txt_" + leaverequestid)).val();
        //alert(days);
      //  var days = ($(item).find("#txt_96")).val();
        var chk = $(item).find("#chk_" + leaverequestid)
        //$(item).find('td').eq(13); // Index 6 - the 7th column in the table
        var ischecked = chk.prop('checked');
        if(ischecked)
        {
            creditbacklist.push({
                LeaveRequestID: leaverequestid,
                HolidayLeaveDays: days
            });
        }
       
    });
    if (creditbacklist.length == 0) {
        ShowMessage("error", "Please select atleast one record.");
        return;
    }
    var model = {
        HolidayID: holidayid,
        PublicHolidayLeaveManagerModelList: creditbacklist

    }
    // console.log(updatedData);
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all records?" }).done(function () {
      //  alert(JSON.stringify(model));
        pageLoaderFrame();
      
        $.ajax({
            url: "/PublicHolidayLeaveManager/Save",
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            data: JSON.stringify(model),
            success: function (result) {
                if (result.Success) {
                    ShowMessage("success", result.Message);
                    LoadPublicHolidayLeave();
                }
                else {
                    ShowMessage("error", result.Message);
                    LoadPublicHolidayLeave();
                }
            }
        });
    });

}
function LoadDepartment(element) {
    var selectedItem = $(element).val();
    if (selectedItem == undefined || selectedItem == "")
        selectedItem = 0;
    selectedItem = parseInt(selectedItem);
    var ddlDepartment = $("#ddlDepartment");
    $.ajax({
        cache: false,
        type: "GET",
        async: false,
        url: "/PublicHolidayLeaveManager/GetDepartments",
        data: { "companyId": selectedItem },
        success: function (data) {
            ddlDepartment.html('');
            ddlDepartment.append($('<option></option>').val("0").html("Select Department"));
            $.each(data, function (id, option) {
                ddlDepartment.append($('<option></option>').val(option.id).html(option.name));
            });
            RefreshSelect("#ddlDepartment");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //  alert('Failed to retrieve states.');
        }
    });
}
