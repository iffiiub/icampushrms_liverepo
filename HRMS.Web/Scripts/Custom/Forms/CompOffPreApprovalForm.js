﻿$(document).ready(function () {
    var formprocessid = $("#txtFormProcessId").val();

    if (formprocessid == null || formprocessid == '' || formprocessid == '0') {
        checkFormLoadValidation(8, $("#txtCompanyId").val(), "#lblCompOffPreApprovalNotification", "CompOffPreApproval");
    }    

    $("#btnCompOffPreApprovalWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#CompOffPreApprovaltWarningBox").remove();
    });

    disbleFieldsForEdit();

    if ($("#txtAdditionalWorkingHours").is(":visible"))
        bindDecimalNumberOnly("#txtAdditionalWorkingHours", 1);

    $('.btnApprove').click(function () {
        var isHrteam = $("#txtIsHrTeam").val();
        if (isHrteam == "False") {
            Approve();
        } else if (isHrteam == "True")
        {
            var isSaved = parseInt($("#txtIsSaved").val());
            var $form = $("#frmCompOffPreApprovalForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            var IsValidFiles = ValidateData();
            if ($form.valid() && IsValidFiles) {
                if (isSaved == 1) {
                    Approve();
                } else {
                    ShowMessage('error', "Please Save pending changes before approving !");
                }
            }
        }                       
    });

    $('.btnReject').click(function () {
        var isHrteam = $("#txtIsHrTeam").val();
        if (isHrteam == "False") {
            Reject();
        } else if (isHrteam == "True")
        {
            var isSaved = parseInt($("#txtIsSaved").val());
            var $form = $("#frmCompOffPreApprovalForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
           // var IsValidFiles = ValidateData();
            //if ($form.valid() && IsValidFiles) {
            //if ($form.valid()){
                if (isSaved == 1) {
                    Reject();
                } else {
                    ShowMessage('error', "Please Save pending changes before approving !");
                }
            }
        //}
    });

    $(".btnupdate").click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#frmCompOffPreApprovalForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            var IsValidFiles = true;
            var IsReinitialized = $("#txtIsReinitialized").val();
            if (IsReinitialized == "False")
            {
                IsValidFiles = ValidateData();
            }                   
            if ($form.valid() && IsValidFiles)
            {
                pageLoaderFrame();              
                UpdateForm($form);               
            }
        }
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#frmCompOffPreApprovalForm");           
            $.validator.unobtrusive.parse($form);
            $form.validate();                     
            if ($form.valid())
            {
                pageLoaderFrame();
                var data = {
                    formID: 8,
                    companyIDs:$("#txtCompanyId").val()
                }
                var ajaxOptions = {
                    url: '/CompOffPreApproval/IsWorkFlowExists',
                    method:"GET",
                    data: data
                }
                $.ajax(ajaxOptions).done(function (result)
                {
                    if (result.InsertedRowId > 0)
                    {                      
                        SaveForm($form);
                    }
                    else
                    {
                        ShowMessage(result.CssClass, result.Message);
                        hideLoaderFrame();
                    }
                });
            }
        }
    });

    $("#txtAdditionalWorkingHours").change(function () {
        $("#txtIsSaved").val("0");
    });
});

function Reject() {
    var IsValidateComment = validateComments($("#txtComments"));
    if (IsValidateComment) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
            var formprocessid = parseInt($("#txtFormProcessId").val());
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/CompOffPreApproval/RejectForm',
                data: { formProcessID: formprocessid, comments: comments },
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (result.InsertedRowId > 0) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        });
    }
    else
        ShowMessage("error", "Comments field is mandatory.");
}

function Approve() {
    pageLoaderFrame();
    var formprocessid = parseInt($("#txtFormProcessId").val());
    var comments = $("#txtComments").val();
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/CompOffPreApproval/ApproveForm',
        data: { formProcessID: formprocessid, comments: comments },
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (result.InsertedRowId > 0) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

function UpdateForm($form)
{
    var fileTF;
    var formData = new FormData($form[0]);   
    var formInstanceId = $("#txtformInstanceId").val();
    var isHrTeam = $("#txtIsHrTeam").val();
    formData.append("formInstanceId", formInstanceId);
    formData.append("Comments1", $("#txtComments").val());
    formData.append("FormProcessID1", $("#txtFormProcessId").val());
    var WorkingDate = $("#txtWorkingDate").val();
    formData.append("WorkingDate1", WorkingDate);
    formData.append("isHrGroup1", isHrTeam);
    if ($('#txtAdditionalWorkingHours').length > 0)
    {
        var AdditionalWorkingHours = $("#txtAdditionalWorkingHours").val();
        formData.append("AdditionalWorkingHours1", AdditionalWorkingHours);
    }
    if ($('#fu_TF').length > 0)
    {
        if ($("#fu_TF").get(0).files.length > 0) {
            fileTF = $("#fu_TF").get(0).files;
            formData.append("TimeSheetFile", fileTF[0]);
        }
    }

    var ajaxOptions = {
        url: '/CompOffPreApproval/UpdateForm',
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData
    }

    $.ajax(ajaxOptions).done(function (result)
    {
        hideLoaderFrame();
        $("#txtIsSaved").val(result.InsertedRowId);
        ShowMessage(result.CssClass, result.Message);
        console.log(result);
        var IsReinitialized = $("#txtIsReinitialized").val();
        var isHrteam = $("#txtIsHrTeam").val();
        if (isHrteam == "False") {
            setTimeout(function () { BackToTaskList(); }, 1000)
        }
        else
        {
            location.href = "/CompOffPreApproval/Edit";
        }
    });
}

function SaveForm($form) {
    var fileTF;
    var isHrteam = $("#txtIsHrTeam").val();  
    var formData = new FormData($form[0]);
    var RequesterComments = $("#txtComments").val();
    var WorkingDate = $("#txtWorkingDate").val();
    formData.append("WorkingDate1", WorkingDate);
    formData.append("RequesterComments", RequesterComments);
    var ajaxOptions = {
        url: '/CompOffPreApproval/SaveForm',
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData
    }
    $.ajax(ajaxOptions).done(function (result)
    {
        hideLoaderFrame();      
        ShowMessage(result.CssClass, result.Message);      
        setTimeout(function () { BackToTaskList(); }, 1000);
        
    });   
}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);
   $("#txtIsSaved").val("0");
}

function ValidateData() {
    if ($("#fu_TF").is(":visible")) {
        var filepf = $("#fu_TF").val();
        var filepfid = $("#hdnFileUpload_TF").val();
        var pffiledeleteid = $("#hdnDeletedFileUpload_TF").val();
        var isvalid = true;
        var ispffile = true;
        if (filepfid === '' || filepfid == '0') {
            if (filepf === '' || filepf === null || filepf === undefined) {
                isvalid = false;
                ispffile = false;
            }
        }
        if (pffiledeleteid !== '0') {
            isvalid = false;
            ispffile = false;
        }
        if (isvalid)
            return true
        else {
            if (!ispffile) {
                $("#txtuploadedMsgAdd_TF").text("This field is mandatory.");
            }
            return false;
        }
    }
    else
        return true;
}

function disbleFieldsForEdit() {
    var formProcessId = parseInt($("#txtFormProcessId").val());
    if (formProcessId > 0) {
        var IsReinitialized = $("#txtIsReinitialized").val();
      //  var isDisabled = $("#ISDisabled").val();
        //if (IsReinitialized == "False" && isDisabled == "False") {
        //    $("#txtWorkingDate").prop("disabled", true);
        //}
        if (IsReinitialized == "False") {
            $("#txtWorkingDate").prop("disabled", true);
        }
    }
}
