﻿$(document).ready(function () {
    var formid = 3;    
    var formprocessid = $("#hdnFormProcessID").val();    
    if (formprocessid != null && formprocessid != '' && formprocessid != '0') {
        $("#ddlSalaryRanges").val($("#hdnSalaryRanges").val());
        $("#ddlSalaryRanges").selectpicker("refresh");
        $("#ddlEmployee").attr("disabled", "disabled");
        $("#ddlCompanyList").attr("disabled", "disabled");
        $("#txtHeadCount").attr("disabled", "disabled");
        $("#ddlPosition").attr("disabled", "disabled");
        //if no salary range is selected ,enable for selection
        var salaryrangeid = $("#hdnSalaryRangesID").val();
        if (salaryrangeid != "0")
            $("#ddlSalaryRanges").attr("disabled", "disabled");
        var actualbudget = $("#txtActualBudget").val();
        if (actualbudget === null && actualbudget === "" && actualbudget === undefined)
            actualbudget = 0;
        if (parseFloat(actualbudget) > 0)
            $("#txtActualBudget").attr("disabled", "disabled");
        var requestInitialize = $("#hdnRequestInitialize").val();
        var reqstatusid = $("#hdnReqStatusID").val();
       
        if ((requestInitialize != "True" || requestInitialize != 'True') && reqstatusid != "3") {
            $(".btnSave").remove();
            $("#txtActualBudget").attr("disabled", "disabled");
            $("#ddlCompanyList").attr("disabled", "disabled");
            $("#ddlAcademicYear").attr("disabled", "disabled");
            $("#ddlEmploymentMode").attr("disabled", "disabled");
            $("#ddlUserType").attr("disabled", "disabled");
            $("#ddlJobCategoryID").attr("disabled", "disabled");
            $("#ddlDepartment").attr("disabled", "disabled");
            $("#txtProjectData").attr("disabled", "disabled");            
            $("#ddlHMEmploye").attr("disabled", "disabled");
            $("#txtPositionLocation").attr("disabled", "disabled");
            $("#txtHeadCount").attr("disabled", "disabled");
            $("#ddlJobGrade").attr("disabled", "disabled");
            $("#ddlReportingEmploye").attr("disabled", "disabled");
            $("#txtMinExpRequired").attr("disabled", "disabled");
            $("#ddlSource").attr("disabled", "disabled");
            $("#ddlRecruitFrom").attr("disabled", "disabled");
            $("#DivisionID").attr("disabled", "disabled");
            $("#ddlPosition").attr("disabled", "disabled");
            $("#ddlSalaryRanges").attr("disabled", "disabled");
            $("#ddlVehicleToolTrade").attr("disabled", "disabled");
            $("#ddlContractStatus").attr("disabled", "disabled");
            $("#ddlFamilySpouse").attr("disabled", "disabled");
            $("#ddlAnnualAirTicket").attr("disabled", "disabled");
            $("#ddlAirfareFrequency").attr("disabled", "disabled");
            $("#ddlAirfareClass").attr("disabled", "disabled");
            $("#ddlHealthInsurance").attr("disabled", "disabled");
            $("#ddlLifeInsurance").attr("disabled", "disabled");
            $("#ddlSalikTag").attr("disabled", "disabled");
            $("#ddlPetrolCard").attr("disabled", "disabled");
            $("#txtPetrolCardAmount").attr("disabled", "disabled");
            $("#ddlParkingCard").attr("disabled", "disabled");
            $("#txtParkingAreas").attr("disabled", "disabled");
            $("#btnRemove_JD").attr("disabled", "disabled");
            $("#btnRemove_OC").attr("disabled", "disabled");
            $("#btnRemove_MP").attr("disabled", "disabled"); 
            $("#ddlJobGrade").attr("disabled", "disabled");
            $("#ddlRecCategory").attr("disabled", "disabled");
        }
        if ($("#ddlContractStatus").val() === 'Family')
            $("#divSpouse").show();
        else
            $("#divSpouse").hide();
        if ($("#ddlSalikTag").val() === 'true')
            $("#divSalikTag").show();
        else
            $("#divSalikTag").hide();

        if ($("#ddlPetrolCard").val() === 'true')
            $("#divPetrolCard").show();
        else
            $("#divPetrolCard").hide();

        if ($("#ddlParkingCard").val() === 'true')
            $("#divParking").show();
        else
            $("#divParking").hide();

    }
    else {
        $("#txtActualBudget").val('');
        $("#divSpouse").hide();
        $("#divSalikTag").hide();
        $("#divPetrolCard").hide();
        $("#divParking").hide();
    }

    $('.DecimalOnly').keypress(function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var companyIds = $("#ddlCompanyList").val();
            var $form = $("#frmHiringR1Replacement");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid() && ValidateData()) {
                if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                    $.ajax({
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: '/HiringR1Replacement/IsWorkFlowExists',
                        data: { formID: formid, companyIDs: companyIds },
                        success: function (result) {
                            hideLoaderFrame();
                            if (result.InsertedRowId > 0)
                                SaveForm($form);
                            else
                                ShowMessage(result.CssClass, result.Message);
                        },
                        error: function (err) {
                            hideLoaderFrame();
                            ShowMessage('error', err.statusText);

                        }
                    });
                }
                else {
                    SaveForm($form);
                }
            }
            else {
                ValidateData();
            }
        }
    });

    $('.btnApprove').click(function () {
        pageLoaderFrame();
        var formprocessid = $("#hdnFormProcessID").val();
        var comments = $("#txtComments").val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/HiringR1Replacement/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });


    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {

                var formprocessid = $("#hdnFormProcessID").val();
                var comments = $("#txtComments").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/HiringR1Replacement/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        }
        else {
            ShowMessage('error', 'Comments field is mandatory.');
            return false;
        }
    });

    $('.btnBack').click(function () {
        BackToTaskList();
    });

    $("#ddlHMEmploye").change(function () {

        $("#txtPosition").val('');
        var empid = $(this).val();
        var Url = "/Common/GetEmployeeDesignation?employeeID=" + empid;

        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: Url,
            success: function (result) {

                $("#txtPosition").val(result);
            }
        });
        //});
    });

    //$("#ddlPosition").change(function () {
    //    GetSalaryRanges();
    //});

    //$("#ddlSalaryRanges").change(function () {
    //    var salrangeid = $("#ddlSalaryRanges").val();
    //    GetSalaryRangeDetails(salrangeid);
    //});

    $('#ddlCompanyList').change(function (event) {
        var companyId = $('#ddlCompanyList').val();
        if (companyId != null || companyId !== '')
            $("#spCompanyIDValidator").text('');
        if (companyId == null || companyId == '' || companyId == '0') {
            $("#ddlHMEmploye").html("<option value='0'> Select Hiring Manager</option>");
            RefreshSelect("#ddlHMEmploye");
            $("#ddlReportingEmploye").html("<option value='0'> Select Reporting To</option>");
            RefreshSelect("#ddlReportingEmploye");
            $("#ddlDepartment").html("<option value='0'>Select Department</option>");
            RefreshSelect("#ddlDepartment");
            $("#ddlPosition").html("<option value='0'>Select Position</option>");
            RefreshSelect("#ddlPosition");
        }
        else {
            $.ajax({
                url: "/HiringR1Replacement/GetAllEmployeeForAdminByCompanyID?CompanyId=" + companyId,
                success: function (result) {
                    $('#ddlHMEmploye').children('option[value!=""]').remove();
                    $('#ddlReportingEmploye').children('option[value!=""]').remove();

                    $.each(result, function (key, value) {
                        $('#ddlHMEmploye')
                            .append($("<option> </option>")
                                .attr("value", value.EmployeeID)
                                .text(value.FullName));

                        $('#ddlReportingEmploye')
                            .append($("<option> </option>")
                                .attr("value", value.EmployeeID)
                                .text(value.FullName));
                    });

                    $("#ddlHMEmploye").selectpicker("refresh");
                    $("#ddlReportingEmploye").selectpicker("refresh");
                }
            });

            $.ajax({
                type: "POST",
                dataType: 'json',
                data: { companyId: companyId },
                url: "/HiringR1Replacement/GetEmployeeDepartments",
                success: function (data) {
                    data = $.map(data, function (item, a) {
                        return "<option value=" + item.Value + ">" + item.Text + "</option>";
                    });
                    $("#ddlDepartment").html(data.join("")); //converts array into the string
                    RefreshSelect("#ddlDepartment");
                }
            });

            $.ajax({
                type: "POST",
                dataType: 'json',
                data: { companyId: companyId },
                url: "/HiringR1Replacement/GetPositions",
                success: function (data) {
                    data = $.map(data, function (item, a) {
                        return "<option value=" + item.Value + ">" + item.Text + "</option>";
                    });
                    $("#ddlPosition").html(data.join("")); //converts array into the string
                    RefreshSelect("#ddlPosition");
                }
            });
        }
        //var companyId = $("#ddlCompanyList").val();
        //if (companyId != null || companyId !== '')
        //    $("#spCompanyIDValidator").text('');
        //var Url = "/HiringR1Replacement/GetAllEmployeeForAdminByCompanyID?CompanyId=" + companyId;

        //$.ajax({
        //    url: Url, success: function (result) {


        //        $('#ddlHMEmploye').children('option[value!=""]').remove();
        //        $('#ddlReportingEmploye').children('option[value!=""]').remove();

        //        $.each(result, function (key, value) {
        //            $('#ddlHMEmploye')
        //                .append($("<option> </option>")
        //                    .attr("value", value.EmployeeID)
        //                    .text(value.FullName));

        //            $('#ddlReportingEmploye')
        //                .append($("<option> </option>")
        //                    .attr("value", value.EmployeeID)
        //                    .text(value.FullName));
        //        });

        //        $("#ddlHMEmploye").selectpicker("refresh");
        //        $("#ddlReportingEmploye").selectpicker("refresh");
        //    }
        //});
       // GetSalaryRanges();
    });

    $("#txtHeadCount").val(1);

    $("#ddlContractStatus").change(function () {
        if ($(this).val() === 'Family') {
            $("#divSpouse").show();
        } else {
            $("#ddlFamilySpouse").val('');
            $("#divSpouse").hide();
        }
    });

    $("#ddlSalikTag").change(function () {
        if ($(this).val() == "true") {
            $("#divSalikTag").show();
        } else {
            $("#txtSalikAmount").val(0);
            $("#divSalikTag").hide();

        }
    });

    $("#ddlPetrolCard").change(function () {
        if ($(this).val() == "true") {
            $("#divPetrolCard").show();
        } else {
            $("#txtPetrolCardAmount").val(0);
            $("#divPetrolCard").hide();

        }
    });

    $("#ddlParkingCard").change(function () {

        if ($(this).val() == "true") {
            $("#divParking").show();
        } else {
            $("#divParking").hide();

        }
    });

    $("#ddlAnnualAirTicket").change(function () {
        if ($(this).val() === 'false') {
            $("#ddlAirfareFrequency").val("0");
            $("#ddlAirfareClass").val("0");
            RefreshSelect('#ddlAirfareFrequency');
            RefreshSelect('#ddlAirfareClass');
            $("#ddlAirfareFrequency").attr("disabled", "disabled");
            $("#ddlAirfareClass").attr("disabled", "disabled");
        } else {
            $("#ddlAirfareFrequency").removeAttr("disabled");
            $("#ddlAirfareClass").removeAttr("disabled");
            $("#ddlAirfareFrequency").val("");
            $("#ddlAirfareClass").val("");
            RefreshSelect('#ddlAirfareFrequency');
            RefreshSelect('#ddlAirfareClass');
        }
    });

});

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);
}

function SaveForm($form) {

    var fileJD, fileOC, fileMP;
    var companyIds = $("#ddlCompanyList").val();
    var formprocessid = $("#hdnFormProcessID").val();
    var R1ReplacementID = $("#hdnR1ReplacementID").val();
    var deletedfileids = "";

    pageLoaderFrame();
    var hiringR1ReplacementModel = {
        ID: $("#hdnReplacementID").val(),
        CompanyID: $("#ddlCompanyList").val(),
        EmploymentModeID: $("#ddlEmploymentMode").val(),
        ReplacedEmployeeID: $("#ddlEmployee").val(),
        UserTypeID: $("#ddlUserType").val(),
        EmployeeJobCategoryID: $("#ddlJobCategoryID").val(),
        DepartmentID: $("#ddlDepartment").val(),
        ProjectData: $("#txtProjectData").val(),
        HMEmployeeID: $("#ddlHMEmploye").val(),
        PositionLocation: $("#txtPositionLocation").val(),
        HeadCount: $("#txtHeadCount").val(),
        JobGradeID: $("#ddlJobGrade").val(),
        ReportingEmployeeID: $("#ddlReportingEmploye").val(),
        MinExpRequired: $("#txtMinExpRequired").val(),
        SourceID: $("#ddlSource").val(),
        RecruitFromID: $("#ddlRecruitFrom").val(),
        PositionID: $("#ddlPosition").val(),
        //SalaryRangesID: $("#ddlSalaryRanges").val(),
        VehicleToolTrade: $("#ddlVehicleToolTrade").val(),
        ContractStatus: $("#ddlContractStatus").val(),
        FamilySpouse: $("#ddlFamilySpouse").val(),
        AnnualAirTicket: $("#ddlAnnualAirTicket").val(),
        AirfareFrequencyID: $("#ddlAirfareFrequency").val(),
        AirfareClassID: $("#ddlAirfareClass").val(),
        HealthInsurance: $("#ddlHealthInsurance").val(),
        LifeInsurance: $("#ddlLifeInsurance").val(),
        SalikTag: $("#ddlSalikTag").val(),
        SalikAmount: $("#txtSalikAmount").val(),
        PetrolCard: $("#ddlPetrolCard").val(),
        PetrolCardAmount: $("#txtPetrolCardAmount").val(),
        ParkingCard: $("#ddlParkingCard").val(),
        ParkingAreas: $("#txtParkingAreas").val(),
        DivisionID: $("#DivisionID").val(),
        AcademicYearID: $("#ddlAcademicYear").val(),
        ActualBudget: $("#txtActualBudget").val(),
        Comments: $("#txtComments").val(),
        IsReplacementRecord: $("#IsReplacementRecord").val(),
        JobDescriptionFileID: $("#hdnJobDescriptionFileID").val(),
        OrgChartFileID: $("#hdnOrgChartFileID").val(),
        ManPowerFileID: $("#hdnManPowerFileID").val(),
        RequestInitialize: $("#hdnRequestInitialize").val(),
        ReqStatusID: $("#hdnReqStatusID").val(),
        RequesterEmployeeID: $("#hdnRequesterEmployeeID").val(),
        SalaryRanges: $("#ddlSalaryRanges").val(),
        RecCategoryID: $("#ddlRecCategory").val()
    }

    var formData = new FormData($form[0]);
    if ($("#fu_JD").get(0).files.length > 0) {
        fileJD = $("#fu_JD").get(0).files;
        formData.append("JobDescription", fileJD[0]);
    }
    else
        formData.append("JobDescription", null);

    if ($("#fu_OC").get(0).files.length > 0) {
        fileOC = $("#fu_OC").get(0).files;
        formData.append("OrganizationChart", fileOC[0]);
    }
    else
        formData.append("OrganizationChart", null);

    if ($("#fu_MP").get(0).files.length > 0) {
        fileMP = $("#fu_MP").get(0).files;
        formData.append("ManpowerPlan", fileMP[0]);
    }
    else
        formData.append("ManpowerPlan", null);

    formData.append("hiringR1ReplacementModel", JSON.stringify(hiringR1ReplacementModel));
    formData.append("JobDescriptionFileID", $("#hdnJobDescriptionFileID").val());
    formData.append("OrgChartFileID", $("#hdnOrgChartFileID").val());
    formData.append("ManPowerFileID", $("#hdnManPowerFileID").val());
    formData.append("FormProcessID", formprocessid);

    $.ajax({
        url: '/HiringR1Replacement/SaveForm',
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
            else {
                setTimeout(function () { location.href = "/HiringR1Replacement/Edit/" }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });

}

function ValidateData() {
    var filejd = $("#fu_JD").val();
    var filempid = $("#hdnFileUpload_MP").val();
    var fileocid = $("#hdnFileUpload_OC").val();
    var filejdid = $("#hdnFileUpload_JD").val();
    //if the file is removed these hiddenfield will hold its ids
    var jdfiledeleteid = $("#divPreview_JD").html().trim();
    var ocfiledeleteid = $("#divPreview_OC").html().trim();
    var mpfiledeleteid = $("#divPreview_MP").html().trim();
    var isvalid = true;
    var isjdfile = true;
    var isocfile = true;
    var ismpfile = true;

    if (jdfiledeleteid == '') {
        isvalid = false;
        isjdfile = false;
    }

    if (ocfiledeleteid == '') {
        isvalid = false;
        isocfile = false;
    }

    if (mpfiledeleteid == '') {
        isvalid = false;
        ismpfile = false;
    }

    if (isvalid)
        return true
    else {
        if (!isjdfile) {
            $("#txtuploadedMsgAdd_JD").text("This field is mandatory.");
        }
        if (!isocfile) {
            $("#txtuploadedMsgAdd_OC").text("This field is mandatory.");
        }
        if (!ismpfile) {
            $("#txtuploadedMsgAdd_MP").text("This field is mandatory.");
        }
        return false;
    }

}

function GetSalaryRanges() {
    var positionId = $('#ddlPosition').val();
    var companyId = $('#ddlCompanyList').val();
    $('#ddlSalaryRanges').find('option').remove();
    $('#ddlSalaryRanges')
        .append($("<option> </option>")
            .attr("value", "")
            .text("Select Salary Range"));
    if (companyId !== '' && positionId !== null && positionId !== '') {
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: { "companyID": companyId, "positionID": positionId },
            url: '/Common/GetSalaryRange',
            async: false,
            success: function (result) {

                $.each(result, function (key, value) {

                    $('#ddlSalaryRanges')
                        .append($("<option> </option>")
                            .attr("value", value.SalaryRangesID)
                            .text(value.SalaryRanges));

                });
                RefreshSelect('#ddlSalaryRanges');
            }
        });
    }
    else {
        RefreshSelect('#ddlSalaryRanges');
    }
}

function GetSalaryRangeDetails(id) {
    if (id !== null && id !== '' && id != undefined) {
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/SalaryRange/GetSalaryRangeByID?salaryRangeID=' + id,
            success: function (result) {
                $("#hdnMaximumSalary").val(result.MaxSalary);
            }
        });
    }
    else
        $("#hdnMaximumSalary").val(0);
}

$("#txtSalikAmount").on('change keyup paste', function () {
    if ($("#txtSalikAmount").val() === '') {
        $("#spSalikAmountValidator").css("color", "red");
        $("#spSalikAmountValidator").text("This field is mandatory.");
    }
    else
        $("#spSalikAmountValidator").text("");
});

$("#ddlEmployee").on('change', function () {
    pageLoaderFrame();
    var EmployeeID = $("#ddlEmployee").val();
    $(".field-validation-error").html('');
    if (EmployeeID == "")
        EmployeeID = null;
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: { "EmployeeID": EmployeeID },
        url: '/HiringR1Replacement/GetR1ReplacementDetails',
        success: function (result) {
            $("#IsReplacementRecord").val(result.IsReplacementRecord);
            $("#ddlCompanyList").val(result.CompanyID).change();
            $("#ddlAcademicYear").val(result.AcademicYearID);
            $("#ddlEmploymentMode").val(result.EmploymentModeID);
            $("#ddlUserType").val(result.UserTypeID);
            $("#ddlJobCategoryID").val(result.EmployeeJobCategoryID);
            $("#ddlDepartment").val(result.DepartmentID);
            $("#txtProjectData").val(result.ProjectData);
            $("#ddlHMEmploye").val(result.HMEmployeeID);
            $("#txtPosition").val(result.HMDesignation);
            $("#txtPositionLocation").val(result.PositionLocation);
            $("#txtHeadCount").val(result.HeadCount == 0 ? 1 : result.HeadCount);
            $("#ddlJobGrade").val(result.JobGradeID);
            $("#ddlReportingEmploye").val(result.ReportingEmployeeID);
            $("#txtMinExpRequired").val(result.MinExpRequired);
            $("#ddlSource").val(result.SourceID);
            $("#ddlRecruitFrom").val(result.RecruitFromID);
            $("#DivisionID").val(result.DivisionID);
            $("#ddlPosition").val(result.PositionID);
            //GetSalaryRanges();
            $("#ddlSalaryRanges").val(result.SalaryRangesID);
            $("#txtActualBudget").val(result.ActualBudget);
            //--If ActualBudget >0 then it should disabled.
            if (result.ActualBudget==''|| result.ActualBudget > 0) {
                $("#txtActualBudget").attr("disabled", "disabled");
            }
            else {
                $("#txtActualBudget").removeAttr("disabled");
            }
            $("#ddlVehicleToolTrade").val(result.VehicleToolTrade == null ? result.VehicleToolTrade : result.VehicleToolTrade.toString());
            $("#ddlContractStatus").val(result.ContractStatus);
            $("#ddlFamilySpouse").val(result.FamilySpouse);
            $("#ddlAnnualAirTicket").val(result.AnnualAirTicket == null ? result.AnnualAirTicket : result.AnnualAirTicket.toString());
            $("#ddlAirfareFrequency").val(result.AirfareFrequencyID);
            $("#ddlAirfareClass").val(result.AirfareClassID);
            $("#ddlHealthInsurance").val(result.HealthInsurance == null ? result.HealthInsurance : result.HealthInsurance.toString());
            $("#ddlLifeInsurance").val(result.LifeInsurance == null ? result.LifeInsurance : result.LifeInsurance.toString());
            $("#ddlSalikTag").val(result.SalikTag == null ? result.SalikTag : result.SalikTag.toString());
            $("#txtSalikAmount").val(result.SalikAmount);
            $("#ddlPetrolCard").val(result.PetrolCard == null ? result.PetrolCard : result.PetrolCard.toString());
            $("#txtPetrolCardAmount").val(result.PetrolCardAmount);
            $("#ddlParkingCard").val(result.ParkingCard == null ? result.ParkingCard : result.ParkingCard.toString());
            $("#txtParkingAreas").val(result.ParkingAreas);
            $('#divPreview_JD').html('');
            $('#divPreview_OC').html('');
            $('#divPreview_MP').html('');

            result.AllFormsFilesModelList.forEach(function (p) {
                if (p.FormFileIDName == 'JobDescriptionFileID') {
                    $('#divPreview_JD').append('<a onclick="OpenFilePreview(\'HiringR1Replacement\',' + p.FileID + ')">' + p.FileName + '</a>');
                    $("#hdnJobDescriptionFileID").val(p.FileID);
                }
                if (p.FormFileIDName == 'OrgChartFileID') {
                    $('#divPreview_OC').append('<a onclick="OpenFilePreview(\'HiringR1Replacement\',' + p.FileID + ')">' + p.FileName + '</a>');
                    $("#hdnOrgChartFileID").val(p.FileID);
                }
                if (p.FormFileIDName == 'ManPowerFileID') {
                    $('#divPreview_MP').append('<a onclick="OpenFilePreview(\'HiringR1Replacement\',' + p.FileID + ')">' + p.FileName + '</a>');
                    $("#hdnManPowerFileID").val(p.FileID);
                }
            });

            RefreshSelect("#ddlAcademicYear");
            RefreshSelect("#ddlEmploymentMode");
            RefreshSelect("#ddlUserType");
            RefreshSelect("#ddlJobCategoryID");
            RefreshSelect("#ddlDepartment");
            RefreshSelect("#ddlHMEmploye");
            RefreshSelect("#ddlJobGrade");
            RefreshSelect("#ddlReportingEmploye");
            RefreshSelect("#ddlSource");
            RefreshSelect("#ddlRecruitFrom");
            RefreshSelect("#DivisionID");
            RefreshSelect("#ddlVehicleToolTrade");
            RefreshSelect("#ddlContractStatus");
            RefreshSelect("#ddlFamilySpouse");
            RefreshSelect("#ddlAnnualAirTicket");
            RefreshSelect("#ddlAirfareFrequency");
            RefreshSelect("#ddlAirfareClass");
            RefreshSelect("#ddlHealthInsurance");
            RefreshSelect("#ddlLifeInsurance");
            RefreshSelect("#ddlSalikTag");
            RefreshSelect("#ddlPetrolCard");
            RefreshSelect("#ddlParkingCard");
            RefreshSelect("#ddlSalaryRanges");

            if (result.IsReplacementRecord) {
                RefreshDisabledDdl("#ddlCompanyList", true);
                RefreshDisabledDdl("#ddlPosition", true);
            }
            else {
                RefreshDisabledDdl("#ddlCompanyList", false);
                RefreshDisabledDdl("#ddlPosition", false);
            }

            if ($("#ddlSalikTag").val() === 'true')
                $("#divSalikTag").show();
            else
                $("#divSalikTag").hide();
            if ($("#ddlPetrolCard").val() === 'true')
                $("#divPetrolCard").show();
            else
                $("#divPetrolCard").hide();
            if ($("#ddlParkingCard").val() === 'true')
                $("#divParking").show();
            else
                $("#divParking").hide();

            if ($("#ddlSalaryRanges").val() != '') {
                $("#ddlSalaryRanges").attr("disabled", "disabled");
            }
            $("#txtActualBudget").attr("disabled", "disabled");

            hideLoaderFrame();
        }
    });
});

function RefreshDisabledDdl(id, mode) {
    if (mode) {
        RefreshSelect(id);
        $(id).attr("disabled", "disabled");
    }
    else {
        $(id).removeAttr("disabled");
        RefreshSelect(id);
    }
}