﻿
function validateComments(element) {
    var IsValidateComment = true;
    if ($(element).val() == '') {
        if ($(element).parent().find('.field-validation-error').length == 0)
            $(element).parent().append('<span class=\"field-validation-error\">This is mandatory field.</span>');
        IsValidateComment = false;
    }
    else if ($(element).parent().find('.field-validation-error').length > 0) {
        $(element).parent().find('.field-validation-error')[0].remove();
    }
    return IsValidateComment;
}

$(".CheckKeyReleaseComments").on('keyup paste', function () {
    if ($(this).val().length > 0) {
        if ($(this).parent().find('.field-validation-error').length > 0)
            $(this).parent().find('.field-validation-error')[0].remove();
    }
});

function checkFormLoadValidation(formId, companyId, labelId, controllerName) {
    pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        url: '/' + controllerName +'/CheckFormLoadValidation',
        data: {
            formID: formId, companyId: companyId
        },
        success: function (result) {
            hideLoaderFrame();
            if (result.InsertedRowId == 0) {
                $('#warningMessage').show();
                $(labelId).html(result.Message);
                $(".btnSave").prop('disabled', true);
            }
            else {
                $('#warningMessage').hide();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

//All Requests related

$('.btnBackToAllRequest').click(function () {
    BackToAllRequestList();
});

function BackToAllRequestList()
{
    location.href = "/AllRequests/Index/";
}
//*** Naresh 2020-02-26 number type validation not being work on Microsoft Edge browser
$(document).ready(function () {  
    $(':input[type="number"]').on("keydown", function (e) {
        if (((e.which === 110 || e.which === 190) && ($(this).val().toString().indexOf(".") === -1)) || e.which === 9 || e.which === 13 || e.which === 46 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.which === 8 || e.which === 109 || e.which === 189 || e.which === 18 || (e.which >= 35 && e.which <= 40)) {
            return true;
        }
        else {
            return false;
        }
    });
});
 

