﻿$(document).ready(function () {
    initReleaseR1RequestList();
});


function initReleaseR1RequestList() {
    var ReleaseR1RequestList = $('#tblReleaseR1Request').DataTable(
        {
            "bFilter": true,
            "bInfo": true,
            "bSortCellsTop": true,
            "bStateSave": false,
            "aaSorting": [[1, 'asc']]
        }
    );
}

$(document).on("click", "#btnSearch", function () {
    pageLoaderFrame();
    LoadReleaseR1RequestGrid(true);
});

function LoadReleaseR1RequestGrid(IsStartLoader) {
    if (IsStartLoader) {
        pageLoaderFrame();
    }
    $.ajax({
        type: 'GET',
        url: '/ReleaseR1Request/GetR1ReleaseByR1RequestID',
        data: { R1RequestID: $("#ddlReleaseR1Request").val() },
        async: false,
        success: function (result) {
            hideLoaderFrame();
            $("#EmployeeReleaseGrid").html(result);
            initReleaseR1RequestList();
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}
function AddReleaseComments(profileCreationID,r1RequestID)
{   
    $('#hdnReleaseProfileID').val(profileCreationID);
    $('#hdnR1RequestID').val(r1RequestID);
    $("#modal_Loader").html("");
    pageLoaderFrame();
    $("#modal_Loader").load("/ReleaseR1Request/AddReleaseComments", function () {
        $("#modal_heading").text('R1 Release Comments');
        hideLoaderFrame();
        $('#myModal').modal({ backdrop: 'static' });
    });
}
//R1RequestID, FormProcessID, FormID
function UpdateReleaseR1Request() {
  
    var profilecreationid = $('#hdnReleaseProfileID').val();
    var requestid = $('#hdnR1RequestID').val();    
    var IsValidateComment = validateComments($("#txtComments"));
    if (IsValidateComment) {
        //  $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to release this R1 Request?" }).done(function () {
         var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                url: '/ReleaseR1Request/UpdateReleaseR1Request',
                data: { profileCreationID: profilecreationid, r1Releasecomments: comments },
                success: function (result) {
                    if (result.InsertedRowId > 0) {
                        LoadReleaseR1RequestGrid(false);
                        ShowMessage('success', 'R1 Request ID (' + requestid + ') released successfully');
                        $("#myModal").modal("hide");

                    }
                    else {
                        ShowMessage('error', 'Unable to release R1 Request');
                        hideLoaderFrame();
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        //});
    }
    else {
        ShowMessage('error', 'Comments field is mandatory.');
    }
}

function OpenFromRequest(formprocessid,formid)
{
    var controller = "";
    if (formid === 1) {
        controller = 'HiringRequisitionR1BudgetedForm';
    }
    else if (formid === 2) {
        controller = 'HiringRequisitionR1UnBudgetedForm';
    }
    else if (formid === 3) {
        controller = 'HiringR1Replacement';
    }
    else if (formid === 29) {
        controller = 'R1UnBudgetedReplacement';
    }
    else if (formid === 4) {
        controller = 'EmployeeProfileCreationForm';
    }
    else
    {
        controller = "";
    }
    $.ajax({
        url: '/' + controller + '/SetR1ReleaseFormProcessSesssion',
        data: { id: formprocessid },
        success: function (result) {
            if (result.id > 0) {
               // LoadReleaseR1RequestGrid(false);
                var urlString = ''
                if (formid === 1) {
                    urlString = '/' + controller + '/ViewDetails';
                }
                else if (formid === 2) {
                    urlString = '/' + controller + '/ViewDetails';
                }
                else if (formid === 3) {
                    urlString = '/' + controller + '/ViewDetails';
                }
                else if (formid === 29) {
                    urlString = '/' + controller + '/ViewDetails';
                }
                else if (formid === 4) {
                    urlString = '/' + controller + '/ViewDetails';
                }
                if (urlString != '') {
                    window.open(urlString, '_blank');
                }
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}



