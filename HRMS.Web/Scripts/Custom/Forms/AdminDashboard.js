﻿var isProbationConfirmationRadiobtnChecked = false;

$(document).ready(function () {
    bindSelectpicker('.selectpickerddl');
    $("#ApproverRequestNote").hide();

    $('.btnProbationConfirmationSave').click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#ManageProbationConfirmationForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($(":radio[name=rdProbationConfirmationOption]").is(":checked"))
                isProbationConfirmationRadiobtnChecked = true;
            else
                isProbationConfirmationRadiobtnChecked = false;
           
            if ($form.valid()) {
                if (!isProbationConfirmationRadiobtnChecked) {
                    ShowMessage("error", "Please select one of the Probation Confirmation option.");
                    return false;
                }
                pageLoaderFrame();
                checkOrGenerateProbationCompletionRequest($("#isProbationConfirmationComplete").is(":checked"), $("#isGenerateProbationConfirmationRequest").is(":checked"));             
            }
            else {
                if (!isProbationConfirmationRadiobtnChecked) {
                    ShowMessage("error", "Please select one of the Probation Confirmation option.");
                    return false;
                }
            }
        }
    });

    $(document).on("change", "#RequestId", function () {
        if ($("#RequestId").val() !== "")
            showPendingApproverEmployeeStatus();
        else
            $("#ApproverRequestNote").hide();      
    });

    $(".btnWorkflowReroutingSave").click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#WorkflowReroutingForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();          
            if ($form.valid()) {
                $.ajax({
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: '/AdminDashboard/UpdateRequestApprovarEmpoyee',
                    data: { requestId: $("#RequestId").val(), employeeId: $("#WorkflowReroutingEmployee").val() },
                    success: function (result) {
                        ShowMessage(result.CssClass, result.Message);
                        hideLoaderFrame();
                        showPendingApproverEmployeeStatus();
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
        }
    });
});

function checkOrGenerateProbationCompletionRequest(isProbationConfirmationComplete, isGenerateProbationConfirmationRequest) {
    if (isProbationConfirmationComplete) {
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/AdminDashboard/CheckProbationConfirmationRequestConditions',
            data: { employeeId: $("#ProbationConfirmationEmployee").val() },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                hideLoaderFrame();
                if (result.Success)
                    location.reload();
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
    else if (isGenerateProbationConfirmationRequest) {
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/AdminDashboard/GenerateProbationConfirmationRequest',
            data: { employeeId: $("#ProbationConfirmationEmployee").val()},
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                hideLoaderFrame();
                if (result.Success)
                    location.reload();
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
}

function showPendingApproverEmployeeStatus() {
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/AdminDashboard/GetPendingApproverEmployeeByRequestId',
        data: { requestId: $("#RequestId").val() },
        success: function (result) {
            if (result !== null) {
                $("#requestIdNote").text(result);
                $("#ApproverRequestNote").show();
            }
            else
                $("#ApproverRequestNote").hide();
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}