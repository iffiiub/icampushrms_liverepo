﻿$(document).ready(function () {
    checkFormLoadValidation(7, $("#CompanyId").val(), "#lblEmployeeLeaveRejoiningNotification", "EmployeeLeaveRejoining");
    $("#btnEmployeeLeaveRejoiningWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#EmployeeLeaveRejoiningWarningBox").remove();
    });

    bindSelectpicker(".selectpicker");
    bindDatePicker(".DOB");  
    var reqstatusid = $("#hdnReqStatusID").val();   
    if (reqstatusid == "3") {
        $(".btnApprove").remove();
        $(".btnReject").remove();
        $(".btnUpdate").prop("title", "Re-Submit");
        $('button#btnUpdate').text("Re-Submit");
    }
    else {
        var formprocessid = $("#FormProcessID").val();
        if (formprocessid != null && formprocessid != '' && formprocessid != '0') {
            $("#ReJoiningDate").attr("disabled", "disabled");
            $(".btnUpdate").remove();
        }
    }


$(document).on("change", "#ReJoiningDate", function () {
    if ($("#ReJoiningDate").val() == "" || $("#ReJoiningDate").val() == undefined) {
        $("#LeavesToAdjust").val(0);
    }
    else {       
        CalculateAndSetRejoinDate();
    }
});

$(document).on("click", ".btnSave", function () {    
    if ($("#ReJoiningDate").val() != "" && $("#ReJoiningDate").val() != undefined && $("#ReJoiningDate").val() != null && $("#ReJoiningDate").val() != '') {
        pageLoaderFrame();
        if ($("#frmEmployeeLeaveReJoining").valid()) {
            $.ajax({
                dataType: 'json',
                url: '/EmployeeLeaveRejoining/IsWorkFlowExists',
                data: {
                    formID: $("#FormId").val(), companyIDs: $("#CompanyId").val()
                },
                success: function (result) {
                   //  hideLoaderFrame();
                    if (result.InsertedRowId > 0) {                      
                        SaveEmployeeLeaveReJoining(); //Save Form
                    }
                    else {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                    }
                },
                error: function (err) {
                   // hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    }
    else
    {
        ShowMessage('error', "Please enter Rejoin date");
    }

});
$(document).on("click", ".btnUpdate", function () {
    if ($("#ReJoiningDate").val() != "" && $("#ReJoiningDate").val() != undefined && $("#ReJoiningDate").val() != null && $("#ReJoiningDate").val() != '') {
        pageLoaderFrame();
        if ($("#frmEmployeeLeaveReJoining").valid()) {    
            $.ajax({
                dataType: 'json',
                url: '/EmployeeLeaveRejoining/IsWorkFlowExists',
                data: {
                    formID: $("#FormId").val(), companyIDs: $("#CompanyId").val()
                },
                success: function (result) {                  
                    if (result.InsertedRowId > 0) {
                        // hideLoaderFrame();
                        UpdateEmployeeLeaveReJoining(); //Save Form
                    }
                    else {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    }
    else {
        ShowMessage('error', "Please enter Rejoin date");
    }

});
$(document).on("click", ".btnApprove", function () {
    pageLoaderFrame();
    var formprocessid = $("#FormProcessID").val();
    var comments = $("#txtComments").val();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/EmployeeLeaveRejoining/ApproveForm',
        data: { formProcessID: formprocessid, comments: comments },
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (result.InsertedRowId > 0) {
                setTimeout(function () { BackToTaskList(); }, 1000);
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
});

$(document).on("click", ".btnReject", function () {
    var IsValidateComment = validateComments($("#txtComments"));
    if (IsValidateComment) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {

            var formprocessid = $("#FormProcessID").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/EmployeeLeaveRejoining/RejectForm',
                data: { formProcessID: formprocessid, comments: comments },
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (result.InsertedRowId > 0) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        });
    }
    else
        ShowMessage('error', 'Comments field is mandatory.');
});

$(document).on("click", ".btnBack", function () {
    var formprocessid = $("#FormProcessID").val();
    if (formprocessid != null && formprocessid != '' && formprocessid != '0') {
        location.href = "/FormsTaskList/Index/";
    }
    else {
        location.href = "/LeaveRejoingList/Index/";
    }
});
});

function SaveEmployeeLeaveReJoining() {   
        CalculateAndSetRejoinDate();
    //((moment($("#ReJoiningDate").val(), 'DD/MM/YYYY').toDate() - moment($("#LeaveEndDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) - 1;
        var noofdiffdays = ((moment($("#ReJoiningDate").val(), momentdateformat).toDate() - moment($("#LeaveStartDate").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24));
        if (parseInt(noofdiffdays) <= 0) {
            hideLoaderFrame();
            ShowMessage("error", "Rejoin date must be a date greater than leave from date.");
            //  return false;
        }
        else {
            var latedays = $("#LeavesToAdjust").val();
            // $("#LeavesToAdjust").val(latedays);
            var leavetypeid = $("#ddlVacationType").val();
            var vtcategoryid = 0;
            var balance = 0;
            var LeaveBalanceInfo = GetLeaveBalanceInfoById(leavetypeid);
            if (LeaveBalanceInfo != null) {
                vtcategoryid = LeaveBalanceInfo.VTCategoryID;
                if (vtcategoryid == 1)
                    balance = parseFloat(LeaveBalanceInfo.RemainingLeave) + parseFloat(LeaveBalanceInfo.LapsingDays) + parseFloat(LeaveBalanceInfo.ExtendedLapsingDays);
                else
                    balance = parseFloat(LeaveBalanceInfo.RemainingLeave);
                //reversing 
                latedays = 0 - latedays;
                if (latedays > 0) {
                    //vtcategoryid == 1||1-annual,
                    //4-Emergency,5-maternity,6-Compassionate,7-Hajj
                    if (vtcategoryid == 4 || vtcategoryid == 5 || vtcategoryid == 6 || vtcategoryid == 7) {
                        if (balance < latedays) {
                            hideLoaderFrame();
                            ShowMessage("error", "Your leave balance is (" + balance + ") days only.You cannot extend days more than your balance");
                            return false;
                        }

                    }
                    else if (vtcategoryid == 1) {
                        //if((balance+4)<latedays)
                        //{
                        //    ShowMessage("error", "Your leave balance is ("+balance+") days and can extend 4 more days.You cannot extend days more than that");
                        //    return false;
                        //}
                        if (balance - latedays < -4) {
                            hideLoaderFrame();
                            ShowMessage("error", "Your current balance is " + balance + " and you cannot apply more than -4 balance.");
                            return false;
                        }
                    }
                    else {
                        hideLoaderFrame();
                        ShowMessage("error", "Join date can't be extended");
                        return false;
                    }

                }

                var employeeLeaveRejoining = {
                    EmployeeID: $("#EmployeeID").val(),
                    ID_LeaveRequest: $("#ID_LeaveRequest").val(),
                    RequestDate: $("#RequestDate").val(),
                    ReJoiningDate: $("#ReJoiningDate").val(),
                    LeavesToAdjust: $("#LeavesToAdjust").val(),
                    Comments: $("#txtComments").val()
                };
                // pageLoaderFrame();
                $.ajax({
                    url: "/EmployeeLeaveRejoining/SaveLeaveRejoiningRequest",
                    type: 'POST',
                    data: {
                        leaveRejoiningModel: JSON.stringify(employeeLeaveRejoining)
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.Success) {
                            hideLoaderFrame();
                            ShowMessage("success", data.Message);
                            setTimeout(function () { location.href = "/LeaveRejoingList/Index" }, 1000);
                            //location.reload();
                        }
                        else {
                            hideLoaderFrame();
                            ShowMessage("error", data.Message);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
            else {
                hideLoaderFrame();
                ShowMessage("error", "Unable to get leave balance");

            }
        }
     
    
}
function GetLeaveBalanceInfoById(LeaveTypeID) {
    
    var LeaveBalanceInfo = [];
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetLeaveBalanceInfoById',
        data: { LeaveTypeID: LeaveTypeID, RequesterEmployeeID: $("#hdnRequesterEmployeeID").val() },
        async: false,
        success: function (LeaveBalanceInfoModel) {
            LeaveBalanceInfo = LeaveBalanceInfoModel;
            //$("#txtTotalAvailLeaves").val(LeaveBalanceInfoModel.RemainingLeave);
            //GetRemainingDays();
            //console.log(LeaveBalanceInfoModel);
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return LeaveBalanceInfo;
}


function UpdateEmployeeLeaveReJoining() {
    if ($("#ReJoiningDate").val() != "" && $("#ReJoiningDate").val() != undefined && $("#ReJoiningDate").val() != null && $("#ReJoiningDate").val() != '') {
        var noofdiffdays = ((moment($("#ReJoiningDate").val(), momentdateformat).toDate() - moment($("#LeaveStartDate").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24));
        if (parseInt(noofdiffdays) <= 0) {
            hideLoaderFrame();
            ShowMessage("error", "Rejoin date must be a date greater than leave from date.");
            //  return false;
        }
        else {
            CalculateAndSetRejoinDate();
            //((moment($("#ReJoiningDate").val(), 'DD/MM/YYYY').toDate() - moment($("#LeaveEndDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) - 1;
            var latedays = $("#LeavesToAdjust").val();
            // $("#LeavesToAdjust").val(latedays);
            var leavetypeid = $("#ddlVacationType").val();
            var vtcategoryid = 0;
            var balance = 0;
            var LeaveBalanceInfo = GetLeaveBalanceInfoById(leavetypeid);
            if (LeaveBalanceInfo != null) {
                vtcategoryid = LeaveBalanceInfo.VTCategoryID;
                if (vtcategoryid == 1)
                    balance = parseFloat(LeaveBalanceInfo.RemainingLeave) + parseFloat(LeaveBalanceInfo.LapsingDays) + parseFloat(LeaveBalanceInfo.ExtendedLapsingDays);
                else
                    balance = parseFloat(LeaveBalanceInfo.RemainingLeave);
                //reversing 
                latedays = 0 - latedays;
                if (latedays > 0) {
                    //vtcategoryid == 1||1-annual,
                    //4-Emergency,5-maternity,6-Compassionate,7-Hajj
                    if (vtcategoryid == 4 || vtcategoryid == 5 || vtcategoryid == 6 || vtcategoryid == 7) {
                        if (balance < latedays) {
                            hideLoaderFrame();
                            ShowMessage("error", "Your leave balance is (" + balance + ") days only.You cannot extend days more than your balance");
                            return false;
                        }

                    }
                    else if (vtcategoryid == 1) {
                        if (balance - latedays < -4) {
                            //if (latedays > 4) {
                            hideLoaderFrame();
                            ShowMessage("error", "Your current balance is " + balance + " and you cannot apply more than -4 balance.");
                            return false;
                        }
                    }
                    else {
                        hideLoaderFrame();
                        ShowMessage("error", "Join date can't be extended");
                        return false;
                    }

                }

                var employeeLeaveRejoining = {
                    ID: $("#ID").val(),
                    FormProcessID: $("#FormProcessID").val(),
                    RequesterEmployeeID: $("#hdnRequesterEmployeeID").val(),
                    ReqStatusID: $("#hdnReqStatusID").val(),
                    RequestDate: $("#RequestDate").val(),
                    ReJoiningDate: $("#ReJoiningDate").val(),
                    LeavesToAdjust: $("#LeavesToAdjust").val(),
                    Comments: $("#txtComments").val()
                };
                //  pageLoaderFrame();
                $.ajax({
                    url: "/EmployeeLeaveRejoining/UpdateLeaveRejoiningRequest",
                    type: 'POST',
                    data: {
                        leaveRejoiningModel: JSON.stringify(employeeLeaveRejoining)
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.Success) {
                            hideLoaderFrame();
                            ShowMessage("success", data.Message);
                            setTimeout(function () { location.href = "/FormsTaskList/Index/" }, 1000);
                            //location.reload();
                        }
                        else {
                            ShowMessage("error", data.Message);
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
            else {
                hideLoaderFrame();
                ShowMessage("error", "Unable to get leave balance");
            }

        }
    }
    
}
  


    function GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate) {
        var vacationmodel = null;
      //  pageLoaderFrame();
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/EmployeeLeaveRequest/GetValidLeaveRequestDates',
            data: { employeeID: employeeid, vacationTypeID: leavetypeid, fromDate: fromdate, toDate: todate },
            async: false,
            success: function (result) {
                vacationmodel = result;
               // hideLoaderFrame();
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
               // hideLoaderFrame();
            }
        });
        return vacationmodel;
    }
    function CalculateAndSetRejoinDate() {
        var employeeid = $("#hdnRequesterEmployeeID").val();
        var leavetypeid = $("#ddlVacationType").val();     
        var noofdays = ((moment($("#ReJoiningDate").val(), momentdateformat).toDate() - moment($("#LeaveEndDate").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24)) - 1;
        var rejoindate = $("#ReJoiningDate").val(); 
        var fromdate, todate, leavestoadjust; 
      
            if (noofdays > 0) {
                fromdate = moment($("#LeaveEndDate").val(), momentdateformat).add(1, 'days').format(momentdateformat);
                todate = moment(rejoindate, momentdateformat).add(-1, 'days').format(momentdateformat);
                var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
                $("#ReJoiningDate").val(moment(vacation.toDate, momentdateformat).add(1, 'days').format(momentdateformat));
                leavestoadjust = vacation.NoOfDays;
                leavestoadjust = 0 - leavestoadjust;
            }
            else if (noofdays < 0) {
                fromdate = moment($("#ReJoiningDate").val(), momentdateformat).format(momentdateformat); //.add(1, 'days')
                todate = $("#LeaveEndDate").val();
                var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
                $("#ReJoiningDate").val(vacation.fromDate);
                leavestoadjust = vacation.NoOfDays;
                // leavestoadjust = 0 - leavestoadjust;
            }
            else {
                leavestoadjust = 0;
            }
        
      
        $("#LeavesToAdjust").val(leavestoadjust);
    }
