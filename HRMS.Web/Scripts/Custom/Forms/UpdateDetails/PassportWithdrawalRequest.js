﻿$(document).ready(function () {
    var formprocessid = $("#FormProcessID").val();

    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });
    var reqstatusid = $("#hdnReqStatusID").val();
    var PassportReasonID = $("#ddlPassportReason").val();
    if (reqstatusid == "3") {
        $("#WithdrawalDate").removeAttr("disabled", "disabled");
        $("#PassportReturnDate").removeAttr("disabled", "disabled");
        if (PassportReasonID == "12") {
            $("#OtherReason").removeAttr("disabled");
        }
        else {
            $("#OtherReason").val('');
            $("#OtherReason").attr("disabled", "disabled");
            $("#OtherReason").removeClass('input-validation-error');
            $(".OtherReason").text('');
        }
    }
    var isEditRequestFromAllRequest = $("#hdnIsEditRequestFromAllRequest").val();
    if ((reqstatusid == "1" && isEditRequestFromAllRequest) || (reqstatusid == "4" && isEditRequestFromAllRequest)) {
        if (PassportReasonID == "12") {
            $("#OtherReason").removeAttr("disabled");
        }
        else {
            $("#OtherReason").val('');
            $("#OtherReason").attr("disabled", "disabled");
            $("#OtherReason").removeClass('input-validation-error');
            $(".OtherReason").text('');
        }
    }
});

$(document).on("change", "#ddlPassportReason", function () {
    if ($(this).val() == 12) {
        $("#OtherReason").removeAttr("disabled");
    }
    else {
        $("#OtherReason").val('');
        $("#OtherReason").attr("disabled", "disabled");
        $("#OtherReason").removeClass('input-validation-error');
        $(".OtherReason").text('');
    }
});

$(document).on("click", ".btnUpdate", function () {
    if ($("#frmPassportWithdrawalRequest").valid()) {
        pageLoaderFrame();
        var param = {
            ID: $("#ID").val(), PassportReasonID: $("#ddlPassportReason").val(), OtherReason: $("#OtherReason").val(),
            WithdrawalDate: $("#WithdrawalDate").val(), PassportReturnDate: $("#PassportReturnDate").val(),
            ReqStatusID: $("#hdnReqStatusID").val(), Comments: $("#txtComments").val()
        };
        $.ajax({
            url: "/PassportWithdrawalRequest/UpdatePassportWithdrawalRequest",
            type: 'POST',
            data: param,
            datatype: "json",
            success: function (data) {
                if (data.InsertedRowId > 0) {
                    ShowMessage("success", data.Message);
                    hideLoaderFrame();
                    if (result.CssClass != 'error') {
                        location.reload();
                    }
                }
                else {
                    ShowMessage("error", data.Message);
                    hideLoaderFrame();
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    }
});
