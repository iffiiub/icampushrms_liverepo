﻿$(document).ready(function () {
    var formid;
    $("#lblValidateDateOfSeparation").css("color", "red");
    $("#lblValidateLastWorkingDay").css("color", "red");
    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());

    });
    var formprocessid = $("#hdnFormProcessID").val();

    $("#ddlEmployeeID").selectpicker("refresh");

    $("#ddlEmployeeID").change(function () {
        var employeeid = $("#ddlEmployeeID").val();
        if (employeeid !== null && employeeid !== '' && employeeid != undefined) {
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'GET',
                url: '/SeparationRequestForm/GetForm',
                data: { formProcessID: null, employeeID: employeeid },
                success: function (result) {
                    hideLoaderFrame();
                    if (result != null) {
                        $("#txtNationality").val(result.Nationality);
                        $("#txtDOJ").val(result.DOJ);
                        $("#txtMobileNumber").val(result.MobileNumber);
                        $("#txtCompany").val(result.Company);
                        $("#txtDepartment").val(result.Department);
                        $("#txtDesignation").val(result.Designation);
                        $("#txtLineManager").val(result.LineManager);
                        $("#txtLineManagerID").val(result.LineManagerID);
                        $("#txtProject").val(result.Project);
                        $("#txtJobGrade").val(result.JobGrade);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    });

    $("#ddlSeparationTypeID").change(function () {
        var separationtypeid = $("#ddlSeparationTypeID").val();
        if (separationtypeid !== null && separationtypeid !== '' && separationtypeid != undefined && separationtypeid > 0) {
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'GET',
                url: '/SeparationRequestForm/GetFormsUniqueKey',
                data: { separationTypeID: separationtypeid },
                success: function (result) {
                    hideLoaderFrame();
                    if (result != null) {
                        $("#hdnFormID").val(result.FormID);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    });

    $(".calculatenoticed").change(function () {
        CalculateDays();
    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var formprocessid = $("#hdnFormProcessID").val();
            var $form = $("#frmSeparationRequestForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid() && ValidateData()) {
                pageLoaderFrame();
                SaveForm();
            }
            else {
                ValidateData();
            }
        }
    });
});


function CalculateDays() {
    var isvalid = true;
    var start = $('#txtDateOfSeparation').val();
    var end = $('#txtLastWorkingDay').val();
    var day = 0;

    if (!(start != '' && start != null && start != undefined)) {
        $("#txtNoticedPeriodDays").val("0");
        isvalid = false;
    }
    else
        $("#lblValidateDateOfSeparation").text("");
    if (!(end != '' && end != null && end != undefined)) {
        $("#txtNoticedPeriodDays").val("0");
        isvalid = false;
    }
    else
        $("#lblValidateLastWorkingDay").text("");
    if (!isvalid) {
        return;
    }
    day = (moment($("#txtLastWorkingDay").val(), momentdateformat).toDate() - moment($("#txtDateOfSeparation").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24)
    day = day + 1;
    $('#txtNoticedPeriodDays').val(day);
}

function SaveForm() {
    pageLoaderFrame();   
    var fileSD;
    var formprocessid = $("#hdnFormProcessID").val();
    var separationrequestformid = $("#hdnSeparationRequestFormID").val();
    var employeeid = $("#ddlEmployeeID").val();
    var comments = $("#txtComments").val();
    var isintiatehr = false;
    if ($("#chkInitiateHiringReplacement").prop("checked") == true) {
        isintiatehr = true;
    }
    var separationtypeid = $("#ddlSeparationTypeID").val();
    if (separationrequestformid == null || separationrequestformid == '' || separationrequestformid == "" || separationrequestformid == undefined)
        separationrequestformid = 0;
    if (formprocessid == null || formprocessid == '' || formprocessid == "" || formprocessid == undefined)
        formprocessid = 0;

    var formData = new FormData();
    var separationrequestformmodel = {
        ID: separationrequestformid,
        EmployeeID: employeeid,
        FormProcessID: formprocessid,
        DateOfSeparation: $("#txtDateOfSeparation").val(),
        LastWorkingDay: $("#txtLastWorkingDay").val(),
        NoticedPeriodDays: $("#txtNoticedPeriodDays").val(),
        SeparationTypeID: separationtypeid,
        InitiateHiringReplacement: isintiatehr,
        SupportingDocFileID: $("#hdnFileUpload_SD").val(),
        ReasonForSeparation: $("#txtReasonForSeparation").val(),
        Comments: comments,
        SeparationRequestFormID: separationrequestformid,
        ReqStatusID: $("#hdnReqStatusID").val()
    };
    formData.append("separationequestformModel", JSON.stringify(separationrequestformmodel));

    if ($("#fu_SD").get(0).files.length > 0) {
        fileJD = $("#fu_SD").get(0).files;
        formData.append("SupportingDocFile", fileJD[0]);
    }
    else
        formData.append("SupportingDocFile", null);
    
    $.ajax({
        url: '/SeparationRequestForm/SaveForm',
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData,
        success: function (result) {
            hideLoaderFrame();
            ShowMessage(result.CssClass, result.Message);
            if (result.CssClass != 'error') {
                location.reload();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

function ValidateData() {
    $("#txtuploadedMsgAdd_SD").css("color", "red");
    var filesd = $("#fu_SD").val();
    var filesdid = $("#hdnFileUpload_SD").val();
    //if the file is removed these hiddenfield will hold its ids
    var sdfiledeleteid = $("#hdnDeletedFileUpload_SD").val();
    var isvalid = true;
    var issdfile = true;

    //for update if file already there filejdid will have id value
    if (filesdid === '' || filesdid == '0') {
        if (filesd === '' || filesd === null || filesd === undefined) {
            isvalid = false;
            issdfile = false;
        }
    }
    if (sdfiledeleteid !== '0') {
        isvalid = false;
        issdfile = false;
    }

    var date1 = $("#txtDateOfSeparation").val();
    var date2 = $("#txtLastWorkingDay").val();


    if (date1 === '' || date1 === null || date1 === undefined) {
        $("#lblValidateDateOfSeparation").text("This field is mandatory.");
        isvalid = false;
    }
    if (date2 === '' || date2 === null || date2 === undefined) {
        $("#lblValidateLastWorkingDay").text("This field is mandatory.");
        isvalid = false;
    }

    if (isvalid)
        return true
    else {
        if (!issdfile) {
            $("#txtuploadedMsgAdd_SD").text("This field is mandatory.");
        }

        return false;
    }

}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);

}


