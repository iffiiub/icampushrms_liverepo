﻿$(document).ready(function () {
    bindSelectpicker(".selectpicker");
    bindDatePicker(".DOB");
    var reqstatusid = $("#hdnReqStatusID").val();
  
    $(document).on("change", "#ReJoiningDate", function () {
        if ($("#ReJoiningDate").val() == "" || $("#ReJoiningDate").val() == undefined) {
            $("#LeavesToAdjust").val(0);
        }
        else {
            CalculateAndSetRejoinDate();
        }
    });
  
    $(document).on("click", ".btnUpdate", function () {
        if ($("#ReJoiningDate").val() != "" && $("#ReJoiningDate").val() != undefined && $("#ReJoiningDate").val() != null && $("#ReJoiningDate").val() != '') {
            pageLoaderFrame();
            if ($("#frmEmployeeLeaveReJoining").valid()) {
                $.ajax({
                    dataType: 'json',
                    url: '/EmployeeLeaveRejoining/IsWorkFlowExists',
                    data: {
                        formID: $("#FormId").val(), companyIDs: $("#CompanyId").val()
                    },
                    success: function (result) {
                        if (result.InsertedRowId > 0) {
                            // hideLoaderFrame();
                            UpdateEmployeeLeaveReJoining(); //Save Form
                        }
                        else {
                            hideLoaderFrame();
                            ShowMessage(result.CssClass, result.Message);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
        }
        else {
            ShowMessage('error', "Please enter Rejoin date");
        }
    });

});

function UpdateEmployeeLeaveReJoining() {

    if ($("#ReJoiningDate").val() != "" && $("#ReJoiningDate").val() != undefined && $("#ReJoiningDate").val() != null && $("#ReJoiningDate").val() != '') {
        var noofdiffdays = ((moment($("#ReJoiningDate").val(), momentdateformat).toDate() - moment($("#LeaveStartDate").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24));
        if (parseInt(noofdiffdays) <= 0) {
            hideLoaderFrame();
            ShowMessage("error", "Rejoin date must be a date greater than leave from date.");
            //  return false;
        }
        else {


            CalculateAndSetRejoinDate();
            var latedays = $("#LeavesToAdjust").val();
            var leavetypeid = $("#ddlVacationType").val();
            var vtcategoryid = 0;
            var balance = 0;
            var LeaveBalanceInfo = GetLeaveBalanceInfoById(leavetypeid);
            if (LeaveBalanceInfo != null) {
                vtcategoryid = LeaveBalanceInfo.VTCategoryID;
                if (vtcategoryid == 1)
                    balance = parseFloat(LeaveBalanceInfo.RemainingLeave) + parseFloat(LeaveBalanceInfo.LapsingDays) + parseFloat(LeaveBalanceInfo.ExtendedLapsingDays);
                else
                    balance = parseFloat(LeaveBalanceInfo.RemainingLeave);
                //reversing 
                latedays = 0 - latedays;
                if (latedays > 0) {
                    if (vtcategoryid == 4 || vtcategoryid == 5 || vtcategoryid == 6 || vtcategoryid == 7) {
                        if (balance < latedays) {
                            hideLoaderFrame();
                            ShowMessage("error", "Your leave balance is (" + balance + ") days only.You cannot extend days more than your balance");
                            return false;
                        }
                    }
                    else if (vtcategoryid == 1) {
                        if (latedays > 4) {
                            hideLoaderFrame();
                            ShowMessage("error", "You cannot apply for late rejoining more then 4 days.");
                            return false;
                        }
                    }
                    else {
                        hideLoaderFrame();
                        ShowMessage("error", "Join date can't be extended");
                        return false;
                    }
                }

                var employeeLeaveRejoining = {
                    ID: $("#ID").val(),
                    FormProcessID: $("#FormProcessID").val(),
                    RequesterEmployeeID: $("#hdnRequesterEmployeeID").val(),
                    ReqStatusID: $("#hdnReqStatusID").val(),
                    RequestDate: $("#RequestDate").val(),
                    ReJoiningDate: $("#ReJoiningDate").val(),
                    LeavesToAdjust: $("#LeavesToAdjust").val(),
                    Comments: $("#txtComments").val()
                };
                //  pageLoaderFrame();
                $.ajax({
                    url: "/EmployeeLeaveRejoining/UpdateLeaveRejoiningRequest",
                    type: 'POST',
                    data: {
                        leaveRejoiningModel: JSON.stringify(employeeLeaveRejoining)
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.Success) {
                            hideLoaderFrame();
                            ShowMessage("success", data.Message);
                            if (data.CssClass != 'error') {
                                location.reload();
                            }
                        }
                        else {
                            ShowMessage("error", data.Message);
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            }
            else {
                hideLoaderFrame();
                ShowMessage("error", "Unable to get leave balance");
            }
        }
    }
}

function GetLeaveBalanceInfoById(LeaveTypeID) {

    var LeaveBalanceInfo = [];
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetLeaveBalanceInfoById',
        data: { LeaveTypeID: LeaveTypeID, RequesterEmployeeID: $("#hdnRequesterEmployeeID").val() },
        async: false,
        success: function (LeaveBalanceInfoModel) {
            LeaveBalanceInfo = LeaveBalanceInfoModel;            
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return LeaveBalanceInfo;
}

function GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate) {
    var vacationmodel = null;
    //  pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetValidLeaveRequestDates',
        data: { employeeID: employeeid, vacationTypeID: leavetypeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            vacationmodel = result;
            // hideLoaderFrame();
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
            // hideLoaderFrame();
        }
    });
    return vacationmodel;
}

function CalculateAndSetRejoinDate() {
    var employeeid = $("#hdnRequesterEmployeeID").val();
    var leavetypeid = $("#ddlVacationType").val();
    var noofdays = ((moment($("#ReJoiningDate").val(), momentdateformat).toDate() - moment($("#LeaveEndDate").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24)) - 1;
    var rejoindate = $("#ReJoiningDate").val();
    var fromdate, todate, leavestoadjust;
    if (noofdays > 0) {
        fromdate = moment($("#LeaveEndDate").val(), momentdateformat).add(1, 'days').format(momentdateformat);
        todate = moment(rejoindate, momentdateformat).add(-1, 'days').format(momentdateformat);
        var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
        $("#ReJoiningDate").val(moment(vacation.toDate, momentdateformat).add(1, 'days').format(momentdateformat));
        leavestoadjust = vacation.NoOfDays;
        leavestoadjust = 0 - leavestoadjust;
    }
    else if (noofdays < 0) {
        fromdate = moment($("#ReJoiningDate").val(), momentdateformat).format(momentdateformat); //.add(1, 'days')
        todate = $("#LeaveEndDate").val();
        var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
        $("#ReJoiningDate").val(vacation.fromDate);
        leavestoadjust = vacation.NoOfDays;
        // leavestoadjust = 0 - leavestoadjust;
    }
    else {
        leavestoadjust = 0;
    }
    $("#LeavesToAdjust").val(leavestoadjust);
}
