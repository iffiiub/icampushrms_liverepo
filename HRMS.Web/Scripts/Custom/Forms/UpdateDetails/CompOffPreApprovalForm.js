﻿$(document).ready(function () {
    
    var formprocessid = $("#txtFormProcessId").val();

    if ($("#txtAdditionalWorkingHours").is(":visible"))
        bindDecimalNumberOnly("#txtAdditionalWorkingHours", 1);

    $(".btnUpdate").click(function () {
        if (window.FormData !== undefined) {
            var $form = $("#frmCompOffPreApprovalForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            var IsValidFiles = true;
            IsValidFiles = ValidateData();

            if ($form.valid() && IsValidFiles) {
                pageLoaderFrame();
                UpdateForm($form);
            }
        }
    });

    $("#txtAdditionalWorkingHours").change(function () {
        $("#txtIsSaved").val("0");
    });
    checkCompOffLeaveRequestLoadValidation();
});

function UpdateForm($form) {
    var fileTF;
    var formData = new FormData($form[0]);
    var formInstanceId = $("#txtformInstanceId").val();
    //var isHrTeam = $("#txtIsHrTeam").val();
    formData.append("formInstanceId", formInstanceId);
    formData.append("Comments1", $("#txtComments").val());
    formData.append("FormProcessID1", $("#txtFormProcessId").val());
    var WorkingDate = $("#txtWorkingDate").val();
    formData.append("WorkingDate1", WorkingDate);
    formData.append("isHrGroup1", true);  
    if ($('#txtAdditionalWorkingHours').length > 0) {
        var AdditionalWorkingHours = $("#txtAdditionalWorkingHours").val();
        formData.append("AdditionalWorkingHours1", AdditionalWorkingHours);
    }
    if ($('#fu_TF').length > 0) {
        if ($("#fu_TF").get(0).files.length > 0) {
            fileTF = $("#fu_TF").get(0).files;
            formData.append("TimeSheetFile", fileTF[0]);
        }
    }

    var ajaxOptions = {
        url: '/CompOffPreApproval/UpdateForm',
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: formData
    }

    $.ajax(ajaxOptions).done(function (result) {
        hideLoaderFrame();
        $("#txtIsSaved").val(result.InsertedRowId);
        ShowMessage(result.CssClass, result.Message);
        console.log(result);
        if (result.CssClass != 'error') {
            location.reload();
        }
    });
}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);
    $("#txtIsSaved").val("0");
}

function ValidateData() {
    if ($("#fu_TF").is(":visible")) {
        var filepf = $("#fu_TF").val();
        var filepfid = $("#hdnFileUpload_TF").val();
        var pffiledeleteid = $("#hdnDeletedFileUpload_TF").val();
        var isvalid = true;
        var ispffile = true;
        if (filepfid === '' || filepfid == '0') {
            if (filepf === '' || filepf === null || filepf === undefined) {
                isvalid = false;
                ispffile = false;
            }
        }
        if (pffiledeleteid !== '0') {
            isvalid = false;
            ispffile = false;
        }
        if (isvalid)
            return true
        else {
            if (!ispffile) {
                $("#txtuploadedMsgAdd_TF").text("This field is mandatory.");
            }
            return false;
        }
    }
    else
        return true;
}

function checkCompOffLeaveRequestLoadValidation() {

    var employeeId = $("#txtEmployeeId").val();
    var id = $("#txtformInstanceId").val();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/CompOffPreApproval/CheckCompOffLeaveRequestLoadValidation',
        data: { employeeId: employeeId, id:id },
        success: function (result) {
            if (!result.Success) {
                $('#warningMessage').show();
                $('#lblCompOffPreApprovalNotification').html(result.Message);
                $(".btnUpdate").prop('disabled', true);
                $('#btnUpdate').attr("disabled", "disabled");
            }
            else {
                $('#warningMessage').hide();
                $(".btnUpdate").prop('disabled', false);
                $('#btnUpdate').prop("disabled", false);
            }        
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}