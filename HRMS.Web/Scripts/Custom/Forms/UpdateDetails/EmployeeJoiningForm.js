﻿$(document).ready(function () {
    var formid = 18;
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");    

    $('.btnUpdate').click(function () {      
        if (window.FormData !== undefined) {
            var fileOL;
            var $form = $("#frmEmployeeJoiningForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid()) {
             UpdateEmployeeJoiningRequest(); //Save Form                      
            }
        }
    });   
});

function UpdateEmployeeJoiningRequest() {
    if (window.FormData !== undefined) {
        var fileOL;
        var $form = $("#frmEmployeeJoiningForm");
        $.validator.unobtrusive.parse($form);
        $form.validate();
        if ($form.valid()) {
            pageLoaderFrame();
            //var formData = new FormData($form[0]);
            var formData = new FormData();
            if ($("#fu_OL").get(0).files.length > 0) {
                fileOL = $("#fu_OL").get(0).files;
                formData.append("OfferLetterFile", fileOL[0]);
            }
            else
                formData.append("OfferLetterFile", null);
            formData.append("ID", $("#hdnEmployeeJoiningID").val());
            formData.append("FormProcessID", $("#hdnFormProcessID").val());
            formData.append("JoiningDate", $("#txtJoiningDate").val());
           // formData.append("Comments", $("#txtComments").val());
            formData.append("OfferLetterFileID", $("#hdnFileUpload_OL").val());
            $.ajax({
                url: '/EmployeeJoiningForm/UpdateForm',
                type: "POST",
                contentType: false, // Not to set any content header  
                processData: false, // Not to process data  
                data: formData,
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (result.CssClass != 'error') {
                        location.reload();
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    // alert(err.statusText);
                    ShowMessage('error', err.statusText);
                }
            });
        }
    }

}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);

}
