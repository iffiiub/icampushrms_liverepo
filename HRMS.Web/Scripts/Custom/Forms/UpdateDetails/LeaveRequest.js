﻿var balancedigitafterdecimal = 2;
var leaveentitletypeid = 0;
var oldfromdate = "";
var oldtodate = "";
$(document).ready(function () {
    var formid = 6;
    var formprocessid = $("#hdnFormProcessID").val();

    $("#chkHalfday").attr("disabled", 'disabled');
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $('#hdnBalanceDays').val(0);
    $('#hdnVTCategoryID').val(0);

    $("#txtToDate").datepicker({
        dateFormat: HRMSDateFormat,
        onSelect: function (f, d, i) {
            if (d !== i.lastVal) {
                $('#txtToDate').trigger("change");
            }
        }
    }).data("datepicker");

    //$('#txtToDate').on("change", function () {
    //    var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
    //    if (NoOfDaysDiff > 0)
    //        CalculateDays();
    //    else {
    //        $('#txtRequestedDays').val(0);
    //        GetRemainingDays();
    //    }
    //});

    $("#txtFromDate").datepicker({
        dateFormat: HRMSDateFormat,
        onSelect: function (f, d, i) {
            if (d !== i.lastVal) {
                $("#txtFromDate").trigger("change");
            }
        }
    }).data("datepicker");

    //$("#txtFromDate").on("change", function () {
    //    var NoOfDaysDiff = ((moment($("#txtToDate").val(), 'DD/MM/YYYY').toDate() - moment($("#txtFromDate").val(), 'DD/MM/YYYY').toDate()) / (1000 * 60 * 60 * 24)) + 1;
    //    if (NoOfDaysDiff > 0)
    //        CalculateDays();
    //    else {
    //        //  ShowMessage("error", "From date should be less than to date.");
    //        $('#txtRequestedDays').val(0);
    //        GetRemainingDays();
    //    }
    //});

    var formprocessid = $("#hdnFormProcessID").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    var LeaveTypeID = $("#VacationTypeID").val();
    var fromdate = $("#txtFromDate").val();
    var todate = $("#txtToDate").val();

    $(':input').each(function () {
        if ($(this).prop("id") != 'ddlLeaveType') //not taking values from company id
            $(this).data('initialValue', $(this).val());
    });

    $('.btnSave').click(function () {
        SaveForm();
    });

    $('.leave-dates').on('mouseleave', function () {
        $("#txtFromDate").datepicker("setDate", $("#txtFromDate").val());
        $("#txtToDate").datepicker("setDate", $("#txtToDate").val());
    });
    $('#txtCancelComments').on("input", function () {
        var str = "Cancel Request Comments(max 2000 characters) ";
        var maxlength = $(this).attr("maxlength");
        var currentLength = $(this).val().length;
        var noofcharsleft;

        if (currentLength >= maxlength) {
            //console.log("You have reached the maximum number of characters.");
            $('#lblCancelRequestComments').text(str + "0 chars left");
        } else {
            // console.log(maxlength - currentLength + " chars left");
            noofcharsleft = maxlength - currentLength;
            $('#lblCancelRequestComments').text(str + " " + noofcharsleft + " chars left");
        }
    });

    $('.btnCancelRequest').on('click', function () {
      //  alert();
        var formProcessId = $("#hdnFormProcessID").val();
        var cancelComments = $("#txtCancelComments").val();
        var IsValidateComment = validateComments($("#txtCancelComments"));
        if (IsValidateComment) {
            pageLoaderFrame();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/EmployeeLeaveRequest/CancelLeaveRequest',
                data: { formProcessId: parseInt(formProcessId), cancelComments: cancelComments },
                success: function (result) {
                    hideLoaderFrame();

                    if (result.insertedRowId > 0) {
                        ShowMessage(result.result, result.resultMessage);
                        setTimeout(function () { location.reload(); }, 1000);
                    }
                    else {
                        ShowMessage('error', result.resultMessage);
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
        else
        {
            ShowMessage('error', 'Cancel Comments field is mandatory.');
        }
    });  
    $(".tasklistemployee").change(function () {
        //$('.tasklistemployee').on('change', function () {  

        var str = $(this).attr('id');
        var index = str.substring(str.length - 1, str.length);

        if (this.value != null && this.value != '' && this.value != undefined) {
            $("#txtTaskDetail" + index).removeAttr("readonly");

        }

        else {

            $("#txtTaskDetail" + index).val('');
            $("#txtTaskDetail" + index).attr("readonly", true);
            var div = $("#txtTaskDetail" + index).parent();
            div.find("span").text('');
        }



    });
   

    if ($("#ddlEmployee1").val()>0)
    {
        $("#ddlEmployee1").attr("disabled", true);
       // $("#txtTaskDetail1").addAttr("readonly", true);
    }
    if ($("#ddlEmployee2").val() > 0) {
        //$("#txtTaskDetail2").addAttr("readonly", true);
        $("#ddlEmployee2").attr("disabled", true);
    }
    if ($("#ddlEmployee3").val() > 0) {
        //  $("#txtTaskDetail3").addAttr("readonly", true);
        $("#ddlEmployee3").attr("disabled", true);
    }
    if ($("#ddlEmployee4").val() > 0) {
        // $("#txtTaskDetail4").addAttr("readonly", true);
        $("#ddlEmployee4").attr("disabled", true);
    }
    if ($("#ddlEmployee5").val() > 0) {
        // $("#txtTaskDetail5").addAttr("readonly", true);
        $("#ddlEmployee5").attr("disabled", true);
    }
  
});

function ValidateData() {
    $("#txtFromDatelbel").css("color", "red");
    $("#txtToDatelbel").css("color", "red");
    var date1 = $("#txtFromDate").val();
    var date2 = $("#txtToDate").val();
    //for update if file already there filejdid will have id value

    if (date1 === '' || date1 === null || date1 === undefined) {
        $("#txtFromDatelbel").text("This field is mandatory.");
        return false;
    }
    if (date2 === '' || date2 === null || date2 === undefined) {
        $("#txtToDatelbel").text("This field is mandatory.");
        return false;
    }
    var isvalidtask = true;
    var taskemp1 = $("#ddlEmployee1").val();
    if (taskemp1 != null && taskemp1 != '' && taskemp1 != undefined) {
        var Istaskemp1 = validateComments($("#txtTaskDetail1"));
        if (Istaskemp1)
            isvalidtask = true;
        else
            isvalidtask = false;
    }
    var taskemp2 = $("#ddlEmployee2").val();
    if (taskemp2 != null && taskemp2 != '' && taskemp2 != undefined) {
        var Istaskemp2 = validateComments($("#txtTaskDetail2"));
        if (Istaskemp2)
            isvalidtask = true;
        else
            isvalidtask = false;
    }
    var taskemp3 = $("#ddlEmployee3").val();
    if (taskemp3 != null && taskemp3 != '' && taskemp3 != undefined) {
        var Istaskemp3 = validateComments($("#txtTaskDetail3"));
        if (Istaskemp3)
            isvalidtask = true;
        else
            isvalidtask = false;
    }
    var taskemp4 = $("#ddlEmployee4").val();
    if (taskemp4 != null && taskemp4 != '' && taskemp4 != undefined) {
        var Istaskemp4 = validateComments($("#txtTaskDetail4"));
        if (Istaskemp4)
            isvalidtask = true;
        else
            isvalidtask = false;
    }
    var taskemp5 = $("#ddlEmployee5").val();
    if (taskemp5 != null && taskemp5 != '' && taskemp5 != undefined) {
        var Istaskemp5 = validateComments($("#txtTaskDetail5"));
        if (Istaskemp5)
            isvalidtask = true;
        else
            isvalidtask = false;
    }
    if (isvalidtask)
        return true;
    else
        return false;
   
    return true;
}

function SaveForm() {
    if (window.FormData !== undefined) {
        var fileMC;
        var HalfDay;
        var In;
        var Out;
        if (ValidateData()) {
            var $form = $("#frmEmployeeLeaveRequestForm");
            var formprocessid = $("#hdnFormProcessID").val();
            var reqstatusid = $("#hdnReqStatusID").val();
            var leavetypeid;
            leavetypeid = $("#hdnVacationTypeID").val();
            if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined)
                formprocessid = 0;
            $.validator.unobtrusive.parse($form);
            $form.validate();

            if ($form.valid()) {
                var employeeid = $("#hdnRequesterEmployeeID").val();
                var fromdate = $("#txtFromDate").val();
                var todate = $("#txtToDate").val();
                var vacation = GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate);
                var applieddays = vacation.NoOfDays;

                var actualrequestdays = $("#hdnRequestedDays").val();
                if (applieddays != actualrequestdays) {
                    ShowMessage("error", "Any modification in dates should be equal to Requested Days (" + actualrequestdays + ")");
                    //ShowMessage("error", "Requested days (" + applieddays + ") must be same as Initial Request Days(" + actualrequestdays + ")");
                    return false;
                }

                var NoOfDaysDiff = ((moment($("#txtToDate").val(), momentdateformat).toDate() - moment($("#txtFromDate").val(), momentdateformat).toDate()) / (1000 * 60 * 60 * 24)) + 1;
                if (parseInt(NoOfDaysDiff) <= 0) {
                    ShowMessage("error", "From date must be a date less than to date .");
                    return false;
                }

                var isleaveexists = IsLeaveDatesAlreadyExists();
                if (isleaveexists) {
                    ShowMessage("error", "Requested leave days are already taken, please select another date.");
                    return true;
                }

                var TaskHandOverEmp1 = $("#ddlEmployee1").val();
                var TaskHandOverEmp2 = $("#ddlEmployee2").val();
                var TaskHandOverEmp3 = $("#ddlEmployee3").val();
                var TaskHandOverEmp4 = $("#ddlEmployee4").val();
                var TaskHandOverEmp5 = $("#ddlEmployee5").val();
                var TaskDetail1 = $("#txtTaskDetail1").val();
                var TaskDetail2 = $("#txtTaskDetail2").val();
                var TaskDetail3 = $("#txtTaskDetail3").val();
                var TaskDetail4 = $("#txtTaskDetail4").val();
                var TaskDetail5 = $("#txtTaskDetail5").val();
                var TaskHandOverID1 = $("#txtTaskDetail1").attr("taskhandoverid");
                var TaskHandOverID2 = $("#txtTaskDetail2").attr("taskhandoverid");
                var TaskHandOverID3 = $("#txtTaskDetail3").attr("taskhandoverid");
                var TaskHandOverID4 = $("#txtTaskDetail4").attr("taskhandoverid");
                var TaskHandOverID5 = $("#txtTaskDetail5").attr("taskhandoverid");

                var updatedData = [];
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp1,
                    TaskDetail: TaskDetail1,
                    TaskHandOverID: TaskHandOverID1
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp2,
                    TaskDetail: TaskDetail2,
                    TaskHandOverID: TaskHandOverID2
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp3,
                    TaskDetail: TaskDetail3,
                    TaskHandOverID: TaskHandOverID3
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp4,
                    TaskDetail: TaskDetail4,
                    TaskHandOverID: TaskHandOverID4
                });
                updatedData.push({
                    TaskAssignEmpID: TaskHandOverEmp5,
                    TaskDetail: TaskDetail5,
                    TaskHandOverID: TaskHandOverID5
                });
               
                var formData = new FormData($form[0]);
                formData.append("updatedData", JSON.stringify(updatedData));
                var FromDate = $('#txtFromDate').val();
                var ToDate = $('#txtToDate').val();               

                pageLoaderFrame();
                $.ajax({
                    url: '/EmployeeLeaveRequest/UpdateLeaveRequest',
                    type: "POST",
                    contentType: false, // Not to set any content header  
                    processData: false, // Not to process data  
                    data: formData,
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.CssClass != 'error') {
                            location.reload();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame()
                        ShowMessage('error', err.statusText);
                    }
                });
            }
        }
    }
}

function IsLeaveDatesAlreadyExists() {
    var isexists = false;
    var fromdate = $("#txtFromDate").val();
    var todate = $("#txtToDate").val();
    var employeeid = $("#hdnRequesterEmployeeID").val();
    var LeaveRequestID = $("#ID").val();
    $.ajax({
        // dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/IsLeaveDatesAlreadyExistsForCurrentReq',
        data: { employeeID: employeeid, fromDate: fromdate, toDate: todate, LeaveRequestID: LeaveRequestID },
        async: false,
        success: function (result) {
            if (result.Success == true) {

                isexists = result.Success;
            }
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return isexists;
}

function GetValidLeaveRequestDates(employeeid, leavetypeid, fromdate, todate) {
    var vacationmodel = null;
    // pageLoaderFrame();
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/EmployeeLeaveRequest/GetValidLeaveRequestDates',
        data: { employeeID: employeeid, vacationTypeID: leavetypeid, fromDate: fromdate, toDate: todate },
        async: false,
        success: function (result) {
            vacationmodel = result;
            // hideLoaderFrame();
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
            //  hideLoaderFrame();
        }
    });
    return vacationmodel;
}