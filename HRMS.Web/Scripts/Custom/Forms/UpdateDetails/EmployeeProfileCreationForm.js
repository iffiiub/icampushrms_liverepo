﻿var AssetsAssignData = [];
var selectList = "";
var i = 0;
var assetDatatable;
var formid = 4;
$(document).ready(function () {
    bindCharsOnly(".charsOnly");
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    $('#ddlNationality').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '300', includeSelectAllOption: true });
    BindAssetTypes();
    initAssetsAssignDetailsGrid();

    if ($("#ddlContractStatus").val() === 'Family')
        $("#divSpouse").show();
    else
        $("#divSpouse").hide();

    if ($("#ddlUserType").val() == 5) {
        $("#divPdrpForm").show();
    }
    else {
        $("#divPdrpForm").hide();
    }

    if ($("#ddlHealthInsurance").val().toLowerCase() === 'true') {
        $("#divInsuranceCategory").show();
        $("#divInsuranceEligibility").show();
    }
    else {
        $("#divInsuranceCategory").hide();
        $("#divInsuranceEligibility").hide();
        
    }
    if ($("#ddlAccommodation").val().toLowerCase() === 'true') {
        $("#divAccommodationType").show();
      
    }
    else {
        $("#divAccommodationType").hide();       

    }   
    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });

    i = parseInt($("#hdnRowCnt").val()) + 1;
   
    $(document).on('keypress', '#EmployeeAlternativeID', function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            ShowMessage("error", "Please enter only numeric.");
            return false;
        }
    });

    $(document).on('paste', '#EmployeeAlternativeID', function (e) {
        if (isNaN(parseInt(e.originalEvent.clipboardData.getData('Text')))) {
            ShowMessage("error", "Please enter only numeric.");
            return false;
        }
    });
  
    $(".selectpicker").selectpicker('refresh');
});

//$(document).on("change", "#ddlRequest", function () {

//    loadRequestDeatails($(this).val());

//});

$(document).on("change", "#ddlNationality", function () {
    var selected = $("#ddlNationality option:selected");
    ValidateNationalityDdl();
    $("#ddlDefaultNationality").empty();
    $("#ddlDefaultNationality").append('<option value="">Select Default Nationality</option>');
    selected.each(function () {
        $("#ddlDefaultNationality").append('<option value="' + $(this).val() + '">' +
            $(this).text() + '</option>');
    });
    RefreshSelect("#ddlDefaultNationality");
});

$(document).on("change", "#ddlDestCountry", function () {
    if ($(this).val() != "") {
        $.ajax({
            url: "/EmployeeProfileCreationForm/GetAirPortListByCountryId",
            type: "GET",
            data: { countryId: $(this).val() },
            success: function (result) {
                $("#ddlAirportList").empty();
                $("#ddlAirportList").append('<option value="">Select Airport</option>');
                $.each(result, function (i, airport) {
                    $("#ddlAirportList").append('<option value="' + airport.id + '">' +
                        airport.text + '</option>');
                });
                RefreshSelect("#ddlAirportList");
            },
            failure: function (data) { globalFunctions.onFailure(); }
        });
    }
});

$(document).on("change", "#ddlDestCountry", function () {
    loadCityByCountryId("ddlDestCountry", "ddlDestCity");
});

$(document).on("change", "#ddlBirthCountry", function () {
    loadBirthPlaceByCountryId("ddlBirthCountry", "ddlBirthCity");
});

$(document).on("change", "#fu_OfferLetter", function () {
    var file = this.files[0];
    fileName = file.name;
    size = file.size;
    type = file.type;
    var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
    $("#txtuploadedMsgAdd_OfferLetter").css("color", "red");

    if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
        if (size <= 2097152) {
            $("#txtuploadedMsgAdd_OfferLetter").text("");
            $("#txtuploadedMsgAdd_OfferLetter").hide();
            $("#divBrowse_OfferLetter").hide();
            $("#btnRemove_OfferLetter").show();
            $("#file_caption_name_OfferLetter").show();
            $("#file_caption_name_OfferLetter").html(fileName.substring(0, 15));
            //$("#file_caption_id_OfferLetter").html(fileName.substring(0, 15));
            $("#chkFileUpload_OfferLetter").val("2");
            var fileJD = URL.createObjectURL(event.target.files[0]);
            $('#divPreview_OfferLetter').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');
        }
        else {
            $("#txtuploadedMsgAdd_OfferLetter").text("Maximum 2MB file size is allowed to upload.");
            $("#divAddInforamtionDialog_OfferLetter").hide();
            $("#divPreview_OfferLetter").hide();
        }
    }
    else {
        $("#txtuploadedMsgAdd_OfferLetter").text("Only JPG,JPEG,PNG,GIF,PDF,XLS,XLSX,PPT,PPTX,DOC and DOCX extension files are allowed to upload.");
        $("#divAddInforamtionDialog_OfferLetter").hide();
    }
});

$(document).on("change", "#fu_InterviewFile", function () {
    var file = this.files[0];
    fileName = file.name;
    size = file.size;
    type = file.type;
    var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
    $("#txtuploadedMsgAdd_InterviewFile").css("color", "red");

    if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
        if (size <= 2097152) {
            $("#txtuploadedMsgAdd_InterviewFile").text("");
            $("#txtuploadedMsgAdd_InterviewFile").hide();
            $("#divBrowse_InterviewFile").hide();
            $("#btnRemove_InterviewFile").show();
            $("#file_caption_name_InterviewFile").show();
            $("#file_caption_name_InterviewFile").html(fileName.substring(0, 15));
            //$("#file_caption_id_InterviewFile").html(fileName.substring(0, 15));
            $("#chkFileUpload_InterviewFile").val("2");
            var fileJD = URL.createObjectURL(event.target.files[0]);
            $('#divPreview_InterviewFile').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');
        }
        else {
            $("#txtuploadedMsgAdd_InterviewFile").text("Maximum 2MB file size is allowed to upload.");
            $("#divAddInforamtionDialog_InterviewFile").hide();
            $("#divPreview_InterviewFile").hide();
        }
    }
    else {
        $("#txtuploadedMsgAdd_InterviewFile").text("Only JPG,JPEG,PNG,GIF,PDF,XLS,XLSX,PPT,PPTX,DOC and DOCX extension files are allowed to upload.");
        $("#divAddInforamtionDialog_InterviewFile").hide();
    }
});

$(document).on("click", ".btnUpdate", function () {
    var isPersonalValid = 1;
    var errorCount = 0;
    var isfilesSelected = filesValidateData();
    var isValid = ValidateNationalityDdl();
    var groupId = $("#hdnGroupId").val();
    if ($("#frmEmployeeProfileCreation").valid() && isfilesSelected && isValid) {
        if (groupId == 11)
            CheckEmployeeValidation(isPersonalValid, errorCount);
        else
            UpdateEmployeeProfileCreationForm(); //Update Form
    }
});

$("#ddlContractStatus").on("change",function () {
    if ($(this).val() === 'Family') {
        $("#divSpouse").show();
    } else {
        $("#ddlFamilySpouse").val('');
        $("#divSpouse").hide();
    }
});
$("#ddlUserType").on("change", function () {
    if ($("#ddlUserType").val() == 5) {
        $("#divPdrpForm").show();
    }
    else {
        $("#divPdrpForm").hide();
    }
});
$("#ddlHealthInsurance").on("change", function () {
    if ($("#ddlHealthInsurance").val().toLowerCase() === 'true') {
        $("#divInsuranceCategory").show();
        $("#divInsuranceEligibility").show();
    }
    else {
        $("#divInsuranceCategory").hide();
        $("#divInsuranceEligibility").hide();

    }
});
$("#ddlAccommodation").on("change", function () {
    if ($("#ddlAccommodation").val().toLowerCase() === 'true') {
        $("#divAccommodationType").show();

    }
    else {
        $("#divAccommodationType").hide();

    }
});

function UpdateEmployeeProfileCreationForm() {
    var isfilesSelected = filesValidateData();
    var isITGroup = $("#hdnIsITGroup").val();
    var groupID = parseFloat($("#hdnGroupId").val());
    if (isITGroup != null && isITGroup != undefined && isITGroup != '' && isITGroup != "")
        isITGroup = parseFloat(isITGroup);
    else
        isITGroup = 0;
    if (groupID != null && groupID != undefined && groupID != '' && groupID != "")
        groupID = parseFloat(groupID);
    else
        groupID = 0;

    if ($("#frmEmployeeProfileCreation").valid() && isfilesSelected) {
        var InterviewFile, OfferLetter, ManPowerFile, AssetsAssignData;
        if ($("#fu_InterviewFile").get(0).files.length > 0) {
            InterviewFile = $("#fu_InterviewFile").get(0).files[0];
        }
        else
            InterviewFile = null;

        if ($("#hdnIsITGroup").val() == "0" && $("#fu_OfferLetter").get(0).files.length > 0) {
            OfferLetter = $("#fu_OfferLetter").get(0).files[0];
        }
        else
            OfferLetter = null;

        if ($("#hdnIsITGroup").val() == "1") {
            AssetsAssignData = getAllAssetsDetails();
            AssetsAssignData = JSON.stringify(AssetsAssignData);
        }
        else
            AssetsAssignData = null;


        var formData = new FormData();
        formData.append("ID", $("#ID").val());
        formData.append("FormId", formid);
        formData.append("FormProcessID", $("#FormProcessID").val());
        formData.append("RequestID", $("#ddlRequest").val());
        formData.append("FirstName", $("#FirstName").val());
        formData.append("MiddleName", $("#MiddleName").val());
        formData.append("SurName", $("#SurName").val());
        formData.append("JoiningDate", $("#JoiningDate").val());
        formData.append("SalaryBasisID", $("#ddlSalaryBasis").val());
        formData.append("CostCenter", $("#txtCostCenter").val());
        formData.append("CostCenterCode", $("#txtCostCenterCode").val());
        formData.append("LocationCode", $("#txtLocationCode").val());
        formData.append("OfficeLocation", $("#txtOfficeLocation").val());
        formData.append("ProbationPeriod", $("#ddlProbationPeriod").val());
        formData.append("SupervisorID", $("#ddlSupervisor").val());
        formData.append("CountryID", $("#ddlDestCountry").val());
        formData.append("CityID", $("#ddlDestCity").val());
        formData.append("AirportListID", $("#ddlAirportList").val());
        formData.append("LeaveEntitleDaysID", $("#ddlLeaveEntitle").val());
        formData.append("LeaveEntitleTypeID", $("#ddlLeaveEntitleType").val());
        formData.append("NationalityID", $("#ddlNationality").val());
        formData.append("DefaultNationalityID", $("#ddlDefaultNationality").val());
        formData.append("PassportNo", $("#PassportNo").val());
        formData.append("PassportIssueDate", $("#PassportIssueDate").val());
        formData.append("PassportExpiryDate", $("#PassportExpiryDate").val());
        formData.append("DOB", $("#DOB").val());
        formData.append("BirthCountryID", $("#ddlBirthCountry").val());
        formData.append("BirthCityID", $("#ddlBirthCity").val());
        formData.append("GenderID", $("#ddlGender").val());
        formData.append("ReligionID", $("#ddlReligion").val());
        formData.append("LanguageID", $("#ddlLanguage").val());
        formData.append("MotherName", $("#MotherName").val());
        formData.append("FatherName", $("#FatherName").val());
        formData.append("MobileNumber", $("#MobileNumber").val());
        formData.append("LandlinePhone", $("#LandlinePhone").val());
        formData.append("MaritalStatusID", $("#ddlMaritalStatus").val());
        formData.append("Extension", $("#Extension").val());
        formData.append("WorkEmailID", $("#WorkEmailID").val());
        formData.append("InterviewFile", InterviewFile);
        formData.append("OfferLetter", OfferLetter);
        formData.append("InterviewFileID", $("#hdnFileUpload_InterviewFile").val());
        formData.append("OfferLetterFileID", $("#hdnFileUpload_OfferLetter").val());
        formData.append("CompanyID", $("#CompanyID").val());
        formData.append("NoticedPeriod", $("#ddlNoticePeriod").val());
        formData.append("HRContractTypeID", $("#ddlHRContractType").val());
        formData.append("Comments", $("#txtComments").val());
        formData.append("ReqStatusID", $("#hdnReqStatusID").val());
        formData.append("RequesterEmployeeID", $("#hdnRequesterEmployeeID").val());
        formData.append("AssetsAssignData", AssetsAssignData);
        if ($("#EmployeeAlternativeID").val() != undefined)
            formData.append("EmployeeAlternativeID", $("#EmployeeAlternativeID").val());
        else
            formData.append("EmployeeAlternativeID", "");
        // Employee Details
        formData.append("ContractStatus", $("#ddlContractStatus").val());
        formData.append("EmploymentModeId", $("#ddlEmploymentMode").val());
        formData.append("FamilySpouse", $("#ddlFamilySpouse").val());
        formData.append("UserTypeID", $("#ddlUserType").val());
        formData.append("AnnualAirTicket", $("#ddlAnnualAirTicket").val());
        formData.append("EmployeeJobCategoryID", $("#ddlJobCategoryID").val());
        formData.append("AirfareFrequencyID", $("#ddlAirfareFrequency").val());
        formData.append("DepartmentID", $("#ddlDepartment").val());
        formData.append("AirfareClassID", $("#ddlAirfareClass").val());
        formData.append("ProjectData", $("#txtProjectData").val());
        formData.append("HealthInsurance", $("#ddlHealthInsurance").val());       
        formData.append("LifeInsurance", $("#ddlLifeInsurance").val());
        formData.append("JobGradeID", $("#ddlJobGrade").val());
        formData.append("DivisionID", $("#DivisionID").val());
        formData.append("PositionID", $("#ddlPositionID").val());
        formData.append("PDRPFormID", $("#ddlForm").val());
        formData.append("InsuranceCategory", $("#ddlHealthInsurance").val() == "true" ? $("#ddlInsuranceCategory").val() : 0);
        formData.append("InsuranceEligibility", $("#ddlHealthInsurance").val() == "true" ? $("#ddlInsuranceEligibility").val() : 0);
        formData.append("Accommodation", $("#ddlAccommodation").val());
        formData.append("AccommodationType", $("#ddlAccommodation").val() == "true" ? $("#ddlAccommodationType").val() : 0);

        formData.append("IsUpdateDetailMode", 1);
        
        pageLoaderFrame();
        $.ajax({
            url: "/EmployeeProfileCreationForm/UpdateEmployeeProfileCreationForm",
            contentType: false,
            processData: false,
            type: "POST",
            data: formData,
            datatype: "json",
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.CssClass != 'error') {
                    location.reload();
                }

            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }

        });
    }
}

function loadCityByCountryId(countryId, cityId) {
    if ($('#' + countryId).val() != "") {
        $.ajax({
            url: "/EmployeeProfileCreationForm/GetCityList",
            type: "GET",
            data: { countryId: $('#' + countryId).val() },
            success: function (result) {
                $("#" + cityId).empty();
                $("#" + cityId).append('<option value="">Select City</option>');
                $.each(result, function (i, Cities) {
                    $("#" + cityId).append('<option value="' + Cities.CityId + '">' +
                        Cities.CityName + '</option>');
                });
                RefreshSelect("#" + cityId);
            },
            failure: function (data) { globalFunctions.onFailure(); }
        });
    }
}

function loadBirthPlaceByCountryId(countryId, cityId) {
    if ($('#' + countryId).val() != "") {
        $.ajax({
            url: "/EmployeeProfileCreationForm/GetBirthPlaceList",
            type: "GET",
            data: { countryId: $('#' + countryId).val() },
            success: function (result) {
                $("#" + cityId).empty();
                $("#" + cityId).append('<option value="">Select Birth Place</option>');
                $.each(result, function (i, Cities) {
                    $("#" + cityId).append('<option value="' + Cities.BirthPlaceID + '">' +
                        Cities.BirthPlaceName_1 + '</option>');
                });
                RefreshSelect("#" + cityId);
            },
            failure: function (data) { globalFunctions.onFailure(); }
        });
    }
}

function loadRequestDeatails(requestId) {
    pageLoaderFrame();
    $.ajax({
        url: "/EmployeeProfileCreationForm/Create",
        type: "GET",
        data: { requestId: requestId },
        success: function (result) {
            $("#employeeRequestDetails").html(result);
            bindSelectpicker(".selectpickerddl");
            bindDatePicker(".DOB");
            $('#ddlNationality').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '300', includeSelectAllOption: true });
            hideLoaderFrame();
        },
        failure: function (data) { globalFunctions.onFailure(); hideLoaderFrame(); }
    });
}

function loadRequestList() {
    $.ajax({
        url: "/EmployeeProfileCreationForm/GetRequestList",
        type: "GET",
        success: function (result) {
            $("#ddlRequest").empty();
            $("#ddlRequest").append('<option value="">Select Request ID</option>');
            $.each(result, function (i, request) {
                $("#ddlRequest").append('<option value="' + request.id + '">' +
                    request.text + '</option>');
            });
            RefreshSelect("#ddlRequest");
        },
        failure: function (data) { globalFunctions.onFailure(); }
    });
}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    if (fileid == '' || fileid == null || fileid == '0') {
        $("#btnRemove_" + ctrl).hide();
        $("#divBrowse_" + ctrl).show();
        $("#file_caption_name_" + ctrl).hide();
        $("#file_caption_name_" + ctrl).html('');
        $("#file_caption_id_" + ctrl).html('');
        $("#txtuploadedMsgAdd_" + ctrl).text("");
        $("#fu_" + ctrl).val("");
        $("#divPreview_" + ctrl).html("");
        $("#hdnFileUpload_" + ctrl).val("0");
    }
    else {
        $("#btnUploadAdd_" + ctrl).hide();
        $("#btnRemove_" + ctrl).hide();
        $("#divBrowse_" + ctrl).show();
        $("#file_caption_name_" + ctrl).hide();
        $("#file_caption_name_" + ctrl).html('');
        $("#file_caption_id_" + ctrl).html('');
        $("#txtuploadedMsgAdd_" + ctrl).text("");
        $("#divPreview_" + ctrl).empty();
        $("#fu_" + ctrl).val("");
    }
}

function getEmployeeProfileCreationFormData() {
    var model = {
        RequestID: $("#ddlRequest").val(),
        FirstName: $("#FirstName").val(),
        MiddleName: $("#MiddleName").val(),
        SurName: $("#SurName").val(),
        JoiningDate: $("#JoiningDate").val(),//Date
        SalaryBasisID: $("#ddlSalaryBasis").val(),
        CostCenter: $("#txtCostCenter").val(),
        CostCenterCode: $("#txtCostCenterCode").val(),
        LocationCode: $("#txtLocationCode").val(),
        OfficeLocation: $("#txtOfficeLocation").val(),
        ProbationPeriod: $("#ddlProbationPeriod").val(),
        SupervisorID: $("#ddlSupervisor").val(),
        CountryID: $("#ddlDestCountry").val(),
        CityID: $("#ddlDestCity").val(),
        AirportListID: $("#ddlAirportList").val(),
        LeaveEntitleDaysID: $("#ddlLeaveEntitle").val(),
        LeaveEntitleTypeID: $("#ddlLeaveEntitleType").val(),
        NationalityID: $("#ddlNationality").val(),
        DefaultNationalityID: $("#ddlDefaultNationality").val(),
        PassportNo: $("#PassportNo").text(),
        PassportIssueDate: $("#PassportIssueDate").val(),//Date
        PassportExpiryDate: $("#PassportExpiryDate").val(),//Date
        DOB: $("#DOB").val(),//Date
        BirthCountryID: $("#ddlBirthCountry").val(),
        BirthCityID: $("#ddlBirthCity").val(),
        GenderID: $("#ddlGender").val(),
        ReligionID: $("#ddlReligion").val(),
        LanguageID: $("#ddlLanguage").val(),
        MotherName: $("#MotherName").val(),
        FatherName: $("#FatherName").val(),
        MobileNumber: $("#MobileNumber").val(),
        LandlinePhone: $("#LandlinePhone").val(),
        MaritalStatusID: $("#ddlMaritalStatus").val(),
        Extension: $("#Extension").val(),
        WorkEmailID: $("#WorkEmailID").val(),
        FormId: $("#ddlForm").val()
    };
    return model;
}

function ValidateNationalityDdl() {
    var isValid = true;
    var field = $(".ColNationality .field-validation-error");
    if ($("#ddlNationality").val() === '' || $("#ddlNationality").val() === null || $("#ddlNationality").val() === undefined) {
        field = $(".ColNationality .field-validation-valid");
        field.attr("class", "field-validation-error");
        field.html("<span for='NationalityID'>This field is mandatory</span>");
        isValid = false;
    }
    else {
        field = $(".ColNationality .field-validation-error");
        field.attr("class", "field-validation-valid");
        field.html("");
    }
    return isValid;
}

function filesValidateData() {
    $("#txtuploadedMsgAdd_InterviewFile").css("color", "red");
    $("#txtuploadedMsgAdd_OfferLetter").css("color", "red");

    var isFilesSelected = true;
    var fileOfferLetterId = $("#hdnFileUpload_OfferLetter").val();
    var fileInterviewFileId = $("#hdnFileUpload_InterviewFile").val();
    //for update if file already there filejdid will have id value
    var fileInterviewFile = $("#fu_InterviewFile").val();
    if (fileInterviewFileId === '' || fileInterviewFileId == '0') {
        if (fileInterviewFile === '' || fileInterviewFile === null || fileInterviewFile === undefined) {
            $("#txtuploadedMsgAdd_InterviewFile").text("This field is mandatory");
            $("#txtuploadedMsgAdd_InterviewFile").show();
            isFilesSelected = false;
        }
    }
    var fileOfferLetter = $("#fu_OfferLetter").val();
    if (fileOfferLetterId === '' || fileOfferLetterId == '0') {
        if (fileOfferLetter === '' || fileOfferLetter === null || fileOfferLetter === undefined) {
            $("#txtuploadedMsgAdd_OfferLetter").text("This field is mandatory");
            $("#txtuploadedMsgAdd_OfferLetter").show();
            isFilesSelected = false;
        }
    }
    return isFilesSelected;
}

function initAssetsAssignDetailsGrid() {
    assetDatatable = $('#tblAssetsAssignDetails').DataTable(
        {
            columnDefs: [
                { width: "20%", targets: [0, 1, 2, 3, 4] },
                {
                    width: "20%", targets: 5, 'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('class', 'center-text-align col-md-3 padding-0');
                    }
                }
            ],
            "bFilter": true,
            "bInfo": true,
            "bSortCellsTop": true,
            "aaSorting": [[0, 'desc']]
        }
    );
}

function saveRow(source) {
    i = i + 1;
    var currentRow = $(source).closest("tr");

    var AssetsTypeId = currentRow.find("#ddlAssetsType").val();
    var AssetsType = currentRow.find("#ddlAssetsType option:selected").text();
    var ModelName = currentRow.find("#ModelName").val();
    var ModelNumber = currentRow.find("#ModelNumber").val();
    var SerialNumber = currentRow.find("#SerialNumber").val();
    var IssueDate = currentRow.find("#IssueDate").val();
    var AssetsID = currentRow.attr("data-assetsid");
    if (AssetsID == "" || AssetsID == undefined) {
        AssetsID = 0;
    }
    if (AssetsTypeId != undefined && AssetsTypeId > 0 && ModelName != undefined && ModelName != "" &&
        ModelNumber != undefined && ModelNumber != "" && SerialNumber != undefined && SerialNumber != "" && IssueDate != undefined && IssueDate != "") {
        var newRow = assetDatatable.row(currentRow).data([
            AssetsType,
            ModelName,
            ModelNumber,
            SerialNumber,
            IssueDate,
            '<div class="center-text-align">' +
            '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditRow(this)" id="btnEdit"  title="Edit"><i class="fa fa-pencil"></i> </a>' +
            '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(this)" title="Delete"><i class="fa fa-times"></i> </a>' +
            '</div>'
        ]).draw();
        var newNode = newRow.nodes();
        $(newNode).attr("data-assetsid", AssetsID);
        $(newNode).attr("data-assetstypeid", AssetsTypeId);
        $(newNode).attr("data-modelname", ModelName);
        $(newNode).attr("data-modelnumber", ModelNumber);
        $(newNode).attr("data-serialnumber", SerialNumber);
        $(newNode).attr("data-issuedate", IssueDate);
    }
    else {
        ShowMessage('error', 'Please fill all field');
        return false;
    }
}

function AddDataRow() {
    var newRow = assetDatatable.row.add([
        '<div class=""><select data-AssetsTypeId="" class="form-control selectpickerddl" data-live-search="true" id="ddlAssetsType">' + selectList + '</select></div>',
        '<div class=""><input class="form-control"  id="ModelName" /></div>',
        '<div class=""><input class="form-control"  id="ModelNumber" /></div>',
        '<div class=""><input class="form-control"  id="SerialNumber" /></div>',
        '<div class=""><div class="input-group">' +
        '<span class= "input-group-addon add-on"> <span class="fa fa-calendar"></span></span>' +
        '<input class="DOB form-control" id="IssueDate" />' +
         '</div >' +
        '</div >',
        '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>'
    ]).draw();
    var newNode = newRow.nodes();
    $(newNode).attr("data-AssetsID", 0);
    $(newNode).attr("data-AssetsTypeId", "");
    $(newNode).attr("data-ModelName", "");
    $(newNode).attr("data-ModelNumber", "");
    $(newNode).attr("data-SerialNumber", "");
    $(newNode).attr("data-IssueDate", "");

    bindDatePicker(".DOB");
    bindSelectpicker(".selectpickerddl");
}

function BindAssetTypes() {
    selectList = "<option value=''>Select Assets Type</option>";
    $.ajax({
        type: 'GET',
        url: '/EmployeeProfileCreationForm/GetAssetTypes',
        datatype: 'Json',
        success: function (data) {
            $.each(data, function (key, value) {
                selectList += '<option value=' + value.id + '>' + value.text + '</option>';
            });
        }
    });
}

function DeleteRow(source) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to delete record?" }).done(function () {
        var currentRow = $(source).closest("tr");
        assetDatatable.row(currentRow).remove().draw();
    });
}

function CancelNewRow(source) {
    var currentRow = $(source).closest("tr");
    assetDatatable.row(currentRow).remove().draw();
}

function SaveMultipleAddition() {
    var field;
    var allowanceId = $("#PaySalaryAllowanceID").val();
    var AddDate = $("#AdditionDate").val();
    var Cycles = $("#AdditionCycles").val();
    var desc = $("#Description").val();

    var ValidateCnt = 0;
    if (allowanceId == "") {
        ValidateCnt++;
        field = $("#PaySalaryAllowanceID").parent().parent().find(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='PaySalaryAllowanceID'>This field is mandatory</span>");

    }

    if (AddDate == "") {
        ValidateCnt++;
        field = $("#AdditionDate").next(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='AdditionDate'>Please enter valid data</span>");
    }
    if (Cycles == 0) {
        ValidateCnt++;
        field = $("#AdditionCycles").next(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='AdditionCycles'>Please enter valid data</span>");
    }
    if (Object.keys(AssetsAssignData).length == 0) {
        ValidateCnt++;
        ShowMessage("error", "Please enter addition data");
    }


    if (ValidateCnt == 0) {

        $.ajax({
            type: "POST",
            data: {
                cycles: Cycles,
                AllowanceId: allowanceId,
                AdditionDate: AddDate,
                description: desc,
                PayAdditions: JSON.stringify(AssetsAssignData)
            },
            url: '/PayAddition/SaveMultipleAddition',
            success: function (data) {
                $("#myModal").modal('hide');
                if (data.Success) {
                    if (data.CssClass == "Request") {
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                } else {
                    ShowMessage("error", data.Message);
                }
                getGrid($("#Empid").val());
                AssetsAssignData = [];
            },
            error: function (xhr) {
            }
        });
    }
}

function EditRow(source) {
    var currentRow = $(source).closest("tr");
    var assetsTypeId = $(currentRow).attr("data-assetstypeid");
    var AssetsID = $(currentRow).attr("data-assetsid");
    var ModelName = $(currentRow).attr("data-modelname");
    var ModelNumber = $(currentRow).attr("data-modelnumber");
    var SerialNumber = $(currentRow).attr("data-serialnumber");
    var IssueDate = $(currentRow).attr("data-issuedate");
    currentRow.find('td:eq(0)').html('<div class=""><select class="form-control selectpickerddl" data-assetstypeid="" data-live-search="true" id="ddlAssetsType" name="AssetsTypeID">' + selectList + '</select></div>');
    currentRow.find('td:eq(1)').html('<div class=""><input class="form-control" id="ModelName" name="ModelName" type="text" value=""></div>');
    currentRow.find('td:eq(2)').html('<div class=""><input class="form-control" id="ModelNumber" name="ModelNumber" type="text" value=""></div>');
    currentRow.find('td:eq(3)').html('<div class=""><input class="form-control" id="SerialNumber" name="SerialNumber" type="text" value=""></div>');
    currentRow.find('td:eq(4)').html('<div class="">' +
                                         '<div class="input-group">' +
                                            '<span class="input-group-addon add-on"><span class="fa fa-calendar"></span></span>' +
                                            '<input class="DOB form-control" id="IssueDate" name="IssueDate" placeholder="To be filled by IT" type="text" value="">' +
                                         '</div>' +
                                     '</div>');    
    currentRow.find('td:eq(5)').html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');
    currentRow.find('#ddlAssetsType').val(assetsTypeId);
    currentRow.find("#ModelName").val(ModelName);
    currentRow.find("#ModelNumber").val(ModelNumber);
    currentRow.find("#SerialNumber").val(SerialNumber);
    currentRow.find("#IssueDate").val(IssueDate);
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
}

function getAllAssetsDetails() {
    var ProfileAssignAssetsModel = new Array();
    var rows = $('#tblAssetsAssignDetails > tbody tr');
    if (rows.length > 0) {
        $(rows).each(function (i, item) {
            var AssetsTypeID = $(item).attr("data-assetstypeid");
            var AssetsID = $(item).attr("data-assetsid");
            var ModelName = $(item).attr("data-modelname");
            var ModelNumber = $(item).attr("data-modelnumber");
            var SerialNumber = $(item).attr("data-serialnumber");
            var IssueDate = $(item).attr("data-issuedate");
            if (AssetsID == "" || AssetsID == undefined) {
                AssetsID = 0;
            }
            var AssetDetails = {};
            if (AssetsTypeID != undefined && AssetsTypeID > 0 && ModelName != undefined && ModelName != "" &&
                ModelNumber != undefined && ModelNumber != "" && SerialNumber != undefined && SerialNumber != "" && IssueDate != undefined && IssueDate != "") {
                AssetDetails.AssetsID = AssetsID;
                AssetDetails.AssetsTypeID = AssetsTypeID;
                AssetDetails.ModelName = ModelName.toString();
                AssetDetails.ModelNumber = ModelNumber.toString();
                AssetDetails.SerialNumber = SerialNumber.toString();
                AssetDetails.IssueDate = IssueDate.toString();
                ProfileAssignAssetsModel.push(AssetDetails);
            }
        });

    }
    return ProfileAssignAssetsModel;
}

function CheckEmployeeValidation(isPersonalValid, errorCount) {
    if (isPersonalValid == 0 || errorCount > 0) {
        ShowMessage("error", "Please fill mandatory fields");
    }
    else {
        var employeeid = $("#EmployeeAlternativeID").val();
        var instanceid = $("#ID").val();
        if (employeeid.trim() != '0') {
            $.ajax({
                type: 'POST',
                data: { employeeAlternativeID: employeeid, profileRequestInstanceID: instanceid },
                url: '/EmployeeProfileCreationForm/IsEmployeeAlternativeIDExists',
                success: function (data) {
                    if (parseInt(data.InsertedRowId) <= 0) {
                        // ShowMessage("error", "Employee ID is already assigned with another employee, Please enter unique Employee ID.");
                        ShowMessage("error", data.Message);
                        return false;
                    }
                    else {

                        UpdateEmployeeProfileCreationForm();
                    }
                },
                error: function (data) { }
            });
        }
        else {
            ShowMessage("error", "Employee ID can't be Zero");
        }
    }
}