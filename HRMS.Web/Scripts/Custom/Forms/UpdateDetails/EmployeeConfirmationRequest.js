﻿$(document).ready(function () {
    bindSelectpicker('.selectpickerddl');
    var EvalTypeId = $('option:selected', this).val();
    if (EvalTypeId == 3)
        $("#ProbationExtensionId").removeClass("hidden");

    var OfferID = $("#OfferLetterFileID").val();
    if (OfferID > 0) {
        $("#divUploadOfferLetter").removeClass("hidden");
    }

    $("#TypeEvaluationID").change(function () {
        var selectedEvalType = $('option:selected', this).val();
        if (selectedEvalType == 3)
            $("#ProbationExtensionId").removeClass("hidden");
        else
            $("#ProbationExtensionId").addClass("hidden");
    });

    $('input[type=radio][name=changeOfferId]').change(function () {
        if (this.value == '1') {
            $("#divUploadOfferLetter").removeClass("hidden");
        }
        else if (this.value == '2') {
            $("#divUploadOfferLetter").addClass("hidden");
        }
    });

    $(':input').each(function () {
        $(this).data('initialValue', $(this).val());
    });  

    $('.btnUpdate').click(function () {
        pageLoaderFrame();
        var formid = 5;
        var companyIds = $("#hdnCompanyID").val()
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/EmployeeConfirmationRequest/IsWorkFlowExists',
            data: { formID: formid, companyIDs: companyIds },
            success: function (result) {               
                if (result.InsertedRowId > 0) {                   
                    if (window.FormData !== undefined) {
                        var $form = $("#ConfirmationRequestFrom");
                        $.validator.unobtrusive.parse($form);
                        $form.validate();
                        var isValidDdls = isValidOptionDdl();
                        if ($form.valid() && isValidDdls) {
                            if (!$("#divUploadOfferLetter").hasClass("hidden") && !ValidateData()) {
                                hideLoaderFrame();
                                return false;
                            }
                            var OrifileOL;
                            var ID = $("#ID").val();
                            var RequestInitialize = $("#RequestInitialize").val();
                            var hdnformProcessID = $("#hdnformProcessID").val();
                            var RequesterEmployeeID = $("#RequesterEmployeeID").val();
                            var OfferLetterFileID = $("#OfferLetterFileID").val();
                            var FinalRatingScaleID = $("#FinalRatingScaleID").val();
                            var TypeEvaluationID = $("#TypeEvaluationID").val();
                            var ExtendedProbationPeriod = 0;
                            var ImprovePerformanceDetail = $("#ImprovePerformanceDetail").val();
                            var TrainingExpDetail = $("#TrainingExpDetail").val();
                            var Comments = $("#txtComments").val();
                            var changeOfferId = $('input[type=radio][name=changeOfferId]:checked').val();
                            var reqstatusid = $("#hdnReqStatusID").val();

                            if (!$("#ProbationExtensionId").hasClass("hidden")) {
                                ExtendedProbationPeriod = $("#ExtendedProbationPeriod").val()
                            }
                            if ($("#fu_OL").get(0).files.length > 0) {
                                OrifileOL = $("#fu_OL").get(0).files;
                                OrifileOL = OrifileOL[0];
                            }
                            else
                                OrifileOL = null;

                            var AllJobEvalData = [];
                            var selectedJobEval = $('select.jobevalddl option:selected')
                            $(selectedJobEval).each(function (id, option) {
                                AllJobEvalData.push({
                                    JobEvaluationID: $(this).data('jobevalid'),
                                    RatingScaleID: option.value
                                });

                            });                            

                            var AllCodeConductData = [];
                            var selectedCodeConduct = $('select.codeconductddl option:selected')
                            $(selectedCodeConduct).each(function (id, option) {
                                AllCodeConductData.push({
                                    CodeOfConductID: $(this).data('codeconductid'),
                                    RatingScaleID: option.value
                                });

                            });                           

                            var AllOtherOptionData = [];
                            var selectedOtherOption = $('select.otheroptionddl option:selected')
                            $(selectedOtherOption).each(function (id, option) {
                                AllOtherOptionData.push({
                                    OthersOptionID: $(this).data('otheroptionid'),
                                    RatingScaleID: option.value
                                });

                            });                          

                            //---------------------------------------------
                            var confirmationDetailModel = {
                                ID: ID,
                                FinalRatingScaleID: FinalRatingScaleID,
                                TypeEvaluationID: TypeEvaluationID,
                                ExtendedProbationPeriod: ExtendedProbationPeriod,
                                ImprovePerformanceDetail: ImprovePerformanceDetail,
                                TrainingExpDetail: TrainingExpDetail,
                                Comments: Comments,
                                OfferLetterFileID: OfferLetterFileID,
                                RequesterEmployeeID: RequesterEmployeeID,
                                RequestInitialize: RequestInitialize,
                                FormProcessID: hdnformProcessID,
                                ReqStatusID: reqstatusid,
                                lstJobEvalModel: AllJobEvalData,
                                lstConductCode: AllCodeConductData,
                                lstOtherOption: AllOtherOptionData
                            }
                            //---------------------------------------------                            
                            var formData = new FormData($form[0]);
                            formData.append("confirmationDetailModel", JSON.stringify(confirmationDetailModel));
                            formData.append("OriginalOfferLetter", OrifileOL);

                            $.ajax({
                                url: '/EmployeeConfirmationRequest/ReInitializeConfirmationRequest',
                                type: 'POST',
                                contentType: false, // Not to set any content header  
                                processData: false, // Not to process data  
                                data: formData,
                                success: function (result) {
                                    ShowMessage(result.CssClass, result.Message);
                                    hideLoaderFrame();
                                    if (result.CssClass != 'error') {
                                        location.reload();
                                    }
                                },
                                error: function (err) {
                                    hideLoaderFrame();
                                    ShowMessage('error', err.statusText);
                                }
                            });                           
                        }
                        else
                            hideLoaderFrame();
                    }
                }

                else {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });

    });

    $("#fu_OL").change(function () {
        var file = this.files[0];
        fileName = file.name;
        size = file.size;
        type = file.type;
        var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
        $("#txtuploadedMsgAdd_OL").css("color", "red");

        if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
            if (size <= 2097152) {
                $("#txtuploadedMsgAdd_OL").text("");
                $("#divBrowse_OL").hide();
                //$jquery("#btnUploadAdd_OL").show();
                $("#btnRemove_OL").show();
                $("#file_caption_name_OL").show();
                $("#file_caption_id_OL").html(fileName.split(".")[0].substring(0, 15));
                $("#chkFileUpload_OL").val("2");
                var fileJD = URL.createObjectURL(event.target.files[0]);
                $("#divPreview_OL").html("");
                $('#divPreview_OL').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');
                $("#hdnDeletedFileUpload_OL").val('0');

            }
            else {
                $("#txtuploadedMsgAdd_OL").text("Maximum 2MB file size is allowed to upload.");
                $("#divAddInforamtionDialog_OL").hide();
                $("#divPreview_OL").hide();
            }
        }
        else {
            $("#txtuploadedMsgAdd_OL").text("Only JPG,JPEG,PNG,GIF,PDF,XLS,XLSX,PPT,PPTX,DOC and DOCX extension files are allowed to upload.");
            $("#divAddInforamtionDialog_OL").hide();
        }
    });

});

function RemoveDocument(ctrl) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);

}

$(document).on("change", ".jobevalddl,.codeconductddl,.otheroptionddl", function () {
    validateOptionDropDown(this);
});

function validateOptionDropDown(element) {
    if ($(element).val() == "0") {
        if ($(element).parent().find('.field-validation-error').length == 0)
            $(element).parent().append('<span class=\"field-validation-error\">This field is mandatory</span>');
    }
    else if ($(element).parent().find('.field-validation-error').length > 0) {
        $(element).parent().find('.field-validation-error')[0].remove();
    }
}

function isValidOptionDdl() {
    var isValid = true;
    var RequestInitialize = $("#RequestInitialize").val();
    if (RequestInitialize.toLowerCase() == "false") {
        $(".jobevalddl").each(function (index, selectedDdl) {
            if ($(selectedDdl).val() == "0") {
                validateOptionDropDown(this);
                isValid = false;
            }
        });
        $(".codeconductddl").each(function (index, selectedDdl) {
            if ($(selectedDdl).val() == "0") {
                validateOptionDropDown(this);
                isValid = false;
            }
        });
        $(".otheroptionddl").each(function (index, selectedDdl) {
            if ($(selectedDdl).val() == "0") {
                validateOptionDropDown(this);
                isValid = false;
            }
        });
        if (!$("#divUploadOfferLetter").hasClass("hidden") && !$('#fu_OL').prop('disabled')) {
            var fileoc = $("#fu_OL").val();
            if (fileoc === '' || fileoc === null || fileoc === undefined) {
                $("#txtuploadedMsgAdd_OL").text("Please select new employment offer letter.");
                isValid = false;
            }
        }
    }
    return isValid;
}

function ValidateData() {
    var fu_OL = $("#fu_OL").val();
    var filempid = $("#hdnFileUpload_OL").val();     
    var olfiledeleteid = $("#divPreview_OL").html().trim();
   
    var isvalid = true;
    var isfileOL = true;
 
    if (olfiledeleteid == '') {
        isvalid = false;
        isfileOL = false;
    }

    if (isvalid)
        return true
    else {
        if (!isfileOL) {
            $("#txtuploadedMsgAdd_OL").text("This field is mandatory.");
        }       
        return false;
    }

}