﻿
$(document).ready(function () {
    $(document).on("change", ".OtherOptionsToStayIsEnable", function () {
        if ($("#rdOtherOptionsToStayEnable").prop("checked")) {
            $("#txtOtherOptionsToStay").removeAttr("disabled");
        }
        else {
            $("#txtOtherOptionsToStay").attr("disabled", "disabled");
            $("#txtOtherOptionsToStay").removeClass('input-validation-error');
            $(".OtherOptionsToStay").text('');
        }
    });

    $(document).on("change", ".OtherBenefitsOfferIsEnable", function () {
        if ($("#rdOtherBenefitsOfferEnable").prop("checked")) {
            $("#txtOtherBenefitsOffer").removeAttr("disabled");
        }
        else {
            $("#txtOtherBenefitsOffer").attr("disabled", "disabled");
            $("#txtOtherBenefitsOffer").removeClass('input-validation-error');
            $(".OtherBenefitsOffer").text('');
        }
    });

    $(document).on("change", ".NewJobGoodCompanyIsEnable", function () {
        if ($("#rdNewJobGoodCompanyEnable").prop("checked")) {
            $("#txtNewJobGoodCompany").removeAttr("disabled");
        }
        else {
            $("#txtNewJobGoodCompany").attr("disabled", "disabled");
            $("#txtNewJobGoodCompany").removeClass('input-validation-error');
            $(".NewJobGoodCompany").text('');
        }
    });

    $(document).on("change", ".GroupRecommendationIsEnable", function () {
        if ($("#rdGroupRecommendationEnable").prop("checked")) {
            $("#txtGroupRecommendation").removeAttr("disabled");
        }
        else {
            $("#txtGroupRecommendation").attr("disabled", "disabled");
            $("#txtGroupRecommendation").removeClass('input-validation-error');
            $(".GroupRecommendation").text('');
        }
    });

    $(document).on("click", ".btnSave", function () {
        if ($("#frmExitInterviewRequest").valid()) {

            var exitInterviewRequest = {
                EmployeeID: parseInt($("#EmployeeID").val()),
                OtherOptionsToStay: $("#rdOtherOptionsToStayEnable").is(":checked") == true ? $("#txtOtherOptionsToStay").val():null,
                OtherBenefitsOffer: $("#rdOtherBenefitsOfferEnable").is(":checked") == true ? $("#txtOtherBenefitsOffer").val():null,
                JobLeastInterest: $("#txtJobLeastInterest").val(),
                JobMostInterest: $("#txtJobMostInterest").val(),
                NewJobGoodCompany: $("#rdNewJobGoodCompanyEnable").is(":checked") == true ? $("#txtNewJobGoodCompany").val():null,
                NewSalaryAndPosition: $("#txtNewSalaryAndPosition").val(),
                GroupRecommendation: $("#rdGroupRecommendationEnable").is(":checked") == true ? $("#txtGroupRecommendation").val():null,
                ShareOtherThoughts: $("#txtShareOtherThoughts").val(),
                CompanyID: parseInt($("#CompanyID").val()),
                ExitInterviewReasonDetailsList: getPrimaryReason(),
                ExitInterviewSupervisorDetailsList: getSupervisorRating(),
                ExitInterviewJobDetailsList: getJobRating(),
                ExitInterviewServiceDetailsList: getServiceRating(),
                FormProcessID: parseInt($("#FormProcessID").val())
            };
            $.ajax({
                url: "/ExitInterviewRequest/SaveExitInterviewRequest",
                type: 'POST',
                data: {
                    exitInterviewRequest: JSON.stringify(exitInterviewRequest)
                },
                dataType: 'json',
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        hideLoaderFrame();
                        location.reload();
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                },
                error: function (err) {
                    hideLoaderFrame();
                    ShowMessage('error', err.statusText);
                }
            });
        }
    });
});

function getPrimaryReason() {
    var primaryreason = [];
    $(".primaryreason").each(function (index, element) {
        if ($(element).prop("checked")) {
            primaryreason.push({ PrimaryReasonID: $(element).data().primaryreasonid });
        }
    });
    return primaryreason;
}

function getSupervisorRating() {
    var supRating = [];
    $(".SupRatingOption").each(function (index, element) {
        if ($(element).prop("checked")) {
            supRating.push({ RatingOptionID: $(element).data().ratingoptionid, SupOptionID: $(element).data().supoptionid });
        }
    });
    return supRating;
}

function getJobRating() {
    var jobRating = [];
    $(".JobRatingOption").each(function (index, element) {
        if ($(element).prop("checked")) {
            jobRating.push({ RatingOptionID: $(element).data().ratingoptionid, JobOptionID: $(element).data().joboptionid });
        }
    });
    return jobRating;
}

function getServiceRating() {
    var serRating = [];
    $(".ServiceRatingOption").each(function (index, element) {
        if ($(element).prop("checked")) {
            serRating.push({ RatingOptionID: $(element).data().ratingoptionid, ServiceOptionID: $(element).data().serviceoptionid });
        }
    });
    return serRating;
}


