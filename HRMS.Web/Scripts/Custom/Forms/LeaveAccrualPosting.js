﻿$(document).ready(function () {

});
function Save() {
    var accrualdate = $('#txtAccrualDate').val();   
    pageLoaderFrame();
    $.ajax({
        url: "/LeaveAccrualPosting/Save",
        contentType: 'application/json; charset=utf-8',
        //type: "POST",
        data: { accrualDate: accrualdate },
        success: function (result) {
            if (result.Success) {
                ShowMessage("success", result.Message);               
                hideLoaderFrame();
            }
            else {
                ShowMessage("error", result.Message);
                hideLoaderFrame();
            }
        }
    });
}