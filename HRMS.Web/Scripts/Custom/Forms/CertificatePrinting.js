﻿$(document).ready(function () {
    loadCertificatePrintingGrid();
    $(document).on('change', '#ddlOrganization', function () {
        selectList = "";
        var count = 0;
        selectList = "<option value=\"\">All Employees</option>";
        var companyId = $(this).val();
        $.ajax({
            type: 'get',
            url: '/CertificatePrinting/LoadEmployeesbasedOnOrganization?companyId=' + companyId,
            datatype: 'Json',
            success: function (data) {
                $.each(data, function (key, value) {
                    count = count + 1;
                    selectList += '<option value=' + value.EmployeeId + '>' + value.FirstName + '</option>';
                });
                $("#ddlEmployee").html();
                $("#ddlEmployee").html(selectList);
                RefreshSelect("#ddlEmployee");

            }
        });
    });
});

function loadCertificatePrintingGrid() {
    var organizationId = $("#ddlOrganization").val();
    if (organizationId == undefined) {
        organizationId = "0";
    }
    var employeeId = $("#ddlEmployee").val();
    if (employeeId == undefined) {
        employeeId = "0";
    }
    oTableChannel = $('#tblCertificatePrintingGrid').dataTable({
        "sAjaxSource": "/CertificatePrinting/GetCertificatePrintingScreenData?organizationId=" + organizationId + "&employeeId=" + employeeId,
        "fnServerParams": function (aoData) {
        },
        "aoColumns": [
            { "mData": "ComapanyName", "sTitle": "Organization Name", 'sWidth': '15%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Emp ID", 'sWidth': '5%' },
            { "mData": "EmployeeName", "sTitle": "Requester Name", 'sWidth': '15%' },
            { "mData": "RequestID", "sTitle": "Request No.", 'sWidth': '5%' },
            { "mData": "RequestDate", "sTitle": "Request Date", 'sWidth': '10%' },
            { "mData": "DocumentType", "sTitle": "Document Type", 'sWidth': '5%' },
            { "mData": "CertificateType", "sTitle": "Certificate Type", 'sWidth': '10%' },
            { "mData": "CertificateAddress", "sTitle": "Certificate Address", 'sWidth': '10%' },
            { "mData": "Action", "sTitle": "Action", 'sWidth': '5%', "sClass": "text-center" },
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 25,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function printCertificateTemplate(formId, templateId, id) {
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/CertificatePrinting/PrintCertificateTemplate',
        data: { formId: formId, templateId: templateId, id: id },
        success: function (result) {
            if (result.success === true)
                window.location.assign('/CertificatePrinting/DownloadPrintCertificate?fileName=' + result.fileName + '&downloadFileName=' + result.downloadFileName);
            else
                ShowMessage('error', "some error has occured");
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}