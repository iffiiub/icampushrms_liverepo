﻿$(document).ready(function () {
    var formid = 6;
    bindSelectpicker(".selectpickerddl");
    bindDatePicker(".DOB");
    var reqstatusid = $("#hdnReqStatusID").val();
    if (reqstatusid == "3") {
        $(".btnApprove").remove();
        $(".btnReject").remove();
        $(".btnUpdate").prop("title", "Re-Submit");
        $('button#btnUpdate').text("Re-Submit");
    }
    $(':input').each(function () {
        //not taking values from company id
        $(this).data('initialValue', $(this).val());

    });

    $('.btnBack').click(function () {
        var formprocessid = $("#hdnFormProcessID").val();
        if (formprocessid <= 0) {
            location.href = "/PendingJoiningRequest/Index/";
        }
        else {
            BackToTaskList();
        }

    });

    $('.btnSave').click(function () {
        if (window.FormData !== undefined) {
            var fileOL;
            var $form = $("#frmEmployeeJoiningForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid()) {
                $.ajax({
                    dataType: 'json',
                    url: '/EmployeeProfileCreationForm/IsWorkFlowExists',
                    data: {
                        formID: $("#hdnFormId").val(), companyIDs: $("#hdnCompanyID").val()
                    },
                    success: function (result) {
                        hideLoaderFrame();
                        if (result.InsertedRowId > 0) {
                            SaveEmployeeJoiningRequest(); //Save Form
                        }
                        else {
                            ShowMessage(result.CssClass, result.Message);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });

            }
        }





    });

    $('.btnUpdate').click(function () {
        if (window.FormData !== undefined) {
            var fileOL;
            var $form = $("#frmEmployeeJoiningForm");
            $.validator.unobtrusive.parse($form);
            $form.validate();
            if ($form.valid()) {
                $.ajax({
                    dataType: 'json',
                    url: '/EmployeeProfileCreationForm/IsWorkFlowExists',
                    data: {
                        formID: $("#FormId").val(), companyIDs: $("#hdnCompanyId").val()
                    },
                    success: function (result) {
                        hideLoaderFrame();
                        if (result.InsertedRowId > 0) {
                            UpdateEmployeeJoiningRequest(); //Save Form
                        }
                        else {
                            ShowMessage(result.CssClass, result.Message);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });

            }
        }
    });

    $('.btnApprove').click(function () {
        pageLoaderFrame();
        var formprocessid = $("#hdnFormProcessID").val();
        var comments = $("#txtComments").val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/EmployeeJoiningForm/ApproveForm',
            data: { formProcessID: formprocessid, comments: comments },
            success: function (result) {
                hideLoaderFrame();
                ShowMessage(result.CssClass, result.Message);
                if (result.InsertedRowId > 0) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });
    });

    $('.btnReject').click(function () {
        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {

                var formprocessid = $("#hdnFormProcessID").val();
                var comments = $("#txtComments").val();
                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/EmployeeJoiningForm/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {
                        hideLoaderFrame();
                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.statusText);
                    }
                });
            });
        }
        else
            ShowMessage('error', 'Comments field is mandatory.');
    });
});

function SaveEmployeeJoiningRequest() {
    if (window.FormData !== undefined) {
        var fileOL;
        var formprocessid = $("#hdnFormProcessID").val();
        var EmployeeJoiningID = $("#hdnEmployeeJoiningID").val();
        var EmployeeId = $("#hdnEmployeeID").val();
        var $form = $("#frmEmployeeJoiningForm");
        $.validator.unobtrusive.parse($form);
        $form.validate();
        if ($form.valid()) {
            pageLoaderFrame();
            var formData = new FormData($form[0]);
            if (formprocessid == 0) {
                if ($("#fu_OL").get(0).files.length > 0) {
                    fileOL = $("#fu_OL").get(0).files;
                    formData.append("OfferLetterFile", fileOL[0]);
                }
                else
                    formData.append("OfferLetterFile", null);
            }

            formData.append("EmployeeJoiningID", EmployeeJoiningID);
            formData.append("EmployeeId", EmployeeId);
            formData.append("OfferLetterFileID", $("#hdnFileUpload_OL").val());
            formData.append("FormProcessID", formprocessid);
            $.ajax({
                url: '/EmployeeJoiningForm/SaveForm',
                type: "POST",
                contentType: false, // Not to set any content header  
                processData: false, // Not to process data  
                data: formData,
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    if (formprocessid === null || formprocessid === '' || formprocessid === '0' || formprocessid === undefined) {
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { location.href = "/PendingJoiningRequest/Index" }, 1000);
                        }
                    }
                    else {
                        setTimeout(function () { location.href = "/EmployeeJoiningForm/Edit/" }, 1000);
                    }

                },
                error: function (err) {
                    hideLoaderFrame();
                    // alert(err.statusText);
                    ShowMessage('error', err.statusText);
                }
            });
        }
    }

}

function UpdateEmployeeJoiningRequest() {
    if (window.FormData !== undefined) {
        var fileOL;
        var $form = $("#frmEmployeeJoiningForm");
        $.validator.unobtrusive.parse($form);
        $form.validate();
        if ($form.valid()) {
            pageLoaderFrame();
            //var formData = new FormData($form[0]);
            var formData = new FormData();
            if ($("#fu_OL").get(0).files.length > 0) {
                fileOL = $("#fu_OL").get(0).files;
                formData.append("OfferLetterFile", fileOL[0]);
            }
            else
                formData.append("OfferLetterFile", null);
            formData.append("ID", $("#hdnEmployeeJoiningID").val());
            formData.append("FormProcessID", $("#hdnFormProcessID").val());
            formData.append("JoiningDate", $("#txtJoiningDate").val());
            formData.append("Comments", $("#txtComments").val());
            formData.append("OfferLetterFileID", $("#hdnFileUpload_OL").val());
            $.ajax({
                url: '/EmployeeJoiningForm/UpdateForm',
                type: "POST",
                contentType: false, // Not to set any content header  
                processData: false, // Not to process data  
                data: formData,
                success: function (result) {
                    hideLoaderFrame();
                    ShowMessage(result.CssClass, result.Message);
                    setTimeout(function () { BackToTaskList(); }, 1000);
                },
                error: function (err) {
                    hideLoaderFrame();
                    // alert(err.statusText);
                    ShowMessage('error', err.statusText);
                }
            });
        }
    }

}

function RemoveDocument(ctrl, fileidfieldname) {
    var fileid = $("#hdnFileUpload_" + ctrl).val();
    $("#btnRemove_" + ctrl).hide();
    $("#divBrowse_" + ctrl).show();
    $("#file_caption_name_" + ctrl).html('');
    $("#file_caption_name_" + ctrl).hide();
    $("#file_caption_id_" + ctrl).html('');
    $("#txtuploadedMsgAdd_" + ctrl).text("");
    $("#fu_" + ctrl).val("");
    $("#divPreview_" + ctrl).html("");
    $("#hdnDeletedFileUpload_" + ctrl).val(fileid);

}
