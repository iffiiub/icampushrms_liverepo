﻿$(document).ready(function () {

    initLeaveRejoiningGrid();
});
function initLeaveRejoiningGrid() {
    var  leaveRejoiningDatatable = $('#tblLeaveRejoiningList').DataTable(
        {
            columnDefs: [
                { width: "20%", targets: [0, 1, 2, 3, 4,5] }
            ],
            "bFilter": true,
            "bInfo": true,
            "bSortCellsTop": true,
            "aaSorting": [[0, 'desc']]
        }
    );
}
function ShowLeaveRejoiningForm(leaveId, urlstring) {
    $.ajax({
        //dataType: 'json',
        type: 'POST',
        url: '/LeaveRejoingList/SetLeaveIDSesssion',
        data: { id: leaveId },
        success: function (result) {
            if (result.id != null)
                location.href = urlstring;

        },
        error: function (err) {
            // hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}
