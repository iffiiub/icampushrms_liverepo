﻿$(document).ready(function () {
    //*** Naresh 2020-03-24 Added export functionality
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"))
    GetData(1);
});
var AllEditMode = 0;
//var AllRefreshMode = 0;
var $tr;
var oTableChannel;
var isDataIsRefresh = true;
function GetData(id) {
    if (id == 1) {
        pageLoaderFrame();
    }
    isDataIsRefresh = true;
    var CompanyID = $("#ddlCompany").val();
    var GroupID = $("#ddlGroup").val();
    if (CompanyID == 0) {
        CompanyID = null;
    }
    if (GroupID == 0) {
        GroupID = null;
    }
    oTableChannel = $('#tblCompanyList').dataTable({
        "sAjaxSource": "/CompanyBasedHierarchy/GetCompanyBasedHeirarchy",
        "aoColumns": [
            { "mData": "CompanyId", "bVisible": true, "sClass": "hidden" },
            { "mData": "CompanyName", "sTitle": "Organization Name", 'width': '15%' },
            { "mData": "GroupID", "bVisible": true, "sClass": "hidden" },
            { "mData": "ApproverGroup", "sTitle": "Approver Group", 'width': '10%' },
            { "mData": "EmployeeId", "bVisible": true, "sClass": "hidden" },
            { "mData": "Approver", "sTitle": "Approver", 'width': '10%' },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '5%', "sClass": "text-center" },
            { "mData": "CBHierarchyID", "bVisible": true, "sClass": "hidden" },
            { "mData": "AutoId", "bVisible": true, "sClass": "hidden" },
            { "mData": "EmpId", "bVisible": true, "sClass": "hidden" }

        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "CompanyID", "value": CompanyID }, { "name": "GroupID", "value": GroupID });
        },
        "processing": false,
        "serverSide": false,
        "ajax": "/CompanyBasedHierarchy/GetCompanyBasedHeirarchy",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var EmployeeId = aData["EmployeeId"];
            var select = $(nRow).find(".ddlApprover");
            $(select).val(EmployeeId);// .attr("selected", "selected");  

        },
        "fnDrawCallback": function () {
            $('#tblCompanyList .selectpickerddl').selectpicker();
            if (AllEditMode == 1)
                DispalyInEditMode();
            //if (AllRefreshMode == 1)
            //  RefreshInEditMode();


        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('GeneralAccountPageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('GeneralAccountPageNo'));
        },
        "fnInitComplete": function (oSettings) {
            hideLoaderFrame();
        }
    });
}
//*** Naresh 2020-03-24 Added export functionality
function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        var companyId = $('#ddlCompany').val();
        if (companyId == undefined || companyId == "")
            companyId = 0;
        companyId = parseInt(companyId);
        var GroupID = $("#ddlGroup").val();
        if (GroupID == undefined || GroupID == "")
            GroupID = 0;
        GroupID = parseInt(GroupID);
        window.location.assign("/CompanyBasedHierarchy/ExportToExcel?companyId=" + companyId + "&groupId=" + GroupID);
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        var companyId = $('#ddlCompany').val();
        if (companyId == undefined || companyId == "")
            companyId = 0;
        companyId = parseInt(companyId);

        var GroupID = $("#ddlGroup").val();
        if (GroupID == undefined || GroupID == "")
            GroupID = 0;
        GroupID = parseInt(GroupID);
        window.location.assign("/CompanyBasedHierarchy/ExportToPdf?companyId=" + companyId + "&groupId=" + GroupID);
    }
    else ShowMessage("warning", "No data for export!");
}
function EditAll() {

    AllEditMode = 1;
    DispalyInEditMode();
}

function RefreshAll() {
    location.reload();

}
function DispalyInEditMode() {

    $tr = $('#tblCompanyList tr ');
    pageLoaderFrame();
    $($tr).each(function (i, item) {

        var CompanyID = $(item).find("td").eq(0).html();
        var GroupID = $(item).find("td").eq(2).html();
        var autoID = $(item).find("td").eq(8).html();
        var CBHierarchyID = $(item).find("td").eq(7).html();
        $(item).find("td").eq(5).html("<select data-live-search='true' onchange='SetValue(this)';  class='form-control selectpickerddl ddlApprover' id='ddlApprover_" + autoID + "' >" + $("#ddlEmployee").text() + "  </select>");
        $(item).find("td").eq(6).html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="UpdateHeirarchy(this,' + CompanyID + ',' + GroupID + ',' + autoID + ', ' + CBHierarchyID + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + CompanyID + ',' + GroupID + ',' + autoID + ', ' + CBHierarchyID + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
        $($tr).addClass("IsEdit");
        var EmployeeId = $(item).find('td:eq(4)').text();
        var select = $(item).find(".ddlApprover");
        $(select).val(EmployeeId);// .attr("selected", "selected");         
        $('#tblCompanyList .selectpickerddl').selectpicker();

    });
    hideLoaderFrame();
}
function SetValue(e) {    
    var closesetRow = $(e).closest('tr');
    var select = $(closesetRow).find(".ddlApprover");
    var esistingid = $(closesetRow).find('td:eq(4)').text();
    var id = $(select).val();
    if (id == null || id == undefined || id == "" || id == '')
        ShowMessage("error", "Please select Approver.");
    else
        $(closesetRow).find('td:eq(4)').text(id);
}
function EditHeirarchy(e, CompanyID, GroupID, autoID, CBHierarchyID) {
    $tr = $(e).closest("tr");
    var closesetRow = $(e).closest('tr');
    $($tr).find("td").eq(5).html("<select data-live-search='true' onchange='SetValue(this)'  class='form-control selectpickerddl ddlApprover' id='ddlApprover_" + autoID + "' >" + $("#ddlEmployee").text() + "  </select>");
    // $(closesetRow).find('td:eq(2)').html("<select data-live-search='true'  class='form-control selectpickerddl ddlAbsentTypeName'  id='ddlAbsentType' > " + employees + " </select>");
    $($tr).find("td").eq(6).html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="UpdateHeirarchy(' + CompanyID + ',' + GroupID + ',' + autoID + ', ' + CBHierarchyID + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + CompanyID + ',' + GroupID + ',' + autoID + ', ' + CBHierarchyID + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
    $($tr).addClass("IsEdit");
    var EmployeeId = $(closesetRow).find('td:eq(4)').text();
    var select = $(closesetRow).find(".ddlApprover");
    $(select).val(EmployeeId);// .attr("selected", "selected");   
    $('#tblCompanyList .selectpickerddl').selectpicker();

}
function CancelNewRow(e, CompanyID, GroupID, autoID, CBHierarchyID) {
  
    var closesetRow = $(e).closest('tr');
    // var EmployeeId = $(closesetRow).find('td:eq(4)').text(); 
    var EmployeeId = $(closesetRow).find('td:eq(9)').text();
    var Employee = "";
    if (EmployeeId != "0") {
        Employee = $("#ddlApprover_" + autoID + " option[value='" + EmployeeId + "']").text();
    }
  
    $(closesetRow).find('td:eq(5)').html(Employee);

    $(closesetRow).find('td:eq(6)').html("<a class='btn btn-success btn-rounded btn-condensed btn-sm'   onclick='EditHeirarchy(this," + CompanyID + "," + GroupID + "," + autoID + "," + CBHierarchyID + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> " +
                                        "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"changeStatus(this, " + CompanyID + "," + GroupID + "," + autoID + "," + CBHierarchyID + ")\" title='Delete'><i class='fa fa-times'></i> </a>");

}
function UpdateHeirarchy(CompanyID, GroupID, autoID, CBHierarchyID) {
    var EmpIdval = $("#ddlApprover_" + autoID).val();
    var EmpId = parseInt(EmpIdval);

    if (isNaN(EmpId)) {
        ShowMessage("error", "Please select Approver.");
    }
    else {

        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { EmployeeID: EmpId, CompanyID: CompanyID, GroupID: GroupID, CBHierarchyID: CBHierarchyID },
            url: '/CompanyBasedHierarchy/AddUpdateCompanyHeirarchy',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                //  window.setTimeout(function () { location.reload() }, 1000)
                GetData(1);
            }
        });

    }
}
function changeStatus(e, CompanyID, GroupID, autoID, CBHierarchyID) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { CBHierarchyID: CBHierarchyID },
            url: '/CompanyBasedHierarchy/DeleteCompanyDetails',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                GetData(1);
            }
        });
    });
}
function SaveAll(e) {

    AllEditMode = 0;
    var table = $("#tblCompanyList").DataTable();
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
       // var UpdatedRowData = table.rows(UpdatedTr).data();
        var updatedData = [];
        $(UpdatedTr).each(function (i, itm) {          
            var CBHierarchyID = $(itm).find('td:eq(7)').text();
            var autoID = $(itm).find('td:eq(8)').text();           
            var EmpId =($(itm).find("#ddlApprover_" + autoID)).val();

            updatedData.push({
                CBHierarchyID: CBHierarchyID,
                EmployeeID: EmpId
            });

        });

        console.log(updatedData);
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save all records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/CompanyBasedHierarchy/SaveMultipleEmployees",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'heirarchyList': updatedData }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        GetData(0)
                    }
                    else {
                        ShowMessage("error", data.Message);
                        hideLoaderFrame();
                    }
                }
            })
        });
    }
    else {
        ShowMessage("error", "Please edit at least one record.");
        hideLoaderFrame();
    }
}
