﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridLabour();
    }
});

var oTableChannel;

function getGridLabour() {

    oTableChannel = $('#tbl_LabourList').dataTable({
       
        "sAjaxSource": "/Labour/GetLabourList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document Name/No", 'width': '20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", 'width': '20%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Labour/GetLabourList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_LabourList");
        }
    });
}

$("#btn_ExportToExcel").click(function () { ExportToExcelLabour(); });
$("#btn_ExportToPdf").click(function () { ExportToPdfLabour(); });

function AddLabour() {
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                var EmpID = parseInt($("#hidSelectedEMP").val());
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/Labour/AddLabour?EmployeeID=" + EmpID);
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add Labour Card');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditLabour(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/Labour/EditLabour/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit Labour Card');

}


function DeleteLabour(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Labour/DeleteLabour',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridLabour();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewLabour(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Labour/ViewLabour/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View Labour Card Details');

}

function ExportToExcelLabour() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Labour/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdfLabour() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Labour/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}