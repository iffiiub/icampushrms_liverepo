﻿$jQuery = jQuery.noConflict(true);
$jQuery(document).ready(function () {
    bindSelectpicker(".selectpickerddl");
});


var i = 0;
var ratingData = [];
var EmployeeIds = [];
function saveRow(e) {

    i = i + 1;
    var id = $("#ddlEmployee").val();
    var AcYearId = $("#ddl_AcademicYears").val();
    var EmployeeName = $("#ddlEmployee option:selected").text();
    var rate = $("#txtRate").val();
    var note = $("#txtNote").val();
    if (id != "") {
        if (rate != "") {
            ratingData = RemoveElement(ratingData, 'EmployeeId', id);

            ratingData.push({
                "EmployeeId": id,
                "Rate": rate,
                "Note": note,
                "AcademicYearId": AcYearId
            });
            EmployeeIds.push({
                EmployeeId: id
            });
            var rowcount = $('#EmpRating-table tr').length;
            $(e).closest('tr').children('td:eq(0)').html(id);
            $(e).closest('tr').children('td:eq(1)').html(EmployeeName);
            $(e).closest('tr').children('td:eq(2)').html(rate);
            $(e).closest('tr').children('td:eq(3)').html(note);
            if (rowcount == 2) {
                $(e).closest('tr').children('td:eq(4)').html('<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditRate(this,' + i + ')" id="btnEdit" data-id=' + i + ' title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>');
            }
            else {
                $(e).closest('tr').children('td:eq(4)').html('<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditRate(this,' + i + ')" id="btnEdit" data-id=' + i + ' title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(this)" title="Delete" ><i class="fa fa-times"></i> </a><a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>');
            }

        }
        else {
            ShowMessage('error', 'Please enter rate');
        }


    }
    else {
        ShowMessage('error', 'Please select employee');
    }

}

function AddDataRow() {

    $.ajax({
        type: 'POST',
        data: { EmpIds: JSON.stringify(EmployeeIds), SelectedEmployeeId: 0 },
        url: '/PerformanceRating/GetEmployeesNotInSelection',
        success: function (data) {
            $('#EmpRating-table > tbody').append(
      '<tr id="rowId_' + i + '" class="edit' + i + '">' +
      '<td class="col-md-1 hidden id=row_' + i + '"></td>' +
     '<td class="col-md-4"><select data-live-search="true"  class="form-control selectpickerddl ddlEmployee" id="ddlEmployee" > ' + data + '</select></td>' +
     '<td class="col-md-1"> <input class="form-control bindMaxAndMinSelector" data-Max="10" data-Min="1" id="txtRate" /></td>' +
     '<td class="col-md-3"><textarea id="txtNote" class="form-control"></textarea></td>' +
     '<td class="center-text-align col-md-4">' +
     '<div class="center-text-align">' +
     '<button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)">' +
     '<img style="width:16px;" src="/Content/images/tick.ico" />' +
     '</button>' +
     '</div>' +
     '</td>' +
     '</tr>')
            bindSelectpicker('.selectpickerddl');
            bindMaxAndMinSelector('.bindMaxAndMinSelector');
        },
        error: function (data) { }
    });


}

function savePerformanceRate() {

    var AcyearID = $("#ddl_AcademicYears").val();
    var Acyear = $("#ddl_AcademicYears option:selected").text();
    var rowcount = ratingData.length;

    if (AcyearID != "0") {
        if (rowcount > 0) {

            $.ajax({
                type: 'GET',
                data: { PerformanceRateDate: JSON.stringify(ratingData), AcYearId: AcyearID },
                url: '/PerformanceRating/CheckAlreadyExist',
                success: function (data) {
                    if (data.Success == true) {
                        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Some records already exists with academic year " + Acyear + " while save it will update that records with current rate. You want to proceed?" }).done(function () {
                            $.ajax({
                                type: 'POST',
                                data: { PerformanceRateDate: JSON.stringify(ratingData), AcYearId: AcyearID },
                                url: '/PerformanceRating/SaveRate',
                                success: function (data) {
                                    if (data.Success == true) {
                                        ShowMessage("success", data.Message);
                                        $("#myModal").modal("hide");
                                        getGrid();
                                    }
                                    else {
                                        ShowMessage("error", data.Message);
                                    }
                                },
                                error: function (data) { }
                            });
                        });
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            data: { PerformanceRateDate: JSON.stringify(ratingData), AcYearId: AcyearID },
                            url: '/PerformanceRating/SaveRate',
                            success: function (data) {
                                if (data.Success == true) {
                                    ShowMessage("success", data.Message);
                                    $("#myModal").modal("hide");
                                    getGrid();
                                }
                                else {
                                    ShowMessage("error", data.Message);
                                }
                            },
                            error: function (data) { }
                        });
                    }
                },
                error: function (data) { }
            });


        }
        else {
            ShowMessage("error", "Please enter performance date in grid.");
        }

    }
    else {
        ShowMessage("error", "Please select academic year.");
    }

}

function EditRate(e, i) {
    var selectedEmployeeId = $(e).closest('tr').children('td:eq(0)').html();
    var rate = $(e).closest('tr').children('td:eq(2)').html();
    var note = $(e).closest('tr').children('td:eq(3)').html();
    $.ajax({
        type: 'POST',
        data: { EmpIds: '', SelectedEmployeeId: selectedEmployeeId },
        url: '/PerformanceRating/GetEmployeesNotInSelection',
        success: function (data) {

            $(e).closest('tr').children('td:eq(1)').html('<select data-live-search="true"  class="form-control selectpickerddl ddlEmployee" id="ddlEmployee" > ' + data + '</select>');
            $(e).closest('tr').children('td:eq(2)').html('<input class="form-control bindMaxAndMinSelector" data-Max="10" data-Min="1" id="txtRate" value=' + rate + ' />');
            $(e).closest('tr').children('td:eq(3)').html('<textarea id="txtNote" class="form-control">' + note + '</textarea>');
            $(e).closest('tr').children('td:eq(4)').html('<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');


            bindSelectpicker('.selectpickerddl');
            bindMaxAndMinSelector('.bindMaxAndMinSelector');
        },
        error: function (data) { }
    });

}

function DeleteRow(e) {
    var selectedEmployeeId = $(e).closest('tr').children('td:eq(0)').html();
    $(e).closest('tr').remove();
    ratingData = RemoveElement(ratingData, 'EmployeeId', selectedEmployeeId);
}


