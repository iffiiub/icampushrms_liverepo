﻿
$(document).ready(function () {
    $('#chkShowActiveEmployeeOnly').prop('checked', true);
    getGrid();
    $("#chkShowActiveEmployeeOnly").click(function () {
        var ShowActiveOnly = $('#chkShowActiveEmployeeOnly').is(":checked");
        if (ShowActiveOnly == true) {
            $('#chkShowAllEmployee').prop("checked", false);
        }
        else {
            $('#chkShowAllEmployee').prop("checked", true);
        }

    });
    $("#chkShowAllEmployee").click(function () {
        var ShowAll = $('#chkShowAllEmployee').is(":checked");
        if (ShowAll == true) {
            $('#chkShowActiveEmployeeOnly').prop("checked", false);
        }
        else {
            $('#chkShowActiveEmployeeOnly').prop("checked", true);
        }
    });
});

function getGrid() {

    var ShowActiveOnly = $('#chkShowActiveEmployeeOnly').is(":checked");
    var ShowAll = $('#chkShowAllEmployee').is(":checked");
    if (ShowAll == true) {
        ShowActiveOnly = false;
    }
    var AcademicYear = $('#ddl_AcYears').val();

    oTableGeneralAccounts = $('#tblPerformanceRating').DataTable({
        "sAjaxSource": "/PerformanceRating/ViewPerformanceratingList",
        "aoColumns": [
          { "mData": "EmpAltID", "sTitle": "Employee ID", "sWidth": "10%", "sClass": "left-text-align", "sType": "Int" },
          { "mData": "EmployeeName", "sTitle": "Employee Name", "sWidth": "20%", "sClass": "left-text-align" },
          { "mData": "Rate", "sTitle": "Rate", "sWidth": "10%" },
          { "mData": "AcademicYear", "sTitle": "Academic Year", "sWidth": "20%" },
          { "mData": "Note", "sTitle": "Note", "sWidth": "20%" },
          { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "20%" }
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "ShowActiveOnly", "value": ShowActiveOnly }, { "name": "ShowAll", "value": ShowAll }, { "name": "AcademicYearId", "value": AcademicYear });
        },
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "asc"]],
        "createdRow": function (row, data, index) {

        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        },
        "fnDrawCallback": function () {
        }
    });

}


function edit(e, RateIndex, id, EmpID, rate, AcYeearId, AcYear, Note) {
    var closesetRow = $(e).closest('tr');
    $(closesetRow).find('td:eq(2)').html("<input class='form-control bindMaxAndMinSelector' data-Max='10' data-Min='1' type='text' id='txtRate_" + RateIndex + "'><label class='lblValidAmount'>*</label>");
    $(closesetRow).find('td:eq(3)').html("<select data-live-search='false'  class='form-control selectpickerddl ddlAcYear' id='ddlAcYear_" + RateIndex + "' > " + $("#ddlAcademicYears").text() + " </select>");
    $(closesetRow).find('td:eq(4)').html("<textarea id='txtGridNote_" + RateIndex + "' class='form-control'>" + $(closesetRow).find('td').eq(4).html() + "</textarea>");
    $(closesetRow).find('td:eq(5)').html("<div class='left-text-align'><button id='btnRowSave' class='tableButton' onclick='updatePerformanceRate(this," + RateIndex + "," + id + "," + EmpID + ")' class='btnAdd'><img style='width:16px;' src='/Content/images/tick.ico' /></button><button class='tableButton' id='btnRowCancel' onClick=\"CancelNewRow(this, " + RateIndex + ", " + id + "," + EmpID + ", " + rate + "," + AcYeearId + " ,'" + AcYear + "','" + Note + "')\"; class='btnRemove'><img style='width:16px;' src='/Content/images/cross.ico' /></button></div>");
    bindMaxAndMinSelector('#txtRate_' + RateIndex + '');
    var select1 = $(closesetRow).find(".selectpickerddl");
    $(select1).val(AcYeearId);// .attr("selected", "selected");
    $("#txtRate_" + RateIndex + "").val(rate);
    $('#tblPerformanceRating .selectpickerddl').selectpicker();
}

function CancelNewRow(e, RateIndex, id, EmpId, rate, AcYeearId, AcYear, Note) {
    var closesetRow = $(e).closest('tr');
    $(closesetRow).find('td:eq(2)').html(rate);
    $(closesetRow).find('td:eq(3)').html(AcYear);
    $(closesetRow).find('td:eq(4)').html(Note);
    $(closesetRow).find('td:eq(5)').html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick=\"edit(this," + RateIndex + "," + id + "," + EmpId + "," + rate + "," + AcYeearId + ",'" + AcYear + "','" + Note + "' );\" title='Edit' ><i class='fa fa-pencil'></i> </a>" +
                                         "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"deletePerformanceRate(this," + id + ")\" title='Delete'><i class='fa fa-times'></i> </a>");


}

function updatePerformanceRate(e, RateIndex, id, EmpID) {
    var rate = $("#txtRate_" + RateIndex + "").val();
    var AcYearId = $("#ddlAcYear_" + RateIndex + "").val();
    var Note = $("#txtGridNote_" + RateIndex + "").val();
    var AcYear = $("#ddlAcYear_" + RateIndex + " option[value='" + AcYearId + "']").text();
    if (rate != "") {
        if (AcYearId != "0") {
            $.ajax({
                type: 'POST',
                data: { PerformanceRateId: id, Rate: rate, EmpID: EmpID, AcYearId: AcYearId, Note: Note },
                url: '/PerformanceRating/UpdateRate',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        //getGrid();
                        var closesetRow = $(e).closest('tr');
                        $(closesetRow).find('td:eq(2)').html(rate);
                        $(closesetRow).find('td:eq(3)').html(AcYear);
                        $(closesetRow).find('td:eq(4)').html(Note);
                        $(closesetRow).find('td:eq(5)').html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick=\"edit(this," + RateIndex + "," + id + "," + EmpID + "," + rate + "," + AcYearId + ",'" + AcYear + "');\" title='Edit' ><i class='fa fa-pencil'></i> </a>" +
                                                             "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"deletePerformanceRate(this," + id + ")\" title='Delete'><i class='fa fa-times'></i> </a>");

                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        }
        else {
            ShowMessage("error", "Please select academic year");
        }

    }
    else {
        ShowMessage("error", "Please enter rate");
    }

}

function deletePerformanceRate(id) {

    $.ajax({
        type: 'POST',
        data: { PerformanceRateId: id },
        url: '/PerformanceRating/DeleteRate',
        success: function (data) {
            if (data.Success == true) {
                ShowMessage("success", data.Message);
                getGrid();
            }
            else {
                ShowMessage("error", data.Message);
            }
        },
        error: function (data) { }
    });
}
$("#btn_addPerformanceRating").click(function () {
    AddPerformanceRating();

});
function AddPerformanceRating() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Performancerating/AddPerformancerating", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add Performance Rating');
        bindMaxAndMinSelector('.bindMaxAndMinSelector');

    });
}

function bindMaxAndMinSelector(selector) {
    $(selector).keypress(function (e) {
        var c = String.fromCharCode(e.which);             
        if (e.which >= 48 && e.which <= 57 || e.which == 46) {
            var maxValue = parseFloat($(this).attr('data-Max'));
            var minValue = parseFloat($(this).attr('data-Min'));
            var value = parseFloat($(this).val() + c);
            var length = value.toString().length;
            if (length > 5)
                return false;
            if (maxValue < value || minValue > value) {
                ShowMessage("error", "Please enter rating between 1.0 and 10.0");
                return false;
            }
        }
        else {
            return false;
        }

    });
};