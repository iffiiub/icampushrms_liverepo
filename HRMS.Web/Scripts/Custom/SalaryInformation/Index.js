﻿var selectList = "";

$(document).ready(function () {
    if (parseInt($("#hdnEmployee").val()) > 0) {
        var id = $("#hdnEmployee").val();
        LoadChart(id);
        LoadEmployeeDetails(id);
        $("#salaryInfoDiv").load("/SalaryInformation/GetSalaryByEmployee/" + id, function () { });
        $("#hdnEmployeeId").val(id);
    }
});


function LoadChart(id) {

    //var id = $(this).find('.clickme').attr('id');
    if (id != null && id != undefined) {
        $.ajax({
            type: 'GET',
            url: '/SalaryInformation/GetChartData/' + id,
            success: function (data) {

                //$.getScript('/Scripts/Custom/HighCharts.js', function () {

                $('#SalaryChart').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [
                        {
                            name: "Brands",
                            colorByPoint: true,
                            data: data.aaData

                        }
                    ]
                });

                //});


                // $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
            },
            error: function (data) { }
        });
    }

}

$("#SearchEmployee").blur(function () {

    var val = $('#SearchEmployee').val();
    if (val != "") {
        $.ajax({

            // The url to which the request is to be sent.
            url: "/SalaryInformation/EmployeeSearch",

            // The type of request. It can be "GET" or "POST" or others.
            type: "GET",

            // The data that is to be sent to the server. It can be omitted if no data is
            // to be sent. In a "GET" request this data is appended to the url.
            data: { "search": val },

            // The function to be called when the server returns some data. The data
            // variable holds the data that was returned.
            success: function (data) {
                //if(data != null)
                //{
                var text = '';
                //$('#ULEmployeeNotInDepartment').html("");
                $(".list-group-item").remove();
                $.each(data, function (index, itemData) {

                    var Name = itemData.FirstName_1 + ' ' + itemData.SurName_1;
                    text += "<a href='javascript:void(0)' id=" + itemData.EmployeeID + " onclick='getBankInfo(" + itemData.EmployeeID + ")' class='list-group-item'>" + Name + "</a>";
                    //text += "<li class='liemployeenotindepartment'>";
                    //text += "<input type='checkbox' class='chkEmployee'  value='" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "' id='" + itemData.EmployeeID + "' />";
                    //text += "<label for='" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "'>" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "</label></li>";

                });

                $('#AllEmployees').append(text);
                //}


            },

            // Show an alert if an error occurs.
            error: function () {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: "Some error occured.",
                });
            }
        });
    }

});

$(document).ready(function () {
    $(".EmpSalaryInfo").click(function () {
        var id = $(this).attr('id');
        $("#hdnEmployeeId").val(id);
        $("#salaryInfoDiv").load("/SalaryInformation/GetSalaryByEmployee/" + id, function () {
        });
    });
});


function AddSalaryInformation(salaryId) {
    //$(".editButton").attr("disabled", true);

    /* These will comented because add functionality is required in pop up
    $(".chkIsActive").attr("disabled", false);

    $("#addPaySalaryInfo").attr("disabled", true)
    $('#paySalary-table').dataTable().fnAddData([
       '<div class="input-group"><select class="ddlPaySalary form-control" id="ddlPaySal">' + selectList + '</select> <span class="input-group-addon add-on"><a onClick="AddPaySalaryAllowances()" style="color:#ffffff;">+</a></span></div>',
        '<input class="form-control positiveOnly" type="text" id="txtAmount"/><label class="lblValidAmount">*</label> ',
        '<input class="form-control" type="text" id="txtTrDate"/>',
        '<textarea  class="form-control"   id="txtNotes"/></textarea>',
        '<div class="center-text-align"><input type="checkbox" id="chkActive"/><div>',
    '<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveRow(0)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow()" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>']);
   */

    if (salaryId > 0)
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to Edit?" }).done(function () {
            loadSalaryInformation(salaryId, "Edit Salary Infromation");
        });
    else {
        loadSalaryInformation(salaryId, "Add Salary Infromation");
    }
}

function loadSalaryInformation(salaryId, title) {
    $("#modal_Loader").html("");
    pageLoadingFrame("show");
    var employeeId = $("#hdnEmployeeId").val();
    if (!parseFloat(employeeId) > 0)
        employeeId = 0;
    $("#modal_Loader").load("/SalaryInformation/AddSalaryInformation?EmployeeId=" + employeeId + "&id=" + salaryId, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
        pageLoadingFrame("hide");
        $('.positiveOnly').unbind('keypress');
        bindPositiveOnly('.positiveOnly');
        bindDatePicker('.txtdatepicker');
        bindSelectpicker(".selectpickerddl");
    });
}

function BindSalaryAllowances() {
    $.ajax({
        type: 'get',
        url: '/PaySalaryAllowance/loadPaySalaryAllowances',
        datatype: 'Json',
        success: function (data) {
            $.each(data, function (key, value) {
                //  selectList.append($('<option></option>').val(value.paySalaryAllowancesID).html(value.paySalaryAllowancesName));
                selectList += '<option value=' + value.paySalaryAllowancesID + '>' + value.paySalaryAllowancesName + '</option>';
            });
        }
    });
}