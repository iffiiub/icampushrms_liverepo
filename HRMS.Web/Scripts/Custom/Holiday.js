﻿$(document).ready(function () {
   
    getGridHoliday();
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    $("#btnTextClear").click(function () {
        $('#txtSearch').val(""); // $('#txtSearch').val() == "";
        getGridHoliday();
    });
    $("#btnSearch").click(function () {

        getGridHoliday();
    });



});
//$(document).on('change', '#ddlCompany', function () {

//    selectList = "";
//    var count = 0;
//    selectList = "<option value=\"\">Select Holiday</option>";
//    var companyid = $(this).val() == "" ? 0 : $(this).val();
//    if (companyid > 0) {
//        pageLoaderFrame();
//        $.ajax({
//            type: 'get',
//            url: '/Holiday/GetHolidayListByCompany?companyID=' + companyid,
//            datatype: 'Json',
//            success: function (data) {
//                $.each(data, function (key, value) {
//                    count = count + 1;
//                    selectList += '<option value=' + value.HolidayID + '>' + value.HolidayName + '</option>';
//                });

//                $("#ddlHoliday").html();
//                $("#ddlHoliday").html(selectList);
//                RefreshSelect("#ddlHoliday");

//                if (count == 0) {
//                    ShowMessage("error", "No holidays are added for selected organization");
//                }
//                hideLoaderFrame();
//            }

//        });
//    }
//    else {
//        ShowMessage("error", "Select an organization");
//    }
//});
$("#btn_addHoliday").click(function () { AddHoliday(); });
var oTableChannel;
function getGridHoliday() {
    var searchval = $("#txtSearch").val();
    var companyid = $("#ddlCompany").val();
    var holidayid = $("#ddlHoliday").val();
    oTableChannel = $('#tbl_HolidayList').dataTable({      
        "sAjaxSource": "/Holiday/GetHolidayList" ,
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "companyID", "value": companyid });           
        },
        "aoColumns": [
            { "mData": "HolidayID", "bVisible": false, "sTitle": "HolidayID" },
            { "mData": "CompanyName", "sTitle": "Organization Name", "width": "35%" },         
            { "mData": "HolidayName", "sTitle": "Holiday Name", "width": "35%" },
            { "mData": "From", "sTitle": "From", "sType": "date-uk", "width": "10%" },
            { "mData": "to", "sTitle": "To", "sType": "date-uk", "width": "10%" },
            { "mData": "Action", "sTitle": "Actions" ,"bSortable":false,"width":"10%"}

        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/Holiday/GetHolidayList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {            
            $('#tbl_HolidayList').css({ 'width': '100%' });
        },
        "bSortCellsTop": true
    });
}

function AddHoliday() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Holiday/AddHoliday");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Public Holiday');
}

function EditHoliday(id) {
    var holiday = GetHoliday(id)
   // alert(holiday.IsUsed);
    if (!holiday.IsUsed) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/Holiday/EditHoliday/" + id);
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Public Holiday');
    }
    else
        ShowMessage('error', "You cannot update the holiday record because it already used in leave request(s).");
   
}

function DeleteHoliday(id) {
    var holiday = GetHoliday(id)
    // alert(holiday.IsUsed);
    if (!holiday.IsUsed) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Holiday/DeleteHoliday',
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                getGridHoliday();
            },
            error: function (data) { }
        });
    });
    }
    else
        ShowMessage('error', "You cannot delete the holiday record because it already used in leave request(s).");
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewHoliday(id) {
    $("#modal_Loader").load("/Holiday/ViewHoliday/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('View Holiday Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}
function GetHoliday(id) {
    var holiday;
    $.ajax({
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: '/Holiday/GetHoliday',
        data: { holidayID: id },
        async: false,
        success: function (result) {
            holiday = result;
           
        },
        error: function (err) {
            ShowMessage('error', err.statusText);
        }
    });
    return holiday;
}