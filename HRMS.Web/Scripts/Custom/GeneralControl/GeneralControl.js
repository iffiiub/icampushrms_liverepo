﻿
var isExistingChange = false;

$(document).ready(function () {
    $(".hiddenDiv").hide(1);
    //var selectedVal= $("#hdnControlId").val();

    var selectedItem = sessionStorage.getItem("SelectedItem");
    //$("#ddl_ControlList").val(selectedItem); 2019-02-19
    //RefreshSelect("#ddl_ControlList"); 2019-02-19
    // GetGeneralControlType();2019-02-19
    ShowGeneralControlType(selectedItem);

});

function GetGeneralControlType() {
    var ddlOption = $("#ddl_ControlList").val();
    sessionStorage.setItem("SelectedItem", ddlOption);
    if (ddlOption == 1) {
        $(".hiddenDiv").hide(1);
        $("#BankDetails").show(1);
        getGridBankDetails();
    }
    else if (ddlOption == 2) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeSection").show(1);
        getGridEmployeeSectionDetails();
    }
    else if (ddlOption == 3) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeIDSetting").show(1);
        getEmployeeIDSetting();
    }
    else if (ddlOption == 4) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeWorkEmail").show(1);
        getGridEmployeeWorkEmailDetails();
    }
    else if (ddlOption == 5) {
        $(".hiddenDiv").hide(1);
        $("#DocumentSetting").show(1);
        getGridDocumentSetting();
    }
    else if (ddlOption == 6) {
        $(".hiddenDiv").hide(1);
        $("#AbsentType").show(1);
        getGridAbsentTypes();
    }
    else if (ddlOption == 7) {
        $(".hiddenDiv").hide(1);
        $("#ValidationSetting").show(1);
        getvalidationSetting();
    }
    else if (ddlOption == 8) {
        $(".hiddenDiv").hide(1);
        $("#MOLTitleDetails").show(1);
        getGridMOLTitleList();
    }
    else if (ddlOption == 9) {
        $(".hiddenDiv").hide(1);
        $("#SMTPConfig").show(1);
        getSMTPConfig();
    }
    else if (ddlOption == 10) {
        $(".hiddenDiv").hide(1);
        $("#OpeningBalanceDetails").show(1);
        GetEmployeeOpeningBalance();
    }
    else if (ddlOption == 11) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeSponsor").show(1);
        getGridEmployeeSponsor();
    }
    else if (ddlOption == 12) {
        $(".hiddenDiv").hide(1);
        $("#BankGroupDetails").show(1);
        getGridPayBankGroups();
    }
    else if (ddlOption == 13) {
        $(".hiddenDiv").hide(1);
        $("#AcademicInstitutes").show(1);
        getAcademicInstitutes();
    }
    else if (ddlOption == 14) {
        $(".hiddenDiv").hide(1);
        $("#QualificationId").show(1);
        getQualifications();
    }
    else if (ddlOption == 15) {
        $(".hiddenDiv").hide(1);
        $("#QualificationSubject").show(1);
        getQualificationSubject();
    }
    else if (ddlOption == 16) {
        $(".hiddenDiv").hide(1);
        GetImage(1);
        GetImage(2);
        GetImage(3);
        $("#ApplicationLogos").show(1);
        //getQualificationSubject();
    }
    else if (ddlOption == 17) {
        $(".hiddenDiv").hide(1);
        $("#PayCategoryControl").show(1);
        CheckCostCenterValidation();
        getPayCategories();
    }
    else if (ddlOption == 18) {
        $(".hiddenDiv").hide(1);
        $("#PaySundryControl").show(1);
        GetSundriesTypes();
    }
    else if (ddlOption == 20) {
        $(".hiddenDiv").hide(1);
        $("#ShortLeaveControl").show(1);
        $("#btn_addGeneralControl").hide();
        ShowShortLeaveSettings();
    }
    else if (ddlOption == 21) {
        $(".hiddenDiv").hide(1);
        $("#InsuranceAmountSection").show(1);
        getGridInsuranceCategoryAmounts();
    }
    else if (ddlOption == 23) {
        $(".hiddenDiv").hide(1);
        $("#TrainingCompetencies").show(1);
        getTrainingCompetencies();
    }
}

function ShowGeneralControlType(selectedVal) {
    pageLoaderFrame();
    $("#hdnSelCtrlId").val(selectedVal);
    // var ddlOption = $("#ddl_ControlList").val();
    var ddlOption = selectedVal;
    sessionStorage.setItem("SelectedItem", ddlOption);
    if (ddlOption == 1) {
        $(".hiddenDiv").hide(1);
        $("#BankDetails").show(1);
        getGridBankDetails();

    }
    else if (ddlOption == 2) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeSection").show(1);
        getGridEmployeeSectionDetails();
    }
    else if (ddlOption == 3) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeIDSetting").show(1);
        getEmployeeIDSetting();
    }
    else if (ddlOption == 4) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeWorkEmail").show(1);
        getGridEmployeeWorkEmailDetails();
    }
    else if (ddlOption == 5) {
        $(".hiddenDiv").hide(1);
        $("#DocumentSetting").show(1);
        getGridDocumentSetting();
    }
    else if (ddlOption == 6) {
        $(".hiddenDiv").hide(1);
        $("#AbsentType").show(1);
        getGridAbsentTypes();
    }
    else if (ddlOption == 7) {
        $(".hiddenDiv").hide(1);
        $("#ValidationSetting").show(1);
        getvalidationSetting();
    }
    else if (ddlOption == 8) {
        $(".hiddenDiv").hide(1);
        $("#MOLTitleDetails").show(1);
        getGridMOLTitleList();
    }
    else if (ddlOption == 9) {
        $(".hiddenDiv").hide(1);
        $("#SMTPConfig").show(1);
        getSMTPConfig();
    }
    else if (ddlOption == 10) {
        $(".hiddenDiv").hide(1);
        $("#OpeningBalanceDetails").show(1);
        GetEmployeeOpeningBalance();
    }
    else if (ddlOption == 11) {
        $(".hiddenDiv").hide(1);
        $("#EmployeeSponsor").show(1);
        getGridEmployeeSponsor();
    }
    else if (ddlOption == 12) {
        $(".hiddenDiv").hide(1);
        $("#BankGroupDetails").show(1);
        getGridPayBankGroups();

    }
    else if (ddlOption == 13) {
        $(".hiddenDiv").hide(1);
        $("#AcademicInstitutes").show(1);
        getAcademicInstitutes();
    }
    else if (ddlOption == 14) {
        $(".hiddenDiv").hide(1);
        $("#QualificationId").show(1);
        getQualifications();
    }
    else if (ddlOption == 15) {
        $(".hiddenDiv").hide(1);
        $("#QualificationSubject").show(1);
        getQualificationSubject();
    }
    else if (ddlOption == 16) {
        $(".hiddenDiv").hide(1);
        GetImage(1);
        GetImage(2);
        GetImage(3);
        $("#ApplicationLogos").show(1);
        hideLoaderFrame();
        //getQualificationSubject();
    }
    else if (ddlOption == 17) {
        $(".hiddenDiv").hide(1);
        $("#PayCategoryControl").show(1);
        CheckCostCenterValidation();
        getPayCategories();
    }
    else if (ddlOption == 18) {
        $(".hiddenDiv").hide(1);
        $("#PaySundryControl").show(1);
        GetSundriesTypes();
    }
    else if (ddlOption == 19) {
        $(".hiddenDiv").hide(1);
        $("#DataImportControl").show(1);
    }
    else if (ddlOption == 20) {
        $(".hiddenDiv").hide(1);
        $("#ShortLeaveControl").show(1);
        $("#btn_addGeneralControl").hide();
        ShowShortLeaveSettings();
    }
    else if (ddlOption == 21) {
        $(".hiddenDiv").hide(1);
        $("#InsuranceAmountSection").show(1);
        getGridInsuranceCategoryAmounts();
    }
    else if (ddlOption == 22) {
        $(".hiddenDiv").hide(1);
        $("#ReminderSetupControl").show(1);
        ViewDocumentReminders();
        ViewAttendanceReminders();
        SetReminderMode();
    }
    else if (ddlOption == 23) {
        $(".hiddenDiv").hide(1);
        $("#TrainingCompetencies").show(1);
        getTrainingCompetencies();
    }
    hideLoaderFrame();
}

//$("#ddl_ControlList").change(function () {
//    GetGeneralControlType();
//}); 2019-02-19 
$(".btnadd").click(function () {

    var catId = $(this).attr("cat-id");
    if (typeof catId != "undefined") {
        AddEditGeneralControl(catId);
    }
    else {
        AddEditGeneralControl(0);
    }

});

var oTableChannel;
function getGridBankDetails() {

    oTableChannel = $('#tbl_BankDetails').dataTable({
        "sAjaxSource": "/GeneralControl/GetBankList",
        "aoColumns": [

            // { "mData": "AcademicID", "bVisible": false, "sTitle": "AcademicID" },
            { "mData": "BankName_1", "sTitle": "Bank Name (EN)", "bSortable": true, "mDataProp": "BankName_1", "sWidth": "10%" },
            //{ "mData": "BankName_2", "sTitle": "Bank Name (AR)", "bSortable": true, "mDataProp": "BankName_2", "sWidth": "12%" },
            //{ "mData": "BankName_3", "sTitle": "Bank Name (FR)", "bSortable": true, "mDataProp": "BankName_3", "sWidth": "12%" },
            //{ "mData": "BankGroup", "sTitle": "Bank Group Name", "bSortable": true, "mDataProp": "BankGroup", "sWidth": "10%" },
            { "mData": "acc_code", "sTitle": "Salary Payble Account Code", "bSortable": false, "mDataProp": "acc_code", "sWidth": "10%" },
            { "mData": "RtnCode", "sTitle": "Rtn/Routing Code", "bSortable": true, "mDataProp": "RtnCode", "sWidth": "10%" },
            { "mData": "SwiftCode", "sTitle": "Swift Code", "bSortable": true, "mDataProp": "SwiftCode", "sWidth": "10%" },
            //{ "mData": "CashBank", "sTitle": "Cash Bank", "bSortable": true, "mDataProp": "CashBank", "sWidth": "7%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "10%" }
        ],
        "fnServerParams": function (aoData) {
        },

        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_BankDetails");
            // hideLoaderFrame();
        }
    });
}

function AddEditGeneralControl(id) {
    //  var ddlOption = $("#ddl_ControlList").val(); 2019-02-19
    var ddlOption = $("#hdnSelCtrlId").val(); //2019-02-19

    if (ddlOption == 1) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditBankDetails/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Bank Details');
            }
            else {
                $("#modal_heading").text('Edit Bank Details');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    } else if (ddlOption == 2) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditEmployeeSection/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Employee Section');
            }
            else {
                $("#modal_heading").text('Edit Employee Section');
            }
            $('#myModal').modal({ backdrop: 'static' });
        });
    } else if (ddlOption == 4) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AutoGenerateWorkEmailSetting/", function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Generate Work Email');
            }
            else {
                $("#modal_heading").text('Generate Work Email');
            }
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 6) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditAbsentType?AbsentTypeId=" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Absent Type');
            }
            else {
                $("#modal_heading").text('Edit Absent Type');
            }
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 8) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditMolTitle?id=" + id, function () {
            if (id == 0) {
                $("#modal_heading").text('Add MOL Title');
            }
            else {
                $("#modal_heading").text('Edit MOL Title');
            }
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 9) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditSMTPConfig", function () {
            $("#modal_heading").text('Update SMTP Configuration');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 12) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditBankGroupDetails/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Bank Group Details');
            }
            else {
                $("#modal_heading").text('Edit Bank Group Details');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 13) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditAcademicInstitutes/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Academic Institute Details');
            }
            else {
                $("#modal_heading").text('Edit Academic Institute Details');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 14) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditQualifications/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Qualification Details');
            }
            else {
                $("#modal_heading").text('Edit Qualification Details');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 15) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditQualificationSubject/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Qualification Subject Details');
            }
            else {
                $("#modal_heading").text('Edit Qualification Subject Details');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 17) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditPayCatagory/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == -1) {
                $("#modal_heading").text('Add Pay Category');
            }
            else {
                $("#modal_heading").text('Edit Pay Category');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 18) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditPaySundryType/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == -1) {
                $("#modal_heading").text('Add Pay Sundry');
            }
            else {
                $("#modal_heading").text('Edit Pay Sundry');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 21) {
        AddMultipleInsuranceAmount(id);
    }
    else if (ddlOption == 23) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/AddEditTrainingCompetency/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Training Competency');
            }
            else {
                $("#modal_heading").text('Edit Training Competency');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    }
}

function ViewGeneralControl(id) {
    //var ddlOption = $("#ddl_ControlList").val();//2019-02-19
    var ddlOption = $("#hdnSelCtrlId").val(); //2019-02-19
    if (ddlOption == 1) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewBankDetails/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            if (id == 0) {
                $("#modal_heading").text('Add Bank Details');
            }
            else {
                $("#modal_heading").text('View Bank Details');
            }

            $('#myModal').modal({ backdrop: 'static' });
        });
    } else if (ddlOption == 2) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewEmployeeSectionDetails/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            $("#modal_heading").text('View Employee Section');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 6) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewAbsentType/" + id, function () {
            $("#modal_heading").text('View Absent Type Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 12) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewBankGroupDetails/" + id, function () {
            $("#modal_heading").text('View Bank Group Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 9) { //2019-02-19 for viewing  SMTPConfig
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewSMTPConfig/", function () {
            $("#modal_heading").text('View SMTP/Email Configuration');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 13) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewAcademicInstitutesDetails/" + id, function () {
            $("#modal_heading").text('View Academic Institute Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 14) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewQualificationsDetails/" + id, function () {
            $("#modal_heading").text('View Qualification Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 15) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewQualificationSubjectDetails/" + id, function () {
            $("#modal_heading").text('View Qualification Subject Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 17) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewPayCategory/" + id, function () {
            $("#modal_heading").text('View Pay Category Details');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }
    else if (ddlOption == 21) {
        $("#modal_Loader").html("");
        $("#modal_Loader").load("/GeneralControl/ViewMultipleInsuranceAmount/" + id, function () {
            bindSelectpicker('.selectpickerddl');
            $("#modal_heading").text('View Insurance Category Amount');
            $('#myModal').modal({ backdrop: 'static' });
        });
    }


}

function getGridEmployeeSectionDetails() {

    oTableChannel = $('#tbl_EmployeeSectionDetails').dataTable({
        "sAjaxSource": "/GeneralControl/GetEmployeeSectionList",
        "aoColumns": [

            // { "mData": "AcademicID", "bVisible": false, "sTitle": "AcademicID" },
            { "mData": "EmployeeSectionName", "sTitle": "Section Name", "bSortable": true, "mDataProp": "EmployeeSectionName" },
            { "mData": "Head", "sTitle": "Section Head", "bSortable": true, "mDataProp": "Head" },
            { "mData": "AssistantName_1", "sTitle": "Section Assistant 1", "bSortable": true, "mDataProp": "AssistantName_1" },
            // { "mData": "AssistantName_2", "sTitle": "Section Assistant 2", "bSortable": true, "mDataProp": "AssistantName_2" },
            //  { "mData": "AssistantName_3", "sTitle": "Section Assistant 3", "bSortable": true, "mDataProp": "AssistantName_3" },
            { "mData": "ShiftName", "sTitle": "Shift", "bSortable": true, "mDataProp": "ShiftName" },
            { "mData": "Department", "sTitle": "Department", "bSortable": true, "mDataProp": "Department" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_EmployeeSectionDetails");
            // hideLoaderFrame();
        }
    });
}

function DeleteGeneralControl(id) {
    //var ddlOption = $("#ddl_ControlList").val();
    var ddlOption = $("#hdnSelCtrlId").val(); //2019-02-19
    if (ddlOption == 1) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this bank record, All employees will unassign from this banks. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteBank',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getGridBankDetails();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });

    } else if (ddlOption == 2) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this employee section, All employees will unassign from this section. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteSection',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getGridEmployeeSectionDetails();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    } else if (ddlOption == 6) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this absent type, All employees will unassign from this absent type. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteAbsentType',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getGridAbsentTypes();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    } else if (ddlOption == 8) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this MOL title, all employees and documents referring to this record will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteMOLTitle',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getGridMOLTitleList();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    } else if (ddlOption == 12) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this Bank Group, all banks under this group will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteBankGroup',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getGridPayBankGroups();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    }
    else if (ddlOption == 13) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this academic institute, all academic institutes under this group will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteAcademicInstitutes',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getAcademicInstitutes();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    }
    else if (ddlOption == 14) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this qualification details, all qualification under this group will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteQualifications',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getQualifications();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    }
    else if (ddlOption == 15) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this qualification subject details, all qualification subject under this group will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteQualificationSubject',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getQualificationSubject();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    }
    else if (ddlOption == 17) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this pay category, all employees under this pay category will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeletePayCategory',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        getPayCategories();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    }
    else if (ddlOption == 18) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "While delete this sundry type, all records under this sundry type will unassigned. Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeletePaySundriesType',
                success: function (data) {
                    if (data.Success == true) {
                        ShowMessage("success", data.Message);
                        GetSundriesTypes();
                    }
                    else {
                        ShowMessage("error", data.Message);
                    }
                },
                error: function (data) { }
            });
        });
    }
    else if (ddlOption == 21) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?." }).done(function () {
            $.ajax({
                type: 'POST',
                data: { id: id },
                url: '/GeneralControl/DeleteInsuranceCategoryAmount',
                success: function (data) {
                    if (data == true) {
                        getGridInsuranceCategoryAmounts();
                        ShowMessage("success", "Record deleted Successfully");
                    }
                    else {
                        ShowMessage("error", "This amount is already used. cant delete this.");
                    }
                },
                error: function (data) { }
            });
        });
    }
}
function DeleteTrainingCompetencies(trainingId, competencyId) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?." }).done(function () {
        $.ajax({
            type: 'POST',
            data: { trainingId: trainingId, competencyId: competencyId },
            url: '/GeneralControl/DeleteTrainingCompetency',
            success: function (data) {
                if (data.Success == true) {
                    getTrainingCompetencies();
                    ShowMessage("success", "Record deleted Successfully");
                }
                else {
                    ShowMessage(data.CssClass, data.Message);
                }
            },
            error: function (data) { }
        });
    });
}
function getEmployeeIDSetting() {
    $.ajax({
        type: 'POST',
        url: '/GeneralControl/GetEmployeeIdSettingDetails',
        success: function (data) {
            if (data.EmployeeIdPrefix != null || data.EmployeeIdStartFrom != 0) {
                $("#tbl_EmployeeIDSetting tr:last").remove();
                $('#tbl_EmployeeIDSetting > tbody').append(
                    '<tr>' +
                    '<td class="col-md-4" id="row_EmployeeIdPrefix">' + data.EmployeeIdPrefix + '</td>' +
                    '<td class="col-md-3">' + data.EmployeeIdStartFrom + '</td>' +
                    '<td class="center-text-align col-md-4">' +
                    '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditEmployeeAlternativeSetting(this)" id="btnEdit"  title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteAlternativeIDSetting()" title="Delete" ><i class="fa fa-times"></i> </a>' +
                    '</td>' +
                    '</tr>')
            }
            else {
                $("#tbl_EmployeeIDSetting tr:last").remove();
                $('#tbl_EmployeeIDSetting > tbody').append(
                    '<tr>' +
                    '<td class="col-md-4"> <div class="input-group"><input class="form-control" id="txtPrefix" /></div></td>' +
                    '<td class="col-md-3"><div class="input-group"><input class="form-control" id="txtIDStartFrom" /></div></td>' +
                    '<td class="center-text-align col-md-4">' +
                    '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveEmployeeAlternativeSettings(this)"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>' +
                    '</td>' +
                    '</tr>')

                bindIntgerOnly("#txtIDStartFrom");
                bindCharsOnly("#txtPrefix");
            }
            if ($("#IsAdmin").val() > 0) {
                $('#tbl_EmployeeIDSetting th:nth-child(3)').hide();
                $('#tbl_EmployeeIDSetting td:nth-child(3)').hide();

            }
            // hideLoaderFrame();
        },
        error: function (data) { }
    });
}

function saveEmployeeAlternativeSettings(e) {
    var prefix = $("#txtPrefix").val();
    var EmployeeIDStartFrom = $("#txtIDStartFrom").val();
    if (prefix != "") {
        if (EmployeeIDStartFrom != "") {
            try {
                $.ajax({
                    type: 'POST',
                    data: { prefix: prefix, StartFrom: EmployeeIDStartFrom },
                    url: '/GeneralControl/SaveEmployeeIdSetting',
                    success: function (data) {
                        if (data.Success) {
                            ShowMessage(data.CssClass, data.Message);
                            var rowcount = $('#EmpRating-table tr').length;
                            $("#tbl_EmployeeIDSetting tr:last").children('td:eq(0)').html(prefix);
                            $("#tbl_EmployeeIDSetting tr:last").children('td:eq(1)').html(EmployeeIDStartFrom);
                            $("#tbl_EmployeeIDSetting tr:last").children('td:eq(2)').html('<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditEmployeeAlternativeSetting(this)" id="btnEdit" title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteAlternativeIDSetting(this)" title="Delete" ><i class="fa fa-times"></i> </a>');

                        }
                        else {
                            ShowMessage(data.CssClass, data.Message);
                        }

                    },
                    error: function (data) { }
                });
            } catch (e) {
                alert(e);
            }
        }
        else {
            ShowMessage('error', 'Please enter correct value for employee Id start field.');
        }
    }
    else if (EmployeeIDStartFrom != "") {
        try {
            $.ajax({
                type: 'POST',
                data: { prefix: prefix, StartFrom: EmployeeIDStartFrom },
                url: '/GeneralControl/SaveEmployeeIdSetting',
                success: function (data) {
                    if (data.Success) {
                        ShowMessage(data.CssClass, data.Message);
                        var rowcount = $('#EmpRating-table tr').length;
                        $("#tbl_EmployeeIDSetting tr:last").children('td:eq(0)').html(prefix);
                        $("#tbl_EmployeeIDSetting tr:last").children('td:eq(1)').html(EmployeeIDStartFrom);
                        $("#tbl_EmployeeIDSetting tr:last").children('td:eq(2)').html('<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditEmployeeAlternativeSetting(this)" id="btnEdit" title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(this)" title="Delete" ><i class="fa fa-times"></i> </a>');

                    }
                    else {
                        ShowMessage(data.CssClass, data.Message);
                    }
                },
                error: function (data) { }
            });
        } catch (e) {
            alert(e);
        }
    }
    else {
        ShowMessage('error', 'Please enter correct value for employee Id start field.');
    }

}

function EditEmployeeAlternativeSetting(e) {
    var prefix = $(e).closest('tr').children('td:eq(0)').html();
    var Startfrom = $(e).closest('tr').children('td:eq(1)').html();
    $(e).closest('tr').children('td:eq(0)').html('<div class="input-group"><input class="form-control" id="txtPrefix" value=' + prefix + '></div>');
    $(e).closest('tr').children('td:eq(1)').html('<div class="input-group"><input class="form-control positiveOnly" id="txtIDStartFrom" value=' + Startfrom + '></div>');
    $(e).closest('tr').children('td:eq(2)').html('<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveEmployeeAlternativeSettings(this)"> <img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');
    bindIntgerOnly("#txtIDStartFrom");
    bindCharsOnly("#txtPrefix");
}

function DeleteAlternativeIDSetting() {

    $.ajax({
        type: 'POST',
        url: '/GeneralControl/DeleteEmployeeIdSettingDetails',
        success: function (data) {
            ShowMessage(data.CssClass, data.Message);
            getEmployeeIDSetting()
        },
        error: function (data) { }
    });
}

function getGridEmployeeWorkEmailDetails() {

    oTableChannel = $('#tbl_EmployeeWorkEmail').dataTable({
        "sAjaxSource": "/GeneralControl/GetEmployeeWorkEmailList",
        "aoColumns": [

            // { "mData": "AcademicID", "bVisible": false, "sTitle": "AcademicID" },
            { "mData": "EmployeeAltId", "sTitle": "Employee ID", "bSortable": true, "mDataProp": "EmployeeAltId", "sWidth": "20%" },
            { "mData": "EmployeeName", "sTitle": "Employee Name", "bSortable": true, "mDataProp": "Employee Name", "sWidth": "40%" },
            { "mData": "WorkEmail", "sTitle": "Work Email", "bSortable": true, "mDataProp": "Work Email", "sWidth": "40%" }

        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_BankDetails");
            //hideLoaderFrame();
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTablePageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTablePageNo'));
        }
    });

}

function SaveEmployeeWorkEmailSetting() {
    var $form = $('form#frmAddEditWorkEmailSetting');
    if (!$form.valid()) {
        ShowMessage("error", "Please fill manadatory fields.");

    }
    else {
        var alterEmailMesg = $("#chkGenrateForNewEmployee").is(":checked") == true ? "Work emails will generate for employees which don't have email. Do you want to continue?. Do you want to continue?." : "While clicking on yes all existing work emails will get updated. Do you want to continue?";
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: alterEmailMesg }).done(function () {
            if ($("#chkGenrateForNewEmployee").is(":checked")) {
                isExistingChange = false;
            } else {
                isExistingChange = true;
            }
            $("#frmAddEditWorkEmailSetting").submit();

        }).fail(function () {

        });
    }
}

function getGridAbsentTypes() {

    oTableChannel = $('#tbl_PayAbsentType').dataTable({
        "sAjaxSource": "/GeneralControl/GetLeaveTypes",
        "aoColumns": [
            { "mData": "PayAbsentTypeName1", "sTitle": "Absent Type Name (EN)", "bSortable": true, "mDataProp": "PayAbsentTypeName", "sWidth": "15%" },
            { "mData": "PayAbsentTypeName2", "sTitle": "Absent Type Name (AR)", "bSortable": true, "mDataProp": "PayAbsentTypeName", "sWidth": "15%" },
            { "mData": "PayAbsentTypeName3", "sTitle": "Absent Type Name (FR)", "bSortable": true, "mDataProp": "PayAbsentTypeName", "sWidth": "15%" },
            { "mData": "IsDeductible", "sTitle": "Deductable", "bSortable": false, "mDataProp": "IsDeductible", "sWidth": "10%" },
            { "mData": "PaidType", "sTitle": "Paid Type", "bSortable": true, "mDataProp": "PaidType", "sWidth": "10%" },
            { "mData": "AnnualLeave", "sTitle": "Annual Leave", "bSortable": false, "mDataProp": "AnnualLeave", "sWidth": "10%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "19%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_PayAbsentType");
            // hideLoaderFrame();
        }
    });
}

function getGridDocumentSetting() {

    oTableChannel = $('#tbl_DocumentSetting').dataTable({
        "sAjaxSource": "/GeneralControl/GetDocumentExprirySetting",
        "aoColumns": [
            { "mData": "DocumentTypeName", "sTitle": "Document Type", "bSortable": true, "sWidth": "60%" },
            { "mData": "DocumentsAboutToExpireDays", "sTitle": "Documents About To Expire Days", "bSortable": true, "sWidth": "15%" },
            { "mData": "DocumentExpiredDays", "sTitle": "Document Expired Days", "bSortable": true, "sWidth": "15%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": true, "sWidth": "10%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            bindPositiveOnly(".number");
            // hideLoaderFrame();
        }
    });
}

function getGridEmployeeSponsor() {

    oTableChannel = $('#tbl_EmployeeSponsor').dataTable({
        "sAjaxSource": "/GeneralControl/GetEmployeeSponsor",
        "aoColumns": [
            { "mData": "SponsorName_1", "sTitle": "Sponsor Name (EN)", "sWidth": "20%" },
            { "mData": "SponsorName_2", "sTitle": "Sponsor Name (AR)", "sWidth": "20%" },
            { "mData": "SponsorName_3", "sTitle": "Sponsor Name (FR)", "sWidth": "20%" },
            { "mData": "IsActive", "sTitle": "Active", "bSortable": false, "sClass": "center-text-align", "sWidth": "20%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "20%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            //hideLoaderFrame();
        }
    });
}

function EditDocumentSetting(e, doucmentTypeId) {
    var $row = $(e).closest("tr");
    var DocumentsAboutToExpireDays = $row.find(".DocumentsAboutToExpireDays").val();
    var DocumentExpiryDays = $row.find(".DocumentExpiredDays").val();
    $.ajax({
        type: 'GET',
        url: "/GeneralControl/UpdateDocumentDaysSetting?DocumentTypeId=" + doucmentTypeId + "&DocumentsAboutToExpireDays=" + DocumentsAboutToExpireDays + "&DocumentExpiredDays=" + DocumentExpiryDays,
        success: function (data) {
            if (data.Success) {
                ShowMessage("success", data.Message);
            } else {
                ShowMessage("error", data.Message);
            }
        }
    })
}

function getvalidationSetting() {

    oTableChannel = $('#tblValidationSetting').dataTable({
        "sAjaxSource": "/GeneralControl/GetValidationSettingDetails",
        "aoColumns": [
            { "mData": "ValidationKey", "sTitle": "Validation key", "bSortable": true, "sWidth": "80%", "sClass": "" },
            { "mData": "IsMandatory", "sTitle": "Is Mandatory", "bSortable": false, "sWidth": "20%" },
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            bindPositiveOnly(".number");
            //hideLoaderFrame();
        }
    });
}

function updateValidationSetting(id) {
    var isChecked = $("#ValidationSetting_" + id).is(":checked");
    $.ajax({
        type: "POST",
        url: "/GeneralControl/UpdateValidationSetting",
        data: {
            ValidationSettingID: id,
            isChecked: isChecked
        },
        success: function (data) {
            ShowMessage(data.CssClass, data.Message);
            getvalidationSetting();
        }
    });
}


function getGridMOLTitleList() {
    oTableChannel = $('#tbl_MOLTitle').dataTable({
        "sAjaxSource": "/GeneralControl/GetMolTitleList",
        "aoColumns": [
            { "mData": "MolTitle_1", "sTitle": "MOL Title (EN)", "bSortable": true, "sWidth": "20%" },
            { "mData": "MolTitle_2", "sTitle": "MOL Title (AR)", "bSortable": true, "sWidth": "20%" },
            { "mData": "MolTitle_3", "sTitle": "MOL Title (FR)", "bSortable": true, "sWidth": "20%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": true, "sWidth": "15%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            // hideLoaderFrame();
        }
    });
}

function getSMTPConfig() {
    $.ajax({
        type: 'POST',
        url: '/GeneralControl/GetSMTPConfiguration',
        success: function (data) {
            if (data.FromEmailAddress != null) {
                $("#tbl_SMTPSetting tr:last").remove();
                $('#tbl_SMTPSetting > tbody').append(
                    '<tr>' +
                    '<td>' + data.FromEmailAddress + '</td>' +
                    '<td>' + data.FromDisplayName + '</td>' +
                    '<td>' + data.ReplyEmail + '</td>' +
                    '<td>' + data.SMTPServer + '</td>' +
                    '<td>' + data.PortNumber + '</td>' +
                    //'<td>' + data.SecureConnection + '</td>' +
                    //'<td>' + data.Username + '</td>' +
                    //'<td>' + data.Password + '</td>' + 
                    '<td class="center-text-align">' +
                    '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="AddEditGeneralControl(9)" id="btnEdit"  title="Edit"><i class="fa fa-pencil"></i> </a>' +
                    '<a class="btn btn-default btn-rounded btn-condensed btn-sm" onclick="ViewGeneralControl()" title="View" ><i class="fa fa-eye"></i> </a>' +
                    '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteSMTPConfig()" title="Delete" ><i class="fa fa-times"></i> </a>' +
                    '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="SendTestEmail()" title="Send Test Email" ><i class="fa fa-inbox"></i> </a>' +
                    '</td>' +
                    '</tr>')
            }
            else {
                $("#tbl_SMTPSetting tr:last").remove();
                $('#tbl_SMTPSetting > tbody').append(
                    '<tr>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    //'<td></td>' +
                    //'<td></td>' +
                    //'<td></td>' +
                    '<td class="center-text-align">' +
                    '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="AddEditGeneralControl(9)" id="btnEdit"  title="Update SMTP Config"><i class="fa fa-pencil"></i> </a>' +
                    '</td>' +
                    '</tr>')
            }
            hideLoaderFrame();
        },
        error: function (data) { }
    });
}

function DeleteSMTPConfig() {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete SMTP Configuration?." }).done(function () {
        $.ajax({
            type: 'POST',
            url: '/GeneralControl/DeleteSMTPConfiguration',
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                getSMTPConfig();
            },
            error: function (data) { }
        });
    });
}

function SendTestEmail() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/GeneralControl/SendTestEmail", function () {
        $("#modal_heading").text('Send Test Email');
        $('#myModal').modal({ backdrop: 'static' });
    });
}

function GetEmployeeOpeningBalance() {
    oTableChannel = $('#tbl_LeaveOpeningBalance').dataTable({
        "sAjaxSource": "/GeneralControl/GetEmployeeOpeningBalanceLeaves",
        "aoColumns": [
            { "mData": "index", "sTitle": "index", "sClass": "hidden", "sWidth": "8%" },
            { "mData": "OBID", "sTitle": "OBID", "sClass": "hidden", "sWidth": "8%" },
            { "mData": "VacationTypeID", "sTitle": "VacationTypeID", "sClass": "hidden", "sWidth": "8%" },
            { "mData": "EmpId", "sTitle": "Emp ID", "bSortable": true, "sClass": "hidden", "sWidth": "8%" },
            { "mData": "EmployeeID", "sTitle": "Emp ID", "bSortable": true, "sWidth": "8%" },
            { "mData": "EmployeeName", "sTitle": "Employee Name", "bSortable": true, "sWidth": "25%" },
            { "mData": "vacationName", "sTitle": "Leave Type", "bSortable": false, "sWidth": "25%" },
            { "mData": "OpeningBalance", "sTitle": "Opening Balance", "bSortable": true, "sWidth": "10%" },
            { "mData": "BalanceDate", "sTitle": "Balance Date", "bSortable": true, "sWidth": "10%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "20%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[5, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('OpeningBalancePageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('OpeningBalancePageNo'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_LeaveOpeningBalance");
            //hideLoaderFrame();
        }
    });

}

function EditOpeningBalance(e, i) {
    var openingBal = $(e).attr("data-OpeningBalance");
    var $row = $(e).parent().parent();
    $row.find('td:eq(7)').html('<input type="text" id="txtOpeningBal_' + i + '" class="form-control positiveOnly valid" value=' + openingBal + '>');
    var op = $row.find('td:eq(8)').text();
    $row.find('td:eq(8)').html('<input type="text" id="txtBalanceDate_' + i + '" class="form-control datepicker valid" value=' + op + '>');
    $("#btnRefresh_" + i).toggle();
    $("#btnEdit_" + i).toggle();
    bindPositiveOnly('.positiveOnly');
    bindDatePicker('.datepicker');
}

function RefreshRow(e, i) {
    var openingBal = $(e).attr("data-OpeningBalance");
    var openingBalDate = $(e).attr("data-OpeningBalDate");
    var $row = $(e).parent().parent();
    $row.find('td:eq(7)').html(openingBal);
    $row.find('td:eq(8)').html(openingBalDate);
    $("#btnRefresh_" + i).toggle();
    $("#btnEdit_" + i).toggle();
}

function PostOpeningBalance() {
    pageLoadingFrame("show");
    var listOpeningBalance = [];
    $('#tbl_LeaveOpeningBalance').DataTable({
        destroy: true,
        paging: false
    });

    var table = $('#tbl_LeaveOpeningBalance').DataTable();
    var rows = table.$(".positiveOnly", { "page": "all" }).closest('tr');
    var rowData = table.rows(rows).data();
    var Validated = true;
    $(rowData).each(function (i, item) {
        var curRow = {};
        var index = item[0];
        if ($('#txtOpeningBal_' + index).length > 0) {
            curRow.AccumulativeOBID = item[1]
            curRow.OpeningBalance = $('#txtOpeningBal_' + index).val();
            if ($('#txtBalanceDate_' + index).val() == '' || $('#txtBalanceDate_' + index).val() == undefined) {
                Validated = false;
            }
            curRow.OpeningBalanceDate = $('#txtBalanceDate_' + index).val();
            curRow.employeeId = item[3];
            curRow.vacationTypeId = item[2];
            listOpeningBalance.push(curRow);
        }
    });
    if (Validated) {
        if (listOpeningBalance.length > 0) {
            listOpeningBalance = JSON.stringify({ 'listOpeningBalance': listOpeningBalance });
            $.ajax({
                dataType: 'json',
                type: 'POST',
                data: listOpeningBalance,
                url: '/GeneralControl/PostEmployeeOpeningBalance',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    pageLoadingFrame("hide");
                    ShowMessage(data.CssClass, data.Message);
                    GetEmployeeOpeningBalance();

                },
                error: function (data) {
                    pageLoadingFrame("hide");
                }
            });
        }
        else {
            ShowMessage("error", "No records to save.");
            pageLoadingFrame("hide");
        }
    }
    else {
        ShowMessage("warning", "Please select balance date for edited records.");
        pageLoadingFrame("hide");
    }

}

function AddEditSponsor(id) {
    pageLoadingFrame("show");
    $("#modal_Loader").html("");
    $("#modal_heading").text(parseInt(id) == 0 ? "Add Sponsor" : "Edit Sponsor");
    $("#modal_Loader").load("/GeneralControl/AddUpdateSponsor/" + id, function () {
        $('#myModal').modal({ backdrop: 'static' });
        pageLoadingFrame("hide");
    });
}

function DeleteSponsor(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete sponsor." }).done(function () {
        pageLoadingFrame("show");
        $.ajax({
            type: 'POST',
            url: '/GeneralControl/DeleteSponsor',
            data: { "SponsorId": id },
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                getGridEmployeeSponsor();
                pageLoadingFrame("hide");
            },
            error: function (data) {
                pageLoadingFrame("hide");
            }
        });
    });
}

function getGridPayBankGroups() {

    oTableChannel = $('#tbl_BankGroupDetails').dataTable({
        "sAjaxSource": "/GeneralControl/GetBankGroupList",
        "aoColumns": [

            // { "mData": "AcademicID", "bVisible": false, "sTitle": "AcademicID" },
            { "mData": "BankGroupName_1", "sTitle": "Bank Group Name (EN)", "bSortable": true, "mDataProp": "BankGroupName_1", "sWidth": "15%" },
            { "mData": "BankGroupName_2", "sTitle": "Bank Group Name (AR)", "bSortable": true, "mDataProp": "BankGroupName_2", "sWidth": "15%" },
            //{ "mData": "BankGroupName_3", "sTitle": "Bank Group Name (FR)", "bSortable": true, "mDataProp": "BankGroupName_3", "sWidth": "15%" },
            { "mData": "SalaryPayableAccountCode", "sTitle": "Salary Payble Account Code", "bSortable": false, "mDataProp": "SalaryPayableAccountCode", "sWidth": "15%" },
            { "mData": "IsActive", "sTitle": "Active", "bSortable": true, "mDataProp": "IsActive", "sWidth": "15%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "12%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_BankGroupDetails");
            //hideLoaderFrame();
        }
    });
}

function getAcademicInstitutes() {

    oTableChannel = $('#tbl_AcademicInstitutes').dataTable({
        "sAjaxSource": "/GeneralControl/GetAcademicInstitutes",
        "aoColumns": [

            // { "mData": "HRInstituteID", "bVisible": false, "sTitle": "Institute ID" },
            { "mData": "HRInstituteName_1", "sTitle": "Institute Name (EN)", "bSortable": true, "mDataProp": "HRInstituteName_1", "sWidth": "15%" },
            { "mData": "HRInstituteName_2", "sTitle": "Institute Name (AR)", "bSortable": true, "mDataProp": "HRInstituteName_2", "sWidth": "15%" },
            { "mData": "HRInstituteName_3", "sTitle": "Institute Name (FR)", "bSortable": true, "mDataProp": "HRInstituteName_3", "sWidth": "15%" },
            { "mData": "IsActive", "sTitle": "Active", "bSortable": true, "mDataProp": "InstituteID", "sWidth": "12%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "13%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('AcademicsInstitute', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('AcademicsInstitute'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_AcademicInstitutes");
            // hideLoaderFrame();
        }
    });
}

function getQualifications() {

    oTableChannel = $('#tbl_Qualification').dataTable({
        "sAjaxSource": "/GeneralControl/GetQualifications",
        "aoColumns": [

            // { "mData": "HRQualificationID", "bVisible": false, "sTitle": "Qualification ID" },
            { "mData": "HRQualificationName_1", "sTitle": "Qualification Name (EN)", "bSortable": true, "mDataProp": "HRQualificationName_1", "sWidth": "15%" },
            { "mData": "HRQualificationName_2", "sTitle": "Qualification Name (AR)", "bSortable": true, "mDataProp": "HRQualificationName_2", "sWidth": "15%" },
            { "mData": "HRQualificationName_3", "sTitle": "Qualification Name (FR)", "bSortable": true, "mDataProp": "HRQualificationName_3", "sWidth": "15%" },
            { "mData": "IsActive", "sTitle": "Active", "bSortable": true, "mDataProp": "HRQualificationID", "sWidth": "12%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "13%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('Qualifications', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('Qualifications'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_Qualification");
            // hideLoaderFrame();
        }
    });
}

function getQualificationSubject() {

    oTableChannel = $('#tbl_QualificationSubject').dataTable({
        "sAjaxSource": "/GeneralControl/GetQualificationSubjects",
        "aoColumns": [

            // { "mData": "HRQualificationID", "bVisible": false, "sTitle": "Qualification ID" },
            { "mData": "HRQualificationName_1", "sTitle": "Subject Name (EN)", "bSortable": true, "mDataProp": "HRQualificationName_1", "sWidth": "15%" },
            { "mData": "HRQualificationName_2", "sTitle": "Subject Name (AR)", "bSortable": true, "mDataProp": "HRQualificationName_2", "sWidth": "15%" },
            { "mData": "HRQualificationName_3", "sTitle": "Subject Name (FR)", "bSortable": true, "mDataProp": "HRQualificationName_3", "sWidth": "15%" },
            { "mData": "IsActive", "sTitle": "Active", "bSortable": true, "mDataProp": "HRQualificationID", "sWidth": "12%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "13%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('QualificationsSubject', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('QualificationsSubject'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_QualificationSubject");
            // hideLoaderFrame();
        }
    });
}

function getTrainingCompetencies() {

    oTableChannel = $('#tbl_TrainingCompetencies').dataTable({
        "sAjaxSource": "/GeneralControl/GetTrainingCompetencies",
        "aoColumns": [

            // { "mData": "HRQualificationID", "bVisible": false, "sTitle": "Qualification ID" },
            { "mData": "TrainingName", "sTitle": "Training Name", "bSortable": true, "mDataProp": "TrainingName", "sWidth": "15%" },
            { "mData": "CompetencyTitle", "sTitle": "Competency Title", "bSortable": true, "mDataProp": "CompetencyTitle", "sWidth": "15%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "13%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('TrainingCompetencies', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('TrainingCompetencies'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_TrainingCompetencies");
            // hideLoaderFrame();
        }
    });
}
$(document).on('change', '#LargeLogoID', function () {
    var tmppath = window.URL.createObjectURL(event.target.files[0]);
    var file_name = event.target.files[0].name;
    var fileName = ""
    var fileExtention = file_name.split('.')[1];
    if ((fileExtention.toLowerCase() == "png") || (fileExtention.toLowerCase() == "jpg") || (fileExtention.toLowerCase() == "jpeg") || (fileExtention.toLowerCase() == "gif")) {
        if (file_name.indexOf("(") >= 0) {
            fileName = file_name.split('(')[0];
        }
        else {
            fileName = file_name.split('.')[0];
        }
        GetImageCropper(tmppath, "LargeLogo", fileName, fileExtention);
    }
    else {
        ShowMessage("error", "Only jpg, jpeg, png, gif file format allow.");
    }
    $("#LargeLogoID").val(null);
});

function GetImageCropper(id, chkImageFor, fileName, fileExtention) {
    $("#modal_Loader").load("/GeneralControl/GetImageCropper?id=" + id + "&chkImageFor=" + chkImageFor + "&fileName=" + fileName + "&fileExtention=" + fileExtention, function () {
        bindSelectpicker('.selectpickerddl');
        $("#myModal").modal("show");
        $("#modal_heading").text('Crop Image');
        $(".modal-dialog").attr("style", "width:800px;");
    });
}

function ClosePriview(id) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/GeneralControl/DeleteImage',
        success: function (data) {
            if (id == "L") {
                $("#previewL").hide();
                $("#RemoveL").hide();
            }
            if (id == "S") {
                $("#previewS").hide();
            }
            if (id == "R") {
                $("#previewR").hide();
            }
            $("#file_caption_name").hide();
            $("#file_caption_id").html('');
        },
        error: function (data) { }
    });
}

function ApplicationLogoModal(modalId) {
    $("#modal_Loader3").load("/GeneralControl/ApplicationLogoModal?modalId=" + modalId, function () {
        bindSelectpicker('.selectpickerddl');
        $("#myModal3").modal("show");
        $("#modal_heading3").text('Add Logo');
        $(".modal-dialog").attr("style", "width:700px;");
    });
}

function SaveApplicationLogo() {
    var selectedFile = $("#file_caption_id").html();
    var modalId = $("#modalId").text();
    if (selectedFile == "") {
        ShowMessage("error", "Please browse the logo.");
    }
    else {
        $.ajax({
            dataType: 'json',
            type: 'GET',
            url: '/GeneralControl/SaveApplicationLogo',
            success: function (data) {
                $("#myModal3").modal("hide");
                ShowMessage("success", data.message);
                if (modalId == "modalL") {
                    $('#LargeLogoViewID').css("background", 'url(' + data.SavedImagePath + ')');
                    GetImage(1);
                }
                if (modalId == "modalS") {
                    $('#SmallLogoViewID').css("background", 'url(' + data.SavedImagePath + ')');
                    GetImage(2);
                }
                if (modalId == "modalR") {
                    GetImage(3);
                }
            },
            error: function (data) { }
        });
    }
}

function DeleteLogo() {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/GeneralControl/DeleteImage',
        success: function (data) {
            $("#myModal3").modal("hide");
        },
        error: function (data) { }
    });
}

function GetImage(id) {
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/Home/GetApplicationLogo?mode=' + id,
        success: function (data) {
            var imagePath = window.location.origin + data.imagepath;
            if (id == 1) {
                $("#PreviewLargeID").show();
                $("#GetLogoLarge").attr("src", imagePath);
            }
            if (id == 2) {
                $("#PreviewSmallID").show();
                $("#GetSmallLarge").attr("src", imagePath);
            }
            if (id == 3) {
                $("#PreviewReportID").show();
                $("#GetReportLogo").attr("src", imagePath);
            }
            //$("#myModal3").modal("hide");
            //ShowMessage("success", data.message);
        },
        error: function (data) { }
    });
}

function getPayCategories() {
    oTableChannel = $('#tbl_PayCategory').dataTable({
        "sAjaxSource": "/GeneralControl/GetPayCategories",
        "aoColumns": [
            // { "mData": "HRQualificationID", "bVisible": false, "sTitle": "Qualification ID" },
            { "mData": "PayCategoryName1", "sTitle": "Pay Category (EN)", "bSortable": true, "mDataProp": "PayCategoryName1", "sWidth": "15%" },
            { "mData": "PayCategoryName2", "sTitle": "Pay Category (AR)", "bSortable": true, "mDataProp": "PayCategoryName2", "sWidth": "13%" },
            { "mData": "PayCategoryName3", "sTitle": "Pay Category (FR)", "bSortable": true, "mDataProp": "PayCategoryName3", "sWidth": "13%" },
            { "mData": "Seq", "sTitle": "Pay Category Sequence", "bSortable": true, "mDataProp": "Seq", "sWidth": "4%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "13%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('PayCategories', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('PayCategories'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_PayCategory");
            //  hideLoaderFrame();
        }
    });
}

function CheckCostCenterValidation() {
    $.ajax({
        type: "GET",
        url: "/GeneralControl/GetCostCenterValidation",
        cache: false,
        success: function (data) {
            if (!data.Success) {
                $('#warningMessage').show();
                $('#lblTotalEmployeeNotinPayroll').html(data.Message);
            } else {
                $('#warningMessage').hide();
            }
        }
    });
}

function GetSundriesTypes() {
    //
    oTableChannel = $('#tbl_SundryCategory').dataTable({
        "sAjaxSource": "/GeneralControl/GetPaySundries",
        "aoColumns": [
            { "mData": "PaySundryName1", "sTitle": "Pay Sundry (EN)", "bSortable": true, "mDataProp": "PaySundryName1", "sWidth": "15%" },
            { "mData": "PaySundryName2", "sTitle": "Pay Sundry (AR)", "bSortable": true, "mDataProp": "PaySundryName2", "sWidth": "15%" },
            { "mData": "PaySundryName3", "sTitle": "Pay Sundry (FR)", "bSortable": true, "mDataProp": "PaySundryName3", "sWidth": "15%" },
            { "mData": "AccountCode", "sTitle": "Account Code", "bSortable": true, "mDataProp": "AccountCode", "sWidth": "15%" },
            { "mData": "IsAccommodation", "sTitle": "Is Accommodation", "bSortable": false, "sWidth": "13%" },
            { "mData": "IsFurniture", "sTitle": "Is Furniture", "bSortable": false, "sWidth": "13%" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false, "sWidth": "13%" }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('PaySundry', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('PaySundry'));
        },
        "fnDrawCallback": function () {
            removeSpan("#tbl_SundryCategory");
            //   hideLoaderFrame();
        }
    });

}


function CheckAlreadyPresent(e, type) {
    var PaySundryID = $("#PaySundryID").val();
    if ($(e).is(":checked")) {
        if (type == 1) {
            $("#IsFurniture").prop("checked", false);
            $("#divAccomodation").removeClass("hidden");
            $("#divFurniture").addClass("hidden");

        }
        else {
            $("#IsAccommodation").prop("checked", false);
            $("#divAccomodation").addClass("hidden");
            $("#divFurniture").removeClass("hidden");

        }

        $.ajax({
            type: 'GET',
            url: '/GeneralControl/CheckIfAlreadySundrySelected?type=' + type + '&SundryId=' + PaySundryID,
            success: function (data) {
                if (data.Success)
                    //ShowMessage(data.result, data.resultMessage);
                    if (type == 1) {
                        $("#warningMessageAcSundry").removeClass("hidden");
                    }
                    else {
                        $("#warningMessageFurSundry").removeClass("hidden");
                    }

            },
            error: function (data) { }
        });

    }
    else {

        if (type == 1) {
            $("#warningMessageAcSundry").addClass("hidden");
            $("#divFurniture").removeClass("hidden");
        }
        else {
            $("#warningMessageFurSundry").addClass("hidden");
            $("#divAccomodation").removeClass("hidden");
        }
    }

}

function SaveShortLeaveSettings() {
    var selectedIds = getSelectedIds(".multiEmployee").join(",");
    $("#EmployeeIdsToNotifyUponApproval").val(selectedIds);
    $("#frmShortLeaveSettings").submit();
}

function ShowShortLeaveSettings() {
    $.ajax({
        type: 'GET',
        url: '/GeneralControl/ShowShortLeaveSettings',
        success: function (data) {
            $("#divShortLeaveSettings").html(data);
            $("#EmployeeListddl option:contains(All Employee)").attr('selected', 'selected');
            $('#EmployeeListddl').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '300', includeSelectAllOption: true });
            bindSelectpicker('.selectpickerddl');
        },
        error: function (data) { }
    });
}

function onShortLeaveSettingsSuccess(arg) {
    if (arg.InsertedRowId != 0)
        $("#ShortLeaveSettingId").val(arg.InsertedRowId);
    ShowMessage(arg.CssClass, arg.Message);
}

function onShortLeaveSettingsFailure(arg) {
    ShowMessage(arg.CssClass, arg.Message);
}

function getGridInsuranceCategoryAmounts() {
    oTableChannel = $('#tbl_InsuranceCategoryAmount').dataTable({
        "sAjaxSource": "/GeneralControl/InsuranceCategoryAmounts",
        "aoColumns": [

            { "mData": "CatAmountID", "bVisible": true, "sClass": "hidden" },
            { "mData": "HRInsuranceTypeID", "bVisible": true, "sClass": "hidden" },
            { "mData": "InsuranceTypeName", "sTitle": "Insurance Category", "bSortable": true, "mDataProp": "InsuranceTypeName" },
            { "mData": "ACYearID", "bVisible": true, "sClass": "hidden" },
            { "mData": "AcademicYear", "sTitle": "Academic Year", "bSortable": true, "mDataProp": "AcademicYear" },
            { "mData": "Amount", "sTitle": "Amount", "bSortable": true, "mDataProp": "Amount" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false }
        ],
        "fnServerParams": function (aoData) {
        },
        language: {
            sLoadingRecords: "<img src='/Content/assets/img/loaders/default.gif'>",
        },
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "autoWidth": false,
        "bSortCellsTop": true,
        "order": [[1, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_InsuranceCategoryAmount");
            // hideLoaderFrame();
        }
    });
}
var InsuranceAmountList = [];
var i = 0;
function AddMultipleInsuranceAmount(id) {
    InsuranceAmountList = [];
    $("#modal_Loader").load("/GeneralControl/AddMultipleInsuranceAmount/" + id, function () {
        bindSelectpicker('.selectpickerddl');
        if (id == 0) {
            $("#modal_heading").text('Add Insurance Category Amount');
        }
        else {
            $("#modal_heading").text('Edit Insurance Category Amount');
        }
        bindSelectpicker('.selectpickerddl');
        $('#myModal').modal({ backdrop: 'static' });
    });
}

function saveAmountRow(e) {
    i = i + 1;
    var academicyearId = $("#ACYearID").val();
    var academicYear = $("#ACYearID option:selected").text();
    var InsuranceType = ($("#HRInsuranceTypeID option:selected").text() + " (" + academicYear + ")");
    var insuranceTypeId = $("#HRInsuranceTypeID").val();
    var amount = $("#Amount").val();
    var IsExistRecord = true;
    var IsExistRecordInDB = false;
    if (!ValidateRow()) {
        IsExistRecordInDB = IsExistsCategoryAmount(insuranceTypeId, academicyearId, amount);
        if (IsExistRecordInDB) {
            ShowMessage("error", "Insurance category amount already exist select other insurance type or year");
            return;
        }
        else {
            var countRec = 0;
            InsuranceAmountList.forEach(function (p) {
                if (p.HRInsuranceTypeID === insuranceTypeId && p.ACYearID === academicyearId && p.Amount === amount) {
                    countRec++;
                }
            });
            if (countRec == 0) {
                IsExistRecord = false;
            }
        }
        if (!IsExistRecord) {
            InsuranceAmountList.push({
                "HRInsuranceTypeID": insuranceTypeId,
                "ACYearID": academicyearId,
                "AcademicYear": academicYear,
                "InsuranceType": InsuranceType,
                "Amount": amount,
            });
            reloadcategoryAmountTbale();
        }
        else {
            ShowMessage("error", "You already added amount for this select other insurance type or year");
        }
    }
    else {
        return;
    }
}

function ValidateRow() {
    var academicYearId = $("#ACYearID").val();
    var insuranceTypeId = $("#HRInsuranceTypeID").val();
    var amount = $("#Amount").val();
    var IsValidAllFields = false;

    if (insuranceTypeId == "0" || insuranceTypeId == "") {
        IsValidInputField("HRInsuranceTypeID", 1);
        IsValidAllFields = true;
    } else {
        IsValidInputField("HRInsuranceTypeID", 2);
    }
    if (academicYearId == "0" || academicYearId == "") {
        IsValidInputField("ACYearID", 1);
        IsValidAllFields = true;
    } else {
        IsValidInputField("ACYearID", 2);
    }
    if (amount == 0) {
        IsValidInputField("txtAmount", 1);
        IsValidAllFields = true;
    }
    else {
        IsValidInputField("Amount", 2);
    }
    return IsValidAllFields;
}

function IsExistsCategoryAmount(insurancetypeId, academicYearId, amount) {
    var IsExistRecordInDB = false;
    $.ajax({
        url: '/GeneralControl/CheckInsuranceCategoryAmountExists',
        dataType: 'json',
        type: "POST",
        data: { insuranceTypeId: insurancetypeId, academicYearId: academicYearId, amount: amount },
        async: false,
        success: function (data) {
            IsExistRecordInDB = data.IscategoryAmountExists;
        },
        error: function (xhr) {
        }
    });
    return IsExistRecordInDB;
}

function IsValidInputField(id, mode) {
    if (mode == 1) {
        if ($("#" + id).parent().find('.field-validation-error').length == 0)
            $("#" + id).parent().append('<span class=\"field-validation-error\">This field is mandatory</span>');
    }
    else if (mode == 2) {
        if ($("#" + id).parent().find('.field-validation-error').length > 0)
            $("#" + id).parent().find('.field-validation-error')[0].remove();
    }
}

function reloadcategoryAmountTbale() {
    var k = 0;
    $("#categoryAmount-table > tbody").empty();

    var j = Object.keys(InsuranceAmountList).length;

    InsuranceAmountList.forEach(function (p, index) {
        k = k + 1;
        if (j == 1) {

            $('#categoryAmount-table > tbody').append(
                '<tr id="rowId_' + p.HRInsuranceTypeID + '" class="edit' + p.HRInsuranceTypeID + '">' +
                '<td class="col-md-1 hidden">' + p.HRInsuranceTypeID + '</td>' +
                '<td class="col-md-4">' + p.InsuranceType + '</td>' +
                '<td class="col-md-3">' + p.Amount + '</td>' +
                '<td class="col-md-3 center-text-align padding-0">' +
                '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditCategoryAmounts(' + index + ',this)" id="btnEdit" data-id=' + p.HRInsuranceTypeID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
                '<a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddNewCategoryAmount()" title="Add"><i class="fa fa-plus"></i></a>' +
                '</td>' +
                '</tr>')

        }

        else if (k < j) {
            $('#categoryAmount-table > tbody').append(
                '<tr id="rowId_' + p.HRInsuranceTypeID + '" class="edit' + p.HRInsuranceTypeID + '">' +
                '<td class="col-md-1 hidden">' + p.HRInsuranceTypeID + '</td>' +
                '<td class="col-md-4">' + p.InsuranceType + '</td>' +
                '<td class="col-md-3">' + p.Amount + '</td>' +
                '<td class="col-md-3 center-text-align padding-0">' +
                '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditCategoryAmounts(' + index + ',this)" id="btnEdit" data-id=' + p.HRInsuranceTypeID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
                '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(' + index + ')" title="Delete"><i class="fa fa-times"></i> </a>' +
                '</td>' +
                '</tr>')

        }
        else if (k == j) {

            $('#categoryAmount-table > tbody').append(
                '<tr id="rowId_' + p.HRInsuranceTypeID + '" class="edit' + p.HRInsuranceTypeID + '">' +
                '<td class="col-md-1 hidden">' + p.HRInsuranceTypeID + '</td>' +
                '<td class="col-md-4">' + p.InsuranceType + '</td>' +
                '<td class="col-md-3">' + p.Amount + '</td>' +
                '<td class="col-md-3 center-text-align padding-0">' +
                '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditCategoryAmounts(' + index + ',this)" id="btnEdit" data-id=' + p.HRInsuranceTypeID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
                '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(' + index + ')" title="Delete"><i class="fa fa-times"></i> </a>' +
                '<a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddNewCategoryAmount()" title="Add"><i class="fa fa-plus"></i></a>' +
                '</td>' +
                '</tr>')
        }
    });

}

function AddNewCategoryAmount() {
    $("#btnAddRow" + i + "").hide();
    $('#categoryAmount-table > tbody').append(
        '<tr>' +
        '<td class="col-md-1 hidden"></td>' +
        '<td class="col-md-4">' +
        '<div class=""><select  class="ddlInsuranceType form-control selectpickerddl" data-live-search="true" id="HRInsuranceTypeID">' + $("#ddlPreInsuranceType").text() + ' </select></div>' +
        '</td>' +
        '<td class="col-md-3">' +
        '<div class=""><input class="form-control txtInput txtAmount"  id="Amount" type="number" /></div>' +
        '</td>' +
        '<td class="center-text-align col-md-3 padding-0">' +
        '<div class="center-text-align">' +
        '<button type="button" id="btnRowSave" class="tableButton" onclick="saveAmountRow(this)" class="btnAdd">' +
        '<img src="/Content/images/tick.ico" style="width:16px;"/>' +
        '</button>' +
        '<button class="tableButton" id="btnRowCancel" onClick="CancelNewAddedRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/>' +
        '</button>' +
        '</div>' +
        '</td>' +
        '</tr>');
    bindSelectpicker(".selectpickerddl")
}

function CancelNewAddedRow(obj) {
    $(obj).parents('tr').remove();
    $("#btnAddRow" + i + "").show();
}

function DeleteRow(id) {
    InsuranceAmountList.splice(id, 1);
    reloadcategoryAmountTbale();
}

function EditCategoryAmounts(RecordIndex, e) {
    var $row = $(e).parent().parent();
    var insuranceTypeId = $row.find('td:eq(0)').text();
    var amount = $row.find('td:eq(2)').text();

    $($row).find('td:eq(1)').html('<div class=""><select class="ddlInsuranceType form-control selectpickerddl" data-live-search="true" id="HRInsuranceTypeID">' + $("#ddlPreInsuranceType").text() + ' </select></div>');
    $($row).find('td:eq(2)').html('<div class=""><input class="form-control txtInput txtAmount"  id="Amount" type="number"/></div>');
    $($row).find('td:eq(3)').html('<div class="center-text-align"><button id="btnSaveEdited" class="tableButton" onclick="saveEditedRow(' + RecordIndex + ',this,' + amount + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');

    $("#txtAmount").val(amount);

    var select = $row.find(".ddlInsuranceType");
    $(select).val(insuranceTypeId);// .attr("selected", "selected");  

    bindSelectpicker(".selectpickerddl")
    $('#categoryAmount-table .selectpickerddl').selectpicker();
}

function saveEditedRow(RecordIndex, e, amount) {
    var academicyearId = $("#ACYearID").val();
    var academicYear = $("#ACYearID option:selected").text();
    var InsuranceType = ($("#HRInsuranceTypeID option:selected").text() + " (" + academicYear + ")");
    var insuranceTypeId = $("#HRInsuranceTypeID").val();
    var amount = $("#Amount").val();
    var IsExistRecord = true;
    var IsExistRecordInDB = false;
    if (!ValidateRow()) {
        IsExistRecordInDB = IsExistsCategoryAmount(insuranceTypeId, academicyearId, amount);
        if (IsExistRecordInDB) {
            ShowMessage("error", "Insurance category amount already exist select other insurance type or year");
            return;
        }
        else {
            var countRec = 0;
            InsuranceAmountList.forEach(function (p, index) {
                if (index != RecordIndex) {
                    if (p.HRInsuranceTypeID === insuranceTypeId && p.ACYearID === academicyearId && p.Amount === amount) {
                        countRec++;
                    }
                }
            });
            if (countRec == 0) {
                IsExistRecord = false;
            }
        }
        if (!IsExistRecord) {
            InsuranceAmountList[RecordIndex].HRInsuranceTypeID = insuranceTypeId;
            InsuranceAmountList[RecordIndex].ACYearID = academicyearId;
            InsuranceAmountList[RecordIndex].InsuranceType = InsuranceType;
            InsuranceAmountList[RecordIndex].AcademicYear = academicYear;
            InsuranceAmountList[RecordIndex].Amount = amount;
            reloadcategoryAmountTbale();
        } else {
            ShowMessage("error", "Insurance category amount already exist select other insurance type or year");
        }

    }
    else {
        return;
    }
}

function changeStatus(catAmountId) {

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { catAmountId: catAmountId },
        url: '/GeneralControl/DeleteInsuranceCategoryAmount',
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
            getGridInsuranceCategoryAmounts(1);
        }
    });
}

function SaveInsuranceCategoryAmountList() {
    if (Object.keys(InsuranceAmountList).length == 0) {
        ShowMessage("error", "Please enter Insurance category amount ");
    }
    else {
        $.ajax({
            url: '/GeneralControl/InsertInsuranceCategoryAmount',
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            data: JSON.stringify({ 'insuranceAmountList': InsuranceAmountList }),
            success: function (data) {
                $("#myModal").modal('hide');
                if (data.Success) {
                    if (data.CssClass == "Request") {
                        ``
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                } else {
                    ShowMessage("error", data.Message);
                }
                getGridInsuranceCategoryAmounts(1);
                InsuranceAmountList = [];
            },
            error: function (xhr) {
            }
        });
    }
}

function EditInsuranceAmountFromList(e, catAmountId) {

    $tr = $(e).closest("tr");
    var insuranceTypeId = $row.find('td:eq(1)').text();
    var catAmountId = $row.find('td:eq(0)').text();
    var academicYearId = $row.find('td:eq(3)').text();
    var amount = $row.find('td:eq(2)').text();

    $($row).find('td:eq(1)').html('<div class=""><select class="ddlInsuranceType form-control selectpickerddl" data-live-search="true" id="ACYearID">' + $("#ddlPreInsuranceType").text() + ' </select></div>');
    $($row).find('td:eq(1)').html('<div class=""><select class="ddlAcademicYear form-control selectpickerddl" data-live-search="true" id="ddlAcademicYear">' + $("#ddlPreAcademicYear").text() + ' </select></div>');
    $($row).find('td:eq(2)').html('<div class=""><input class="form-control txtInput txtAmount"  id="txtAmount" type="number"/></div>');
    $($row).find('td:eq(3)').html('<div class="center-text-align"><button id="btnSaveEdited" class="tableButton" onclick="UpdateInsuranceCategoryAmount(' + RecordIndex + ',this,' + catAmountId + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');

    $("#txtAmount").val(amount);

    var select = $row.find(".ddlInsuranceType");
    $(select).val(insuranceTypeId);// .attr("selected", "selected");  

    var select = $row.find(".ddlAcademicYear");
    $(select).val(academicYearId);// .attr("selected", "selected");  
    bindSelectpicker(".selectpickerddl")
}

//function UpdateInsuranceCategoryAmount(e, catAmountId) {
//    var $tr = $(e).closest("tr");
//    var academicyearId = $($tr).find("#ddlAcademicYear").val();
//    var academicYear = $($tr).find("#ddlAcademicYear option:selected").text();
//    var InsuranceType = $($tr).find("#ddlInsuranceType option:selected").text();
//    var insuranceTypeId = $($tr).find("#ddlInsuranceType").val();
//    var amount = $($tr).find("#txtAmount").val();

//    IsExistRecordInDB = IsExistsCategoryAmount(insuranceTypeId, academicyearId, amount);
//    if (IsExistRecordInDB) {
//        ShowMessage("error", "Insurance category amount already exist select other insurance type or year");
//        return;
//    }
//    else {
//        var InsuranceCategoryAmount = {
//            CatAmountID = catAmountId,
//            HRInsuranceTypeID = insuranceTypeId,
//            ACYearID = academicyearId,
//            InsuranceTypeName = InsuranceType,
//            AcademicYear = academicYear,
//            Amount = amount,

//        }
//        $.ajax({
//            dataType: 'json',
//            type: 'POST',
//            data: InsuranceCategoryAmount,
//            url: '/SalaryRange/UpdateInsuranceCategoryAmount',
//            success: function (data) {
//                ShowMessage(data.result, data.resultMessage);
//                reloadcategoryAmountTbale();
//            }
//        });
//    }
//}
function ViewDocumentReminders() {
    $("#documentReminders").load("/GeneralControl/AddEditDocumentReminder", function () {
        bindPositiveOnly('.positiveOnly');
        bindSelectpicker('.selectpickerddl');
    });
}
function AboutToExpireChanged() {
    $("#ExpiringNumberOfDays").rules('remove');
    if ($('#chkAboutToExpire').is(":checked")) {
        $('#expiringOptions').show();
        //$("#ExpiringNumberOfDays").rules('add', 'required');
        $("#ExpiringNumberOfDays").rules('add', {
            required: true,
            range: [1, 100],
            messages: {
                required: "Please enter number of days"
            }
        });
    }
    else {
        $('#expiringOptions').hide();
        $("#ExpiringNumberOfDays").rules('remove');
    }
}

function AlreadyExpireChanged() {
    $("#ExpiredNumberOfDays").rules('remove');
    if ($('#chkAlreadyExpire').is(":checked")) {
        $('#expiredOptions').show();
        //$("#ExpiredNumberOfDays").rules('add', 'required');
        $("#ExpiredNumberOfDays").rules('add', {
            required: true,
            range: [1, 100],
            messages: {
                required: "Please enter number of days"
            }
        });
    }
    else {
        $('#expiredOptions').hide();
        $("#ExpiredNumberOfDays").rules('remove');
    }
}
function ViewDaysOptions(elm, detailedDiv) {
    if ($(elm).is(":checked")) {
        //$("#DropInEmployeeModel_LessonTopic").rules(ruleMode, 'required')

        $(detailedDiv).show();
    }
    else
        $(detailedDiv).hide();
}
function FrequencyChanged(elm) {
    var frequency = $(elm).val();
    var message = "";
    if (frequency == "Daily") {
        message = $(elm).data('daily-reminder-msg');
    }
    else if (frequency == "Weekly") {
        message = $(elm).data('weekly-reminder-msg');
    }
    else if (frequency == "Monthly") {
        message = $(elm).data('monthly-reminder-msg');
    }
    $('#frequencyMsg .frequency-msg').html(message);
}
function ViewAttendanceReminders() {
    $("#attendanceReminders").load("/GeneralControl/AddEditAttendanceReminder", function () {
        bindPositiveOnly('.positiveOnly');
        bindSelectpicker('.selectpickerddl');
    });
}
function SetReminderMode() {

    $("#btnSaveReminderSetup").unbind("click");
    $('#btnSaveReminderSetup').click(function () {
        var selectedMode = $(".nav-tabs .active").data('mode');
        var formSelector = '';
        if (selectedMode == 'Document')
            formSelector = '#frmAddEditQualifications';
        else if (selectedMode == 'Attendance')
            formSelector = '#frmAddEditAttendanceReminder';
        $(formSelector).validate();
        $(formSelector).submit();
    });
}
function GetSelectedCompetencies() {
    var trainingId = $('#TrainingID').val();
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/GeneralControl/GetSelectedTrainingCompetencies?trainingId=' + trainingId,
        success: function (data) {
            //Remove the element and re-added
            $('#ddlCompetenciesList').multiselect('destroy');
            $('.ddlCompetencies').html('');
            $('<select multiple="multiple" id="ddlCompetenciesList" class="form-control" name="CompetenciesList">').appendTo('.ddlCompetencies');
            $.each(data, function (i, item) {
                $('#ddlCompetenciesList').append($('<option></option>', {
                    value: item.Value,
                    text: item.Text
                }));
            });
            $('#ddlCompetenciesList').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '200', includeSelectAllOption: true });

        },
        error: function (data) { }
    });
}