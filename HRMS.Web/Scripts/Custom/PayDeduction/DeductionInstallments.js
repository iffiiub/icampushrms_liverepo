﻿var Installments = [];
var ModifiedInstallments = [];
var NoOfDecimalPlaces = 0;

$(document).ready(function () {
    GetAllInstallments($("#hdnPayDeductionID").val());
    $("#tbl_EditPayDeductionDetails1").DataTable();
    $(".close").addClass('hidden');
    NoOfDecimalPlaces = $("#NoOfDecimalPlaces").val();
});


function EditAll(payDeductionId) {
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/Deduction/GetDeductionInstallmentDetails?PayDeductionId=' + payDeductionId,
        success: function (data) {
            var firstRecord = "IsFirstRecord";
            Installments = data.list;
            $('#tbl_EditPayDeductionDetails1 > tbody').html('');
            $.each($(ModifiedInstallments), function (ind, item) {
                if (!item.IsDeleted) {
                    var chkActiveStatus = '';
                    var chkWaivedStatus = '';
                    if (item.IsActive) {
                        chkActiveStatus = 'checked';
                    }
                    if (item.IsWaived) {
                        chkWaivedStatus = 'checked';
                    }
                    if (!item.IsProcessed) {

                        $('#tbl_EditPayDeductionDetails1 > tbody').append(
                             '<tr id="rowId_' + item.PayDeductionDetailID + '" data-rowId=' + item.PayDeductionDetailID + ' class=' + firstRecord + '>' +
                               '<td class="col-md-3">' +
                                    '<div class=""><input type="textbox" class="form-control datepicker" id="InstallmentDate' + item.PayDeductionDetailID + '" Value="' + item.DeductionDate + '"></div>' +
                               '</td>' +
                               '<td class="col-md-3">' +
                                   '<div class=""><input class="form-control positiveOnly"  id="txtAmount' + item.PayDeductionDetailID + '" Value="' + item.Amount + '"/></div>' +
                               '</td>' +
                               '<td class="col-md-3">' +
                                   '<div class=""><input class="form-control"  id="txtComments' + item.PayDeductionDetailID + '" value="' + item.Comments + '" /></div>' +
                               '</td>' +
                               '<td class="col-md-1">' +
                                   '<input ' + chkActiveStatus + ' id="chkActive_' + item.PayDeductionDetailID + '" name="chkActive_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsActive + '">' +
                               '</td>' +
                               '<td class="col-md-1">' +
                                   '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item.PayDeductionDetailID + '" name="chkWaived_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsWaived + '">' +
                               '</td>' +
                               '<td class="center-text-align col-md-2 padding-0">' +
                                   '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>' +
                               '</td>' +
                             '</tr>');
                        bindDatePicker('.datepicker');
                        bindPositiveOnly('.positiveOnly');
                    }
                    else {
                        $('#tbl_EditPayDeductionDetails1 > tbody').append('<tr id="rowId_' + item.PayDeductionDetailID + '" class=' + firstRecord + '>' +
                                   '<td>' + item.DeductionDate + '</td>' +
                                   '<td>' + item.Amount + '</td>' +
                                   '<td>' + item.Comments + '</td>' +
                                   '<td class="col-md-1">' +
                                   '<input ' + chkActiveStatus + ' disabled id="chkActive_' + item.PayDeductionDetailID + '" name="chkActive_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsActive + '">' +
                                   '</td>' +
                                   '<td class="col-md-1">' +
                                   '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item.PayDeductionDetailID + '" name="chkWaived_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsWaived + '">' +
                                   '</td>' +
                                   '<td><a disabled class="btn btn-success btn-rounded btn-condensed btn-sm" data-id="' + item.PayDeductionDetailID + '" onclick="EditDeductionDetail(' + item.PayDeductionDetailID + ')" title="Edit"><i class="fa fa-pencil"></i> </a></td>' +
                                 '</tr>')
                    }
                }
                firstRecord = "NoIsFirstRecord";
            });
            //$("#tbl_EditPayDeductionDetails1 tbody").append('<tr>' +
            //               '<td><label class="label-form">Total Deduction Amount</label></td>' +
            //               '<td><label class="label-form" id="TotalDedAmount">' + data.TotalAmount + '</label></td>' +
            //               '<td><label class="label-form">Total Installment Amount</label></td>' +
            //               '<td><label class="label-form" id="TotalDedAmount">' + data.TotalAmount + '</label></td>' +
            //           '</tr>');

        },
        error: function (data) { }
    });
}

function EditDeductionDetail(e) {
    var clickedIndex = $(e).closest("tr").index();
    $("#tbl_EditPayDeductionDetails1 tbody tr").each(function (i, item) {
        var id = $(this).attr('data-rowId');
        if (clickedIndex == i) {
            $.each((ModifiedInstallments), function (k, item1) {
                var chkActiveStatus = '';
                if (item1.PayDeductionDetailID == id) {
                    if (!item1.IsProcessed) {
                        if (item1.IsActive) {
                            chkActiveStatus = 'checked';
                        }
                        var chkWaivedStatus = '';
                        if (item.IsWaived) {
                            chkWaivedStatus = 'checked';
                        }
                        $(item).html('<td class="col-md-3">' +
                            '<div class=""><input type="textbox" class="form-control datepicker" id="InstallmentDate' + item1.PayDeductionDetailID + '" Value="' + item1.DeductionDate + '"></div>' +
                       '</td>' +
                       '<td class="col-md-3">' +
                           '<div class=""><input class="form-control positiveOnly"  id="txtAmount' + item1.PayDeductionDetailID + '" Value="' + item1.Amount + '"/></div>' +
                       '</td>' +
                       '<td class="col-md-3">' +
                           '<div class=""><input class="form-control"  id="txtComments' + item1.PayDeductionDetailID + '" value="' + item1.Comments + '" /></div>' +
                       '</td>' +
                       '<td class="col-md-1">' +
                                   '<input ' + chkActiveStatus + ' id="chkActive_' + item1.PayDeductionDetailID + '" name="chkActive_' + item1.PayDeductionDetailID + '" type="checkbox" value="' + item1.IsActive + '">' +
                               '</td>' +
                       '<td class="col-md-1">' +
                                   '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item1.PayDeductionDetailID + '" name="chkWaived_' + item1.PayDeductionDetailID + '" type="checkbox" value="' + item1.IsWaived + '">' +
                               '</td>' +
                       '<td class="center-text-align col-md-3 padding-0">' +
                           '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>' +
                       '</td>');
                    }
                }
            });
            bindDatePicker('.datepicker');
            bindPositiveOnly('.positiveOnly');
        }
    });
}

function GetAllInstallments(payDeductionId) {
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/Deduction/GetDeductionInstallmentDetails?PayDeductionId=' + payDeductionId,
        success: function (data) {
            Installments = data.list;
            ModifiedInstallments = data.list;
        },
        error: function (data) { }
    });
}

function CancelNewRow(e) {
    var id = $(e).closest("tr").attr('data-rowId');
    var IsFirstRecord = $(e).closest("tr").hasClass('IsFirstRecord');
    $("#tbl_EditPayDeductionDetails1 tbody tr").each(function (i, item) {
        var EditedId = $(this).attr('data-rowId');
        if (id == EditedId) {
            $.each((Installments), function (k, item1) {
                if (item1.PayDeductionDetailID == id) {
                    if (item1.IsActive) {
                        chkActiveStatus = 'checked';
                    }
                    var chkWaivedStatus = '';
                    if (item.IsWaived) {
                        chkWaivedStatus = 'checked';
                    }
                    if (IsFirstRecord) {
                        $(item).html('<td>' + item1.DeductionDate + '</td>' +
                          '<td>' + item1.Amount + '</td>' +
                          '<td>' + item1.Comments + '</td>' +
                          '<td class="col-md-1">' +
                                '<input disabled ' + chkActiveStatus + ' id="chkActive_' + item.PayDeductionDetailID + '" name="chkActive_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsActive + '">' +
                          '</td>' +
                          '<td class="col-md-1">' +
                                '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item.PayDeductionDetailID + '" name="chkWaived_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsWaived + '">' +
                          '</td>' +
                          '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" data-id="' + item1.PayDeductionDetailID + '" onclick="EditDeductionDetail(this)" title="Edit"><i class="fa fa-pencil"></i> </a></td>');
                    }
                    else {
                        $(item).html('<td>' + item1.DeductionDate + '</td>' +
                          '<td>' + item1.Amount + '</td>' +
                          '<td>' + item1.Comments + '</td>' +
                          '<td class="col-md-1">' +
                                '<input disabled ' + chkActiveStatus + ' id="chkActive_' + item.PayDeductionDetailID + '" name="chkActive_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsActive + '">' +
                          '</td>' +
                          '<td class="col-md-1">' +
                                '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item.PayDeductionDetailID + '" name="chkWaived_' + item.PayDeductionDetailID + '" type="checkbox" value="' + item.IsWaived + '">' +
                          '</td>' +
                          '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" data-id="' + item1.PayDeductionDetailID + '" onclick="EditDeductionDetail(this)" title="Edit"><i class="fa fa-pencil"></i> </a>' +
                          '<a class="btn btn-danger btn-rounded btn-condensed btn-sm custom-margin" onclick="DeleteDeductionDetail(this)" title="Delete"><i class="fa fa-times"></i> </a></td>');
                    }

                }
            });
        }
    });
}

function saveRow(e) {
    var DedId = $(e).closest("tr").attr('data-rowId');
    var IsFirstRecord = $(e).closest("tr").hasClass('IsFirstRecord');
    for (var i = 0; i < ModifiedInstallments.length; i++) {
        if (ModifiedInstallments[i].PayDeductionDetailID == parseFloat(DedId)) {
            ModifiedInstallments[i].DeductionDate = $("#InstallmentDate" + DedId).val();
            ModifiedInstallments[i].Amount = $("#txtAmount" + DedId).val();
            ModifiedInstallments[i].Comments = $("#txtComments" + DedId).val();
            ModifiedInstallments[i].IsActive = $("#chkActive_" + DedId).is(":checked");
            ModifiedInstallments[i].IsWaived = $("#chkWaived_" + DedId).is(":checked");

            $("#tbl_EditPayDeductionDetails1 tbody tr").each(function (i, item) {
                var EditedId = $(this).attr('data-rowId');
                if (DedId == EditedId) {
                    var chkActiveStatus = '';
                    $.each((ModifiedInstallments), function (k, item1) {
                        if (item1.PayDeductionDetailID == DedId) {
                            if (item1.IsActive) {
                                chkActiveStatus = 'checked';
                            }
                            var chkWaivedStatus = '';
                            if (item.IsWaived) {
                                chkWaivedStatus = 'checked';
                            }
                            if (IsFirstRecord) {
                                $(item).html('<td>' + item1.DeductionDate + '</td>' +
                                 '<td>' + item1.Amount + '</td>' +
                                 '<td>' + item1.Comments + '</td>' +
                                 '<td class="col-md-1">' +
                                 '<input disabled ' + chkActiveStatus + ' id="chkActive_' + item1.PayDeductionDetailID + '" name="chkActive_' + item1.PayDeductionDetailID + '" type="checkbox" value="' + item1.IsActive + '">' +
                                 '</td>' +
                                 '<td class="col-md-1">' +
                                 '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item1.PayDeductionDetailID + '" name="chkWaived_' + item1.PayDeductionDetailID + '" type="checkbox" value="' + item1.IsWaived + '">' +
                                 '</td>' +
                                 '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditDeductionDetail(this)" data-id=' + item1.PayDeductionDetailID + ' title="Edit"><i class="fa fa-pencil"></i> </a></td>');
                            }
                            else {
                                $(item).html('<td>' + item1.DeductionDate + '</td>' +
                                  '<td>' + item1.Amount + '</td>' +
                                  '<td>' + item1.Comments + '</td>' +
                                  '<td class="col-md-1">' +
                                  '<input disabled ' + chkActiveStatus + ' id="chkActive_' + item1.PayDeductionDetailID + '" name="chkActive_' + item1.PayDeductionDetailID + '" type="checkbox" value="' + item1.IsActive + '">' +
                                  '</td>' +
                                 '<td class="col-md-1">' +
                                 '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + item1.PayDeductionDetailID + '" name="chkWaived_' + item1.PayDeductionDetailID + '" type="checkbox" value="' + item1.IsWaived + '">' +
                                 '</td>' +
                                  '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditDeductionDetail(this)" data-id=' + item1.PayDeductionDetailID + ' title="Edit"><i class="fa fa-pencil"></i> </a><a class="btn btn-danger btn-rounded btn-condensed btn-sm custom-margin" onclick="DeleteDeductionDetail(this)" title="Delete"><i class="fa fa-times"></i> </a></td>');
                            }
                        }
                    });
                }
            });

            break;
        }
    }
    var TotalSum = 0;
    $.each((ModifiedInstallments), function (k, item) {
        if (item.IsActive && !item.IsDeleted) {
            TotalSum = TotalSum + parseFloat(item.Amount);
        }
    });
    $("#TotalInstDedAmount").text(TotalSum.toFixed(NoOfDecimalPlaces));

}

function RefreshGrid() {
    var payDeductionId = $("#hdnPayDeductionID").val();
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/Deduction/GetDeductionInstallmentDetails?PayDeductionId=' + payDeductionId,
        success: function (data) {
            Installments = data.list;
            ModifiedInstallments = data.list;
            $("#tbl_EditPayDeductionDetails1 tbody").html("");
            for (var i = 0; i < Installments.length; i++) {
                var chkActiveStatus = '';
                if (Installments[i].IsActive) {
                    chkActiveStatus = 'checked';
                }
                var chkWaivedStatus = '';
                if (Installments[i].IsWaived) {
                    chkWaivedStatus = 'checked';
                }
                if (Installments[i].IsProcessed) {

                    $("#tbl_EditPayDeductionDetails1 tbody").append('<tr id="rowId_"' + Installments[i].PayDeductionDetailID + ' data-rowId=' + Installments[i].PayDeductionDetailID + '><td>' + Installments[i].DeductionDate + '</td>' +
                          '<td>' + Installments[i].Amount + '</td>' +
                           '<td>' + Installments[i].Comments + '</td>' +
                           '<td class="col-md-1">' +
                                   '<input disabled ' + chkActiveStatus + ' id="chkActive_' + Installments[i].PayDeductionDetailID + '" name="chkActive_' + Installments[i].PayDeductionDetailID + '" type="checkbox" value="' + Installments[i].IsActive + '">' +
                                   '</td>' +
                           '<td class="col-md-1">' +
                                   '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + Installments[i].PayDeductionDetailID + '" name="chkWaived_' + Installments[i].PayDeductionDetailID + '" type="checkbox" value="' + Installments[i].IsWaived + '">' +
                                   '</td>' +
                           '<td><a disabled class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditDeductionDetail(this)" data-id=' + Installments[i].PayDeductionDetailID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
                           '</td></tr>');
                }
                else {
                    if (i == 0) {
                        $("#tbl_EditPayDeductionDetails1 tbody").append('<tr id="rowId_"' + Installments[i].PayDeductionDetailID + ' data-rowId=' + Installments[i].PayDeductionDetailID + '><td>' + Installments[i].DeductionDate + '</td>' +
                                    '<td>' + Installments[i].Amount + '</td>' +
                                   '<td>' + Installments[i].Comments + '</td>' +
                                   '<td class="col-md-1">' +
                                       '<input disabled ' + chkActiveStatus + ' id="chkActive_' + Installments[i].PayDeductionDetailID + '" name="chkActive_' + Installments[i].PayDeductionDetailID + '" type="checkbox" value="' + Installments[i].IsActive + '">' +
                                       '</td>' +
                                    '<td class="col-md-1">' +
                                            '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + Installments[i].PayDeductionDetailID + '" name="chkWaived_' + Installments[i].PayDeductionDetailID + '" type="checkbox" value="' + Installments[i].IsWaived + '">' +
                                    '</td>' +
                                    '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditDeductionDetail(this)" data-id=' + Installments[i].PayDeductionDetailID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
                                    '</td></tr>')
                    }
                    else {
                        $("#tbl_EditPayDeductionDetails1 tbody").append('<tr id="rowId_"' + Installments[i].PayDeductionDetailID + ' data-rowId=' + Installments[i].PayDeductionDetailID + '><td>' + Installments[i].DeductionDate + '</td>' +
                                    '<td>' + Installments[i].Amount + '</td>' +
                                   '<td>' + Installments[i].Comments + '</td>' +
                                   '<td class="col-md-1">' +
                                       '<input disabled ' + chkActiveStatus + ' id="chkActive_' + Installments[i].PayDeductionDetailID + '" name="chkActive_' + Installments[i].PayDeductionDetailID + '" type="checkbox" value="' + Installments[i].IsActive + '">' +
                                       '</td>' +
                                   '<td class="col-md-1">' +
                                        '<input ' + chkWaivedStatus + ' onclick = "UpdateWaiveStatus(this)" id="chkWaived_' + Installments[i].PayDeductionDetailID + '" name="chkWaived_' + Installments[i].PayDeductionDetailID + '" type="checkbox" value="' + Installments[i].IsWaived + '">' +
                                   '</td>' +
                                    '<td><a class="btn btn-success btn-rounded btn-condensed btn-sm" onclick="EditDeductionDetail(this)" data-id=' + Installments[i].PayDeductionDetailID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
                                    '<a class="btn btn-danger btn-rounded btn-condensed btn-sm custom-margin" onclick="DeleteDeductionDetail(this)" title="Delete"><i class="fa fa-times"></i> </a></td></tr>')
                    }
                }
            }
            $("#TotalInsAmount").text(data.TotalAmount);
            $("#TotalInstDedAmount").text((parseFloat(data.TotalAmount)).toFixed(NoOfDecimalPlaces));
        },
        error: function (data) { }
    });
}

function SaveInstallments() {    
    var InstAmt = $("#TotalInsAmount").text();
    var TotalDedAmount = $("#TotalInstDedAmount").text();
    var payDeductionId = $("#hdnPayDeductionID").val();
    var arrWaived = [];
    var IsWaivedRecord = false;
    $.each((ModifiedInstallments), function (k, item) {
        if (item.IsActive && !item.IsDeleted && item.IsWaived) {
            arrWaived.push(item.PayDeductionDetailID.toString());
        }
    });

    if (arrWaived.length > 0) {
        var DeductDetailIds = arrWaived.join();
        $.ajax({
            dataType: "json",
            type: 'POST',
            data: { DeductDetailIds: DeductDetailIds, PayDeductionID: payDeductionId },
            url: '/Deduction/CheckWaivedInstallments',
            async: false,
            success: function (data) {
                if (data > 0) {
                    IsWaivedRecord = true;
                    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "This will update the total deduction amount and the number of installments. Do you want to proceed?." }).done(function () {
                        $.ajax({
                            dataType: 'json',
                            type: 'POST',
                            data: { InstallmentDetails: JSON.stringify(ModifiedInstallments), PayDeductionID: payDeductionId, IsWaivedRecord: IsWaivedRecord },
                            url: '/Deduction/SaveMultipleInstallments',
                            async: false,
                            success: function (data) {
                                $("#myModal3").modal("hide");
                                if (data.CssClass == "Request") {
                                    customShowMessage("information", data.Message, 40000, "center");
                                } else {
                                    ShowMessage("success", data.Message);
                                }
                                getGrid($("#Empid").val());
                            },
                            error: function (data) { }
                        });
                    });

                }
            },
            error: function (data) { }
        });
    }
    if (!IsWaivedRecord) {
        if (TotalDedAmount == parseFloat(InstAmt)) {
            //ShowMessage("success", "Old validation is working proper.");
            $.ajax({
                dataType: 'json',
                type: 'POST',
                data: { InstallmentDetails: JSON.stringify(ModifiedInstallments), PayDeductionID: payDeductionId, IsWaivedRecord: IsWaivedRecord },
                url: '/Deduction/SaveMultipleInstallments',
                success: function (data) {
                    $("#myModal3").modal("hide");
                    if (data.CssClass == "Request") {
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                    getGrid($("#Empid").val());
                },
                error: function (data) { }
            });
        }
        else {
            ShowMessage("error", "Total Deduction Amount and Total Instalment Amount not matching, Please fix installment amounts before saving!.");
        }

    }
    $(".close").removeClass('hidden');
}

function UpdateWaiveStatus(e) {    
    var DedId = parseInt(e.id.split('_')[1]);
    for (var i = 0; i < ModifiedInstallments.length; i++) {
        if (ModifiedInstallments[i].PayDeductionDetailID == DedId) {
            ModifiedInstallments[i].IsWaived = e.checked;
        }
    }
}

function DeleteDeductionDetail(e) {
    var DedId = $(e).closest("tr").attr('data-rowId');
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Please adjust installment amount with other installments after delete, Do you want to proceed?." }).done(function () {
        $(e).closest('tr').remove();
        for (var i = 0; i < ModifiedInstallments.length; i++) {
            if (ModifiedInstallments[i].PayDeductionDetailID == DedId) {
                ModifiedInstallments[i].IsDeleted = true;
                if (ModifiedInstallments[i].IsActive) {
                    var LastUpdatedAmt = $("#TotalInstDedAmount").text();
                    $("#TotalInstDedAmount").text((parseFloat(LastUpdatedAmt) - ModifiedInstallments[i].Amount).toFixed(NoOfDecimalPlaces));
                    break;
                }
            }
        }
        var TotalDedAmount = $("#TotalInstDedAmount").text();
        //EditAll($("#hdnPayDeductionID").val());
    });


}

$("#tblExportToPdf").click(function () {
    if ($("#exportTbl").hasClass("hidden")) {
        $("#exportTbl").removeClass("hidden");
    }
    else {
        $("#exportTbl").addClass("hidden");
    }
});


function CloseModal() {
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/Deduction/GetDeductionInstallmentDetails?PayDeductionId=' + $("#hdnPayDeductionID").val(),
        success: function (data) {
            Installments = data.list;
            var IsMatched = false;
            for (var i = 0; i < Installments.length; i++) {
                if (Installments[i].Amount == ModifiedInstallments[i].Amount && Installments[i].IsActive == ModifiedInstallments[i].IsActive && Installments[i].Comments == ModifiedInstallments[i].Comments && Installments[i].DeductionDate == ModifiedInstallments[i].DeductionDate) {
                    IsMatched = true;
                }
                else {
                    IsMatched = false;
                    break;
                }
            }
            if (!IsMatched) {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "You will lost changes while cancel, Do you want to proceed?." }).done(function () {
                    $("#myModal3").modal("hide");
                });
            }
            else {
                $("#myModal3").modal("hide");
            }
        },
        error: function (data) { }
    });
    //$(".close").removeClass('hidden');
}

