﻿

function CalculateDays()
{
    var EmployeeId = $("#EmployeeID").val();
    var days = $("#Days").val();
    var EffectiveDate=$("#EffectiveDate").val();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { "Id": EmployeeId, "days": days, "effectiveDate": EffectiveDate },
            url: '/Deduction/CalculateDays',
            success: function (data) {
                if (data.result == "success") {
                    $("#Amount").val(data.Amount);
                }
                else {
                    ShowMessage(data.result, data.resultMessage);
                    $("#Amount").val(data.Amount);
                }
            },
            error: function (data) { }
        });
}


function BindDeductionType() {
   
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: {},
        url: '/Deduction/GetAllDeductionTypes',
        success: function (data) {

            var listB = $('#DeductionTypeID');
            listB.empty();
            listB.append(
                    $('<option>', {
                        value: 0,
                        text: 'Select Deduction Type'
                    }, '<option/>'))
            $.each(data, function (index, item) {
                listB.append(
                    $('<option>', {
                        value: item.DeductionTypeID,
                        text: item.DeductionTypeName_1
                    }, '<option/>'))
            });
        },
        error: function (data) { }
    });
}