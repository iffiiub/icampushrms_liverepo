﻿var actionRow;

var deductionList = [];

$(document).on("click", ".dropdown-toggle", function () {
    $(this).parent().addClass("open");
});

var oTableChannel;
function getGrid(employeeId) {

    oTableChannel = $('#tbl_PayDeduction').dataTable({
        "sAjaxSource": "/Deduction/GetPayDeductionList",

        "aoColumns": [
            { "mData": "EffectiveDate", "sTitle": "Date", "width": "12%", "sClass": "left-text-align", "sType": "date-uk" },
            { "mData": "RefNumber", "sTitle": "Ref.", "width": "10%", "sClass": "left-text-align" },
            { "mData": "DeductionType", "sTitle": "DeductionType", "width": "15%", "sClass": "left-text-align" },
            { "mData": "Amount", "sTitle": "Total Amount", "sClass": "right-text-align", "width": "13%", "sClass": "left-text-align" },
            { "mData": "PaidCycle", "sTitle": "Paid Cycle", "width": "10%", "sClass": "left-text-align" },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "40%", "sClass": "left-text-align" },
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Deduction/GetPayDeductionList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "paging": true,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "aaSorting": [[0, 'desc']],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": employeeId });
        },
        "fnDrawCallback": function () {
        }
    });
}

function BindEmployees() {
    $("#ULEmployeeNotInDepartment").load("/Deduction/GetAllEmployees")
}

function getPayDuduction(employeeId) {
    if (parseInt(employeeId) > 0) {
        $(".list-group-item").removeClass("active");
        $('#' + employeeId).addClass("active");
        getGrid(employeeId);
    }
}

$("#SearchEmployee").blur(function () {

    var val = $('#SearchEmployee').val();
    if (val != "") {
        $.ajax({
            // The url to which the request is to be sent.
            url: "/Deduction/EmployeeSearch",

            // The type of request. It can be "GET" or "POST" or others.
            type: "GET",

            // The data that is to be sent to the server. It can be omitted if no data is
            // to be sent. In a "GET" request this data is appended to the url.
            data: { "search": val },

            // The function to be called when the server returns some data. The data
            // variable holds the data that was returned.
            success: function (data) {
                var text = '';
                $(".list-group-item").remove();
                $.each(data, function (index, itemData) {
                    var Name = itemData.FirstName_1 + ' ' + itemData.SurName_1;
                    text += "<a href='javascript:void(0)' id=" + itemData.EmployeeID + " onclick='getPayDuduction(" + itemData.EmployeeID + ")' class='list-group-item'>" + Name + "</a>";
                });

                $('#AllEmployees').append(text);

            },

            // Show an alert if an error occurs.
            error: function () {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: "Some error occured.",
                });
            }
        });
    }

});

function ViewDeductionDetails(PayDeductionId) {
    InstallmentPayDeductionId = PayDeductionId;
    $("#modal_Loader3").load("/Deduction/ViewDeductionDetails?DeductionID=" + PayDeductionId, function () {
        $("#myModal3").modal("show");
        $(".modal-dialog").attr("style", "width:750px;");
        $("#modal_heading3").text('View Deduction Detail');
        $(".close").removeClass('hidden');
    });
}

function EditDeductionDetails(PayDeductionId) {
    $("#modal_Loader3").load("/Deduction/GetDeductionInstallments?PayDeductionId=" + PayDeductionId, function () {
        $("#myModal3").modal("show");
        $(".modal-dialog").attr("style", "width:750px;");
        $("#modal_heading3").text('View Installment Details');
    });
}

//function ShowAmountleftMessage(amountLeft) {
//    if (parseFloat(amountLeft) < 0)
//        customShowMessage("warning", "Installment amount is more than total amount by " + Math.abs(amountLeft), 10000, "center");
//    if (parseFloat(amountLeft) > 0)
//        customShowMessage("warning", "Installment amount is less than total amount by " + Math.abs(amountLeft), 10000, "center");

//}
var oTableDeductionDetails;
var InstallmentPayDeductionId = 0;

function DeleteDeduction(PayDeductionId) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete Pay Deduction?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { "Id": PayDeductionId },
            url: '/Deduction/DeletePayDeduction',
            success: function (data) {
                if (data.Success) {
                    if (data.CssClass == "Request") {
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                } else {
                    ShowMessage("error", data.Message);
                }
                getGrid($("#Empid").val());
            },
            error: function (data) { }
        });
    });
}

var oTableDeductionDetails;
function getDeductionDetails(PayDeductionId) {
    //<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>
    oTableDeductionDetails = $('#tbl_PayDeductionDetails').dataTable({
        "dom": 'rt',
        "sAjaxSource": "/Deduction/GetViewDeductionDetailsList",
        "aoColumns": [
            { "mData": "PayDeductionDetailID", "bVisible": false, "sTitle": "PayDeductionDetailID" },
            { "mData": "PayDeductionID", "bVisible": false, "sTitle": "PayDeductionID" },
            { "mData": "DeductionDate", "sTitle": "Deduction Date" },
            { "mData": "Amount", "sTitle": "Amount", "sClass": "right-text-align" },
            { "mData": "Comments", "sTitle": "Comments" },
            { "mData": "IsActive", "sTitle": "Active", "bSortable": false },
            { "mData": "IsWaived", "sTitle": "IsWaived", "bSortable": false }
        ],
        "processing": false,
        "serverSide": true,
        "ajax": "/Deduction/GetViewDeductionDetailsList",
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bAutoWidth": false,
        "bSortCellsTop": true,
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "PayDeductionId", "value": PayDeductionId });
        }
    });
}

$("#btn_Deduction").click(function () {

    window.location.href = '/Deduction/DeductionTypes';

});

function ExportToExcel() {
    var Empid = $("#Empid").val();
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Deduction/ExportToExcel?EmployeeId=" + Empid);
    }
    else ShowMessage("warning", "No data for export!");
}

function AddMultipleDeduction() {

    $("#modal_Loader").load("/Deduction/AddMultipleDeduction", function () {
        $("#modal_heading").text('Add Multiple Deduction');
        $('#myModal').modal({ backdrop: 'static' });
        bindSelectpicker('.selectpickerddl');
        bindDatePicker('.datepicker');
        bindPositiveOnly('.positiveOnly');
    });
}

function addDeductionDetails(e) {
    var row = $(e).closest('tr');
    var empDdl = row.find('.ddlEmployee').find('option');
    var employeeId = row.find('.ddlEmployee').val();
    var employeeName = row.find('.ddlEmployee option:selected').text();
    var cycle = row.find('.txtCycle').val();
    var amount = row.find('.txtAmount').val();
    if (checkValidation(employeeId, cycle, amount)) {
        var html = getHtmlRow(employeeId, employeeName, cycle, amount, empDdl, "Save");
        addRowInTable(html);
        row.find('.ddlEmployee').val('');
        row.find('.ddlEmployee').change();
        row.find('.txtCycle').val('0');
        row.find('.txtAmount').val('0');
        row.find('.removeActionrow').show();
        row.hide();
        alterHtml();
        AddRemoveOption(employeeId, employeeName, "Remove");
    }
}

function addRowInTable(html) {

    var noRow = $("#Deductiontbl > tbody tr").length;
    if (noRow == 1)
        $("#Deductiontbl > tbody").prepend(html);
    else
        $("#Deductiontbl > tbody tr:nth-child(" + (noRow - 1) + ")").after(html);
}

function getHtmlRow(employeeId, employeeName, cycle, amount, empDdlOption, mode) {
    var html = "";
    if (mode == "Save") {
        html = "<tr class='data-row' data-Edit='false'><td class='hidden'><span class='lblEmployeeID'>" + employeeId + "</label></td>";
        html = html + "<td class='col-md-3'><span class='lblEmployeeName'>" + employeeName + "</label></td>";
        html = html + "<td class='col-md-3'><span class='lblCycle'>" + cycle + "</label></td>";
        html = html + "<td class='col-md-3'><span class='lblAmount'>" + amount + "</label></td>";
        var noRow = $("#Deductiontbl > tbody tr").length;
        html = html + "<td class='col-md-3 center-text-align'>" +
            "<a class='btn btn-success btn-rounded btn-condensed btn-sm btn-space btnEdit' onclick='editSaveRow(this)'   title='Edit'><i class='fa fa-pencil'></i> </a>" +
            "<a class='btn btn-danger btn-rounded btn-condensed btn-sm btn-space btnRemoveRow' onclick='removeSaveRow(this)'   title='Remove'><i class='fa fa-times'></i> </a>" +
            "<a class='btn btn-info btn-rounded btn-condensed btn-sm btn-space btnAddRow' onclick='addNewRow(this)'  title='Add'><i class='fa fa-plus'></i></a></td><tr>";

        return html;
    }
    if (mode == "Edit") {
        html = "<td class='hidden'><span class='lblEmployeeID'>" + employeeId + "</label></td>";
        html = html + "<td class='col-md-3'><select class='form-control selectpickerddl ddlEmployee' data_live_search='true'>" + empDdlOption + "</select></td>";
        html = html + "<td class='col-md-3'><input class='form-control positiveOnly txtCycle' value='" + cycle + "' /></td>";
        html = html + "<td class='col-md-3'><input class='form-control positiveOnly txtAmount' value='" + amount + "' /></td>";
        var noRow = $("#Deductiontbl > tbody tr").length;
        html = html + "<td class='col-md-3 center-text-align'>" +
            "<a class='btn btn-success btn-rounded btn-condensed btn-sm btn-space btnEdit' onclick='UpdateSaveRow(this)'   title='Edit'><i class='fa fa-check'></i> </a>" +
            //"<a class='btn btn-danger btn-rounded btn-condensed btn-sm btn-space btnRemoveRow' onclick=\"removeDescardRow(this,'"+ employeeId + "','" + employeeName + "','" + cycle + "','" + amount + "')\"   title='Remove'><i class='fa fa-times'></i> </a>";
            "<a class='btn btn-danger btn-rounded btn-condensed btn-sm btn-space btnRemoveRow' onclick='removeSaveRow(this)'   title='Remove'><i class='fa fa-times'></i> </a></td>";
        return html;
    }
    if (mode == "Discard") {
        html = "<td class='hidden'><span class='lblEmployeeID'>" + employeeId + "</label></td>";
        html = html + "<td class='col-md-3'><span class='lblEmployeeName'>" + employeeName + "</label></td>";
        html = html + "<td class='col-md-3'><span class='lblCycle'>" + cycle + "</label></td>";
        html = html + "<td class='col-md-3'><span class='lblAmount'>" + amount + "</label></td>";
        var noRow = $("#Deductiontbl > tbody tr").length;
        html = html + "<td class='col-md-3 center-text-align'>" +
            "<a class='btn btn-success btn-rounded btn-condensed btn-sm btn-space btnEdit' onclick='editSaveRow(this)'   title='Edit'><i class='fa fa-pencil'></i> </a>" +
            "<a class='btn btn-danger btn-rounded btn-condensed btn-sm btn-space btnRemoveRow' onclick='removeSaveRow(this)'   title='Remove'><i class='fa fa-times'></i> </a>" +
            "<a class='btn btn-info btn-rounded btn-condensed btn-sm btn-space btnAddRow' onclick='addNewRow(this)' style='display:none'  title='Add'><i class='fa fa-plus'></i></a></td>";

        return html;
    }
}

function alterHtml() {
    $.each($("#Deductiontbl tbody tr"), function (ind, item) {
        if ($(item).find('td').length == 0)
            $(item).remove();
    });

    var count = ($("#Deductiontbl tbody tr").length - 1);
    $.each($("#Deductiontbl tbody tr"), function (ind, item) {
        if (count == (ind + 1)) {
            $(item).find('.btnAddRow').show();
            $(item).find('.btnRemoveRow').hide();
        } else {
            $(item).find('.btnAddRow').hide();
            $(item).find('.btnRemoveRow').show();
        }
    });
    if ($("#Deductiontbl tbody tr").length == 1) {
        $("#Deductiontbl tbody tr:eq(0)").show();
        $("#Deductiontbl tbody tr:eq(0)").find('.removeActionrow').hide();
    }
}

function editSaveRow(e) {
    var row = $(e).closest('tr');
    var empID = row.find('.lblEmployeeID').text().trim();
    var employeeName = row.find('.lblEmployeeName').text().trim();
    var cycle = row.find('.lblCycle').text().trim();
    var amount = row.find('.lblAmount').text().trim();
    AddRemoveOption(empID, employeeName, "Add");
    var option = $('#ddlEmployee').html();
    row.html(getHtmlRow(empID, employeeName, cycle, amount, option, "Edit"));
    row.find('.ddlEmployee').val(empID);
    row.attr('data-Edit', 'true')
    bindEvent();
}

function addNewRow(e) {
    var row = $(e).closest('body').find('.actionRow');
    row.find('.ddlEmployee').val('');
    row.find('.ddlEmployee').change();
    row.find('.txtCycle').val('1');
    row.find('.txtAmount').val('0');
    $(e).closest('body').find('.actionRow').show();
    $(e).closest('body').find('.btnRemoveRow').show();
    $(e).closest('body').find('.btnAddRow').hide();
}

function removeSaveRow(e) {
    $(e).closest('tr').remove();
    alterHtml();
}

function UpdateSaveRow(e) {
    var row = $(e).closest('tr');
    var empID = row.find('.ddlEmployee').val();
    var employeeName = row.find('.ddlEmployee option:selected').text();
    var cycle = row.find('.txtCycle').val();
    var amount = row.find('.txtAmount').val();
    if (checkValidation(empID, cycle, amount)) {
        row.html(getHtmlRow(empID, employeeName, cycle, amount, '', "Discard"));
        AddRemoveOption(empID, employeeName, "Remove");
        row.attr('data-Edit', 'false');
        alterHtml();
    }
}

function removeDescardRow(e, empID, employeeName, cycle, amount) {
    var row = $(e).closest('tr');
    row.html(getHtmlRow(empID, employeeName, cycle, amount, '', "Discard"));
    row.attr('data-Edit', 'false');
    alterHtml();
}

function hideActionRow(e) {
    $(e).closest('tr').hide();
    alterHtml();
}

function bindEvent() {
    bindSelectpicker('.selectpickerddl');
    bindPositiveOnly('.positiveOnly');
}

function CreateJson() {
    deductionList = [];
    if ($("#Deductiontbl tbody tr").length > 1) {
        $.each($("#Deductiontbl tbody tr"), function (ind, item) {
            if (!$(item).hasClass('actionRow')) {
                if ($(item).attr('data-edit') == "false") {
                    var multipleDeduction = {
                        'employeeId': parseInt($(item).find('.lblEmployeeID').text().trim()),
                        'cycle': $(item).find('.lblCycle').text().trim(),
                        'amount': $(item).find('.lblAmount').text().trim()
                    }
                    deductionList.push(multipleDeduction);
                }
            }
        });
        $('#hdnPayAdditionList').val(JSON.stringify(deductionList));
    } else {
        ShowMessage("error", "Record is empty");
    }

}

function AddRemoveOption(id, name, mode) {
    if (mode == "Remove") {
        $("#ddlEmployee option[value='" + id + "']").remove();
    } else {
        $("#ddlEmployee").append(new Option(name, id))
    }
    RefreshSelect("#ddlEmployee");
}

function onSuccessDeduction(data) {
    if (data.Success) {
        if (data.CssClass == "Request") {
            customShowMessage("information", data.Message, 40000, "center");
        } else {
            ShowMessage("success", data.Message);
        }
    } else {
        ShowMessage("error", data.Message);
    }
    if (data.Success) {
        getGrid($("#Empid").val());
        CloseClosetModel('#frmAddMultipleDeduction');
    }
}

function checkValidation(empID, cycle, amount) {
    if (parseInt(empID) > 0) {
        if (parseInt(cycle) > 0) {
            if (parseInt(amount) > 0) {
                return true;
            } else {
                if (parseInt(amount) == 0)
                    ShowMessage('error', 'Amount should be greater than 0');
                else
                    ShowMessage('error', 'Please enter amount');
            }
        } else {
            ShowMessage('error', 'Please enter cycle');
        }
    } else {
        ShowMessage('error', 'Please select employee');
    }
}


function ExportDeductionDetailToPdf(PayDeductionId) {
    StartPanelLoader("#EmpUnpaidLeaveDiv");
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: '/Deduction/GetInstallmentDataForExport?InstallmentPayDeductionId=' + PayDeductionId,
        success: function (data) {
            StopPanelLoader("#EmpUnpaidLeaveDiv");
            if (data.Success) {               
                window.location.assign("/Deduction/ExportDeductionDetailToPdf");                
            }
            else {
                StopPanelLoader("#EmpUnpaidLeaveDiv");
            }           
        },
        error: function (data) { }
    });    
}

function EditFOSTBalanceDetails(PayDeductionID) {
    console.log(PayDeductionID);
    $("#modal_Loader3").load("/Deduction/FOSTBalanceDetails?PayDeductionId=" + PayDeductionID, function () {
        $("#myModal3").modal("show");
        $("#modal_heading3").text('School Fess Deduction Details');
    });
}
