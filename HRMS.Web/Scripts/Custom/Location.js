﻿$(document).ready(function () {

    //alert("IN");
    getGrid();

    $("#btn_add").click(function () {
        //EditChannel(0);
        window.location.href = '/Location/Edit/0';
    });
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);

});

var oTableChannel;

function getGrid() {

    oTableChannel = $('#tbl_location').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/Location/GetLocationList",
        "aoColumns": [
            
            { "mData": "LocationID", "bVisible": false, "sTitle": "LocationID" },
            { "mData": "CountryName", "sTitle": "Country" },
            { "mData": "CityName", "sTitle": "City" }, 
            { "mData": "StateName", "sTitle": "State" },
            { "mData": "Actions", "sTitle": "Actions" }

        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/Location/GetLocationList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_location");
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    
    window.location.href = '/Location/Edit/' + id;

}

function viewLocation(id, companyid) {

    //alert("in");
    $("#modal_Loader").load("/Location/ViewLocationSummarry?LocationId=" + id + "&CompanyID=" + companyid);
    $("#myModal").modal("show");
    $("#modal_heading").text('Location Summary');

}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Location/DeleteLocation',
            success: function (data) {
                getGrid();
                ShowMessage(data.result, data.resultMessage)
            },
            error: function (data) {
                ShowMessage(data.result, data.resultMessage)
            }
        });
    });
}



function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}



function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}



function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Location/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Location/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

