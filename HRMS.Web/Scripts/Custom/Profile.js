﻿function allCheckedDyas() {

    var value = '';
    if ($('#8').is(':checked')) {
        $("#checkids").val('8;');
        //alert('inside check')
        $('.no_of_days').each(function () {
            if (this.id != "8") {
                value += this.id + ";";
                document.getElementById(this.id).checked = true;
            }
            //$("#" + this.id).attr("checked", true);
        });
    } else {
        $("#checkids").val('');
        //alert('inside uncheck')
        $('.no_of_days').each(function () {
            if (this.id != "8")
                document.getElementById(this.id).checked = false;
        });
    }

    $("#checkids").val(value);
}

function checkedChanged() {
    var checkedid = '';

    document.getElementById('8').checked = false;
    var value = '';
    var idArray = [];
    $('.no_of_days').each(function () {
        if (this.id != "8") {
            if ($("#" + this.id).is(':checked')) {
                value += this.id + ";";
            }
        }

        $("#checkids").val(value);
    });
}

var oTableChannel;

function getGrid() {

    oTableChannel = $('#tbl_profile').dataTable({
        "sAjaxSource": "/Profile/GetProfileList",
        "aoColumns": [
            { "mData": "ProfileName", "sTitle": "Profile Name" },
            { "mData": "SessionTimeout", "sTitle": "Session Timeout" },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/Profile/GetProfileList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_profile");
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var buttonName = id > 0 ? "Update": "Submit";
    $("#modal_Loader").load("/Profile/Edit/" + id, function () {
        $("#myModal").modal("show");
        if (id == 0) {

            $("#modal_heading").text('Add Profile');

        }
        else {
            $("#modal_heading").text('Edit Profile');

        }

        $("#frmProfile").find('input[type="submit"]').val(buttonName);
        checkeitems();
    });
}


function checkeitems() {

    var value = $("#checkids").val();

    if ($("#checkids").val() != "") {
        if (value == "8;") {
            var checkedids = $("#checkids").val().split(';');
            document.getElementById(checkedids[0]).checked = true;
            $('.no_of_days').each(function () {
                document.getElementById(this.id).checked = true;
            });
        }
        else {
            var i;
            var checkedids = $("#checkids").val().split(';');
            for (i = 0; i < checkedids.length - 1; i++) {
                document.getElementById(checkedids[i]).checked = true;
            }
        }

        if (value.length == 14) {
            document.getElementById(8).checked = true;
        }
    }
}



function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Profile/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}


function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Profile/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Profile/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function ValidateForm(evt) {
    
    var flag = false;
    $('#chkTable').find('input[type="checkbox"]:checked').each(function () {
        //this is the current checkbox
        flag = true;
    });
    // if ($("#frmAddProfile").valid()) {

    //}
    if (!flag) {
        $("#frmProfile").valid();
        ShowMessage("error", "Please select atleast one access day field.");
    }
    else {
        $("#frmProfile").submit();
    }
}
function Start() {

}

function Sucess(arg) {
    $("#myModal").modal("hide");
    getGrid();
    ShowMessage(arg.responseJSON.result, arg.responseJSON.resultMessage);
}

