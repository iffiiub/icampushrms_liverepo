﻿$(document).ready(function () {
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));
    getGrid();
    $("#btn_add").click(function () {
        EditChannel(0);
    });
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);

});
$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});
var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_company').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/PayCycle/GetPayCycleList",
        "aoColumns": [
           //  { "mData": "PayCycleID", "bVisible": false, "sTitle": "PayCycleID" },
           { "mData": "PayCycleName_1", "sTitle": "Pay Cycle Name (En)", "width": "20%" },
           { "mData": "PayCycleName_2", "sTitle": "Pay Cycle Name (Ar)", "width": "20%" },
           // { "mData": "PayCycleName_3", "bVisible": false, "sTitle": "Payroll Cycle Name (FR)" },
           { "mData": "DateFrom", "sTitle": "Date From", "sType": "date-uk", "width": "12%" },
           { "mData": "DateTo", "sTitle": "Date To", "sType": "date-uk", "width": "12%" },
           { "mData": "acyear", "sTitle": "Ac. Year", "width": "12%" },
           { "mData": "Active", "sTitle": "Active", "width": "12%", "sClass": "text-center" },
           { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "width": "12%" }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PayCycle/GetPayCycleList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var buttonName = id > 0 ? "Update" : "Submit";
    $("#modal_Loader").load("/PayCycle/Edit/" + id, function () {

        bindSelectpicker('.selectpickerddl');
        if (id == 0) {
            $("#modal_heading").text('Add Pay Cycle');
        }
        if (id != 0 && id != null) {
            $("#modal_heading").text('Edit Pay Cycle');
        }
        $("#myModal").modal("show");
        //$("#form0").find('input[type="submit"]').val(buttonName);
    });

}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/PayCycle/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}



//}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/PayCycle/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/PayCycle/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function ValidateCycle(e, CycleId) {
    if (CycleId != 0) {
        if ($(e).is(":checked")) {
            $.ajax({
                type: 'POST',
                data: { CycleId: CycleId },
                url: '/PayCycle/ValidateCycleBeforeActivate',
                success: function (data) {
                    if (data.result == 'success')
                    {
                        $("#AlertMessage").text(" " + data.resultMessage);
                        $("#warningMessage").removeClass("hidden");
                        $(e).attr('checked', false);
                    }
                },
                error: function (data) { }
            });

        }
        else {
            $("#warningMessage").addClass("hidden");
        }
    }

}