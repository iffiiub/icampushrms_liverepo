﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridjobApproval();
    }
});

//$("#btn_addMunicipality").click(function () { AddMunicipality(); });
var oTableChannel;

function getGridjobApproval() {


    oTableChannel = $('#tbl_JobApprovalList').dataTable({
        "sAjaxSource": "/JobApproval/GetJobApprovalList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document Name/No", 'width': '20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", 'width': '20%' }

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/JobApproval/GetJobApprovalList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_JobApprovalList");
        }
    });
}

function AddJobApproval() {
    //alert($("#hdnEmployee_ID").val());
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/JobApproval/AddJobApproval");
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add Job Approval');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditJobApproval(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/JobApproval/EditJobApproval/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit Job Approval');

}


function DeleteJobApproval(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/JobApproval/DeleteJobApproval',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridjobApproval();
            },
            error: function (data) {
                ShowMessage("error", data.Message);
            }
        });
    });
}



function ViewJobApproval(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/JobApproval/ViewJobApproval/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View Job Approval Details');
}

