﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();
    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        getGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            getGrid();
        }
    });
});

var oTableChannel;

function getGrid() {
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();

    oTableChannel = $('#tblLeaveRequestStatusReportGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/LeaveRequestStatusReportGrid",
        "fnServerParams": function (aoData) {
            aoData.push(
                        { "name": "RequestID", "value": RequestID },
                        { "name": "CompanyID", "value": CompanyID },
                        { "name": "DepartmentID", "value": DepartmentID },
                        { "name": "CurrentStatusID", "value": CurrentStatusID },
                        { "name": "FromDate", "value": FromDate },
                        { "name": "ToDate", "value": ToDate }
                        );
        },
        "aoColumns": [
                         { "mData": "BU_Name", "sTitle": "BU Name", 'width': '5%' },
                         { "mData": "RequestID", "sTitle": "Request ID",'width': '5%' },
                         { "mData": "RequestDate", "sTitle": "Request Date",'width': '5%' },
                         { "mData": "EmployeeID", "sTitle": "Employee ID", 'width': '5%' },
                         { "mData": "EmployeeName", "sTitle": "Employee Name", 'width': '10%' },
                         { "mData": "Category", "sTitle": "Category", 'width': '5%' },
                         { "mData": "Department", "sTitle": "Department", 'width': '5%' },
                         { "mData": "Designation", "sTitle": "Designation", 'width': '5%' },
                         { "mData": "LeaveType", "sTitle": "Leave Type", 'width': '5%' },
                         { "mData": "RequestedDays", "sTitle": "Requested Days", 'width': '5%' },
                         { "mData": "BeforeAvailableLeaveDays", "sTitle": "Balance Before Leave Application", 'width': '5%' },
                         { "mData": "AfterAvailableLeaveDays", "sTitle": "Balance After Leave Application", 'width': '5%' },                       
                         { "mData": "PublicHolidays", "sTitle": "Public Holidays", 'width': '5%' },
                         { "mData": "LeaveAdjusted", "sTitle": "Leave Adjusted", 'width': '5%' },
                         { "mData": "TotalDays", "sTitle": "Total Days", 'width': '5%' },
                         { "mData": "CurrentStatus", "sTitle": "Current Status", 'width': '5%' },
                         { "mData": "CurrentApprover", "sTitle": "Current Approver", 'width': '5%' }
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}


function ExportToExcel() {
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    var RecordCount = $("#tblLeaveRequestStatusReportGrid").DataTable().rows("tr").data().length;

    if (RecordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelLeaveRequestStatusReport?RequestID=" + RequestID + "&CompanyID=" + CompanyID +
            "&DepartmentID=" + DepartmentID + "&CurrentStatusID=" + CurrentStatusID + "&FromDate=" + FromDate + "&ToDate=" + ToDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}
