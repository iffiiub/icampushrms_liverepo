﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();

    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        loadTeachingAppraisalStatusReportGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            loadTeachingAppraisalStatusReportGrid();
        }
    });

    $(document).on('change', '#ddlOrganization', function () {
        selectList = "";
        var count = 0;
        selectList = "<option value=\"\">All Status</option>";
        var companyId = $(this).val() == "" ? 0 : $(this).val();
        var formId = 34;
        $.ajax({
            type: 'get',
            url: '/Forms/Reporting/GetFormsWorkflowBasedOnCompany',
            data: { formId: formId, companyId: companyId },
            success: function (data) {
                $.each(data, function (key, value) {
                    count = count + 1;
                    selectList += '<option value=' + value.GroupID + '>' + value.GroupName + ' Approval </option>';
                });
                selectList += '<option value=0> Completed </option>';
                $("#ddlStatus").html();
                $("#ddlStatus").html(selectList);
                RefreshSelect("#ddlStatus");
             
            }
        });
    });

});

var oTableChannel;

function loadTeachingAppraisalStatusReportGrid() {
    var companyId = $("#ddlOrganization").val();
    var departmentId = $("#ddlDepartment").val();
    var statusId = $("#ddlStatus").val();
    var fromDate = $("#DateFrom").val();
    var toDate = $("#DateTo").val();


    oTableChannel = $('#tblTeachingAppraisalStatusGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetTeachingAppraisalStatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "companyId", "value": companyId },
                { "name": "departmentId", "value": departmentId },
                { "name": "statusId", "value": statusId },
                { "name": "fromDate", "value": fromDate },
                { "name": "toDate", "value": toDate }
            );
        },
        "aoColumns": [
            { "mData": "OrganizationName", "sTitle": "BU Name", 'sWidth': '10%' },
            { "mData": "Department", "sTitle": "Department", 'sWidth': '5%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '5%' },
            { "mData": "IsLessonObservationCompleted", "sTitle": "Is Lesson Observation Completed", 'sWidth': '5%' },
            { "mData": "DropInsCompleted", "sTitle": "Is 3 drop ins completed & acknowledged", 'sWidth': '10%' },
            { "mData": "IsAppraisalInitiated", "sTitle": "Is Appraisal Initialized", 'sWidth': '5%' },
            { "mData": "RequestId", "sTitle": "Request Number", 'sWidth': '5%' },
            { "mData": "Year", "sTitle": "Year", 'sWidth': '10%' },
            { "mData": "StartDate", "sTitle": "Start Date", 'sWidth': '5%' },
            { "mData": "DueDate", "sTitle": "Due Date", 'sWidth': '5%' },
            { "mData": "AppraisalSignOffDate", "sTitle": "Appraisal Sign Off Date", 'sWidth': '5%' },
            { "mData": "CurrentStatus", "sTitle": "Current Status", 'sWidth': '10%' },
            { "mData": "CurrentApprover", "sTitle": "Current Approver", 'sWidth': '5%' }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function ExportToExcel() {
    var companyId = $("#ddlOrganization").val();
    var departmentId = $("#ddlDepartment").val();
    var statusId = $("#ddlStatus").val();
    var fromDate = $("#DateFrom").val();
    var toDate = $("#DateTo").val();
    var recordCount = $("#tblTeachingAppraisalStatusGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelTeachingAppraisalStatusReport?companyId=" + companyId + "&departmentId=" + departmentId + "&statusId=" + statusId + "&fromDate=" + fromDate + "&toDate=" + toDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}