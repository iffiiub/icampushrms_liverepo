﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();
    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        loadDropInStatusReportGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            loadDropInStatusReportGrid();
        }
    });

    $(document).on('change', '#ddlOrganization', function () {
        selectList = "";
        var count = 0;
        selectList = "<option value=\"\">All Employees</option>";
        var companyId = $(this).val();
        $.ajax({
            type: 'get',
            url: '/Forms/Reporting/LoadEmployeesbasedOnOrganization?companyId=' + companyId,
            datatype: 'Json',
            success: function (data) {
                $.each(data, function (key, value) {
                    count = count + 1;
                    selectList += '<option value=' + value.EmployeeId + '>' + value.FirstName + '</option>';
                });
                $("#ddlEmployee").html();
                $("#ddlEmployee").html(selectList);
                RefreshSelect("#ddlEmployee");
             
            }
        });
    });
});

var oTableChannel;

function loadDropInStatusReportGrid() {
    var dropInYearId = $("#ddlDropInYear").val();
    var companyId = $("#ddlOrganization").val();
    var employeeId = $("#ddlEmployee").val();

    oTableChannel = $('#tblDropInStatusGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetDropInStatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "dropInYearId", "value": dropInYearId },
                { "name": "companyId", "value": companyId },
                { "name": "employeeId", "value": employeeId }              
            );
        },
        "aoColumns": [
            { "mData": "RequestNo", "sTitle": "Request No", 'sWidth': '10%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '10%' },
            { "mData": "Department", "sTitle": "Department", 'sWidth': '10%' },
            { "mData": "VisitDate", "sTitle": "Visit Date", 'sWidth': '10%' },
            { "mData": "ObservationYear", "sTitle": "Observation Year", 'sWidth': '10%' },
            { "mData": "LineManager", "sTitle": "Observer", 'sWidth': '10%' },
            { "mData": "EmployeeAcknowledgedDropIn", "sTitle": "isAcknowledged?", 'sWidth': '10%' },
            { "mData": "Rating", "sTitle": "Rating", 'sWidth': '10%' },
            { "mData": "TotalScore", "sTitle": "Total Score", 'sWidth': '10%' }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function ExportToExcel() {
    var dropInYearId = $("#ddlDropInYear").val();
    var companyId = $("#ddlOrganization").val();
    var employeeId = $("#ddlEmployee").val();
    var recordCount = $("#tblDropInStatusGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelDropInStatusReport?dropInYearId=" + dropInYearId + "&companyId=" + companyId + "&employeeId=" + employeeId);
    }
    else
        ShowMessage("warning", "No data for export!");
}
