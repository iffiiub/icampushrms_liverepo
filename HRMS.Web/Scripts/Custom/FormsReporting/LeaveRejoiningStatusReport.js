﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();
    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        getGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            getGrid();
        }
    });
});

var oTableChannel;

function getGrid() {
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();

    oTableChannel = $('#tblLeaveRejoiningStatusReportGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/LeaveRejoiningStatusReportGrid",
        "fnServerParams": function (aoData) {
            aoData.push(
                        { "name": "RequestID", "value": RequestID },
                        { "name": "CompanyID", "value": CompanyID },
                        { "name": "DepartmentID", "value": DepartmentID },
                        { "name": "CurrentStatusID", "value": CurrentStatusID },
                        { "name": "FromDate", "value": FromDate },
                        { "name": "ToDate", "value": ToDate }
                        );
        },
        "aoColumns": [
                         { "mData": "BU_Name", "sTitle": "BU Name" },
                         { "mData": "RequestID", "sTitle": "Request ID" },
                         { "mData": "RequestDate", "sTitle": "Request Date" },
                         { "mData": "EmployeeID", "sTitle": "Employee ID" },
                         { "mData": "EmployeeName", "sTitle": "Employee Name" },
                         { "mData": "Category", "sTitle": "Category" },
                         { "mData": "Department", "sTitle": "Department" },
                         { "mData": "Designation", "sTitle": "Designation" },
                         { "mData": "LeaveType", "sTitle": "Leave Type" },
                         { "mData": "RequestedDays", "sTitle": "Requested Days" },
                         { "mData": "ReJoiningDate", "sTitle": "Rejoin Date" },
                         { "mData": "LeavesToAdjust", "sTitle": "Leave Days Adjusted" },
                         { "mData": "AfterAvailableLeaveDays", "sTitle": "Balance After Rejoining" },                         
                         { "mData": "CurrentStatus", "sTitle": "Current Status" },
                         { "mData": "CurrentApprover", "sTitle": "Current Approver" }
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}


function ExportToExcel() {
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    var RecordCount = $("#tblLeaveRejoiningStatusReportGrid").DataTable().rows("tr").data().length;

    if (RecordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelLeaveRejoiningStatusReport?RequestID=" + RequestID + "&CompanyID=" + CompanyID +
            "&DepartmentID=" + DepartmentID + "&CurrentStatusID=" + CurrentStatusID + "&FromDate=" + FromDate + "&ToDate=" + ToDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}
