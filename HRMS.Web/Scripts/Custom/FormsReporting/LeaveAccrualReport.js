﻿$(document).ready(function () {

    $('#btnSearch').on('click', function () {
        loadLeaveAccrualReportGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            loadLeaveAccrualReportGrid();
        }
    });

    $(document).on('change', '#ddlOrganization', function () {
        selectList = "";
        var count = 0;
        selectList = "<option value=\"\">All Employees</option>";
        var companyId = $(this).val();
        $.ajax({
            type: 'get',
            url: '/Forms/Reporting/LoadEmployeesbasedOnOrganization?companyId=' + companyId,
            datatype: 'Json',
            success: function (data) {
                $.each(data, function (key, value) {
                    count = count + 1;
                    selectList += '<option value=' + value.EmployeeId + '>' + value.FirstName + '</option>';
                });
                $("#ddlEmployee").html();
                $("#ddlEmployee").html(selectList);
                RefreshSelect("#ddlEmployee");
             
            }
        });
    });
});

var oTableChannel;

function loadLeaveAccrualReportGrid() {
    var companyId = $("#ddlOrganization").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var fromDate = $("#FromDate").val();
    var toDate = $("#ToDate").val();


    oTableChannel = $('#tblLeaveAccrualGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/LeaveAccrualReportGrid",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "CompanyID", "value": companyId },
                { "name": "DepartmentID", "value": departmentId },
                { "name": "EmployeeID", "value": employeeId } ,
                { "name": "FromDate", "value": fromDate },
                { "name": "ToDate", "value": toDate }
            );
        },
        "aoColumns": [
            { "mData": "CompanyName", "sTitle": "Business Unit", 'sWidth': '10%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '10%' },           
            { "mData": "EmailID", "sTitle": "Email ID", 'sWidth': '10%' },
            { "mData": "DepartmentName", "sTitle": "Department", 'sWidth': '10%' },
            { "mData": "DesignationName", "sTitle": "Designation", 'sWidth': '10%' },
            { "mData": "AccrualDate", "sTitle": "Date Of Accrual", 'sWidth': '10%' },
            { "mData": "AccrualDays", "sTitle": "Balance Accrued", 'sWidth': '10%' },
            { "mData": "TotalAccrualDaysBefore", "sTitle": "Total Leave Days Before Accrual", 'sWidth': '10%' },
            { "mData": "TotalAccrualDaysAfter", "sTitle": "Total Leave Days After Accrual", 'sWidth': '10%' },
            { "mData": "ModifiedByName", "sTitle": "Updated By", 'sWidth': '10%' },
            { "mData": "ModifiedOn", "sTitle": "Updated Date", 'sWidth': '10%' },
            { "mData": "BalanceUpdated", "sTitle": "Balance Updated", 'sWidth': '10%' }
        ],

      
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function ExportToExcel() {
    var companyId = $("#ddlOrganization").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var fromDate = $("#FromDate").val();
    var toDate = $("#ToDate").val();
    var recordCount = $("#tblLeaveAccrualGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelLeaveAccrualReport?CompanyID=" + companyId + "&DepartmentID=" + departmentId + "&EmployeeID=" + employeeId + "&FromDate=" + fromDate + "&ToDate=" + toDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}
