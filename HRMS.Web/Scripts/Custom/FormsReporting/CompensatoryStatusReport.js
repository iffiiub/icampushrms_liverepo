﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();
    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        getGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            getGrid();
        }
    }); 
});

var oTableChannel;

function getGrid() {    
    var RequestID=$("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();

    oTableChannel = $('#tblCompensatoryStatusGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/CompensatoryStatusReportGrid",
        "fnServerParams": function (aoData) {
            aoData.push(
                        { "name": "RequestID", "value": RequestID },
                        { "name": "CompanyID", "value": CompanyID },
                        { "name": "DepartmentID", "value": DepartmentID },
                        { "name": "CurrentStatusID", "value": CurrentStatusID },
                        { "name": "FromDate", "value": FromDate },
                        { "name": "ToDate", "value": ToDate }
                        );
        },
        "aoColumns": [
                         { "mData": "BU_Name", "sTitle": "BU Name", 'width': '8%' },
                         { "mData": "RequestID", "sTitle": "Request ID", 'width': '5%' },
                         { "mData": "RequestDate", "sTitle": "Request Date", 'width': '5%' },
                         { "mData": "EmployeeID", "sTitle": "Employee ID", 'width': '8%' },
                         { "mData": "EmployeeName", "sTitle": "Employee Name", 'width': '8%' },
                         { "mData": "DepartmentName", "sTitle": "Department", 'width': '8%' },
                         { "mData": "Designation", "sTitle": "Designation", 'width': '8%' },
                         { "mData": "AttendanceDate", "sTitle": "Attendance Date", 'width': '8%' },
                         { "mData": "CompensatoryHours", "sTitle": "Compensatory Hours", 'width': '3%' },
                         { "mData": "CurrentStatus", "sTitle": "Current Status", 'width': '5%' },
                         { "mData": "CurrentApprover", "sTitle": "Current Approver", 'width': '9%' }                                                                                                                                                                                              
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {          
        }
    });
}



function ExportToExcel() {    
    var RequestID = $("#RequestID").val();
    var CompanyID = $("#ddlCompany").val();
    var DepartmentID = $("#ddlDepartment").val();
    var CurrentStatusID = $("#ddlCurrentStatus").val();
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    var RecordCount = $("#tblCompensatoryStatusGrid").DataTable().rows("tr").data().length;

    if (RecordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelCompensatoryStatus?RequestID=" + RequestID + "&CompanyID=" + CompanyID +
            "&DepartmentID=" + DepartmentID +"&CurrentStatusID=" + CurrentStatusID + "&FromDate=" + FromDate + "&ToDate=" + ToDate);
    }
    else
        ShowMessage("warning", "No data for export!");
}
