﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();

    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        loadCertificateRequestStatusReportGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            loadCertificateRequestStatusReportGrid();
        }
    })
});

var oTableChannel;

function loadCertificateRequestStatusReportGrid() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();

    oTableChannel = $('#tblCertificateRequestStatusGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetCertificateRequestStatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "requestId", "value": requestId },
                { "name": "companyId", "value": companyId },
                { "name": "employeeId", "value": employeeId },
                { "name": "departmentId", "value": departmentId },
                { "name": "requestStatusId", "value": requestStatusId },
                { "name": "dateFrom", "value": dateFrom },
                { "name": "dateTo", "value": dateTo }
            );
        },
        "aoColumns": [
            { "mData": "BUName", "sTitle": "BU Name", 'sWidth': '10%' },
            { "mData": "RequestID", "sTitle": "Request Number", 'sWidth': '5%' },
            { "mData": "RequestDate", "sTitle": "Request Date", 'sWidth': '5%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '5%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "Category", "sTitle": "Category", 'sWidth': '5%' },
            { "mData": "DepartmentName", "sTitle": "Department", 'sWidth': '10%' },
            { "mData": "Project", "sTitle": "Project", 'sWidth': '5%' },
            { "mData": "Designation", "sTitle": "Designation", 'sWidth': '10%' },
            { "mData": "RequestedCertificateType", "sTitle": "Requested Certificate Type", 'sWidth': '10%' },
            { "mData": "CurrentStatus", "sTitle": "Current Status", 'sWidth': '5%' },
            { "mData": "CurrentApprover", "sTitle": "Current Approver", 'sWidth': '10%' }          
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

$('#btnSearch').on('click', function () {
    loadCertificateRequestStatusReportGrid();
});

$("#RequestID").on('keypress', function (e) {
    if (e.which == 13) {
        loadCertificateRequestStatusReportGrid();
    }
});

function ExportToExcel() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();
    var recordCount = $("#tblCertificateRequestStatusGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelCertificateRequestStatus?requestId=" + requestId + "&companyId=" + companyId + "&employeeId=" + employeeId + "&departmentId=" + departmentId + "&requestStatusId=" + requestStatusId + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo);
    }
    else
        ShowMessage("warning", "No data for export!");
}
