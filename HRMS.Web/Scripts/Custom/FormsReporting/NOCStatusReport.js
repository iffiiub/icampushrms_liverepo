﻿$(document).ready(function () {
    $("#divDownloadExcel").hide();

    $('#btnSearch').on('click', function () {
        $("#divDownloadExcel").show();
        loadNOCStatusReportGrid();
    });

    $("#RequestID").on('keypress', function (e) {
        if (e.which == 13) {
            loadNOCStatusReportGrid();
        }
    });
});

var oTableChannel;

function loadNOCStatusReportGrid() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();

    oTableChannel = $('#tblNOCStatusGrid').dataTable({
        "sAjaxSource": "/Forms/Reporting/GetNOCStatusReportData",
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "requestId", "value": requestId },
                { "name": "companyId", "value": companyId },
                { "name": "employeeId", "value": employeeId },
                { "name": "departmentId", "value": departmentId },
                { "name": "requestStatusId", "value": requestStatusId },
                { "name": "dateFrom", "value": dateFrom },
                { "name": "dateTo", "value": dateTo }
            );
        },
        "aoColumns": [
            { "mData": "BUName", "sTitle": "BU Name", 'sWidth': '10%' },
            { "mData": "RequestID", "sTitle": "Request Number", 'sWidth': '5%' },
            { "mData": "RequestDate", "sTitle": "Request Date", 'sWidth': '5%' },
            { "mData": "EmployeeAlternativeID", "sTitle": "Employee ID", 'sWidth': '5%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'sWidth': '10%' },
            { "mData": "DepartmentName", "sTitle": "Department", 'sWidth': '10%' },
            { "mData": "Designation", "sTitle": "Designation", 'sWidth': '10%' },
            { "mData": "Category", "sTitle": "Category", 'sWidth': '5%' },
            { "mData": "NOCLanguage", "sTitle": "NOC Language", 'sWidth': '5%' },
            { "mData": "AddressTo", "sTitle": "Addressed To", 'sWidth': '10%' },
            { "mData": "RequestReason", "sTitle": "Request Reason", 'sWidth': '10%' },
            { "mData": "CurrentStatus", "sTitle": "Current Status", 'sWidth': '5%' },
            { "mData": "CurrentApprover", "sTitle": "Current Approver", 'sWidth': '10%' }
        ],
        "autoWidth": false,
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
        }
    });
}

function ExportToExcel() {
    var requestId = $("#RequestID").val();
    var companyId = $("#ddlCompany").val();
    var employeeId = $("#ddlEmployee").val();
    var departmentId = $("#ddlDepartment").val();
    var requestStatusId = $("#ddlRequestStatus").val();
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();
    var recordCount = $("#tblNOCStatusGrid").DataTable().rows("tr").data().length;

    if (recordCount > 0) {
        window.location.assign("/Forms/Reporting/ExportToExcelNOCStatus?requestId=" + requestId + "&companyId=" + companyId + "&employeeId=" + employeeId + "&departmentId=" + departmentId + "&requestStatusId=" + requestStatusId + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo);
    }
    else
        ShowMessage("warning", "No data for export!");
}
