﻿$(document).ready(function () {
    getGrid();
});
var NoOfDecimalPlaces = 0;
var CheckSubmit = 0;
function getGrid() {
    pageLoaderFrame();
    EmployeeId = $("#hdnEmployeeId").val();
    oTableLateduduction = $('#tblFaamilyOutStandings').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4"><".col-md-4 pageNo"p>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "EmployeeId", "value": EmployeeId }
                )
        },
        "sAjaxSource": "/FamilyOutStandingBalance/GetFamilyOutstanding",
        "aoColumns": [
        { "mData": "EmployeeAlternativeId", "sTitle": "Emp. ID" },
        { "mData": "EmployeeName", "sTitle": "Emp. Name" },
        { "mData": "FamilyId", "sTitle": "Family ID" },
        { "mData": "FamilyName", "sTitle": "Family Name" },
        { "mData": "FatherName", "sTitle": "Father Name" },
        { "mData": "Balance", "sTitle": "Outstanding Balance" },
        { "mData": "DeductedAmount", "sTitle": "Deducted Amount" },
        { "mData": "Action", "sTitle": "Action", "bSortable": false }
        ],
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "autoWidth": false,
        "drawCallback": function (settings) {

        },
        "initComplete": function (settings, json) {
            hideLoaderFrame();
        }
    });
}

function CreateDeduction(id) {

    $("#modal_Loader").load("/FamilyOutstandingBalance/AddFamilyOSTtoDeduction?EmployeeId=" + id, function () {
        $("#myModal").modal("show");
        bindSelectpicker('.selectpickerddl');
        $("#modal_heading").text('Generate Deduction');
        $(".modal-dialog").attr("style", "width:660px;");

        $(".wizard").smartWizard({
            onLeaveStep: function (obj) {
                NoOfDecimalPlaces = parseInt($("#NoOfDecimalPlaces").val());
                var wizard = obj.parents(".wizard");
                var RefNumber = $("#RefNumber").val();
                var PaidCycle = $("#PaidCycle").val();
                var Amount = $("#Amount").val();
                var OldAmount = $("#OldAmount").val();
                var Comments = $("#Comments").val();
                var DeductionTypeID = $("#DeductionTypeID").val();
                var EffectiveDate = $("#EffectiveDate").val();
                var step_num = obj.attr('rel');
                var step_max = obj.parents(".anchor").find("li").length;
                if (step_num == "1") {
                    if (RefNumber == "" || PaidCycle == "" || Comments == "" || Amount == "" || DeductionTypeID == "" || EffectiveDate == "") {
                        customShowMessage("error", "All * fields are required.", 10000, "center");
                        //wizard.find(".stepContainer").removeAttr("style");
                        return false;
                    }
                    else if (parseFloat(Amount) > parseFloat(OldAmount)) {
                        customShowMessage("error", "Amount should be less than ouststanding balance.", 10000, "center");
                        return false;
                    }
                    else {                        
                        ClearFields();
                        var AmountPerMonth = (Amount / PaidCycle).toFixed(NoOfDecimalPlaces);
                        $("#AmountPerMonthId").html(AmountPerMonth);
                        var data = $('#StudDetailId input[type="text"]');
                        AmountPerMonth = parseFloat(AmountPerMonth);
                        var PerStudAmount = (AmountPerMonth / data.length);                       
                        PerStudAmount = PerStudAmount.toFixed(NoOfDecimalPlaces);

                        var differenceAmt = (AmountPerMonth - (PerStudAmount * data.length)).toFixed(NoOfDecimalPlaces);
                        differenceAmt = parseFloat(differenceAmt);

                        var lastStudId;
                        $.each(data, function (key, value) {
                            $('#' + value.id).val(PerStudAmount);
                            lastStudId = value.id;
                        });
                        var newAmtForLastStud = parseFloat($("#" + lastStudId).val()) + differenceAmt;
                        $("#" + lastStudId).val(newAmtForLastStud.toFixed(NoOfDecimalPlaces));

                        PerStudAmount = parseFloat(PerStudAmount);
                        var TotalBal = AmountPerMonth - ((PerStudAmount * data.length) + differenceAmt);

                        $("#TotalBalance").html(TotalBal.toFixed(NoOfDecimalPlaces));
                        $('#step-2 input[type="text"]').on('input', function () {
                            CalculateAllStudentTotal();
                        });
                    }
                }
                return true;
            },
            onShowStep: function (obj) {
                var wizard = obj.parents(".wizard");

                if (wizard.hasClass("show-submit")) {

                    var step_num = obj.attr('rel');
                    var step_max = obj.parents(".anchor").find("li").length;

                    if (step_num == step_max) {
                        $(".actionBar>a.btn.btn-default.NextBtn").css("display", "none");
                        obj.parents(".wizard").find(".actionBar .btn-primary").css("display", "block");
                        if (CheckSubmit == 0) {
                            $(".saveSettlement").on("click", function () {
                                SaveFamilyOSTBDetails();
                            });
                            $("#UnRegStud").on("click", function () {
                                if ($("#UnRegStud>label>i.fa").hasClass("fa-plus-circle")) {
                                    $("#UnRegStud>label>i.fa").removeClass("fa-plus-circle");
                                    $("#UnRegStud>label>i.fa").addClass("fa-minus-circle");
                                    $("#fieldsetId").addClass("fieldset-Section");
                                    $("#fieldsetId").removeClass("fieldsetstd");
                                }
                                else {
                                    $("#UnRegStud>label>i.fa").removeClass("fa-minus-circle");
                                    $("#UnRegStud>label>i.fa").addClass("fa-plus-circle");
                                    $("#fieldsetId").removeClass("fieldset-Section");
                                    $("#fieldsetId").addClass("fieldsetstd");
                                }
                            });
                            CheckSubmit = CheckSubmit + 1;
                        }
                    }
                    if ($(".PreviousBtn").hasClass("disabled")) {
                        obj.parents(".wizard").find(".actionBar .btn-primary").css("display", "none");
                        $(".actionBar>a.btn.btn-default.NextBtn").css("display", "block");
                    }
                }
                return true;
            }
        });
    });
}

function CalculateAllStudentTotal() {
    var TotalOfAllStud = 0.00;
    var AmountPerMonth = $("#AmountPerMonthId").html();
    var data = $('#step-2 input[type="text"]');

    $.each(data, function (key, value) {
        var PerStudBal = parseFloat($('#' + value.id).val());
        if (isNaN(PerStudBal)) {
            PerStudBal = 0;
        }
        TotalOfAllStud = TotalOfAllStud + PerStudBal;       
    });  

    TotalOfAllStud = TotalOfAllStud.toFixed(NoOfDecimalPlaces);
    var TotalBal = AmountPerMonth - parseFloat(TotalOfAllStud);
    TotalBal = TotalBal.toFixed(NoOfDecimalPlaces);  
    $("#TotalBalance").html(TotalBal);
}

function SaveFamilyOSTBDetails() {
    var AmountPerMonth = parseFloat($("#AmountPerMonthId").html());
    var data = $('#step-2 input[type="text"]');
    var StudentDetails = [];
    $.each(data, function (key, value) {
        var PerStudBal = parseFloat($('#' + value.id).val());
        if (isNaN(PerStudBal)) {
            PerStudBal = 0;
        }
        StudentDetails.push({
            FamilyID: $("#FamilyID").val(),
            StudentID: (value.id).replace('StudId_', ''),
            Amount: $('#' + value.id).val()
        });
    });

    var TotalBal = parseFloat($("#TotalBalance").html());
   
    if (TotalBal > 0 || TotalBal < 0) {
        ShowMessage("error", "Sum of student payments doesn’t equal to the monthly payment.");
    }
    else {
        var FamilyOSTBDetails = {
            EmployeeID: $("#EmployeeID").val(),
            PayDeductionID: $("#PayDeductionID").val(),
            RefNumber: $("#RefNumber").val(),
            DeductionTypeID: $("#DeductionTypeID").val(),
            EffectiveDate: $("#EffectiveDate").val(),
            Amount: $("#Amount").val(),
            PaidCycle: $("#PaidCycle").val(),
            Comments: $("#Comments").val(),
            StudentDetails: StudentDetails
        };
        var OldAmount = $("#OldAmount").val();
        var OldPaidCycle = $("#OldPaidCycle").val();
        $.ajax({
            url: "/FamilyOutstandingBalance/AddFamilyOSTtoDeduction/",
            type: "POST",
            data: {
                payDeductionData: JSON.stringify(FamilyOSTBDetails),
                OldAmount: OldAmount,
                OldPaidCycle: OldPaidCycle
            },
            success: function (data) {
                $("#myModal").modal("hide");
                ShowMessage("success", "Deduction generated successfully.");
                getGrid();
            },
            error: function () {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: "Some error occured.",
                });
            }
        });
    }
}

function ClearFields() {
    var data = $('#step-2 input[type="text"]');
    $.each(data, function (key, value) {
        $('#' + value.id).val("0.00");
    });
    $("#collapseOne").removeClass("in");
    $("#UnRegStud>label>i.fa").removeClass("fa-minus-circle");
    $("#UnRegStud>label>i.fa").addClass("fa-plus-circle");
    $("#fieldsetId").removeClass("fieldset-Section");
}