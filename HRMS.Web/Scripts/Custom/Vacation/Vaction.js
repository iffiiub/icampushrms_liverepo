﻿var oEmployeeTable;
var oTableChannel;
var vaction = function () {
    loadVactionGrid = function () {
        var isView = $("#hdnIsViewPermission").val();
        var isUpdate = $("#hdnIsUpdatePermission").val();
        var isDelete = $("#hdnIsDeletePermission").val();
     
        if (isView == "True") {
            var deptId = $('#ddlDepartment').val();
            var sectionId = $('#ddlSectionMain').val();
            var url = "/Vacation/FilterGrid"
            if (parseInt(deptId) > 0) {
                url = url + "?deptId=" + deptId;
            } else {
                url = url + "?deptId=NULL";
            }
            if (parseInt(sectionId) > 0) {
                url = url + "&sectionId=" + sectionId;
            }
            oTableChannel = $('#tblVaction').DataTable({
                "order": [[1, "asc"]],
                "sAjaxSource": url,
                //"fnServerParams": function (aoData) {
                //aoData.push({ "deptId": "includeInActive", "value": includeInActive }, { "name": "inActiveOnly", "value": inactiveOnly }, { "name": "search", "value": searchval });
                //},
                "aoColumns": [
                    { "mData": "EmployeeAlternativeId", "sTitle": "EmpID", "bSortable": false, "sType": "Int" },
                    { "mData": "EmployeeName", "sTitle": "Name", },
                    { "mData": "VacationType", "sTitle": "Leave Type" },
                    { "mData": "FromDate", "sTitle": "Start Date", "sType": "date-uk" },
                    { "mData": "ToDate", "sTitle": "End Date", "sType": "date-uk" },
                    { "mData": "Action", "sTitle": "Action", "bSortable": false },

                ],

                "ajax": url,
                "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
                "iDisplayLength": 10,
                "bDestroy": true,
                "bFilter": true,
                "bInfo": true,
                "bSortCellsTop": true,
                "fnDrawCallback": function (settings) {
                //    alert(isUpdate);
                    if (isUpdate == "False" && isDelete == "False") {
                        $('#tblVaction').DataTable().columns([5]).visible(false);
                     
                    }
                }
            });
        }
       
    },

    loadVaction = function () {
        $("#modal_Loader3").html("");
        $("#modal_Loader3").load("/Vacation/LoadVacation/", function () {
            $("#myModal3").modal("show");
            $(".modal-dialog").attr("style", "width:700px;");
            $("#modal_heading3").text('Add Leave');
            $('#vactionddl').change(function () {
                checkVacationType(this);
            });
            $("#btnSave").click(function () {
                saveVactionData();
            });
            //$('.disabled').attr("disabled", "disabled");
            //$('.disabled[dataEnable="shift"]').removeAttr("disabled");
            bindSelectpicker('.selectpickerddl');
            DisabledSelect('.disableddl', true);
            DisabledSelect('.disableddl[dataEnable="shift"]', false);
            $('.oprationType').click(function () {
                var ddlType = $(this).attr('data-type');
                //$('.disabled').attr("disabled", "disabled");
                DisabledSelect('.disableddl', true);
                //$('.disabled[dataEnable="' + ddlType + '"]').removeAttr("disabled");
                DisabledSelect('.disableddl[dataEnable="' + ddlType + '"]', false);
            });

        });
    },

    editVaction = function (id) {
        $("#modal_Loader3").html("");
        $("#modal_Loader3").load("/Vacation/EditVactionRecord?id=" + id, function () {
            $("#myModal3").modal("show");
            $("#modal_heading3").text('Edit Leave');
            bindDatePicker("#fromDate");
            bindDatePicker("#toDate");
            $('form#frmEditVaction').submit(function () {
                if (parseInt($('form#frmEditVaction #vacationTypeId').val()) > 0) {
                    return true;
                } else {
                    ShowMessage("error", "Please select leave type.");
                    return false;
                }
            })
            bindSelectpicker('.selectpickerddl');

        });
    },

    filterVactionGrid = function () {
        var deptId = $('#ddlDepartment').val();
        var sectionId = $('#ddlSectionMain').val();
        var url = "/Vacation/Index"
        if (parseInt(deptId) > 0) {
            url = url + "?deptId=" + deptId;
        } else {
            url = url + "?deptId=NULL";
        }
        if (parseInt(sectionId) > 0) {
            url = url + "&sectionId=" + sectionId;
        }
        window.location = url;
    },

    checkValidation = function (selector, type, message) {
        if (parseInt($(selector).val()) > 0) {
            if (parseInt($("#vactionddl").val()) > 0) {
                if ($("#fromDate").val() != "" && $("#toDate").val() != "") {
                    var d1 = Date.parse($("#fromDate").datepicker("getDate"));
                    var d2 = Date.parse($("#toDate").datepicker("getDate"));
                    if (d2 >= d1) {
                        return true;
                    }
                    else {
                        ShowMessage("error", "From date should be less than End date.");
                    }
                } else { ShowMessage("error", "Please select leave date."); }
            } else { ShowMessage("error", "Please select leave type."); }

        } else {
            ShowMessage("error", message);
            return false;
        }
    }

    checkVacationType = function (e) {       
        var leavetypeid = $("#vactionddl").val();
        var employeeid = $('#ddlEmployee').val();
        // var url =
        if (employeeid <= 0)
            employeeid = 0;
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/Vacation/GetVacationBalanceDetails',
            data: { leaveTypeID: leavetypeid,  employeeID : employeeid },
            async: false,
            success: function (result) {
                if (result != null) {
                    $('#lblFullpaidDays').text(result.FullPaidDays);
                    $('#lblHalfpaidDays').text(result.HalfPaidDays);
                    $('#lblUnpaidDays').text(result.UnPaidDays);
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
        //$('#lblStatusPaid').text($(e).find('option:selected').attr('data-Paidtype'));
        //$('#lblStatusLeave').text($(e).find('option:selected').attr('data-Leavetype'));
    },

    saveVactionData = function () {
        var optionType = $('.oprationType:checked').attr("data-type");
        switch (optionType) {
            case "shift":
                if (checkValidation("#ddlShift", "", "Please select shift first")) {
                    saveChanges("shift", $('#ddlShift').val(), $('#fromDate').val(), $('#toDate').val(), $('#vactionddl').val());
                }
                break;
            case "department":
                if (checkValidation("#ddlDept", "", "Please select department first")) {
                    saveChanges("department", $('#ddlDept').val(), $('#fromDate').val(), $('#toDate').val(), $('#vactionddl').val());
                }
                break;
            case "section":
                if (checkValidation("#ddlSection", "", "Please select section first")) {
                    saveChanges("section", $('#ddlSection').val(), $('#fromDate').val(), $('#toDate').val(), $('#vactionddl').val());
                }
                break;
            case "employee":
                if (checkValidation("#ddlEmployee", "", "Please select employee first")) {

                    $.ajax({
                        type: 'POST',
                        url: '/Vacation/checkEmployeeHiredate?toDate=',
                        data: { toDate: $('#toDate').val(), empId: $('#ddlEmployee').val() },
                        success: function (data) {
                            isEmpHiredateRulePass = data;
                            if (isEmpHiredateRulePass) {
                                saveChanges("employee", $('#ddlEmployee').val(), $('#fromDate').val(), $('#toDate').val(), $('#vactionddl').val());
                            }
                            else {
                                ShowMessage("error", "Selected employee is not eligible for leave because joining date is after selected leave dates.");
                            }

                        },
                        error: function (data) { }
                    });
                }
                break;
            default:
                ShowMessage("error", "Please select");
                break;
        }
    },

    deleteVaction = function (id) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
            $.ajax({
                type: 'POST',
                url: '/Vacation/DeleteVactionRecord?id=' + id,
                success: function (data) {
                    if (data == true) {
                        ShowMessage("success", "Record has been deleted");
                        loadVactionGrid();
                    } else { ShowMessage("error", "Technical error has occurred"); }
                },
                error: function (data) { }
            });
        });

    },

    saveChanges = function (mode, id, fromDate, toData, typeId) {     
        $("#divEmployeeList").addClass("hidden");
        $.blockUI();
        $.ajax({
            type: 'POST',
            data: { modeType: mode, typeId: id, fromDate: fromDate, toDate: toData, vacationTypeId: typeId },
            url: '/Vacation/CheckVactionRules/',
            success: function (data) {
                $.unblockUI();
                if (data.mode == 4) {
                    if (data.countRulefail > 0) {
                        //textAlignNotyMessage("left");
                        customShowMessage("error", data.message, 30000, "left");
                    } else {
                        if (data.paid == 0) {                          
                            if (data.isSalaryForWorkingDays == 0) {
                                $.MessageBox({
                                    buttonDone: "Yes",
                                    buttonFail: "No",
                                    message: "Selected leave type is not paid so there will be absent deduction will added.",
                                }).done(function () {
                                    addDeduction(mode, id, fromDate, toData, typeId);
                                }).fail(function () {
                                });
                            }
                            else {
                                addDeduction(mode, id, fromDate, toData, typeId);
                            }

                        } else
                            addVacation(mode, id, fromDate, toData, typeId);
                    }
                } else {
                    if (data.countRulefail > 0) {
                        $("#divEmployeeList").removeClass("hidden");
                        var tr = "";
                        $.each(data.employeeList, function (ind, item) {
                            tr = tr + "<tr data-toggle='tooltip' data-placement='top' data-original-title=\"" + item.ruleMessage + "\" ><td class=''>" + item.employeeName + "</td><td class='text-center'>"
                                 + (item.isRulePass ? "<span class=\"fa fa-check fa-2x text-success\"></span>" : "<span class=\"fa fa-times fa-2x text-danger\"></span>") +
                                 "</td></tr>"
                        });
                        if (oEmployeeTable == null) {
                            $("#tblEmployeeList tbody").html(tr);
                            oEmployeeTable = $("#tblEmployeeList").dataTable({
                                "bSort": false
                            });
                        }
                        else {
                            oEmployeeTable.fnDestroy();
                            $("#tblEmployeeList tbody").html(tr);
                            oEmployeeTable = $("#tblEmployeeList").dataTable({
                                "bSort": false
                            });
                        }
                    } else {
                        $("#divEmployeeList").addClass("hidden");
                        if (data.paid == 0) {
                            if (data.isSalaryForWorkingDays == 0) {
                                $.MessageBox({
                                    buttonDone: "Yes",
                                    buttonFail: "No",
                                    message: "Selected leave type is not paid so there will be absent deduction will added.",
                                }).done(function () {
                                    addDeduction(mode, id, fromDate, toData, typeId);
                                }).fail(function () {
                                });
                            }
                            else {
                                addDeduction(mode, id, fromDate, toData, typeId);
                            }
                        } else
                            addVacation(mode, id, fromDate, toData, typeId);
                    }
                }
            },
            error: function (data) { }
        });
    },

    addVacation = function (mode, id, fromDate, toData, typeId) {
        $.ajax({
            type: 'POST',
            data: { mode: mode, id: id, fromDate: fromDate, toDate: toData, typeId: typeId },
            url: '/Vacation/SaveVactionRecord/',
            success: function (data) {
                $.unblockUI();
                if (data.Success == true) {
                    ShowMessage(data.CssClass, data.Message);
                    vaction.loadVactionGrid();
                    $('#myModal3').modal('hide');
                }
                else
                    ShowMessage("error", "Technical error has occured");

            },
            error: function (data) { }
        });
    },

    addDeduction = function (mode, id, fromDate, toData, typeId) {
        $.ajax({
            type: 'POST',
            data: { modeType: mode, typeId: id, fromDate: fromDate, toDate: toData, vacationTypeId: typeId },
            url: '/Vacation/AddDeductionForVacation/',
            success: function (data) {
                if (data.Success)
                    addVacation(mode, id, fromDate, toData, typeId);
                else
                    ShowMessage(data.CssClass, data.Message);
            },
            error: function (data) { }
        })
    },


    addPortalVacation = function (mode, id, fromDate, toData, typeId, requestId) {
        $.ajax({
            type: 'POST',
            data: { mode: mode, id: id, fromDate: fromDate, toDate: toData, typeId: typeId, requestId: requestId },
            url: '/PortalLeaveRequest/SavePortalVactionRecord/',
            success: function (data) {
                $.unblockUI();
                if (data.Success == true) {
                    ShowMessage(data.CssClass, data.Message);
                    GetPortalLeaveRequest();
                    $('#myModal3').modal('hide');
                }
                else
                    ShowMessage("error", "Technical error has occured");

            },
            error: function (data) { }
        });
    },

    addPortalDeduction = function (mode, id, fromDate, toData, typeId, requestId) {
        $.ajax({
            type: 'POST',
            data: { modeType: mode, typeId: id, fromDate: fromDate, toDate: toData, vacationTypeId: typeId },
            url: '/Vacation/AddDeductionForVacation/',
            success: function (data) {
                if (data.Success)
                    addPortalVacation(mode, id, fromDate, toData, typeId, requestId);
                else
                    ShowMessage(data.CssClass, data.Message);
            },
            error: function (data) { }
        })
    },

    intiDataTable = function () {

    };

    return {
        intiDataTable: intiDataTable,
        loadVaction: loadVaction,
        checkVacationType: checkVacationType,
        saveVactionData: saveVactionData,
        filterVactionGrid: filterVactionGrid,
        editVaction: editVaction,
        deleteVaction: deleteVaction,
        loadVactionGrid: loadVactionGrid,
        addVacation: addVacation,
        addDeduction: addDeduction
    };
}();
-
$(document).ready(function () {
    vaction.loadVactionGrid();

    $('#btnVacationPopover').click(function () {
        vaction.loadVaction();
    });
});

$(document).on("click", "#btnSave", function () {

    if ($("#fromDate").val() != "" && $("#toDate").val() != "") {
        var d1 = Date.parse($("#fromDate").datepicker("getDate"));
        var d2 = Date.parse($("#toDate").datepicker("getDate"));
        if (d2 >= d1) {
            $("#frmEditVaction").submit();
        }
        else {
            ShowMessage("error", "From date should be less than End date.");
        }
    } else { ShowMessage("error", "Please select leave date."); }
});

function onFailure(xhr, status, error) {
    console.log("xhr", xhr);
    console.log("status", status);
    $.MessageBox({
        buttonDone: "Ok",
        message: error,
    });
}

function onSuccess(data, status, xhr) {
    $('#myModal3').modal('hide');
    if (data.Success == true) {
        vaction.loadVactionGrid();
        ShowMessage(data.CssClass, data.Message);
    }
    else
        ShowMessage(data.CssClass, data.Message);
}

function GetPortalLeaveRequest() {

    var url = "/PortalLeaveRequest/GetPortalLeaveRequest"
    oPortalReqTable = $('#tblPortalLeaveRequest').dataTable({
        "order": [[1, "asc"]],
        "sAjaxSource": url,
        "aoColumns": [
            { "mData": "EmployeeAlternativeId", "sTitle": "EmpID", "bSortable": false, "sType": "Int" },
            { "mData": "EmployeeName", "sTitle": "Name", },
            { "mData": "VacationType", "sTitle": "Leave Type" },
            { "mData": "FromDate", "sTitle": "Start Date", "sType": "date-uk" },
            { "mData": "ToDate", "sTitle": "End Date", "sType": "date-uk" },
            { "mData": "Reason", "sTitle": "Reason" },
            { "mData": "Comments", "sTitle": "Comments" },
            { "mData": "RejectReason", "sTitle": "Reject Reason" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false },

        ],

        "ajax": url,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
        }
    });
}

function ApproveRequest(requestId) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to approve leave request?" }).done(function () {
        //Logic to delete the item
        $.ajax({
            type: 'POST',
            data: { RequestId: requestId },
            url: '/PortalLeaveRequest/approveRequest',
            success: function (data) {
                var EmpID = data.employeeId;
                var fromDate = data.fromDate;
                var toDate = data.toDate;
                var vacTypeId = data.vacationTypeId;
                $.ajax({
                    type: 'POST',
                    url: '/Vacation/checkEmployeeHiredate?toDate=',
                    data: { toDate: data.toDate, empId: data.employeeId },
                    success: function (data) {
                        isEmpHiredateRulePass = data;
                        if (isEmpHiredateRulePass) {
                            CheckVactionRules("employee", EmpID, fromDate, toDate, vacTypeId, requestId);
                        }
                        else {
                            ShowMessage("error", "Selected employee is not eligible for leave because joining date is after selected leave dates.");
                        }

                    },
                    error: function (data) { }
                });
            },
            error: function (data) { }
        });
    }).fail(function () { });
}

function CheckVactionRules(mode, id, fromDate, toData, typeId, requestId) {   
    pageLoadingFrame("show");
    $.ajax({
        type: 'POST',
        data: { modeType: mode, typeId: id, fromDate: fromDate, toDate: toData, vacationTypeId: typeId },
        url: '/Vacation/CheckVactionRules/',
        success: function (data) {
            pageLoadingFrame("hide");
            if (data.mode == 4) {
                if (data.countRulefail > 0) {
                    //textAlignNotyMessage("left");
                    customShowMessage("error", data.message, 30000, "left");
                    GetPortalLeaveRequest();
                } else {
                    if (data.paid == 0) {
                        if (data.isSalaryForWorkingDays == 0) {
                            $.MessageBox({
                                buttonDone: "Yes",
                                buttonFail: "No",
                                message: "Selected leave type is not paid so there will be absent deduction will added.",
                            }).done(function () {
                                addPortalDeduction(mode, id, fromDate, toData, typeId, requestId);
                            }).fail(function () {
                            });
                        }
                        else {
                            addPortalDeduction(mode, id, fromDate, toData, typeId, requestId);
                        }

                    } else
                        addPortalVacation(mode, id, fromDate, toData, typeId, requestId);
                }
            }
        },
        error: function (data) { }
    });
}

function RejectRequest(requestId) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/PortalLeaveRequest/RejectRequest?RequestId=" + requestId, function () {
        $("#myModal3").modal("show");
        $("#modal_heading3").text('Reject Request');
    });
}

function beginRejectRequest() {
    pageLoadingFrame("show");
}

function onRejectFailure(xhr, status, error) {
    pageLoadingFrame("hide");
    console.log("xhr", xhr);
    console.log("status", status);

    $.MessageBox({
        buttonDone: "Ok",
        message: error,
    });
}

function onRejectSuccess(data, status, xhr) {
    pageLoadingFrame("hide");
    console.log("data", data);
    console.log("status", status);
    console.log("xhr", xhr);
    if (data.Success == true) {
        $("#myModal3").modal("hide");
        ShowMessage(data.CssClass, data.Message);
        GetPortalLeaveRequest();

    }
    else {
        ShowMessage("error", data.Message);
    }

}

