﻿


$(document).on("change", "#departmentList", function () {
    var EmployeeID = $(this).val() == "" ? 0 : $(this).val()
    $("#DepartmentLeaderID").val(EmployeeID);
});


function AddEmployeeToDepartment(empId, e) {
    //var depit = $("#departmentList").val();
    if ($("#Div_Department_Employee > a").length == 1) {
        if ($("#Div_Department_Employee > a").eq("0").attr("data-EmployeeId") == "NoEmployee")
            $("#Div_Department_Employee").html("");
    }
    var depit = $('#DepartmentId').val();
    var displayText = $(e).attr('data-name').trim();
    $(e).closest("a").remove();
    var addHtml = '<a href="#" data-EmployeeId="' + empId + '"  class="list-group-item  emplistcontent list-group-item-condensed">'
                   + '<div class="row">'
                   + '<div class="col-sm-12 padding-0">'
                   + '<div class="col-sm-10 padding-0">'
                   + '<span class="contacts-title contacts-title-emplist employeeName">' + displayText + '</span>'
                   + '</div>'
                   + '<div class="col-sm-2 padding-0">'
                   + '<div class="list-group-controls-emplist">'
                   + '<button type="button" class="btn btn-danger btn-rounded btn-condensed btn-sm pull-right" style="" onclick="RemoveDepartmentEmployee(\'' + empId + '\',\'' + displayText + '\',this)" title="Remove"><i class="fa fa-times"></i></button>'
                   + '</div>'
                   + '</div>'
                   + '</div>'
                   + '</div>'
                   + '</a>';
    $("#Div_Department_Employee").append(addHtml);
    $('#departmentList').append($('<option>', {
        value: empId,
        text:  displayText
    }));
    RefreshSelect("#departmentList");
    calculateCountOfEmployee()
}

function RemoveDepartmentEmployee(employeeid, empName, e) {
    //var depit = $("#departmentList").val();    
    var depit = $("#DepartmentId").val();
    var addHtml = "";
    $(e).closest("a").remove();

    if ($("#Div_Department_Employee > a").length == 0) {
        addHtml = '<a href="#" data-EmployeeId="NoEmployee"  class="list-group-item  emplistcontent list-group-item-condensed">'
                  + '<div class="row">'
                  + '<div class="col-sm-12 padding-0">'
                  + '<div class="col-sm-10 padding-0">'
                  + '<span class="contacts-title contacts-title-emplist employeeName">No employee in the department.</span>'
                  + '</div>'                  
                  + '</div>'
                  + '</div>'
                  + '</a>';
        $("#Div_Department_Employee").append(addHtml);
    }
    addHtml = '<a href="#" class="list-group-item  emplistcontent list-group-item-condensed liemployeenotindepartment">'
                  + '<div class="row">'
                  + '<div class="col-sm-12 padding-0">'
                  + '<div class="col-sm-1 text-right">'
                  + '<div class="list-group-controls-emplist" style="padding-top:4px;">'
                  + '<input type="checkbox" onchange="AddEmployeeToDepartment(\'' + employeeid + '\',this);" data-name="' + empName + '" value="' + employeeid + '" id="Employee_' + employeeid + '">'
                  + '</div>'
                  + '</div>'
                  + '<div class="col-sm-11 padding-0">'
                  + '<span class="contacts-title contacts-title-emplist employeeName">' + empName + '</span>'
                  + '</div>'
                  + '</div>'
                  + '</div>'
                  + '</a>';

    $("#ULEmployeeNotInDepartment").append(addHtml);
    $("#departmentList option[value=" + employeeid + "]").remove();
    RefreshSelect("#departmentList");
    calculateCountOfEmployee()
}


function AddRemoveLeaderFromDropdown(employeeId, type, text) {
    if (type == "Add") {
        $('#departmentList').append($('<option>', {
            value: employeeId,
            text: text
        }));
    }
    else if (type == "Remove") {
        $("#departmentList option[value='" + employeeId + "']").remove();
        //$("#departmentList").val("0");
    }
    RefreshSelect('#departmentList');
}

//function SearchShiftEmployee(id, e) {
//    var val = $.trim($(e).val()).replace(/ +/g, ' ').toLowerCase();
//    var $emp = $("#" + id + " ul > li");
//    $emp.show().filter(function () {
//        var text = $(this, ' label').text().trim().replace(/\s+/g, ' ').toLowerCase();
//        return !~text.indexOf(val);
//    }).hide();
//}

function calculateCountOfEmployee() {
    $("#nonDepartmentEmployeetotal").html("(" + $("#ULEmployeeNotInDepartment > a").length + ")");

    if ($("#Div_Department_Employee > a").length == 1) {
        if ($("#Div_Department_Employee > a").eq("0").attr("data-EmployeeId") == "NoEmployee")
            $("#departmentEmployeetotal").html("(0)");
        else
            $("#departmentEmployeetotal").html("(" + $("#Div_Department_Employee > a").length + ")");
    } else {
        $("#departmentEmployeetotal").html("(" + $("#Div_Department_Employee > a").length + ")");
    }
}







