﻿var oldTrObj;
var tableHtml;
var firstLoad = false;

$(document).ready(function () {
    Search();
    getGrid();
    $(document).on('click', '#btnGetPayroll', function () {
        $("#hdnCategoryId").val($("#CategoryID").val());
    });

    getEmployeeNotinPayroll();

    $("#payrollWarningBox").click(function () {
        var url = $(this).attr("data-Url");
        newTabWindow(url);
    });

    $("#btnpayrollWarningBoxClose").click(function (e) {
        e.stopPropagation();
        $("#payrollWarningBox").remove();
    });

    //if ($("#SelectedCode").val() != "") {
    //    $("#ddl_AccountCode").val($("#SelectedCode").val());
    //}

    $("body").on('click', '.lsb-box', function () {
        $(".lockscreen-box .lsb-form").css({ "display": "block" })
    })

    $("input[name=jvsTemplate]").click(function () {
        var JVgroupingType = $("input[name=jvsTemplate]:checked").attr("data-val");
        if (JVgroupingType != 3) {
            $('[data-toggle=popoverGroupByEmp3]').popover('hide');
        }
    });

    $("[data-toggle=Jvspopoverp3]")
    .popover({
        datahtml: true,
        html: true,
        container: 'body',
        'title': '<div class="">' +
                 'Salary Account' +
                 '<ul class="panel-controls">' +
                 '<li class="dropdown open">' +
                 '<a href="javascript:void(0);" onclick="ClosePopOver(\'Jvspopoverp3\')" title="Close" id="" class="btnClosable btnadd"><span class="fa fa-times fa-1x"></span></a>' +
                 '</li>' +
                 '</ul>' +
                 '</button>' +
            '</div>',
        content: function () {
            var iD = $(this).attr('data-Id');
            var htmlCode = $(".jvAccountCode").html();
            $(htmlCode).find("#ddl_AccountCode").val($("#AccountCodeGroupBy").val());
            return htmlCode;
        }
    }).on('show.bs.popover', function () {
        ClosePopOver("Jvspopoverp4");
        setTimeout(function () {
            $(".popover-content").find("#ddl_AccountCode").val($("#AccountCodeGroupBy").val());
            $(".popover-content").find("#ddl_AccountCode").attr("data-live-search", true);
            $(".popover-content").find("#ddl_AccountCode").addClass('selectpickerddl');
            bindSelectpicker('.selectpickerddl');

        }, 300);
    });

    $("[data-toggle=Jvspopoverp4]")
    .popover({
        datahtml: true,
        html: true,
        container: 'body',
        'title': '<div class="">' +
                 'Salary Account' +
                 '<ul class="panel-controls">' +
                 '<li class="dropdown open">' +
                 '<a href="javascript:void(0);" onclick="ClosePopOver(\'Jvspopoverp4\')" title="Close" id="" class="btnClosable btnadd"><span class="fa fa-times fa-1x"></span></a>' +
                 '</li>' +
                 '</ul>' +
                 '</button>' +
            '</div>',
        content: function () {            
            var iD = $(this).attr('data-Id');
            var htmlCode = $(".jvAccountCode").html();
            $(htmlCode).find("#ddl_AccountCode").val($("#AccountCodeOneJv").val());
            return htmlCode;
        }
    }).on('show.bs.popover', function () {
        ClosePopOver("Jvspopoverp3");
        setTimeout(function () {
            $(".popover-content").find("#ddl_AccountCode").val($("#AccountCodeOneJv").val());
            $(".popover-content").find("#ddl_AccountCode").attr("data-live-search", true);
            $(".popover-content").find("#ddl_AccountCode").addClass('selectpickerddl');
            bindSelectpicker('.selectpickerddl');

        }, 300);
    });
});

function SetData() {
    var fd = ($('#form-search'))[0];
    var form = new FormData(fd);
    $.ajax({
        type: "POST",
        url: "/Payroll/SetPayrollData",
        data: $('#form-search').serialize(),//form,
        cache: false,
        success: function (data) {            
            GetGrid();
        }
    });
}

var oTableEmp;

function getGrid() {
    oTableEmp = $('#tbl_payroll').dataTable({
        "dom": 'rt',
        "language": {
            "lengthMenu": "Records per page _MENU_ ",
        },
        "sAjaxSource": "/Payroll/GetPayrollData",
        "aoColumns": [
            { "mData": "StaffID", 'width': '10%' },
            { "mData": "EmployeeName", 'width': '40%' },
            { "mData": "CategoryName", 'width': '25%' },
            { "mData": "Checked", "className": "dt-head-center", 'width': '25%' },
        ],
        //"processing": true,
        "serverSide": false,
        "ajax": "/Payroll/GetPayrollData",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        //"iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true,
        "paging": false,
        "bSort": false,
        //"scrollY": "300px",
        //"scrollX": "100%",
        //"scrollCollapse": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // $(nRow).attr("id", 'row' + aData["Id"]);
            //return nRow;
        },
        "fnDrawCallback": function () {
            var x = document.getElementById("tbl_payroll").rows.length;
            if (x < 9) {
                $("#divPayroll").removeClass("scroll-tbl_payroll");
            }
            else {
                $("#divPayroll").addClass("scroll-tbl_payroll");
            }
        }
    });



}

function SetData() {    
    var fd = ($('#form-search'))[0];
    var form = new FormData(fd);
    var msg = "";
    $.ajax({
        type: "POST",
        url: "/Payroll/SetPayrollData",
        data: $('#form-search').serialize(),//form,
        cache: false,
        success: function (data) {
            getGrid();
            if ($("#isPayActive").is(':checked'))
                msg = "All InActive Employees Records";
            else
                msg = "All Active Employees Records";

            //$("#lblEmployees").html(msg);
        }
    });
}

function Search() {    
    $('#payRollSearch').load("/Payroll/Search", function () {
        bindSelectpicker('.selectpickerddl');
    });
}

function checkall() {

    var showMsg = ($('#allcheck').is(':checked') == true ? "Are sure you want to active all employee in Accounts?" : "Are sure you want to deactive all employee in Accounts?");
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: showMsg }).done(function () {
        var values = [];
        var rows = "";
        var isPayActive;
        if ($('#allcheck').is(':checked')) {
            $(".checkbox").prop("checked", true);
            $(".checkbox").parent().parent().addClass('selected');
            rows = $('#tbl_payroll tr.selected');
            isPayActive = true;
        }
        else {
            $(".checkbox").prop("checked", false);
            $(".checkbox").parent().parent().removeClass('selected');
            rows = $('#tbl_payroll tr');
            isPayActive = false;
        }
        var table = $('#tbl_payroll').DataTable();
        var rowData = table.rows(rows).data();
        $(rowData).each(function (i, item) {
            //alert(item.Id);
            values.push(item.Id);
        });
        if (values != "") {
            $.ajax({
                type: "POST",
                url: "/Payroll/UpdateAllIsPayctive",
                data: { values: values, isPayActive: isPayActive },
                cache: false,
                success: function (data) {
                    getGrid();
                },
                dataType: "json",
                traditional: true
            });
        }
    }).fail(function () {
        var toggleChk = ($('#allcheck').is(':checked') == true ? false : true);
        $('#allcheck').prop('checked', toggleChk);
    })
}

function OnClickTr(obj) {
    var showMsg = ($(obj).is(':checked') == true ? "Are sure you want to active employee in Accounts?" : "Are sure you want to deactive employee in Accounts?");
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: showMsg }).done(function () {
        $('#allcheck').prop('checked', false);
        var isPayActive;
        if ($(obj).is(':checked')) {
            //$(obj).parent().parent().addClass('selected');
            $(obj).parent().parent().removeClass('selected');
            isPayActive = true;
        }
        else {
            //$(obj).parent().parent().removeClass('selected');
            $(obj).parent().parent().addClass('selected');
            isPayActive = false;
        }
        var employeeid = $(obj).data("val");
        $.ajax({
            type: "POST",
            url: "/Payroll/UpdateIsPayctive",
            data: { EmployeeID: employeeid, isPayActive: isPayActive },
            cache: false,
            success: function (data) {
                SetData();

            }
        });
    }).fail(function () {
        //ShowMessage("success", "No");
        var toggleChk = ($(obj).is(':checked') == true ? false : true);
        $(obj).prop('checked', toggleChk);
    })
}

function runCycle() {

    var bankID = "0";
    if ($("#BankID").val() != "")
        bankID = $("#BankID").val();
    var payCycleID = $("#PayCycleID").val();
    var runFinal = $('#RunFinal').prop("checked");
    var vacationMonth = $('#VacationMonth').prop("checked");
    var fromDate = $('#FromDate').val();
    var toDate = $('#ToDate').val();
    var noOfDays = $('#NoOfDays').val();
    var departmentID = $('#DepartmentID').val();

    if ((payCycleID != "" && bankID != "")) {

        var payrollSettings = {
            BankID: bankID,
            PayCycleID: payCycleID,
            RunFinal: runFinal,
            VacationMonth: vacationMonth,
            FromDate: fromDate,
            ToDate: toDate,
            NoOfDays: noOfDays,
            DepartmentID: departmentID
        }

        $.ajax({
            url: '/Payroll/runCycle',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ 'payrollSettings': payrollSettings }),
            success: function (data) {
                if (data.showType == "custom")
                    customShowMessage(data.result, data.resultMessage, 120000, "center");
                else
                    ShowMessage(data.result, data.resultMessage);
                if (data.result == 'success') {
                    if (runFinal)
                        window.location.reload();
                }
            }
        });

    } else {
        ShowMessage("error", "Please Select Cycle");
    }

}

function report() {
    var BankID = 0;
    if ($("#BankID").val() != "")
        BankID = $("#BankID").val();
    var PayCycleID = $("#PayCycleID").val();
    var RunFinal = $('#check_id').prop("checked");
    var categoryId = 0;
    if ($("#CategoryID").val() != "")
        categoryId = $("#CategoryID").val();

    if ((cycleid != "" && bankid != 0)) {
        $.ajax({
            type: "POST",
            url: "/Payroll/report",
            data: { bankid: bankid, cycleid: cycleid, categoryId: categoryId },
            cache: false,
            success: function (data) {

            }
        });

    } else {
        ShowMessage("error", "Please Select Bank and Cycle");
    }
}

function banklist() {
    var cycleid = $("#PayCycleID").val();
    if (cycleid != "") {
        window.location.assign("/payroll/getBankListReport?cycleId=" + cycleid);
    }
    else {
        ShowMessage("error", "Please Select Cycle");
    }
}

function oldCycles() {
    $("#modal_Loader").load("/Payroll/OldCycles");
    $("#myModal").modal("show");
    $(".modal-dialog").attr("style", "width:700px;");
    $("#modal_heading").text('Old Cycles');
}

function reportPDF() {

    var cycleid = $("#PayCycleID").val();
    var bankid = $("#BankID").val();
    var categoryId = $(".ddlCategory").val();
    var departmentId = $('#DepartmentID').val();
    if (cycleid != "") {
        $.ajax({
            type: "POST",
            url: "/Payroll/CheckIfCycleGenerated",
            data: { CycleId: cycleid },
            cache: false,
            success: function (data) {
                if (data.isGenerated) {
                    window.location.assign("/payroll/ExportToPdf?cycleid=" + cycleid + "&bankid=" + bankid + "&categoryId=" + categoryId + "&departmentId=" + departmentId);
                }
                else {
                    ShowMessage("error", "Please run the selected cycle before downloding report.");
                }
            }
        });
    }
    else {
        ShowMessage("error", "Please Select Cycle");
    }

}

function report() {
   
    var cycleid = $("#PayCycleID").val();
    var bankid = $("#BankID").val();
    var categoryId = $(".ddlCategory").val();
    var departmentId = $('#DepartmentID').val();
    if (cycleid != "") {
        $.ajax({
            type: "POST",
            url: "/Payroll/CheckIfCycleGenerated",
            data: { CycleId: cycleid },
            cache: false,
            success: function (data) {
                if (data.isGenerated) {
                    window.location.assign("/payroll/GetReport?cycleid=" + cycleid + "&bankid=" + bankid + "&categoryId=" + categoryId + "&departmentId=" + departmentId);
                }
                else {
                    ShowMessage("error", "Please run the selected cycle before downloding report.");
                }
            }
        });
        
    }
    else {
        ShowMessage("error", "Please Select Cycle");
    }

}

$("#PayCycleID").change(function () {
    var cycleid = $("#PayCycleID").val();
    if (cycleid != "") {
        $.ajax({
            type: "POST",
            url: "/Payroll/GetCyclesByID",
            data: { cycleid: cycleid },
            cache: false,
            success: function (data) {
                $("#FromDate").val(data.fromaDate);
                $("#ToDate").val(data.toDate);
                $("#NoOfDays").val(data.NoOfCycleDays);
            }
        });
    }

});

function getEmployeeNotinPayroll() {
    $.ajax({
        type: "POST",
        url: "/Payroll/GetEmployeeNotInPayRoll",
        cache: false,
        success: function (data) {
            if (data.Success) {
                $('#warningMessage').show();
                $('#lblTotalEmployeeNotinPayroll').html(data.Message);
            } else {
                $('#warningMessage').hide();
            }
        }
    });
}

function SavePayrollTemplateSetting() {
    var JVgroupingType = $("input[name=jvsTemplate]:checked").attr("data-val");
    var accountCode = $(".popover-content").find("#ddl_AccountCode").val();

    if (accountCode == undefined) {

        if (JVgroupingType == 3) {
            accountCode = $("#AccountCodeGroupBy").val();
        }
        if (JVgroupingType == 4) {
            accountCode = $("#AccountCodeOneJv").val();
        }
    }

    if (JVgroupingType == 3) {
        $("#AccountCodeGroupBy").val(accountCode);
    }
    if (JVgroupingType == 4) {
        $("#AccountCodeOneJv").val(accountCode);
    }

    if ((JVgroupingType == 3 && accountCode == "") || (JVgroupingType == 4 && accountCode == "")) {
        if (JVgroupingType == 3) {
            ShowMessage("error", "Please select default salary account for group by employee Jv.");
            $("[data-toggle=Jvspopoverp3]").popover('show');
        }
        if (JVgroupingType == 4) {
            ShowMessage("error", "Please select default salary account for one Jv.");
            $("[data-toggle=Jvspopoverp4]").popover('show');
        }
    }
    else {
        if (JVgroupingType != 3 && JVgroupingType != 4) {
            accountCode = "";
        }
        $.ajax({
            type: "POST",
            url: "/Payroll/SavePayrollTemplateSetting",
            data: {
                payrollTemplateId: $("input[name=payrollTemplate]:checked").attr("data-val"),
                jvsTemplateId: $("input[name=jvsTemplate]:checked").attr("data-val"),
                accountCode: accountCode
            },
            success: function (data) {
                if (data.parollSetting && data.jvsSetting) {
                    ShowMessage("success", "Payroll and Jvs template setting updated successfully");
                } else {
                    ShowMessage("error", "Technical error has occurred when updating payroll setting");
                }
                $('[data-toggle=popoverGroupByEmp3]').popover('hide');
            }
        });
    }

}

function retrieveFinalRun() {
    var cycleIds = [];
    $.each($("input[name='chkCycle']:checked"), function () {
        cycleIds.push($(this).val());
    });
    if (cycleIds.length == 1) {
        $("#payrollRetrieval").load("/Payroll/ValidateUser");
        $("#payrollRetrievalModal").modal("show");
        $("#payrollRetrievalModal_heading").text('Please Log in Again.');
    } else if (cycleIds.length == 0) {
        ShowMessage("error", "Please select cycle");
    } else {
        ShowMessage("error", "Please select one cycle only");
    }

}


function ClosePopOver(id) {
    //var accountCode = $(".popover-content").find("#ddl_AccountCode").val();
    //if (id.trim() == "Jvspopoverp3")
    //    $("#AccountCodeGroupBy").val(accountCode);

    //if (id.trim() == "Jvspopoverp4")
    //    $("#AccountCodeOneJv").val(accountCode);

    $('[data-toggle=' + id.trim() + ']').popover('hide');
}

function SelectJvAccount() {
    var JVgroupingType = $("input[name=jvsTemplate]:checked").attr("data-val");
    var accountCode = $(".popover-content").find("#ddl_AccountCode").val();
    if (JVgroupingType == 3) {
        $("#AccountCodeGroupBy").val(accountCode);
    }
    if (JVgroupingType == 4) {
        $("#AccountCodeOneJv").val(accountCode);
    }
}