﻿var oTableEmp;
$(document).ready(function () {
    getGrid();
});

function getGrid() {
    oTableEmp = $('#tbl_Payrollvarification').dataTable({
        "ajax": "/PayRequestConfirmation/PayrollConfirmationList?Mode=All&IsCancel=0&IsConfirmed=0",
        "aoColumns": [
            { "mData": "Date", "sTitle": "Request Date", 'width': '15%' },
            { "mData": "TypeName", "sTitle": "Type Name", 'width': '15%' },
            { "mData": "ActionType", "sTitle": "Action Type", 'width': '15%' },
            { "mData": "UserName", "sTitle": "Requested Employee Name", 'width': '15%' },
            { "mData": "Approved", "sTitle": "Approved Request", 'width': '15%' },
            { "mData": "Cancel", "sTitle": "Cancel Request", 'width': '15%' },
            { "mData": "Action", "sTitle": "Action", 'width': '10%' },
        ],
        "serverSide": false,
        "ajax": "/PayRequestConfirmation/PayrollConfirmationList?Mode=All&IsCancel=false&IsConfirmed=false",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        //"iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
    });
}

function CancelPayrollRequest(TypeId, e) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to cancel Payroll Request?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { "PayrollConfirmationId": TypeId },
            url: '/PayRequestConfirmation/CancelPayrollRequest',
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                if (data.Success)
                    getGrid();
            },
            error: function (data) { }
        });
    }).fail(function () {
        if ($(e).is(':checked'))
            $(e).prop("checked", false);
        else
            $(e).prop("checked", true);
    });
}

function ApprovedPayrollRequest(TypeId, e) {

    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to approved Payroll Request?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { "PayrollConfirmationId": TypeId },
            url: '/PayRequestConfirmation/ApprovedPayrollRequest',
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                if (data.Success)
                    getGrid();
            },
            error: function (data) { }
        });
    }).fail(function () {
        if ($(e).is(':checked'))
            $(e).prop("checked", false);
        else
            $(e).prop("checked", true);
    });
}

function DownloadExcelPayrollRequest(cycleId) {
    window.location.assign("/PayRequestConfirmation/GetReportForRequest?cycleid=" + cycleId);
}

function GetInfoPayrollRequest(payrollConfirmationId, typeName) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/PayRequestConfirmation/GetDetailsForRequest?PayrollConfirmationId=" + payrollConfirmationId + "&Type=" + encodeURIComponent(typeName), function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('View Payroll Request Details');
    });
}

function LoadPayrollVerification() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/PayRequestConfirmation/PayrollVerificationSetting", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Payroll Verification Setting');
    });
}

var PayrollVerificationSetting = [];
function SavePayrollVerificationSetting() {
    PayrollVerificationSetting = [];
    $("#PayrollSettingTable tbody tr").each(function () {
        var id = $(this).find('.checkbox').attr('data-id');
        PayrollVerificationSetting.push({
            "PayrollVerificationID": id,
            "IsEnabled": $("#chkisEnabled" + id).is(":checked")
        });
    });

    $.ajax({
        type: "POST",
        data: {
            VerificationSetting: JSON.stringify(PayrollVerificationSetting),
        },
        url: '/PayRequestConfirmation/SavePayrollVerificationSetting',
        success: function (data) {
            $("#myModal").modal('hide');
            if (data.Success) {
                ShowMessage(data.CssClass, data.Message);
            } else {
                ShowMessage(data.CssClass, data.Message);
            }
            PayrollVerificationSetting = [];
        },
        error: function (xhr) {
        }
    });

}