﻿var shiftChangejson = [];
var SelectedShift = [];
var ShiftData = [];
$(document).ready(function () {

    GetShiftNameList();

    $('input.timepicker').timepicker({       
        minTime: '00:00:00',
        timeFormat: 'h:iA',
        maxHour: 0,
        maxMinutes: 10,
        step: 5,        
        startTime: new Date(0, 0, 0, 15, 0, 0),
        disableTextInput: true,
        _twelveHourTime: true
    }).on('change', function () {
        createModificationJson(this);
    });
    var ShiftID = sessionStorage.getItem("SelectedShift");
    if (ShiftID == null) {
    }
    else {      
        GetShiftDetails(ShiftID);
    }
    $("#ShiftID_1").val(-1);
    bindIntgerOnly('.gracein');
    bindIntgerOnly('.graceout');
    bindIntgerOnly('.breakhours');
    $("#chkSelectAll").on("click", function () {

        if ($("#chkSelectAll").is(":checked")) {            
            $('.chkSelectShift').prop('checked', true);
            $.ajax({
                type: 'GET',                
                url: '/Shift/GetAllShiftNamesList',
                success: function (data) {
                    var s;
                    SelectedShift = [];
                    $.each(data, function (id, option) {
                        SelectedShift.push({
                            ShiftID: option.ShiftID,
                            ShiftName: option.ShiftName
                        });
                        if (id == 0) {
                            s = option.ShiftName;
                        }
                        else {
                            s += ', ' + option.ShiftName
                        }
                        
                    });
                    $("#SelectedShift").text(s);
                },
                error: function (data) { }
            });
        }
        else {
            $('.chkSelectShift').prop('checked', false);
            $("#SelectedShift").text('');
        }
    });     
});

function createModificationJson(e) {
    
    var exist = false;
    var shiftDay = $(e).closest('tr').attr('data-shiftday');
    var shiftId = (parseInt($('#ShiftID').val()) > -1 ? parseInt($('#ShiftID').val()) : -1);
    if (shiftDay == 7) {
        shiftDay = 0;
    }
    if ($(event.target).hasClass('weekend')) {
        if ($(e).is(':checked')) {
            $("#ShiftStart_" + (parseInt(shiftDay) + 1)).val('00:00:00')
            $("#ShiftStart_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftEnd_" + (parseInt(shiftDay) + 1)).val('00:00:00');
            $("#ShiftEnd_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftGraceIn_" + (parseInt(shiftDay) + 1)).val(0);
            $("#ShiftGraceIn_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftGraceOut_" + (parseInt(shiftDay) + 1)).val(0);
            $("#ShiftGraceOut_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#ShiftBreakHours_" + (parseInt(shiftDay) + 1)).val(0);
            $("#ShiftBreakHours_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
            $("#CutOffTime_" + (parseInt(shiftDay) + 1)).val('00:00:00');
            $("#CutOffTime_" + (parseInt(shiftDay) + 1)).prop('disabled', true);
        }
        else {
            $("#ShiftStart_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftEnd_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftGraceIn_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftGraceOut_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#ShiftBreakHours_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
            $("#CutOffTime_" + (parseInt(shiftDay) + 1)).prop('disabled', false);
        }
    }
    var objshiftChangejson = {
        ShiftID: shiftId,
        shiftDay: parseInt(shiftDay),
        isChanged: true
    }
    if (shiftId > -1) {
        if (shiftChangejson.length == 0) {
            shiftChangejson.push(objshiftChangejson);
        } else {
            for (var i = 0; i < shiftChangejson.length; i++) {
                if (shiftChangejson[i].shiftDay == parseInt(shiftDay)) {
                    exist = true;
                    break;
                }
            }
            if (!exist)
                shiftChangejson.push(objshiftChangejson);
        }
    }
    if (shiftChangejson.length > 0)
        $("#hdnShiftChange").val(JSON.stringify(shiftChangejson))
}

function BindShiftDetails(data) {
    
    $("#ObjShiftModelList_0__EndDate").removeClass("datepickerTo");
    $("#ObjShiftModelList_0__StartDate").removeClass("datepickerFrom");

    $("#ShiftName").text(data[0].ShiftName);
    $("#ShiftNameTxt").val(data[0].ShiftName);
    $("#ShiftID_1").val(data[0].ShiftID);
    if (data[0].StartDate == undefined || data[0].StartDate == null || data[0].StartDate == " ") {
        $("#ObjShiftModelList_0__EndDate").datepicker('setDate', getCurrentDateDDMMYYY());
        $("#ObjShiftModelList_0__StartDate").datepicker('setDate', getCurrentDateDDMMYYY());
    }
    else {
        $("#ObjShiftModelList_0__EndDate").datepicker('setDate', data[0].EndDate);
        $("#ObjShiftModelList_0__StartDate").datepicker('setDate', data[0].StartDate);

    }

    $("#ObjShiftModelList_0__EndDate").addClass(".datepickerTo");
    $("#ObjShiftModelList_0__StartDate").addClass(".datepickerFrom");

    var i = 1;
    $.each(data, function (id, option) {
        $.each(weekDaysJson, function (index, item) {
            if (item.Value == option.ShiftDay)
                i = item.bindValue;
        })
        $("#ObjShiftModelList_" + (i - 1) + "__ShiftDetailsId").val(option.ShiftDetailsId);

        if (option.IsWeekend) {
            $("#ShiftStart_" + i).val(option.ShiftStart).prop('disabled', true);
            $("#ShiftEnd_" + i).val(option.ShiftEnd).prop('disabled', true);
            $("#ShiftGraceIn_" + i).val(option.ShiftGraceIn).prop('disabled', true);
            $("#ShiftGraceOut_" + i).val(option.ShiftGraceOut).prop('disabled', true);
            $("#ShiftBreakHours_" + i).val(option.ShiftBreakHours).prop('disabled', true);
            $("#CutOffTime_" + i).val(option.CutOffTime).prop('disabled', true);
        }
        else {
            $("#ShiftStart_" + i).val(option.ShiftStart);
            $("#ShiftEnd_" + i).val(option.ShiftEnd);
            $("#ShiftGraceIn_" + i).val(option.ShiftGraceIn);
            $("#ShiftGraceOut_" + i).val(option.ShiftGraceOut);
            $("#ShiftBreakHours_" + i).val(option.ShiftBreakHours);
            $("#CutOffTime_" + i).val(option.CutOffTime);
        }

        $("#hdnisSpecial").val(option.isSpecialShift)
        $("#hdnisIncludeInAttendance").val(option.IncludeInAttendance)
        $("#IsWeekend_" + i).prop('checked', option.IsWeekend);
        $("#CutOffTime_" + i).val(option.CutOffTime);
        //i++;
    });
    if (parseInt(data[0].ShiftID) > -1) {
        $("#GetAllEmployeesNotInShift").load("/Shift/GetAllEmployeesNotInShift/" + data[0].ShiftID);
        $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift/" + data[0].ShiftID);               
    }
    $('.hiddenShiftID').val(data[0].ShiftID);
}

function GetShiftDetails(ShiftId) {
    sessionStorage.setItem("SelectedShift", ShiftId);
    if ($("#hdnOldModel").val() != "") {
        var counter = 0;
        var oldData = JSON.parse($("#hdnOldModel").val());

        if (oldData[0].ShiftName != $("#ShiftName").text())
            counter++;

        var i = 1;

        $.each(oldData, function (id, option) {

            if (option.ShiftStart != $("#ShiftStart_" + i).val())
                counter++;
            if (option.ShiftEnd != $("#ShiftEnd_" + i).val())
                counter++;
            if (option.ShiftGraceIn != $("#ShiftGraceIn_" + i).val())
                counter++;
            if (option.ShiftGraceOut != $("#ShiftGraceOut_" + i).val())
                counter++;
            if (option.ShiftBreakHours != $("#ShiftBreakHours_" + i).val())
                counter++;
            if (option.IsWeekend != $("#IsWeekend_" + i).prop('checked'))
                counter++;
            if (option.CutOffTime != $("#CutOffTime_" + i).val())
                counter++;
            i++;
        });

        if (counter > 0) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save previous changes?" }).done(function () {
                pageLoaderFrame();
                $("#frmAddShift").ajaxSubmit(function (data) {
                    ShowMessage("success", data.Message);
                    $("#hdnOldModel").val("");
                    //window.location.reload();
                    $.ajax({
                        type: 'POST',
                        data: { ShiftID: ShiftId },
                        url: '/Shift/GetSpecialShiftDetails',
                        success: function (data) {
                            hideLoaderFrame();
                            BindShiftDetails(data);
                            //keep in hidden value
                            $("#hdnOldModel").val(JSON.stringify(data));
                        },
                        error: function (data) { }
                    });
                });
            }).fail(function () {
                GetAndBindShiftDetails(ShiftId);
            });
        }
        else {
            GetAndBindShiftDetails(ShiftId);
        }
    }
    else {
        GetAndBindShiftDetails(ShiftId);
    }
    SelectShift(ShiftId);
}

function GetAndBindShiftDetails(ShiftId) {

    $.ajax({
        type: 'POST',
        data: { ShiftID: ShiftId },
        url: '/Shift/GetSpecialShiftDetails',
        async: true,
        success: function (data) {
            BindShiftDetails(data);
            //keep in hidden value
            $("#hdnOldModel").val(JSON.stringify(data));
        },
        error: function (data) { }
    });
}

function DeleteShiftID(ShiftID) {
    $.ajax({
        type: 'POST',
        data: { Id: ShiftID },
        url: '/Shift/GetAllEmployeesShiftCount',
        success: function (data) {
            var count = parseInt(data);
            if (count > 0) {
                ShowMessage("error", "Shift cannot be deleted as it has Employees in it.");
            }
            else {
                $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are You sure You want to delete Shift?" }).done(function () {
                    $.ajax({
                        type: 'POST',
                        data: { id: ShiftID },
                        url: '/Shift/DeleteShift',
                        success: function (data) {
                            ShowMessage("success", "Shift deleted successfully");
                            GetShiftNameList();
                        },
                        error: function (data) { }
                    });
                });
            }
        },
        error: function (data) { }
    });
}

function CopyShift() {
    var key = 7;
    for (var j = 1 ; j <= 7 ; j++) {
        var s = $("#ShiftStart_" + j).val();
        var s1 = $("#ShiftEnd_" + j).val();
        var s2 = $("#ShiftGraceIn_" + j).val();
        var s2 = $("#ShiftGraceOut_" + j).val();
        var s1 = $("#ShiftBreakHours_" + j).val();
        var s2 = $("#CutOffTime_" + j).val();

        if (s != "") {
           // var key = j;

            for (var k = 1; k <= 7; k++) {
                $("#ShiftStart_" + k).val($("#ShiftStart_" + key).val());
                $("#ShiftEnd_" + k).val($("#ShiftEnd_" + key).val());
                $("#ShiftGraceIn_" + k).val($("#ShiftGraceIn_" + key).val());
                $("#ShiftGraceOut_" + k).val($("#ShiftGraceOut_" + key).val());
                $("#ShiftBreakHours_" + k).val($("#ShiftBreakHours_" + key).val());
                $("#CutOffTime_" + k).val($("#CutOffTime_" + key).val());
            }
            break;
        }
    }
}

function SaveShiftDetails() {
    var SelectedShiftID = $("#ShiftName").text();
    if ((SelectedShiftID != null && SelectedShiftID != '') || SelectedShift.length>0)
    {
        submitShiftDetails();
    }
    else {
        ShowMessage("error","Please select shift first.");
    }
 
}

function checkFuturRecord(shiftId, EmpId, callBack) {
    $("#hdnOldModel").val("");
    $.ajax({
        type: 'GET',
        data: { shiftId: shiftId, EmpId: EmpId },
        url: '/Shift/CheckFuturRecordInAttShiftTable',
        success: function (data) {
            if (typeof callBack === "function")
                callBack(data);
        },
        error: function (data) { }
    });
}

$("#btnCancel").click(function () {
    $("#myModal").modal("hide");
});

function submitShiftDetails() {    
    if ($('form#frmAddShift').valid()) {
        pageLoaderFrame();
    }
    $("#hdnSelectedShift").val(JSON.stringify(SelectedShift));
    $('form#frmAddShift').submit();
}

function onFailure(xhr, status, error) {
    hideLoaderFrame();
    console.log("xhr", xhr);
    console.log("status", status);
    ShowMessage("info", error);
}

function onSuccess(data, status, xhr, arg) {
    console.log("data", data);
    console.log("status", status);
    console.log("xhr", xhr);
    hideLoaderFrame();
    if (data.Success == true) {
        ShowMessage("success", data.Message);
        location.reload();
        GetShiftNameList();
    }
    else {
        ShowMessage("error", data.Message);
    }

    $('.hiddenShiftID').val(data.InsertedRowId);
}

function closeform() {
    $("#myModal").modal("hide");
}

function GetShiftNameList() {    
    StartPanelLoader("#shiftListView");
    $("#ShiftList").load("/Shift/GetAllShiftNames?isDeleteEnable=false");
}

function AddEmployeeToShift(empId, currentshiftId, e) {
    checkFuturRecord(0, empId, function (a) {
        if (a.IsRecordExist == true) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "This shift change will overwrite the user shifts modification. Are you sure you want to save changes.?" }).done(function () {
                SubmitEmployeeToShift(empId, currentshiftId, e, true);
            }).fail(function () {

            });
        } else {
            SubmitEmployeeToShift(empId, currentshiftId, e, false);
        }
    });
}

function SubmitEmployeeToShift(empId, currentshiftId, e, isPass) {
    if (currentshiftId != "-1" && isPass == false) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to update shift for this employee?" }).done(function () {
            var depit = $('#ShiftID_1').val();
            $.ajax({
                type: 'POST',
                data: { shiftId: depit, EmployeeID: empId },
                url: '/Shift/InsertEmployeeShift',
                success: function (data) {
                    $("#GetAllEmployeesNotInShift").load("/Shift/GetAllEmployeesNotInShift/" + depit, function () {
                        $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift/" + depit);
                    });
                },
                error: function (data) { }

            });
        }).fail(function () {
            $(e).attr("checked", false);
            opResult = false;
        });
    }
    else {
        var depit = $('#ShiftID_1').val();
        $.ajax({
            type: 'POST',
            data: { shiftId: depit, EmployeeID: empId },
            url: '/Shift/InsertEmployeeShift',
            success: function (data) {
                $("#GetAllEmployeesNotInShift").load("/Shift/GetAllEmployeesNotInShift/" + depit, function () {
                    $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift/" + depit);
                });
            },
            error: function (data) { }
        });
    }
}

function RemoveShiftEmployee(employeeid) {
    var depit = $("#ShiftID_1").val();
    $.ajax({
        type: 'POST',
        data: { EmployeeID: employeeid, shiftId: depit },
        url: '/Shift/RemoveEmployeeShift',
        success: function (data) {
            $("#GetAllEmployeesNotInShift").load("/Shift/GetAllEmployeesNotInShift/" + depit, function () {
                $("#ShiftEmployeesList").load("/Shift/GetAllEmployeesShift/" + depit);
            });
        },
        error: function (data) { }
    });
}

function SetSpecialShiftIds(ShiftID, ShiftName) {
    var s;
    if ($("#chkSelectShift_" + ShiftID).is(":checked")) {
        SelectedShift.push({
            ShiftID: ShiftID,
            ShiftName: ShiftName
        });

        $.each($(SelectedShift), function (ind, item) {
            if (ind == 0) {
                s = item.ShiftName;
            }
            else {
                s += ', ' + item.ShiftName
            }
        });
        $("#SelectedShift").text(s);
    } else {
        s = '';
        SelectedShift.forEach(function (result, index) {
            if (result.ShiftID == ShiftID) {
                //selectList += '<option value=' + id + '>' + result.EmployeeName + '</option>';
                SelectedShift.splice(index, 1);
            }
        });
        $.each($(SelectedShift), function (ind, item) {
            if (ind == 0) {
                s = item.ShiftName;
            }
            else {
                s += ', ' + item.ShiftName
            }
        });
        $("#SelectedShift").text(s);
    }
}

function SelectShift(ShiftID) {
    $('a').removeClass('active');
    $("#ancShift_" + ShiftID).addClass('active');
}

function UpdateShiftDetails()
{
      ShiftData.push({
          ShiftDay: $("ObjShiftModelList_" + i + "__ShiftDay").val(),
          ShiftStart: $("ObjShiftModelList_" + i + "__ShiftDay").val()
        });
    
 
}