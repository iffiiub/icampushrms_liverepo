﻿var AddtionReport = function () {

    loadAdditionReport = function () {
        var text = "/Reporting/AdditionReportviewer?reportType=AdditionReport";
        text = urlParams(text);
        pageLoaderFrame();
        $('#LoadReportDiv').html('').load(encodeURI(text), function () {
            hideLoaderFrame();
        });
    },
    urlParams = function (url) {
        var startDate = $('#txtStartDate').val();
        var toDate = $('#txtToDate').val();
        if (startDate != "")
            url = url + "&startDate=" + startDate;
        if (toDate != "") {
            url = url + "&toDate=" + toDate;
        }

        url = url + "&addtionType=" + $('#additionddl').val();
        return url;
    },
    inti = function () {
    };
    return {
        loadAdditionReport: loadAdditionReport
    };
}();
$(document).ready(function () {
    $("#txtStartDate").val(getCurrentDateDDMMYYY());
    $("#txtToDate").val(getCurrentDateDDMMYYY());

    $('#btnAddition').click(function () {
        AddtionReport.loadAdditionReport();
    })
});