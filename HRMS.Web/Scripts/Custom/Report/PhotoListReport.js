﻿$(document).ready(function () {
    $("#btnStatment").click(function () {      
        var employeeIds = getSelectedIds('.multiddl').join(',');
        var DeptId = $("#DepartmentList").val();
        var PosId = $("#PositionList").val();
        if (employeeIds != "") {
            //'/Reporting/LoadEmployeePhotoCrReport?Empids=' + employeeIds + '&DeptID=' + DeptId + "&PosID=" + PosId
            globalFunctions.previewCRReport('/Reporting/LoadEmployeePhotoCrReport?Empids=' + employeeIds + '&DeptID=' + DeptId + "&PosID=" + PosId);
           
        } else {
            ShowMessage("error","Please select employee.")
        }
               
    });
});

function rebuildEmpDDl()
{    
    $jq14("#ddlEmployeeList").next().addClass('open');
    $jq14("#ddlEmployeeList").next();
    $jq14("#ddlEmployeeList").multiselect("selectAll");
    $jq14("#ddlEmployeeList").multiselect('updateButtonText');
    $jq14("#ddlEmployeeList").multiselect('rebuild');
    $jq14("#ddlEmployeeList").next().removeClass('open');
}
function IncludeIncativeEmployee(e) {
    var active = null;
    if (!$(e).is(':checked')) {
        active = true;
    }
    else {
     
    }
    
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { 'active': active },
        url: '/Common/GetEmployeeDdl',
        success: function (data) {
            $jq14("#ddlEmployeeList").empty();
            $jq14("#ddlEmployeeList").multiselect('refresh');
            var parsed = data;
            for (var i = 0; i < data.length; i++) {
                
                $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');
               
            }
            rebuildEmpDDl();
        },
        error: function (data) {
        }
    })
}


