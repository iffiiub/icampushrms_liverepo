﻿$jquery = jQuery.noConflict(true);
$jquery(document).ready(function () {
    $jquery('#ddlCompanyList').multiselect({ enableFiltering: false, enableCaseInsensitiveFiltering: true, maxHeight: '300', includeSelectAllOption: true });

    $("#wPSMonthlySalaryReportPreview").click(function () {
        var PayCycleId = $("#ddlPayCycle").val();
        var ComplanyIds = getSelectedIds('.ddlCompany').join(',');       
        globalFunctions.previewCRReport('/Reporting/LoadWPSMonthlySalaryReport?complanyIds=' + ComplanyIds + '&payCycleId=' + PayCycleId + '&payCycleTitle=' + $("#ddlPayCycle option:selected").text());
    });
});

function WPSMonthlySalaryReportExport() {
    var PayCycleId = $("#ddlPayCycle").val();
    var ComplanyIds = getSelectedIds('.ddlCompany').join(',');
    if (ComplanyIds.length > 1)
        var organizationtitle = "";
    else
        var organizationtitle = $("#ddlCompanyList option:selected").text();
    window.location.assign('/Reporting/WPSMonthlySalaryReportExport?complanyIds=' + ComplanyIds + '&payCycleId=' + PayCycleId + '&payCycleTitle=' + $("#ddlPayCycle option:selected").text() + '&organizationtitle=' + organizationtitle);
}

