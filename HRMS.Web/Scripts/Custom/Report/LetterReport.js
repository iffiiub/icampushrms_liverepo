﻿$(document).ready(function () {
    $('#txtletterReport').summernote({
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['fontsize', ['fontsize']],
          ['fontname', ['fontname']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['Insert', ['picture', 'link', 'ulink', 'table', 'hr', 'ltr', 'rtl']],
          ['height', ['height']],
           ['removeTableBorder', ['removeTableBorder']]
        ],
        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Arial UniCode MS', 'Simplified Arabic']
    });
    $(".note-editable").droppable();

    $(document).on('keyup', '#txtSearchField', function () {

        var searchText = $(this).val().toLowerCase();

        $('.reportFieldsList a').each(function () {

            var currentLiText = $(this).find('span.title').html().toLowerCase(),
                showCurrentLi = currentLiText.indexOf(searchText) !== -1;

            $(this).toggle(showCurrentLi);

        });
    });

    $(document).on('keyup', '#txtSearchLetterTemplates', function () {

        var searchText = $(this).val().toLowerCase();

        $('.LetterTemplatesList a').each(function () {

            var currentLiText = $(this).find('span.title').html().toLowerCase(),
                showCurrentLi = currentLiText.indexOf(searchText) !== -1;

            $(this).toggle(showCurrentLi);

        });
    });

    $(document).on('keyup', '.note-editable.ui-droppable', function (e) {
        var keyCode = e.keyCode || e.which;

        if (keyCode == 9) {
            event.stopPropagation();
            e.preventDefault();

        }
    })

    if ($("#hdnletterTemplatefields") != "") {
        var letterTemplatearray = $("#hdnletterTemplatefields").val().split(",");

        $.each(letterTemplatearray, function (ind, item) {
            TransferToSelectedById(item);
        })
    }
   //GetIsActiveEmployee();

    $(document).on("click", "table>tbody>tr>td", function () {
        if ($(this).parents('table').hasClass("removeTableBorder")) {
            $(this).parents('table').removeClass("removeTableBorder");
        }
        else {
            if (!($(this).parents('table').hasClass("borderless"))) {
                $(this).parents('table').addClass("removeTableBorder");
            }
        }
    });
});

function GetIsActiveEmployee() {   
    var chkIsActive = $("#chkIsActive").is(":checked")
    $.ajax({
        type: "POST",       
        url: "/Reporting/GetIsActiveEmployee",
        data: { IsActive: chkIsActive},      
        dataType: "json",
        success: function (data) {                       
            $jq14("#ddlEmployeeList").empty();
            $jq14("#ddlEmployeeList").multiselect('refresh');
            var parsed = data;
            for (var i = 0; i < data.length; i++) {
                $("#ddlEmployeeList").append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
                $("#ddlEmployeeList").parents('.multiddl').find('ul').append('<li class="" style="display: list-item;"><a tabindex="0"><label class="checkbox"><input type="checkbox" value=' + data[i].Value + '>' + data[i].Text + '</label></a></li>');

            }
            $jq14("#ddlEmployeeList").next().addClass('open');
            $jq14("#ddlEmployeeList").next();
           // $jq14("#ddlEmployeeList").multiselect("selectAll");
            $jq14("#ddlEmployeeList").multiselect('updateButtonText');
            $jq14("#ddlEmployeeList").multiselect('rebuild');
            $jq14("#ddlEmployeeList").next().removeClass('open');
        },
        error: function () {
            alert("error");
        }
    });
}



function TransferToSelected(e) {
    var id = $(e).attr("id");
    TransferToSelectedById(id);
    //var sqlId = $(e).attr("data-SqlField");
    //var description = $(e).text();
    //description = description.replace(">>", "");
    //description = description.trim();
    //if ($(e).attr('data-selected') == 'no') {
    //    $("#divSelectedFields").append("<div id='nestable' class='dd'>" +
    //                               "<ol class='dd-list'>" +
    //                               "<li data-id='" + id + "' class='dd-item list-group-item dragitems' fieldid='" + id + "'>" +
    //                               "<div class='dd-handle' data-SqlField='" + sqlId + "' onclick='insertText(this)' data-id='" + id + "' data-desc='" + description + "'>" + description + "</div>" +
    //                               "<div class='list-group-controls Report-list-controls'>" +
    //                               "<button class='btn btn-danger btn-rounded btn-condensed btn-sm btn-space' fieldid='" + id + "' onclick='RemoveField(this);'><span class='fa fa-times'></span></button>" +
    //                               "</div>" +
    //                               "</li>" +
    //                               "</ol>" +
    //                               "</div>");
    //    $(e).attr('data-selected', 'yes');
    //    $(".dd-list").draggable();

    //}
}

function TransferToSelectedById(id) {
    var e = $("#tblReportsFields").find("a[id='" + id + "']")
    var id = $(e).attr("id");
    var sqlId = $(e).attr("data-SqlField");
    var description = $(e).text();
    description = description.replace(">>", "");
    description = description.trim();
    if ($(e).attr('data-selected') == 'no') {
        $("#divSelectedFields").append("<div id='nestable' class='dd'>" +
                                   "<ol class='dd-list'>" +
                                   "<li data-id='" + id + "' class='dd-item list-group-item dragitems' fieldid='" + id + "'>" +
                                   "<div class='dd-handle' data-SqlField='" + sqlId + "' onclick='insertText(this)' data-id='" + id + "' data-desc='" + description + "'>" + description + "</div>" +
                                   "<div class='list-group-controls Report-list-controls'>" +
                                   "<button class='btn btn-danger btn-rounded btn-condensed btn-sm btn-space' fieldid='" + id + "' onclick='RemoveField(this);'><span class='fa fa-times'></span></button>" +
                                   "</div>" +
                                   "</li>" +
                                   "</ol>" +
                                   "</div>");
        $(e).attr('data-selected', 'yes');
        $(".dd-list").draggable();

    }
}

function insertText(e) {

    var selection = window.getSelection();
    var range = document.createRange();
    var toInsert = "";
    if (selection.type == "Caret" || selection.type == "Range") {
        if ($(selection.focusNode.parentElement).attr("class") != "dd-handle") {
            var endPos = selection.focusOffset;
            var cursorPos = selection.anchorOffset;
            var fieldId = $(e).attr("data-id").trim();
            var desc = $(e).attr("data-desc").trim();
            var sqlId = $(e).attr("data-SqlField").trim();
            fieldId = fieldId.replace(" ", "");
            toInsert = '{' + fieldId + ":" + sqlId + '}';
            var newContent = "";
            if (endPos == cursorPos) {
                var oldContent = selection.focusNode.wholeText;

                if (oldContent == undefined) {

                    newContent = toInsert;
                    $(document.getSelection().anchorNode).append(newContent);
                }
                else {
                    oldContent = oldContent;
                    newContent = oldContent.substring(0, cursorPos) + toInsert + oldContent.substring(cursorPos);
                    newContent = newContent.trimRight();
                    selection.anchorNode.nodeValue = newContent;
                }
            }
            else {
                var oldContent = selection.focusNode.wholeText;
                var textToReplace = oldContent.substring(cursorPos, endPos);
                newContent = oldContent.replace(oldContent.substring(cursorPos, endPos), toInsert);
                newContent = newContent.trimRight();
                selection.anchorNode.nodeValue = newContent;
            }
            var setPostion = selection.focusNode.wholeText.indexOf(toInsert) + toInsert.length;
            range.setStart(selection.focusNode, setPostion);
            range.collapse(true);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }
}

$("#btnSaveReport").click(function () { SaveDocFile(); });

function SaveDocFile() {
    var textareaValue = $('.note-editable').html();
    var fileName = $("#txtLetterName").val();
    textareaValue = textareaValue.trim();
    if (textareaValue != "") {
        if (fileName != "") {

            $.ajax({
                type: 'post',
                url: '/Reporting/checkForAvailableReport',
                datatype: 'JSON',
                data: {
                    LetterId: $("#hdnLetterId").val(),
                    FileName: fileName
                },
                success: function (op) {
                    var msg = "error";
                    if (op.Success) {
                        var msg = "Letter template is already exists with " + fileName + " name.";                      
                        ShowMessage("error", msg);

                    }
                    else {
                        $.ajax({
                            type: 'post',
                            url: '/Reporting/GetDocFile',
                            datatype: 'JSON',
                            data: {
                                LetterId: $("#hdnLetterId").val(),
                                FileName: fileName,
                                LetterField: GetSelectedId().join(','),
                                Content: textareaValue,
                                oldFileName: $("#hdnLetterName").val()
                            },
                            success: function (op) {
                                var msg = "error";
                                if (op.Success) {
                                    msg = "success";
                                }
                                ShowMessage(msg, op.Message);
                                if (op.Success) {
                                    setTimeout(function () {
                                        window.location.href = "/Reporting/LetterReport"
                                    }, 1000)
                                }

                            }
                        });

                    }
                }
            });


        }
        else {

            ShowMessage("error", "Please enter letter template name");
        }
    }
    else {
        ShowMessage("error", "Please enter content in text editor");
    }
}

function DeleteLetterTemplate(id, filename) {
    var msg = "Do you want to delete " + filename + " template?";


    $.MessageBox({
        buttonDone: "Yes",
        buttonFail: "No",
        message: msg,
    }).done(function () {
        $.ajax({
            type: 'post',
            url: '/Reporting/DeleteLetterTemplate',
            datatype: 'JSON',
            data: {
                templateId: id,
                FileName: filename,

            },
            success: function (op) {
                var msg = "error";
                if (op.Success) {
                    msg = "success";
                }
                ShowMessage(msg, "Letter template deleted successfully.");
                if (op.Success) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)
                }

            }
        });

    }).fail(function () {


    });
}

$("#btnDownload").click(function () {
    DownloadLetter();
});

function DownloadLetter() {   
    var employeeId = getSelectedIds('.ddlEmployee').join(',');
    var templateName = $('.list-group-item.highlightRow span').html();;
    var templateId = $('.list-group-item.highlightRow span').attr('data-TemplateId');   
    var validated = false;
    if (employeeId != "") {
        validate = true;
        if (templateId != undefined) {
            validated = true
        }
        else {
            validated = false;
            ShowMessage("error", "Please select letter template");

        }
    }
    else {
        validated = false;
        ShowMessage("error", "Please select employee");
    }


    if (validated) {
        pageLoaderFrame();
        $.ajax({
            type: 'post',
            url: '/Reporting/DownloadEmployeeLetter',
            datatype: 'JSON',
            data: {
                EmpId: employeeId,
                TemplateName: templateName
            },
            success: function (op) {
                var msg = "error";
                if (op.Success) {
                    hideLoaderFrame();
                    msg = "success";
                    window.location.assign("/Reporting/DownloadEmpletter?EmployeeId=" + employeeId + "&TemplateName=" + templateName);
                }
            }

        });

    }
}

function RemoveSelected() {
    $('.reportFieldsList a').each(function () {
        $(this).attr('data-selected', 'no');
    });
    $("#divSelectedFields").html("");
}

function RemoveField(e) {
    $('.reportFieldsList a').each(function () {

        //alert($(this).val());
        if ($(this).attr("id") == $(e).attr("fieldid")) {
            $(this).attr('data-selected', 'no');
        }

    });


    $(e).closest('.dd').remove();
}

$(document).on("click", "#tblLetterTemplates > a ", function () {
    $("#tblLetterTemplates > a ").removeClass('highlightRow');
    if ($(this).hasClass('highlightRow')) {
        $(this).removeClass('highlightRow');
    }
    else {
        $(this).addClass('highlightRow');
    }
});

function EditTemplate(ReportId, ReportName) {
    window.location.href = "/Reporting/LetterReport/" + ReportId;
}

function GetSelectedId() {
    var selectedIds = [];
    $.each($("#divSelectedFields li"), function (ind, item) {
        selectedIds.push($(item).attr('data-id'));
    });
    return selectedIds;
}