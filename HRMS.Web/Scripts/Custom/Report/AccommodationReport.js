﻿$(document).ready(function () {
    $("#btnPreviewAccommodationReport").click(function () {
        var academicId = $("#ddlAcademicYear").val();
        var employeeId = $("#ddlEmployee").val();
        var txtEmpId = $("#txtEmpId").val();
        var text = "/Reporting/AccommodationreportViewer?employeeId=" + employeeId + "&academicYearId=" + academicId;
        $('#LoadReportDiv').load(encodeURI(text));
    })
});