﻿$(document).ready(function () {
    //$("#btnSalaryIncrementPreview").click();
});
$("#btnSalaryIncrementPreview").click(function () {
    var EmpIds = getSelectedIds('.multi-Select').join(',');
    var url = "/Reporting/SalaryIncrementReportViewer?employeeIds=" + EmpIds.toString();
    if (parseInt($("#ddlDepartment").val()) > -1)
        url = url + "&departmentId=" + $("#ddlDepartment").val();
    if (parseInt($("#ddlPayCategory").val()) > -1)
        url = url + "&payCategory=" + $("#ddlPayCategory").val();
    $("#LoadReportDiv").load(url);
});

$("#chkActive").click(function () {
    var checked = false;
    if ($(this).is(":checked"))
        checked = true;
    else
        checked = false;

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { active: checked },
        url: '/Common/GetEmployeeDdl',
        success: function (data) {
            $("#ddlEmployeeList").find("option").remove();
            $.each(data, function (ind, item) {
                $("#ddlEmployeeList").append(
                    $('<option></option>').val(item.Value).html(item.Text)
                 );
            });
            $jquery("#ddlEmployeeList").multiselect("rebuild");
        },
        error: function (data) {
        }
    })

});