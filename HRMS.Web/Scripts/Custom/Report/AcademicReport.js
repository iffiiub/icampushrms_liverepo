﻿var urlText = "";
$(document).ready(function () {

   
    $("#btnEmployeeTraining").click(function () {
        var traingId = $("#ddlTraning").val();
        var countryId = $("#ddlContry").val();
        var fromDate = $("#txtFromDate").val();
        var toDate = $("#txtToDate").val();
        var employeeId = $("#ddlEmployee").val();
        var reportByTraining = $('#chkByTraining').prop('checked');
        $("#divStireport").hide();
        $("#divCrReport").hide();
        var parameterstring = "";

        if (parseFloat(traingId) > 0) {
            parameterstring = parameterstring + "?trainingId=" + traingId;
        }
        else {
            parameterstring = parameterstring + "?trainingId=0";
        }
        if (parseFloat(employeeId) > 0) {
            parameterstring = parameterstring + "&employeeId=" + employeeId;
        }

        if (parseFloat(countryId) > 0) {
            parameterstring = parameterstring + "&countryId=" + countryId;
        }

        if (fromDate != "") {
            parameterstring = parameterstring + "&fromDate=" + fromDate;
        }
        if (toDate != "") {
            parameterstring = parameterstring + "&toDate=" + toDate;
        }
        if ($("#chkTotalHour").is(":checked")) {
            parameterstring = parameterstring + "&TotalHour=true";
        }
        else {
            parameterstring = parameterstring + "&TotalHour=false";
        }
        if (reportByTraining) {
            parameterstring = parameterstring + "&reportByTraining=true";
        }
        else {
            parameterstring = parameterstring + "&reportByTraining=false";
        }
        if (reportByTraining) {
            urlText = "/Reporting/GetEmployeeProfessionalDevelopmentByTraining";
            urlText = urlText + parameterstring;
            $("#divCrReport").show();
            globalFunctions.previewCRReport(encodeURI(urlText));
        }
        else {
            urlText = "/Reporting/EmployeeProfessionalDevelopmentReportViewer";
            urlText = urlText + parameterstring;
            $("#divStireport").show();
            pageLoaderFrame();
            $('#LoadReportDiv').load(encodeURI(urlText), function () {
                hideLoaderFrame();

            });

        }
    })

    $("#btnStaffQualifiaction").click(function () {
        var jobCategory = $("#ddlJobCategory").val();
        var employeeId = $("#ddlEmployee").val();
        var nationalityId = $("#ddlNationality").val();
        if (parseFloat(jobCategory) > 0)
            urlText = "/Reporting/StaffQualificationReportViewer?jobCategory=" + jobCategory;
        else
            urlText = "/Reporting/StaffQualificationReportViewer?jobCategory=0";

        if (parseFloat(employeeId) > 0)
            urlText = urlText + "&employeeId=" + employeeId;

        if (parseFloat(nationalityId) > 0)
            urlText = urlText + "&nationalityId=" + nationalityId;

        $("#LoadReportDiv").load(urlText);
    })

    $('#chkByTraining').click(function () {

        if ($('#chkByTraining').prop('checked')) {
            $("#chkTotalHour").removeAttr("checked");
            $("#chkTotalHour").attr("disabled", true);
        }
        else {
            $("#chkTotalHour").attr("disabled", false);
        }
    });
});