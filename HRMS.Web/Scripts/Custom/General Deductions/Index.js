﻿var confirmedIds = [];
var fromDate = "";
var toDate = "";
var deductionType = "";
var EmployeeId = "";
$(document).ready(function () {

    GetGrid();
    $("#IsGenerated").click(function () {
        var isChecked = $("#IsGenerated").is(":checked");
        if (isChecked) {
            $("#GenerateDate").removeAttr('disabled');
            $("#Cycle").attr('disabled', false)
            $("#Cycle").selectpicker('refresh');
        }
        else {
            $("#GenerateDate").attr('disabled', 'disabled');
            $("#Cycle").attr('disabled', true)
            $("#Cycle").selectpicker('refresh');
        }
    });
})
$("#btn_addGeneralDeductions").click(function () {
    AddGeneralDeductions();

});

function AddGeneralDeductions() {

    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/GeneralDeductions/AddGeneralDeductions", function () {
        $("#myModal3").modal("show");
        $("#modal_heading3").text('Add General Deductions');
        $(".modal-dialog").attr("style", "width:700px;");
        bindSelectpicker(".selectpickerddl");
        bindDatePicker(".datepicker");
    });
}

function GetGrid() {
    pageLoaderFrame();
    fromDate = $("#FromDate").val();
    toDate = $("#ToDate").val();
    deductionType = $("#ddl_DeductionType").val();
    EmployeeId = $("#ddl_EmployeeName").val();
    var isConformed = $("#chkGetConfirmed").is(":checked");
    oTableLateduduction = $('#tblGeneralDeductions').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4"><".col-md-4 pageNo"p>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "FromDate", "value": fromDate },
                { "name": "ToDate", "value": toDate },
                { "name": "IsConfirmed", "value": isConformed },
                { "name": "EmployeeId", "value": EmployeeId },
                { "name": "DeductionTypeId", "value": deductionType }
                );
        },
        "sAjaxSource": "/GeneralDeductions/GetDeductableEmployeeList",
        "aoColumns": [
            { "mData": "isConfirmed", "sTitle": " <label class='control-label'>Select All <input id='chkGetConfirmed' onclick='checkall(this)' type='checkbox' value='1' /></label>", "bSortable": false },
            { "mData": "EmployeeAlternativeId", "sTitle": "Employee ID" },
            { "mData": "EmployeeName", "sTitle": "Employee Name" },
            { "mData": "DeductionDate", "sTitle": "Deduction Date", "sType": "date-uk" },
            { "mData": "DeductionRuleDesc", "sTitle": "Deduction Type" },
            { "mData": "DeductionAmount", "sTitle": "Amount", "sClass": "text-center" },
            { "mData": "Action", "sTitle": "Action", "bSortable": false }
        ],
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "autoWidth": false,
        "drawCallback": function (settings) {

        }
    });
    hideLoaderFrame();
    if (isConformed) {
        oTableLateduduction.fnSetColumnVis(6, false);
    }
    else {
        oTableLateduduction.fnSetColumnVis(6, true);
    }

}


function editDeduction(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/GeneralDeductions/EditGeneralDeduction?EmployeeGeneralDeductionID=" + id, function () {
        $("#myModal3").modal("show");
        $("#modal_heading3").text('Edit General Deductions');
        $(".modal-dialog").attr("style", "width:700px;");
        bindSelectpicker(".selectpickerddl");
        bindDatePicker(".datepicker");
    });
}

function checkCycleExistForDate(DeductionDate) {
    $.ajax({
        type: 'GET',
        data: { EmployeeId: 0, DeductionRuleId: 0, DeductionDate: DeductionDate },
        url: '/GeneralDeductions/CalculateAmount',
        success: function (data) {
            if (data.Amount.indexOf('-1') >= 0) {
                ShowMessage("error", "Cycle is not generated for selected deduction date.");
                $("#btnSubmit").attr('disabled', 'disabled');
                $(".selectpickerddl").attr('disabled', true)
                $(".selectpickerddl").selectpicker('refresh');
            }
            else {
                $("#btnSubmit").attr('disabled', false);
                $(".selectpickerddl").attr('disabled', false)
                $(".selectpickerddl").selectpicker('refresh');
            }
        },
        error: function (data) { }
    });
}

function ConfirmEmployeeDeductionStatus() {

    if (fromDate != "" && toDate != "" && GenerateDate != "" && $('#IsGenerated').is(':checked') == true) {

        var GenerateDate = $('#GenerateDate').val();
        var CycleID = $('#Cycle').val();
        if (CycleID == 0) {
            ShowMessage("error", "Please Select Cycle");
        }
        else {
            $.ajax({
                url: '/GeneralDeductions/SaveGeneralDeduction',
                type: 'POST',
                data: {
                    SelectedIds: JSON.stringify(confirmedIds),
                    GenerateDate: GenerateDate,
                    CycleID: CycleID
                },
                success: function (data) {
                    //alert(data.Result);
                    ShowMessage(data.result, data.resultMessage);
                    if (data.result == "success") {
                        window.setTimeout(function () { GetGrid() }, 1000)
                    }
                },
            });
        }

    }
    else {
        ShowMessage("error", "Please Select Date to confirm and Check Generate");
    }
}
function updateConfirmStatus(EmployeeGeneralDeductionID) {
    var isChecked = $("#chkConfirmed" + EmployeeGeneralDeductionID).is(":checked");
    if (isChecked) {
        confirmedIds.push({
            "EmployeeGeneralDeductionID": EmployeeGeneralDeductionID
        });
    }
    else {
        confirmedIds = RemoveElement(confirmedIds, 'EmployeeGeneralDeductionID', EmployeeGeneralDeductionID);
    }
}

function checkall(e) {
    var isSelectAll = $(e).is(":checked");
    confirmedIds = [];

    if (isSelectAll) {
        $('.chkConfirmed').prop('checked', true);

        $.ajax({
            url: '/GeneralDeductions/GetAllSelectedEmployeeList',
            data: {
                EmployeeId: EmployeeId,
                DeductionTypeId: deductionType,
                FromDate: fromDate,
                ToDate: toDate
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',

            success: function (data) {
                //alert(data.Result);            
                $.each(data, function (index, jsonObject) {
                    $.each(jsonObject, function (key, val) {
                        if (key == "EmployeeGeneralDeductionID") {
                            confirmedIds.push({
                                "EmployeeGeneralDeductionID": val
                            });
                        }
                    });
                });
            },

        });

    }
    else {
        $('.chkConfirmed').prop('checked', false);
    }


}

function deleteDeduction(EmployeeGeneralDeductionID) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete selected General Deduction?" }).done(function () {
        $.ajax({
            url: '/GeneralDeductions/DeleteGeneralDeduction',
            type: 'POST',
            data: {
                EmployeeGeneralDeductionID: EmployeeGeneralDeductionID
            },
            success: function (data) {
                //alert(data.Result);                   
                ShowMessage(data.CssClass, data.Message);
                if (data.CssClass == "success") {
                    window.setTimeout(function () { GetGrid() }, 1000)
                    
                }
            },
        });

    })



}
