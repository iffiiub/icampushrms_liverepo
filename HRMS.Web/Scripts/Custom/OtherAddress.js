﻿$(document).ready(function () {
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));
    getGrid($('#hiddenEmployeeId').val());
    $("#btn_add").click(function () {
        //if ($('#hiddenEmployeeId').val() == '')
        //    window.location.href = "/ViewEmployeeProfile/Index/" + $('#hiddenEmployeeId').val();
        //else
        //    window.location.href = "/OtherAddress/OtherAddress?EmployeeId=" + $('#hiddenEmployeeId').val() + "&RefId=0";

        //AddEditOtherContact(0);
        AddEditOtherAddress(0);
    });

    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    $("#btn_BackProfile").click(function () { window.location.href = "/ViewEmployeeProfile/Index/" + $('#hiddenEmployeeId').val(); });
});

var oTableChannel;
function getGrid(employeeId) {

    oTableChannel = $('#tbl_OtherAddressList').dataTable({
        //"dom": 'rt',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/OtherAddress/GetOtherAddressList",
        "aoColumns": [
                { "mData": "OtherAddressId", "bVisible": false, "sTitle": "OtherAddressId" },
            { "mData": "EmployeeID", "bVisible": false, "sTitle": "EmployeeID" },
            { "mData": "ApartmentNo", "sTitle": "Apt No." },
            { "mData": "AddressTypeName", "sTitle": "Address type" },
            { "mData": "Address", "sTitle": "Address" },
            { "mData": "Street", "sTitle": "Street" },
            { "mData": "CountryName", "sTitle": "Country" },
            { "mData": "CityName", "sTitle": "City" },
            { "mData": "AreaName", "sTitle": "Area" },
            { "mData": "Actions", "sTitle": "Actions" }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/OtherAddress/GetOtherAddressList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "paging": true,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "EmployeeID", "value": employeeId });
        },
        "fnDrawCallback": function () {
            
        }
    });
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/OtherAddress/ExportToExcel?EmployeeId=" + $("#hiddenEmployeeId").val());
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/OtherAddress/ExportToPdf?EmployeeId=" + $("#hiddenEmployeeId").val());
    }
    else ShowMessage("warning", "No data for export!");
}

function AddEditOtherAddress(AddressId) {
    var href = "/OtherAddress/OtherAddress?EmployeeId=" + $('#hiddenEmployeeId').val() + "&RefId=" + AddressId;
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader").load(href, function () {
        if (parseInt(AddressId) > 0)
            $("#modal_heading").text('Edit Other Address Contact');
        else
            $("#modal_heading").text('Add Other Address Contact');
        $('#myModal').modal({ backdrop: 'static' });
        bindSelectpicker(".selectpickerddl");
    });
    //$("#myModal").modal("show");    
}

function DeleteContact(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/OtherAddress/DeleteAddress',
            success: function (data) {
                ShowMessage(data.Success == true ? "success" : "error", data.Message);
                getGrid($('#hiddenEmployeeId').val());
            },
            error: function (data) {
                ShowMessage("error", data.message);
            }
        });
    });
}

$(document).on('change', '#CountryID', function () {
    var selectedItem = $(this).val();
    if (parseInt(selectedItem) >= 0) {
        var url = "/Employee/GetStatesByCountryId?countryId=" + selectedItem;
        FilterDropDown('#CountryID', '#StateID', 'Select State', false, url);
    } else { }
});
$(document).on('change', '#StateID', function () {
    var selectedItem = $(this).val();
    if (parseInt($('#CountryID').val()) >= 0) {
        var url = "/Employee/GetCityByStateId?stateId=" + selectedItem;
        FilterDropDown('#StateID', '#CityID', 'Select City', false, url);
    } else { ShowMessage("warning", "Plese select country"); }
});
$(document).on('change', '#CityID', function () {
    var selectedItem = $(this).val();
    if (parseInt($('#StateID').val()) >= 0) {
        var url = "/Employee/GetAreaByCityId?cityId=" + selectedItem;
        FilterDropDown('#CityID', '#AreaId', 'Select Area', false, url);
    } else {
        ShowMessage("warning", "Plese select city");
    }
});

function FilterDropDown(sourceSelector, targetedSelector, selectedText, onlyText, url) {
    $.ajax({
        cache: false,
        dataType: 'json',
        type: 'GET',
        async: false,
        url: url,
        success: function (data) {
            $(targetedSelector).html('');
            $(targetedSelector).append($('<option></option>').val('').html(selectedText));
            if (onlyText) {
                $.each(data, function (ind, item) {
                    //text = text + '<option value="' + item.Text + '">' + item.Text + '</option>';
                    $(targetedSelector).append($('<option></option>').val(item.id).html(item.name));
                });

            } else {
                $.each(data, function (ind, item) {
                    //text = text + '<option value="' + item.Value + '">' + item.Text + '</option>';
                    $(targetedSelector).append($('<option></option>').val(item.id).html(item.name));
                });
            }
            RefreshSelect(targetedSelector);
        },
        error: function (data) {
            ShowMessage("error", "Some technical error occurred");
        }
    });
}
function AddAddressType(title, id, dropdownId, obj) {
    $("#hdnDropDownReloadId").val(dropdownId);
    $("#hdnDropDownId").val(id);
    $("#modal_Loader1").load("/Employee/AddType/" + id);
    $("#myModal1").modal("show");
    $("#modal_heading1").text(title);
}
function AddState() {
    var countryID = $('#CountryID').val();
    if (parseInt(countryID) >= 0) {
        var url = "/Employee/AddState?id=" + countryID;
        addDifferenceType('Add State', 19, '#StateID', url);
    } else
        ShowMessage("warning", "Plese select country");
}

function AddCity() {
    var countryID = $('#CountryID').val();
    var stateID = $('#StateID').val();
    if (parseInt(countryID) >= 0) {
        if (parseInt(stateID) >= 0) {
            var url = "/Employee/AddCity?id=" + stateID + "&CountryID=" + countryID;
            addDifferenceType('Add City', 20, '#CityID', url);
        } else { ShowMessage("warning", "Plese select state"); }
    } else
        ShowMessage("warning", "Plese select country");
}

function AddArea() {
    var cityID = $('#CityID').val();
    if (parseInt(cityID) >= 0) {
        var url = "/Employee/AddArea?id=" + cityID;
        addDifferenceType('Add Area', 20, '#AreaId', url);
    } else
        ShowMessage("warning", "Plese select city");
}