﻿
$(document).on('click', '#btn_add', function () {
    document.body.style.minHeight = "700px";
    EditChannel(0);
});

$(document).on('click', '.close', function () {
    $("body").css("min-height", "auto");
});

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});

$("#btnTextSearch").click(function () {
    getGrid();
});


var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_EmployeeTraining').dataTable({
        "sAjaxSource": "/ProfessionalTraining/GetEmployeeTrainingList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "search", "value": searchval });
        },
        "aoColumns": [
            { "mData": "CourseTitleName", "sTitle": "Course Name", "width": "15%" },
             { "mData": "Trainee", "sTitle": "Trainer", "width": "15%" },
             { "mData": "Location", "sTitle": "Location", "width": "10%" },
             { "mData": "CountryName", "sTitle": "Country Name", "width": "10%" },
             { "mData": "CityName", "sTitle": "City Name", "width": "10%" },
              { "mData": "Cost", "sTitle": "Cost", "sClass": "ClsPrice", "width": "8%" },
            { "mData": "Deductable", "sTitle": "Deductable", "bSortable": false, "width": "9%", "sClsss": "text-center" },
            { "mData": "isComplete", "bSortable": false, "sTitle": "Completed", "width": "9%", "sClsss": "text-center" },
        { "mData": "Actions", "sTitle": "Actions", "bSortable": false, "sClass": "ClsAction", "width": "14%" }

        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/ProfessionalTraining/GetEmployeeTrainingList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bAutoWidth": false,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_EmployeeTraining");
        },
        "bSortCellsTop": true
    });


}

function EditChannel(id) {
    var buttonName = id > 0 ? "Update" : "Save";
    $("#modal_Loader3").load("/ProfessionalTraining/AddEmployeeTraining/" + id, function () {

        if (id == 0) {
            $("#modal_heading3").text('Add Professional Training');
        }
        else {
            $("#modal_heading3").text('Edit Professional Training');
        }
        $(".modal-dialog").attr("style", "width:700px;");
        $("#EmployeeTrainingForm").find('input[type="submit"]').val(buttonName);
        bindPositiveOnly('.numberType');
        $("#myModal3").modal("show");
    });
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { Id: id },
            url: '/ProfessionalTraining/DeleteEmployeeTraining',
            success: function (data) {
                getGrid();
                ShowMessage("success", "Record deleted successfully");
            },
            error: function (data) { }
        });
    });
}



function ViewChannel(id) {
    $("#modal_Loader").load("/ProfessionalTraining/ViewTrainingDetails?Id=" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('View Employee Training');
        $(".modal-dialog").attr("style", "width:700px;");
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ValidateForm() {

}

function onFailure(xhr, status, error) {

    console.log("xhr", xhr);
    console.log("status", status);

    // TODO: make me pretty
    ShowMessage("warning", xhr.responseText.Message);
}

function AddNewCourse() {

    //$("#hdnDropDownReloadId").val(dropdownId);
    //$("#hdnDropDownId").val(id);
    //$("#modal_Loader2").load("/Employee/AddType/" + id);
    //$("#myModal2").modal("show");
    //$("#modal_heading2").text(title);


    $("#modal_Loader2").html("");
    $("#modal_Loader2").load("/ProfessionalTraining/ViewCourse");
    $("#myModal2").modal("show");
    $("#modal_heading2").text('Add Course');


    //$("#CourseModal_Loader").html("");
    //$("#CourseModal_Loader").load("/ProfessionalTraining/_AddCourse");
    //$("#CourseModal").modal("show");
    //$("#CourseModal_heading").text('Add Course');
}

function getCheckedBoxes(EmployeeTrainingId) {
    var checkboxesChecked = [];
    $("input:checkbox[name=EmployeeChkBox]:checked").each(function () {
        checkboxesChecked.push($(this).attr("id"));
    });


    AddEmployeeListTOTraining(checkboxesChecked, EmployeeTrainingId);
}



function AddEmployeeListTOTraining(checkboxesChecked, EmployeeTrainingId) {
    var postData = { EmployeeList: checkboxesChecked, EmployeeTrainingId: EmployeeTrainingId };
    $.ajax({
        type: "POST",
        url: "/ProfessionalTraining/UpdateDepartmentEmployeeByEmployeeId",
        data: postData,
        success: function (data) {
            //ShowMessage("success", "Employee Training Added successfully.");

        },
        dataType: "json",
        traditional: true
    });

}

function AddCity(title, id, dropdownId) {
    if ($('#CountryId').val() != undefined && $('#CountryId').val() != "" && $('#CountryId').val() != "0") {
        $("#hdnDropDownReloadId").val(dropdownId);
        $("#hdnDropDownId").val(id);
        $("#modal_Loader").load("/Employee/AddNewCityForCountry?CountryID=" + $('#CountryId').val());
        $("#myModal").modal("show");
        $("#modal_heading").text(title);
    }
    else {
        $.MessageBox({
            buttonDone: "Ok",
            message: 'Please select Country First',
        });
        return;
    }
}

function onSuccess(data, status, xhr) {

    console.log("data", data);
    console.log("status", status);
    console.log("xhr", xhr);

    if (data.Success == true) {

        //var checkedBoxes = getCheckedBoxes(data.InsertedRowId);
        $("#myModal3").modal("hide");
        ShowMessage("success", data.Message);
        getGrid();
    }
    else {
        ShowMessage("warning", data.Message);
    }
}


function trainingDetails(EmployeeId) {
    $("#TrainingModal_Loader").html("");
    $("#TrainingModal_Loader").load("/ProfessionalTraining/ViewTrainingDetails/" + EmployeeId);
    $("#TrainingModal").modal("show");
    $("#TrainingModal_heading").text('Training Details');
}

function BindCourse() {

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: {},
        url: '/ProfessionalTraining/GetCourse',
        success: function (refData) {

            var listB = $('#CourseTitle');
            listB.empty();
            listB.append(
                    $('<option>', {
                        value: 0,
                        text: 'Select Course'
                    }, '</option>'))
            $.each(refData, function (index, item) {
                listB.append(
                    $('<option>', {
                        value: item.CourseTitle,
                        text: item.CourseTitleName
                    }, '</option>'))
            });
            RefreshSelect('#CourseTitle');
        },
        error: function (refData) { }
    });
}

