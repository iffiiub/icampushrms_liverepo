﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridWorkPermit();
    }
});

var oTableChannel;

function getGridWorkPermit() {
    oTableChannel = $('#tbl_WorkPermitDetails').dataTable({
        "sAjaxSource": "/WorkPermit/GetWorkPermitList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document Name/No", 'width': '20%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place", 'width': '20%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '15%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction", "bSortable": false, 'width': '20%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/WorkPermit/GetWorkPermitList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_EmiratesList");
        }
    });
}

$("#btn_ExportToExcel").click(function () { ExportToExcelEmirates(); });
$("#btn_ExportToPdf").click(function () { ExportToPdfEmirates(); });

function AddWorkPermit() {
    //alert($("#hdnEmployee_ID").val());
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/WorkPermit/AddWorkPermit");
                $("#myModal3").modal({ backdrop: 'static' });
                $("#modal_heading3").text('Add Work Permit');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditWorkPermit(id, DocTypeName) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/WorkPermit/EditWorkPermit/" + id);
    $("#myModal3").modal({ backdrop: 'static' });
    $("#modal_heading3").text('Edit Work Permit');

}


function DeleteWorkPermit(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/WorkPermit/DeleteWorkPermit',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridWorkPermit();
            },
            error: function (data) { }
        });
    });
}



function ViewWorkPermit(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/WorkPermit/ViewWorkPermit/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View Work Permit Details');

}

