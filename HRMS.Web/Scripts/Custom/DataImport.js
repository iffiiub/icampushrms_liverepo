﻿$(document).ready(function () {

});

$(document).on("click", "#fu_ExcelFile", function (event) {
    if ($("#ddlImportType").val() == "" || $("#ddlImportType").val() == undefined) {
        ShowMessage("error", "Please, Select data import type");
        return false;
    }
});

$(document).on("change", "#fu_ExcelFile", function (event) {
    var validExtensions = ['xls', 'xlsx'];
    var file = this.files[0];
    fileName = file.name;
    size = file.size;
    type = file.type;
    $("#DataImport").empty();
    var fileNameExt = file.name.substr(fileName.lastIndexOf('.') + 1);
    $("#txtuploadedMsgAdd_ExcelFile").css("color", "red");
    if ($("#ddlImportType").val() == "" || $("#ddlImportType").val() == undefined) {
        ShowMessage("error", "Please, Select data import type");
        return;
    }
    if ($.inArray(fileNameExt.toLowerCase(), validExtensions) != -1) {
        if (size <= 2097152) {
            $("#txtuploadedMsgAdd_ExcelFile").text("");
            $("#txtuploadedMsgAdd_ExcelFile").hide();
            $("#divBrowse_ExcelFile").hide();
            $("#btnRemove_ExcelFile").show();
            $("#file_caption_name_ExcelFile").show();
            $("#file_caption_id_ExcelFile").html(fileName.substring(0, 15));
            var fileJD = URL.createObjectURL(event.target.files[0]);
            $('#divPreview_ExcelFile').append('<a href="' + fileJD + '" target="_blank">' + event.target.files[0].name + '</a>');

            PreviewImportExcel();
        }
        else {
            $("#txtuploadedMsgAdd_ExcelFile").show();
            $("#txtuploadedMsgAdd_ExcelFile").text("Maximum 2MB file size is allowed to upload.");
        }
    }
    else {
        $("#txtuploadedMsgAdd_ExcelFile").show();
        $("#txtuploadedMsgAdd_ExcelFile").text("Only xls and xlsx extension files are allowed to upload.");
    }
});

$(document).on("change", "#ddlImportType", function () {

    $("#DataImport").empty();
    RemoveDocument();
});

$(document).on("click", "#btnDownloadTemplate", function () {
    if ($("#ddlImportType").val() == undefined || $("#ddlImportType").val() == "") {
        ShowMessage("error", "Please select data import type.");
        return;
    }
    if ($("#ddlImportType").val() != "") {
        $.ajax({
            type: "POST",
            data: { DataImportTypeId: $("#ddlImportType").val() },
            datatype: "json",
            url: '/DataImport/GetDataImportFileTemplatePath',
            success: function (data) {
                window.location.assign('/DataImport/DownloadExcel?FileName=' + data.FileName + '&DataImportTypeId=' + $("#ddlImportType").val())
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }
});

function RemoveDocument() {
    $("#btnRemove_ExcelFile").hide();
    $("#divBrowse_ExcelFile").show();
    $("#file_caption_name_ExcelFile").hide();
    $("#file_caption_id_ExcelFile").html('');
    $("#txtuploadedMsgAdd_ExcelFile").text("");
    $("#fu_ExcelFile").val("");
    $("#divPreview_ExcelFile").html("");
    $("#DataImport").empty();
}

function PreviewImportExcel() {
    var importExcelFile;
    if ($("#fu_ExcelFile").get(0).files.length > 0) {
        importExcelFile = $("#fu_ExcelFile").get(0).files[0];
    }
    else {
        ShowMessage("error", "Please, Upload file");
        return;
    }


    var formData = new FormData();
    formData.append("file", importExcelFile);
    formData.append("DataImportTypeId", $("#ddlImportType").val());
    pageLoaderFrame();
    $.ajax({
        contentType: false,
        processData: false,
        type: "POST",
        data: formData,
        datatype: "json",
        url: '../DataImport/UploadFile',
        success: function (data) {
            if (data.result == undefined) {
                $("#DataImport").html(data);
                var dataImportType = $("#ddlImportType").val();
                if (dataImportType == "1") {
                    var tblPayAdditionImportExcel = new $('#tblPayAdditionImportExcel').dataTable({
                        columnDefs: [
                            { width: "15%", targets: [0, 3] },
                            { width: "20%", targets: [2] },
                            { width: "25%", targets: [1, 4] }
                        ],
                        destroy: true

                    });
                }
                else if (dataImportType == "2") {
                    var tblInsuranceImportExcel = new $('#tblInsuranceImportExcel').dataTable({
                        columnDefs: [
                            { width: "15%", targets: [0, 3] },
                            { width: "20%", targets: [2] },
                            { width: "25%", targets: [1, 4] }
                        ],
                        destroy: true

                    });
                }
                else if (dataImportType == "3") {
                    var tblAccommodationImportExcel = new $('#tblAccommodationImportExcel').dataTable({
                        //columnDefs: [
                        //    { width: "15%", targets: [0, 3] },
                        //    { width: "15%", targets: [2] },
                        //    { width: "30%", targets: [1, 4] }
                        //],
                        destroy: true

                    });
                }

                hideLoaderFrame();
            }
            else {
                ShowMessage("error", data.Message);
                hideLoaderFrame();
            }
        },
        error: function (err) {
            hideLoaderFrame();
            ShowMessage('error', err.statusText);
        }
    });
}

$(document).on("click", "#btnSaveImportExcel", function () {
    if ($("#IsValidData").val() == "False") {
        ShowMessage("error", "The highlighted data is in incorrect format, Please re-upload file after correct the data format.");
        return;
    }

    if ($("#ddlImportType").val() == "" || $("#ddlImportType").val() == undefined) {
        ShowMessage("error", "Please select data import type.");
        return;
    }

    var ImportExcelData = new Array();
    if ($("#ddlImportType").val() == 1) {
        var rows = $('#tblPayAdditionImportExcel').DataTable().rows().nodes();
        if (rows.length > 0) {
            $(rows).each(function (i, item) {
                var PaySalaryAllowanceID = $(item).attr("PaySalaryAllowanceId");
                var EmployeeID = $(item).attr("EmployeeId");
                var AdditionDate = $(item).attr("AdditionDate");
                var Amount = $(item).attr("Amount");
                var Description = $(item).attr("Description");

                var PayAdditionDetails = {};
                if (PaySalaryAllowanceID != undefined && PaySalaryAllowanceID > 0 && EmployeeID != undefined && EmployeeID > 0 && AdditionDate != undefined && AdditionDate != "" &&
                    Amount != undefined && Amount != "") {
                    PayAdditionDetails.PaySalaryAllowanceID = PaySalaryAllowanceID;
                    PayAdditionDetails.EmployeeID = EmployeeID;
                    PayAdditionDetails.AdditionDate = AdditionDate.toString();
                    PayAdditionDetails.Amount = Amount.toString();
                    PayAdditionDetails.Description = Description;
                    ImportExcelData.push(PayAdditionDetails);
                }
            });
        }
        else {
            ShowMessage("error", "Please upload file data before save.");
            return;
        }

        $.ajax({
            type: "POST",
            data: { PayAdditions: JSON.stringify(ImportExcelData), DataImportID: $("#DataImportId").val() },
            datatype: "json",
            url: '/DataImport/ImportExcelToPayAddition',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    RemoveDocument();
                }
                else {
                    ShowMessage("error", data.Message);
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }
    else if ($("#ddlImportType").val() == 2) {
        var rows = $('#tblInsuranceImportExcel').DataTable().rows().nodes();
        if (rows.length > 0) {
            $(rows).each(function (i, item) {
                var InsuranceCategory = $(item).attr("insuranceCategoryId");
                var InsuranceEligibility = $(item).attr("insuranceEligibiltyId");
                var CardNumber = $(item).attr("cardNumber");
                var PolicyNumber = $(item).attr("policyNumber");
                var InsuranceAmount = $(item).attr("insuranceAmount");
                var AmountPaidBySchool = $(item).attr("amountpaidBySchool");
                var EmployeeID = $(item).attr("EmployeeId");
                var ExpiryDate = $(item).attr("expiryDate");
                var IssueDate = $(item).attr("issueDate");
                var CancellationDate = $(item).attr("cancellationDate");
                var Note = $(item).attr("Note");

                var InsuranceDetails = {};
                if (InsuranceCategory != undefined && InsuranceCategory > 0 && InsuranceEligibility != undefined && InsuranceEligibility > 0
                    && CardNumber != undefined && CardNumber > 0 && PolicyNumber != undefined && PolicyNumber > 0
                    && InsuranceAmount != undefined && InsuranceAmount > 0 && AmountPaidBySchool != undefined && AmountPaidBySchool > 0
                    && EmployeeID != undefined && EmployeeID > 0 && ExpiryDate != undefined && ExpiryDate != ""
                    && IssueDate != undefined && IssueDate != "") {
                    InsuranceDetails.InsuranceType = InsuranceCategory;
                    InsuranceDetails.InsuranceEligibility = InsuranceEligibility;
                    InsuranceDetails.CardNumber = CardNumber;
                    InsuranceDetails.PolicyNumber = PolicyNumber;
                    InsuranceDetails.TotalAmount = InsuranceAmount;
                    InsuranceDetails.TotalAmountBySchool = AmountPaidBySchool;
                    InsuranceDetails.EmployeeID = EmployeeID;
                    InsuranceDetails.ExpiryDate = ExpiryDate;
                    InsuranceDetails.IssueDate = IssueDate;
                    InsuranceDetails.CancellationDate = CancellationDate;
                    InsuranceDetails.Note = Note;
                    ImportExcelData.push(InsuranceDetails);
                }
            });
        }
        else {
            ShowMessage("error", "Please upload file data before save.");
            return;
        }

        $.ajax({
            type: "POST",
            data: { Insurances: JSON.stringify(ImportExcelData), DataImportID: $("#DataImportId").val() },
            datatype: "json",
            url: '/DataImport/ImportExcelToInsurance',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    RemoveDocument();
                }
                else {
                    ShowMessage("error", data.Message);
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }
    else if ($("#ddlImportType").val() == 3) {
        var rows = $('#tblAccommodationImportExcel').DataTable().rows().nodes();
        if (rows.length > 0) {
            $(rows).each(function (i, item) {
                var HRAccommodationTypeID = $(item).attr("type");
                var Appartment = $(item).attr("ApartmentNo");
                var CountryID = $(item).attr("Country");
                var CityID = $(item).attr("City");
                var Building = $(item).attr("Building");
                var BuildingFloor = $(item).attr("Floor");
                var Street = $(item).attr("Street");
                var EmployeeID = $(item).attr("EmployeeId");

                var AllowanceDetails = {};
                AllowanceDetails.HRAccommodationTypeID = HRAccommodationTypeID;
                AllowanceDetails.Appartment = Appartment;
                AllowanceDetails.CountryID = CountryID;
                AllowanceDetails.CityID = CityID;
                AllowanceDetails.Building = Building;
                AllowanceDetails.BuildingFloor = BuildingFloor;
                AllowanceDetails.Street = Street;
                AllowanceDetails.EmployeeID = EmployeeID;
                ImportExcelData.push(AllowanceDetails);
            });
        }
        else {
            ShowMessage("error", "Please upload file data before save.");
            return;
        }

        $.ajax({
            type: "POST",
            data: { Allowance: JSON.stringify(ImportExcelData), DataImportID: $("#DataImportId").val() },
            datatype: "json",
            url: '/DataImport/ImportExcelToAccommodation',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    RemoveDocument();
                }
                else {
                    ShowMessage("error", data.Message);
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }
    else if ($("#ddlImportType").val() == 4) {
        var rows = $('#tblAirfareImportExcel').DataTable().rows().nodes();
        if (rows.length > 0) {
            $(rows).each(function (i, item) {
                var AirTicketCountry = $(item).attr("AirTicketCountry");
                var AirTicketCity = $(item).attr("AirTicketCity");
                var AirTicketClass = $(item).attr("AirTicketClass");
                var Airport = $(item).attr("Airport");
                var AirTicketAmount = $(item).attr("AirTicketAmount");
                var AirfareFrequency = $(item).attr("AirfareFrequency");
                var NoOfDependencies = $(item).attr("NoOfDependencies");
                var AirTicketAmountDependannt = $(item).attr("AirTicketAmountDependannt");
                var TicketClassForDependant = $(item).attr("TicketClassForDependant");
                var EmployeeID = $(item).attr("EmployeeId");

                var AirfareDetails = {};
                AirfareDetails.AirTicketCountryId = AirTicketCountry;
                AirfareDetails.AirTicketCityId = AirTicketCity;
                AirfareDetails.AirTicketClassId = AirTicketClass;
                AirfareDetails.AirTicketAmount = AirTicketAmount;
                AirfareDetails.NoOfDependants = NoOfDependencies;
                AirfareDetails.AirTicketDependantClassId = TicketClassForDependant;
                AirfareDetails.AirTicketDependantAmount = AirTicketAmountDependannt;
                AirfareDetails.EmployeeAirfareID = EmployeeID;
                AirfareDetails.AirfareFrequencyID = AirfareFrequency;
                AirfareDetails.AirportListID = Airport;
                ImportExcelData.push(AirfareDetails);
            });
        }
        else {
            ShowMessage("error", "Please upload file data before save.");
            return;
        }

        $.ajax({
            type: "POST",
            data: { Airfare: JSON.stringify(ImportExcelData), DataImportID: $("#DataImportId").val() },
            datatype: "json",
            url: '/DataImport/ImportExcelToAirfare',
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);
                    RemoveDocument();
                }
                else {
                    ShowMessage("error", data.Message);
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
            }
        });
    }
});