﻿$(document).ready(function () {
    getGrid();
    $("#btn_add").click(function () {
        EditChannel(0);
    });
});

$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {
    getGrid();
});

var oTableChannel;

function getGrid() {
    var isView = $("#hdnIsViewPermission").val();
    var isUpdate = $("#hdnIsUpdatePermission").val();
    var isDelete = $("#hdnIsDeletePermission").val();
    
     {
        var searchval = $("#txtSearch").val();
        oTableChannel = $('#tbl_VactionType').DataTable({
            "sAjaxSource": "/VacationType/GetVacationTypeList",
            "aoColumns": [

                { "mData": "Id", "bVisible": false, "sTitle": "Leave Type Id" },
                { "mData": "VacationTypeName", "sTitle": "Leave Type" },
                { "mData": "Term", "sTitle": "Term" },
                { "mData": "FullPaidDays", "sTitle": "Full Paid Days" },
                { "mData": "HalfPaidDays", "sTitle": "Half Paid Days" },
                { "mData": "UnPaidDays", "sTitle": "Un Paid Days" },
                //{ "mData": "PaidType", "sTitle": "Paid" },
                { "mData": "ApplicableAfterDigits", "sTitle": "Applicable After" },
                { "mData": "ApplicableAfterTypeID", "sTitle": "Unit" },
                { "mData": "GenderName_1", "sTitle": "Gender" },
                { "mData": "ReligionName_1", "sTitle": "Religion" },
                { "mData": "NationalityName_1", "sTitle": "Nationality" },
            // { "mData": "AnnualLeave", "sTitle": "Annual<br/>Leave" },
            //{ "mData": "LifetimeVacation", "sTitle": "Lifetime<br/>Leave" },
            //{ "mData": "MonthlySplit", "sTitle": "Monthly<br/>Split" },
            { "mData": "Action", "sTitle": "Actions", "bSortable": false, "sClass": "ClsAction" }
            ],
            "processing": true,
            "serverSide": false,
            "ajax": "/VacationType/GetVacationTypeList",
            "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
            "iDisplayLength": 10,
            "bDestroy": true,
            "bFilter": true,
            "bInfo": true,
            "order": [[1, "asc"]],
            "fnDrawCallback": function (settings) {
                if (isUpdate == "False" && isDelete == "False" && isView=="False")
                {
                    $('#tbl_VactionType').DataTable().columns([10]).visible(false);
                }
            },
            "bSortCellsTop": true
        });
    }
   
   
}

function AddLeaveType() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/VacationType/AddLeaveType");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Leave Type');
}


function EditChannel(id) {

    var buttonName = id > 0 ? "Update" : "Save";

    $("#modal_Loader").load("/VacationType/Edit/" + id, function () {
        $("#myModal").modal("show");
        bindPositiveOnly('.numberOnly');
        bindSelectpicker('.selectpickerddl');
        if (id == 0) {

            $("#modal_heading").text('Add Leave Type');
        }
        else {

            $("#modal_heading").text('Edit Leave Type');

        }
        $(".modal-dialog").attr("style", "width:1000px;");
        $("#form0").find('input[type="submit"]').val(buttonName);
    });
}


function Start() {
    blockUiById("vacationTypeAddEditPannel");
}

function Saveall() {    
    pageLoadingFrame("show");
    var $form = $('form#frmEditVactionType');
    var isChecked = $("#isAccumulatedLeave").is(":checked");
    if (isChecked) {
        $("#AccumulateDetails").removeClass("hidden");
        var vacationTypeID = $("#VacationTypeId").val();
        var PositionIds = getSelectedIds('.position').join(',');
        var Jobtitleids = getSelectedIds('.jobtitle').join(',');
        var Deparmentids = getSelectedIds('.department').join(',');
        var Jobcategoryids = getSelectedIds('.jobcategory').join(',');    
       // if (PositionIds == '' && Jobtitleids == '' && Deparmentids == '' && Jobcategoryids == '') {
         if (PositionIds != '' || Jobtitleids != '' || Deparmentids != '' || Jobcategoryids != '') {
            $.ajax({
                type: 'POST',
                url: '/VacationType/GetExistingPositionInAccrual/',
                data: { PositionIds: PositionIds, VacationTypeID: vacationTypeID },
                success: function (data) {
                    if (data != "") {
                        customShowMessage("error", 'Following positions ' + data + ' are already assigned to another accural leave.', 50000, "left");
                        pageLoadingFrame("hide");
                    }
                    else {
                        if ($form.valid()) {
                            $form.submit();                          
                        }
                        if ($("#Name").val() == "") {
                            pageLoadingFrame("hide");
                        }
                    }
                },
                error: function (data) { }
            });
        }
        else {      
            customShowMessage("error", "Accrual leave can not be saved without selecting atleast one Position , Job Title , Department or  Job Category.", 5000, "center");           
            pageLoadingFrame("hide");
        }
    }
    else {       
        if ($form.valid()) {
            $form.submit();           
        }
        if ($("#Name").val() == "") {
            pageLoadingFrame("hide");
        }
    }
}
function SucessLeaveType(arg) {
    //$.unblockUI();
    //unBlockUiById("vacationTypeAddEditPannel");
    try {
        if (arg.result == 'success')
        {
            $("#myModal").modal("hide");
        }
        pageLoadingFrame("hide");
        ShowMessage(arg.result, arg.resultMessage);
    } catch (e) { }   
    getGrid();
}



function EditLeaveType(id) {
    var buttonName = id > 0 ? "Update" : "Submit";

    //location.href = "/VacationType/EditLeaveType/" + id;
    $("#modal_Loader").load("/VacationType/Edit/" + id, function () {
        bindSelectpicker('.selectpickerddl');
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Leave Type');
        $(".modal-dialog").attr("style", "width:1000px;");
        $("#form0").find('input[type="submit"]').val(buttonName);
    });
}


function DeleteVacationType(id) {

    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/VacationType/DeleteVacationType',
            success: function (data) {
                getGrid();
                ShowMessage(data.result, data.resultMessage);
            },
            error: function (data) {
                ShowMessage("error", data.resultMessage);
            }
        });
    });
}
//}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewLeaveType(id) {
    //location.href = "/VacationType/ViewLeaveType/" + id;
    $("#modal_Loader").load("/VacationType/ViewVacationType/" + id);
    $("#myModal").modal("show");
    $(".modal-dialog").attr("style", "width:1000px;");
    $("#modal_heading").text('View Leave Type Details');

}


//function ExportToExcel() {
//    if (oTableChannel.fnGetData().length > 0) {
//        window.location.assign("/Position/ExportToExcel");
//    }
//    else ShowMessage("warning", "No data for export!");
//}

//function ExportToPdf() {
//    if (oTableChannel.fnGetData().length > 0) {
//        window.location.assign("/Position/ExportToPdf");
//    }
//    else ShowMessage("warning", "No data for export!");
//}