﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridLabourContract();
    }
});

var oTableChannel;

function getGridLabourContract() {

    oTableChannel = $('#tbl_LabourContractList').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/LabourContract/GetLabourContractList",
        "aoColumns": [
            { "mData": "DocumentNo", "sTitle": "Document<br/>Name/No", 'width': '18%' },
            { "mData": "SponsorName", "sTitle": "Sponsor<br/>Name", 'width': '18%' },
            { "mData": "IssuePlace", "sTitle": "Issue Place",'width':'13%' },
            { "mData": "IssueDate", "sTitle": "Issue Date", 'width': '13%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", 'width': '13%', "sType": "date-uk" },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction vertical-Middle",'width':'15%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/LabourContract/GetLabourContractList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_LabourContractList");
        }
    });
}

function AddLabourContract() {
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                var EmpID = parseInt($("#hidSelectedEMP").val());
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/LabourContract/AddLabourContract?EmployeeID=" + EmpID);
                $("#myModal3").modal({backdrop: 'static'});
                $("#modal_heading3").text('Add Labour Contract');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });
}

function EditLabourContract(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/LabourContract/EditLabourContract/" + id);
    $("#myModal3").modal({backdrop: 'static'});
    $("#modal_heading3").text('Edit Labour Contract');
}


function DeleteLabourContract(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/LabourContract/DeleteLabourContract',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridLabourContract();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewLabourContract(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/LabourContract/ViewLabourContract/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('View Labour Contract Details');

}

function ExportToExcelLabourContract() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/LabourContract/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdfLabourContract() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/LabourContract/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}