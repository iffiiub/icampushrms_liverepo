﻿var oTableGeneralAccounts;

$(document).ready(function () {    
    getGrid();   
});

$("#btnSaveJvsSetting").click(function () {
    var isChecked = $("#chkisRunWithJvs").is(':checked');

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { isRunWithJvs: isChecked },
        url: '/GeneralAccounts/SaveJvsSetting',
        success: function (data) {
            ShowMessage(data.result, data.resultMessage);
        }
    });
});

function getGrid()
{
    var CategoryID = $("#ddlCategories").val();
    var AllwanceType = $("#ddlAllwanceType").val();
    var CycleId = $("#ddlCycle").val();
    var ShowMissingOnly = false;
    if ($('#chkShowMissing').is(":checked")) {
        ShowMissingOnly = true;
    }
    else {
        ShowMissingOnly = false;
    }
    if (CategoryID == 0)
    {
        CategoryID=null;
    }
    if (AllwanceType == 0)
    {
        AllwanceType = null;
    }
    if (CycleId == 0)
    {
        CycleId = null;
    }
    oTableGeneralAccounts = $('#tblGenralAccounting').DataTable({
        "sAjaxSource": "/GeneralAccounts/ViewGeneralAccounts",
        "aoColumns": [
          { "mData": "PayCategory", "sTitle": "Pay Category", "bSortable": true, "sWidth": "20%", "sClass": "left-text-align" },
          { "mData": "Type", "sTitle": "Type", "sWidth": "20%" },
          { "mData": "AccountCode", "sTitle": "Account Code", "bSortable": false, "sWidth": "22%", "sClass": "left-text-align" },
          { "mData": "VacationAccountCode", "sTitle": "Vacation Account Code", "sWidth": "22%", "bSortable": false },
         // { "mData": "Active", "sTitle": "Active", "sWidth": "10%", "bSortable": false },
           { "mData": "Action", "sTitle": "Actions", "sWidth": "15%", "bSortable": false, "sClass": "center-text-align" },
          { "mData": "AccountCodeId", "bVisible": true, "sClass": "hidden" },
          { "mData": "VacationCodeId", "bVisible": true, "sClass": "hidden" },
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "CategoryID", "value": CategoryID }, { "name": "AllwanceType", "value": AllwanceType }, { "name": "ShowMissingOnly", "value": ShowMissingOnly }, { "name": "CycleId", "value": CycleId });
        },
        "processing": false,
        "serverSide": false,
        'iDisplayLength': 10,
        "bPaginate": true,
        //"bLengthChange": false,
        "ajax": "/GeneralAccounts/ViewGeneralAccounts",
        "aLengthMenu": [[10, 25, 50], [10, 25, 50]],
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "createdRow": function (row, data, index) {
            // ($('td', row).eq(2).html() == "")
            //{
            //    $('td', row).eq(2).html("<select onchange =\"editAccoutCode(this)\" class='form-control selectpickerddl ddlAccountCode' id='ddlAccountCode' > " + $("#ddlAccountCodes").text() + " </select>");
            //}
            //if($('td', row).eq(3).html() == "")
            //{
            //    $('td', row).eq(3).html("<select onchange =\"editVacationAccoutCode(this)\" class='form-control selectpickerddl ddlVacationAccountCode' id='ddlVacationAccountCode' > " + $("#ddlAccountCodes").text() + " </select>");
            //}
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var AccountCodeId = aData["AccountCodeId"];
            var select = $(nRow).find(".ddlAccountCode");
            $(select).val(AccountCodeId);// .attr("selected", "selected");  

            var VacationCodeId = aData["VacationCodeId"];
            var select1 = $(nRow).find(".ddlVacationAccountCode");
            $(select1).val(VacationCodeId);// .attr("selected", "selected");    
        },
        "fnDrawCallback": function () {
            //  bindSelectpicker('#tblGenralAccounting .selectpickerddl');
            $('#tblGenralAccounting .selectpickerddl').selectpicker();
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('GeneralAccountPageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('GeneralAccountPageNo'));
        }
    });

}

function changeStatus(e, id, allowanceType, autoID)
{   
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { id: id, allowanceType: allowanceType },
        url: '/GeneralAccounts/DeleteAccountDetails',
        success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                //window.setTimeout(function () { location.reload() }, 1000)
                getGrid();
            }
    });
}

function edit(e, allowanceType, id, payCategoriesId, allowanceId, autoID) {
    var closesetRow = $(e).closest('tr');
    $(closesetRow).find('td:eq(2)').html("<select data-live-search='true'  class='form-control selectpickerddl ddlAccountCode' id='ddlAccountCode_"+autoID+"' > " + $("#ddlAccountCodes").text() + " </select>");
    $(closesetRow).find('td:eq(3)').html("<select data-live-search='true' class='form-control selectpickerddl ddlVacationAccountCode' id='ddlVacationAccountCode_" + autoID + "'> " + $("#ddlAccountCodes").text() + " </select>");
    $(closesetRow).find('td:eq(4)').html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveAccountDetails(' + allowanceType + ',' + id + ',' + payCategoriesId + ',' + allowanceId + ',' + autoID + ')" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this,' + allowanceType + ',' + id + ',' + payCategoriesId + ',' + allowanceId + ',' + autoID + ')" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button></div>');
    var AccountCodeId = $(closesetRow).find('td:eq(5)').text();
    var select = $(closesetRow).find(".ddlAccountCode");
    $(select).val(AccountCodeId);// .attr("selected", "selected");  

    var VacationCodeId = $(closesetRow).find('td:eq(6)').text();
    var select1 = $(closesetRow).find(".ddlVacationAccountCode");
    $(select1).val(VacationCodeId);// .attr("selected", "selected"); 
    $('#tblGenralAccounting .selectpickerddl').selectpicker();
}
function CancelNewRow(e, allowanceType, id, payCategoriesId, allowanceId, autoID)
{
    var closesetRow = $(e).closest('tr');
    var AccountCodeId = $(closesetRow).find('td:eq(5)').text();
    var VacationAccountCodeId = $(closesetRow).find('td:eq(6)').text();
    var Accountcode = "";
    if (AccountCodeId != "0")
    {
        Accountcode = $("#ddlAccountCode_" + autoID + " option[value='" + AccountCodeId + "']").text();       
    }

    var VacationAccountCode = "";
    if (VacationAccountCodeId != "0")
    {    
       VacationAccountCode = $("#ddlVacationAccountCode_" + autoID + " option[value='" + VacationAccountCodeId + "']").text();
    }
    
    $(closesetRow).find('td:eq(2)').html(Accountcode);
    $(closesetRow).find('td:eq(3)').html(VacationAccountCode);
    $(closesetRow).find('td:eq(4)').html("<a class='btn btn-success btn-rounded btn-condensed btn-sm editButton' id=\"btnEdit\" data-id=" + autoID + " title='Edit' onclick=\"edit(this," + allowanceType + "," + id + "," + payCategoriesId + "," + allowanceId + "," +autoID + ")\"><i class='fa fa-pencil'></i> </a>"+
                                         "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"changeStatus(this," + id + "," + allowanceType + "," + autoID + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
}

function saveAccountDetails(allowanceType, id, payCategoriesId, allowanceId, autoID)
{
    var AccountCodeIdval = $("#ddlAccountCode_"+autoID).val();
    var AccountCodeId = parseInt(AccountCodeIdval);

    var VacationAccountCodeIdval = $("#ddlVacationAccountCode_" + autoID).val();
    var VacationAccountCodeId = parseInt(VacationAccountCodeIdval);
    if (AccountCodeId == 0) {
        ShowMessage("error", "Please select account code.");
    }
    
    else {      
        
            $.ajax({
                dataType: 'json',
                type: 'POST',
                data: { AccountCodeIdval: AccountCodeId, VacationAccountCodeId: VacationAccountCodeId, allowanceType: allowanceType, id: id, payCategoryId: payCategoriesId, allowanceId: allowanceId },
                url: '/GeneralAccounts/AddUpdateAccountCode',
                success: function (data) {
                    ShowMessage(data.result, data.resultMessage);
                    //  window.setTimeout(function () { location.reload() }, 1000)
                    getGrid();
                }
            });
        
    }
    

}
