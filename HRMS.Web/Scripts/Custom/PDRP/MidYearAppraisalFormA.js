﻿var selectedValues = [];
var CurrRatingScales = [];
function SavePage(e)
{
    delete (selectedValues);
	delete (CurrRatingScales);
    var Tweight = 0;
	var invalidKPI = 0;
	selectedValues = [];
	var businessbargetno = 1;
    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var businessTargetId = row.data("businesstargetid");
        var businessTargetDetails = row.find('.businesstarget').val();
        var weight = row.find('.weight').val();
		//*** F3
        if (weight != "") {
            Tweight = parseInt(Tweight) + parseInt(weight);
        }
        selectedValues.push({ BusinessTargetNo: businessbargetno, businessTargetId: businessTargetId, businessTargetDetails: businessTargetDetails, weight: weight });
        //*** F12
        if (weight == "0") {
            invalidKPI = 11;
        }
		if ((businessTargetDetails == "" && weight != "") || (businessTargetDetails != "" && weight == "")) {

			invalidKPI = 1;
		}
		businessbargetno = parseInt(businessbargetno) + 1;

        //if ((businessTargetDetails != "") && (weight != ""))
        //    selectedValues.push({ businessTargetId: businessTargetId, businessTargetDetails: businessTargetDetails, weight: weight });
    });
	//***F2
	CurrRatingScales = [];
	$(".RatingScales").each(function () {
		var row1 = $(this);
		var RatingScaleCharacter = row1.data("ratingscalecharacter");
		var RatingScaleNumber = row1.data("ratingscalenumber");
		var FinalScoreSlabMIN = row1.data("finalscoreslabmin");
		var FinalScoreSlabMAX = row1.data("finalscoreslabmax");
		var DefinitionName = row1.data("definitionname");
		var DefinitionDetails = row1.data("definitiondetails");

		CurrRatingScales.push({
			RatingScaleCharacter: RatingScaleCharacter, RatingScaleNumber: RatingScaleNumber, FinalScoreSlabMIN: FinalScoreSlabMIN,
			FinalScoreSlabMAX: FinalScoreSlabMAX, DefinitionName: DefinitionName, DefinitionDetails: DefinitionDetails
		});
	});
    //*** F12
	if (invalidKPI == 11) {
	    ShowMessage("error", "KPI Weight can not be Zero (0)");

	}
	else {
	    if ((Tweight > 100 || Tweight < 100 || invalidKPI == 1) && e != 0) {//*** F3
	        if (invalidKPI == 1) {
	            ShowMessage("error", "Invalid KPI");
	        }
	        else {
	            ShowMessage("error", "Total Weightage of KPIs should be 100%");
	        }
	    }
	    else {
	        //*** F3
	        var Validating = 0;
	        if (e != 0) {
	            var grade = $("#txtGrade").val();
	            //*** F24 LM Comment and Employee Comment
	            var IsLM = $("#IsLineManager").val();
	            var IsEmp = $("#IsEmployee").val();

	            var lineManagerEvaluationComments = $("#lineManagerEvaluationComments").val();
	            var TrainingNeeds = $("#TrainingNeeds").val();
	            var lineManagerMidYearPerformanceComments = $("#lineManagerMidYearPerformanceComments").val();
	            var employeeMidYearPerformanceComments = $("#employeeMidYearPerformanceComments").val();

	            if (grade == '' || grade == undefined) {
	                Validating = 1;
	            }
	            else if (selectedValues.length == 0) {

	                Validating = 2;
	            }
	                //*** F24 LM Comment and Employee Comment
	            else if (((lineManagerEvaluationComments == '' || TrainingNeeds == '' || lineManagerMidYearPerformanceComments == '') && IsLM == 1) || (employeeMidYearPerformanceComments == '' && IsEmp==1)) {
	                Validating = 3;
	            }
	        }

	        if (Validating == 1) {
	            $("#txtGrade-validationMsg").css("color", "red");
	            $("#txtGrade-validationMsg").text("This field is mandatory");
	            $("#txtGrade-validationMsg").show();
	        }
	        else if (Validating == 2) {
	            ShowMessage("error", "Please add business target details.");
	        }
	        else if (Validating == 3) {
	            ShowMessage("error", "Section 4:Mid Year Appraisal All fields are mandatory");
	        }
	        else {
	            $("#txtGrade-validationMsg").text("");
	            $("#txtGrade-validationMsg").hide();

	            //if (selectedValues.length > 0) {
	            pageLoaderFrame();
	            $.ajax({
	                url: "/PDRP/AnnualAppraisal/SaveAnnualMidFormCData",
	                type: 'POST',
	                data: { model: JSON.stringify(getBusinessTargetModeldata()), PerformanceGroupId: $("#PerformanceGroupId").val() },
	                success: function (result) {
	                    //*** Naresh 2020-02-27 Update successfully message was displayed 2 times, message was being displayed in each respective section e-0, e-3 etc
	                    //ShowMessage(result.CssClass, result.Message);
	                    selectedValues = [];
	                    if (result.Success) {
	                        if (e == 0) {
	                            ShowMessage(result.CssClass, result.Message);
	                            hideLoaderFrame();
	                            //*** Naresh 2020-03-03 Do not redirect to list page on save. 
	                            //if (result.Success) {
	                            //    setTimeout(function () { BackToTaskList(); }, 1000);
	                            //    hideLoaderFrame();
	                            //}
	                            //else {
	                            //    hideLoaderFrame();
	                            //}

	                        }
	                        else if (e == 3) {//*** F3 Reintialized

	                            var performanceGroupId = $("#PerformanceGroupId").val();
	                            var annualGoalSettingFormAId = $("#AnnualGoalSettingFormAId").val();
	                            var midyearformid = result.InsertedRowId;
	                            var employeeId = $("#EmployeeID").val();
	                            var formId = 38;
	                            var goalSettingId = $("#GoalSettingId").val();
	                            var comments = $("#txtComments").val();

	                            $.ajax({
	                                url: "/PDRP/AnnualAppraisal/Reintialized",
	                                type: 'POST',
	                                data: { MIdYearFormId: midyearformid, id: annualGoalSettingFormAId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
	                                success: function (result) {
	                                    ShowMessage(result.CssClass, result.Message);
	                                    selectedValues = [];
	                                    if (result.Success) {
	                                        $("#prebtnSubmit").removeClass("btnSubmit");
	                                        $("#prebtnSubmit").addClass("LMbtnSubmit");
	                                        setTimeout(function () { BackToTaskList(); }, 1000);
	                                        hideLoaderFrame();

	                                    }
	                                    else {
	                                        hideLoaderFrame();
	                                    }
	                                }
	                            });
	                        }
	                        else { //*** Submit
	                            //$("#FormMode").val(1) 
	                            var performanceGroupId = $("#PerformanceGroupId").val();
	                            var AnnualGoalSettingFormAId = $("#AnnualGoalSettingFormAId").val();
	                            var midyearformid = result.InsertedRowId;
	                            var employeeId = $("#EmployeeID").val();
	                            var formId = 38;
	                            var goalSettingId = $("#GoalSettingId").val();
	                            var comments = $("#txtComments").val();
	                            $.ajax({
	                                //url: "/PDRP/GoalSetting/ApproveMidForm",
	                                url: "/PDRP/AnnualAppraisal/ApproveMidForm",
	                                type: 'POST',
	                                data: { MIdYearFormId: midyearformid, id: AnnualGoalSettingFormAId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
	                                success: function (result) {
	                                    ShowMessage(result.CssClass, result.Message);
	                                    selectedValues = [];
	                                    if (result.Success) {
	                                        $("#prebtnSubmit").removeClass("btnSubmit");
	                                        $("#prebtnSubmit").addClass("LMbtnSubmit");
	                                        setTimeout(function () { BackToTaskList(); }, 1000);
	                                        hideLoaderFrame();

	                                    }
	                                    else {
	                                        hideLoaderFrame();
	                                    }
	                                }

	                            });
	                        }

	                    }
	                    else {
	                        ShowMessage(result.CssClass, result.Message);
	                        hideLoaderFrame();
	                    }
	                }

	            });
	            //}
	            //else
	            //    ShowMessage("error", "Please add business target details.");
	        }
	    }
	}
}
$(document).ready(function () {
    //$(document).on("click", "#LMMidbtnSave", function () {
    //    SavePage();

    //});

    $(document).on("click", "#prebtnSave, #tprebtnSave", function () {
        SavePage(0);
    });
    //*** Employee Submiting
    $(document).on("click", ".btnSubmit", function () {
        var isitEditable = $("#isEditable").val()
        if (($("#MidFormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(1);
        }
        else {
            //$("#FormMode").val(1) 
            var performanceGroupId = $("#PerformanceGroupId").val();
            var AnnualGoalSettingFormAId = $("#AnnualGoalSettingFormAId").val();

            var midyearformid = $("#ID").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 38;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                //url: "/PDRP/GoalSetting/ApproveMidForm",
                url: "/PDRP/AnnualAppraisal/ApproveMidForm",
                type: 'POST',
                data: { MIdYearFormId: midyearformid, id: AnnualGoalSettingFormAId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }

            });
        }
    });

    //*** F3
    $(document).on("click", ".btnReSubmit", function () {
        var isitEditable = $("#isEditable").val()
        if (($("#MidFormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(3);
        }
        else {

            var performanceGroupId = $("#PerformanceGroupId").val();
            var midyearformid = $("#ID").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 40;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/AnnualAppraisal/Reintialized",
                type: 'POST',
                data: { id: midyearformid, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {



                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }
            });

        }

    });

    $('.btnReject').click(function () {

        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();

                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/PDRP/AnnualAppraisal/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {

                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                            hideLoaderFrame();

                        }
                        else {
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.message);
                    }
                });
            });
        } else {
            ShowMessage('error', 'Comments field is mandatory.');
            return false;
        }
    });

    $(document).on("click", ".btnEmpSignOffSubmit", function () {
        var AnnualGoalSettingFormAId = $("#AnnualGoalSettingFormAId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 40;
        var requestId = $("#RequestId").val();
        var goalSettingId = $("#GoalSettingId").val();
        //*** F21 SignOff tEXT
        //*** F24 Need to show in Signing off also so remove this condition
        var comments = $("#txtComments").val();
        //var comments = "SignOff";
        //*** F24 LM Comment and Employee Comment
        var IsLM = $("#IsLineManager").val();
        var IsEmp = $("#IsEmployee").val();
        var employeeMidYearPerformanceComments = $("#employeeMidYearPerformanceComments").val();

        if ((employeeMidYearPerformanceComments == '' && IsEmp == 1)) {
            ShowMessage("error", "Section 4:Mid Year Appraisal 'Employee Comments on Mid Year Performance' fields is mandatory");
        }
        else {
            pageLoaderFrame();

            $.ajax({
                url: "/PDRP/AnnualAppraisal/SubmitEmployeeSignOffForms",
                type: 'POST',
                data: { id: AnnualGoalSettingFormAId, employeeId: employeeId, formId: formId, performanceGroupId: 3, requestId: requestId, goalSettingId: goalSettingId, comments: comments, EmployeeMidYearPerformanceComments: employeeMidYearPerformanceComments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        setTimeout(function () { BackToTaskList(); }, 1000);
                    }
                    hideLoaderFrame();
                }
            });
        }
    });
    FullYearControls();
  

    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });

    $("#divBusinessTargets").on('change', '.weight', function () {
        var calculated_total_sum = 0;

        $("#tblBusinessTargets .weight").each(function () {

            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                //calculated_total_sum += parseFloat(get_textbox_value);
                //*** F12
                if (get_textbox_value > 0) {
                    calculated_total_sum += parseFloat(get_textbox_value);
                }
                else {
                    //ShowMessage("error", "Invalid KPI");
                    $(this).val(0);
                }
            }

        });
        if (calculated_total_sum > 100) {
            $("#txtTotalWeightage").css({ "border-color": "Red", "border-width": "thick" });
        }
        else {
            $("#txtTotalWeightage").css({ "border-color": "#d5d5d5", "border-width": "thin" });
        }
        $("#txtTotalWeightage").val(calculated_total_sum);
    });

    $(".wid180").on('change', function () {

        var totalScore = 0;
        $("#tblBusinessTargets #ProficiencyScore").each(function () {
            var item_Weight = $(this).closest('tr').find('#item_Weight').val();
            var Score = $(this).closest('tr').find('#item_Score');
            if (parseInt(item_Weight) > 0) {
                var sc = parseInt($(this).val()) * parseInt(item_Weight);
                Score.val(sc);
                totalScore = parseInt(totalScore) + parseInt(sc);

            }

        });

        alert(0);

        $("#BusinessTargetsTota").val(totalScore);
        $("#divTotalScore").html(totalScore);
        $("#TotalScore1").val(totalScore);
        $("#divTotalScore1").html(totalScore);

        OverallAppraisalScore();
    });

    $(".wid181").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblBehavioralCompetenciesFormB #ProficiencyScore").each(function () {

            if (parseInt($(this).val()) > 0) {
                var sc = parseInt($(this).val());
                totalScore = parseInt(totalScore) + parseInt(sc);
                cnt++;
            }

        });


        $("#BehavioralTotalScore").val(totalScore / 5);
        $("#divBehavioralTotalScore").html(totalScore / 5);

        OverallAppraisalScore();

    });
    $(".wid182").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblProfessionalCompetenciesFormB #ProficiencyScore").each(function () {

            if (parseInt($(this).val()) > 0) {
                var sc = parseInt($(this).val());
                totalScore = parseInt(totalScore) + parseInt(sc);
                cnt++;
            }

        });


        $("#ProfessionalTotalScore").val(totalScore / 5);
        $("#divProfessionalTotalScore").html(totalScore / 5);

        OverallAppraisalScore();
    });

    //*** Naresh 2020-03-13 Display last comment
    //if ($('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
    //*** Danny to block at REject mode
    if ($('#ReqStatusId').val() < 3 && $('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
        $('#txtComments').val($('#lblComments').data('last-comment'))
});

function getBusinessTargetModeldata() {
    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinesstargetComments: $("#businessTargetComments").val(),
        CompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),  
        JsonString: JSON.stringify(selectedValues),
        ID: $("#AnnualGoalSettingFormAId").val(),
        JobGrade: $("#txtGrade").val(),
        //***F2
        JsonStringCurrRatingScales: JSON.stringify(CurrRatingScales),
        GoalSettingId: $("#GoalSettingId").val(),
        EmployeeName: $("#EmployeeName").val(),
        Company: $("#Company").val(),
        Designation: $("#Designation").val(),
        RequestEmail: $("#RequestEmail").val(),
        Department: $("#Department").val(),
        LineManager: $("#LineManager").val(),
        DOJ: $("#DOJ").val(),
        FormState: $("#FormMode").val(),
        MidFormMode: $("#MidFormMode").val(),

        //PerformanceGroupId: $("#PerformanceGroupId").val(),

        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds: $("#TrainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val(),
        //*** Naresh 2020-03-11 Save comment during save
        Comments: $("#txtComments").val()
    };
    return businessTargetModel;
}
function FullYearControls() {
    var PeriodId = $('#PeriodId').val();
    if (PeriodId == 2) {
        $('#item_Score').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_BusinessTargetDetails').bind('keypress', function (e) {
            e.preventDefault();
        });
        $('#item_Weight').bind('keypress', function (e) {
            e.preventDefault();
        });
    }
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}