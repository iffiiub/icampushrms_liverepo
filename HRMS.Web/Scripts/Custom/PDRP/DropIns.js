﻿var data = {
    "items": [{
        "id": "en-gb",
        "direction": "ltr",
        "EmpID": "Emp ID",
        "FirstName": "First Name",
        "LastName": "Last Name",
        "PositionName": "Position Name",
        "DepartmentName": "Department Name",
        "Action": "Action",
        "KPIck": "KPIs not set for selected year.",
        "RollStNo": "No-of students present (Male/Female) cannot be greater than No-of students on Roll",
        "TechErr": "Technical Error!",
        "ID": "ID",
        "VisitDateT": "Visit Date",
        "RequestID": "Request ID",
        "IsAcknowledged": "Is Acknowledged?",
        "Total": "Total",
        "OverAllScore": "Over All Score",
        "DropInsFor": "Drop Ins - For:"


    }, {
        "id": "ar",
        "direction": "rtr",
        "EmpID": "معرف Emp",
        "FirstName": "الاسم الاول",
        "LastName": "الكنية",
        "PositionName": "اسم المركز",
        "DepartmentName": "اسم القسم",
        "Action": "عمل",
        "KPIck": "مؤشرات الأداء الرئيسية غير محددة للسنة المحددة.",
        "RollStNo": "لا يمكن أن يكون عدد الطلاب الحاضرين (ذكور / إناث) أكبر من عدد الطلاب الدائمين",
        "TechErr": "خطأ تقني!",
        "ID": "هوية شخصية",
        "VisitDateT": "تاريخ الزيارة",
        "RequestID": "طلب معرف",
        "IsAcknowledged": "يتم الاعتراف؟",
        "Total": "مجموع",
        "OverAllScore": "المجموع النهائي",
        "DropInsFor": "انخفاض الوظائف - من أجل:"
    }]
};

var LanguageData = $.grep(data.items, function (element, index) {
    return element.id == $('#culture').val();
});


function BackToTaskList() {
    //*** Naresh 2020-03-17 Redirect to specific page based on the origin
    var mode = $('#btnBack').data('mode');
    if (mode == undefined || mode == "") mode = "DropInManage";
    location.href = "/PDRP/DropIns/" + mode;
}
function KPICheck() {

    var vKPICount = $(".KPICount").text();
    //alert(vKPICount);
    if (vKPICount != "") {
        if (vKPICount == 0) {
            //ShowMessage("error", "KPI is not Set for the current year.</br> You cannot process Appraisal for this year");

            $("#lblKPI").text(LanguageData[0].KPIck);


        }
        else {

            $("#lblKPI").text("");
        }
    }
}
function getGrid() {

    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }

    var employeeStatus = $("#employeeStatusddl").val();
    var ddlDropInsYearID = $("#ddlKPIYear").val();
    var ddlDropInsYearTxt = $("#ddlKPIYear option:selected").text();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }

    if (ddlDropInsYearID == "") {
        ddlDropInsYearID = 0
    }
    var companyID = $("#ddlCompany").val();
    if (companyID == undefined || companyID == "") {
        companyID = 0;
    }
    var employeeID = $("#ddlEmployee").val();
    if (employeeID == undefined || employeeID == "") {
        employeeID = 0;
    }
    //*** Naresh 2020-03-17 Redirect to specific page based on the origin
    var mode = $('#PageMode').val();
    //var IncludeInactive = $("#includeInActiveChk").is(':checked');
    //var searchval = $("#txtSearch").val();
    pageLoaderFrame();
    oTableChannel = $('#tbl_employee').dataTable({

        "sAjaxSource": "/PDRP/DropIns/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt + "&EmployeeId=" + employeeID + "&CompanyId=" + companyID + "&mode=" + mode,
        "aoColumns": [


            { "mData": "EmployeeAlternativeID", "sTitle": LanguageData[0].EmpID, "width": "6%", "sType": "Int", "className": "center-text-align" },
            { "mData": "FirstName", "sTitle": LanguageData[0].FirstName, "width": "15%", "className": "center-text-align" },
            { "mData": "LastName", "sTitle": LanguageData[0].LastName, "width": "15%", "className": "center-text-align" },
            { "mData": "Gender", "bVisible": false, "sTitle": "Gender", "sWidth": "10%" },
            //{ "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            { "mData": "PositionName", "sTitle": LanguageData[0].PositionName, "sWidth": "13%", "className": "center-text-align" },
            { "mData": "Department", "sTitle": LanguageData[0].DepartmentName, "sWidth": "13%", "className": "center-text-align" },
            //{ "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            //{ "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            //{ "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            //{ "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            { "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" },
            { "mData": "ViewDropIns", "bVisible": false, "sTitle": "View DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "CreateDropIns", "bVisible": false, "sTitle": "Create DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "Action", "sTitle": LanguageData[0].Action, "width": "12%", "bSortable": false, "className": "center-text-align" },
            { "mData": "KPICount", "className": "hidden" },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/DropIns/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt + "&EmployeeId=" + employeeID + "&CompanyId=" + companyID + "&mode=" + mode,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            //KPICheck();
            hideLoaderFrame();

            if (LanguageData[0].direction == "rtr") {
                $('.dataTables_length').css("float", "right");
                $('.dataTables_filter').css("float", "left");
                $('.dataTables_filter label').css("float", "left");
            }
        },
        "language": {
            "loadingRecords": "<span class='loader'></span>",
            "url": "/Content/MultiLang/" + $("#culture").val() + ".json"


        }
    });

}
function CheckNoOfStudents(e, i) {
    e = parseInt(e);
    var Male = parseInt($("#DropInEmployeeModel_StudentsOnRollMale").val());
    var FeMale = parseInt($("#DropInEmployeeModel_StudentsOnRollFemale").val());

    if (i == 1) {
        if (e > Male) {
            ShowMessage("error", LanguageData[0].RollStNo);
            $("#DropInEmployeeModel_StudentsPresentMale").val(Male);
        }
    }
    if (i == 2) {
        if (e > FeMale) {
            ShowMessage("error", LanguageData[0].RollStNo);
            $("#DropInEmployeeModel_StudentsPresentFemale").val(FeMale);
        }
    }
}
function RemoveValidator() {
    ManageValidator('remove');
}
function ManageValidator(ruleMode) {
    $("#DropInEmployeeModel_LessonTopic").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_Branch").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_GradeYear").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_GroupSection").rules(ruleMode, 'required')

    //$("#DropInEmployeeModel_StudentsOnRollMale").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_StudentsOnRollFemale").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_StudentsPresentMale").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_StudentsPresentFemale").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_VisitDATE").rules(ruleMode, 'required')
    $("#DropInEmployeeModel_VisitTime").rules(ruleMode, 'required')
    $("#radioYes").rules(ruleMode, 'required')
    $("#radioNo").rules(ruleMode, 'required')

    //
    if (ruleMode == 'add') {
        var controlSelector = "#DropInEmployeeModel_StudentsOnRollMale";
        $(controlSelector).rules({
            range: [parseInt($(controlSelector).attr('data-val-range-min')), parseInt($(controlSelector).attr('data-val-range-max'))]
        });

        controlSelector = "#DropInEmployeeModel_StudentsOnRollFemale";
        $(controlSelector).rules({
            range: [parseInt($(controlSelector).attr('data-val-range-min')), parseInt($(controlSelector).attr('data-val-range-max'))]
        });

        controlSelector = "#DropInEmployeeModel_StudentsPresentMale";
        $(controlSelector).rules({
            range: [parseInt($(controlSelector).attr('data-val-range-min')), parseInt($(controlSelector).attr('data-val-range-max'))]
        });

        controlSelector = "#DropInEmployeeModel_StudentsPresentFemale";
        $(controlSelector).rules({
            range: [parseInt($(controlSelector).attr('data-val-range-min')), parseInt($(controlSelector).attr('data-val-range-max'))]
        });


    }
    else {
        $("#DropInEmployeeModel_StudentsOnRollMale").rules(ruleMode, 'range');
        $("#DropInEmployeeModel_StudentsOnRollFemale").rules(ruleMode, 'range');
        $("#DropInEmployeeModel_StudentsPresentMale").rules(ruleMode, 'range');
        $("#DropInEmployeeModel_StudentsPresentFemale").rules(ruleMode, 'range');
    }
   


    $("#Outline").rules(ruleMode, 'required')
    $("#Feedback").rules(ruleMode, 'required')


    //$("#tblDropInProficiencyLevel #ProficiencyScore").each(function () {
    //    $(this).rules('remove', 'required')

    //});
}

function AddValidator() {
    ManageValidator('add');
}
$(document).ready(function () {
    $('#DropInEmployeeModel_VisitDATE').datepicker({
        changeMonth: false,
        changeYear: false,
        dateFormat: HRMSDateFormat,
        format: HRMSDateFormat,
        todayHighlight: true,
        autoclose: true
    });

    //*** MultiLanguage

    //alert(LanguageData[0].id + "  " + LanguageData[0].category);
    if (location.href.toLocaleLowerCase().indexOf("pdrp/dropins/dropinmanage") == -1
        && location.href.toLocaleLowerCase().indexOf("pdrp/dropins/mydropins") == -1
        && location.href.toLocaleLowerCase().indexOf("pdrp/dropins/admindropins") == -1
    )
        $('#btnShowLanguage').removeClass("hide");
    //===========================================================================
    $('#DropInEmployeeModel_VisitDATE').bind('keypress', function (e) {
        e.preventDefault();
    });
    $('#DropInEmployeeModel_VisitTime').bind('keypress', function (e) {
        e.preventDefault();
    });
    //getGrid();

    $(".wid180").on('change', function () {
        var scorevalues = [];
        $("#tblDropInProficiencyLevel #ProficiencyScore").each(function () {
            var get_textbox_value = $(this).val();
            if (get_textbox_value > 0) {
                scorevalues.push(get_textbox_value);
            }
        });
        CalculateDropInScore(scorevalues);
    });
    
    $("#btnSearch").on('click', function () {
        getGrid();
    });
    //$("#ddlKPIYear").on('change', function () {
    //    getGrid();
    //});
    //$("#ddlEmployee").on('change', function () {
    //    getGrid();
    //});
    //$("#ddlCompany").on('change', function () {
    //    getGrid();
    //});


    $("#tbtnSave").on('click', function () {

        RemoveValidator();
        $("#isAddMode").val("2");
        $("#DropInsForm").submit();
    });
    $("#tbtnSubmit").on('click', function () {
        AddValidator();
        $("#isAddMode").val("3");
        $("#DropInsForm").submit();
    });

    $("#btnSubmit").on('click', function () {
        AddValidator();
        $("#isAddMode").val("3");
    });
    $("#btnSave").on('click', function () {
        RemoveValidator();
        $("#isAddMode").val("2");
    });

    var ReqStatusID = $("#ReqStatusID").val();
    if (ReqStatusID == 4) {
        $(".btnSave").hide();
        $(".btn-submit").hide();

    }

    var RequesterID = $("#DropInEmployeeModel_RequesterID").val();
    var SuperviserID = $("#DropInEmployeeModel_SuperviserID").val();
    if (RequesterID != SuperviserID) {
        //*** Naresh 2020-04-14 For Arabic support moved to single container
        //$("#TopButtons").html("");
        $("#TopButtons").remove('#tbtnSave');
        $("#TopButtons").remove('#tbtnSubmit');
    }

    $(".selectpicker").selectpicker("refresh");
    //if ($('#hdVisitDATE').val() == "") {
    //    $('#DropInEmployeeModel_VisitDATE').val("");
    //}
    //else
    //{
    //    $('#DropInEmployeeModel_VisitDATE').val($('#hdVisitDATE').val());

    //}
    //if ($('#hdVisitTime').val() == "") {
    //    $('#DropInEmployeeModel_VisitTime').val("");
    //    //alert($('#hdVisitTime').val());
    //}
    //else {
    //    $('#DropInEmployeeModel_VisitTime').val($('#hdVisitTime').val());

    //}
    $("#DropInEmployeeModel_StudentsPresentMale, #DropInEmployeeModel_StudentsOnRollMale").on('change', function () {

        CheckNoOfStudents($("#DropInEmployeeModel_StudentsPresentMale").val(), 1);
    });
    $("#DropInEmployeeModel_StudentsPresentFemale, #DropInEmployeeModel_StudentsOnRollFemale").on('change', function () {
        CheckNoOfStudents($("#DropInEmployeeModel_StudentsPresentFemale").val(), 2);
    });

});

//*** MultiLanguage
function PDRPLoadLanguage(e) {
    pageLoaderFrame();
    $.ajax({
        url: '/PDRP/DropIns/SetCulture',
        data: { languageid: e },
        type: 'GET',
        success: function (data) {
            location.reload();
            hideLoaderFrame();
        },
        error: function (data, xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            ShowMessage("error", LanguageData[0].TechErr);
            hideLoaderFrame();
        }
    });
}
function CalculateDropInScore(scorevalues) {

    var ddlDropInsYearID = $("#DropInEmployeeModel_DropinYearID").val();

    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, FormulaYearID: ddlDropInsYearID },
        traditional: true,
        url: '/PDRP/DropIns/CalculateDropInScore',
        success: function (data) {
            $("#ProficiencyScoreFormula").val(data.Formula);
            $("#ProficiencyScoreTotal").val(data.Total);
            $("#divProficiencyScoreTotal").text(data.Total);

            $("#ProficiencyScoreOverall").val(data.OverallScore);
            $("#divProficiencyScoreOverall").text(data.OverallScore);

        },
        error: function (data, xhr, status) {
            ShowMessage("error", LanguageData[0].TechErr);
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}


function ViewDeductionDetails(TeacherID, TeacherName, DropInYearID, DropInYear) {
    //InstallmentPayDeductionId = PayDeductionId;
    $("#modal_Loader3").load("/PDRP/DropIns/ViewAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYearID, function () {
        $("#myModal3").modal("show");
        if ($('#culture').val() == "ar") {
            $("#myModal3").attr("dir", "rtl");
        }
        $(".modal-dialog").attr("style", "width:750px;");
        $("#modal_heading3").html("<i class='fa fa-user'></i> " + TeacherName + "'s " + LanguageData[0].DropInsFor + DropInYear);
        $(".close").removeClass('hidden');
        getAllDropInsListGrid(TeacherID, DropInYearID);
    });
}
function getAllDropInsListGrid(TeacherID, DropInYear) {

    //alert(TeacherID);
    oTableChannel = $('#tbl_AllDropIns').dataTable({
        "sAjaxSource": "/PDRP/DropIns/GetAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear,
        "aoColumns": [


            { "mData": "ID", "sTitle": LanguageData[0].ID, "bVisible": false, "width": "6%", "className": "center-text-align" },
            { "mData": "VisitDATE", "sTitle": LanguageData[0].VisitDateT, "width": "30%" },
            { "mData": "RequestID", "sTitle": LanguageData[0].RequestID, "width": "30%" },
            { "mData": "IsAcknowledged", "sTitle": LanguageData[0].IsAcknowledged, "width": "30%", "className": "center-text-align" },
            { "mData": "Total", "sTitle": LanguageData[0].Total, "width": "30%" },
            { "mData": "OverAllScore", "sTitle": LanguageData[0].OverAllScore, "sWidth": "10%" },
            //{ "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            //{ "mData": "PositionName", "sTitle": "Position Name", "sWidth": "13%" },
            //{ "mData": "Department", "sTitle": "Department Name", "sWidth": "13%" },
            //{ "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            //{ "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            //{ "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            //{ "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            //{ "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "ViewDropIns", "bVisible": true, "sTitle": "View DropIns", "width": "5%", "className": "center-text-align" },
            //{ "mData": "CreateDropIns", "bVisible": true, "sTitle": "Create DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "Action", "sTitle": LanguageData[0].Action, "width": "24%", "bSortable": false, "className": "center-text-align" },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/DropIns/GetAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            if (LanguageData[0].direction == "rtr") {
                $('.dataTables_length').css("float", "right");
                $('.dataTables_filter').css("float", "left");
                $('.dataTables_filter label').css("float", "left");
            }
        },
        "language": {
            "loadingRecords": "<span class='loader'></span>",
            "url": "/Content/MultiLang/" + $("#culture").val() + ".json"
        }
    });

}

function ViewDropIns(PDRPDropInID, TeacherID, DropInYear, requestid) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { PDRPDropInID: PDRPDropInID, isAddMode: 2, EMployeeID: TeacherID, DropInYear: DropInYear, RequestID: requestid },
        url: '/PDRP/DropIns/SetDropInsActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.open("/PDRP/DropIns/DropInForm/");
            }
            else {
                window.open("/PDRP/DropIns/DropInForm/");
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].TechErr);
        }
    });
}
function CreateDropIn(id, dropinyear) {
    ////*** Block Dropin creation if Annual appraisal is Started for this employee
    //$.ajax({
    //   dataType: 'json',
    //   type: 'POST',
    //   data: { EMployeeID: id, DropInYear: dropinyear,isAddMode:1 },
    //   url: '/PDRP/DropIns/ValidatingAnnualAppraisal',
    //   success: function (data) {
    //       if (data.result.length > 0) {
    //           ShowMessage(data.result, data.resultMessage);
    //       }
    //       else
    //       {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, DropInYear: dropinyear, isAddMode: 1 },
        url: '/PDRP/DropIns/SetDropInsActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/DropIns/DropInForm/";
            }
            else {
                ShowMessage("error", data.message);
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].TechErr);
        }
    });
    //        }
    //    },
    //    error: function (data) {
    //        ShowMessage("error", "Some technical error occurred");
    //    }
    //});

}







function ExportToExcel() {
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToExcel?employeeStatus=" + employeeStatus);
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    var employeeStatus = $("#employeeStatusddl").val();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToPdf?employeeStatus=" + employeeStatus);
    }
    else ShowMessage("warning", "No data for export!");
}
function CompanyIDChange(object) {
    var selectedItem = $(object).val();
    if (selectedItem == undefined || selectedItem == "")
        selectedItem = 0;
    selectedItem = parseInt(selectedItem);
    
    var ddlEmployees = $("#ddlEmployee");
    $.ajax({
        cache: false,
        type: "GET",
        url: "/PDRP/DropIns/GetEmployees",
        data: { "companyId": selectedItem },
        success: function (data) {
            ddlEmployees.html('');
            ddlEmployees.append($('<option></option>').val("0").html("Select Employee"));
            $.each(data, function (id, option) {
                ddlEmployees.append($('<option></option>').val(option.id).html(option.name));
            });
            RefreshSelect("#ddlEmployee");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //  alert('Failed to retrieve states.');
        }
    });
}