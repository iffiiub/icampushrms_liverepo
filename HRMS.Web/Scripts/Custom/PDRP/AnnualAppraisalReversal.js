﻿var selectedValues = [];
var isSelectedValues = false;
var appliedSelections = [];
var isFilter = false;

$(document).ready(function () {
    //loadAnnualAppraisalProcessData();

    $('#btnSearch').on('click', function () {
        loadAnnualAppraisalProcessData();
        isSelectedValues = false;
        isFilter = true;
    });






    loadAnnualAppraisalProcessData();
});
function ReverseTask(FormID, FormInstanceID,PerformanceGroupId)
{
    if (FormID != '' && FormInstanceID != '') {
        pageLoaderFrame();
        $.ajax({
            type: 'POST',
            data: { FormID: FormID, FormInstanceID: FormInstanceID ,PerformanceGroupId : PerformanceGroupId },
            traditional: true,
            url: '/PDRP/AnnualAppraisal/ReverseTask',
            success: function (data) {                
                hideLoaderFrame();
                if (data.Success)
                {
                    ShowMessage("success", data.Message);
                    loadAnnualAppraisalProcessData();
                }
                else {
                    ShowMessage("error", "Some technical error occurred");
                }
                
                //alert(data);
            },
            error: function (data, xhr, status) {
                ShowMessage("error", "Some technical error occurred");
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    }
}
function loadAnnualAppraisalProcessData() {
    var ddlDropInsYearID = $("#ddlKPIYear").val();

    var periodId = $("#ddlPeriod").val();
    if (ddlDropInsYearID == '') ddlDropInsYearID = '0';
    if (periodId == '') periodId = '0';

    if (periodId != '' && ddlDropInsYearID != '') {
        pageLoaderFrame();
        $.ajax({
            type: 'POST',
            data: { companyId: $("#ddlCompany").val(), yearId: $("#ddlKPIYear").val(), PeriodId: periodId },
            traditional: true,
            url: '/PDRP/AnnualAppraisal/GetAnnualAppraisalReverseList',
            success: function (data) {
                $("#divAnnualAppraisalSettings").html(data);
                hideLoaderFrame();
                $('#tblAnnualAppraisalSettingGrid').DataTable({
                    "aoColumnDefs": [
                        { "sWidth": "10%", "aTargets": [2,3,4,5] },
                        { "sWidth": "15%", "aTargets": [6] }
                    ],
                    "fnDrawCallback": function () {                      
                        
                    },
                });                
               

            },
            error: function (data, xhr, status) {
                ShowMessage("error", "Some technical error occurred");
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    }
}
