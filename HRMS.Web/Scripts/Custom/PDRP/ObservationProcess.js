﻿var data = {
    "items": [{
        "id": "en-gb",
        "direction": "ltr",
        "ID": "Emp ID",
        "FirstName": "First Name",
        "LastName": "Last Name",
        "VisitDateT": "Visit Date",
        "VisitTimeT": "Visit Time",

        "EmpID": "Emp ID",

        "PositionName": "Position Name",
        "DepartmentName": "Department Name",
        "Action": "Action",
        "KPIck": "KPIs not set for selected year.",
        "RollStNo": "No-of students present(Male/Female) cannot be greater than No-of students on Roll",
        "NoofSEN": "No-of SEN students (Male/Female) cannot be greater than No-of students on Roll",
        "TechErr": "Technical Error!",
        "PreObservationDueDate": "Pre-Observation Due Date",

        "RequestID": "Request ID",
        "IsAcknowledged": "Is Acknowledged?",
        "Total": "Total",
        "OverAllScore": "Over All Score",
        "DropInsFor": "Drop Ins - For:",
        "Pleaseselect": "Please select at least one record.",
        "PleaseselectVisit": "Please select Visit Date / Visit Time.",
        "Sometechnical": "Some technical error occurred"

    }, {
        "id": "ar",
        "direction": "rtr",
        "ID": "هوية شخصية",
        "FirstName": "الاسم الاول",
        "LastName": "الكنية",
        "VisitDateT": "تاريخ الزيارة",
        "VisitTimeT": "وقت الزيارة",
        "PreObservationDueDate": "قبل تاريخ الاستحقاق",
        "EmpID": "معرف Emp",

        "PositionName": "اسم المركز",
        "DepartmentName": "اسم القسم",
        "Action": "عمل",
        "KPIck": "مؤشرات الأداء الرئيسية غير محددة للسنة المحددة.",
        "RollStNo": "لا يمكن أن يكون عدد الطلاب الحاضرين (ذكور / إناث) أكبر من عدد الطلاب الدائمين",
        "NoofSEN": "لا يمكن أن يكون عدد الطلاب SEN (ذكر / أنثى) أكبر من عدد الطلاب في Roll",
        "TechErr": "خطأ تقني!",


        "RequestID": "طلب معرف",
        "IsAcknowledged": "يتم الاعتراف؟",
        "Total": "مجموع",
        "OverAllScore": "المجموع النهائي",
        "DropInsFor": "انخفاض الوظائف - من أجل:",
        "Pleaseselect": "يرجى تحديد سجل واحد على الأقل.",
        "PleaseselectVisit": "يرجى تحديد تاريخ الزيارة / وقت الزيارة.",
        "Sometechnical": "حدث خطأ فني"
    }]
};

var LanguageData = $.grep(data.items, function (element, index) {
    return element.id == $('#culture').val();
});

//*** MultiLanguage
function PDRPLoadLanguage(e) {
    pageLoaderFrame();
    $.ajax({
        url: '/PDRP/Observations/SetCulture',
        data: { languageid: e },
        type: 'GET',
        success: function (data) {
            //*** Naresh 2020-04-20 Open the other language in new tab
            var win = window.open(location.href, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it

            }
            //location.reload();
            hideLoaderFrame();
        },
        error: function (data, xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            ShowMessage("error", LanguageData[0].TechErr);
            hideLoaderFrame();
        }
    });
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}

function SelectAll() {
    var table = $("#tbl_employee").DataTable();
    var UpdatedTr1 = table.$(".odd");
    var UpdatedTr2 = table.$(".even");
    $(UpdatedTr1).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', false);
            }

        }
        else {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', true);
            }

        }
        ChkChange(chbox);

    });
    $(UpdatedTr2).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', false);
            }
        }
        else {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', true);
            }
        }
        ChkChange(chbox);

    });

}
function ViewPreObservation(id) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        url: '/PDRP/Observations/SetViewPreObservationSesson',
        success: function (data) {
            if (data.success == true) {


                window.open("/PDRP/Observations/PreObservation/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}
function ViewPostObservation(id) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        url: '/PDRP/Observations/SetViewPostObservationSesson',
        success: function (data) {
            if (data.success == true) {


                window.open("/PDRP/Observations/PostObservation/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}
function RemoveValidatorPre() {
    ManageValidatorPre('remove');
}

function AddValidatorPre() {
    ManageValidatorPre('add');
}
function ManageValidatorPre(ruleMode) {
    $("#ObservationsEmployeeModel_Subject").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_LessonTopic").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_YearPlan").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_Branch").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_GradeYear").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_GroupSection").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_Period").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_UnitThemeLessonTopic").rules(ruleMode, 'required')

    $("#ObjectivesList").rules(ruleMode, 'required')
    $("#ObservationDayLesson").rules(ruleMode, 'required')
    $("#SpecificItemsToWatch").rules(ruleMode, 'required')
    $("#ItemsToBeAware").rules(ruleMode, 'required')


    if (ruleMode == 'remove') {
        $("#ObservationsEmployeeModel_StudentsOnRollMale").removeAttr("min");
        $("#ObservationsEmployeeModel_StudentsOnRollFemale").removeAttr("min");
        $("#ObservationsEmployeeModel_StudentsPresentMale").removeAttr("min");
        $("#ObservationsEmployeeModel_StudentsPresentFemale").removeAttr("min");

        $("#ObservationsEmployeeModel_StudentsOnRollMale").rules(ruleMode, 'range');
        $("#ObservationsEmployeeModel_StudentsOnRollFemale").rules(ruleMode, 'range');
        $("#ObservationsEmployeeModel_StudentsPresentMale").rules(ruleMode, 'range');
        $("#ObservationsEmployeeModel_StudentsPresentFemale").rules(ruleMode, 'range');
    }
    else {
        AddRangeValidator("#ObservationsEmployeeModel_StudentsOnRollMale");
        AddRangeValidator("#ObservationsEmployeeModel_StudentsOnRollFemale");
        AddRangeValidator("#ObservationsEmployeeModel_StudentsPresentMale");
        AddRangeValidator("#ObservationsEmployeeModel_StudentsPresentFemale");
    }
    //$("#Outline").rules('remove', 'required')
    //$("#Feedback").rules('remove', 'required')
}
function AddRangeValidator(controlSelector) {
    $(controlSelector).rules({
        range: [parseInt($(controlSelector).attr('data-val-range-min')), parseInt($(controlSelector).attr('data-val-range-max'))]
    });
}
function RemoveValidatorObsEvl() {
    ManageValidatorObsEvl('remove');
}
function AddValidatorObsEvl() {
    ManageValidatorObsEvl('add');
}
function ManageValidatorObsEvl(ruleMode) {
    $("#ObservationsEmployeeModel_Subject").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_LessonTopic").rules(ruleMode, 'required')
    //$("#ObservationsEmployeeModel_YearPlan").rules('remove', 'required')
    $("#ObservationsEmployeeModel_Branch").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_GradeYear").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_GroupSection").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_Period").rules(ruleMode, 'required')

    //$("#ObservationsEmployeeModel_UnitThemeLessonTopic").rules('remove', 'required')

    $("#ObservationsEmployeeModel_OBComments").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_ProgressEvidence").rules(ruleMode, 'required')
    $("#tblObservationsProficiencyLevel #ProficiencyScore").each(function () {
        $(this).rules(ruleMode, 'required')

    });
    if (ruleMode == 'add') {
        AddRangeValidator("#ObservationsEmployeeModel_StudentsOnRollMale");
        AddRangeValidator("#ObservationsEmployeeModel_StudentsOnRollFemale");
        AddRangeValidator("#ObservationsEmployeeModel_StudentsPresentMale");
        AddRangeValidator("#ObservationsEmployeeModel_StudentsPresentFemale");
        AddRangeValidator("#ObservationsEmployeeModel_SENStudentsMale");
        AddRangeValidator("#ObservationsEmployeeModel_SENStudentsFemale");
    }
    else {
        $("#ObservationsEmployeeModel_StudentsOnRollMale").rules(ruleMode, 'range')
        $("#ObservationsEmployeeModel_StudentsOnRollFemale").rules(ruleMode, 'range')
        $("#ObservationsEmployeeModel_StudentsPresentMale").rules(ruleMode, 'range')
        $("#ObservationsEmployeeModel_StudentsPresentFemale").rules(ruleMode, 'range')
        $("#ObservationsEmployeeModel_SENStudentsMale").rules(ruleMode, 'range')
        $("#ObservationsEmployeeModel_SENStudentsFemale").rules(ruleMode, 'range')
    }
    //$(".wid180").rules('remove', 'required')
    //$("#ItemsToBeAware").rules('remove', 'required')



    $("#ObservationsEmployeeModel_StudentsOnRollMale").removeAttr("min");
    $("#ObservationsEmployeeModel_StudentsOnRollFemale").removeAttr("min");
    $("#ObservationsEmployeeModel_StudentsPresentMale").removeAttr("min");
    $("#ObservationsEmployeeModel_StudentsPresentFemale").removeAttr("min");

    $("#ObservationsEmployeeModel_SENStudentsMale").removeAttr("min");
    $("#ObservationsEmployeeModel_SENStudentsFemale").removeAttr("min");

    //$("#Outline").rules('remove', 'required')
    //$("#Feedback").rules('remove', 'required')
}

function RemoveValidatorPost() {
    ManageValidatorPost('remove');
}
function AddValidatorPost() {
    ManageValidatorPost('add');
}
function ManageValidatorPost(ruleMode) {
    //$("#ObservationsEmployeeModel_Subject").rules('remove', 'required')
    $("#ObservationsEmployeeModel_LessonTopic").rules(ruleMode, 'required')
    //$("#ObservationsEmployeeModel_YearPlan").rules('remove', 'required')
    $("#ObservationsEmployeeModel_Branch").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_GradeYear").rules(ruleMode, 'required')
    $("#ObservationsEmployeeModel_GroupSection").rules(ruleMode, 'required')
    //$("#ObservationsEmployeeModel_Period").rules('remove', 'required')
    //$("#ObservationsEmployeeModel_UnitThemeLessonTopic").rules('remove', 'required')

    $("#ParticularlyWellLesson").rules(ruleMode, 'required')
    $("#FurtherImprovementLesson").rules(ruleMode, 'required')
    $("#ChildrensHighAbility").rules(ruleMode, 'required')
    $("#ChildrensAverageAbility").rules(ruleMode, 'required')
    $("#ChildrensBelowAverageAbility").rules(ruleMode, 'required')


    //$("#ObservationsEmployeeModel_StudentsOnRollMale").removeAttr("min");
    //$("#ObservationsEmployeeModel_StudentsOnRollFemale").removeAttr("min");
    //$("#ObservationsEmployeeModel_StudentsPresentMale").removeAttr("min");
    //$("#ObservationsEmployeeModel_StudentsPresentFemale").removeAttr("min");

    //$("#Outline").rules('remove', 'required')
    //$("#Feedback").rules('remove', 'required')
}
$(document).ready(function () {

    //*** MultiLanguage
    //alert(LanguageData[0].id + "  " + LanguageData[0].category);

    if (location.href.toLocaleLowerCase().indexOf('pdrp/observations/observationprocess') == -1)
        $('#btnShowLanguage').removeClass("hide");
    $("#tprebtnSave").on('click', function () {
        //$("#isAddMode").val("2");
        RemoveValidatorPre();
        $("#PreObservationsForm").submit();
    });
    $("#tprebtnSubmit").on('click', function () {
        AddValidatorPre();
        $("#isAddMode").val("3");
        $("#PreObservationsForm").submit();
    });
    $("#prebtnSubmit").on('click', function () {
        AddValidatorPre();
        $("#isAddMode").val("3");
        //$("#PreObservationsForm").submit();
    });
    $("#prebtnSave").on('click', function () {
        RemoveValidatorPre();
        $("#isAddMode").val("1");
        //$("#PreObservationsForm").submit();
    });


    $("#postbtnSubmit").on('click', function () {
        AddValidatorPost();
        $("#isAddMode").val("3");

    });
    $("#postbtnSave").on('click', function () {
        RemoveValidatorPost();
        $("#isAddMode").val("1");

    });
    $("#tpostbtnSubmit").on('click', function () {
        AddValidatorPost();
        $("#isAddMode").val("3");
        $("#PostObservationsForm").submit();
    });
    $("#tpostbtnSave").on('click', function () {
        RemoveValidatorPost();
        $("#isAddMode").val("1");
        $("#PostObservationsForm").submit();
    });

    $('#txtVisitDate').datepicker({
        changeMonth: false,
        changeYear: false,
        dateFormat: HRMSDateFormat,
        format: HRMSDateFormat,
        todayHighlight: true,
        autoclose: true
    });
    //$('#txtVisitDate').datepicker({
    //    changeMonth: false,
    //    changeYear: false,
    //    dateFormat: HRMSDateFormat,
    //    todayHighlight: true,
    //    autoclose: true
    //});


    $(".wid180").on('change', function () {

        var scorevalues = [];
        $("#tblObservationsProficiencyLevel #ProficiencyScore").each(function () {

            var get_textbox_value = $(this).val();
            if (get_textbox_value > 0) {
                scorevalues.push(get_textbox_value);
            }
        });
        CalculateScore(scorevalues);

    });

    //***Initiate=========================================
    //**** 101 initiate Post Observation
    //**** 102 Submit Evaluation Form
    //**** 103 Employee Sign Off
    //**** 105 Save
    $("#OBEvlbtnInitiate").on('click', function () {
        AddValidatorObsEvl();
        $("#isAddMode").val("101");
    });
    $("#tOBEvlbtnInitiate").on('click', function () {
        AddValidatorObsEvl();
        $("#isAddMode").val("101");
        $("#ObservationForm").submit();
    });

    //***Submit=========================================
    $("#OBEvlbtnSubmit").on('click', function () {
        $("#isAddMode").val("102");
    });
    $("#tOBEvlbtnSubmit").on('click', function () {
        $("#isAddMode").val("102");
        $("#ObservationForm").submit();
    });

    //***signoff=========================================
    $("#OBEvlbtnsignoff").on('click', function () {
        $("#isAddMode").val("103");
    });
    $("#tOBEvlbtnsignoff").on('click', function () {
        $("#isAddMode").val("103");
        $("#ObservationForm").submit();
    });

    //***Save=========================================
    $("#tOBEvlbtnSave").on('click', function () {
        RemoveValidatorObsEvl();
        $("#isAddMode").val("105");
        $("#ObservationForm").submit();
    });
    $("#OBEvlbtnSave").on('click', function () {
        RemoveValidatorObsEvl();
        $("#isAddMode").val("105");
    });
    //=========================================



    var ReqStatusID = $("#ReqStatusID").val();

    if (ReqStatusID == 4) {
        $(".btnSave").hide();
        $(".btn-submit").hide();

    }

    var FormMode = $("#FormMode").val();

    if (FormMode == 2) {
        $(".btnSave").hide();
        $(".btn-submit").hide();

    }

    if ($("#radioYes").is(":checked")) {

        $("textarea#IsOnTrackYearlyPlansNo").attr("disabled", "disabled");
    }

    $("#radioYes").on('click', function () {
        $("textarea#IsOnTrackYearlyPlansNo").attr("disabled", "disabled");

    });
    $("#radioNo").on('click', function () {
        $("textarea#IsOnTrackYearlyPlansNo").removeAttr("disabled", "disabled");
    });



    $("#ObservationsEmployeeModel_StudentsPresentMale, #ObservationsEmployeeModel_StudentsOnRollMale").on('change', function () {
        CheckNoOfStudents($("#ObservationsEmployeeModel_StudentsPresentMale").val(), 1);
    });
    $("#ObservationsEmployeeModel_StudentsPresentFemale, #ObservationsEmployeeModel_StudentsOnRollFemale").on('change', function () {
        CheckNoOfStudents($("#ObservationsEmployeeModel_StudentsPresentFemale").val(), 2);
    });


});
function CheckNoOfStudents(e, i) {
    var Male = parseInt($("#ObservationsEmployeeModel_StudentsOnRollMale").val());
    var FeMale = parseInt($("#ObservationsEmployeeModel_StudentsOnRollFemale").val());
    e = parseInt(e);

    if (i == 1) {
        if (e > Male) {
            ShowMessage("error", LanguageData[0].RollStNo);
            $("#ObservationsEmployeeModel_StudentsPresentMale").val(Male);
        }
    }
    if (i == 2) {
        if (e > FeMale) {
            ShowMessage("error", LanguageData[0].RollStNo);
            $("#ObservationsEmployeeModel_StudentsPresentFemale").val(FeMale);
        }
    }
    if (i == 3) {
        if (e > Male) {
            ShowMessage("error", LanguageData[0].NoofSEN);
            $("#ObservationsEmployeeModel_SENStudentsMale").val(Male);
        }
    }
    if (i == 4) {
        if (e > FeMale) {
            ShowMessage("error", LanguageData[0].NoofSEN);
            $("#ObservationsEmployeeModel_SENStudentsFemale").val(FeMale);
        }
    }


}
function CalculateScore(scorevalues) {
    var ddlDropInsYearID = $("#ObservationsEmployeeModel_ObservationsYearID").val();
    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, FormulaYearID: ddlDropInsYearID },
        traditional: true,
        url: '/PDRP/Observations/CalculateDropInScore',
        success: function (data) {
            $("#ProficiencyScoreFormula").val(data.Formula);
            $("#ProficiencyScoreTotal").val(data.Total);
            $("#divProficiencyScoreTotal").text(data.Total);

            $("#ProficiencyScoreOverall").val(data.OverallScore);
            $("#divProficiencyScoreOverall").text(data.OverallScore);

        },
        error: function (data, xhr, status) {
            ShowMessage("error", LanguageData[0].Sometechnical);
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}
$("#ddlPDRPYears").on('change', function () {

    getEmployeeGrid();

});
function SetObservationVisitDate(e) {   
    //AllEditMode = 0;
    var table = $("#tbl_employee").DataTable();
    var vYearId = $("#ddlPDRPYears").val();
    var ddlYearTxt = $("#ddlPDRPYears option:selected").text();
    var UpdatedTr = table.$(".IsEdit");

    if (UpdatedTr.length > 0) {
        var updatedData = [];       
        if (ValidateDateSelection(UpdatedTr)){
            $(UpdatedTr).each(function () {
                //if visit date is selected then only add to array
                if ($(this).find('.datepicker').val().length > 0) {
                    var vEmployeeId = $(this).find('#EmployeeID').val();
                    //alert(vEmployeeId);
                    var vVisitDate = $(this).find('.datepicker').val() + ' ' + $(this).find('.timepicker').val();
                    var vPreObservationDueDate = $(this).find('#txtPreObsProc').text();
                    //var autoID = $(itm).find('td:eq(8)').text();
                    //var EmpId = ($(itm).find("#ddlApprover_" + autoID)).val();

                    updatedData.push({
                        EmployeeID: vEmployeeId,
                        VisitDate: vVisitDate,
                        PreObservationDueDate: vPreObservationDueDate,
                    });
                }
            });

            if (updatedData.length > 0) {
                //console.log(updatedData);
                //$.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save "+updatedData.length+" records?" }).done(function () {
                pageLoaderFrame();
                $.ajax({
                    url: "/PDRP/Observations/SetObservationVisitDateMulty",
                    contentType: 'application/json; charset=utf-8',
                    type: "POST",
                    data: JSON.stringify({ 'SelectedTeachersList': updatedData, 'YearId': vYearId }),
                    success: function (data) {
                        if (data.Success) {
                            ShowMessage("success", data.Message);
                            getEmployeeGrid();
                        }
                        else {
                            ShowMessage("error", data.Message);

                        }
                        hideLoaderFrame();
                    }
                })
            }
            else
            {
                //ShowMessage("error", LanguageData[0].Pleaseselect);
                hideLoaderFrame();
            }
            //});
        }
        else {
            ShowMessage("error", LanguageData[0].PleaseselectVisit);
            hideLoaderFrame();

        }
    }
    else
    {

        ShowMessage("error", LanguageData[0].Pleaseselect);
        hideLoaderFrame();
    }
}
function ValidateDateSelection(data) {    
    var isvalid = true;
    $(data).each(function () {
        if ($(this).find('.datepicker').val().length <= 0) {
            isvalid = false;
            return false;
        }

    });  
    return isvalid;
}
function getEmployeeGrid() {

    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }

    var employeeStatus = $("#employeeStatusddl").val();
    var ddlDropInsYearID = $("#ddlPDRPYears").val();
    var ddlDropInsYearTxt = $("#ddlPDRPYears option:selected").text();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    if (ddlDropInsYearID == "") {
        ddlDropInsYearID = 0
    }
    //var IncludeInactive = $("#includeInActiveChk").is(':checked');
    //var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        "sAjaxSource": "/PDRP/Observations/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt,
        "aoColumns": [
            { "mData": "Default", "bVisible": false },

            { "mData": "Select", "bVisible": true, "sTitle": "<input type=checkbox id=chkSelevctAll onclick=SelectAll()>", "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "EmployeeAlternativeID", "bVisible": true, "sTitle": LanguageData[0].ID, "width": "6%", "sType": "Int", "className": "center-text-align" },
            { "mData": "FirstName", "sTitle": LanguageData[0].FirstName, "width": "15%" },
            { "mData": "LastName", "sTitle": LanguageData[0].LastName, "width": "15%" },
            { "mData": "KPICount", "className": "hidden" },
            { "mData": "VisitDate", "sTitle": LanguageData[0].VisitDateT, "bSortable": true, "width": "15%" },
            { "mData": "VisitTime", "sTitle": LanguageData[0].VisitTimeT, "width": "15%" },
            { "mData": "PreObservationDueDate", "sTitle": LanguageData[0].PreObservationDueDate, "bSortable": true, "width": "15%" },
            { "mData": "EmployeeId", "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "Action", "sTitle": "Action", "bSortable": true },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/Observations/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&DropInYear=" + ddlDropInsYearID + "&DropInsYearTxt=" + ddlDropInsYearTxt,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            //$(".datepicker").datepicker("destroy");
            $(".datepicker").datepicker({
                changeMonth: true,
                dateFormat: HRMSDateFormat,
                format: HRMSDateFormat,
                changeYear: false,
                autoclose: true
            }).on('changeDate', function (ev) {
                $(this).datepicker('hide');

            })

           

            $('.timepicker').timepicker({
                showMeridian: true,
                showInputs: true,
                minuteStep: 5,
            });
            //KPICheck();
            bindSelectpicker('.datepicker');
            if (LanguageData[0].direction == "rtr") {
                $('.dataTables_length').css("float", "right");
                $('.dataTables_filter').css("float", "left");
                $('.dataTables_filter label').css("float", "left");
            }
        },
        "language": {
            "loadingRecords": "<span class='loader'></span>",
            "url": "/Content/MultiLang/" + $("#culture").val() + ".json"
        }
    });

}
function KPICheck() {

    var vKPICount = $(".KPICount").text();
    //alert(vKPICount);
    if (vKPICount != "") {
        if (vKPICount == 0) {
            //ShowMessage("error", "KPI is not Set for the current year.</br> You cannot process Appraisal for this year");

            $("#lblKPI").text(LanguageData[0].KPIck);

            $("#btnDateApply").addClass("disabled")
            $(".btnSave").addClass("disabled")
        }
        else {

            $("#lblKPI").text("");
            $("#btnDateApply").removeClass("disabled")
            $(".btnSave").removeClass("disabled")
        }
    }
    else {
        $("#lblKPI").text("");
        $("#btnDateApply").removeClass("disabled")
        $(".btnSave").removeClass("disabled")
    }
}
function ChkChange(source) {  
    var closesetRow = $(source).closest('tr');
   // var vdatepicker = $(closesetRow).find('.datepicker').val();
    //var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if ($(source).prop("checked") == true) {
        // if (vdatepicker.length > 0) {

        var employeeId = $(closesetRow).addClass('IsEdit');
        //}
    }
    else if ($(source).prop("checked") == false) {
        var employeeId = $(closesetRow).removeClass('IsEdit');
    }








}
function DateAdd(source) {
    var closesetRow = $(source).closest('tr');
    var vdatepicker = $(closesetRow).find('.datepicker').val();
    var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if (vdatepicker.length > 0) {
        vchkEmployeeId.prop('checked', true);
        var employeeId = $(closesetRow).addClass('IsEdit');
    }


}

function SaveSingleEmployeeHeirarchy(source) {
    var closesetRow = $(source).closest('tr');
    var employeeId = $(closesetRow).data().employeeid;
    var updatedData = [];
    $(closesetRow).find(".manger").each(function (index, obj) {
        var groupId = $(obj).data().groupid;
        var oldMangerId = $(obj).data().managerid;
        var managerId = $("#ddlManger_" + groupId + "_" + employeeId + "").val();
        if ((oldMangerId != "" && oldMangerId != "0") || (managerId != "" && managerId != '0')) {
            updatedData.push({
                EmployeeID: employeeId,
                ManagerID: managerId,
                GroupID: groupId
            });
        }
    });
    if (updatedData.length > 0) {
        pageLoaderFrame();
        $.ajax({
            url: "/EmployeeBasedHierarchy/SaveMultipleEmployees",
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            data: JSON.stringify({ 'employeeHeirarchyList': updatedData }),
            success: function (data) {
                if (data.Success) {
                    ShowMessage("success", data.Message);

                    var closesetRow = $(source).closest('tr');
                    var employeeId = $(closesetRow).data().employeeid;
                    $(closesetRow).find(".manger").each(function (index, obj) {
                        var groupId = $(obj).data().groupid;
                        $(obj).data().managerid = $("#ddlManger_" + groupId + "_" + employeeId).val();
                        var managerId = $(obj).data().managerid;
                        var managerName = managerId != "" ? $("#ddlManger_" + groupId + "_" + employeeId + " option[value='" + managerId + "']").text() : "";
                        $(obj).html(managerName);
                    });
                    $(closesetRow).find(".action").html("<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHeirarchy(this)' title='Edit' ><i class='fa fa-pencil'></i> </a> ");
                    //+
                    //   "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"DeleteEmployeeHierachy(this," + employeeId + ")\" title='Delete'><i class='fa fa-times'></i> </a>");
                    $(closesetRow).removeClass("IsEdit");
                    hideLoaderFrame();
                }
                else {
                    ShowMessage("error", data.Message);
                    hideLoaderFrame();
                }
            }
        });
    }
    else {
        ShowMessage("error", LanguageData[0].Pleaseselect);
        hideLoaderFrame();
    }
}
function ApplytoallSelections() {

    var VisitDate = $('#txtVisitDate').val().trim();
    var VisitTime = $('#txtVisitTime').val().trim();
    var Ch = 0;

    if (VisitDate.length > 0 && VisitTime.length > 0) {
        var table = $("#tbl_employee").DataTable();
        var UpdatedTr1 = table.$(".odd");
        var UpdatedTr2 = table.$(".even");
        $(UpdatedTr1).each(function () {
            var row = $(this);
            console.log(row);
            if (row.find('input[type="checkbox"]').prop('disabled')) {

                //** No Action need
            }
                //*** Removed On Request
                //else if (row.find('input[type="checkbox"]').is(':checked') &&
                //   row.find('.datepicker').val().length <= 0) {
            else if (row.find('input[type="checkbox"]').is(':checked')) {

                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);

                $(this).find('.timepicker').val(VisitTime);
                Ch = 1;
            }

        });
        $(UpdatedTr2).each(function () {
            var row = $(this);
            console.log(row);

            if (row.find('input[type="checkbox"]').prop('disabled')) {

                //** No Action need
            }
                //*** Removed On Request
                //else if (row.find('input[type="checkbox"]').is(':checked') &&
                //   row.find('.datepicker').val().length <= 0) {
            else if (row.find('input[type="checkbox"]').is(':checked')) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);

                $(this).find('.timepicker').val(VisitTime);
                Ch = 1;
            }
        });

        if (Ch == 0) {
            ShowMessage("error", LanguageData[0].Pleaseselect);
        }
    }
    else {
        ShowMessage("error", LanguageData[0].PleaseselectVisit);
    }
}


function PreObservation(id, observationyear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, ObservationYear: observationyear, isAddMode: 1 },
        url: '/PDRP/Observations/SetObservationActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/Observations/PreObservation/";
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}

function PostObservation(id, observationyear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, ObservationYear: observationyear, isAddMode: 1 },
        url: '/PDRP/Observations/SetObservationActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/Observations/PostObservation/";
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}
function ObservationEvaluation(id, observationyear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { EMployeeID: id, ObservationYear: observationyear, isAddMode: 1 },
        url: '/PDRP/Observations/SetObservationActiveEmployeeIDSesson',
        success: function (data) {
            if (data.success == true) {
                window.location.href = "/PDRP/Observations/ObservationEvaluation/";
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}
