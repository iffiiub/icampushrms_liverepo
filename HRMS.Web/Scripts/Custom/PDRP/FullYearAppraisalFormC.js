﻿var selectedValues = [];
var Competencies1 = [];
var Competencies2 = [];

function OverallAppraisalScore() {
    var ddlDropInsYearID = $("#YearId").val();
    var performanceGroupId = $("#PerformanceGroupId").val();
    var scorevalues = [];

    var CompetenciesTotAvg = ((parseFloat($("#ProfessionalTotalScore1").val()) + parseFloat($("#BehavioralTotalScore1").val())) / 2).toFixed(2);
    $("#CompetenciesAVG").val(CompetenciesTotAvg);
    $("#divCompetenciesAVG").html(CompetenciesTotAvg);
    //scorevalues.push($("#BusinessTargetsTota1").val());
    
    //scorevalues.push($("#ProfessionalTotalScore1").val());
   
    var scorevalues11 = parseFloat($("#BusinessTargetsTota1").val());
    //*** Naresh 2020-03-01 We need to pass average of professional and behavioral score.
    var scorevalues21 = CompetenciesTotAvg;// parseFloat($("#ProfessionalTotalScore1").val());
    pageLoaderFrame();
    $.ajax({
        type: 'POST',
        data: { Scorevalues1: scorevalues11, Scorevalues2: scorevalues21, FormulaYearID: ddlDropInsYearID, PerformanceGroupId: performanceGroupId },
        traditional: true,
        url: '/PDRP/AnnualAppraisal/CalculateFullScore',
        success: function (data) {
            if (data.Formula == '')
            {
                ShowMessage("error", "There is no Formula Found.");
            }
            $("#OverallScoreFormula").val(data.Formula);
            $("#OverallAppraisalScore").val(data.OverallScore.toFixed(2));
            $("#divOverallScore").text(data.OverallScore.toFixed(2));
            hideLoaderFrame();


        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred" + data);
            globalFunctions.onFailure(data, xhr, status);
            hideLoaderFrame();

        }
    });

}
function SavePage(e) {
    var Errormsg = "";
    delete (selectedValues)
    delete (Competencies1)
    delete (Competencies2)
    var invalidKPI = 0;
    selectedValues = [];
    Competencies1 = [];
    Competencies2 = [];
    var Tweight = 0;
    var businessbargetno = 1;
    var ErrMess = "";
    var wid180 = "";
    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var businesstargetid = row.data("businesstargetid");
        var businessTargetDetails = row.find('.businesstarget').val();
        var accomplishments = row.find('.accomplishments').val();
        var weight = row.find('.weight').val();
        wid180 = row.find('.wid180').val();
        //*** F5
        if (wid180 == "N/A") {
            wid180 = "0";
        }
        var score = row.find('.Score').val();
        if (weight != "") {
            Tweight = parseInt(Tweight) + parseInt(weight);
        }
        selectedValues.push({ BusinessTargetNo: businessbargetno, businesstargetid: businesstargetid, businessTargetDetails: businessTargetDetails, Accomplishments: accomplishments, weight: weight, Rating: wid180, Score: score });
        //if (((score == "0" || score == "" || score == "0.00") && weight != "")) {
        if (((wid180 == '' && wid180 != '0') && weight != "")) {
            invalidKPI = 1;
        }
        //***F24
        if (businessTargetDetails == "" && wid180 != "") {
            if (ErrMess != "") {
                ErrMess = ErrMess + "</br>";
            }
            ErrMess = ErrMess + "Invalid Business Targets Description at Row No: " + businessbargetno;
        }
        if (businessTargetDetails != "" && wid180 == "") {

            if (ErrMess != "") {
                ErrMess = ErrMess + "</br>";
            }
            ErrMess = ErrMess + "Invalid Business Targets Rating at Row No: " + businessbargetno;
        }

        businessbargetno = parseInt(businessbargetno) + 1;
 
    });
    
    var Validating = 0;
    Competencies1 = [];
    $(".clsProfessionalCompetencies").each(function () {
        var row = $(this);
        var wid182 = row.find('.wid182').val();

        //*** F5
        if (wid182 == "N/A") {
            wid182 = 0;
        }
        var competencyid = row.find('.wid182').data("competencyid");
        var vNo = row.find('td:eq(0)').text();
        Competencies1.push({ No: parseInt(vNo), Score: parseInt(wid182), CompetencyID: competencyid })
        //if (wid182 == '' || wid182 == '0') {
        if (wid182 == '' && wid182 != '0') {
            if (e != 0) {
                Validating = 5;
            }
        }
    });
    Competencies2 = [];
    $(".clsBehavioralCompetencies").each(function () {
        var row = $(this);
        var wid181 = row.find('.wid181').val();
        //*** F5
        if (wid181 == "N/A") {
            wid181 = 0;
        }
        var competencyid = row.find('.wid181').data("competencyid");
        var vNo = row.find('td:eq(0)').text();
        Competencies2.push({ No: parseInt(vNo), Score: parseInt(wid181), CompetencyID: competencyid })
        //if (wid181 == '' || wid181 == '0') {
        if (wid181 == '' && wid181 != '0') {
            if (e != 0) {
                Validating = 6;
            }
        }
    });
    if (ErrMess != "") {
        ShowMessage("error", ErrMess);
    }
    else {
        if ((invalidKPI == 1) && e != 0) {//*** F3
            if (invalidKPI == 1) {
                ShowMessage("error", "Invalid KPI");
            }
            else {
                ShowMessage("error", "Total Weightage of KPIs should be 100%");
            }
        }
        else {
            //*** F3



            if (e != 0) {
                var grade = $("#txtGrade").val();
                var lineManagerEvaluationComments = $("#lineManagerEvaluationComments").val();
                var TrainingNeeds = $("#TrainingNeeds").val();
                var lineManagerMidYearPerformanceComments = $("#lineManagerMidYearPerformanceComments").val();
                var employeeMidYearPerformanceComments = $("#employeeMidYearPerformanceComments").val();

                //*** F24 LM Comment and Employee Comment
                var IsLM = $("#IsFLineManager").val();
                var IsEmp = $("#IsFEmployee").val();

                var MyThreeKeyAchivements = $("#MyThreeKeyAchivements").val();
                var MyTwoDevelopmentAreas = $("#MyTwoDevelopmentAreas").val();
                var EmployeeCommentsonFullYearPerformance = $("#EmployeeCommentsonFullYearPerformance").val();
                var Strengths = $("#Strengths").val();
                var FullTrainingNeeds = $("#FullTrainingNeeds").val();
                var LineManagerCommentsonFullYearPerformance = $("#LineManagerCommentsonFullYearPerformance").val();

                if (grade == '' || grade == undefined) {
                    Validating = 1;
                }
                else if (selectedValues.length == 0) {

                    Validating = 2;
                }
                    //*** F24 Only LM & Employee Can Fill this field so no need to check hear
                //else if (lineManagerEvaluationComments == '' || TrainingNeeds == '' || lineManagerMidYearPerformanceComments == '' || employeeMidYearPerformanceComments == '') {
                //    Validating = 3;
                //}
                else if (((MyThreeKeyAchivements == '' || MyTwoDevelopmentAreas == '' || EmployeeCommentsonFullYearPerformance == '') && IsEmp == 1) ||
                   ((Strengths == '' || FullTrainingNeeds == '' || LineManagerCommentsonFullYearPerformance == '') && IsLM == 1)) {
                    Validating = 4;
                }


            }

            if (Validating == 1) {
                $("#txtGrade-validationMsg").css("color", "red");
                $("#txtGrade-validationMsg").text("This field is mandatory");
                $("#txtGrade-validationMsg").show();
            }
            else if (Validating == 2) {
                ShowMessage("error", "Please add business target details.");
            }
                //*** F24 Only LM & Employee Can Fill this field so no need to check hear
            //else if (Validating == 3) {
            //    ShowMessage("error", "Section 4:Mid Year Appraisal All fields are mandatory");
            //}
            else if (Validating == 4) {
                ShowMessage("error", "Section 7:Full Year Appraisal All fields are mandatory");
            }
            else if (Validating == 5) {
                ShowMessage("error", "Select All Professional Competencies Ratings");
            }
            else if (Validating == 6) {
                ShowMessage("error", "Select All Behavioral Competencies Ratings");
            }
            else {
                $("#txtGrade-validationMsg").text("");
                $("#txtGrade-validationMsg").hide();
                //alert(Validating);
                if ($("#BusinessTargetsTota1").val() == 0) {
                    Errormsg = Errormsg + "</br>Business Targets Total Score cannot be zero!"
                }
                else if ($("#ProfessionalTotalScore1").val() == 0) {
                    Errormsg = Errormsg + "</br>Core Competencies Total Score cannot be zero!"
                }
                else if ($("#OverallAppraisalScore").val() == 0) {
                    Errormsg = Errormsg + "</br>Overall Score cannot be zero!"
                }
                else if ($("#OverallScoreFormula").val() == 0) {
                    Errormsg = Errormsg + "</br>Overall Score Formula not found!"
                }

                if (Errormsg != "" && e != 0) {
                    ShowMessage("error", Errormsg);
                }
                else {

                    //if (selectedValues.length > 0 && Competencies1.length>0) {

                    pageLoaderFrame();
                    $.ajax({
                        url: "/PDRP/AnnualAppraisal/SaveFullYearAppraisalFormData",
                        type: 'POST',
                        data: { model: JSON.stringify(getFullYearAppraisalModelData()), PerformanceGroupId: $("#PerformanceGroupId").val() },
                        success: function (result) {
                            //ShowMessage(result.CssClass, result.Message);
                            selectedValues = [];
                            if (result.Success) {
                                if (e == 0) {
                                    ShowMessage(result.CssClass, result.Message);
                                    hideLoaderFrame();
                                    //*** Naresh 2020-03-03 Do not redirect to list page on save.
                                    //if (result.Success) {
                                    //    setTimeout(function () { BackToTaskList(); }, 1000);
                                    //    hideLoaderFrame();
                                    //}
                                    //else {
                                    //    hideLoaderFrame();
                                    //}

                                }
                                else if (e == 3) {//*** F3 Reintialized

                                    var performanceGroupId = $("#PerformanceGroupId").val();
                                    var AnnualGoalSettingFormCId = $("#AnnualGoalSettingFormCId").val();
                                    var fullyearformid = result.InsertedRowId;
                                    var employeeId = $("#EmployeeID").val();
                                    var formId = 38;
                                    var goalSettingId = $("#GoalSettingId").val();
                                    var comments = $("#txtComments").val();

                                    $.ajax({
                                        url: "/PDRP/AnnualAppraisal/ReintializeFullYear",
                                        type: 'POST',
                                        data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                                        success: function (result) {
                                            ShowMessage(result.CssClass, result.Message);
                                            selectedValues = [];
                                            if (result.Success) {
                                                $("#prebtnSubmit").removeClass("btnSubmit");
                                                $("#prebtnSubmit").addClass("LMbtnSubmit");
                                                setTimeout(function () { BackToTaskList(); }, 1000);
                                                hideLoaderFrame();

                                            }
                                            else {
                                                hideLoaderFrame();
                                            }
                                        }
                                    });
                                }
                                else { //*** Submit
                                    //$("#FormMode").val(1) 
                                    var performanceGroupId = $("#PerformanceGroupId").val();
                                    var AnnualGoalSettingFormCId = $("#AnnualGoalSettingFormCId").val();
                                    var fullyearformid = result.InsertedRowId;
                                    var employeeId = $("#EmployeeID").val();
                                    var formId = 38;
                                    var goalSettingId = $("#GoalSettingId").val();
                                    var comments = $("#txtComments").val();
                                    $.ajax({
                                        //url: "/PDRP/GoalSetting/ApproveMidForm",
                                        url: "/PDRP/AnnualAppraisal/ApproveFullForm",
                                        type: 'POST',
                                        data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                                        success: function (result) {
                                            ShowMessage(result.CssClass, result.Message);
                                            selectedValues = [];
                                            if (result.Success) {
                                                $("#prebtnSubmit").removeClass("btnSubmit");
                                                $("#prebtnSubmit").addClass("LMbtnSubmit");
                                                setTimeout(function () { BackToTaskList(); }, 1000);
                                                hideLoaderFrame();

                                            }
                                            else {
                                                hideLoaderFrame();
                                            }
                                        }

                                    });
                                }

                            }
                            else {
                                ShowMessage(result.CssClass, result.Message);
                                hideLoaderFrame();
                            }
                        }

                    });
                    //}
                    //else
                    //    ShowMessage("error", "Please add business target details.");
                }
            }
        }
    }

}
$(document).ready(function () {
 
    $(document).on("click", "#prebtnSave, #tprebtnSave", function () {
        
        SavePage(0);
    });
    //*** Employee Submiting
    $(document).on("click", ".btnSubmit", function () {
   
        var isitEditable = $("#isEditable").val()
        if (($("#FullFormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(1);
        }
        else {
            //$("#FormMode").val(1) 
            var performanceGroupId = $("#PerformanceGroupId").val();
            var AnnualGoalSettingFormCId = $("#AnnualGoalSettingFormCId").val();

            var fullyearformid = $("#ID").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 38;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                //url: "/PDRP/GoalSetting/ApproveMidForm",
                url: "/PDRP/AnnualAppraisal/ApproveFullForm",
                type: 'POST',
                data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }

            });
        }
    });
    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });
    $('.btnReject').click(function () {

        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();

                pageLoaderFrame();
                //*** F25 On Reject new task create for Employee
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/PDRP/AnnualAppraisal/RejectPDRPNonTeachForm',
                    data: { formProcessID: formprocessid, comments: comments, EmployeeID: $("#EmployeeID").val(), FormID: 42, FormInstanceID: $("#GoalSettingId").val() },
                    success: function (result) {

                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                            hideLoaderFrame();

                        }
                        else {
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.message);
                    }
                });
            });
        } else {
            ShowMessage('error', 'Comments field is mandatory.');
            return false;
        }
    });
    //*** F3
    $(document).on("click", ".btnReSubmit", function () {
        var isitEditable = $("#isEditable").val()
        if (($("#FullFormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(3);
        }
        else {

            var performanceGroupId = $("#PerformanceGroupId").val();
            var fullyearformid = $("#ID").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 42;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/AnnualAppraisal/ReintializeFullYear",
                type: 'POST',
                //data: { id: fullyearformid, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {



                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }
            });

        }

    });
    $(document).on("click", ".btnEmpSignOffSubmit", function () {
        var performanceGroupId = $("#PerformanceGroupId").val();
        var AnnualGoalSettingFormCId = $("#AnnualGoalSettingFormCId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 42;
        var requestId = $("#RequestId").val();
        var goalSettingId = $("#GoalSettingId").val();
        //*** F21 SignOff tEXT
        //*** F24 Need to show in Signing off also so remove this condition
        var comments = $("#txtComments").val();
        //var comments = "SignOff";

        //*** F24 LM Comment and Employee Comment
        var MyThreeKeyAchivements = $("#MyThreeKeyAchivements").val();
        var MyTwoDevelopmentAreas = $("#MyTwoDevelopmentAreas").val();
        var EmployeeCommentsonFullYearPerformance = $("#EmployeeCommentsonFullYearPerformance").val();

        pageLoaderFrame();

        $.ajax({
            url: "/PDRP/AnnualAppraisal/SubmitEmployeeSignOffFullYeraForms",
            type: 'POST',
            data: {
                id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, performanceGroupId: performanceGroupId, requestId: requestId, goalSettingId: goalSettingId, comments: comments,
                MyThreeKeyAchivements: MyThreeKeyAchivements, MyTwoDevelopmentAreas: MyTwoDevelopmentAreas, EmployeeCommentsonFullYearPerformance: EmployeeCommentsonFullYearPerformance
            },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                selectedValues = [];
                if (result.Success) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
                hideLoaderFrame();
            }
        });
    });
    FullYearControls();

    $(".wid180").on('change', function () {       
        var totalScore = 0;
        $("#tblBusinessTargets #ProficiencyScore").each(function () {
            var item_Weight = $(this).closest('tr').find('#item_Weight').val();
            var Score = $(this).closest('tr').find('#item_Score');           
            if (parseInt(item_Weight) > 0) {
                //*** F5                
                var wid180val = $(this).val();
                if (wid180val == "N/A" || wid180val == "") {
                    wid180val = 0;
                }
                var result = parseFloat((item_Weight / 100) * parseFloat(wid180val)).toFixed(2);
                
                $(this).closest('tr').find('#divScore').html(result);
                Score.val(result);
                totalScore = parseFloat(parseFloat(totalScore) + parseFloat(result)).toFixed(2);          
            }

        });
        $("#BusinessTargetsTota").val(totalScore);
        $("#divBusinessTargetsTota").html(totalScore);
        $("#BusinessTargetsTota1").val(totalScore);
        $("#divBusinessTargetsTota1").html(totalScore);

        OverallAppraisalScore();
    });

    $(".wid181").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblBehavioralCompetenciesFormC #ProficiencyScore").each(function () {
            //*** F5
            var wid181val = $(this).val();
            if (wid181val == "N/A") {
                wid181val = 0;
            }
            if (parseFloat(wid181val) > 0) {
                var sc = parseFloat(wid181val);
                //totalScore = parseFloat(totalScore) + parseFloat(sc);
                totalScore = parseFloat(parseFloat(totalScore) + parseFloat(wid181val)).toFixed(2);
                cnt++;
            }

        });

        if (totalScore > 0 && cnt > 0) {
            $("#BehavioralTotalScore").val(parseFloat(totalScore / cnt).toFixed(2));
            $("#divBehavioralTotalScore").html(parseFloat(totalScore / cnt).toFixed(2));
            $("#BehavioralTotalScore1").val(parseFloat(totalScore / cnt).toFixed(2));



            OverallAppraisalScore();
        }
        else {
            $("#BehavioralTotalScore").val(parseFloat(0).toFixed(2));
            $("#divBehavioralTotalScore").html(parseFloat(0).toFixed(2));
            $("#BehavioralTotalScore1").val(parseFloat(0).toFixed(2));
        }

    });
    $(".wid182").on('change', function () {
       
        var totalScore = 0;
        var cnt = 0;
        $("#tblProfessionalCompetenciesFormC #ProficiencyScore").each(function () {
            //*** F5
            var wid182val = $(this).val();
            if (wid182val == "N/A") {
                wid182val = 0;
            }
            if (parseFloat(wid182val) > 0) {
              
                totalScore = parseFloat(parseFloat(totalScore) + parseFloat(wid182val)).toFixed(2);
                cnt++;
            }

        });

        if (totalScore > 0 && cnt > 0) {
            $("#ProfessionalTotalScore").val(parseFloat(totalScore / cnt).toFixed(2));
            $("#divProfessionalTotalScore").html(parseFloat(totalScore / cnt).toFixed(2));
            $("#ProfessionalTotalScore1").val(parseFloat(totalScore / cnt).toFixed(2));
            $("#divProfessionalTotalScore1").html(parseFloat(totalScore / cnt).toFixed(2));
            OverallAppraisalScore();
        }
        else {
            $("#ProfessionalTotalScore").val(parseFloat(0).toFixed(2));
            $("#divProfessionalTotalScore").html(parseFloat(0).toFixed(2));
            $("#ProfessionalTotalScore1").val(parseFloat(0).toFixed(2));
            $("#divProfessionalTotalScore1").html(parseFloat(0).toFixed(2));
        }
    });
  
    //*** Naresh 2020-03-13 Display last comment
    //if ($('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
    //*** Danny to block at REject mode
    if ($('#ReqStatusId').val() < 3 && $('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
        $('#txtComments').val($('#lblComments').data('last-comment'))
});


function getFullYearAppraisalModelData() {

    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinessTargetsComments: $("#businessTargetComments").val(),
        CoreCompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        JsonString1: JSON.stringify(Competencies1),
        JsonString2: JSON.stringify(Competencies2),
        ID: $("#AnnualGoalSettingFormCId").val(),
        PerformanceGroupId: $("#PerformanceGroupId").val(),
        JobGrade: $("#txtGrade").val(),

        //***F2
        //JsonStringCurrRatingScales: JSON.stringify(CurrRatingScales),
        GoalSettingId: $("#GoalSettingId").val(),
        EmployeeName: $("#EmployeeName").val(),
        Company: $("#Company").val(),
        Designation: $("#Designation").val(),
        RequestEmail: $("#RequestEmail").val(),
        Department: $("#Department").val(),
        LineManager: $("#LineManager").val(),
        DOJ: $("#DOJ").val(),
        FormState: $("#FormMode").val(),
        MidFormMode: $("#MidFormMode").val(),
        FullFormMode: $("#FullFormMode").val(),
        
        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds: $("#TrainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val(),

        MyThreeKeyAchivements: $("#MyThreeKeyAchivements").val(),
        MyTwoDevelopmentAreas: $("#MyTwoDevelopmentAreas").val(),
        EmployeeCommentsonFullYearPerformance: $("#EmployeeCommentsonFullYearPerformance").val(),
        Strengths: $("#Strengths").val(),
        FullTrainingNeeds: $("#FullTrainingNeeds").val(),
        LineManagerCommentsonFullYearPerformance: $("#LineManagerCommentsonFullYearPerformance").val(),

        BusinessTargetsTota: $("#BusinessTargetsTota").val(),
        ProfessionalCompetenciesAVG: $("#ProfessionalTotalScore").val(),
        BehavioralCompetenciesAVG: $("#BehavioralTotalScore").val(),
        CompetenciesAVG: $("#CompetenciesAVG").val(),
        OverallAppraisalScore: $("#OverallAppraisalScore").val(),
        OverallScoreFormula: $("#OverallScoreFormula").val(),
        FormSubmit: $("#FormSubmit").val(),
        RequestId: $("#RequestId").val(),
        //*** Naresh 2020-03-11 Save comments on save
        Comments: $("#txtComments").val()

    };

    return businessTargetModel;
}

function FullYearControls() {
    var PeriodId = $('#PeriodId').val();
    if (PeriodId == 2) {
        $('#item_Score').bind('keydown', function (e) {
            e.preventDefault();
        });
        $('#item_BusinessTargetDetails').bind('keydown', function (e) {
            e.preventDefault();
        });
        $('#item_Weight').bind('keydown', function (e) {
            e.preventDefault();
        });
    }
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}