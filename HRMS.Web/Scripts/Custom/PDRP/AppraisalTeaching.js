﻿var data = {
    "items": [{
        "id": "en-gb",
        "direction": "ltr",
        "DropInsDone": "3 DropIns Done?",
        "LessonObservationDone": "Lesson Observation Done?",
        "EmpID": "Emp ID",
        "FirstName": "First Name",
        "LastName": "Last Name",
        "EvaluationDate": "Evaluation Date",
        "EvaluationDueDate": "Evaluation Due Date",
        "KPIck": "KPIs not set for selected year.",
        "Commentsfield":"Comments field is mandatory.",
        "Pleaseselectone": "Please select at least one record.",
        "Pleaseenter":"Please enter valid evaluation date & evaluation due date",


        "PositionName": "Position Name",
        "DepartmentName": "Department Name",
        "Action": "Action",
        
        "RollStNo": "No-of students present(Male/Female) cannot be greater than No-of students on Roll",
        "TechErr": "Technical Error!",
        "ID": "ID",
        "VisitDateT": "Visit Date",
        "RequestID": "Request ID",
        "IsAcknowledged": "Is Acknowledged?",
        "Total": "Total",
        "OverAllScore": "Over All Score",
        "DropInsFor": "Drop Ins - For:",
        "Sometechnical": "Some technical error occurred"

    }, {
        "id": "ar",
        "direction": "rtr",
        "DropInsDone": "3 تم إنجاز DropIns؟",
        "LessonObservationDone": "مراقبة الدرس انتهى؟",
        "EmpID": "معرف Emp",
        "FirstName": "الاسم الاول",
        "LastName": "الكنية",
        "EvaluationDate": "تاريخ التقييم",
        "EvaluationDueDate": "تاريخ استحقاق التقييم",
        "KPIck": "مؤشرات الأداء الرئيسية غير محددة للسنة المحددة.",
        "Commentsfield": "حقل التعليقات إلزامي.",
        "Pleaseselectone":"يرجى تحديد سجل واحد على الأقل.",
        "Pleaseenter": "الرجاء إدخال تاريخ تقييم صالح وتاريخ استحقاق التقييم",




        "PositionName": "اسم المركز",
        "DepartmentName": "اسم القسم",
        "Action": "عمل",
        
        "RollStNo": "لا يمكن أن يكون عدد الطلاب الحاضرين (ذكور / إناث) أكبر من عدد الطلاب الدائمين",
        "TechErr": "خطأ تقني!",
        "ID": "هوية شخصية",
        "VisitDateT": "تاريخ الزيارة",
        "RequestID": "طلب معرف",
        "IsAcknowledged": "يتم الاعتراف؟",
        "Total": "مجموع",
        "OverAllScore": "المجموع النهائي",
        "DropInsFor": "انخفاض الوظائف - من أجل:",
        "Sometechnical": "حدث خطأ فني"
    }]
};

var LanguageData = $.grep(data.items, function (element, index) {
    return element.id == $('#culture').val();
});


function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}
function PDRPLoadLanguage(e) {
    pageLoaderFrame();

    $.ajax({
        url: '/PDRP/AppraisalTeaching/SetCulture',
        data: { languageid: e },
        type: 'GET',
        success: function (data) {
            //*** Naresh 2020-04-20 Open the other language in new tab
            var win = window.open(location.href, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it

            }
            //location.reload();
            hideLoaderFrame();
        },
        error: function (data, xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            ShowMessage("error", LanguageData[0].TechErr);
            hideLoaderFrame();
        }
    });
}
function ViewObservation(id) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        url: '/PDRP/Observations/SetViewObservationSesson',
        success: function (data) {
            if (data.success == true) {


                window.open("/PDRP/Observations/ObservationEvaluation/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}
function DateAdd(source) {
    var closesetRow = $(source).closest('tr');
    var vtxtEvlDate = $(closesetRow).find('#txtEvlDate').val();
    var vtxtEvlDueDate = $(closesetRow).find('#txtEvlDueDate').val();
    var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')
    
    if (vtxtEvlDate.length > 0 && vtxtEvlDueDate.length > 0) {
        vchkEmployeeId.prop('checked', true);
        var employeeId = $(closesetRow).addClass('IsEdit');
    }


}
function ViewDeductionDetails(TeacherID, TeacherName, DropInYearID, DropInYear) {
    //InstallmentPayDeductionId = PayDeductionId;
    //alert(DropInYear);
    //$("#modal_Loader3").load("/PDRP/DropIns/ViewAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYearID, function () {
    //    $("#myModal3").modal("show");
    //    $(".modal-dialog").attr("style", "width:750px;");
    //    $("#modal_heading3").html("<i class='fa fa-user'></i> " + TeacherName + "'s Drop Ins - For: " + DropInYear);
    //    $(".close").removeClass('hidden');
    //    getTop3DropInsListGrid(TeacherID, DropInYearID);
    //});   
    var companyid = $('#hdnCompanyID').val();  
    $("#modal_Loader3").load("/PDRP/DropIns/ViewAllDropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYearID, function () {
        $("#myModal3").modal("show");
        if ($('#culture').val() == "ar") {
            $("#myModal3").attr("dir", "rtl");
        }
        $(".modal-dialog").attr("style", "width:750px;");
        $("#modal_heading3").html("<i class='fa fa-user'></i> " + TeacherName + "'s Drop Ins - For: " + DropInYear);
        $(".close").removeClass('hidden');
        getTop3DropInsListGrid(TeacherID, DropInYearID, companyid);
    });
}

function SelectAll() {



    var table = $("#tbl_employee").DataTable();
    var UpdatedTr1 = table.$(".odd");
    var UpdatedTr2 = table.$(".even");
    $(UpdatedTr1).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', false);
            }

        }
        else {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', true);
            }

        }
        ChkChange(chbox);

    });
    $(UpdatedTr2).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.prop('checked')) {
            chbox.prop('checked', false);

        }
        else {
            if (chbox.is(':disabled')) {
            }
            else {
                chbox.prop('checked', true);
            }

        }
        ChkChange(chbox);

    });

}
function ChkChange(source) {   
    var closesetRow = $(source).closest('tr');
    var vdatepicker = $(closesetRow).find('.datepicker').val();
    //var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if ($(source).prop("checked") == true) {
      //  if (vdatepicker.length > 0) {

            var employeeId = $(closesetRow).addClass('IsEdit');
       // }
    }
    else if ($(source).prop("checked") == false) {
        var employeeId = $(closesetRow).removeClass('IsEdit');
    }


}
function ValidateDateSelection(data) {
    var isvalid = true;  
    $(data).each(function () {
        //If  Evaluavtion or EValuation Due Date is not selected
        if ($(this).find('.datepicker').val().length <= 0 || $(this).find('.EvlDate').val().length <= 0) {
            isvalid = false;
            return false;
        }

    });
    return isvalid;
}
function getTop3DropInsListGrid(TeacherID, DropInYear, companyid) {

    //alert(TeacherID);
    oTableChannel = $('#tbl_AllDropIns').dataTable({
        "sAjaxSource": "/PDRP/DropIns/GetTop3DropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear + "&CompanyID=" + companyid,
        "aoColumns": [


            { "mData": "ID", "sTitle": LanguageData[0].ID, "bVisible": false, "width": "6%", "className": "center-text-align" },
            { "mData": "VisitDATE", "sTitle": LanguageData[0].VisitDateT, "width": "30%" },
             { "mData": "RequestID", "sTitle": LanguageData[0].RequestID, "width": "30%" },
             { "mData": "IsAcknowledged", "sTitle": LanguageData[0].IsAcknowledged, "width": "30%", "className": "center-text-align" },
            { "mData": "Total", "sTitle": LanguageData[0].Total, "width": "30%" },
            { "mData": "OverAllScore", "sTitle": LanguageData[0].OverAllScore, "sWidth": "10%" },
            //{ "mData": "MobileNumber", "sTitle": "Mobile Number", "sWidth": "10%" },           //Changed Telephone to Mobile Number
            //{ "mData": "PositionName", "sTitle": "Position Name", "sWidth": "13%" },
            //{ "mData": "Department", "sTitle": "Department Name", "sWidth": "13%" },
            //{ "mData": "HireDate", "sTitle": "Date Of Joining", "sWidth": "10%", "sType": "date-uk" },//Task#9120 2019-02-11
            //{ "mData": "DateOfBirth", "sTitle": "Date Of Birth", "sWidth": "9%", "sType": "date-uk" },
            //{ "mData": "isActive", "sTitle": "Status", "bSortable": false, "sWidth": "6%", "sClass": ShowStatusColumnClass },
            //{ "mData": "CompanyId", "bVisible": false, "sTitle": "Company Id", "className": "hidden" },
            //{ "mData": "EmployeeId", "bVisible": false, "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "ViewDropIns", "bVisible": true, "sTitle": "View DropIns", "width": "5%", "className": "center-text-align" },
            //{ "mData": "CreateDropIns", "bVisible": true, "sTitle": "Create DropIns", "width": "5%", "className": "center-text-align" },
            { "mData": "Action", "sTitle": LanguageData[0].Action, "width": "24%", "bSortable": false, "className": "center-text-align" },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/DropIns/GetTop3DropIns?TeacherID=" + TeacherID + "&DropInYear=" + DropInYear + "&CompanyID=" + companyid,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () { },
        "language": {
            "loadingRecords": "<span class='loader'></span>",
            "url": "/Content/MultiLang/" + $("#culture").val() + ".json"
        }
    });

}

function ViewDropIns(PDRPDropInID, TeacherID, DropInYear) {
    //*** isAddMode
    //*** 0 = View
    //*** 1 = Add
    //*** 2 = Edit

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { PDRPDropInID: PDRPDropInID, isAddMode: 2, EMployeeID: TeacherID, DropInYear: DropInYear },
        url: '/PDRP/DropIns/SetDropInsViewIDSession',
        success: function (data) {
            if (data.success == true) {               
                window.open("/PDRP/DropIns/DropInForm/", '_blank');
            }
        },
        error: function (data) {
            ShowMessage("error", LanguageData[0].Sometechnical);
        }
    });
}

$('#TalentAreaImage').bind('change', function () {
    //if (jQuery('#TalentAreaImage').val() > 0) {
    //    var imgeUrl = '/Areas/PDRP/images/' + jQuery('#TalentAreaImage').val() + '.jpg';
    //    jQuery('#imageHolder').html('<img class="block-full-width" src="' + imgeUrl + '" />');
    //}
    //else {
    //    jQuery('#imageHolder').html('');
    //}
});
function RemoveValidator() {
    
    
    $("#AppraisalTeachingEmployeeModel_Branch").rules('remove', 'required')
    $("#AppraisalTeachingEmployeeModel_GradeYear").rules('remove', 'required')
    $("#AppraisalTeachingEmployeeModel_GroupSection").rules('remove', 'required')
    $("#ThisYearScoreDescription").rules('remove', 'required')
    $("#DropInComment").rules('remove', 'required')
    $("#ObservEvlComment").rules('remove', 'required')
    $("#DevelopmentPlan").rules('remove', 'required')
    $("#PromotionPotantialComment").rules('remove', 'required')
    $("#NextPromotionComment").rules('remove', 'required')
    $("#LeavingRiskComment").rules('remove', 'required')
    $("#LeavingReasonComment").rules('remove', 'required')
    $("#AppraisalTeachingEmployeeModel_ThisYearScoreDescription").rules('remove', 'required')
    $("#AppraisalComment").rules('remove', 'required')


    $('#idrow input[type=radio]').each(function () {
        $(this).rules('remove', 'required');
    });

    $("#tblKPIs #KPIRatings").each(function () {
        $(this).rules('remove', 'required')

    });
    //$("#radio1").rules('remove', 'required')
    //$("#radio2").rules('remove', 'required')
    //$("#radio3").rules('remove', 'required')

    //$("#radio0").rules('remove', 'required')
    //$("#radio1").rules('remove', 'required')
    //$("#radio2").rules('remove', 'required')
    //$("#radio3").rules('remove', 'required')

    //$("#ChildrensBelowAverageAbility").rules('remove', 'required')


    //$("#ObservationsEmployeeModel_StudentsOnRollMale").removeAttr("min");
    //$("#ObservationsEmployeeModel_StudentsOnRollFemale").removeAttr("min");
    //$("#ObservationsEmployeeModel_StudentsPresentMale").removeAttr("min");
    //$("#ObservationsEmployeeModel_StudentsPresentFemale").removeAttr("min");

    //$("#Outline").rules('remove', 'required')
    //$("#Feedback").rules('remove', 'required')
}
$(document).ready(function () {
    //*** MultiLanguage

    //alert(LanguageData[0].id + "  " + LanguageData[0].category);
    //if (location.href.toLocaleLowerCase().indexOf("pdrp/appraisalteaching") == -1
    //    && location.href.toLocaleLowerCase().indexOf("pdrp/appraisalteaching/#") == -1
    //    && location.href.toLocaleLowerCase().indexOf("pdrp/appraisalteaching/index#") == -1
    //    && location.href.toLocaleLowerCase().indexOf("pdrp/appraisalteaching/index") == -1
    //)
    $('#btnShowLanguage').removeClass("hide");

    $('.KPIScoreT').bind('keydown', function (e) {
       
        e.preventDefault();
    });

    $('#txtVisitDate').datepicker({
        changeMonth: false,
        changeYear: false,
        dateFormat: HRMSDateFormat,
        format: HRMSDateFormat,
        maxDate: new Date('2020-3-26'),
        todayHighlight: true,
        autoclose: true
    });

    $('#txtVisitTime').datepicker({
        changeMonth: false,
        changeYear: false,
        maxDate: new Date('26/3/2026'),
        dateFormat: HRMSDateFormat,
        format: HRMSDateFormat,
        todayHighlight: true,
        autoclose: true
        
    });

    $(".wid180").on('change', function () {
        
        var scorevalues = [];
        
        $("#tblKPIs #KPIRatings").each(function () {

            var get_textbox_value = $(this).val();
            //alert(scorevalues);
            //if (get_textbox_value > 0) {
                scorevalues.push(get_textbox_value);
            //}
        });
        var weightages = [];
        $("#tblKPIs #KPIWeightage").each(function () {

            var get_textbox_value = $(this).val();
            //if (get_textbox_value > 0) {
                weightages.push(get_textbox_value);
            //}
        });

        CalculateKPIScore(scorevalues,weightages);

    });

    $("#tbtnSave").on('click', function () {
        RemoveValidator();
        $("#FormMode").val(1);        
        $("#AppraisalTeachingForm").submit();
    });

    $("#tbtnSubmit").on('click', function () {    
        $("#FormMode").val(2);        
        $("#AppraisalTeachingForm").submit();
    });

    $("#tbtnSignOff").on('click', function () {
        
        $("#FormMode").val(4);
        
        $("#AppraisalTeachingForm").submit();
    });

    $("#btnSave").on('click', function () {
    RemoveValidator();
        $("#FormMode").val(1);        
    });


   

    $("#btnSubmit").on('click', function () {
     $("#FormMode").val(2);       
    });
    $("#btnSignOff").on('click', function () {
        $("#FormMode").val(4);        
    });
    $("#btnCancel").on('click', function () {
        $("#FormMode").val(10);
        
    });

    //*** F3
    $('.btnReject').click(function () {

        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();

                pageLoaderFrame();
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/PDRP/AppraisalTeaching/RejectForm',
                    data: { formProcessID: formprocessid, comments: comments },
                    success: function (result) {

                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                            hideLoaderFrame();

                        }
                        else {
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.message);
                    }
                });
            });
        } else {
            ShowMessage('error', LanguageData[0].Commentsfield);
            return false;
        }
    });
   
    $(document).on("click", ".btnReSubmit", function () {
        $("#FormMode").val(7);
        $("#AppraisalTeachingForm").submit();
        //var isitEditable = $("#isEditable").val()
        //if (($("#MidFormMode").val() <= 1) && (isitEditable == 1)) {
        //    SavePage(3);
        //}
        //else {

        //    var performanceGroupId = $("#PerformanceGroupId").val();
        //    var midyearformid = $("#ID").val();
        //    var employeeId = $("#EmployeeID").val();
        //    var formId = 40;
        //    var goalSettingId = $("#GoalSettingId").val();
        //    var comments = $("#txtComments").val();
        //    pageLoaderFrame();
        //    $.ajax({
        //        url: "/PDRP/AppraisalTeaching/Reintialized",
        //        type: 'POST',
        //        data: { id: midyearformid, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
        //        success: function (result) {
        //            ShowMessage(result.CssClass, result.Message);
        //            selectedValues = [];
        //            if (result.Success) {



        //                $("#prebtnSubmit").removeClass("btnSubmit");
        //                $("#prebtnSubmit").addClass("LMbtnSubmit");
        //                setTimeout(function () { BackToTaskList(); }, 1000);
        //                hideLoaderFrame();

        //            }
        //            else {
        //                hideLoaderFrame();
        //            }
        //        }
        //    });

        //}

    });

});
function GetScoreSlab(thisyearscore)
{

    $.ajax({
        type: 'POST',
        data: { ThisYearScore: thisyearscore},
        traditional: true,
        url: '/PDRP/AppraisalTeaching/GetScoreSlab',
        success: function (data) {
            $("#ThisYearScoreDescription").val(data.ScoreSlab);
            $("#AppraisalTeachingEmployeeModel_ThisYearScoreDescription").val(data.ScoreSlab);          

        },
        error: function (data, xhr, status) {
            ShowMessage("error", LanguageData[0].Sometechnical);
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}
function CalculateKPIScore(scorevalues, weightages) {
   
    var ddlDropInsYearID = $("#AppraisalTeachingEmployeeModel_AppraisalTeachingYearID").val();
    pageLoaderFrame();
    $.ajax({
        type: 'POST',
        data: { Scorevalues: scorevalues, Weightages: weightages, FormulaYearID: ddlDropInsYearID },
        traditional: true,
        url: '/PDRP/AppraisalTeaching/CalculateKPIScore',
        success: function (data) {
            $("#KPIFormula").val(data.Formula);
            $("#KPIScore").val(data.Total);
            
            $("#divTotalKPIScore").html(data.OverallScore);
            $("#TotalKPIScore").val(data.OverallScore);
            $("#tTotalKPIScore").val(data.OverallScore);
            var drop = $("#DropInScoreOverall").val();
            var observ = $("#ObservEvlScoreOverall").val();
           
            observ = parseFloat(observ) + parseFloat(drop) + parseFloat(data.OverallScore);
           

            
            $("#TopThisYearScore").val(parseFloat(observ).toFixed(2));
            $("#ThisYearScore").val(parseFloat(observ).toFixed(2));

            $.each(data.ItemTotal, function (index, element) {
               
                //alert(element.toFixed(2));
                //var formElements = $('#tblKPIs #KPIScore'+(index+1)).val();
                //alert(formElements);
                $('#tblKPIs #KPIScore' + (index + 1)).val(element.toFixed(2));
               
            });
           
            GetScoreSlab($("#ThisYearScore").val());
            hideLoaderFrame();
        },
        error: function (data, xhr, status) {
            ShowMessage("error", LanguageData[0].Sometechnical);
            globalFunctions.onFailure(data, xhr, status);
            hideLoaderFrame();
        }
    });
}
$("#ddlPDRPYears").on('change', function () {

    getEmployeeGrid();

});

$("#ddlKPICompany").on('change', function () {
    getEmployeeGrid(); 

});
function ApplytoallSelections() {
    var VisitDate = $('#txtVisitDate').val().trim();
    var VisitTime = $('#txtVisitTime').val().trim();
    var Ch = 0;
    if (VisitDate.length > 0 && VisitTime.length > 0) {
        var table = $("#tbl_employee").DataTable();
        var UpdatedTr1 = table.$(".odd");
        var UpdatedTr2 = table.$(".even");
        $(UpdatedTr1).each(function () {
            var row = $(this);
            console.log(row);
  
            if (row.find('input[type="checkbox"]').prop('disabled'))
            {
                
                //** No Action need
            }
            //*** Removed On Request
            //else if (row.find('input[type="checkbox"]').is(':checked') &&
            //   row.find('.datepicker').val().length <= 0) {
            else if (row.find('input[type="checkbox"]').is(':checked')) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);
                $(this).find('.EvlDate').val(VisitTime);
                Ch = 1;
            }
        });
        $(UpdatedTr2).each(function () {
            var row = $(this);
            console.log(row);
            if (row.find('input[type="checkbox"]').prop('disabled'))
            {
                
                //** No Action need
            }
            //*** Removed On Request
            //else if (row.find('input[type="checkbox"]').is(':checked') &&
            //   row.find('.datepicker').val().length <= 0) {
            else if (row.find('input[type="checkbox"]').is(':checked')) {
                row.addClass("IsEdit");
                $(this).find('.datepicker').val(VisitDate);
                $(this).find('.EvlDate').val(VisitTime);
                Ch = 1;
            }
        });

        if (Ch == 0) {
            ShowMessage("error", LanguageData[0].Pleaseselectone);
        }
    }
    else {
        ShowMessage("error", LanguageData[0].Pleaseenter);
    }
}
function SetObservationVisitDate(e) {

    //AllEditMode = 0;
    var table = $("#tbl_employee").DataTable();
    var vYearId = $("#ddlPDRPYears").val();
    var ddlYearTxt = $("#ddlPDRPYears option:selected").text();


    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        if (ValidateDateSelection(UpdatedTr)) {
            var updatedData = [];
            $(UpdatedTr).each(function () {

                var vEmployeeId = $(this).find('#EmployeeID').val();
                var vVisitDate = $(this).find('.datepicker').val();
                var vPreObservationDueDate = $(this).find('.EvlDate').val();
                //var autoID = $(itm).find('td:eq(8)').text();
                //var EmpId = ($(itm).find("#ddlApprover_" + autoID)).val();
                if (vPreObservationDueDate != null && vVisitDate != null) {
                    updatedData.push({

                        EmployeeID: vEmployeeId,
                        VisitDate: vVisitDate,
                        PreObservationDate: vPreObservationDueDate
                    });
                }

            });

            //console.log(updatedData);
            //$.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save " + updatedData.length + " records?" }).done(function () {
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/AppraisalTeaching/SetAppraisalVisitDateMulty",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                data: JSON.stringify({ 'SelectedTeachersList': updatedData, 'YearId': vYearId }),
                success: function (data) {
                    if (data.Success) {
                        ShowMessage("success", data.Message);
                        getEmployeeGrid();
                    }
                    else {
                        ShowMessage("error", data.Message);

                    }
                    hideLoaderFrame();
                }
           
            });
        }
    
    else {
            ShowMessage("error", LanguageData[0].Pleaseenter);
            hideLoaderFrame();
         }
    }
    else {

        ShowMessage("error", LanguageData[0].Pleaseselectone);
        hideLoaderFrame();
    }
}
function getEmployeeGrid() {

    var ShowStatusColumnClass = "";
    if ($("#hdnchangeStatusPermission").is(":checked")) {
        ShowStatusColumnClass = "text-center";
    }
    else {
        ShowStatusColumnClass = "hidden";
    }

    var employeeStatus = $("#employeeStatusddl").val();
    var ddlDropInsYearID = $("#ddlPDRPYears").val();
    var ddlDropInsYearTxt = $("#ddlPDRPYears option:selected").text();
    if (employeeStatus == undefined) {
        employeeStatus = "1";
    }
    var vCompanyID = $('#ddlKPICompany').val();
    if (ddlDropInsYearID == "") {
        ddlDropInsYearID = 0
    }
    if (vCompanyID == "") {
        vCompanyID = 0
    }
    //var IncludeInactive = $("#includeInActiveChk").is(':checked');
    //var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_employee').dataTable({
        "sAjaxSource": "/PDRP/AppraisalTeaching/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&AppraisalYear=" + ddlDropInsYearID + "&AppraisalYearTxt=" + ddlDropInsYearTxt + "&CompanyID=" + vCompanyID,
        "aoColumns": [
             { "mData": "Default", "bVisible": false },
            { "mData": "Select", "bVisible": true, "sTitle": "", "sTitle": "<input type=checkbox id=chkSelevctAll onclick=SelectAll()>", "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "Observation", "bVisible": true, "sTitle": LanguageData[0].LessonObservationDone, "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "DropInsCnt3", "bVisible": true, "sTitle": LanguageData[0].DropInsDone, "width": "6%", "bSortable": false, "className": "center-text-align" },
            { "mData": "EmployeeAlternativeID", "bVisible": true, "sTitle": LanguageData[0].EmpID, "width": "6%", "sType": "Int", "className": "center-text-align" },
            { "mData": "FirstName", "bVisible": true, "sTitle": LanguageData[0].FirstName, "width": "15%", "className": "center-text-align" },
            { "mData": "LastName", "sTitle": LanguageData[0].LastName, "width": "15%", "className": "center-text-align" },
            { "mData": "StartDate", "sTitle": LanguageData[0].EvaluationDate, "bSortable": true, "className": "center-text-align" },
            { "mData": "DueDate", "sTitle": LanguageData[0].EvaluationDueDate, "bSortable": true, "className": "center-text-align" },
            //{ "mData": "VisitTime", "sTitle": "Visit Time", "width": "15%" },
            //{ "mData": "PreObservationDueDate", "sTitle": "Pre-Observation Due Date", "bSortable": true },
            { "mData": "EmployeeId", "sTitle": "EmployeeId", "className": "hidden" },
            //{ "mData": "Action", "sTitle": "Action", "bSortable": true },
            { "mData": "KPICount", "bVisible": false},
            { "mData": "DropInCount", "sTitle": "DropIns", "bVisible": false },

        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PDRP/AppraisalTeaching/GetAllTeachingEmployee?Statustype=" + employeeStatus + "&AppraisalYear=" + ddlDropInsYearID + "&AppraisalYearTxt=" + ddlDropInsYearTxt + "&CompanyID=" + vCompanyID,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            //$(".datepicker").datepicker("destroy");
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: false,
                dateFormat: HRMSDateFormat,
                format: HRMSDateFormat,
                autoclose: true
            }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });
            KPICheck(); //*** Removed On Request && Implimented on Request
        },
        "language": { "loadingRecords": "<span class='loader'></span>",
            "url": "/Content/MultiLang/" + $("#culture").val() + ".json"
        }
    });

   

}
function KPICheck()
{
   
    var vKPICount = $(".KPICount").text();
    //alert(vKPICount);
    if (vKPICount != "") {
        if (vKPICount == 0) {
            //ShowMessage("error", "KPI is not Set for the current year.</br> You cannot process Appraisal for this year");

            $("#lblKPI").text(LanguageData[0].KPIck);
           
            $("#btnDateApply").addClass("disabled")
            $(".btnSave").addClass("disabled")
        }
        else {
           
            $("#lblKPI").text("");
            $("#btnDateApply").removeClass("disabled")
            $(".btnSave").removeClass("disabled")
        }
    }
    else
    {
        $("#lblKPI").text("");
        $("#btnDateApply").removeClass("disabled")
        $(".btnSave").removeClass("disabled")
    }
}


