﻿var selectedValues = [];



$(document).ready(function () {
    loadPDRPFormAssignmentData();

    $('#btnSearch').on('click', function () {
        loadPDRPFormAssignmentData();
    });

    $(document).on('change', '.ch', function () {
        ChkChange(this);
    });


    $(document).on('change', '.performcacegroup', function () {
        ChkChange(this);
    });
    //delete (selectedValues);
    

    $(document).on("click", "#btnApprove", function () {
        GetSelectedEmployees();
    });
});

function ChkChange(source) {

    var closesetRow = $(source).closest('tr');
    var vch = $(closesetRow).find('.ch');
    var vdatepicker = $(closesetRow).find('.performcacegroup').val();

    console.log(vdatepicker);
    console.log($(vch).prop("checked"));

    if ($(vch).prop("checked") == true && vdatepicker > 0) {
        $(closesetRow).addClass('IsEdit');

    }
    else {
        $(closesetRow).removeClass('IsEdit');
    }
}


function GetSelectedEmployees() {
    var table = $("#tblFormAssignmentGrid").DataTable();
        
    var UpdatedTr = table.$(".IsEdit");
    if (UpdatedTr.length > 0) {
        var updatedData = [];
        $(UpdatedTr).each(function () {

            var vEmployeeId = $(this).find('td:eq(1)').text();
            var vPerformanceGroupId = $(this).find('.performcacegroup');
           
            var performceGroupId = $(vPerformanceGroupId).val();
            var employeeId = $(vPerformanceGroupId).data("employeeid");
            

            var id = $(vPerformanceGroupId).data("id");
            

            updatedData.push({
                id: id, performanceGroupId: performceGroupId, EmployeeID: employeeId
            });

        });
        
        if (updatedData.length > 0) {
            //$.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Do you want to save " + updatedData.length + " records?" }).done(function () {
                pageLoaderFrame();
                    $.ajax({
                        url: "/PDRP/FormAssignment/SaveFormAssignmentData",
                        type: 'POST',
                        data: { jsonString: JSON.stringify(updatedData) },
                        success: function (result) {
                            ShowMessage(result.CssClass, result.Message);
                            loadPDRPFormAssignmentData();
                            hideLoaderFrame();
                        }
                    });
            //});
        }
        else {
            ShowMessage("error", "Please select at least one Performance Group for employee.");
            hideLoaderFrame();
        }
    }
    else {
        ShowMessage("error", "Please select at least one record.");
        hideLoaderFrame();
       
    }
   
}


function loadPDRPFormAssignmentData() {

    $.ajax({
        type: 'POST',
        data: { companyId: $("#ddlCompany").val(), departmentId: $("#ddlDepartment").val(), lineManagerId: $("#ddlLineManager").val(), performanceGroupId: $("#ddlPerformanceGroup").val() },
        traditional: true,
        url: '/PDRP/FormAssignment/GetPDRPFormAssignmentDataList',
        success: function (data) {
            $("#divFormsAssignment").html(data);
            $('#tblFormAssignmentGrid').DataTable({
                "aoColumnDefs": [
                    { "sWidth": "10%", "aTargets": [0, 1, 3, 5, 6, 7, 8] },
                    { "sWidth": "15%", "aTargets": [2, 4] }
                ],
                "fnDrawCallback": function () {
                    bindSelectpicker('#tblFormAssignmentGrid .selectpickerddl');                   
                },
            });         
        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}


function CompanyIDChange(object) {
    var selectedItem = $(object).val();
    if (selectedItem == undefined || selectedItem == "")
        selectedItem = 0;
    selectedItem = parseInt(selectedItem);
    var ddlDepartment = $("#ddlDepartment");
    $.ajax({
        cache: false,
        type: "GET",
        url: "/PDRP/FormAssignment/GetDepartments",
        data: { "companyId": selectedItem },
        success: function (data) {
            ddlDepartment.html('');
            ddlDepartment.append($('<option></option>').val("0").html("Select Department"));
            $.each(data, function (id, option) {
                ddlDepartment.append($('<option></option>').val(option.id).html(option.name));
            });
            RefreshSelect("#ddlDepartment");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //  alert('Failed to retrieve states.');
        }
    });

    var ddlEmployees = $("#ddlLineManager");
    $.ajax({
        cache: false,
        type: "GET",
        url: "/PDRP/FormAssignment/GetEmployees",
        data: { "companyId": selectedItem },
        success: function (data) {
            ddlEmployees.html('');
            ddlEmployees.append($('<option></option>').val("0").html("Select Line Manager"));
            $.each(data, function (id, option) {
                ddlEmployees.append($('<option></option>').val(option.id).html(option.name));
            });
            RefreshSelect("#ddlLineManager");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //  alert('Failed to retrieve states.');
        }
    });
}

