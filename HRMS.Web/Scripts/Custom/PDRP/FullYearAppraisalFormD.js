﻿var selectedValues = [];
var Competencies1 = [];


function SavePage(e) {
    var Errormsg = "";
    //delete (selectedValues)
    delete (Competencies1)
    //delete (Competencies2)
    var invalidKPI = 0;
    selectedValues = [];
    Competencies1 = [];
    //Competencies2 = [];
    var Tweight = 0;

    var Validating = 0;
    Competencies1 = [];
    var ErrMess = "";
    $(".clsProfessionalCompetencies").each(function () {
        var row = $(this);
        var wid182 = row.find('.wid182').val();
        var wid181 = row.find('.wid181').val();
        //*** F5
        if (wid182 == "N/A") {
            wid182 = 0;
        }
        if (wid181 == "N/A") {
            wid181 = 0;
        }
        var competencyid = row.find('.wid182').data("competencyid");
        var vNo = row.find('td:eq(0)').text();
        Competencies1.push({ No: parseInt(vNo), Score: parseInt(wid182), FullScore: parseInt(wid181), CompetencyID: competencyid })
        //if ((wid182 == '' || wid182 == '0')){
        if (wid182 == '' && wid182 != '0') {
            if (e != 0) {
                Validating = 5;
            }
        }
        //if (wid181 == '' || wid181 == '0') {
        if (wid181 == '' && wid181 != '0') {
                if (e != 0) {
                    Validating = 6;
                }
            }
    });
 
        //*** F3
        

        
        if (e != 0) {
            var grade = $("#txtGrade").val();            

            var lineManagerEvaluationComments = $("#lineManagerEvaluationComments").val();
            var TrainingNeeds = $("#TrainingNeeds").val();
            var lineManagerMidYearPerformanceComments = $("#lineManagerMidYearPerformanceComments").val();
            var employeeMidYearPerformanceComments = $("#employeeMidYearPerformanceComments").val();


            //*** F24 LM Comment and Employee Comment
            var IsLM = $("#IsFLineManager").val();
            var IsEmp = $("#IsFEmployee").val();

            var MyThreeKeyAchivements = $("#MyThreeKeyAchivements").val();
            var MyTwoDevelopmentAreas = $("#MyTwoDevelopmentAreas").val();
            var EmployeeCommentsonFullYearPerformance = $("#EmployeeCommentsonFullYearPerformance").val();
            var Strengths = $("#Strengths").val();
            var FullTrainingNeeds = $("#FullTrainingNeeds").val();
            var LineManagerCommentsonFullYearPerformance = $("#LineManagerCommentsonFullYearPerformance").val();

            if (grade == '' || grade == undefined) {
                Validating = 1;
            }
            //else if (selectedValues.length == 0) {

            //    Validating = 2;
                //}
                //*** F24 Only LM & Employee Can Fill this field so no need to check hear
            //else if (lineManagerEvaluationComments == '' || TrainingNeeds == '' || lineManagerMidYearPerformanceComments == '' || employeeMidYearPerformanceComments == '') {
            //    Validating = 3;
            //}
            else if (((MyThreeKeyAchivements == '' || MyTwoDevelopmentAreas == '' || EmployeeCommentsonFullYearPerformance == '') && IsEmp==1) ||
                ((Strengths == '' || FullTrainingNeeds == '' || LineManagerCommentsonFullYearPerformance == '') && IsLM == 1)) {
                Validating = 4;
            }

            
        }
        if (ErrMess != "") {
            ShowMessage("error", ErrMess);
        }
        else {
            if (Validating == 1) {
                $("#txtGrade-validationMsg").css("color", "red");
                $("#txtGrade-validationMsg").text("This field is mandatory");
                $("#txtGrade-validationMsg").show();
            }
            else if (Validating == 2) {
                ShowMessage("error", "Please add business target details.");
            }
            //*** F24 Only LM & Employee Can Fill this field so no need to check hear
            //else if (Validating == 3) {
            //    ShowMessage("error", "Section 4:Mid Year Appraisal All fields are mandatory");
            //}
            else if (Validating == 4) {
                ShowMessage("error", "Section 7:Full Year Appraisal All fields are mandatory");
            }
            else if (Validating == 5) {
                ShowMessage("error", "Select All Mid Year Ratings in Professional Competencies.");
            }
            else if (Validating == 6) {
                ShowMessage("error", "Select All Full Year Ratings in Professional Competencies.");
            }
            else {
                $("#txtGrade-validationMsg").text("");
                $("#txtGrade-validationMsg").hide();
                //alert(Validating);
                if ($("#MidProfessionalTotalScore").val() == 0) {
                    Errormsg = Errormsg + "</br>Mid Year Total Rating cannot be zero!"
                }
                else if ($("#FullProfessionalTotalScore").val() == 0) {
                    Errormsg = Errormsg + "</br>Full Year Total Rating cannot be zero!"
                }
                //else if ($("#OverallAppraisalScore").val() == 0) {
                //    Errormsg = Errormsg + "</br>Overall Score cannot be zero!"
                //}
                //else if ($("#OverallScoreFormula").val() == 0) {
                //    Errormsg = Errormsg + "</br>Overall Score Formula not found!"
                //}

                if (Errormsg != "" && e != 0) {
                    ShowMessage("error", Errormsg);
                }
                else {



                    pageLoaderFrame();
                    $.ajax({
                        url: "/PDRP/AnnualAppraisal/SaveFullYearAppraisalFormData",
                        type: 'POST',
                        data: { model: JSON.stringify(getFullYearAppraisalModelData()), PerformanceGroupId: $("#PerformanceGroupId").val() },
                        success: function (result) {

                            selectedValues = [];
                            if (result.Success) {
                                if (e == 0) {
                                    ShowMessage(result.CssClass, result.Message);
                                    hideLoaderFrame();
                                    //*** Naresh 2020-03-03 Do not redirect to list page on save.
                                    //if (result.Success) {
                                    //    setTimeout(function () { BackToTaskList(); }, 1000);
                                    //    hideLoaderFrame();
                                    //}
                                    //else {
                                    //    hideLoaderFrame();
                                    //}

                                }
                                else if (e == 3) {//*** F3 Reintialized

                                    var performanceGroupId = $("#PerformanceGroupId").val();
                                    var AnnualGoalSettingFormDId = $("#AnnualGoalSettingFormDId").val();
                                    var fullyearformid = result.InsertedRowId;
                                    var employeeId = $("#EmployeeID").val();
                                    var formId = 38;
                                    var goalSettingId = $("#GoalSettingId").val();
                                    var comments = $("#txtComments").val();

                                    $.ajax({
                                        url: "/PDRP/AnnualAppraisal/ReintializeFullYear",
                                        type: 'POST',
                                        data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormDId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                                        success: function (result) {
                                            ShowMessage(result.CssClass, result.Message);
                                            selectedValues = [];
                                            if (result.Success) {
                                                $("#prebtnSubmit").removeClass("btnSubmit");
                                                $("#prebtnSubmit").addClass("LMbtnSubmit");
                                                setTimeout(function () { BackToTaskList(); }, 1000);
                                                hideLoaderFrame();

                                            }
                                            else {
                                                hideLoaderFrame();
                                            }
                                        }
                                    });
                                }
                                else { //*** Submit

                                    var performanceGroupId = $("#PerformanceGroupId").val();
                                    var AnnualGoalSettingFormDId = $("#AnnualGoalSettingFormDId").val();
                                    var fullyearformid = result.InsertedRowId;
                                    var employeeId = $("#EmployeeID").val();
                                    var formId = 38;
                                    var goalSettingId = $("#GoalSettingId").val();
                                    var comments = $("#txtComments").val();
                                    $.ajax({
                                        //url: "/PDRP/GoalSetting/ApproveMidForm",
                                        url: "/PDRP/AnnualAppraisal/ApproveFullForm",
                                        type: 'POST',
                                        data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormDId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                                        success: function (result) {
                                            ShowMessage(result.CssClass, result.Message);
                                            selectedValues = [];
                                            if (result.Success) {
                                                $("#prebtnSubmit").removeClass("btnSubmit");
                                                $("#prebtnSubmit").addClass("LMbtnSubmit");
                                                setTimeout(function () { BackToTaskList(); }, 1000);
                                                hideLoaderFrame();

                                            }
                                            else {
                                                hideLoaderFrame();
                                            }
                                        }

                                    });
                                }

                            }
                            else {
                                ShowMessage(result.CssClass, result.Message);
                                hideLoaderFrame();
                            }
                        }

                    });

                }
            }

        }


}
$(document).ready(function () {
 
    $(document).on("click", "#prebtnSave, #tprebtnSave", function () {
        
        SavePage(0);
    });
    //*** Employee Submiting
    $(document).on("click", ".btnSubmit", function () {
   
        var isitEditable = $("#isEditable").val()
        if (($("#FullFormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(1);
        }
        else {
            //$("#FormMode").val(1) 
            var performanceGroupId = $("#PerformanceGroupId").val();
            var AnnualGoalSettingFormDId = $("#AnnualGoalSettingFormDId").val();

            var fullyearformid = $("#ID").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 38;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                //url: "/PDRP/GoalSetting/ApproveMidForm",
                url: "/PDRP/AnnualAppraisal/ApproveFullForm",
                type: 'POST',
                data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormDId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }

            });
        }
    });
    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });
    $('.btnReject').click(function () {

        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();

                pageLoaderFrame();
                //*** F25 On Reject new task create for Employee
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/PDRP/AnnualAppraisal/RejectPDRPNonTeachForm',
                    data: { formProcessID: formprocessid, comments: comments, EmployeeID: $("#EmployeeID").val(), FormID: 47, FormInstanceID: $("#GoalSettingId").val() },
                    success: function (result) {

                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                            hideLoaderFrame();

                        }
                        else {
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.message);
                    }
                });
            });
        } else {
            ShowMessage('error', 'Comments field is mandatory.');
            return false;
        }
    });
    //*** F3
    $(document).on("click", ".btnReSubmit", function () {
        var isitEditable = $("#isEditable").val()
        if (($("#FullFormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(3);
        }
        else {

            var performanceGroupId = $("#PerformanceGroupId").val();
            var fullyearformid = $("#ID").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 47;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/AnnualAppraisal/ReintializeFullYear",
                type: 'POST',
                //data: { id: fullyearformid, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                data: { FullYearFormId: fullyearformid, id: AnnualGoalSettingFormDId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {



                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }
            });

        }

    });
    $(document).on("click", ".btnEmpSignOffSubmit", function () {
        var performanceGroupId = $("#PerformanceGroupId").val();
        var AnnualGoalSettingFormDId = $("#AnnualGoalSettingFormDId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 47;
        var requestId = $("#RequestId").val();
        var goalSettingId = $("#GoalSettingId").val();
        //*** F21 SignOff tEXT
        //*** F24 Need to show in Signing off also so remove this condition
        var comments = $("#txtComments").val();
        //var comments = "SignOff";

        //*** F24 LM Comment and Employee Comment
        var MyThreeKeyAchivements = $("#MyThreeKeyAchivements").val();
        var MyTwoDevelopmentAreas = $("#MyTwoDevelopmentAreas").val();
        var EmployeeCommentsonFullYearPerformance = $("#EmployeeCommentsonFullYearPerformance").val();

        pageLoaderFrame();

        $.ajax({
            url: "/PDRP/AnnualAppraisal/SubmitEmployeeSignOffFullYeraForms",
            type: 'POST',
            data: {
                id: AnnualGoalSettingFormDId, employeeId: employeeId, formId: formId, performanceGroupId: performanceGroupId, requestId: requestId, goalSettingId: goalSettingId, comments: comments,
                MyThreeKeyAchivements: MyThreeKeyAchivements, MyTwoDevelopmentAreas: MyTwoDevelopmentAreas, EmployeeCommentsonFullYearPerformance: EmployeeCommentsonFullYearPerformance
            },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                selectedValues = [];
                if (result.Success) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
                hideLoaderFrame();
            }
        });
    });
    FullYearControls();

    //$(".wid180").on('change', function () {       
    //    var totalScore = 0;
    //    $("#tblBusinessTargets #ProficiencyScore").each(function () {
    //        var item_Weight = $(this).closest('tr').find('#item_Weight').val();
    //        var Score = $(this).closest('tr').find('#item_Score');           
    //        if (parseInt(item_Weight) > 0) {
    //            var result = parseFloat((item_Weight / 100) * parseFloat($(this).val())).toFixed(2);
                
    //            $(this).closest('tr').find('#divScore').html(result);
    //            Score.val(result);
    //            totalScore = parseFloat(parseFloat(totalScore) + parseFloat(result)).toFixed(2);          
    //        }

    //    });
    //    $("#BusinessTargetsTota").val(totalScore);
    //    $("#divBusinessTargetsTota").html(totalScore);
    //    $("#BusinessTargetsTota1").val(totalScore);
    //    $("#divBusinessTargetsTota1").html(totalScore);

    //    OverallAppraisalScore();
    //});


    $(".wid181").on('change', function () {

        var totalScore = 0;
        var cnt = 0;
        $("#tblProfessionalCompetenciesFormD #FullProficiencyScore").each(function () {

            if (parseFloat($(this).val()) > 0) {
                var sc = parseFloat($(this).val());
                //totalScore = parseFloat(totalScore) + parseFloat(sc);
                totalScore = parseFloat(parseFloat(totalScore) + parseFloat($(this).val())).toFixed(2);
                cnt++;
            }

        });
        //*** Naresh 2020-03-03 Handled NaN
        var displayValue = parseFloat(totalScore / cnt).toFixed(2);
        if (isNaN(displayValue))
            displayValue = 0.00;
        $("#FullProfessionalTotalScore").val(displayValue);
        $("#divFullProfessionalTotalScore").html(displayValue);
        //$("#BehavioralTotalScore1").val(parseFloat(totalScore / cnt).toFixed(2));

        

        //OverallAppraisalScore();

    });
    $(".wid182").on('change', function () {
       
        var totalScore = 0;
        var cnt = 0;
        $("#tblProfessionalCompetenciesFormD #MidProficiencyScore").each(function () {
            
            if (parseFloat($(this).val()) > 0) {
              
                totalScore = parseFloat(parseFloat(totalScore) + parseFloat($(this).val())).toFixed(2);
                cnt++;
            }

        });

        
        $("#MidProfessionalTotalScore").val(parseFloat(totalScore / cnt).toFixed(2));
        $("#divMidProfessionalTotalScore").html(parseFloat(totalScore / cnt).toFixed(2));
        //$("#ProfessionalTotalScore1").val(parseFloat(totalScore / cnt).toFixed(2));
        //$("#divProfessionalTotalScore1").html(parseFloat(totalScore / cnt).toFixed(2));

        //OverallAppraisalScore();
    });

    //*** Naresh 2020-03-13 Display last comment
    //if ($('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
    //*** Danny to block at REject mode
    if ($('#ReqStatusId').val() < 3 && $('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
        $('#txtComments').val($('#lblComments').data('last-comment'))
});


function getFullYearAppraisalModelData() {

    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        BusinessTargetsComments: $("#businessTargetComments").val(),
        CoreCompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        JsonString1: JSON.stringify(Competencies1),
        //JsonString2: JSON.stringify(Competencies2),
        ID: $("#AnnualGoalSettingFormDId").val(),
        PerformanceGroupId: $("#PerformanceGroupId").val(),
        JobGrade: $("#txtGrade").val(),

        //***F2
        //JsonStringCurrRatingScales: JSON.stringify(CurrRatingScales),
        GoalSettingId: $("#GoalSettingId").val(),
        EmployeeName: $("#EmployeeName").val(),
        Company: $("#Company").val(),
        Designation: $("#Designation").val(),
        RequestEmail: $("#RequestEmail").val(),
        Department: $("#Department").val(),
        LineManager: $("#LineManager").val(),
        DOJ: $("#DOJ").val(),
        FormState: $("#FormMode").val(),
        MidFormMode: $("#MidFormMode").val(),
        FullFormMode: $("#FullFormMode").val(),
        
        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
        TrainingNeeds: $("#TrainingNeeds").val(),
        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val(),

        MyThreeKeyAchivements: $("#MyThreeKeyAchivements").val(),
        MyTwoDevelopmentAreas: $("#MyTwoDevelopmentAreas").val(),
        EmployeeCommentsonFullYearPerformance: $("#EmployeeCommentsonFullYearPerformance").val(),
        Strengths: $("#Strengths").val(),
        FullTrainingNeeds: $("#FullTrainingNeeds").val(),
        LineManagerCommentsonFullYearPerformance: $("#LineManagerCommentsonFullYearPerformance").val(),

        BusinessTargetsTota: $("#BusinessTargetsTota").val(),
        ProfessionalCompetenciesAVG: $("#MidProfessionalTotalScore").val(),
        BehavioralCompetenciesAVG: $("#FullProfessionalTotalScore").val(),
        CompetenciesAVG: $("#CompetenciesAVG").val(),
        OverallAppraisalScore: $("#OverallAppraisalScore").val(),
        OverallScoreFormula: $("#OverallScoreFormula").val(),
        FormSubmit: $("#FormSubmit").val(),
        RequestId: $("#RequestId").val(),
        //*** Naresh 2020-03-11 Save comments on save
        Comments: $("#txtComments").val()

    };

    return businessTargetModel;
}


//function getMidYearAppraisalModelData() {
//    var businessTargetModel = {
//        EmployeeId: $("#EmployeeID").val(),
//        BusinessTargetsComments: $("#businessTargetComments").val(),
//        CoreCompetenciesComments: $("#CompetenciesComments").val(),
//        YearId: $("#YearId").val(),
//        CompanyId: $("#CompanyId").val(),
//        JsonString: JSON.stringify(selectedValues),
//        ID: $("#MidYearAppraisalFormModelId").val(),
//        PerformanceGroupId: $("#PerformanceGroupId").val(),
//        LineManagerEvaluationComments: $("#lineManagerEvaluationComments").val(),
//        TrainingNeeds: $("#trainingNeeds").val(),
//        LineManagerMidYearPerformanceComments: $("#lineManagerMidYearPerformanceComments").val(),
//        EmployeeMidYearPerformanceComments: $("#employeeMidYearPerformanceComments").val()
//    };
//    return businessTargetModel;
//}
function FullYearControls() {
    var PeriodId = $('#PeriodId').val();
    if (PeriodId == 2) {
        $('#item_Score').bind('keydown', function (e) {
            e.preventDefault();
        });
        $('#item_BusinessTargetDetails').bind('keydown', function (e) {
            e.preventDefault();
        });
        $('#item_Weight').bind('keydown', function (e) {
            e.preventDefault();
        });
    }
}
function BackToTaskList() {
    location.href = "/FormsTaskList/Index";
}