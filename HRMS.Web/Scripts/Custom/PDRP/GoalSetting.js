﻿var selectedValues = [];
var isSelectedValues = false;
var appliedSelections = [];
var isFilter = false;
$(document).ready(function () {
    loadGoalSettingData();

    $('#btnSearch').on('click', function () {
        loadGoalSettingData();
        isSelectedValues = false;
        isFilter = true;
    });

    $(document).on("click", "#btnApplyToAll", function () {
        if ($("#ddlKPIYear").val() == '')
            ShowMessage("error", "Please select Year.");
        else if ($("#SettingGoalsStartDate").val() == '')
            ShowMessage("error", "Please select Setting Goals Start date.");
        else if ($("#SettingGoalsDueDate").val() == '')
            ShowMessage("error", "Please select Setting Goals Due date.");
        else {
            $(".YNClass").not("[disabled]").each(function () {
                if ($(this).is(':checked')) {
                    isSelectedValues = true;
                    //*** Naresh 2020-02-26 Allow updating the date
                    var startDate = $(this).closest('tr').find('.settingGolasStartDate');
                    var vProcessName = $(this).closest('tr').find('.ProcessName');
                    //if (startDate.val() == '' || startDate.val() == undefined)
                    var dueDate = $(this).closest('tr').find('.settingGolasDueDate');


                    startDate.val($("#SettingGoalsStartDate").val());
                    dueDate.val($("#SettingGoalsDueDate").val());
                    vProcessName.val($("#txtProcessName").val());
                    appliedSelections.push({ employeeId: $(this).data("employeeid"), companyId: $(this).data("companyid"), ProcessName: vProcessName });
                    //}
                    //}
                }
            });
            if (!isSelectedValues)
                ShowMessage("error", "Please seelct at least one checkbox");
            else
                isSelectedValues = false;
        }
    });

    $(document).on("click", "#btnSubmit", function () {
        var ValidChk = 0;
        $(".YNClass").not("[disabled]").each(function () {
            if ($(this).is(':checked')) {
                var performceGroupId = $(this).closest('tr').find('.performcacegroup').val();
                var employeeId = $(this).data("employeeid");
                var yearId = $("#ddlKPIYear").val();
                var startdate = $(this).closest('tr').find('.settingGolasStartDate').val();
                var duedate = $(this).closest('tr').find('.settingGolasDueDate').val();
                //*** Naresh 2020-03-08 Process name selector was wrong
                var vProcessName = $('#txtProcessName').val();

                if (yearId == '') {
                    ValidChk = 1;
                    ShowMessage('error', 'Please Select Year');
                }
                else if (startdate == '' || duedate == '')
                {
                    ValidChk = 1;
                    ShowMessage('error', 'Please Select Goals Start / Due Date');
                }
                
                else {
                    selectedValues.push({ performanceGroupId: performceGroupId, EmployeeID: employeeId, YearId: yearId, SettingGoalsStartDate: startdate, SettingGoalsDueDate: duedate, ProcessName: vProcessName });
                }
            }
        });
        if (ValidChk == 0) {
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/GoalSetting/SaveGoalSettingsData",
                type: 'POST',
                data: { jsonString: JSON.stringify(selectedValues) },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    //*** Naresh 2020-02-26 Do not refresh the page on validation
                    if(result.Success==true)
                        loadGoalSettingData();
                    selectedValues = [];
                    appliedSelections = [];
                    isSelectedValues = false;
                    isFilter = false;
                    hideLoaderFrame();
                }
            });
        }
    });

    $(document).on("click", "#btnCancel", function () {
        $("#SettingGoalsStartDate, #SettingGoalsDueDate").val("");
        $(".YNClass").each(function () {
            if ($(this).is(':checked')) {
                $(this).prop('checked', false);
                $(this).closest('tr').find('.settingGolasStartDate').val("");
                $(this).closest('tr').find('.settingGolasDueDate').val("");
            }
        });
    });
    /*
    $(document).on("change", "#ddlKPIYear", function () {
        loadGoalSettingData();
        isSelectedValues = false;
        isFilter = true;
    });
    */
    //$("datepicker").datepicker("destroy");
    //$("datepicker").datepicker({
    //    changeMonth: false,
    //    changeYear: false,
    //    dateFormat: "dd/mm/yy",
    //    todayHighlight: true,
    //    autoclose: true

    //});

});
function SelectAll() {
    var Bool = false;
    var chk = $("#chkSelevctAll");
    if (chk.prop('checked')) {
        Bool = true;
    }

    var table = $("#tblGoalSettingGrid").DataTable();
    var UpdatedTr1 = table.$(".odd");
    var UpdatedTr2 = table.$(".even");
    $(UpdatedTr1).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');


        if (chbox.is(':disabled')) {
        }
        else {
            chbox.prop('checked', Bool);
        }
        //if (chbox.prop('checked')) {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', false);
        //    }

        //}
        //else {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', true);
        //    }

        //}
        //ChkChange(this);

    });
    $(UpdatedTr2).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.is(':disabled')) {
        }
        else {
            chbox.prop('checked', Bool);
        }
        //if (chbox.prop('checked')) {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', false);
        //    }
        //}
        //else {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', true);
        //    }
        //}
        //ChkChange(this);

    });

}
function ChkChange(source) {

    var closesetRow = $(source).closest('tr');
    var vdatepicker = $(closesetRow).find('.datepicker').val();
    //var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if ($(source).prop("checked") == true) {
        if (vdatepicker.length > 0) {

            var employeeId = $(closesetRow).addClass('IsEdit');
        }
    }
    else if ($(source).prop("checked") == false) {
        var employeeId = $(closesetRow).removeClass('IsEdit');
    }
}
function loadGoalSettingData() {
    $.ajax({
        type: 'POST',
        data: { companyId: $("#ddlCompany").val(), yearId: $("#ddlKPIYear").val() },
        traditional: true,
        url: '/PDRP/GoalSetting/GetGoalSettingDataList',
        success: function (data) {
            $("#divGoalSettings").html(data);
            
            $('#tblGoalSettingGrid').DataTable({
                "aoColumnDefs": [
                    { "sWidth": "10%", "aTargets": [0, 1, 2, 4, 5] },
                    { "sWidth": "15%", "aTargets": [3, 6, 7] }
                ],
                "fnDrawCallback": function () {
                    bindSelectpicker('#tblGoalSettingGrid .selectpickerddl');
                    //** Naresh 2020-02-26 Date picker was not being displayed on grid
                    $('.datepicker').datepicker({
                        dateFormat: HRMSDateFormat,
                        changeMonth: true,
                        changeYear: true,
                        yearRange: '-100:+100',
                        buttonImage: "../../Content/images/calender-icon.png",
                        showOn: "both",
                        buttonImageOnly: true,
                        autoclose: true,
                        buttonImageText: "Calendar",
                        format: HRMSDateFormat,
                        todayHighlight: true
                    });
                    //*** Danny Date Formate wrong so at save error will come
                    //$(".datepicker").datepicker("destroy");
                    //$(".datepicker").datepicker({
                    //    changeMonth: true,
                    //    changeYear: false,
                    //    autoclose: true,
                    //    dateFormat: "dd/mm/yyyy",
                    //    todayHighlight: true

                    //}).on('changeDate', function (ev) {
                    //    $(this).datepicker('hide');
                    //});
                },
            });
            //*** This function may not need. Need to check ???
            //if (isFilter) {
            //    var companyId = appliedSelections[0].companyId;
            //    if (companyId == $("#ddlCompany").val()) {
            //        $.each(appliedSelections, function (key, value) {
            //            var row = $('tr[data-employeeid="' + value["employeeId"] + '"]');
            //            row.find(".YNClass").prop("checked", true);
            //            row.find(".settingGolasStartDate").val($("#SettingGoalsStartDate").val());
            //            row.find(".settingGolasDueDate").val($("#SettingGoalsDueDate").val());
            //        });
            //    }
            //}
        },
        error: function (data, xhr, status) {
            ShowMessage("error", "Some technical error occurred");
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}