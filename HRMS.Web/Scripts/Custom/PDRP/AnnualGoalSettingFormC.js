﻿var selectedValues = [];
var CurrRatingScales = [];

function SavePage(e) {
    
    delete (selectedValues);
    delete (CurrRatingScales);
    var Tweight = 0;
    var invalidKPI = 0;
    selectedValues = [];
    var businessbargetno = 1;
    var ErrMess = "";
    $(".businesstargetandweightsection").each(function () {
        var row = $(this);
        var kpiId = row.data("kpiid");
        var businessTargetDetails = row.find('.kpibusinesstarget').val();
        var weight = row.find('.weight').val();

        //*** F3

        if (weight != "") {
            Tweight = parseInt(Tweight) + parseInt(weight);
        }
        selectedValues.push({ BusinessTargetNo: businessbargetno, kpiId: kpiId, businessTargetDetails: businessTargetDetails, weight: weight });
        //*** F12
        if (weight == "0") {
            invalidKPI = 11;
        }
        //alert(businessbargetno + ':' + invalidKPI + '/businessTargetDetails:' + businessTargetDetails + '/weight:' + weight);
        if ((businessTargetDetails == "" && weight != "") || (businessTargetDetails != "" && weight == "")) {

            invalidKPI = 1;
        }

        //***F24
        if ((businessTargetDetails == "" && (weight != "" && weight != "0"))) {
            if (ErrMess != "") {
                ErrMess = ErrMess + "</br>";
            }
            ErrMess = ErrMess + "Invalid Business Targets Description at Row No: " + businessbargetno;
        }
        if ((businessTargetDetails != "" && (weight == "" || weight == "0"))) {
            if (ErrMess != "") {
                ErrMess = ErrMess + "</br>";
            }
            ErrMess = ErrMess + "Invalid Business Targets Weightage at Row No: " + businessbargetno;
        }

        businessbargetno = parseInt(businessbargetno) + 1;
    });
    //***F2
    CurrRatingScales = [];
    $(".RatingScales").each(function () {
        var row1 = $(this);
        var RatingScaleCharacter = row1.data("ratingscalecharacter");
        var RatingScaleNumber = row1.data("ratingscalenumber");
        var FinalScoreSlabMIN = row1.data("finalscoreslabmin");
        var FinalScoreSlabMAX = row1.data("finalscoreslabmax");
        var DefinitionName = row1.data("definitionname");
        var DefinitionDetails = row1.data("definitiondetails");

        CurrRatingScales.push({
            RatingScaleCharacter: RatingScaleCharacter, RatingScaleNumber: RatingScaleNumber, FinalScoreSlabMIN: FinalScoreSlabMIN,
            FinalScoreSlabMAX: FinalScoreSlabMAX, DefinitionName: DefinitionName, DefinitionDetails: DefinitionDetails
        });
    });
    if (ErrMess != "") {
        ShowMessage("error", ErrMess);
    }
    else {
        //*** F12
        if (invalidKPI == 11) {
            ShowMessage("error", "KPI Weight can not be Zero (0)");

        }
        else {
            if ((Tweight > 100 || Tweight < 100 || invalidKPI == 1) && e != 0) {//*** F3
                if (invalidKPI == 1) {
                    ShowMessage("error", "Invalid KPI");
                }
                else {
                    ShowMessage("error", "Total Weightage of KPIs should be 100%");
                }
            }
            else {
                //*** F3
                var Validating = 0;
                if (e != 0) {
                    var grade = $("#txtGrade").val();
                    if (grade == '' || grade == undefined) {
                        Validating = 1;
                    }
                    else if (selectedValues.length == 0) {

                        Validating = 2;
                    }
                }


                if (Validating == 1) {
                    $("#txtGrade-validationMsg").css("color", "red");
                    $("#txtGrade-validationMsg").text("This field is mandatory");
                    $("#txtGrade-validationMsg").show();
                }
                else if (Validating == 2) {
                    ShowMessage("error", "Please add business target details.");
                }
                else {
                    $("#txtGrade-validationMsg").text("");
                    $("#txtGrade-validationMsg").hide();
                    //if (selectedValues.length > 0) {
                    pageLoaderFrame();
                    $.ajax({
                        url: "/PDRP/GoalSetting/SaveAnnualGoalSettingsFormCData",
                        type: 'POST',
                        data: { model: JSON.stringify(getBusinessTargetModeldata()) },
                        success: function (result) {
                            selectedValues = [];
                            if (result.Success) {
                                if (e == 0) {
                                    ShowMessage(result.CssClass, result.Message);
                                    hideLoaderFrame();
                                    //*** Naresh 2020-03-03 Do not redirect to list page on save.
                                    //if (result.Success) {
                                    //    setTimeout(function () { BackToTaskList(); }, 1000);
                                    //    hideLoaderFrame();
                                    //}
                                    //else {
                                    //    hideLoaderFrame();
                                    //}

                                }
                                else if (e == 3) {//*** F3 Reintialized
                                    var performanceGroupId = $("#PerformanceGroupId").val();
                                    var annualGoalSettingFormAId = $("#AnnualGoalSettingFormCId").val();
                                    var employeeId = $("#EmployeeID").val();
                                    var formId = 38;
                                    var goalSettingId = $("#GoalSettingId").val();
                                    var comments = $("#txtComments").val();

                                    $.ajax({
                                        url: "/PDRP/GoalSetting/Reintialized",
                                        type: 'POST',
                                        data: { id: annualGoalSettingFormAId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                                        success: function (result) {
                                            ShowMessage(result.CssClass, result.Message);
                                            selectedValues = [];
                                            if (result.Success) {
                                                $("#prebtnSubmit").removeClass("btnSubmit");
                                                $("#prebtnSubmit").addClass("LMbtnSubmit");
                                                setTimeout(function () { BackToTaskList(); }, 1000);
                                                hideLoaderFrame();

                                            }
                                            else {
                                                hideLoaderFrame();
                                            }
                                        }
                                    });
                                }
                                else { //*** Employee Submit
                                    //$("#FormMode").val(1) 
                                    var performanceGroupId = $("#PerformanceGroupId").val();
                                    var AnnualGoalSettingFormCId = result.InsertedRowId;
                                    var employeeId = $("#EmployeeID").val();
                                    var formId = 38;
                                    var goalSettingId = $("#GoalSettingId").val();
                                    var comments = $("#txtComments").val();
                                    $.ajax({
                                        url: "/PDRP/GoalSetting/ApproveGoalForm",
                                        type: 'POST',
                                        data: { id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                                        success: function (result) {
                                            ShowMessage(result.CssClass, result.Message);
                                            selectedValues = [];
                                            if (result.Success) {
                                                $("#prebtnSubmit").removeClass("btnSubmit");
                                                $("#prebtnSubmit").addClass("LMbtnSubmit");
                                                setTimeout(function () { BackToTaskList(); }, 1000);
                                                hideLoaderFrame();

                                            }
                                            else {
                                                hideLoaderFrame();
                                            }
                                        }
                                    });
                                }
                            }
                            else {
                                ShowMessage(result.CssClass, result.Message);

                                hideLoaderFrame();
                            }

                        }
                    });
                    //}
                    //else
                    //    ShowMessage("error", "Please add business target details.");
                }
            }
        }
    }
}
$(document).ready(function () {
    
    $(document).on("click", "#prebtnSave, #tprebtnSave", function () {
        SavePage(0);
    });
    

    //*** Employee Submiting
    $(document).on("click", ".btnSubmit", function () {
        var isitEditable = $("#isEditable").val()
        if (($("#FormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(1);
        }
        else {
            //$("#FormMode").val(1) //*** Employee Submit
            var performanceGroupId = $("#PerformanceGroupId").val();
            var AnnualGoalSettingFormCId = $("#AnnualGoalSettingFormCId").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 38;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/GoalSetting/ApproveGoalForm",
                type: 'POST',
                data: { id: AnnualGoalSettingFormCId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();
                    }
                    else {
                        hideLoaderFrame();
                    }
                    
                }
            });            
        }
    });
    //*** F3
    $(document).on("click", ".btnReSubmit", function () {
        var isitEditable = $("#isEditable").val()
        if (($("#FormMode").val() <= 1) && (isitEditable == 1)) {
            SavePage(3);
        }
        else {
            var performanceGroupId = $("#PerformanceGroupId").val();
            var annualGoalSettingFormAId = $("#AnnualGoalSettingFormCId").val();
            var employeeId = $("#EmployeeID").val();
            var formId = 38;
            var goalSettingId = $("#GoalSettingId").val();
            var comments = $("#txtComments").val();
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/GoalSetting/Reintialized",
                type: 'POST',
                data: { id: annualGoalSettingFormAId, employeeId: employeeId, formId: formId, goalSettingId: goalSettingId, FormState: 1, performanceGroupId: performanceGroupId, comments: comments },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    selectedValues = [];
                    if (result.Success) {
                        $("#prebtnSubmit").removeClass("btnSubmit");
                        $("#prebtnSubmit").addClass("LMbtnSubmit");
                        setTimeout(function () { BackToTaskList(); }, 1000);
                        hideLoaderFrame();

                    }
                    else {
                        hideLoaderFrame();
                    }
                }
            });

        }

    });

    //*** F3
    $('.btnReject').click(function () {

        var IsValidateComment = validateComments($("#txtComments"));
        if (IsValidateComment) {
            $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to reject this request?" }).done(function () {
                var formprocessid = $("#hdnformProcessID").val();
                var comments = $("#txtComments").val();

                pageLoaderFrame();
                //*** F25 On Reject new task create for Employee
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/PDRP/GoalSetting/RejectPDRPNonTeachForm',
                    data: { formProcessID: formprocessid, comments: comments, EmployeeID: $("#EmployeeID").val(), FormID: 35, FormInstanceID: $("#GoalSettingId").val() },
                    success: function (result) {

                        ShowMessage(result.CssClass, result.Message);
                        if (result.InsertedRowId > 0) {
                            setTimeout(function () { BackToTaskList(); }, 1000);
                            hideLoaderFrame();

                        }
                        else {
                            hideLoaderFrame();
                        }
                    },
                    error: function (err) {
                        hideLoaderFrame();
                        ShowMessage('error', err.message);
                    }
                });
            });
        } else {
            ShowMessage('error', 'Comments field is mandatory.');
            return false;
        }
    });

    $(document).on("click", ".btnEmpSignOffSubmit", function () {
        var annualGoalSettingFormCId = $("#AnnualGoalSettingFormCId").val();
        var employeeId = $("#EmployeeID").val();
        var formId = 35;
        var requestId = $("#RequestId").val();
        var goalSettingId = $("#GoalSettingId").val();
        //*** F21 SignOff tEXT
         //*** F24 Need to show in Signing off also so remove this condition
        var comments = $("#txtComments").val();
        //var comments = "SignOff";

        pageLoaderFrame();

        $.ajax({
            url: "/PDRP/GoalSetting/SubmitEmployeeSignOffForms",
            type: 'POST',
            data: { id: annualGoalSettingFormCId, employeeId: employeeId, formId: formId, performanceGroupId: 3, requestId: requestId, goalSettingId: goalSettingId,comments:comments },
            success: function (result) {
                ShowMessage(result.CssClass, result.Message);
                selectedValues = [];
                if (result.Success) {
                    setTimeout(function () { BackToTaskList(); }, 1000);
                }
                hideLoaderFrame();
            }
        });
    });

    $(document).on("click", "#btnCancel", function () {
        setTimeout(function () { BackToTaskList(); }, 1000);
    });
   
    $("#divKPIFormC").on('change', '.weight', function () {
        
        var calculated_total_sum = 0;

        $("#tblKPIFormC .weight").each(function () {

            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                //calculated_total_sum += parseFloat(get_textbox_value);
                //*** F12
                if (get_textbox_value > 0) {
                    calculated_total_sum += parseFloat(get_textbox_value);
                }
                else {
                    //ShowMessage("error", "Invalid KPI");
                    $(this).val(0);
                }
            }

        });
        if (calculated_total_sum > 100) {
            $("#txtTotalWeightage").css({ "border-color": "Red", "border-width": "thick" });
        }
        else {
            $("#txtTotalWeightage").css({ "border-color": "#d5d5d5", "border-width": "thin" });
        }
        $("#txtTotalWeightage").val(calculated_total_sum);
    });

    //*** Naresh 2020-03-13 Display last comment
    if ($('#txtComments').length > 0 && $('#lblComments').data('last-comment').length > 0)
        $('#txtComments').val($('#lblComments').data('last-comment'))
});

function getBusinessTargetModeldata() {
    var businessTargetModel = {
        EmployeeId: $("#EmployeeID").val(),
        KPIsComments: $("#kpiComments").val(),
        CompetenciesComments: $("#CompetenciesComments").val(),
        YearId: $("#YearId").val(),
        CompanyId: $("#CompanyId").val(),
        JsonString: JSON.stringify(selectedValues),
        ID: $("#AnnualGoalSettingFormCId").val(),
        JobGrade: $("#txtGrade").val(),
        //***F2
        JsonStringCurrRatingScales: JSON.stringify(CurrRatingScales),
        GoalSettingId: $("#GoalSettingId").val(),
        EmployeeName: $("#EmployeeName").val(),
        Company: $("#Company").val(),
        Designation: $("#Designation").val(),
        RequestEmail: $("#RequestEmail").val(),
        Department: $("#Department").val(),
        LineManager: $("#LineManager").val(),
        DOJ: $("#DOJ").val(),
        FormState: $("#FormMode").val(),
        //*** Naresh 2020-03-12 Save comment during save
        Comments: $("#txtComments").val(),
    };
    return businessTargetModel;
}

