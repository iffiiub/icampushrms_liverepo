﻿$(document).ready(function () {
    //*** MultiLanguage
    $('#btnShowLanguage').removeClass("hide");    
    var lng ='LNG_'+ $('#culture').val();

    var data = {
        "items": [{
            "id": "en-gb",
            "category": "cat1"
        }, {
            "id": "ar",
            "category": "لعام المحدد"
        }]
    };

    var returnedData = $.grep(data.items, function (element, index) {
        return element.id == $('#culture').val();
    });


    //alert(returnedData[0].id + "  " + returnedData[0].category);

    //alert($('#culture').val());
//===========================================================================
    $("#divYearlyKPIs").on('input', '.weightage', function () {
        var calculated_total_sum = 0;
        
        $("#tblKPIs .weightage").each(function () {
            
            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                calculated_total_sum += parseFloat(get_textbox_value);
            }
            
        });
        if (calculated_total_sum > 100)
        {
            $("#txtTotalWeightage").css({ "border-color": "Red","border-width": "thick" });
        }
        else
        {
            $("#txtTotalWeightage").css({ "border-color": "#d5d5d5", "border-width": "thin" });
        }
        $("#txtTotalWeightage").val(calculated_total_sum);
    });

   
    //================================================================================

    $("#btn_saveAll").on('click', function () {
       
        $("#frmKPIs").submit();

    });

//=====================================================================================

    $("#ddlKPIYear").on('change', function () {
        
        GetKPI();
        
    });
    
    $("#ddlKPICompany").on('change', function () {
        GetKPI();

    });

});
//*** MultiLanguage
function PDRPLoadLanguage(e)
{
    pageLoaderFrame();
    $.ajax({
        url: '/PDRP/KPIs/SetCulture',
        data: { languageid: e },
        type: 'GET',
        success: function (data) {
            //location.reload();
            //*** Naresh 2020-04-13 Open the other language in new tab
            var win = window.open(location.href, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                
            }
            hideLoaderFrame();
        },
        error: function (data, xhr, status,error) {
            var err = eval("(" + xhr.responseText + ")");           
            ShowMessage("error", "Technical Error!");
            hideLoaderFrame();
        }
    });
}
function GetKPI()
{
    pageLoaderFrame();
    var vCompanyID = $('#ddlKPICompany').val();

    var vkpiyear = $('#ddlKPIYear').val();
    //if (vkpiyear == "") {
    //    var currentTime = new Date();
    //    var year = currentTime.getFullYear()
    //    vkpiyear = year;
    //}


    $('#KPIYear').val(vkpiyear);
    if (vCompanyID == '')
    {
        //vCompanyID = 0;
    }
    if (vCompanyID != '' && vkpiyear != "") {
        $.ajax({
            url: '/PDRP/KPIs/GetYearlyKPIs',
            data: { kpiyear: vkpiyear, CompanyID: vCompanyID },
            type: 'GET',
            success: function (data) {

                $("#divYearlyKPIs").html(data);
                setTimeout(function () {

                    if ($("#IsEditable").val() == 0) {
                        $("#btn_saveAll").addClass("hide");
                        $('.btnSave').prop('disabled',true);
                    }
                    else {
                        $("#btn_saveAll").removeClass("hide");
                        $('.btnSave').prop('disabled',false);
                    }
                    hideLoaderFrame();
                }, 50);

                
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);
                hideLoaderFrame();
            }
        });
    }
    else
    {
        hideLoaderFrame();
    }
}