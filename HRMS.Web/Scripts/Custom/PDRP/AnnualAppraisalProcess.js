﻿var selectedValues = [];
var isSelectedValues = false;
var appliedSelections = [];
var isFilter = false;

$(document).ready(function () {
    //loadAnnualAppraisalProcessData();

    $('#btnSearch').on('click', function () {
        loadAnnualAppraisalProcessData();
        isSelectedValues = false;
        isFilter = true;
    });

    $(document).on("click", "#btnApplyToAll", function () {
        delete (appliedSelections);
        appliedSelections = [];
        if ($("#EvaluationStartDate").val() == '') {
            ShowMessage("error", "Please select Setting Goals Start date.");
        }
        else if ($("#EvaluationDueDate").val() == '') {
            ShowMessage("error", "Please select Setting Goals Due date.");
        }
        else {
            var periodId = $("#ddlPeriod").val();
            //alert(periodId);
            if (periodId == 2 || periodId == 1) {
                $(".YNClass").not("[disabled]").each(function () {
                    //*** Naresh 2020-03-04 Do not update the disabled items
                    if ($(this).is(':checked')) {

                        isSelectedValues = true;
                        var startDate = $(this).closest('tr').find('.evaluationStartDate');
                        var MidYearStat = $(this).closest('tr').find("td").eq(5).text();

                        //*** Naresh 2020-02-26 Allow to update the date
                        //if (startDate.val() == '' || startDate.val() == undefined)
                        var dueDate = $(this).closest('tr').find('.evaluationDueDate');
                        //if ((startDate.val() == '' || startDate.val() == undefined) && (dueDate.val() == '' || dueDate.val() == undefined)) {
                        if ($("#EvaluationStartDate").val() == '')
                            ShowMessage("error", "Please select Setting Goals Start date.");
                        else if ($("#EvaluationDueDate").val() == '')
                            ShowMessage("error", "Please select Setting Goals Due date.");
                        else {
                            //--*** Danny 22/09/2019
                            //alert($(this).data("employeeid"));
                            //alert(MidYearStat);
                            //alert(periodId);

                            if (periodId == 2 && MidYearStat == 'Yes') {
                                startDate.val($("#EvaluationStartDate").val());
                                dueDate.val($("#EvaluationDueDate").val());
                                appliedSelections.push({ employeeId: $(this).data("employeeid"), companyId: $(this).data("companyid") });
                            }
                            else if (periodId == 1 && MidYearStat == 'No') {

                                startDate.val($("#EvaluationStartDate").val());
                                dueDate.val($("#EvaluationDueDate").val());
                                appliedSelections.push({ employeeId: $(this).data("employeeid"), companyId: $(this).data("companyid") });
                            }
                            else {
                                ShowMessage("error", "Invalid Period Selection.");
                            }
                        }
                        //}
                    }
                });
                if (!isSelectedValues)
                    ShowMessage("error", "Please seelct at least one checkbox");
                else
                    isSelectedValues = false;
            }
            else {
                ShowMessage("error", "Invalid Period Selection.");
            }

        }



    });

    $(document).on("click", "#btnSubmit", function () {
        var employees = " ";
        var isMidYearAppraisalSubmitted;
        var periodId;
        delete (selectedValues);
        var validateit = 0;
        $(".YNClass").not("[disabled]").each(function () {
            if ($(this).is(':checked')) {

                //--*** Danny 22/09/2019
                var performceGroupId = $(this).closest('tr').find('.performcacegroup').val();
                var employeeId = $(this).data("employeeid");
                var goalsettingid = $(this).data("goalsettingid");
                var yearId = $("#ddlKPIYear").val();
                var startdate = $(this).closest('tr').find('.evaluationStartDate').val();
                var duedate = $(this).closest('tr').find('.evaluationDueDate').val();
                periodId = $("#ddlPeriod").val();
                isMidYearAppraisalSubmitted = $(this).closest('tr').data("ismidyearappraisalsubmitted");
                var employeeName = $(this).closest('tr').find("td").eq(6).text();
                var vProcessName = $('#ProcessName').val();

                employees = employees + employeeName + ",";
                if (startdate != "" || duedate != "") {
                    selectedValues.push({
                        performanceGroupId: performceGroupId, EmployeeID: employeeId, YearId: yearId, EvaluationStartDate: startdate,
                        EvaluationDueDate: duedate, periodId: periodId, ProcessName: vProcessName, GoalSettingId: goalsettingid
                    });
                }
                else
                {
                    validateit = 1;
                }
            }
        });
        //*** Naresh 2020-02-26 Display the validation message
        //*** Danny Modifid to show message if any one'd date is blank
        if (validateit == 1 || selectedValues.length == 0) {
            ShowMessage("error", "Please enter performance start and end dates for selected records");
        }
        else if (periodId == 2 && isMidYearAppraisalSubmitted == "False")
            ShowMessage("error", "Please submit Mid year Appraisal Process first for employees :" + employees.slice(0, -1));
        else {
            pageLoaderFrame();
            $.ajax({
                url: "/PDRP/AnnualAppraisal/SaveAnnualAppraisalProcessData",
                type: 'POST',
                data: { jsonString: JSON.stringify(selectedValues), PeriodId: periodId },
                success: function (result) {
                    ShowMessage(result.CssClass, result.Message);
                    //*** Naresh 2020-02-26 Naresh Do not refresh the page in case of error or validation failure
                    if (result.Success)
                        loadAnnualAppraisalProcessData();
                    selectedValues = [];
                    hideLoaderFrame();


                }
            });
        }
    });
    /* Removed On Request 14/06/2020
    $(document).on("change", "#ddlKPIYear", function () {
        loadAnnualAppraisalProcessData();
        isSelectedValues = false;
        isFilter = true;
    });
    $(document).on("change", "#ddlPeriod", function () {
        loadAnnualAppraisalProcessData();
        isSelectedValues = false;
        isFilter = true;
    });
    */
    //*** Naresh 2020-02-26 Display the grid with no data
    loadAnnualAppraisalProcessData();
});

function loadAnnualAppraisalProcessData() {
    var ddlDropInsYearID = $("#ddlKPIYear").val();
    //if (ddlDropInsYearID == "") {
    //    ddlDropInsYearID = 0
    //}
    var periodId = $("#ddlPeriod").val();
    if (ddlDropInsYearID == '') ddlDropInsYearID = '0';
    if (periodId == '') periodId = '0';
    appliedSelections = [];
    if (periodId != '0' && periodId != '' && ddlDropInsYearID != '' && ddlDropInsYearID != '0') {
        pageLoaderFrame();
        $.ajax({
            type: 'POST',
            data: { companyId: $("#ddlCompany").val(), yearId: $("#ddlKPIYear").val(), isFormDCategory: $("#FormDCategory").is(":checked"), PeriodId: periodId },
            traditional: true,
            url: '/PDRP/AnnualAppraisal/GetAnnualAppraisalProcessList',
            success: function (data) {
                $("#divAnnualAppraisalSettings").html(data);
                $('#tblAnnualAppraisalSettingGrid').DataTable({
                    "aoColumnDefs": [
                        { "sWidth": "5%", "aTargets": [0] },
                        { "sWidth": "10%", "aTargets": [1, 2, 3, 4, 6, 7, 8, 9] },
                        { "sWidth": "15%", "aTargets": [5] }
                    ],
                    "fnDrawCallback": function () {
                        bindSelectpicker('#tblAnnualAppraisalSettingGrid .selectpickerddl');
                        //*** Naresh 2020-02-26 Date picker was not being displayed on grid
                        $('.datepicker').datepicker({
                            dateFormat: HRMSDateFormat,
                            changeMonth: true,
                            changeYear: true,
                            yearRange: '-100:+100',
                            buttonImage: "../../Content/images/calender-icon.png",
                            showOn: "both",
                            buttonImageOnly: true,
                            autoclose: true,
                            buttonImageText: "Calendar",
                            format: HRMSDateFormat,
                            todayHighlight: true
                        });
                    },
                });

                ////if (isFilter) {
                ////    var companyId = appliedSelections[0].companyId;
                ////    if (companyId == $("#ddlCompany").val()) {
                ////        $.each(appliedSelections, function (key, value) {
                ////            var row = $('tr[data-employeeid="' + value["employeeId"] + '"]');
                ////            row.find(".YNClass").prop("checked", true);
                ////            row.find(".evaluationStartDate").val($("#EvaluationStartDate").val());
                ////            row.find(".evaluationDueDate").val($("#EvaluationDueDate").val());
                ////        });
                ////    }
                ////}
                hideLoaderFrame();

            },
            error: function (data, xhr, status) {
                ShowMessage("error", "Some technical error occurred");
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    }
}
function SelectAll() {


    var Bool = false;
    var chk = $("#chkSelevctAll");
    if (chk.prop('checked')) {
        Bool = true;
    }

    var table = $("#tblAnnualAppraisalSettingGrid").DataTable();
    var UpdatedTr1 = table.$(".odd");
    var UpdatedTr2 = table.$(".even");
    $(UpdatedTr1).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');
        if (chbox.is(':disabled')) {
        }
        else {
            chbox.prop('checked', Bool);
        }

        //if (chbox.prop('checked')) {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', Bool);
        //    }

        //}
        //else {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', Bool);
        //    }

        //}
        //ChkChange(this);

    });
    $(UpdatedTr2).each(function () {
        var row = $(this);
        console.log(row);
        var chbox = row.find('#chkEmployeeId');

        if (chbox.is(':disabled')) {
        }
        else {
            chbox.prop('checked', Bool);
        }
        //if (chbox.prop('checked')) {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', false);
        //    }
        //}
        //else {
        //    if (chbox.is(':disabled')) {
        //    }
        //    else {
        //        chbox.prop('checked', true);
        //    }
        //}
        //ChkChange(this);

    });

}
function ChkChange(source) {

    var closesetRow = $(source).closest('tr');
    var vdatepicker = $(closesetRow).find('.datepicker').val();
    //var vchkEmployeeId = $(closesetRow).find('#chkEmployeeId')

    if ($(source).prop("checked") == true) {
        if (vdatepicker.length > 0) {

            var employeeId = $(closesetRow).addClass('IsEdit');
        }
    }
    else if ($(source).prop("checked") == false) {
        var employeeId = $(closesetRow).removeClass('IsEdit');
    }
}