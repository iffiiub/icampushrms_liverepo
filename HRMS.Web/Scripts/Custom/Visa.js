﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridVisa();
    }
    $("#btn_ExportToExcel").click(function () { ExportToExcelVisa(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdfVisa(); });
});

//$("#btn_addVisa").click(function () { AddVisa(); });

var oTableChannel;

function getGridVisa() {

    oTableChannel = $('#tbl_VisaList').dataTable({
        
        "sAjaxSource": "/Visa/GetVisaList",
        "aoColumns": [            
            { "mData": "DocumentNo", "sTitle": "Document<br/>Name/No", 'width': '15%' },
            { "mData": "UidNo", "sTitle": "Uid<br>No", 'width': '15%' },
            { "mData": "IssuePlace", "sTitle": "Issue<br>Place", 'width': '11%' },
            { "mData": "IssueDate", "sTitle": "Issue<br>Date", 'width': '11%', "sType": "date-uk" },
            { "mData": "ExpiryDate", "sTitle": "Expiry<br>Date", 'width': '11%', "sType": "date-uk" },
            { "mData": "SponsorName", "sTitle": "Sponsor<br>Name", 'width': '11%' },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '11%'  },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction vertical-Middle", 'width': '15%' }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Visa/GetVisaList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_VisaList");
        }
    });
}

function AddVisa() {

    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/Visa/AddVisa");
                $("#myModal3").modal({backdrop: 'static'});
                $("#modal_heading3").text('Add Visa ');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditVisa(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/Visa/EditVisa/" + id);
    $("#myModal3").modal({backdrop: 'static'});
    $("#modal_heading3").text('Edit Visa');

}


function DeleteVisa(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Visa/DeleteVisa',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridVisa();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {

    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewVisa(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Visa/ViewVisa/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('View Visa  Details');

}

function ExportToExcelVisa() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Visa/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdfVisa() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Visa/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}
