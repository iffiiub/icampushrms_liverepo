﻿$(function () {

    $('.datepicker').datepicker({
        dateFormat: HRMSDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: HRMSDateFormat,
        todayHighlight: true
    });

    $('.DOB').datepicker({
        dateFormat: HRMSDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: HRMSDateFormat,
        todayHighlight: true
    }).on('change', function () {
        try {
            $(this).valid();  // triggers the validation test
        }
        catch (e)
        { }
    });

    var date = new Date();
    date.setDate(date.getDate() + 1);

    $('.NextDaydatepicker').datepicker({
        dateFormat: HRMSDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: HRMSDateFormat,
        todayHighlight: true,
        startDate: date
    }).on('change', function () {
        try {
            $(this).valid();  // triggers the validation test
        }
        catch (e)
        { }
    });

    $('.CurrentDate').datepicker({
        dateFormat: HRMSDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: HRMSDateFormat,
        todayHighlight: true
    }).datepicker('setDate', new Date());


    $('.datepickerFrom').datepicker({
        dateFormat: HRMSDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: HRMSDateFormat,
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepickerTo').datepicker('setStartDate', minDate);
    }).on('show', function (selected) {
        //if (selected.date != undefined) {
        //    var minDate = new Date(selected.date.valueOf());
        //    $('.datepickerTo').datepicker('setStartDate', minDate);
        //}
    });

    $('.datepickerTo').datepicker({
        dateFormat: HRMSDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+100',
        buttonImage: "../../Content/images/calender-icon.png",
        showOn: "both",
        buttonImageOnly: true,
        autoclose: true,
        buttonImageText: "Calendar",
        format: HRMSDateFormat,
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepickerFrom').datepicker('setEndDate', minDate);
    }).on('show', function (selected) {   
        //if (selected.date != undefined) {
        //    var minDate = new Date(selected.date.valueOf());
        //    $('.datepickerFrom').datepicker('setEndDate', minDate);
        //}
    });
});

function getDatepickerDate(element) {
    var dateFormat = "mm/dd/yy";
    var date;
    try {
        //date = $.datepicker.parseDate(dateFormat, element.value);
        date = $(element).val();
    } catch (error) {
        date = null;
    }
    return date;
}

//$(document).ready(function () {

//    $('#StartDate').datepicker({
//        dateFormat: 'dd-mm-yy',
//        changeMonth: true,
//        changeYear: true,
//        yearRange: '-50:+50',
//        buttonImage: "../../Content/images/calender-icon.png",
//        showOn: "both",
//        buttonImageOnly: true,
//        minDate: new Date(),
//        buttonImageText: "Calendar",
//        onClose: function (selectedDate) {
//            if (selectedDate != '') {
//                $("#EndDate").datepicker("option", "minDate", selectedDate);
//            }
//        }
//    });
//    $('#EndDate').datepicker({
//        dateFormat: 'dd-mm-yy',
//        changeMonth: true,
//        changeYear: true,
//        yearRange: '-50:+50',
//        buttonImage: "../../Content/images/calender-icon.png",
//        showOn: "both",
//        buttonImageOnly: true,
//        //maxDate: new Date(),       
//        buttonImageText: "Calendar",
//        onClose: function (selectedDate) {
//            if (selectedDate != '') {
//                $("#StartDate").datepicker("option", "maxDate", selectedDate);
//            }
//        }
//    });
//});
function FormatDate(dateString) {

    dateString = dateString.substr(6);
    var date = new Date(parseInt(dateString));
    var dd = date.getDate();
    //var mm = date.getMonth() + 1; //January is 0!
    var mm = GetFormattedMonth(date);
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    //if (mm < 10) {
    //    mm = '0' + mm;
    //}
    var date = dd + '/' + mm + '/' + yyyy;
    return date;
}
function FormatDateToString(date) {
    var dd = date.getDate();
    //var mm = date.getMonth() + 1; //January is 0!
    var mm = GetFormattedMonth(date);

    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    //if (mm < 10) {
    //    mm = '0' + mm;
    //}
    var date = dd + '/' + mm + '/' + yyyy;
    return date;
}
function GetFormattedMonth(date) {
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    var m = month[date.getMonth()];
    return m
}