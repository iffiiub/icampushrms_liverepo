﻿
$(document).ready(function () {
    if (tMode != "All") {
        FilterLateDeduction();
    } else { GetGrid(); }
});

$(document).ready(function () {
    $("#IsGenerated").click(function () {
        var isChecked = $("#IsGenerated").is(":checked");
        if (isChecked) {
            $("#GenerateDate").removeAttr('disabled');
            $("#Cycle").attr('disabled', false)
            $("#Cycle").selectpicker('refresh');
        }
        else {
            $("#GenerateDate").attr('disabled', 'disabled');
            $("#Cycle").attr('disabled', true)
            $("#Cycle").selectpicker('refresh');
        }
    });
});


var oTableLateduduction;
var ddType;
function GetGrid() {
    pageLoaderFrame();
    ddType = $('#ddl_DeductionType').val();
    if (ddType == 2) {
        $('#isDeductionRulesEnable').prop('checked', false);
    }
    var isMarkingExcused = $("#isMarkingExcused").is(":checked");
    var isMarkingDeduct = $("#isMarkingDeduct").is(":checked");
    var ExcusedCheckBox = "Excused   <input type='checkbox' id='chkAllExcused'  onclick='CheckAllExcused()'/>";
    var DeductCheckBox = "Deduct   <input type='checkbox' id='chkAllDeduct' class='chkExcusedCheck hide' onclick='CheckAllDeduct()'/>";
    if (isMarkingExcused) {
        ExcusedCheckBox = "Excused   <input type='checkbox' id='chkAllExcused'  onclick='CheckAllExcused()'/>";
    }
    else {
        ExcusedCheckBox = "Excused   <input type='checkbox' id='chkAllExcused' disabled onclick='CheckAllExcused()'/>";
    }
    if (isMarkingDeduct) {
        DeductCheckBox = "Deduct   <input type='checkbox' id='chkAllDeduct' class='chkExcusedCheck hide' onclick='CheckAllDeduct()'/>";
    }
    else {
        DeductCheckBox = "Deduct   <input type='checkbox' id='chkAllDeduct' disabled class='chkExcusedCheck hide' onclick='CheckAllDeduct()'/>";
    }

    oTableLateduduction = $('#tbl_LateDeduction').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4"><".col-md-4 pageNo"p>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "FromDate", "value": $('#FromDate').val() },
                { "name": "ToDate", "value": $('#ToDate').val() },
                { "name": "DeductionType", "value": $('#ddl_DeductionType').val() },
                { "name": "IsExcused", "value": $('#chkGetExcused').is(':checked') }
                );
        },
        "sAjaxSource": "/LateDeduction/GetDeductableEmployeeList",
        "aoColumns": [
            { "mData": "EmployeeAlternativeId", "sTitle": "Emp. ID", "sType": "Int", "sWidth": "4%" },
            { "mData": "EmployeeName", "sTitle": "Employee Name", "sWidth": "12%" },
            { "mData": "AttendanceDate", "sTitle": "Attendance Date", "sType": "date-uk", "sWidth": "12%" },
            { "mData": "Time", "sTitle": "InTime", "sWidth": "8%" },
            { "mData": "DeductableMinutes", "sTitle": "Deductable Late<br/>Minutes", "sWidth": "10%" },
            { "mData": "Amount", "sTitle": "Amount", "sWidth": "12%" },            
            { "mData": "excused", "sTitle": ExcusedCheckBox, "bSortable": false, "sWidth": "10%" },
            { "mData": "deduct", "sTitle": DeductCheckBox, "bSortable": false, "sWidth": "10%" },
            { "mData": "DelayToOtherWorkers", "sTitle": "Delay To Other Workers   <input type='checkbox' id='chkAllDeduct' class='chkExcusedCheck hide' onclick='CheckAllDeduct()'/>", "bSortable": false, "sWidth": "12%" },
            { "mData": "LateReason", "sTitle": "Late Reason", "sWidth": "16%" },
            { "mData": "Actions", "sTitle": "Action", "sWidth": "16%" },
            { "mData": "EmployeeID", "bVisible": false },
            { "mData": "NoOfNullCycleCount", "sTitle": "NoOfNullCycleCount", "sClass": "hidden" }
        ],
        "processing": true,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "autoWidth": false,
        "drawCallback": function (settings) {
            var isRuleApplied = $("#isDeductionRulesEnable").is(":checked");
            if (isRuleApplied == false) {
                $('td:nth-child(9),th:nth-child(9)').hide();
            }
            var isShowAmtColumn = $("#isShowAmountColumn").is(":checked");
            if (!isShowAmtColumn)
            {
                $('td:nth-child(6),th:nth-child(6)').hide();
            }
            
            if (ddType == 1) {
                $('#tbl_LateDeduction tr:eq(0) th:eq(3)').text("In Time");
                $('#tbl_LateDeduction tr:eq(0) th:eq(4)').text("Deductable Late Minutes");
                $('#tbl_LateDeduction tr:eq(0) th:eq(9)').text("Late Reason");
            }
            else {
                $('#tbl_LateDeduction tr:eq(0) th:eq(3)').text("Out Time");
                $('#tbl_LateDeduction tr:eq(0) th:eq(4)').text("Deductable Early Minutes");
                $('#tbl_LateDeduction tr:eq(0) th:eq(9)').text("Early Reason");
            }
            MarkExcusedAll();
            var itemValue = $('#tbl_LateDeduction tbody tr:first td:last').html();
            if (itemValue == "No data available in table")
            {
                $("#warningMessageCycle").addClass("hidden");
            }
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData["NoOfNullCycleCount"] > 0) {
                $("#warningMessageCycle").removeClass("hidden");
            }
            else {
                $("#warningMessageCycle").addClass("hidden");
            }


        },
        "initComplete": function (settings, json) {
            var itemValue = $('#tbl_Deductable tbody tr:first td:last').html();
            if (itemValue == "No data available in table") {                
                $("#AcWarningMessage").addClass("hidden");
            }
            else {
                GetNullAcYearMessage();
            }
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('LateDeductionPageNo', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('LateDeductionPageNo')); 
        }
    });
    hideLoaderFrame();
}

function CheckAllExcused() {
    if ($("#chkAllExcused").is(':checked')) {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to mark all employees to excused.?" }).done(function () {
            $('.chkExcusedCheck', oTableLateduduction.fnGetNodes()).prop('checked', true);
            $(".chkExcusedCheck").closest('tr').addClass("selected");
            $('.chkDeductCheck', oTableLateduduction.fnGetNodes()).prop('checked', false);
            $("#chkAllDeduct").prop('checked', false);
            UpdateisExcusedStatusWithAllEmployee();
        }).fail(function () {
            $("#chkAllExcused").prop('checked', false)
        });
    }
    else {
        $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to mark all employees to deductable.?" }).done(function () {
            $('.chkExcusedCheck', oTableLateduduction.fnGetNodes()).prop('checked', false);
            $('.chkDeductCheck', oTableLateduduction.fnGetNodes()).prop('checked', true);
            $(".chkExcusedCheck").closest('tr').removeClass("selected");
            $("#chkAllDeduct").prop('checked', true);
            UpdateisExcusedStatusWithAllEmployee();
        }).fail(function () {
            $("#chkAllExcused").prop('checked', true)
        });
    }

}

function CheckAllDeduct() {
    if ($("#chkAllDeduct").is(':checked')) {
        $('.chkDeductCheck', oTableLateduduction.fnGetNodes()).prop('checked', true);
        $(".chkDeductCheck").closest('tr').addClass("selected");
        $('.chkExcusedCheck', oTableLateduduction.fnGetNodes()).prop('checked', false);
        $("#chkAllExcused").prop('checked', false);
    }
    else {
        $('.chkDeductCheck', oTableLateduduction.fnGetNodes()).prop('checked', false);
        $(".chkDeductCheck").closest('tr').removeClass("selected");
    }
    UpdateisExcusedStatusWithAllEmployee();
}

function checkall() {
    if ($('#chkAll').is(':checked')) {
        $(".checkbox").prop("checked", true);
    }
    else {
        $(".checkbox").prop("checked", false);
    }
}

function Confirm() {


    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    var GenerateDate = $("#GenerateDate").val();



    if (FromDate != "" && ToDate != "" && GenerateDate != "" && $('#IsGenerated').is(':checked') == true) {
        var x = 0;

        var stringArray = new Array();

        var lateDeductionList = [];
        $("#tbl_LateDeduction tbody tr").each(function (i, item) {
            var id = $(this).find('.checkbox').attr('id');
            if ($(this).find('.checkbox').prop('checked')) {
                var curRow = {};
                curRow.EmployeeId = $(this).find('.checkbox').attr('id');
                curRow.Amount = $(this).find('#lblAmount').text();
                curRow.FromDate = FromDate;
                curRow.ToDate = ToDate;
                curRow.GenerateDate = GenerateDate;
                lateDeductionList.push(curRow);
            }
        });

        //$('.checkbox').each(function () {
        //    //alert(this.id);
        //    var $row = $(this).closest("tr");


        //    if ($("#" + this.id).is(':checked')) {

        //        stringArray[x]= this.id;
        //        stringArray[x] = this.id;
        //        x++;
        //        //alert(this.id);
        //    }
        //});
        //stringArray[0] = 45;
        //alert(stringArray.length);


        //   alert(lateDeductionList.length);
        //var postData = { lateDeductionList: lateDeductionList, FromDate: FromDate, ToDate: ToDate, GenerateDate: GenerateDate };
        if (lateDeductionList.length != 0) {
            lateDeductionList = JSON.stringify({ 'lateDeductionList': lateDeductionList });
            $.ajax({
                url: '/LateDeduction/SaveLateDeduction',
                type: 'POST',
                data: lateDeductionList,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',

                success: function (data) {
                    //alert(data.Result);
                    ShowMessage("success", "Late addition gererated successfully.");
                    window.setTimeout(function () { location.reload() }, 1000)
                },
            });
        }
        else {
            ShowMessage("error", "Please select employees");
        }
    }
    else {
        ShowMessage("error", "Please Select Date to confirm and Check Generate");
    }
}

function updateExcusedStatus(PayEmployeeLateID, e) {
    if ($("#chkDeduct" + PayEmployeeLateID).is(":checked")) {
        $("#chkDeduct" + PayEmployeeLateID).prop('checked', false);
    }
    else {
        $("#chkDeduct" + PayEmployeeLateID).prop('checked', true);
        $("#chkAllExcused").prop('checked', false);
    }
    var isDeucted = $("#chkDeduct" + PayEmployeeLateID).is(":checked");
    var isExcused = $("#chkExcused" + PayEmployeeLateID).is(":checked");
    $.ajax({
        url: '/LateDeduction/UpdateisExcusedStatus',
        type: 'POST',
        data: {
            payEmployeeLateID: PayEmployeeLateID,
            isExcused: isExcused,
            deductionType: ddType,
            isDeducted: isDeucted
        },
        success: function (data) {
            //alert(data.Result);
            MarkExcusedAll();
            ShowMessage(data.result, data.resultMessage);          
        },
    });
}

function UpdateLateReason(payEmployeeLateId)
{
    $("#modal_Loader3").load("/LateDeduction/UpdateLateReason?payEmployeeLateID=" + payEmployeeLateId, function () {
        $("#myModal3").modal("show");
        $("#modal_heading3").text('Enter Late Reason:');
    });
}

function updateDeductionStatus(PayEmployeeLateID, e) {
    if ($("#chkExcused" + PayEmployeeLateID).is(":checked")) {
        $("#chkExcused" + PayEmployeeLateID).prop('checked', false);
        $("#chkAllExcused").prop('checked', false);
    }
    else {
        $("#chkExcused" + PayEmployeeLateID).prop('checked', true);
    }

    var isDeucted = $("#chkDeduct" + PayEmployeeLateID).is(":checked");
    var isExcused = $("#chkExcused" + PayEmployeeLateID).is(":checked");
    $.ajax({
        url: '/LateDeduction/UpdateisDeductedStatus',
        type: 'POST',
        data: {
            payEmployeeLateID: PayEmployeeLateID,
            isDeducted: isDeucted,
            deductionType: ddType,
            isExcused: isExcused
        },
        success: function (data) {
            //alert(data.Result);
            ShowMessage(data.result, data.resultMessage);
            MarkExcusedAll();
        },
    });

}



function UpdateisExcusedStatusWithAllEmployee() {
    var FromDate = $('#FromDate').val();
    var ToDate = $('#ToDate').val();
    var DeductionType = $('#ddl_DeductionType').val();
    //var IsExcused = $('#chkGetExcused').is(':checked');
    var isExcusedAll = $('#chkAllExcused').is(':checked');
    var isDeductAll = $('#chkAllDeduct').is(':checked');
    $.ajax({
        url: '/LateDeduction/UpdateisExcusedStatusWithAllEmployee',
        type: 'POST',
        data: {
            FromDate: FromDate,
            ToDate: ToDate,
            DeductionType: DeductionType,
            //   IsExcused: IsExcused,
            isExcusedAll: isExcusedAll,
            isDeductAll: isDeductAll
        },
        success: function (data) {
            //alert(data.Result);
            ShowMessage(data.result, data.resultMessage);

        },
    });
}

function ConfirmEmployeeDeductionStatus() {

    if (FromDate != "" && ToDate != "" && GenerateDate != "" && $('#IsGenerated').is(':checked') == true) {

        var FromDate = $('#FromDate').val();
        var ToDate = $('#ToDate').val();
        var GenerateDate = $('#GenerateDate').val();
        var CycleID = $('#Cycle').val();
        if (CycleID == 0) {
            ShowMessage("error", "Please Select Cycle");
        }
        else {
            $.ajax({
                url: '/LateDeduction/SaveLateDeduction',
                type: 'POST',
                data: {
                    FromDate: FromDate,
                    ToDate: ToDate,
                    GenerateDate: GenerateDate,
                    deductionType: ddType,
                    CycleID: CycleID
                },
                success: function (data) {
                    //alert(data.Result);
                    ShowMessage(data.result, data.resultMessage);
                    if (data.result == "success") {
                        window.setTimeout(function () { location.reload() }, 1000)
                    }
                },
            });
        }

    }
    else {
        ShowMessage("error", "Please Select Date to confirm and Check Generate");
    }
}


function MarkExcusedAll() {
    var FromDate = $('#FromDate').val();
    var ToDate = $('#ToDate').val();
    var DeductionType = $('#ddl_DeductionType').val();
    //var IsExcused = $('#chkGetExcused').is(':checked');
    var isExcusedAll = $('#chkAllExcused').is(':checked');
    var isDeductAll = $('#chkAllDeduct').is(':checked');
    $.ajax({
        url: '/LateDeduction/CheckAllExcused',
        type: 'POST',
        data: {
            FromDate: FromDate,
            ToDate: ToDate,
            DeductionType: DeductionType
        },
        success: function (data) {
            if (data.CheckAll == true) {
                $("#chkAllExcused").prop('checked', true);
            }
            else {
                $("#chkAllExcused").prop('checked', false);
            }

        }
    });
}

function updateAmountForDelayCause(PayEmployeeLateId, EmployeeID, LateMinutes, CycleID, CountLateInCycle, e) {
    var isChecked = $(e).is(":checked")
    $.ajax({
        url: '/LateDeduction/UpdateCauseDelayStatus',
        type: 'POST',
        data: {
            PayEmployeeLateId: PayEmployeeLateId,
            EmployeeID: EmployeeID,
            LateMinutes: LateMinutes,
            CycleID: CycleID,
            isDelayCaused: isChecked,
            CountLateInCycle: CountLateInCycle
        },
        success: function (data) {
            ShowMessage("success", "Record save successfully.")
            $(e).closest("tr").find('td:eq(5)').text(data.Amount);
        },
        error: function (data) {
            ShowMessage();
        }



    });


}

function GetNullAcYearMessage() {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Startdate: $('#FromDate').val(), EndDate: $('#ToDate').val() ,deductiontype:$('#ddl_DeductionType').val()  },
        url: '/LateDeduction/GetNullAcademicYears',
        success: function (data) {
            if (data.AcRecords != '') {
                $("#AcWarningMessage").removeClass("hidden");
                $("#lblAcYearWarnong").text(data.Message);

            }
            else {
                $("#AcWarningMessage").addClass("hidden");
            }
        }
    });

}

