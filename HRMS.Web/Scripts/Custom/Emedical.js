﻿
$("#btn_addMedicalVaccination").click(function () {
    if (parseInt($("#EmployeeID").val()) > 0 && $("#EmployeeID").val() !== undefined) {
        var EmployeeId = $("#EmployeeID").val();
        $("#ActiveEmployee").val(EmployeeId);
        AddMedicalVaccination();
    }
    else {
        ShowMessage("error", "Please select employee.")
    }
});
$("#btn_addMedicalHistory").click(function () {
    if (parseInt($("#EmployeeID").val()) > 0 && $("#EmployeeID").val() !== undefined) {
        var EmployeeId = $("#EmployeeID").val();
        $("#ActiveEmployee").val(EmployeeId);
        AddMedicalHistory();
    }
    else {
        ShowMessage("error", "Please select employee.")
    }
});
$("#btn_addMedicalalerts").click(function () {
    if (parseInt($("#EmployeeID").val()) > 0 && $("#EmployeeID").val() !== undefined) {
        var EmployeeId = $("#EmployeeID").val();
        $("#ActiveEmployee").val(EmployeeId);
        AddMedicalAlert();
    }
    else {
        ShowMessage("error", "Please select employee.")
    }
});

var oTableChannel;

function EditInsuranceDependence(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Insurance/EditInsdependence/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Insdependence');
        $("form").find('input[type="submit"]').val('Update');
    });
}

function getGridMedicalHistory(empid) {

    oTableChannel = $('#tbl_MedicalHistory').dataTable({
        "sAjaxSource": "/EMedical/GetMedicalHistory",
        "aoColumns": [
                        { "mData": "EmployeeHealthHistoryDate", "sTitle": "Date", "sType": "date-uk" },
                        { "mData": "Description", "sTitle": "Description" },
                        { "mData": "Action_Result", "sTitle": "Action/Result" },
                        { "mData": "Note", "sTitle": "Note" },
                        { "mData": "Action", "sTitle": "Action", "sWidth": "2%", "bSortable": false, "sClass": "ClsAction" }
        ],

        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "ajax": "/EMedical/GetMedicalHistory",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": false,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_MedicalHistory");
        },
        "bSortCellsTop": true

    });
}


function getGridMedicalVaccination(empid) {

    oTableChannel = $('#tbl_MedicalVaccination').dataTable({
        "sAjaxSource": "/EMedical/GetMedicalVaccination",
        "aoColumns": [
                        { "mData": "EmployeeVaccinationID", "sTitle": "EmployeeVaccination", "bVisible": false },
                        { "mData": "EmployeeVaccinationDate", "sTitle": "Date", "sType": "date-uk" },
                        { "mData": "Vaccination", "sTitle": "Vaccination" },
                        { "mData": "Administrated", "sTitle": "Administrated" },
                        { "mData": "Note", "sTitle": "Note", "sWidth": "2%" },
                        { "mData": "Action", "sTitle": "Action", "bSortable": false, "sClass": "ClsAction" },
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "ajax": "/EMedical/GetMedicalVaccination",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": false,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_MedicalVaccination");
        },
        "bSortCellsTop": true
    });
}





function getGridMedicalAlert(empid) {

    oTableChannel = $('#tbl_MedicalMainAlerts').dataTable({
        "sAjaxSource": "/EMedical/GetMedicalAlertList",
        "aoColumns": [
                        { "mData": "EmployeeMedicalAlertID", "sTitle": "EmployeeMedicalAlertID", "bVisible": false },
                        { "mData": "MedicalAlert", "sTitle": "Alert(ENG)" },
                        { "mData": "MedicalAlertAr", "sTitle": "Alert(Arabic)" },
                        { "mData": "Action", "sTitle": "Action", "bSortable": false, "sClass": "ClsAction" }
        ],
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid });
        },
        "processing": true,
        "serverSide": false,
        "ajax": "/EMedical/GetMedicalAlertList",
        "aLengthMenu": [[8, 10, 25, 50, 100, 1000], [8, 10, 25, 50, 100, "All"]],
        "iDisplayLength": 8,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": false,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_MedicalMainAlerts");
        },
        "bSortCellsTop": true

    });
}
function AddMedicalAlert() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/AddMedicalAlert", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add Medical Alert');
    });
}

function AddMedicalVaccination() {
    $("#addPaySalaryModal_Loader").html("");
    $("#addPaySalaryModal_Loader").load("/EMedical/AddMedicalVaccination");
    $("#addPaySalaryModal").modal("show");
    $("#addPaySalaryModal_heading").text('Add Medical Vaccination');
}


function AddMedicalHistory() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/AddMedicalHistory");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Medical History');
}

function EditMedicalVaccination(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/EditMedicalVaccination/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Medical Vaccination');
        //$("form").find('input[type="submit"]').val('Update');
    });
}

function EditHealthHistory(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/EditMedicalHistory/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Medical History');
        //$("form").find('input[type="submit"]').val('Update');
    });
}

function EditAbsent(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Absent/EditAbsent/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Absent');
        //$("form").find('input[type="submit"]').val('Update');
    });
}

function EditMedicalAlert(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/EMedical/EditMedicalAlert/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Medical Alert');
        //$("#frmAddMedicalAlert").find('input[type="submit"]').val('Update');
    });
}

function DeleteHealthHistory(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EMedical/DeleteHealthHistory',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                var id = $("#hdnEmployee_ID").val();


                getGridMedicalHistory(id);
            },
            error: function (data) { }
        });
    });
}


function DeleteMedicalVaccination(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EMedical/DeleteMedicalVaccination',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                var id = $("#hdnEmployee_ID").val();
                getGridMedicalVaccination(id);
            },
            error: function (data) { }
        });
    });
}

function DeleteMedicalAlert(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/EMedical/DeleteMedicalAlert',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                var id = $("#hdnEmployee_ID").val();

                getGridMedicalAlert(id);

            },
            error: function (data) { }
        });
    });
}