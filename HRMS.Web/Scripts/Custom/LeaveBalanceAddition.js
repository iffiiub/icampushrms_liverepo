﻿$(document).ready(function () { 
    loadLeaveBalanceAdditionGrid();
    $(document).on('click', '#btnAddLeaveBalance', function (e) {
        $("#modal_Loader").load("/LeaveBalanceAddition/AddEditLeaveBalanceForm/", function () {
            $("#myModal").modal("show");       
            $("#modal_heading").text('Add Leave Balance');
        });
    });

    $(document).on('click', '#btnSearch', function (e) {
        loadLeaveBalanceAdditionGrid();
    });

    $(document).on('change', '#ddlEmployee, #EmployeeID', function () {
       
        selectList = "";
        var count = 0;
        selectList = "<option value=\"\">Select Vacation Type</option>";
        var EmpID = $(this).val() == "" ? 0 : $(this).val();
        if (EmpID > 0) {
            pageLoaderFrame();
            $.ajax({
                type: 'get',
                url: '/LeaveBalanceAddition/LoadVacationTypesBasedOnEmployee?employeeId=' + EmpID,
                datatype: 'Json',
                success: function (data) {
                    $.each(data, function (key, value) {
                        count = count + 1;
                        selectList += '<option value=' + value.VacationTypeId + '>' + value.Name + '</option>';
                    });
                    if ($("#IsAddMode").val() == undefined) {
                        $("#ddlVacationType").html();
                        $("#ddlVacationType").html(selectList);
                        RefreshSelect("#ddlVacationType");
                    }
                    else {
                        $("#VacationTypeID").html();
                        $("#VacationTypeID").html(selectList);
                        RefreshSelect("#ddlVacationType");
                        RefreshSelect("#VacationTypeID");
                    }

                    if (count == 0) {
                        ShowMessage("error", "Selected employee is not linked with any leave type, Please assign first any leave type.");
                    }
                    hideLoaderFrame();
                }

            });
        }
    });

    $(document).on('change', '#VacationTypeID', function ()
    {
      
        var vacationTypeid = $("#VacationTypeID").val();
        pageLoaderFrame();
        $.ajax({
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: '/LeaveBalanceAddition/GetVacationType',
            data: { leaveTypeID: vacationTypeid },
            async: false,           
            success: function (result) {
                hideLoaderFrame();
                if (result != null) {                   
                    //vacationtype = result;
                    var vtcategoryid = result.VTCategoryID;
                    EnableDisableDaysFields(vtcategoryid);
                    //hideLoaderFrame();
                }
            },
            error: function (err) {
                ShowMessage('error', err.statusText);
                hideLoaderFrame();
            }
        });

       


    });
});
function EnableDisableDaysFields(vtcategoryid)
{
    // alert(vtcategoryid);
    //halfpaid and unpaid days enabled for Sick leave only
    if (vtcategoryid == 2) {
        $("#txtFullPaidDays").attr("readonly", false);
        $("#txtHalfPaidDays").attr("readonly", false);
        $("#txtUnPaidDays").attr("readonly", false);
    }
        //unpaid
    else if (vtcategoryid == 8) {
        $("#txtFullPaidDays").val(0);
        $("#txtHalfPaidDays").val(0);
        $("#txtUnPaidDays").attr("readonly", false);
        $("#txtHalfPaidDays").attr("readonly", true);
        $("#txtFullPaidDays").attr("readonly", true);

    }
        //Hajj Leave
    else if (vtcategoryid == 7) {
        $("#txtHalfPaidDays").val(0);
        $("#txtUnPaidDays").attr("readonly", false);
        $("#txtHalfPaidDays").attr("readonly", true);
        $("#txtFullPaidDays").attr("readonly", false);
    }
    else {
        $("#txtHalfPaidDays").val(0);
        $("#txtUnPaidDays").val(0);
        $("#txtFullPaidDays").attr("readonly", false);
        $("#txtHalfPaidDays").attr("readonly", true);
        $("#txtUnPaidDays").attr("readonly", true);

    }
}
function loadLeaveBalanceAdditionGrid() {
    var employeeId = $("#ddlEmployee").val();
    var vacationTypeId = $("#ddlVacationType").val(); 
    oTableChannel = $('#tblLeaveBalanceAddition').dataTable({
        //"dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/LeaveBalanceAddition/GetLeaveBalanceAdditionList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "employeeId", "value": employeeId });
            aoData.push({ "name": "vacationTypeId", "value": vacationTypeId });
        },
        "aoColumns": [

            { "mData": "EmployeeID", "bVisible": true, "bSortable": true, "sTitle": "Employee ID", 'width': '7%' },
            { "mData": "EmployeeName", "bVisible": true, "bSortable": true, "sTitle": "Employee Name", 'width': '20%' },
            { "mData": "VacationTypeName", "bVisible": true, "bSortable": true, "sTitle": "Leave Type Name", 'width': '10%' },
            { "mData": "FullPaidDays", "bVisible": true, "sTitle": "FullPaid Days", 'width': ' 7%' },
              { "mData": "HalfPaidDays", "bVisible": true, "sTitle": "HalfPaid Days", 'width': '7%' },
                { "mData": "UnPaidDays", "bVisible": true, "sTitle": "UnPaid Days", 'width': '7%' },
            { "mData": "FormattedLeaveEffectiveDate", "bSortable": true, "sTitle": "Effective Date", 'width': '8%', "sType": "date-uk" },
            { "mData": "Comments", "bVisible": true, "sTitle": "Comments", 'width': '15%' },
            { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '10%' }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/LeaveBalanceAddition/GetLeaveBalanceAdditionList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[2, "asc"]],
        "fnDrawCallback": function () {
           // hideLoaderFrame();
        }
    });
   
}

function editLaveBalance(id) {
    $("#modal_Loader").load("/LeaveBalanceAddition/AddEditLeaveBalanceForm?leaveBalanceId=" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Leave Balance');
    });
}


function deleteLeaveBalance(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { leaveAdditionId: id },
            url: '/LeaveBalanceAddition/DeleteLeaveBalance',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                loadLeaveBalanceAdditionGrid();
            },
            error: function (data) { }
        });
    });
}