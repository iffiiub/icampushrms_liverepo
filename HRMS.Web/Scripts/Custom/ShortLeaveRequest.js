﻿$("#btnGetShortLeaveRecords").click(function () {
    var d1 = $("#txtStartDate").val();
    var d2 = $("#txtToDate").val();
    var id = $("#ddl_EmpList").val();
    getGridShortLeaveLateInEarlyOut(id, d1, d2);
});

$("#btn_addShortLeaveLateInEarlyOut").click(function () {
    AddShortLeaveLateInEarlyOut();
});

var oTableChannel;

function getGridShortLeaveLateInEarlyOut(empid, startDate, endDate) {
    oTableChannel = $('#tbl_ShortLeaveInEarlyOutList').dataTable({
        "sAjaxSource": "/ShortLeaveRequest/GetShortLeaveRequestList",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "empid", "value": empid }, { "name": "FromDate", "value": startDate }, { "name": "ToDate", "value": endDate });
        },
        "aoColumns": [
            { "mData": "OrganizationName", "sTitle": "Org. Name", 'width': '5%' },
            { "mData": "EmpID", "sTitle": "Emp ID", 'width': '3%' },
            { "mData": "EmployeeName", "sTitle": "Employee Name", 'width': '15%' },
            { "mData": "LeaveDate", "sTitle": "Leave Date", "sType": "date-uk", "bSortable": true, 'width': '9%' },
            { "mData": "LeaveTime", "sTitle": "Leave Time", 'width': '5%' },
            { "mData": "ReturnTime", "sTitle": "Return Time", 'width': '5%' },
            { "mData": "LateMinutes", "sTitle": "Late Min.", 'width': '5%' },
            { "mData": "EarlyMinutes", "sTitle": "Early Min.", 'width': '5%' },
            { "mData": "ApprovalStatus", "sTitle": "Approval Status", 'width': '5%' },
            { "mData": "Status", "sTitle": "Status", 'width': '5%' },
            { "mData": "Action", "sTitle": "Actions", "sClass": "ClsAction", "bSortable": false, 'width': '15%' }
        ],
        "processing": false,
        "serverSide": false,
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        //"scrollX": true,
        "bSortCellsTop": true,
        "order": [[0, "desc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_ShortLeaveInEarlyOutList");
        }
    });
}

function AddShortLeaveLateInEarlyOut() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/ShortLeaveRequest/AddShortLeaveRequest", function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add Short Leave');
        $('.timePicker12').timepicker({
            showMeridian: true,
            showInputs: true,
            minuteStep: 5,
            defaultTime: '12:00 AM'
        });
        bindSelectpicker(".selectpicker");
    });
}

function CalculateLateminutes(MinutesId) {
    var TotalEarlyMinutes = GetEarlyMinutes(), TotalLateMinutes = GetLateMinutes(), status = '';

    if (MinutesId == "EarlyMinutes") {
        if (TotalEarlyMinutes < 0)
            $("#EarlyMinutes").val(0);
        else
            $("#EarlyMinutes").val(TotalEarlyMinutes);
    }
    else if (MinutesId == "LateMinutes") {
        if (TotalLateMinutes < 0)
            $("#LateMinutes").val(0);
        else
            $("#LateMinutes").val(TotalLateMinutes);
    }

    if ($("#ActualLeaveTime").val() == "" && $("#ReturnNextDay").is(":checked") == false)
        status = "Didn't sign in";
    else if ($("#ActualReturnTime").val() == "")
        status = "Didn't sign out";
    else if (TotalEarlyMinutes > 0 && TotalLateMinutes > 0)
        status = 'Early and Late';
    else if (TotalEarlyMinutes > 0)
        status = 'Early';
    else if (TotalLateMinutes > 0)
        status = 'Late';
    else status = 'On time';

    $("#Status").text(status);
}

function GetEarlyMinutes() {
    var leaveTime = ConvertTo24Hour($("#LeaveTime").text());
    var actualLeaveTime = ConvertTo24Hour($("#ActualLeaveTime").val());

    var LeaveTimeHrs = new Date("1970-1-1 " + leaveTime).getHours();
    var LeaveTimeHrsTemp = LeaveTimeHrs;

    if (LeaveTimeHrs < 12) {
        LeaveTimeHrs = 24 - LeaveTimeHrs;
    }
    var LeaveTimeMns = new Date("1970-1-1 " + leaveTime).getMinutes();
    var ActualLeaveTimeHrs = new Date("1970-1-1 " + actualLeaveTime).getHours();
    if (ActualLeaveTimeHrs == 0 && LeaveTimeHrs > 12) {
        ActualLeaveTimeHrs = 24;
    }
    if (ActualLeaveTimeHrs == 0 && LeaveTimeHrs < 12) {
        LeaveTimeHrs = 12;
    }
    if (ActualLeaveTimeHrs < 12) {
        LeaveTimeHrs = LeaveTimeHrsTemp;
    }
    var ActualLeaveTimeMns = new Date("1970-1-1 " + actualLeaveTime).getMinutes();
    var LeaveTimeTotalMns = (LeaveTimeHrs * 60) + LeaveTimeMns;
    ActualLeaveTimeMns = (ActualLeaveTimeHrs * 60) + ActualLeaveTimeMns;
    return (LeaveTimeTotalMns - ActualLeaveTimeMns);
}

function GetLateMinutes() {
    var returnTime = ConvertTo24Hour($("#ReturnTime").text());
    var actualReturnTime = ConvertTo24Hour($("#ActualReturnTime").val());

    var ReturnTimeHrs = new Date("1970-1-1 " + returnTime).getHours();
    var ReturnTimeHrsTemp = ReturnTimeHrs;
    if (ReturnTimeHrs < 12) {
        ReturnTimeHrs = 24 - ReturnTimeHrs;
    }
    var ReturnTimeMns = new Date("1970-1-1 " + returnTime).getMinutes();
    var ActualReturnTimeHrs = new Date("1970-1-1 " + actualReturnTime).getHours();
    if (ActualReturnTimeHrs < 12) {
        ReturnTimeHrs = ReturnTimeHrsTemp;
    }
    var ActualReturnTimeMns = new Date("1970-1-1 " + actualReturnTime).getMinutes();
    var ReturnTimeTotalMns = (ReturnTimeHrs * 60) + ReturnTimeMns;
    ActualReturnTimeMns = (ActualReturnTimeHrs * 60) + ActualReturnTimeMns;
    return (ActualReturnTimeMns - ReturnTimeTotalMns);
}

function ConvertTo24Hour(time) {
    if (time == "" || time == undefined) return '';
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return sHours + ":" + sMinutes;
}

function EditShortLeaveRequest(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/ShortLeaveRequest/EditShortLeaveRequest/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Edit Short Leave Request');
        $('.timePicker12').timepicker({
            showMeridian: true,
            showInputs: true,
            minuteStep: 5
        });
        $('#timepicker').timepicker('setTime', $('#timepicker').val());
    });
}

function DeleteShortLeaveRequest(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/ShortLeaveRequest/DeleteShortLeaveRequest',
            success: function (data) {
                var id = $("#hdnEmployee").val();
                ShowMessage("success", data.Message);
                var id = $("#ddl_EmpList").val();
                var d1 = $("#txtStartDate").val();
                var d2 = $("#txtToDate").val();
                getGridShortLeaveLateInEarlyOut(id, d1, d2);
            },
            error: function (data) { }
        });
    }).fail(function () { });
}

function ReviewShortLeaveRequest(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/ShortLeaveRequest/ReviewShortLeaveRequest/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Review Short Leave Request');
        bindSelectpicker(".selectpickerddl");
    });
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        var startDate = $("#txtStartDate").val();
        var endDate = $("#txtToDate").val();
        var empId = $("#ddl_EmpList").val();
        window.location.assign("/ShortLeaveRequest/ExportToExcel?empId=" + empId + "&startDate=" + startDate + "&endDate=" + endDate);
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        var startDate = $("#txtStartDate").val();
        var endDate = $("#txtToDate").val();
        var empId = $("#ddl_EmpList").val();
        window.location.assign("/ShortLeaveRequest/ExportToPdf?empId=" + empId + "&startDate=" + startDate + "&endDate=" + endDate);
    }
    else ShowMessage("warning", "No data for export!");
}

function ViewShortLeaveRequest(id, EmpId) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/ShortLeaveRequest/ViewShortLeaveRequest?id=" + id + "&EmpId=" + EmpId);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View Short Leave Details');

}

function UpdateActualTimes(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/ShortLeaveRequest/UpdateActualTimes/" + id, function () {
        $("#myModal").modal("show");
        $("#modal_heading").text('Add/Edit Actual Times');
        $('.timePicker12').timepicker({
            showMeridian: true,
            showInputs: true,
            minuteStep: 5
        });
        $('#timepicker').timepicker('setTime', $('#timepicker').val());
    });
}
