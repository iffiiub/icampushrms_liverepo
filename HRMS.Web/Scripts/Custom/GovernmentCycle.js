﻿var btnSubmite = "Update";
$(document).ready(function () {
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));

    getGrid();

    $("#btn_add").click(function () {
        btnSubmite = "Add";
        EditChannel(-1);
    });

    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);
    $("#btnTextClear").click(function () {
        $('#txtSearch').val(""); // $('#txtSearch').val() == "";
        getGrid();
    });
    $("#btnTextSearch").click(function () {

        getGrid();
    });

});

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_company').dataTable({
        
        "sAjaxSource": "/GovernmentCycle/GetGovernmentCycleList" ,
        "aoColumns": [

            { "mData": "GovernmentCycleID", "bVisible": false, "sTitle": "GovernmentCycleID" },
            { "mData": "GovernmentCycleName_1", "bSortable": true, "sTitle": "Government Cycle Name (En)", 'width': '28%' },
           { "mData": "GovernmentCycleName_2", "bSortable": true, "sTitle": "Government Cycle Name (Ar)", 'width': '28%' },
           { "mData": "GovernmentCycleName_3", "bSortable": true, "sTitle": "Government Cycle Name (Fr)", 'width': '28%' },
           { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '16%' }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/GovernmentCycle/GetGovernmentCycleList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {
            
        }
    });
}

function EditChannel(id) {
    var buttonName = id > -1 ? "Update" : "Submit";
    $("#modal_Loader").load("/GovernmentCycle/Edit/" + id, function () {
        $("#myModal").modal("show");
        if (id > -1) {
            $("#modal_heading").text('Edit Government Cycle');
        }
        else {
            $("#modal_heading").text('Add Government Cycle');
        }
        //$("#form0").find('input[type="submit"]').val(btnSubmite);

    });
}


function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/GovernmentCycle/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}



//}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/GovernmentCycle/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/GovernmentCycle/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}