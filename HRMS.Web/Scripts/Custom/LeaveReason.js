﻿$(document).ready(function () {

    //alert("IN");
    getGrid();    
    $("#btn_add").click(function () {

        EditChannel(-1);
    });
    $("#btn_ExportToExcel").click(function () { ExportToExcel(); });
    $("#btn_ExportToPdf").click(function () { ExportToPdf(); });
    //EditChannel(1);
        
});
$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});
$("#btnTextSearch").click(function () {

    getGrid();
});
var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();


    oTableChannel = $('#tbl_employee').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/LeaveType/GetLeaveReasonList",
        "aoColumns": [

            { "mData": "ID", "bVisible": false, "sTitle": "Code" },
            { "mData": "LeaveReason_1", "sTitle": "Termination Reasons (En)", "width": "30%" },
            { "mData": "LeaveReason_2", "sTitle": "Termination Reasons (Ar)", "width": "28%" },
            { "mData": "LeaveReason_3", "sTitle": "Termination Reasons (Fr)", "width": "28%" },
            { "mData": "Action", "sTitle": "Actions", "bSortable": false, "width": "14%" },

        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/LeaveType/GetLeaveReasonList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[1, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_employee");
        },
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    var buttonName = id > -1 ? "Update": "Submit";

    $("#modal_Loader").load("/LeaveType/AddEditLeaveReason/" + id, function () {
        $("#myModal").modal("show");

        if (id > -1)
        { $("#modal_heading").text('Edit Leave Reason'); }
        else { $("#modal_heading").text('Add Leave Reason'); }
        //$("#form0").find('input[type="submit"]').val(buttonName);
    });


}



function Sucess() {
    $("#myModal").modal("hide");
    ShowMessage("success", "Leave reason save successfully");
    getGrid();
}




function TerminateEmployee(id) {
    $("#modal_Loader").load("/Employee/ViewTerminateEmployee/" + id);
    $("#myModal").modal("show");
    $("#modal_heading").text('Terminate Employee');

}

function UpdateStatus(id, status) {
    //alert("In");
    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { "Id": id, "status": status },
        url: '/Employee/UpdateStatus',
        success: function (data) {
            ShowMessage("success", data.resultMessage);
            getGrid();
        },
        error: function (data) { }
    });
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete record?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/LeaveType/DeleteLeaveReason',
            success: function (data) {
                ShowMessage("success", "Record deleted successfully");
                getGrid();
            },
            error: function (data) { }
        });
    });

}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}
function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}


function AddEmployeeDependent() {

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/LeaveType/AddEditLeaveReason/0");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Leave Reason');
}

function AddEmployeeLeaveRequest() {
    var EmployeeId = $("#hdnEmployeeId").val();

    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Employee/AddEmployeeLeaveRequest/" + EmployeeId);
    $("#myModal").modal("show");
    $("#modal_heading").text('Add Employee Leave Request');
}

function ViewProfileChannel(id) {
    window.location.href = "/ViewEmployeeProfile/Index/" + id;
}



// View Profile 

function AddPastEmployment() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/pastemployment/index/" + EmployeeId;
}

function AddInternalEmployment() {
    var EmployeeId = $("#hdnEmployeeId").val();
    window.location.href = "/employee/InternalEmploymentList/" + EmployeeId;
}


function ChangePassword() {
    var EmployeeId = $("#hdnEmployeeId").val();
    $("#modal_Loader").load("/Employee/ChangeEmployeePassword/" + EmployeeId);
    $("#myModal").modal("show");
    $("#modal_heading").text('Change Password');
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportEmployeeRecordToPDF(id) {

    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Employee/ExportEmployeeToPDF/" + id);
    }
    else ShowMessage("warning", "No data for export!");

}
//--------------------