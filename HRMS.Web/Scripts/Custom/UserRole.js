﻿var selectedNode;
$(document).ready(function () {
    loadMenuTree();
})

function getTreeAsPerParent() {
    $("#SearchId").val("");
    var role = $('#Div_UserRoles a.active');
    var selectedRole = role.data('id');
    if (selectedRole !== undefined) {
        loadMenuTree($("#ddl_ParentNav").val());
        $('#panel-permissioninfo').html("");
    }
    else {
        $("#chkOnlyPermitted").prop('checked', false);
        globalFunctions.ShowMessage("warning", "Please select user role.");
    }
}

function loadMenuTree(ParentNavId) {
    StartPanelLoader("#divShowMenuTree");
    var showOnlyAssigned = $("#chkOnlyPermitted").is(":checked");
    var role = $('#Div_UserRoles a.active');
    var url = '';
    var selectedRole = role.data('id');
    if (selectedRole == undefined) {
        url = '/UserRole/GetModuleTree?ParentNavId=' + ParentNavId + '&ShowOnlyPermitted=' + showOnlyAssigned
    }
    else {
        url = '/UserRole/GetModuleTree?ParentNavId=' + ParentNavId + '&ShowOnlyPermitted=' + showOnlyAssigned + '&selectedRole=' + selectedRole.RoleId
    }
    $('#divShowMenuTree').empty();

    $('#divShowMenuTree').tree({
        url: url,
        loadFilter: function (rows) {
            return convert(rows);
        },
        onLoadSuccess: function () {
            ExpanTree();
            StopPanelLoader("#divShowMenuTree");
        },
        onClick: function (node) {
            showModuleRolePermission(node);
            selectedNode = node;
        }
    });

}

function ExpanTree() {
    $('#divShowMenuTree').tree('expandAll');
}

function showModuleRolePermission(node) {
    var moduleId = node["id"];
    var moduleName = node["text"];
    var role = $('#Div_UserRoles a.active');
    var selectedRole = role.data('id');;
    var parentId = "";

    if (node['children'] != undefined) {
        parentId = node["id"];
    }
    if (selectedRole != "" && selectedRole != undefined) {
        $('#panel-permissioninfo').html("");
        $("#SearchId").val("");
        var showOnlyAssigned = $("#chkOnlyPermitted").is(":checked");
        loadPermissionList(moduleId, moduleName, parseInt(selectedRole.RoleId), showOnlyAssigned, parentId);
    }
    else {
        globalFunctions.ShowMessage("warning", "Please select user role.");
    }
}

function loadPermissionList(moduleId, moduleName, roleId, showOnlyAssigned, parentId) {
    StartPanelLoader("#panel-permissioninfo");
    $('#panel-permissioninfo').load("/UserRole/GetModulePermissions?moduleID=" + moduleId + "&moduleName=" + encodeURI(moduleName) + "&roleId=" + roleId + "&showOnlyAssigned=" + showOnlyAssigned + "&parentId=" + parentId, function (response, status, xhr) {
        StopPanelLoader("#panel-permissioninfo");      
    });
}

$(document).on('click', '#Div_UserRoles a ', function () {
    $('#Div_UserRoles a').removeClass('active');
    $(this).addClass('active');
    var showOnlyAssigned = $("#chkOnlyPermitted").is(":checked");
    if (selectedNode != undefined) {
        if (showOnlyAssigned == false) {
            showModuleRolePermission(selectedNode);
        }
        else {
            loadMenuTree();
            $('#panel-permissioninfo').html("");
        }
    }
    else {
        loadMenuTree();
        $('#panel-permissioninfo').html("");
    }
});

$(document).on('click', "#btn_addUserRole", function () {
    $("#modal_Loader").load("/UserRole/AddUserRole/");
    $("#myModal").modal("show");
    $("#modal_heading").text('Add User Role');
});

//function for updateing select all permission
function updateAllPermission(source) {
    if (source.is(":checked")) {
        $(".chkPermission").attr("checked", "checked");
    }
    updatePermission(source);
}
///function for updating menu permission
function updatePermission(source) {
    var showOnlyAssigned = $("#chkOnlyPermitted").is(":checked");
    var mappingDetails = [];
    var selectedRole = $('#Div_UserRoles  a.active').data('id');
    var OperationType = source.data('action');
    //var moduleId = $("#hdnModuleId").val();
    var moduleId = source.data('moduleid');
    var ActNavID = source.data('actnavid');

    var isSelectAll = $("#selectall_" + moduleId).is(":checked");

    if (source.data('action') == "selectAll") {
        mappingDetails.push({
            PermissionId: source.data('permissionid'),
            GrantPermission: source.is(':checked'),
            PermissionType: source.data('action')
        });
        $("#tbl_permissionDataList > tbody > tr > td > input:checkbox:visible").each(function () {
            if ($(this).data('moduleid') == moduleId) {
                mappingDetails.push({
                    PermissionId: $(this).data('permissionid'),
                    GrantPermission: source.is(':checked'),
                    PermissionType: $(this).data('action')
                });
            }
        });
    }
    else {
        $("#tbl_permissionDataList > tbody > tr > td > input:checkbox:visible").each(function () {
            if ($(this).data('moduleid') == moduleId) {
                mappingDetails.push({
                    PermissionId: $(this).data('permissionid'),
                    GrantPermission: $(this).is(':checked'),
                    PermissionType: $(this).data('action')
                });
            }
        });
    }
    if (selectedRole.RoleId != "" && selectedRole.RoleId != undefined) {        
        var saveparams = {
            mappingDetails: mappingDetails, operationType: OperationType, userRoleId: selectedRole.RoleId, ActNavID: ActNavID, moduleId: moduleId, IsFoolControl: isSelectAll
        };
        $.ajax({
            type: 'POST',
            url: '/UserRole/UpdatePermissionTypes',
            async: false,
            contentType: 'application/json',
            data: JSON.stringify(saveparams),
            success: function (data) {               
                ShowMessage(data.CssClass, data.Message)
                showModuleRolePermission(selectedNode);
                if (showOnlyAssigned == true) {
                    loadMenuTree($("#ddl_ParentNav").val());
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    }
    else {
        globalFunctions.ShowMessage("error", translatedResources.userRoleErrorMsg);
    }
}

function DeleteUserRole(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/UserRole/DeleteUserRole',
            success: function (data) {
                ShowMessage(data.CssClass, data.Message);
                $("#divUserRoles").html("");
                $("#divUserRoles").load("/UserRole/ShowUserRoles");
                loadMenuTree();
                $('#panel-permissioninfo').html("");

            },
            error: function (data) { }
        });
    });
}