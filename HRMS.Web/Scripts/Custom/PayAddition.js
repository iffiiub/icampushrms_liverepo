﻿

$("#btn_add").click(function () {
    if (parseInt($('#Empid').val()) > 0)
        //getGrid(employeeId);
        EditChannel(0);
    else
        ShowMessage("warning", "Please select Employee first");
});

$("#btn_ExportToExcel").click(function () { ExportToExcel(); });
$("#btn_ExportToPdf").click(function () { ExportToPdf(); });

var oTableChannel;

function getGrid() {

    oTableChannel = $('#tbl_company').dataTable({
        "sAjaxSource": "/PayAddition/GetPayAdditionListEmployee",
        //"dom": 'rt<".divFooter"<".col-md-12" <".col-md-4 pageEntries"i ><".col-md-4 col-md-offset-4 "l>><".col-md-12"<".col-md-8 perPage"p><"#ExportTo">>>',

        "aoColumns": [
           { "mData": "AdditionDate", "sTitle": "Addition Date", "sType": "date-uk" },
           { "mData": "PaySalaryAllowanceName", "sTitle": "Addition Type" },
           { "mData": "Amount", "sTitle": "Amount", "sClass": "right-text-align" },
           { "mData": "Description", "sTitle": "Description" },
           { "mData": "IsActive", "sTitle": "Active", "bSortable": false },
           { "mData": "Actions", "sTitle": "Actions", "sWidth": "20%", "sClass": "center-text-align", "bSortable": false }
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/PayAddition/GetPayAdditionListEmployee",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "aaSorting": [[0, 'desc']],
        "fnDrawCallback": function () {
        }
    });


    var Hostname = window.location.host;
    //$("#ExportTo").empty();
    //$("#ExportTo").html('<b> Export As </b><button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToExcel"> <img  style="width:20px;height:20px;" <img src="http://' + Hostname + '/Content/images/excel.png" id="ImgExportToExcel"/>  Excel</button>  <button class="btn btn-primary" style="padding:2px;    width: 80px;" id="btn_ExportToPdf"> <img src="http://' + Hostname + '/Content/images/pdf-icon-transparent-background.png" id="ImgExportToPDf" style="width:20px;height:20px;" />  Pdf</button>');

}


function getPayAdditionGrid() {

    oTableChannel = $('#tbl_company').dataTable({
        "sAjaxSource": "/PayAddition/GetPayAdditionListEmployee",
        "dom": 'rt<".divFooter"<".col-md-12" <".col-md-4 pageEntries"i ><".col-md-4 col-md-offset-4 "l>><".col-md-12"<".col-md-8 perPage"p><"#ExportTo">>>',

        "aoColumns": [
            { "mData": "Actions", "sTitle": "Actions" },
            { "mData": "PayAdditionID", "bVisible": false, "sTitle": "PayAdditionID" },
            { "mData": "PaySalaryAllowanceID", "bVisible": false, "bSortable": true, "sTitle": "PaySalaryAllowanceID" },
            { "mData": "PaySalaryAllowanceName", "bSortable": true, "sTitle": "Addition Type" },

           { "mData": "EmployeeID", "bVisible": false, "bSortable": true, "sTitle": "EmployeeID" },
           { "mData": "AdditionDate", "sTitle": "Addition Date" },
           { "mData": "Amount", "sTitle": "Amount" },
           { "mData": "Description", "sTitle": "Description" },

           { "mData": "IsActive", "sTitle": "Active" },
           { "mData": "TransactionDate", "bVisible": false, "sTitle": "TransactionDate" },
           { "mData": "AcademicYearID", "bVisible": false, "sTitle": "AcademicYearID" }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/PayAddition/GetPayAdditionListEmployee",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": false,
        "bInfo": true,
        "bSortCellsTop": true
    });
}

function EditChannel(id) {
    LoadEmployeeDetails($("#Empid").val());
    var type = id > 0 ? "Edit" : "Add";
    var buttonName = id > 0 ? "Update" : "Submit";

    $("#modal_Loader").load("/PayAddition/Edit/?id=" + id + "&EmployeeId=" + $("#Empid").val(), function () {
        $("#modal_heading").text(type + ' Pay Addition');
        $('#myModal').modal({ backdrop: 'static' })
        $("#myModal").modal("show");
        bindSelectpicker('.selectpickerddl');
        //$("#form0").find('input[type="submit"]').val(buttonName);
    });
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete Pay Addition?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { "id": id },
            url: '/PayAddition/Delete',
            success: function (data) {
                if (data.Success){
                    if (data.CssClass == "Request") {
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                }                   
                else
                    ShowMessage("error", data.Message);

                getGrid($("#Empid").val());
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/PayAddition/ExportToExcel?EmployeeId=" + $("#Empid").val());
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/PayAddition/ExportToPdf?EmployeeId=" + $("#Empid").val());
    }
    else ShowMessage("warning", "No data for export!");
}

function getPayAddition() {
    if (parseInt($("#Empid").val()) > 0)
        getGrid();
}


$("#SearchEmployee").blur(function () {

    var val = $('#SearchEmployee').val();
    if (val != "") {
        $.ajax({

            // The url to which the request is to be sent.
            url: "/Deduction/EmployeeSearch",

            // The type of request. It can be "GET" or "POST" or others.
            type: "GET",

            // The data that is to be sent to the server. It can be omitted if no data is
            // to be sent. In a "GET" request this data is appended to the url.
            data: { "search": val },

            // The function to be called when the server returns some data. The data
            // variable holds the data that was returned.
            success: function (data) {
                //if(data != null)
                //{
                var text = '';
                //$('#ULEmployeeNotInDepartment').html("");
                $(".list-group-item").remove();
                $.each(data, function (index, itemData) {

                    var Name = itemData.FirstName_1 + ' ' + itemData.SurName_1;
                    text += "<a href='javascript:void(0)' id=" + itemData.EmployeeID + " onclick='getPayDuduction(" + itemData.EmployeeID + ")' class='list-group-item'>" + Name + "</a>";
                    //text += "<li class='liemployeenotindepartment'>";
                    //text += "<input type='checkbox' class='chkEmployee'  value='" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "' id='" + itemData.EmployeeID + "' />";
                    //text += "<label for='" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "'>" + itemData.FirstName_1 + ' ' + itemData.SurName_1 + "</label></li>";

                });

                $('#AllEmployees').append(text);
                //}


            },

            // Show an alert if an error occurs.
            error: function () {
                $.MessageBox({
                    buttonDone: "Ok",
                    message: "Some error occured.",
                });
            }
        });
    }

});


///Add Multiple Addition

var i = 0;
var selectList = "";
var PayAdditionData = [];
var orignalList = "";

$(document).ready(function () {

    BindEmployee();
    $(document).on("click", ".close", function () {
        PayAdditionData = [];
    });

    // reloadTableNew();
});

$("#btn_addMultipleAdditions").click(function () {
    if ($('#Empid').val() == '')
        //getGrid(employeeId);
        ShowMessage("warning", "Please select Employee first");
    else
        AddMultipleAddition(0);
});

function AddMultipleAddition(id) {
    i = 0;
    BindEmployee();
    var PayAdditionData = [];
    var type = "Add";
    var buttonName = "Submit";

    $("#modal_Loader").load("/PayAddition/AddMultipleAddition/?id=" + id + "&EmployeeId=" + $("#Empid").val(), function () {
        $("#modal_heading").text(type + ' Pay Addition');
        $('#myModal').modal({ backdrop: 'static' })
        $("#myModal").modal("show");
        $("#form0").find('input[type="submit"]').val(buttonName);
    });
}

function ShowAdditionType() {
    $("#addPaySalaryModal_Loader").html("");
    $("#addPaySalaryModal_Loader").load("/PaySalaryAllowance/Edit/0", function () {
        $("#addPaySalaryModal").modal("show");
        $("#addPaySalaryModal_heading").text('Add Pay Addition');
    });
}

function saveRow(e) {
    i = i + 1;
    var id = $("#ddlEmployee").val();
    var tempEmployeeId1 = id;
    var EmployeeName = $("#ddlEmployee option:selected").text();
    var percent = $("#txtAmount").val();
    var isEmpSelected = false;
    var isAmountEntred = false;
    var tempEmployeeId = $("#ddlEmployee").attr("data-EmployeeId");
    if (parseInt(tempEmployeeId) > 0) {
        id = parseInt(tempEmployeeId)
    }
    if (id != "0" && id != "") {
        percent = +percent;
        isEmpSelected = true;
        if (percent > 0) {
            isAmountEntred = true;
            var isAvailable = false;
            PayAdditionData.forEach(function (p) {
                if (p.EmployeeID == id) {
                    isAvailable = true;
                    //selectList = selectList.replace('<option value=' + id + '>' + EmployeeName + '</option>', '');
                    p.Amount = percent;
                    p.EmployeeID = tempEmployeeId1;
                    p.EmployeeName = EmployeeName;
                }
            });

            if (!isAvailable) {
                //selectList = selectList.replace('<option value=' + id + '>' + EmployeeName + '</option>', '');
                PayAdditionData.push({
                    "EmployeeID": id,
                    "EmployeeName": EmployeeName,
                    "Amount": percent
                });
            }
            filterSelectList();
            reloadTableNew();
        } //else { ShowMessage('error', 'Please enter amount '); }
    } //else { ShowMessage('error', 'Please select employee'); }
    if (!isEmpSelected) {
        ShowMessage('error', 'Please select employee');
    }
    if (!isAmountEntred) {
        ShowMessage('error', 'Please enter amount ');
    }
}

function reloadTableNew() {
    var k = 0;

    $("#paySalary-table > tbody").empty();
    var j = Object.keys(PayAdditionData).length;

    PayAdditionData.forEach(function (p) {
        k = k + 1;
        if (j == 1) {

            $('#paySalary-table > tbody').append(
           '<tr id="rowId_' + p.EmployeeID + '" class="edit' + p.EmployeeID + '">' +
           '<td class="col-md-1 hidden">' + p.EmployeeID + '</td>' +
          '<td class="col-md-6">' + p.EmployeeName + '</td>' +
          '<td class="col-md-3">' + p.Amount + '</td>' +
          '<td class="col-md-3 center-text-align padding-0">' +
          '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditAllowance(' + p.EmployeeID + ',this)" id="btnEdit" data-id=' + p.EmployeeID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +

          '<a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>' +
          '</td>' +
          '</tr>')

        }

        else if (k < j) {
            $('#paySalary-table > tbody').append(
       '<tr id="rowId_' + p.EmployeeID + '" class="edit' + p.EmployeeID + '">' +
               '<td class="col-md-1 hidden">' + p.EmployeeID + '</td>' +
      '<td  class="col-md-6">' + p.EmployeeName + '</td>' +
      ' <td class="col-md-3">' + p.Amount + '</td>' +
      '<td class="col-md-3 center-text-align padding-0">' +
      '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditAllowance(' + p.EmployeeID + ',this)" id="btnEdit" data-id=' + p.EmployeeID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
      '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(' + p.EmployeeID + ')" title="Delete"><i class="fa fa-times"></i> </a>' +
      '</td>' +
     '</tr>')

        }
        else if (k == j) {

            $('#paySalary-table > tbody').append(
          '<tr id="rowId_' + p.EmployeeID + '" class="edit' + p.EmployeeID + '">' +
           '<td class="col-md-1 hidden">' + p.EmployeeID + '</td>' +
          '<td class="col-md-6">' + p.EmployeeName + '</td>' +
          '<td class="col-md-3">' + p.Amount + '</td>' +
          '<td class="col-md-3 center-text-align padding-0">' +
          '<a class="btn btn-success  btn-rounded btn-condensed btn-sm btn-space" onclick="EditAllowance(' + p.EmployeeID + ',this)" id="btnEdit" data-id=' + p.EmployeeID + ' title="Edit"><i class="fa fa-pencil"></i> </a>' +
          '<a class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="DeleteRow(' + p.EmployeeID + ')" title="Delete"><i class="fa fa-times"></i> </a>' +
          '<a class="btn btn-info  btn-rounded btn-condensed btn-sm btn-space" id="btnAddRow' + i + '" onclick="AddDataRow()" title="Add"><i class="fa fa-plus"></i></a>' +
          '</td>' +
          '</tr>')
        }
    });

}

function AddDataRow() {

    $("#btnAddRow" + i + "").hide();
    $("#addPaySalaryInfo").attr("disabled", true)
    $('#paySalary-table > tbody').append(
      '<tr>' +
        '<td class="col-md-1 hidden"></td>' +
        '<td class="col-md-6">' +
             '<div class=""><select data-EmployeeId="" class="ddlEmployee form-control selectpickerddl" data-live-search="true" id="ddlEmployee">' + selectList + '</select></div>' +
        '</td>' +
        '<td class="col-md-3">' +
            '<div class=""><input class="form-control positiveOnly"  id="txtAmount" /></div>' +
        '</td>' +
        '<td class="center-text-align col-md-3 padding-0">' +
            '<div class="center-text-align"><button type="button" id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img src="/Content/images/tick.ico" style="width:16px;"/></button><button class="tableButton" id="btnRowCancel" onClick="CancelNewRow(this)" class="btnRemove"><img src="/Content/images/cross.ico" style="width:16px;"/></button></div>' +
        '</td>' +
       '</tr>');
    $('.positiveOnly').unbind('keypress');
    bindPositiveOnly('.positiveOnly');
    bindSelectpicker(".selectpickerddl")
}



function BindEmployee() {
    selectList = "<option value=0>Select Employee</option>";
    if (orignalList == "") {
        $.ajax({
            type: 'get',
            url: '/PayAddition/loadEmployeeList',
            datatype: 'Json',
            success: function (data) {
                $.each(data, function (key, value) {
                    //  selectList.append($('<option></option>').val(value.paySalaryAllowancesID).html(value.paySalaryAllowancesName));
                    selectList += '<option value=' + value.empId + '>' + value.EmpName + '</option>';
                });
                orignalList = selectList;
            }
        });

    } else {
        selectList = orignalList;
    }
}

function DeleteRow(id) {
    PayAdditionData.forEach(function (result, index) {
        if (result['EmployeeID'] == id) {
            //selectList += '<option value=' + id + '>' + result.EmployeeName + '</option>';
            PayAdditionData.splice(index, 1);
        }
    });
    filterSelectList();
    reloadTableNew();
}

function CancelNewRow(e) {
    $(e).closest('tr').remove();
    reloadTableNew();
}


function SaveMultipleAddition() {

    var allowanceId = $("#PaySalaryAllowanceID").val();
    var AddDate = $("#AdditionDate").val();
    var Cycles = $("#AdditionCycles").val();
    var desc = $("#Description").val();

    var ValidateCnt = 0;
    if (allowanceId == "") {
        ValidateCnt++;
        var field = $("#PaySalaryAllowanceID").parent().parent().find(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='PaySalaryAllowanceID'>This field is mandatory</span>");

    }

    if (AddDate == "") {
        ValidateCnt++;
        var field = $("#AdditionDate").next(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='AdditionDate'>Please enter valid data</span>");
    }
    if (Cycles == 0) {
        ValidateCnt++;
        var field = $("#AdditionCycles").next(".field-validation-valid");//span
        field.attr("class", "field-validation-error");
        field.html("<span for='AdditionCycles'>Please enter valid data</span>");
    }
    if (Object.keys(PayAdditionData).length == 0) {
        ValidateCnt++;
        ShowMessage("error", "Please enter addition data");
    }


    if (ValidateCnt == 0) {

        $.ajax({
            type: "POST",
            data: {
                cycles: Cycles,
                AllowanceId: allowanceId,
                AdditionDate: AddDate,
                description: desc,
                PayAdditions: JSON.stringify(PayAdditionData)
            },
            url: '/PayAddition/SaveMultipleAddition',
            success: function (data) {
                $("#myModal").modal('hide');
                if (data.Success) {
                    if (data.CssClass == "Request") {
                        customShowMessage("information", data.Message, 40000, "center");
                    } else {
                        ShowMessage("success", data.Message);
                    }
                } else {
                    ShowMessage("error", data.Message);
                }
                getGrid($("#Empid").val());
                PayAdditionData = [];
            },
            error: function (xhr) {
            }
        });
    }
}

function EditAllowance(payallowanceId, e) {
    filterSelectList();
    var id = payallowanceId; //$(this).attr('data-id');
    var rowPos = $('tr.edit' + id)[0];
    var rowPos = $('#paySalary-table tr#rowId_' + id);
    var $row = $(e).parent().parent();
    var SalAllowanceType = $row.find('td:eq(1)').text();
    var AllowanceAmount = $row.find('td:eq(2)').text();
    var tempEmployeeId = $row.find('td:eq(0)').text().trim();

    selectList += '<option value=' + id + '>' + SalAllowanceType + '</option>';

    $(rowPos).find('td:eq(1)').html('<div class=""><select data-employeeName="" data-EmployeeId="' + tempEmployeeId + '" class="ddlEmployee form-control selectpickerddl" data-live-search="true" id="ddlEmployee">' + selectList + '</select></div>');
    $(rowPos).find('td:eq(2)').html('<div class=""><input class="form-control positiveOnly"  id="txtAmount" /></div>');
    $(rowPos).find('td:eq(3)').html('<div class="center-text-align"><button id="btnRowSave" class="tableButton" onclick="saveRow(this)" class="btnAdd"><img style="width:16px;" src="/Content/images/tick.ico" /></button></div>');
    //<button class="tableButton" id="btnRowCancel" onClick="CancelNewRow()" class="btnRemove"><img style="width:16px;" src="/Content/images/cross.ico" /></button>
    $('.positiveOnly').unbind('keypress');
    bindPositiveOnly('.positiveOnly');
    $("#ddlEmployee option:contains(" + SalAllowanceType + ")").attr('selected', 'selected');
    $("#txtAmount").val(AllowanceAmount);
    bindSelectpicker(".selectpickerddl");
}

function filterSelectList() {
    BindEmployee();
    PayAdditionData.forEach(function (p) {
        selectList = selectList.replace('<option value=' + p.EmployeeID + '>' + p.EmployeeName + '</option>', '');
    });
}