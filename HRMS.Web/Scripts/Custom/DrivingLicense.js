﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridDrivingLicense();
    }
});

var oTableChannel;

function getGridDrivingLicense() {

    oTableChannel = $('#tbl_DrivingLicenseList').dataTable({
        
        "sAjaxSource": "/DrivingLicense/GetDrivingLicenseList",
        "aoColumns": [             
            { "mData": "DocumentNo", "sTitle": "Document<br/>Name/No", 'width': '18%' },
            { "mData": "IssueCountry", "sTitle": "Issue Country"  , 'width': '18%'},
            { "mData": "IssuePlace", "sTitle": "Issue Place"  , 'width': '13%'},
            { "mData": "IssueDate", "sTitle": "Issue Date", "sType": "date-uk", 'width': '13%' },
            { "mData": "ExpiryDate", "sTitle": "Expiry Date", "sType": "date-uk" , 'width': '13%'},
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%', 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction" , 'width': '15%'}
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/DrivingLicense/GetDrivingLicenseList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_DrivingLicenseList");
        }
    });
}

function AddDrivingLicense() {
     
    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/DrivingLicense/AddDrivingLicense");
                $("#myModal3").modal({backdrop: 'static'});
                $("#modal_heading3").text('Add Driving License');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditDrivingLicense(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/DrivingLicense/EditDrivingLicense/" + id);
    $("#myModal3").modal({backdrop: 'static'});
    $("#modal_heading3").text('Edit Driving License');

}


function DeleteDrivingLicense(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/DrivingLicense/DeleteDrivingLicense',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridDrivingLicense();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewDrivingLicense(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/DrivingLicense/ViewDrivingLicense/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('View Driving License Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/DrivingLicense/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/DrivingLicense/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}