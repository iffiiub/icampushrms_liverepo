﻿$(document).ready(function () {
    if ($("#hidSelectedEMP").val() != "") {
        getGridPassport();
    }
});

var oTableChannel;

function getGridPassport() {

    oTableChannel = $('#tbl_PassportList').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
        "sAjaxSource": "/Passport/GetPassportList",
        "aoColumns": [           
            { "mData": "DocumentNo", "sTitle": "Document<br>Name/No", 'width': '18%' },
            { "mData": "IssueCountry", "sTitle": "Issue<br>Country", 'width': '18%' },
            { "mData": "IssuePlace", "sTitle": "Issue<br>Place", 'width': '13%', "sType": "date-uk" },
            { "mData": "IssueDate", "sTitle": "Issue<br>Date", 'width': '13%', "sType": "date-uk" },  
            { "mData": "ExpiryDate", "sTitle": "Expiry<br>Date", 'width': '13%' },
            { "mData": "IsPrimary", "sTitle": "Primary", 'width': '10%' },
            { "mData": "Action", "sTitle": "Action", "sClass": "ClsAction vertical-Middle" , 'width': '15%'}
        ],
        "processing": false,
        "serverSide": false,
        "ajax": "/Passport/GetPassportList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[0, "asc"]],
        "bSortCellsTop": true,
        "fnDrawCallback": function () {
            removeSpan("#tbl_PassportList");
        }
    });
}

function AddPassport() {

    $.ajax({
        type: 'POST',
        data: {},
        url: '/Document/CheckEmployee',
        success: function (data) {
            if (data.result != "error") {
                $("#modal_Loader3").html("");
                $("#modal_Loader3").load("/Passport/AddPassport");
                $("#myModal3").modal({backdrop: 'static'});
                $("#modal_heading3").text('Add Passport');
            }
            else {
                ShowMessage("error", "Please select an employee");
            }
        },
        error: function (data) { }
    });

}

function EditPassport(id) {
    $("#modal_Loader3").html("");
    $("#modal_Loader3").load("/Passport/EditPassport/" + id);
    $("#myModal3").modal({backdrop: 'static'});
    $("#modal_heading3").text('Edit Passport');
}


function DeletePassport(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/Passport/DeletePassport',
            success: function (data) {
                ShowMessage("success", data.Message);
                getGridPassport();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewPassport(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Passport/ViewPassport/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('View Passport Details');
}

function AddJobDescription(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Passport/ViewJobDescription/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('Add Job Description');
}

function EditJobDescription(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/Passport/EditJobDescription/" + id);
    $("#myModal").modal({backdrop: 'static'});
    $("#modal_heading").text('Edit Job Description');
}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Passport/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Passport/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}