﻿var isWhere = 1;
var isTextField = 1;

$(document).ready(function () {

    $("#chkPortrait").attr("checked", "checked");

    if ($("#hdnOldWhere").val().trim() == "") {
        $("#divOldWhereCondition").hide();
    }
    else {
        $("#chkOldWhereCondition").attr('checked', 'checked');
        $("#divOldWhereCondition").show();
    }
    CheckOldWhere();

    //Show textbox and hide dropdown for where values   
    $("#txtWhereConditionId").show();
    $("#ddlWhereConditionId").hide();
    isTextField = 1;

    //On change of all fields, if sqltable is empty show textbox else show dropdown
    $("#ddlAllFieldsList").change(function () {          
        var selectedItem = $(this).val();
        $.ajax({
            cache: false,
            type: "GET",
            url: "/ReportBuilder/GetWhereConditionValues",
            data: { "id": selectedItem },
            success: function (data) {                
                if (data != "") {
                    var ddlValues = $("#ddlWhereConditionValues");
                    ddlValues.empty();

                    $.each(data, function (id, option) {
                        ddlValues.append($('<option></option>').val(option.Value).html(option.Text));
                    });
                    ddlValues.show();
                    //bindSelectpicker("#ddlWhereConditionValues");
                    RefreshSelect("#ddlWhereConditionValues");
                    $("#txtWhereConditionId").hide();
                    $("#ddlWhereConditionId").show();
                    isTextField = 0;
                }
                else {
                    $("#txtWhereConditionId").show();
                    $("#ddlWhereConditionId").hide();
                    $("#ddlWhereConditionValues.btn-group.bootstrap-select.form-control").hide();
                    isTextField = 1;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    });

    $("#btnAdd").click(function () {
        $.ajax({
            cache: false,
            type: "GET",
            url: "/ReportBuilder/GetWhere",
            data: {
                "id": $("#ddlAllFieldsList").val(), "isWhere": isWhere, "strAndOr": $("#ddlWhereConditionOperators").val(),
                "isTextField": isTextField, "whereOperator": $("#ddlLogicalOperators").val(), "ddlValue": $("#ddlWhereConditionValues").val(),
                "ddlText": $("#ddlWhereConditionValues").val(), "txtValue": $("#txtWhereConditionValue").val()
            },
            success: function (data) {
                $("#divWhereConditions").html(data);
                $("#lblWhere").hide();
                $("#ddlWhereConditionOperators").show();
                $('.ddlWhereConditionOperators-class').show();
                isWhere = 0;
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    });

    $("#btnSaveReport").click(function () {
        var hdnNeSqlWhereCondition = '';
        var hdnNewWhereDescription = '';
        var selectedFields = "";
        $("#divSelectedFields li.dd-item").each(function (index) {
            selectedFields += $(this).attr("fieldId") + ",";
        });
        if (selectedFields != "")
            selectedFields = selectedFields.substring(0, selectedFields.length - 1);
        if ($("#hdnNeSqlWhereCondition").length > 0) {
            hdnNeSqlWhereCondition = $("#hdnNeSqlWhereCondition").val();
        }
        if ($("#hdnNewWhereDescription").length > 0) {
            hdnNewWhereDescription = $("#hdnNewWhereDescription").val();
        }
        $.ajax({
            cache: false,
            type: "POST",
            url: "/ReportBuilder/SaveReport",
            data: {
                "id": $("#hdnReportID").val(),
                "isForMe": $("#chkForMeOnly").is(":checked"),
                "reportTitle": $("#txtReportTitle").val(),
                "selectedFields": selectedFields,
                "isOrderBy": $("#chkOrderBy").is(":checked"),
                "orderBy": $("#ddlOrderByList").val(),
                "isGroupBy": $("#chkGroupBy").is(":checked"),
                "groupBy": $("#ddlGroupByList").val(),
                "hdnOldSqlWhereCondition": encodeURIComponent($("#hdnOldSqlWhereCondition").val()),
                "hdnOldWhereDescription": encodeURIComponent($("#hdnOldWhereDescription").val()),
                "hdnNeSqlWhereCondition": encodeURIComponent(hdnNeSqlWhereCondition),
                "hdnNewWhereDescription": encodeURIComponent(hdnNewWhereDescription),
            },
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                window.location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    });

    $("#btnPreview").click(function () {
        var orderBy = 0;
        var groupBy = 0;
        var SqlWhereCondition = '';
        var ReportId = $("#hdnReportID").val();
        if ($("#txtReportTitle").val() != "") {
            var orientation = $("#chkPortrait").val();

            if ($("#chkOrderBy").is(":checked"))
                orderBy = $("#ddlOrderByList").val();

            if ($("#chkGroupBy").is(":checked"))
                groupBy = $("#ddlGroupByList").val();

            if ($("#chkLandscape").is(":checked"))
                orientation = $("#chkLandscape").val();
            if ($("#hdnNeSqlWhereCondition").length > 0) {
                SqlWhereCondition = encodeURIComponent($("#hdnNeSqlWhereCondition").val());
            }

            var selectedFields = "";
            $("#divSelectedFields li.dd-item").each(function (index) {
                selectedFields += $(this).attr("fieldId") + ",";
            });
            if (selectedFields != "")
                selectedFields = selectedFields.substring(0, selectedFields.length - 1);

            window.open("/ReportBuilder/ReportBuilderReportingViewer?orderBy=" + orderBy + "&groupBy=" + groupBy
                + "&orientation=" + orientation + "&selectedFields=" + selectedFields + "&reportName=" + $("#txtReportTitle").val() + "&ReportId=" + ReportId + "&SqlWhereCondition=" + SqlWhereCondition, "_blank");
        }
        else {
            ShowMessage('error', 'Please enter report title')
        }
    });

    $("#chkOldWhereCondition").click(function () {
        CheckOldWhere();
    });
});

$(document).on("dblclick", ".reportFieldsListContent", function () {    
    SelectFieldItems(this);
});

$(document).on("click", ".reportFieldsListContent", function () {    
    SelectFieldItems(this);
});

function CheckOldWhere() {
    if ($("#chkOldWhereCondition").is(":checked")) {

        $("#ddlWhereConditionOperators").attr('disabled', 'disabled');
        $("#ddlAllFieldsList").attr('disabled', 'disabled');
        $("#ddlLogicalOperators").attr('disabled', 'disabled');
        $("#ddlWhereConditionValues").attr('disabled', 'disabled');
        $("#txtWhereConditionValue").attr('disabled', 'disabled');
        $("#btnAdd").attr('disabled', 'disabled');

        RemoveAllWhereConditions();
        $("#lblWhere").show();
        $("#ddlWhereConditionOperators").hide();
        $('.ddlWhereConditionOperators-class').hide();
        isWhere = 1;
    }
    else {
        $("#ddlWhereConditionOperators").removeAttr('disabled');
        $("#ddlAllFieldsList").removeAttr('disabled');
        $("#ddlLogicalOperators").removeAttr('disabled');
        $("#ddlWhereConditionValues").removeAttr('disabled');
        $("#txtWhereConditionValue").removeAttr('disabled');
        $("#btnAdd").removeAttr('disabled');

        if ($("#divWhereConditions").html().trim() == "") {
            $("#lblWhere").show();
            $("#ddlWhereConditionOperators").hide();
            $('.ddlWhereConditionOperators-class').hide();
            isWhere = 1;
        }
        else {
            $("#lblWhere").hide();
            $("#ddlWhereConditionOperators").show();
            $('.ddlWhereConditionOperators-class').show();
            isWhere = 0;
        }
    }
}

function SelectFieldItems(lnk) {
    var id = $(lnk).attr('fieldId');
    var desc = $(lnk).find('span.title').html();

    var selectedFields = "";
    $("#divSelectedFields li.dd-item").each(function (index) {
        selectedFields += $(this).attr("fieldId") + ",";
    });
    if (selectedFields != "")
        selectedFields = selectedFields.substring(0, selectedFields.length - 1);

    $.ajax({
        type: 'POST',
        data: { fieldId: id, description: desc, selectedFields: selectedFields },
        url: '/ReportBuilder/GetSelectedFieldsList',
        success: function (data) {
            $("#divSelectedFields").html(data);
        },
        error: function (data) { }
    });
}

function GetSavedReports() {
    $.ajax({
        type: 'GET',
        url: '/ReportBuilder/GetSavedReports',
        success: function (data) {
            $("#divSavedReports").html(data);
        },
        error: function (data) { }
    });
}

function DeleteSavedReport(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/ReportBuilder/DeleteSavedReport',
            success: function (data) {
                if (id == $("#hdnReportID").val()) {
                    window.location.href = '/ReportBuilder/Index';
                }
                else
                    $("#divSavedReports").html(data);
            },
            error: function (data) { }
        });
    });
}

function DeleteWhereCondition(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/ReportBuilder/DeleteWhereCondition',
            success: function (data) {
                $("#divWhereConditions").html(data[0]);

                if (parseInt(data[1]) > 0) {
                    $("#lblWhere").hide();
                    $("#ddlWhereConditionOperators").show();
                    $('.ddlWhereConditionOperators-class').show();
                    isWhere = 0;
                }
                else {
                    $("#lblWhere").show();
                    $("#ddlWhereConditionOperators").hide();
                    $('.ddlWhereConditionOperators-class').hide();
                    isWhere = 1;
                }
            },
            error: function (data) { }
        });
    });
}

function RemoveSelected() {
    $.ajax({
        type: 'GET',
        url: '/ReportBuilder/RemoveSelected',
        success: function (data) {            
            $("#divSelectedFields").html("");
            var ddlValues = $("#ddlGroupByList");
            ddlValues.empty();
            RefreshSelect("#ddlGroupByList");
        },
        error: function (data) { }
    });
}

function RemoveField(id) {
    $.ajax({
        type: 'GET',
        url: '/ReportBuilder/RemoveField',
        data: { fieldId: id },
        success: function (data) {            
            $("#divSelectedFields").html(data);
        },
        error: function (data) { }
    });
}

function RemoveAllWhereConditions() {
    $.ajax({
        type: 'POST',
        url: '/ReportBuilder/RemoveAllWhereConditions',
        success: function (data) {
            $("#divWhereConditions").html("");
        },
        error: function (data) { }
    });
}

function EditSavedReport(id) {
    window.location.href = '/ReportBuilder/Index/' + id;
}

function GetGroupByClauseValues() {
    $.ajax({       
        type: "GET",
        url: "/ReportBuilder/GetGroupByClauseValues",
        success: function (data) {            
            if (data != "") {
                var ddlValues = $("#ddlGroupByList");
                ddlValues.empty();

                $.each(data, function (id, option) {
                    ddlValues.append($('<option></option>').val(option.Value).html(option.Text));
                });
                bindSelectpicker("#ddlGroupByList");
                RefreshSelect("#ddlGroupByList");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}