﻿$("#btnTextClear").click(function () {
    $('#txtSearch').val(""); // $('#txtSearch').val() == "";
    getGrid();
});

$("#btnTextSearch").click(function () {
    getGrid();
});

$(document).ready(function () {
    $("#exportIcon").html(getExportIcon("1", "ExportToExcel()", "ExportToPdf()"));     
})

var oTableChannel;

function getGrid() {
    var searchval = $("#txtSearch").val();
    oTableChannel = $('#tbl_company').dataTable({
        //"dom": 'rt<".divFooter"<".col-md-4 pageEntries"i><".col-md-4 pageNo"p><".col-md-4 perPage"l>>',
        //"language": {
        //    "lengthMenu": "Records per page _MENU_ ",
        //},
       //"dom": 'rt<<".col-md-12"<".col-md-4 nopadding" l><".col-md-3 nopadding pageEntries"i><".col-md-5 pageNo"p>>>',
        "sAjaxSource": "/Company/GetCompanyList",
        //"fnServerParams": function (aoData) {
        //    aoData.push({ "name": "search", "value": searchval });
        //},
        "aoColumns": [

            { "mData": "CompanyId", "bVisible": false, "sTitle": "Code" },
            { "mData": "name", "sTitle": "Oganization Name", 'width': '15%' },
            { "mData": "OrganizationCode", "sTitle": "Oganization Code", 'width': '5%' },
           { "mData": "EmailId", "sTitle": "Email", 'width': '12%' },
           { "mData": "ContactPersonName", "sTitle": "Contact Person", 'width': '12%' },
           { "mData": "Address1", "sTitle": "Address 1", "bSortable": false, 'width': '10%' },
           //{ "mData": "Address2", "sTitle": "Address 2", "bSortable": false, 'width': '10%' },
           { "mData": "Country", "sTitle": "Country", 'width': '8%' },
           { "mData": "City", "sTitle": "City", 'width': '8%' },
            { "mData": "Pincode", "sTitle": "P.O. Box", "bSortable": false, 'width': '8%' },
           { "mData": "Phone", "sTitle": "Phone", 'width': '8%' },
           { "mData": "Actions", "sTitle": "Actions", "bSortable": false, 'width': '8%' }

        ],
        "processing": false,
        "serverSide": false,
        "bLengthChange": true,
        "iDisplayLength": 10,
        "ajax": "/Company/GetCompanyList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],        
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "order": [[ 0, "asc" ]],
        "fnDrawCallback": function () {
            //removeSpan("#tbl_company");
            //$("#tbl_company_length").css("width", "100%");           
        },
        "bSortCellsTop": true
    });
}

var checkSchoolOrgnizationId=0;

function EditChannel(id) {
    checkSchoolOrgnizationId= id > 0 ? id : 0;
    var type = id > 0 ? "Edit" : "Add";
    var buttonName = id > 0 ? "Update" : "Save";
    var ModalHeadingLabel = id > 0 ? "Edit Organization" : "Add Organization";
    $("#modal_Loader3").load("/Company/Edit/" + id, function () {
        bindSelectpicker('.selectpickerddl');
        $("#myModal3").modal("show");
        $("#modal_heading3").text(ModalHeadingLabel);
        $("#form0").find('input[type="submit"]').val(buttonName); 
        $("#myModal3 .modal-dialog").attr("style", "width:800px;");       
    });
}

function ViewCategoryChannel(id) {
    checkSchoolOrgnizationId = id > 0 ? id : 0;  
    var ModalHeadingLabel = "View Organization Details";
    $("#modal_Loader3").load("/Company/ViewCategoryDetails/" + id, function () {
        $("#myModal3").modal("show");
        $("#modal_heading3").text(ModalHeadingLabel);
        $("#form0").find('input[type="submit"]').val(buttonName);
        $("#myModal3 .modal-dialog").attr("style", "width:800px;");
    });
}

function DeleteChannel(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: { Id: id },
            url: '/Company/Delete',
            success: function (data) {
                ShowMessage(data.result, data.resultMessage);
                getGrid();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {

    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {

    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Company/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Company/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}

