﻿
var oTableChannel;
var companyIds;
$(document).ready(function () {

    $(document).on("click", "#chkShowAll", function() {
        var id = $("#UserGroupId").val();
        var ischecked = $("#chkShowAll").is(':checked');
        AssignEmployees(id,ischecked);
    });
   

});

function getGridUserGroup() {

    oTableChannel = $('#tbl_UserGroupList').dataTable({
        "sAjaxSource": "/UserGroup/GetUserGroupList",
        "aoColumns": [
            //{ "mData": "UserGroupId", "bVisible": false, "sTitle": "UserGroupId" },
            { "mData": "UserGroupName", "sTitle": "Group Name" },
            { "mData": "Description", "sTitle": "Description" },
            { "mData": "RoleNames", "sTitle": "Roles" },
            { "mData": "ListAccessName", "sTitle": "Employee List Access" },
            { "mData": "CompanyNames", "sTitle": "Company" },
            { "mData": "Action", "sTitle": "Actions", "sWidth": "23%", "bSortable": false }
        ],
        "processing": true,
        "serverSide": false,
        "ajax": "/UserGroup/GetUserGroupList",
        "aLengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]],
        "iDisplayLength": 10,
        "bDestroy": true,
        "bFilter": true,
        "bInfo": true,
        "bSortCellsTop": true,
        "order": [[0, "asc"]],
        "fnDrawCallback": function () {
            removeSpan("#tbl_UserGroupList");
        },
    });
}

function AddUserGroup() {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/UserGroup/AddUserGroup");
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('Add User Group');
}

function EditUserGroup(id) {
    $("#modal_Loader").html("");
    $("#modal_Loader").load("/UserGroup/EditUserGroup/" + id, function () {
        $("#myModal").modal({ backdrop: 'static' });
        $("#modal_heading").text('Edit User Group');
        $("form").find('input[type="submit"]').val('Update');
    });
}

function AssignEmployees(id, isSelectedAll) {
    $.ajax({
        type: 'GET',
        data: { id: id, isSelectedAll: isSelectedAll },
        url: '/UserGroup/AssignEmployees',
        success: function (data) {
            $("#divGrpEmpList").css("display", "block");
            $("#divGrpEmpList").html(data);
        },
        error: function (data) { }
    });
}

function DeleteUserGroup(id) {
    $.MessageBox({ buttonDone: "Yes", buttonFail: "No", message: "Are you sure you want to delete?" }).done(function () {
        $.ajax({
            type: 'POST',
            data: { id: id },
            url: '/UserGroup/DeleteUserGroup',
            success: function (data) {
                getGridUserGroup();
            },
            error: function (data) { }
        });
    });
}

function ViewChannel(id) {
    $('.tblChannel').hide();

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: { Id: id },
        url: '/Channel/GetId',
        success: function (data) {
            $("#channelAddUpdate").load("/Channel/ViewDetails?Id=" + id);
        },
        error: function (data) { }
    });
}

function loadAddUpdateView() {
    $('.tblChannel').hide();
    $("#channelAddUpdate").load("/Channel/Edit");
}

function closeViewChannel() {
    $(".channelView").html("");
    $(".tblChannel").show();

}

function closeEditChannel() {
    $("#channelAddUpdate").html("");
    $(".tblChannel").show();

}

function ViewUserGroup(id) {
    $("#modal_Loader").load("/UserGroup/ViewUserGroup/" + id);
    $("#myModal").modal({ backdrop: 'static' });
    $("#modal_heading").text('View User Group Details');

}

function ExportToExcel() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToExcel");
    }
    else ShowMessage("warning", "No data for export!");
}

function ExportToPdf() {
    if (oTableChannel.fnGetData().length > 0) {
        window.location.assign("/Position/ExportToPdf");
    }
    else ShowMessage("warning", "No data for export!");
}
function GetCompanyIds() {
    var companyIds = getSelectedIds('.multiCompany').join(',');
    // alert(companyIds);
}

function SaveUserRole()
{

    $.ajax({
        url: '/Employee/SaveSelectedLanguages',
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        processData: false,
        data: JSON.stringify(arrayForLanguages),
        async: false,
        success: function (data) { }
    });
}