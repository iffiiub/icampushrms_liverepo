﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HRMS.Web.Startup))]
namespace HRMS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
