/*
 * jQuery Currency Converter Plugin
 * Version : 1.0
 * Plugin URL : https://valllabh.github.io/jquery.currency.converter
 * Author URL : http://vallabhjoshi.com
 * Author : Vallabh Joshi - copyright 2014
 * License : GPL Version 3
 * http://www.opensource.org/licenses/GPL-3.0
 */

(function ($) {
    $.fn.currencyconverter = function (fromcurrency, tocurrency, selector, callback) {

        //alert(fromcurrency);
        //alert(tocurrency);
        pageLoaderFrame();
        const url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=' + fromcurrency + '&to_currency=' + tocurrency + '&apikey=HT8WFHGDXYYT491T'
        $.ajax({
            dataType: "json",
            url: url,
            data: {},
            success: function (data) {

                //alert( data["Realtime Currency Exchange Rate"]["5. Exchange Rate"] );
                var exc = data["Realtime Currency Exchange Rate"]["5. Exchange Rate"];
                $(selector).val(exc);
                //return exc;
                //console.log(data["Realtime Currency Exchange Rate"]["5. Exchange Rate"]);
                hideLoaderFrame();
                if (typeof callback == 'function') { // make sure the callback is a function
                    callback.call(selector, exc); // brings the scope to the callback
                }
            },
            error: function (err) {
                hideLoaderFrame();
                ShowMessage('error', err.statusText);
            }
        });



        //return this;
    };



})(jQuery)