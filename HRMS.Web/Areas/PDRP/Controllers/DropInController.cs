﻿using HRMS.DataAccess;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.Common;

namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class DropInsController : PDRPCommonController
    {
        public string OpenPage(string isAddMode)
        {

            string PageName = "";
            FormsDB FormsDB = new FormsDB();
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

            Session["PDRPDropInRequestID"] = objRequestFormsProcessModel.RequestID;
            Session["PDRPDropInID"] = objRequestFormsProcessModel.FormInstanceID;

            DropInsModelDB DropInsModelDB = new DropInsModelDB();
            DropInsModel DropInsModel = new DropInsModel();
            DropInsModel.DropInEmployeeModel.ID = objRequestFormsProcessModel.FormInstanceID;
            DropInsModel = DropInsModelDB.GetDropInsEmployeeDetails(DropInsModel);
            Session["TeacherID"] = DropInsModel.DropInEmployeeModel.TempTeacherID;


            Session["DropInYear"] = DropInsModel.DropInEmployeeModel.DropinYearID;
            Session["DropInMode"] = "2";


            return "DropInForm";
        }
        public ActionResult Edit()
        {
            //Session["ObservationMode"] = "";

            return RedirectToAction(OpenPage(""));
        }
        public ActionResult ViewDetails()
        {
            //Session["ObservationMode"] = "";
            return RedirectToAction(OpenPage("View"));
        }
        // GET: DropIn
        DropInsModelDB DropInsModelDB = new DropInsModelDB();

        public ActionResult index()
        {
            return RedirectToAction("DropInManage", "DropIns");
        }
        public ActionResult AdminDropIns()
        {
            if (CheckViewLevelAccess())
                return DropInManageInternal("AdminDropIn");
            return RedirectToHome();
        }
        public ActionResult MyDropIns()
        {
            if (CheckViewLevelAccess())
                return DropInManageInternal("MyDropIn");
            return RedirectToHome();
        }
        public ActionResult DropInManage()
        {
            if (CheckViewLevelAccess())
                return DropInManageInternal("");
            return RedirectToHome();
        }
        private ActionResult DropInManageInternal(string DropInYear)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var dBHelper = new HRMS.DataAccess.DBHelper();
            var menuNavigationModels = dBHelper.GetMenuNavigationList();

            if (DropInYear == "MyDropIn")
            {
                Session["MyDropIn"] = "Yes";
                ViewBag.Mode = "MyDropIn";
            }
            else if (DropInYear == "AdminDropIn")
            {
                Session["MyDropIn"] = "Admin";
                ViewBag.Mode = "Admin";
            }
            else
            {
                Session["MyDropIn"] = null;
                ViewBag.Mode = "";
            }
            KPIsListModel model = new KPIsListModel();
            ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYearLanguage().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            //ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true).OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            EmployeeDB objEmployeeDB = new EmployeeDB();

            var empList = new List<Entities.Employee>(); //objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList<HRMS.Entities.Employee>();
            ViewBag.Employee = new SelectList(empList, "EmployeeId", "FullName");
            ViewBag.Company = GetCompanySelectList();
            //return View(model);
            return View("DropInManage", model);
        }
        public ActionResult MyDropIn(string DropInYear)
        {
            return RedirectToAction("DropInManage", "DropIns");
        }
        public ActionResult GetAllTeachingEmployee(string Statustype, int DropInYear, string DropInsYearTxt, int EmployeeId, int CompanyId, string mode)
        {
            List<PDRPTeachersModel> objDropInsTeachersList = new List<PDRPTeachersModel>();

            DropInsModelDB DropInsModelDB = new DropInsModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int employeeId = objUserContextViewModel.UserId;
            int? companyId = null;
            int? supervisorId = objUserContextViewModel.UserId;
            bool allowCreateDropIns = false;
            if (mode != null && mode.Equals("Admin"))
            {
                allowCreateDropIns = true;
                employeeId = EmployeeId;
                if (CompanyId > 0)
                    companyId = CompanyId;
            }

            objDropInsTeachersList = DropInsModelDB.IsLM(DropInYear, objDropInsTeachersList, employeeId, supervisorId, (mode == null || mode.ToString() == "") ? "" : mode.ToString(), companyId, objUserContextViewModel.UserId);

            //objDropInsTeachersList = DropInsModelDB.GetAllLMEmployeeList(DropInYear, null, objUserContextViewModel.UserId);


            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            bool canViewEmployee = PermissionList.Where(x => x.OtherPermissionId == 25).Count() > 0 ? true : false;

            string Actions_View = "<a class='btn btn-info  btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDeductionDetails(***EmployeeId*** , \"***FirstName*** ***LastName***\" , ***DropInYear*** , \"***DropInsYearTxt***\")' title='View'>(0)</a>";

            string Actions_View_Count = "<a class='btn btn-info btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDeductionDetails(***EmployeeId*** , \"***FirstName*** ***LastName***\" , ***DropInYear*** , \"***DropInsYearTxt***\")' title='View' >(***Count***)</a>";

            string Actions_Create = "<a target= “_blank” class='btn btn-primary btn-rounded btn-condensed btn-sm custom-margin' onclick='CreateDropIn(***EmployeeId***, ***DropInYear***)' title='Create' ><i class='fa fa-plus'></i></a>";


            var vList = new object();
            vList = new
            {
                aaData = (from item in objDropInsTeachersList
                          where item.employeeDetailsModel.usertypeid == 1
                          select new
                          {
                              EmployeeId = item.EmployeeId,
                              FirstName = canViewEmployee == false ? item.FirstName : item.FirstName,
                              LastName = item.LastName,
                              Gender = item.genderModel.GenderName_1,
                              CompanyId = item.CompanyId,
                              EmployeeAlternativeID = item.employeeDetailsModel.EmployeeAlternativeID,
                              PositionName = item.employmentInformation.PositionName,
                              Department = item.employeeDetailsModel.BasicDetails.Department,
                              ViewDropIns = item.DropInsCount == 0 ? "<a style='text-decoration:none' class='cursorPointer hide' onclick='AddDropIns(" + item.EmployeeId.ToString() + ",\"" + DropInYear + "\")'>View</a>" : "<a style='text-decoration:none' class='cursorPointer' onclick='AddDropIns(" + item.EmployeeId.ToString() + ",\"" + DropInYear + "\")'>View(" + item.DropInsCount.ToString() + ")</a>",
                              CreateDropIns = "<a style='text-decoration:none' class='cursorPointer hide' onclick='CreateDropIn(" + item.EmployeeId.ToString() + ",\"" + DropInYear + "\")'>Create</a>",
                              //Action = (item.KPICount > 0 ? (item.DropInsCount > 0 ? Actions_View_Count + " " + (item.employeeDetailsModel.usertypeid == 1 ? (objUserContextViewModel.UserId == item.employmentInformation.SuperviserID ? Actions_Create : "") : "") : Actions_View + " " + (item.employeeDetailsModel.usertypeid == 1 ? (objUserContextViewModel.UserId == item.employmentInformation.SuperviserID ? Actions_Create : "") : "")) : "").Replace("***DropInsYearTxt***", DropInsYearTxt.ToString())
                              //*** Naresh 2020-03-16 Allow to create DropIns
                              Action = (item.KPICount > 0 ? (item.DropInsCount > 0 ? Actions_View_Count + " " + (item.employeeDetailsModel.usertypeid == 1 ? (objUserContextViewModel.UserId == item.employmentInformation.SuperviserID || allowCreateDropIns ? Actions_Create : "") : "") : Actions_View + " " + (item.employeeDetailsModel.usertypeid == 1 ? (objUserContextViewModel.UserId == item.employmentInformation.SuperviserID || allowCreateDropIns ? Actions_Create : "") : "")) : "<label class=\"red\">No KPI Exists</label>").Replace("***DropInsYearTxt***", DropInsYearTxt.ToString())
                                            .Replace("***DropInYear***", DropInYear.ToString())
                                            .Replace("***LastName***", item.LastName.ToString())
                                                .Replace("***FirstName***", item.FirstName.ToString())
                                                .Replace("***EmployeeId***", item.EmployeeId.ToString())
                                                .Replace("***Count***", item.DropInsCount.ToString()),
                              KPICount = " <label class=\"KPICount hide\">" + item.KPICount + "</label>",
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult ViewAllDropIns()
        {
            return View("_ViewAllDropIns");
        }
        public ActionResult GetTop3DropIns(int TeacherID, int DropInYear, int? CompanyID = null)
        {

            List<DropIns> DropInsList = new List<DropIns>();
            DropInsModelDB DropInsModelDB = new DropInsModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            DropInsList = DropInsModelDB.GetTop3DropInsList(TeacherID, DropInYear, CompanyID);

            var vList = new object();
            vList = new
            {
                aaData = (from item in DropInsList
                          select new
                          {
                              ID = item.ID,
                              VisitDATE = item.VisitDATE != null ? ((DateTime)item.VisitDATE).ToString(HRMSDateFormat) : "",
                              IsAcknowledged = (item.isEmployeeAcknowledgedDropIn == 0 ? "No" : "Yes"),
                              RequestID = (item.RequestID == 0 ? "" : item.RequestID.ToString()),
                              Total = item.Total,
                              OverAllScore = item.OverAllScore,
                              Action = "<a target= “_blank” class='btn btn-info btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDropIns(" + item.ID.ToString() + "," + item.TeacherID + "," + DropInYear + ")' title='View' >" + PDRPDropInsXMLResources.GetResource("ViewDropInsDetail") + " </a>",

                          }).Take(3).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public ActionResult GetAllDropIns(int TeacherID, int DropInYear)
        {

            List<DropIns> DropInsList = new List<DropIns>();
            DropInsModelDB DropInsModelDB = new DropInsModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            DropInsList = DropInsModelDB.GetAllDropInsList(TeacherID, DropInYear);

            var vList = new object();
            vList = new
            {
                aaData = (from item in DropInsList
                          select new
                          {
                              ID = item.ID,
                              VisitDATE = item.VisitDATE != null ? ((DateTime)item.VisitDATE).ToString(HRMSDateFormat) : "",
                              IsAcknowledged = (item.isEmployeeAcknowledgedDropIn == 0 ? "No" : "Yes"),
                              RequestID = (item.RequestID == 0 ? "" : item.RequestID.ToString()),
                              Total = item.Total,
                              OverAllScore = item.OverAllScore,
                              Action = "<a target= “_blank” class='btn btn-info btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDropIns(" + item.ID.ToString() + "," + item.TeacherID + "," + DropInYear + "," + item.RequestID + ")' title='View' >" + PDRPDropInsXMLResources.GetResource("ViewDropInsDetail") + "</a>",

                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public OperationDetails ValidatingAnnualAppraisal(int? PDRPDropInID, int? EMployeeID, int? DropInYear, int? isAddMode)
        {
            //*** F4
            OperationDetails OperationDetails = new OperationDetails();

            if (DropInsModelDB.IsAnnualAppraisalStarted(DropInYear, EMployeeID) > 0)
            {
                OperationDetails.Success = false;
                OperationDetails.Message = PDRPDropInsXMLResources.GetResource("Validation5");
                OperationDetails.CssClass = "error";
            }
            else
            {
                OperationDetails.Success = true;
            }
            return OperationDetails;
        }

        public ActionResult SetDropInsActiveEmployeeIDSesson(int? PDRPDropInID, int? EMployeeID, string DropInYear, int? isAddMode, int? RequestID)
        {
            Session["TeacherID"] = EMployeeID;
            Session["DropInYear"] = DropInYear;
            Session["DropInMode"] = isAddMode;
            Session["PDRPDropInID"] = PDRPDropInID;
            Session["PDRPDropInRequestID"] = RequestID;
            //*** F4
            OperationDetails OperationDetails = new OperationDetails();            
            OperationDetails = ValidatingAnnualAppraisal(PDRPDropInID, EMployeeID, Convert.ToInt32(DropInYear), isAddMode);            
            return Json(new { success = OperationDetails.Success, message = OperationDetails.Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetDropInsViewIDSession(int? PDRPDropInID, int? EMployeeID, string DropInYear, int? isAddMode, int? RequestID)
        {
            Session["TeacherID"] = EMployeeID;
            Session["DropInYear"] = DropInYear;
            Session["DropInMode"] = isAddMode;
            Session["PDRPDropInID"] = PDRPDropInID;
            Session["PDRPDropInRequestID"] = RequestID;
            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            OperationDetails.Success = true;
            return Json(new { success = OperationDetails.Success, message = OperationDetails.Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DropInForm()
        {
            OperationDetails OperationDetails = new OperationDetails();
            if (Session["TeacherID"] != null || Session["PDRPDropInID"] != null)
            {
                bool allowCreateDropIns = false;
                if (Session["MyDropIn"] != null && Session["MyDropIn"].ToString().Equals("Admin"))
                {
                    allowCreateDropIns = true;
                }
                ViewBag.AllowCreateDropIns = allowCreateDropIns;
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.CompanyId = objUserContextViewModel.CompanyId;

                var UserId = objUserContextViewModel.UserId;
                var CompanyId = objUserContextViewModel.CompanyId;

                //CountryDB objCountryDB = new CountryDB();
                DropInsModel DropInsModel = new DropInsModel();
                DropInsModel.DropInEmployeeModel.RequesterID = objUserContextViewModel.UserId;
                DropInsModel.DropInEmployeeModel.TeacherID = Convert.ToInt32(Session["TeacherID"] == null ? 0 : Session["TeacherID"]);
                DropInsModel.DropInEmployeeModel.ID = Convert.ToInt32(Session["PDRPDropInID"] == null ? 0 : Session["PDRPDropInID"]);
                DropInsModel.isAddMode = Session["DropInMode"].ToString();
                DropInsModel.DropInEmployeeModel.DropinYearID = Convert.ToInt32(Session["DropInYear"] == null ? 0 : Session["DropInYear"]);
                DropInsModelDB DropInsModelDB = new DropInsModelDB();
                ViewBag.Formula = DropInsModelDB.GetPDRPFormula("DropIns", DropInsModel.DropInEmployeeModel.DropinYearID);
                //*** F4                
                DropInsModel = DropInsModelDB.GetDropInsEmployeeCurrCompany(DropInsModel);
                if (string.IsNullOrEmpty(DropInsModel.DropInEmployeeModel.RequestID))
                    DropInsModel.DropInEmployeeModel.RequestID = Convert.ToString((Session["PDRPDropInRequestID"] == null || Session["PDRPDropInRequestID"].ToString() == "0") ? "" : Session["PDRPDropInRequestID"].ToString());

                OperationDetails = ValidatingAnnualAppraisal(DropInsModel.DropInEmployeeModel.ID, DropInsModel.DropInEmployeeModel.TeacherID, DropInsModel.DropInEmployeeModel.DropinYearID, Convert.ToInt32(DropInsModel.isAddMode));
                if (OperationDetails.Success == false)
                {
                    ViewBag.ValidatingAnnualAppraisal = PDRPDropInsXMLResources.GetResource("Validation1");
                }
                return View(DropInsModel);
            }
            else
            {
                return RedirectToAction("DropInManage", "DropIns");
            }
        }
        public ActionResult GetDropInsEmployeeInformation(Int16 formID, DropInsModel DropInsModel)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLowerInvariant();
            DropInsModel = DropInsModelDB.GetDropInsEmployeeInformation(DropInsModel, culture);
            if (DropInsModel.isAddMode == "2") //*** Edit
            {
                DropInsModel = DropInsModelDB.GetDropInsEmployeeDetails(DropInsModel);
            }
            DropInsModel.DropInEmployeeModel.RequestDate = DateTime.Now;
            DropInsModel.DropInEmployeeModel.FormID = formID;

            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            OperationDetails = ValidatingAnnualAppraisal(DropInsModel.DropInEmployeeModel.ID, DropInsModel.DropInEmployeeModel.TeacherID, DropInsModel.DropInEmployeeModel.DropinYearID, Convert.ToInt32(DropInsModel.isAddMode));
            if (OperationDetails.Success == false)
            {
                ViewBag.ValidatingAnnualAppraisal = PDRPDropInsXMLResources.GetResource("Validation1");
            }
            bool allowCreateDropIns = false;
            if (Session["MyDropIn"] != null && Session["MyDropIn"].ToString().Equals("Admin"))
            {
                allowCreateDropIns = true;
            }
            ViewBag.AllowCreateDropIns = allowCreateDropIns;
            return PartialView(VirtualPathUtility.ToAbsolute("~/Areas/PDRP/Views/Shared/_RequestedPDRPEmployeeDetails.cshtml"), DropInsModel);
        }

        public ActionResult GetDropInsProficiencyLevel(Int16 formID, DropInsModel DropInsModel)
        {

            DropInsModel = DropInsModelDB.GetDropInProficiencyLevel(DropInsModel);

            List<DropInProficiencyLevel> listObjects = (from obj in DropInsModel.DropInProficiencyLevel
                                                        select obj).GroupBy(n => new { n.ProficiencyGroupID, n.ProficiencyGroupName })
                                           .Select(g => g.FirstOrDefault())
                                           .ToList();
            ViewBag.ProficiencyGroupName = listObjects;

            //*** F5
            List<DropInsRatingScaleWeightagesNA> DropInsRatingScaleWeightagesNAList = new List<DropInsRatingScaleWeightagesNA>();
            DropInsRatingScaleWeightagesNA DropInsRatingScaleWeightagesNA;
            for (int i = 0; i < DropInsModel.DropInsRatingScaleWeightages.Count; i++)
            {
                DropInsRatingScaleWeightagesNA = new DropInsRatingScaleWeightagesNA();
                DropInsRatingScaleWeightagesNA.RatingScaleID = DropInsModel.DropInsRatingScaleWeightages[i].RatingScaleID;
                if (DropInsModel.DropInsRatingScaleWeightages[i].RatingScaleNumber == 0)
                {
                    DropInsRatingScaleWeightagesNA.RatingScaleNumber = "N/A";
                }
                else
                {
                    DropInsRatingScaleWeightagesNA.RatingScaleNumber = DropInsModel.DropInsRatingScaleWeightages[i].RatingScaleNumber.ToString();
                }

                DropInsRatingScaleWeightagesNAList.Add(DropInsRatingScaleWeightagesNA);

            }

            //ViewBag.DropInsRatingScaleWeightages = new SelectList(DropInsModel.DropInsRatingScaleWeightages.OrderBy(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            ViewBag.DropInsRatingScaleWeightages = new SelectList(DropInsRatingScaleWeightagesNAList.OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            ViewBag.Formula = DropInsModelDB.GetPDRPFormula("DropIns", DropInsModel.DropInEmployeeModel.DropinYearID);

            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            OperationDetails = ValidatingAnnualAppraisal(DropInsModel.DropInEmployeeModel.ID, DropInsModel.DropInEmployeeModel.TeacherID, DropInsModel.DropInEmployeeModel.DropinYearID, Convert.ToInt32(DropInsModel.isAddMode));
            if (OperationDetails.Success == false)
            {
                ViewBag.ValidatingAnnualAppraisal = PDRPDropInsXMLResources.GetResource("Validation1");
            }
            bool allowCreateDropIns = false;
            if (Session["MyDropIn"] != null && Session["MyDropIn"].ToString().Equals("Admin"))
            {
                allowCreateDropIns = true;
            }
            ViewBag.AllowCreateDropIns = allowCreateDropIns;
            return PartialView("_ProficiencyLevel", DropInsModel);
        }

        [HttpPost]
        public JsonResult DropInFormSave(DropInsModel DropInsModel)
        {
            string result = "";
            string resultMessage = "", requestID = "", resultMode = "";
            objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            OperationDetails operationDetails = new OperationDetails();
            try
            {
                if (DropInsModel.isAddMode != "2" && DropInsModel.ProficiencyScoreFormula == null)
                {
                    result = "error";
                    resultMessage = PDRPDropInsXMLResources.GetResource("Validation6");
                }
                else
                {
                    if (DropInsModel.isAddMode == "3" && DropInsModel.ProficiencyScoreTotal == 0)
                    {
                        result = "error";
                        resultMessage = PDRPDropInsXMLResources.GetResource("Validation6");
                    }
                    else
                    {
                        if (DropInsModel.isAddMode == "3" && DropInsModel.EmployeeAcknowledgedDropIn == false)
                        {
                            DropInsModel.isAddMode = "2";
                            result = "error";
                            resultMessage = PDRPDropInsXMLResources.GetResource("Validation7");

                        }
                        else
                        {
                            if (DropInsModel.isAddMode == "2" && DropInsModel.EmployeeAcknowledgedDropIn == true)
                            {
                                DropInsModel.EmployeeAcknowledgedDropIn = false; //*** On Request F4
                            }
                            //*** F5
                            foreach (DropInProficiencyLevel Level in DropInsModel.DropInProficiencyLevel)
                            {

                                if (DropInsModel.isAddMode == "3" && Level.ProficiencyScore == null)
                                {
                                    result = "error";
                                    resultMessage = PDRPDropInsXMLResources.GetResource("Validation8");

                                }
                            }
                            string Result = "";
                            if (resultMessage != PDRPDropInsXMLResources.GetResource("Validation8"))
                            {
                                if (DropInsModel.isAddMode == "3")
                                {
                                    Result = DropInsModelDB.SaveDropIns(DropInsModel, 4);
                                }
                                else
                                {
                                    Result = DropInsModelDB.SaveDropIns(DropInsModel, 1);
                                }

                                if (Result.Split(',')[0] == "SUCCESS")
                                {
                                    Session["TeacherID"] = DropInsModel.DropInEmployeeModel.TeacherID;
                                    Session["DropInYear"] = DropInsModel.DropInEmployeeModel.DropinYearID;
                                    Session["DropInMode"] = "2";
                                    Session["PDRPDropInID"] = Result.Split(',')[1].ToString();
                                    string pdrpDropInID = Session["PDRPDropInID"].ToString();
                                    if (Result.Split(',').Length > 2)
                                        requestID = Result.Split(',')[2];
                                    Session["PDRPDropInRequestID"] = !string.IsNullOrEmpty(requestID) ? requestID : Session["PDRPDropInRequestID"];
                                    result = "success";
                                    resultMessage = PDRPDropInsXMLResources.GetResource("Validation9");
                                    resultMode = DropInsModel.isAddMode;
                                    // Email is send only when request generated/saved for first time.No email on Submit.
                                    if (DropInsModel.isAddMode == "2" && DropInsModel.DropInEmployeeModel.ID == 0)
                                    {
                                        //*** Sending Email to Requester and Receiver for Task creastion                                        
                                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                        requestFormsApproverEmailModelList = DropInsModelDB.GetApproverEmailList(pdrpDropInID, objUserContextViewModel.UserId);
                                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                                    }
                                }
                                else
                                {
                                    result = "error";
                                    resultMode = "0";
                                    resultMessage = PDRPDropInsXMLResources.GetResource("Validation10");
                                }
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = PDRPDropInsXMLResources.GetResource("Validation11");

            }
            return Json(new { result = result, resultMessage = resultMessage, resultMode = resultMode }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateDropInScore(IEnumerable<int> Scorevalues, int FormulaYearID)
        {


            int FixedInt = 500;
            int FixedDiv = 5;
            int result = 0;
            //int Count =  Scorevalues == null ? 0 : Scorevalues.Count();
            PDRPFormulasTotalScore PDRPFormulasTotalScore = DropInsModelDB.GetPDRPFormulaCalculations(Scorevalues, "DropIns", FormulaYearID);

            return Json(new { Formula = PDRPFormulasTotalScore.Formula, Total = PDRPFormulasTotalScore.Total, OverallScore = Math.Round(PDRPFormulasTotalScore.TotalScore, 2) }, JsonRequestBehavior.AllowGet);
        }
    }
}