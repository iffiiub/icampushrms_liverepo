﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using HRMS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.Common;
namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class ObservationsController : PDRPCommonController
    {     
        #region Common
        public string OpenPage(string isAddMode)
        {

            string PageName = "";
            FormsDB FormsDB = new FormsDB();          
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

            Session["PreObservationRequestID"] = objRequestFormsProcessModel.RequestID;
            if (objRequestFormsProcessModel.FormID == 31) //*** Pre-Observation 
            {
                Session["PrePDRPObservationFormID"] = objRequestFormsProcessModel.FormID;
                Session["PDRPObservationID"] = objRequestFormsProcessModel.FormInstanceID;
                if (isAddMode == "View")
                {
                    Session["PreObservationMode"] = "100";
                }
                //*** 1=Employer Create
                //Session["PreObservationMode"] = 1;

                Session["PreObservationFormProcessID"] = objRequestFormsProcessModel.FormProcessID;
                PageName = "PreObservation";
            }
            else if (objRequestFormsProcessModel.FormID == 32)
            {
                Session["PDRPObservationFormID"] = objRequestFormsProcessModel.FormID;
                if (isAddMode == "View")
                {
                    Session["ObservationMode"] = "100";
                }
                Session["PDRPObservationEvlID"] = objRequestFormsProcessModel.FormInstanceID;
                PageName = "ObservationEvaluation";
            }
            else if (objRequestFormsProcessModel.FormID == 33)
            {
                Session["PostPDRPObservationFormID"] = objRequestFormsProcessModel.FormID;
                if (isAddMode == "View")
                {
                    Session["PostObservationMode"] = "100";
                }
                Session["PDRPPostObservationID"] = objRequestFormsProcessModel.FormInstanceID;
                PageName = "PostObservation";
            }
            else
            {
                //Session["PDRPPostObservationID"] = objRequestFormsProcessModel.FormInstanceID;
                PageName = "";
            }
            return PageName;
        }
        public ActionResult Edit()
        {
            //Session["ObservationMode"] = "";

            return RedirectToAction(OpenPage(""));
        }
        public ActionResult ViewDetails()
        {
            //Session["ObservationMode"] = "";
            return RedirectToAction(OpenPage("View"));
        }
        #endregion

        #region ObservationProcess Starts
        // GET: ObservationProcess
        ObservationsModelDB ObservationsModelDB = new ObservationsModelDB();
        public ActionResult ObservationProcess()
        {

            //ViewBag.PDRPYears = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true).OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            ViewBag.PDRPYears = new SelectList(new AcademicYearDB().GetAllAcademicYearLanguage().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");

            return View();
        }
        public ActionResult GetAllTeachingEmployee(int DropInYear, string DropInsYearTxt)
        {
            List<PDRPTeachersModel> objObservationProcessTeachersList = new List<PDRPTeachersModel>();

            PDRPCommonModelDB PDRPCommonModelDB = new PDRPCommonModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objObservationProcessTeachersList = PDRPCommonModelDB.GetAllLMEmployeeList(DropInYear, null, objUserContextViewModel.UserId, null, "", objUserContextViewModel.UserId);

            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
           
            bool canViewEmployee = PermissionList.Where(x => x.OtherPermissionId == 25).Count() > 0 ? true : false;
           
            string Actions_View = "<a class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='PreObservation(***EmployeeId*** , ***DropInYear*** , \"***DropInsYearTxt***\")' title='Create/View'><i class='fa fa-plus'></i></a>";
            string Actions_Viewp = "<a class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='PostObservation(***EmployeeId*** , ***DropInYear*** , \"***DropInsYearTxt***\")' title='Create/View'><i class='fa fa-plus'></i></a>";
            string Actions_Viewpp = "<a class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='ObservationEvaluation(***EmployeeId*** , ***DropInYear*** , \"***DropInsYearTxt***\")' title='Create/View'><i class='fa fa-plus'></i></a>";

            string Actions_View_Count = "<a class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='PreObservation(***EmployeeId*** , \"***FirstName*** ***LastName***\" , ***DropInYear*** , \"***DropInsYearTxt***\")' title='View' ><i class='fa fa-eye'></i>(***Count***)</a>";

            string Actions_Create = "<a  class='btn btn-danger btn-rounded btn-condensed btn-sm custom-margin' onclick='CreateDropIn(***EmployeeId***, ***DropInYear***)' title='Create' ><i class='fa fa-plus'></i></a>";


            var vList = new object();
            vList = new
            {
                aaData = (from item in objObservationProcessTeachersList
                          where item.employeeDetailsModel.usertypeid == 1
                          select new
                          {
                              Default="",
                              Select = item.KPICount==0 ? "<label class=\"red\">No KPI Exists</label>" : item.VisitDate.Length > 0 ? "<input class='form-group disabled' id='chkEmployeeId' name='chkEmployeeId' disabled style='margin-top:10px;' type='checkbox' value='true' checked>" : "<input class='form-group' onChange='ChkChange(this)' id='chkEmployeeId' name='chkEmployeeId'  style='margin-top:10px;' type='checkbox' value='true'>",
                              //Select = item.KPICount == 0 ? "<input class='form-group disabled' id='chkEmployeeId' name='chkEmployeeId' disabled style='margin-top:10px;' type='checkbox' value='false'>" : item.VisitDate.Length > 0 ? "<input class='form-group disabled' id='chkEmployeeId' name='chkEmployeeId' disabled style='margin-top:10px;' type='checkbox' value='true' checked>" : "<input class='form-group' onChange='ChkChange(this)' id='chkEmployeeId' name='chkEmployeeId'  style='margin-top:10px;' type='checkbox' value='true'>",
                              EmployeeId = "<input type='text' id='EmployeeID' class='form-control' value="+item.EmployeeId+" />",
                              FirstName = canViewEmployee == false ? item.FirstName : item.FirstName,
                              LastName = item.LastName,
                              KPICount = " <label class=\"KPICount hide\">" + item.KPICount + "</label>",
                              EmployeeAlternativeID = item.employeeDetailsModel.EmployeeAlternativeID,
                              VisitDate = item.VisitDate.Length > 0 ? "<div class='input-group' >" +
                                            "<span class='input-group-addon add-on cal "+ (CultureHelper.IsRightToLeft() ? "ritrds" : "") + "'><span class='fa fa-calendar'></span></span><input data-val='true' onChange='DateAdd(this)' class='form-control datepicker  tbdate " + (CultureHelper.IsRightToLeft() ? "lftrds" : "") + "' data-date-today-highlight='true' readOnly disabled yearRange: '1999:2012' id='txtVisitDate_" + item.EmployeeId + "' name='txtVisitDate_" + item.EmployeeId + "'  type='text' value='" + item.VisitDate + "'></div>" : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal " + (CultureHelper.IsRightToLeft() ? "ritrds" : "") + "'><span class='fa fa-calendar'></span></span><input data-val='true' onChange='DateAdd(this)' class='form-control datepicker tbdate pointer " + (CultureHelper.IsRightToLeft() ? "lftrds" : "") + "'  data-date-today-highlight='true'  readOnly yearRange: '1999:2012' id='txtVisitDate_" + item.EmployeeId + "' name='txtVisitDate_" + item.EmployeeId + "'  type='text' value='" + item.VisitDate + "'></div>",
                              VisitTime = item.VisitDate.Length > 0 ? "<div class='input-group' ><span class='input-group-addon add-on cal " + (CultureHelper.IsRightToLeft() ? "ritrds" : "") + "'><span class='fa fa-calendar'></span></span><input class='form-control timepicker ' disabled id='txtVisitTime_" + item.EmployeeId + "' name='txtVisitTime_" + item.EmployeeId + "' type='text' value='" + item.VisitTime + "'></div>" : "<div class='input-group' ><span class='input-group-addon add-on cal " + (CultureHelper.IsRightToLeft() ? "ritrds" : "") + "'><span class='fa fa-calendar'></span></span><input class='form-control timepicker pointer' readOnly id='txtVisitTime_" + item.EmployeeId + "' name='txtVisitTime_" + item.EmployeeId + "' type='text' value='" + item.VisitTime + "'></div>",
                              /*Commented 2020-04-26 Nithin  Show claendar Icon PreObservationDueDate
                              //PreObservationDueDate = item.VisitDate.Length > 0 ? "<input class='form-control nocur'  readonly='readonly'  id='txtPreObsProc' name='txtPreObsProc'  type='text' value='" + item.PreObservationDate + "'>" : "<input class='form-control nocur' readonly='readonly'  id='txtPreObsProc' name='txtPreObsProc'  type='text' value='" + item.PreObservationDate + "'>",
                              */
                              PreObservationDueDate = item.VisitDate.Length > 0 ? "<div class='input-group' >" +
                                            "<span class='input-group-addon add-on cal " + (CultureHelper.IsRightToLeft() ? "ritrds" : "") + "'><span class='fa fa-calendar'></span></span><input class='form-control nocur " + (CultureHelper.IsRightToLeft() ? "lftrds" : "") + "'  readonly='readonly'  id='txtPreObsProc' name='txtPreObsProc'  type='text' value='" + item.PreObservationDate + "'></ div>" : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal " + (CultureHelper.IsRightToLeft() ? "ritrds" : "") + "'><span class='fa fa-calendar'></span></span><input class='form-control nocur " + (CultureHelper.IsRightToLeft() ? "lftrds" : "") + "' readonly='readonly'  id='txtPreObsProc' name='txtPreObsProc'  type='text' value='" + item.PreObservationDate + "'></div>",

                              //Action = (Actions_View + Actions_Viewp + Actions_Viewpp).Replace("***DropInYear***", DropInYear.ToString()).Replace("***EmployeeId***", item.EmployeeId.ToString()).Replace("***DropInsYearTxt***", DropInsYearTxt.ToString()),

                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public ActionResult SetObservationVisitDateMulty(List<PDRPTeachersModel> SelectedTeachersList, PDRPTeachersModel SelectedTeacherl, int YearId)
        {

            ObservationsModelDB ObservationsModelDB = new ObservationsModelDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            SelectedTeacherl.CreatedBy = objUserContextViewModel.UserId;
            SelectedTeacherl.CompanyId = objUserContextViewModel.CompanyId;
            ////*** F4
            //OperationDetails OperationDetails = new OperationDetails();
            //for (int i = 0; i < SelectedTeachersList.Count; i++)
            //{
            //    OperationDetails = ValidatingAnnualAppraisal(SelectedTeachersList[i].EmployeeId, YearId);

            //}
            return Json(ObservationsModelDB.SetObservationVisitDateMulty(SelectedTeachersList, SelectedTeacherl, YearId), JsonRequestBehavior.AllowGet);
        }
        public OperationDetails ValidatingAnnualAppraisal( int? EMployeeID, int? DropInYear)
        {
            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            DropInsModelDB DropInsModelDB = new DropInsModelDB();
            if (DropInsModelDB.IsAnnualAppraisalStarted(DropInYear, EMployeeID) > 0)
            {
                OperationDetails.Success = false;
                OperationDetails.Message = PDRPObservationsXMLResources.GetResource("Validation1");
                OperationDetails.CssClass = "error";
            }
            else
            {
                OperationDetails.Success = true;
            }
            return OperationDetails;
        }
        #endregion

        #region Pre-Observation
        [HttpPost]
        public JsonResult PreObservationsFormSave(ObservationsModel ObservationsModel)
        {
            string result = "";
            string resultMessage = "";
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ObservationsModel.ObservationsEmployeeModel.CompanyID = objUserContextViewModel.CompanyId;
            try
            {
                RequestFormsProcessModel objRequestFormsProcessModel;
                if (ObservationsModel.isAddMode == "3" && (ObservationsModel.IsOnTrackYearlyPlansNo == "" || ObservationsModel.IsOnTrackYearlyPlansNo  == null)&& ObservationsModel.IsOnTrackYearlyPlans == 1)
                {
                    result = "error";
                    resultMessage = PDRPObservationsXMLResources.GetResource("Pleaseexplain");
                }
                else
                {
                    if (ObservationsModel.isAddMode == "3")
                    {
                        objRequestFormsProcessModel = ObservationsModelDB.SavePreObservations(ObservationsModel, 4, objUserContextViewModel.UserId);
                    }
                    else
                    {
                        objRequestFormsProcessModel = ObservationsModelDB.SavePreObservations(ObservationsModel, 1, objUserContextViewModel.UserId);
                    }

                    PassportWithdrawalRequestDB objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();

                    if (objRequestFormsProcessModel != null && objRequestFormsProcessModel.RequestID > 0)
                    {
                        //*** Sending Email to Requester and Receiver for Task creastion 
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(objRequestFormsProcessModel.FormProcessID), objUserContextViewModel.UserId);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                        if (ObservationsModel.isAddMode == "3")//*** SignOff Employee. Completed Final Approval is done
                        {
                            //*** final email notification
                            //*** ReqStatusID is 4/Completed Final Approval is done
                            //if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                            //}                                    //{
                            //*** Getting the final email notification group's employee & email details
                            FormsDB formsDB = new FormsDB();
                            requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(objRequestFormsProcessModel.FormInstanceID); //*** old FormProcessID
                            if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                            {
                                SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                                operationDetails.Message += @" & Final Email Notification send";
                            }
                        }
                        result = "success";
                        resultMessage =PDRPObservationsXMLResources.GetResource("ObservationsSaved");
                    }
                    else
                    {
                        if (objRequestFormsProcessModel != null)
                        {
                            result = "success";
                            resultMessage = PDRPObservationsXMLResources.GetResource("ObservationsSaved");
                        }
                        else
                        {
                            result = "error";
                            resultMessage = PDRPObservationsXMLResources.GetResource("Erroroccured");
                        }

                    }
                }
                //result = "error";
                //resultMessage = "Error occured while adding Observations.";

            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = PDRPObservationsXMLResources.GetResource("Erroroccured");
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetViewPreObservationSesson(int ID)
        {

            Session["PreObservationMode"] = 100;
            Session["PDRPObservationID"] = ID;
            Session["PrePDRPObservationFormID"] = 31;
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PreObservation()
        {
            if (Session["PDRPObservationID"] != null)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.CompanyId = objUserContextViewModel.CompanyId;


                var CompanyId = objUserContextViewModel.CompanyId;


                ObservationsModel ObservationsModel = new ObservationsModel();

                ObservationsModel.ObservationsEmployeeModel.ID = Convert.ToInt32(Session["PDRPObservationID"] == null ? 0 : Session["PDRPObservationID"]);
                ObservationsModelDB ObservationsModelDB = new ObservationsModelDB();
                ObservationsModel = ObservationsModelDB.GetPreObservationDetail(ObservationsModel);
                ObservationsModel.isAddMode = Session["PreObservationMode"] == null ? "1" : Session["PreObservationMode"].ToString();
                //ObservationsModel.ObservationsEmployeeModel.FormID = Convert.ToInt16(Session["PrePDRPObservationFormID"] == null ? 0 : Session["PrePDRPObservationFormID"]);
              // ObservationsModel.ObservationsEmployeeModel.RequestID = Session["PreObservationRequestID"] == null ? "0" : Session["PreObservationRequestID"].ToString(); --28-04-2020 Nithin , Request ID is taken from DB
                ObservationsModel.ObservationsEmployeeModel.FormID = 31;

                //*** F6               
                int formProcessID = GetFormProcessSessionID();
                //*** F10
                //ObservationsModel.FormProcessID = formProcessID;

                int CurrFormProceeID = ObservationsModelDB.GetCorrectFormProcessID(Convert.ToInt16(ObservationsModel.ObservationsEmployeeModel.ID),31);
                if(CurrFormProceeID!= formProcessID)
                {
                    ObservationsModel.FormProcessID = CurrFormProceeID;
                }
                else
                {
                    ObservationsModel.FormProcessID = formProcessID;
                }
                //*** F4
                OperationDetails OperationDetails = new OperationDetails();
                OperationDetails = ValidatingAnnualAppraisal(ObservationsModel.ObservationsEmployeeModel.TeacherID, ObservationsModel.ObservationsEmployeeModel.ObservationsYearID);
                if (OperationDetails.Success == false)
                {
                    ViewBag.ValidatingAnnualAppraisal = PDRPObservationsXMLResources.GetResource("Validation1");
                }
                return View(ObservationsModel);
            }
            else
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
        }
        // old GetObservationsEmployeeInformation
        public ActionResult GetPreObservationsEmployeeInformation(Int16 formID, ObservationsModel ObservationsModel)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLowerInvariant();
            //ObservationsModel.ObservationsEmployeeModel = ObservationsModelDB.GetObservationsEmployeeInformationOb(ObservationsModel.ObservationsEmployeeModel);
            ObservationsModel.ObservationsEmployeeModel = ObservationsModelDB.GetPreObservationsEmployeeInformation(ObservationsModel.ObservationsEmployeeModel,culture);           
            ObservationsModel.ObservationsEmployeeModel.RequestDate = DateTime.Now;
            ObservationsModel.ObservationsEmployeeModel.FormID = formID;

            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            OperationDetails = ValidatingAnnualAppraisal(ObservationsModel.ObservationsEmployeeModel.TeacherID, ObservationsModel.ObservationsEmployeeModel.ObservationsYearID);
            if (OperationDetails.Success == false)
            {
                ViewBag.ValidatingAnnualAppraisal = PDRPObservationsXMLResources.GetResource("Validation1");
            }
            if (ObservationsModel.ObservationsEmployeeModel.FormID == 31)
            {
                return PartialView("_PreObEmployeeDetails", ObservationsModel);
            }
            else if (ObservationsModel.ObservationsEmployeeModel.FormID == 32)
            {
                return PartialView("_ObEvlEmployeeDetails", ObservationsModel);
            }
            else if (ObservationsModel.ObservationsEmployeeModel.FormID == 33)
            {
                return PartialView("_PostObEmployeeDetails", ObservationsModel);
            }
            else
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
        }

        public ActionResult GetPreObservationsDetails(Int16 formID, ObservationsModel ObservationsModel)
        {
            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            OperationDetails = ValidatingAnnualAppraisal(ObservationsModel.ObservationsEmployeeModel.TeacherID, ObservationsModel.ObservationsEmployeeModel.ObservationsYearID);
            if (OperationDetails.Success == false)
            {
                ViewBag.ValidatingAnnualAppraisal = PDRPObservationsXMLResources.GetResource("Validation1");
            }
            return PartialView("_PreObservation", ObservationsModel);
        }

        #endregion






        #region Observation Eval

        public ActionResult GetObservationsEvlProficiencyLevel(Int16 formID, ObservationsModel ObservationsModel)
        {
            
            ObservationsModel = ObservationsModelDB.GetObservationsProficiencyLevel(ObservationsModel);


            //*** F5
            DropInsModel DropInsModel = new DropInsModel();
            List<DropInsRatingScaleWeightagesNA> DropInsRatingScaleWeightagesNAList = new List<DropInsRatingScaleWeightagesNA>();
            DropInsRatingScaleWeightagesNA DropInsRatingScaleWeightagesNA;
            for (int i = 0; i < ObservationsModel.ObservationsRatingScaleWeightages.Count; i++)
            {
                DropInsRatingScaleWeightagesNA = new DropInsRatingScaleWeightagesNA();
                DropInsRatingScaleWeightagesNA.RatingScaleID = ObservationsModel.ObservationsRatingScaleWeightages[i].RatingScaleID;
                if (ObservationsModel.ObservationsRatingScaleWeightages[i].RatingScaleNumber == 0)
                {
                    DropInsRatingScaleWeightagesNA.RatingScaleNumber = "N/A";
                }
                else
                {
                    DropInsRatingScaleWeightagesNA.RatingScaleNumber = ObservationsModel.ObservationsRatingScaleWeightages[i].RatingScaleNumber.ToString();
                }

                DropInsRatingScaleWeightagesNAList.Add(DropInsRatingScaleWeightagesNA);

            }

            
            ViewBag.ObservationsRatingScaleWeightages = new SelectList(DropInsRatingScaleWeightagesNAList.OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            //ViewBag.ObservationsRatingScaleWeightages = new SelectList(ObservationsModel.ObservationsRatingScaleWeightages.OrderBy(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            //*** F4
            OperationDetails OperationDetails = new OperationDetails();
            OperationDetails = ValidatingAnnualAppraisal(ObservationsModel.ObservationsEmployeeModel.TeacherID, ObservationsModel.ObservationsEmployeeModel.ObservationsYearID);
            if (OperationDetails.Success == false)
            {
                ViewBag.ValidatingAnnualAppraisal = PDRPObservationsXMLResources.GetResource("Validation1");
            }
            return PartialView("_OBProficiencyLevel", ObservationsModel);
        }
        
        public ActionResult SetViewObservationSesson(int ID)
        {

            Session["ObservationMode"] = 100;
            Session["PDRPObservationEvlID"] = ID;
            Session["PDRPObservationFormID"] = 33;
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObservationEvaluation()
        {
            if (Session["PDRPObservationEvlID"] != null)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.CompanyId = objUserContextViewModel.CompanyId;
                ViewBag.UserID = objUserContextViewModel.UserId;

                var CompanyId = objUserContextViewModel.CompanyId;


                ObservationsModel ObservationsModel = new ObservationsModel();

                ObservationsModel.ObservationsEmployeeModel.ID = Convert.ToInt32(Session["PDRPObservationEvlID"] == null ? 0 : Session["PDRPObservationEvlID"]);
                ObservationsModelDB ObservationsModelDB = new ObservationsModelDB();
                ObservationsModel = ObservationsModelDB.GetObservationEvlDetail(ObservationsModel);
                ObservationsModel.isAddMode = Session["ObservationMode"] == null ? "1" : Session["ObservationMode"].ToString();
                ObservationsModel.ObservationsEmployeeModel.FormID = Convert.ToInt16(Session["PDRPObservationFormID"] == null ? 0 : Session["PDRPObservationFormID"]);
                
                /* //Commented Nithin, 05-06-2020, RequestID will be bind from 
                 ObservationsModel.ObservationsEmployeeModel.RequestID = Session["PreObservationRequestID"] == null ? "1" : Session["PreObservationRequestID"].ToString();
                */

              //*** F6

              /* //Commented Nithin, 05-06-2020, FormProcessID & 
                int formProcessID = GetFormProcessSessionID();
                ObservationsModel.FormProcessID = formProcessID;
               */

                //30-04-2020 Nithin,Show SignOff Button if LM and Employee are Same
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(ObservationsModel.FormProcessID);
                //Request is pending for employee,GroupID is 1, So SignOffMode =1
                ViewBag.SignOffMode = requestFormsApproveModel.GroupID;
               //*** F4
               OperationDetails OperationDetails = new OperationDetails();
                OperationDetails = ValidatingAnnualAppraisal(ObservationsModel.ObservationsEmployeeModel.TeacherID, ObservationsModel.ObservationsEmployeeModel.ObservationsYearID);
                if (OperationDetails.Success == false)
                {
                    ViewBag.ValidatingAnnualAppraisal = PDRPObservationsXMLResources.GetResource("Validation1");
                }
                return View(ObservationsModel);
            }
            else
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
        }

        [HttpPost]
        public JsonResult ObservationFormSave(ObservationsModel ObservationsModel)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ObservationsModel.ObservationsEmployeeModel.CompanyID = objUserContextViewModel.CompanyId;

            string result = "";
            string resultMessage = "";
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                if (ObservationsModel.isAddMode != "105" && ObservationsModel.ProficiencyScoreFormula == null)
                {
                    result = "error";
                    resultMessage = PDRPObservationsXMLResources.GetResource("Youmust");
                }
                else
                {
                    if (ObservationsModel.isAddMode != "105" && ObservationsModel.ProficiencyScoreTotal == 0)
                    {
                        result = "error";
                        resultMessage = PDRPObservationsXMLResources.GetResource("Youmust"); 
                    }
                    else
                    { 
                        RequestFormsProcessModel objRequestFormsProcessModel;
                        PassportWithdrawalRequestDB objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
                        //if (ObservationsModel.isAddMode == "3") --** This never reche
                        //{
                        //    objRequestFormsProcessModel = ObservationsModelDB.SaveObservationsEvl(ObservationsModel, 4);
                        //}
                        //else
                        //{


                        if (ObservationsModel.isAddMode=="105") //*** Form Saved
                        {
                            if (ObservationsModel.FormMode == 1)
                            {
                                ObservationsModel.FormMode = 10;
                            }
                        }
                        else if (ObservationsModel.isAddMode == "101") //*** Initiate Post Observation
                        {
                            ObservationsModel.FormMode = 1;
                        }
                        else if (ObservationsModel.isAddMode == "102") //*** Observation Submit after Post Observation
                        {
                            ObservationsModel.FormMode = 2;
                        }
                        else if (ObservationsModel.isAddMode == "103") //*** Employee Signing Off
                        {
                            ObservationsModel.FormMode = 3;
                            ObservationsModel.Comments = "SignOff"; //Auto add signoff as comment when employee signoff
                        }

                        //*** F5
                        foreach (ObservationsProficiencyLevel Level in ObservationsModel.ObservationsProficiencyLevel)
                        {

                            if ((ObservationsModel.isAddMode == "105" || ObservationsModel.isAddMode == "103"))
                            {
                               
                                //*** No Validatipon on Save & Signing off
                            }
                            else
                            {
                                if (Level.ProficiencyScore == null)
                                {
                                    result = "error";
                                    resultMessage = PDRPObservationsXMLResources.GetResource("Pleaseselect");
                                }
                            }


                        }
                        if ( resultMessage != PDRPObservationsXMLResources.GetResource("Pleaseselect"))
                        {
                            objRequestFormsProcessModel = ObservationsModelDB.SaveObservationsEvl(ObservationsModel, 1);
                            //}
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            if (objRequestFormsProcessModel != null && objRequestFormsProcessModel.RequestID == 0 && objRequestFormsProcessModel.FormInstanceID > 0)
                            {
                                //if (ObservationsModel.isAddMode == "3")//*** SignOff Employee. Completed Final Approval is done
                                //{
                                //*** final email notification
                                //*** ReqStatusID is 4/Completed Final Approval is done
                                //if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                                //}                                    //{
                                //*** Getting the final email notification group's employee & email details
                                FormsDB formsDB = new FormsDB();
                                requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(objRequestFormsProcessModel.FormInstanceID); //*** old FormProcessID
                                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                {
                                    SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                                    operationDetails.Message += @" & " +PDRPObservationsXMLResources.GetResource("FinalEmail");
                                }
                                //}
                            }
                            if (objRequestFormsProcessModel != null && objRequestFormsProcessModel.RequestID > 0)
                            {

                                requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(objRequestFormsProcessModel.FormProcessID), objUserContextViewModel.UserId);
                                if (ObservationsModel.FormMode == 2 && requestFormsApproverEmailModelList.Count >= 2)//*** LM Submit to Employee. 2 Approver exist in this case so get Employee details
                                {
                                    requestFormsApproverEmailModelList = requestFormsApproverEmailModelList.Where(x => x.GroupID == 1).ToList();
                                }
                                SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);


                                //////*** Get Employee Email ID
                                ////requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(objRequestFormsProcessModel.FormProcessID), objUserContextViewModel.UserId).Where(x => x.GroupID == 1).ToList();
                                //////requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(objRequestFormsProcessModel.FormProcessID), objUserContextViewModel.UserId);


                                ////SendOneUserEmail(requestFormsApproverEmailModelList[requestFormsApproverEmailModelList.Count - 1], false);
                                result = "success";
                                resultMessage = PDRPObservationsXMLResources.GetResource("Observationcreated");
                            }
                            else
                            {
                                if (objRequestFormsProcessModel != null)
                                {
                                    result = "success";
                                    resultMessage = PDRPObservationsXMLResources.GetResource("Observationcreated");
                                }
                                else
                                {
                                    result = "error";
                                    resultMessage = PDRPObservationsXMLResources.GetResource("Erroroccured");
                                }

                            }
                        }
                    }
                   
                }

            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = PDRPObservationsXMLResources.GetResource("Erroroccured");
            }
            return Json(new { result = result, resultMessage = resultMessage, AddMode= ObservationsModel.isAddMode }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CalculateDropInScore(IEnumerable<int> Scorevalues, int FormulaYearID)
        {


            int FixedInt = 500;
            int FixedDiv = 5;
            int result = 0;
            //int Count =  Scorevalues == null ? 0 : Scorevalues.Count();
            PDRPFormulasTotalScore PDRPFormulasTotalScore = ObservationsModelDB.GetPDRPFormulaCalculations(Scorevalues, "Observat", FormulaYearID);

            return Json(new { Formula = PDRPFormulasTotalScore.Formula, Total = PDRPFormulasTotalScore.Total, OverallScore = Math.Round(PDRPFormulasTotalScore.TotalScore, 2) }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Post-Observation

        public ActionResult SetViewPostObservationSesson(int ID)
        {

            Session["PostObservationMode"] = 100;
            Session["PDRPPostObservationID"] = ID;
            Session["PostPDRPObservationFormID"] = 31;
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PostObservation()
        {
            if (Session["PDRPPostObservationID"] != null)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.CompanyId = objUserContextViewModel.CompanyId;


                var CompanyId = objUserContextViewModel.CompanyId;


                ObservationsModel ObservationsModel = new ObservationsModel();

                ObservationsModel.ObservationsEmployeeModel.ID = Convert.ToInt32(Session["PDRPPostObservationID"] == null ? 0 : Session["PDRPPostObservationID"]);
                ObservationsModelDB ObservationsModelDB = new ObservationsModelDB();
                ObservationsModel = ObservationsModelDB.GetPostObservationDetail(ObservationsModel);
                ObservationsModel.isAddMode = Session["PostObservationMode"] == null ? "1" : Session["PostObservationMode"].ToString();
                ObservationsModel.ObservationsEmployeeModel.FormID = Convert.ToInt16(Session["PostPDRPObservationFormID"] == null ? 0 : Session["PostPDRPObservationFormID"]);
                // ObservationsModel.ObservationsEmployeeModel.RequestID = Session["PreObservationRequestID"] == null ? "1" : Session["PreObservationRequestID"].ToString();             

                //*** F6
                
                int formProcessID = GetFormProcessSessionID();
                //*** F10
                //ObservationsModel.FormProcessID = formProcessID;

                int CurrFormProceeID = ObservationsModelDB.GetCorrectFormProcessID(Convert.ToInt16(ObservationsModel.ObservationsEmployeeModel.ID), 33);
                if (CurrFormProceeID != formProcessID)
                {
                    ObservationsModel.FormProcessID = CurrFormProceeID;
                }
                else
                {
                    ObservationsModel.FormProcessID = formProcessID;
                }

                //*** F4
                OperationDetails OperationDetails = new OperationDetails();
                OperationDetails = ValidatingAnnualAppraisal(ObservationsModel.ObservationsEmployeeModel.TeacherID, ObservationsModel.ObservationsEmployeeModel.ObservationsYearID);
                if (OperationDetails.Success == false)
                {
                    ViewBag.ValidatingAnnualAppraisal =PDRPObservationsXMLResources.GetResource("Validation1");
                }
                return View(ObservationsModel);

            }
            else
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
        }

        public ActionResult GetPostObservationsDetails(Int16 formID, ObservationsModel ObservationsModel)
        {
                       return PartialView("_PostObservation", ObservationsModel);
        }
        [HttpPost]
        public JsonResult PostObservationsFormSave(ObservationsModel ObservationsModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ObservationsModel.ObservationsEmployeeModel.CompanyID = objUserContextViewModel.CompanyId;
            string result = "";
            string resultMessage = "";
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                RequestFormsProcessModel objRequestFormsProcessModel;
                if (ObservationsModel.isAddMode == "3")
                {
                    objRequestFormsProcessModel = ObservationsModelDB.SavePostObservations(ObservationsModel, 4);
                }
                else
                {
                    objRequestFormsProcessModel = ObservationsModelDB.SavePostObservations(ObservationsModel, 1);
                }

                PassportWithdrawalRequestDB objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();

                if (objRequestFormsProcessModel != null && objRequestFormsProcessModel.RequestID > 0)
                {
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    //////requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(objRequestFormsProcessModel.FormProcessID), objUserContextViewModel.UserId);
                    //////SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                    if (ObservationsModel.isAddMode == "3")//*** SignOff Employee. Completed Final Approval is done
                    {
                        //*** final email notification
                        //*** ReqStatusID is 4/Completed Final Approval is done
                        //if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                        //}                                    //{
                        //*** Getting the final email notification group's employee & email details
                        FormsDB formsDB = new FormsDB();
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(objRequestFormsProcessModel.FormInstanceID); //*** old FormProcessID
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }
                    }
                    result = "success";
                    resultMessage = PDRPObservationsXMLResources.GetResource("ObservationsSaved");
                }
                else
                {
                    if (objRequestFormsProcessModel != null)
                    {
                        result = "success";
                        resultMessage = PDRPObservationsXMLResources.GetResource("ObservationsSaved");
                    }
                    else
                    {
                        result = "error";
                        resultMessage = PDRPObservationsXMLResources.GetResource("Erroroccured");
                    }

                }
                //result = "error";
                //resultMessage = "Error occured while adding Observations.";

            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = PDRPObservationsXMLResources.GetResource("Erroroccured");
            }
            return Json(new { result = result, resultMessage = resultMessage, AddMode= ObservationsModel.isAddMode }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}