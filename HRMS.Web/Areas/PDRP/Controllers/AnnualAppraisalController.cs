﻿using HRMS.DataAccess;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess.FormsDB;
using System.Data;
using HRMS.Entities.Common;
namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class AnnualAppraisalController : PDRPCommonController
    {
        // GET: PDRP/AnnualAppraisal
        #region Annual Appraisal
        AnnualAppraisalProcessModel annualAppraisalPrcessSetings;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AnnualAppraisal()
        {
            //*** F22 Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYearLanguage();
            int currentAcademicYear = listAcYear.Where(x => x.IsActive).FirstOrDefault().AcademicYearId;
            ViewBag.KPIYears = new SelectList(objAcademicYearDB.GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration", currentAcademicYear.ToString());
            ////ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYearLanguage().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");

            ViewBag.Company = GetCompanySelectListWithCurrentCompany();

            //ViewBag.Company =  GetCompanySelectList();
            return View();
        }
        public ActionResult AnnualAppraisalReversal()
        {
            ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYearLanguage().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            ViewBag.Company = GetCompanySelectList();// new SelectList(new CompanyDB().GetAllCompany(), "CompanyId", "Name");
            return View();
        }
        public ActionResult GetAnnualAppraisalReverseList(int? companyId, int? yearId, int PeriodId)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            List<SelectListItem> performanceGroups = new List<SelectListItem>().ToList();

            IEnumerable<AnnualAppraisalReverceData> annualAppraisalProcessList = annualAppraisalDB.GetAnnualAppraisalReverseList(companyId, yearId, performanceGroups, PeriodId,objUserContextViewModel.UserId);
            ViewBag.PerformanceGroup = performanceGroups;
   
            return PartialView("_AnnualAppraisalReversalGrid", annualAppraisalProcessList);
        }

        public ActionResult GetAnnualAppraisalProcessList(int? companyId, int? yearId, bool? isFormDCategory, int PeriodId)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            List<SelectListItem> performanceGroups = new List<SelectListItem>().ToList();
            //performanceGroups.Add(new SelectListItem() { Text = "Select", Value = "0" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form A", Value = "1" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form B", Value = "2" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form C", Value = "3" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form D", Value = "4" });
            //IEnumerable<GoalSettingModel> goalSettingsModelList = annualAppraisalDB.GetGoalSettingDataList(companyId, yearId, performanceGroups);
            
            IEnumerable<AnnualAppraisalProcessModel> annualAppraisalProcessList = annualAppraisalDB.GetAnnualAppraisalProcessList(companyId, yearId, isFormDCategory, performanceGroups, PeriodId,objUserContextViewModel.UserId);
            ViewBag.PerformanceGroup = performanceGroups;
            return PartialView("_AnnualAppraisalSettingFormGrid", annualAppraisalProcessList);
        }
        private OperationDetails Validate_SetAnnualAppraisalProcessData(IEnumerable<GoalSettingModel> goalformSettings, int PeriodId)
        {
            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            OperationDetails operationDetails = new OperationDetails();
            FormsDB formsDB = new FormsDB();
            DataTable dtResult = new DataTable();
            operationDetails = annualAppraisalDB.CheckAnnualAppraisalWorkFlowAllEmployee(goalformSettings, dtResult, PeriodId);

            return operationDetails;
        }
        public ActionResult SaveAnnualAppraisalProcessData(string jsonString, int PeriodId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            IEnumerable<GoalSettingModel> goalformSettings = JsonConvert.DeserializeObject<IEnumerable<GoalSettingModel>>(jsonString);
            IEnumerable<AnnualAppraisalProcessModel> annualAppraisalProcessSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualAppraisalProcessModel>>(jsonString);
            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            OperationDetails operationDetails = new OperationDetails();

            //*** Form Flow Check
            operationDetails = Validate_SetAnnualAppraisalProcessData(goalformSettings, PeriodId);
            if (operationDetails.Success == true)
            {
                try
                {
                    if (annualAppraisalProcessSettings == null || annualAppraisalProcessSettings.Count() == 0) //--***Danny 22 / 09 / 2019
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "You must Select/Edit atleaset one Annual Appraisal Setting.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {
                        operationDetails = annualAppraisalDB.SaveAnnualAppraisalProcessData(annualAppraisalProcessSettings, objUserContextViewModel.UserId, objUserContextViewModel.CompanyId, PeriodId);
                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Annual Appraisal Setting.";
                    operationDetails.CssClass = "error";
                }
            }
            else
            {
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public string OpenPage(int FormStatus)
        {
            string PageName = "";
            int formProcessID = GetFormProcessSessionID();

            FormsDB FormsDB = new FormsDB();
            RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();

            annualAppraisalPrcessSetings = annualAppraisalDB.GetAnnualAppraisalProcessDataByFormProcessId(formProcessID, objRequestFormsProcessModel.FormID);
            //if (objRequestFormsProcessModel.FormID == 40) //*** Form A
            //{                
            if (annualAppraisalPrcessSetings.PerformanceGroupId == 1) //*** Form A
            {
                if (annualAppraisalPrcessSetings.PeriodId == 1) //*** Mid Year
                {
                    PageName = "MidAppraisalFormA";
                }
                else
                {
                    PageName = "FullAppraisalFormA";
                }

            }
            else if (annualAppraisalPrcessSetings.PerformanceGroupId == 2) //*** Form B
            {
                if (annualAppraisalPrcessSetings.PeriodId == 1) //*** Mid Year
                {
                    PageName = "MidAppraisalFormB";
                }
                else
                {
                    PageName = "FullAppraisalFormB";
                }

            }
            else if (annualAppraisalPrcessSetings.PerformanceGroupId == 3) //*** Form C
            {
                if (annualAppraisalPrcessSetings.PeriodId == 1) //*** Mid Year
                {
                    PageName = "MidAppraisalFormC";
                }
                else
                {
                    PageName = "FullAppraisalFormC";
                }

            }
            else if (annualAppraisalPrcessSetings.PerformanceGroupId == 4) //*** Form D
            {
                if (annualAppraisalPrcessSetings.PeriodId == 1) //*** Mid Year
                {
                    PageName = "MidAppraisalFormD";
                }
                else
                {
                    PageName = "FullAppraisalFormD";
                }

            }

            return PageName;
        }
        public ActionResult Edit()
        {
            //*** F3
            Session["MidEditOption"] = "Yes";
            return RedirectToAction(OpenPage(0));
        }


        public ActionResult ViewDetails()
        {
            //*** F3
            Session["MidEditOption"] = "No";
            return RedirectToAction(OpenPage(1));
        }
        #endregion


        #region Full year Appraisal Form

        public ActionResult FullAppraisalFormA()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.UserID = objUserContextViewModel.UserId;
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

                //*** F3
                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 42);


                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                //*** F3
                if (Session["MidEditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    /*** F21 Removed on Request <<<<<<<<<<<<<<<<<<<<<<<<<
                   goalSettingModel.IsAddMode = true;
                   //*** Re-Initialize option to only A1 user
                   ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                   if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                   {
                       if (objUserContextViewModel.UserId != ViewBag.A1User)
                       {
                           goalSettingModel.IsAddMode = false;
                       }
                   }
                    >>>>>>>>>>>>>>>>>>>>>>> */
                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }
                }
               //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

               GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormAModelDetails(formProcessID,goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,3);
               //*** F10
               goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
               goalSettingModel.MidReqStatusID = goalSettingModelEditData.MidReqStatusID;

               goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
               goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
               goalSettingModelEditData.YearId = goalSettingModel.YearId;
               ViewBag.CurrPeriod = 3; //*** 1=Goal, 2=Mid yera, 3=Fullyer
               if (goalSettingModelEditData.FormABusinessTargetsModel != null)
               {
                   goalSettingModel.FormABusinessTargetsModel = goalSettingModelEditData.FormABusinessTargetsModel;
                   goalSettingModel.FormABusinessTargetsModelList = goalSettingModelEditData.FormABusinessTargetsModelList;

                   goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                   goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;

                   goalSettingModel.FullFormMode = goalSettingModelEditData.FullFormMode;
                   goalSettingModel.IsFullSignOffForm = goalSettingModelEditData.IsFullSignOffForm;
                   goalSettingModel.OverallScoreFormula = goalSettingModelEditData.OverallScoreFormula;
               }
               else
                   goalSettingModel.FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();
               ViewBag.IsViewDetails = false;
               if (goalSettingModel.FormABusinessTargetsModel.FormState == null)
               {
                   goalSettingModel.FormABusinessTargetsModel.FormState = "";
               }
               goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
               goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

                //////***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                ////RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                ////goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //***F20, F21 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;

               //////////*** F3 Finding Employee Based Group
               ////////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
               ////////{
               ////////    goalSettingModel.EmployeeBasedGroup = true;
               ////////}
               ////////else
               ////////{
               ////////    //*** F15
               ////////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
               ////////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
               ////////}

               goalSettingModel = goalSettingDB.GetAppraisalNonTeachProficiencyLevel(goalSettingModel, goalSettingModel.YearId, goalSettingModel.CompanyId);
               goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
               goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

               if (goalSettingModel.ProfessionalCompetencies == null || goalSettingModel.ProfessionalCompetencies.Count == 0)
               {
                   ProfessionalCompetencies ProfessionalCompetencies;
                   goalSettingModel.ProfessionalCompetencies = new List<ProfessionalCompetencies>();

                   BehavioralCompetencies BehavioralCompetencies;
                   goalSettingModel.BehavioralCompetencies = new List<BehavioralCompetencies>();

                   for (int i = 0; i < 5; i++)
                   {
                       ProfessionalCompetencies = new ProfessionalCompetencies();
                       BehavioralCompetencies = new BehavioralCompetencies();
                       ProfessionalCompetencies.No = i + 1;
                       ProfessionalCompetencies.Score = "N/A";//*** F5
                       BehavioralCompetencies.No = i + 1;
                       BehavioralCompetencies.Score = "N/A";
                       goalSettingModel.ProfessionalCompetencies.Add(ProfessionalCompetencies);
                       goalSettingModel.BehavioralCompetencies.Add(BehavioralCompetencies);
                   }

               }

               //*** F5
               List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightagesList = new List<AppraisalNonTeachDetailsRatingScaleWeightages>();
               AppraisalNonTeachDetailsRatingScaleWeightages AppraisalNonTeachDetailsRatingScaleWeightages;
               for (int i = 0; i < goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New.Count; i++)
               {
                   AppraisalNonTeachDetailsRatingScaleWeightages = new AppraisalNonTeachDetailsRatingScaleWeightages();
                   AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleID = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages[i].RatingScaleID;
                   if (goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber == 0)
                   {
                       AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = "N/A";
                   }
                   else
                   {
                       AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber.ToString();
                   }

                   AppraisalNonTeachDetailsRatingScaleWeightagesList.Add(AppraisalNonTeachDetailsRatingScaleWeightages);

               }
               ViewBag.AppraisalNonTeachDetailsRatingScaleWeightagesList = new SelectList(AppraisalNonTeachDetailsRatingScaleWeightagesList.OrderBy(s => s.RatingScaleID), "RatingScaleNumber", "RatingScaleNumber");

                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormBKPIModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }
                goalSettingModel.IsFullyear = true;
                return View(goalSettingModel);
           }
       }

       public ActionResult FullAppraisalFormB()
       {
           UserContextViewModel objUserContextViewModel = new UserContextViewModel();
           objUserContextViewModel = (UserContextViewModel)Session["userContext"];
           ViewBag.UserID = objUserContextViewModel.UserId;
           GoalSettingDB goalSettingDB = new GoalSettingDB();
           int formProcessID = GetFormProcessSessionID();
           if (formProcessID == 0)
           {
               return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
           }
           else
           {
               FormsDB FormsDB = new FormsDB();
               RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

               //*** F3
               GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 42);


               goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
               goalSettingModel.CreatedOn = DateTime.Now.Date;
               //*** F3
               if (Session["MidEditOption"].ToString() == "No")
               {
                   goalSettingModel.IsAddMode = false;
               }
               else
               {
                    /*** F21 Removed on Request <<<<<<<<<<<<<<<<<<<<<<<<<
                   goalSettingModel.IsAddMode = true;
                   //*** Re-Initialize option to only A1 user
                   ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                   if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                   {
                       if (objUserContextViewModel.UserId != ViewBag.A1User)
                       {
                           goalSettingModel.IsAddMode = false;
                       }
                   }
                   >>>>>>>>>>>>>>>>>>>>>>> */
                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                   {
                       goalSettingModel.IsAddMode = false;

                   }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }

                }
               //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

               GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormBModelDetails(formProcessID,goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,3);
               //*** F10
               goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
               goalSettingModel.MidReqStatusID = goalSettingModelEditData.MidReqStatusID;


               goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
               goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
               goalSettingModelEditData.YearId = goalSettingModel.YearId;

               ViewBag.CurrPeriod = 3; //*** 1=Goal, 2=Mid yera, 3=Fullyer
               if (goalSettingModelEditData.FormBKPIModel != null)
               {
                   goalSettingModel.FormBKPIModel = goalSettingModelEditData.FormBKPIModel;
                   goalSettingModel.FormBKPIModelList = goalSettingModelEditData.FormBKPIModelList;

                   goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                   goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;

                   goalSettingModel.FullFormMode = goalSettingModelEditData.FullFormMode;
                   goalSettingModel.IsFullSignOffForm = goalSettingModelEditData.IsFullSignOffForm;
                   goalSettingModel.OverallScoreFormula = goalSettingModelEditData.OverallScoreFormula;
               }
               else
                   goalSettingModel.FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();
               ViewBag.IsViewDetails = false;
               if (goalSettingModel.FormBKPIModel.FormState == null)
               {
                   goalSettingModel.FormBKPIModel.FormState = "";
               }
               goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
               goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

                //////***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                ////RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                ////goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //***F20, F21 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;


               ////////*** F3 Finding Employee Based Group
               //////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
               //////{
               //////    goalSettingModel.EmployeeBasedGroup = true;
               //////}
               //////else
               //////{
               //////    //*** F15
               //////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
               //////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
               //////}

               goalSettingModel = goalSettingDB.GetAppraisalNonTeachProficiencyLevel(goalSettingModel, goalSettingModel.YearId, goalSettingModel.CompanyId);
               goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
               goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

               if (goalSettingModel.ProfessionalCompetencies == null || goalSettingModel.ProfessionalCompetencies.Count == 0)
               {
                   ProfessionalCompetencies ProfessionalCompetencies;
                   goalSettingModel.ProfessionalCompetencies = new List<ProfessionalCompetencies>();

                   BehavioralCompetencies BehavioralCompetencies;
                   goalSettingModel.BehavioralCompetencies = new List<BehavioralCompetencies>();

                   for (int i = 0; i < 5; i++)
                   {
                       ProfessionalCompetencies = new ProfessionalCompetencies();
                       BehavioralCompetencies = new BehavioralCompetencies();
                       ProfessionalCompetencies.No = i + 1;
                       ProfessionalCompetencies.Score = "N/A"; //*** F5
                       BehavioralCompetencies.No = i + 1;
                       BehavioralCompetencies.Score = "N/A";
                       goalSettingModel.ProfessionalCompetencies.Add(ProfessionalCompetencies);
                       goalSettingModel.BehavioralCompetencies.Add(BehavioralCompetencies);
                   }

               }

               //*** F5
               List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightagesList = new List<AppraisalNonTeachDetailsRatingScaleWeightages>();
               AppraisalNonTeachDetailsRatingScaleWeightages AppraisalNonTeachDetailsRatingScaleWeightages;
               for (int i = 0; i < goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New.Count; i++)
               {
                   AppraisalNonTeachDetailsRatingScaleWeightages = new AppraisalNonTeachDetailsRatingScaleWeightages();
                   AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleID = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages[i].RatingScaleID;
                   if (goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber == 0)
                   {
                       AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = "N/A";
                   }
                   else
                   {
                       AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber.ToString();
                   }

                   AppraisalNonTeachDetailsRatingScaleWeightagesList.Add(AppraisalNonTeachDetailsRatingScaleWeightages);

               }


               ViewBag.AppraisalNonTeachDetailsRatingScaleWeightagesList = new SelectList(AppraisalNonTeachDetailsRatingScaleWeightagesList.OrderBy(s => s.RatingScaleID), "RatingScaleNumber", "RatingScaleNumber");

                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormBKPIModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }
                goalSettingModel.IsFullyear = true;
                return View(goalSettingModel);
           }
       }
       public ActionResult FullAppraisalFormC()
       {
           UserContextViewModel objUserContextViewModel = new UserContextViewModel();
           objUserContextViewModel = (UserContextViewModel)Session["userContext"];
           ViewBag.UserID = objUserContextViewModel.UserId;
           GoalSettingDB goalSettingDB = new GoalSettingDB();
           int formProcessID = GetFormProcessSessionID();
           if (formProcessID == 0)
           {
               return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
           }
           else
           {
               FormsDB FormsDB = new FormsDB();
               RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

               //*** F3
               GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 42);


               goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
               goalSettingModel.CreatedOn = DateTime.Now.Date;
               //*** F3
               if (Session["MidEditOption"].ToString() == "No")
               {
                   goalSettingModel.IsAddMode = false;
               }
               else
               {
                    /*** F21 Removed on Request <<<<<<<<<<<<<<<<<<<<<<<<<
                   goalSettingModel.IsAddMode = true;
                   //*** Re-Initialize option to only A1 user
                   ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                   if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                   {
                       if (objUserContextViewModel.UserId != ViewBag.A1User)
                       {
                           goalSettingModel.IsAddMode = false;
                       }
                   }
                    >>>>>>>>>>>>>>>>>>>>>>> */
                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                   {
                       goalSettingModel.IsAddMode = false;

                   }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }
                }
               //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

               GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormCModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,3);
               //*** F10
               goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
               goalSettingModel.MidReqStatusID = goalSettingModelEditData.MidReqStatusID;

               goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
               goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
               goalSettingModelEditData.YearId = goalSettingModel.YearId;
               ViewBag.CurrPeriod = 3; //*** 1=Goal, 2=Mid yera, 3=Fullyer
               if (goalSettingModelEditData.FormCKPIModel != null)
               {
                   goalSettingModel.FormCKPIModel = goalSettingModelEditData.FormCKPIModel;
                   goalSettingModel.FormCKPIModelList = goalSettingModelEditData.FormCKPIModelList;                    

                   goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                   goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;

                   goalSettingModel.FullFormMode = goalSettingModelEditData.FullFormMode;
                   goalSettingModel.IsFullSignOffForm = goalSettingModelEditData.IsFullSignOffForm;
                   goalSettingModel.OverallScoreFormula = goalSettingModelEditData.OverallScoreFormula;
               }
               else
                   goalSettingModel.FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();
               ViewBag.IsViewDetails = false;
               if (goalSettingModel.FormCKPIModel.FormState == null)
               {
                   goalSettingModel.FormCKPIModel.FormState = "";
               }
               goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
               goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

               //////***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
               ////RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
               ////goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
               ////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
               //////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
               ////goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //***F20, F21 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;


               ////////*** F3 Finding Employee Based Group
               //////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
               //////{
               //////    goalSettingModel.EmployeeBasedGroup = true;
               //////}
               //////else
               //////{
               //////    //*** F15
               //////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
               //////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
               //////}

               goalSettingModel = goalSettingDB.GetAppraisalNonTeachProficiencyLevel(goalSettingModel, goalSettingModel.YearId, goalSettingModel.CompanyId);
               goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
               goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

               if (goalSettingModel.ProfessionalCompetencies == null || goalSettingModel.ProfessionalCompetencies.Count == 0)
               {
                   ProfessionalCompetencies ProfessionalCompetencies;
                   goalSettingModel.ProfessionalCompetencies = new List<ProfessionalCompetencies>();

                   BehavioralCompetencies BehavioralCompetencies;
                   goalSettingModel.BehavioralCompetencies = new List<BehavioralCompetencies>();

                   for (int i = 0; i < 5; i++)
                   {
                       ProfessionalCompetencies = new ProfessionalCompetencies();
                       BehavioralCompetencies = new BehavioralCompetencies();
                       ProfessionalCompetencies.No = i + 1;
                       ProfessionalCompetencies.Score = "N/A"; //*** F5
                       BehavioralCompetencies.No = i + 1;
                       BehavioralCompetencies.Score = "N/A";
                       goalSettingModel.ProfessionalCompetencies.Add(ProfessionalCompetencies);
                       goalSettingModel.BehavioralCompetencies.Add(BehavioralCompetencies);
                   }

               }
               //*** F5
               List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightagesList = new List<AppraisalNonTeachDetailsRatingScaleWeightages>();
               AppraisalNonTeachDetailsRatingScaleWeightages AppraisalNonTeachDetailsRatingScaleWeightages;
               for (int i = 0; i < goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New.Count; i++)
               {
                   AppraisalNonTeachDetailsRatingScaleWeightages = new AppraisalNonTeachDetailsRatingScaleWeightages();
                   AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleID = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages[i].RatingScaleID;
                   if (goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber == 0)
                   {
                       AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = "N/A";
                   }
                   else
                   {
                       AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber.ToString();
                   }

                   AppraisalNonTeachDetailsRatingScaleWeightagesList.Add(AppraisalNonTeachDetailsRatingScaleWeightages);

               }
               ViewBag.AppraisalNonTeachDetailsRatingScaleWeightagesList = new SelectList(AppraisalNonTeachDetailsRatingScaleWeightagesList.OrderBy(s => s.RatingScaleID), "RatingScaleNumber", "RatingScaleNumber");

                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormBKPIModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }
                goalSettingModel.IsFullyear = true;
                return View(goalSettingModel);
           }
       }

       public ActionResult FullAppraisalFormD()
       {
           UserContextViewModel objUserContextViewModel = new UserContextViewModel();
           objUserContextViewModel = (UserContextViewModel)Session["userContext"];
           ViewBag.UserID = objUserContextViewModel.UserId;
           GoalSettingDB goalSettingDB = new GoalSettingDB();
           int formProcessID = GetFormProcessSessionID();
           if (formProcessID == 0)
           {
               return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
           }
           else
           {
               FormsDB FormsDB = new FormsDB();
               RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

               //*** F3
               GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 47);


               goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
               goalSettingModel.CreatedOn = DateTime.Now.Date;
               //*** F3
               if (Session["MidEditOption"].ToString() == "No")
               {
                   goalSettingModel.IsAddMode = false;
               }
               else
               {
                   /*** F21 Removed on Request <<<<<<<<<<<<<<<<<<<<<<<<<
                   goalSettingModel.IsAddMode = true;
                   //*** Re-Initialize option to only A1 user
                   ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                   if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                   {
                       if (objUserContextViewModel.UserId != ViewBag.A1User)
                       {
                           goalSettingModel.IsAddMode = false;
                       }
                   }
                    >>>>>>>>>>>>>>>>>>>>>>> */

                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }

                }
                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormDModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,3);
                //*** F10
                goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
                goalSettingModel.MidReqStatusID = goalSettingModelEditData.MidReqStatusID;

                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 3; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormDModel != null)
                {
                    goalSettingModel.FormDModel = goalSettingModelEditData.FormDModel;
                    //goalSettingModel.FormDModelList = goalSettingModelEditData.FormDModelList;

                    goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                    goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;

                    goalSettingModel.FullFormMode = goalSettingModelEditData.FullFormMode;
                    goalSettingModel.IsFullSignOffForm = goalSettingModelEditData.IsFullSignOffForm;
                    goalSettingModel.OverallScoreFormula = goalSettingModelEditData.OverallScoreFormula;
                }
                else
                    goalSettingModel.FormDModel = new AnnualGoalSettingFormDModel();
                ViewBag.IsViewDetails = false;
                if (goalSettingModel.FormDModel.FormState == null)
                {
                    goalSettingModel.FormDModel.FormState = "";
                }
                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;


                //////***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                ////RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //////goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                ////goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //***F20, F21 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;

                //*** F22 Enabled On Request
                //*** F3 Finding Employee Based Group
                //*** F21 Commented
                //if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                //{
                //    goalSettingModel.EmployeeBasedGroup = true;
                //}
                //else
                //{
                //    //*** F15
                //    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                //    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                //}

                goalSettingModel = goalSettingDB.GetAppraisalNonTeachProficiencyLevel(goalSettingModel, goalSettingModel.YearId, goalSettingModel.CompanyId);
                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

                if (goalSettingModel.ProfessionalCompetencies == null || goalSettingModel.ProfessionalCompetencies.Count == 0)
                {
                    ProfessionalCompetencies ProfessionalCompetencies;
                    goalSettingModel.ProfessionalCompetencies = new List<ProfessionalCompetencies>();

                    BehavioralCompetencies BehavioralCompetencies;
                    goalSettingModel.BehavioralCompetencies = new List<BehavioralCompetencies>();

                    for (int i = 0; i < 10; i++)
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        BehavioralCompetencies = new BehavioralCompetencies();
                        ProfessionalCompetencies.No = i + 1;
                        ProfessionalCompetencies.Score = "N/A"; //*** F5
                        BehavioralCompetencies.No = i + 1;
                        BehavioralCompetencies.Score = "N/A";
                        goalSettingModel.ProfessionalCompetencies.Add(ProfessionalCompetencies);
                        //goalSettingModel.BehavioralCompetencies.Add(BehavioralCompetencies);
                    }

                }
                //*** F5
                List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightagesList = new List<AppraisalNonTeachDetailsRatingScaleWeightages>();
                AppraisalNonTeachDetailsRatingScaleWeightages AppraisalNonTeachDetailsRatingScaleWeightages;
                for (int i = 0; i < goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New.Count; i++)
                {
                    AppraisalNonTeachDetailsRatingScaleWeightages = new AppraisalNonTeachDetailsRatingScaleWeightages();
                    AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleID = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages[i].RatingScaleID;
                    if (goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber == 0)
                    {
                        AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = "N/A";
                    }
                    else
                    {
                        AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber.ToString();
                    }

                    AppraisalNonTeachDetailsRatingScaleWeightagesList.Add(AppraisalNonTeachDetailsRatingScaleWeightages);

                }
                ViewBag.AppraisalNonTeachDetailsRatingScaleWeightagesList = new SelectList(AppraisalNonTeachDetailsRatingScaleWeightagesList.OrderBy(s => s.RatingScaleID), "RatingScaleNumber", "RatingScaleNumber");

                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormBKPIModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }

                goalSettingModel.IsFullyear = true;
                return View(goalSettingModel);
            }
        }

        public JsonResult CalculateFullScore(float Scorevalues1, float Scorevalues2, int FormulaYearID, int PerformanceGroupId)
        {

            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            int FixedInt = 500;
            int FixedDiv = 5;
            int result = 0;
            //int Count = Scorevalues1 == null ? 0 : Scorevalues1.Count();
            PDRPFormulasTotalScore PDRPFormulasTotalScore = new PDRPFormulasTotalScore();
            string FormType = "";
            if (PerformanceGroupId == 1)
            {
                FormType = "NonTeachFormA";
            }
            else if (PerformanceGroupId == 2)
            {
                FormType = "NonTeachFormB";
            }
            else if (PerformanceGroupId == 3)
            {
                FormType = "NonTeachFormC";
            }
            PDRPFormulasTotalScore = annualAppraisalDB.GetPDRPAnualFormulaCalculations(Scorevalues1, Scorevalues2, FormType, FormulaYearID);



            return Json(new { Formula = PDRPFormulasTotalScore.Formula, Total = PDRPFormulasTotalScore.Total, OverallScore = Math.Round(PDRPFormulasTotalScore.TotalScore, 2) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveFullYearAppraisalFormData(string model, int PerformanceGroupId)
        {
            
            OperationDetails operationDetails = new OperationDetails();
            try
            {

                int formProcessID = GetFormProcessSessionID();
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                AnnualAppraisalProcessModel AnnualAppraisalProcessModel = JsonConvert.DeserializeObject<AnnualAppraisalProcessModel>(model);
                AnnualAppraisalProcessModel.MidYearAppraisalFormModel = JsonConvert.DeserializeObject<MidYearAppraisalForm>(model);
                IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings = JsonConvert.DeserializeObject<IEnumerable<BusinessTargetsMidYearAppraisal>>(AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JsonString);

                List<ProfessionalCompetencies> ProfessionalCompetencies = JsonConvert.DeserializeObject<List<ProfessionalCompetencies>>(AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JsonString1);
                
                AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
                //OperationDetails operationDetails = new OperationDetails();

                if (businessTargetMidYearAppraisalSettings == null)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "You must add at least one target or weight details.";
                    operationDetails.CssClass = "error";
                }
                else
                {
                    if (PerformanceGroupId == 1)
                    {
                        List<BehavioralCompetencies> BehavioralCompetencies = JsonConvert.DeserializeObject<List<BehavioralCompetencies>>(AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JsonString2);
                        operationDetails = annualAppraisalDB.SaveFullYearAppraisalFormAData(objRequestFormsProcessModel.RequestID, businessTargetMidYearAppraisalSettings, ProfessionalCompetencies, BehavioralCompetencies, objUserContextViewModel.UserId, AnnualAppraisalProcessModel,42);
                    }
                    else if (PerformanceGroupId == 2)
                    {
                        List<BehavioralCompetencies> BehavioralCompetencies = JsonConvert.DeserializeObject<List<BehavioralCompetencies>>(AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JsonString2);
                        operationDetails = annualAppraisalDB.SaveFullYearAppraisalFormBData(objRequestFormsProcessModel.RequestID, businessTargetMidYearAppraisalSettings, ProfessionalCompetencies, BehavioralCompetencies, objUserContextViewModel.UserId, AnnualAppraisalProcessModel,42);
                    }
                    else if (PerformanceGroupId == 3)
                    {
                        List<BehavioralCompetencies> BehavioralCompetencies = JsonConvert.DeserializeObject<List<BehavioralCompetencies>>(AnnualAppraisalProcessModel.MidYearAppraisalFormModel.JsonString2);
                        operationDetails = annualAppraisalDB.SaveFullYearAppraisalFormCData(objRequestFormsProcessModel.RequestID, businessTargetMidYearAppraisalSettings, ProfessionalCompetencies, BehavioralCompetencies, objUserContextViewModel.UserId, AnnualAppraisalProcessModel,42);
                    }
                    else if (PerformanceGroupId == 4)
                    {

                        operationDetails = annualAppraisalDB.SaveFullYearAppraisalFormDData(objRequestFormsProcessModel.RequestID, businessTargetMidYearAppraisalSettings, ProfessionalCompetencies,  objUserContextViewModel.UserId, AnnualAppraisalProcessModel,47);
                    }
                }

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReintializeFullYear(int FullYearFormId, int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            FormsDB formsDB = new FormsDB();
            if (employeeId > 0)
            {

                GoalSettingDB goalSettingDB = new GoalSettingDB();
                AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                try
                {
                    if (id == null || employeeId == null)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Invalid Input.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {

                        int formProcessID = GetFormProcessSessionID();
                        //goalSettingDB.SaveLog(id, objUserContextViewModel.UserId, performanceGroupId);
                        //AnnualAppraisalProcessDB.SaveLog(MIdYearFormId, objUserContextViewModel.UserId, performanceGroupId);
                        AnnualAppraisalProcessDB.SaveLogFull(FullYearFormId, objUserContextViewModel.UserId, performanceGroupId);
                        operationDetails = AnnualAppraisalProcessDB.ReintializeFullYear(goalSettingId, employeeId, formProcessID, comments);

                        if (operationDetails.InsertedRowId > 0)
                        {
                            //operationDetails = SubmitForm(formProcessID, comments);


                            if (operationDetails.Success == true)
                            {
                                //*** F22 Email to A1 
                                if (operationDetails.InsertedRowId == (int)RequestStatus.Pending)//*** Status need to be pending here Danny 21/12/2019
                                {
                                    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                    {
                                        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                        operationDetails.Message += @" & Email send to next approver";
                                    }
                                }
                                //ReqStatusID is 4/Completed Final Approval is done
                                if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                                {
                                    //*** New=================================================================================
                                    operationDetails = AnnualAppraisalProcessDB.FullFormStatUpdate(FullYearFormId, 2, performanceGroupId, formProcessID, employeeId);
                                    //operationDetails = TaskForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId,requestId);
                                    //List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                    //FormsDB formsDB = new FormsDB();
                                    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                    {
                                        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                        operationDetails.Message += @" & Email send to next approver";
                                    }
                                }
                                else
                                {
                                    //operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                                }
                            }


                        }
                        //0 means no more approval permission for this user /already approved or rejected
                        else if (operationDetails.InsertedRowId == 0)
                        {
                            operationDetails.CssClass = "error";
                            operationDetails.Success = false;

                        }
                        //Exception is hit at db level while saving approval operation
                        else
                        {
                            operationDetails.CssClass = "error";
                            operationDetails.Success = false;
                            operationDetails.Message = "Error while approving form request";
                        }

                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveFullForm(int FullYearFormId, int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (employeeId > 0)
            {

                GoalSettingDB goalSettingDB = new GoalSettingDB();
                AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                try
                {
                    if (id == null || employeeId == null)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Invalid Input.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {
                        //if (comments == null)
                        //{
                        //    comments = "Employee Submited. Created Task form A1";
                        //}
                        int formProcessID = GetFormProcessSessionID();
                        //goalSettingDB.SaveLog(id, objUserContextViewModel.UserId, performanceGroupId);
                        AnnualAppraisalProcessDB.SaveLogFull(FullYearFormId, objUserContextViewModel.UserId, performanceGroupId);

                        operationDetails = SubmitForm(formProcessID, comments);


                        if (operationDetails.Success == true)
                        {
                            //ReqStatusID is 4/Completed Final Approval is done
                            if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                            {
                                //*** New=================================================================================
                                operationDetails = AnnualAppraisalProcessDB.FullFormStatUpdate(FullYearFormId, 2, performanceGroupId, formProcessID, employeeId);
                                //operationDetails = TaskForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId,requestId);
                                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                FormsDB formsDB = new FormsDB();
                                requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                {
                                    SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                    operationDetails.Message += @" & Email send to next approver";
                                }
                            }
                            else
                            {
                                //operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                            }
                        }


                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubmitEmployeeSignOffFullYeraForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? goalSettingId, string comments, string MyThreeKeyAchivements, string MyTwoDevelopmentAreas, string EmployeeCommentsonFullYearPerformance)
        {
            //GoalSettingDB goalSettingDB = new GoalSettingDB();
            AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                operationDetails = AnnualAppraisalProcessDB.FullEmployeeSignOffForms(id, employeeId, formId, performanceGroupId, requestId, goalSettingId, comments, MyThreeKeyAchivements, MyTwoDevelopmentAreas, EmployeeCommentsonFullYearPerformance);


                //*** final email notification
                //*** ReqStatusID is 4/Completed Final Approval is done               
                //*** Getting the final email notification group's employee & email details
                FormsDB formsDB = new FormsDB();
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                int formProcessID = GetFormProcessSessionID();
                requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                requestFormsApproverEmailModelList = requestFormsApproverEmailModelList.Where(x => x.IsRequester != true).ToList();
                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                {
                    SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                    operationDetails.Message += @" & Final Email Notification send";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }







        private AnnualAppraisalProcessModel GetAppraisalFormDetails(AnnualAppraisalProcessModel annualAppraisalPrcessSetingModel)
        {
            //int formProcessID = GetFormProcessSessionID();
            //FormsDB FormsDB = new FormsDB();
            //RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            //annualAppraisalPrcessSetingModel = annualAppraisalDB.GetAnnualAppraisalProcessDataByFormProcessId(formProcessID, objRequestFormsProcessModel.FormID);
            //UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //annualAppraisalPrcessSetingModel.RequesterEmployeeId = objUserContextViewModel.UserId;
            //annualAppraisalPrcessSetingModel.CreatedOn = DateTime.Now.Date;
            //annualAppraisalPrcessSetingModel.IsAddMode = true;
            //annualAppraisalPrcessSetingModel.CompanyId = objUserContextViewModel.CompanyId;
            AnnualAppraisalProcessModel annualAppraisalprocessSettingEditModel = annualAppraisalDB.GetFull_MidYearAppraisalFormDetails(annualAppraisalPrcessSetingModel.EmployeeID, annualAppraisalPrcessSetingModel.PeriodId, annualAppraisalPrcessSetingModel.YearId, annualAppraisalPrcessSetingModel.RequestId);
            if (annualAppraisalprocessSettingEditModel.MidYearAppraisalFormModel != null)
            {
                annualAppraisalPrcessSetingModel.MidYearAppraisalFormModel = annualAppraisalprocessSettingEditModel.MidYearAppraisalFormModel;
                annualAppraisalPrcessSetingModel.BusinessTargetMidYearAppraisalList = annualAppraisalprocessSettingEditModel.BusinessTargetMidYearAppraisalList;
                annualAppraisalPrcessSetingModel.MidYearAppraisalDetailsModel = annualAppraisalprocessSettingEditModel.MidYearAppraisalDetailsModel;
                annualAppraisalPrcessSetingModel.BehavioralCompetencies = annualAppraisalprocessSettingEditModel.BehavioralCompetencies;
                annualAppraisalPrcessSetingModel.ProfessionalCompetencies = annualAppraisalprocessSettingEditModel.ProfessionalCompetencies;

            }
            else
                annualAppraisalPrcessSetingModel.MidYearAppraisalFormModel = new MidYearAppraisalForm();
            return annualAppraisalPrcessSetingModel;
        }    
       
        #endregion



        #region Mid year Appraisal Form
        public ActionResult MidAppraisalFormA()
        {


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);
                //*** F3
                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 41);

                ////GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID);


                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                //*** F3
                if (Session["MidEditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    goalSettingModel.IsAddMode = true;
                    //*** Re-Initialize option to only A1 user
                    ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                    if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                    {
                        if (objUserContextViewModel.UserId != ViewBag.A1User)
                        {
                            goalSettingModel.IsAddMode = false;
                        }
                    }

                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                    
                }
                


                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                //*** F3
                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormAModelDetails(formProcessID,goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,2);
                //*** F10
                goalSettingModel.IsReversed = goalSettingModelEditData.IsReversed;
                goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
                goalSettingModel.GolReqStatusID = goalSettingModelEditData.GolReqStatusID;

                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 2; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormABusinessTargetsModel != null)
                {
                    goalSettingModel.FormABusinessTargetsModel = goalSettingModelEditData.FormABusinessTargetsModel;
                    goalSettingModel.FormABusinessTargetsModelList = goalSettingModelEditData.FormABusinessTargetsModelList;
                    //*** F3
                    goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                    goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;
                }
                else
                    goalSettingModel.FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();
                ViewBag.IsViewDetails = false;
                if (goalSettingModel.FormABusinessTargetsModel.FormState == null)
                {
                    goalSettingModel.FormABusinessTargetsModel.FormState = "";
                }
                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);                
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;
                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;
                

                //*** F3 Finding Employee Based Group
                //if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                //{
                //    goalSettingModel.EmployeeBasedGroup = true;
                //}
                //else
                //{
                //    //*** F15
                //    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                //    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                //}
                //*** Naresh 2020-03-02 Populate A1 user to process for re-initialization mode
                //ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                ViewBag.UserID = objUserContextViewModel.UserId;
                return View(goalSettingModel);
            }
        }       

        public ActionResult MidAppraisalFormB()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);
                //*** F3
                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 41);
                ////GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID);


                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                //*** F3
                if (Session["MidEditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    goalSettingModel.IsAddMode = true;
                    //*** Re-Initialize option to only A1 user
                    ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                    if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                    {
                        if (objUserContextViewModel.UserId != ViewBag.A1User)
                        {
                            goalSettingModel.IsAddMode = false;
                        }
                    }

                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                }
                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormBModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,2);
                //*** F10
                goalSettingModel.IsReversed = goalSettingModelEditData.IsReversed;
                goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
                goalSettingModel.GolReqStatusID = goalSettingModelEditData.GolReqStatusID;


                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 2; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormBKPIModel != null)
                {
                    goalSettingModel.FormBKPIModel = goalSettingModelEditData.FormBKPIModel;
                    goalSettingModel.FormBKPIModelList = goalSettingModelEditData.FormBKPIModelList;
                    //*** F3
                    goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                    goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;
                }
                else
                    goalSettingModel.FormBKPIModel = new AnnualGoalSettingFormBModel();
                ViewBag.IsViewDetails = false;
                if (goalSettingModel.FormBKPIModel.FormState == null)
                {
                    goalSettingModel.FormBKPIModel.FormState = "";
                }
                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;
                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;

                ////*** F3 Finding Employee Based Group
                //if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                //{
                //    goalSettingModel.EmployeeBasedGroup = true;
                //}
                //else
                //{
                //    //*** F15
                //    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                //    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                //}
                ////*** Naresh 2020-03-02 Populate A1 user to process for re-initialization mode
                //ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                ViewBag.UserID = objUserContextViewModel.UserId;
                return View(goalSettingModel);
            }
        }       

        public ActionResult MidAppraisalFormC()
        {


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);
                //*** F3
                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 41);


                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                //*** F3
                if (Session["MidEditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    goalSettingModel.IsAddMode = true;
                    //*** Re-Initialize option to only A1 user
                    ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                    if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                    {
                        if (objUserContextViewModel.UserId != ViewBag.A1User)
                        {
                            goalSettingModel.IsAddMode = false;
                        }
                    }

                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                }
                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                //GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormCModelDetails2(goalSettingModel.EmployeeID);
                //*** F3
                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormCModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,2);
                //*** F10
                goalSettingModel.IsReversed = goalSettingModelEditData.IsReversed;
                goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
                goalSettingModel.GolReqStatusID = goalSettingModelEditData.GolReqStatusID;

                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 2; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormCKPIModel != null)
                {
                    goalSettingModel.FormCKPIModel = goalSettingModelEditData.FormCKPIModel;
                    goalSettingModel.FormCKPIModelList = goalSettingModelEditData.FormCKPIModelList;
                    //*** F3
                    goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                    goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;
                }
                else
                    goalSettingModel.FormCKPIModel = new AnnualGoalSettingFormCModel();
                ViewBag.IsViewDetails = false;
                if (goalSettingModel.FormCKPIModel.FormState == null)
                {
                    goalSettingModel.FormCKPIModel.FormState = "";
                }
                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;
                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;

                //////*** F3 Finding Employee Based Group
                ////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                ////{
                ////    goalSettingModel.EmployeeBasedGroup = true;
                ////}
                ////else
                ////{
                ////    //*** F15
                ////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////}
                //////*** Naresh 2020-03-02 Populate A1 user to process for re-initialization mode
                ////ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                ViewBag.UserID = objUserContextViewModel.UserId;
                return View(goalSettingModel);
            }
        }

        public ActionResult MidAppraisalFormD()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

                //*** F3
                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 46);


                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                //*** F3
                if (Session["MidEditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    goalSettingModel.IsAddMode = true;
                    //*** Re-Initialize option to only A1 user
                    ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                    if (goalSettingModel.ReqStatusId == 3) // to show Re-inisialize button to only A1 user
                    {
                        if (objUserContextViewModel.UserId != ViewBag.A1User)
                        {
                            goalSettingModel.IsAddMode = false;
                        }
                    }

                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                }
                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                //GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormDModelDetails(goalSettingModel.EmployeeID);
                //*** F3
                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormDModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,2);
                //*** F10
                goalSettingModel.IsReversed = goalSettingModelEditData.IsReversed;
                goalSettingModel.MidIsReversed = goalSettingModelEditData.MidIsReversed;
                goalSettingModel.GolReqStatusID = goalSettingModelEditData.GolReqStatusID;

                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 2; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormDModel != null)
                {
                    goalSettingModel.FormDModel = goalSettingModelEditData.FormDModel;
                    //goalSettingModel.FormCKPIModelList = goalSettingModelEditData.FormCKPIModelList;
                    //*** F3
                    goalSettingModel.MidFormMode = goalSettingModelEditData.MidFormMode;
                    goalSettingModel.IsMidSignOffForm = goalSettingModelEditData.IsMidSignOffForm;
                }
                else
                    goalSettingModel.FormDModel = new AnnualGoalSettingFormDModel();
                ViewBag.IsViewDetails = false;
                if (goalSettingModel.FormDModel.FormState == null)
                {
                    goalSettingModel.FormDModel.FormState = "";
                }
                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                //goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = requestFormsApproveModel.GroupID;
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;
                //*** These ViewBags need to remove and use goalSettingModel.PDRPA1UserAndCurrentUserDetails in future to minimize coding.
                ViewBag.A1User = goalSettingModel.PDRPA1UserAndCurrentUserDetails.A1User;

                ////////*** F3 Finding Employee Based Group
                //////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                //////{
                //////    goalSettingModel.EmployeeBasedGroup = true;
                //////}
                //////else
                //////{
                //////    //*** F15
                //////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                //////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                //////}

                goalSettingModel = goalSettingDB.GetAppraisalNonTeachProficiencyLevel(goalSettingModel, goalSettingModel.YearId, goalSettingModel.CompanyId);
                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

                if (goalSettingModel.ProfessionalCompetencies == null || goalSettingModel.ProfessionalCompetencies.Count == 0)
                {
                    ProfessionalCompetencies ProfessionalCompetencies;
                    goalSettingModel.ProfessionalCompetencies = new List<ProfessionalCompetencies>();

                    BehavioralCompetencies BehavioralCompetencies;
                    goalSettingModel.BehavioralCompetencies = new List<BehavioralCompetencies>();

                    for (int i = 0; i < 10; i++)
                    {
                        ProfessionalCompetencies = new ProfessionalCompetencies();
                        BehavioralCompetencies = new BehavioralCompetencies();
                        ProfessionalCompetencies.No = i + 1;
                        ProfessionalCompetencies.Score = "N/A"; //*** F5
                        BehavioralCompetencies.No = i + 1;
                        BehavioralCompetencies.Score = "N/A";
                        goalSettingModel.ProfessionalCompetencies.Add(ProfessionalCompetencies);
                        //goalSettingModel.BehavioralCompetencies.Add(BehavioralCompetencies);
                    }

                }
                //*** F5
                List<AppraisalNonTeachDetailsRatingScaleWeightages> AppraisalNonTeachDetailsRatingScaleWeightagesList = new List<AppraisalNonTeachDetailsRatingScaleWeightages>();
                AppraisalNonTeachDetailsRatingScaleWeightages AppraisalNonTeachDetailsRatingScaleWeightages;
                for (int i = 0; i < goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New.Count; i++)
                {
                    AppraisalNonTeachDetailsRatingScaleWeightages = new AppraisalNonTeachDetailsRatingScaleWeightages();
                    AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleID = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages[i].RatingScaleID;
                    if (goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber == 0)
                    {
                        AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = "N/A";
                    }
                    else
                    {
                        AppraisalNonTeachDetailsRatingScaleWeightages.RatingScaleNumber = goalSettingModel.AppraisalNonTeachDetailsRatingScaleWeightages_New[i].RatingScaleNumber.ToString();
                    }

                    AppraisalNonTeachDetailsRatingScaleWeightagesList.Add(AppraisalNonTeachDetailsRatingScaleWeightages);

                }



                ViewBag.AppraisalNonTeachDetailsRatingScaleWeightagesList = new SelectList(AppraisalNonTeachDetailsRatingScaleWeightagesList.OrderBy(s => s.RatingScaleID), "RatingScaleNumber", "RatingScaleNumber");
                //*** Naresh 2020-03-02 Populate A1 user to process for re-initialization mode
                //ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(formProcessID);
                ViewBag.UserID = objUserContextViewModel.UserId;
                return View(goalSettingModel);
            }
        }

        public ActionResult SaveAnnualMidFormCData(string model, int PerformanceGroupId)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //int formProcessID = GetFormProcessSessionID();
            //FormsDB FormsDB = new FormsDB();
            //RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);


            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                if (PerformanceGroupId == 1)
                {
                    AnnualGoalSettingFormAModel goalSettingFormCModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormAModel>(model);
                    IEnumerable<AnnualGoalSettingFormAModel> kpiFormCSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormAModel>>(goalSettingFormCModel.JsonString);

                    //*** F2
                    IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(goalSettingFormCModel.JsonStringCurrRatingScales);
                    operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormAData(kpiFormCSettings, GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, goalSettingFormCModel,40);
                }
                else if (PerformanceGroupId == 2)
                {
                    AnnualGoalSettingFormBModel goalSettingFormBModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormBModel>(model);
                    IEnumerable<AnnualGoalSettingFormBModel> kpiFormCSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormBModel>>(goalSettingFormBModel.JsonString);

                    //*** F2
                    IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(goalSettingFormBModel.JsonStringCurrRatingScales);
                    operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormBData(kpiFormCSettings, GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, goalSettingFormBModel, 40);
                }
                else if (PerformanceGroupId == 3)
                {
                    AnnualGoalSettingFormCModel goalSettingFormCModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormCModel>(model);
                    IEnumerable<AnnualGoalSettingFormCModel> kpiFormCSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormCModel>>(goalSettingFormCModel.JsonString);

                    //*** F2
                    IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(goalSettingFormCModel.JsonStringCurrRatingScales);
                    operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormCData(kpiFormCSettings, GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, goalSettingFormCModel, 40);
                }
                else if (PerformanceGroupId == 4)
                {
                    AnnualGoalSettingFormDModel goalSettingFormDModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormDModel>(model);
                    IEnumerable<AnnualGoalSettingFormDModel> kpiFormDSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormDModel>>(goalSettingFormDModel.JsonString);

                    //*** F2
                    IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(goalSettingFormDModel.JsonStringCurrRatingScales);
                    List<ProfessionalCompetencies> ProfessionalCompetencies = JsonConvert.DeserializeObject<List<ProfessionalCompetencies>>(goalSettingFormDModel.JsonString1);
                    operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormDData(GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, ProfessionalCompetencies, goalSettingFormDModel,46);
                }
                if (operationDetails.Success)
                {
                    int formProcessID = GetFormProcessSessionID();
                    FormsDB FormsDB = new FormsDB();
                    RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);
                    AnnualGoalSettingFormAModel businessTargetModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormAModel>(model);
                    IEnumerable<AnnualGoalSettingFormAModel> businessTargetSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormAModel>>(businessTargetModel.JsonString);
                    AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                    //OperationDetails operationDetails = new OperationDetails();
                    try
                    {
                        operationDetails = AnnualAppraisalProcessDB.SaveAnnualGoalSettingsFormCData(objRequestFormsProcessModel.RequestID, businessTargetSettings, objUserContextViewModel.UserId, businessTargetModel);
                    }
                    catch (Exception ex)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while adding Mid Year Appraisal.";
                        operationDetails.CssClass = "error";
                    }
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Mid Year Appraisal.";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Mid Year Appraisal.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);




            //UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //int formProcessID = GetFormProcessSessionID();
            //FormsDB FormsDB = new FormsDB();
            //RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);
            //AnnualGoalSettingFormAModel businessTargetModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormAModel>(model);
            //IEnumerable<AnnualGoalSettingFormAModel> businessTargetSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormAModel>>(businessTargetModel.JsonString);
            //AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
            ////OperationDetails operationDetails = new OperationDetails();
            //try
            //{
            //    if (businessTargetSettings == null)
            //    {
            //        operationDetails.Success = false;
            //        operationDetails.Message = "You must add at least one target or weight details.";
            //        operationDetails.CssClass = "error";
            //    }
            //    else
            //    {
            //        operationDetails = AnnualAppraisalProcessDB.SaveAnnualGoalSettingsFormCData(objRequestFormsProcessModel.RequestID, businessTargetSettings, objUserContextViewModel.UserId, businessTargetModel);

            //    }

            //}
            //catch (Exception ex)
            //{
            //    operationDetails.Success = false;
            //    operationDetails.Message = "Error occured while adding Goal Settings.";
            //    operationDetails.CssClass = "error";
            //}
            //return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]

        public ActionResult ApproveMidForm(int MIdYearFormId, int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (employeeId > 0)
            {

                GoalSettingDB goalSettingDB = new GoalSettingDB();
                AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                try
                {
                    if (id == null || employeeId == null)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Invalid Input.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {
                        if (comments == null)
                        {
                            comments = "Employee Submited. Created Task form A1";
                        }
                        int formProcessID = GetFormProcessSessionID();
                        goalSettingDB.SaveLog(id, objUserContextViewModel.UserId, performanceGroupId);
                        AnnualAppraisalProcessDB.SaveLog(MIdYearFormId, objUserContextViewModel.UserId, performanceGroupId);

                        operationDetails = SubmitForm(formProcessID, comments);


                        if (operationDetails.Success == true)
                        {
                            //ReqStatusID is 4/Completed Final Approval is done
                            if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                            {
                                //*** New=================================================================================
                                operationDetails = AnnualAppraisalProcessDB.FormStatUpdate(MIdYearFormId, 2, performanceGroupId, formProcessID, employeeId);
                                //operationDetails = TaskForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId,requestId);
                                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                FormsDB formsDB = new FormsDB();
                                requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                {
                                    SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                    operationDetails.Message += @" & Email send to next approver";
                                }
                            }
                            else
                            {
                                //operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                            }
                        }


                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //*** F3
        public ActionResult Reintialized(int MIdYearFormId, int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            FormsDB formsDB = new FormsDB();
            if (employeeId > 0)
            {

                GoalSettingDB goalSettingDB = new GoalSettingDB();
                AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                try
                {
                    if (id == null || employeeId == null)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Invalid Input.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {

                        int formProcessID = GetFormProcessSessionID();
                        goalSettingDB.SaveLog(id, objUserContextViewModel.UserId, performanceGroupId);
                        AnnualAppraisalProcessDB.SaveLog(MIdYearFormId, objUserContextViewModel.UserId, performanceGroupId);
                        operationDetails = AnnualAppraisalProcessDB.Reintialized(goalSettingId, employeeId, formProcessID);

                        if (operationDetails.InsertedRowId > 0)
                        {
                            operationDetails = SubmitForm(formProcessID, comments);


                            if (operationDetails.Success == true)
                            {
                                //ReqStatusID is 4/Completed Final Approval is done
                                if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                                {
                                    //*** New=================================================================================
                                    operationDetails = AnnualAppraisalProcessDB.FormStatUpdate(MIdYearFormId, 2, performanceGroupId, formProcessID, employeeId);
                                    //operationDetails = TaskForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId,requestId);
                                    //List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                    //FormsDB formsDB = new FormsDB();
                                    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                    {
                                        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                        operationDetails.Message += @" & Email send to next approver";
                                    }
                                }
                                else
                                {
                                    //operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                                }
                            }


                            //operationDetails.Success = true;
                            //operationDetails.CssClass = "success";
                            //If ReqStatus is approved, then only need to send next approval related emails
                            //if (operationDetails.InsertedRowId == (int)RequestStatus.Pending)//*** Status need to be pending here Danny 21/12/2019
                            //{
                            //    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                            //    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                            //    {
                            //        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                            //        operationDetails.Message += @" & Email send to next approver";
                            //    }
                            //}
                            ////ReqStatusID is 4/Completed Final Approval is done. This code may not reach at this satage so commenting
                            //if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                            //{
                            //    //*** New=================================================================================
                            //    operationDetails = goalSettingDB.FormStatUpdate(id, 2, performanceGroupId, formProcessID, employeeId);                               
                            //    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                            //    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                            //    {
                            //        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                            //        operationDetails.Message += @" & Email send to next approver";
                            //    }
                            //}
                            //else
                            //{
                            //    operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                            //}

                        }
                        //0 means no more approval permission for this user /already approved or rejected
                        else if (operationDetails.InsertedRowId == 0)
                        {
                            operationDetails.CssClass = "error";
                            operationDetails.Success = false;

                        }
                        //Exception is hit at db level while saving approval operation
                        else
                        {
                            operationDetails.CssClass = "error";
                            operationDetails.Success = false;
                            operationDetails.Message = "Error while approving form request";
                        }

                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        //*** F24 LM Comment and Employee Comment
        public ActionResult SubmitEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? goalSettingId, string comments,string EmployeeMidYearPerformanceComments)
        {
            //GoalSettingDB goalSettingDB = new GoalSettingDB();
            AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                operationDetails = AnnualAppraisalProcessDB.MidEmployeeSignOffForms(id, employeeId, formId, performanceGroupId, requestId, goalSettingId, comments, EmployeeMidYearPerformanceComments);


                //*** final email notification
                //*** ReqStatusID is 4/Completed Final Approval is done               
                //*** Getting the final email notification group's employee & email details
                FormsDB formsDB = new FormsDB();
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                int formProcessID = GetFormProcessSessionID();
                requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                requestFormsApproverEmailModelList = requestFormsApproverEmailModelList.Where(x => x.IsRequester != true).ToList();
                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                {
                    SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                    operationDetails.Message += @" & Final Email Notification send";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public OperationDetails SubmitForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            //return Json(operationDetails, JsonRequestBehavior.AllowGet);
            return operationDetails;
        }
       
       
        
        
        
        
       





        public ActionResult AnnualAppraisalMidFormALMSubmit(int? id, int? employeeId, int? formId, int? goalSettingId)
        {
            AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                if (id == null || employeeId == null)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "You must save at least one target or weight details first.";
                    operationDetails.CssClass = "error";
                }
                else
                {
                    operationDetails = AnnualAppraisalProcessDB.AnnualAppraisalMidFormALMSubmit(id, employeeId, formId, goalSettingId);

                }

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MidYearAppraisalForm()
        {
            AnnualAppraisalProcessModel annualAppraisalPrcessSetingModel = new AnnualAppraisalProcessModel();

            annualAppraisalPrcessSetingModel = GetAppraisalFormDetails(annualAppraisalPrcessSetingModel);

            return View(annualAppraisalPrcessSetingModel);
        }


        //[HttpPost]
        //public ActionResult SaveMidYearAppraisalFormData(string model)
        //{
        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    MidYearAppraisalForm midYearAppraisalFormModel = JsonConvert.DeserializeObject<MidYearAppraisalForm>(model);
        //    IEnumerable<BusinessTargetsMidYearAppraisal> businessTargetMidYearAppraisalSettings = JsonConvert.DeserializeObject<IEnumerable<BusinessTargetsMidYearAppraisal>>(midYearAppraisalFormModel.JsonString);
        //    AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
        //    OperationDetails operationDetails = new OperationDetails();
        //    try
        //    {
        //        if (businessTargetMidYearAppraisalSettings == null)
        //        {
        //            operationDetails.Success = false;
        //            operationDetails.Message = "You must add at least one target or weight details.";
        //            operationDetails.CssClass = "error";
        //        }
        //        else
        //        {
        //            operationDetails = annualAppraisalDB.SaveMidYearAppraisalFormData(businessTargetMidYearAppraisalSettings, objUserContextViewModel.UserId, midYearAppraisalFormModel);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        operationDetails.Success = false;
        //        operationDetails.Message = "Error occured while adding Goal Settings.";
        //        operationDetails.CssClass = "error";
        //    }
        //    return Json(operationDetails, JsonRequestBehavior.AllowGet);
        //}


        public ActionResult SaveFormApprovalForEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? annualAppraisalProcessId)
        {
            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                if (id == null || employeeId == null)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "You must save at least one target or weight details first.";
                    operationDetails.CssClass = "error";
                }
                else
                {
                    operationDetails = annualAppraisalDB.SaveFormApprovalForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId, requestId, annualAppraisalProcessId);
                }

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

    }
}