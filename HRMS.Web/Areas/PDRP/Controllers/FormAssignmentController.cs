﻿using HRMS.DataAccess;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using HRMS.Web.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class FormAssignmentController : PDRPCommonController
    {
        // GET: PDRP/FormAssignment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FormAssignment()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            FormAssignmentDB formAssignmentDB = new FormAssignmentDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            ViewBag.Company = GetCompanySelectList();
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            //objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            var lineManagers = new List<Entities.Employee>();
            ViewBag.LineManager = new SelectList(lineManagers, "EmployeeID", "FullName");
            return View();
        }

        public ActionResult GetPDRPFormAssignmentDataList(int? companyId, int? departmentId, int? lineManagerId, int? performanceGroupId)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            FormAssignmentDB formAssignmentDB = new FormAssignmentDB();
            List<SelectListItem> performanceGroups = new List<SelectListItem>().ToList();
            if (departmentId.HasValue && departmentId.Value == 0) departmentId = null;
            if (lineManagerId.HasValue && lineManagerId.Value == 0) lineManagerId = null;
            //performanceGroups.Add(new SelectListItem() { Text = "Select", Value = "0" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form A", Value = "1" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form B", Value = "2" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form C", Value = "3" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form D", Value = "4" });
            //ViewBag.PerformanceGroup = performanceGroups;
            IEnumerable<FormAssignmentModel> formAssignmentModelList = formAssignmentDB.GetPDRPFormAssignmentDataList(companyId, departmentId, lineManagerId, performanceGroupId, performanceGroups, objUserContextViewModel.UserId);
            //*** Naresh 2020-02-26 Displayed line manager with id and BU
            if (formAssignmentModelList.Any())
            {
                formAssignmentModelList.Where(a => a.LineManagerId > 0).ToList().ForEach(a => a.LineManager = (a.LineManagerId > 0 ? a.LineManagerId.ToString() + "-" : "") + a.LineManager + "-" + a.LMBUName);
            }
            ViewBag.PerformanceGroup = performanceGroups;
            return PartialView("_FormAssignmentGrid", formAssignmentModelList);
        }

        public ActionResult SaveFormAssignmentData(string jsonString)
        {
            IEnumerable<FormAssignmentModel> formGroupSettings = JsonConvert.DeserializeObject<IEnumerable<FormAssignmentModel>>(jsonString);
            FormAssignmentDB formAssignmentDB = new FormAssignmentDB();
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            try
            {
                if (formGroupSettings == null)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "You must Select/Edit atleaset one Performance Group.";
                    operationDetails.CssClass = "error";
                }
                else
                {
                    operationDetails = formAssignmentDB.SaveFormAssignmentData(formGroupSettings, objUserContextViewModel.UserId);

                }

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Form Assignment.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public override ActionResult GetEmployees(int companyId)
        {
            FormAssignmentDB formAssignmentDB = new FormAssignmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var employees = new List<Entities.Employee>();
            if (companyId > 0)
                employees = formAssignmentDB.GetLineManagerList(UserId: objUserContextViewModel.UserId, CompanyID: companyId);
            var result = employees.Select(d => new { id = d.EmployeeId, name = d.FullName }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}