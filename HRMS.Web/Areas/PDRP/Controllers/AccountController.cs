﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess.PDRP;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class AccountController : Controller
    {
        // GET: PDRP/Account
        public ActionResult LogOff()
        {
            ObservationsModelDB ObservationsModelDB = new ObservationsModelDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return RedirectToAction("LogOff", "Account", new { Area = "" });
        }
    }
}