﻿using HRMS.DataAccess;
using HRMS.DataAccess.PDRP;
using HRMS.Entities.PDRP;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System.Data;
using HRMS.Entities.Common;

namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class AppraisalTeachingController : PDRPCommonController
    {
        // GET: PDRP/AnnualAppraisalTeaching
        AppraisalTeachingModelDB AppraisalTeachingModelDB = new AppraisalTeachingModelDB();
        public ActionResult Index()
        {
            ViewBag.PDRPYears = new SelectList(new AcademicYearDB().GetAllAcademicYearLanguage().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            ViewBag.Company = GetCompanySelectList();// new SelectList(new CompanyDB().GetAllCompanyLanguage(), "CompanyId", "Name");
            return View();
        }
        private OperationDetails Validate_SetAppraisalVisitDateMulty(List<PDRPTeachersModel> SelectedTeachersList, PDRPTeachersModel SelectedTeacherl, int YearId)
        {

            AppraisalTeachingModelDB AppraisalTeachingModelDB = new AppraisalTeachingModelDB();
            OperationDetails operationDetails = new OperationDetails();
            FormsDB formsDB = new FormsDB();
            DataTable dtResult = new DataTable();
            operationDetails = AppraisalTeachingModelDB.CheckAnnualWorkFlowAllEmployee(SelectedTeachersList, dtResult);

            return operationDetails;
        }
        public ActionResult SetAppraisalVisitDateMulty(List<PDRPTeachersModel> SelectedTeachersList, PDRPTeachersModel SelectedTeacherl, int YearId)
        {
            OperationDetails operationDetails = new OperationDetails();
            //*** Form Flow Check
            operationDetails = Validate_SetAppraisalVisitDateMulty(SelectedTeachersList, SelectedTeacherl, YearId);
            if (operationDetails.Success == true)
            {

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                AppraisalTeachingModelDB AppraisalTeachingModelDB = new AppraisalTeachingModelDB();

                SelectedTeacherl.CreatedBy = objUserContextViewModel.UserId;
                SelectedTeacherl.CompanyId = objUserContextViewModel.CompanyId;

                DataTable dtResult = new DataTable();
                operationDetails = AppraisalTeachingModelDB.SetAppraisalVisitDateMulty(SelectedTeachersList, SelectedTeacherl, YearId, dtResult);
                if (operationDetails.Success == true)
                {
                    PassportWithdrawalRequestDB objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
                    //*** Sending Email to Requester and Receiver for Task creastion 
                    foreach (DataRow row in dtResult.Rows)
                    {


                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(row[0].ToString()), objUserContextViewModel.UserId);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                    }

                }

            }

            else
            {
                operationDetails.Success = false;
                operationDetails.CssClass = "error";
                operationDetails.Message = operationDetails.Message.ToString();

            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllTeachingEmployee(int AppraisalYear, string AppraisalYearTxt, int CompanyID)
        {
            List<PDRPTeachersModel> objObservationProcessTeachersList = new List<PDRPTeachersModel>();

            PDRPCommonModelDB PDRPCommonModelDB = new PDRPCommonModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            objObservationProcessTeachersList = PDRPCommonModelDB.GetAllApprovalEmployeeList(AppraisalYear, null, objUserContextViewModel.UserId, CompanyID);

            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            bool canViewEmployee = PermissionList.Where(x => x.OtherPermissionId == 25).Count() > 0 ? true : false;


            var vList = new object();
            vList = new
            {
                aaData = (from item in objObservationProcessTeachersList
                          where item.employeeDetailsModel.usertypeid == 1
                          select new
                          {
                              Default = "",
                              Select = item.Observation > 0 ? item.DropInsCount >= 3 ? item.VisitDate.Length != 0 ? "<input class='form-group' id='chkEmployeeId' name='chkEmployeeId' disabled style='margin-top:10px;' type='checkbox' value='true' checked>" : "<input class='form-group' id='chkEmployeeId' name='chkEmployeeId'  style='margin-top:10px;' type='checkbox' onChange='ChkChange(this)' value='true'>" : "<input class='form-group' id='chkEmployeeId' name='chkEmployeeId' disabled style='margin-top:10px;' type='checkbox' value='true'>" : "<input class='form-group' id='chkEmployeeId' name='chkEmployeeId' disabled style='margin-top:10px;' type='checkbox' value='true'>",
                              Observation = item.Observation > 0 ? "YES" : "NO",
                              DropInsCnt3 = item.DropInsCount >= 3 ? "YES" : "NO",
                              EmployeeId = "<input type='text' id='EmployeeID' class='form-control' value=" + item.EmployeeId + " />",
                              FirstName = canViewEmployee == false ? item.FirstName : item.FirstName,
                              LastName = item.LastName + " <label class=\"KPICount hide\">" + item.KPICount + "</label>",
                              EmployeeAlternativeID = item.employeeDetailsModel.EmployeeAlternativeID,
                              KPICount = " <label class=\"KPICount hide\">" + item.KPICount + "</label>",
                              DropInCount = " <label class=\"DropInCount hide\">" + item.DropInsCount + "</label>",
                              StartDate = item.Observation > 0 ? item.DropInsCount >= 3 ? item.VisitDate.Length == 0 ? "<div class='input-group' >" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true' onChange='DateAdd(this)' class='form-control pointer datepicker  tbdate' data-date-today-highlight='true'  readonly  yearRange: '1999:2012' id='txtEvlDate' name='txtEvlDate'  'type='text' value='" + item.VisitDate + "'></div>" : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true' class='form-control  '  disabled  yearRange: '1999:2012' id='txtEvlDate' name='txtEvlDate'  'type='text' value='" + item.VisitDate + "'></div>" : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true'  class='form-control '  disabled  id='txtEvlDate' name='txtEvlDate'  'type='text' value='" + item.VisitDate + "'></div>" : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true'  class='form-control '  disabled id='txtEvlDate' name='txtEvlDate'  'type='text' value='" + item.VisitDate + "'></div>",
                              DueDate = item.Observation > 0 ? item.DropInsCount >= 3 ? item.PreObservationDate.Length == 0 ? "<div class='input-group' >" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true' onChange='DateAdd(this)' class='form-control pointer datepicker tbdate EvlDate' data-date-today-highlight='true'  readonly yearRange: '1999:2012' id='txtEvlDueDate' name='txtEvlDueDate'  'type='text' value='" + item.PreObservationDate + "'></div>" : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true' class='form-control'  disabled yearRange: '1999:2012' id='txtEvlDueDate' name='txtEvlDueDate'  'type='text' value='" + item.PreObservationDate + "'></div> " : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true' class='form-control'  disabled yearRange: '1999:2012' id='txtEvlDueDate' name='txtEvlDueDate'  'type='text' value='" + item.PreObservationDate + "'></div> " : "<div class='input-group'>" +
                                            "<span class='input-group-addon add-on cal'><span class='fa fa-calendar'></span></span><input data-val='true' class='form-control'  disabled yearRange: '1999:2012' id='txtEvlDueDate' name='txtEvlDueDate'  'type='text' value='" + item.PreObservationDate + "'></div> ",

                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public string OpenPage(int InViewMode)
        {
            string PageName = "";
            FormsDB FormsDB = new FormsDB();
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);
            Session["PDRPAppraisalTeachingFormID"] = objRequestFormsProcessModel.FormID;
            Session["PreAppraisalTeachingRequestID"] = objRequestFormsProcessModel.RequestID;
            Session["InViewMode"] = InViewMode;
            if (objRequestFormsProcessModel.FormID == 34) //*** Pre-Observation 
            {
                Session["PDRPAppraisalTeachingID"] = objRequestFormsProcessModel.FormInstanceID;
                Session["PreAppraisalTeachingFormProcessID"] = objRequestFormsProcessModel.FormProcessID;
                PageName = "TeacherOverAllAppraisal";
            }
            else
            {

                PageName = "";
            }
            return PageName;
        }
        public ActionResult Edit()
        {
            return RedirectToAction(OpenPage(0));
        }
        public ActionResult ViewDetails()
        {

            return RedirectToAction(OpenPage(1));
        }
        public ActionResult TeacherOverAllAppraisal()
        {
            if (Session["PDRPAppraisalTeachingID"] != null)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.CompanyId = objUserContextViewModel.CompanyId;
                var CompanyId = objUserContextViewModel.CompanyId;


                AppraisalTeachingModel AppraisalTeachingModel = new AppraisalTeachingModel();

                AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID = Convert.ToInt32(Session["PDRPAppraisalTeachingID"] == null ? 0 : Session["PDRPAppraisalTeachingID"]);
                AppraisalTeachingModelDB AppraisalTeachingModelDB = new AppraisalTeachingModelDB();
                AppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestID = Session["PreAppraisalTeachingRequestID"] == null ? "1" : Session["PreAppraisalTeachingRequestID"].ToString();
                AppraisalTeachingModel.LoginID = objUserContextViewModel.UserId;
                AppraisalTeachingModel = AppraisalTeachingModelDB.GetAppraisalTeachingHeader(AppraisalTeachingModel);                
                AppraisalTeachingModel.AppraisalTeachingEmployeeModel.FormID = Convert.ToInt16(Session["PDRPAppraisalTeachingFormID"] == null ? 0 : Session["PDRPAppraisalTeachingFormID"]);
                int formProcessID = GetFormProcessSessionID();
                AppraisalTeachingModel.FormProcessID = formProcessID;
                AppraisalTeachingModel.InViewMode = Convert.ToInt16(Session["InViewMode"] == null ? 1 : Session["InViewMode"]);;

                ViewBag.A1User= AppraisalTeachingModelDB.GetNextApproverGroupAndEmployee(formProcessID);
                //*** F3 Finding Employee Based Group
                //Nithin 12-02-2020 , commented, if condition not required
               // if (objUserContextViewModel.UserId == AppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID)
                //{
                   // AppraisalTeachingModel.EmployeeBasedGroup = true;
                //}
               // else
                //{
                    //*** F15
                    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                    AppraisalTeachingModel.EmployeeBasedGroup = AppraisalTeachingModelDB.FindEmployeeBasedGroup(AppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ReqStatusID, formProcessID);

                //}

                if (AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ReqStatusID == 3) // to show Re-inisialize button to only A1 user
                {
                    if (objUserContextViewModel.UserId == ViewBag.A1User)
                    {
                        AppraisalTeachingModel.InViewMode = 0;
                        AppraisalTeachingModel.ApproverEmployeeID = ViewBag.A1User;
                    }
                    else
                    {
                        AppraisalTeachingModel.InViewMode = 1;
                    }
                }
                else
                {
                    AppraisalTeachingModel.InViewMode = 0;

                }
                return View(AppraisalTeachingModel);

            }
            else
            {
                return RedirectToAction("Index", "FormsTaskList", new { Area = "" });
            }
        }
        public ActionResult GetAppraisalTeachingEmployeeInformation(Int16 formID, AppraisalTeachingModel AppraisalTeachingModel)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLowerInvariant();
            AppraisalTeachingModel = AppraisalTeachingModelDB.GetAppraisalTeachingEmployeeInformation(AppraisalTeachingModel,culture);
            //if (ObservationsModel.isAddMode == "2") //*** Edit
            //{
            //ObservationsModel = ObservationsModelDB.GetObservationsEmployeeDetails(ObservationsModel);
            //}
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestDate = DateTime.Now;
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.FormID = formID;
            //DropInsModel.DropInEmployeeModel.DropinYear = Session["DropInYear"].ToString();

            //DropInManageModel.PDRPEmployeeModel = PDRPEmployeeModel;

            return PartialView("_TeacherOverAllAppraisal", AppraisalTeachingModel);

        }
        public ActionResult GetAppraisalTeachingKPIs(Int16 formID, AppraisalTeachingModel AppraisalTeachingModel)
        {
            ViewBag.comapanyId = AppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID;


            AppraisalTeachingModel = LoadKPIs(AppraisalTeachingModel);

            return PartialView("_YearlyKPIs", AppraisalTeachingModel);
        }
        public ActionResult GetAppraisalSections(Int16 formID, AppraisalTeachingModel AppraisalTeachingModel)
        {
            //////*** F5
            ////ViewBag.AppraisalKPIYears = new SelectList(LoadRatings(AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            ViewBag.AppraisalKPIYears = new SelectList(AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages.Where(m => m.RatingScaleNumber != 0).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            return PartialView("_Sections", AppraisalTeachingModel);
        }
        //*** F5
        public List<DropInsRatingScaleWeightagesNA> LoadRatings(List<AppraisalTeachingRatingScaleWeightages> AppraisalTeachingRatingScaleWeightages)
        {
            
            DropInsModel DropInsModel = new DropInsModel();
            List<DropInsRatingScaleWeightagesNA> DropInsRatingScaleWeightagesNAList = new List<DropInsRatingScaleWeightagesNA>();
            DropInsRatingScaleWeightagesNA DropInsRatingScaleWeightagesNA;
            for (int i = 0; i < AppraisalTeachingRatingScaleWeightages.Count; i++)
            {
                DropInsRatingScaleWeightagesNA = new DropInsRatingScaleWeightagesNA();
                DropInsRatingScaleWeightagesNA.RatingScaleID = AppraisalTeachingRatingScaleWeightages[i].RatingScaleID;
                if (AppraisalTeachingRatingScaleWeightages[i].RatingScaleNumber == 0)
                {
                    DropInsRatingScaleWeightagesNA.RatingScaleNumber = "N/A";
                }
                else
                {
                    DropInsRatingScaleWeightagesNA.RatingScaleNumber = AppraisalTeachingRatingScaleWeightages[i].RatingScaleNumber.ToString();
                }

                DropInsRatingScaleWeightagesNAList.Add(DropInsRatingScaleWeightagesNA);

            }


            //ViewBag.ObservationsRatingScaleWeightages = new SelectList(DropInsRatingScaleWeightagesNAList.OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            return DropInsRatingScaleWeightagesNAList;
        }
        public AppraisalTeachingModel LoadKPIs(AppraisalTeachingModel AppraisalTeachingModel)
        {

            //KPIsListModel model = new KPIsListModel();
            //List<SelectListItem> ObjSelectedList = new List<SelectListItem>();


            string KPIyear = AppraisalTeachingModelDB.GetAppraisalBeginYear(AppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYearID);
            AppraisalTeachingModel.AppraisalKPIsListModel = AppraisalTeachingModelDB.GetKPIsById(KPIyear, AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID, true, AppraisalTeachingModel.AppraisalKPIsListModel, AppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID);

            //////*** F5
            ////// ViewBag.ObservationsRatingScaleWeightages = new SelectList(DropInsRatingScaleWeightagesNAList.OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");

            ////ViewBag.AppraisalKPIYears = new SelectList(LoadRatings(AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            ViewBag.AppraisalKPIYears = new SelectList(AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages.Where(m=>m.RatingScaleNumber!=0).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");

            return AppraisalTeachingModel;
        }
        private string ValidateSave(AppraisalTeachingModel AppraisalTeachingModel)
        {
            string resultMessage = "";
            int isAddModeint =Convert.ToInt32(AppraisalTeachingModel.isAddMode);
            if (isAddModeint>1 && AppraisalTeachingModel.AppraisalKPIsListModel.TotalKPIScore == 0)
            {
                resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("PleaseselectKPIscore");
            }
            else if(AppraisalTeachingModel.FormMode == 2 && AppraisalTeachingModel.AppraisalKPIsListModel.TotalKPIScore == 0)
            {
                resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("PleaseselectKPIscore");
            }
            else
            {
                foreach (AppraisalKPIsModel s in AppraisalTeachingModel.AppraisalKPIsListModel.AppraisalKPIsModel)
                {
                    //*** F5
                    if ( isAddModeint>1 && s.KPIWeightage > 0 && (s.Ratings == null || s.Ratings == 0 || s.Score == 0))
                    {
                        resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("PleaseselectAllKPIscore");
                    }
                    //*** F5
                    if (AppraisalTeachingModel.FormMode == 2 && (s.Ratings == null || s.Ratings == 0 || s.Score == 0))
                    {
                        resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("PleaseselectAllKPIscore");
                    }
                }
            }

            if (isAddModeint > 1 && AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ThisYearScore == 0)
            {
                resultMessage = resultMessage + "</br>"+ PDRPAnnualAppraisalXMLResources.GetResource("TotalPerformance");
            }
            if (isAddModeint > 1 && AppraisalTeachingModel.TalentAreaImage == "0")
            {
                resultMessage = resultMessage + "</br>" + PDRPAnnualAppraisalXMLResources.GetResource("PleaseselectTalentArea");
            }
            return resultMessage;
        }
        [HttpPost]
        public JsonResult AppraisalTeachingFormSave(AppraisalTeachingModel AppraisalTeachingModel)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID = objUserContextViewModel.CompanyId;


            string result = "";
            string resultMessage = "";
            //*** Form Flow checking
            FormsDB formsDB = new FormsDB();
            OperationDetails operationDetails = new OperationDetails();
            //*** Is form flow exist check removed from hear on request
            if (AppraisalTeachingModel.FormMode < 10 ) //*** Check Form Submited/Saved/Reinitialized not
            {
                //*** FormMode=1 Form is saved
                //*** FormMode=2 Form is submited
                //*** FormMode=3 Final Approver Approved and assigned to Employee to Sign Off
                //*** FormMode=4 Employee to Sign Off
                //*** FormMode=6 Reinitialized
                //if (AppraisalTeachingModel.isAddMode == "5" && AppraisalTeachingModel.FormMode == 1)//*** Save Click 
                //{
                //    //** NO ACTION NEED 
                //    AppraisalTeachingModel.FormMode = 5;
                //    result = "success";
                //    resultMessage = "Teacher Over-All Appraisal Saved successful";
                //}
                //else
                //{
                    if ((AppraisalTeachingModel.isAddMode == "7" || AppraisalTeachingModel.isAddMode == "5") && AppraisalTeachingModel.FormMode == 2)//*** Submit Click Other than A1 User
                    {
                        AppraisalTeachingModel.FormMode = 6;
                    }


                    if (AppraisalTeachingModel.FormMode < 4) //*** Save not need on Employee Signing off
                    {
                        resultMessage = ValidateSave(AppraisalTeachingModel);
                    }
                    if (resultMessage.Length > 0)
                    {
                        result = "error";
                    }
                    else
                    {
                        try
                        {
                            int isAddModeint = Convert.ToInt32(AppraisalTeachingModel.isAddMode);
                        //*** Save

                        if (AppraisalTeachingModel.FormMode == 4) //*** F23 SignOff Employee. SigningOff Comment
                        {
                            AppraisalTeachingModel.Comments = "SignOff";
                        }
                            RequestFormsProcessModel objRequestFormsProcessModel = AppraisalTeachingModelDB.SaveAppraisalTeaching(AppraisalTeachingModel, 1);
                            PassportWithdrawalRequestDB objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
                            if (isAddModeint > 1 && AppraisalTeachingModel.TalentAreaImage == "0")
                            {
                                result = "error";
                                resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("PleaseselectTalentArea");
                            }
                            else if (objRequestFormsProcessModel != null && objRequestFormsProcessModel.RequestID > 0)
                            {
                                //*** Email to Requesster & to Receiver 
                                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(objRequestFormsProcessModel.FormProcessID);
                               



                                if (AppraisalTeachingModel.FormMode == 4) //*** SignOff Employee. Completed Final Approval is done
                                {
                                    //*** final email notification
                                    //*** ReqStatusID is 4/Completed Final Approval is done
                                    //if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                                    //}                                    //{
                                    //*** Getting the final email notification group's employee & email details
                                    requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(objRequestFormsProcessModel.FormProcessID);
                                    requestFormsApproverEmailModelList = requestFormsApproverEmailModelList.Where(x => x.IsRequester != true).ToList();
                                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                    {
                                        SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                                        operationDetails.Message += @" & "+ PDRPAnnualAppraisalXMLResources.GetResource("FinalEmail");
                                    }



                                }
                                else
                                {
                                    ////*** Email to next approver only  
                                    //RequestFormsApproverEmailModel requestFormsApproverEmailModelList1 = new RequestFormsApproverEmailModel();
                                    //requestFormsApproverEmailModelList1 = objPassportWithdrawalRequestDB.GetApproverEmail(objRequestFormsProcessModel.FormProcessID);
                                    //SendApproverEmail(requestFormsApproverEmailModelList1);
                                    SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                }

                                result = "success";
                                resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("TeacherSavedsuccessful");
                            }
                            else
                            {
                                if (objRequestFormsProcessModel.RequestID == -1)
                                {
                                    result = "error";
                                    resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("Noapprover");
                                }
                                else if (objRequestFormsProcessModel != null)
                                {
                                    result = "success";
                                    resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("TeacherSavedsuccessful");
                                }
                                else
                                {
                                    result = "error";
                                    resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("Erroroccured");
                                }

                            }

                            //result = "error";
                            //resultMessage = "Error occured while adding Observations.";
                        }
                        catch (Exception ex)
                        {
                            result = "error";
                            resultMessage = PDRPAnnualAppraisalXMLResources.GetResource("Erroroccured");
                    }
                    }
                //}
                
            }
            return Json(new { result = result, resultMessage = resultMessage, resultFormMode = AppraisalTeachingModel.FormMode.ToString() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CalculateKPIScore(IList<int> Scorevalues, IList<int> Weightages, int FormulaYearID)
        {
            //int FixedInt = 500;
            //int FixedDiv = 5;
            //int result = 0;
            //int Count =  Scorevalues == null ? 0 : Scorevalues.Count();
            PDRPFormulasTotalScore PDRPFormulasTotalScore = AppraisalTeachingModelDB.GetPDRPFormulaCalculations(Scorevalues, Weightages, "AppraisalTeachingSingleKPI", FormulaYearID);
            //PDRPFormulasTotalScore.TotalScore = PDRPFormulasTotalScore.TotalScore + Convert.ToDecimal(.5);

            return Json(new { ItemTotal = PDRPFormulasTotalScore.ItemTotal, Formula = PDRPFormulasTotalScore.Formula, Total = PDRPFormulasTotalScore.Total, OverallScore = Math.Round(PDRPFormulasTotalScore.TotalScore, 2) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetScoreSlab(decimal ThisYearScore)
        {
            string ScoreSlab = "";
            ScoreSlab = AppraisalTeachingModelDB.GetTeachingScoreSlab(ThisYearScore);
            //PDRPFormulasTotalScore.TotalScore = PDRPFormulasTotalScore.TotalScore + Convert.ToDecimal(.5);

            return Json(new { ScoreSlab = ScoreSlab }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        //*** F3
        public ActionResult Reintialized(int MIdYearFormId, int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}