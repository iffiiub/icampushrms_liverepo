﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using HRMS.Web.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class GoalSettingController : PDRPCommonController
    {
        #region Goal Setting
        // GET: PDRP/GoalSetting
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GoalSetting()
        {

            //*** F22 Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYearLanguage();
            int currentAcademicYear = listAcYear.Where(x => x.IsActive).FirstOrDefault().AcademicYearId;
            ViewBag.KPIYears = new SelectList(objAcademicYearDB.GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration", currentAcademicYear.ToString());
            //// ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");

            ViewBag.Company = GetCompanySelectListWithCurrentCompany();            
           
            //ViewBag.Company =  GetCompanySelectList();
            return View();
        }

        public ActionResult GetGoalSettingDataList(int? companyId, int? yearId)
        {
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<SelectListItem> performanceGroups = new List<SelectListItem>().ToList();
            //performanceGroups.Add(new SelectListItem() { Text = "Select", Value = "0" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form A", Value = "1" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form B", Value = "2" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form C", Value = "3" });
            //performanceGroups.Add(new SelectListItem() { Text = "Form D", Value = "4" });
            //ViewBag.PerformanceGroup = performanceGroups;
            IEnumerable<GoalSettingModel> goalSettingsModelList = goalSettingDB.GetGoalSettingDataList(companyId, yearId, performanceGroups, objUserContextViewModel.UserId);
            ViewBag.PerformanceGroup = performanceGroups;
            return PartialView("_GoalSettingFormGrid", goalSettingsModelList);
        }
        private OperationDetails Validate_SetAppraisalVisitDateMulty(IEnumerable<GoalSettingModel> goalformSettings)
        {

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            FormsDB formsDB = new FormsDB();
            DataTable dtResult = new DataTable();
            operationDetails = goalSettingDB.CheckGoalsettingWorkFlowAllEmployee(goalformSettings, dtResult,35);

            return operationDetails;
        }
        public ActionResult SaveGoalSettingsData(string jsonString)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            IEnumerable<GoalSettingModel> goalformSettings = JsonConvert.DeserializeObject<IEnumerable<GoalSettingModel>>(jsonString);
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();

            //*** Form Flow Check
            operationDetails = Validate_SetAppraisalVisitDateMulty(goalformSettings);

            if (operationDetails.Success == true)
            {
                try
                {
                    if (goalformSettings == null || goalformSettings.Count() == 0) //--***Danny 22 / 09 / 2019
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "You must Select/Edit atleaset one Goal Setting.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {
                        operationDetails = goalSettingDB.SaveGoalSettingsData(goalformSettings, objUserContextViewModel.UserId, objUserContextViewModel.CompanyId);

                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            else
            {               
                operationDetails.CssClass = "error";                
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OpenPage()
        {
            int formProcessID = GetFormProcessSessionID();
            

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID);

            if (goalSettingModel.PerformanceGroupId == 1)
                return RedirectToAction("AnnualGoalSettingFormA");
            else if (goalSettingModel.PerformanceGroupId == 2)
                return RedirectToAction("AnnualGoalSettingFormB");
            else if (goalSettingModel.PerformanceGroupId == 3)
                return RedirectToAction("AnnualGoalSettingFormC");
            return View();
        }
        public ActionResult Edit()
        {
            Session["EditOption"] = "Yes";
            return RedirectToAction("OpenPage");

            //int formProcessID = GetFormProcessSessionID();
            

            //GoalSettingDB goalSettingDB = new GoalSettingDB();
            //GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID);
            
            //if (goalSettingModel.PerformanceGroupId == 1)
            //    return RedirectToAction("AnnualGoalSettingFormA");
            //else if(goalSettingModel.PerformanceGroupId == 2)
            //    return RedirectToAction("AnnualGoalSettingFormB");
            //else if (goalSettingModel.PerformanceGroupId == 3)
            //    return RedirectToAction("AnnualGoalSettingFormC");
            //return View();
        }

        public ActionResult ViewDetails()
        {
            Session["EditOption"] = "No";
            return RedirectToAction("OpenPage");            
        }
        #endregion

        #region Annual Goal Setting Form A
        //***
        public ActionResult AnnualGoalSettingFormA()
        {
            

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList");
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 36);

               


                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.UserID = objUserContextViewModel.UserId;

                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                if (Session["EditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    //goalSettingModel.IsAddMode = true;
                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }

                }
                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormAModelDetails(formProcessID,goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,1);

                 goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 1; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormABusinessTargetsModel != null)
                {
                    goalSettingModel.FormABusinessTargetsModel = goalSettingModelEditData.FormABusinessTargetsModel;
                    goalSettingModel.FormABusinessTargetsModelList = goalSettingModelEditData.FormABusinessTargetsModelList;
                }
                else
                {
                    goalSettingModel.FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();
                }

               
                goalSettingModel.FormMode = goalSettingModelEditData.FormMode;


                ViewBag.IsViewDetails = false;

                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;
                if (goalSettingModel.FormABusinessTargetsModel.FormState == null)
                {
                    goalSettingModel.FormABusinessTargetsModel.FormState = "";
                }

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;


                //////*** Finding Employee Based Group
                ////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                ////{
                ////    goalSettingModel.EmployeeBasedGroup = true;
                ////}
                ////else
                ////{                   
                ////    //*** F15
                ////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////}

                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormABusinessTargetsModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }
                return View(goalSettingModel);

            }



        }

        [HttpPost]

        public ActionResult SaveAnnualGoalSettingsFormAData(string model)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AnnualGoalSettingFormAModel businessTargetModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormAModel>(model);
            IEnumerable<AnnualGoalSettingFormAModel> businessTargetSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormAModel>>(businessTargetModel.JsonString);

            //*** F2
            IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(businessTargetModel.JsonStringCurrRatingScales);

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                //*** F3
                //if (businessTargetSettings == null && Mode>0)
                //{
                //    operationDetails.Success = false;
                //    operationDetails.Message = "You must add at least one target or weight details.";
                //    operationDetails.CssClass = "error";
                //}
                //else
                //{
                    operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormAData(businessTargetSettings, GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, businessTargetModel,35);

                //}

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

       
        #endregion

        #region Annual Goal Setting Form B
        //***
        public ActionResult AnnualGoalSettingFormB()
        {
             

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList");
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID,37);

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.UserID = objUserContextViewModel.UserId;

                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                if (Session["EditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }
                       
                }                
                //goalSettingModel.CurrCompanyId = objUserContextViewModel.CompanyId;

                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormBModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,1);
                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;
                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 1; //*** 1=Goal, 2=Mid yera, 3=Fullyer

                if (goalSettingModelEditData.FormBKPIModel != null)
                {
                    goalSettingModel.FormBKPIModel = goalSettingModelEditData.FormBKPIModel;
                    goalSettingModel.FormBKPIModelList = goalSettingModelEditData.FormBKPIModelList;
                }
                else
                {
                    goalSettingModel.FormBKPIModel = new AnnualGoalSettingFormBModel();
                }
                goalSettingModel.FormMode = goalSettingModelEditData.FormMode;


                ViewBag.IsViewDetails = false;

                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;
                if (goalSettingModel.FormABusinessTargetsModel.FormState == null)
                {
                    goalSettingModel.FormABusinessTargetsModel.FormState = "";
                }

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;

                //////*** Finding Employee Based Group
                ////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                ////{
                ////    goalSettingModel.EmployeeBasedGroup = true;
                ////}
                ////else
                ////{
                ////    //*** F15
                ////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////}


                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormBKPIModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }

                return View(goalSettingModel);

            }



        }

        [HttpPost]
        public ActionResult SaveAnnualGoalSettingsFormBData(string model)
        {
           
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AnnualGoalSettingFormBModel goalSettingFormBModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormBModel>(model);
            IEnumerable<AnnualGoalSettingFormBModel> kpiFormBSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormBModel>>(goalSettingFormBModel.JsonString);
            
            //*** F2
            IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(goalSettingFormBModel.JsonStringCurrRatingScales);

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                //*** F3
                //if (kpiFormBSettings == null)
                //{
                //    operationDetails.Success = false;
                //    operationDetails.Message = "You must add at least one target or weight details.";
                //    operationDetails.CssClass = "error";
                //}
                //else
                //{
                operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormBData(kpiFormBSettings, GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, goalSettingFormBModel,35);

                //}

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
       
        
        #endregion

        #region Annunal Goal Setting Form C
        
        public ActionResult AnnualGoalSettingFormC()
        {


            GoalSettingDB goalSettingDB = new GoalSettingDB();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0)
            {
                return RedirectToAction("Index", "FormsTaskList");
            }
            else
            {
                FormsDB FormsDB = new FormsDB();
                RequestFormsProcessModel objRequestFormsProcessModel = FormsDB.GetRequestFormsProcess(formProcessID);

                GoalSettingModel goalSettingModel = goalSettingDB.GetGoalSettingFormDataByFormProcessId(formProcessID, 38);

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.UserID = objUserContextViewModel.UserId;

                goalSettingModel.RequesterEmployeeId = goalSettingModel.RequesterEmployeeId;
                goalSettingModel.CreatedOn = DateTime.Now.Date;
                if (Session["EditOption"].ToString() == "No")
                {
                    goalSettingModel.IsAddMode = false;
                }
                else
                {
                    //goalSettingModel.IsAddMode = true;
                    //*** F9
                    if (goalSettingModel.CompanyId != goalSettingModel.CurrCompanyId)
                    {
                        goalSettingModel.IsAddMode = false;

                    }
                    else
                    {
                        goalSettingModel.IsAddMode = true;
                    }

                }
                //goalSettingModel.CompanyId = objUserContextViewModel.CompanyId;

                GoalSettingModel goalSettingModelEditData = goalSettingDB.GetGoalSettingFormCModelDetails(formProcessID, goalSettingModel.EmployeeID, goalSettingModel.CompanyId, goalSettingModel.YearId,1);
                goalSettingModel.ProfessionalCompetencies = goalSettingModelEditData.ProfessionalCompetencies;
                goalSettingModel.BehavioralCompetencies = goalSettingModelEditData.BehavioralCompetencies;

                goalSettingModelEditData.YearId = goalSettingModel.YearId;
                ViewBag.CurrPeriod = 1; //*** 1=Goal, 2=Mid yera, 3=Fullyer
                if (goalSettingModelEditData.FormABusinessTargetsModel != null)
                {
                    goalSettingModel.FormCKPIModel = goalSettingModelEditData.FormCKPIModel;
                    goalSettingModel.FormCKPIModelList = goalSettingModelEditData.FormCKPIModelList;
                }
                else
                {
                    goalSettingModel.FormABusinessTargetsModel = new AnnualGoalSettingFormAModel();
                }


                goalSettingModel.FormMode = goalSettingModelEditData.FormMode;


                ViewBag.IsViewDetails = false;

                goalSettingModel.FormProcessID = objRequestFormsProcessModel.FormProcessID;
                goalSettingModel.RequestId = objRequestFormsProcessModel.RequestID;
                if (goalSettingModel.FormABusinessTargetsModel.FormState == null)
                {
                    goalSettingModel.FormABusinessTargetsModel.FormState = "";
                }

                //***F20 Reject Button remove for A1 but not for A3 if A1 & A3 are same users
                RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails = goalSettingDB.FindA1UserAndEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUser = objUserContextViewModel.UserId;
                if (goalSettingModel.ReqStatusId == 3 && objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                {
                    goalSettingModel.PDRPA1UserAndCurrentUserDetails.CurUserGroupID = 1;
                }
                goalSettingModel.EmployeeBasedGroup = goalSettingModel.PDRPA1UserAndCurrentUserDetails.EmployeeBasedGorup;
                //////*** Finding Employee Based Group
                ////if (objUserContextViewModel.UserId == goalSettingModel.EmployeeID)
                ////{
                ////    goalSettingModel.EmployeeBasedGroup = true;
                ////}
                ////else
                ////{
                ////    //*** F15
                ////    RequestFormsApproveModel requestFormsApproveModel = new FormsDB().GetPendingFormsApproval(formProcessID);
                ////    goalSettingModel.EmployeeBasedGroup = goalSettingDB.FindEmployeeBasedGroup(goalSettingModel.EmployeeID, objUserContextViewModel.UserId, requestFormsApproveModel.GroupID, goalSettingModel.ReqStatusId, goalSettingModel.FormProcessID);
                ////}

                //*** F12 Need to load Form Table comments at Employee Page. For all approvers load last comment from Approver History
                if (goalSettingModel.FormCKPIModel.ID > 0 && goalSettingModel.RequesterEmployeeId != ViewBag.UserID)
                {
                    goalSettingModel.Comments = "";
                }
                return View(goalSettingModel);

            }



        }
        [HttpPost]
       
        public ActionResult SaveAnnualGoalSettingsFormCData(string model)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AnnualGoalSettingFormCModel goalSettingFormCModel = JsonConvert.DeserializeObject<AnnualGoalSettingFormCModel>(model);
            IEnumerable<AnnualGoalSettingFormCModel> kpiFormCSettings = JsonConvert.DeserializeObject<IEnumerable<AnnualGoalSettingFormCModel>>(goalSettingFormCModel.JsonString);

            //*** F2
            IEnumerable<GoalSettingOverAllRatingScalesModel> GoalSettingOverAllRatingScalesModel = JsonConvert.DeserializeObject<IEnumerable<GoalSettingOverAllRatingScalesModel>>(goalSettingFormCModel.JsonStringCurrRatingScales);

            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                //*** F3
                //if (kpiFormCSettings == null)
                //{
                //    operationDetails.Success = false;
                //    operationDetails.Message = "You must add at least one target or weight details.";
                //    operationDetails.CssClass = "error";
                //}
                //else
                //{
                operationDetails = goalSettingDB.SaveAnnualGoalSettingsFormCData(kpiFormCSettings, GoalSettingOverAllRatingScalesModel, objUserContextViewModel.UserId, goalSettingFormCModel,35);

                //}

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveFormApprovalForLineManagerFormC(int? id, int? employeeId, int? formId, int? goalSettingId)
        {
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                if (id == null || employeeId == null)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "You must save at least one target or weight details first.";
                    operationDetails.CssClass = "error";
                }
                else
                {
                    operationDetails = goalSettingDB.SaveFormApprovalForGoalSettingFormsLineManager(id, employeeId, formId, goalSettingId);
                }

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public OperationDetails SubmitForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //////ReqStatusID is 4/Completed Final Approval is done
                    ////if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    ////{
                    ////    //*** OLD=================================================================================
                    ////    //Getting the final email notification group's employee & email details
                    ////    requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                    ////    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                    ////    {
                    ////        SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                    ////        operationDetails.Message += @" & Final Email Notification send";
                    ////    }

                    ////    //Add any logic to be done once approval process is completed
                    ////}

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            //return Json(operationDetails, JsonRequestBehavior.AllowGet);
            return operationDetails;
        }
        [HttpPost]
        //*** F3
        public ActionResult Reintialized(int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            FormsDB formsDB = new FormsDB();
            if (employeeId > 0)
            {

                GoalSettingDB goalSettingDB = new GoalSettingDB();

                try
                {
                    if (id == null || employeeId == null)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Invalid Input.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {
                        
                        int formProcessID = GetFormProcessSessionID();
                        goalSettingDB.SaveLog(id, objUserContextViewModel.UserId, performanceGroupId);
                        operationDetails =goalSettingDB.Reintialized(goalSettingId, employeeId, formProcessID, comments);

                        if (operationDetails.InsertedRowId > 0)
                        {
                            operationDetails.Success = true;
                            operationDetails.CssClass = "success";
                            //If ReqStatus is approved, then only need to send next approval related emails
                            if (operationDetails.InsertedRowId == (int)RequestStatus.Pending)//*** Status need to be pending here Danny 21/12/2019
                            {
                                requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                {
                                    SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                    operationDetails.Message += @" & Email send to next approver";
                                }
                            }
                            ////ReqStatusID is 4/Completed Final Approval is done. This code may not reach at this satage so commenting
                            //if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                            //{
                            //    //*** New=================================================================================
                            //    operationDetails = goalSettingDB.FormStatUpdate(id, 2, performanceGroupId, formProcessID, employeeId);                               
                            //    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                            //    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                            //    {
                            //        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                            //        operationDetails.Message += @" & Email send to next approver";
                            //    }
                            //}
                            //else
                            //{
                            //    operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                            //}

                        }
                        //0 means no more approval permission for this user /already approved or rejected
                        else if (operationDetails.InsertedRowId == 0)
                        {
                            operationDetails.CssClass = "error";
                            operationDetails.Success = false;

                        }
                        //Exception is hit at db level while saving approval operation
                        else
                        {
                            operationDetails.CssClass = "error";
                            operationDetails.Success = false;
                            operationDetails.Message = "Error while approving form request";
                        }

                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ApproveGoalForm(int? id, int? employeeId, int? formId, int? goalSettingId, int FormState, int? performanceGroupId, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (employeeId > 0)
            {

                GoalSettingDB goalSettingDB = new GoalSettingDB();

                try
                {
                    if (id == null || employeeId == null)
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Invalid Input.";
                        operationDetails.CssClass = "error";
                    }
                    else
                    {
                        if (comments == null)
                        {
                            comments = "Employee Submited. Created Task form A1";
                        }
                        int formProcessID = GetFormProcessSessionID();
                        goalSettingDB.SaveLog(id, objUserContextViewModel.UserId, performanceGroupId);
                        operationDetails = SubmitForm(formProcessID, comments);


                        if (operationDetails.Success == true)
                        {
                            //ReqStatusID is 4/Completed Final Approval is done
                            if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                            {
                                //*** New=================================================================================
                                operationDetails = goalSettingDB.FormStatUpdate(id, 2, performanceGroupId, formProcessID, employeeId);
                                //operationDetails = TaskForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId,requestId);
                                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                FormsDB formsDB = new FormsDB();
                                requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                                {
                                    SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                                    operationDetails.Message += @" & Email send to next approver";
                                }
                            }
                            else
                            {
                                operationDetails = goalSettingDB.FormStatUpdate(id, FormState, performanceGroupId, formProcessID, employeeId);
                            }
                        }
                       

                    }

                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Goal Settings.";
                    operationDetails.CssClass = "error";
                }
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        ////public OperationDetails TaskForEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId)
        ////{
        ////    GoalSettingDB goalSettingDB = new GoalSettingDB();
        ////    OperationDetails operationDetails = new OperationDetails();
        ////    try
        ////    {
        ////        if (id == null || employeeId == null)
        ////        {
        ////            operationDetails.Success = false;
        ////            operationDetails.Message = "You must save at least one target or weight details first.";
        ////            operationDetails.CssClass = "error";
        ////        }
        ////        else
        ////        {
        ////            operationDetails = goalSettingDB.TaskForEmployeeSignOffForms(id, employeeId, formId, performanceGroupId, requestId);                  
        ////        }

        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        operationDetails.Success = false;
        ////        operationDetails.Message = "Error occured while adding Goal Settings.";
        ////        operationDetails.CssClass = "error";
        ////    }
        ////    //return Json(operationDetails, JsonRequestBehavior.AllowGet);
        ////    return operationDetails;
        ////}

        public ActionResult SubmitEmployeeSignOffForms(int? id, int? employeeId, int? formId, int? performanceGroupId, int? requestId, int? goalSettingId, string comments)
        {
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                operationDetails = goalSettingDB.SubmitEmployeeSignOffForms(id, employeeId, formId, performanceGroupId, requestId, goalSettingId, comments);


                //*** final email notification
                //*** ReqStatusID is 4/Completed Final Approval is done               
                //*** Getting the final email notification group's employee & email details
                FormsDB formsDB = new FormsDB();
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                int formProcessID = GetFormProcessSessionID();
                requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                requestFormsApproverEmailModelList = requestFormsApproverEmailModelList.Where(x => x.IsRequester != true).ToList();
                if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                {
                    SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                    operationDetails.Message += @" & Final Email Notification send";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Goal Settings.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}