﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using HRMS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class PDRPCommonController : FormsController
    {
        // GET: PDRP
        PDRPCommonModelDB PDRPCommonModelDB = new PDRPCommonModelDB();
        //*** F10
        public ActionResult ReverseTask(int FormID, int FormInstanceID,int PerformanceGroupId)
        {
            AnnualAppraisalProcessDB annualAppraisalDB = new AnnualAppraisalProcessDB();
            List<SelectListItem> performanceGroups = new List<SelectListItem>().ToList();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails OperationDetails = annualAppraisalDB.ReverseTask(FormID, PerformanceGroupId, FormInstanceID, objUserContextViewModel.UserId);


            return Json(OperationDetails, JsonRequestBehavior.AllowGet); 
        }
        
        public ActionResult GetOverAllRatingScalesDetails(int YearID, int CompanyID, int GoalSettingId, int PerformanceGroupId)
        {
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            IEnumerable<GoalSettingOverAllRatingScalesModel> overAllRatingScalesList = goalSettingDB.GetOverAllRatingScalesDetails(YearID, CompanyID, GoalSettingId, PerformanceGroupId);
            return PartialView("~/Areas/PDRP/Views/Shared/_OverAllRatingScale.cshtml", overAllRatingScalesList);
        }

        public ActionResult GetRequesterEmployeeInfoDetails( GoalSettingModel GoalSettingModel, int RequestID, int YearID, Int16 formID, DateTime? requestDate, int employeeID, bool? isAddMode, int? formProcessId, string grade, int CurrPeriod)
        {
            ViewBag.CurrPeriod = CurrPeriod; //*** 1=Goal, 2=Mid yera, 3=Fullyer
            ApprovalRequesterInfo requesterInfoViewModel = PDRPCommonModelDB.GetRequesterEmployeeInfoDetailsForPDRPNonTeachers(GoalSettingModel, employeeID, isAddMode, formProcessId);

            requesterInfoViewModel.RequestDate = requestDate == null ? DateTime.Now : (DateTime)requestDate;
            requesterInfoViewModel.FormID = formID;
            requesterInfoViewModel.JobGrade = grade;
            requesterInfoViewModel.RequestID = RequestID.ToString();

            if (requesterInfoViewModel.AcademicYearID == 0)
            {
                string sTR = "SELECT AcademicYearName_1 Name FROM Gen_AcademicYear WHERE AcademicYearID=" + YearID.ToString();
                requesterInfoViewModel.AcademicYearName = PDRPCommonModelDB.GetYear(sTR);
            }
            if (formID == 36 && GoalSettingModel.FormABusinessTargetsModel.EmployeeName != null && GoalSettingModel.FormABusinessTargetsModel.EmployeeName.Length > 0)
            {
                GoalSettingModel.ApprovalRequesterInfo.RequesterName = GoalSettingModel.FormABusinessTargetsModel.EmployeeName;
                GoalSettingModel.ApprovalRequesterInfo.CompanyName = GoalSettingModel.FormABusinessTargetsModel.Company;
                GoalSettingModel.ApprovalRequesterInfo.PositionTitle = GoalSettingModel.FormABusinessTargetsModel.Designation;
                GoalSettingModel.ApprovalRequesterInfo.WorkEmail = GoalSettingModel.FormABusinessTargetsModel.RequestEmail;
                GoalSettingModel.ApprovalRequesterInfo.DepartmentName = GoalSettingModel.FormABusinessTargetsModel.Department;
                GoalSettingModel.ApprovalRequesterInfo.SuperviserName = GoalSettingModel.FormABusinessTargetsModel.LineManager;
                GoalSettingModel.ApprovalRequesterInfo.HireDate = Convert.ToDateTime(GoalSettingModel.FormABusinessTargetsModel.DOJ);
            }
            if (formID == 37 && GoalSettingModel.FormBKPIModel.EmployeeName != null && GoalSettingModel.FormBKPIModel.EmployeeName.Length > 0)
            {
                GoalSettingModel.ApprovalRequesterInfo.RequesterName = GoalSettingModel.FormBKPIModel.EmployeeName;
                GoalSettingModel.ApprovalRequesterInfo.CompanyName = GoalSettingModel.FormBKPIModel.Company;
                GoalSettingModel.ApprovalRequesterInfo.PositionTitle = GoalSettingModel.FormBKPIModel.Designation;
                GoalSettingModel.ApprovalRequesterInfo.WorkEmail = GoalSettingModel.FormBKPIModel.RequestEmail;
                GoalSettingModel.ApprovalRequesterInfo.DepartmentName = GoalSettingModel.FormBKPIModel.Department;
                GoalSettingModel.ApprovalRequesterInfo.SuperviserName = GoalSettingModel.FormBKPIModel.LineManager;
                GoalSettingModel.ApprovalRequesterInfo.HireDate = Convert.ToDateTime(GoalSettingModel.FormBKPIModel.DOJ);
            }
            if (formID == 38 && GoalSettingModel.FormCKPIModel.EmployeeName != null && GoalSettingModel.FormCKPIModel.EmployeeName.Length > 0)
            {
                GoalSettingModel.ApprovalRequesterInfo.RequesterName = GoalSettingModel.FormCKPIModel.EmployeeName;
                GoalSettingModel.ApprovalRequesterInfo.CompanyName = GoalSettingModel.FormCKPIModel.Company;
                GoalSettingModel.ApprovalRequesterInfo.PositionTitle = GoalSettingModel.FormCKPIModel.Designation;
                GoalSettingModel.ApprovalRequesterInfo.WorkEmail = GoalSettingModel.FormCKPIModel.RequestEmail;
                GoalSettingModel.ApprovalRequesterInfo.DepartmentName = GoalSettingModel.FormCKPIModel.Department;
                GoalSettingModel.ApprovalRequesterInfo.SuperviserName = GoalSettingModel.FormCKPIModel.LineManager;
                GoalSettingModel.ApprovalRequesterInfo.HireDate = Convert.ToDateTime(GoalSettingModel.FormCKPIModel.DOJ);
            }
            GoalSettingModel.ApprovalRequesterInfo = requesterInfoViewModel;
            //*** Naresh 2020-03-02 Populate A1 user to process for re-initialization mode
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            GoalSettingDB goalSettingDB = new GoalSettingDB();
            ViewBag.A1User = goalSettingDB.GetNextApproverGroupAndEmployee(GoalSettingModel.FormProcessID);
            ViewBag.UserID = objUserContextViewModel.UserId;

            return PartialView("~/Areas/PDRP/Views/Shared/_RequesterEmployeeInfoDetails.cshtml", GoalSettingModel);
        }
        [HttpPost]
        public override ActionResult RejectForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.RejectRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    //requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);                   
                    //*** Get A1's Email ID avoiding RequesterEmployee
                    RequestFormsProcessModel objRequestFormsProcessModel = formsDB.GetRequestFormsProcess(formProcessID);
                    AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                    requestFormsApproverEmailModelList = AnnualAppraisalProcessDB.GetFirstApproverEmailList(formProcessID, objRequestFormsProcessModel.RequestID);
                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                    {
                        SendRejectionEmail(requestFormsApproverEmailModelList);
                        operationDetails.Message += @" & Final Email Notification send";
                    }

                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while rejecting forms request";
                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }
        //*** F25 Peding task for Employee on Reject
        [HttpPost]
        public ActionResult RejectPDRPNonTeachForm(int formProcessID, string comments,int EmployeeID,int FormID,int FormInstanceID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                PDRPCommonModelDB PDRPCommonModelDB = new PDRPCommonModelDB();
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = PDRPCommonModelDB.RejectPDRPRequestForm(requestFormsApproveModel, EmployeeID,FormID, FormInstanceID);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    //requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);                   
                    //*** Get A1's Email ID avoiding RequesterEmployee
                    RequestFormsProcessModel objRequestFormsProcessModel = formsDB.GetRequestFormsProcess(formProcessID);
                    //AnnualAppraisalProcessDB AnnualAppraisalProcessDB = new AnnualAppraisalProcessDB();
                    //requestFormsApproverEmailModelList = AnnualAppraisalProcessDB.GetFirstApproverEmailList(formProcessID, objRequestFormsProcessModel.RequestID);
                    //if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                    //{
                    //    SendRejectionEmail(requestFormsApproverEmailModelList);
                    //    operationDetails.Message += @" & Final Email Notification send";
                    //}

                    //*** F25 Email on Pending task to Employee
                    requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                    {
                        SendApproverEmail(requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).ToList());
                        operationDetails.Message += @" & Email send to next approver";
                    }

                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while rejecting forms request";
                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }



    }
}