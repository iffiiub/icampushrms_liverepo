﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using HRMS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.Common;


namespace HRMS.Web.Areas.PDRP.Controllers
{
    public class KPIsController : PDRPCommonController
    {
        // GET: PDRP
        //*** KPIs=================================================================================================================
        public ActionResult GetYearlyKPIs(string kpiyear,int CompanyID)
        {
            if (kpiyear.IndexOf("-") > 0)
            {
                kpiyear = kpiyear.Substring(0, kpiyear.IndexOf("-"));
            }

            return PartialView("_YearlyKPIs", LoadKPIs(kpiyear, CompanyID));
        }
        // GET: PDRP
        public JsonResult KPISave(int CompanyID, int[] KPIOrder, string[] txtKPIAreaEng, string[] txtKPIDescriptionEng, string[] txtKPIAreaArab, string[] txtKPIDescriptionArab, string[] txtKPIWeightage, string KPIYear)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            string ddlKPIYear = KPIYear;
            string result = "";
            string resultMessage = "";

            try
            {

                if (txtKPIAreaEng == null || KPIYear == "" || CompanyID == 0)
                {
                    result = "error";
                    resultMessage = PDRPKPIFormResources.GetKPIFromResource("InvalidKPIData");
                }
                else
                { 
                    int Weightage = 0;
                    for (int i = 0; i < txtKPIAreaEng.Length; i++)
                    {
                        if (txtKPIAreaEng[i].Trim().Length == 0 &&
                            (txtKPIDescriptionEng[i].Trim().Length > 0 ||
                                txtKPIAreaArab[i].Trim().Length > 0 ||
                                txtKPIDescriptionArab[i].Trim().Length > 0))
                        {
                            result = "error";
                            resultMessage = PDRPKPIFormResources.GetKPIFromResource("Validation8") + (i + 1).ToString();
                            break;
                        }
                        else
                        {
                            if ((txtKPIAreaEng[i].Trim().Length == 0 && Convert.ToInt32(txtKPIWeightage[i]) > 0) ||
                                    (Convert.ToInt32(txtKPIWeightage[i]) == 0 && txtKPIAreaEng[i].Trim().Length > 0))
                            {
                                result = "error";
                                resultMessage = PDRPKPIFormResources.GetKPIFromResource("Validation8") + (i + 1).ToString();
                                break;
                            }
                            else
                            {
                                Weightage = Weightage + Convert.ToInt32(txtKPIWeightage[i]);

                            }
                        }
                    }


                    if (resultMessage.Length == 0)
                    {
                        if (Weightage != 100)
                        {
                            result = "error";
                            resultMessage = PDRPKPIFormResources.GetKPIFromResource("Validation9");
                        }
                        else
                        {
                            KPIsModuleDB KPIsModuleDB = new KPIsModuleDB();
                            if (!string.IsNullOrWhiteSpace(ddlKPIYear))
                            {
                                resultMessage = KPIsModuleDB.AddKPIs(CompanyID, KPIOrder.ToArray(), txtKPIAreaEng.ToArray(), txtKPIDescriptionEng.ToArray(), txtKPIAreaArab.ToArray(), txtKPIDescriptionArab.ToArray(), txtKPIWeightage.ToArray(), ddlKPIYear, objUserContextViewModel.UserId.ToString());
                            }

                            if (resultMessage == "SUCCESS")
                            {
                                result = "success";
                                resultMessage = PDRPKPIFormResources.GetKPIFromResource("Validation10"); 
                            }
                        }
                    }
                }
               
               
            }
            catch (Exception)
            {
                result = "error";
                resultMessage = PDRPKPIFormResources.GetKPIFromResource("Validation11"); 
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult YearlyKPIs(string kpiyear)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (objUserContextViewModel == null)
            {

            }
            ViewBag.comapanyId = objUserContextViewModel.CompanyId;
            var UserId = objUserContextViewModel.UserId;
            int CompanyId = objUserContextViewModel.CompanyId;


            KPIsListModel model = new KPIsListModel();
            model = LoadKPIs(kpiyear, CompanyId);
            //*** F8
            KPIsModuleDB KPIsModuleDB = new KPIsModuleDB();
            ViewBag.PDRPLanguageList = KPIsModuleDB.GetLanguageList(null);

            return View(model);
        }
        public KPIsListModel LoadKPIs(string kpiyear,int CompanyID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];           
            var UserId = objUserContextViewModel.UserId;
            //var CompanyId = objUserContextViewModel.CompanyId;

            //ViewBag.Company = new SelectList(new CompanyDB().GetAllCompanyLanguage(), "CompanyId", "Name");
            ViewBag.Company = GetCompanySelectList();
            KPIsListModel model = new KPIsListModel();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            KPIsModuleDB objKPIsDB = new KPIsModuleDB();
            
            model = objKPIsDB.GetKPIsById(kpiyear, true, CompanyID);
            //model.CompanyName = objKPIsDB.GetCompanyName(CompanyID);
            //model.CompanyID = CompanyID;
            
                       
            //*** On Request ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYear().Where(x => x.IsActive == true || x.IsNextYear == true).OrderByDescending(s => s.Duration), "Duration", "Duration");
            ViewBag.KPIYears = new SelectList(new AcademicYearDB().GetAllAcademicYearLanguage().OrderByDescending(s => s.Duration), "Duration", "Duration");

            return model;
        }
    }
}