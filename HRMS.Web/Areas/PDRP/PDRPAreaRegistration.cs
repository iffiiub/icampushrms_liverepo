﻿using System.Web.Mvc;

namespace HRMS.Web.Areas.PDRP
{
    public class PDRPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PDRP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PDRP_default",
                "PDRP/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}