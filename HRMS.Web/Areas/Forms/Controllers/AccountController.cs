﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Areas.Forms.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult LogOff()
        {
            return RedirectToAction("LogOff", "Account", new { Area = "" });
        }
    }
}