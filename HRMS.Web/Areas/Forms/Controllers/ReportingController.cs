﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using HRMS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Web.CommonHelper;
using System.Data;
using HRMS.Entities.PDRP;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;

namespace HRMS.Web.Areas.Forms.Controllers
{
    public partial class ReportingController : FormsController
    {
        // GET: Forms/Reporting      
        public ActionResult Index()
        {
            return View();
        }

        #region R1 Status Report
        public ActionResult R1StatusReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            //ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            return View();
        }
        public ActionResult R1StatusReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<R1StatusReportModel> listR1StatusReportModel = new RecruitR1BudgetedDB().GetAllR1StatusReportDetails(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listR1StatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              DepartmentName = item.DepartmentName,
                              Project = item.Project,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'" + item.UrlString + "')>" + item.RequestID.ToString() + "</a>",
                              Requester = item.Requester,
                              RequestDate = item.RequestDate,
                              Position = item.Position,
                              EmployeeJobCategory = item.EmployeeJobCategory,
                              HeadCount = item.HeadCount,
                              SalaryRanges = item.SalaryRanges,
                              ReplacementEmployee = item.ReplacementEmployee,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover,
                              IsBudgeted = item.IsBudgeted,
                              IsReplacement = item.IsReplacement
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelR1Status(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<R1StatusReportModel> listR1StatusReportModel = new RecruitR1BudgetedDB().GetAllR1StatusReportDetails(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listR1StatusReportModel);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/" + row["UrlString"] + "/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "R1StatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("ReqStatusID");
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns.Remove("UrlString");
            clonedTable.Columns["BU_Name"].ColumnName = "BU Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["RequestID"].ColumnName = "Request ID";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["HeadCount"].ColumnName = "Head Count";
            clonedTable.Columns["SalaryRanges"].ColumnName = "Salary Ranges";
            clonedTable.Columns["ReplacementEmployee"].ColumnName = "Replacement Employee";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            clonedTable.Columns["IsBudgeted"].ColumnName = "Budgeted";
            clonedTable.Columns["IsReplacement"].ColumnName = "Replacement";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["EmployeeJobCategory"].ColumnName = "Employee Job Category";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Compensatory Status Report
        public ActionResult CompensatoryStatusReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            return View();
        }
        public ActionResult CompensatoryStatusReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<CompensatoryStatusReportModel> listStatusReportModel = new CompOffPreApprovalFormDB().GetCompensatoryStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listStatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'CompOffPreApproval')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Designation = item.Designation,
                              AttendanceDate = item.AttendanceDate,
                              CompensatoryHours = item.CompensatoryHours,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelCompensatoryStatus(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<CompensatoryStatusReportModel> listStatusReportModel = new CompOffPreApprovalFormDB().GetCompensatoryStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listStatusReportModel);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/CompOffPreApproval/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "CompOffStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BU_Name"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request ID";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["AttendanceDate"].ColumnName = "Attendance Date";
            clonedTable.Columns["CompensatoryHours"].ColumnName = "Compensatory Hours";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region CompOff Balance Expiry Report
        public ActionResult CompOffBalanceExpiryReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            return View();
        }
        public ActionResult GetCompOffBalanceReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<CompOffBalanceReportModel> listStatusReportModel = new CompOffPreApprovalFormDB().GetCompOffBalanceReport(RequestID, CompanyID, DepartmentID, FromDate, ToDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listStatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Designation = item.Designation,
                              JoiningDate = item.JoiningDate,
                              Email = item.Email,
                              ApprovedDate = item.ApprovedDate,
                              CompOffBalance = item.CompOffBalance,
                              ExpiryDate = item.ExpiryDate
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelCompOffBalanceReport(int? RequestID, int? CompanyID, int? DepartmentID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<CompOffBalanceReportModel> listStatusReportModel = new CompOffPreApprovalFormDB().GetCompOffBalanceReport(RequestID, CompanyID, DepartmentID, FromDate, ToDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listStatusReportModel);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "CompOffBalanceReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            dt.Columns["BU_Name"].ColumnName = "BU Name";
            dt.Columns["EmployeeID"].ColumnName = "Employee ID";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["DepartmentName"].ColumnName = "Department";
            dt.Columns["JoiningDate"].ColumnName = "Joining Date";
            dt.Columns["ApprovedDate"].ColumnName = "Fully Approved Date";
            dt.Columns["CompOffBalance"].ColumnName = "Comp-Off Balance";
            dt.Columns["ExpiryDate"].ColumnName = "Expiry Date";
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Probation Status Report
        public ActionResult ProbationStatusReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            return View();
        }
        public ActionResult ProbationStatusReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int ProbationPeriodTypeID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ProbationStatusReportModel> listStatusReportModel = new EmployeeConfirmationRequestDB().GetProbationStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, ProbationPeriodTypeID, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listStatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              Requester = item.Requester,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'EmployeeConfirmationRequest')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Designation = item.Designation,
                              TypeOfEvaluation = item.TypeOfEvaluation,
                              JoiningDate = item.JoiningDate,
                              Project = item.Project,
                              ProbationCompletionDate = item.ProbationCompletionDate,
                              Category = item.Category,
                              LineManager = item.LineManager,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover,
                              ProbationPeriodType = item.ProbationPeriodType
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelProbationStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate, int ProbationPeriodTypeID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ProbationStatusReportModel> listStatusReportModel = new EmployeeConfirmationRequestDB().GetProbationStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, ProbationPeriodTypeID, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listStatusReportModel);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/EmployeeConfirmationRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "ProbationStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BU_Name"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request ID";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeID"].ColumnName = "Oracle Number";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["TypeOfEvaluation"].ColumnName = "Type Of Evaluation";
            clonedTable.Columns["JoiningDate"].ColumnName = "Joining Date";
            clonedTable.Columns["ProbationCompletionDate"].ColumnName = "Probation Completion Date";
            clonedTable.Columns["LineManager"].ColumnName = "Line Manager";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            clonedTable.Columns["ProbationPeriodType"].ColumnName = "Probation Period Type";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Leave Request Status Report
        public ActionResult LeaveRequestStatusReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            return View();
        }
        public ActionResult LeaveRequestStatusReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<LeaveRequestStatusReportModel> listStatusReportModel = new FormLeaveRequestDB().GetLeaveRequestStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listStatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'EmployeeLeaveRequest')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              Category = item.Category,
                              Department = item.Department,
                              Designation = item.Designation,
                              LeaveType = item.LeaveType,
                              RequestedDays = item.RequestedDays,
                              BeforeAvailableLeaveDays = item.BeforeAvailableLeaveDays,
                              AfterAvailableLeaveDays = item.AfterAvailableLeaveDays,
                              PublicHolidays = item.PublicHolidays,
                              LeaveAdjusted = item.LeaveAdjusted,
                              TotalDays = item.TotalDays,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelLeaveRequestStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<LeaveRequestStatusReportModel> listStatusReportModel = new FormLeaveRequestDB().GetLeaveRequestStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listStatusReportModel);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/EmployeeLeaveRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "LeaveRequestStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BU_Name"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request ID";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["LeaveType"].ColumnName = "Leave Type";
            clonedTable.Columns["LeaveDateFrom"].ColumnName = "Leave Date From";
            clonedTable.Columns["LeaveDateTo"].ColumnName = "Leave Date To";
            clonedTable.Columns["RequestedDays"].ColumnName = "Requested Days";
            clonedTable.Columns["BeforeAvailableLeaveDays"].ColumnName = "Balance Before Leave Application";
            clonedTable.Columns["AfterAvailableLeaveDays"].ColumnName = "Balance After Leave Application";
            clonedTable.Columns["PublicHolidays"].ColumnName = "Public Holidays";
            clonedTable.Columns["LeaveAdjusted"].ColumnName = "Leave Adjusted";
            clonedTable.Columns["TotalDays"].ColumnName = "Total Days";            
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Leave ReJoining Status Report
        public ActionResult LeaveRejoiningStatusReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            return View();
        }
        public ActionResult LeaveRejoiningStatusReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<LeaveRejoiningStatusReport> listStatusReportModel = new EmployeeJoiningRequestDB().GetLeaveRejoiningStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listStatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'EmployeeLeaveRejoining')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              Category = item.Category,
                              Department = item.Department,
                              Designation = item.Designation,
                              LeaveType = item.LeaveType,
                              RequestedDays = item.RequestedDays,
                              ReJoiningDate = item.ReJoiningDate,
                              LeavesToAdjust = item.LeavesToAdjust,
                              AfterAvailableLeaveDays = item.AfterAvailableLeaveDays,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelLeaveRejoiningStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<LeaveRejoiningStatusReport> listStatusReportModel = new EmployeeJoiningRequestDB().GetLeaveRejoiningStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listStatusReportModel);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/EmployeeLeaveRejoining/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "LeaveRejoiningStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BU_Name"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request ID";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["LeaveType"].ColumnName = "Leave Type";
            clonedTable.Columns["LeaveDateFrom"].ColumnName = "Leave Date From";
            clonedTable.Columns["LeaveDateTo"].ColumnName = "Leave Date To";
            clonedTable.Columns["RequestedDays"].ColumnName = "Requested Days";
            clonedTable.Columns["ReJoiningDate"].ColumnName = "Rejoin Date";
            clonedTable.Columns["AfterAvailableLeaveDays"].ColumnName = "Balance After Rejoining";
            clonedTable.Columns["LeavesToAdjust"].ColumnName = "Leave Days Adjusted";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Expense Claim Status Report
        public ActionResult ExpenseClaimStatusReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult ExpenseClaimStatusReportGrid(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ExpenseClaimStatusReportModel> listStatusReportModel = new ExpenseClaimRequestDB().GetExpenseClaimStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listStatusReportModel
                          select new
                          {
                              BU_Name = item.BU_Name,
                              Department = item.Department,
                              EmployeeName = item.EmployeeName,
                              EmployeeID = item.EmployeeID,
                              Designation = item.Designation,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'ExpenseClaimRequest')>" + item.RequestID.ToString() + "</a>",
                              TotalExpenseAmount = item.TotalExpenseAmount,
                              Amount = item.Amount,
                              RequestDate = item.RequestDate,
                              ProjectName = item.ProjectName,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelExpenseClaimStatusReport(int? RequestID, int? CompanyID, int? DepartmentID, int? CurrentStatusID, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ExpenseClaimStatusReportModel> listStatusReportModel = new ExpenseClaimRequestDB().GetExpenseClaimStatusReport(RequestID, CompanyID, DepartmentID, CurrentStatusID, FromDate, ToDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(listStatusReportModel);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/ExpenseClaimRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "ExpenseClaimStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BU_Name"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request ID";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["TotalExpenseAmount"].ColumnName = "Expense Amount";
            clonedTable.Columns["ProjectName"].ColumnName = "Project Name";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Drop In Status Report
        public ActionResult DropInStatusReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objempDB = new EmployeeDB();
            ViewBag.DropInYears = new SelectList(new AcademicYearDB().GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId).Where(x => x.employeeDetailsModel.usertypeid == 1), "EmployeeId", "FirstName");
            return View();
        }

        public ActionResult GetDropInStatusReportData(int? dropInYearId, int? companyId, int? employeeId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<DropInStatusReportModel> dropInStatusReportList = new DropInsModelDB().GetDropInStatusReportData(dropInYearId, companyId, employeeId, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in dropInStatusReportList
                          select new
                          {
                              RequestNo = item.RequestNo,
                              EmployeeName = item.EmployeeName,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              Department = item.Department,
                              VisitDate = item.VisitDate,
                              ObservationYear = item.ObservationYear,
                              LineManager = item.LineManager,
                              EmployeeAcknowledgedDropIn = item.EmployeeAcknowledgedDropIn,
                              Rating = item.Rating,
                              TotalScore = item.TotalScore
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadEmployeesbasedOnOrganization(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objempDB = new EmployeeDB();
            var employeeList = objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId).Where(x => x.employeeDetailsModel.usertypeid == 1 && x.employeeDetailsModel.CompanyID == companyId);
            return new JsonResult()
            {
                Data = employeeList.ToList(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public ActionResult ExportToExcelDropInStatusReport(int? dropInYearId, int? companyId, int? employeeId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<DropInStatusReportModel> dropInStatusReportList = new DropInsModelDB().GetDropInStatusReportData(dropInYearId, companyId, employeeId, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(dropInStatusReportList);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "DropInStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["RequestNo"].ColumnName = "Request No";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            dt.Columns["VisitDate"].ColumnName = "Visit Date";
            dt.Columns["ObservationYear"].ColumnName = "Observation Year";
            dt.Columns["LineManager"].ColumnName = "Observer";
            dt.Columns["EmployeeAcknowledgedDropIn"].ColumnName = "isAcknowledged?";
            dt.Columns["TotalScore"].ColumnName = "Total Score";
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Observation Process Status Report
        public ActionResult ObservationProcessStatusReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB objDepartmentDB = new DepartmentDB();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Years = new SelectList(new AcademicYearDB().GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            return View();
        }

        public ActionResult GetObservationtatusReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? yearId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ObservationStatusReportModel> observationStatusReportList = new ObservationsModelDB().GetObservationtatusReportData(companyId, departmentId, statusId, fromDate, toDate, yearId, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in observationStatusReportList
                          select new
                          {
                              OrganizationName = item.OrganizationName,
                              Department = item.Department,
                              EmployeeName = item.EmployeeName,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              IsObservationProcessInitiated = item.IsObservationProcessInitiated,
                              Year = item.Year,
                              TotalScore = item.TotalScore,
                              TotalRating = item.TotalRating,
                              ObservationVisitDate = item.ObservationVisitDate,
                              PreObservationRequest = item.PreObservationRequest,
                              PostObservationRequest = item.PostObservationRequest,
                              ObservationRequest = item.ObservationRequest,
                              ObservationSignOffDate = item.ObservationSignOffDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelObservationStatusReport(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? yearId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ObservationStatusReportModel> observationStatusReportList = new ObservationsModelDB().GetObservationtatusReportData(companyId, departmentId, statusId, fromDate, toDate, yearId, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(observationStatusReportList);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "ObservationStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["OrganizationName"].ColumnName = "Organization Name";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            dt.Columns["IsObservationProcessInitiated"].ColumnName = "Is Observation Process Initiated";
            dt.Columns["TotalScore"].ColumnName = "Total Score";
            dt.Columns["TotalRating"].ColumnName = "Total Rating";
            dt.Columns["ObservationVisitDate"].ColumnName = "Observation Visit Date";
            dt.Columns["PreObservationRequest"].ColumnName = "Pre-Observation Request";
            dt.Columns["PostObservationRequest"].ColumnName = "Post-Observation-Request";
            dt.Columns["ObservationRequest"].ColumnName = "Observation Request";
            dt.Columns["ObservationSignOffDate"].ColumnName = "Observation Sign Off Date";
            dt.Columns["CurrentStatus"].ColumnName = "Current Status";
            dt.Columns["CurrentApprover"].ColumnName = "Current Approver";

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Teaching Appraisal Status Report
        public ActionResult TeachingAppraisalStatusReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB objDepartmentDB = new DepartmentDB();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.Company = GetCompanySelectList();
            return View();
        }

        public ActionResult GetTeachingAppraisalStatusReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<TeachingAppraisalStatusReportModel> teachingAppraisalStatusReportList = new AppraisalTeachingModelDB().GetTeachingAppraisalStatusReportData(companyId, departmentId, statusId, fromDate, toDate, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in teachingAppraisalStatusReportList
                          select new
                          {
                              OrganizationName = item.OrganizationName,
                              Department = item.Department,
                              EmployeeName = item.EmployeeName,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              IsLessonObservationCompleted = item.IsLessonObservationCompleted,
                              DropInsCompleted = item.DropInsCompleted,
                              IsAppraisalInitiated = item.IsAppraisalInitiated,
                              RequestId = item.RequestId, 
                              Year = item.Year,
                              StartDate = item.StartDate,
                              DueDate = item.DueDate,
                              AppraisalSignOffDate = item.AppraisalSignOffDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelTeachingAppraisalStatusReport(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<TeachingAppraisalStatusReportModel> teachingAppraisalStatusReportList = new AppraisalTeachingModelDB().GetTeachingAppraisalStatusReportData(companyId, departmentId, statusId, fromDate, toDate, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(teachingAppraisalStatusReportList);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "TeachingAppraisalStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["OrganizationName"].ColumnName = "Organization Name";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            dt.Columns["IsLessonObservationCompleted"].ColumnName = "Is Lesson Observation Completed";
            dt.Columns["DropInsCompleted"].ColumnName = "Is 3 drop ins completed & acknowledged";
            dt.Columns["IsAppraisalInitiated"].ColumnName = "Is Appraisal Initiated";
            dt.Columns["RequestId"].ColumnName = "Request Number";
            dt.Columns["StartDate"].ColumnName = "Start Date";
            dt.Columns["DueDate"].ColumnName = "Due Date";
            dt.Columns["AppraisalSignOffDate"].ColumnName = "Appraisal Sign Off Date";
            dt.Columns["CurrentStatus"].ColumnName = "Current Status";
            dt.Columns["CurrentApprover"].ColumnName = "Current Approver";

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Leave Accrual Report
        public ActionResult LeaveAccrualReport()
        {
            EmployeeDB objempDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            return View();
        }
        public ActionResult LeaveAccrualReportGrid(int? CompanyID, int? DepartmentID, int? EmployeeID,string FromDate, string ToDate)
        {
            //,  string FromDate, string ToDate
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<LeaveAccrualReportModel> listLeaveAccrualReport = new LeaveBalanceManagerDB().GetLeaveAccrualReport(CompanyID, DepartmentID, EmployeeID, objUserContextViewModel.UserId, FromDate, ToDate);
            var vList = new object();
            vList = new
            {
                aaData = (from item in listLeaveAccrualReport
                          select new
                          {
                              CompanyName = item.CompanyName,                           
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,                            
                              DepartmentName = item.DepartmentName,
                              DesignationName = item.DesignationName,
                              EmailID = item.EmailID,
                              AccrualDate = item.AccrualDate,
                              AccrualDays = item.AccrualDays,
                              TotalAccrualDaysBefore = item.TotalAccrualDaysBefore,
                              TotalAccrualDaysAfter = item.TotalAccrualDaysAfter,
                              ModifiedByName = item.ModifiedByName,
                              ModifiedOn = item.ModifiedOn,
                              BalanceUpdated = item.BalanceUpdated
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToExcelLeaveAccrualReport(int? CompanyID, int? DepartmentID, int? EmployeeID, string FromDate, string ToDate)
        {
            //, string FromDate, string ToDate
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<LeaveAccrualReportModel> listLeaveAccrualReport = new LeaveBalanceManagerDB().GetLeaveAccrualReport(CompanyID, DepartmentID, EmployeeID, objUserContextViewModel.UserId, FromDate,ToDate);
            DataTable dt = ConvertListToDataTable.ToDataTable(listLeaveAccrualReport);

            //DataTable clonedTable = dt.Clone();        
            //int cnt = 0;
            //foreach (DataRow row in dt.Rows)
            //{
            //    clonedTable.ImportRow(row);
            //    clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
            //        "/EmployeeLeaveRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
            //    cnt++;
            //}

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "LeaveAccrualReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            // clonedTable.Columns.Remove("A");
            dt.Columns["CompanyName"].ColumnName = "Business Unit";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeAlternativeID"].ColumnName = "Orcale Id";
            dt.Columns["EmailID"].ColumnName = "Email Id";
            dt.Columns["DepartmentName"].ColumnName = "Department";
            dt.Columns["DesignationName"].ColumnName = "Designation";
            dt.Columns["AccrualDate"].ColumnName = "Date of Accrual";
            dt.Columns["AccrualDays"].ColumnName = "Balance Accrued";
            dt.Columns["TotalAccrualDaysBefore"].ColumnName = "Total Leave Days before accrual";
            dt.Columns["TotalAccrualDaysAfter"].ColumnName = "Total Leave Days after accrual";
            dt.Columns["ModifiedByName"].ColumnName = "Updated By";
            dt.Columns["ModifiedOn"].ColumnName = "Updated Date";
            dt.Columns["BalanceUpdated"].ColumnName = "Balance Updated";
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Non Teaching Reports

        #region Goal Setting Report 
        public ActionResult GoalSettingReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.Years = new SelectList(new AcademicYearDB().GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");
            return View();
        }

        public ActionResult GetGoalSettingReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate,int? year)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<GoalSettingReportModel> goalSettingReports = new AppraisalTeachingModelDB().GetGoalSettingReportData(companyId, departmentId, statusId, fromDate, toDate, year);
            var vList = new object();
            vList = new
            {
                aaData = (from item in goalSettingReports
                          select new
                          {
                              BUName = item.BUName,
                              Department = item.Department,
                              EmployeeName = item.EmployeeName,
                              EmployeeOracleNumber = item.EmployeeOracleNumber,
                              IsGoalSettingInitialize = item.IsGoalSettingInitialize,
                              RequestId = item.RequestId,
                              Year = item.Year,
                              StartDate = item.StartDate,
                              DueDate = item.DueDate,
                              GoalSettingSignOffDate = item.GoalSettingSignOffDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelGoalSettingReport(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate,int? year)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<GoalSettingReportModel> goalSettingReports = new AppraisalTeachingModelDB().GetGoalSettingReportData(companyId, departmentId, statusId, fromDate, toDate, year);
            DataTable dt = ConvertListToDataTable.ToDataTable(goalSettingReports);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "GoalSettingReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["BUName"].ColumnName = "BU Name";
            dt.Columns["Department"].ColumnName = "Department";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeOracleNumber"].ColumnName = "Employee ID";
            dt.Columns["IsGoalSettingInitialize"].ColumnName = "Is Goal Setting Initialize";
            dt.Columns["RequestId"].ColumnName = "Request Number";
            dt.Columns["StartDate"].ColumnName = "Start Date";
            dt.Columns["DueDate"].ColumnName = "Due Date";
            dt.Columns["GoalSettingSignOffDate"].ColumnName = "Goal Setting Sign Off Date";
            dt.Columns["CurrentStatus"].ColumnName = "Current Status";
            dt.Columns["CurrentApprover"].ColumnName = "Current Approver";

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Mid Year Evaluation Report

        public ActionResult MidYearEvaluationReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.Years = new SelectList(new AcademicYearDB().GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");

            return View();
        }

        public ActionResult GetMidYearEvaluationReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate,int? year)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<MidYearEvaluationReportModel> midYearEvaluationReports = new AppraisalTeachingModelDB().GetMidYearEvaluationReportData(companyId, departmentId, statusId, fromDate, toDate, year);
            var vList = new object();
            vList = new
            {
                aaData = (from item in midYearEvaluationReports
                          select new
                          {
                              BUName = item.BUName,
                              Department = item.Department,
                              EmployeeName = item.EmployeeName,
                              EmployeeOracleNumber = item.EmployeeOracleNumber,
                              IsGoalSettingInitialize = item.IsGoalSettingInitialize,
                              IsGoalSettingCompleted = item.IsGoalSettingCompleted,
                              IsMidYrEvaluationInitialize = item.IsMidYrEvaluationInitialize,
                              RequestID = item.RequestID,
                              Year = item.Year,
                              StartDate = item.StartDate,
                              DueDate = item.DueDate,
                              MidYearSignOffDate = item.MidYearSignOffDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelMidYearEvaluationReport(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? year)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<MidYearEvaluationReportModel> midYearEvaluationReports = new AppraisalTeachingModelDB().GetMidYearEvaluationReportData(companyId, departmentId, statusId, fromDate, toDate, year);
            DataTable dt = ConvertListToDataTable.ToDataTable(midYearEvaluationReports);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "MidYearEvaluationReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["BUName"].ColumnName = "BU Name";
            dt.Columns["Department"].ColumnName = "Department";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeOracleNumber"].ColumnName = "Employee ID";
            dt.Columns["IsGoalSettingInitialize"].ColumnName = "Is Goal Setting Initialize";
            dt.Columns["IsGoalSettingCompleted"].ColumnName = "Is Goal Setting Completed";
            dt.Columns["IsMidYrEvaluationInitialize"].ColumnName = "Is Mid Year Evaluation Initialize";
            dt.Columns["RequestID"].ColumnName = "Request Number";
            dt.Columns["StartDate"].ColumnName = "Start Date";
            dt.Columns["DueDate"].ColumnName = "Due Date";
            dt.Columns["MidYearSignOffDate"].ColumnName = "Mid Year Sign Off Date";
            dt.Columns["CurrentStatus"].ColumnName = "Current Status";
            dt.Columns["CurrentApprover"].ColumnName = "Current Approver";

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Full Year Evaluation Report

        public ActionResult FullYearEvaluationReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.Department = new SelectList(objDepartmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.Years = new SelectList(new AcademicYearDB().GetAllAcademicYear().OrderByDescending(s => s.Duration), "AcademicYearId", "Duration");

            return View();
        }

        public ActionResult GetFullYearEvaluationReportData(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? year)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<FullYearEvaluationReportModel> fullYearEvaluationReports = new AppraisalTeachingModelDB().GetFullYearEvaluationReportData(companyId, departmentId, statusId, fromDate, toDate, year);
            var vList = new object();
            vList = new
            {
                aaData = (from item in fullYearEvaluationReports
                          select new
                          {
                              BUName = item.BUName,
                              Department = item.Department,
                              EmployeeName = item.EmployeeName,
                              EmployeeOracleNumber = item.EmployeeOracleNumber,
                              IsGoalSettingInitialize = item.IsGoalSettingInitialize,
                              IsGoalSettingCompleted = item.IsGoalSettingCompleted,
                              IsMidYrEvaluationInitialize = item.IsMidYrEvaluationInitialize,
                              IsMidYrEvaluationCompleted = item.IsMidYrEvaluationCompleted,
                              IsFullYrEvaluationInitialize = item.IsFullYrEvaluationInitialize,
                              RequestID = item.RequestID,
                              Year = item.Year,
                              StartDate = item.StartDate,
                              DueDate = item.DueDate,
                              FullYearSignOffDate = item.FullYearSignOffDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelFullYearEvaluationReport(int? companyId, int? departmentId, int? statusId, string fromDate, string toDate, int? year)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<FullYearEvaluationReportModel> fullYearEvaluationReports = new AppraisalTeachingModelDB().GetFullYearEvaluationReportData(companyId, departmentId, statusId, fromDate, toDate, year);
            DataTable dt = ConvertListToDataTable.ToDataTable(fullYearEvaluationReports);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "FullYearEvaluationReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["BUName"].ColumnName = "BU Name";
            dt.Columns["Department"].ColumnName = "Department";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeOracleNumber"].ColumnName = "Employee ID";
            dt.Columns["IsGoalSettingInitialize"].ColumnName = "Is Goal Setting Initialize";
            dt.Columns["IsGoalSettingCompleted"].ColumnName = "Is Goal Setting Completed";
            dt.Columns["IsMidYrEvaluationInitialize"].ColumnName = "Is Mid Year Evaluation Initialize";
            dt.Columns["IsMidYrEvaluationCompleted"].ColumnName = "Is Mid Year Evaluation Completed";
            dt.Columns["IsFullYrEvaluationInitialize"].ColumnName = "Is Full Year Evaluation Initialize";
            dt.Columns["RequestID"].ColumnName = "Request Number";
            dt.Columns["StartDate"].ColumnName = "Start Date";
            dt.Columns["DueDate"].ColumnName = "Due Date";
            dt.Columns["FullYearSignOffDate"].ColumnName = "Mid Year Sign Off Date";
            dt.Columns["CurrentStatus"].ColumnName = "Current Status";
            dt.Columns["CurrentApprover"].ColumnName = "Current Approver";

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #endregion

        #region Noc Status Report
        public ActionResult NocStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objempDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetNOCStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            NocRequestDB nocRequestDB = new NocRequestDB();
            List<NOCStatusReportModel> nocRequestStatusList = nocRequestDB.GetNOCStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in nocRequestStatusList
                          select new
                          {
                              BUName = item.BUName,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'NocRequest')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Designation = item.Designation,
                              Category = item.Category,
                              NOCLanguage = item.NOCLanguage,
                              AddressTo = item.AddressTo,
                              RequestReason = item.RequestReason,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelNOCStatus(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            NocRequestDB nocRequestDB = new NocRequestDB();
            List<NOCStatusReportModel> nocRequestStatusList = nocRequestDB.GetNOCStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(nocRequestStatusList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/NocRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "NOCStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request Number";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["NOCLanguage"].ColumnName = "NOC Language";
            clonedTable.Columns["AddressTo"].ColumnName = "Address To";
            clonedTable.Columns["RequestReason"].ColumnName = "Request Reason";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Certificate Request Status Report
        public ActionResult CertificateRequestStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objempDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetCertificateRequestStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CertificateRequestFormDB certificateRequestformDB = new CertificateRequestFormDB();
            List<CertificateRequestStatusReportModel> certificateRequestStatusReportList = certificateRequestformDB.GetCertificateRequestStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in certificateRequestStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'CertificateRequestForm')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              Category = item.Category,
                              DepartmentName = item.DepartmentName,
                              Project = item.Project,
                              Designation = item.Designation,
                              RequestedCertificateType = item.RequestedCertificateType,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelCertificateRequestStatus(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CertificateRequestFormDB certificateRequestformDB = new CertificateRequestFormDB();
            List<CertificateRequestStatusReportModel> certificateRequestStatusList = certificateRequestformDB.GetCertificateRequestStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(certificateRequestStatusList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/CertificateRequestForm/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "CertificateRequestStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request Number";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["RequestedCertificateType"].ColumnName = "Requested Certificate Type";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Passport Withdrawal Status Report
        public ActionResult PassportWithdrawalStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objempDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetPassportWithdrawalStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PassportWithdrawalRequestDB passportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
            List<PassportWithdrawalStatusReportModel> passportWithdrawalStatusReportList = passportWithdrawalRequestDB.GetPassportWithdrawalStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in passportWithdrawalStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'PassportWithdrawalRequest')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Project = item.Project,
                              Designation = item.Designation,
                              Category = item.Category,
                              RequestReason = item.RequestReason,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelPassportWthdrawalStatus(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PassportWithdrawalRequestDB passportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
            List<PassportWithdrawalStatusReportModel> passportWithdrawalStatusReportList = passportWithdrawalRequestDB.GetPassportWithdrawalStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(passportWithdrawalStatusReportList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/PassportWithdrawalRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "PassportWithdrawalStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request Number";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["RequestReason"].ColumnName = "Request Reason";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Separation Request Status Report
        public ActionResult SeparationStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(separationRequestFormDB.GetAllSeparationRequestedEmployee(objUserContextViewModel.UserId), "EmployeeId", "FullName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetSeparationStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            List<SeparationStatusReportModel> separationStatusReportList = separationRequestFormDB.GetSeparationStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in separationStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'SeparationRequestForm')>" + item.RequestID.ToString() + "</a>",
                              RequestDate = item.RequestDate,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Project = item.Project,
                              Designation = item.Designation,
                              Category = item.Category,
                              SeparationType = item.SeparationType,
                              DateOfResignation = item.DateOfResignation,
                              LastWorkingDay = item.LastWorkingDay,
                              ReasonOfResignation = item.ReasonOfResignation,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelSeparationStatusReport(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            List<SeparationStatusReportModel> separationStatusReportList = separationRequestFormDB.GetSeparationStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(separationStatusReportList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/SeparationRequestForm/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "SeparationStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["RequestID"].ColumnName = "Request Number";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["SeparationType"].ColumnName = "Separation Type";
            clonedTable.Columns["DateOfResignation"].ColumnName = "Date Of Resignation";
            clonedTable.Columns["LastWorkingDay"].ColumnName = "Last Working Day";
            clonedTable.Columns["ReasonOfResignation"].ColumnName = "Reason Of Resignation";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region End Of Term Status Report
        public ActionResult EndOfTermStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(separationRequestFormDB.GetAllSeparationRequestedEmployee(objUserContextViewModel.UserId), "EmployeeId", "FullName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetEndOfTermStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ClearanceRequestDB clearanceRequestDB = new ClearanceRequestDB();
            List<EndOfTermStatusReportModel> endOfTermStatusReportList = clearanceRequestDB.GetEndOfTermStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in endOfTermStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              DepartmentName = item.DepartmentName,
                              EmployeeName = item.EmployeeName,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              RequestedBy = item.RequestedBy,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'ClearanceRequest')>" + item.RequestID.ToString() + "</a>",
                              ResignationNumber = item.ResignationNumber,
                              RequestDate = item.RequestDate,
                              Category = item.Category,
                              Designation = item.Designation,
                              Project = item.Project,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelEndOfTermStatusReport(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ClearanceRequestDB clearanceRequestDB = new ClearanceRequestDB();
            List<EndOfTermStatusReportModel> endOfTermStatusReportList = clearanceRequestDB.GetEndOfTermStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(endOfTermStatusReportList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/ClearanceRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "EndOfTermStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            clonedTable.Columns["RequestedBy"].ColumnName = "Requested By";
            clonedTable.Columns["RequestID"].ColumnName = "Request Number";
            clonedTable.Columns["ResignationNumber"].ColumnName = "Resignation Number";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["Project"].ColumnName = "Project Name";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Exit Interview Status Report
        public ActionResult ExitInterviewStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(separationRequestFormDB.GetAllSeparationRequestedEmployee(objUserContextViewModel.UserId), "EmployeeId", "FullName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetExitInterviewStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ExitInterviewRequestDB exitInterviewRequestDB = new ExitInterviewRequestDB();
            List<ExitInterviewStatusReportModel> exitInterviewStatusReportList = exitInterviewRequestDB.GetExitInterviewStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in exitInterviewStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              DepartmentName = item.DepartmentName,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'ExitInterviewRequest')>" + item.RequestID.ToString() + "</a>",
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              RequestDate = item.RequestDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelExitInterviewStatusReport(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ExitInterviewRequestDB exitInterviewRequestDB = new ExitInterviewRequestDB();
            List<ExitInterviewStatusReportModel> exitInterviewStatusReportList = exitInterviewRequestDB.GetExitInterviewStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(exitInterviewStatusReportList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/ExitInterviewRequest/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "ExitInterviewStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["RequestID"].ColumnName = "Request no";
            clonedTable.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Pending With";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Employee Joining Status Report
        public ActionResult EmployeeJoiningStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objempDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetEmployeeJoiningStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PendingJoiningRequestDB pendingJoiningRequestDB = new PendingJoiningRequestDB();
            List<EmployeeJoiningStatusReportModel> employeeJoiningStatusReportList = pendingJoiningRequestDB.GetEmployeeJoiningStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in employeeJoiningStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              RequestID = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'EmployeeJoiningForm')>" + item.RequestID.ToString() + "</a>",
                              Requester = item.Requester,
                              RequestDate = item.RequestDate,
                              EmployeeID = item.EmployeeID,
                              RequesterID = item.RequesterID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Project = item.Project,
                              Designation = item.Designation,
                              Category = item.Category,
                              ExpectedJoiningDate = item.ExpectedJoiningDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelEmployeeJoiningStatusReport(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PendingJoiningRequestDB pendingJoiningRequestDB = new PendingJoiningRequestDB();
            List<EmployeeJoiningStatusReportModel> employeeJoiningStatusReportList = pendingJoiningRequestDB.GetEmployeeJoiningStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(employeeJoiningStatusReportList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["RequestID"].Caption = "hyperlink";
            clonedTable.Columns["RequestID"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["RequestID"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/EmployeeJoiningForm/ViewDetails/" + row["FormProcessID"] + "|" + row["RequestID"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "EmployeeJoiningStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["Requester"].ColumnName = "Requester";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["DepartmentName"].ColumnName = "Department";
            clonedTable.Columns["ExpectedJoiningDate"].ColumnName = "Joining Date";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Employee Profile Status Report
        public ActionResult EmployeeProfileStatusReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objempDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetEmployeeProfileStatusReportData(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeProfileCreationFormDB employeeProfileCreationDB = new EmployeeProfileCreationFormDB();
            List<EmployeeProfileStatusReportModel> employeeProfileStatusReportList = employeeProfileCreationDB.GetEmployeeProfileStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in employeeProfileStatusReportList
                          select new
                          {
                              BUName = item.BUName,
                              R1ReferenceNo = "<a href='javascript:void(0);' onclick =ViewDetails(" + item.FormProcessID.ToString() + ",\'EmployeeProfileCreationForm')>" + item.R1ReferenceNo.ToString() + "</a>",
                              Requester = item.Requester,
                              RequestDate = item.RequestDate,
                              EmployeeProfile = item.EmployeeProfile,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentName = item.DepartmentName,
                              Project = item.Project,
                              Designation = item.Designation,
                              ExpectedJoiningDate = item.ExpectedJoiningDate,
                              CurrentStatus = item.CurrentStatus,
                              CurrentApprover = item.CurrentApprover
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelEmployeeProfileStatusReport(int? requestId, int? companyId, int? employeeId, int? departmentId, int? requestStatusId, string dateFrom, string dateTo)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeProfileCreationFormDB employeeProfileCreationDB = new EmployeeProfileCreationFormDB();
            List<EmployeeProfileStatusReportModel> employeeProfileStatusReportList = employeeProfileCreationDB.GetEmployeeProfileStatusReportData(requestId, companyId, employeeId, departmentId, requestStatusId, dateFrom, dateTo, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(employeeProfileStatusReportList);

            DataTable clonedTable = dt.Clone();
            clonedTable.Columns["R1ReferenceNo"].Caption = "hyperlink";
            clonedTable.Columns["R1ReferenceNo"].DataType = typeof(String);
            int cnt = 0;
            foreach (DataRow row in dt.Rows)
            {
                clonedTable.ImportRow(row);
                clonedTable.Rows[cnt]["R1ReferenceNo"] = Request.Url.OriginalString.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0] + "://" + Request.Url.Authority +
                    "/EmployeeProfileCreationForm/ViewDetails/" + row["FormProcessID"] + "|" + row["R1ReferenceNo"];
                cnt++;
            }

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "EmployeeProfileStatusReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            clonedTable.Columns.Remove("FormProcessID");
            clonedTable.Columns["BUName"].ColumnName = "BU Name";
            clonedTable.Columns["R1ReferenceNo"].ColumnName = "R1 Reference No";
            clonedTable.Columns["RequestDate"].ColumnName = "Request Date";
            clonedTable.Columns["EmployeeProfile"].ColumnName = "Employee Profile";
            clonedTable.Columns["EmployeeName"].ColumnName = "Employee Name";
            clonedTable.Columns["ExpectedJoiningDate"].ColumnName = "Expected Joining Date";
            clonedTable.Columns["CurrentStatus"].ColumnName = "Current Status";
            clonedTable.Columns["CurrentApprover"].ColumnName = "Current Approver";
            DataSet ds = new DataSet();
            ds.Tables.Add(clonedTable);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion

        #region Employee Master Report
        public ActionResult EmployeeMasterReport()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objempDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId), "EmployeeId", "FirstName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View();
        }

        public ActionResult GetEmployeeMasterReportData(int? companyId, int? employeeId, int? departmentId, bool? isActive)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeProfileCreationFormDB employeeProfileCreationDB = new EmployeeProfileCreationFormDB();
            List<EmployeeMasterReportModel> employeeMasterReportList = employeeProfileCreationDB.GetEmployeeMasterReportData(companyId, employeeId, departmentId, isActive, objUserContextViewModel.UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in employeeMasterReportList
                          select new
                          {
                              BusinessUnit = item.BusinessUnit,
                              OperatingUnitID = item.OperatingUnitID,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              FirstName = item.FirstName,
                              MiddleName = item.MiddleName,
                              LastName = item.LastName,
                              EmployeeName = item.EmployeeName,
                              EmployeeType = item.EmployeeType,
                              ExpectedJoiningDate = item.ExpectedJoiningDate,
                              HireDate = item.HireDate,
                              ProbationMonths = item.ProbationMonths,
                              NoticedPeriod = item.NoticedPeriod,
                              Religion = item.Religion,
                              DOB = item.DOB,
                              BirthCity = item.BirthCity,
                              BirthCountry = item.BirthCountry,
                              Citizenship = item.Citizenship,
                              MaritalStatus = item.MaritalStatus,
                              Gender = item.Gender,
                              BloodGroup = item.BloodGroup,
                              MotherName = item.MotherName,
                              PositionOfferLetter = item.PositionOfferLetter,
                              PositionLabourContract = item.PositionLabourContract,
                              Email = item.Email,
                              UserName = item.UserName,
                              DepartmentName = item.DepartmentName,
                              DesignationTitle = item.DesignationTitle,
                              Location = item.Location,
                              Category = item.Category,
                              SubCategory = item.SubCategory,
                              SalaryBasis = item.SalaryBasis,
                              //FirstLineManagerOracleNo = item.FirstLineManagerOracleNo,
                              //FirstLineManagerName = item.FirstLineManagerName,
                              //FirstLineManagerEmail = item.FirstLineManagerEmail,
                              //FirstLineManagerAD = item.FirstLineManagerAD,
                              SponsorName = item.SponsorName,
                              IBAN = item.IBAN,
                              BankName = item.BankName,
                              AnnualLeaveEntitlement = item.AnnualLeaveEntitlement,
                              AnnualLeaveCalculation = item.AnnualLeaveCalculation,
                              TicketEntitlement = item.TicketEntitlement,
                              TicketEntitlementFamily = item.TicketEntitlementFamily,
                              TicketCapedAmount = item.TicketCapedAmount,
                              TicketClass = item.TicketClass,
                              TicketFrequency = item.TicketFrequency,
                              HomeTownForTicket = item.HomeTownForTicket,
                              MedicalInsurance = item.MedicalInsurance,
                              MedicalInsuranceFamily = item.MedicalInsuranceFamily,
                              ContactNumber = item.ContactNumber,
                              PassportNo = item.PassportNo,
                              PassportExpiryDate = item.PassportExpiryDate,
                              PassportCustody = item.PassportCustody,
                              VisaNumber = item.VisaNumber,
                              VisaExpiryDate = item.VisaExpiryDate,
                              EmiratesIDNumber = item.EmiratesIDNumber,
                              EmiratesIDExpiryDate = item.EmiratesIDExpiryDate,
                              LabourCardNumber = item.LabourCardNumber,
                              LabourCardExpiryDate = item.LabourCardExpiryDate,
                              SecondLineManagerName = item.SecondLineManagerName,
                              SecondLineManager = item.SecondLineManager,
                              HODName = item.HODName,
                              HODOracleNumber = item.HODOracleNumber,
                              BUHeadName = item.BUHeadName,
                              BUHeadOracleNumber = item.BUHeadOracleNumber
                          }).ToArray()
            };
           
            JsonResult result = Json(vList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;//8675309;//2097152
            return result;
                //Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelEmployeeMasterReport(int? companyId, int? employeeId, int? departmentId, bool? isActive)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeProfileCreationFormDB employeeProfileCreationDB = new EmployeeProfileCreationFormDB();
            List<EmployeeMasterReportModel> employeeMasterReportList = employeeProfileCreationDB.GetEmployeeMasterReportData(companyId, employeeId, departmentId, isActive, objUserContextViewModel.UserId);
            DataTable dt = ConvertListToDataTable.ToDataTable(employeeMasterReportList);

            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "EmployeeMasterReport" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns["BusinessUnit"].ColumnName = "Business Unit";
            dt.Columns["OperatingUnitID"].ColumnName = "Operating Unit ID";
            dt.Columns["EmployeeAlternativeID"].ColumnName = "Employee ID";
            dt.Columns["FirstName"].ColumnName = "First Name";
            dt.Columns["MiddleName"].ColumnName = "Middle Name";
            dt.Columns["LastName"].ColumnName = "Last Name";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["EmployeeType"].ColumnName = "Employee Type";
            dt.Columns["ExpectedJoiningDate"].ColumnName = "Expected Joining Date";
            dt.Columns["HireDate"].ColumnName = "Hire Date";
            dt.Columns["ProbationMonths"].ColumnName = "Probation Months";
            dt.Columns["NoticedPeriod"].ColumnName = "Notice Period";
            dt.Columns["BirthCity"].ColumnName = "Birth City";
            dt.Columns["BirthCountry"].ColumnName = "Birth Country";
            dt.Columns["MaritalStatus"].ColumnName = "Marital Status";
            dt.Columns["BloodGroup"].ColumnName = "Blood Group";
            dt.Columns["MotherName"].ColumnName = "Mother Name";
            dt.Columns["PositionOfferLetter"].ColumnName = "Position Offer Letter";
            dt.Columns["PositionLabourContract"].ColumnName = "Position Labour Contract";
            dt.Columns["UserName"].ColumnName = "User Name";
            dt.Columns["DepartmentName"].ColumnName = "Department";
            dt.Columns["DesignationTitle"].ColumnName = "Designation Title";
            dt.Columns["SalaryBasis"].ColumnName = "Salary Basis";
            dt.Columns["FirstLineManagerOracleNo"].ColumnName = "First Line Manager ID";
            dt.Columns["FirstLineManagerName"].ColumnName = "First Line Manager Name";
            dt.Columns["FirstLineManagerEmail"].ColumnName = "First Line Manager Email";
            dt.Columns["FirstLineManagerAD"].ColumnName = "First Line Manager AD";
            dt.Columns["SponsorName"].ColumnName = "Sponsor Name";
            dt.Columns["BankName"].ColumnName = "Bank Name";
            dt.Columns["AnnualLeaveEntitlement"].ColumnName = "Annual Leave Entitlement";
            dt.Columns["AnnualLeaveCalculation"].ColumnName = "Annual Leave Calculation";
            dt.Columns["TicketEntitlement"].ColumnName = "Ticket Entitlement";
            dt.Columns["TicketEntitlementFamily"].ColumnName = "Ticket Entitlement Family";
            dt.Columns["TicketCapedAmount"].ColumnName = "Ticket Caped Amount";
            dt.Columns["TicketClass"].ColumnName = "Ticket Class";
            dt.Columns["TicketFrequency"].ColumnName = "Ticket Frequency";
            dt.Columns["HomeTownForTicket"].ColumnName = "Home Town For Ticket";
            dt.Columns["MedicalInsurance"].ColumnName = "Medical Insurance";
            dt.Columns["MedicalInsuranceFamily"].ColumnName = "Medical Insurance Family";
            dt.Columns["ContactNumber"].ColumnName = "Contact No";
            dt.Columns["PassportNo"].ColumnName = "Passport No";
            dt.Columns["PassportExpiryDate"].ColumnName = "Passport Expiry Date";
            dt.Columns["PassportCustody"].ColumnName = "Passport Custody";
            dt.Columns["VisaNumber"].ColumnName = "Visa Number";
            dt.Columns["VisaExpiryDate"].ColumnName = "Visa Expiry Date";
            dt.Columns["EmiratesIDNumber"].ColumnName = "Emirates ID Number";
            dt.Columns["EmiratesIDExpiryDate"].ColumnName = "Emirates ID Expiry Date";
            dt.Columns["LabourCardNumber"].ColumnName = "Labour Card Number";
            dt.Columns["LabourCardExpiryDate"].ColumnName = "Labour Card Expiry Date";
            dt.Columns["SecondLineManagerName"].ColumnName = "Second Line Manager Name";
            dt.Columns["SecondLineManager"].ColumnName = "Second Line Manager ID";
            dt.Columns["HODName"].ColumnName = "HOD Name";
            dt.Columns["HODOracleNumber"].ColumnName = "HOD ID";
            dt.Columns["BUHeadName"].ColumnName = "BU Head Name";
            dt.Columns["BUHeadOracleNumber"].ColumnName = "BU Head ID";

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion
    }
}