﻿using System.Web.Mvc;

namespace HRMS.Web.Areas.Employee
{
    public class EmployeeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Employee";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Index",
                "Employee/{controller}/{action}/{id}",
                new {Controller="Employee", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}