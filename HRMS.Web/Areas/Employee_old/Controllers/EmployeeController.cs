﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;

namespace HRMS.Web.Areas.Employee.Controllers
{
    public class EmployeeController : Controller
    {
        //
        // GET: /Employee/Employee/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployeeList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        {

            //-------------Data Objects--------------------
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
//objEmployeeList = objEmployeeDB.GetEmployeeList();


            //---------------------------------------------

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objEmployeeList
                          select new
                          {
                              Actions = "<a class='btn btn-primary' onclick='EditChannel(" + item.EmployeeId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> Edit</a>",
                              EmployeeId = item.EmployeeId,
                              FirstName = item.FirstName,
                              LastName = item.LastName,
                              EmailID = item.EmailId,
                              PhoneNo = item.PhoneNo.ToString()                         
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };

            
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Test()
        {
            return View();
        }
    
    }
}