﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace HRMS.Web.FileUpload
{
    public static class FileUploadHelper
    {
        public static string ConstructAndReturnFullUrlFromBasicUrl(string basicUrl)
        {
            var photoUrl = new StringBuilder("http://", 100);
            photoUrl.Append(HttpContext.Current.Request.Url.Host);
            if (!HttpContext.Current.Request.Url.IsDefaultPort)
            {
                photoUrl.Append(":");
                photoUrl.Append(HttpContext.Current.Request.Url.Port);
            }
            photoUrl.Append("/");
            photoUrl.Append(basicUrl.Replace("\\", "/"));

            return photoUrl.ToString();
        }

        public static string GetDefaultImage()
        {
            return "../Content/themes/base/images/DefaultLogo.png";
        }

        public static string GetUrlOfAgentLogo(int systemUserId, string fileName)
        {
            string basicUrl = ConfigurationManager.AppSettings["PermanentAgentLogoUrl"] + systemUserId + "\\" + fileName + ".jpg";
            return ConstructAndReturnFullUrlFromBasicUrl(basicUrl);
        }

        public static string GetUrlOfBannerImage(int systemUserId, string fileName)
        {
            string basicUrl = ConfigurationManager.AppSettings["PermanentBannerUrl"] + systemUserId + "\\" + fileName + ".jpg";
            return ConstructAndReturnFullUrlFromBasicUrl(basicUrl);
        }

        public static string GetBaseUrlForTemporayAgentLogo(int userId)
        {
            string url = Path.Combine(HttpContext.Current.Server.MapPath("/"),
                ConfigurationManager.AppSettings["TemporaryAgentLogoUrl"] + userId);
            return url;
        }

        public static string GetBaseUrlForTemporayBanner(int userId)
        {
            string url = Path.Combine(HttpContext.Current.Server.MapPath("/"),
                ConfigurationManager.AppSettings["TemporaryBannerUrl"] + userId);
            return url;
        }

        public static bool CreateDirectory(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
                return true;
            }

            return false;
        }

        

        public static string AppendTimeStamp(this string fileName)
        {
            return string.Concat(
                Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                Path.GetExtension(fileName)
                );
        }

        public static void DeleteDirectoryContentsLeavingDirectoryIntact(string directoryPath)
        {
            if (directoryPath != null)
            {
                if (Directory.Exists(directoryPath))
                {
                    string[] files = Directory.GetFiles(directoryPath);
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                }
            }
        }


      

        public static void UploadFile(string directoryPath, HttpPostedFileBase file)
        {
            var directoryInfo = new DirectoryInfo(directoryPath);

            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            var combine = "";
            if (file != null)
            {

                HttpPostedFileBase httpPostedFileBase = file;
                combine = Path.Combine(directoryPath, file.FileName);
                httpPostedFileBase.SaveAs(combine);
            }
            
        }


        public static void UploadFile(string directoryPath, HttpPostedFileBase file, int EmploymentID, out string FullPath,out string file_name)
        {
            var directoryInfo = new DirectoryInfo(directoryPath);

            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            var combine = "";
            var fileName = "";
            //var filetype = "";
            if (file != null)
            {
                // extract only the fielname
                var filename = Path.GetFileName(file.FileName).Split('.');

                fileName = "Past_" + EmploymentID + "_" + filename[0] + "_" + DateTime.Now.ToShortDateString().Replace(",", "").Replace("-", "").Replace(":", "") + "." + filename[1];
                //filetype = filename[1].ToString();
                // TODO: need to define destination
                HttpPostedFileBase httpPostedFileBase = file;
                combine = Path.Combine(directoryPath,fileName);
                httpPostedFileBase.SaveAs(combine); 
            }
            FullPath = combine;
            file_name = fileName;
            
        }

        public static void UploadFile(byte[] bytes, string fileName)
        {          
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(new MemoryStream(bytes)))
            {
                image.Save(fileName);               
            }           
        }

        public static void DeleteFile(string fileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }
        }
    }
}