﻿using HRMS.Web.AuthenticationExt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace HRMS.Web.Filters
{
    public class SecurityAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public string Permissions { get; set; }

        protected virtual IPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as IPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string action = filterContext.ActionDescriptor.ActionName;
            if (CurrentUser != null && CurrentUser.Identity != null)
            {
                HttpCookie authCookie =
                  filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
                {

                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    AuthenticationSerialize serialiseAuth = serializer.Deserialize<AuthenticationSerialize>(authTicket.UserData);
                    Authentication auth = new Authentication(authTicket.Name);
                    if (authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
                        auth.UserContext = serialiseAuth.UserContext;
                    if (!string.IsNullOrEmpty(Permissions))
                    {
                        if (serialiseAuth != null)
                        {
                            //if (!serialiseAuth.UserContext.Permisssion.Contains(Permissions))
                            //{
                            //    filterContext.Result = new RedirectResult("/Account/UserLogin");
                            //}
                        }
                    }
                    //HttpContext.Current.User = (IPrincipal)auth;
                }
            }
            else
            {
                filterContext.Controller.TempData["message"] = "You are not logged in.";
                filterContext.Result = new RedirectResult("/Account/UserLogin");
            }
        }
    }
}