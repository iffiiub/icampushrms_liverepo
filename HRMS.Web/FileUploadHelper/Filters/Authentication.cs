﻿using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace HRMS.Web.Filters
{
    public class Authentication
    {
        public Authentication()
        {

        }

        public IIdentity Identity { get; private set; }

        public Authentication(string email)
        {
            this.Identity = new GenericIdentity(email);
        }
        public UserContextViewModel UserContext
        {
            get;
            set;
        }

        public bool IsInRole(string role)
        {
            if ((UserContext != null))
            {
                string[] roleArray = role.Split(',');
                for (int i = 0; i < roleArray.Length; i++)
                {
                   // bool isContain = UserContext.Permisssion.Contains(roleArray[i]);
                //    if (isContain == true)
                //    {
                //        return true;
                //    }
                }
            }

            return false;
        }
    }
}