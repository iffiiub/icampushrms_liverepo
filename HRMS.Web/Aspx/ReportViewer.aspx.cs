﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using HRMS.Web.CommonHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Web.Aspx
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        ReportDocument _reportDoc = new ReportDocument();

        protected void Page_Init(object sender, EventArgs e)
        {
            CrViewer.HasCrystalLogo = false;


            //Get ReportDocPersister from cache of unique key
            string key = GetQueryStringValueString("r");

            if (Cache[key] != null)
            {
                ReportDocumentPersister reportDocumentPersister = Cache[key] as ReportDocumentPersister;

                if (reportDocumentPersister != null)
                {
                    _reportDoc = new ReportDocument();
                    _reportDoc.Load(reportDocumentPersister.ReportPhysicalPath);

                    if (reportDocumentPersister.SubReports != null)
                    {
                        for (int subRptIdx = 0; subRptIdx < reportDocumentPersister.SubReports.Count; subRptIdx++)
                        {
                            CustomSubReport subReport = reportDocumentPersister.SubReports[subRptIdx];
                            if (subReport.DataSource != null)
                            {
                                _reportDoc.Subreports[subReport.SubReportName].SetDataSource(subReport.DataSource);
                            }
                        }
                    }

                    if (reportDocumentPersister.IsMultipleTables)
                    {
                        int k = 0;
                        foreach (DataTable dt in reportDocumentPersister.TableCollections.Tables)
                        {
                            _reportDoc.Database.Tables[k].SetDataSource(dt);
                            k++;
                        }
                    }

                    else if (reportDocumentPersister.DataSource != null)
                    {
                        _reportDoc.SetDataSource(reportDocumentPersister.DataSource);
                    }

                    if (reportDocumentPersister.ParameterFieldList != null)
                    {
                        foreach (CustomParameterField paramField in reportDocumentPersister.ParameterFieldList)
                        {
                            if (paramField.ParameterValue != null)
                            {
                                if (IsReportParameterExists(_reportDoc, paramField.ParameterName))
                                {
                                    _reportDoc.SetParameterValue(paramField.ParameterName, paramField.ParameterValue.ToString());
                                }
                            }
                        }
                    }

                    if (reportDocumentPersister.ExportData)
                    {
                        pnl.Visible = false;
                        CrViewer.Visible = false;
                        ExportOptions CrExportOptions;
                        CrExportOptions = _reportDoc.ExportOptions;
                        {
                            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                            CrExportOptions.DestinationOptions = reportDocumentPersister.ExportFileDestinationOptions;
                            CrExportOptions.FormatOptions = new PdfRtfWordFormatOptions();

                        }
                        _reportDoc.Export();
                    }
                    else
                    {
                        ShowReport();
                    }
                }
            }
            else
            {

            }
        }
        public bool IsReportParameterExists(ReportDocument reportDoc, string parameterName)
        {
            bool result = false;
            foreach (ParameterFieldDefinition param in reportDoc.DataDefinition.ParameterFields)
            {
                if (param.ParameterFieldName.Equals(parameterName, StringComparison.OrdinalIgnoreCase))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
        public string GetQueryStringValueString(string queryStringName)
        {
            string result = string.Empty;
            if (HttpContext.Current != null && HttpContext.Current.Request.QueryString[queryStringName] != null)
                result = Convert.ToString(HttpContext.Current.Request.QueryString[queryStringName]);
            return result;
        }

        protected void ShowReport()
        {
            try
            {
                if (_reportDoc != null)
                {
                    CrViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
                    CrViewer.ReportSource = _reportDoc;
                }
            }
            catch
            {
                throw;
            }
        }

        private void ShowMessage(string Message)
        {
            CrViewer.Visible = false;
            lblMessage.Text = Message;
            lblMessage.Visible = true;
        }

        protected void CrViewer_Load(object sender, EventArgs e)
        {

        }

        protected void CrViewer_Unload(object sender, EventArgs e)
        {

        }
        protected void Page_UnLoad(object sender, EventArgs e)
        {
            if (_reportDoc != null)
            {
                _reportDoc.Close();
                _reportDoc.Dispose();
                GC.Collect();
            }
        }
    }
}