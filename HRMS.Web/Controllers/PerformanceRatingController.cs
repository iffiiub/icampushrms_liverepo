﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class PerformanceRatingController : BaseController
    {
        // GET: PerformanceRating
        public ActionResult Index()
        {
            List<SelectListItem> lstDeductionType = new List<SelectListItem>();
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYearList();
            int currentAcademicYear = listAcYear.Where(x => x.IsActive).FirstOrDefault().AcademicYearId;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            List<AcademicYearModel> objAcYearList = new List<AcademicYearModel>();
            objAcYearList = objAcademicYearDB.GetAllAcademicYear();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Year", Value = "0" });
            foreach (var item in objAcYearList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.Duration, Value = item.AcademicYearId.ToString() });
            }

            string AcademicYears = "<option value='0'>Select Year</option>";
            foreach (var m in objAcYearList)
            {
                AcademicYears += "<option value='" + m.AcademicYearId + "'>" + m.Duration + "</option>";
            }
            ViewBag.AcademicYears = AcademicYears;

            ViewBag.AcademicYear = ObjSelectedList;
            return View();
        }

        public ActionResult ViewPerformanceratingList(bool ShowActiveOnly, bool ShowAll, int? AcademicYearId)
        {
            List<PerformanceRatingModel> performanceRatelist = new List<PerformanceRatingModel>();
            PerformanceRatingDB objPerformanceRatingDB = new PerformanceRatingDB();
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<AcademicYearModel> AcYearlist = new List<AcademicYearModel>();
            performanceRatelist = objPerformanceRatingDB.GetPerformanceRatingList(ShowActiveOnly, AcademicYearId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in performanceRatelist
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick=\"edit(this," + item.RateIndex + "," + item.PerformanceRatingID.ToString() + "," + item.EmployeeId + "," + item.Rate.ToString() + "," + item.AcademicYearId.ToString() + ",'" + item.AcademicYear + "','" + item.Note + "')\" title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='deletePerformanceRate(" + item.PerformanceRatingID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                              EmpAltID = item.EmployeeAltId,
                              EmployeeName = item.EmployeeName,
                              Rate = item.Rate,
                              AcademicYear = item.AcademicYear,
                              Note = item.Note
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult SaveRate(string PerformanceRateDate, int AcYearId)
        {
            List<PerformanceRatingModel> lstSelectedEmployees = new List<PerformanceRatingModel>();
            lstSelectedEmployees = JsonConvert.DeserializeObject<List<PerformanceRatingModel>>(PerformanceRateDate);
            OperationDetails op = new OperationDetails();
            PerformanceRatingDB objPerformanceRateDB = new PerformanceRatingDB();
            foreach (var item in lstSelectedEmployees)
            {
                op = objPerformanceRateDB.AddUpdatePerformanceRate(item.PerformanceRatingID, item.EmployeeId, item.Rate, AcYearId, item.Note, 1);
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckAlreadyExist(string PerformanceRateDate, int AcYearId)
        {
            List<PerformanceRatingModel> lstSelectedEmployees = new List<PerformanceRatingModel>();
            lstSelectedEmployees = JsonConvert.DeserializeObject<List<PerformanceRatingModel>>(PerformanceRateDate);
            OperationDetails op = new OperationDetails();
            PerformanceRatingDB objPerformanceRateDB = new PerformanceRatingDB();
            string EmployeeIds = "";
            EmployeeIds = string.Join(",", lstSelectedEmployees.Select(m => m.EmployeeId));
            op = objPerformanceRateDB.CheckEmployeeExistence(EmployeeIds, AcYearId);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteRate(int PerformanceRateId)
        {
            OperationDetails op = new OperationDetails();
            op = new PerformanceRatingDB().AddUpdatePerformanceRate(PerformanceRateId, 0, 0, 0, "", 3);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPerformancerating()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            PerformanceRatingModel PerformanceRating = new PerformanceRatingModel();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> objAcYearList = new List<AcademicYearModel>();
            objAcYearList = objAcademicYearDB.GetAllAcademicYear();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Year", Value = "0" });
            foreach (var item in objAcYearList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.Duration, Value = item.AcademicYearId.ToString() });
            }
            ViewBag.AcademicYear = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Employee", Value = "" });
            foreach (var item in new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.FullName, Value = item.EmployeeId.ToString() });
            }
            ViewBag.EmployeeList = ObjSelectedList;

            return View();
        }

        public ActionResult GetEmployeesNotInSelection(string EmpIds, int SelectedEmployeeId)
        {
            List<Employee> lstSelectedEmployees = new List<Employee>();
            if (!string.IsNullOrEmpty(EmpIds))
            {
                lstSelectedEmployees = JsonConvert.DeserializeObject<List<Employee>>(EmpIds);
            }
            string EmployeeIds = "";
            EmployeeIds = string.Join(",", lstSelectedEmployees.Select(m => m.EmployeeId));
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<Employee> EmpList = objEmployeeDB.GetALLEmployeeListNotinSelection(EmployeeIds).Where(x => x.IsActive == 1).OrderBy(x => x.FullName).ToList();
            string EmployeeName = "<option value=''>Select Employee</option>";
            foreach (var m in EmpList)
            {
                if (m.EmployeeId == SelectedEmployeeId)
                {
                    EmployeeName += "<option value='" + m.EmployeeId + "' selected>" + m.FullName + "</option>";
                }
                else
                {
                    EmployeeName += "<option value='" + m.EmployeeId + "'>" + m.FullName + "</option>";
                }

            }
            return Json(EmployeeName, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateRate(int PerformanceRateId, int EmpID, decimal Rate, int AcYearId, string Note)
        {
            OperationDetails op = new OperationDetails();
            op = new PerformanceRatingDB().AddUpdatePerformanceRate(PerformanceRateId, EmpID, Rate, AcYearId, Note, 1);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
    }
}