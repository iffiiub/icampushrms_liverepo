﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class ShiftGRoupController: BaseController
    {
        //
        // GET: /Department/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetShiftGroupList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, string search = "")
        {

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<HRMS.Entities.ShiftGRoupModel> objShiftGroupList = new List<HRMS.Entities.ShiftGRoupModel>();
            ShiftGRoupDB objShiftGRoupDB = new ShiftGRoupDB();
                
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            objShiftGroupList = objShiftGRoupDB.GetAllShiftGRoup(pageNumber, numberOfRecords, sortColumn, sortOrder, objUserContextViewModel.CompanyId, search);


            //---------------------------------------------

            var vList = new object();
            totalCount = objShiftGroupList.Count();
            vList = new
            {
                aaData = (from item in objShiftGroupList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.GroupNameID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.GroupNameID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewShiftGroup(" + item.GroupNameID.ToString() + ")' title='View Details' ><i class='fa fa-eye'></i></a>",
                              GroupNameID = item.GroupNameID,
                              CompanyId = item.CompanyId,
                              GroupNameEn = item.GroupNameEn,
                              GroupNameAr = item.GroupNameAr,
                              GroupNameFr = item.GroupNameFr,
                              Head = item.Head,
                              
                              
                              //Assistant1 = item.Assistant1,
                              //Assistant2 = item.Assistant2,
                              //Assistant3 = item.Assistant3,
                              //Section = item.Section,
                              //Shift = item.Shift,
 
                              
                               
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();

            foreach (var m in ObjGenderDB.GetTypes("stp_GetEmployeeSectionName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }

            ViewBag.SectionList = ObjSelectedList;

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            HRMS.Entities.ShiftGRoupModel objShiftGroup = new HRMS.Entities.ShiftGRoupModel();
            ShiftGRoupDB objShiftGRoupDB = new ShiftGRoupDB();

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FirstName");

            ShiftDB objShiftDb = new ShiftDB();
            ViewBag.ShiftList = new SelectList(objShiftDb.GetAllShiftNames(objUserContextViewModel.CompanyId), "ShiftID", "ShiftName");

            if (id > -1)
            {
                objShiftGroup = objShiftGRoupDB.GetShiftGRoupByID(id);
            }
            else
            {
                objShiftGroup.GroupNameID = id;
            }

            return View(objShiftGroup);
        }

        public ActionResult ViewDetails(int id)
        {
            HRMS.Entities.ShiftGRoupModel objShiftGroup = new HRMS.Entities.ShiftGRoupModel();
            ShiftGRoupDB objShiftGRoupDB = new ShiftGRoupDB();

            
             

            if (id != 0)
            {
                objShiftGroup = objShiftGRoupDB.GetShiftGRoupByID(id);
                 
                EmployeeDB ObjEmployeeDb = new EmployeeDB();
             if(objShiftGroup.Assistant1 !=  0)
             {
           
         objShiftGroup.Assistant1Name =  ObjEmployeeDb.GetEmployee(objShiftGroup.Assistant1).FirstName;
                       }

                    if(objShiftGroup.Assistant2 != 0)
             {
           
         objShiftGroup.Assistant2Name =  ObjEmployeeDb.GetEmployee(objShiftGroup.Assistant2).FirstName;
                       }
                
                    if(objShiftGroup.Assistant3 != 0)
             {
           
         objShiftGroup.Assistant3Name =  ObjEmployeeDb.GetEmployee(objShiftGroup.Assistant3).FirstName;
                       }
                    if(objShiftGroup.Head != 0)
             {
           
         objShiftGroup.HeadName =  ObjEmployeeDb.GetEmployee(objShiftGroup.Head).FirstName;
                       }

                    if (objShiftGroup.Shift != 0)
                    {
                        ShiftDB objShiftDb = new ShiftDB();
                        objShiftGroup.ShiftName = objShiftDb.GetShiftById(objShiftGroup.Shift)[0].ShiftName;
                    }
                 
                 





            }

            return View(objShiftGroup);
        }

        public ActionResult Save(ShiftGRoupModel objShiftGroup)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ShiftGRoupDB objShiftGRoupDB = new ShiftGRoupDB();

                if (objShiftGroup.GroupNameID == -1)
                {
                    objShiftGroup.CompanyId = objUserContextViewModel.CompanyId;
                    objShiftGroup.CreatedBy = objUserContextViewModel.UserId;
                    objShiftGroup.CreatedOn = DateTime.Now;
                    objShiftGRoupDB.AddShiftGRoup(objShiftGroup);

                    // Add class
                    result = "success";
                    resultMessage = "ShiftGroup Added Successfully.";
                }
                else
                {
                    objShiftGroup.CompanyId = objUserContextViewModel.CompanyId;
                    objShiftGroup.ModifiedBy = objUserContextViewModel.UserId;
                    objShiftGroup.ModifiedOn = DateTime.Now;
                    objShiftGRoupDB.UpdateShiftGRoup(objShiftGroup);

                    // Add class
                    result = "success";
                    resultMessage = "ShiftGroup Updated Successfully.";
                }
            }
            catch (Exception)
            {
                result = "error";
                resultMessage = "Error occured while adding ShiftGroup Details.";
            }

            //return Redirect("Index");
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            ShiftGRoupDB objShiftGRoupDB = new ShiftGRoupDB();
            HRMS.Entities.ShiftGRoupModel objShiftGroup = new HRMS.Entities.ShiftGRoupModel();
            objShiftGroup.GroupNameID = int.Parse(id);
            objShiftGRoupDB.DeleteShiftGRoupByID(objShiftGroup.GroupNameID);
            //return Redirect("Index");

            return Json(new { result = "success", resultMessage = "ShiftGroup Deleted Successfully." }, JsonRequestBehavior.AllowGet);

        }


        ///// Export content to List
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="listToExport"></param>
        ///// <returns></returns>
        ///// 
        //public object ExportListToExcel()
        // {

        //   var dtExportTable =  GetDepartmentList();
        //    //System.Data.DataTable dtExportTable;
        //    try
        //    {
        //        GridView gridvw = new GridView();
        //        gridvw.DataSource = dtExportTable; //bind the data table to the grid view
        //        gridvw.DataBind();
        //        StringWriter swr = new StringWriter();
        //        HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        //        gridvw.RenderControl(tw);
        //        byte[] content = Encoding.ASCII.GetBytes(swr.ToString());
        //        return content;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public ActionResult ExportToExcel()
        //{
        //    byte[] content;
        //    string fileName = "Department" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    ShiftGRoupDB ShiftGRoupDB = new ShiftGRoupDB();

        //    List<ShiftGRoupModel> departmentList = ShiftGRoupDB.GetDepartmentList(objUserContextViewModel.CompanyId);
        //    var report = (from item in departmentList.AsEnumerable()
        //                  select new
        //                  {
        //                      //Id = item.Id,
        //                      GroupNameID = item.GroupNameID,
        //                      Name = item.DepartmentName_1,
        //                      Description = item.Description,
        //                      //StartDate = item.StartDate.Value,
        //                      CreatedBy = item.CreatedBy

        //                  }).ToList();
        //    content = ExportListToExcel(report);
        //    return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        //}

        //public void ExportToPdf()
        //{
        //    Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

        //    string fileName = "Department" + DateTime.Now.ToString("ddMMyyyy") + ".pdf";


        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    ShiftGRoupDB ShiftGRoupDB = new ShiftGRoupDB();


        //    List<ShiftGRoupModel> departmentList = ShiftGRoupDB.GetDepartmentList(objUserContextViewModel.CompanyId);
        //    var report = (from item in departmentList.AsEnumerable()
        //                  select new
        //                  {
        //                      //Id = item.Id,
        //                      GroupNameID = item.GroupNameID,
        //                      Name = item.DepartmentName_1,
        //                      Description = item.Description,

        //                      //StartDate = item.StartDate.Value,
        //                      CreatedBy = item.CreatedBy

        //                  }).ToList();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    GridView gridvw = new GridView();
        //    gridvw.DataSource = report; //bind the data table to the grid view
        //    gridvw.DataBind();
        //    StringWriter swr = new StringWriter();
        //    HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        //    gridvw.RenderControl(tw);
        //    StringReader sr = new StringReader(swr.ToString());
        //    Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    pdfDoc.Open();
        //    htmlparser.Parse(sr);
        //    pdfDoc.Close();
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}

    }
}