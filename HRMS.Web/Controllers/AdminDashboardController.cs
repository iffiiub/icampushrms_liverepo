﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class AdminDashboardController : BaseController
    {
        // GET: AdminDashboard
        public AdminDashboardController()
        {
            XMLLogFile = "LoggerAdminDashboard.xml";
        }

        public ActionResult Index()
        {
            AdminDashboardDB adminDashboardDB = new AdminDashboardDB();
            AdminDashboardModel adminDashboardModel = new AdminDashboardModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<Employee> empList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList<Employee>();
            ViewBag.EmployeeList = new SelectList(empList, "EmployeeId", "FullName");
            ViewBag.RequestList = new SelectList(adminDashboardDB.GetRequestList(Convert.ToInt32(RequestStatus.Pending), null), "id", "text");
            return View(adminDashboardModel);
        }

        #region Manage Probation Confirmation
        public ActionResult CheckProbationConfirmationRequestConditions(int employeeId)
        {
            AdminDashboardDB adminDashboardDB = new AdminDashboardDB();
            OperationDetails operationDetails = new OperationDetails();
            int result = adminDashboardDB.CheckProbationConfirmationIsCompletedForSelectedEmployee(employeeId);
            if(result == 0)
                operationDetails = adminDashboardDB.UpdateProbationCompletionDateManual(employeeId);
            else
            {
                operationDetails.Success = false;
                operationDetails.CssClass = "error";
                operationDetails.Message = "Selected employee has already completed the probation.";
            }                            
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateProbationConfirmationRequest(int employeeId)
        {
            int result = 0;
            AdminDashboardDB adminDashboardDB = new AdminDashboardDB();
            OperationDetails operationDetails = new OperationDetails();
            operationDetails = CheckRequestAndSupervisorValidations(employeeId);
            if(operationDetails.Success == true)
            {
                result = adminDashboardDB.InitializeProbationConfirmationRequest(employeeId);
                operationDetails.Success = result > 0 ? true : false;
                operationDetails.CssClass = result > 0 ? "success" : "error";
                operationDetails.Message = result > 0 ? "Probation Confirmation request generated successfully." : "some error has occured";
            }            
            return Json(operationDetails, JsonRequestBehavior.AllowGet);        
        }

        public OperationDetails CheckRequestAndSupervisorValidations(int employeeId)
        {
            AdminDashboardDB adminDashboardDB = new AdminDashboardDB();
            OperationDetails operationDetails = adminDashboardDB.IsRequestAndSupervisorIsPresentForSelectedEmployee(employeeId);
            if (operationDetails.InsertedRowId > 0)
            {
                operationDetails.Success = false;
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = true;
                operationDetails.CssClass = "success";

            }          
            return operationDetails;
        }
        #endregion

        #region Workflow Rerouting
        public ActionResult GetPendingApproverEmployeeByRequestId(int requestId)
        {
            AdminDashboardDB adminDashboardDB = new AdminDashboardDB();
            string result = adminDashboardDB.GetPendingApproverEmployeeByRequestId(requestId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult UpdateRequestApprovarEmpoyee(int requestId, int employeeId)
        {
            AdminDashboardDB adminDashboardDB = new AdminDashboardDB();
            OperationDetails operationDetails = adminDashboardDB.UpdateRequestApprovarEmpoyee(requestId, employeeId);
            if (operationDetails.Success)
            {                
                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Approver of Request updated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.InsertedRowId = -1;
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}