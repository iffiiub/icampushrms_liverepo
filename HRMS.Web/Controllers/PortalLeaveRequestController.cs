﻿using HRMS.DataAccess;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class PortalLeaveRequestController : BaseController
    {
        // GET: PortalLeaveRequest
        public ActionResult Index()
        {
            return View();
        }
        
        public JsonResult GetPortalLeaveRequest()
        {
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            VacationDB VacationDB = new VacationDB();
            IEnumerable<VacationModel> modelList;
            modelList = VacationDB.GetPortalVacationRequests();
            var vList = new
            {
                aaData = (from item in modelList.OrderBy(x => x.employeeName)
                          select new
                          {
                              EmployeeId = item.employeeId,
                              EmployeeAlternativeId = item.employeeAlternativeId,
                              EmployeeName = item.employeeName,
                              VacationType = item.vacationType,
                              FromDate = item.fromDate,
                              ToDate = item.toDate,
                              NoOFDays = item.NoOfDays,
                              Reason = item.Reason,
                              Status = item.Status.ToString(),
                              Comments = item.Comments,
                              RejectReason = item.RejectReason,
                              Action = item.StatusId == 0 ? "<button type='button' id='btnRowSave' class='tableButton' onclick='ApproveRequest(" + item.id + ")' class='btnAdd' title='Approve'><img src='/Content/images/tick.ico' style='width:16px;'/></button><button class='tableButton' id='btnRowCancel' onClick='RejectRequest(" + item.id + ")' class='btnRemove' title='Reject'><img src='/Content/images/cross.ico' style='width:16px;'/></button>" : "",
                          }).ToArray(),
                recordsTotal = modelList.Count(),
                recordsFiltered = modelList.Count()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult approveRequest(int RequestId)
        {
            VacationDB objVacationdDB = new VacationDB();
            VacationModel vc = objVacationdDB.GetPortalVacationRequestByID(RequestId);
            return Json(vc, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult RejectRequest(int RequestId)
        {
            VacationDB objVacationdDB = new VacationDB();
            VacationModel vc = objVacationdDB.GetPortalVacationRequestByID(RequestId);
            return PartialView(vc);
        }

        [HttpPost]
        public ActionResult RejectRequestPost(VacationModel objVacation)
        {
            UserContextViewModel model = (UserContextViewModel)Session["userContext"];
            OperationDetails op = new OperationDetails();
            VacationDB objVacationDB = new VacationDB();
            objVacation.StatusId = 2;
            op = objVacationDB.UpdatePortalLeaveRequestStatus(objVacation, model.UserId);
            if(op.Success)
            {
                EmployeeDB objEmpDB = new EmployeeDB();
                EmployeeContactModel EC = objEmpDB.GetEmployeeWorkEmailDetailsByEmployeeID(objVacation.employeeId);
                EmployeeContactModel MonitorUserDetails = objEmpDB.GetEmployeeWorkEmailDetailsByEmployeeID(model.UserId);
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += "-" + sc.CampusName_1;
                }
                string EmailSubject = Schoolname + " - Leave Request Rejected";
                string EmailBody = "Dear " + EC.EmployeeName + ",<br /><br /> Your leave request have been rejected by " + MonitorUserDetails.EmployeeName + ".Please have a look below the rejection reason.<br />" +
                                   "<i>" + objVacation.RejectReason + "</i><br />" +
                                   "You can contact with school administration in case of any question.<br /><br />" +
                                   "Thank You";
                CommonHelper.CommonHelper objCommon = new CommonHelper.CommonHelper();
                objCommon.SendMail(EC.WorkEmail, EmailSubject, EmailBody);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePortalVactionRecord(string mode, int id, string fromDate, string toDate, int typeId,int requestId)
        {
            OperationDetails op = new OperationDetails();
            VacationDB vacationdb = new VacationDB();
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            op = vacationdb.SaveVationDate(mode, id, fromDate, toDate, typeId, CommonDB.GetUserId(userContext));
            VacationModel objVacation = new VacationModel();
            objVacation.id = requestId;
            objVacation.StatusId = 1;
            objVacation.RejectReason = "";
            objVacation.Comments = "";
            // objVacation.Comments = "Approved";
            op = vacationdb.UpdatePortalLeaveRequestStatus(objVacation, userContext.UserId);
            if (op.Success)
            {
                op.Message = "Leave request approved successfully.";
                EmployeeDB objEmpDB = new EmployeeDB();
                EmployeeContactModel EC = objEmpDB.GetEmployeeWorkEmailDetailsByEmployeeID(id);
                EmployeeContactModel MonitorUserDetails = objEmpDB.GetEmployeeWorkEmailDetailsByEmployeeID(userContext.UserId);
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += "-" + sc.CampusName_1;
                }
                string EmailSubject = Schoolname + " -  Leave Request Get Approved";
                string EmailBody = "Dear " + EC.EmployeeName + ",<br /><br />Your leave request have been approved by " + MonitorUserDetails.EmployeeName + ".<br />" +
                                   "You can contact with school administration in case of any question.<br /><br />" +
                                   "Thank You";
                CommonHelper.CommonHelper objCommon = new CommonHelper.CommonHelper();
                if (!string.IsNullOrEmpty(EC.WorkEmail))
                {
                    objCommon.SendMail(EC.WorkEmail, EmailSubject, EmailBody);
                }                
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult PortalLeaveRequestConfirmation()
        {
            VacationDB objVacDB = new VacationDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            List<VacationModel> RequstList = null;
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.IsPermisssion = CommonHelper.CommonHelper.checkUrlPermission("/PortalLeaveRequest", "Url");
            if (ViewBag.IsPermisssion)
            {
                RequstList = objVacDB.GetPortalVacationRequests().Where(m => m.StatusId == 0).ToList();
            }
            else
            {
                RequstList = new List<VacationModel>();
            }

            return PartialView("~/Views/PortalLeaveRequest/PortalLeaveRequestConfirmation.cshtml", RequstList.Count);
        }
    }
}