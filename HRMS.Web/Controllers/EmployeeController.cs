﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.Entities.General;
using HRMS.DataAccess.GeneralDB;
using HRMS.Web.Filters;
using System.Web.Script.Serialization;
using HRMS.Web.Controllers;
using HRMS.Entities.ViewModel;
using System.Configuration;
using HRMS.Web.FileUpload;
using System.IO;
using Newtonsoft.Json;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Globalization;

namespace HRMS.Web.Areas.Employee.Controllers
{
    public class EmployeeController : BaseController
    {
        //
        // GET: /Employee/Employee/
        //Modified by Mahesh 29-09-2016

        public void GetDropDowns(int? companyId = null)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];


            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> ObjSelectedList = null;


            GenderDB ObjGenderDB = new GenderDB();
            LanguageDB ObjLanguageDB = new LanguageDB();
            MaritialStatusDB ObjMaritialStatusDB = new MaritialStatusDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            DepartmentDB ObjDepartmentDB = new DepartmentDB();
            PositionDB ObjPositionDB = new PositionDB();
            EmployeeDB ObjEmployeeDB = new EmployeeDB();
            CountryDB objCountryDB = new CountryDB();
            PassportIssueDB objPassportIssueDB = new PassportIssueDB();
            PassportExpRemDB objPassportRemDB = new PassportExpRemDB();
            AccessRoleDB objAccessRoleDB = new AccessRoleDB();
            BankDB objBankDB = new BankDB();
            JobDB objJobDb = new JobDB();
            ReligionDB objReligionDB = new ReligionDB();
            CompanyDB objCompanyDB = new CompanyDB();
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            JobCategoryDB objJobCategoryDB = new JobCategoryDB();
            ShiftDB objShiftDB = new ShiftDB();
            ShiftGRoupDB objShiftGRoupDB = new ShiftGRoupDB();
            MOETitleDB objMOETitleDB = new MOETitleDB();
            MOLTitleDB objMOLTitleDB = new MOLTitleDB();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            StatusDB objStatusDb = new StatusDB();
            LeaveReasonDB objLeaveReasonDB = new LeaveReasonDB();

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select MOL.", Value = "0" });

            foreach (var m in objMOLTitleDB.GetAllMOLTitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MOLTitleName_1, Value = m.MOLTitleID.ToString() });

            }
            ViewBag.MOLTitleList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Nationality Type", Value = "0" });
            foreach (var item in ObjNationalityDB.GetNationalityTypeList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.NationalityType + " - " + item.NationalityTypeDesc, Value = item.NationalityTypeId.ToString() });
            }
            ViewBag.NationalityTypeList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select MOE. ", Value = "0" });

            foreach (var m in objMOETitleDB.GetAllMOETitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MOETitleName_1, Value = m.MOETitleID.ToString() });

            }
            ViewBag.MOETitleList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Shift Group", Value = "0" });

            foreach (var m in objShiftGRoupDB.GetAllShiftGRoupList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.GroupNameEn, Value = m.GroupNameID.ToString() });

            }
            ViewBag.ShiftGroupList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Shift", Value = "0" });

            foreach (var m in objShiftDB.GetAllShiftNames(CompanyId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.ShiftName, Value = m.ShiftID.ToString() });

            }
            ViewBag.ShiftList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "N/A", Value = "N/A" });
            ObjSelectedList.Add(new SelectListItem { Text = "N", Value = "N" });
            ObjSelectedList.Add(new SelectListItem { Text = "E", Value = "E" });
            ObjSelectedList.Add(new SelectListItem { Text = "C", Value = "C" });
            ViewBag.EmployeeStatus = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Job Category", Value = "0" });

            foreach (var m in objJobCategoryDB.GetJobCategory())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.EmployeeJobCategoryName_1, Value = m.EmployeeJobCategoryID.ToString() });

            }
            ViewBag.JobCategoryList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Section", Value = "0" });

            //foreach (var m in objEmployeeSectionDB.GetEmployeeSection())
            //{
            //    ObjSelectedList.Add(new SelectListItem { Text = m.EmployeeSectionName_1, Value = m.EmployeeSectionID.ToString() });

            //}
            ViewBag.SectionList = ObjSelectedList;

            //ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Organization", Value = "0" });

            //foreach (var m in objCompanyDB.GetAllCompanyList())
            //{
            //    ObjSelectedList.Add(new SelectListItem { Text = m.name, Value = m.CompanyId.ToString() });

            //}
            ViewBag.CompanyList = GetCompanySelectList();

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Religion", Value = "0" });

            foreach (var m in objReligionDB.GetAllReligion())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.ReligionName_1, Value = m.ReligionID.ToString() });

            }
            ViewBag.ReligionList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select language", Value = "0" });

            foreach (var m in ObjLanguageDB.GetAllLanguages())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.LanguageName_1, Value = m.LanguageID.ToString() });

            }
            ViewBag.LanguageList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Marital Status", Value = "0" });

            foreach (var m in ObjMaritialStatusDB.getAllMaritialStatus())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MaritalStatusName_1, Value = m.MaritalStatusID.ToString() });

            }
            ViewBag.MaritalStatus = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            //foreach (var m in ObjNationalityDB.getAllNationalities())
            //{
            //    ObjSelectedList.Add(new SelectListItem { Text = m.NationalityName_1, Value = m.NationalityID.ToString() });

            //}

            //ViewBag.NationalityList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Department", Value = "0" });
            foreach (var m in ObjDepartmentDB.GetAllDepartmentByCompanyId(CompanyId: companyId ?? CompanyId, UserId: objUserContextViewModel.UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.DepartmentName_1, Value = m.DepartmentId.ToString() });

            }
            ViewBag.DepartmentList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Position", Value = "0" });
            foreach (var m in ObjPositionDB.GetPositionList(companyId: companyId, userId: objUserContextViewModel.UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PositionTitle, Value = m.PositionID.ToString() });

            }
            ViewBag.PositionList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Supervisor", Value = "0" });
            foreach (var m in ObjEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });

            }
            ViewBag.SuperviserList = ObjSelectedList;

            //for LEFT REASON dropdown          
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Leave Reason", Value = "0" });
            foreach (var m in objLeaveReasonDB.GetAllLeaveReason())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.LeaveReason_1, Value = m.ID.ToString() });

            }
            ViewBag.LeaveReasonList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Email Type", Value = "0" });
            ViewBag.EmailTypeList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select IMAddress Type", Value = "0" });
            ViewBag.IMAddressTypeList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Web Address", Value = "0" });
            ViewBag.WebAddressList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Mobile Number", Value = "0" });
            ViewBag.MobileNumberList = ObjSelectedList;


            ViewBag.EmployementModeList = GetEmploymentModeList();

            ViewBag.JobTitleList = GetJobTitleList();
            //  ViewBag.QualificationList = GetQulalification();

            ViewBag.NameTitle = GetNameTitles();


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Email Type", Value = "0" });
            foreach (var m in ObjGenderDB.GetTypes("stp_Gen_GetEmailType"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.EmailTypeList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select IM Type", Value = "0" });
            foreach (var m in ObjGenderDB.GetTypes("stp_Gen_GetIMType"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.IMAddressTypeList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Web Type", Value = "0" });
            foreach (var m in ObjGenderDB.GetTypes("stp_Gen_WebType"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.WebAddressList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Mobile Type", Value = "0" });
            foreach (var m in ObjGenderDB.GetTypes("stp_Gen_MobileType"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.MobileNumberList = ObjSelectedList;

            //Get Country List
            ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "0" });
            foreach (var m in objCountryDB.GetAllContries())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });

            }
            ViewBag.CountryList = ObjSelectedList;



            //Get Address type List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Address Type", Value = "0" });
            foreach (var m in ObjEmployeeDB.GetAddressTypeList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.AddressTypeName, Value = m.AddressType.ToString() });

            }
            ViewBag.AddressTypeList = ObjSelectedList;




            // State List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select State", Value = "0" });
            ViewBag.StateList = ObjSelectedList;

            // City List
            ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });
            ViewBag.CityList = ObjSelectedList;

            // BirthPlace List
            ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Birth Place", Value = "0" });
            ViewBag.BirthPlaceList = ObjSelectedList;

            // Area List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Area", Value = "0" });
            ViewBag.AreaList = ObjSelectedList;

            // Passport Issue By
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Passport IssueBy", Value = "0" });
            foreach (var m in objPassportIssueDB.GetAllPassportIssueBy())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PassportIssueName, Value = m.PassportIssueId.ToString() });

            }
            ViewBag.PassportIssueByList = ObjSelectedList;

            // Expiration Reminder
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Expiration Reminder", Value = "0" });
            foreach (var m in objPassportRemDB.GetAllPassportExpReminder())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PassportExpRemName, Value = m.PassportExpRemId.ToString() });

            }
            ViewBag.ExpirationReminderList = ObjSelectedList;

            // Account Status
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Active", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "InActive", Value = "0" });

            ViewBag.AccountStatus = ObjSelectedList;

            // Access Roles
            List<UserGroupModel> objAccessRoleModel = new List<UserGroupModel>();
            objAccessRoleModel = objAccessRoleDB.GetAllAccessRoles();
            ViewBag.AccessRoles = objAccessRoleModel;

            // Bank List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Bank", Value = "0" });
            foreach (var m in objBankDB.GetAllBanks())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.BankName, Value = m.BankId.ToString() });

            }
            ViewBag.Banks = ObjSelectedList;

            // Job Title List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Job Title", Value = "0" });
            foreach (var m in objJobDb.GetJobTitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.JobTitleName_1, Value = m.JobTitleID.ToString() });

            }
            ViewBag.JobTitle = ObjSelectedList;



            // Qualification Title List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Qualification", Value = "0" });
            foreach (var m in objHRQualificationDB.GetAllHRQualification().OrderBy(x => x.HRQualificationName_1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.HRQualificationName_1, Value = m.HRQualificationID.ToString() });

            }
            ViewBag.QualificationList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Contract Term" });
            foreach (var m in objJobDb.GetContractTermList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.ContarctTermName, Value = m.EmployeeContractTermsID.ToString() });

            }
            ViewBag.ContractTermList = ObjSelectedList;

            // Status List
            ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });
            foreach (var m in objStatusDb.GetAllStatus())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.StatusName, Value = m.StatusID.ToString() });

            }
            ViewBag.StatusList = ObjSelectedList.OrderBy(a => a.Text).ToList();

            //// Status List
            //ObjSelectedList = new List<SelectListItem>();
            //// ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });
            //foreach (var m in objStatusDb.GetAllStatus())
            //{
            //    ObjSelectedList.Add(new SelectListItem { Text = m.StatusName, Value = m.StatusID.ToString() });

            //}
            //ViewBag.StatusList = ObjSelectedList;

            //User Type List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select User Type", Value = "" });
            UserTypeDB objUserTypeDB = new UserTypeDB();
            foreach (var m in objUserTypeDB.GetUserTypesForEmployee())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.UserTypeName, Value = m.UserTypeID.ToString() });
            }
            ViewBag.UserTypeList = ObjSelectedList;

            //School House List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select School House", Value = "" });
            foreach (var m in ObjEmployeeDB.GetSchoolHouseList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.HousingName_1, Value = m.HOID.ToString() });
            }
            ViewBag.SchoolHouseList = ObjSelectedList;
            @ViewBag.IBanLength = new BankInformationDB().GetIBanLength();
            @ViewBag.IsIBanMandatory = new CommonDB().GetValidationSettingByName(CommonHelper.CommonHelper.GetMemberName(() => new PayDirectDepositModel().UniAccountNumber)).IsMandatory;
        }

        public ActionResult Index(int Id)
        {


            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            EmployeeDB objEmpDB = new EmployeeDB();
            HRMS.Entities.Employee employeeModel = objEmpDB.GetAllDetailsOfEmployee(Id);
            GetDropDowns(employeeModel?.employmentInformation?.CompanyId);
            employeeModel.lstUserRoleDetails = new List<UserRoleDetails>();
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/BankInformation/Index");
            Session["BankPermission"] = permissionModel;
            foreach (CheckListBoxItem item in employeeModel.userGroupModel.Items)
            {
                UserRoleDetails objUserRole = new UserRoleDetails();
                objUserRole.userRole = item.Text;
                objUserRole.userRoleId = Convert.ToInt32(item.Value);
                foreach (string checkeditem in employeeModel.userGroupModel.SelectedValues)
                {
                    if (item.Value == checkeditem)
                    {
                        objUserRole.isSelected = true;
                        break;
                    }
                    else
                    {
                        objUserRole.isSelected = false;
                    }
                }
                employeeModel.lstUserRoleDetails.Add(objUserRole);
            }
            employeeModel.EmployeeId = Id;
            if (Id == 0)
            {
                employeeModel.userGroupModel.Status = 1;//default to Active
            }

            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();

            bool changeStatusPermission = PermissionList.Where(x => x.OtherPermissionId == 22).Count() > 0 ? true : false;
            ViewBag.changeStatusPermission = changeStatusPermission;

            BankInformationDB bankInformationDB = new BankInformationDB();
            EmployeeBasicDetails objBasicDetails = new EmployeeBasicDetails();
            int PositionID = 0;
            if (Id != 0)
            {
                HRMS.Entities.Employee employee = objEmpDB.GEtBasicEmployeeDetails(Id);
                objBasicDetails.ID = Id;
                objBasicDetails.Name = employee.employeeDetailsModel.FirstName_1 + "   " + employee.employeeDetailsModel.SurName_1;
                objBasicDetails.Position = employee.employmentInformation.PositionName;
                PositionID = employee.employmentInformation.PositionID;
                objBasicDetails.Department = employee.employmentInformation.DepartmentName;
                objBasicDetails.AlternativeId = employee.employeeDetailsModel.EmployeeAlternativeID;
                ObjSelectedList = new List<SelectListItem>();
                ObjSelectedList.Add(new SelectListItem { Text = "Select State", Value = "0" });
                StateDB objState = new StateDB();
                foreach (var item in objState.GetAllStatesByCountryId(employeeModel.employeeAddressInfoModel.CountryID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = item.StateName, Value = item.StateId.ToString() });
                }
                ViewBag.StateList = ObjSelectedList;
                ObjSelectedList = new List<SelectListItem>();
                // ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });
                CityDB objCity = new CityDB();
                foreach (var item in objCity.GetAllCityByStateId(employeeModel.employeeAddressInfoModel.StateID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = item.CityName, Value = item.CityId.ToString() });
                }
                ViewBag.CityList = ObjSelectedList;

                //Birthplace respect to Birth Country
                ObjSelectedList = new List<SelectListItem>();
                foreach (var item in objCity.GetBirthPlaceByCountryId(employeeModel.employeeDetailsModel.BirthCountryID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = item.BirthPlaceName_1, Value = item.BirthPlaceID.ToString() });
                }
                ViewBag.BirthPlaceList = ObjSelectedList;
                ObjSelectedList = new List<SelectListItem>();
                ObjSelectedList.Add(new SelectListItem { Text = "Select Area", Value = "0" });
                AreaDB objArea = new AreaDB();
                foreach (var item in objArea.GetAllAreaByCityId(employeeModel.employeeAddressInfoModel.CityID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = item.AreaName, Value = item.AreaId.ToString() });
                }
                ViewBag.AreaList = ObjSelectedList;
            }
            employeeModel.employeeDetailsModel.BasicDetails = objBasicDetails;
            employeeModel.payDirectDepositModel.EmployeeID = Convert.ToInt32(Id);
            employeeModel.payDirectDepositModel.BankList = new SelectList(bankInformationDB.GetBanksList(), "BankID", "BankName_1");
            employeeModel.payDirectDepositModel.BranchList = new SelectList(bankInformationDB.GetBankBranchesList(), "BranchID", "BranchName_1");
            employeeModel.payDirectDepositModel.CategoriesList = new SelectList(bankInformationDB.GetPayCategoriesList(), "CategoryID", "CategoryName_1");

            List<PayAccountTypesModel> lstAccntType = bankInformationDB.GetPayAccountTypesList();
            PayAccountTypesModel model = new PayAccountTypesModel();
            model.AccountTypeID = 0;
            model.AccountTypeName_1 = "Select Account Type";
            lstAccntType.Insert(0, model);
            employeeModel.payDirectDepositModel.AccountTypeList = new SelectList(lstAccntType, "AccountTypeID", "AccountTypeName_1");

            ViewBag.hdnEmployeeID = Id;

            //Get selected nationalities

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Default Nationality", Value = "0" });
            foreach (HRMS.Entities.CheckListBoxItem item in employeeModel.employeeDetailsModel.Items)
            {
                if (employeeModel.employeeDetailsModel.SelectedValues.Exists(value => value == item.Value))
                    ObjSelectedList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            ViewBag.NationalityList = ObjSelectedList;
            ViewBag.LanguageList = new SelectList(employeeModel.employeeDetailsModel.SpokenLanguages, "LanguageId", "LanguageName");

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Section", Value = "0" });

            foreach (var m in new EmployeeSectionDB().GetEmployeeSectionByDepartmentID(employeeModel.employmentInformation.DepartmentID))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.EmployeeSectionName_1, Value = m.EmployeeSectionID.ToString() });
            }
            ViewBag.SectionList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Salary Package", Value = "0" });
            ObjSelectedList.Add(new SelectListItem { Text = "1", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "6", Value = "6" });
            ObjSelectedList.Add(new SelectListItem { Text = "7", Value = "7" });
            ObjSelectedList.Add(new SelectListItem { Text = "9", Value = "9" });
            ViewBag.SalaryPackage = ObjSelectedList;
            ViewBag.EmpPositionID = PositionID;
            HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
            ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
            ViewBag.SalaryBasisFrequencyList = new SelectList(employeeProfileCreationFormDB.GetSalaryBasisFrequencyList(), "id", "text");
            ViewBag.LeaveEntitlementList = new SelectList(employeeProfileCreationFormDB.GetLeaveEntitlementDaysList(), "id", "text");
            ViewBag.LeaveEntitlementTypeList = new SelectList(employeeProfileCreationFormDB.GetLeaveEntitlementTypeList(), "id", "text");
            ViewBag.DivisionList = new SelectList(employeeProfileCreationFormDB.GetDivisionList(), "id", "text");
            ViewBag.RecCategoryList = new SelectList(employeeProfileCreationFormDB.GetRecruitCategoryList(), "id", "text");
            ViewBag.JobStatusList = new SelectList(employeeProfileCreationFormDB.GetJobStatusList(), "id", "text");
            ViewBag.PDRPFormsList = new SelectList(employeeProfileCreationFormDB.GetPDRPFormsList(), "id", "text");
            ViewBag.InsuranceCategory = new SelectList(new InsuranceDB().GetInsuranceTypeList(), "HRInsuranceTypeID", "InsuranceTypeName_1");
            ViewBag.InsuranceEligibility = new SelectList(new InsuranceDB().GetInsuranceEligibilityList(), "InsuranceEligibilityId", "InsuranceEligibilityName_1");
            ViewBag.AccommodationTypeList = new SelectList(new AllowanceDB().GetAccommodationtype(), "HRAccommodationTypeID", "HRAccommodationTypeName");

            return View(employeeModel);
        }

        public JsonResult UpdateEmployeeDetails(HRMS.Entities.Employee objEmp, IList<string> txtDynamicEmails, IList<string> txtDynamicPersonalEmail, IList<string> ddDynamicEmailType, IList<string> txtDynamicIM, IList<string> ddDynamicIMType, IList<string> txtDynamicMobile, IList<string> txtDynamicPhone, IList<string> ddDynamicMobileType, string hidMultipleIMContact)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            int EmployeeID = 0;
            try
            {
                objEmp.employeeDetailsModel.EmployeeID = objEmp.EmployeeId;
                objEmp.employeeDetailsModel.CompanyID = objEmp.employmentInformation.CompanyId;
                EmployeeID = UpdateEmployeePersonalDetails(objEmp.employeeDetailsModel);
                objEmp.employeeContactModel.EmployeeID = EmployeeID;
                UpdateEmployeeContactDetails(objEmp.employeeContactModel, objEmp.employeeAddressInfoModel, txtDynamicEmails, txtDynamicPersonalEmail, ddDynamicEmailType, txtDynamicIM, ddDynamicIMType, txtDynamicMobile, txtDynamicPhone, ddDynamicMobileType, hidMultipleIMContact);
                //objEmp.employeeAddressInfoModel.EmployeeID = EmployeeID;
                //UpdateEmployeeAddress(objEmp.employeeAddressInfoModel);
                objEmp.employmentInformation.EmployeeID = EmployeeID;
                UpdateEmploymentInformation(objEmp.employmentInformation);
                objEmp.payDirectDepositModel.EmployeeID = EmployeeID;
                SaveBankInformation(objEmp.payDirectDepositModel);
                objEmp.userGroupModel.EmployeeID = EmployeeID;
                objEmp.userGroupModel.Status = objEmp.employeeDetailsModel.isActive.HasValue && objEmp.employeeDetailsModel.isActive.Value ? 1 : 0;
                foreach (var item in objEmp.lstUserRoleDetails)
                {
                    if (item.isSelected)
                    {
                        objEmp.userGroupModel.SelectedValues.Add(item.userRoleId.ToString());
                    }
                }
                UpdateEmployeeAccountInformation(objEmp.userGroupModel);
                objEmp.employeePersonalIdentityInfoModel.EmployeeID = EmployeeID;
                UpdateEmployeePersonalIdentity(objEmp.employeePersonalIdentityInfoModel);
                result = "success";
                resultMessage = "Employee details saved successfully";
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error while saving employee details";
            }
            ViewBag.hdnEmployeeID = operationDetails.InsertedRowId;
            return Json(new { EmployeeID = EmployeeID, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddType(int ID)
        {
            string PostAddress = "";
            string title = "";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CreatedBy = 0;
            switch (ID)
            {
                case 1:
                    PostAddress = "AddEmailType";
                    title = "Email Type:";
                    break;

                case 2:
                    PostAddress = "IMAddressType";
                    title = "Address Type";
                    break;

                case 3:
                    PostAddress = "WebAddressType";
                    title = "Web Address:";
                    break;

                case 4:
                    PostAddress = "MobileNoType";
                    title = "Mobile Type";
                    break;
                case 5:
                    PostAddress = "AddExpirationReminder";
                    title = "Expiration Reminder:";
                    break;
                case 6:
                    PostAddress = "AddNationality";
                    title = "Nationality Name:";
                    break;
                case 7:
                    PostAddress = "AddBank";
                    title = "Bank Name:";
                    break;
                case 8:
                    PostAddress = "AddAreaForCity";
                    ViewBag.AddCityId = "8";
                    title = "Area Name:";
                    break;

                case 9:
                    PostAddress = "AddCompany";
                    title = "Company Name";
                    break;

                case 10:
                    PostAddress = "AddEmployeeSection";
                    title = "Section Name:";
                    break;


                case 11:
                    PostAddress = "AddDutyShift";
                    title = "Shift Duty Name:";
                    break;


                case 12:
                    PostAddress = "AddJobCategory";
                    title = "Job Category Name:";
                    break;


                case 13:
                    PostAddress = "AddShiftGroup";
                    title = "Shift Group Name:";
                    break;


                case 14:
                    PostAddress = "AddBirthCountry";
                    title = "Birth Country Name:";
                    break;


                case 15:
                    PostAddress = "AddReligion";
                    title = "Religion Name:";
                    break;

                case 16:
                    PostAddress = "AddMaritalStatus";
                    title = "Marital Status Name:";
                    break;

                case 17:
                    PostAddress = "AddAddressType";
                    title = "Address Name:";
                    break;

                case 18:
                    PostAddress = "AddCountry";
                    title = "Country Name:";
                    break;
                case 19:
                    PostAddress = "AddStateForCountry";
                    ViewBag.AddCityId = "19";
                    title = "State Name:";
                    break;


                case 20:
                    PostAddress = "AddCityForState";
                    ViewBag.AddCityId = "20";
                    title = "City Name:";
                    break;
                case 21:
                    PostAddress = "AddLanguage";
                    title = "Language Name:";
                    break;

                case 22:
                    PostAddress = "AddCourse";
                    title = "Course Name:";
                    break;

                case 23:
                    PostAddress = "AddPosition";
                    title = "Position Name:";
                    break;

                case 24:
                    PostAddress = "AddJobTitle";
                    title = "Job Title:";
                    break;
                case 25:
                    PostAddress = "AddMOETitle";
                    title = "MOE Title:";
                    break;

                case 26:
                    PostAddress = "AddMOLTitle";
                    title = "MOE Title:";
                    break;

                case 27:
                    PostAddress = "AddAccessRoleName";
                    title = "Role Name:";
                    break;

                case 28:
                    PostAddress = "AddPassportIssueByName";
                    title = "Passport Issue Name:";
                    break;


                case 29:
                    PostAddress = "AddEmploymentMode";
                    title = "Employee Mode:";
                    break;


                case 30:
                    PostAddress = "AddBirthPlaceforBirthCountry";
                    title = "Birth Country:";
                    break;

                case 31:
                    PostAddress = "AddEmployeeSection";
                    ViewBag.Department = new SelectList(new DepartmentDB().GetAllDepartment(userId: objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
                    title = "Section Name:";
                    break;

                case 32:
                    PostAddress = "AddRelation";
                    title = "Relation Name:";
                    break;


                case 33:
                    PostAddress = "AddBankName";
                    title = "Bank Name:";
                    break;
                case 34:
                    PostAddress = "AddBranchName";
                    title = "Branch Name:";
                    break;

                case 35:
                    PostAddress = "AddCategory";
                    title = "Category Name:";
                    break;

                case 36:
                    PostAddress = "AddAccountType";
                    title = "Account Type:";
                    break;

                case 37:
                    PostAddress = "AddDepartment";
                    title = "Department Name:";
                    break;
                case 38:
                    PostAddress = "AddLocation";
                    title = "Allocation Name:";
                    break;

                case 39:
                    PostAddress = "AddInsuranceType";
                    title = "Insurance Name:";
                    break;
                case 40:
                    PostAddress = "AddBloodType";
                    title = "Blood Type:";
                    break;
                case 41:
                    PostAddress = "AddVaccinationType";
                    title = "Vaccination Name:";
                    break;
                case 42:
                    PostAddress = "AddCarType";
                    title = "Car Type Name:";
                    break;
                case 43:
                    PostAddress = "AddDropNationalities";
                    title = "Nationality Name";
                    break;
                case 44:
                    PostAddress = "AddDropLanguage";
                    title = "Langauage Name:";
                    break;
                case 45:
                    PostAddress = "AddJobFamily";
                    title = "Job Family Name:";
                    break;
                case 46:
                    PostAddress = "AddIssuePlace";
                    title = "Issue Place Name:";
                    break;
                case 47:
                    PostAddress = "AddLeaveReason";
                    title = "Leave Reason Name:";
                    break;
                case 48:
                    PostAddress = "AddQualification";
                    title = "Qualification Name:";
                    break;

                case 49:
                    PostAddress = "AddAccommodation";
                    title = "Accommodation Name:";
                    break;

                case 50:
                    PostAddress = "AddRegTemp";
                    title = "Reg Type:";
                    break;
                case 51:
                    PostAddress = "AddEmailType";
                    title = "Email Type:";
                    break;
                case 52:
                    PostAddress = "AddSchoolHouse";
                    title = "School House:";
                    break;
                case 53:
                    PostAddress = "AddInsuranceEligibility";
                    title = "Insurance Eligibility Name:";
                    CreatedBy = objUserContextViewModel.UserId;
                    break;
                    //case 20:
                    //    PostAddress = "AddCityForCountry";
                    //    ViewBag.AddCityId = "20";
                    //    break;
            }
            ViewBag.titleName = title;
            ViewBag.PostAddress = PostAddress;

            return View();

        }

        [HttpPost]
        public ActionResult AddRegTemp(Types objTypes)
        {
            string query = "insert into HR_RegTemp values('" + objTypes.Text + "')";
            GenderDB ObjGenderDB = new GenderDB();
            var returnVal = DataAccess.GeneralDB.CommonDB.ExecuteQueryString(query);
            return Json("DynamicRegType", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddAccommodation(Types objTypes)
        {
            string query = "insert into GEN_HRAccommodationType values('" + objTypes.Text + "','" + objTypes.Text + "','" + objTypes.Text + "')";
            GenderDB ObjGenderDB = new GenderDB();
            DataAccess.GeneralDB.CommonDB.ExecuteQueryString(query);
            return Json("DynamicAccomodation", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddQualification(Types objTypes)
        {
            string query = "insert into GEN_HRQualification values('" + objTypes.Text + "',N'" + objTypes.TextArabic + "',N'" + objTypes.TextFrench + "')";
            GenderDB ObjGenderDB = new GenderDB();
            DataAccess.GeneralDB.CommonDB.ExecuteQueryString(query);
            return Json("DynamicQualification", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddSchoolHouse(Types objTypes)
        {
            string query = "insert into gen_housing values('" + objTypes.Text + "',N'" + objTypes.TextArabic + "',N'" + objTypes.TextFrench + "')";

            DataAccess.GeneralDB.CommonDB.ExecuteQueryString(query);
            return Json("AddSchoolHouse", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddAccessRoleName(Types objTypes)
        {

            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_AccessRoleName");



            return Json("AddRole", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateRole()
        {

            AccessRoleDB objAccessRoleModel = new AccessRoleDB();

            return Json(objAccessRoleModel.GetAllAccessRoles(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddEmailType(Types objTypes)
        {

            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertEmailType");
            return Json("DynamicEmail", JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddMOLTitle(Types objTypes)
        {

            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_GEN_MOLTitleName");
            return Json("MOLTitle", JsonRequestBehavior.AllowGet);
        }

        public ActionResult IMAddressType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertIMType");
            return Json("DynamicIM", JsonRequestBehavior.AllowGet);
        }

        public ActionResult WebAddressType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertWebType");
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MobileNoType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertMobileType");
            return Json("DynamicMobile", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListEmployee()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;
            ViewBag.Company = GetCompanySelectList();
            return View();
        }

        public ActionResult ExportToExcel(string employeeStatus, int? companyId)
        {
            //UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "Employee" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            var ds = GetEmployees(employeeStatus, companyId);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(string employeeStatus, int? companyId)
        {

            string fileName = "Employee" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);


            var ds = GetEmployees(employeeStatus, companyId);
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }
        private System.Data.DataSet GetEmployees(string employeeStatus, int? companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int totalCount = 0;
            EmployeeDB objEmployeeDB = new EmployeeDB();
            System.Data.DataSet ds = objEmployeeDB.GetEmployeeDataSet(employeeStatus, objUserContextViewModel.UserId, companyId, out totalCount);
            var row = ds.Tables[0].Select("EmployeeId<=0");
            foreach (var item in row)
            {
                ds.Tables[0].Rows.Remove(item);
            }

            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.Remove("CompanyId");
            ds.Tables[0].Columns.Remove("CreatedBy");
            ds.Tables[0].Columns.Remove("CreatedOn");
            ds.Tables[0].Columns.Remove("ModifiedOn");
            ds.Tables[0].Columns["EmployeeAlternativeId"].SetOrdinal(0);
            ds.Tables[0].Columns["EmployeeAlternativeId"].ColumnName = "ID";
            ds.Tables[0].Columns["FirstName"].ColumnName = "First Name";
            ds.Tables[0].Columns["MiddleName"].ColumnName = "Middle Name";
            ds.Tables[0].Columns["LastName"].ColumnName = "Last Name";
            ds.Tables[0].Columns["GenderName"].ColumnName = "Gender";
            ds.Tables[0].Columns["DOB"].ColumnName = "Date of Birth";
            ds.Tables[0].Columns["isActive"].SetOrdinal(ds.Tables[0].Columns.Count - 1);
            ds.Tables[0].Columns["isActive"].ColumnName = "Active";
            ds.Tables[0].Columns["MobileNumber"].ColumnName = "Mobile Number";
            ds.Tables[0].Columns["PositionTitle"].ColumnName = "Position Name";
            ds.Tables[0].Columns["HireDate"].ColumnName = "Date Of Joining";//Task #9120 2019-02-11
            return ds;
        }
        public List<SelectListItem> GetEmploymentModeList()
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            DBHelper objDBHelper = new DBHelper();
            List<EmploymentModeModel> objEmploymentModeModel = new List<EmploymentModeModel>();
            objEmploymentModeModel = objDBHelper.GetEmploymentModeList();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Employement Mode", Value = "0" });
            foreach (var item in objEmploymentModeModel)
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.EmploymentModeName1 + "/" + item.EmploymentModeName2 + "/" + item.EmploymentModeName3, Value = item.EmploymentModeId.ToString() });
            }

            return ObjSelectedList;

        }

        public List<SelectListItem> GetNameTitles()
        {
            TitleDB titleDb = new TitleDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var item in titleDb.GetTitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.TitleName, Value = item.TitleId.ToString() });
            }
            return ObjSelectedList;
        }

        public List<SelectListItem> GetQulalification()
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            ObjSelectedList.Add(new SelectListItem { Text = "Engineer", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "Masters(IT)", Value = "2" });
            ObjSelectedList.Add(new SelectListItem { Text = "Graduate(IT)", Value = "3" });
            return ObjSelectedList;

        }

        public List<SelectListItem> GetJobTitleList()
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Title", Value = "0" });
            ObjSelectedList.Add(new SelectListItem { Text = "Software Developer", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "Web Designer", Value = "2" });
            return ObjSelectedList;

        }

        public ActionResult GetEmployeeList(string Statustype, int? companyId)
        {
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool? isActive = null;
            if (!string.IsNullOrEmpty(Statustype))
            {
                if (Statustype == "1")
                    isActive = true;
                //objEmployeeList = objEmployeeDB.GetAllEmployeeList(objUserContextViewModel.UserId, true);
                if (Statustype == "0")
                    isActive = false;
                //objEmployeeList = objEmployeeDB.GetAllEmployeeList(objUserContextViewModel.UserId, false);
                //if (Statustype == "0,1")
                //  objEmployeeList = objEmployeeDB.GetAllEmployeeList(objUserContextViewModel.UserId, null);
            }
            //else
            objEmployeeList = objEmployeeDB.GetAllEmployeeList(objUserContextViewModel.UserId, isActive, companyId);



            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            bool canActiveInActiveEmployee = PermissionList.Where(x => x.OtherPermissionId == 22).Count() > 0 ? true : false;
            bool canEditEmployee = PermissionList.Where(x => x.OtherPermissionId == 24).Count() > 0 ? true : false;
            bool canViewEmployee = PermissionList.Where(x => x.OtherPermissionId == 25).Count() > 0 ? true : false;
            bool canExportEmployee = PermissionList.Where(x => x.OtherPermissionId == 26).Count() > 0 ? true : false;

            //----------------------------------------------------------------          
            var vList = new object();
            vList = new
            {
                aaData = (from item in objEmployeeList
                          select new
                          {
                              Actions = "<div class='btn-group'>  <button class='btn btn-primary btn-xs dropdown-toggle' href='#' data-toggle='dropdown'>  <span class='caret'></span> </button> <ul class='dropdown-menu stay-open pull-left' role='menu' style='min-width: 300px;'>" +
                                        "<li class='cursorPointer " + (canViewEmployee == false ? "hidden" : "") + "' ><a  onclick='ViewProfileChannel(" + item.EmployeeId.ToString() + ",true)' title='View Profile' ><i class='fa fa-pencil'></i>EMPLOYEE PROFILE</a></li>" +
                                        "<li class='cursorPointer " + (canEditEmployee == false ? "hidden" : "") + "' ><a  onclick='EditChannel(" + item.EmployeeId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i>EDIT PROFILE</a></li>" +
                                         "<li class='cursorPointer " + (canActiveInActiveEmployee == false ? "hidden" : "") + "' ><a onclick=UpdateStatus(" + item.EmployeeId.ToString() + ",'" + item.employeeDetailsModel.isActive + "') title='Active' ><i class='fa fa-pencil'></i>" + ((item.employeeDetailsModel.isActive == true) ? "DEACTIVATE" : "ACTIVATE") + "</a></li>" +
                                        "<li class='cursorPointer' ><a onclick='DeleteChannel(" + item.EmployeeId.ToString() + ")' title='Delete'  style='display:none' ><i class='fa fa-pencil'></i> REMOVE</a></li>" +
                                        "<li class='cursorPointer " + (canExportEmployee == false ? "hidden" : "") + "' ><a  onclick='ExportEmployeeRecordToPDF(" + item.EmployeeId + ")' title='Export to Pdf' ><i class='fa fa-pencil'></i>EXPORT TO PDF</a></li>" +
                                        "</ul> </div>",
                              EmployeeId = item.EmployeeId,
                              FirstName = canViewEmployee == false ? item.FirstName : "<a style='text-decoration:none' class='cursorPointer' onclick='ViewProfileChannel(" + item.EmployeeId.ToString() + ",true)'>" + item.FirstName + "</a>",
                              MiddleName = item.MiddleName,
                              LastName = item.LastName,
                              Gender = item.genderModel.GenderName_1,
                              DateOfBirth = item.employeeDetailsModel.BirthDate,
                              HireDate = item.employmentInformation.HireDate,//2019-02-11
                              CompanyId = item.CompanyId,
                              isActive = item.employeeDetailsModel.isActive == true ? "<input type='checkbox' class='checkbox' checked='true' id='" + item.EmployeeId + "' onclick=UpdateStatus(" + item.EmployeeId + ",'" + item.employeeDetailsModel.isActive + "') class='checkbox' />" : "<input type='checkbox' id='" + item.EmployeeId + "' onclick=UpdateStatus(" + item.EmployeeId + ",'" + item.employeeDetailsModel.isActive + "') class='checkbox' />",
                              EmployeeAlternativeID = item.employeeDetailsModel.EmployeeAlternativeID,
                              MobileNumber = item.employeeContactModel.MobileNumber,//Changed Telephone to Mobile Number
                              PositionName = item.employmentInformation.PositionName
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public JsonResult GetAllEmployee(string Active)
        {
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);


            int[] ids = Active.Split(',').Select(str => int.Parse(str)).ToArray();
            var query = (from item in objEmployeeList
                         where ids.Contains(item.IsActive)
                         select new
                         {
                             EmployeeId = item.EmployeeId,
                             EmployeeAlternativeID = item.EmployeeAlternativeID,
                             FirstName = item.FirstName,
                             LastName = item.LastName,
                             FullName = item.FullName
                         });


            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllEmpSearch(string Active, string value)
        {
            if (value == null)
            {
                value = "";
            }
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);


            int[] ids = Active.Split(',').Select(str => int.Parse(str)).ToArray();
            var query = (from item in objEmployeeList
                         where ids.Contains(item.IsActive) && ((item.FullName.ToLower()).Contains(value.ToLower()) || (item.EmployeeAlternativeID).Contains(value.ToLower()))
                         select new
                         {
                             EmployeeId = item.EmployeeId,
                             EmployeeAlternativeID = item.EmployeeAlternativeID,
                             FirstName = item.FirstName,
                             LastName = item.LastName,
                             FullName = item.FullName
                         });



            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public ActionResult deleteMultipleDetails(int EmpId, int id, string fieldType)
        {
            OperationDetails op = new OperationDetails();
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [Security()]
        public ActionResult Edit(int id)
        {
            HRMS.Entities.Employee objEmployee = new HRMS.Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();

            if (id != 0)
            {
                objEmployee = objEmployeeDB.GetEmployee(id);

            }

            return View(objEmployee);
        }

        public ActionResult Save(HRMS.Entities.Employee objEmployee)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();

            if (objEmployee.EmployeeId == 0)
            {
                objEmployee.CreatedBy = 1;
                objEmployeeDB.AddEmployee(objEmployee);

            }
            else
            {
                objEmployee.ModifiedBy = 1;
                objEmployeeDB.UpdateEmployee(objEmployee);
            }

            return Redirect("Index");

        }

        [HttpPost]
        public ActionResult SaveBankInformation(PayDirectDepositModel payDirectDepositModel)
        {
            string result = "";
            string resultMessage = "";
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();
            OperationDetails operationDetails = new OperationDetails();
            if (payDirectDepositModel.DirectDepositID != 0)
            {
                operationDetails = payDirectDepositDB.InsertPayDirectDeposit(payDirectDepositModel, 2);
            }
            else
            {
                operationDetails = payDirectDepositDB.InsertPayDirectDeposit(payDirectDepositModel, 1);
            }

            if (operationDetails.Message.Contains("Error"))
            {
                result = "error";
            }
            else
            {
                result = "success";
            }

            return Json(new { result = result, resultMessage = operationDetails.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRMS.Entities.Employee objEmployee = new Entities.Employee();
            objEmployee.EmployeeId = int.Parse(id);
            objEmployeeDB.DeleteEmployee(objEmployee);
            //   return Redirect("Index");
            return Json(0, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Add(int EmployeeID = 0)
        {
            HRMS.Entities.Employee objEmployee = new HRMS.Entities.Employee();
            EmployeeDetailsModel objEmpDetails = new EmployeeDetailsModel();

            if (EmployeeID != 0)
            {


            }
            else
            {
                //objEmpDetails.GenderID = 0;
                //objEmpDetails.NationalityID = 0;
                objEmployee.employeeDetailsModel.GenderID = 0;
                objEmployee.genderList = getAllGenderList();
                objEmployee.languageList = new SelectList(getAllLanguageList(), "LanguageID", "LanguageName_1");
                objEmployee.maritialStausList = new SelectList(getAllMaritialStatusList(), "MaritalStatusID", "MaritalStatusName_1");
                objEmployee.nationalityList = new SelectList(getAllNationalityList(), "NationalityID", "NationalityName_1");

            }
            return View("Add", objEmployee);
        }

        public ActionResult SaveEmployee(HRMS.Entities.Employee objEmployee)
        {
            if (objEmployee.EmployeeId == 0)
            {

            }
            else
            {

            }

            return RedirectToAction("Add");
        }

        private List<GenderModel> getAllGenderList()
        {
            GenderDB objGender = new GenderDB();
            List<GenderModel> genderList = objGender.GetAllGenders();
            return genderList;
        }

        private List<LanguageModel> getAllLanguageList()
        {
            LanguageDB objLanguageDB = new LanguageDB();
            List<LanguageModel> languageList = objLanguageDB.GetAllLanguages();
            return languageList;
        }

        private List<MaritialStausModel> getAllMaritialStatusList()
        {
            MaritialStatusDB objMaritialStatusDB = new MaritialStatusDB();
            List<MaritialStausModel> maritialStausList = objMaritialStatusDB.getAllMaritialStatus();
            return maritialStausList;
        }

        private List<NationalityModel> getAllNationalityList()
        {
            NationalityDB objNationalityDB = new NationalityDB();
            List<NationalityModel> maritialStausList = objNationalityDB.getAllNationalities();
            return maritialStausList;
        }

        public string LoadType(int ID)
        {

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            GenderDB ObjGenderDB = new GenderDB();
            string SP = "";
            string selectText = "Select";
            switch (ID)
            {
                case 1:
                    SP = "stp_Gen_GetEmailType";
                    selectText = "Select Email Type";
                    break;

                case 2:
                    SP = "stp_Gen_GetIMType";
                    selectText = "Select IM Type";
                    break;

                case 3:
                    SP = "stp_Gen_GetWebType";
                    selectText = "Select Web Type";
                    break;

                case 4:
                    SP = "stp_Gen_MobileType";
                    selectText = "Select Mobile Type";
                    break;
                case 5:
                    SP = "stp_GetAllPassportExpReminder";
                    break;
                case 6:
                    SP = "stp_GetLatestNationality";
                    selectText = "Select Nationality";
                    break;
                case 7:
                    SP = "stp_GetAllBanks";
                    selectText = "Select Bank";
                    break;


                case 9:
                    SP = "stp_GetCompanyName";
                    selectText = "Select Company";
                    break;

                case 14:
                    SP = "stp_GetBirthCountryName";
                    selectText = "Select Birth Country";
                    break;

                case 15:
                    SP = "stp_GetReligionName";
                    selectText = "Select Religion";
                    break;

                case 16:
                    SP = "stp_GetMaritalStatusName";
                    selectText = "Select Marital Status";
                    break;

                case 17:
                    SP = "stp_GetAddressTypeName";
                    selectText = "Select Address Type";
                    break;


                case 18:
                    SP = "stp_GetCountryName";
                    selectText = "Select Country";
                    break;

                case 21:
                    SP = "stp_GetLatestLanguage";
                    selectText = "Select Language";
                    break;

                case 22:
                    SP = "stp_GetCourseName";
                    selectText = "Select Course";
                    break;
                case 23:
                    SP = "stp_GetPositionName";
                    selectText = "Select Position";
                    break;
                case 24:
                    SP = "Hr_stp_GetJobTitleName";
                    selectText = "Select Job Title";
                    break;

                case 25:
                    SP = "stp_GetMOETitleName";
                    selectText = "Select MOE.";
                    break;

                case 26:
                    SP = "stp_GetMOLTitleName";
                    selectText = "Select MOL.";
                    break;

                case 27:
                    SP = "stp_GetAccessRoleName";
                    selectText = "Select Access Role";
                    break;

                case 28:
                    SP = "stp_GetPassportIssueByName";
                    selectText = "Select";
                    break;

                case 29:
                    SP = "stp_GetEmploymentMode";
                    selectText = "Select Employment Mode";
                    break;

                case 31:
                    SP = "stp_GetEmployeeSectionName";
                    selectText = "Select Employee Section";
                    break;

                case 32:
                    SP = "stp_GetRelationName";
                    selectText = "Select Relation";
                    break;

                case 33:
                    SP = "stp_GetBankName";
                    selectText = "Select Bank";
                    break;

                case 34:
                    SP = "stp_GetBranchName";
                    selectText = "Select Branch";
                    break;

                case 35:
                    SP = "stp_GetCategoryName";
                    selectText = "Select Category";
                    break;

                case 36:
                    SP = "stp_GetAccountTypesName";
                    selectText = "Select Account Type";
                    break;

                case 37:
                    SP = "stp_GetDepartmentName";
                    selectText = "Select Department";
                    break;

                case 38:
                    SP = "stp_GetLocationName";
                    selectText = "Select Location";
                    break;

                case 39:
                    SP = "stp_GetInsuranceTypeName";
                    selectText = "Select InsuranceType";
                    break;

                case 40:
                    SP = "stp_GetBloodTypeName";
                    selectText = "Select Blood type";
                    break;

                case 41:
                    SP = "stp_GetVaccinationTypeName";
                    selectText = "Select Vaccination Type";
                    break;

                case 42:
                    SP = "stp_GetCarTypeName";
                    selectText = "Select Car Type";
                    break;
                case 45:
                    SP = "stp_GetJobFamilyName";
                    selectText = "Select Job Family";
                    break;
                case 46:
                    SP = "stp_GetIssuePlaceNameNew";
                    selectText = "Select Issue Place";
                    break;
                case 47:
                    SP = "stp_GetLeaveReasonNew";
                    break;
                case 53:
                    SP = "HR_stp_GetInsuranceEligibilityName";
                    selectText = "Select Insurance Eligibility";
                    break;
                default:
                    SP = "Custom";
                    break;
            }


            ViewBag.QualificationList = ObjSelectedList;

            string json = "";
            if (SP != "")
            {
                if (ID == 48)
                {
                    HRQualificationDB objHRQualificationDB = new HRQualificationDB();
                    ObjSelectedList = new List<SelectListItem>();
                    ObjSelectedList.Add(new SelectListItem { Text = "Select Qualification", Value = "0" });
                    foreach (var m in objHRQualificationDB.GetAllHRQualification().OrderBy(x => x.HRQualificationName_1))
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.HRQualificationName_1, Value = m.HRQualificationID.ToString() });

                    }
                }
                else if (ID == 49)
                {
                    AllowanceDB AllowanceDB = new DataAccess.AllowanceDB();
                    ObjSelectedList = new List<SelectListItem>();
                    ObjSelectedList.Add(new SelectListItem { Text = "Select Accomodation", Value = "0" });
                    foreach (var m in AllowanceDB.GetAccommodationtype())
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.text, Value = m.id.ToString() });
                    }
                }
                else if (ID == 50)
                {
                    DBHelper objDBHelper = new DBHelper();
                    ObjSelectedList = new List<SelectListItem>();
                    ObjSelectedList.Add(new SelectListItem { Text = "Select Regular/Temporary" });
                    foreach (var m in objDBHelper.GetRegTempList())
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.Reg_TempName, Value = m.Reg_TempID.ToString() });
                    }
                }
                else if (ID == 51)
                {
                    EmployeeDB objDBHelper = new EmployeeDB();
                    ObjSelectedList = new List<SelectListItem>();
                    ObjSelectedList.Add(new SelectListItem { Text = "Select School House", Value = "" });
                    foreach (var m in objDBHelper.GetSchoolHouseList())
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.HousingName_1, Value = m.HOID.ToString() });
                    }
                }
                else
                {
                    ObjSelectedList = new List<SelectListItem>();
                    ObjSelectedList.Add(new SelectListItem { Text = selectText, Value = "0" });
                    foreach (var m in ObjGenderDB.GetTypes(SP))
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
                    }
                }
                JavaScriptSerializer jss = new JavaScriptSerializer();
                json = jss.Serialize(ObjSelectedList);
            }
            return json;
        }

        #region Employment Details
        public ViewResult GetPersonalDetails(int ID)
        {
            EmployeeDetailsModel ObjEmployeeDetail = new EmployeeDetailsModel();
            EmployeeDB objEmployee = new EmployeeDB();

            GetDropDowns();
            return View("PersonalDetails", ObjEmployeeDetail);


        }


        public ViewResult GetContactDetails(int ID)
        {
            EmployeeContactModel ObjContactDetail = new EmployeeContactModel();

            GetDropDowns();

            return View("ContactDetails", ObjContactDetail);


        }


        public ViewResult GetEmploymentInformation(int ID)
        {
            EmploymentInformation ObjEmploymentInformation = new EmploymentInformation();

            GetDropDowns();
            return View("EmploymentInformation", ObjEmploymentInformation);


        }


        public ViewResult GetBankAccountInformation(int ID)
        {

            EmployeeBankAccountInfoModel ObjEmployeeBankAccountInfoModel = new EmployeeBankAccountInfoModel();

            GetDropDowns();
            return View("BankAccountInformation", ObjEmployeeBankAccountInfoModel);


        }


        public ViewResult GetAccountInformation(int ID)
        {


            EmployeeAccountInfoModel ObjEmployeeAccountInfoModel = new EmployeeAccountInfoModel();

            //GetDropDowns();
            GetDropDowns();
            return View("AccountInformation", ObjEmployeeAccountInfoModel);


        }


        public ViewResult GetPersonalIdentityInformation(int ID)
        {
            EmployeePersonalIdentityInfoModel ObjEmployeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();

            GetDropDowns();
            return View("PersonalIdentityInformation", ObjEmployeePersonalIdentityInfoModel);


        }


        public ViewResult GetAddressInformation(int ID)
        {
            EmployeeAddressInfoModel ObjEmployeeAddressInfoModel = new EmployeeAddressInfoModel();

            GetDropDowns();
            return View("AddressInformation", ObjEmployeeAddressInfoModel);

        }

        #endregion

        #region Updating Employee
        //Updating Personal Info
        public ActionResult IsExistsEmployeeID(int EmployeeID)
        {
            EmployeeDB employeeDB = new EmployeeDB();
            int EmployeeIDCount = employeeDB.GetALLEmployeeByUserId(0).Where(x => x.EmployeeAlternativeID.Trim() == EmployeeID.ToString().Trim()).ToList().Count();
            return Json(EmployeeIDCount, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateEmploymentDetails(EmployeeDetailsModel ObjEmployeeDetailsModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<SpokenLanguages> list = (List<SpokenLanguages>)TempData["SelectedSpokenLanguages"];
            ObjEmployeeDetailsModel.SpokenLanguages = list;
            ObjEmployeeDetailsModel.NationalityID = TempData["SelectedNationalities"] != null ? TempData["SelectedNationalities"].ToString() : "";
            ObjEmployeeDetailsModel.NationalityIDCardNo = ObjEmployeeDetailsModel.NationalityIDCardNo ?? "";

            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            MaritialStatusDB objMaritialStatusDB = new MaritialStatusDB();
            MaritialStausModel objMaritialStausModel = new MaritialStausModel();

            ObjEmployeeDetailsModel.CompanyID = objUserContextViewModel.CompanyId;
            ObjEmployeeDetailsModel.ModifiedBy = objUserContextViewModel.UserId;

            #region Generate random password for new employee

            if (ObjEmployeeDetailsModel.EmployeeID == 0)
            {
                ObjEmployeeDetailsModel.Password = RandomString(8); // HRMS.Web.CommonHelper.CommonHelper.EncryptPassword(ObjEmployeeDetailsModel.NormalPassword);
            }

            #endregion

            if (ObjEmployeeDetailsModel.SpokenLanguages.Count() > 0)
                ObjEmployeeDetailsModel.HomeLanguageID = ObjEmployeeDetailsModel.SpokenLanguages.FirstOrDefault().LanguageId;

            operationDetails = objEmployeeDB.UpdateEmployeePersonalInformation(ObjEmployeeDetailsModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmployeeDetailsModel.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Personal Information Added Successfully.";
                }
                else if (ObjEmployeeDetailsModel.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Personal Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Past Employment Details.";
            }

            ViewBag.hdnEmployeeID = operationDetails.InsertedRowId;
            return Json(new { EmployeeID = operationDetails.InsertedRowId, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public int UpdateEmployeePersonalDetails(EmployeeDetailsModel ObjEmployeeDetailsModel)
        {
            int EmployeeID = 0;
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<SpokenLanguages> list = (List<SpokenLanguages>)TempData["SelectedSpokenLanguages"];
            ObjEmployeeDetailsModel.SpokenLanguages = list;
            ObjEmployeeDetailsModel.NationalityID = TempData["SelectedNationalities"] != null ? TempData["SelectedNationalities"].ToString() : "";
            ObjEmployeeDetailsModel.NationalityIDCardNo = ObjEmployeeDetailsModel.NationalityIDCardNo ?? "";

            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            MaritialStatusDB objMaritialStatusDB = new MaritialStatusDB();
            MaritialStausModel objMaritialStausModel = new MaritialStausModel();

            ObjEmployeeDetailsModel.ModifiedBy = objUserContextViewModel.UserId;

            #region Generate random password for new employee

            if (ObjEmployeeDetailsModel.EmployeeID == 0)
            {
                ObjEmployeeDetailsModel.Password = RandomString(8);// HRMS.Web.CommonHelper.CommonHelper.EncryptPassword(ObjEmployeeDetailsModel.NormalPassword);
            }

            #endregion
            if (ObjEmployeeDetailsModel.SpokenLanguages != null)
            {
                if (ObjEmployeeDetailsModel.SpokenLanguages.Where(x => x.IsNative == true).Count() > 0)
                {
                    ObjEmployeeDetailsModel.HomeLanguageID = ObjEmployeeDetailsModel.SpokenLanguages.FirstOrDefault(x => x.IsNative == true).LanguageId;
                }
                else
                    ObjEmployeeDetailsModel.HomeLanguageID = null;
            }
            else
                ObjEmployeeDetailsModel.HomeLanguageID = null;

            operationDetails = objEmployeeDB.UpdateEmployeePersonalInformation(ObjEmployeeDetailsModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmployeeDetailsModel.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Personal Information Added Successfully.";
                }
                else if (ObjEmployeeDetailsModel.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Personal Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Past Employment Details.";
            }

            ViewBag.hdnEmployeeID = operationDetails.InsertedRowId;
            EmployeeID = operationDetails.InsertedRowId;
            return EmployeeID;
        }

        public PartialViewResult GetEmployeeBasicDetails(int ID)
        {
            EmployeeBasicDetails objBasicDetails = new EmployeeBasicDetails();
            if (ID != 0)
            {
                EmployeeDB objEmpDB = new EmployeeDB();
                HRMS.Entities.Employee employeeModel = objEmpDB.GEtBasicEmployeeDetails(ID);
                objBasicDetails.ID = ID;
                objBasicDetails.Name = employeeModel.employeeDetailsModel.FirstName_1 + "   " + employeeModel.employeeDetailsModel.SurName_1;
                objBasicDetails.Position = employeeModel.employmentInformation.PositionName;
                objBasicDetails.Department = employeeModel.employmentInformation.DepartmentName;
                employeeModel.employeeDetailsModel.BasicDetails = objBasicDetails;
            }
            return PartialView("_BasicDetailsPartial", objBasicDetails);
        }

        public JsonResult SaveSelectedLanguages(List<SpokenLanguages> lstSpokenLanguages)
        {
            List<SpokenLanguages> list = lstSpokenLanguages;
            TempData["SelectedSpokenLanguages"] = list;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveSelectedNationalities(string nationalityIds)
        {
            TempData["SelectedNationalities"] = nationalityIds;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //Updating Employee Info
        public JsonResult UpdateEmploymentInformation(EmploymentInformation ObjEmploymentInformation)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ObjEmploymentInformation.ModifiedBy = objUserContextViewModel.UserId;
            EmploymentModeModel objEmploymentModeModel = new EmploymentModeModel();
            DBHelper objDBHelper = new DBHelper();

            operationDetails = objEmployeeDB.UpdateEmploymentInformation(ObjEmploymentInformation);

            ShiftDB objShift = new ShiftDB();
            var opResult = objShift.UpdateAtt_ShiftTableByUser(ObjEmploymentInformation.ShiftID, ObjEmploymentInformation.EmployeeID, 0, 1, "", "", "", "", "", "", false, "");
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmploymentInformation.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Employment Information Added Successfully.";
                }
                else if (ObjEmploymentInformation.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Employment Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Employment Information.";
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        //Updating ContactDetail Info
        public JsonResult UpdateEmployeeContactDetails(EmployeeContactModel ObjEmployeeDetailsModel, EmployeeAddressInfoModel AddressModel, IList<string> txtDynamicEmails, IList<string> txtDynamicPersonalEmail, IList<string> ddDynamicEmailType, IList<string> txtDynamicIM, IList<string> ddDynamicIMType, IList<string> txtDynamicMobile, IList<string> txtDynamicPhone, IList<string> ddDynamicMobileType, string hidMultipleIMContact)
        {
            int empId = ObjEmployeeDetailsModel.EmployeeID;

            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ObjEmployeeDetailsModel.ModifiedBy = objUserContextViewModel.UserId;
            operationDetails = objEmployeeDB.UpdateEmployeeContactDetails(ObjEmployeeDetailsModel, AddressModel, txtDynamicEmails, txtDynamicPersonalEmail, ddDynamicEmailType, txtDynamicIM, ddDynamicIMType, txtDynamicMobile, txtDynamicPhone, ddDynamicMobileType);

            if (hidMultipleIMContact != null && hidMultipleIMContact != "")
            {
                hidMultipleIMContact = hidMultipleIMContact.TrimEnd('#');
                string[] IMContactArray = hidMultipleIMContact.Split('#');
                EmployeeIMContactDB objEmployeeIMContactDB = new EmployeeIMContactDB();
                foreach (var item in IMContactArray)
                {
                    string[] IMContactDetailArray = item.Split(',');
                    EmployeeIMContactModel objEmployeeIMContactModel = new EmployeeIMContactModel();
                    objEmployeeIMContactModel.IMContact = IMContactDetailArray[0];
                    objEmployeeIMContactModel.IMContactType = IMContactDetailArray[1];
                    if (empId != 0)
                    {
                        objEmployeeIMContactModel.EmployeeId = ObjEmployeeDetailsModel.EmployeeID;
                    }
                    else
                    {
                        objEmployeeIMContactModel.EmployeeId = operationDetails.InsertedRowId;
                    }
                    objEmployeeIMContactDB.InsertEmployeeIMContact(objEmployeeIMContactModel);
                }
            }


            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmployeeDetailsModel.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Contact Details Added Successfully.";
                }
                else if (ObjEmployeeDetailsModel.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Contact Details Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Contact Details.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        //Updating Employee Address Information
        public JsonResult UpdateEmployeeAddress(EmployeeAddressInfoModel ObjEmployeeAddressInfoModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            operationDetails = objEmployeeDB.UpdateEmployeeAddressInformation(ObjEmployeeAddressInfoModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                result = "success";
                if (ObjEmployeeAddressInfoModel.EmployeeID == 0)
                {
                    resultMessage = "Address Information Added Successfully.";
                }
                else
                {
                    resultMessage = "Address Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Address Information.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        //Updating Employee Bank Account Information
        public JsonResult UpdateEmployeeBankAccount(EmployeeBankAccountInfoModel ObjEmployeeBankAccountInfoModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            operationDetails = objEmployeeDB.UpdateEmployeeBankAccountInformation(ObjEmployeeBankAccountInfoModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmployeeBankAccountInfoModel.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Bank Account Information Added Successfully.";
                }
                else if (ObjEmployeeBankAccountInfoModel.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Bank Account Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Bank Account Information.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        //Updating Employee Personal Identity Information
        public JsonResult UpdateEmployeePersonalIdentity(EmployeePersonalIdentityInfoModel ObjEmployeePersonalIdentityInfoModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            int mode = 1;

            EmployeeDB objEmployeeDB = new EmployeeDB();
            if (ObjEmployeePersonalIdentityInfoModel.DocPassportId > 0)
                mode = 2;

            operationDetails = objEmployeeDB.UpdateEmployeePeronalIdentityInformation(ObjEmployeePersonalIdentityInfoModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmployeePersonalIdentityInfoModel.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Personal Identity Information Added Successfully.";
                }
                else if (ObjEmployeePersonalIdentityInfoModel.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Personal Identity Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Personal Identity Information.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        //Updating Employee Account Information
        //public JsonResult UpdateEmployeeAccountInformation(EmployeeAccountInfoModel ObjEmployeeAccountInfoModel)
        public JsonResult UpdateEmployeeAccountInformation(UserGroupModel ObjUserGroupModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmployeeDB objEmployeeDB = new EmployeeDB();
            operationDetails = objEmployeeDB.UpdateEmployeeAccountInformation(ObjUserGroupModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjUserGroupModel.EmployeeID == 0)
                {
                    result = "success";
                    resultMessage = "Account Information Added Successfully.";
                }
                else if (ObjUserGroupModel.EmployeeID != 0)
                {
                    result = "success";
                    resultMessage = "Account Information Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Account Information.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpGet]
        public ActionResult GetStatesByCountryId(string countryId)
        {
            StateDB objState = new StateDB();
            if (String.IsNullOrEmpty(countryId))
            {
                throw new ArgumentNullException("countryId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(countryId, out id);
            var states = objState.GetAllStatesByCountryId(countryId);
            var result = (from s in states
                          select new
                          {
                              id = s.StateId,
                              name = s.StateName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCityByStateId(string stateId)
        {
            CityDB objCity = new CityDB();
            if (String.IsNullOrEmpty(stateId))
            {
                throw new ArgumentNullException("stateId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(stateId, out id);
            var cities = objCity.GetAllCityByStateId(stateId);
            var result = (from s in cities
                          select new
                          {
                              id = s.CityId,
                              name = s.CityName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCityByCountryId(string countryId)
        {
            CityDB objCity = new CityDB();
            if (String.IsNullOrEmpty(countryId))
            {
                throw new ArgumentNullException("countryId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(countryId, out id);
            var cities = objCity.GetCityByCountryId(countryId);
            var result = (from s in cities
                          select new
                          {
                              id = s.CityId,
                              name = s.CityName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAreaByCityId(string cityId)
        {
            AreaDB objArea = new AreaDB();
            if (String.IsNullOrEmpty(cityId))
            {
                throw new ArgumentNullException("cityId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(cityId, out id);
            var areas = objArea.GetAllAreaByCityId(cityId);
            var result = (from s in areas
                          select new
                          {
                              id = s.AreaId,
                              name = s.AreaName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetBirthPlaceByCountryId(string countryId)
        {
            CityDB objCity = new CityDB();
            if (String.IsNullOrEmpty(countryId))
            {
                throw new ArgumentNullException("countryId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(countryId, out id);
            var BirthPlaces = objCity.GetBirthPlaceByCountryId(countryId);
            var result = (from s in BirthPlaces
                          select new
                          {
                              id = s.BirthPlaceID,
                              name = s.BirthPlaceName_1
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddExpirationReminder(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertPassportExpirationReminder");
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddNationality(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertNationality");
            return Json("Listbox", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddDepartment(Types objTypes)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objTypes.companyId = objUserContextViewModel.CompanyId;
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "Hr_Stp_Gen_InsertDepartment");
            return Json("Department", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddLocation(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertLocation");
            return Json("Location", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCompany(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_CompanyName"); //@Name
            return Json("Company", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRelation(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "Hr_stp_Add_RelationName"); //@Name
            return Json("Relation", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEmployeeSection(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_GEN_EmployeeSection"); //@Name
            return Json("EmployeeSection", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEmploymentMode(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_EmploymentMode"); //@Name
            return Json("EmploymentMode", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPosition(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_PositionName"); //@Name
            return Json("Position", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInsuranceType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_InsuranceType"); //@Name
            return Json("InsuranceType", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInsuranceEligibility(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_InsuranceEligibility"); //@Name
            return Json("InsuranceEligibility", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCarType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_CarType"); //@Name
            return Json("CarType", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBloodType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_BloodType"); //@Name
            return Json("BloodType", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddVaccinationType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_VaccinationType"); //@Name
            return Json("VaccinationType", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddJobTitle(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "Hr_Stp_Add_HR_JobTitleName"); //@Name
            return Json("JobTitle", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMOETitle(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_GEN_MOETitleName"); //@Name
            return Json("MOETitle", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddJobFamily(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_Gen_JobFamily"); //@Name
            return Json("JobFamily", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddIssuePlace(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_Gen_Issueplace"); //@Name
            return Json("IssuePlace", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddLeaveReason(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_LeaveReason"); //@Name
            return Json("LeaveReason", JsonRequestBehavior.AllowGet);
        }


        //public ActionResult AddAccessRoleName(Types objTypes)
        //{
        //    GenderDB ObjGenderDB = new GenderDB();
        //    ObjGenderDB.AddType(objTypes, "stp_Add_HR_AccessRoleName"); //@Name
        //    return Json("AccessRoleName", JsonRequestBehavior.AllowGet);
        //}

        public ActionResult AddPassportIssueByName(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_PassportIssueByName"); //@Name
            return Json("PassportIssueByName", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCourse(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_CourseName"); //@Name
            return Json("Course", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBankName(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_BankName"); //@Name
            return Json("BankName", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBranchName(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_BranchName"); //@Name
            return Json("BranchName", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCategory(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_PayCategoryName"); //@Name
            return Json("PayCategoryName", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAccountType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_PayAccountTypesName"); //@Name
            return Json("PayAccountTypesName", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddLanguage(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_Gen_LanguageName"); //@Name
            return Json("Language", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCountry(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_CountryName"); //@Name
            return Json("Country", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAddressType(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_AddressType"); //@Name
            return Json("AddressType", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMaritalStatus(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_MaritalStatusName"); //@Name
            return Json("MaritalStatus", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBirthCountry(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_CountryName"); //@Name
            return Json("BirthCountry", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddReligion(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_ReligionName"); //@Name
            return Json("Religion", JsonRequestBehavior.AllowGet);
        }


        //public ActionResult AddEmployeeSection(Types objTypes)
        //{
        //    GenderDB ObjGenderDB = new GenderDB();
        //    ObjGenderDB.AddType(objTypes, "stp_Add_HR_EmployeeSectionName"); //@EmployeeSectionName_1
        //    return Json("EmployeeSection", JsonRequestBehavior.AllowGet);
        //}

        public ActionResult AddDutyShift(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_ShiftName"); //@ShiftName
            return Json("DutyShift", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddJobCategory(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_JobCategoryName"); //@EmployeeJobCategoryName_1
            return Json("JobCategory", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddShiftGroup(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Add_HR_ShiftGRoupName");//@GroupNameEn
            return Json("ShiftGroup", JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddDropNationalities(string text)
        {
            Types objTypes = new Types();
            objTypes.Text = text;
            AddNationality(objTypes);
            return Json("AddDropNationalities", JsonRequestBehavior.AllowGet);//LoadNationality(), 
        }

        public JsonResult AddDropLanguage(Types objTypes)
        {
            AddLanguage(objTypes);
            return Json("AddDropLanguage", JsonRequestBehavior.AllowGet);//LoadLanguage()
        }

        /// <summary>
        /// Loads the nationality when user adds new one
        /// </summary>
        /// <returns></returns>
        public string LoadNationality()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in ObjGenderDB.GetTypes("stp_GetLatestNationality"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadLanguage()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in ObjGenderDB.GetTypes("stp_GetLatestLanguage"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadCompany()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetCompanyName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadBankName()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetBankName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadBranchName()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetBranchName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadCategory()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetCategoryName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadAccountType()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetAccountTypesName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadDepartment()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetDepartmentName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadLocation()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetLocationName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadRelation()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetRelationName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadEmploymentMode()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetEmploymentMode"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadEmployeeSection()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetEmployeeSectionName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadPosition()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetPositionName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadJobTitle()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("Hr_stp_GetJobTitleName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadMOETitle()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetMOETitleName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadMOLTitle()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetMOLTitleName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadAccessRole()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetAccessRoleName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadPassportIssue()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetPassportIssueByName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadCourse()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetCourseName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadCountry()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetCountryName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadAddressType()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetAddressTypeName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadMaritalStatus()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetMaritalStatusName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadBirthCountry()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetBirthCountryName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadReligion()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetReligionName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadSection()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetSectionName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadJobCategory()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetJobCategoryName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadDutyShift()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetShiftName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadDutyShiftGroup()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetShiftGroupName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadIssuePlace()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetIssuePlaceName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public string LoadLeaveReason()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjGenderDB.GetTypes("stp_GetLeaveReasonName"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;
        }

        public ActionResult AddBank(Types objTypes)
        {
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertBank");
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddArea(int ID)
        {
            string PostAddress = "";
            PostAddress = "AddAreaForCity";
            ViewBag.titleName = "Area Name:";
            ViewBag.AddCityId = ID.ToString();
            ViewBag.PostAddress = PostAddress;
            return View("AddType");

        }

        public ActionResult AddState(int ID)
        {
            string PostAddress = "";
            PostAddress = "AddStateForCountry";
            ViewBag.titleName = "State Name";
            ViewBag.AddCountryId = ID.ToString();
            ViewBag.PostAddress = PostAddress;
            return View("AddType");

        }

        public ActionResult AddBirthPLace(int ID)
        {
            string PostAddress = "";
            PostAddress = "AddBirthPlaceforBirthCountry";
            ViewBag.titleName = "Birth Place";
            ViewBag.AddBirthCountryId = ID.ToString();
            ViewBag.PostAddress = PostAddress;
            return View("AddType");

        }

        public ActionResult AddCity(int ID, int CountryID)
        {
            string PostAddress = "";
            PostAddress = "AddCityForState";
            ViewBag.titleName = "City Name";
            ViewBag.AddStateId = ID.ToString();
            ViewBag.AddCountryId = CountryID.ToString();
            ViewBag.PostAddress = PostAddress;
            return View("AddType");
        }

        public ActionResult AddNewCityForCountry(int CountryID)
        {
            string PostAddress = "";
            PostAddress = "AddCityForCountry";
            ViewBag.titleName = "City Name";
            ViewBag.AddCountryId = CountryID.ToString();

            ViewBag.PostAddress = PostAddress;
            return View("AddType");

        }

        public ActionResult AddStateForCountry(Types objTypes, FormCollection collection)
        {
            if (collection["hiddenForCountry"] != null)
            {
                objTypes.CountryId = int.Parse(collection["hiddenForCountry"].ToString());
                //objTypes.CountryId = int.Parse(collection["hdnCountryID"].ToString());
            }
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertStateForCountry");
            return Json("LoadState", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBirthPlaceforBirthCountry(Types objTypes, FormCollection collection)
        {
            if (collection["hiddenForBirthCountry"] != null)
            {

                objTypes.CountryId = int.Parse(collection["hiddenForBirthCountry"].ToString());
                //  objTypes.CountryId = int.Parse(collection["hiddenForBirthCountry"].ToString());
                // objTypes.CountryId = int.Parse(collection["hdnBirthCountryID"].ToString());
            }
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertBirthplaceForCountry");
            return Json("LoadBirthPlace", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCityForState(Types objTypes, FormCollection collection)
        {
            if (collection["hiddenForState"] != null)
            {
                if (collection["hiddenForState"].ToString() != "")
                {
                    objTypes.StateId = int.Parse(collection["hiddenForState"].ToString());
                }
                if (collection["hiddenForCountry"].ToString() != "")
                {
                    objTypes.CountryId = int.Parse(collection["hiddenForCountry"].ToString());
                }
            }
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertCityForState");
            return Json("LoadCity", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCityForCountry(Types objTypes, FormCollection collection)
        {
            if (collection["hiddenForState"] != null)
            {
                objTypes.CountryId = int.Parse(collection["hiddenForCountry"].ToString());
            }
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertCityForCountry");
            return Json("LoadNewCityByCountry", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAreaForCity(Types objTypes, FormCollection collection)
        {
            if (collection["hiddenForCity"] != null)
            {
                objTypes.CityId = int.Parse(collection["hiddenForCity"].ToString());
            }
            GenderDB ObjGenderDB = new GenderDB();
            ObjGenderDB.AddType(objTypes, "stp_Gen_InsertAreaForCity");
            return Json("LoadAreas", JsonRequestBehavior.AllowGet);
        }

        public string LoadStateByCountryID(int ID)
        {
            StateDB objState = new StateDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select State", Value = "0" });

            var states = objState.GetAllStatesByCountryId(ID.ToString());
            foreach (var m in states)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.StateName, Value = m.StateId.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;

        }

        public string LoadBirthPlaceForCountry(int ID)
        {
            CityDB objCity = new CityDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Birth Place", Value = "0" });

            var BirthPlace = objCity.GetBirthPlaceByCountryId(ID.ToString());
            foreach (var m in BirthPlace)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.BirthPlaceName_1, Value = m.BirthPlaceID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;

        }

        public string LoadCityByStateID(int ID)
        {
            CityDB objCity = new CityDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });

            var city = objCity.GetAllCityByStateId(ID.ToString());
            foreach (var m in city)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;

        }

        public string LoadCityByCountryID(int ID)
        {
            CityDB objCity = new CityDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });

            var city = objCity.GetCityByCountryId(ID.ToString());
            foreach (var m in city)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;

        }

        public string LoadAreasByCityId(int ID)
        {
            AreaDB objArea = new AreaDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Area", Value = "0" });

            var areas = objArea.GetAllAreaByCityId(ID.ToString());
            foreach (var m in areas)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.AreaName, Value = m.AreaId.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String json = jss.Serialize(ObjSelectedList);
            return json;

        }

        public ActionResult LoadFieldsForEmailIMPhone(string type)
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();


            if (type == "Email")
            {
                ObjSelectedList.Add(new SelectListItem { Text = "Select Email Type", Value = "0" });

                foreach (var m in ObjGenderDB.GetTypes("stp_Gen_GetEmailType"))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

                }
                ViewBag.EmailTypeList = ObjSelectedList;

                return PartialView("LoadFieldsForEmailIMPhone", type);
            }
            else if (type == "IM")
            {
                ObjSelectedList.Add(new SelectListItem { Text = "Select IM Type", Value = "0" });

                foreach (var m in ObjGenderDB.GetTypes("stp_Gen_GetIMType"))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

                }
                ViewBag.IMAddressTypeList = ObjSelectedList;

                return PartialView("LoadFieldsForEmailIMPhone", type);
            }
            else if (type == "Mobile")
            {
                ObjSelectedList.Add(new SelectListItem { Text = "Select Landline Type", Value = "0" });

                foreach (var m in ObjGenderDB.GetTypes("stp_Gen_MobileType"))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

                }
                ViewBag.MobileNumberList = ObjSelectedList;

                return PartialView("LoadFieldsForEmailIMPhone", type);
            }
            else if (type == "Phone")
            {
                //foreach (var m in ObjGenderDB.GetTypes("stp_Gen_MobileType"))
                //{
                //    ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

                //}
                //ViewBag.MobileNumberList = ObjSelectedList;

                return PartialView("LoadFieldsForEmailIMPhone", type);
            }
            else if (type == "PersonalEmail")
            {


                return PartialView("LoadFieldsForEmailIMPhone", type);
            }

            return PartialView("LoadFieldsForEmailIMPhone");
        }

        #region Employee Profile

        public ActionResult EmployeeProfile()
        {
            return View();
        }

        #endregion

        #region Employee Profile Shortcuts

        public ActionResult AddEmployeeDependent(int employeeId)
        {
            //string Id = Convert.ToString(Session["empID"]);
            CountryDB objCountryDB = new CountryDB();
            ViewBag.Countries = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");
            EmployeeDependantModel objEmployeeDependantModel = new EmployeeDependantModel();
            objEmployeeDependantModel.EmployeeId = employeeId;
            DBHelper objDBHelper = new DBHelper();
            //ViewBag.Relationship = new SelectList(objDBHelper.GetEmployee_DependantRelationshipList(), "DependantRelationshipId", "RelationshipName");

            var ObjSelectedList = new List<SelectListItem>();
            var relationships2 = new DBHelper().GetEmployee_DependantRelationshipList();
            foreach (var m in relationships2)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.RelationshipName, Value = m.DependantRelationshipId.ToString() });

            }
            ViewBag.Relationship = ObjSelectedList;
            List<CityModel> cities = new List<CityModel>();
            if (objEmployeeDependantModel != null && objEmployeeDependantModel.Country != null)
            {
                cities = new CityDB().GetCityByCountryId(objEmployeeDependantModel.Country.ToString());
            }
            ObjSelectedList = new List<SelectListItem>();
            foreach (var m in cities)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });

            }
            ViewBag.Cities = ObjSelectedList;
            //ViewBag.Cities = new SelectList(new List<CityModel>(), "CityId", "CityName");
            ViewBag.PageType = "Add";
            return View(objEmployeeDependantModel);
        }

        public ActionResult EmployeeDependentDetail(int DependantId, string type)
        {
            EmployeeDependantModel objEmployeeDependantModel = new EmployeeDependantModel();
            List<CityModel> cities = new List<CityModel>();
            if (DependantId > 0)
            {
                objEmployeeDependantModel = new EmployeeDependantDB().GetEmployeeDependantByDependantId(DependantId);
            }
            ViewBag.Countries = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName");
            //var Countries = new CountryDB().GetAllContries();
            //var ObjSelectedList = new List<SelectListItem>();
            //// ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "0" });
            //var countries = new CountryDB().GetAllContries();

            //ViewBag.Relationship = new SelectList(new DBHelper().GetEmployee_DependantRelationshipList(), "DependantRelationshipId", "RelationshipName");
            if (objEmployeeDependantModel != null && objEmployeeDependantModel.Country != null)
            {
                cities = new CityDB().GetCityByCountryId(objEmployeeDependantModel.Country.ToString());
            }
            var ObjSelectedList = new List<SelectListItem>();
            foreach (var m in cities)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString(), Selected=m.CityId.ToString()==objEmployeeDependantModel.City });

            }
            ViewBag.Cities = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            var relationships2 = new DBHelper().GetEmployee_DependantRelationshipList();
            foreach (var m in relationships2)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.RelationshipName, Value = m.DependantRelationshipId.ToString(), Selected = m.DependantRelationshipId.ToString() == objEmployeeDependantModel.Relationship.ToString() });

            }
            ViewBag.Relationship = ObjSelectedList;
            //ViewBag.Cities = new SelectList(cities, "CityId", "CityName");
            ViewBag.PageType = type;
            return View("AddEmployeeDependent", objEmployeeDependantModel);
        }

        public ActionResult DeleteEmployeeDependant(int dependentId)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            operationDetails = new EmployeeDependantDB().DeleteEmployeeDependant(dependentId, objUserContextViewModel.UserId);
            if (operationDetails.Success)
            {
                result = "success";
                resultMessage = "Record deleted successfully";

            }
            else
            {
                result = "error";
                resultMessage = "Technical error has occurred";
            }
            return Json(new { DependentId = dependentId, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateContactInformation(int Id)
        {
            EmployeeContactModel employeeContact = new EmployeeContactModel();
            employeeContact = new EmployeeDB().GetEmployeeConatctDetail(Id);
            employeeContact.EmployeeID = Id;

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select IM Type", Value = "0" });
            var types = new GenderDB().GetTypes("stp_Gen_GetIMType");
            foreach (var m in types)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.IMAddressTypeList = ObjSelectedList;
            return View(employeeContact);
        }

        [HttpPost]
        public ActionResult UpdateContactInformation(EmployeeContactModel employeeContactModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            employeeContactModel.ModifiedBy = objUserContextViewModel.UserId;
            objOperationDetails = new EmployeeDB().UpdateContactInformation(employeeContactModel);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpdateAddressInformation(int Id)
        {
            EmployeeAddressInfoModel employeeAddressInfo = new EmployeeAddressInfoModel();
            employeeAddressInfo = new EmployeeDB().GetEmployeeAddressInformation(Id);
            employeeAddressInfo.EmployeeID = Id;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "0" });
            var countries = new CountryDB().GetAllContries();
            foreach (var m in countries)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });

            }
            ViewBag.CountryList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select State", Value = "0" });
            if (Id > 0)
            {
                var states = new StateDB().GetAllStatesByCountryId(employeeAddressInfo.CountryID.ToString());
                foreach (var m in states)
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.StateName, Value = m.StateId.ToString() });

                }
            }

            ViewBag.StateList = ObjSelectedList;

            // City List
            ObjSelectedList = new List<SelectListItem>();
            CityDB objCity = new CityDB();
            foreach (var item in objCity.GetAllCityByStateId(employeeAddressInfo.StateID.ToString()))
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.CityName, Value = item.CityId.ToString() });
            }
            //ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });
            ViewBag.CityList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Area", Value = "0" });
            AreaDB objArea = new AreaDB();
            foreach (var item in objArea.GetAllAreaByCityId(employeeAddressInfo.CityID.ToString()))
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.AreaName, Value = item.AreaId.ToString() });
            }
            ViewBag.AreaList = ObjSelectedList;
            return View(employeeAddressInfo);
        }
        [HttpPost]
        public ActionResult UpdateAddressInformation(EmployeeAddressInfoModel employeeAddressInfo)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objOperationDetails = new EmployeeDB().UpdateAddressInformation(employeeAddressInfo, objUserContextViewModel.UserId);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult AddEmployeeDependent(EmployeeDependantModel objEmployeeDependantModel)
        {
            if (ModelState.IsValid)
            {
                EmployeeDependantDB objEmployeeDependantDB = new EmployeeDependantDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                OperationDetails objOperationDetails = new OperationDetails();
                if (objEmployeeDependantModel.DependantId == 0)
                {
                    objEmployeeDependantModel.CreatedBy = objUserContextViewModel.UserId;
                    objOperationDetails = objEmployeeDependantDB.InsertEmployeeDependant(objEmployeeDependantModel);
                }
                else
                {
                    objEmployeeDependantModel.ModifiedBy = objUserContextViewModel.UserId;
                    objOperationDetails = objEmployeeDependantDB.UpdateEmployeeDependant(objEmployeeDependantModel);
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public ActionResult GetNoOfLeavesPending(int id, int EmployeeId)
        {

            int TotalEligibleLeaves;
            int NoOfLeaves;
            int NoOfLeavesTaken;
            int LeaveTypeID = id;
            int EmployeeID = EmployeeId;


            TotalEligibleLeaves = 0;
            LeaveTypeDB objleavetypedb = new LeaveTypeDB();
            LeaveTypeModel objleavetype = new LeaveTypeModel();

            objleavetype = objleavetypedb.GetLeaveTypeById(LeaveTypeID);


            EmployeeLeaveRequestDB objEmployeeLeaveRequestDB = new EmployeeLeaveRequestDB();
            EmployeeLeaveRequestModel objEmployeeLeaveReasonModel = new EmployeeLeaveRequestModel();
            objEmployeeLeaveReasonModel = objEmployeeLeaveRequestDB.GetEmployeeLeaveRequestByEmployeeIdAndLeaveId(EmployeeID, LeaveTypeID);



            NoOfLeaves = objleavetype.NoOfDays;
            NoOfLeavesTaken = objEmployeeLeaveReasonModel.DaysTaken;



            if (NoOfLeavesTaken <= NoOfLeaves)
            {
                TotalEligibleLeaves = NoOfLeaves - NoOfLeavesTaken;
            }
            if (NoOfLeavesTaken > NoOfLeaves)
            {
                TotalEligibleLeaves = -1;
            }

            return Json(new { TotalEligibleLeaves = TotalEligibleLeaves }, JsonRequestBehavior.AllowGet);
        }



        //public string CheckIsCompenstedLeave(int leavetypeid, int employeeid)
        //{
        //     objEmployeeLeaveRequestDB = new EmployeeLeaveRequestDB();






        //    return "s";






        //}

        public ActionResult AddEmployeeLeaveRequest(int id)
        {
            EmployeeLeaveRequestModel employeeLeaveRequestModel = new EmployeeLeaveRequestModel();
            employeeLeaveRequestModel.EmployeeId = id;
            int applicableafterdigit;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DBHelper objDBHelper = new DBHelper();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(id);
            // ViewBag.Employee = objEmployeeDB.GetEmployee(employeeid);
            ViewBag.LeaveApprover = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId);
            EmployeeLeaveRequestDB objEmployeeLeaveRequestDB = new EmployeeLeaveRequestDB();
            HRMS.Entities.Employee objemployee = new HRMS.Entities.Employee();
            objemployee = objEmployeeLeaveRequestDB.GetProfileDetailsOfEmployee(id);
            objemployee.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.GenderID = objemployee.employeeDetailsModel.GenderID;
            ViewBag.ReligionID = objemployee.employeeDetailsModel.ReligionID;
            ViewBag.NAtionalityID = objemployee.employeeDetailsModel.NationalityID;
            ViewBag.MaritalStatusID = objemployee.employeeDetailsModel.MaritalStatusID;
            ViewBag.HireDate = objemployee.employmentInformation.HireDate;
            //Leave details of Employee
            List<EmployeeLeaveRequestModel> objEmployeeLeaveReasonModellist = new List<EmployeeLeaveRequestModel>();
            objEmployeeLeaveReasonModellist = objEmployeeLeaveRequestDB.GetEmployeeLeaveReasonByEmployeeId(id);
            ViewBag.EmployeeLeaveReasonModellist = objEmployeeLeaveReasonModellist;
            if (objemployee.employmentInformation.HireDate == null)
            {
                objemployee.employmentInformation.HireDate = DateTime.Now.ToString("dd/MM/yyyy");
            }
            TimeSpan difference = TimeSpan.Parse(((DateTime.Now) - DateTime.ParseExact(objemployee.employmentInformation.HireDate, "dd/MM/yyyy", CultureInfo.InstalledUICulture)).ToString());
            applicableafterdigit = (int)difference.TotalDays;
            List<LeaveReason> objleavemodellist = new List<LeaveReason>();
            LeaveTypeDB objleavetypedb = new LeaveTypeDB();
            ////objleavemodellist = objleavetypedb.GetLeaveReasonList(0);
            ////for (int i = 0; i < objleavemodellist.Count; i++)
            ////{
            ////    if (objleavemodellist[i].LifetimeLeave == 1)
            ////    {
            ////        int NoOfLeaves;
            ////        //check lifetime leave taken if yes remove it 
            ////        int[] a = new int[2];
            ////        a = objleavetypedb.GetLeaveTypeForLifetimeleave(objleavemodellist[i].LeaveTypeId, objemployee.EmployeeId, objleavemodellist[i].RepeatedFor);
            ////        NoOfLeaves = a[1];

            ////        if (a[0] >= objleavemodellist[i].RepeatedFor && a[1] >= objleavemodellist[i].NoOfDays)
            ////        {
            ////            objleavemodellist.RemoveAt(i);
            ////        }

            ////    }

            //if (objleavemodellist[i].AnnualLeave == 1)
            //{
            //    //check annual leave taken if yes remove it 
            //    int flag = objleavetypedb.GetLeaveTypeForAnnualleave(objleavemodellist[i].LeaveTypeId, objemployee.EmployeeId);
            //    if (flag > 0)
            //    {
            //        if (objleavemodellist[i].NoOfDays <= flag)
            //        {
            //            objleavemodellist.Remove(objleavemodellist[i]);
            //        }
            //        else
            //        {
            //            flag = objleavetypedb.GetLeaveTypeForAnnualleaveNoOFRepeat(objleavemodellist[i].LeaveTypeId, objemployee.EmployeeId);
            //            if (flag >= objleavemodellist[i].RepeatedFor)
            //            {
            //                objleavemodellist.Remove(objleavemodellist[i]);
            //            }
            //        }
            //    }
            //}
            //}
            ViewBag.LeaveReason = new SelectList(objleavetypedb.GetLeaveReasonList(0, 2), "ID", "LeaveReason_1");
            ViewBag.LeaveType = new SelectList(objleavetypedb.GetLeaveTypeName(), "LeaveTypeId", "LeaveTypeName");
            //  ViewBag.LeaveReason = new SelectList(objDBHelper.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");
            return View(employeeLeaveRequestModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddEmployeeLeaveRequest(EmployeeLeaveRequestModel objEmployeeLeaveRequestModel)
        {
            string result = "";
            string resultMessage = "";
            int TotalEligibleLeaves;
            int NoOfLeaves;
            int NoOfLeavesRequest;
            int LeaveTypeID = objEmployeeLeaveRequestModel.leaveType;
            int EmployeeID = objEmployeeLeaveRequestModel.EmployeeId;
            string startdate = objEmployeeLeaveRequestModel.StartDate;
            string enddate = objEmployeeLeaveRequestModel.EndDate;

            LeaveTypeDB objleavetypedb = new LeaveTypeDB();
            LeaveTypeModel objleavetype = new LeaveTypeModel();

            objleavetype = objleavetypedb.GetLeaveTypeById(LeaveTypeID);
            int flag = 0;
            System.DateTime SD = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(startdate)); //DateTime.ParseExact(startdate, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
            System.DateTime ED = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(enddate));//DateTime.ParseExact(enddate, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
            NoOfLeavesRequest = (ED.Subtract(SD)).Days;
            int Monthdigit = int.Parse(SD.Month.ToString());

            //check Lifetime Leave no of days remaining
            if (objleavetype.LifetimeLeave == 1)
            {
                int[] a = new int[2];
                a = objleavetypedb.GetLeaveTypeForLifetimeleave(objleavetype.LeaveTypeId, EmployeeID, objleavetype.RepeatedFor);
                NoOfLeaves = a[1];

                if (a[0] < objleavetype.RepeatedFor && a[1] < objleavetype.NoOfDays)
                {
                    flag = 1;
                }
                else
                {
                    flag = 0;
                    if (a[0] > objleavetype.RepeatedFor)
                    {
                        resultMessage = "You have Repeated leave more then eligible repitition of leave for the year";
                    }
                    if (a[1] > objleavetype.NoOfDays)
                    {
                        resultMessage = "You have Less number of days for this leave for this year";
                    }

                }
            }
            //Lifetime 




            //Annual Leave 
            if (objleavetype.AnnualLeave == 1)
            {
                if (objleavetype.MonthlySplit == 0)       //no
                {
                    int[] a = new int[2];
                    a = objleavetypedb.GetLeaveTypeForAnnualleaveNoOFRepeatAndDays(objleavetype.LeaveTypeId, EmployeeID, startdate);
                    if (a[0] < objleavetype.RepeatedFor && a[1] < objleavetype.NoOfDays)
                    {
                        flag = 1;
                    }
                    else
                    {
                        flag = 0;
                        if (a[0] > objleavetype.RepeatedFor)
                        {
                            resultMessage = "You have Repeated leave more then eligible repitition of leave for the year";
                        }
                        if (a[1] > objleavetype.NoOfDays)
                        {
                            resultMessage = "You have Less number of days for this leave for this year";
                        }

                    }


                }



                if (objleavetype.MonthlySplit == 1)       //yes
                {
                    //check no of days
                    int[] a = new int[2];
                    a = objleavetypedb.GetLeaveTypeForAnnualleaveIsMonthlySplit(LeaveTypeID, EmployeeID, startdate, enddate);
                    //Requested Leaves
                    NoOfLeaves = a[1];
                    //string dtstart = Convert.ToDateTime(startdate).ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    //string dtenddate = Convert.ToDateTime(enddate).ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

                    decimal MonthlyEligibleHolidays = (Math.Round(Convert.ToDecimal(objleavetype.NoOfDays) / 12, 2));
                    //decimal MonthlyEligibleHolidays = Convert.ToDecimal(objleavetype.NoOfDays / 12);

                    //float testNo = (float)(objleavetype.NoOfDays / 12);

                    TotalEligibleLeaves = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(MonthlyEligibleHolidays * Monthdigit)));


                    TotalEligibleLeaves = TotalEligibleLeaves - NoOfLeaves;


                    if (NoOfLeavesRequest < TotalEligibleLeaves && a[0] < objleavetype.RepeatedFor) // eligible for leave
                    {

                        flag = 1;
                    }
                    else                          //not eligible for leave
                    {

                        flag = 0;
                        if (a[0] > objleavetype.RepeatedFor)
                        {
                            resultMessage = "You have Repeated leave more then eligible repitition of leave for the year";
                        }
                        if (a[1] > objleavetype.NoOfDays)
                        {
                            resultMessage = "You have Less number of days for this leave for the year";
                        }

                    }
                }
                //MonthlySPlit
                else
                {
                    flag = 1;

                }
            }
            //Annual Leave
            if (objleavetype.LifetimeLeave == 0 && objleavetype.AnnualLeave == 0)
            {

                int[] a = new int[2];
                a = objleavetypedb.GetLeaveTypeForLifetimeleave(objleavetype.LeaveTypeId, EmployeeID, objleavetype.RepeatedFor);
                NoOfLeaves = a[1];

                if (a[0] < objleavetype.RepeatedFor && a[1] < objleavetype.NoOfDays)
                {
                    flag = 1;
                }
                else
                {
                    flag = 0;
                    if (a[0] > objleavetype.RepeatedFor)
                    {
                        resultMessage = "You have Repeated leave more then eligible repitition of leave for the year";
                    }
                    if (a[1] > objleavetype.NoOfDays)
                    {
                        resultMessage = "You have Less number of days for this leave for this year";
                    }

                }


            }

            if (flag == 1)
            {



                if (ModelState.IsValid)
                {
                    DBHelper objDBHelper = new DBHelper();
                    EmployeeLeaveRequestDB objEmployeeLeaveRequestDB = new EmployeeLeaveRequestDB();
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    objEmployeeLeaveRequestModel.CreatedBy = objUserContextViewModel.UserId;
                    objEmployeeLeaveRequestModel.EmployeeId = objUserContextViewModel.UserId;
                    objEmployeeLeaveRequestModel.ApproverId = objUserContextViewModel.UserId;
                    objEmployeeLeaveRequestModel.CreatedBy = objUserContextViewModel.UserId;
                    //EmployeeLeaveReasonModel objEmployeeLeaveReasonModel = new EmployeeLeaveReasonModel();
                    //// objEmployeeLeaveReasonModel.EmployeeLeaveReason = Request.Form["newLeaveReason"].ToString();
                    ///  objEmployeeLeaveReasonModel.EmployeeLeaveReason = objleavetype.LeaveTypeId.ToString();

                    ////objEmployeeLeaveRequestModel.Reason = objleavetype.LeaveTypeId;

                    objEmployeeLeaveRequestModel.EmployeeId = EmployeeID;
                    //if (objEmployeeLeaveReasonModel.EmployeeLeaveReason != null )
                    //{
                    //    objEmployeeLeaveRequestModel.Reason = objDBHelper.InsertEmployeeLeaveReason(objEmployeeLeaveReasonModel).InsertedRowId;
                    //}

                    OperationDetails objOperationDetails = new OperationDetails();
                    objOperationDetails = objEmployeeLeaveRequestDB.InsertEmployeeLeaveRequest(objEmployeeLeaveRequestModel);

                    //string[] employeeList = objEmployeeLeaveRequestModel.NotificationSentTo.Split(',');
                    try
                    {
                        //foreach (string item in employeeList)
                        //{
                        //    EmployeeDB objEmployeeDB = new EmployeeDB();
                        //    // HRMS.Web.CommonHelper.CommonHelper.SendMail(objEmployeeDB.GetEmployee(Convert.ToInt32(item)).EmailId, "test@test.com", "Hi", "Hi", "");
                        //}
                    }
                    catch (Exception ex)
                    {
                    }

                    result = "success";
                    resultMessage = "Leave Request added successfully.";
                }
                else
                {
                    result = "error";
                    resultMessage = "Please fill all the fields carefully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Sorry,you are not eligible for the leave.";

            }
            if (flag == 0)
            {
                result = "error";
            }


            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Internal Employment

        public ActionResult InternalEmploymentList(int Id = 0)
        {
            ViewBag.EmployeeID = Id;
            return View("InternalEmploymentList");

        }


        int LoginUser = 1;


        public ActionResult EditInternalEmployment(int Id = 0, int EmployeeID = 0)
        {
            ViewBag.EmployeeID = EmployeeID;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();
            InternalEmploymentModel objInternalEmploymentModel = new InternalEmploymentModel();
            if (Id != 0)
            {
                objInternalEmploymentModel = objInternalEmploymentDB.GetInternalEmploymentByID(Id);
                LocationDB objLocationDB = new LocationDB();
                LocationModel locationModel = new LocationModel();
                List<LocationModel> locationList = objLocationDB.GetLocationName();
                objInternalEmploymentModel.LocationList = new SelectList(locationList, "LocationID", "LocationName", objInternalEmploymentModel.LocationID);

                PositionDB objPositionDB = new PositionDB();
                PositionModel positionModel = new PositionModel();
                List<PositionModel> positionList = objPositionDB.GetPositionList();
                objInternalEmploymentModel.PositionList = new SelectList(positionList, "PositionID", "PositionTitle", objInternalEmploymentModel.PositionID);


                DepartmentDB objDepartmentDB = new DepartmentDB();
                DepartmentModel departmentModelModel = new DepartmentModel();
                List<DepartmentModel> departmentList = objDepartmentDB.GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId);
                objInternalEmploymentModel.DepartmentList = new SelectList(departmentList, "DepartmentID", "DepartmentName_1", objInternalEmploymentModel.DepartmentID);

            }
            else
            {

                objInternalEmploymentModel.EmployeeID = EmployeeID;
                objInternalEmploymentModel.CompanyId = objUserContextViewModel.CompanyId;

                LocationDB objLocationDB = new LocationDB();
                LocationModel locationModel = new LocationModel();
                List<LocationModel> locationList = objLocationDB.GetLocationName();
                objInternalEmploymentModel.LocationList = new SelectList(locationList, "LocationID", "LocationName");

                PositionDB objPositionDB = new PositionDB();
                PositionModel positionModel = new PositionModel();
                List<PositionModel> positionList = objPositionDB.GetPositionList();
                objInternalEmploymentModel.PositionList = new SelectList(positionList, "PositionID", "PositionTitle");


                DepartmentDB objDepartmentDB = new DepartmentDB();
                DepartmentModel departmentModelModel = new DepartmentModel();
                List<DepartmentModel> departmentList = objDepartmentDB.GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId);
                objInternalEmploymentModel.DepartmentList = new SelectList(departmentList, "DepartmentID", "DepartmentName_1");
            }
            return View("AddEditInternalEmployment", objInternalEmploymentModel);
        }


        public ActionResult UpdateInternalEmploymentDetails(InternalEmploymentModel internalEmploymentModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            int InternalEmploymentID = 0;


            if (Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(internalEmploymentModel.StartDate)) > Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(internalEmploymentModel.EndDate)))
            {
                result = "error";
                resultMessage = "Start date must be before end date";
                return Json(new { InternalEmploymentID = InternalEmploymentID, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
            }


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();

            if (internalEmploymentModel.InternalEmploymentID == 0)
            {
                internalEmploymentModel.CreatedBy = objUserContextViewModel.UserId;
                operationDetails = objInternalEmploymentDB.AddInternalEmployment(internalEmploymentModel);
                InternalEmploymentID = operationDetails.InsertedRowId;
            }
            else
            {
                InternalEmploymentID = internalEmploymentModel.InternalEmploymentID;
                internalEmploymentModel.ModifiedBy = objUserContextViewModel.UserId;
                operationDetails = objInternalEmploymentDB.UpdateInternalEmployment(internalEmploymentModel);
            }

            if (operationDetails.Message == "Success")
            {
                if (internalEmploymentModel.InternalEmploymentID == 0)
                {
                    result = "success";
                    resultMessage = "Internal employment details save successfully";
                }
                else if (internalEmploymentModel.InternalEmploymentID != 0)
                {
                    result = "success";
                    resultMessage = "Internal employment details save successfully";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Technical error has occurred";
            }

            return Json(new { InternalEmploymentID = InternalEmploymentID, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult DeleteInternalEmployment(int Id)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();
            operationDetails = objInternalEmploymentDB.DeleteInternalEmployment(Id);
            if (operationDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Record deleted successfully";

            }
            else
            {
                result = "error";
                resultMessage = "Technical error has occurred";
            }
            return Json(new { InternalEmploymentID = Id, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetInternalEmploymentDocsList(int Id = 0)
        {

            //-------------Data Objects--------------------
            InternalEmploymentModel objInternalEmployment = new InternalEmploymentModel();

            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();
            objInternalEmployment.objInternalEmploymentList = objInternalEmploymentDB.GetInternalEmploymentDocs(Id);
            objInternalEmployment.InternalEmploymentID = Id;
            //---------------------------------------------

            //var vList = new object();

            //vList = new
            //{
            //    aaData = (from item in objPastEmploymentList
            //              select new
            //              {

            //                  PastEmploymentID = item.PastEmploymentID,
            //                  FileName = "<a href='" + item.FilePath + "' target='_blank'  title='" + item.FileName + "' ><i class='fa fa-pencil'></i> " + item.FileName + "</a>",
            //                  Description = item.Description,
            //                  Size = item.FileSize,
            //                  Type = item.FileType,                                                          
            //                  Actions = "<a class='btn btn-primary' onclick='DeleteChannel(" + item.AttatchmentId.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
            //              }).ToArray(),               
            //};


            return PartialView("InternalEmploymentDocs", objInternalEmployment);
        }




        [HttpPost]
        public ActionResult UploadInternalEmploymentDocs(HttpPostedFileBase file, int EmploymentID, string Description, string FileType)
        {

            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();
            InternalEmploymentModel objInternalEmploymentModel = new InternalEmploymentModel();
            try
            {

                //for (int i = 0; i < Request.Files.Count; i++)
                //{

                //}
                string Fullpath = "";
                string filename = "";
                string type = "";
                if (file != null && file.ContentLength > 0)
                {
                    var Directory = Server.MapPath(ConfigurationManager.AppSettings["InternalEmploymentDocsPath"].ToString());
                    //file.SaveAs(path);
                    FileUploadHelper.UploadFile(Directory, file, EmploymentID, out Fullpath, out filename);
                    objInternalEmploymentModel.InternalEmploymentID = EmploymentID;
                    objInternalEmploymentModel.FileName = filename;
                    objInternalEmploymentModel.FilePath = Directory + filename;
                    objInternalEmploymentModel.FileType = FileType;
                    objInternalEmploymentModel.Description = Description;
                    objInternalEmploymentModel.CreatedBy = LoginUser;
                    if (file.ContentLength >= 1048576)
                    {
                        objInternalEmploymentModel.FileSize = Convert.ToString(file.ContentLength / 1048576) + " MB";
                    }
                    else if (file.ContentLength >= 1024)
                    {
                        objInternalEmploymentModel.FileSize = Convert.ToString(file.ContentLength / 1024) + " KB";
                    }
                    else if (file.ContentLength < 1024)
                    {
                        objInternalEmploymentModel.FileSize = Convert.ToString(file.ContentLength) + " bytes";
                    }
                    string message = objInternalEmploymentDB.InsertInternalEmploymentDocs(objInternalEmploymentModel);
                    objInternalEmploymentModel.objInternalEmploymentList = objInternalEmploymentDB.GetInternalEmploymentDocs(EmploymentID);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return Json(new PastEmploymentModel { PastEmploymentID = objPastEmploymentModel.PastEmploymentID }, JsonRequestBehavior.AllowGet);
            return new HttpStatusCodeResult(200, "Inserted");
        }


        public ActionResult RemoveInternalEmploymentDocs(int Id)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();
            operationDetails = objInternalEmploymentDB.DeleteInternalEmploymentDocs(Id);
            if (operationDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Internal Employment Attatchment Removed Successfully.";

            }
            else
            {
                result = "error";
                resultMessage = "Error occured while removing Internal Employment Attatchment.";
            }

            return Json(new { AttatchmentId = Id, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DownloadInternalEmploymentDocs(string filename)
        {
            string PastEmploymentDocsPath = Server.MapPath(ConfigurationManager.AppSettings["InternalEmploymentDocsPath"].ToString());
            //StandardFramework objStandardFramework = new StandardFramework(); 
            string[] extension = filename.Split('.');
            var path = Path.Combine(PastEmploymentDocsPath, filename);
            return File(path, "application/all", extension[0] + "." + extension[1]);

        }

        #endregion

        #region Employee ExpenseReport

        public ActionResult AddExpenseReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            //ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId);
            ViewBag.EpenseReportApprover = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId);
            ExpenseReportModel objExpenseReportModel = new ExpenseReportModel();
            objExpenseReportModel.ExpenseReportTempId = Guid.NewGuid();
            ViewBag.BillTo = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FirstName");
            ViewBag.EmployeeID = objUserContextViewModel.UserId;
            return View(objExpenseReportModel);
        }

        [HttpPost]
        public ActionResult AddExpenseReport(ExpenseReportModel objExpenseReportModel, HttpPostedFileBase expenseReportAttachment)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (ModelState.IsValid)
            {
                ExpenseReportDB objExpenseReportDB = new ExpenseReportDB();

                objExpenseReportModel.CreatedBy = objUserContextViewModel.UserId;
                objExpenseReportModel.ApproverId = objUserContextViewModel.UserId;
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Uploads/ExpenseReport"));
                if (expenseReportAttachment != null)
                {
                    expenseReportAttachment.SaveAs(Server.MapPath("~/Uploads/ExpenseReport/" + Guid.NewGuid().ToString() + expenseReportAttachment.FileName));
                    objExpenseReportModel.Attachments = "~/Uploads/ExpenseReport/" + Guid.NewGuid().ToString() + expenseReportAttachment.FileName;
                }
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objExpenseReportDB.InsertExpenseReport(objExpenseReportModel);
                List<ExpenseReportDetails> objExpenseReportDetailsList = new List<ExpenseReportDetails>();
                if (Session[objExpenseReportModel.ExpenseReportTempId.ToString()] != null)
                {
                    objExpenseReportDetailsList = (List<ExpenseReportDetails>)Session[objExpenseReportModel.ExpenseReportTempId.ToString()];
                }
                foreach (var item in objExpenseReportDetailsList)
                {
                    item.ExpenseReportId = objOperationDetails.InsertedRowId;
                    objExpenseReportDB.InsertExpenseReportDetails(item);
                }
                return RedirectToAction("EmployeeProfile");
                //return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.EpenseReportApprover = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId);
                return View(objExpenseReportModel);

                //return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddExpenseReportDetails(string jsonofExpenseReportDetailsObject)
        {
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            ExpenseReportDetails objExpenseReportDetails = JsonConvert.DeserializeObject<ExpenseReportDetails>(jsonofExpenseReportDetailsObject);
            objExpenseReportDetails.ExpenseReportDetailsTempId = Guid.NewGuid();
            decimal? Tax = (objExpenseReportDetails.CostPerUnit * objExpenseReportDetails.Units * objExpenseReportDetails.Tax) / 100;
            objExpenseReportDetails.Total = (objExpenseReportDetails.CostPerUnit * objExpenseReportDetails.Units) + Tax;
            List<ExpenseReportDetails> objExpenseReportDetailsList = new List<ExpenseReportDetails>();
            if (Session[objExpenseReportDetails.ExpenseReportTempId.ToString()] != null)
            {
                objExpenseReportDetailsList = (List<ExpenseReportDetails>)Session[objExpenseReportDetails.ExpenseReportTempId.ToString()];
            }
            objExpenseReportDetailsList.Add(objExpenseReportDetails);
            Session[objExpenseReportDetails.ExpenseReportTempId.ToString()] = objExpenseReportDetailsList;
            return Json(objExpenseReportDetails, JsonRequestBehavior.AllowGet);
            //return View(objExpenseReportDetails);
        }

        public ActionResult RefereshExpenseReportEntires(string TempId)
        {
            List<ExpenseReportDetails> objExpenseReportDetailsList = new List<ExpenseReportDetails>();
            if (Session[TempId] != null)
            {
                objExpenseReportDetailsList = (List<ExpenseReportDetails>)Session[TempId];
            }
            ViewBag.objExpenseReportDetailsList = objExpenseReportDetailsList;
            return View();
        }

        #endregion

        public ActionResult UpdateStatus(int Id, string status)
        {
            string result = "";
            string resultMessage = "";
            OperationDetails opertaionDetails = new OperationDetails();
            EmployeeDB employeeDB = new EmployeeDB();
            HRMS.Entities.Employee employee = new Entities.Employee();
            employee.EmployeeId = Id;
            employee.employeeDetailsModel.isActive = Convert.ToBoolean(status);
            opertaionDetails = employeeDB.UpdateEmployeeStatus(employee);

            if (opertaionDetails.Message == "Success")
            {
                result = "success";
                if (Convert.ToBoolean(status) == false)
                {
                    resultMessage = "Employee deactivated successfully";
                }
                else
                {
                    resultMessage = "Employee activated successfully";
                }

            }
            return Json(new { result = result, resultMessage = resultMessage, EmployeeId = Id }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewTerminateEmployee(int Id)
        {
            TerminatedEmployee terminateEmployee = new TerminatedEmployee();
            terminateEmployee.EmployeeId = Id;
            return PartialView("_TerminateEmployee", terminateEmployee);
        }

        public ActionResult TerminateEmployee(TerminatedEmployee terminatedEmployee)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            string result = "";
            string resultMessage = "";
            if (ModelState.IsValid)
            {
                OperationDetails opertaionDetails = new OperationDetails();
                EmployeeDB employeeDB = new EmployeeDB();
                terminatedEmployee.CreatedBy = objUserContextViewModel.UserId;
                opertaionDetails = employeeDB.TerminateEmployee(terminatedEmployee);

                if (opertaionDetails.Message == "Success")
                {
                    result = "success";
                    resultMessage = "Employee terminated successfully";
                }
                else
                {
                    result = "error";
                    resultMessage = "Error occured during teminating employee.";
                }
            }
            else
            {
                result = "warning";
                resultMessage = "Please enter all termination details.";
            }
            return Json(new { result = result, resultMessage = resultMessage, EmployeeId = terminatedEmployee.EmployeeId }, JsonRequestBehavior.AllowGet);
        }

        public void ExportEmployeeToPDF(int Id)
        {

            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "EmployeeRecord" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int companyid = objUserContextViewModel.CompanyId;

            HRMS.Entities.Employee objEmployee = new HRMS.Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objEmployee = objEmployeeDB.GetEmployee(Id);
            //List<object> objEmployeelist = objEmployee as List<object>;
            List<HRMS.Entities.Employee> employeeList = new List<HRMS.Entities.Employee>();
            employeeList.Add(objEmployee);
            var report = (from item in employeeList
                          select new
                          {
                              //Id = item.Id,
                              EmployeeId = item.EmployeeId,
                              FirstName = item.FirstName,
                              LastName = item.LastName,
                              Gender = item.genderModel.GenderName_1,
                              DateOfBirth = item.employeeDetailsModel.BirthDate,
                              CompanyId = item.CompanyId
                          });
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            System.Web.UI.WebControls.GridView gridvw = new System.Web.UI.WebControls.GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


        }


        //public void ExportEmployeeToPDF(int Id)
        //{

        //    Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

        //    string fileName = "EmployeeRecord" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    //ProfileDB profileDB = new ProfileDB();

        //    int companyid = objUserContextViewModel.CompanyId;

        //    EmployeeDB objEmpDB = new EmployeeDB();
        //    HRMS.Entities.Employee employeeModel = objEmpDB.ViewEmployeeProfile(Id);

        //                //List<object> objEmployeelist = objEmployee as List<object>;
        //    List<HRMS.Entities.Employee> employeeList = new List<HRMS.Entities.Employee>();
        //   // employeeList.Add(objEmployee);
        //    //var report = (from item in employeeList
        //    //              select new
        //    //{
        //    //    //Id = item.Id,
        //    //    EmployeeId = item.EmployeeId,
        //    //    FirstName = item.FirstName,
        //    //    LastName = item.LastName,
        //    //    Gender = item.genderModel.GenderName_1,
        //    //    DateOfBirth = item.employeeDetailsModel.BirthDate != null ? item.employeeDetailsModel.BirthDate.Value.ToShortDateString(): "",
        //    //    CompanyId = item.CompanyId
        //    //});
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    //System.Web.UI.WebControls.GridView gridvw = new System.Web.UI.WebControls.GridView();
        //    //gridvw.DataSource = report; //bind the data table to the grid view
        //    //gridvw.DataBind();


        //    StringWriter swr = new StringWriter();
        //    HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        // //   gridvw.RenderControl(tw);


        //    StringReader sr = new StringReader(swr.ToString());
        //    Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        //    pdfDoc.Open();
        //    htmlparser.Parse(sr);
        //    pdfDoc.Close();
        //    Response.Write(pdfDoc);
        //    Response.End();


        //}


        public ActionResult UpdateEmployeeStatus(int Id, string status)
        {
            string result = "";
            string resultMessage = "";
            OperationDetails opertaionDetails = new OperationDetails();
            EmployeeDB employeeDB = new EmployeeDB();
            HRMS.Entities.Employee employee = new Entities.Employee();
            employee.EmployeeId = Id;
            employee.employeeDetailsModel.isActive = Convert.ToBoolean(status);
            opertaionDetails = employeeDB.UpdateEmployeeStatus(employee);

            if (opertaionDetails.Message == "Success")
            {
                result = "success";
                if (status == "true")
                {
                    resultMessage = "Employee activated successfully";
                }
                else
                {
                    resultMessage = "Employee deactivated successfully";
                }

            }
            return Json(new { result = result, resultMessage = resultMessage, EmployeeId = Id }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEmployeeLeaveReason(int EmployeeId, string status)
        {
            EmploymentInformation ObjEmploymentInformation = new EmploymentInformation();
            ObjEmploymentInformation.EmployeeID = EmployeeId;
            ObjEmploymentInformation.EmployeeStatus = Convert.ToBoolean(status);
            LeaveReasonDB objLeaveReasonDB = new LeaveReasonDB();
            ViewBag.LeaveReasonList = new SelectList(objLeaveReasonDB.GetAllLeaveReason(), "ID", "LeaveReason_1");
            return PartialView("_AddEmployeeLeaveReasonPartial", ObjEmploymentInformation);
        }

        public ActionResult UpdateRejoingDate(int EmployeeId)
        {
            EmploymentInformation ObjEmploymentInformation = new EmploymentInformation();
            ObjEmploymentInformation.EmployeeID = EmployeeId;
            return PartialView("_UpdateRejoiningDatePartial", ObjEmploymentInformation);
        }

        public ActionResult UpdateRejoingDateAndStatus(EmploymentInformation objEmpInfo)
        {
            string result = "";
            string resultMessage = "";
            OperationDetails opertaionDetails = new OperationDetails();
            EmployeeDB employeeDB = new EmployeeDB();
            opertaionDetails = employeeDB.UpdateEmploymeeJoingDateWithStatus(objEmpInfo);

            if (opertaionDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Employee activated successfully";
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateEmployeeLeaveReason(EmploymentInformation ObjEmploymentInformation)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            EmployeeDB objEmployeeDB = new EmployeeDB();
            operationDetails = objEmployeeDB.UpdateEmploymeeLeaveReason(ObjEmploymentInformation);
            result = operationDetails.Message;
            if (result == "Success")
            {
                result = "success";
            }
            else
            {
                result = "error";
            }
            return Json(new { result = result, id = ObjEmploymentInformation.EmployeeID, status = ObjEmploymentInformation.EmployeeStatus }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddIMContact()
        {
            GenderDB ObjGenderDB = new GenderDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Selected", Value = "0" });
            foreach (var m in ObjGenderDB.GetTypes("stp_Gen_GetIMType"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.IMAddressTypeList = ObjSelectedList;
            return View();
        }

        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public ActionResult GetUserImage(int EmployeeId)
        {
            EmployeeDB employeeDB = new EmployeeDB();
            var dataContain = employeeDB.GetUserImage(EmployeeId);
            if (dataContain.Count() > 0)
                return File(employeeDB.GetUserImage(EmployeeId), "image/png");
            else
            {
                return File(CommonHelper.CommonHelper.GetBlankImage(), "image/png");
            }
        }

        public ActionResult GetEmployeeSectionAsPerDepartment(int DeptId)
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Section", Value = "0" });

            foreach (var m in new EmployeeSectionDB().GetEmployeeSectionByDepartmentID(DeptId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.EmployeeSectionName_1, Value = m.EmployeeSectionID.ToString() });

            }

            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetEmployeeDepartments(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Department", Value = "0" });

            foreach (var m in new DepartmentDB().GetAllDepartmentByCompanyId(companyId, objUserContextViewModel.UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.DepartmentName_1, Value = m.DepartmentId.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPositions(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Position", Value = "0" });

            foreach (var m in new PositionDB().GetPositionList(companyId: companyId, userId: objUserContextViewModel.UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PositionTitle, Value = m.PositionID.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckUniqueNationalityId(int EmployeeId, string NataionalityIdCardNo)
        {
            bool isUnique = false;
            EmployeeDB objEmpDB = new EmployeeDB();
            isUnique = objEmpDB.checkUniqueNationalityId(EmployeeId, NataionalityIdCardNo);
            return Json(new { isSuccess = isUnique }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult CheckUni_BanAccountNo(int DirectDepositID, int EmployeeeId, string UniAccountNumber)
        {
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();

            if (payDirectDepositDB.CheckUni_BanAccountNumber(DirectDepositID, EmployeeeId, UniAccountNumber))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetImageCropper(string id, string chkImageFor, string fileName, string fileExtention)
        {
            Session["fileName"] = fileName;
            Session["fileExtention"] = fileExtention;
            ViewBag.imagePath = id;
            ViewBag.chkImageFor = chkImageFor;
            ViewBag.fileName = fileName;
            return PartialView("~/Views/Shared/GetImageCropper.cshtml");
        }
        public ActionResult GetImageCropperTest(string id, string chkImageFor, string fileName, string fileExtention)
        {

            return PartialView("~/Views/Shared/GetImageCropper.cshtml");
        }
        public ActionResult UploadImage(string base64String, string chkImageForId)
        {
            base64String = base64String.Replace("data:image/jpeg;base64,", "");
            byte[] bytes = Convert.FromBase64String(base64String);
            string filename = (string)Session["fileName"] + DateTime.Now.ToString("ddMMyyyyhhmmss") + "." + (string)Session["fileExtention"];
            var filePathOriginal = "";
            string savedFileName = "";
            string dbFilePath = "";
            if (chkImageForId.Equals("modalL"))
            {
                filePathOriginal = Server.MapPath("~/Content/images/Logo");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/Logo/" + filename;
                Session["LargeLogoPath"] = dbFilePath;
            }
            else if (chkImageForId.Equals("modalS"))
            {
                filePathOriginal = Server.MapPath("~/Content/images/Logo");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/Logo/" + filename;
                Session["SmallLogoPath"] = dbFilePath;
            }
            else if (chkImageForId.Equals("modalR"))
            {
                filePathOriginal = Server.MapPath("~/Content/images");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/" + filename;
                Session["ReportLogoPath"] = dbFilePath;
            }
            else
            {
                filePathOriginal = Server.MapPath("~/Content/images");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/" + filename;
                Session["ProfileLogo"] = dbFilePath;
                Session["ProfileFileExtension"] = (string)Session["fileExtention"];
            }


            FileUploadHelper.UploadFile(bytes, savedFileName);
            Session["fileName"] = null;
            Session["fileExtention"] = null;

            return Json(new { chkImageForId = chkImageForId }, JsonRequestBehavior.AllowGet);
        }
    }
}