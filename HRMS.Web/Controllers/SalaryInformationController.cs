﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class SalaryInformationController : BaseController
    {
        //
        // GET: /SalaryInformation/
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/SalaryInformation/Index");
            Session["SalaryPermission"] = permissionModel;
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);

            if (Session["EmployeeListID"] != null)
            {

                ViewBag.empid = Session["EmployeeListID"].ToString();
            }

            return View(employeeDetailsModelList);
        }

        public JsonResult EmployeeSearch(string search)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyIdandEmpName(objUserContextViewModel.CompanyId, search);

            return Json(employeeDetailsModelList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSalaryByEmployee(int id)
        {
            Session["EmployeeListID"] = id;
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            var paySalaryList = paySalaryDB.GetEmployeeSalaryById(id);
            return PartialView("_PaySalaryList", paySalaryList);
        }

        public ActionResult AddSalaryInformation(int EmployeeId = 0, int id = 0)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            PaySalaryModel paySalaryModel = null;
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            if (id == 0)
            {
                paySalaryModel = new PaySalaryModel();
            }
            else
            {
                paySalaryModel = paySalaryDB.GetEmployeeSalaryById(EmployeeId).FirstOrDefault(x => x.PaySalaryID == id);
            }

            ViewBag.SalaryAllowanceList = new SelectList(paySalaryDB.GetAllPaySalaryAllowanceList(0).Where(x => x.PaySalaryAllowanceID>0).ToList(), "PaySalaryAllowanceID", "PaySalaryAllowanceName_1");

            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1).ToList(), "EmployeeId", "FullName");
            return PartialView(paySalaryModel);
        }

        public ActionResult PaySalaryCrud(PaySalaryModel paySalaryModel)
        {
            OperationDetails OperationDetails = new OperationDetails();
            try
            {
                PaySalaryDB paySalaryDB = new PaySalaryDB();
                PayrollDB payrollDb = new PayrollDB();
                AdditionPaidCycleDB AdditionDb = new AdditionPaidCycleDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                paySalaryModel.TrDate = paySalaryModel.TrDate == null ? DateTime.Now.ToString("dd/MM/yyyy") : paySalaryModel.TrDate;
                paySalaryModel.AllowanceEndDate = paySalaryModel.AllowanceEndDate == null ? DateTime.Now.ToString("dd/MM/yyyy") : paySalaryModel.AllowanceEndDate;
                paySalaryModel.Note = paySalaryModel.Note == null ? "" : paySalaryModel.Note;

                PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
                PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
                objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 1).FirstOrDefault();
                objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
                bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
                bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
                bool IsDeductionSendForApproval = false;
                if (IsConfirmRunPayroll && IsSendForConfirmation)
                {
                    IsDeductionSendForApproval = true;
                }
                else
                {
                    IsDeductionSendForApproval = false;
                }

                if (paySalaryModel.PaySalaryID > 0)
                {
                    if (AdditionDb.GetAllPaySalaryAllowanceList(true, false).Where(x => x.PaySalaryAllowanceID == paySalaryModel.PaySalaryAllowanceID).Count() > 0 || !paySalaryModel.IsActive)
                    {
                        if (!payrollDb.GetPayrollGeneralSetting().IsconfirmRunPayroll || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                        {
                            paySalaryDB.PaySalaryCRUD(paySalaryModel, 2);
                            OperationDetails.CssClass = "DirectUpdate";
                            OperationDetails.Message = "Pay salary information details updated successfully.";
                        }
                        else
                        {
                            paySalaryDB.SavePayrollConfirmationRequestForPaySalary(paySalaryModel, objUserContextViewModel.UserId, 2);
                            OperationDetails.CssClass = "Request";
                            OperationDetails.Message = "Pay salary modification request send for confirmation.";
                        }
                        OperationDetails.Success = true;

                    }
                    else
                    {
                        OperationDetails.Success = false;
                        OperationDetails.CssClass = "error";
                        OperationDetails.Message = "This salary allowance no longer exist so you cannot update this record.";
                    }
                }
                else
                {
                    if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                    {
                        paySalaryDB.PaySalaryCRUD(paySalaryModel, 1);
                        OperationDetails.CssClass = "DirectSave";
                        OperationDetails.Message = "Pay salary information details save successfully.";
                    }
                    else
                    {
                        paySalaryDB.SavePayrollConfirmationRequestForPaySalary(paySalaryModel, objUserContextViewModel.UserId, 1);
                        OperationDetails.CssClass = "Request";
                        OperationDetails.Message = "Pay salary modification request send for confirmation.";
                    }
                    OperationDetails.Success = true;
                }
            }
            catch (Exception ex)
            {
                OperationDetails.Success = false;
                OperationDetails.CssClass = "error";
                OperationDetails.Message = "Technical error has occurred";
            }
            return Json(OperationDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSalaryInformation(PaySalaryModel paySalaryModel)
        {
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails operationDetails;

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 1).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                operationDetails = paySalaryDB.PaySalaryCRUD(paySalaryModel, 3);
                operationDetails.CssClass = "DirectDelete";
                if (operationDetails.Success)
                    operationDetails.Message = "Pay salary details deleted successfully.";
                else
                    operationDetails.Message = "Technical error has occurred while deleting salary.";
            }
            else
            {
                var paySalaryList = paySalaryDB.GetEmployeeSalaryById(paySalaryModel.EmployeeId);
                paySalaryModel = paySalaryList.FirstOrDefault(x => x.PaySalaryID == paySalaryModel.PaySalaryID);
                operationDetails = paySalaryDB.SavePayrollConfirmationRequestForPaySalary(paySalaryModel, objUserContextViewModel.UserId, 3);
                operationDetails.CssClass = "Request";
                if (operationDetails.Success)
                    operationDetails.Message = "Pay salary modification request send for confirmation.";
                else
                    operationDetails.Message = "Technical error has occurred while deleting salary.";

            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSalaryByPaySalaryID(int id, int employeeID)
        {
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            var paySalary = paySalaryDB.GetEmployeeSalaryById(employeeID).Where(x => x.PaySalaryID == id).FirstOrDefault(); ;
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();

            var data = new
            {
                PaySalaryAllowanceID = paySalary.PaySalaryAllowanceID,
                Amount = paySalary.Amount.ToString(AmountFormat),
                IsActive = paySalary.IsActive,
                TrDate = paySalary.TrDate,
                Note = paySalary.Note
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartData(int id)
        {
            Session["EmployeeListID"] = id;
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            var paySalaryList = paySalaryDB.GetEmployeeSalaryById(id).Where(x => x.IsActive == true).ToList();
            float totalAmount = paySalaryList.Sum(x => x.Amount);

            var vList = new
            {
                aaData = (from item in paySalaryList.GroupBy(x => x.PaySalaryAllowanceID)
                          select new
                          {
                              //  var pointPercentage = Math.Round(Convert.ToDecimal(Convert.ToDecimal(a[i].Strength.ToString()) / Convert.ToDecimal(TotalStrength)), 2);

                              name = item.FirstOrDefault().PaySalaryAllowanceName_1,
                              y = Math.Round((Convert.ToDecimal(Convert.ToDecimal(item.Sum(x => x.Amount)) / Convert.ToDecimal(totalAmount))), 2),
                          }).ToArray()

            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updatePaySalaryStatus(int Id, string status)
        {
            string result = "";
            string resultMessage = "";
            OperationDetails opertaionDetails = new OperationDetails();

            PaySalaryDB objPaySalaryDB = new PaySalaryDB();
            HRMS.Entities.PaySalaryModel objPaySalaryModel = new HRMS.Entities.PaySalaryModel();





            objPaySalaryModel.PaySalaryID = Id;
            objPaySalaryModel.IsActive = Convert.ToBoolean(status) == true ? true : false;

            opertaionDetails = objPaySalaryDB.UpdatePaySalaryStatus(objPaySalaryModel);

            if (opertaionDetails.Message == "Success")
            {
                result = "success";
                if (status == "true")
                {
                    resultMessage = "Pay Salary   activated successfully";
                }
                else
                {

                    resultMessage = "Pay  Salary   deactivated successfully";
                }

            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewSalaryInformation(int EmployeeId = 0, int id = 0)
        {
            PaySalaryModel paySalaryModel = null;
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            if (id == 0)
            {
                paySalaryModel = new PaySalaryModel();
            }
            else
            {
                paySalaryModel = paySalaryDB.GetEmployeeSalaryById(EmployeeId).FirstOrDefault(x => x.PaySalaryID == id);
            }
            //ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId().Where(x => x.IsActive == 1).ToList(), "EmployeeId", "FullName");
            return PartialView(paySalaryModel);
        }
    }
}