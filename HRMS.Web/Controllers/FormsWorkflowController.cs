﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class FormsWorkflowController : BaseController
    {
        FormsWorkflowDB formsWorkflowDB;
        public FormsWorkflowController()
        {
          formsWorkflowDB = new FormsWorkflowDB();
          XMLLogFile = "LoggerFormsWorkflow.xml";
        }
        // GET: FormsWorkflow
        public ActionResult Index()
        {
            ViewBag.Company =  GetCompanySelectList();     
            ViewBag.FormList  = new SelectList(new FormsUniqueKeyDB().GetAllFormsUniqueKey(0), "FormID", "FormName");
            List<FormsWorkflowGroupModel> formsWorkflowGroupModelList = formsWorkflowDB.GetAllFormsWorkflowGroups(null,true,true);
            string groups = "";
            groups += "<option value='0'>Select Group</option>";
            foreach (var obj in formsWorkflowGroupModelList)
            {
                groups += "<option value='" + obj.GroupID + "'>" + obj.GroupName + "</option>";
            }
            ViewBag.Group = groups;

            List<FormsWorkflowPriorityModel> formsWorkflowPriorityModelList = formsWorkflowDB.GetAllWorkflowPriorityList(null);
            string priority = "";
            priority += "<option value='0'>Select Priority</option>";          
            foreach (var obj in formsWorkflowPriorityModelList)
            {
                priority += "<option value='" + obj.PriorityListID + "'>" + obj.PriorityListName + "</option>";
            }
            ViewBag.Priority = priority;
            return View();
        }
     
        public ActionResult GetAllFormsWorkflow(Int16 companyId, Int16? formID)
        {
            List<FormsWorkflowModel> formsWorkflowModelList = new List<FormsWorkflowModel>();
            formsWorkflowModelList = formsWorkflowDB.GetAllFormsWorkflow(companyId, formID);
            int companyTypeId = formsWorkflowModelList.FirstOrDefault().CompanyTypeID;
            if(companyTypeId == 2)
                formsWorkflowModelList = formsWorkflowModelList.Where(s=>s.GroupID != 2).ToList();
            CompanyBasedHeirarchyDB objCompany = new CompanyBasedHeirarchyDB();
          
            int i=0;
            var vList = new object();
            vList = new
            {
                aaData = (from item in formsWorkflowModelList
                          select new
                          {
                              WorkFlowID = item.WorkflowID,
                              FormID = item.FormID,
                              FormName = item.FormName,
                              CompanyTypeID = item.CompanyTypeID,
                              GroupID = item.GroupID,
                              GroupName = item.GroupName,
                              SchoolBasedGroup = item.SchoolBasedGroup,
                              PriorityListID = item.PriorityListID,
                              PriorityListName = string.IsNullOrEmpty(item.PriorityListName)?"Select Priority": item.PriorityListName,
                              FinalApprovalNotification = (item.FinalApprovalNotification) ? "<input data-toggle='tooltip' data-placement='top' data-original-title='Email notification on Final Approval'  type='checkbox' id=chk_" + item.WorkflowID + " checked  disabled='disabled' >" : "<input data-toggle='tooltip' data-placement='top' data-original-title='Email notification on Final Approval'  type='checkbox' id=chk_" +item.WorkflowID + "  disabled='disabled'>",
                              BaseGroupID = item.BaseGroupID,
                              BaseGroupName = !string.IsNullOrEmpty(item.BaseGroupName)? item.BaseGroupName : (item.SchoolBasedGroup)?"": "Select Group",                              
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditWorkflow(this," + item.WorkflowID + "," + item.PriorityListID + "," + Convert.ToInt32(item.FinalApprovalNotification) + "," + item.BaseGroupID + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> "                            
                             
                          }).ToArray()
            };
            //+
            //"<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"ChangeStatus(this," + item.WorkflowID + "," + item.PriorityListID + "," + Convert.ToInt32(item.FinalApprovalNotification) + "," + item.BaseGroupID + ")\" title='Delete'><i class='fa fa-times'></i> </a>"
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        [HttpPost]
        public ActionResult SaveFormsWorkflowDetails(List<FormsWorkflowModel> formsWorkflowList)
        {
            OperationDetails op = new OperationDetails();
            FormsWorkflowModelSaver formsWorkflow = new FormsWorkflowModelSaver();
            formsWorkflow.FormsWorkflowList = formsWorkflowList;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            formsWorkflow.ModifiedBy = objUserContextViewModel.UserId;
            op = new FormsWorkflowDB().SaveAll(formsWorkflow);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateFormsWorkflow(FormsWorkflowModel formsWorkflow)
        {
            OperationDetails op = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            formsWorkflow.ModifiedBy = objUserContextViewModel.UserId;
            op = formsWorkflowDB.UpdateFormsWorkflow(formsWorkflow);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IsPendingApprovalExists(int groupID, int formID, int companyTypeID, int PriorityListID,int UpdatedPriorityListID)
        {
            return Json(formsWorkflowDB.IsPendingApprovalExists(groupID, formID, companyTypeID, PriorityListID, UpdatedPriorityListID), JsonRequestBehavior.AllowGet);
        }
        public ActionResult MarkToCompletePendingApprovals(int groupID, int formID, int companyTypeID, int PriorityListID, int UpdatedPriorityListID)
        {
            return Json(formsWorkflowDB.MarkToCompletePendingApprovals(groupID, formID, companyTypeID, PriorityListID,UpdatedPriorityListID), JsonRequestBehavior.AllowGet);
        }
    }
}