﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using System.Data;
using HRMS.Entities.ViewModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using HRMS.Entities.General;

namespace HRMS.Web.Controllers
{
    public class YearController: BaseController
    {
        //
        // GET: / /
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult GetYearList()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];            
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<YearModel> objYearList = new List<YearModel>();
            YearDB objYearDB = new YearDB();
            objYearList = objYearDB.GetAllYears();
            
            var vList = new object();

            vList = new
            {
                aaData = (from item in objYearList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.YearID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.YearID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              YearID = item.YearID,
                              
                              WeekStartDay = ((WeekdaysEnum)item.WeekStartDay).ToString(),
                              WeekEndDay = ((WeekdaysEnum)item.WeekEndDay).ToString(),
                              StartMonth = ((MonthsEnum)item.StartMonth).ToString(),
                              EndMonth = ((MonthsEnum)item.EndMonth).ToString(),
                              
                                                  
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            YearModel objYear = new YearModel();
            YearDB objYearDB = new YearDB();

            var monthValues = Enum.GetValues(typeof(MonthsEnum)).Cast<MonthsEnum>();
            var weekValues = Enum.GetValues(typeof(WeekdaysEnum)).Cast<WeekdaysEnum>();

            ViewBag.Month = from value in monthValues
                                                select new SelectListItem
                                                {
                                                    Text = value.ToString(),
                                                    Value = value.ToString(),
                                                };

            ViewBag.Week = from value in weekValues
                            select new SelectListItem
                            {
                                Text = value.ToString(),
                                Value = value.ToString(),
                            };

            if (id != 0)
            {
                objYear = objYearDB.Get_YearByYearId(id);                    
                return View(objYear);
            }
            else
            {
                objYear = new YearModel();
                return View(objYear);
            }
            
        }

        public ActionResult Save(YearModel objYear)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            YearDB objYearDB = new YearDB();

            if (objYear.YearID == 0)
            {
                objYear.CompanyId = objUserContextViewModel.CompanyId;
                objYear.CreatedBy = objUserContextViewModel.UserId;
                operationDetails = objYearDB.AddYear(objYear);

            }
            else
            {
                objYear.CompanyId = objUserContextViewModel.CompanyId;
                objYear.ModifiedBy = objUserContextViewModel.UserId;
                operationDetails = objYearDB.UpdateYear(objYear);

            }

            if (operationDetails.Message == "Success")
            {
                if (objYear.YearID == 0)
                {
                    result = "success";
                    resultMessage = "Year Details Added Successfully.";
                }
                else if (objYear.YearID != 0)
                {
                    result = "success";
                    resultMessage = "Year Details Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Year Details.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);

        }        

        public JsonResult Delete(string id)
        {
            YearDB objYearDB = new YearDB();
            YearModel objYear = new YearModel();
            objYear.YearID = int.Parse(id);
            objYearDB.DeleteYear(objYear);

            return Json(new { result = "success", resultMessage = "Year Deleted Successfully." }, JsonRequestBehavior.AllowGet);
            //return Redirect("Index");
        }

        public ActionResult ExportToExcel()
        {
            byte[] content;
            string fileName = "Year" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];                          
            List<YearModel> objYearList = new List<YearModel>();
            YearDB objYearDB = new YearDB();
            objYearList = objYearDB.GetAllYears().Where(x=>x.YearID != 0).OrderBy(x=>x.WeekStartDay).ToList();
            var report = (from item in objYearList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              YearID = item.YearID,
                              WeekStartDay = item.WeekStartDay,
                              WeekEndDay = item.WeekEndDay,
                              StartMonth = (MonthsEnum)item.StartMonth,
                              EndMonth = (MonthsEnum)item.EndMonth,                   
                          }).ToList();

            DataTable dt = new DataTable();
            dt.Columns.Add("Week Start Day");
            dt.Columns.Add("Week End Day");
            dt.Columns.Add("Start Month");
            dt.Columns.Add("End Month");
            foreach (var item in objYearList)
            {
                DataRow dr = dt.NewRow();
                dr[0] = item.WeekStartDay;
                dr[1] = item.WeekEndDay;
                dr[2] = item.StartMonth;
                dr[3] = item.EndMonth;
                dt.Rows.Add(dr);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {   
            string fileName = "Year" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];           
            List<YearModel> objYearList = new List<YearModel>();
            YearDB objYearDB = new YearDB();
            objYearList = objYearDB.GetAllYears().Where(x => x.YearID != 0).OrderBy(x => x.WeekStartDay).ToList();
            var report = (from item in objYearList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              YearID = item.YearID,
                              WeekStartDay = item.WeekStartDay,
                              WeekEndDay = item.WeekEndDay,
                              StartMonth = (MonthsEnum)item.StartMonth,
                              EndMonth = (MonthsEnum)item.EndMonth,                              
                          }).ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add("Week Start Day");
            dt.Columns.Add("Week End Day");
            dt.Columns.Add("Start Month");
            dt.Columns.Add("End Month");
            foreach (var item in objYearList)
            {
                DataRow dr = dt.NewRow();
                dr[0] = item.WeekStartDay;
                dr[1] = item.WeekEndDay;
                dr[2] = item.StartMonth;
                dr[3] = item.EndMonth;
                dt.Rows.Add(dr);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();
        }
    }
}