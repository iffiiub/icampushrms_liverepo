﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.ViewModel;
using HRMS.Entities;
using System.Threading.Tasks;
using HRMS.DataAccess;
using System.Web.SessionState;
using System.Globalization;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class LongOperationsController : BaseController
    {
        public async Task<ActionResult> GetAttendanceCountNew()
        {
            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();
            //-------------------------------------------------------------Daily attendance based on user logged in
            int? employeeId = null;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserRoleDB userRoleDB = new UserRoleDB();
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            //------------------------------------------------------------
            objDailyAttendanceViewModelList = await objDailyAttendanceViewDB.GetDailyAttendanceViewListAsync(DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"), employeeId, null, null, null,
                null, null, null, null);
            //---------Sort Data by UserGroupAcessList permission------------

            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);

            objDailyAttendanceViewModelList = (from attEmp in objDailyAttendanceViewModelList
                                               join UGA in objEmployeeList on attEmp.HRID equals UGA.EmployeeId
                                               select attEmp).ToList();

            //----------------------------------------------------------------
            string[] a = new string[3];
            string[] s = new string[12];
            if (objDailyAttendanceViewModelList.Count > 0)
            {
                for (int i = 0; i <= 11; i++)
                {
                    s[i] = objDailyAttendanceViewModelList.Where(x => x.StatusID == i).Count().ToString();

                }
            }
            else
            {
                for (int i = 0; i <= 11; i++)
                {
                    s[i] = "0";
                }
            }
            return Json(s, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetDailyAttendanceViewList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status",
       int iSortCol_0 = 0, string FromDate = "", string ToDate = "", string DepID = "", string statusid = "", string empid = "", string sectionid = "", string companyId = "")
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            bool? Proceed = null, active = null;
            if (empid == "0") empid = "";
            if (DepID == "0") DepID = "";
            if (companyId == "0") companyId = "";
            int? employeeId = null, statusId = null, aIntDepartmentID = null, aIntSectionID = null, aIntShiftID = null, UserId = null, aCompanyId = null;
            if (!string.IsNullOrEmpty(empid))
                employeeId = Convert.ToInt32(empid);
            if (!string.IsNullOrEmpty(statusid))
                statusId = Convert.ToInt32(statusid);
            if (!string.IsNullOrEmpty(sectionid))
                aIntSectionID = Convert.ToInt32(sectionid);
            if (!string.IsNullOrEmpty(DepID))
                aIntDepartmentID = Convert.ToInt32(DepID);
            if (!string.IsNullOrEmpty(companyId))
                aCompanyId = Convert.ToInt32(companyId);

            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();

            List<EmploymentInformation> employeeHirDatelist = objEmployeeDB.GetEmployeeHireDateInformation();

            if (objUserContextViewModel.UserId != 0)
            {
                UserId = objUserContextViewModel.UserId;
            }

            //-------------------------------------------------------------Daily attendance based on user logged in

            UserRoleDB userRoleDB = new UserRoleDB();
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            //-------------------------------------------------------------

            objDailyAttendanceViewModelList = await objDailyAttendanceViewDB.GetDailyAttendanceViewListAsync(FromDate, ToDate, employeeId, statusId, Proceed, active,
                aIntDepartmentID, aIntSectionID, aIntShiftID, UserId, aCompanyId);

            objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.HRID != 0).ToList();

            //---------Sort Data by UserGroupAcessList permission------------

            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);

            objDailyAttendanceViewModelList = (from attEmp in objDailyAttendanceViewModelList
                                               join UGA in objEmployeeList on attEmp.HRID equals UGA.EmployeeId
                                               select attEmp).ToList();

            //----------------------------------------------------------------

            Session["FromDate"] = FromDate;
            Session["ToDate"] = ToDate;
            Session["DailyAttendanceList"] = objDailyAttendanceViewModelList;

            #region Sorting
            //------------------------------------------------------------------
            //  DepID = "1";
            if (!string.IsNullOrEmpty(DepID))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.DeptID == Convert.ToInt32(DepID)).ToList();
            }
            if (!string.IsNullOrEmpty(empid))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.HRID == Convert.ToInt32(empid)).ToList();
            }

            switch (iSortCol_0)
            {
                case 0:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.HRAltID).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.HRAltID).ToList();
                    }
                    break;

                case 1:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.EmployeeName).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.EmployeeName).ToList();
                    }
                    break;

                case 2:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.DepName).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.DepName).ToList();
                    }
                    break;
                case 3:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.AttDate).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.AttDate).ToList();
                    }
                    break;
                case 4:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.InTime).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.InTime).ToList();
                    }
                    break;

                case 5:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.OutTime).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.OutTime).ToList();
                    }
                    break;
                case 6:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.Status).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.Status).ToList();
                    }
                    break;
                //case 6:
                //    if (sSortDir_0 == "asc")
                //    {
                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.HrHireDate).ToList();
                //    }
                //    else
                //    {

                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.HrHireDate).ToList();
                //    }
                //    break;
                //case 7:
                //    if (sSortDir_0 == "asc")
                //    {
                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.HrInductionDate).ToList();
                //    }
                //    else
                //    {

                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.HrInductionDate).ToList();
                //    }
                //    break;
                case 7:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.EarlyMinutes).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.EarlyMinutes).ToList();
                    }
                    break;
                case 8:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.LateMinutes).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.LateMinutes).ToList();
                    }
                    break;


                default:

                    objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.EmployeeName).ToList();
                    break;

                    //              { "mData": "EmpoyeeName", "sTitle": "Name" },

                    // { "mData": "Status", "sTitle": "Status" },


                    //{ "mData": "DepName", "sTitle": "Department Name" },
                    //  { "mData": "AttDate", "sTitle": "Date" },
                    //{ "mData": "InTime", "sTitle": "In Time" },
                    //{ "mData": "OutTime", "sTitle": "Out Time" },
                    // { "mData": "HrHireDate", "sTitle": "Hire Date" },
                    //{ "mData": "HrInductionDate", "sTitle": "Induction Date" },

                    ////{ "mData": "HolidayName", "sTitle": "Holiday Name" },
                    ////{ "mData": "HolidayDate", "sTitle": "Holiday Date" },
                    //{ "mData": "LateMinute", "sTitle": "Late Minute" },
                    //{ "mData": "earlyMinute", "sTitle": "Early Minute" },
            }
            #endregion

            string[] a = new string[3];
            string[] s = new string[14];
            if (objDailyAttendanceViewModelList.Count > 0)
            {
                for (int i = 0; i <= 12; i++)
                {
                    s[i] = objDailyAttendanceViewModelList.Where(x => x.StatusID == i).Count().ToString();
                }
                a[0] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 2 && x.Proceed == false).Count().ToString();
                a[1] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 3 && x.Proceed == false).Count().ToString();
                a[2] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 4 && x.Proceed == false).Count().ToString();
            }
            else
            {
                a[0] = "0";
                a[1] = "0";
                a[2] = "0";
            }

            s[13] = (objDailyAttendanceViewModelList.Where(x => x.StatusID == 1).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 3).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 4).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 8).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 9).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 11).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 12).Count()).ToString();
            Session["RecordsNotPosted"] = a;
            Session["test"] = s;

            // statusid = "7";
            if (!string.IsNullOrEmpty(statusid))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.StatusID == Convert.ToInt32(statusid)).ToList();
            }
            if (!string.IsNullOrEmpty(sectionid))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.SectionID == Convert.ToInt32(sectionid)).ToList();
            }
            int Count = objDailyAttendanceViewModelList.Count;
            int pageindex = pageNumber + 1;
            //objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Skip((pageindex - 1) * numberOfRecords).Take(numberOfRecords).ToList();
            string checkBoxItem = "<input type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'ChkcheckHRAltID' name = 'checkHRAltID'/>";

            // checkBoxItem = "<input type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'ChkcheckHRAltID' name = 'checkHRAltID' disabled />";

            var vList = new object();

            vList = new
            {
                aaData = (from item in objDailyAttendanceViewModelList
                          select new
                          {
                              //Action = "<a class='btn btn-primary' onclick='EditDailyAttendanceView(" + item.DailyAttendanceViewID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> Edit</a><a class='btn btn-primary' onclick='DeleteDailyAttendanceView(" + item.DailyAttendanceViewID.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
                              //  CheckRow = "<input type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'Chkcheck" + item.HRAltID + "' name = 'check" + item.HRAltID + "'/>",
                              CheckRow = item.Proceed == true ? " <input type = 'checkbox' onclick = 'ChkRowCheckClick(this)' class='ChkRowCheck' id = 'Chkcheck" + item.HRAltID + "' name = 'check" + item.HRAltID + "' disabled />" : checkBoxItem.Replace("HRAltID", item.HRAltID),
                              HRAltID = item.HRAltID,
                              AttDate = item.AttDate,
                              EmpoyeeName = item.EmployeeName,
                              HrHireDate = item.HrHireDate,
                              HrInductionDate = item.HrInductionDate,
                              Status = item.Status,
                              StatusID = item.StatusID,
                              DeptID = item.DeptID,
                              DepName = item.DepName,
                              InTime = item.InTime,
                              OutTime = item.OutTime,
                              //HolidayName = item.HolidayName,
                              //HolidayDate = item.HolidayDate,
                              LateMinutes = item.LateMinutes,
                              EarlyMinutes = item.EarlyMinutes,

                              PayEmployeeAbsentID = item.PayEmployeeAbsentID,
                              PayEmployeeLateID = item.PayEmployeeLateID,
                              absentExcused = item.absentExcused,
                              LateExcused = item.LateExcused,
                              earlyExcused = item.earlyExcused,
                              Proceed = item.Proceed,
                              IsActive = item.IsActive,
                              HRID = item.HRID,
                              SectionName = item.SectionName,
                              SectionID = item.SectionID,
                              totalHrs = item.totalHrs,
                              shiftHrs = item.shiftHrs,
                              overHrs = item.overHrs,
                              shortHrs = item.shortHrs,
                              IsHireDateIsGreater = objEmployeeDB.checkHireDateIsGreater(DateTime.ParseExact(item.AttDate, HRMSDateFormat, CultureInfo.InvariantCulture), employeeHirDatelist.FirstOrDefault(x => x.EmployeeID == item.HRID).HireDate)
                          }).ToArray(),
                recordsTotal = Count,
                recordsFiltered = Count
            };

            //  return Json(vList, JsonRequestBehavior.AllowGet);

            var serializer = new JavaScriptSerializer();

            // For simplicity just use Int32's max value.
            // You could always read the value from the config section mentioned above.
            serializer.MaxJsonLength = Int32.MaxValue;

            var resultData = new { Value = "foo", Text = "var" };
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;

        }


    }
}