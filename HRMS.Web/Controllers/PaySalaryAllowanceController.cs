﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;


namespace HRMS.Web.Controllers
{
    public class PaySalaryAllowanceController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPaySalaryAllowanceList()
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.PaySalaryAllowance> objPaySalaryAllowanceList = new List<HRMS.Entities.PaySalaryAllowance>();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            objPaySalaryAllowanceList = objAdditionPaidCycleDB.GetAllPaySalaryAllowance();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objPaySalaryAllowanceList
                          select new
                          {
                              Actions = getActionLink(item.PaySalaryAllowanceID, item.IsLockedUpdate, item.IsLockedDelete),
                              PaySalaryAllowanceID = item.PaySalaryAllowanceID,
                              PaySalaryAllowanceName_1 = item.PaySalaryAllowanceName_1,
                              PaySalaryAllowanceName_2 = item.PaySalaryAllowanceName_2,
                              PaySalaryAllowanceName_3 = item.PaySalaryAllowanceName_3,
                              DateFrom = item.TransactionDate != null ? item.TransactionDate : "",

                              PaySalaryAllowanceShortName_1 = item.PaySalaryAllowanceShortName_1,
                              PaySalaryAllowanceShortName_2 = item.PaySalaryAllowanceShortName_2,
                              PaySalaryAllowanceShortName_3 = item.PaySalaryAllowanceShortName_3,

                              AccountCode = item.AccountCode,
                              IsAddition = item.IsAddition,
                              IsIncludeInDeduction = item.IsIncludeInDeduction == true ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.IsIncludeInDeduction + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.IsIncludeInDeduction + "'/>",
                              // IsIncludeInDeduction = item.IsIncludeInDeduction,
                              Sequence = item.Sequence,
                              IsVacactionDeductible = item.IsVacactionDeductible,
                              IsExcludeInJV = item.IsExcludeInJV,

                          }).ToArray()
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            HRMS.Entities.PaySalaryAllowance objPaySalaryAllowance = new HRMS.Entities.PaySalaryAllowance();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "All", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            if (id > -1)
            {
                objPaySalaryAllowance = objAdditionPaidCycleDB.GetPaySalaryAllowance(id);
            }
            else
            {
                objPaySalaryAllowance.PaySalaryAllowanceID = id;
            }
            ViewBag.isAddition = false;
            return View(objPaySalaryAllowance);
        }

        public ActionResult EditAdditioinAllowance(int id)
        {
            HRMS.Entities.PaySalaryAllowance objPaySalaryAllowance = new HRMS.Entities.PaySalaryAllowance();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "All", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            if (id > -1)
            {
                objPaySalaryAllowance = objAdditionPaidCycleDB.GetPaySalaryAllowance(id);
            }
            else
            {
                objPaySalaryAllowance.PaySalaryAllowanceID = id;
            }
            ViewBag.isAddition = true;
            return View("Edit", objPaySalaryAllowance);
        }

        public JsonResult Save(PaySalaryAllowance objPaySalaryAllowance, bool IsAdditionReq)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                if (IsAdditionReq)
                {
                    objPaySalaryAllowance.IsAddition = true;
                }
                AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();

                if (objPaySalaryAllowance.PaySalaryAllowanceID == -1)
                {
                    objAdditionPaidCycleDB.AddUpdateDeletePaySalaryAllowance(objPaySalaryAllowance, "insert");

                }
                else
                {
                    objAdditionPaidCycleDB.AddUpdateDeletePaySalaryAllowance(objPaySalaryAllowance, "update");
                }

                //return Redirect("Index");
                if (objPaySalaryAllowance.PaySalaryAllowanceID == -1)
                {
                    result = "success";
                    resultMessage = "Pay Salary Allowance Added Successfully.";
                }
                else
                {
                    result = "success";
                    resultMessage = "Pay Salary Allowance Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while adding Pay Salary Allowance.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            HRMS.Entities.PaySalaryAllowance objPaySalaryAllowance = new HRMS.Entities.PaySalaryAllowance();
            objPaySalaryAllowance.PaySalaryAllowanceID = int.Parse(id);
            if (objAdditionPaidCycleDB.CheckPayAllowanceAllocation(objPaySalaryAllowance.PaySalaryAllowanceID))
            {
                objAdditionPaidCycleDB.AddUpdateDeletePaySalaryAllowance(objPaySalaryAllowance, "delete");
                return Json(new { result = "success", resultMessage = "Pay Salary Allowance Deleted Successfully." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "warning", resultMessage = "You are not able to delete this allowance as its already assign to employees." }, JsonRequestBehavior.AllowGet);
            }
            //return Redirect("Index");
            //return Json(0, JsonRequestBehavior.AllowGet);

        }

        //public ActionResult ExportToExcel()
        //{
        //    byte[] content;
        //    string fileName = "PaySalaryAllowance" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    //ProfileDB profileDB = new ProfileDB();

        //    int pageNumber = 0;
        //    int numberOfRecords = 0;
        //    string sortColumn = "PaySalaryAllowanceID";
        //    string sortOrder = "DESC";
        //    int PaySalaryAllowanceID = objUserContextViewModel.CompanyId;
        //    int totalCount = 0;
        //    List<HRMS.Entities.PaySalaryAllowance> objPaySalaryAllowanceList = new List<HRMS.Entities.PaySalaryAllowance>();
        //    AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
        //    objPaySalaryAllowanceList = objAdditionPaidCycleDB.GetAllPaySalaryAllowance(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount, "");
        //    var report = (from item in objPaySalaryAllowanceList.AsEnumerable()
        //                  select new
        //                  {
        //                      //Id = item.Id,                              
        //                      PaySalaryAllowanceID = item.PaySalaryAllowanceID,
        //                      PaySalaryAllowanceName_1 = item.PaySalaryAllowanceName_1,
        //                      PaySalaryAllowanceName_2 = item.PaySalaryAllowanceName_2,
        //                      PaySalaryAllowanceName_3 = item.PaySalaryAllowanceName_3,
        //                      DateFrom = item.TransactionDate,

        //                      PaySalaryAllowanceShortName_1 = item.PaySalaryAllowanceShortName_1,
        //                      PaySalaryAllowanceShortName_2 = item.PaySalaryAllowanceShortName_2,
        //                      PaySalaryAllowanceShortName_3 = item.PaySalaryAllowanceShortName_3,

        //                      AccountCode = item.AccountCode,
        //                      IsAddition = item.IsAddition,
        //                      IsIncludeInDeduction = item.IsIncludeInDeduction,
        //                      Sequence = item.Sequence,
        //                      IsVacactionDeductible = item.IsVacactionDeductible,
        //                      IsExcludeInJV = item.IsExcludeInJV,
        //                  }).ToList();
        //    content = ExportListToExcel(report);
        //    return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        //}

        //public void ExportToPdf()
        //{
        //    Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

        //    string fileName = "PaySalaryAllowance" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    //ProfileDB profileDB = new ProfileDB();

        //    int pageNumber = 0;
        //    int numberOfRecords = 0;
        //    string sortColumn = "PaySalaryAllowanceID";
        //    string sortOrder = "Desc";
        //    int PaySalaryAllowanceID = objUserContextViewModel.CompanyId;
        //    int totalCount = 0;
        //    List<HRMS.Entities.PaySalaryAllowance> objPaySalaryAllowanceList = new List<HRMS.Entities.PaySalaryAllowance>();
        //    AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
        //    objPaySalaryAllowanceList = objAdditionPaidCycleDB.GetAllPaySalaryAllowance(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount, "");
        //    var report = (from item in objPaySalaryAllowanceList.AsEnumerable()
        //                  select new
        //                  {
        //                      PaySalaryAllowanceID = item.PaySalaryAllowanceID,
        //                      PaySalaryAllowanceName_1 = item.PaySalaryAllowanceName_1,
        //                      PaySalaryAllowanceName_2 = item.PaySalaryAllowanceName_2,
        //                      PaySalaryAllowanceName_3 = item.PaySalaryAllowanceName_3,
        //                      DateFrom = item.TransactionDate,

        //                      PaySalaryAllowanceShortName_1 = item.PaySalaryAllowanceShortName_1,
        //                      PaySalaryAllowanceShortName_2 = item.PaySalaryAllowanceShortName_2,
        //                      PaySalaryAllowanceShortName_3 = item.PaySalaryAllowanceShortName_3,

        //                      AccountCode = item.AccountCode,
        //                      IsAddition = item.IsAddition,
        //                      IsIncludeInDeduction = item.IsIncludeInDeduction,
        //                      Sequence = item.Sequence,
        //                      IsVacactionDeductible = item.IsVacactionDeductible,
        //                      IsExcludeInJV = item.IsExcludeInJV,
        //                  }).ToList();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    GridView gridvw = new GridView();
        //    gridvw.DataSource = report; //bind the data table to the grid view
        //    gridvw.DataBind();
        //    StringWriter swr = new StringWriter();
        //    HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        //    gridvw.RenderControl(tw);
        //    StringReader sr = new StringReader(swr.ToString());
        //    Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    pdfDoc.Open();
        //    htmlparser.Parse(sr);
        //    pdfDoc.Close();
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}


        public ActionResult ExportToExcel()
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "PaySalaryAllowance" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            System.Data.DataSet ds = objAdditionPaidCycleDB.GetAllPaySalaryAllowanceDatasSet();
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            string fileName = "PaySalaryAllowance" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            System.Data.DataSet ds = objAdditionPaidCycleDB.GetAllPaySalaryAllowanceDatasSet();


            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();

        }

        public JsonResult loadPaySalaryAllowances()
        {
            PaySalaryDB objPaySalaryDB = new PaySalaryDB();
            var salaryAllowances = objPaySalaryDB.GetAllPaySalaryAllowanceList(0);
            var salaryAllowancesList = (from p in salaryAllowances.OrderBy(c => c.PaySalaryAllowanceID)
                                        select new
                                        {
                                            paySalaryAllowancesID = p.PaySalaryAllowanceID,
                                            paySalaryAllowancesName = p.PaySalaryAllowanceName_1,
                                        });
            return Json(salaryAllowancesList.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult loadPaySalaryAdditions()
        {
            PaySalaryDB objPaySalaryDB = new PaySalaryDB();
            var salaryAllowances = objPaySalaryDB.GetAllPaySalaryAllowanceList(1);
            var salaryAllowancesList = (from p in salaryAllowances.OrderBy(c => c.PaySalaryAllowanceID)
                                        select new
                                        {
                                            paySalaryAllowancesID = p.PaySalaryAllowanceID,
                                            paySalaryAllowancesName = p.PaySalaryAllowanceName_1,
                                        });
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Addition Type", Value = "0" });
            foreach (var m in salaryAllowancesList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.paySalaryAllowancesName, Value = m.paySalaryAllowancesID.ToString() });
            }

            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckSalaryAllowance(string salaryAllowanceName)
        {

            AdditionPaidCycleDB additionPaidCycleDB = new AdditionPaidCycleDB();
            int salaryAllowancesCount = 0;
            salaryAllowancesCount = additionPaidCycleDB.GetAllPaySalaryAllowanceList(true, false).Where(x => x.PaySalaryAllowanceName_1.Trim().ToLower() == salaryAllowanceName.ToLower()).Count();
            salaryAllowancesCount = salaryAllowancesCount + additionPaidCycleDB.GetAllPaySalaryAllowanceList(true, true).Where(x => x.PaySalaryAllowanceName_1.Trim().ToLower() == salaryAllowanceName.ToLower()).Count();

            if (salaryAllowancesCount > 0)
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }

        public string getActionLink(int PaySalaryAllowanceID, bool IsUpdateLock, bool IsDeleteLock)
        {
            string ActionLink = string.Empty;
            if (PaySalaryAllowanceID > 0)
            {
                if (!IsUpdateLock)
                {
                    ActionLink = ActionLink + "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + PaySalaryAllowanceID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>";
                }
                if (!IsDeleteLock)
                {
                    ActionLink = ActionLink + "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + PaySalaryAllowanceID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>";
                }
            }
            else
            {
                ActionLink = "";
            }
            return ActionLink;
        }
    }
}