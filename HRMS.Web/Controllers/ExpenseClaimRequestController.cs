﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using HRMS.Entities.Forms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess.FormsDB;

namespace HRMS.Web.Controllers
{
    public class ExpenseClaimRequestController : FormsController
    {
        // GET: ExpenseClaimRequest
        ExpenseClaimRequestDB objExpenseClaimRequestDB;
        public ExpenseClaimRequestController()
        {
            XMLLogFile = "LoggerExpenseClaimRequest.xml";
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
        }
        public ActionResult Index()
        {
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            ExpenseClaimRequestDB objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            ViewBag.ClaimTypeList = new SelectList(objExpenseClaimRequestDB.GetExpenseClaimTypeList(), "id", "text");
            ViewBag.CurrencyList = new SelectList(objExpenseClaimRequestDB.GetCurrencyForAmountList(), "id", "text");
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.IsReadOnlyView = false;

            ViewBag.FormId = 9;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId.ToString();
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
            objExpenseClaimRequestModel.CreatedOn = DateTime.Today;
            objExpenseClaimRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            objExpenseClaimRequestModel.IsAddMode = true;
            return View(objExpenseClaimRequestModel);
        }
        public ActionResult Edit()
        {
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            int formProcessID = GetFormProcessSessionID();
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            //get current/pending approver group and employee
            RequestFormsApproveModel requestFormsApproveModel = objExpenseClaimRequestDB.GetPendingFormsApproval(formProcessID);
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.FormId = 9;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId.ToString();
            RequestFormsProcessModel objRequestFormsProcessModel = objExpenseClaimRequestDB.GetRequestFormsProcess(formProcessID);
            objExpenseClaimRequestModel = objExpenseClaimRequestDB.GetExpenseClaimRequest(objRequestFormsProcessModel.FormInstanceID, formProcessID);
            //if login user is the present approver then can see form in edit mode 
            if ((objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID || objUserContextViewModel.UserId == 0)
                ||(objUserContextViewModel.UserId== objExpenseClaimRequestModel.RequesterEmployeeID && objExpenseClaimRequestModel.ReqStatusID == (int)RequestStatus.Rejected))
            {
                ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
                int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
                ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
                //If form is pending then only show for edit
                if (objExpenseClaimRequestModel.ReqStatusID == (int)RequestStatus.Pending || objExpenseClaimRequestModel.ReqStatusID == (int)RequestStatus.Rejected)
                {
                    ViewBag.ClaimTypeList = new SelectList(objExpenseClaimRequestDB.GetExpenseClaimTypeList(), "id", "text");
                    ViewBag.CurrencyList = new SelectList(objExpenseClaimRequestDB.GetCurrencyForAmountList(), "id", "text");
                    if (objExpenseClaimRequestModel.ReqStatusID == (int)RequestStatus.Rejected)
                    {
                        ViewBag.IsReadOnlyView = false;
                    }
                    else
                    {
                        ViewBag.IsReadOnlyView = true;
                    }
                    objExpenseClaimRequestModel.FormProcessID = formProcessID;
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
            {
                return RedirectToAction("ViewDetails");
            }
            return View(objExpenseClaimRequestModel);
        }
        public ActionResult UpdateDetails()
        {
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            int formProcessID = GetFormProcessSessionID();
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            RequestFormsProcessModel objRequestFormsProcessModel = objExpenseClaimRequestDB.GetRequestFormsProcess(formProcessID);
            objExpenseClaimRequestModel = objExpenseClaimRequestDB.GetExpenseClaimRequest(objRequestFormsProcessModel.FormInstanceID, formProcessID);
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 9;

            if (formProcessID > 0)
            {
                if (objExpenseClaimRequestModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.ClaimTypeList = new SelectList(objExpenseClaimRequestDB.GetExpenseClaimTypeList(), "id", "text");
                    ViewBag.CurrencyList = new SelectList(objExpenseClaimRequestDB.GetCurrencyForAmountList(), "id", "text");
                    int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
                    ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
                    ViewBag.IsReadOnlyView = false;
                    ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
                    ViewBag.FormProcessID = formProcessID;
                    objExpenseClaimRequestModel.FormProcessID = formProcessID;
                    return View("UpdateDetails", objExpenseClaimRequestModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Index");
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            ViewBag.ClaimTypeList = new SelectList(objExpenseClaimRequestDB.GetExpenseClaimTypeList(), "id", "text");
            ViewBag.CurrencyList = new SelectList(objExpenseClaimRequestDB.GetCurrencyForAmountList(), "id", "text");
            ViewBag.IsReadOnlyView = true;
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
            RequestFormsProcessModel objRequestFormsProcessModel = objExpenseClaimRequestDB.GetRequestFormsProcess(formProcessID);
            ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
            objExpenseClaimRequestModel = objExpenseClaimRequestDB.GetExpenseClaimRequest(objRequestFormsProcessModel.FormInstanceID, formProcessID);
            objExpenseClaimRequestModel.FormProcessID = formProcessID;
            return View(objExpenseClaimRequestModel);
        }
        public JsonResult GetExpenseClaimTypes()
        {
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            return Json(objExpenseClaimRequestDB.GetExpenseClaimTypeList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCurrency()
        {
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            return Json(objExpenseClaimRequestDB.GetCurrencyForAmountList(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveExpenseClaimRequest(string expenseClaimRequestDetailList, string Comments, HttpPostedFileBase[] FilesArray)
        {
            List<ExpenseClaimRequestDetailModel> objExpenseClaimRequestDetailList = JsonConvert.DeserializeObject<List<ExpenseClaimRequestDetailModel>>(expenseClaimRequestDetailList);
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            objExpenseClaimRequestModel.Comments = Comments;
            objExpenseClaimRequestModel.ExpenseClaimRequestDetailList = objExpenseClaimRequestDetailList;

            OperationDetails operationDetails = new OperationDetails();
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
            AllFormsFilesModel ObjFile;
            if (FilesArray != null)
            {
                string fileName = string.Empty;
                foreach (var item in FilesArray)
                {
                    string extension = "." + item.FileName.Substring(item.FileName.LastIndexOf('.') + 1);
                    string RowID = item.FileName.Replace(extension, "");
                    ObjFile = new AllFormsFilesModel();
                    ObjFile.FileName = objExpenseClaimRequestDetailList.Where(r => r.RowID == Convert.ToInt32(RowID)).Select(s => s.FileName).FirstOrDefault();
                    ObjFile.FileContentType = item.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(item);

                    ObjFile.FormFileIDName = RowID;
                    int ExpClaimFileID = objExpenseClaimRequestDetailList.Where(r => r.RowID == Convert.ToInt32(RowID)).Select(s => s.ExpClaimFileID).FirstOrDefault();
                    if (ExpClaimFileID > 0)
                    {
                        ObjFile.FileID = ExpClaimFileID;
                    }
                    uploadFileList.Add(ObjFile);
                }
            }
            objExpenseClaimRequestModel.AllFormsFilesModelList = uploadFileList;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objExpenseClaimRequestModel.CompanyID = objUserContextViewModel.CompanyId;
            RequestFormsProcessModel objRequestFormsProcessModel = objExpenseClaimRequestDB.SaveExpenseClaimRequest(objExpenseClaimRequestModel, objUserContextViewModel.UserId);
            if (objRequestFormsProcessModel != null)
            {
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                requestFormsApproverEmailModelList = objExpenseClaimRequestDB.GetApproverEmailList(objRequestFormsProcessModel.FormProcessID.ToString(), objUserContextViewModel.UserId);
                SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);

                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateExpenseClaimRequest(string expenseClaimRequestDetailList, int FormProcessID, int ID, string Comments, string DeletedClaimDetailIDs, string DeletedClaimFileIDs, HttpPostedFileBase[] FilesArray)
        {
            List<ExpenseClaimRequestDetailModel> objExpenseClaimRequestDetailList = JsonConvert.DeserializeObject<List<ExpenseClaimRequestDetailModel>>(expenseClaimRequestDetailList);
            ExpenseClaimRequestModel objExpenseClaimRequestModel = new ExpenseClaimRequestModel();
            objExpenseClaimRequestModel.ID = ID;
            objExpenseClaimRequestModel.FormProcessID = FormProcessID;
            objExpenseClaimRequestModel.Comments = Comments;
            objExpenseClaimRequestModel.ExpenseClaimRequestDetailList = objExpenseClaimRequestDetailList;
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            OperationDetails operationDetails = new OperationDetails();
            objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
            AllFormsFilesModel ObjFile;
            if (FilesArray != null)
            {
                string fileName = string.Empty;
                foreach (var item in FilesArray)
                {
                    string extension = "." + item.FileName.Substring(item.FileName.LastIndexOf('.') + 1);
                    string RowID = item.FileName.Replace(extension, "");
                    ObjFile = new AllFormsFilesModel();
                    ObjFile.FileName = objExpenseClaimRequestDetailList.Where(r => r.RowID == Convert.ToInt32(RowID)).Select(s => s.FileName).FirstOrDefault();
                    ObjFile.FileContentType = item.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(item);

                    ObjFile.FormFileIDName = RowID;
                    int ExpClaimFileID = objExpenseClaimRequestDetailList.Where(r => r.RowID == Convert.ToInt32(RowID)).Select(s => s.ExpClaimFileID).FirstOrDefault();
                    if (ExpClaimFileID > 0)
                    {
                        ObjFile.FileID = ExpClaimFileID;
                    }
                    uploadFileList.Add(ObjFile);
                }
            }

            objExpenseClaimRequestModel.AllFormsFilesModelList = uploadFileList;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objExpenseClaimRequestModel.CompanyID = objUserContextViewModel.CompanyId;
            if (DeletedClaimFileIDs.Length > 0)
            {
                foreach (var fileId in DeletedClaimFileIDs.Split(','))
                {
                    new AllFormsFilesDB().DeleteAllFormsFile(Convert.ToInt32(fileId), objExpenseClaimRequestModel.FormProcessID, fileId, objUserContextViewModel.UserId);
                }
            }
            operationDetails = objExpenseClaimRequestDB.UpdateExpenseClaimRequest(objExpenseClaimRequestModel, DeletedClaimDetailIDs, objUserContextViewModel.UserId);
            if (operationDetails.Success)
            {
                if (!isEditRequestFromAllRequests)
                {
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = objExpenseClaimRequestDB.GetApproverEmailList(objExpenseClaimRequestModel.FormProcessID.ToString(), objUserContextViewModel.UserId);
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                }
               
                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";                  
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }                   
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {                       
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }

                        objExpenseClaimRequestDB = new ExpenseClaimRequestDB();
                        int output = objExpenseClaimRequestDB.PushFinalExpenseClaimPayAddition(formProcessID);
                    }

                }              
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }               
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}