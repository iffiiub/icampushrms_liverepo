﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class PendingJoiningRequestController : BaseController
    {
        // GET: PendingJoiningRequest
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ViewBag.Employee = new SelectList(new PendingJoiningRequestDB().GetAllPendingJoiningListEmployee(objUserContextViewModel.UserId), "EmployeeId", "FullName");
            List<HRMS.Entities.Employee> employees = new List<HRMS.Entities.Employee>();
            ViewBag.Employee = new SelectList(employees, "EmployeeId", "FullName");
            ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", "UserTypeName");
            ViewBag.Company = GetCompanySelectList();
            return View();
        }
        public ActionResult GetPendingJoiningRequest(int? EmployeeId, int? CategoryId, int? companyId)
        {
            List<PendingJoiningRequestModel> objPendingModelList = new List<PendingJoiningRequestModel>();
            PendingJoiningRequestDB objJoining = new PendingJoiningRequestDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            var UserId = objUserContextViewModel.UserId;
            objPendingModelList = objJoining.GetAllPendingRequest(EmployeeId, CategoryId, companyId, UserId);

            var vList = new object();
            vList = new
            {
                aaData = (from item in objPendingModelList
                          select new
                          {
                              EmployeeId = item.EmployeeId,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              DepartmentId = item.DepartmentId,
                              DepartmentName = item.DepartmentName,
                              CategoryId = item.UserTypeId,
                              CategoryName = item.UserTypeName,
                              Designation = item.Designation,
                              Organization = item.Organization,
                              CompanyId = item.CompanyID,
                              Actions = "<a style='text-decoration:none' class='cursorPointer' onclick='ViewPendingJoiningRequestForm(" + item.EmployeeId.ToString() + ",true)'>Joining Request</a>",
                              JoiningDate = item.JoiningDate,
                              AutoId = item.autoID

                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public ActionResult SetCurrentSessionPendingJoiningRequestForm(int EmployeeID)
        {
            Session["EmployeePendingRequestID"] = EmployeeID;
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public override ActionResult GetEmployees(int companyId)
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();

            List<HRMS.Entities.Employee> employees = new List<HRMS.Entities.Employee>();
            if (companyId > 0)
                employees = new PendingJoiningRequestDB().GetAllPendingJoiningListEmployee(objUserContextViewModel.UserId, companyId);
            //employees = objEmployeeDB.GetAllEmployeeByCompanyAndUser(CompanyID: companyId, UserID: objUserContextViewModel.UserId);
            var result = employees.Select(d => new { id = d.EmployeeId, name = d.FullName }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}