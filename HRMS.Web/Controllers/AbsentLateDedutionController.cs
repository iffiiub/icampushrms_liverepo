﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class AbsentLateDeductionController : BaseController
    {
        #region Absent Deduction
        public ActionResult Index()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Month", Value = "0" });
            selectListItems.Add(new SelectListItem { Text = "January (1)", Value = "1" });
            selectListItems.Add(new SelectListItem { Text = "February (2)", Value = "2" });
            selectListItems.Add(new SelectListItem { Text = "March (3)", Value = "3" });
            selectListItems.Add(new SelectListItem { Text = "April (4)", Value = "4" });
            selectListItems.Add(new SelectListItem { Text = "May (5)", Value = "5" });
            selectListItems.Add(new SelectListItem { Text = "June (6)", Value = "6" });
            selectListItems.Add(new SelectListItem { Text = "July (7)", Value = "7" });
            selectListItems.Add(new SelectListItem { Text = "August (8)", Value = "8" });
            selectListItems.Add(new SelectListItem { Text = "September (9)", Value = "9" });
            selectListItems.Add(new SelectListItem { Text = "October (10)", Value = "10" });
            selectListItems.Add(new SelectListItem { Text = "November (11)", Value = "11" });
            selectListItems.Add(new SelectListItem { Text = "December (12)", Value = "12" });
            foreach (var item in selectListItems)
            {
                if (item.Value == DateTime.Now.Month.ToString())
                    item.Selected = true;
            }
            ViewBag.MonthList = selectListItems;//.Where(x=>x.Value == DateTime.Now.Month.ToString()).ToList().ForEach(i=>i.Selected = true);
            bool isPostDeductionrecordEnable = new AbsentDeductionDB().GetDeductionSetting().PostAbsentRecords;
            ViewBag.isPostDeductionrecordEnable = isPostDeductionrecordEnable;
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Year", Value = "0" });
            foreach (var items in objAcademicYearDB.GetAcademicYears())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.Year.ToString(),
                    Value = items.Year.ToString()
                };
                selectListItems.Add(selectListItem);
            }
            foreach (var item in selectListItems)
            {
                if (item.Value == DateTime.Now.Year.ToString())
                    item.Selected = true;
            }
            ViewBag.YearList = selectListItems;//.Where(x => x.Value == DateTime.Now.Year.ToString()).ToList().FirstOrDefault().Selected = true;

            PayCycleDB objPayCycleDB = new PayCycleDB();
            selectListItems = new List<SelectListItem>();

            foreach (var items in objPayCycleDB.GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                    Selected = items.active

                };
                selectListItems.Add(selectListItem);
            }
            ViewBag.CycleList = selectListItems;


            if (Session["Month"] != null && Session["Year"] != null)
            {

                ViewBag.hdnMonth = Session["Month"];
                ViewBag.hdnYear = Session["Year"];
            }
            int FilterCondition = 0;
            AbsentDeductionDB objAbsentDeductionDB = new AbsentDeductionDB();
            FilterCondition = objAbsentDeductionDB.GetFilterCondition();
            ViewBag.FilterCondition = FilterCondition;
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();
            AcademicYearModel objYearModel = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault();

            bool isAcYearWarningEnable = false;
            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 3)
            {
                isAcYearWarningEnable = true;
            }
            ViewBag.ShowAcYearWarning = isAcYearWarningEnable;
            bool IsShowCycleWarning = false;
            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 1)
            {
                IsShowCycleWarning = true;
            }
            ViewBag.ShowCycleWarning = IsShowCycleWarning;
            return View();
        }

        public ActionResult SetEmployeeLeaveTypeStatus(int PayAbsentTypeID, int PayEmployeeAbsentID)
        {

            AbsentLateDeductionDB objAbsentLateDeductionDB = new AbsentLateDeductionDB();

            var result = objAbsentLateDeductionDB.SetEmployeeLeaveTypeStatusData(PayAbsentTypeID, PayEmployeeAbsentID);
            return Json(new { Result = "result" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAbsentForCurrentMonth(string FromDate, string ToDate, bool ShowGeneratedOnly, string Month = "", string Year = "")
        {
            DataAccess.GeneralDB.CommonDB common = new DataAccess.GeneralDB.CommonDB();
            DBHelper objDBHelper = new DBHelper();
            Session["Month"] = Month;
            Session["Year"] = Year;
            if (Year.Contains("-"))
            {
                string[] words = Year.Split('-');
                Year = words[0];
            }
            else
            {
                Year = DateTime.Now.Year.ToString();
            }

            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();

            AbsentDeductionDB objAbsentDeductionDB = new AbsentDeductionDB();
            List<AbsentsForCurrentMonthModel> absentList = new List<AbsentsForCurrentMonthModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
          
            int? UserId = null;

            UserId = objUserContextViewModel.UserId;

            absentList = objAbsentDeductionDB.GetAbsentForCurrentMonth(Month, Year, FromDate, ToDate, ShowGeneratedOnly, UserId);
            var vList = new object();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            List<AbsentTypeModel> objabsenttypeList = new List<AbsentTypeModel>();
            objabsenttypeList = objAbsentTypeDB.GetAbsentTypeList();
            string values = "";
            if (objabsenttypeList.Count > 0)
            {
                foreach (var obj in objabsenttypeList)
                {
                    values += "<option value='" + obj.PayAbsentTypeID + "'>" + obj.PayAbsentTypeName_1 + "</option>";
                }
            }
            vList = new
            {
                aaData = (from item in absentList  //.Where(x => x.IsConfirmed == false)
                          select new
                          {

                              Checkboxes = (item.CycleID == 0 || item.IsConfirmed == true) ? "<input type = checkbox class='Deductablecheckbox' id='" + item.PayEmployeeAbsentID + "' disabled/>" : "<input type=checkbox class='Deductablecheckbox' id='" + item.PayEmployeeAbsentID + "'/>",
                              AbsentTypeName = item.IsConfirmed==true? "<select onchange=\"ChangeAbsentType(this," + item.PayEmployeeAbsentID + ",'Deduction')\" class='form-control selectpickerddl ddlAbsentTypeName' id='ddlabsentCurrent_" + item.PayAbsentTypeID + "' disabled> " + values + " </select>" : "<select onchange=\"ChangeAbsentType(this," + item.PayEmployeeAbsentID + ",'Deduction')\" class='form-control selectpickerddl ddlAbsentTypeName' id='ddlabsentCurrent_" + item.PayAbsentTypeID + "' > " + values + " </select>",
                              Days = "<label id='lblDays' style='font-weight:normal'>" + item.Days + "</label>",
                              FromDate = "<label data-FromDate='" + item.FromDate.Date.ToShortDateString() + "' id='lblFromDate' style='font-weight:normal'>"+(item.Days>=4? DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.FromDate.Date.ToShortDateString())+" - "+ DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.ToDate.Date.ToShortDateString()) : DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.FromDate.Date.ToShortDateString()) )+ "</label>",
                              ToDate = "<label data-Todate='" + item.ToDate.Date.ToShortDateString() + "' id='lblToDate' style='font-weight:normal'>" + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.ToDate.Date.ToShortDateString()) + "</label>",
                              EmployeeID = "<label id='lblEmployeeID' style='font-weight:normal'>" + item.EmployeeID + "</label>",
                              PayEmployeeAbsentHeaderID = "<label id='lblPayEmployeeAbsentHeaderID' style='font-weight:normal'>" + item.PayEmployeeAbsentHeaderID + "</label>",
                              Amount = "<label id='lblAmount' style='font-weight:normal'>" + item.Amount.ToString(AmountFormat) + "</label>",
                              EmployeeName = item.EmployeeName,
                              AbsentID = item.AbsentID,
                              TotalDedSalary = "<label id='lblTotalDedSalary' style='font-weight:normal'>" + item.TotalDedSalary + "</label>",
                              PayEmployeeAbsentID = "<label id='lblPayEmployeeAbsentID' style='font-weight:normal'>" + item.PayEmployeeAbsentID + "</label>",
                              PayAbsentTypeID = "<label id='lblPayAbsentTypeID' style='font-weight:normal'>" + item.PayAbsentTypeID + "</label>",
                              IsConfirmed = item.IsConfirmed,
                              Status = "<label id='lblStatusID' style='font-weight:normal'>" + (item.StatusID != null ? common.AbsentStatus[Convert.ToInt32(item.StatusID)] : "") + "</label>",
                              CycleID = item.CycleID,
                              NoOfNullCycleCount = item.NoOfNullCycleCount
                          }).ToArray(),

            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        [HttpPost]
        public JsonResult ConfirmEmployeeForAbsentDeductedStatus(List<AbsentsForCurrentMonthModel> AbsentDeductionList, int PaidCycleId)
        {
            OperationDetails operationDetails = new OperationDetails();
            //LateDeductionModel lateDeductionModel = new LateDeductionModel();
            AbsentDeductionDB absentDeductionDB = new AbsentDeductionDB();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            int deductionId = absentDeductionDB.GetDeductionSetting().AbsentDeductionTypeId;
            foreach (AbsentsForCurrentMonthModel AbsentsForCurrentMonthModel in AbsentDeductionList)
            {
                AbsentsForCurrentMonthModel.EmployeeID = AbsentsForCurrentMonthModel.EmployeeID;
                AbsentsForCurrentMonthModel.PayEmployeeAbsentHeaderID = AbsentsForCurrentMonthModel.PayEmployeeAbsentHeaderID;
                AbsentsForCurrentMonthModel.EffectiveDate = AbsentsForCurrentMonthModel.EffectiveDate;
                var result = absentDeductionDB.ConfirmEmployeeForAbsentDeductedStatus(AbsentsForCurrentMonthModel.EmployeeID, AbsentsForCurrentMonthModel.PayEmployeeAbsentHeaderID, AbsentsForCurrentMonthModel.EffectiveDate);
                if (result.InsertedRowId > 0)
                {
                    PayDeductionModel payDeductionModel = new PayDeductionModel();
                    payDeductionModel.DeductionTypeID = deductionId;
                    payDeductionModel.PaidCycle = 1;
                    payDeductionModel.IsInstallment = false;
                    payDeductionModel.Amount = Convert.ToDouble(AbsentsForCurrentMonthModel.Amount);
                    payDeductionModel.Installment = 0;
                    //payDeductionModel.Comments = AbsentsForCurrentMonthModel.Comments;
                    payDeductionModel.Comments = "";
                    payDeductionModel.RefNumber = "";
                    payDeductionModel.PvId = 0;
                    payDeductionModel.EmployeeID = AbsentsForCurrentMonthModel.EmployeeID;
                    payDeductionModel.EffectiveDate = AbsentsForCurrentMonthModel.EffectiveDate;
                    payDeductionModel.PaidCycle = PaidCycleId;
                    payDeductionModel.Comments = "Absent Deduction (" + AbsentsForCurrentMonthModel.Status + ") From " + AbsentsForCurrentMonthModel.FromDate.ToString("dd/MM/yyyy") + " To " + AbsentsForCurrentMonthModel.ToDate.ToString("dd/MM/yyyy");
                    operationDetails = absentDeductionDB.PayDeductionAbsentCrud(payDeductionModel, 1, AbsentsForCurrentMonthModel.PayEmployeeAbsentID);
                    PayDeductionDetailModel payDeductionDetailModel = new PayDeductionDetailModel();
                    payDeductionDetailModel.PayDeductionID = operationDetails.InsertedRowId;
                    payDeductionDetailModel.DeductionDate = payDeductionModel.EffectiveDate;
                    payDeductionDetailModel.Amount = Convert.ToDouble(AbsentsForCurrentMonthModel.Amount);
                    payDeductionDetailModel.IsActive = true;
                    payDeductionDetailModel.Comments = payDeductionModel.Comments;
                    absentDeductionDB.PayDeductionDetailsAbsentCrud(payDeductionDetailModel, 1);
                }
            }
            return Json(new { result = "success", resultMessage = " Employee Confirmed for Absent Deducted Status successfully." }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAbsentsForCurrentMonthExcused(string StartDate, string EndDate, string Month = "", string Year = "")
        {
            DataAccess.GeneralDB.CommonDB common = new DataAccess.GeneralDB.CommonDB();

            DBHelper objDBHelper = new DBHelper();
            Session["Month"] = Month;
            Session["Year"] = Year;
            if (Year.Contains("-"))
            {
                string[] words = Year.Split('-');
                Year = words[0];
            }
            else
            {
                Year = DateTime.Now.Year.ToString();
            }


            AbsentDeductionDB objAbsentDeductionDB = new AbsentDeductionDB();
            List<AbsentsForCurrentMonthExcusedModel> absentList = new List<AbsentsForCurrentMonthExcusedModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            int? UserId = null;
            UserId = objUserContextViewModel.UserId;
            absentList = objAbsentDeductionDB.GetAbsentsForCurrentMonthExcused(UserId, Month, Year, StartDate, EndDate);
            var vList = new object();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            List<AbsentTypeModel> objabsenttypeList = new List<AbsentTypeModel>();
            objabsenttypeList = objAbsentTypeDB.GetAbsentTypeList();
            string values = "";
            if (objabsenttypeList.Count > 0)
            {

                foreach (var obj in objabsenttypeList)
                {
                    values += "<option value='" + obj.PayAbsentTypeID + "'>" + obj.PayAbsentTypeName_1 + "</option>";
                }
            }
            vList = new
            {
                aaData = (from item in absentList.Where(x => x.IsConfirmed == false)
                          select new
                          {
                              AbsentID = item.AbsentID,
                              Days = item.Days,
                              //FromDate =  item.FromDate.ToShortDateString(),
                              //ToDate = item.ToDate.ToShortDateString(),
                              FromDate = "<label data-FromDate='" + item.FromDate.Date.ToShortDateString() + "' id='lblFromDate' style='font-weight:normal'>" + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.ToDate.Date.ToShortDateString()) + "</label>",
                              ToDate = "<label data-Todate='" + item.ToDate.Date.ToShortDateString() + "' id='lblToDate' style='font-weight:normal'>" + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.ToDate.Date.ToShortDateString()) + "</label>",
                              EmployeeID = item.EmployeeID,
                              PayEmployeeAbsentHeaderID = item.PayEmployeeAbsentHeaderID,
                              EmployeeName = item.EmployeeName,
                              PayAbsentTypeID = item.PayAbsentTypeID,

                              PayEmployeeAbsentID = item.PayEmployeeAbsentID,
                              IsConfirmed = "<label id='lblIsConfirmed' style='font-weight:normal'>" + item.IsConfirmed + "</label>",
                              AbsentTypeName = "<select class='form-control selectpickerddl' onchange=\"ChangeAbsentType(this," + item.PayEmployeeAbsentID + ",'UnDeduction')\" id='ddlabsentCurrent_" + item.PayAbsentTypeID + "' > " + values + " </select>",
                              Status = "<label id='lblStatusID' style='font-weight:normal'>" + (item.StatusID != null ? common.AbsentStatus[Convert.ToInt32(item.StatusID)] : "") + "</label>",
                              CycleID = item.CycleID
                          }).ToArray(),
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }


        public ActionResult CheckUpdateAbsentType(int absenttypeId, int payemployeeAbsentId)
        {
            AbsentDeductionDB absentDeductiondb = new AbsentDeductionDB();
            PayAbsentTypeModel payAbsentType = absentDeductiondb.GetPayAbsentType().Where(x => x.PayAbsentTypeID == absenttypeId).FirstOrDefault();
            SetEmployeeLeaveTypeStatus(absenttypeId, payemployeeAbsentId);
            return Json(new { isDeductable = payAbsentType.ISDeductable, LeaveTypeId = payAbsentType.AbsentPaidTypeId }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetNullcycleCount(string Startdate, string EndDate, string Month = "", string Year = "")
        {
            int count = 0;
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();
            if (objRuleSetting.AppliedDeductionRules == true)
            {
                AbsentDeductionDB objDB = new AbsentDeductionDB();
                count = objDB.GetNullcycleCountBetweenDates(Month, Year, Startdate, EndDate);
            }

            return Json(new { count }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetNullAcademicYears(string Startdate, string EndDate, string Month = "", string Year = "")
        {
            string AcRecords = string.Empty;
            string Message = string.Empty;
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();                       

            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 3)
            {
                AbsentDeductionDB objDB = new AbsentDeductionDB();
                AcRecords = objDB.GetNullAcYearsBetweenDates(Month, Year, Startdate, EndDate);
                Message = " Ending date of Academic year " + AcRecords + " is not define and we cannot calculate absent deduction amount";
            }
            else
            {
                AcRecords = string.Empty;
            }

            return Json(new { AcRecords, Message }, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Late Deduction
        public ActionResult LateDedutions()
        {
            LateDeduction objLateDeduction = new LateDeduction();
            AbsentLateDeductionDB objAbsentLateDeductionDB = new AbsentLateDeductionDB();
            objLateDeduction = objAbsentLateDeductionDB.GetAttendanceSetup();
            return View(objLateDeduction);
        }
        #endregion
    }
}