﻿using HRMS.DataAccess;
using HRMS.Entities;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class JobController: BaseController
    {
        //
        // GET: /Job/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetJobTitleList()
         {            
            List<HRMS.Entities.JobTitle> objJobTitle = new List<HRMS.Entities.JobTitle>();
            JobDB ObjJob = new JobDB();
            objJobTitle = ObjJob.GetAllJobTitle();

            var vList = new object();

            vList = new
            {
                aaData = (from item in objJobTitle
                          select new
                          {

                               Actions = "<a  onclick='EditChannel(" + item.JobTitleID.ToString() + ")' class='btn btn-success btn-rounded btn-condensed btn-sm'  title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm'  onclick='DeleteChannel(" + item.JobTitleID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i></a>",
                              JobTitleID = item.JobTitleID,
                              JobTitleName_1 = item.JobTitleName_1,
                              JobTitleName_2 = item.JobTitleName_2,
                              JobTitleShortName_1 = item.JobTitleShortName_1,
                              JobTitleShortName_2 = item.JobTitleShortName_2

                          }).ToArray()
            };


            return Json(vList, JsonRequestBehavior.AllowGet);





        }


        public JsonResult InsertUpdateJobTitle(HRMS.Entities.JobTitle ObjTitle)
        {

            JobDB ObjJob = new JobDB();
            ObjJob.InsertUpdateJobTitle(ObjTitle);
            return Json(0, JsonRequestBehavior.AllowGet);

        }


        public JsonResult Delete(int ID)
        {

            JobDB ObjJob = new JobDB();
            string result = "";
            string resultMessage = "";
            int success = ObjJob.DeleteJobTitle(ID);
            result = success > 0 ? "success": "warning";
            resultMessage = success > 0 ? "Record Deleted Successfully": "This Job Title has been assigned to Employee.Unable to delete the record";
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult AddEditJob(int Id)
        {

            HRMS.Entities.JobTitle ObjTitle = new HRMS.Entities.JobTitle();

            if (Id != -1)
            {
                JobDB ObjJob = new JobDB();
                ObjTitle = ObjJob.GetJobTitleById(Id);
            }
            else
            {
                ObjTitle.JobTitleID = Id;
            }

            return View(ObjTitle);
        }


        public ActionResult ExportToExcel()
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "Job" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            
            JobDB objJobDB = new JobDB();
            System.Data.DataSet ds = objJobDB.GetAllJobTitleDatasSet();
         
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            string fileName = "Job" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            
            JobDB objJobDB = new JobDB();
            System.Data.DataSet ds = objJobDB.GetAllJobTitleDatasSet();
            
           
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }
    }
}