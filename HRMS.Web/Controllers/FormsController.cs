﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.PDRP;

namespace HRMS.Web.Controllers
{
    public class FormsController : BaseController
    {
        public UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        FormsDB formsDB;
        public FormsController()
        {
            //  objUserContextViewModel = new UserContextViewModel();
            formsDB = new FormsDB();
        }

        #region Forms Request variables
        string approvaltemplate = @"Hi {0}, <br /><br /> The request ID: <b>{1}</b> is assigned to you. Please review and approve the assigned request. <br /><br /><br /> Best Regards,<br /> Beam Management";
        //string approvaltitle = "Request ID : {0} - {1} - {2} Assigned To You.";
        string approvaltitle = "Request ID : {0} - {1} - {2}";
        string rejecttemplate = @"Hi {0}, <br /><br /> The request ID: <b>{1}</b> has been rejected. Take your appropriate action if required.<br /><br /><br /> Best Regards,<br /> Beam Management";
        //string rejecttitle = "Request ID : {0} - {1} - {2} has Been Rejected.";
        string rejecttitle = "Request ID : {0} - {1} - {2}";
        string finalnotificationtemplate = @"Hi {0}, <br /><br /> The request ID: <b>{1}</b> complete the process of all approvals. You may proceed to review and take appropriate action if required.<br /><br /><br /> Best Regards,<br /> Beam Management";
        // string finalnotificationtitle = "Request ID : {0} - {1} - {2} Completed All Approvals.";
        string finalnotificationtitle = "Request ID : {0} - {1} - {2}";
        string requesterapprovaltemplate = @"Hi {0}, <br /><br /> The request ID: <b>{1}</b> is approved by {2} and currently its pending with {3}. <br /><br /><br /> Best Regards,<br /> Beam Management";
        //string requesterapprovaltitle = "Request ID : {0} - {1} - {2} is Approved.";
        string requesterapprovaltitle = "Request ID : {0} - {1} - {2}";
        string requesterinitializetemplate = @"Hi {0}, <br /><br /> The request ID: <b>{1}</b> is generated and currently its pending with {2}. <br /><br /><br /> Best Regards,<br /> Beam Management";
       // string requesterinitializetitle = "Request ID : {0} - {1} - {2} is Generated.";
        string requesterinitializetitle = "Request ID : {0} - {1} - {2}";
        //string requesterreinitializetitle = "Request ID : {0} - {1} - {2} is Reinitialized.";
        string requesterreinitializetitle = "Request ID : {0} - {1} - {2}";
        string requesterreinitializetemplate = @"Hi {0}, <br /><br /> The request ID: <b>{1}</b> is reinitialized and currently its pending with {2}. <br /><br /><br /> Best Regards,<br /> Beam Management";

        string message = "", subject = "";
        string receiver = "";
        #endregion

        // GET: Forms
        #region Forms Request Email Methods
        /// <summary>
        /// Method for Sending email to mutiple approvers 
        ///Usage,when mutiple forms are saved at once
        /// </summary>
        /// <param name="requestFormsApproverEmailModelList"></param>
        public void SendApproverEmail(List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList)
        {

            OperationDetails op = new OperationDetails();         
            var approver = requestFormsApproverEmailModelList.Where(x => x.IsApprover == true).FirstOrDefault();
            var nextapprover = requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).FirstOrDefault();
            foreach (RequestFormsApproverEmailModel obj in requestFormsApproverEmailModelList)
            {
                subject = "";
                message = "";
                receiver = "";
                if (obj.IsRequester)
                {
                    if (approver != null && nextapprover != null)
                    {
                        receiver = obj.WorkEmail;
                        subject = string.Format(requesterapprovaltitle, obj.RequestID.ToString(), obj.BUShortName_1,obj.FormName);
                        message = string.Format(requesterapprovaltemplate, obj.EmployeeName, approver.RequestID.ToString(), approver.GroupName + " [ " + approver.EmployeeFullName + " ]", nextapprover.GroupName + " [ " + nextapprover.EmployeeFullName + " ] ").Replace("\n", Environment.NewLine);
                    }
                }
                if (obj.IsNextApprover)
                {
                    receiver = obj.WorkEmail; 
                    subject = string.Format(approvaltitle, obj.RequestID.ToString(), obj.BUShortName_1, obj.FormName);
                    message = string.Format(approvaltemplate, obj.EmployeeName, obj.RequestID.ToString()).Replace("\n", Environment.NewLine);
                }

                if(!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(receiver))
                op = new CommonHelper.CommonHelper().SendMail(receiver, subject, message);
            }

        }
        public void SendRequestInitializeEmail(List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList, bool isReIntialized)
        {
            OperationDetails op = new OperationDetails();          
            var nextapprover = requestFormsApproverEmailModelList.Where(x => x.IsNextApprover == true).FirstOrDefault();
            foreach (RequestFormsApproverEmailModel obj in requestFormsApproverEmailModelList)
            {
                subject = "";
                message = "";
                receiver = "";

                if (obj.IsRequester)
                {
                    if (nextapprover != null)
                    {
                        receiver = obj.WorkEmail;
                        subject = string.Format(isReIntialized ? requesterreinitializetitle : requesterinitializetitle, obj.RequestID.ToString(),obj.BUShortName_1, obj.FormName);
                        message = string.Format(isReIntialized ? requesterreinitializetemplate : requesterinitializetemplate, obj.EmployeeName, obj.RequestID.ToString(), nextapprover.GroupName + " [ " + nextapprover.EmployeeFullName + " ] ").Replace("\n", Environment.NewLine);
                    }
                }
                if (obj.IsNextApprover)
                {
                    receiver = obj.WorkEmail;
                    subject = string.Format(approvaltitle, obj.RequestID.ToString(), obj.BUShortName_1, obj.FormName);
                    message = string.Format(approvaltemplate, obj.EmployeeName, obj.RequestID.ToString()).Replace("\n", Environment.NewLine);
                }
                if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(receiver))
                    op = new CommonHelper.CommonHelper().SendMail(obj.WorkEmail, subject, message);

            }
        }
        /// <summary>
        /// Method for Sending email to approver
        /// </summary>
        /// <param name="requestFormsApproverEmailModel"></param>
        public void SendApproverEmail(RequestFormsApproverEmailModel requestFormsApproverEmailModel)
        {
            OperationDetails op = new OperationDetails();
            subject = string.Format(approvaltitle, requestFormsApproverEmailModel.RequestID.ToString());
            message = string.Format(approvaltemplate, requestFormsApproverEmailModel.EmployeeName, requestFormsApproverEmailModel.RequestID.ToString()).Replace("\n", Environment.NewLine);
            op = new CommonHelper.CommonHelper().SendMail(requestFormsApproverEmailModel.WorkEmail, subject, message);

        }
        /// <summary>
        ///  Method for Sending email notification on final approval of a form
        /// </summary>
        /// <param name="requestFormsApproverEmailModelList"></param>
        public void SendFinalNotificationEmail(List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList)
        {
            
            OperationDetails op = new OperationDetails();
            //  RequestFormsApproveModel requestFormsApproveModel; 28/11 Remove Duplicate
            foreach (RequestFormsApproverEmailModel obj in requestFormsApproverEmailModelList.GroupBy(x => x.EmployeeID).Select(x => x.FirstOrDefault()).ToList())
            {
                //requestFormsApproveModel = requestFormsApproveModelList.Single(s => s.FormProcessID == obj.FormProcessID);
                subject = string.Format(finalnotificationtitle, obj.RequestID.ToString(),obj.BUShortName_1, obj.FormName);
                message = string.Format(finalnotificationtemplate, obj.EmployeeName, obj.RequestID.ToString()).Replace("\n", Environment.NewLine);
                //  requestFormsApproveModel.RequestFormsProcessModel.RequestID;
                op = new CommonHelper.CommonHelper().SendMail(obj.WorkEmail, subject, message);
            }
        }
        /// <summary>
        /// Method for Sending email notification on rejection of a form
        /// </summary>
        /// <param name="requestFormsApproverEmailModelList"></param>
        public void SendRejectionEmail(List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList)
        {
            OperationDetails op = new OperationDetails();           
            foreach (RequestFormsApproverEmailModel obj in requestFormsApproverEmailModelList)
            {             
                subject = string.Format(rejecttitle, obj.RequestID.ToString(),obj.BUShortName_1, obj.FormName);
                message = string.Format(rejecttemplate, obj.EmployeeName, obj.RequestID.ToString()).Replace("\n", Environment.NewLine);             
                op = new CommonHelper.CommonHelper().SendMail(obj.WorkEmail, subject, message);
            }
        }

        #endregion
        /// <summary>
        /// //Downloads the uploaded doucments 
        /// </summary>
        /// <param name="fileID"></param>
        public FileContentResult PreviewDocument(int fileID)
        {
            try
            {
                AllFormsFilesModel allFormsFilesModel = new AllFormsFilesDB().GetAllFormsFiles(fileID);
                byte[] doc = allFormsFilesModel.DocumentFile;
                Response.AppendHeader("Content-Disposition", "inline; filename=" + allFormsFilesModel.FileName);
                return File(doc, allFormsFilesModel.FileContentType);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        /// <summary>
        /// Common method for any Approving Form ,Virtual function if need a different 
        /// implementation in any individual form controller can override 
        /// </summary>
        /// <param name="formProcessID"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {                        
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }

                        //Add any logic to be done once approval process is completed
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Common method for any Rejecting Form ,Virtual function if need a different 
        /// implementation in any individual form controller can override 
        /// </summary>
        /// <param name="formProcessID"></param>
        /// <param name="comments"></param>
        /// <returns></returns>

        [HttpPost]
        public virtual ActionResult RejectForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.RejectRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                    {
                        SendRejectionEmail(requestFormsApproverEmailModelList);
                        operationDetails.Message += @" & Final Email Notification send";
                    }

                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while rejecting forms request";
                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Get All Employees by passing mutiple companyids
        /// </summary>
        /// <param name="CompanyIds"></param>
        /// <returns></returns>
        public ActionResult GetAllEmployeeForAdminByCompanyID(int CompanyId)
        {
            List<EmployeeDetailsModel> employeeList = new EmployeeDB().GetAllEmployeeForAdminByCompanyID(CompanyId);
            return Json(employeeList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeDepartments(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Department", Value = "0" });

            foreach (var m in new DepartmentDB().GetAllDepartmentByCompanyId(companyId, objUserContextViewModel.UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.DepartmentName_1, Value = m.DepartmentId.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPositions(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Position", Value = "0" });

            foreach (var m in new PositionDB().GetPositionList(companyId: companyId, userId: objUserContextViewModel.UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PositionTitle, Value = m.PositionID.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Sets the FormProcessID to session passed from My Tasklist Screen.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SetFormProcessSesssion(int id)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            Session[controllerName] = id;
            // Session["FormProcessID"] = id;
            Session["IsEditRequestFromAllRequests"] = false;
            Session["IsR1Release"] = false;
            return Json(new { id = id }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SetR1ReleaseFormProcessSesssion(int id)
        {
            //Session["FormProcessID"] = id;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            Session[controllerName] = id;
            Session["IsEditRequestFromAllRequests"] = false;
            Session["IsR1Release"] = true;
            return Json(new { id = id }, JsonRequestBehavior.AllowGet);

        }
        //public bool IsFromR1Release()
        //{
        //    if (Session["IsR1Release"] != null)
        //        return (bool)Session["FormProcessID"];
        //    else
        //        return false;
        //}
        /// <summary>
        /// Returns selected FormProcessID from the Session
        /// </summary>
        /// <returns>FormProcessID </returns>
        public int GetFormProcessSessionID()
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int id = 0;
            if (Session[controllerName] != null)
                id = (int)Session[controllerName];
            else
                id = 0;
            if (id == 0)
                RedirectToAction("Index", "FormsTaskList");
            return id;
        }

        public ActionResult SetEditRequestsFormProccessSession(int id)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            Session[controllerName] = id;
           // Session["FormProcessID"] = id;
            Session["IsEditRequestFromAllRequests"] = true;
            return Json(new { id = id}, JsonRequestBehavior.AllowGet);
        }

        public bool GetEditRequestsFormProccessSession()
        {           
            bool isEditRequestFromAllRequests = false;
            if (Session["IsEditRequestFromAllRequests"] != null)
                isEditRequestFromAllRequests = (bool)Session["IsEditRequestFromAllRequests"];
            return isEditRequestFromAllRequests;
        }

        public virtual ActionResult IsWorkFlowExists(Int16 formID, string companyIDs)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                FormsDB formsDB = new FormsDB();
                operationDetails = formsDB.IsWorkFlowExists(formID, companyIDs, objUserContextViewModel.UserId);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";

                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }
        public RecruitR1BudgetedViewModel GetRecruitFormDetails(int? employeeID)
        {
            FormsDB formsDB = new FormsDB();
            return formsDB.GetRecruitFormDetails(employeeID);
        }
        //[ChildActionOnly]
        public ActionResult GetRequesterInfoDetails(Int16 formID, DateTime? requestDate, int employeeID, bool? isAddMode, int? formProcessId)
        {
            RequesterInfoViewModel requesterInfoViewModel = formsDB.GetRequesterInformation(employeeID, isAddMode, formProcessId);
            requesterInfoViewModel.RequestDate = requestDate == null ? DateTime.Now : (DateTime)requestDate;
            requesterInfoViewModel.FormID = formID;
            return PartialView("~/Views/Forms/_RequesterInfoDetails.cshtml", requesterInfoViewModel);
        }

        

        public ActionResult GetFormsApprovalHistory(int formProcessID)
        {
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            requestFormsApproveModelList = formsDB.GetAllRequestFormsApprovalHistory(formProcessID);
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //*** Naresh 2020-03-12 Display last comment
            string lastComment = "";
            var lastItem = requestFormsApproveModelList.OrderBy(s => s.ModifiedOn).LastOrDefault();
            if (lastItem != null && lastItem.ApproverEmployeeID == objUserContextViewModel.UserId)
            {
                lastComment = lastItem.Comments;
            }
            ViewBag.LastComment = lastComment;
            return PartialView("~/Views/Forms/_ApprovalHistory.cshtml", requestFormsApproveModelList);
        }

        public virtual ActionResult CheckFormLoadValidation(int formID, int companyId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                FormsDB formsDB = new FormsDB();
                operationDetails = formsDB.CheckFormLoadValidation(formID, companyId, objUserContextViewModel.UserId);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";

                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }

        public List<Entities.General.OtherPermissions> GetPermissionList()
        {
            UserRoleDB userRoleDB = new UserRoleDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            return PermissionList;
        }

        public ActionResult GetFormsWorkflowBasedOnCompany(short? formId, short companyId)
        {
            FormsWorkflowDB formsWorkFlowDB = new FormsWorkflowDB();
            List<FormsWorkflowModel> groupList = formsWorkFlowDB.GetAllFormsWorkflow(companyId, formId);
            return Json(groupList.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}