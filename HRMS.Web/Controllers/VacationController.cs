﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.General;
using HRMS.DataAccess;
using HRMS.Entities.ViewModel;
using HRMS.Entities;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.Web.Controllers
{
    public class VacationController : BaseController
    {
        // GET: Vacation

        VacationDB vacationdb;
        string ruleMessage = "";
        int messageCounter = 0;
        string specingType = "";
        public ActionResult Index(int? deptId, int? sectionId)
        {
            vacationdb = new VacationDB();
            DepartmentDB objDepartmentMentDb = new DepartmentDB();
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            UserContextViewModel userContext = CheckSession();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.department = new SelectList(objDepartmentMentDb.GetDepartmentListWithPermission(UserId), "DepartmentID", "DepartmentName_1");
            ViewBag.Section = new SelectList(objEmployeeSectionDB.GetEmployeeSection(UserId), "EmployeeSectionID", "EmployeeSectionName_1");
            ViewBag.rDeptId = deptId;
            ViewBag.rSectionId = sectionId;
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/Vacation/Index");
            Session["Vacation"] = permissionModel;
            return View();
        }

        public ActionResult LoadVacation()
        {
            UserContextViewModel userContext = CheckSession();
            DepartmentDB departmentDB = new DepartmentDB();
            EmployeeDB employeeDB = new EmployeeDB();
            EmployeeSectionDB employeeSectionDB = new EmployeeSectionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            vacationdb = new VacationDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.empddl = new SelectList(employeeDB.GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            ViewBag.shiftddl = new SelectList(vacationdb.GetShiftList(), "id", "text");
            ViewBag.vactionddl = vacationdb.GetVactionTypeList();
            ViewBag.deptddl = new SelectList(departmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.sectddl = new SelectList(employeeSectionDB.GetSectionListByUserId(objUserContextViewModel.UserId), "EmployeeSectionID", "EmployeeSectionName_1");
            List<Employee> lstEmployee = employeeDB.GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1).ToList();
            return PartialView(lstEmployee);
        }

        public ActionResult EditVactionRecord(int id)
        {
            vacationdb = new VacationDB();
            ViewBag.vactionddl = new SelectList(vacationdb.GetVactionTypeList(), "id", "text");
            VacationModel vactionmodel = vacationdb.GetVactionModelById(id);
            return PartialView(vactionmodel);
        }

        [HttpPost]
        public ActionResult EditVactionRecord(VacationModel Model)
        {
            vacationdb = new VacationDB();
            OperationDetails op = new OperationDetails();
            VacationTypeModel vacType = vacationdb.GetVacationTypeById(Model.vacationTypeId);
            bool isAccumulativeCheck = false;
            if (vacType.isAccumulatedLeave)
            {
                isAccumulativeCheck = CheckAccumulativeLeaves(vacType, Model.employeeId, Model.fromDate, Model.toDate);
            }
            else
            {
                isAccumulativeCheck = true;
            }

            bool check = false;
            if (isAccumulativeCheck)
            {
                check = vacationdb.UpdateVaction(Model);
                if (check)
                {
                    op.Success = true;
                    op.CssClass = "success";
                    op.Message = "Leave is save successfully";
                }
                else
                {
                    op.Success = false;
                    op.CssClass = "error";
                    op.Message = "Technical error has occured";
                }
            }
            else
            {
                op.Success = false;
                op.CssClass = "error";
                op.Message = ruleMessage;
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteVactionRecord(int id)
        {
            vacationdb = new VacationDB();
            return Json(vacationdb.DeleteVaction(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveVactionRecord(string mode, int id, string fromDate, string toDate, int typeId)
        {
            OperationDetails op = new OperationDetails();
            vacationdb = new VacationDB();
            UserContextViewModel userContext = CheckSession();
            op = vacationdb.SaveVationDate(mode, id, fromDate, toDate, typeId, CommonDB.GetUserId(userContext));
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterGrid(int? deptId, int? sectionId)
        {
            UserContextViewModel userContext = CheckSession();
            vacationdb = new VacationDB();
            IEnumerable<VacationModel> modelList;
            modelList = vacationdb.GetVactionData(CommonDB.GetUserId(userContext), sectionId, deptId, null);
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/Vacation/Index");
            string Action = "";

            if (permissionModel.IsUpdateOnlyPermission)
            {
                Action += "<a onclick='vaction.editVaction(item)' class='btn btn-success  btn-rounded btn-condensed btn-sm btn-space' title='Edit'><i class='fa fa-pencil'></i></a>";
            }
            if (permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a onclick='vaction.deleteVaction(item)' class='btn btn-danger  btn-rounded btn-condensed btn-sm btn-space' title='Delete'><i class='fa fa-times'></i> </a>";
            }

            var vList = new
            {
                aaData = (from item in modelList.OrderBy(x => x.employeeName)
                          select new
                          {
                              EmployeeId = item.employeeId,
                              EmployeeAlternativeId = item.employeeAlternativeId,
                              EmployeeName = item.employeeName,
                              VacationType = item.vacationType,
                              FromDate = item.fromDate,
                              ToDate = item.toDate,
                              Action = Action.Replace("item", item.id.ToString())
                          }).ToArray(),
                recordsTotal = modelList.Count(),
                recordsFiltered = modelList.Count()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public UserContextViewModel CheckSession()
        {
            if (Session["userContext"] != null)
            {
                UserContextViewModel model = (UserContextViewModel)Session["userContext"];
                return model;
            }
            else
            {
                UserContextViewModel model = new UserContextViewModel();
                return model;
            }
        }

        public ActionResult CheckVactionRules(string modeType, int typeId, string fromDate, string toDate, int vacationTypeId)
        {
            int mode = 0;
            bool isGenderRulePass = false;
            bool isNationalityRulePass = false;
            bool isMaretialRulePass = false;
            bool isReligionRulePass = false;
            bool isVacationPositionRulePass = false;
            bool isApplicableAfterrulePass = false;
            bool isOtherrulePass = false;
            bool isAcumulatedLeavePass = false;
            bool isDatesinBetweenAlreadyTaken = false;
            bool isDepartmentRulePass = false;
            bool isJobCategoryRulePass = false;
            bool isJobTitleRulePass = false;
            VacationDB vacationdb = new VacationDB();
            EmployeeDB employeedb = new EmployeeDB();
            VacationTypeModel vacationModel = vacationdb.GetVacationTypeById(vacationTypeId);
            bool isCountSalaryForWorkDays = vacationdb.CheckIfCountSalaryForWorkDays();
            List<VactionEmoloyeeInformationModel> employeeReturnList = new List<VactionEmoloyeeInformationModel>();
            IEnumerable<VactionPositionModel> vacationPositionList = vacationdb.getPositionByVactionId(vacationTypeId);
            IEnumerable<VacationDepartmentModel> vacationDepartmentList = vacationdb.getDepartmentByVactionId(vacationTypeId);
            IEnumerable<VacationJobCategoryModel> vacationjobCategoryList = vacationdb.getJobCategoryByVactionId(vacationTypeId);
            IEnumerable<VacationjobTitleModel> vacationjobTitleList = vacationdb.getjobTitleByVactionId(vacationTypeId);
            //Beam Changes
            int isAnnual = 0;
            float leavesTaken = 0;
            DateTime StartDate = new DateTime();
            DateTime EndDate = new DateTime();
            float noOfDays = 0;
            switch (modeType)
            {
                case "shift":
                    mode = 1;
                    specingType = "\n";
                    break;
                case "department":
                    mode = 2;
                    specingType = "\n";
                    break;
                case "section":
                    mode = 3;
                    specingType = "\n";
                    break;
                case "employee":
                    mode = 4;
                    specingType = "<br>";
                    break;
            }
            IEnumerable<VactionEmoloyeeInformationModel> employeeInformationList = vacationdb.GetEmployeeInformationForVacation(mode, typeId);
            var returnVal = new { };
            DateTime startDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            foreach (var item in employeeInformationList)
            {
                if (item.hiredate != "")
                {
                    if (Convert.ToDateTime(item.hiredate) < startDate)
                    {

                        //Beam Changes                      
                        leavesTaken = new VacationDB().GetAlreadyTakenLeaveDays(item.employeeId, vacationTypeId, Convert.ToDateTime(item.hiredate), vacationModel.AnnualLeave);                      
                        StartDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        EndDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        noOfDays = float.Parse((EndDate - StartDate).TotalDays.ToString()) + 1;

                        ruleMessage = "";
                        messageCounter = 0;
                        isGenderRulePass = CheckGenderRule(vacationModel.GenderID, item.genderId);
                        isNationalityRulePass = CheckNationalityRule(vacationModel.NationalityID, item.nationalityId);
                        isMaretialRulePass = CheckMaritalStatusRule(vacationModel.MaritalStatusId, item.maritalStatusId);
                        isReligionRulePass = CheckReligionRule(vacationModel.ReligionID, item.religionId);
                        isVacationPositionRulePass = CheckPositionRule(vacationPositionList.Select(x => x.positionId).ToList(), item.positionId);
                        isDepartmentRulePass = CheckDepartmentRule(vacationDepartmentList.Select(x => x.departmentId).ToList(), item.departmentId);
                        isJobTitleRulePass = CheckJobTitleRule(vacationjobTitleList.Select(x => x.jobTitleId).ToList(), item.JobTitleId);
                        isJobCategoryRulePass = CheckJobcategoryRule(vacationjobCategoryList.Select(x => x.jobCategoryId).ToList(), item.JobCategoryId);
                        isApplicableAfterrulePass = (item.hiredate != "" ? CheckAppicableAfterRule(Convert.ToDateTime(item.hiredate), fromDate, vacationModel.ApplicableAfterDigits, vacationModel.ApplicableAfterTypeID) : false);
                        bool isAnnualLimitExceeded = false;
                        if (!vacationModel.isAccumulatedLeave)
                        {
                            //27-05-2019 
                            //isOtherrulePass = CheckOtherRule(vacationTypeId, item.employeeId, vacationModel.AnnualLeave, vacationModel.PerServiceVacation, vacationModel.RepeatedFor, vacationModel.NoOfDays, vacationModel.MonthlySplit, fromDate, toDate, out isAnnualLimitExceeded);
                            isOtherrulePass = CheckOtherRule(vacationTypeId, item.employeeId, vacationModel.AnnualLeave, vacationModel.PerServiceVacation, vacationModel.RepeatedFor, vacationModel.TotalNoOfDays, vacationModel.MonthlySplit, fromDate, toDate, out isAnnualLimitExceeded);
                        }
                        else
                        {
                            isOtherrulePass = CheckOtherRule(vacationTypeId, item.employeeId, vacationModel.AnnualLeave, vacationModel.PerServiceVacation, vacationModel.RepeatedFor, vacationModel.AccMaxNumberOfDays, vacationModel.MonthlySplit, fromDate, toDate, out isAnnualLimitExceeded);
                        }
                        if (isAnnualLimitExceeded)
                        {
                            isAcumulatedLeavePass = true;
                        }
                        else
                        {
                            isAcumulatedLeavePass = CheckAccumulativeLeaves(vacationModel, item.employeeId, fromDate, toDate);
                        }
                        isDatesinBetweenAlreadyTaken = CheckDatesinBetweenAlreadyTaken(vacationTypeId, item.employeeId, vacationModel.AnnualLeave, vacationModel.PerServiceVacation, fromDate, toDate);
                        if (isGenderRulePass && isNationalityRulePass && isMaretialRulePass && isReligionRulePass && isVacationPositionRulePass && isApplicableAfterrulePass
                            && isOtherrulePass && isAcumulatedLeavePass && isDatesinBetweenAlreadyTaken && isDepartmentRulePass && isJobTitleRulePass && isJobCategoryRulePass)
                        {
                            item.isRulePass = true;
                            item.ruleMessage = ruleMessage;
                            employeeReturnList.Add(item);
                        }
                        else
                        {
                            item.isRulePass = false;
                            item.ruleMessage = ruleMessage;
                            employeeReturnList.Add(item);
                        }
                    }
                    else
                    {
                        int r4 = 4;
                    }
                }
            }

            var vList = new
            {
                employeeList = employeeReturnList,
                message = ruleMessage,
                mode = mode,
                //27-05-2019 
                //paid = vacationModel.isCompensate,
                //|| vacationModel.HalfPaidDays > 0
                //Beam changes
                //if total leaves is greater than full paid days then setting flag for pushing deduction
                paid = (vacationModel.FullPaidDays > leavesTaken+noOfDays)?1:0,
                isSalaryForWorkingDays = isCountSalaryForWorkDays,
                countRulefail = employeeReturnList.Count(x => x.isRulePass == false)
            };

            return Json(vList, JsonRequestBehavior.AllowGet);

        }

        public bool CheckGenderRule(int? genderId, int? employeeGenderId)
        {
            bool isGenderRulePass = false;
            if (genderId == null || genderId == -1)
                isGenderRulePass = true;
            else
            {

                if (genderId == employeeGenderId)
                    isGenderRulePass = true;
                else
                    isGenderRulePass = false;
            }
            if (!isGenderRulePass)
                ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee gender is not eligible for selected leave type";
            return isGenderRulePass;
        }

        public bool CheckNationalityRule(int nationalityId, int employeeNationalityId)
        {
            bool isNationalityRulePass = false;
            if (nationalityId == 0)
                isNationalityRulePass = true;
            else
            {
                if (nationalityId == employeeNationalityId)
                    isNationalityRulePass = true;
                else
                    isNationalityRulePass = false;
            }
            if (!isNationalityRulePass)
                ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee nationality is not eligible for selected leave type";
            return isNationalityRulePass;
        }

        public bool CheckReligionRule(int religionId, int employeeReligionId)
        {
            bool isReligionRulePass = false;
            if (religionId == 0)
                isReligionRulePass = true;
            else
            {
                if (religionId == employeeReligionId)
                    isReligionRulePass = true;
                else
                    isReligionRulePass = false;
            }
            if (!isReligionRulePass)
                ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee religion is not eligible for selected leave type";
            return isReligionRulePass;
        }

        public bool CheckMaritalStatusRule(int maritalId, int employeeMaritalId)
        {
            bool isMaritalStatusRulePass = false;
            if (maritalId == 0)
                isMaritalStatusRulePass = true;
            else
            {
                if (maritalId == employeeMaritalId)
                    isMaritalStatusRulePass = true;
                else
                    isMaritalStatusRulePass = false;
            }
            if (!isMaritalStatusRulePass)
                ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee marital status is not eligible for selected leave type";
            return isMaritalStatusRulePass;
        }

        public bool CheckPositionRule(List<int> vacationPositionIds, int employeePositionId)
        {
            if (vacationPositionIds.Count() > 0)
            {
                bool isPoistionSRulePass = vacationPositionIds.Contains(employeePositionId);
                if (!isPoistionSRulePass)
                    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee position is not eligible for selected leave type";
                return isPoistionSRulePass;
            }
            else
            {
                return true;
            }
        }

        public bool CheckAccumulativeLeaves(VacationTypeModel vacTypeModel, int employeeID, string fromDate, string toDate)
        {
            DateTime StartDate = new DateTime();
            DateTime EndDate = new DateTime();
            vacationdb = new VacationDB();
            StartDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            EndDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            float noOfDays = float.Parse((EndDate - StartDate).TotalDays.ToString()) + 1;

            bool isChecked = false;
            if (vacTypeModel.isAccumulatedLeave)
            {
                float TotalAccvacDays = vacationdb.GetAcumulatedLeavesofEmployeeByVacationType(employeeID, vacTypeModel.VacationTypeId);
                float TotalAvlLeaves = TotalAccvacDays;
                if (noOfDays <= vacTypeModel.AccMaxNumberOfDays)
                {
                    if (noOfDays > TotalAvlLeaves)
                    {
                        float ExtraLeavesRequired = noOfDays - TotalAvlLeaves;
                        ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee is allowed for max. " + TotalAvlLeaves + " days but no. of leaves exceeded by " + ExtraLeavesRequired + " days.";
                        isChecked = false;
                    }
                    if (noOfDays <= TotalAvlLeaves)
                    {
                        isChecked = true;
                    }
                }
                else if (noOfDays > vacTypeModel.AccMaxNumberOfDays)
                {
                    float ExtraLeavesRequired = noOfDays - vacTypeModel.AccMaxNumberOfDays;
                    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee is allowed for max. " + vacTypeModel.AccMaxNumberOfDays + " accumulative days, applied leave days exceeded by " + ExtraLeavesRequired + ".";
                    isChecked = false;
                }
                else
                {
                    isChecked = true;
                }
            }
            else
            {
                float VacAlreadyTakenDays = 0;
                string HireDate = vacationdb.GetHireDate(employeeID);
                DateTime d2 = new DateTime();
                d2 = DateTime.ParseExact(HireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                VacAlreadyTakenDays = vacationdb.GetAlreadyTakenLeaveDays(employeeID, vacTypeModel.VacationTypeId, d2, vacTypeModel.AnnualLeave);
                //27-05-2019 
                //if (VacAlreadyTakenDays == vacTypeModel.NoOfDays)
                if (VacAlreadyTakenDays == vacTypeModel.TotalNoOfDays)
                {
                    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee already taken leave for this leave type.";
                    isChecked = false;
                }
                //27-05-2019 
                else if (VacAlreadyTakenDays < vacTypeModel.TotalNoOfDays && vacTypeModel.AnnualLeave == 1)
                //else if (VacAlreadyTakenDays < vacTypeModel.NoOfDays && vacTypeModel.AnnualLeave == 1)
                {
                    //27-05-2019                  
                    // float noOfDaysCanTake = vacTypeModel.NoOfDays - VacAlreadyTakenDays;
                    float noOfDaysCanTake = vacTypeModel.TotalNoOfDays - VacAlreadyTakenDays;
                    if (noOfDays <= noOfDaysCanTake)
                    {
                        isChecked = true;
                    }
                    else
                    {
                        ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee can apply for max. " + noOfDaysCanTake + " days leave as employee already taken " + VacAlreadyTakenDays + " leave days.";
                        isChecked = false;
                    }
                }
                //27-05-2019     
                //else if (VacAlreadyTakenDays < vacTypeModel.NoOfDays * (vacTypeModel.RepeatedFor == 0 ? 1 : vacTypeModel.RepeatedFor))
                else if (VacAlreadyTakenDays < vacTypeModel.TotalNoOfDays * (vacTypeModel.RepeatedFor == 0 ? 1 : vacTypeModel.RepeatedFor))
                {
                    //27-05-2019  
                    // float noOfDaysCanTake = vacTypeModel.NoOfDays * vacTypeModel.RepeatedFor - VacAlreadyTakenDays;
                    float noOfDaysCanTake = vacTypeModel.TotalNoOfDays * vacTypeModel.RepeatedFor - VacAlreadyTakenDays;
                    if (noOfDays <= noOfDaysCanTake)
                    {
                        isChecked = true;
                    }
                    else
                    {
                        ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee can apply for max. " + noOfDaysCanTake + " days leave as employee already taken " + VacAlreadyTakenDays + " leave days.";
                        isChecked = false;
                    }
                }
            }

            return isChecked;
        }

        public bool CheckAppicableAfterRule(DateTime hireDate, string sStartDate, int applicableAfter, int applicableType)
        {
            bool isApplicableAfterRulePass = false;
            int daysDifference = 0;
            DateTime startDate = DateTime.ParseExact(sStartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (applicableType == 1)
            {
                int hireDateMonth = hireDate.Month;
                int dayInApplicableAfterMonth = 0;
                int currentYear = DateTime.Now.Year;
                for (int i = 0; i < applicableAfter; i++)
                {
                    dayInApplicableAfterMonth = dayInApplicableAfterMonth + DateTime.DaysInMonth(currentYear, hireDateMonth);
                }
                applicableAfter = dayInApplicableAfterMonth;
            }
            hireDate = hireDate.AddDays(applicableAfter);
            if (hireDate <= startDate)
                isApplicableAfterRulePass = true;
            else
            {
                isApplicableAfterRulePass = false;
                daysDifference = (int)((hireDate - startDate).TotalDays + 1);
            }
            if (!isApplicableAfterRulePass)
                ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee is eligible for leave after " + daysDifference + " days";
            return isApplicableAfterRulePass;
        }

        public bool CheckOtherRule(int vacationTypeId, int employeeId, int annualVacation, int perServiceVacation, int repeatPerService, int numberOfdays, int monthlySplit, string sStartDate, string sEndDate, out bool isAnnualLimitExceeded)
        {
            isAnnualLimitExceeded = false;
            bool isRulePass = false;
            int mode = 0;
            int vacationTakenDays = 0;
            int appliedVacationDay = 0;
            int totalVacationDay = 0;
            VacationDB vacationDb = new VacationDB();
            DateTime startDate = DateTime.ParseExact(sStartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(sEndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            appliedVacationDay = Convert.ToInt32(((endDate - startDate).TotalDays) + 1);
            if (annualVacation == 1 && perServiceVacation == 0)
            {
                mode = 1;
            }
            if (annualVacation == 0 && perServiceVacation == 1)
            {
                mode = 2;
            }
            IEnumerable<VacationModel> vacationData = vacationDb.GetVacationForAnnualAndPerservice(mode, employeeId, vacationTypeId);

            if (monthlySplit > 0 && annualVacation == 1)
            {
                if (vacationData.Count() == 0)
                {

                    if (appliedVacationDay <= numberOfdays)
                        isRulePass = true;
                    else
                        isRulePass = false;

                    if (!isRulePass)
                        ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Applied leave days exceeded by maximum limit.";

                }
                else
                {
                    isRulePass = true;
                    //if (!isRulePass)
                    //    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "leave already taken by employee";
                }
            }
            else
            {
                /*Annual Vacation And Per Service Vacation*/
                if (repeatPerService <= 1)
                {
                    if (appliedVacationDay == numberOfdays)
                        isRulePass = true;
                    else
                        isRulePass = false;

                    if (!isRulePass)
                        ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "No of leave days does not matched with leave type days";
                }
                else
                {
                    if (repeatPerService > vacationData.Count())
                    {

                        foreach (var item in vacationData)
                        {
                            DateTime startDate1 = Convert.ToDateTime(item.fromDate);
                            DateTime endDate1 = Convert.ToDateTime(item.toDate);
                            vacationTakenDays = vacationTakenDays + Convert.ToInt32(((endDate1 - startDate1).TotalDays) + 1);
                        }
                        if (vacationTakenDays <= numberOfdays * repeatPerService)
                        {
                            if (vacationTakenDays == numberOfdays * repeatPerService)
                            {
                                isRulePass = false;
                            }
                            else
                            {
                                totalVacationDay = appliedVacationDay + vacationTakenDays;
                                if (totalVacationDay <= numberOfdays * repeatPerService)
                                    isRulePass = true;
                                else
                                    isRulePass = false;
                            }
                        }
                        else
                        {
                            isRulePass = false;
                        }
                        if (!isRulePass)
                            ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "No of leave days are greater than number of days allowed for selected leave type";
                    }
                    else
                    {
                        if (repeatPerService <= vacationData.Count())
                        {
                            isRulePass = false;
                        }
                        if (!isRulePass)
                            ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "This leave can't be save because max. limit for per service is " + repeatPerService + "";
                        isAnnualLimitExceeded = true;
                    }
                }
            }
            return isRulePass;

        }

        public bool CheckDatesinBetweenAlreadyTaken(int vacationTypeId, int employeeId, int annualVacation, int perServiceVacation, string sStartDate, string sEndDate)
        {
            bool isRulePass = false;
            int mode = 0;
            VacationDB vacationDb = new VacationDB();
            DateTime startDate = DateTime.ParseExact(sStartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(sEndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (annualVacation == 1 && perServiceVacation == 0)
            {
                mode = 1;
            }
            if (annualVacation == 0 && perServiceVacation == 1)
            {
                mode = 2;
            }
            IEnumerable<VacationModel> vacationData = vacationDb.GetVacationForAnnualAndPerservice(mode, employeeId, vacationTypeId);
            if (vacationData.Count() == 0)
            {
                isRulePass = true;
            }
            else
            {
                foreach (var item in vacationData)
                {
                    DateTime startDate1 = Convert.ToDateTime(item.fromDate);
                    DateTime endDate1 = Convert.ToDateTime(item.toDate);
                    if (startDate >= startDate1 && endDate <= endDate1)
                    {
                        isRulePass = false;
                        ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Selected leave (Dates) are already taken by employee.";
                        break;
                    }
                    else
                    {
                        isRulePass = true;
                    }
                }

            }
            return isRulePass;
        }

        public bool CheckDepartmentRule(List<int> vacationDepartmentIds, int employeedepartmentId)
        {
            if (vacationDepartmentIds.Count() > 0)
            {
                bool isDepartmentRulePass = vacationDepartmentIds.Contains(employeedepartmentId);
                if (!isDepartmentRulePass)
                    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee department is not eligible for selected leave type";
                return isDepartmentRulePass;
            }
            else
            {
                return true;
            }
        }

        public bool CheckJobcategoryRule(List<int> vacationJobcategoryIds, int employeeJobcategoryId)
        {
            if (vacationJobcategoryIds.Count() > 0)
            {
                bool isJobCategoryRulePass = vacationJobcategoryIds.Contains(employeeJobcategoryId);
                if (!isJobCategoryRulePass)
                    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee job category is not eligible for selected leave type";
                return isJobCategoryRulePass;
            }
            else
            {
                return true;
            }
        }

        public bool CheckJobTitleRule(List<int> vacationJobTitleIds, int employeeJobTitleId)
        {
            if (vacationJobTitleIds.Count() > 0)
            {
                bool isJobTitleRulePass = vacationJobTitleIds.Contains(employeeJobTitleId);
                if (!isJobTitleRulePass)
                    ruleMessage = ruleMessage + (ruleMessage.Length > 0 ? specingType : "") + (++messageCounter) + ": " + "Employee job title is not eligible for selected leave type";
                return isJobTitleRulePass;
            }
            else
            {
                return true;
            }
        }
        public ActionResult AddDeductionForVacation(string modeType, int typeId, string fromDate, string toDate, int vacationTypeId)
        {
            int mode = 0;
            switch (modeType)
            {
                case "shift":
                    mode = 1;
                    specingType = "\n";
                    break;
                case "department":
                    mode = 2;
                    specingType = "\n";
                    break;
                case "section":
                    mode = 3;
                    specingType = "\n";
                    break;
                case "employee":
                    mode = 4;
                    specingType = "<br>";
                    break;
            }
            DeductionDB deductionDB = new DeductionDB();
            AbsentDeductionDB absentDeduction = new AbsentDeductionDB();
            vacationdb = new VacationDB();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DeductionController deductionController = new DeductionController();
            IEnumerable<VactionEmoloyeeInformationModel> employeeInformationList = vacationdb.GetEmployeeInformationForVacation(mode, typeId);
            List<multipleDeduction> multipleDeductionDetails = new List<multipleDeduction>();
            multipleDeduction deductionDetails;
            OperationDetails oDetails = new OperationDetails();
            DateTime vacationFromDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime vacationToDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime currentdate = new DateTime();
            int noOfCycledays = 30;
            int vacationDays = (int)((vacationToDate - vacationFromDate).TotalDays + 1);
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            string comments = "";
            //Beam Changes
            float leavesTaken = 0;
            VacationTypeModel vacationModel = vacationdb.GetVacationTypeById(vacationTypeId);
            DateTime hireDate;
           // int isAnnual = 0;
            float halfPaidDays = 0, fullPaidDays = 0, halfPaidLeaveBalanceDays = 0, fullPaidLeaveBalanceDays = 0,totalNoOfDays = 0,
                    unPaidDedutionDays = 0,halfPaidDeductionDays = 0,leaveDays=0, totalBalanceDays = 0;
            decimal unPaidDeductionAmount = 0, halfPaidDeductionAmount = 0;
            totalNoOfDays = vacationModel.TotalNoOfDays;
            halfPaidDays = vacationModel.HalfPaidDays;
            fullPaidDays = vacationModel.FullPaidDays;

            foreach (var item in employeeInformationList)
            {
                if (item.hiredate != "")
                {
                    if (Convert.ToDateTime(item.hiredate) < vacationFromDate)
                    {                      
                        comments  = "Leave Deductions " + fromDate + " to " + toDate;
                        hireDate = Convert.ToDateTime(item.hiredate);
                        //if (vacationModel.isAnualLeave)
                        //    isAnnual = 1;
                        //else
                        //    isAnnual = 0;
                        leavesTaken = new VacationDB().GetAlreadyTakenLeaveDays(item.employeeId, vacationTypeId, hireDate, vacationModel.AnnualLeave);
                        //Finding the fullpaid, halfpaid and unpaid balance
                        if(leavesTaken>= totalNoOfDays || leavesTaken >= (halfPaidDays + fullPaidDays))
                        {
                            halfPaidLeaveBalanceDays = 0;
                            fullPaidLeaveBalanceDays = 0;
                        }
                        else if(leavesTaken>= fullPaidDays)
                        {
                            fullPaidLeaveBalanceDays = 0;
                            float balance = leavesTaken - fullPaidDays;
                            if (balance >= halfPaidDays)
                            {
                                halfPaidLeaveBalanceDays = 0;
                            }
                            else
                            {
                                halfPaidLeaveBalanceDays = halfPaidDays - balance;
                            }
                           
                        }
                        else
                        {
                            fullPaidLeaveBalanceDays = fullPaidDays - leavesTaken;
                          //  float balance = leavesTaken - fullPaidDays;
                            halfPaidLeaveBalanceDays = halfPaidDays;
                        }
                        leaveDays = (float)vacationDays;
                        totalBalanceDays = fullPaidLeaveBalanceDays + halfPaidLeaveBalanceDays;                      
                        if(leaveDays>= fullPaidLeaveBalanceDays)
                        {
                            leaveDays = leaveDays - fullPaidLeaveBalanceDays;
                        }
                        else
                        {
                            halfPaidDeductionDays = 0;
                            unPaidDedutionDays = 0;
                        }
                        if(leaveDays>=halfPaidLeaveBalanceDays)
                        {
                            halfPaidDeductionDays = halfPaidLeaveBalanceDays;
                            leaveDays = leaveDays - halfPaidLeaveBalanceDays;
                            if(halfPaidDeductionDays>0)
                                 comments = comments + " ;Half Paid Days : " + halfPaidDeductionDays.ToString();
                        }
                        else
                        {
                            halfPaidDeductionDays = leaveDays;
                            unPaidDedutionDays = 0;
                            leaveDays = 0;
                            if (halfPaidDeductionDays > 0)
                                comments = comments + " ;Half Paid Days : " + halfPaidDeductionDays.ToString();
                        }
                        if (leaveDays >0)
                        {
                            unPaidDedutionDays = leaveDays;
                            comments = comments + " ;Un Paid Days : " + unPaidDedutionDays.ToString();
                        }
                        if (halfPaidDeductionDays > 0 || unPaidDedutionDays > 0)
                        {
                            deductionDetails = new multipleDeduction();
                            deductionDetails.employeeId = item.employeeId;
                            if(unPaidDedutionDays>0)
                                 unPaidDeductionAmount = Convert.ToDecimal(((deductionController.CalculateAmountForDays(item.employeeId, 1.ToString(), fromDate, 2, out currentdate, out noOfCycledays)) * unPaidDedutionDays).ToString(AmountFormat));
                            if (halfPaidDeductionDays > 0)
                                halfPaidDeductionAmount = Convert.ToDecimal(((deductionController.CalculateAmountForDays(item.employeeId, 1.ToString(), fromDate, 2, out currentdate, out noOfCycledays)) * halfPaidDeductionDays).ToString(AmountFormat));
                            halfPaidDeductionAmount = halfPaidDeductionAmount / 2;
                            deductionDetails.amount = unPaidDeductionAmount + halfPaidDeductionAmount;
                                //Convert.ToDecimal(((deductionController.CalculateAmountForDays(item.employeeId, 1.ToString(), fromDate, 2, out currentdate, out noOfCycledays)) * vacationDays).ToString(AmountFormat));
                            int StartMonth = vacationFromDate.Month;
                            int EndMonth = vacationToDate.Month;
                            int noofMonths = EndMonth - StartMonth;
                            deductionDetails.cycle = noofMonths == 0 ? 1 : (noofMonths + 1);
                            deductionDetails.comments = comments;
                            multipleDeductionDetails.Add(deductionDetails);
                        }

                        #region "Old code b4 beam"
                        //deductionDetails = new multipleDeduction();
                        //deductionDetails.employeeId = item.employeeId;                       
                        //deductionDetails.amount = Convert.ToDecimal(((deductionController.CalculateAmountForDays(item.employeeId, 1.ToString(), fromDate, 2, out currentdate, out noOfCycledays)) * vacationDays).ToString(AmountFormat));
                        //int StartMonth = vacationFromDate.Month;
                        //int EndMonth= vacationToDate.Month; 
                        //int noofMonths = EndMonth - StartMonth;                       
                        //deductionDetails.cycle = noofMonths==0?1: (noofMonths+1);
                        //deductionDetails.comments = comments;
                        //multipleDeductionDetails.Add(deductionDetails);
                        #endregion
                    }
                }
            }
            if (multipleDeductionDetails.Count > 0)
            {
                if (multipleDeductionDetails.Count(x => x.amount != 0) > 0)
                {
                    if (!payrollDb.GetPayrollGeneralSetting().IsconfirmRunPayroll || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                    {
                        deductionDB.AddMultipleDeduction(absentDeduction.GetDeductionSetting().AbsentDeductionTypeId, fromDate, "", comments, multipleDeductionDetails, objUserContextViewModel.UserId, true);
                    }
                    else
                    {
                        deductionDB.AddMultipleDeduction(absentDeduction.GetDeductionSetting().AbsentDeductionTypeId, fromDate, "", comments, multipleDeductionDetails, objUserContextViewModel.UserId, false);
                    }
                    oDetails.Success = true;
                    oDetails.Message = "";
                    oDetails.CssClass = "success";
                    return Json(oDetails, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    oDetails.Message = "Vacation deductable Salary allowances are zero. Please update salary allowances.";
                    oDetails.CssClass = "error";
                    return Json(oDetails, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                oDetails.Success = true;
                oDetails.Message = "Record save successfully";
                oDetails.CssClass = "success";
                return Json(oDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult loadVacationTypePerEmployeePosition(int EmployeeID)
        {
            var vacationList = new VacationDB().GetVactionTypeListAsPerEmployeeId(EmployeeID);
            return Json(vacationList.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult loadAllVacations()
        {
            var vacationList = new VacationDB().GetVactionTypeList();
            return Json(vacationList.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult checkEmployeeHiredate(string toDate,int empId)
        {
          bool isHiredateLess = false;
          EmployeeDB objEmployeeDB = new EmployeeDB();
          List<EmploymentInformation> lstEmployeeList = objEmployeeDB.GetEmployeeHireDateInformation();
          EmploymentInformation EI = new EmploymentInformation();
          if (lstEmployeeList.Where(m => m.EmployeeID == empId).ToList().Count > 0)
          {
                EI = lstEmployeeList.Where(m => m.EmployeeID == empId).FirstOrDefault();
                DateTime dt1 = new DateTime();
                DateTime dt2 = new DateTime();
                dt1 = Convert.ToDateTime(EI.HireDate); 
                dt2 = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (dt1<=dt2)
                {
                    isHiredateLess = true;
                }
                else {
                    isHiredateLess = false;
                }
          }
            return Json(isHiredateLess, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadEmployeePopover()
        {
            
            DepartmentDB departmentDB = new DepartmentDB();
            EmployeeDB employeeDB = new EmployeeDB();            
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();            
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.empddl = new SelectList(employeeDB.GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");            
            ViewBag.deptddl = new SelectList(departmentDB.GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentId", "DepartmentName_1");
            List<Employee> lstEmployee = employeeDB.GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1).ToList();
            return PartialView(lstEmployee);
        }

        public ActionResult GetDepartmentEmployeeList(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int DepartmentID = Id;
            int CompanyID = objUserContextViewModel.CompanyId;
            DepartmentEmployeeViewModel viewModel = new DepartmentEmployeeViewModel();
            List<EmployeeDetails> EmpList = new List<EmployeeDetails>();
            DepartmentEmployeeDB objDepartmentDB = new DepartmentEmployeeDB();
            EmpList = (from items in objDepartmentDB.GetEmployeeByDepartmentId(Id, CompanyID)
                                                select new EmployeeDetails
                                                {
                                                    EmployeeID = items.EmployeeID,
                                                    FirstName_1 = items.FirstName_1,
                                                    SurName_1 = items.SurName_1,
                                                    FullName = items.FullName,
                                                    isLeader = items.isLeader,
                                                    isActive = items.isActive
                                                }).Where(m => m.isActive == true).OrderBy(m => m.FullName).ToList();
            return PartialView("DepartmentEmployeeList", EmpList);
        }
        public JsonResult GetVacationBalanceDetails(int leaveTypeID,int employeeID)
        {
            VacationTypeModel vacationTypeModel = new VacationDB().GetVacationTypeById(leaveTypeID);
            float halfPaidDays = 0, fullPaidDays = 0, unPaidDays = 0, halfPaidLeaveBalanceDays = 0, fullPaidLeaveBalanceDays = 0,totalNoOfDays = 0, leavesTaken = 0, unpaidLeaveBalanceDays =0;        
           // int isAnnual = 0;
            vacationdb = new VacationDB();
            if (employeeID > 0)
            {
                if (vacationTypeModel.isAccumulatedLeave)
                {
                    totalNoOfDays = vacationdb.GetAcumulatedLeavesofEmployeeByVacationType(employeeID, vacationTypeModel.VacationTypeId);
                    fullPaidDays = totalNoOfDays;
                    halfPaidDays = 0;
                    leavesTaken = 0;
                }
                else
                {

                    fullPaidDays = vacationTypeModel.FullPaidDays;
                    halfPaidDays = vacationTypeModel.HalfPaidDays;
                    unPaidDays = vacationTypeModel.UnPaidDays;
                    totalNoOfDays = fullPaidDays + halfPaidDays + unPaidDays;
                    //if (vacationTypeModel.isAnualLeave)
                    //    isAnnual = 1;
                    //else
                    //    isAnnual = 0;
                    leavesTaken = vacationdb.GetAlreadyTakenLeaveDays(employeeID, leaveTypeID, vacationTypeModel.AnnualLeave);
                }

                //Finding the fullpaid, halfpaid and unpaid balance
                if (leavesTaken >= totalNoOfDays || leavesTaken >= (halfPaidDays + fullPaidDays))
                {
                    halfPaidLeaveBalanceDays = 0;
                    fullPaidLeaveBalanceDays = 0;
                    unpaidLeaveBalanceDays = unPaidDays - (leavesTaken - (halfPaidDays + fullPaidDays));
                }
                else if (leavesTaken >= fullPaidDays)
                {
                    fullPaidLeaveBalanceDays = 0;
                    float balance = leavesTaken - fullPaidDays;
                    if(balance>=halfPaidDays)
                    {
                        halfPaidLeaveBalanceDays = 0;
                    }
                    else
                    {
                        halfPaidLeaveBalanceDays = halfPaidDays - balance;
                    }                    
                    unpaidLeaveBalanceDays = unPaidDays;
                }
                else
                {
                    fullPaidLeaveBalanceDays = fullPaidDays - leavesTaken;
                    //  float balance = leavesTaken - fullPaidDays;
                    halfPaidLeaveBalanceDays = halfPaidDays;
                    unpaidLeaveBalanceDays = unPaidDays;
                }
                vacationTypeModel.FullPaidDays = (int)fullPaidLeaveBalanceDays;
                vacationTypeModel.HalfPaidDays = (int)halfPaidLeaveBalanceDays;
                vacationTypeModel.UnPaidDays = (int)unpaidLeaveBalanceDays;
            }

            return Json(vacationTypeModel, JsonRequestBehavior.AllowGet);
        }
     

    }
}