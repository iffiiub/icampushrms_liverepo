﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class SeparationRequestFormController : FormsController
    {

        SeparationRequestFormDB separationRequestFormDB;
        SeparationRequestFormModel separationRequestFormModel;
        RecruitR1BudgetedViewModel recruitR1BudgetedViewModel;
        HiringR1ReplacementDB hiringR1ReplacementDB;
        public SeparationRequestFormController()
        {
            //  XMLLogFile = "LoggerHiringRequisitionR1BudgetedForm.xml";
            separationRequestFormDB = new SeparationRequestFormDB();
            hiringR1ReplacementDB = new HiringR1ReplacementDB();
        }
        // GET: SeparationRequestForm
        public ActionResult Index()
        {
            return View();
        }

        // GET: SeparationRequestForm/Create
        public ActionResult Create()
        {
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int id = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.Employee = new SelectList(separationRequestFormDB.GetAllEmployeeForSeparationRequest(true, objUserContextViewModel.UserId), "EmployeeId", "FullName");
            ViewBag.CompanyID = objUserContextViewModel.CompanyId;
            ViewBag.SeparationType = GetSeparationTypes(true, 0);
            SeparationRequestFormModel separationRequestFormModel = new SeparationRequestFormModel();
            separationRequestFormModel.ID = 0;
            separationRequestFormModel.AllFormsFilesModelList = new List<AllFormsFilesModel>();
            separationRequestFormModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            separationRequestFormModel.CreatedOn = DateTime.Now.Date;
            separationRequestFormModel.FormProcessID = 0;
            separationRequestFormModel.IsAddMode = true;
            ViewBag.FormProcessID = null;
            List<Entities.General.OtherPermissions> permissionList = GetPermissionList();
            bool isEditCompletedFormRequest = permissionList.Where(x => x.OtherPermissionId == 42).Count() > 0 ? true : false;
            ViewBag.PermissionList = permissionList;
            ViewBag.IsEditRequestFromAllRequests = isEditRequestFromAllRequests;
            return View(separationRequestFormModel);
        }

        public ActionResult Edit()
        {
            AllFormsFilesModel allFormsFilesModel;
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<Entities.General.OtherPermissions> permissionList = GetPermissionList();
            bool isEditFormRequest = permissionList.Where(x => x.OtherPermissionId == 42).Count() > 0 ? true : false;
            ViewBag.PermissionList = permissionList;
            int id = GetFormProcessSessionID();           
            if (id > 0)
            {
                //get current/pending approver group and employee
                RequestFormsApproveModel requestFormsApproveModel = separationRequestFormDB.GetPendingFormsApproval(id);
                separationRequestFormModel = separationRequestFormDB.GetForm(id, null, objUserContextViewModel.UserId);
                requestFormsApproveModelList = separationRequestFormDB.GetAllRequestFormsApprovals(id);
                separationRequestFormModel.RequestFormsApproveModelList = requestFormsApproveModelList;
                if (separationRequestFormModel.SupportingDocFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(separationRequestFormModel.SupportingDocFileID);
                    allFormsFilesModel.FormFileIDName = "SupportingDocFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);

                }
                separationRequestFormModel.AllFormsFilesModelList = allFormsFilesModelList;
                //if login user is the present approver then can see form in edit mode or is admin user
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                     || (separationRequestFormModel.ReqStatusID == (int)RequestStatus.Rejected && separationRequestFormModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {

                    if (separationRequestFormModel.EmployeeID > 0)
                    {
                        recruitR1BudgetedViewModel = GetRecruitFormDetails(separationRequestFormModel.EmployeeID);
                        separationRequestFormModel.JobGrade = recruitR1BudgetedViewModel.JobGradeName;
                        separationRequestFormModel.Project = recruitR1BudgetedViewModel.RecruitR1BudgetedModel.ProjectData;
                    }
                    ViewBag.RequestID = separationRequestFormModel.RequestID;
                    //If form is pending then only show for edit
                    if (separationRequestFormModel.ReqStatusID == (int)RequestStatus.Pending || (separationRequestFormModel.ReqStatusID == (int)RequestStatus.Rejected && separationRequestFormModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                    {
                        ViewBag.Employee = new SelectList(new EmployeeDB().GetALLEmployeeByUserId(0).Where(x => x.IsActive == 1), "EmployeeId", "FullName", separationRequestFormModel.EmployeeID);
                        ViewBag.FormProcessID = separationRequestFormModel.FormProcessID;
                        ViewBag.SeparationRequestFormID = separationRequestFormModel.ID;
                        ViewBag.CompanyID = separationRequestFormModel.CompanyID;
                        ViewBag.SeparationType = GetSeparationTypes(true, separationRequestFormModel.SeparationTypeID);                       
                        return View("Create", separationRequestFormModel);
                    }
                    else
                        return RedirectToAction("ViewDetails");

                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");

        }
        public ActionResult UpdateDetails()
        {
            AllFormsFilesModel allFormsFilesModel;
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<Entities.General.OtherPermissions> permissionList = GetPermissionList();
            bool isEditFormRequest = permissionList.Where(x => x.OtherPermissionId == 42).Count() > 0 ? true : false;
            ViewBag.PermissionList = permissionList;
            int FormProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            if (FormProcessID > 0)
            {
                RequestFormsApproveModel requestFormsApproveModel = separationRequestFormDB.GetPendingFormsApproval(FormProcessID);
                separationRequestFormModel = separationRequestFormDB.GetForm(FormProcessID, null, objUserContextViewModel.UserId);
                requestFormsApproveModelList = separationRequestFormDB.GetAllRequestFormsApprovals(FormProcessID);
                separationRequestFormModel.RequestFormsApproveModelList = requestFormsApproveModelList;
                if (separationRequestFormModel.SupportingDocFileID > 0)
                {
                    allFormsFilesModel = new AllFormsFilesModel();
                    allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(separationRequestFormModel.SupportingDocFileID);
                    allFormsFilesModel.FormFileIDName = "SupportingDocFileID";
                    allFormsFilesModelList.Add(allFormsFilesModel);
                }
                separationRequestFormModel.AllFormsFilesModelList = allFormsFilesModelList;
                //if login user is the present approver then can see form in edit mode or is admin user
                if (separationRequestFormModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    if (separationRequestFormModel.EmployeeID > 0)
                    {
                        recruitR1BudgetedViewModel = GetRecruitFormDetails(separationRequestFormModel.EmployeeID);
                        separationRequestFormModel.JobGrade = recruitR1BudgetedViewModel.JobGradeName;
                        separationRequestFormModel.Project = recruitR1BudgetedViewModel.RecruitR1BudgetedModel.ProjectData;
                    }
                    ViewBag.RequestID = separationRequestFormModel.RequestID;
                    ViewBag.Employee = new SelectList(new EmployeeDB().GetALLEmployeeByUserId(0).Where(x => x.IsActive == 1), "EmployeeId", "FullName", separationRequestFormModel.EmployeeID);
                    ViewBag.FormProcessID = separationRequestFormModel.FormProcessID;
                    ViewBag.SeparationRequestFormID = separationRequestFormModel.ID;
                    ViewBag.CompanyID = separationRequestFormModel.CompanyID;
                    ViewBag.SeparationType = GetSeparationTypes(true, separationRequestFormModel.SeparationTypeID);
                    return View(separationRequestFormModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");

        }
        [HttpPost]
        public ActionResult SaveForm(string separationequestformModel, HttpPostedFileBase SupportingDocFile)
        {
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            OperationDetails operationDetails = new OperationDetails();
            // HttpPostedFileBase fileSD;
            string formProcessIDs = "";
            int result = 0;
            int formprocessid = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];          
            SeparationRequestFormModel ObjSeparationRequestFormModel = new SeparationRequestFormModel();
            try
            {
                if (!string.IsNullOrEmpty(separationequestformModel))
                {
                    ObjSeparationRequestFormModel = Newtonsoft.Json.JsonConvert.DeserializeObject<SeparationRequestFormModel>(separationequestformModel);
                    ObjSeparationRequestFormModel.CompanyID = objUserContextViewModel.CompanyId;
                    if (ObjSeparationRequestFormModel.ID == 0)
                        ObjSeparationRequestFormModel.SupportingDocFileID = 0;

                    ObjSeparationRequestFormModel.ModifiedBy = objUserContextViewModel.UserId;
                    ObjSeparationRequestFormModel.RequesterEmployeeID = objUserContextViewModel.UserId;

                    AllFormsFilesModel ObjFile;
                    List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();

                    if (SupportingDocFile != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile.FileName = SupportingDocFile.FileName;
                        ObjFile.FileContentType = SupportingDocFile.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(SupportingDocFile);
                        ObjFile.FormFileIDName = "SupportingDocFileID";
                        uploadFileList.Add(ObjFile);
                        ObjSeparationRequestFormModel.SupportingDocFileID = 0;
                    }
                    ObjSeparationRequestFormModel.AllFormsFilesModelList = uploadFileList;

                    if (ObjSeparationRequestFormModel.ID > 0)
                    {
                        // ObjSeparationRequestFormModel.ReqStatusID = Convert.ToInt16(data["hdnReqStatusID"]);
                        formprocessid = ObjSeparationRequestFormModel.FormProcessID;
                        RequestFormsApproveModel requestFormsApproveModel = separationRequestFormDB.GetPendingFormsApproval(formprocessid);
                        if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                             || (ObjSeparationRequestFormModel.ReqStatusID == (int)RequestStatus.Rejected && ObjSeparationRequestFormModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                             || isEditRequestFromAllRequests)
                        {
                            ObjSeparationRequestFormModel.FormProcessID = formprocessid;
                            result = separationRequestFormDB.UpdateForm(ObjSeparationRequestFormModel);
                        }
                        else
                        {
                            operationDetails.Success = true;
                            operationDetails.Message = "No permissions to update.";
                            operationDetails.CssClass = "error";
                            return Json(operationDetails, JsonRequestBehavior.AllowGet);
                        }
                        if (result > 0)
                        {
                            if (ObjSeparationRequestFormModel.ReqStatusID == (int)RequestStatus.Rejected && ObjSeparationRequestFormModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                            {
                                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                                requestFormsApproverEmailModelList = separationRequestFormDB.GetApproverEmailList(Convert.ToString(formprocessid), ObjSeparationRequestFormModel.RequesterEmployeeID);
                                SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                            }
                            operationDetails.InsertedRowId = result;
                            operationDetails.Success = true;
                            operationDetails.Message = "Request updated successfully.";
                            operationDetails.CssClass = "success";
                        }
                        else
                        {
                            operationDetails.Success = false;
                            operationDetails.Message = "Error occured while updating Details";
                            operationDetails.CssClass = "error";
                        }
                    }
                    else
                    {
                        formprocessid = 0;
                        requestFormsProcessModel = separationRequestFormDB.SaveForm(ObjSeparationRequestFormModel);
                        if (requestFormsProcessModel != null)
                        {
                            formProcessIDs = requestFormsProcessModel.FormProcessID.ToString();
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = separationRequestFormDB.GetApproverEmailList(Convert.ToString(formProcessIDs), objUserContextViewModel.UserId);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);

                            operationDetails.InsertedRowId = 1;
                            operationDetails.Success = true;
                            operationDetails.Message = "Request generated successfully.";
                            operationDetails.CssClass = "success";
                        }
                        else
                        {
                            operationDetails.Success = false;
                            operationDetails.Message = "Error occured while Saving Details";
                            operationDetails.CssClass = "error";

                        }

                    }
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while Saving Details";
                    operationDetails.CssClass = "error";
                }
            }


            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = ex.Message;
                operationDetails.CssClass = "error";
                //operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            AllFormsFilesModel allFormsFilesModel;
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            List<AllFormsFilesModel> allFormsFilesModelList = new List<AllFormsFilesModel>();
            List<RequestFormsApproveModel> requestFormsApproveModelList = new List<RequestFormsApproveModel>();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            separationRequestFormModel = new SeparationRequestFormModel();
            RecruitR1BudgetedViewModel recruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            separationRequestFormModel = separationRequestFormDB.GetForm(formProcessID, null, objUserContextViewModel.UserId);
            requestFormsApproveModelList = separationRequestFormDB.GetAllRequestFormsApprovals(formProcessID);
            if (requestFormsApproveModelList != null && requestFormsApproveModelList.Count > 0)
                separationRequestFormModel.RequestFormsApproveModelList = requestFormsApproveModelList;
            if (separationRequestFormModel.SupportingDocFileID > 0)
            {
                allFormsFilesModel = new AllFormsFilesModel();
                allFormsFilesModel = allFormsFilesDB.GetAllFormsFiles(separationRequestFormModel.SupportingDocFileID);
                allFormsFilesModel.FormFileIDName = "SupportingDocFileID";
                allFormsFilesModelList.Add(allFormsFilesModel);

            }
            separationRequestFormModel.AllFormsFilesModelList = allFormsFilesModelList;
            separationRequestFormModel.FormProcessID = formProcessID;
            return View("ViewDetails", separationRequestFormModel);
        }
        [HttpGet]
        public ActionResult GetForm(int? formProcessID, int? employeeID)
        {
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            separationRequestFormModel = new SeparationRequestFormModel();
            RecruitR1BudgetedViewModel recruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            separationRequestFormModel = separationRequestFormDB.GetForm(formProcessID, employeeID, objUserContextViewModel.UserId);
            if (employeeID != null)
            {
                recruitR1BudgetedViewModel = GetRecruitFormDetails(employeeID);
                separationRequestFormModel.JobGrade = recruitR1BudgetedViewModel.JobGradeName;
                separationRequestFormModel.Project = recruitR1BudgetedViewModel.RecruitR1BudgetedModel.ProjectData;
            }
            return Json(separationRequestFormModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFormsUniqueKey(Int16? formID, Int16? separationTypeID, Int16? leaveTypeID, Int16? certificateTypeID)
        {
            return Json(new FormsUniqueKeyDB().GetFormsUniqueKey(formID, separationTypeID, leaveTypeID, certificateTypeID), JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetSeparationTypes(bool? IsActive, Int16 selectedID)
        {
            List<SelectListItem> separationTypesList = new List<SelectListItem>();
            separationTypesList.Add(new SelectListItem { Text = "Select Separation Type", Value = "" });
            foreach (var m in separationRequestFormDB.GetAllSeparationRequestTypes(true))
            {
                separationTypesList.Add(new SelectListItem { Text = m.SeparationTypeName_1.ToString(), Value = m.SeparationTypeID.ToString(), Selected = m.SeparationTypeID == selectedID ? true : false });
            }

            return separationTypesList;
        }

        public List<SelectListItem> GetSeparationRequestedEmployees(int userID)
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem { Text = "Select Employee", Value = "" });
            foreach (var m in separationRequestFormDB.GetAllSeparationRequestedEmployee(userID))
            {
                selectlist.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });
            }

            return selectlist;
        }

        [HttpPost]
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormsDB formsDB = new FormsDB();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //if ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }

                        //getting separation request details
                        separationRequestFormModel = separationRequestFormDB.GetForm(formProcessID, null, 0);
                        //if InitiateHiringReplacement is true, have to genarate request for hiringreplacement
                        if (separationRequestFormModel.InitiateHiringReplacement)
                        {
                            HiringR1ReplacementModel hiringR1ReplacementModel = new HiringR1ReplacementModel();
                            hiringR1ReplacementModel = hiringR1ReplacementDB.GetR1ReplacementDetails(separationRequestFormModel.EmployeeID);
                            hiringR1ReplacementModel.SepartionFormID = separationRequestFormModel.ID;
                            hiringR1ReplacementModel.ReplacedEmployeeID = separationRequestFormModel.EmployeeID;
                            hiringR1ReplacementModel.HeadCount = 1;
                            requestFormsProcessModel = hiringR1ReplacementDB.SaveFormOnSeparationFinalApproval(hiringR1ReplacementModel);
                            requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = hiringR1ReplacementDB.GetApproverEmailList(requestFormsProcessModel.FormProcessID.ToString(), objUserContextViewModel.UserId);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                        }
                        if (separationRequestFormModel.SeparationTypeID == 1)
                        {
                            ExitInterviewRequestDB exitInterviewRequestDB = new ExitInterviewRequestDB();
                            separationRequestFormModel.RequesterEmployeeID = objUserContextViewModel.UserId;
                            exitInterviewRequestDB.CreateExitInterviewRequest(separationRequestFormModel);
                        }

                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}
