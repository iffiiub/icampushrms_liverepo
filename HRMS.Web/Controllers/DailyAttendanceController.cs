﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;
using System.Text.RegularExpressions;

namespace HRMS.Web.Controllers
{
    public class DailyAttendanceController : BaseController
    {
        public ActionResult Index()
        {
            DateTime now = DateTime.Now;
            int dayOfWeek = (int)now.DayOfWeek;
            DateTime fdayOfWeek = now.AddDays(-dayOfWeek);
            DateTime edayOfWeek = fdayOfWeek.AddDays(6);


            PayCycleDB objPayCycleDB = new PayCycleDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            ViewBag.PayCycle = new SelectList(objPayCycleDB.GetAllPayCycle(), "PayCycleID", "PayCycleName_1");
            //ViewBag.DeptList = new SelectList(objDepartmentDB.GetDepartmentList(), "DepartmentId", "DepartmentName_1");

            List<Employee> Emplist = new List<Entities.Employee>();
            List<Employee> newList = new List<Entities.Employee>();
            int UserId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserId = objUserContextViewModel.UserId;
            UserGroupDB objUserGroupDB = new UserGroupDB();
            //ViewBag.DeptList = new SelectList(objDepartmentDB.GetDepartmentListByUserId(UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.DeptList = new SelectList(new List<DepartmentModel>(), "DepartmentId", "DepartmentName_1");
            AttendenceSetupDB objAttendenceSetupDB = new AttendenceSetupDB();
            ViewBag.GetAttPostingSetting = objAttendenceSetupDB.GetAttPostingSetting();
            UserRoleDB userRoleDB = new UserRoleDB();
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;

            //ViewBag.EmployeeList = new SelectList(objEmployeeDB.GetALLEmployeeList(), "EmployeeId", "FullName");
            //ViewBag.EmployeeList = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName");
            ViewBag.EmployeeList = new SelectList(new List<Employee>(), "EmployeeId", "FullName");
            ViewBag.SectionList = new SelectList(objEmployeeSectionDB.GetSectionListByUserId(UserId), "EmployeeSectionID", "EmployeeSectionName_1");
            ViewBag.AbsenType = new AbsentTypeDB().GetAbsentType(1);
            ViewBag.Company = GetCompanySelectList();
            //-----------IsPersonalOnly permission---------
            bool IsPersonalOnly = true;
            if (UserId != 0)
            {
                List<HRMS.Entities.UserGroupModel> listUserGroupModel = new List<HRMS.Entities.UserGroupModel>();
                listUserGroupModel = new HRMS.DataAccess.UserGroupDB().GetAllUserGroupsByUserId(UserId);
                int IsPersonalCount = listUserGroupModel.Where(x => x.UserListAccessID == 1).ToList().Count();
                int IsNotPersonalCount = listUserGroupModel.Where(x => x.UserListAccessID > 1).ToList().Count();
                if (IsPersonalCount > 0 && IsNotPersonalCount == 0)
                {
                    IsPersonalOnly = false;
                }
            }
            ViewBag.IsPersonalOnly = IsPersonalOnly;
            //--------------------------------------------------

            return View();
        }

        public ActionResult GetCyclePayInfo(int PayCycleId)
        {
            PayCycleDB objPayCycleDB = new PayCycleDB();
            PayCycleModel objPayCycleModel = new PayCycleModel();
            objPayCycleModel = objPayCycleDB.GetAllPayCycle().Where(x => x.PayCycleID == PayCycleId).Select(x => x).SingleOrDefault();
            return Json(objPayCycleModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDailyAttendanceViewList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status",
            int iSortCol_0 = 0, string FromDate = "", string ToDate = "", string DepID = "", string statusid = "", string empid = "", string sectionid = "")
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            bool? Proceed = null, active = null;

            int? employeeId = null, statusId = null, aIntDepartmentID = null, aIntSectionID = null, aIntShiftID = null, UserId = null;
            if (!string.IsNullOrEmpty(empid))
                employeeId = Convert.ToInt32(empid);
            if (!string.IsNullOrEmpty(statusid))
                statusId = Convert.ToInt32(statusid);
            if (!string.IsNullOrEmpty(sectionid))
                aIntSectionID = Convert.ToInt32(sectionid);
            if (!string.IsNullOrEmpty(DepID))
                aIntDepartmentID = Convert.ToInt32(DepID);

            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();

            List<EmploymentInformation> employeeHirDatelist = objEmployeeDB.GetEmployeeHireDateInformation();

            if (objUserContextViewModel.UserId != 0)
            {
                UserId = objUserContextViewModel.UserId;
            }

            objDailyAttendanceViewModelList = objDailyAttendanceViewDB.GetDailyAttendanceViewList(FromDate, ToDate, employeeId, statusId, Proceed, active,
                aIntDepartmentID, aIntSectionID, aIntShiftID, UserId).OrderBy(x => x.EmployeeName).ToList();

            objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.HRID != 0).ToList();
            Session["FromDate"] = FromDate;
            Session["ToDate"] = ToDate;
            Session["DailyAttendanceList"] = objDailyAttendanceViewModelList;

            #region Sorting




            //---------------------------------------------
            //  DepID = "1";
            if (!string.IsNullOrEmpty(DepID))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.DeptID == Convert.ToInt32(DepID)).ToList();
            }




            if (!string.IsNullOrEmpty(empid))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.HRID == Convert.ToInt32(empid)).ToList();
            }

            switch (iSortCol_0)
            {
                case 0:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.HRAltID).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.HRAltID).ToList();
                    }
                    break;

                case 1:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.EmployeeName).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.EmployeeName).ToList();
                    }
                    break;

                case 2:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.DepName).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.DepName).ToList();
                    }
                    break;
                case 3:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.AttDate).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.AttDate).ToList();
                    }
                    break;
                case 4:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.InTime).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.InTime).ToList();
                    }
                    break;

                case 5:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.OutTime).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.OutTime).ToList();
                    }
                    break;
                case 6:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.Status).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.Status).ToList();
                    }
                    break;
                //case 6:
                //    if (sSortDir_0 == "asc")
                //    {
                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.HrHireDate).ToList();
                //    }
                //    else
                //    {

                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.HrHireDate).ToList();
                //    }
                //    break;
                //case 7:
                //    if (sSortDir_0 == "asc")
                //    {
                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.HrInductionDate).ToList();
                //    }
                //    else
                //    {

                //        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.HrInductionDate).ToList();
                //    }
                //    break;
                case 7:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.EarlyMinutes).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.EarlyMinutes).ToList();
                    }
                    break;
                case 8:
                    if (sSortDir_0 == "asc")
                    {
                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderBy(x => x.LateMinutes).ToList();
                    }
                    else
                    {

                        objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.LateMinutes).ToList();
                    }
                    break;


                default:

                    objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.OrderByDescending(x => x.EmployeeName).ToList();
                    break;


                    //              { "mData": "EmpoyeeName", "sTitle": "Name" },

                    // { "mData": "Status", "sTitle": "Status" },


                    //{ "mData": "DepName", "sTitle": "Department Name" },
                    //  { "mData": "AttDate", "sTitle": "Date" },
                    //{ "mData": "InTime", "sTitle": "In Time" },
                    //{ "mData": "OutTime", "sTitle": "Out Time" },
                    // { "mData": "HrHireDate", "sTitle": "Hire Date" },
                    //{ "mData": "HrInductionDate", "sTitle": "Induction Date" },

                    ////{ "mData": "HolidayName", "sTitle": "Holiday Name" },
                    ////{ "mData": "HolidayDate", "sTitle": "Holiday Date" },
                    //{ "mData": "LateMinute", "sTitle": "Late Minute" },
                    //{ "mData": "earlyMinute", "sTitle": "Early Minute" },



            }
            #endregion

            string[] a = new string[3];
            string[] s = new string[13];
            if (objDailyAttendanceViewModelList.Count > 0)
            {
                for (int i = 0; i <= 11; i++)
                {
                    s[i] = objDailyAttendanceViewModelList.Where(x => x.StatusID == i).Count().ToString();

                }
                a[0] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 2 && x.Proceed == false).Count().ToString();
                a[1] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 3 && x.Proceed == false).Count().ToString();
                a[2] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 4 && x.Proceed == false).Count().ToString();
            }
            else
            {
                a[0] = "0";
                a[1] = "0";
                a[2] = "0";
            }
            s[12] = (objDailyAttendanceViewModelList.Where(x => x.StatusID == 1).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 3).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 4).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 8).Count() + objDailyAttendanceViewModelList.Where(x => x.StatusID == 9).Count()).ToString();
            Session["RecordsNotPosted"] = a;
            Session["test"] = s;

            // statusid = "7";
            if (!string.IsNullOrEmpty(statusid))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.StatusID == Convert.ToInt32(statusid)).ToList();
            }
            if (!string.IsNullOrEmpty(sectionid))
            {
                objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Where(x => x.SectionID == Convert.ToInt32(sectionid)).ToList();
            }
            int Count = objDailyAttendanceViewModelList.Count;
            int pageindex = pageNumber + 1;
            //objDailyAttendanceViewModelList = objDailyAttendanceViewModelList.Skip((pageindex - 1) * numberOfRecords).Take(numberOfRecords).ToList();
            string checkBoxItem = "<input type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'ChkcheckHRAltID' name = 'checkHRAltID'/>";

            checkBoxItem = "<input type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'ChkcheckHRAltID' name = 'checkHRAltID'/>";

            var vList = new object();

            vList = new
            {
                aaData = (from item in objDailyAttendanceViewModelList
                          select new
                          {
                              //Action = "<a class='btn btn-primary' onclick='EditDailyAttendanceView(" + item.DailyAttendanceViewID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> Edit</a><a class='btn btn-primary' onclick='DeleteDailyAttendanceView(" + item.DailyAttendanceViewID.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
                              //  CheckRow = "<input type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'Chkcheck" + item.HRAltID + "' name = 'check" + item.HRAltID + "'/>",
                              CheckRow = checkBoxItem.Replace("HRAltID", item.HRAltID),
                              HRAltID = item.HRAltID,
                              AttDate = item.AttDate,
                              EmpoyeeName = item.EmployeeName,
                              HrHireDate = item.HrHireDate,
                              HrInductionDate = item.HrInductionDate,
                              Status = item.Status,
                              StatusID = item.StatusID,
                              DeptID = item.DeptID,
                              DepName = item.DepName,
                              InTime = item.InTime,
                              OutTime = item.OutTime,
                              //HolidayName = item.HolidayName,
                              //HolidayDate = item.HolidayDate,
                              LateMinutes = item.LateMinutes,
                              EarlyMinutes = item.EarlyMinutes,

                              PayEmployeeAbsentID = item.PayEmployeeAbsentID,
                              PayEmployeeLateID = item.PayEmployeeLateID,
                              absentExcused = item.absentExcused,
                              LateExcused = item.LateExcused,
                              earlyExcused = item.earlyExcused,
                              Proceed = item.Proceed,
                              IsActive = item.IsActive,
                              HRID = item.HRID,
                              SectionName = item.SectionName,
                              SectionID = item.SectionID,
                              totalHrs = item.totalHrs,
                              shiftHrs = item.shiftHrs,
                              overHrs = item.overHrs,
                              shortHrs = item.shortHrs,
                              IsHireDateIsGreater = objEmployeeDB.checkHireDateIsGreater(DateTime.ParseExact(item.AttDate, "dd/MM/yyyy", CultureInfo.InvariantCulture), employeeHirDatelist.FirstOrDefault(x => x.EmployeeID == item.HRID).HireDate)
                          }).ToArray(),
                recordsTotal = Count,
                recordsFiltered = Count
            };

            //return Json(vList, JsonRequestBehavior.AllowGet);

            var serializer = new JavaScriptSerializer();

            // For simplicity just use Int32's max value.
            // You could always read the value from the config section mentioned above.
            serializer.MaxJsonLength = Int32.MaxValue;

            var resultData = new { Value = "foo", Text = "var" };
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;

        }




        public ActionResult GetDailyAttendanceStatusCount(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, string FromDate = "", string ToDate = "", string DepID = "", string statusid = "", string empid = "")
        {
            var list = Session["test"];
            Session.Remove("test");
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetRecordsNotPostedCount()
        {



            var list = Session["RecordsNotPosted"];

            Session.Remove("RecordsNotPosted");



            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /*
        public ActionResult GetRecordsNotPostedCountAfterPosting()
        {

            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();
            //objDailyAttendanceViewModelList = (List < DailyAttendanceViewModel >) Session["DailyAttendanceList"];

            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();

            string FromDate = Session["FromDate"].ToString();
            string ToDate = Session["ToDate"].ToString();

            objDailyAttendanceViewModelList = objDailyAttendanceViewDB.GetDailyAttendanceViewList(FromDate, ToDate, 1);








            string[] a = new string[3];

            if (objDailyAttendanceViewModelList.Count > 0)
            {

                a[0] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 2 && x.Proceed == false).Count().ToString();
                a[1] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 3 && x.Proceed == false).Count().ToString();
                a[2] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 4 && x.Proceed == false).Count().ToString();

            }
            else
            {



                a[0] = "0";
                a[1] = "0";
                a[2] = "0";


            }

            var list = a;
            return Json(list, JsonRequestBehavior.AllowGet);

        }
        */
       
        public int[] GetDailyAttendanceEmployeeCount(string FromDate = "", string ToDate = "")
        {
            DBHelper objDBHelper = new DBHelper();


            //-------------Data Objects--------------------
            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();

            //UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
            //EmployeeDB objEmployeeDB = new EmployeeDB();
            //objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objDailyAttendanceViewModelList = objDailyAttendanceViewDB.GetDailyAttendanceViewList(FromDate, ToDate, null);
            //---------------------------------------------
            //  DepID = "1";



            int[] s = new int[12];
            for (int i = 0; i <= 11; i++)
            {
                s[i] = objDailyAttendanceViewModelList.Where(x => x.StatusID == i).Count();

            }




            int Count = objDailyAttendanceViewModelList.Count;

            return s;



        }

        public ActionResult AddDailyAttendanceView()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Absent", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "Present", Value = "2" });

            ViewBag.AttendanceCategory = ObjSelectedList;
            ViewBag.Employee = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");
            ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            PayLateTypeDB objPayLateTypeDB = new PayLateTypeDB();
            ViewBag.PayLateType = new SelectList(objPayLateTypeDB.GetPayLateTypeList(), "PayLateTypeID", "PayLateTypeName_1");

            return View();
        }

        [HttpPost]
        public ActionResult AddDailyAttendanceView(DailyAttendanceViewModel objDailyAttendanceViewModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DateTime date = DateTime.ParseExact(objDailyAttendanceViewModel.PayEmployeeAbsentDate, HRMSDateFormat, CultureInfo.InvariantCulture);
            DateTime inTimeDate = Convert.ToDateTime(objDailyAttendanceViewModel.InTime);
            DateTime outTimeDate = Convert.ToDateTime(objDailyAttendanceViewModel.OutTime);
            objDailyAttendanceViewModel.InTime = (new DateTime(date.Year, date.Month, date.Day, inTimeDate.Hour, inTimeDate.Minute, inTimeDate.Second)).ToString();
            objDailyAttendanceViewModel.OutTime = (new DateTime(date.Year, date.Month, date.Day, outTimeDate.Hour, outTimeDate.Minute, outTimeDate.Second)).ToString();
            if (objDailyAttendanceViewModel.AttendanceCategory == 1 || objDailyAttendanceViewModel.AttendanceCategory == 6 || objDailyAttendanceViewModel.AttendanceCategory == 7 || objDailyAttendanceViewModel.AttendanceCategory == 8)
            {
                if (objDailyAttendanceViewModel.AttendanceCategory == 1)
                {
                    objDailyAttendanceViewModel.StatusID = 2;
                }
                PayEmployeeAbsentHeaderDB objPayEmployeeAbsentHeaderDB = new PayEmployeeAbsentHeaderDB();
                PayEmployeeAbsentHeaderModel objPayEmployeeAbsentHeaderModel = new PayEmployeeAbsentHeaderModel();
                objPayEmployeeAbsentHeaderModel.EmployeeID = objDailyAttendanceViewModel.EmployeeID;
                objPayEmployeeAbsentHeaderModel.PayAbsentTypeID = objDailyAttendanceViewModel.PayAbsentTypeID;
                objPayEmployeeAbsentHeaderModel.EntryDate = DateTime.Now.ToString();
                objPayEmployeeAbsentHeaderModel.Active = true;
                objDailyAttendanceViewModel.PayEmployeeAbsentHeaderID = objPayEmployeeAbsentHeaderDB.InsertPayEmployeeAbsentHeader(objPayEmployeeAbsentHeaderModel).InsertedRowId;
                AbsentDB objAbsentDB = new AbsentDB();
                if (objAbsentDB.GetAbsentList(objDailyAttendanceViewModel.EmployeeID).Where(x => x.PayEmployeeAbsentFromDate == objDailyAttendanceViewModel.PayEmployeeAbsentDate).Select(x => x).ToList().Count() == 0)
                {
                    AbsentModel objAbsentModel = new AbsentModel();
                    objAbsentModel.EmployeeID = objDailyAttendanceViewModel.EmployeeID;
                    objAbsentModel.PayEmployeeAbsentFromDate = objDailyAttendanceViewModel.PayEmployeeAbsentDate;
                    objAbsentModel.PayEmployeeAbsentToDate = objDailyAttendanceViewModel.PayEmployeeAbsentDate;
                    objAbsentModel.AbsentPaidTypeID = objDailyAttendanceViewModel.LeaveTypeID;
                    objAbsentModel.InTime = objDailyAttendanceViewModel.InTime;
                    objAbsentModel.OutTime = objDailyAttendanceViewModel.OutTime;
                    objAbsentModel.PayEmployeeAbsentHeaderID = objDailyAttendanceViewModel.PayEmployeeAbsentHeaderID;
                    objAbsentModel.PayAbsentTypeID = objDailyAttendanceViewModel.PayAbsentTypeID;
                    objAbsentModel.StatusID = objDailyAttendanceViewModel.StatusID;
                    objAbsentDB.InsertAbsent(objAbsentModel);
                }
            }
            else if (objDailyAttendanceViewModel.AttendanceCategory == 2 || objDailyAttendanceViewModel.AttendanceCategory == 3 || objDailyAttendanceViewModel.AttendanceCategory == 4 || objDailyAttendanceViewModel.AttendanceCategory == 5)
            {
                AbsentLateInEarlyOutDB objAbsentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
                if (objAbsentLateInEarlyOutDB.GetAbsentLateInEarlyOutListByID(objDailyAttendanceViewModel.EmployeeID).Where(x => x.PayEmployeeLateDate == objDailyAttendanceViewModel.PayEmployeeAbsentDate).Select(x => x).ToList().Count() == 0)
                {
                    AbsentLateInEarlyOutModel objAbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
                    objAbsentLateInEarlyOutModel.EmployeeID = objDailyAttendanceViewModel.EmployeeID;
                    objAbsentLateInEarlyOutModel.PayEmployeeLateDate = objDailyAttendanceViewModel.PayEmployeeAbsentDate;
                    objAbsentLateInEarlyOutModel.PayLateTypeID = objDailyAttendanceViewModel.PayLateTypeID;
                    objAbsentLateInEarlyOutModel.InTime = objDailyAttendanceViewModel.InTime;
                    objAbsentLateInEarlyOutModel.OutTime = objDailyAttendanceViewModel.OutTime;
                    int LateMinutes = 0;
                    int EarlyMinutes = 0;
                    try
                    {
                        LateMinutes = Convert.ToInt32(objDailyAttendanceViewModel.LateMinutes);
                        EarlyMinutes = Convert.ToInt32(objDailyAttendanceViewModel.EarlyMinutes);
                    }
                    catch (Exception ex)
                    {
                    }
                    objAbsentLateInEarlyOutModel.LateMinutes = LateMinutes;
                    objAbsentLateInEarlyOutModel.EarlyMinutes = EarlyMinutes;
                    objAbsentLateInEarlyOutModel.IsManual = true;
                    objAbsentLateInEarlyOutModel.LateReason = objDailyAttendanceViewModel.LateReason;
                    objAbsentLateInEarlyOutDB.InsertAbsentLateInEarlyOut(objAbsentLateInEarlyOutModel);
                }
            }
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails.Message = "Record saved successfully.";
            objOperationDetails.Success = true;
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult EditDailyAttendanceView(int id)
        //{
        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
        //    EmployeeDB objEmployeeDB = new EmployeeDB();
        //    HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
        //    CountryDB objCountryDB = new CountryDB();
        //    CityDB objCityDB = new CityDB();

        //    ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

        //    ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");

        //    ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
        //    DailyAttendanceViewDB objDailyAttendanceViewModelDB = new DailyAttendanceViewDB();
        //    DailyAttendanceViewModel objDailyAttendanceViewModel = new DailyAttendanceViewModel();
        //    objDailyAttendanceViewModel = objDailyAttendanceViewModelDB.DailyAttendanceViewById(id);
        //    ViewBag.City = new SelectList(objCityDB.GetCityByCountryId(objDailyAttendanceViewModel.CountryID.ToString()), "CityId", "CityName");
        //    return View(objDailyAttendanceViewModel);
        //}

        //[HttpPost]
        //public JsonResult EditDailyAttendanceView(DailyAttendanceViewModel objDailyAttendanceViewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //        objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //        objDailyAttendanceViewModel.EmployeeID = objUserContextViewModel.UserId;
        //        DailyAttendanceViewDB objDailyAttendanceViewModelDB = new DailyAttendanceViewDB();
        //        OperationDetails objOperationDetails = new OperationDetails();
        //        objOperationDetails = objDailyAttendanceViewModelDB.UpdateDailyAttendanceView(objDailyAttendanceViewModel);
        //        return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //        objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //        DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
        //        EmployeeDB objEmployeeDB = new EmployeeDB();
        //        HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
        //        CountryDB objCountryDB = new CountryDB();
        //        CityDB objCityDB = new CityDB();

        //        ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

        //        ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
        //        List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
        //        ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
        //        ViewBag.CityList = ObjSelectedList;
        //        ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
        //        OperationDetails objOperationDetails = new OperationDetails();
        //        objOperationDetails.Success = false;
        //        objOperationDetails.Message = "Please fill all the fields carefully.";
        //        return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult ViewDailyAttendanceView(int id)
        //{
        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
        //    EmployeeDB objEmployeeDB = new EmployeeDB();
        //    HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
        //    CountryDB objCountryDB = new CountryDB();
        //    CityDB objCityDB = new CityDB();

        //    ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

        //    ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
        //    List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
        //    ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
        //    ViewBag.CityList = ObjSelectedList;
        //    ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
        //    DailyAttendanceViewDB objDailyAttendanceViewModelDB = new DailyAttendanceViewDB();
        //    DailyAttendanceViewModel objDailyAttendanceViewModel = new DailyAttendanceViewModel();
        //    objDailyAttendanceViewModel = objDailyAttendanceViewModelDB.DailyAttendanceViewById(id);
        //    return View(objDailyAttendanceViewModel);
        //}

        //public ActionResult DeleteDailyAttendanceView(int id)
        //{
        //    DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
        //    objDailyAttendanceViewDB.DeleteDailyAttendanceViewById(id);
        //    return Json(0, JsonRequestBehavior.AllowGet);
        //}


        [HttpPost]
        public ActionResult PushRecords(List<DailyAttendanceViewModel> listDailyAttendanceViewModel)
        {
            EmployeeDB employeeDb = new EmployeeDB();
            List<EmploymentInformation> employeeHirDatelist = employeeDb.GetEmployeeHireDateInformation();
            AttendenceSetupDB objAttendenceSetupDB = new AttendenceSetupDB();
            AttPostingSetting attPostingSetting = objAttendenceSetupDB.GetAttPostingSetting();
            int LateDidntSignOutId = attPostingSetting.LateAndDidntSignOutPosting;
            string strErrorId = string.Empty;
            int EmployeeID = 0;
            bool checkHireDate = false;
            for (int i = 0; i < listDailyAttendanceViewModel.Count; i++)
            {

                try
                {
                    EmployeeID = listDailyAttendanceViewModel[i].HRID;
                    int statusid = listDailyAttendanceViewModel[i].StatusID;
                    if (statusid == 12)
                    {
                        statusid = LateDidntSignOutId;
                    }
                    if (employeeHirDatelist.FirstOrDefault(x => x.EmployeeID == EmployeeID).HireDate != "")
                    {
                        DateTime attdate = Convert.ToDateTime(CommonDB.SetCulturedDate(listDailyAttendanceViewModel[i].AttDate));
                        checkHireDate = employeeDb.checkHireDateIsGreater(attdate, employeeHirDatelist.FirstOrDefault(x => x.EmployeeID == EmployeeID).HireDate);
                    }
                    else
                        checkHireDate = false;

                    if (!checkHireDate)
                    {
                        if (statusid == 2 || statusid == 9 || statusid == 11 || statusid == 12)
                        {
                            AbsentDB objAbsentDB = new AbsentDB();
                            string Absentdate = listDailyAttendanceViewModel[i].AttDate;
                            DateTime AttendanceDate = Convert.ToDateTime(CommonDB.SetCulturedDate(Absentdate));
                            string AttendanceDateOnly = AttendanceDate.ToShortDateString();
                            int EmployeeExist = objAbsentDB.CheckEmployeeAbsentByDate(EmployeeID, Absentdate);
                            if (EmployeeExist < 1)
                            {
                                int AbsentTypeID = objAbsentDB.GetAbsentType();
                                //Employee IS Absent
                                OperationDetails objOperationDetails = new OperationDetails();
                                AbsentModel objAbsentModel = new Entities.AbsentModel();
                                //PAss PArameter
                                objAbsentModel.PayEmployeeAbsentHeaderID = listDailyAttendanceViewModel[i].PayEmployeeAbsentHeaderID; //not there
                                objAbsentModel.EmployeeID = listDailyAttendanceViewModel[i].HRID;
                                objAbsentModel.PayAbsentTypeID = AbsentTypeID; //AbsentTypeID DOne
                                objAbsentModel.EntryDate = DateTime.Now.ToString("dd/MM/yyyy");
                                objAbsentModel.isActive = true; //listDailyAttendanceViewModel[i].IsActive;
                                objOperationDetails = objAbsentDB.InsertAbsentHeader(objAbsentModel);
                                if (objOperationDetails.Success)
                                {
                                    if (listDailyAttendanceViewModel[i].LeaveTypeID == 0)
                                    {
                                        listDailyAttendanceViewModel[i].LeaveTypeID = 1;
                                    }
                                    listDailyAttendanceViewModel[i].PayEmployeeAbsentHeaderID = objOperationDetails.InsertedRowId;
                                    AbsentModel objAbsentModel1 = new Entities.AbsentModel();
                                    //PAss PArameter
                                    objAbsentModel1.PayEmployeeAbsentHeaderID = listDailyAttendanceViewModel[i].PayEmployeeAbsentHeaderID;
                                    objAbsentModel1.EmployeeID = listDailyAttendanceViewModel[i].HRID;
                                    objAbsentModel1.PayEmployeeAbsentFromDate = listDailyAttendanceViewModel[i].AttDate;
                                    objAbsentModel1.PayEmployeeAbsentToDate = listDailyAttendanceViewModel[i].AttDate;
                                    objAbsentModel1.PayAbsentTypeID = AbsentTypeID;
                                    objAbsentModel1.AbsentPaidTypeID = listDailyAttendanceViewModel[i].LeaveTypeID;
                                    objAbsentModel1.IsMedicalCertificateProvided = listDailyAttendanceViewModel[i].IsMedicalCertificateProvided;
                                    objAbsentModel1.IsDeducted = listDailyAttendanceViewModel[i].IsDeducted;
                                    objAbsentModel1.StatusID = listDailyAttendanceViewModel[i].StatusID;
                                    objAbsentModel1.InTime = AttendanceDateOnly + " " + listDailyAttendanceViewModel[i].InTime;
                                    objAbsentModel1.OutTime = AttendanceDateOnly + " " + listDailyAttendanceViewModel[i].OutTime;
                                    objOperationDetails = objAbsentDB.InsertAbsent(objAbsentModel1);
                                }

                            }
                        }
                        if (statusid == 3 || statusid == 4 || statusid == 8 || statusid == 12)
                        {
                            AbsentDB objAbsentDB = new AbsentDB();
                            string LAteDate = listDailyAttendanceViewModel[i].AttDate;
                            DateTime AttendanceDate = Convert.ToDateTime(CommonDB.SetCulturedDate(LAteDate));
                            string AttendanceDateOnly = AttendanceDate.ToShortDateString();
                            int EmployeeExist = objAbsentDB.CheckEmployeeLateByDate(EmployeeID, LAteDate);
                            //  EmployeeExist = 1;
                            if (EmployeeExist < 1)
                            {
                                AbsentLateInEarlyOutModel objAbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
                                AbsentLateInEarlyOutDB objAbsentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
                                OperationDetails objOperationDetails = new OperationDetails();
                                objAbsentLateInEarlyOutModel.EmployeeID = listDailyAttendanceViewModel[i].HRID;
                                objAbsentLateInEarlyOutModel.PayEmployeeLateDate = LAteDate;// listDailyAttendanceViewModel[i].PayEmployeeLateDate;
                                objAbsentLateInEarlyOutModel.IsExcused = listDailyAttendanceViewModel[i].IsExcused;
                                DateTime inTimeDate = Convert.ToDateTime(listDailyAttendanceViewModel[i].InTime);
                                DateTime outTimeDate = Convert.ToDateTime(listDailyAttendanceViewModel[i].OutTime);
                                objAbsentLateInEarlyOutModel.InTime = (new DateTime(AttendanceDate.Year, AttendanceDate.Month, AttendanceDate.Day, inTimeDate.Hour, inTimeDate.Minute, inTimeDate.Second)).ToString();
                                objAbsentLateInEarlyOutModel.OutTime = (new DateTime(AttendanceDate.Year, AttendanceDate.Month, AttendanceDate.Day, outTimeDate.Hour, outTimeDate.Minute, outTimeDate.Second)).ToString();
                                //objAbsentLateInEarlyOutModel.InTime = DateTime.ParseExact(AttendanceDateOnly + " " + listDailyAttendanceViewModel[i].InTime, "dd/MM/yyyy hh:mm", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy h:mm:ss tt"); // AttendanceDateOnly + " " + listDailyAttendanceViewModel[i].InTime;
                                // objAbsentLateInEarlyOutModel.OutTime = DateTime.ParseExact(AttendanceDateOnly + " " + listDailyAttendanceViewModel[i].OutTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy hh:mm tt"); //AttendanceDateOnly + " " + listDailyAttendanceViewModel[i].OutTime;
                                objAbsentLateInEarlyOutModel.LateMinutes = listDailyAttendanceViewModel[i].LateMinutes;
                                objAbsentLateInEarlyOutModel.EarlyMinutes = listDailyAttendanceViewModel[i].EarlyMinutes;
                                objAbsentLateInEarlyOutModel.IsDeducted = listDailyAttendanceViewModel[i].IsDeducted; //not there  
                                objAbsentLateInEarlyOutModel.statusID = statusid;
                                objAbsentLateInEarlyOutModel.LateReason = "";
                                objOperationDetails = objAbsentLateInEarlyOutDB.InsertAbsentLateInEarlyOut(objAbsentLateInEarlyOutModel);
                            }
                        }
                    }
                }
                catch (Exception EX)
                {
                    strErrorId += EmployeeID.ToString();
                }
            }
            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();
            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
            string FromDate = Session["FromDate"].ToString();
            string ToDate = Session["ToDate"].ToString();
            objDailyAttendanceViewModelList = objDailyAttendanceViewDB.GetDailyAttendanceViewList(FromDate, ToDate, null);

            string[] a = new string[3];
            if (objDailyAttendanceViewModelList.Count > 0)
            {
                a[0] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 2 && x.Proceed == false).Count().ToString();
                a[1] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 3 && x.Proceed == false).Count().ToString();
                a[2] = objDailyAttendanceViewModelList.Where(x => x.StatusID == 4 && x.Proceed == false).Count().ToString();
            }
            else
            {
                a[0] = "0";
                a[1] = "0";
                a[2] = "0";
            }
            var list = a;
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PushRecordToDoc(string type, string FromDate = "", string ToDate = "", string DepID = "", string statusid = "", string empid = "", string sectionid = "", string companyId = "")
        {
            if (empid == "0") empid = "";
            if (DepID == "0") DepID = "";
            if (companyId == "0") companyId = "";
            bool? Proceed = null, active = null;
            int? employeeId = null, statusId = null, aIntDepartmentID = null, aIntSectionID = null, aIntShiftID = null, UserId = null, aCompanyId = null;
            if (!string.IsNullOrEmpty(empid) && empid != "0")
                employeeId = Convert.ToInt32(empid);
            if (!string.IsNullOrEmpty(statusid))
                statusId = Convert.ToInt32(statusid);
            if (!string.IsNullOrEmpty(sectionid))
                aIntSectionID = Convert.ToInt32(sectionid);
            if (!string.IsNullOrEmpty(DepID) && DepID != "0")
                aIntDepartmentID = Convert.ToInt32(DepID);
            if (!string.IsNullOrEmpty(companyId) && companyId != "0")
                aCompanyId = Convert.ToInt32(companyId);
            //----------------------------------------------------------------------
            //-------------Data Objects--------------------
            List<DailyAttendanceViewModel> objDailyAttendanceViewModelList = new List<DailyAttendanceViewModel>();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (objUserContextViewModel.UserId != 0)
            {
                UserId = objUserContextViewModel.UserId;
            }
            if (objUserContextViewModel.UserId == 0)
            {
                UserId = null;
            }

            DailyAttendanceViewDB objDailyAttendanceViewDB = new DailyAttendanceViewDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objDailyAttendanceViewModelList = objDailyAttendanceViewDB.GetDailyAttendanceViewList(FromDate, ToDate, employeeId, statusId, Proceed, active,
                aIntDepartmentID, aIntSectionID, aIntShiftID, UserId, aCompanyId).OrderBy(x => x.EmployeeName).ToList();

            List<HRMS.Entities.Employee> sortedEmployeeList = new List<HRMS.Entities.Employee>();
            sortedEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);

            objDailyAttendanceViewModelList = (from Emplist in objDailyAttendanceViewModelList
                                               join UGA in sortedEmployeeList on Emplist.HRID equals UGA.EmployeeId
                                               select Emplist).ToList();

            DataSet ds = new DataSet();
            ds.Tables.Add(objDailyAttendanceViewDB.ExportInDataTable(objDailyAttendanceViewModelList));

            if (type == "Excel")
            {
                byte[] content;
                string fileName = "DailyAttendance_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";
                CompanyDB objCompanyDB = new CompanyDB();
                content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
                return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
            }
            else
            {
                if (type == "Pdf")
                {
                    string fileName = "DailyAttendance_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
                    CompanyDB objCompanyDB = new CompanyDB();
                    MemoryStream workStream = new MemoryStream();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
                    PdfWriter.GetInstance(dc, workStream).CloseStream = false;
                    byte[] byteInfo = workStream.ToArray();
                    workStream.Write(byteInfo, 0, byteInfo.Length);
                    workStream.Position = 0;
                    return new FileContentResult((workStream.ToArray()), "application/pdf");
                }
                else
                {
                    string fileName = "DailyAttendance_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".docx";
                    byte[] content;
                    //int[] widthArray = { 5, 15, 7, 8, 8, 8, 7, 7, 7, 7, 7, 7, 7 };
                    int[] widthArray = { 5, 12, 7, 7, 7, 7, 7, 7, 7, 7, 7, 6, 7, 7 };
                    content = CommonHelper.CommonHelper.CreateWordDoc(ds.Tables[0], widthArray);
                    return new FileContentResult(content, "application/msword") { FileDownloadName = fileName };
                }
            }

        }

        public JsonResult CheckAbsentType(int AbsentTypeId)
        {
            var AbsentType = new AbsentTypeDB().GetAbsentType(AbsentTypeId);
            return Json(new { LeaveTypeName = AbsentType.PaidTypeName, LeaveTypeId = AbsentType.PaidType }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmployeeInLate(int EmpId, string FromDate, string ToDate)
        {
            AbsentLateInEarlyOutDB absentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
            var data = absentLateInEarlyOutDB.GetAbsentLateInEarlyWithEmployeeIds(EmpId.ToString(), FromDate, ToDate);
            if (data.Count > 0)
                return Json(new { isPresent = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { isPresent = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmployeeInAbsent(int EmpID, string FromDate, string ToDate)
        {
            AbsentDB absentDB = new AbsentDB();
            var data = absentDB.GetAbsentList(FromDate, ToDate, EmpID);
            if (data.Count > 0)
                return Json(new { isPresent = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { isPresent = false }, JsonRequestBehavior.AllowGet);
        }
    }
}