﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class EmployeeConfirmationRequestController : FormsController
    {
        EmployeeConfirmationRequestDB objEmployeeConfirmationRequestFromDB;
        // GET: EmployeeConfirmationRequest
        public EmployeeConfirmationRequestController()
        {
            objEmployeeConfirmationRequestFromDB = new EmployeeConfirmationRequestDB();
            XMLLogFile = "LoggerEmployeeConfirmationRequest.xml";
        }
        public ActionResult Edit()
        {
            int formProcessID = GetFormProcessSessionID();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (formProcessID > 0)
            {
                ConfirmationDetailModel confirmationDetailModel = objEmployeeConfirmationRequestFromDB.GetConfirmationDetailByID(formProcessID, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = objEmployeeConfirmationRequestFromDB.GetPendingFormsApproval(formProcessID);
                if (objUserContextViewModel.UserId == 0
                    || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (confirmationDetailModel.ReqStatusID == (int)RequestStatus.Rejected && confirmationDetailModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    if (confirmationDetailModel.ReqStatusID == (int)RequestStatus.Pending
                        || (confirmationDetailModel.ReqStatusID == (int)RequestStatus.Rejected && confirmationDetailModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || confirmationDetailModel.ReqStatusID == (int)RequestStatus.Completed)
                    {
                        AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                        ViewBag.CompanyID = confirmationDetailModel.CompanyID;
                        confirmationDetailModel.OriginalFileName = confirmationDetailModel.OfferLetterFileID > 0 ? allFormsFilesDB.GetAllFormsFiles(confirmationDetailModel.OfferLetterFileID).FileName : "";

                        List<SelectListItem> lstEvaluationType = new SelectList(objEmployeeConfirmationRequestFromDB.GetAllEvaluationType().Where(x => x.id > 0).ToList(), "id", "text").ToList();
                        ViewBag.lstEvaluationType = lstEvaluationType;

                        List<RatingScaleModel> lstRatingScaleModel = objEmployeeConfirmationRequestFromDB.GetAllRatingScale().Where(x => x.RatingScaleID > 0).ToList();
                        ViewBag.lstRatingScaleModel = lstRatingScaleModel;

                        ViewBag.lstRatingScaleForDdl = new SelectList(lstRatingScaleModel, "RatingScaleID", "RatingScaleCode").ToList();

                        List<ConfirmationJobEvaluationModel> lstJobEvaluation = objEmployeeConfirmationRequestFromDB.GetAllJobEvaluation(formProcessID, confirmationDetailModel.RequestInitialize);
                        ViewBag.lstJobEvaluation = lstJobEvaluation;

                        List<ConfirmationCodeOfConductModel> lstCodeOfConduct = objEmployeeConfirmationRequestFromDB.GetAllCodeOfConduct(formProcessID, confirmationDetailModel.RequestInitialize);
                        ViewBag.lstCodeOfConduct = lstCodeOfConduct;

                        List<ConfirmationOthersOptionModel> lstOthersOption = objEmployeeConfirmationRequestFromDB.GetAllConfirmationOthersOption(formProcessID, confirmationDetailModel.RequestInitialize);
                        ViewBag.lstOthersOption = lstOthersOption;

                        List<SelectListItem> extensionProbation = new List<SelectListItem>
                                                          {
                                                            new SelectListItem { Text = "1", Value = "1"},
                                                            new SelectListItem { Text = "2", Value = "2"},
                                                            new SelectListItem { Text = "3", Value = "3"}
                                                          };
                        ViewBag.extensionProbation = extensionProbation;
                        ViewBag.formProcessID = formProcessID;
                        return View(confirmationDetailModel);
                    }
                    else
                        return RedirectToAction("ViewDetails");
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }

            }
            else
                return RedirectToAction("ViewDetails");
        }
        public ActionResult UpdateDetails()
        {
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (formProcessID > 0)
            {
                ConfirmationDetailModel confirmationDetailModel = objEmployeeConfirmationRequestFromDB.GetConfirmationDetailByID(formProcessID, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = objEmployeeConfirmationRequestFromDB.GetPendingFormsApproval(formProcessID);
                if (confirmationDetailModel.ReqStatusID != (int)RequestStatus.Rejected || isEditRequestFromAllRequests)
                {
                    AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
                    ViewBag.CompanyID = confirmationDetailModel.CompanyID;
                    confirmationDetailModel.OriginalFileName = confirmationDetailModel.OfferLetterFileID > 0 ? allFormsFilesDB.GetAllFormsFiles(confirmationDetailModel.OfferLetterFileID).FileName : "";

                    List<SelectListItem> lstEvaluationType = new SelectList(objEmployeeConfirmationRequestFromDB.GetAllEvaluationType().Where(x => x.id > 0).ToList(), "id", "text").ToList();
                    ViewBag.lstEvaluationType = lstEvaluationType;

                    List<RatingScaleModel> lstRatingScaleModel = objEmployeeConfirmationRequestFromDB.GetAllRatingScale().Where(x => x.RatingScaleID > 0).ToList();
                    ViewBag.lstRatingScaleModel = lstRatingScaleModel;

                    ViewBag.lstRatingScaleForDdl = new SelectList(lstRatingScaleModel, "RatingScaleID", "RatingScaleCode").ToList();

                    List<ConfirmationJobEvaluationModel> lstJobEvaluation = objEmployeeConfirmationRequestFromDB.GetAllJobEvaluation(formProcessID, confirmationDetailModel.RequestInitialize);
                    ViewBag.lstJobEvaluation = lstJobEvaluation;

                    List<ConfirmationCodeOfConductModel> lstCodeOfConduct = objEmployeeConfirmationRequestFromDB.GetAllCodeOfConduct(formProcessID, confirmationDetailModel.RequestInitialize);
                    ViewBag.lstCodeOfConduct = lstCodeOfConduct;

                    List<ConfirmationOthersOptionModel> lstOthersOption = objEmployeeConfirmationRequestFromDB.GetAllConfirmationOthersOption(formProcessID, confirmationDetailModel.RequestInitialize);
                    ViewBag.lstOthersOption = lstOthersOption;

                    List<SelectListItem> extensionProbation = new List<SelectListItem>
                                                          {
                                                            new SelectListItem { Text = "1", Value = "1"},
                                                            new SelectListItem { Text = "2", Value = "2"},
                                                            new SelectListItem { Text = "3", Value = "3"}
                                                          };
                    ViewBag.extensionProbation = extensionProbation;
                    confirmationDetailModel.FormProcessID = formProcessID;
                    return View(confirmationDetailModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("ViewDetails");
        }
        public ActionResult UpdateEmployeeConfirmationRequest(string confirmationDetailModel, HttpPostedFileBase OriginalOfferLetter)
        {
            OperationDetails operationDetails = new OperationDetails();
            AllFormsFilesModel ObjFile;
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
            if (!string.IsNullOrEmpty(confirmationDetailModel))
            {
                ConfirmationDetailModel confirmationDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfirmationDetailModel>(confirmationDetailModel);
                if (OriginalOfferLetter != null)
                {
                    ObjFile = new AllFormsFilesModel();
                    ObjFile.FileID = confirmationDetail.OfferLetterFileID;
                    ObjFile.FileName = OriginalOfferLetter.FileName;
                    ObjFile.FileContentType = OriginalOfferLetter.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(OriginalOfferLetter);
                    ObjFile.FormFileIDName = "";
                    uploadFileList.Add(ObjFile);
                }
                confirmationDetail.AllFormsFilesModelList = uploadFileList;
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                int result = 0;

                result = new EmployeeConfirmationRequestDB().UpdateConfirmationRequest(confirmationDetail, objUserContextViewModel.UserId);
                operationDetails.InsertedRowId = result;
                if (result > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully";
                    operationDetails.CssClass = "success";
                    //if false, request is in intialized state ,next approver must be informed  
                    if (!confirmationDetail.RequestInitialize && confirmationDetail.FormProcessID > 0)
                    {
                        string formProcessIDs = confirmationDetail.FormProcessID.ToString();
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = objEmployeeConfirmationRequestFromDB.GetApproverEmailList(Convert.ToString(formProcessIDs), objUserContextViewModel.UserId);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        operationDetails.Message += @" & Email send to next approver";
                    }

                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating Details";
                    operationDetails.CssClass = "error";
                }
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReInitializeConfirmationRequest(string confirmationDetailModel, HttpPostedFileBase OriginalOfferLetter)
        {
            OperationDetails operationDetails = new OperationDetails();
            AllFormsFilesModel ObjFile;
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            if (!string.IsNullOrEmpty(confirmationDetailModel))
            {
                try
                {
                    ConfirmationDetailModel confirmationDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfirmationDetailModel>(confirmationDetailModel);
                    if (OriginalOfferLetter != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile.FileID = confirmationDetail.OfferLetterFileID;
                        ObjFile.FileName = OriginalOfferLetter.FileName;
                        ObjFile.FileContentType = OriginalOfferLetter.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(OriginalOfferLetter);
                        ObjFile.FormFileIDName = "";
                        uploadFileList.Add(ObjFile);
                    }
                    confirmationDetail.AllFormsFilesModelList = uploadFileList;
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    int result = 0;

                    RequestFormsApproveModel requestFormsApproveModel = objEmployeeConfirmationRequestFromDB.GetPendingFormsApproval(confirmationDetail.FormProcessID);
                    if ((confirmationDetail.ReqStatusID == (int)RequestStatus.Rejected && confirmationDetail.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || isEditRequestFromAllRequests)
                    {
                        result = objEmployeeConfirmationRequestFromDB.ReInitializeConfirmationRequest(confirmationDetail, objUserContextViewModel.UserId);
                    }
                    else
                    {
                        operationDetails.Success = true;
                        operationDetails.Message = "No permissions to update.";
                        operationDetails.CssClass = "error";
                        return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    }
                    if (result > 0)
                    {
                        if (confirmationDetail.ReqStatusID == (int)RequestStatus.Rejected && confirmationDetail.RequesterEmployeeID == objUserContextViewModel.UserId)
                        {
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = objEmployeeConfirmationRequestFromDB.GetApproverEmailList(Convert.ToString(confirmationDetail.FormProcessID), confirmationDetail.RequesterEmployeeID);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        }
                        operationDetails.InsertedRowId = result;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request updated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while updating Details";
                        operationDetails.CssClass = "error";
                    }
                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating details";
                    operationDetails.CssClass = "error";
                    operationDetails.Exception = ex;
                }
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating Details";
                operationDetails.CssClass = "error";
            }

            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            // UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AllFormsFilesDB allFormsFilesDB = new AllFormsFilesDB();
            EmployeeConfirmationRequestDB objEmployeeConfirmationRequestFromDB = new EmployeeConfirmationRequestDB();
            ConfirmationDetailModel confirmationDetailModel = objEmployeeConfirmationRequestFromDB.GetConfirmationDetailByID(formProcessID, objUserContextViewModel.UserId);
            confirmationDetailModel.OriginalFileName = confirmationDetailModel.OfferLetterFileID > 0 ? allFormsFilesDB.GetAllFormsFiles(confirmationDetailModel.OfferLetterFileID).FileName : "";

            List<PickList> lstEvaluationType = new List<PickList>();
            List<RatingScaleModel> lstRatingScaleModel = new List<RatingScaleModel>();
            List<RatingScaleModel> FinalRatingCode = new List<RatingScaleModel>();

            lstEvaluationType = objEmployeeConfirmationRequestFromDB.GetAllEvaluationType().ToList();
            string EvaluationType = "";
            string textFinalRatingCode = "";

            if (lstEvaluationType.Where(x => x.id > 0 && x.id == confirmationDetailModel.TypeEvaluationID).ToList().Count > 0)
            {
                EvaluationType = lstEvaluationType.Where(x => x.id > 0 && x.id == confirmationDetailModel.TypeEvaluationID).FirstOrDefault().text;
            }
            ViewBag.lstEvaluationType = EvaluationType;

            lstRatingScaleModel = objEmployeeConfirmationRequestFromDB.GetAllRatingScale().Where(x => x.RatingScaleID > 0).ToList();
            ViewBag.lstRatingScaleModel = lstRatingScaleModel;

            if (lstRatingScaleModel.Where(x => x.RatingScaleID == confirmationDetailModel.FinalRatingScaleID).ToList().Count > 0)
            {
                textFinalRatingCode = lstRatingScaleModel.Where(x => x.RatingScaleID == confirmationDetailModel.FinalRatingScaleID).FirstOrDefault().RatingScaleCode;
            }
            ViewBag.FinalRatingCode = textFinalRatingCode;

            List<ConfirmationJobEvaluationModel> lstJobEvaluation = objEmployeeConfirmationRequestFromDB.GetAllJobEvaluation(formProcessID, confirmationDetailModel.RequestInitialize);
            ViewBag.lstJobEvaluation = lstJobEvaluation;

            List<ConfirmationCodeOfConductModel> lstCodeOfConduct = objEmployeeConfirmationRequestFromDB.GetAllCodeOfConduct(formProcessID, confirmationDetailModel.RequestInitialize);
            ViewBag.lstCodeOfConduct = lstCodeOfConduct;

            List<ConfirmationOthersOptionModel> lstOthersOption = objEmployeeConfirmationRequestFromDB.GetAllConfirmationOthersOption(formProcessID, confirmationDetailModel.RequestInitialize);
            ViewBag.lstOthersOption = lstOthersOption;

            ViewBag.formProcessID = formProcessID;
            confirmationDetailModel.FormProcessID = formProcessID;

            return View(confirmationDetailModel);
        }

        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }
                        ConfirmationDetailModel confirmationDetailModel = objEmployeeConfirmationRequestFromDB.GetConfirmationDetailByID(formProcessID, objUserContextViewModel.UserId);
                        if (confirmationDetailModel.TypeEvaluationID != 0)
                        {
                            objEmployeeConfirmationRequestFromDB.UpdateProbationCompletionDate(formProcessID);
                        }
                        //Add any logic to be done once approval process is completed
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}