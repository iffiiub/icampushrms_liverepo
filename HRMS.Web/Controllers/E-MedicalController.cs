﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class EMedicalController : BaseController
    {
        public ActionResult Index(int Id = 0)
        {
            Id = CommonHelper.CommonHelper.GetEmployeeId(Id);
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            int EmployID = 0;
            if (Id != 0)
            {
                EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                ViewBag.EmployeeIDNo = Id.ToString();
                ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();

            }
            else
            {
                ViewBag.EmployeeIDNo = 0;
                if (Session["EmployeeListID"] != null)
                {
                    EmployID = Convert.ToInt32(Session["EmployeeListID"]);
                    objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = EmployID.ToString();
                    ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();

                }
                else
                {

                    Id = objEmployeeDB.GetFirstActiveEmployee();


                    Session["EmployeeListID"] = Id.ToString();
                    EmployID = Id;
                    objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = Id.ToString();
                    ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();

                }
            }






            MedicalDB objMedicalDB = new MedicalDB();

            ViewBag.BloodTypeList = new SelectList(objMedicalDB.GetBloodTypeList(), "HRBloodID", "HRBloodName_1");


            MedicalMainModel objMedicalMainModel = new MedicalMainModel();

            if (EmployID > 0)
                objMedicalMainModel = objMedicalDB.GetMedicalMainByEmployee(EmployID);
            objMedicalMainModel.EmployeeID = EmployID;




            return View(objMedicalMainModel);



        }

        public ActionResult GetInsdependenceList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "DESC", string mDataProp_1 = "InsuranceDependence", int iSortCol_0 = 0, int empid = 0)
        {

            if (empid != 0)
            {

                Session["EmployeeListID"] = empid;
            }







            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            switch (iSortCol_0)
            {



                default:
                    sortColumn = "InsuranceDependence";
                    break;
            }


            //-------------Data Objects--------------------
            List<InsdependenceModel> objInsdependenceList = new List<InsdependenceModel>();
            InsuranceDB objInsuranceDB = new InsuranceDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //EmployeeDB objEmployeeDB = new EmployeeDB();

            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objInsdependenceList = objInsuranceDB.GetInsdependenceList(empid);
            //---------------------------------------------


            var vList = new object();

            vList = new
            {
                aaData = (from item in objInsdependenceList
                          select new
                          {


                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditInsuranceDependence(" + item.InsuranceDependenceID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteInsuranceDependence(" + item.InsuranceDependenceID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              InsuranceDependence = item.InsuranceDependenceID,
                              Name = item.Name,
                              Relation = item.Relation,
                              RelationName = item.RelationName,
                              CardNumber = item.CardNumber,
                              Note = item.Note

                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetMedicalAlertList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<MedicalMainModel> objMedicalMainModelList = new List<MedicalMainModel>();
            MedicalDB objMedicalDB = new MedicalDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objMedicalMainModelList = objMedicalDB.GetMedicalAlertsByEmployee(empid);
            var vList = new object();
            vList = new
            {
                aaData = (from item in objMedicalMainModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditMedicalAlert(" + item.EmployeeMedicalAlertID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteMedicalAlert(" + item.EmployeeMedicalAlertID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeMedicalAlertID = item.EmployeeMedicalAlertID,
                              MedicalAlert = item.MedicalAlert,
                              MedicalAlertAr = item.MedicalAlertAr

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInsdependence()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            InsdependenceModel objInsdependenceModel = new InsdependenceModel();
            InsuranceDB objInsuranceDB = new InsuranceDB();
            objInsdependenceModel.EmployeeID = empid;

            ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");

            return View(objInsdependenceModel);
        }


        [HttpPost]
        public ActionResult AddMedicalMain(MedicalMainModel MedicalMainModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    MedicalMainModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                MedicalDB objMedicalDB = new MedicalDB();
                OperationDetails objOperationDetails = new OperationDetails();

                if (MedicalMainModel.EmployeeMedicalMainID > 0)
                {
                    MedicalMainModel.aTntTranMode = 2;
                }
                else
                {
                    MedicalMainModel.aTntTranMode = 1;
                }


                objOperationDetails = objMedicalDB.InsertMedicalMain(MedicalMainModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];


                MedicalMainModel.EmployeeID = empid;

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }



        public ActionResult AddMedicalAlert()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            MedicalMainModel objMedicalMainModel = new MedicalMainModel();

            objMedicalMainModel.EmployeeID = empid;


            return View(objMedicalMainModel);
        }

        [HttpPost]
        public ActionResult AddMedicalAlert(MedicalMainModel objMedicalMainModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objMedicalMainModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                MedicalDB objMedicalDB = new MedicalDB();
                OperationDetails objOperationDetails = new OperationDetails();


                if (objMedicalMainModel.EmployeeMedicalAlertID > 0)
                {
                    objMedicalMainModel.aTntTranMode = 2;
                }
                else
                {
                    objMedicalMainModel.aTntTranMode = 1;

                }


                objOperationDetails = objMedicalDB.InsertMedicalAlert(objMedicalMainModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];


                objMedicalMainModel.EmployeeID = empid;





                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }


        public ActionResult AddMedicalHistory()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            MedicalHistoryModel objMedicalHistoryModel = new MedicalHistoryModel();

            objMedicalHistoryModel.EmployeeID = empid;


            return View(objMedicalHistoryModel);
        }

        [HttpPost]
        public ActionResult AddMedicalHistory(MedicalHistoryModel objMedicalHistoryModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objMedicalHistoryModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                MedicalDB objMedicalDB = new MedicalDB();
                OperationDetails objOperationDetails = new OperationDetails();


                if (objMedicalHistoryModel.EmployeeHealthHistoryID > 0)
                {
                    objMedicalHistoryModel.aTntTranMode = 2;
                }
                else
                {
                    objMedicalHistoryModel.aTntTranMode = 1;

                }


                objOperationDetails = objMedicalDB.InsertMedicalHistory(objMedicalHistoryModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                InsuranceDB objInsuranceDB = new InsuranceDB();
                objMedicalHistoryModel.EmployeeID = empid;





                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }



        public ActionResult AddMedicalVaccination()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            MedicalVaccinationModel objMedicalVaccinationModel = new MedicalVaccinationModel();

            objMedicalVaccinationModel.EmployeeID = empid;
            MedicalDB objMedicalDB = new MedicalDB();

            ViewBag.VaccinationTypeList = new SelectList(objMedicalDB.GetVaccinationTypeList(), "HRVaccinationID", "HRVaccinationName_1");


            return View(objMedicalVaccinationModel);
        }

        [HttpPost]
        public ActionResult AddMedicalVaccination(MedicalVaccinationModel objMedicalVaccinationModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objMedicalVaccinationModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                MedicalDB objMedicalDB = new MedicalDB();
                OperationDetails objOperationDetails = new OperationDetails();


                if (objMedicalVaccinationModel.EmployeeVaccinationID > 0)
                {
                    objMedicalVaccinationModel.aTntTranMode = 2;
                }
                else
                {
                    objMedicalVaccinationModel.aTntTranMode = 1;

                }


                objOperationDetails = objMedicalDB.InsertMedicalVaccination(objMedicalVaccinationModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                InsuranceDB objInsuranceDB = new InsuranceDB();
                objMedicalVaccinationModel.EmployeeID = empid;

                MedicalDB objMedicalDB = new MedicalDB();
                ViewBag.VaccinationTypeList = new SelectList(objMedicalDB.GetVaccinationTypeList(), "HRVaccinationID", "HRVaccinationName_1");


                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }


        public ActionResult EditMedicalAlert(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            MedicalDB objMedicalDB = new MedicalDB();
            MedicalMainModel objMedicalMainModel = new MedicalMainModel();
            objMedicalMainModel = objMedicalDB.GetMedicalAlertsByID(id);

            return View("AddMedicalAlert", objMedicalMainModel);
        }

        public ActionResult EditMedicalVaccination(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            MedicalDB objMedicalDB = new MedicalDB();
            MedicalVaccinationModel objMedicalVaccinationModel = new MedicalVaccinationModel();
            objMedicalVaccinationModel = objMedicalDB.GetMedicalVaccinationModelByID(id);
            //objMedicalVaccinationModel.EmployeeVaccinationDate =

            // Convert.ToDateTime(objMedicalVaccinationModel.EmployeeVaccinationDate).ToShortDateString();



            ViewBag.VaccinationTypeList = new SelectList(objMedicalDB.GetVaccinationTypeList(), "HRVaccinationID", "HRVaccinationName_1");


            return View("AddMedicalVaccination", objMedicalVaccinationModel);
        }


        public ActionResult EditMedicalHistory(int id)
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            MedicalHistoryModel objMedicalHistoryModel = new MedicalHistoryModel();
            MedicalDB objMedicalDB = new MedicalDB();
            objMedicalHistoryModel = objMedicalDB.GetMedicalHistoryModelByID(id);






            return View("AddMedicalHistory", objMedicalHistoryModel);
        }

        public ActionResult GetMedicalHistory(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }

            DBHelper objDBHelper = new DBHelper();
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<MedicalHistoryModel> objMedicalHistoryModelList = new List<MedicalHistoryModel>();
            MedicalDB objMedicalDB = new MedicalDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objMedicalHistoryModelList = objMedicalDB.GetMedicalHistoryModelList(empid);
            //---------------------------------------------
            var vList = new object();
            vList = new
            {
                aaData = (from item in objMedicalHistoryModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHealthHistory(" + item.EmployeeHealthHistoryID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteHealthHistory(" + item.EmployeeHealthHistoryID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeHealthHistoryID = item.EmployeeHealthHistoryID,
                              EmployeeHealthHistoryDate = item.EmployeeHealthHistoryDate,
                              Description = item.Description,
                              Action_Result = item.Action,
                              Note = item.Note
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetMedicalMainList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "DESC", string mDataProp_1 = "EmployeeMedicalMainID", int iSortCol_0 = 0, int empid = 0)
        {

            if (empid != 0)
            {

                Session["EmployeeListID"] = empid;
            }

            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            switch (iSortCol_0)
            {
                default:
                    sortColumn = "EmployeeMedicalMainID";
                    break;
            }


            //-------------Data Objects--------------------
            List<MedicalMainModel> objMedicalAlertsList = new List<MedicalMainModel>();
            MedicalDB objMedicalDB = new MedicalDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //EmployeeDB objEmployeeDB = new EmployeeDB();

            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objMedicalAlertsList = objMedicalDB.GetMedicalAlertsByEmployee(empid);
            //---------------------------------------------


            var vList = new object();

            vList = new
            {
                aaData = (from item in objMedicalAlertsList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditMedicalAlert(" + item.EmployeeMedicalMainID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteMedicalAlert(" + item.EmployeeMedicalMainID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeMedicalMainID = item.EmployeeMedicalMainID,
                              Alert1 = item.Alert1,
                              Alerta1 = item.Alerta1

                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetMedicalVaccination(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<MedicalVaccinationModel> objMedicalVaccinationModelList = new List<MedicalVaccinationModel>();
            MedicalDB objMedicalDB = new MedicalDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objMedicalVaccinationModelList = objMedicalDB.GetMedicalVaccinationModelList(empid);
            var vList = new object();
            vList = new
            {
                aaData = (from item in objMedicalVaccinationModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditMedicalVaccination(" + item.EmployeeVaccinationID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteMedicalVaccination(" + item.EmployeeVaccinationID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeVaccinationID = item.EmployeeVaccinationID,
                              EmployeeVaccinationDate = item.EmployeeVaccinationDate,
                              Vaccination = item.VaccinationName,
                              Administrated = item.Administrated ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.Administrated + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.Administrated + "'/>",
                              Note = item.Note
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddInsurance(InsuranceModel objInsuranceModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objInsuranceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                InsuranceDB objInsuranceDB = new InsuranceDB();
                OperationDetails objOperationDetails = new OperationDetails();





                objOperationDetails = objInsuranceDB.InsertInsurance(objInsuranceModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditInsdependence(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            InsuranceDB objInsuranceDB = new InsuranceDB();
            InsdependenceModel objInsdependenceModel = new InsdependenceModel();
            objInsdependenceModel = objInsuranceDB.GetInsdependenceByID(id);

            ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");


            return View("AddInsdependence", objInsdependenceModel);
        }

        [HttpPost]
        public JsonResult EditInsdependence(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //     objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentModelDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                //if (objAbsentModel.PayEmployeeAbsentFromDate != "")
                //    objAbsentModel.PayEmployeeAbsentFromDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentFromDate).ToShortDateString();
                //if (objAbsentModel.PayEmployeeAbsentToDate != "")
                //    objAbsentModel.PayEmployeeAbsentToDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentToDate).ToShortDateString();
                objOperationDetails = objAbsentModelDB.UpdateAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();




                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;


                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpdateLeaveType(int LeavetypeID, int PayEmployeeAbsentID)
        {



            AbsentDB objAbsentModelDB = new AbsentDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAbsentModelDB.UpdateLeavetypeAbsent(LeavetypeID, PayEmployeeAbsentID);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);


        }

        public ActionResult ViewInsurance(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;

            return View(objAbsentModel);
        }

        public ActionResult DeleteInsdependence(int id)
        {
            InsuranceDB objInsuranceDB = new InsuranceDB();
            objInsuranceDB.DeleteInsdependenceById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }



        public ActionResult DeleteHealthHistory(int id)
        {
            MedicalDB objMedicalDB = new MedicalDB();
            objMedicalDB.DeleteHealthHistory(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteMedicalAlert(int id)
        {
            MedicalDB objMedicalDB = new MedicalDB();
            objMedicalDB.DeleteMedicalAlert(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteMedicalVaccination(int id)
        {
            MedicalDB objMedicalDB = new MedicalDB();
            objMedicalDB.DeleteMedicalVaccination(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }
    }
}