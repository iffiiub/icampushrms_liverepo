﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class EmployeeJoiningFormController : FormsController
    {
        EmployeeJoiningRequestDB joiningRequestDB;
        public EmployeeJoiningFormController()
        {
            joiningRequestDB = new EmployeeJoiningRequestDB();
        }
        // GET: EmployeeJoiningForm
        public ActionResult Index()
        {
            return View("Create");
        }
        public ActionResult Create()
        {
            int id = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(Session["EmployeePendingRequestID"])) ? "0" : Session["EmployeePendingRequestID"]);
            if (id <= 0)
            {
                return RedirectToAction("Index", "Home");
            }
            FormsEmployeeJoiningModel joiningRequestModel = new FormsEmployeeJoiningModel();
            EmployeeJoiningRequestDB RequestDB = new EmployeeJoiningRequestDB();
            joiningRequestModel = RequestDB.GetDetails(id);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            joiningRequestModel.CreatedOn = DateTime.Today;
            joiningRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            ViewBag.CompanyID = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 18;
            joiningRequestModel.JoiningDate = null;

            return View(joiningRequestModel);
        }
        public ActionResult SaveForm()
        {
            FormsEmployeeJoiningModel joiningRequest = new FormsEmployeeJoiningModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HttpPostedFileBase fileOL;
            string formProcessIDs = "";                                   
            try
            {
                var data = Request.Form;
                joiningRequest.ID = Convert.ToInt32(string.IsNullOrEmpty(data["EmployeeJoiningID"]) ? "0" : data["EmployeeJoiningID"]);
                if (joiningRequest.ID <= 0)
                {
                    joiningRequest.EmployeeId = (int)Session["EmployeePendingRequestID"];
                    joiningRequest.JoiningDate = Convert.ToString(data["JoiningDate"]);
                }
                else
                {
                    joiningRequest.EmployeeId = int.Parse(data["EmployeeId"]);
                }

                joiningRequest.ReqStatusID = 1;
                joiningRequest.Comments = Convert.ToString(data["Comments"]);

                if (joiningRequest.ID > 0)
                {
                    joiningRequest.OfferLetterFileID = Convert.ToInt32(string.IsNullOrEmpty(data["OfferLetterFileID"]) ? "0" : data["OfferLetterFileID"]);
                }
                else
                {
                    joiningRequest.OfferLetterFileID = Convert.ToInt32(data["OfferLetterFileID"]);
                }
                joiningRequest.CreatedBy = objUserContextViewModel.UserId;
                joiningRequest.CreatedOn = DateTime.Now;
                joiningRequest.ModifiedBy = objUserContextViewModel.UserId;
                joiningRequest.ModifiedOn = DateTime.Now;
                joiningRequest.RequesterEmployeeID = objUserContextViewModel.UserId;
                joiningRequest.CompanyID = objUserContextViewModel.CompanyId;

                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();

                if (Request.Files["OfferLetterFile"] != null)
                {
                    ObjFile = new AllFormsFilesModel();
                    fileOL = Request.Files["OfferLetterFile"];
                    ObjFile.FileName = fileOL.FileName;
                    ObjFile.FileContentType = fileOL.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileOL);
                    ObjFile.FormFileIDName = "OfferLetterFileID";
                    uploadFileList.Add(ObjFile);
                    joiningRequest.OfferLetterFileID = 0;
                }
                joiningRequest.AllFormsFilesModelList = uploadFileList;
                                
                operationDetails = joiningRequestDB.IsWorkFlowExists(18, joiningRequest.CompanyID.ToString(), objUserContextViewModel.UserId);

                if (operationDetails.InsertedRowId > 0)
                {
                    requestFormsProcessModelList = new EmployeeJoiningRequestDB().SaveForm(joiningRequest);

                    if (requestFormsProcessModelList != null)
                    {
                        formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = new EmployeeJoiningRequestDB().GetApproverEmailList(formProcessIDs, joiningRequest.RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList,false);

                        operationDetails.Success = true;
                        operationDetails.Message = "Request generated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while adding Details";
                        operationDetails.CssClass = "error";
                    }
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit()
        {
            FormsEmployeeJoiningModel joiningRequest = new FormsEmployeeJoiningModel();           
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int formProcessID = GetFormProcessSessionID();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 18;

            if (formProcessID > 0)
            {
                //get current/pending approver group and employee
                RequestFormsApproveModel requestFormsApproveModel = joiningRequestDB.GetPendingFormsApproval(formProcessID);
                RequestFormsProcessModel objRequestFormsProcessModel = joiningRequestDB.GetRequestFormsProcess(formProcessID);
                joiningRequest = joiningRequestDB.GetForm(formProcessID, objRequestFormsProcessModel.FormInstanceID);
                //if login user is the present approver then can see form in edit mode 
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (joiningRequest.ReqStatusID == (int)RequestStatus.Rejected && joiningRequest.RequesterEmployeeID == objUserContextViewModel.UserId))                   
                {
                    joiningRequest.RequestID = objRequestFormsProcessModel.RequestID;
                    //If form is pending then only show for edit
                    if (joiningRequest.ReqStatusID == (int)RequestStatus.Pending 
                        || (joiningRequest.ReqStatusID == (int)RequestStatus.Rejected && joiningRequest.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || joiningRequest.ReqStatusID == (int)RequestStatus.Completed)
                    {
                        ViewBag.EmployeeJoiningID = joiningRequest.ID;
                        ViewBag.FormProcessID = formProcessID.ToString();
                        ViewBag.RequestID = joiningRequest.RequestID;
                        ViewBag.FormProcessID = formProcessID.ToString();
                        ViewBag.EmployeeId = joiningRequest.EmployeeId;                      
                        return View("Edit", joiningRequest);
                    }
                    else
                        return RedirectToAction("ViewDetails");
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }
        public ActionResult UpdateDetails()
        {
            FormsEmployeeJoiningModel joiningRequest = new FormsEmployeeJoiningModel();
            int formProcessID = GetFormProcessSessionID();
            RequestFormsProcessModel objRequestFormsProcessModel = joiningRequestDB.GetRequestFormsProcess(formProcessID);
            joiningRequest = joiningRequestDB.GetForm(formProcessID, objRequestFormsProcessModel.FormInstanceID);
            joiningRequest.RequestID = objRequestFormsProcessModel.RequestID;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];          
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 18;

            if (formProcessID > 0)
            {
                //Pending Or Completed and is coming from all requests page
                if (joiningRequest.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.EmployeeJoiningID = joiningRequest.ID;
                    ViewBag.FormProcessID = formProcessID.ToString();
                    ViewBag.RequestID = joiningRequest.RequestID;
                    ViewBag.FormProcessID = formProcessID.ToString();
                    ViewBag.EmployeeId = joiningRequest.EmployeeId;            
                    return View("UpdateDetails", joiningRequest);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
              
            }
            else
                return RedirectToAction("Create");
        }
        public ActionResult ViewDetails(int? id)
        {
            int detailId = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            if (id <= 0)
                return RedirectToAction("Index", "Home");
            FormsEmployeeJoiningModel joiningModel = new FormsEmployeeJoiningModel();
            joiningModel = joiningRequestDB.GetFormDetails(detailId);
            List<RequestFormsApproveModel> requestFormsApproveModelList = joiningRequestDB.GetAllRequestFormsApprovals(detailId);
            joiningModel.RequestFormsApproveModelList = requestFormsApproveModelList;
            joiningModel.FormProcessID = detailId;
            return View("ViewDetail", joiningModel);
        }
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            OperationDetails operationDetails = new OperationDetails();
            int result = 0;
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";

                    //if ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send.";
                        }

                        int id = GetFormProcessSessionID();
                        FormsEmployeeJoiningModel joiningModel = new FormsEmployeeJoiningModel();
                        joiningModel = joiningRequestDB.GetFormDetails(id);
                        EmployeeDetailsModel employee = new EmployeeDetailsModel();
                        result = new EmployeeJoiningRequestDB().UpdateFlag(joiningModel.EmployeeId, Convert.ToDateTime(joiningModel.JoiningDate));
                        //Add any logic to be do once approval process is completed
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateForm(FormsEmployeeJoiningModel joiningRequest, int formProcessID, HttpPostedFileBase OfferLetterFile)
        {
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];            
            AllFormsFilesModel ObjFile;
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
            if (OfferLetterFile != null)
            {
                ObjFile = new AllFormsFilesModel();
                ObjFile.FileName = OfferLetterFile.FileName;
                ObjFile.FileContentType = OfferLetterFile.ContentType;
                ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(OfferLetterFile);
                ObjFile.FormFileIDName = "OfferLetterFileID";
                uploadFileList.Add(ObjFile);
                joiningRequest.OfferLetterFileID = 0;
            }
            joiningRequest.AllFormsFilesModelList = uploadFileList;
            operationDetails = joiningRequestDB.UpdateForm(joiningRequest, formProcessID, objUserContextViewModel.UserId);
            if (operationDetails.Success)
            {
                if (joiningRequest.ReqStatusID == (int)RequestStatus.Rejected && joiningRequest.RequesterEmployeeID == objUserContextViewModel.UserId)
                {
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = joiningRequestDB.GetApproverEmailList(formProcessID.ToString(), objUserContextViewModel.UserId);
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                }
                
                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request updated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}