﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class AllRequestsController : BaseController
    {
        // GET: AllRequests
        public ActionResult Index()
        {
            var formsUniqueKeyList = new FormsUniqueKeyDB().GetAllFormsUniqueKey(0);
            formsUniqueKeyList.Add(new FormsUniqueKeyModel() { FormName = "Exit Interview", FormID = 30 });
            ViewBag.FormList = new SelectList(formsUniqueKeyList, "FormID", "FormName");
            return View();
        }
        
        public ActionResult GetAllRequestsList(int? requestId, int? formID, int? requestStatusId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AllRequestsFormsDB requestFormsDB = new AllRequestsFormsDB();
            List<AllRequestsListViewModel> allRequestsList = requestFormsDB.GetAllRequestsList(objUserContextViewModel.UserId, requestId, formID, requestStatusId);
            List<string> allRequestsListWithPermission = allRequestsList.Select(x=>x.ControllerName).Distinct().ToList();//Select(e=>new { e.ControllerName }).Distinct();
            List<PermissionModel> lstPermissionModel = new List<PermissionModel>();
            PermissionModel permissionModel;
         
            foreach (var item in allRequestsListWithPermission)
            {
                if (CommonHelper.CommonHelper.CheckPermissionForUser("/"+item).IsMainNavigationPermission)
                {
                    permissionModel = new PermissionModel();
                    permissionModel.ModuleName = item;
                    permissionModel.IsMainNavigationPermission = true;
                    lstPermissionModel.Add(permissionModel);
                }
            }
         
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            UserRoleDB userRoleDB = new UserRoleDB();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            bool canUpdate = PermissionList.Where(x => x.OtherPermissionId == 42).Count() > 0 ? true : false;

            var vList = new object();
            vList = new
            {
                aaData = (from item in allRequestsList
                          select new
                          {
                              FormApprovalID = item.FormApprovalID,
                              FormProcessID = item.FormProcessID,
                              GroupID = item.GroupID,
                              FormID = item.FormID,
                              RequestID = item.RequestID,
                              ReqStatusID = item.ReqStatusID,
                              CreatedOn = item.CreatedOn.ToString(HRMSDateFormat),
                              ModifiedOn = item.ModifiedOn,
                              ModifiedBy = item.ModifiedBy,
                              RequesterEmployeeID = item.RequesterEmployeeID,
                              GroupName = item.GroupName,
                              ModifiedEmployeeName = item.ModifiedEmployeeName,
                              RequesterEmployeeName = item.RequesterEmployeeName,
                              ParentFormID = item.ParentFormID,
                              RequesterCompanyName = item.RequesterCompanyName,
                              CompletedRejectedStatus = item.CompletedRejectedStatus,
                              ApproverEmployeeName = (item.ReqStatusID == 2 || item.ReqStatusID == 1)?item.ApproverEmployeeName:(item.ReqStatusID == 3)?item.ModifiedEmployeeName:"",
                              FormName = item.FormName,
                              Status= item.Status,
                              Action = (item.ReqStatusID == 4 || item.ReqStatusID == 1) && canUpdate?
                              "<a class='btn btn-primary btn-rounded btn-condensed btn-sm' onclick=EditRequestForm('"+item.ControllerName.ToString()+"',"+item.FormProcessID+ ") title='Update Request Detail'><i class='fa fa-edit fa-1x'></i></a>" : ""
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
    }
}