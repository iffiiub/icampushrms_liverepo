﻿using HRMS.DataAccess;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using HRMS.Entities.ViewModel;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using Stimulsoft.Report.Components;
using Stimulsoft.Base.Drawing;
using System.Drawing;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using System.Collections;
using System.Text;

namespace HRMS.Web.Controllers
{
    public class ReportBuilderController : BaseController
    {
        // GET: ReportBuilder
        public ActionResult Index(int? id)
        {
            ReportBuilderDB objDB = new ReportBuilderDB();
            ReportBuilderModel model = new ReportBuilderModel();

            if (id == null)
                id = 0;

            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            model.PayReportBuilderSavedReportsID = (int)id;

            //Delete all where conditions
            // objDB.DeletePayReportBuilderWhere(objUserContextViewModel.UserId);

            if (id == 0)
            {
                Session["SelectedFieldsList"] = model.SelectedFieldsList = new List<ReportBuilderFieldsModel>();
                Session["WhereConditionList"] = model.WhereConditionsList = new List<PayReportBuilderWhereModel>();
            }
            else
            {
                model = objDB.GetSavedReportById(model.PayReportBuilderSavedReportsID);
                if (model != null)
                {
                    //Get fields
                    string selFields = model.SelectedFields;
                    int colsCount = Convert.ToInt32(selFields.Substring(0, 3));//take first 3 chars of count
                    selFields = selFields.Substring(3);//remove first 3 chars of count
                    model.SelectedFieldsList = new List<ReportBuilderFieldsModel>();
                    for (int i = 0; i < colsCount; i++)
                    {
                        ReportBuilderFieldsModel fieldModel = objDB.GetReportFieldsDataById(Convert.ToInt32(selFields.Substring(0, 3)));
                        if (fieldModel != null)
                            model.SelectedFieldsList.Add(fieldModel);

                        selFields = selFields.Substring(3);
                    }
                    Session["SelectedFieldsList"] = model.SelectedFieldsList;
                    Session["WhereConditionList"] = model.WhereConditionsList = new List<PayReportBuilderWhereModel>();
                }
            }

            model.AllFieldsList = GetAllFieldsList();
            model.AllGroupByClauseList = new List<SelectListItem>();
            model.WhereConditionOperators = GetWhereConditionOperators();
            model.LogicalOperators = GetLogicalOperators();
            model.WhereConditionValues = new List<SelectListItem>();

            model.SavedReportList = objDB.GetAllSavedReportsByUser(objUserContextViewModel.UserId);
            return View(model);
        }

        #region Fields

        public List<SelectListItem> GetAllFieldsList()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<ReportBuilderFieldsModel> list = new ReportBuilderDB().GetReportFieldsData();
            foreach (var item in list)
            {
                selectlist.Add(new SelectListItem { Text = item.description, Value = item.PayReportBuilderFieldsID.ToString() });
            }
            return selectlist;
        }

        public JsonResult GetReportFieldsData()
        {
            List<ReportBuilderFieldsModel> list = new ReportBuilderDB().GetReportFieldsData();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetSelectedFieldsList(int fieldId, string description, string selectedFields)
        {
            List<ReportBuilderFieldsModel> updatedListFields = new List<ReportBuilderFieldsModel>();
            List<ReportBuilderFieldsModel> oldListFields = updatedListFields = (List<ReportBuilderFieldsModel>)Session["SelectedFieldsList"];
            ReportBuilderFieldsModel model = oldListFields.Where(a => a.PayReportBuilderFieldsID == fieldId).FirstOrDefault();
            if (model == null)//Add only if item does not exist in model
            {
                //Add the new sorted values
                updatedListFields = new List<ReportBuilderFieldsModel>();
                string[] arrFields = selectedFields.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in arrFields)
                {
                    model = oldListFields.Where(a => a.PayReportBuilderFieldsID == Convert.ToInt32(item)).FirstOrDefault();
                    if (model != null)
                        updatedListFields.Add(model);
                }

                //Add new
                model = new ReportBuilderFieldsModel();
                model.PayReportBuilderFieldsID = fieldId;
                model.description = description;
                updatedListFields.Add(model);

                Session["SelectedFieldsList"] = updatedListFields;
            }
            return PartialView("_SelectedFieldsList", updatedListFields);
        }

        public PartialViewResult RemoveSelected()
        {
            List<ReportBuilderFieldsModel> listFields = new List<ReportBuilderFieldsModel>();
            Session["SelectedFieldsList"] = listFields;
            return PartialView("_SelectedFieldsList", listFields);
        }

        public PartialViewResult RemoveField(int fieldId)
        {
            List<ReportBuilderFieldsModel> listFields = new List<ReportBuilderFieldsModel>();
            if (Session["SelectedFieldsList"] != null)
            {
                listFields = (List<ReportBuilderFieldsModel>)Session["SelectedFieldsList"];
                ReportBuilderFieldsModel model = listFields.Where(a => a.PayReportBuilderFieldsID == fieldId).FirstOrDefault();
                if (model != null)
                    listFields.Remove(model);
                Session["SelectedFieldsList"] = listFields;
            }
            return PartialView("_SelectedFieldsList", listFields);
        }

        public JsonResult GetGroupByClauseValues()
        {           
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist = GetAllGroupByClauseValues();
            return Json(selectlist, JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetAllGroupByClauseValues()
        {
            List<ReportBuilderFieldsModel> listFields = new List<ReportBuilderFieldsModel>();
            List<SelectListItem> selectlist = new List<SelectListItem>();
            if (Session["SelectedFieldsList"] != null)
            {
                listFields = (List<ReportBuilderFieldsModel>)Session["SelectedFieldsList"];
                foreach (var item in listFields)
                {
                    selectlist.Add(new SelectListItem { Text = item.description.ToString().Trim(), Value = item.PayReportBuilderFieldsID.ToString().Trim() });
                }
            }
            return selectlist;
        }

        #endregion

        #region Where Conditions

        public JsonResult GetWhereConditionValues(int id)
        {
            if (id != 0)
            {
                List<SelectListItem> selectlist = new List<SelectListItem>();
                string textField = "", valueField = "";
                DataTable dtValues = new ReportBuilderDB().GetWhereConditionValues(id, out textField, out valueField);

                foreach (DataRow row in dtValues.Rows)
                {
                    selectlist.Add(new SelectListItem { Text = row[textField.Trim()].ToString(), Value = row[valueField.Trim()].ToString() });
                }
                return Json(selectlist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetReportFieldsDataSearch(string value)
        {
            if (value == null)
            {
                value = "";
            }
            List<ReportBuilderFieldsModel> list = new ReportBuilderDB().GetReportFieldsData(value);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetWhereConditionOperators()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem { Text = "AND", Value = "AND" });
            selectlist.Add(new SelectListItem { Text = "OR", Value = "OR" });
            return selectlist;
        }

        public List<SelectListItem> GetLogicalOperators()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem { Text = "=", Value = "=" });
            selectlist.Add(new SelectListItem { Text = ">", Value = ">" });
            selectlist.Add(new SelectListItem { Text = "<", Value = "<" });
            selectlist.Add(new SelectListItem { Text = ">=", Value = ">=" });
            selectlist.Add(new SelectListItem { Text = "<=", Value = "<=" });
            selectlist.Add(new SelectListItem { Text = "<>", Value = "<>" });
            selectlist.Add(new SelectListItem { Text = "Like", Value = "Like" });
            return selectlist;
        }

        public PartialViewResult GetWhere(int id, int isWhere, string strAndOr, int isTextField,
            string whereOperator, string ddlValue, string ddlText, string txtValue)
        {
            ReportBuilderDB objDB = new ReportBuilderDB();
            string whereDisplay = "", where = "";

            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (id != 0)
            {
                if (isWhere == 1)
                {
                    where = where + "&where=where";
                    whereDisplay += "Where ";
                    //Delete previous values from temp where table
                    //objDB.DeletePayReportBuilderWhere(objUserContextViewModel.UserId);
                }
                else
                {
                    where = " " + strAndOr + " ";
                    whereDisplay = " " + strAndOr + " ";
                }

                ReportBuilderFieldsModel model = objDB.GetReportFieldsDataById(id);
                if (model != null)
                {
                    where += model.sqlID + " " + whereOperator + " ";
                    whereDisplay += model.description + " " + whereOperator + " ";

                    if (isTextField == 0)
                    {
                        where += "'" + ddlText + "' ";
                        whereDisplay += ddlText + " ";
                    }
                    else
                    {
                        if (whereOperator == "Like")
                            where += " '" + txtValue + "%'";
                        else if (model.sqlID.ToUpper().IndexOf("ID").ToString() == "-1")
                        {
                            if (model.sqlID.ToUpper().IndexOf("DATETIME").ToString() == "-1")
                                where += " '" + txtValue + "'";
                            else
                                where += " '" + txtValue.Substring(3, 2) + "/" + txtValue.Substring(0, 2) + "/" + txtValue.Substring(6, 4) + "'";
                        }
                        else
                            where += " '" + txtValue + "'";

                        whereDisplay += txtValue + " ";
                    }
                }
                else
                {
                    where += "&where=where";
                }
            }
            else
                where += "&where=where";

            //Insert into HR_PayReportBuilderWhere

            PayReportBuilderWhereModel whereModel = new PayReportBuilderWhereModel();
            whereModel.PayReportBuilderWhere = whereDisplay;
            whereModel.whereSQL = where;
            whereModel.Eqal = "0";
            whereModel.UserID = objUserContextViewModel.UserId.ToString();
            whereModel.PayReportBuilderWhereID = id;
            //OperationDetails operationDetails = objDB.InsertPayReportBuilderWhere(whereModel);

            List<PayReportBuilderWhereModel> listWhereConditions = new List<PayReportBuilderWhereModel>();
            if (Session["WhereConditionList"] != null)
                listWhereConditions = (List<PayReportBuilderWhereModel>)Session["WhereConditionList"];
            listWhereConditions.Add(whereModel);
            Session["WhereConditionList"] = listWhereConditions;

            return PartialView("_WhereConditionList", listWhereConditions);
        }

        public JsonResult DeleteWhereCondition(int id)
        {
            ReportBuilderDB objDB = new ReportBuilderDB();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<PayReportBuilderWhereModel> listWhereConditions = new List<PayReportBuilderWhereModel>();
            if (Session["WhereConditionList"] != null)
            {
                listWhereConditions = (List<PayReportBuilderWhereModel>)Session["WhereConditionList"];
                PayReportBuilderWhereModel model = listWhereConditions.Where(a => a.PayReportBuilderWhereID == id).FirstOrDefault();

                if (model != null)
                {
                    if (model.whereSQL.Contains("&where"))//delete all where condiitons
                    {
                        //Delete all
                        //objDB.DeletePayReportBuilderWhere(objUserContextViewModel.UserId);

                        //Reset list
                        listWhereConditions = new List<PayReportBuilderWhereModel>();
                    }
                    else
                    {
                        //Delete node
                        //objDB.DeletePayReportBuilderWhereById(id);

                        //Delet from list
                        listWhereConditions.Remove(model);
                    }
                }
            }
            Session["WhereConditionList"] = listWhereConditions;

            return Json(new object[] { CommonHelper.CommonHelper.RenderViewToString(this.ControllerContext, "_WhereConditionList", listWhereConditions), listWhereConditions.Count });
        }

        public void RemoveAllWhereConditions()
        {
            Session["WhereConditionList"] = new List<PayReportBuilderWhereModel>();
        }

        #endregion

        #region Saved Reports

        public JsonResult SaveReport(int id, bool isForMe, string reportTitle, string selectedFields, bool isOrderBy, int orderBy, bool isGroupBy, int groupBy, string hdnOldSqlWhereCondition, string hdnOldWhereDescription, string hdnNeSqlWhereCondition, string hdnNewWhereDescription)
        {
            ReportBuilderDB objDB = new ReportBuilderDB();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ReportBuilderModel model = new ReportBuilderModel();
            hdnOldSqlWhereCondition = HttpUtility.UrlDecode(hdnOldSqlWhereCondition);
            hdnOldWhereDescription = HttpUtility.UrlDecode(hdnOldWhereDescription);
            hdnNeSqlWhereCondition = HttpUtility.UrlDecode(hdnNeSqlWhereCondition);
            hdnNewWhereDescription = HttpUtility.UrlDecode(hdnNewWhereDescription);
            model.PayReportBuilderSavedReportsID = id;
            string strSelectedFields = "";
            string[] arrFields = selectedFields.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in arrFields)
            {
                strSelectedFields = strSelectedFields + item.ToString().PadLeft(3, '0');
            }
            model.SelectedFields = arrFields.Length.ToString().PadLeft(3, '0') + strSelectedFields;
            model.UserID = isForMe ? objUserContextViewModel.UserId : 0;
            model.OrderBy = isOrderBy ? orderBy : 0;
            model.GroupBy = isGroupBy ? groupBy : 0;
            model.ReportName = reportTitle;
            model.PayReportBuilderSavedReportsWhere = hdnNeSqlWhereCondition == "" ? hdnOldSqlWhereCondition : hdnNeSqlWhereCondition;
            model.WhereDescription = hdnNewWhereDescription == "" ? hdnOldWhereDescription : hdnNewWhereDescription;
            OperationDetails operationDetails = objDB.InsertUpdate_PayReportBuilderSavedReports(model, objUserContextViewModel.UserId);
            string result = "";
            if (operationDetails.Message.Contains("Error"))
                result = "error";
            else
                result = "success";

            return Json(new { result = result, resultMessage = operationDetails.Message }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetSavedReports()
        {
            ReportBuilderDB objDB = new ReportBuilderDB();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<ReportBuilderModel> listSavedReports = objDB.GetAllSavedReportsByUser(objUserContextViewModel.UserId);
            return PartialView("_SavedReportsList", listSavedReports);
        }

        public PartialViewResult DeleteSavedReport(int id)
        {
            ReportBuilderDB objDB = new ReportBuilderDB();
            objDB.DeletePayReportBuilderSavedReportsById(id);

            //Get all records
            return GetSavedReports();
        }

        #endregion

        #region Report Preview
        private StiReport PreviewReport(DataTable dt, string orientation, string groupBySqlID, string reportName)
        {
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');
            dt.TableName = "ReportBuilder";
            if (groupBySqlID.Contains("(AR)"))
            {
                groupBySqlID = groupBySqlID.Replace("(AR)", "") + "_ArabicT";
            }
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ColumnName.Contains("(AR)"))
                {
                    column.ColumnName = column.ColumnName.Replace("(AR)", "") + " ArabicT";
                }
                else
                {
                    if (column.ColumnName.Contains("("))
                    {
                        column.ColumnName = column.ColumnName.Replace("(", "LeftOpeningBracket");
                    }
                    if (column.ColumnName.Contains(")"))
                    {
                        column.ColumnName = column.ColumnName.Replace(")", "RightClosingBracket");
                    }
                }
            }
                       
            if (orientation == "portrait")
                report.Load(Server.MapPath("~/Content/Reports/ReportTemplates/ReportPotrait.mrt"));
            else
                report.Load(Server.MapPath("~/Content/Reports/ReportTemplates/ReportLandscape.mrt"));
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            report.ReportName = reportName;
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            txtReportName.Text = reportName;
            //Add data to datastore
            report.RegData(dt);
            //Fill dictionary
            report.Dictionary.Synchronize();
            StiPage page = report.Pages[0];
            //page.Width = 26;

            //Create HeaderBand
            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Height = 1.2;

            headerBand.Name = "HeaderBand";
            headerBand.MinHeight = 1.2;
            headerBand.MaxHeight = 2;
            headerBand.CanShrink = true;
            headerBand.CanGrow = true;
            page.Components.Add(headerBand);

            StiGroupHeaderBand groupHeaderBand = new StiGroupHeaderBand();
            if (groupBySqlID != "")
            {
                //Create GroupHeaderBand
                groupHeaderBand.Height = 1.2;
                groupHeaderBand.Name = "GroupHeaderBand";
                groupHeaderBand.Condition.Value = "{ReportBuilder." + groupBySqlID + "}";
                page.Components.Add(groupHeaderBand);
            }

            //Create Databand
            StiDataBand dataBand = new StiDataBand();
            dataBand.DataSourceName = "ReportBuilder";
            dataBand.Height = 1;
            dataBand.MaxHeight = 2;
            dataBand.MinHeight = 1;
            dataBand.Name = "DataBand";
            headerBand.CanShrink = true;
            headerBand.CanGrow = true;
            page.Components.Add(dataBand);

            double pos = 0;
            //double columnWidth = StiAlignValue.AlignToMinGrid(page.Width / dt.Columns.Count, 0.1, true);
            double columnWidth = page.Width / dt.Columns.Count;
            int nameIndex = 1;

            //Create texts
            foreach (DataColumn column in dt.Columns)
            {
                //Create text on header
                StiText headerText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                if (column.ColumnName.Contains("ArabicT"))
                {
                    headerText.Text.Value = column.ColumnName.Replace(" ArabicT", "") + "(AR)";
                }
                else
                {
                    if (column.ColumnName.Contains("LeftOpeningBracket"))
                    {
                        string columnName = column.ColumnName;
                        columnName = columnName.Replace("LeftOpeningBracket", "(");
                        headerText.Text.Value = columnName.Replace("RightClosingBracket", ")");
                    }
                    else
                    {
                        headerText.Text.Value = column.ColumnName;
                    }
                }
                headerText.Name = "HeaderText" + nameIndex.ToString();
                CommonParameters(true, ref headerText);
                headerBand.Components.Add(headerText);

                //Create text on Data Band
                StiText dataText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                dataText.Text.Value = "{ReportBuilder." + column.ColumnName.Trim().Replace(" ", "_").Replace(".", "_") + "}";
                dataText.Name = "DataText" + nameIndex.ToString();
                CommonParameters(false, ref dataText);
                dataBand.Components.Add(dataText);

                pos += columnWidth;

                nameIndex++;
            }

            StringBuilder sbText = new StringBuilder();
            sbText.Append(schoolInfo.SchoolName_1);

            if (groupBySqlID != "")
            {
                //Add group header text
                StiText groupHeaderText = new StiText(new RectangleD(0, 0, pos, 0.5f));
                groupHeaderText.Text.Value = "  " + groupBySqlID + ": { ReportBuilder." + groupBySqlID + " }";
                groupHeaderText.Name = "GroupHeaderText";
                CommonParameters(false, ref groupHeaderText);
                groupHeaderText.HorAlignment = StiTextHorAlignment.Left;
                groupHeaderText.Font = new Font("Arial", 10f, FontStyle.Bold);
                groupHeaderBand.Components.Add(groupHeaderText);
            }

            ////Create FooterBand
            //StiFooterBand footerBand = new StiFooterBand();
            //footerBand.Height = 0.5;
            //footerBand.Name = "FooterBand";
            //page.Components.Add(footerBand);

            ////Create text on footer
            //StiText footerText = new StiText(new RectangleD(0, 0, pos, 0.5f));
            //footerText.Text = "Count - {Count()}";
            //footerText.Name = "FooterText";
            //CommonParameters(true, ref footerText);
            //footerText.HorAlignment = StiTextHorAlignment.Right;
            //footerBand.Components.Add(footerText);

            return report;
        }

        public void GetPageHeader(ref StiPageHeaderBand pageheader)
        {
            //Create Image
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation();
            StiImage image = new StiImage();
            image.Name = "ImageLogo";
            image.Image = Image.FromFile(Server.MapPath(schoolInfo.Logo));
            image.Left = 0;
            image.Top = 0;
            image.Width = 2;
            image.Height = 2;
            pageheader.Components.Add(image);

        }
        public ActionResult ReportBuilderReportingViewer(int orderBy, int groupBy, string orientation, string selectedFields, string reportName, int ReportId, string SqlWhereCondition)
        {
            return View();
        }

        public ActionResult GetReportBuilderReporting(int orderBy, int groupBy, string orientation, string selectedFields, string reportName, int ReportId, string SqlWhereCondition)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            StiReport report = new StiReport();
            SqlWhereCondition = HttpUtility.UrlDecode(SqlWhereCondition);
            if (selectedFields != "")
            {
                string groupBySqlID = "", orderBySqlID = "";
                ReportBuilderDB objDB = new ReportBuilderDB();
                DataTable dt = objDB.GetReportData(1, selectedFields, orderBy, groupBy, ReportId, SqlWhereCondition,1,0,"", UserId,out groupBySqlID, out orderBySqlID);
                report = PreviewReport(dt, orientation, groupBySqlID, reportName);
            }
            return StiMvcViewer.GetReportSnapshotResult(this.HttpContext, report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        private void CommonParameters(bool isHeader, ref StiText text)
        {
            text.HorAlignment = StiTextHorAlignment.Center;
            text.VertAlignment = StiVertAlignment.Center;
            text.Border.Side = StiBorderSides.All;
            string fontName = "Arial";
            float fontEmSize = 10f;
            text.GrowToHeight = true;
            text.CanShrink = true;
            text.CanGrow = true;
            text.Margins = new StiMargins(3, 3, 3, 3);
            text.WordWrap = true;
            if (isHeader)
            {
                text.Font = new Font(fontName, 10, FontStyle.Bold);
                text.Height = 1.2;
            }
            else
            {
                text.Font = new Font(fontName, 8, FontStyle.Regular);
                text.Height = 1;
            }
        }

        private void PageHeaderParameters(ref StiText text)
        {
            text.HorAlignment = StiTextHorAlignment.Center;
            text.VertAlignment = StiVertAlignment.Center;
            text.Font = new Font("Cambria", 18f, FontStyle.Bold);
        }

        #endregion


    }
}