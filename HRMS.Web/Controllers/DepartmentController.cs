﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace HRMS.Web.Controllers
{
    public class DepartmentController : BaseController
    {
        //
        // GET: /Department/
        public ActionResult Index()
        {
            ViewBag.Company = GetCompanySelectList();
            return View();
        }

        public ActionResult GetDepartmentList(int companyId)
        {
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            ShiftDB objShiftDB = new ShiftDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CompanyDB objCompanyDB = new CompanyDB();
            objDepartmentList = objDepartmentDB.GetAllDepartment(companyId: companyId, userId: objUserContextViewModel.UserId);
            var vList = new object();
            //*** Naresh 2020-03-18 No need to fetch the data in loop
            var allOrganizations = objCompanyDB.GetAllCompanyList();
            var allShifts = objShiftDB.GetAllShiftNames(0);//Return all shifts
            vList = new
            {
                aaData = (from item in objDepartmentList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.DepartmentId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.DepartmentId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewSalaryGrade(" + item.DepartmentId.ToString() + ")' title='View Details' ><i class='fa fa-eye'></i></a>",
                              DepartmentID = item.DepartmentId,
                              CompanyId = allOrganizations.Where(x => x.CompanyId == item.CompanyId).Select(x => x.name).SingleOrDefault(),
                              DepartmentName_1 = item.DepartmentName_1,
                              DepartmentName_2 = item.DepartmentName_2,
                              DepartmentName_3 = item.DepartmentName_3,
                              Description = item.Description,
                              StartDate = item.StartDate,
                              CreatedBy = item.CreatedBy,
                              ShiftID = allShifts.Where(x => x.ShiftID == item.ShiftID).Select(x => x.ShiftName).SingleOrDefault(),
                              ShortCode = item.ShortCode,
                              CompanyName = item.CompanyName
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGridForDepartmentEmployee(int CompanyId, int DepartmentId, string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        {

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<HRMS.Entities.EmployeeDetails> objemployeelist = new List<HRMS.Entities.EmployeeDetails>();
            DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            objemployeelist = objDepartmentEmployeeDB.GetEmployeeByDepartmentId(DepartmentId, CompanyId);

            //---------------------------------------------
            var vList = new object();

            vList = new
            {
                aaData = (from item in objemployeelist
                          select new
                          {
                              FirstName = item.FirstName_1,
                              LastName = item.SurName_1
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ShiftDB objShiftDB = new ShiftDB();
            var shifts = new List<ShiftModel>();
            var employeeList = new List<Employee>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HRMS.Entities.DepartmentModel objDepartment = new HRMS.Entities.DepartmentModel();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            if (id != -1)
            {
                objDepartment = objDepartmentDB.GetDepartment(id);
                employeeList = objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1 && x.CompanyId == objDepartment.CompanyId).ToList();
                shifts = objShiftDB.GetAllShiftNames(objDepartment.CompanyId);

            }
            ViewBag.Shift = new SelectList(shifts, "ShiftID", "ShiftName");
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(employeeList, "EmployeeId", "FullName");
            return View(objDepartment);
        }

        public ActionResult LoadEmployeeAndShiftForDepartmentByCompanyId(int companyId)
        {
            DepartmentViewModel departmentViewModel = new DepartmentViewModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            var departmentEmployeeCntl = new DepartmentEmployeeController();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            departmentViewModel.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1 && x.CompanyId == companyId).ToList(), "EmployeeId", "FullName");
            departmentViewModel.Shifts = new SelectList(new ShiftDB().GetAllShiftNames(companyId),"ShiftID", "ShiftName");
            return Json(departmentViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDetails(int id)
        {
            HRMS.Entities.DepartmentModel objDepartment = new HRMS.Entities.DepartmentModel();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");

            if (id != 0)
            {
                objDepartment = objDepartmentDB.GetDepartment(id);
                ShiftDB objShiftDB = new ShiftDB();
                CompanyDB objCompanyDB = new CompanyDB();
                var company = objCompanyDB.GetCompany(objDepartment.CompanyId);
                if (company != null)
                    objDepartment.CompanyName = company.name;
                try
                {

                    objDepartment.CompanyName = objCompanyDB.GetCompany(objDepartment.CompanyId).name;
                    objDepartment.ShiftName = objShiftDB.GetShiftById(objDepartment.ShiftID).FirstOrDefault().ShiftName;
                }
                catch (Exception ex)
                {
                    objDepartment.CompanyName = "";
                    objDepartment.ShiftName = "";
                }
            }

            return View(objDepartment);
        }

        public JsonResult Save(DepartmentModel objDepartment, string hdnDepartmentEmployeeIds, int DepartmentLeaderID)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                DepartmentDB objDepartmentDB = new DepartmentDB();
                objDepartment.CreatedBy = objUserContextViewModel.UserId;
                objDepartment.ModifiedBy = objUserContextViewModel.UserId;
                objDepartment.DepartmentLeader = DepartmentLeaderID;
                objDepartment.DepartmentEmployees = new List<int>();
                int employeeId = 0;
                if (hdnDepartmentEmployeeIds != "")
                {

                    hdnDepartmentEmployeeIds.Split(',').ToList().ForEach(e =>
                    {
                        employeeId = 0;
                        if (int.TryParse(e, out employeeId))
                        {
                            objDepartment.DepartmentEmployees.Add(employeeId);
                        }
                    });
                }
                var response = objDepartmentDB.DepartmentInsertUpdate(objDepartment);
                result = "success";
                resultMessage = "Department Details Saved Successfully.";
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while adding Department Details.";
            }

            //return Redirect("Index");
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            HRMS.Entities.DepartmentModel objDepartment = new HRMS.Entities.DepartmentModel();
            objDepartment.DepartmentId = int.Parse(id);
            objDepartmentDB.DeleteDepartment(objDepartment.DepartmentId);
            //return Redirect("Index");

            return Json(new { result = "success", resultMessage = "Department Deleted Successfully." }, JsonRequestBehavior.AllowGet);

        }


        ///// Export content to List
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="listToExport"></param>
        ///// <returns></returns>
        ///// 
        //public object ExportListToExcel()
        // {

        //   var dtExportTable =  GetDepartmentList();
        //    //System.Data.DataTable dtExportTable;
        //    try
        //    {
        //        GridView gridvw = new GridView();
        //        gridvw.DataSource = dtExportTable; //bind the data table to the grid view
        //        gridvw.DataBind();
        //        StringWriter swr = new StringWriter();
        //        HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        //        gridvw.RenderControl(tw);
        //        byte[] content = Encoding.ASCII.GetBytes(swr.ToString());
        //        return content;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        public ActionResult ExportToExcel(int? companyId)
        {
            byte[] content;
            string fileName = "Department" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB departmentDB = new DepartmentDB();
            System.Data.DataSet ds = departmentDB.GetAllDepartmentDataset(companyId);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(int? companyId)
        {
            // Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "Department" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int companyid = objUserContextViewModel.CompanyId;
            DepartmentDB departmentDB = new DepartmentDB();


            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //CompanyDB objCompanyDB = new CompanyDB();
            System.Data.DataSet ds = departmentDB.GetAllDepartmentDataset(companyId);
            //Document dc = CommonHelper.CommonHelper.GenrateDepartmentPDF(Response, ds, true, companyid);
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }

    }
}