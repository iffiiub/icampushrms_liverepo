﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class UserGroupController : BaseController
    {
        //
        // GET: /UserGroup/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUserGroupList()
        {
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<UserGroupModel> objUserGroupModelList = new List<UserGroupModel>();
            UserGroupDB objUserGroupDB = new UserGroupDB();
            objUserGroupModelList = objUserGroupDB.GetAllUserGroupsList();
            var vList = new object();

            vList = new
            {
                aaData = (from item in objUserGroupModelList
                          select new
                          {
                              Action = item.UserGroupId == 1 ? "<a class='btnSelect btn btn-default btn-rounded btn-condensed btn-sm' onclick='AssignEmployees(" + item.UserGroupId.ToString() + ",false)' title='Assign Employees' ><i class='fa fa-user'></i> </a>" :
                              item.IsEditable ? "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditUserGroup(" + item.UserGroupId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteUserGroup(" + item.UserGroupId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btnSelect btn btn-default btn-rounded btn-condensed btn-sm' onclick='AssignEmployees(" + item.UserGroupId.ToString() + ",false)' title='Assign Employees' ><i class='fa fa-user'></i> </a>" : "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditUserGroup(" + item.UserGroupId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> <a class='btnSelect btn btn-default btn-rounded btn-condensed btn-sm' onclick='AssignEmployees(" + item.UserGroupId.ToString() + ",false)' title='Assign Employees' ><i class='fa fa-user'></i> </a>",
                              UserGroupId = item.UserGroupId,
                              UserGroupName = item.UserGroupName,
                              Description = item.Description,
                              RoleIds = item.RoleIds,
                              RoleNames = item.RoleNames,
                              ListAccessName = item.ListAccessName,
                              CompanyNames = item.CompanyNames
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUserGroup()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            List<SelectListItem> userGroupListAccess;
            userGroupListAccess = new SelectList(objUserGroupDB.GetUserGroupListAccess(), "UserListAccessID", "ListAccessName").ToList();
            ViewBag.userGroupListAccess = userGroupListAccess;
            List<SelectListItem> _CompanyTypeList;
            _CompanyTypeList = new SelectList(objUserGroupDB.GetCompanyTypeList().Where(m => m.CompanyTypeID > 0).ToList(), "CompanyTypeID", "CompanyTypeName").ToList();
            CompanyModel company = new CompanyDB().GetCompany(objUserContextViewModel.CompanyId);
            //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
            ViewBag.CompanyList = GetCompanySelectList();
            // ViewBag._CompanyTypeList = _CompanyTypeList;
            ViewBag.UserRoleList = new UserRoleDB().GetUserRoleList();

            return View();
        }

        [HttpPost]
        public ActionResult AddUserGroup(UserGroupModel userGroupModel)
        {
            ModelState.Remove("LeftDate");
            userGroupModel.CompanyIds = Request["CompanyList"];
            if (string.IsNullOrEmpty(userGroupModel.CompanyIds))
                ModelState.AddModelError("Company", "Please select atleast one Organization");
            if (ModelState.IsValid)
            {
                userGroupModel.RoleIds = Request["UserRoleList"];
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserGroupDB objUserGroupDB = new UserGroupDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objUserGroupDB.InsertUserGroup(userGroupModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                ViewBag.UserRoleList = new UserRoleDB().GetUserRoleList();
                string messages = string.Join("<br/> ", ModelState.Values
                                                       .SelectMany(x => x.Errors)
                                                       .Select(x => x.ErrorMessage));
                objOperationDetails.Message = messages;
                //objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditUserGroup(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            UserGroupDB objUserGroupDB = new UserGroupDB();
            UserGroupModel userGroupModel = objUserGroupDB.GetUserGroupById(id);
            List<SelectListItem> userGroupListAccess;
            userGroupListAccess = new SelectList(objUserGroupDB.GetUserGroupListAccess(), "UserListAccessID", "ListAccessName").ToList();
            ViewBag.userGroupListAccess = userGroupListAccess;
            List<SelectListItem> _CompanyTypeList;
            _CompanyTypeList = new SelectList(objUserGroupDB.GetCompanyTypeList().Where(m => m.CompanyTypeID > 0).ToList(), "CompanyTypeID", "CompanyTypeName").ToList();
            ViewBag._CompanyTypeList = _CompanyTypeList;
            List<int> companyIdList = new List<int>();
            if (!string.IsNullOrEmpty(userGroupModel.CompanyIds))
                companyIdList = userGroupModel.CompanyIds.Split(',').Select(int.Parse).ToList();
            ViewBag.CompanyIdList = companyIdList;
            //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
            ViewBag.CompanyList = GetCompanySelectList();
            ViewBag.UserRoleList = new UserRoleDB().GetUserRoleList();
            List<int> userRoleList = new List<int>();
            if (!string.IsNullOrEmpty(userGroupModel.RoleIds))
                userRoleList = userGroupModel.RoleIds.Split(',').Select(int.Parse).ToList();
            ViewBag.UserRoleIDList = userRoleList;
            return View(userGroupModel);
        }

        [HttpPost]
        public ActionResult UpdateUserGroup(UserGroupModel userGroupModel)
        {
            ModelState.Remove("LeftDate");
            userGroupModel.CompanyIds = Request["CompanyList"];
            if (string.IsNullOrEmpty(userGroupModel.CompanyIds))
                ModelState.AddModelError("Company", "Please select atleast one Organization");
            if (ModelState.IsValid)
            {
                userGroupModel.RoleIds = Request["UserRoleList"];
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserGroupDB objUserGroupDB = new UserGroupDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objUserGroupDB.UpdateUserGroup(userGroupModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                ViewBag.UserRoleList = new UserRoleDB().GetUserRoleList();
                string messages = string.Join("<br/> ", ModelState.Values
                                                        .SelectMany(x => x.Errors)
                                                        .Select(x => x.ErrorMessage));
                objOperationDetails.Message = messages;
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult DeleteUserGroup(int id)
        {
            UserGroupDB objUserGroupDB = new UserGroupDB();
            objUserGroupDB.DeleteUserGroup(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignEmployees(int id, bool isSelectedAll)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            UserGroupDB objUserGroupDB = new UserGroupDB();
            List<Employee> lstAllEmployees = objUserGroupDB.GetAllEmployees();
            ViewBag.EmployeeList = lstAllEmployees;

            UserGroupModel model = new UserGroupModel();
            model.UserGroupId = id;
            List<Employee> lstGroupEmployees = objUserGroupDB.GetAllEmployeesByUserGroup(id);
            model.EmployeeIds = string.Join(",", lstGroupEmployees.Select(x => x.EmployeeId));
            model.isSelectedAll = isSelectedAll;
            return PartialView("_GroupEmployeeList", model);
        }

        public ActionResult SearchEmployees(int id, string searchval)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            UserGroupDB objUserGroupDB = new UserGroupDB();
            List<Employee> lstAllEmployees = objUserGroupDB.GetAllEmployees();

            lstAllEmployees = lstAllEmployees.Take(400).ToList();
            if (searchval != "")
            {
                lstAllEmployees = lstAllEmployees.Where(x => x.FullName.ToLower().ToString().Contains(searchval.ToLower())).Take(400).ToList();

            }

            var vList = new object();
            vList = lstAllEmployees;

            //UserGroupModel model = new UserGroupModel();
            //model.UserGroupId = id;
            //List<Employee> lstGroupEmployees = objUserGroupDB.GetAllEmployeesByUserGroup(id);
            //model.EmployeeIds = string.Join(",", lstGroupEmployees.Select(x => x.EmployeeId));

            //return PartialView("_GroupEmployeeList", model);

            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InsertUserGroupEmployees(UserGroupModel userGroupModel)
        {
            ModelState.Remove("LeftDate");
            if (ModelState.IsValid)
            {
                userGroupModel.EmployeeIds = Request["EmployeeList"];

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserGroupDB objUserGroupDB = new UserGroupDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objUserGroupDB.InsertUserGroupEmployees(userGroupModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;

                UserGroupDB objUserGroupDB = new UserGroupDB();
                List<Employee> lstAllEmployees = objUserGroupDB.GetAllEmployees();
                ViewBag.EmployeeList = lstAllEmployees;

                UserGroupModel model = new UserGroupModel();
                model.UserGroupId = userGroupModel.UserGroupId;
                List<Employee> lstGroupEmployees = objUserGroupDB.GetAllEmployeesByUserGroup(model.UserGroupId);
                model.EmployeeIds = string.Join(",", lstGroupEmployees.Select(x => x.EmployeeId));

                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

    }
}