﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using System.Globalization;

namespace HRMS.Web.Controllers
{
    public class PayCycleController: BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPayCycleList()
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //-------------Data Objects--------------------
            List<HRMS.Entities.PayCycle> objPayCycleList = new List<HRMS.Entities.PayCycle>();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            objPayCycleList = objAdditionPaidCycleDB.GetAllPayCycle();
            //---------------------------------------------
            var vList = new object();

            vList = new
            {
                aaData = (from item in objPayCycleList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.PayCycleID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.PayCycleID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PayCycleID = item.PayCycleID,
                              PayCycleName_1 = item.PayCycleName_1,
                              PayCycleName_2 = item.PayCycleName_2,
                              PayCycleName_3 = item.PayCycleName_3,
                              DateFrom = item.DateFrom,
                              DateTo = item.DateTo,
                              Active = item.active  ? "Yes": "No",

                              acyear = item.acyear,
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            HRMS.Entities.PayCycle objPayCycle = new HRMS.Entities.PayCycle();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();

            // Dropdown for Academic Year
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Year", Value = "0" });

            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            foreach (var m in objAcademicYearDB.GetAllAcademicYearList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Duration, Value = m.AcademicYearId.ToString() });

            }
            ViewBag.AcademicYearList = ObjSelectedList;

            if (id != 0)
            {
                objPayCycle = objAdditionPaidCycleDB.GetPayCycle(id);
            }

            return View(objPayCycle);
        }

        public JsonResult Save(PayCycle objPayCycle)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
                CultureInfo provider = CultureInfo.InvariantCulture;
                DateTime d1 =new DateTime();
                DateTime d2 = new DateTime();
                d1 =DateTime.ParseExact(objPayCycle.DateFrom, "dd/MM/yyyy", provider);
                d2 = DateTime.ParseExact(objPayCycle.DateTo, "dd/MM/yyyy", provider);
                double daysDiff = (d2 - d1).TotalDays;
                if (daysDiff > 0)
                {


                    if (objAdditionPaidCycleDB.ValidateCycleBeforeInsertTion(objPayCycle.DateFrom, objPayCycle.DateTo, objPayCycle.PayCycleID))
                    {
                        if (objPayCycle.PayCycleID == 0)
                        {
                            objAdditionPaidCycleDB.AddUpdateDeletePayCycle(objPayCycle, "insert");

                        }
                        else
                        {
                            objAdditionPaidCycleDB.AddUpdateDeletePayCycle(objPayCycle, "update");
                        }

                        //return Redirect("Index");
                        if (objPayCycle.PayCycleID == 0)
                        {
                            result = "success";
                            resultMessage = "Pay cycle Added Successfully.";
                        }
                        else if (objPayCycle.PayCycleID != 0)
                        {
                            result = "success";
                            resultMessage = "Pay cycle Updated Successfully.";
                        }
                    }
                    else
                    {
                        result = "error";
                        resultMessage = "Selected dates of this cycle are already assinged to existing cycle. Please select another dates.";
                    }
                }
                else
                {
                    result = "error";
                    resultMessage = "Date To should be greater than Date From.";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while adding Pay Scale.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            HRMS.Entities.PayCycle objPayCycle = new HRMS.Entities.PayCycle();
            objPayCycle.PayCycleID = int.Parse(id);
            objAdditionPaidCycleDB.AddUpdateDeletePayCycle(objPayCycle, "delete");
            //return Redirect("Index");
            //return Json(0, JsonRequestBehavior.AllowGet);
            return Json(new { result = "success", resultMessage = "Pay Scale Deleted Successfully." }, JsonRequestBehavior.AllowGet);
        }

        

        public void ExportToPdf()
        {
            string fileName = "PayCycle" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            System.Data.DataSet ds = objAdditionPaidCycleDB.GetAllPayCycleDataSet();

          
          
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }

        public ActionResult ExportToExcel()
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "PayCycle" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            System.Data.DataSet ds = objAdditionPaidCycleDB.GetAllPayCycleDataSet();
            
          
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public ActionResult ValidateCycleBeforeActivate(int CycleId)
        {            
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            bool isFinal = objAdditionPaidCycleDB.ValidateCycleBeforeActivate(CycleId);

            if (isFinal)
            {
                return Json(new { result = "success", resultMessage = "Can not activate cycle again it is already run as final." });
            }
            else
            {
                return Json(new { result = "error", resultMessage = "" });
            }

        }
    }
}