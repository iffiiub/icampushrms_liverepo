﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class PayRequestConfirmationController : BaseController
    {
        // GET: PayrollConfirmation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PayrollVerificationSetting()
        {
            List<PayrollVerificationSetting> lstSetting = new List<PayrollVerificationSetting>();
            PayrollDB objPayrollDB = new PayrollDB();
            lstSetting = objPayrollDB.GetPayrollVerificationSetting();
            return View(lstSetting);
        }
        public JsonResult PayrollConfirmationList(string Mode, bool IsCancel, bool IsConfirmed)
        {
            PayrollDB payrollDB = new PayrollDB();
            var payrollConfirmationList = payrollDB.GetPayrollConfirmationRequestDate(Mode, IsCancel, IsConfirmed, 0).ToList();
            var vList = new
            {
                aaData = (from item in payrollConfirmationList
                          select new
                          {
                              TypeName = item.TypeName,
                              Date = item.Date,
                              UserName = item.RequestEmployeeName,
                              ActionType = item.ActionType,
                              Approved = "<input type='checkbox' class='Confirm'  onclick=\"ApprovedPayrollRequest(" + item.PayrollConfirmationId + ",this)\" " + (item.IsConfirmed == true ? "checked=\"checked\"" : "") + " data-val=\"" + item.PayrollConfirmationId + "\"  />",
                              Cancel = "<input type='checkbox' class='Cancel' onclick=\"CancelPayrollRequest(" + item.PayrollConfirmationId + ",this)\" " + (item.IsCancel == true ? "checked=\"checked\"" : "") + " data-val=\"" + item.PayrollConfirmationId + "\"  />",
                              Action = "<a class='btn btn-info btn-rounded btn-condensed btn-sm' onclick=\"GetInfoPayrollRequest(" + item.PayrollConfirmationId + ",'" + item.TypeName + "')\" title='View details' ><i class='fa fa-info-circle'></i> </a>",
                          }).ToArray(),
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CancelPayrollRequest(int PayrollConfirmationId)
        {
            PayrollDB payrollDB = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(payrollDB.PayrollRequestCancelation(PayrollConfirmationId, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApprovedPayrollRequest(int PayrollConfirmationId)
        {
            PayrollDB payrollDB = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(payrollDB.PayrollRequestConfirmation(PayrollConfirmationId, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDetailsForRequest(int PayrollConfirmationId, string Type)
        {
            PayrollDB payrollDb = new PayrollDB();
            PayrollRequestDetailModel requestDetailsModel = payrollDb.GetPayrollRequestDetails(PayrollConfirmationId, Type);
            ViewBag.Details = payrollDb.GetAllPayDeductionDetailsListForPayrollRequest(PayrollConfirmationId);
            if (Type == "Deduction")
            {
                return View("PayrollRequestDeductionDetail", requestDetailsModel);
            }
            else
            {

                return View(requestDetailsModel);
            }
        }

        public JsonResult SavePayrollVerificationSetting(string VerificationSetting)
        {
            OperationDetails op = new OperationDetails();
            PayrollDB objPayrollDB = new PayrollDB();
            List<PayrollVerificationSetting> multipleDeductionDetails = JsonConvert.DeserializeObject<List<PayrollVerificationSetting>>(VerificationSetting);
            op = objPayrollDB.UpdatePayrollverificationSetting(multipleDeductionDetails);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
    }
}