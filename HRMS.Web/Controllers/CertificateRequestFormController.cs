﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.ViewModel;
using System.IO;
using System.Globalization;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class CertificateRequestFormController : FormsController
    {
        CertificateRequestFormDB formCertificateRequestDB;
        public CertificateRequestFormController()
        {
            XMLLogFile = "LoggerCertificateRequestForm.xml";
            formCertificateRequestDB = new CertificateRequestFormDB();
        }
        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }
        public ActionResult Create()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var UserId = objUserContextViewModel.UserId;
            var CompanyId = objUserContextViewModel.CompanyId;
            CerficateRequestFormModel certificateRequest = new CerficateRequestFormModel();
            ViewBag.CertificateType = new SelectList(new CertificateRequestFormDB().GetAllCertificateTypes(), "CertificateTypeID", "CertificateTypeName");
            ViewBag.CertificateReason = new SelectList(new CertificateRequestFormDB().GetAllCertificateReason(), "CertificateReasonID", "CertificateReasonName");
            certificateRequest = formCertificateRequestDB.GetFileDetails(UserId);
            certificateRequest.RequesterEmployeeID = objUserContextViewModel.UserId;
            certificateRequest.CreatedOn = DateTime.Now.Date;
            certificateRequest.IsAddMode = true;
            certificateRequest.CompanyID = CompanyId;

            return View(certificateRequest);
        }
        public ActionResult SaveForm()
        {
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            HttpPostedFileBase filePF, fileVF;
            string formProcessIDs = "";
            int formprocessid = GetFormProcessSessionID();          
            int result = 0;
            CerficateRequestFormModel certificateRequest = new CerficateRequestFormModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            try
            {
                var data = Request.Form;
                certificateRequest.ID = Convert.ToInt32(string.IsNullOrEmpty(data["CertificateRequestid"]) ? "0" : data["CertificateRequestid"]);
                certificateRequest.ReqStatusID = 1;
                certificateRequest.CertificateTypeID = Convert.ToInt16(string.IsNullOrEmpty(data["CertificateTypeID"]) ? "0" : data["CertificateTypeID"]);
                certificateRequest.CertificateReasonID = Convert.ToInt16(string.IsNullOrEmpty(data["CertificateReason"]) ? "0" : data["CertificateReason"]);
                certificateRequest.RequestPurpose = Convert.ToString(string.IsNullOrEmpty(data["RequestPurpose"]) ? "" : data["RequestPurpose"]);
                certificateRequest.BankNameAddress = Convert.ToString(string.IsNullOrEmpty(data["BankNameAddress"]) ? "" : data["BankNameAddress"]);
                certificateRequest.PassportFileID = Convert.ToInt32(string.IsNullOrEmpty(data["pfileid"]) ? "0" : data["pfileid"]);
                certificateRequest.VisaFileID = Convert.ToInt32(string.IsNullOrEmpty(data["pfileid"]) ? "0" : data["vfileid"]);
                certificateRequest.EmployeeID = objUserContextViewModel.UserId;
                certificateRequest.CreatedBy = objUserContextViewModel.UserId;
                certificateRequest.CreatedOn = DateTime.Now;
                certificateRequest.ModifiedBy = objUserContextViewModel.UserId;
                certificateRequest.ModifiedOn = DateTime.Now;
                certificateRequest.RequesterEmployeeID = objUserContextViewModel.UserId;
                certificateRequest.CompanyID = objUserContextViewModel.CompanyId;
                certificateRequest.Comments = Convert.ToString(data["Comments"]);

                EmployeeDocumentModel documentModel;
                documentModel = new EmployeeDocumentModel();
                documentModel = new EmployeeDocumentModel();
                DocumentDB documentDb = new DocumentDB();
                AllFormsFilesModel allfiles = new AllFormsFilesModel();
                List<AllFormsFilesModel> uploadList = new List<AllFormsFilesModel>();
                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
                if (Request.Files.Count > 0)
                {
                    if (Request.Files["PassportFile"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        filePF = Request.Files["PassportFile"];
                        ObjFile.FileName = filePF.FileName;
                        ObjFile.FileContentType = filePF.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(filePF);
                        ObjFile.FormFileIDName = "PassportFileID";
                        uploadFileList.Add(ObjFile);
                        certificateRequest.PassportFileID = 0;
                    }
                    if (Request.Files["VisaFile"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileVF = Request.Files["VisaFile"];
                        ObjFile.FileName = fileVF.FileName;
                        ObjFile.FileContentType = fileVF.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileVF);
                        ObjFile.FormFileIDName = "VisaFileID";
                        uploadFileList.Add(ObjFile);
                        certificateRequest.VisaFileID = 0;
                    }
                    if (certificateRequest.PassportFileID > 0)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile = new AllFormsFilesDB().GetAllFormsFiles(certificateRequest.PassportFileID);
                        if (ObjFile.FileID > 0)
                        {
                            certificateRequest.PassportFileID = certificateRequest.PassportFileID;
                        }
                        else
                        {
                            documentModel = new EmployeeDocumentModel();
                            documentModel = documentDb.GetEmployeeDocumentById(certificateRequest.PassportFileID);
                            ObjFile.DocumentFile = documentModel.DocumentFile;
                            ObjFile.FileContentType = documentModel.FileContentType;
                            ObjFile.FileName = documentModel.ImageName;
                            ObjFile.FormFileIDName = "PassportFileID";
                            uploadFileList.Add(ObjFile);
                            certificateRequest.PassportFileID = 0;
                        }
                    }
                    if (certificateRequest.VisaFileID > 0)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile = new AllFormsFilesDB().GetAllFormsFiles(certificateRequest.VisaFileID);
                        if (ObjFile.FileID > 0)
                        {
                            certificateRequest.VisaFileID = certificateRequest.VisaFileID;
                        }
                        else
                        {
                            documentModel = new EmployeeDocumentModel();
                            documentModel = documentDb.GetEmployeeDocumentById(certificateRequest.VisaFileID);
                            ObjFile.DocumentFile = documentModel.DocumentFile;
                            ObjFile.FileContentType = documentModel.FileContentType;
                            ObjFile.FileName = documentModel.ImageName;
                            ObjFile.FormFileIDName = "VisaFileID";
                            uploadFileList.Add(ObjFile);
                            certificateRequest.VisaFileID = 0;
                        }
                    }
                }
                certificateRequest.AllFormsFilesModelList = uploadFileList;

                if (certificateRequest.ID>0)
                {
                    certificateRequest.ReqStatusID = Convert.ToInt16(data["hdnReqStatusID"]);
                    formprocessid = Convert.ToInt32(string.IsNullOrEmpty(data["FormProcessID"]) ? "0" : data["FormProcessID"]);
                    RequestFormsApproveModel requestFormsApproveModel = formCertificateRequestDB.GetPendingFormsApproval(formprocessid);
                    if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                         || (certificateRequest.ReqStatusID == (int)RequestStatus.Rejected && certificateRequest.RequesterEmployeeID == objUserContextViewModel.UserId)
                         || isEditRequestFromAllRequests)
                    {
                        certificateRequest.FormProcessID = formprocessid;
                        result = formCertificateRequestDB.UpdateForm(certificateRequest);
                    }
                    else
                    {
                        operationDetails.Success = true;
                        operationDetails.Message = "No permissions to update.";
                        operationDetails.CssClass = "error";
                        return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    }
                    if (result > 0)
                    {
                        if (certificateRequest.ReqStatusID == (int)RequestStatus.Rejected && certificateRequest.RequesterEmployeeID == objUserContextViewModel.UserId)
                        {
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = formCertificateRequestDB.GetApproverEmailList(Convert.ToString(formprocessid), certificateRequest.RequesterEmployeeID);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        }
                        operationDetails.InsertedRowId = result;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request updated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while updating Details";
                        operationDetails.CssClass = "error";
                    }
                }
                else
                {
                    requestFormsProcessModelList = formCertificateRequestDB.SaveForm(certificateRequest);

                    if (requestFormsProcessModelList != null)
                    {
                        formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));                       
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = formCertificateRequestDB.GetApproverEmailList(Convert.ToString(formProcessIDs), objUserContextViewModel.UserId);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                        operationDetails.InsertedRowId = 1;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request generated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.InsertedRowId = 0;
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while adding Details";
                        operationDetails.CssClass = "error";
                    }
                }                
            }
            catch (Exception ex)
            {
                operationDetails.InsertedRowId = 0;
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit()
        {
            CerficateRequestFormModel certificateRequest = new CerficateRequestFormModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];           
            int id = GetFormProcessSessionID();          
            var UserId = objUserContextViewModel.UserId;
            var CompanyId = objUserContextViewModel.CompanyId;
            if (id > 0)
            {
                certificateRequest = formCertificateRequestDB.GetForm(id, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = formCertificateRequestDB.GetPendingFormsApproval(id);
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (certificateRequest.ReqStatusID == (int)RequestStatus.Rejected && certificateRequest.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    if (certificateRequest.ReqStatusID == (int)RequestStatus.Pending || (certificateRequest.ReqStatusID == (int)RequestStatus.Rejected && certificateRequest.RequesterEmployeeID == objUserContextViewModel.UserId))
                    {
                        ViewBag.FormProcessID = id.ToString();
                        ViewBag.CertificateRequestID = certificateRequest.ID;
                        ViewBag.FormProcessID = id.ToString();
                        ViewBag.RequestID = certificateRequest.RequestID;
                        ViewBag.CertificateTypeId = certificateRequest.CertificateTypeID;
                        FormsUniqueKeyModel uniqueKeyModel = new FormsUniqueKeyModel();
                        uniqueKeyModel = formCertificateRequestDB.GetFormID(certificateRequest.CertificateTypeID);
                        ViewBag.FormId = uniqueKeyModel.FormID;
                        ViewBag.FormName = uniqueKeyModel.FormName;                                               
                        ViewBag.CertificateType = new SelectList(new CertificateRequestFormDB().GetAllCertificateTypes(), "CertificateTypeID", "CertificateTypeName");
                        ViewBag.CertificateReason = new SelectList(new CertificateRequestFormDB().GetAllCertificateReason(), "CertificateReasonID", "CertificateReasonName");
                        return View("Edit", certificateRequest);
                    }
                    else
                        return RedirectToAction("ViewDetails");
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }

        public ActionResult UpdateDetails()
        {
            CerficateRequestFormModel certificateRequest = new CerficateRequestFormModel();
            int formProcessID = GetFormProcessSessionID();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            certificateRequest = formCertificateRequestDB.GetForm(formProcessID, objUserContextViewModel.UserId);
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;

            if (formProcessID > 0)
            {
                if (certificateRequest.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.CertificateReason = new SelectList(new CertificateRequestFormDB().GetAllCertificateReason(), "CertificateReasonID", "CertificateReasonName");
                    ViewBag.CertificateType = new SelectList(new CertificateRequestFormDB().GetAllCertificateTypes(), "CertificateTypeID", "CertificateTypeName");
                    ViewBag.CertificateTypeId = certificateRequest.CertificateTypeID;
                    ViewBag.CertificateRequestID = certificateRequest.ID;
                    ViewBag.RequestID = certificateRequest.RequestID;
                    ViewBag.FormProcessID = formProcessID;
                    FormsUniqueKeyModel uniqueKeyModel = new FormsUniqueKeyModel();
                    uniqueKeyModel = formCertificateRequestDB.GetFormID(certificateRequest.CertificateTypeID);
                    ViewBag.FormId = uniqueKeyModel.FormID;
                    ViewBag.FormName = uniqueKeyModel.FormName;
                    certificateRequest.FormProcessID = formProcessID;
                    return View("UpdateDetails", certificateRequest);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");         
        }

        public void PreviewDocumentFile(int fileID)
        {
            try
            {
                AllFormsFilesModel allFormsModel = new CertificateRequestFormDB().GetEmployeeDocumentById(fileID);
                if (allFormsModel.FileID > 0)
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.AddHeader("content-disposition", "attachment; filename=" + allFormsModel.FileName);
                    Response.ContentType = allFormsModel.FileContentType;

                    this.Response.BinaryWrite(allFormsModel.DocumentFile);
                    this.Response.End();
                }
                else
                {
                    AllFormsFilesModel allFormsFilesModel = new AllFormsFilesDB().GetAllFormsFiles(fileID);
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.AddHeader("content-disposition", "attachment; filename=" + allFormsFilesModel.FileName);
                    Response.ContentType = allFormsFilesModel.FileContentType;
                    this.Response.BinaryWrite(allFormsFilesModel.DocumentFile);
                    this.Response.End();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CerficateRequestFormModel certificateModel = new CerficateRequestFormModel();
            certificateModel = formCertificateRequestDB.GetFormDetails(formProcessID);
            FormsUniqueKeyModel uniqueKeyModel = new FormsUniqueKeyModel();
            uniqueKeyModel = formCertificateRequestDB.GetFormID(certificateModel.CertificateTypeID);
            ViewBag.FormName = uniqueKeyModel.FormName;
            List<RequestFormsApproveModel> requestFormsApproveModelList = formCertificateRequestDB.GetAllRequestFormsApprovals(formProcessID);
            certificateModel.RequestFormsApproveModelList = requestFormsApproveModelList;                    
            certificateModel.FormProcessID = formProcessID;
            return View("ViewDetail", certificateModel);
        }
        public ActionResult IsWorkFlowExistsByCertificate(Int16 CertificateTypeID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormsUniqueKeyModel uniqueKeyModel = new FormsUniqueKeyModel();
                uniqueKeyModel = formCertificateRequestDB.GetFormID(CertificateTypeID);
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                FormsDB formsDB = new FormsDB();
                operationDetails = formsDB.IsWorkFlowExists(uniqueKeyModel.FormID, Convert.ToString(objUserContextViewModel.CompanyId), objUserContextViewModel.UserId);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckCertificateRequestFormValidation(int certificateTypeId)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormsUniqueKeyModel uniqueKeyModel = new FormsUniqueKeyModel();
                uniqueKeyModel = formCertificateRequestDB.GetFormID(certificateTypeId);
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                FormsDB formsDB = new FormsDB();
                operationDetails = formsDB.CheckFormLoadValidation(uniqueKeyModel.FormID, objUserContextViewModel.CompanyId, objUserContextViewModel.UserId);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}