﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class ReleaseR1RequestController : FormsController
    {
        ReleaseR1RequestDB releaseR1RequestDB;
        public ReleaseR1RequestController()
        {
            XMLLogFile = "LoggerReleaseR1Request.xml";
            releaseR1RequestDB = new ReleaseR1RequestDB();
        }
        // GET: ReleaseR1Request
        public ActionResult Index()
        {
            List<ReleaseR1RequestModel> listReleaseR1RequestModel = new List<ReleaseR1RequestModel>();
            listReleaseR1RequestModel = releaseR1RequestDB.GetEmployeeDetailOfR1Release(null);
            ViewBag.R1RequestList= new SelectList(listReleaseR1RequestModel, "R1RequestID", "R1RequestID");
            return View(listReleaseR1RequestModel);
        }
        public ActionResult GetR1ReleaseByR1RequestID(int? R1RequestID)
        {
            List<ReleaseR1RequestModel> listReleaseR1RequestModel = new List<ReleaseR1RequestModel>();
            listReleaseR1RequestModel = releaseR1RequestDB.GetEmployeeDetailOfR1Release(R1RequestID);                    
            return PartialView("_EmployeeReleaseList", listReleaseR1RequestModel);
        }      

        public ActionResult UpdateReleaseR1Request(int profileCreationID, string r1Releasecomments)
        {
            //objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];           
            int InsertedRowId = 0;
            InsertedRowId = releaseR1RequestDB.UpdateReleaseR1Request(profileCreationID, r1Releasecomments, objUserContextViewModel.UserId);
            return Json(new { InsertedRowId = InsertedRowId }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddReleaseComments()
        {
            return PartialView("_AddReleaseComments");
        }
    }
}