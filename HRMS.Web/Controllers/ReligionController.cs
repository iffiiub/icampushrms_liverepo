﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class ReligionController: BaseController
    {
        public ActionResult AddReligion()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DBHelper objDBHelper = new DBHelper();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            ObjSelectedList.Add(new SelectListItem { Text = "Christian", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "Islam", Value = "2" });
            ViewBag.ReligionCategory = ObjSelectedList;
            
            return View();
        }

        [HttpPost]
        public ActionResult AddReligion(ReligionModel objReligionModel)
        {
            DBHelper objDBHelper = new DBHelper();
            if (ModelState.IsValid)
            {
                ReligionDB objReligionModelDB = new ReligionDB();
                
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objReligionModelDB.InsertReligion(objReligionModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
                ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
                ObjSelectedList.Add(new SelectListItem { Text = "Christian", Value = "1" });
                ObjSelectedList.Add(new SelectListItem { Text = "Islam", Value = "2" });
                ViewBag.ReligionCategory = ObjSelectedList;
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                // return View(objReligionModel);
            }
        }
    }
}