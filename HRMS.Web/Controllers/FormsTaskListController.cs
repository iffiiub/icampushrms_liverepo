﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class FormsTaskListController : BaseController
    {
        // GET: FormsTaskList
        public ActionResult Index()
        {            
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<FormsUniqueKeyModel> formsUniqueKeyModelList = new List<FormsUniqueKeyModel>();
            formsUniqueKeyModelList = new FormsUniqueKeyDB().GetAllFormsUniqueKeyByUser(objUserContextViewModel.UserId);
            List<FormsTaskListViewModel> requestFormsApproveModelList = new FormsTaskListDB().GetAllRequestFormsApprovalsByUser(objUserContextViewModel.UserId);
            RequestFormsTaskListViewModel requestFormsTaskListViewModel = new RequestFormsTaskListViewModel();
            requestFormsTaskListViewModel.FormsUniqueKeyModelList = formsUniqueKeyModelList;
            requestFormsTaskListViewModel.FormsTaskViewModelList = requestFormsApproveModelList;
            ViewBag.UserId = objUserContextViewModel.UserId;

            UserRoleDB userRoleDB = new UserRoleDB();
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Where(s=>!string.IsNullOrEmpty(s)).Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;

            return View(requestFormsTaskListViewModel);
        }
    }
}