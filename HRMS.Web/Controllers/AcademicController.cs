﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class AcademicController : BaseController
    {
        public ActionResult Index()
        {
           CommonHelper.CommonHelper.GetEmployeeId(0);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);

            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }

            return View(employeeDetailsModelList);
        }

        public ActionResult GetAcademicList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }

            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<AcademicModel> objAcademicModelList = new List<AcademicModel>();
            AcademicDB objAcademicDB = new AcademicDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            CountryDB objCountryDB = new CountryDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objAcademicModelList = objAcademicDB.GetAcademicList(empid);
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objAcademicModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAcademic(" + item.AcademicID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewAcademic(" + item.AcademicID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteAcademic(" + item.AcademicID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              AcademicID = item.AcademicID,
                              EmployeeID = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName,
                              NameAsInDegree = item.NameAsInDegree,
                              StartDate = item.StartDate == "" ? "" : Convert.ToDateTime(item.StartDate).ToString("dd/MM/yyyy"),
                              EndDate = item.EndDate == "" ? "" : Convert.ToDateTime(item.EndDate).ToString("dd/MM/yyyy"),
                              HRQualificationID = objHRQualificationDB.GetAllHRQualification().Where(x => x.HRQualificationID == item.HRQualificationID).Select(x => x.HRQualificationName_1).SingleOrDefault(),
                              HRDegreeID = objHRDegreeDB.GetAllHRDegree().Where(x => x.HRDegreeID == item.HRDegreeID).Select(x => x.HRDegreeName_1).SingleOrDefault(),
                              HRInstituteID = objHRInstituteDB.GetAllHRInstitute().Where(x => x.HRInstituteID == item.HRInstituteID).Select(x => x.HRInstituteName_1).SingleOrDefault(),
                              CountryID = objCountryDB.GetAllContries().Where(x => x.CountryId == item.CountryID).Select(x => x.CountryName).SingleOrDefault(),
                              HRQualificationName_1 = item.HRQualificationName_1,
                              HRDegreeName_1 = item.HRDegreeName_1,
                              HRInstituteName_1 = item.HRInstituteName_1,
                              CountryName_1 = item.CountryName_1,
                              MinistryApproved = item.MinistryApproved
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult GetCertificateList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<CertificateModel> objCertificateList = new List<CertificateModel>();
            AcademicDB objAcademicDB = new AcademicDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            CountryDB objCountryDB = new CountryDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objCertificateList = objAcademicDB.GetcertificateList(empid);
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objCertificateList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditCertificate(" + item.CertificateID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewCertificate(" + item.CertificateID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteCertificate(" + item.CertificateID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              CertificateID = item.CertificateID,
                              CertificationName = item.CertificationName,
                              EmployeeID = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName,
                              NameAsInDegree = item.NameAsInDegree,
                              CertificationDate = CommonDB.GetFormattedDate_DDMMYYYY(item.CertificationDate),
                              HRInstituteID = objHRInstituteDB.GetAllHRInstitute().Where(x => x.HRInstituteID == item.HRInstituteID).Select(x => x.HRInstituteName_1).SingleOrDefault(),
                              CountryID = objCountryDB.GetAllContries().Where(x => x.CountryId == item.CountryID).Select(x => x.CountryName).SingleOrDefault(),
                              HRInstituteName_1 = item.HRInstituteName_1,
                              CountryName_1 = item.CountryName_1,

                              MinistryApproved = item.MinistryApproved
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult AddAcademic()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            CountryDB objCountryDB = new CountryDB();

            ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");
            ViewBag.Degree = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1");
            ViewBag.Qualification = new SelectList(objHRQualificationDB.GetAllHRQualification(), "HRQualificationID", "HRQualificationName_1");
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            // ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");

            if (Session["EmployeeListID"] != null)
            {

                ViewBag.empid = Session["EmployeeListID"].ToString();
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddAcademic(AcademicModel objAcademicModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //  objAcademicModel.EmployeeID = objUserContextViewModel.UserId;
                AcademicDB objAcademicDB = new AcademicDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objAcademicDB.InsertAcademic(objAcademicModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                EmployeeDB objEmployeeDB = new EmployeeDB();
                HRInstituteDB objHRInstituteDB = new HRInstituteDB();
                HRDegreeDB objHRDegreeDB = new HRDegreeDB();
                HRQualificationDB objHRQualificationDB = new HRQualificationDB();
                CountryDB objCountryDB = new CountryDB();

                ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");

                ViewBag.Degree = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1");
                ViewBag.Qualification = new SelectList(objHRQualificationDB.GetAllHRQualification(), "HRQualificationID", "HRQualificationName_1");
                ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
                //   ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;

                ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditAcademic(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            CountryDB objCountryDB = new CountryDB();

            ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");

            ViewBag.Degree = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1");
            ViewBag.Qualification = new SelectList(objHRQualificationDB.GetAllHRQualification(), "HRQualificationID", "HRQualificationName_1");
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            //   ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;

            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");
            AcademicDB objAcademicModelDB = new AcademicDB();
            AcademicModel objAcademicModel = new AcademicModel();
            objAcademicModel = objAcademicModelDB.AcademicById(id);
            if (Session["EmployeeListID"] != null)
            {

                ViewBag.empid = Session["EmployeeListID"].ToString();
            }


            return View(objAcademicModel);
        }

        [HttpPost]
        public JsonResult EditAcademic(AcademicModel objAcademicModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //  objAcademicModel.EmployeeID = objUserContextViewModel.UserId;
                AcademicDB objAcademicModelDB = new AcademicDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objAcademicModelDB.UpdateAcademic(objAcademicModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                EmployeeDB objEmployeeDB = new EmployeeDB();
                HRInstituteDB objHRInstituteDB = new HRInstituteDB();
                HRDegreeDB objHRDegreeDB = new HRDegreeDB();
                HRQualificationDB objHRQualificationDB = new HRQualificationDB();
                CountryDB objCountryDB = new CountryDB();

                ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");

                ViewBag.Degree = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1");
                ViewBag.Qualification = new SelectList(objHRQualificationDB.GetAllHRQualification(), "HRQualificationID", "HRQualificationName_1");
                ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
                //     ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;

                ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewAcademic(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            AcademicDB objAcademicModelDB = new AcademicDB();
            AcademicModel objAcademicModel = new AcademicModel();
            objAcademicModel = objAcademicModelDB.AcademicById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            CountryDB objCountryDB = new CountryDB();

            ViewBag.Institute = objAcademicModel.HRInstituteID > 0 ? objHRInstituteDB.GetAllHRInstitute().Where(a => a.HRInstituteID == objAcademicModel.HRInstituteID).FirstOrDefault().HRInstituteName_1 : "";
            ViewBag.Degree = objAcademicModel.HRDegreeID > 0 ? objHRDegreeDB.GetAllHRDegree().Where(a => a.HRDegreeID == objAcademicModel.HRDegreeID).FirstOrDefault().HRDegreeName_1 : "";
            ViewBag.Qualification = objAcademicModel.HRQualificationID > 0 ? objHRQualificationDB.GetAllHRQualification().Where(a => a.HRQualificationID == objAcademicModel.HRQualificationID).FirstOrDefault().HRQualificationName_1 : "";

            ViewBag.Country = objAcademicModel.CountryID > 0 ? objCountryDB.GetAllContries().Where(a => a.CountryId == objAcademicModel.CountryID).FirstOrDefault().CountryName : "";
            ViewBag.Employee = objAcademicModel.EmployeeID > 0 ? objEmployeeDB.GetEmployeeList().Where(a => a.EmployeeId == objAcademicModel.EmployeeID).FirstOrDefault().FullName : "";
            objAcademicModel.StartDate = objAcademicModel.StartDate.Split(' ')[0].ToString();
            objAcademicModel.EndDate = objAcademicModel.EndDate.Split(' ')[0].ToString();

            return View(objAcademicModel);
        }

        public ActionResult DeleteAcademic(int id)
        {
            AcademicDB objAcademicDB = new AcademicDB();
            objAcademicDB.DeleteAcademicById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCertificates()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            CountryDB objCountryDB = new CountryDB();

            ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");
            ViewBag.Degree = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1");
            ViewBag.Qualification = new SelectList(objHRQualificationDB.GetAllHRQualification(), "HRQualificationID", "HRQualificationName_1");
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            // ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");

            if (Session["EmployeeListID"] != null)
            {

                ViewBag.empid = Session["EmployeeListID"].ToString();
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddCertificates(CertificateModel objCertificateModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //  objAcademicModel.EmployeeID = objUserContextViewModel.UserId;
                AcademicDB objAcademicDB = new AcademicDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objAcademicDB.InsertCertificate(objCertificateModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                EmployeeDB objEmployeeDB = new EmployeeDB();
                HRInstituteDB objHRInstituteDB = new HRInstituteDB();
                CountryDB objCountryDB = new CountryDB();
                ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");
                ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditCertificates(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            CountryDB objCountryDB = new CountryDB();
            ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            AcademicDB objAcademicModelDB = new AcademicDB();
            CertificateModel objCertificateModel = new CertificateModel();
            objCertificateModel = objAcademicModelDB.GetCertificateById(id);
            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }

            return View(objCertificateModel);
        }

        [HttpPost]
        public JsonResult EditCertificates(CertificateModel objCertificateModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //  objAcademicModel.EmployeeID = objUserContextViewModel.UserId;
                AcademicDB objAcademicModelDB = new AcademicDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objAcademicModelDB.UpdateCertificate(objCertificateModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                EmployeeDB objEmployeeDB = new EmployeeDB();
                HRInstituteDB objHRInstituteDB = new HRInstituteDB();
                HRDegreeDB objHRDegreeDB = new HRDegreeDB();
                HRQualificationDB objHRQualificationDB = new HRQualificationDB();
                CountryDB objCountryDB = new CountryDB();
                ViewBag.Institute = new SelectList(objHRInstituteDB.GetAllHRInstitute(), "HRInstituteID", "HRInstituteName_1");
                ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewCertificateDetails(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            AcademicDB objAcademicModelDB = new AcademicDB();
            CertificateModel objCertificateModel = new CertificateModel();
            objCertificateModel = objAcademicModelDB.GetCertificateById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            CountryDB objCountryDB = new CountryDB();

            ViewBag.Institute = objCertificateModel.HRInstituteID > 0 ? objHRInstituteDB.GetAllHRInstitute().Where(a => a.HRInstituteID == objCertificateModel.HRInstituteID).FirstOrDefault().HRInstituteName_1 : "";
            ViewBag.Country = objCertificateModel.CountryID > 0 ? objCountryDB.GetAllContries().Where(a => a.CountryId == objCertificateModel.CountryID).FirstOrDefault().CountryName : "";
            ViewBag.Employee = objCertificateModel.EmployeeID > 0 ? objEmployeeDB.GetEmployeeList().Where(a => a.EmployeeId == objCertificateModel.EmployeeID).FirstOrDefault().FullName : "";

            objCertificateModel.CertificationDate = objCertificateModel.CertificationDate.Split(' ')[0].ToString();
            return View(objCertificateModel);
        }

        public ActionResult DeleteCertificate(int id)
        {
            AcademicDB objAcademicDB = new AcademicDB();
            objAcademicDB.DeletecertificateById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }
        #region Qualification

        public JsonResult GetAllHRQualification()
        {
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            List<HRQualificationModel> HRQualificationModelList = new List<HRQualificationModel>();
            HRQualificationModelList = objHRQualificationDB.GetAllHRQualification();
            return Json(HRQualificationModelList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddQualification()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddQualification(HRQualificationModel objQualificationModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                HRQualificationDB objHRQualificationDB = new HRQualificationDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objHRQualificationDB.InsertHRQualification(objQualificationModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Degree

        public JsonResult GetAllHRDegree()
        {
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            List<HRDegreeModel> HRDegreeModelList = new List<HRDegreeModel>();
            HRDegreeModelList = objHRDegreeDB.GetAllHRDegree();
            return Json(HRDegreeModelList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddDegree()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddDegree(HRDegreeModel objDegreeModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                HRDegreeDB objHRDegreeDB = new HRDegreeDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objHRDegreeDB.InsertHRDegree(objDegreeModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Institute

        public JsonResult GetAllHRInstitute()
        {
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            List<HRInstituteModel> HRInstituteModelList = new List<HRInstituteModel>();
            HRInstituteModelList = objHRInstituteDB.GetAllHRInstitute();
            return Json(HRInstituteModelList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInstitute()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddInstitute(HRInstituteModel objInstituteModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                HRInstituteDB objHRInstituteDB = new HRInstituteDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objHRInstituteDB.InsertHRInstitute(objInstituteModel);                
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult CheckRelevant(int EmployeeID,int AcademicId)
        {
            
            AcademicDB objAcademicDB = new AcademicDB();
            bool isRelevant = objAcademicDB.CheckRelevant(EmployeeID, AcademicId);

            if (isRelevant)
            {
                return Json(new { result = "success", resultMessage = "Relevant academic is already exists, If you save record then other academic will remove from relevant." });
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Please select an employee." });
            }

        }
    }
}