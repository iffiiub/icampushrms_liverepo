﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class ProfessionalTrainingController : BaseController
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GetEmployeeTrainingList()
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);           
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat(); 
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int CompanyID = objUserContextViewModel.CompanyId;
            List<EmployeeTrainingModel> objEmployeeTrainingModelList = new List<EmployeeTrainingModel>();
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            objEmployeeTrainingModelList = objEmployeeTrainingDB.GetTrainingDataList();
            //---------------------------------------------

            var vList = new object();

            try
            {
                vList = new
                {
                    aaData = (from item in objEmployeeTrainingModelList
                              select new
                              {
                                  Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.EmployeeTrainingID.ToString() + ")' title='Update' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' style='display:none;' onclick='trainingDetails(" + item.EmployeeID.ToString() + ")' title='Ongoing/History Training Details' ><i class='fa fa-pencil'></i>Training Details</a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.EmployeeTrainingID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm'   onclick='ViewChannel(" + item.EmployeeTrainingID.ToString() + ")' title='View Employee Training Details' ><i class='fa fa-eye'></i></a>",
                                  CourseTitleName = item.CourseTitleName,
                                  Cost = Convert.ToDecimal(item.Cost).ToString(AmountFormat),
                                  Deductable = item.Deductable ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.Deductable + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.Deductable + "'/>",
                                  Trainee = item.Trainee,
                                  StartDate = item.StartDate,
                                  EndDate = item.EndDate,
                                  StartTime = item.StartTime,
                                  EndTime = item.EndTime,
                                  Location = item.Location,
                                  CityName = item.CityName,
                                  CountryName = item.CountryName,
                                  Allowance = item.Allowance,
                                  Results = item.Results,
                                  Note = item.Note,
                                  isComplete = item.isComplete ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.isComplete + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.isComplete + "'/>",
                                  Total = item.Total
                              }).ToArray()
                };
            }
            catch (Exception ex)
            {

                throw;
            }
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        [HttpGet]
        public ActionResult GetCityByCountryId(string countryId)
        {
            CityDB objCity = new CityDB();
            if (String.IsNullOrEmpty(countryId))
            {
                throw new ArgumentNullException("countryId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(countryId, out id);
            var cities = objCity.GetCityByCountryId(countryId).OrderBy(x => x.CityName).AsEnumerable();
            var result = (from s in cities
                          select new
                          {
                              id = s.CityId,
                              name = s.CityName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCourse()
        {
            //List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            List<EmployeeTrainingModel> employeeTrainingList = objEmployeeTrainingDB.GetAllCourse();
            return Json(employeeTrainingList.OrderBy(x => x.CourseTitleName).AsEnumerable(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddEmployeeTraining(int ID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.EmployeeID = objUserContextViewModel.UserId;

            CountryDB objCountryDB = new CountryDB();
            CityDB citiDB = new CityDB();
            DepartmentDB departmentDB = new DepartmentDB();

            //Get Country List
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "" });
            foreach (var m in objCountryDB.GetAllContries())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });

            }
            ViewBag.CountryList = ObjSelectedList;

            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            //ViewBag.Course = new SelectList(objEmployeeTrainingDB.GetAllCourse(), "HRCourseID", "HRCourseName_1");
            EmployeeTrainingModel objEmployeeTrainingModel = new EmployeeTrainingModel();

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Course", Value = "" });
            foreach (var m in objEmployeeTrainingDB.GetAllCourse())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CourseTitleName, Value = m.CourseTitle.ToString() });

            }
            ViewBag.Course = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "" });
            if (ID != 0)
            {
                objEmployeeTrainingModel = objEmployeeTrainingDB.GetEmployeeTrainingById(ID);
                foreach (var m in citiDB.GetCityByCountryId(objEmployeeTrainingModel.CountryId.ToString()))
                {
                    if (objEmployeeTrainingModel.CityId == m.CityId)
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString(), Selected = true });
                    }
                    else
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });
                    }

                }
                objEmployeeTrainingModel.employeeDetailsModelList = objEmployeeTrainingDB.GetEmployeeGridList(objUserContextViewModel.CompanyId, objEmployeeTrainingModel.EmployeeTrainingID, -1);
            }
            else
            {

                objEmployeeTrainingModel.employeeDetailsModelList = objEmployeeTrainingDB.GetEmployeeGridList(objUserContextViewModel.CompanyId, 0, -1);
            }
            ViewBag.CityList = ObjSelectedList;
                                  
            int UserId = objUserContextViewModel.UserId;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Department", Value = "0" });
            foreach (var m in departmentDB.GetDepartmentListWithPermission(UserId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.DepartmentName_1, Value = m.DepartmentId.ToString() });

            }
            ViewBag.Department = ObjSelectedList;
            return View(objEmployeeTrainingModel);
        }

        [HttpPost]
        public ActionResult AddNewCourse(CourseModel CourseModel)
        {
            CourseDB objCourseDB = new CourseDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objCourseDB.AddCourse(CourseModel);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ViewCourse()
        {
            CourseModel CourseModel = new CourseModel();
            return PartialView("_AddCourse", CourseModel);
        }

        [HttpPost]
        public ActionResult SaveEmployeeTraining(EmployeeTrainingModel objEmployeeTrainingModel, string SelecteFeild)
        {
            if (ModelState.IsValid)
            {
                List<EmployeeProfessionalTrainingModel> tainingUpdateModel = new List<EmployeeProfessionalTrainingModel>();
                tainingUpdateModel = JsonConvert.DeserializeObject<List<EmployeeProfessionalTrainingModel>>(SelecteFeild);
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objEmployeeTrainingModel.EmployeeID = objUserContextViewModel.UserId;

                if (objEmployeeTrainingModel.EmployeeTrainingID > 0)
                {
                    objOperationDetails = objEmployeeTrainingDB.UpdateEmployeeTraining(objEmployeeTrainingModel);
                    if (objOperationDetails.Success)
                    {
                        objOperationDetails = objEmployeeTrainingDB.BulkCRUDEmployeeTrainingProfessional(tainingUpdateModel, objEmployeeTrainingModel.EmployeeTrainingID);
                        objOperationDetails.Message = "Employee training details updated successfully";
                    }
                }
                else
                {
                    objOperationDetails = objEmployeeTrainingDB.InsertEmployeeTraining(objEmployeeTrainingModel);
                    if (objOperationDetails.Success)
                    {
                        objOperationDetails = objEmployeeTrainingDB.BulkCRUDEmployeeTrainingProfessional(tainingUpdateModel, objOperationDetails.InsertedRowId);
                        objOperationDetails.Message = "Employee training details save successfully";
                    }
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult UpdateDepartmentEmployeeByEmployeeId(List<string> EmployeeList, int EmployeeTrainingId)
        {
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            OperationDetails objOperationDetails = new OperationDetails();

            if (EmployeeList != null)
            {
                if (EmployeeList.Count > 0)
                {
                    for (int i = 0; i < EmployeeList.Count; i++)
                    {
                        objOperationDetails = objEmployeeTrainingDB.UpdateEmployeeTrainingByEmployeeId(EmployeeList[i], EmployeeTrainingId);

                    }

                }
            }
            return Json(new { Result = String.Format("Success") });
        }

        public JsonResult DeleteProfessionalDevelopment(int id)
        {
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objEmployeeTrainingDB.DeleteProfessionalDevelopment(id);
            return Json(new { Result = String.Format("Success") });
        }

        [HttpPost]
        public JsonResult DeleteEmployeeTraining(int Id)
        {
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objEmployeeTrainingDB.DeleteEmployeeTraining(Id);
            return Json(new { Result = String.Format("Success") });
        }      

        public ActionResult GetOngoingTrainingList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, int EmployeeID = 0)
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------
            //-------------Data Objects--------------------
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyID = objUserContextViewModel.CompanyId;
            List<EmployeeTrainingModel> objEmployeeTrainingModelList = new List<EmployeeTrainingModel>();
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            objEmployeeTrainingModelList = objEmployeeTrainingDB.GetOnGoingTrainingList(EmployeeID);
            //---------------------------------------------
            var vList = new object();
            vList = new
            {
                aaData = (from item in objEmployeeTrainingModelList
                          select new
                          {
                              //Actions = "<a class='btn btn-primary' onclick='EditChannel(" + item.EmployeeTrainingID.ToString() + ")' title='Update' ><i class='fa fa-pencil'></i> Update</a><a class='btn btn-primary' onclick='trainingDetails(" + item.EmployeeID.ToString() + ")' title='Ongoing/History Training Details' ><i class='fa fa-pencil'></i>Training Details</a><a class='btn btn-primary' style='display:none' onclick='DeleteHealth(" + item.EmployeeTrainingID.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a><a class='btn btn-primary' style='display:none' onclick='ViewHealth(" + item.EmployeeTrainingID.ToString() + ")' title='View Details' ><i class='fa fa-pencil'></i>View Details</a>",

                              EmployeeID = item.EmployeeID,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeTrainingID = item.EmployeeTrainingID,
                              Fullname = item.Fullname,
                              isAssign = item.isAssign,
                              CourseTitleName = item.CourseTitleName,
                              StartDate = item.StartDate,
                              EndDate = item.EndDate,
                              Trainee = item.Trainee
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTrainingHistoryList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "DESC", string mDataProp_1 = "StartDate", int iSortCol_0 = 0, int EmployeeID = 0)
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------




            //-------------Data Objects--------------------

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int CompanyID = objUserContextViewModel.CompanyId;
            List<EmployeeTrainingModel> objEmployeeTrainingModelList = new List<EmployeeTrainingModel>();
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            objEmployeeTrainingModelList = objEmployeeTrainingDB.GetHistoryTrainingList(EmployeeID);
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objEmployeeTrainingModelList
                          select new
                          {
                              //Actions = "<a class='btn btn-primary' onclick='EditChannel(" + item.EmployeeTrainingID.ToString() + ")' title='Update' ><i class='fa fa-pencil'></i> Update</a><a class='btn btn-primary' onclick='trainingDetails(" + item.EmployeeID.ToString() + ")' title='Ongoing/History Training Details' ><i class='fa fa-pencil'></i>Training Details</a><a class='btn btn-primary' style='display:none' onclick='DeleteHealth(" + item.EmployeeTrainingID.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a><a class='btn btn-primary' style='display:none' onclick='ViewHealth(" + item.EmployeeTrainingID.ToString() + ")' title='View Details' ><i class='fa fa-pencil'></i>View Details</a>",

                              EmployeeID = item.EmployeeID,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeTrainingID = item.EmployeeTrainingID,
                              Fullname = item.Fullname,
                              isAssign = item.isAssign,
                              CourseTitleName = item.CourseTitleName,
                              StartDate = item.StartDate,
                              EndDate = item.EndDate,
                              Trainee = item.Trainee
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewTrainingDetails(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeTrainingModel objEmployeeTrainingModel = new EmployeeTrainingModel();
            if (Id != 0)
            {
                EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
                objEmployeeTrainingModel = objEmployeeTrainingDB.GetEmployeeTrainingById(Id);
                //foreach (var m in citiDB.GetCityByCountryId(objEmployeeTrainingModel.CountryId.ToString()))
                //{
                //    if (objEmployeeTrainingModel.CityId == m.CityId)
                //    {
                //        ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString(), Selected = true });
                //    }
                //    else
                //    {
                //        ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });
                //    }

                //}
                objEmployeeTrainingModel.employeeDetailsModelList = objEmployeeTrainingDB.GetEmployeeGridList(objUserContextViewModel.CompanyId, objEmployeeTrainingModel.EmployeeTrainingID, -1);
            }

            objEmployeeTrainingModel.EmployeeID = Id;
            return View(objEmployeeTrainingModel);
        }

        public ActionResult GetEmployeeByDepartment(int TrainingId, int DepartmentId)
        {
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (DepartmentId == 0)
                return Json(objEmployeeTrainingDB.GetEmployeeGridList(objUserContextViewModel.CompanyId, TrainingId, -1), JsonRequestBehavior.AllowGet);
            else
                return Json(objEmployeeTrainingDB.GetEmployeeGridList(objUserContextViewModel.CompanyId, TrainingId, DepartmentId), JsonRequestBehavior.AllowGet);
        }

    }
}