﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class EmployeeLeaveRejoiningController : FormsController
    {
        // GET: EmployeeLeaveRejoining
        LeaveRejoingDB objLeaveRejoingDB;
        public EmployeeLeaveRejoiningController()
        {
            XMLLogFile = "LoggerEmployeeLeaveRejoining.xml";
            objLeaveRejoingDB = new LeaveRejoingDB();
        }
        public ActionResult Index()
        {
            //  int formProcessID = GetFormProcessSessionID();
            int LeaveID = Session["LeaveID"] != null ? Convert.ToInt32(Session["LeaveID"]) : 0;
            if (LeaveID <= 0)
                return RedirectToAction("Index", "Home");
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveRejoiningModel objLeaveRejoiningModel = new LeaveRejoiningModel();
            objLeaveRejoingDB = new LeaveRejoingDB();
            int ID = objLeaveRejoingDB.CheckEmployeeLeavesReJoiningExist(LeaveID);
            if (ID > 0)
            {
                return RedirectToAction("Index", "Home");
            }
            objLeaveRejoiningModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            ViewBag.FormId = 7;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            objLeaveRejoiningModel.CreatedOn = DateTime.Today.Date.ToString();
            objLeaveRejoiningModel.LeaveRequestDetailModelList = objLeaveRejoingDB.GetLeaveRequestDetail(LeaveID);
            //objLeaveRejoiningModel.RequestDate = HRMS.DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(DateTime.Today.Date.ToString());
            objLeaveRejoiningModel.RequestDate = DateTime.Now.ToString(HRMSDateFormat);
            objLeaveRejoiningModel.ReJoiningDate = objLeaveRejoiningModel.LeaveRequestDetailModelList[0].IsHalfDayLeave ? objLeaveRejoiningModel.LeaveRequestDetailModelList[0].LeaveEndDate : string.Empty;
            objLeaveRejoiningModel.ID_LeaveRequest = LeaveID;
            objLeaveRejoiningModel.LeaveRequestFormRequestID = objLeaveRejoiningModel.LeaveRequestDetailModelList[0].RequestID;
            objLeaveRejoiningModel.EmployeeID = objLeaveRejoiningModel.LeaveRequestDetailModelList[0].EmployeeID;
            ViewBag.VacationTypeList = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(objUserContextViewModel.UserId), "id", "text", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].VacationTypeID);
            ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].Destination);
            objLeaveRejoiningModel.IsAddMode = true;
            // objLeaveRejoiningModel.FormProcessID = formProcessID;
            return View(objLeaveRejoiningModel);
        }
        public ActionResult Edit()
        {
            int formProcessID = GetFormProcessSessionID();
            objLeaveRejoingDB = new LeaveRejoingDB();
            LeaveRejoiningModel objLeaveRejoiningModel = new LeaveRejoiningModel();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.FormId = 7;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;

            //get current/pending approver group and employee
            RequestFormsApproveModel requestFormsApproveModel = objLeaveRejoingDB.GetPendingFormsApproval(formProcessID);
            RequestFormsProcessModel objRequestFormsProcessModel = objLeaveRejoingDB.GetRequestFormsProcess(formProcessID);
            objLeaveRejoiningModel = objLeaveRejoingDB.GetEmployeeLeavesReJoining(objRequestFormsProcessModel.FormInstanceID);
            objLeaveRejoiningModel.RequestFormsApproveModelList = new FormsDB().GetAllRequestFormsApprovals(formProcessID);
            objLeaveRejoiningModel.LeaveRequestDetailModelList = objLeaveRejoingDB.GetLeaveRequestDetail(objLeaveRejoiningModel.ID_LeaveRequest);
            //if login user is the present approver then can see form in edit mode 
            if (objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID || objUserContextViewModel.UserId == 0
                || (objLeaveRejoiningModel.ReqStatusID == (int)RequestStatus.Rejected && objLeaveRejoiningModel.RequesterEmployeeID == objUserContextViewModel.UserId))
            {

                //If form is pending then only show for edit
                if (objLeaveRejoiningModel.ReqStatusID == (int)RequestStatus.Pending
                    || (objLeaveRejoiningModel.ReqStatusID == (int)RequestStatus.Rejected && objLeaveRejoiningModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
                    ViewBag.FormProcessID = formProcessID.ToString();
                    objLeaveRejoiningModel.FormProcessID = formProcessID;
                    ViewBag.VacationTypeList = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(objLeaveRejoiningModel.RequesterEmployeeID), "id", "text", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].VacationTypeID);
                    ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].Destination);

                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
            {
                return RedirectToAction("ViewDetails");
            }
            return View("Edit", objLeaveRejoiningModel);
        }
        public ActionResult UpdateDetails()
        {
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            objLeaveRejoingDB = new LeaveRejoingDB();
            LeaveRejoiningModel objLeaveRejoiningModel = new LeaveRejoiningModel();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.FormId = 7;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            
            RequestFormsApproveModel requestFormsApproveModel = objLeaveRejoingDB.GetPendingFormsApproval(formProcessID);
            RequestFormsProcessModel objRequestFormsProcessModel = objLeaveRejoingDB.GetRequestFormsProcess(formProcessID);
            objLeaveRejoiningModel = objLeaveRejoingDB.GetEmployeeLeavesReJoining(objRequestFormsProcessModel.FormInstanceID);
            objLeaveRejoiningModel.RequestFormsApproveModelList = new FormsDB().GetAllRequestFormsApprovals(formProcessID);
            objLeaveRejoiningModel.LeaveRequestDetailModelList = objLeaveRejoingDB.GetLeaveRequestDetail(objLeaveRejoiningModel.ID_LeaveRequest);
           
            if (formProcessID > 0)
            {
                if (objLeaveRejoiningModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
                    ViewBag.FormProcessID = formProcessID.ToString();
                    objLeaveRejoiningModel.FormProcessID = formProcessID;
                    ViewBag.VacationTypeList = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(objLeaveRejoiningModel.RequesterEmployeeID), "id", "text", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].VacationTypeID);
                    ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].Destination);
                    return View(objLeaveRejoiningModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("Index");           
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            LeaveRejoiningModel objLeaveRejoiningModel = new LeaveRejoiningModel();
            objLeaveRejoingDB = new LeaveRejoingDB();

            RequestFormsProcessModel objRequestFormsProcessModel = objLeaveRejoingDB.GetRequestFormsProcess(formProcessID);
            objLeaveRejoiningModel = objLeaveRejoingDB.GetEmployeeLeavesReJoining(objRequestFormsProcessModel.FormInstanceID);
            objLeaveRejoiningModel.LeaveRequestDetailModelList = objLeaveRejoingDB.GetLeaveRequestDetail(objLeaveRejoiningModel.ID_LeaveRequest);
            ViewBag.VacationTypeList = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(objLeaveRejoiningModel.RequesterEmployeeID), "id", "text", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].VacationTypeID);
            ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName", objLeaveRejoiningModel.LeaveRequestDetailModelList[0].Destination);
            ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
            objLeaveRejoiningModel.FormProcessID = formProcessID;
            return View(objLeaveRejoiningModel);
        }
        [HttpPost]
        public ActionResult SaveLeaveRejoiningRequest(string leaveRejoiningModel)
        {
            LeaveRejoiningModel objLeaveRejoiningModel = JsonConvert.DeserializeObject<LeaveRejoiningModel>(leaveRejoiningModel);
            OperationDetails operationDetails = new OperationDetails();
            objLeaveRejoingDB = new LeaveRejoingDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objLeaveRejoiningModel.CompanyID = objUserContextViewModel.CompanyId;
            objLeaveRejoiningModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            RequestFormsProcessModel objRequestFormsProcessModel = objLeaveRejoingDB.SaveLeaveRejoiningRequest(objLeaveRejoiningModel, objUserContextViewModel.UserId);
            if (objRequestFormsProcessModel != null)
            {
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                requestFormsApproverEmailModelList = objLeaveRejoingDB.GetApproverEmailList(objRequestFormsProcessModel.FormProcessID.ToString(), objUserContextViewModel.UserId);
                SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);

                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateLeaveRejoiningRequest(string leaveRejoiningModel)
        {
            LeaveRejoiningModel objLeaveRejoiningModel = JsonConvert.DeserializeObject<LeaveRejoiningModel>(leaveRejoiningModel);
            OperationDetails operationDetails = new OperationDetails();
            objLeaveRejoingDB = new LeaveRejoingDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            RequestFormsApproveModel requestFormsApproveModel = objLeaveRejoingDB.GetPendingFormsApproval(objLeaveRejoiningModel.FormProcessID);
            if ((objLeaveRejoiningModel.ReqStatusID == (int)RequestStatus.Rejected && objLeaveRejoiningModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                || isEditRequestFromAllRequests)
            {
                operationDetails = objLeaveRejoingDB.UpdateLeaveRejoiningRequest(objLeaveRejoiningModel, objUserContextViewModel.UserId);
                if (operationDetails.Success)
                {
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = objLeaveRejoingDB.GetApproverEmailList(objLeaveRejoiningModel.FormProcessID.ToString(), objLeaveRejoiningModel.RequesterEmployeeID);
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while Saving Details";
                    operationDetails.CssClass = "error";
                }
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "No permissions to update.";
                operationDetails.CssClass = "error";
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }

                        //Add any logic to be done once approval process is completed
                        //Credit/Debit Leave Logic As per Early / Late joining Employee
                        objLeaveRejoingDB = new LeaveRejoingDB();
                        LeaveRejoiningModel objLeaveRejoiningModel = new LeaveRejoiningModel();
                        RequestFormsProcessModel objRequestFormsProcessModel = objLeaveRejoingDB.GetRequestFormsProcess(formProcessID);
                        objLeaveRejoiningModel = objLeaveRejoingDB.GetEmployeeLeavesReJoining(objRequestFormsProcessModel.FormInstanceID);
                        if (objLeaveRejoiningModel.LeavesToAdjust != 0)
                        {
                            objLeaveRejoiningModel = objLeaveRejoingDB.GetEmployeeLeavesReJoining(objRequestFormsProcessModel.FormInstanceID);
                            objLeaveRejoiningModel.LeaveRequestDetailModelList = objLeaveRejoingDB.GetLeaveRequestDetail(objLeaveRejoiningModel.ID_LeaveRequest);
                            //Leave to adjust logic calculation , before it was calculating to adjust from leave days,now changed to find from balance days
                            objLeaveRejoiningModel.LeavesToAdjust = 0 - objLeaveRejoiningModel.LeavesToAdjust;
                            objLeaveRejoingDB.SaveCreditDebitLeavesReJoiningEmployee(formProcessID, objLeaveRejoiningModel.LeavesToAdjust, objUserContextViewModel.UserId);
                        }
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}