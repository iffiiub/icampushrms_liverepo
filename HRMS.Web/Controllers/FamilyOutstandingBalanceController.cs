﻿using HRMS.DataAccess;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class FamilyOutstandingBalanceController : BaseController
    {
        // GET: FamilyOutstandingBalance
        public ActionResult Index(int? EmployeeId)
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList = new List<SelectListItem>();

            foreach (var items in new PayCycleDB().GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                };
                ObjSelectedList.Add(selectListItem);
            }
            ViewBag.CycleList = ObjSelectedList;
            ViewBag.EmployeeId = EmployeeId;
            return View();
        }

        public ActionResult FamilyOutStandingBalance(int? EmployeeId)
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList = new List<SelectListItem>();

            foreach (var items in new PayCycleDB().GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                };
                ObjSelectedList.Add(selectListItem);
            }
            ViewBag.CycleList = ObjSelectedList;
            ViewBag.EmployeeId = EmployeeId;
            return View();
        }

        public ActionResult GetFamilyOutstanding(int? EmployeeId)
        {
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            List<FamilyDetails> lstFamilyOstDetails = new List<FamilyDetails>();
            DeductionDB objDeductionDB = new DeductionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            lstFamilyOstDetails = objDeductionDB.GetFamilyOutstandingDetails(EmployeeId, UserId);
            string Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm custom-margin' onclick='CreateDeduction(employeeId)' title='Generate Deductions' ><i class='fa fa-bars'></i></a>";
            string NonEditedActins = "<a disabled class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='CreateDeduction(employeeId)' title='Generate Deductions' ><i class='fa fa-bars'></i></a>";
            var vList = new
            {
                aaData = (from item in lstFamilyOstDetails
                          select new
                          {
                              FamilyId = item.FamilyID,
                              EmployeeAlternativeId = item.EmpAltId,
                              EmployeeName = item.EmployeeName,
                              FamilyName = item.FamilyName_1,
                              FatherName = item.FatherName_1,
                              Balance = item.OutstandingBalance.ToString(AmountFormat),
                              DeductedAmount = item.DeductedAmount.ToString(AmountFormat),
                              Action = item.IsOSTDeducted == true ? NonEditedActins.Replace("employeeId", item.EmployeeId.ToString()) : Actions.Replace("employeeId", item.EmployeeId.ToString())
                          }).ToArray(),
                recordsTotal = lstFamilyOstDetails.Count,
                recordsFiltered = lstFamilyOstDetails.Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddFamilyOSTtoDeduction(int EmployeeId)
        {
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            PayDeductionModel payDeductionModel = new PayDeductionModel();
            DeductionDB deductionDB = new DeductionDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<FamilyDetails> lstFamilyOstDetails = new List<FamilyDetails>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            lstFamilyOstDetails = deductionDB.GetFamilyOutstandingDetails(EmployeeId, UserId);
            FamilyDetails objFamilyDetails = new FamilyDetails();
            objFamilyDetails = lstFamilyOstDetails.FirstOrDefault();
            payDeductionModel.EmployeeID = objFamilyDetails.EmployeeId;
            payDeductionModel.Amount = objFamilyDetails.OutstandingBalance;
            payDeductionModel.PaidCycle = 1;
            List<PayDeductionTypeModel> lstPayDeduction = deductionDB.GetAllPayDeductionType().Where(x => x.DeductionTypeID > 0).ToList();
            ViewBag.DeductionType = new SelectList(lstPayDeduction.Where(m => m.IsFamilyBalanceDeduction == true), "DeductionTypeID", "DeductionTypeName_1");
            Employee objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployeeId);
            ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + " " + objEmployee.employeeDetailsModel.SurName_1;
            ViewBag.Positon = objEmployee.employmentInformation.PositionName;
            ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
            ViewBag.EmployeeIDNo = EmployeeId;
            payDeductionModel.DeductionTypeID = lstPayDeduction.Where(m => m.IsFamilyBalanceDeduction == true).FirstOrDefault().DeductionTypeID;
            payDeductionModel.StudentDetails = deductionDB.GetStudentDetailsByFamilyId(objFamilyDetails.FamilyID).ToList();
            ViewBag.FamilyID = objFamilyDetails.FamilyID;
            ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
            return View("AddFamilyOutStandingDetails", payDeductionModel);
        }

        [HttpPost]
        public ActionResult AddFamilyOSTtoDeduction(string payDeductionData, double OldAmount, int OldPaidCycle)
        {
            PayDeductionModel payDeductionModel = Newtonsoft.Json.JsonConvert.DeserializeObject<PayDeductionModel>(payDeductionData);
            OperationDetails operationDetails = new OperationDetails();

            DeductionDB deductionDB = new DeductionDB();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool IsDeductionSendForApproval = false;
            string resultMessage = string.Empty;
            int cycle = 0;
            if (payDeductionModel.EmployeeID == 0)
            {
                return Json(new OperationDetails { Success = false, Message = "Please select Employee first" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<PayDeductionModel> payDeductionModelList = new List<PayDeductionModel>();
                PayDeductionDetailModel payDeductionDetailModel;
                PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
                PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
                objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 3).FirstOrDefault();
                objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
                bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
                bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;

                if (IsConfirmRunPayroll && IsSendForConfirmation)
                {
                    IsDeductionSendForApproval = true;
                }
                else
                {
                    IsDeductionSendForApproval = false;
                }
                cycle = payDeductionModel.PaidCycle;
                CommonHelper.CommonHelper.GetAmountFormat();
                if (payDeductionModel.PaidCycle > 0)
                {
                    payDeductionModel.IsInstallment = true;
                }

                if (payDeductionModel.PayDeductionID == 0)//Add
                {
                    if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                    {
                        operationDetails = deductionDB.PayDeductionCrud(payDeductionModel, 1);
                        operationDetails.CssClass = "success";
                        //Add details
                        if (operationDetails.Success)
                        {
                            for (int i = 0; i < cycle; i++)
                            {
                                payDeductionDetailModel = new PayDeductionDetailModel();
                                payDeductionDetailModel.PayDeductionID = operationDetails.InsertedRowId;
                                if (i == 0)
                                {
                                    payDeductionDetailModel.DeductionDate = payDeductionModel.EffectiveDate;
                                }
                                else
                                {
                                    DateTime dt = ((DateTime)CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate)).AddMonths(i);//changed AddMonths to AddDays
                                    payDeductionDetailModel.DeductionDate = dt.ToString("dd/MM/yyyy");
                                }

                                payDeductionDetailModel.Amount = Math.Round(payDeductionModel.Amount / cycle, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
                                payDeductionDetailModel.Comments = payDeductionModel.RefNumber;
                                payDeductionDetailModel.IsActive = true;
                                deductionDB.PayDeductionDetailsCrud(payDeductionDetailModel, 1);
                            }
                            StudentFOSTBDetails studentFOSTBDetails;
                            for (int i = 0; i < cycle; i++)
                            {
                                foreach (var item in payDeductionModel.StudentDetails)
                                {
                                    if (item.Amount > 0)
                                    {
                                        studentFOSTBDetails = new StudentFOSTBDetails();
                                        studentFOSTBDetails.FamilyID = item.FamilyID;
                                        studentFOSTBDetails.StudentID = item.StudentID;
                                        studentFOSTBDetails.Amount = item.Amount;
                                        studentFOSTBDetails.PayDeductionID = operationDetails.InsertedRowId;
                                        if (i == 0)
                                        {
                                            studentFOSTBDetails.DeductionDate = payDeductionModel.EffectiveDate;
                                        }
                                        else
                                        {
                                            DateTime dt = ((DateTime)CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate)).AddMonths(i);//changed AddMonths to AddDays
                                            studentFOSTBDetails.DeductionDate = dt.ToString("dd/MM/yyyy");
                                        }
                                        deductionDB.StudentFOSTBDetailsCrud(studentFOSTBDetails, 1);
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        payDeductionDetailModel = new PayDeductionDetailModel()
                        {
                            DeductionDate = "",
                            Amount = Math.Round(payDeductionModel.Amount / cycle, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero),
                            PayDeductionDetailID = 0,
                            PayDeductionID = 0,
                            IsActive = false,
                            Comments = ""
                        };
                        DataTable dt = new DataTable();
                        operationDetails = deductionDB.SavePayrollConfirmationRequestForDeduction(payDeductionModel, payDeductionDetailModel, dt, objUserContextViewModel.UserId, 1);
                        operationDetails.CssClass = "Request";
                    }
                }
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
        }
    }
}