﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class NocRequestController : FormsController
    {
        public NocRequestController()
        {
            XMLLogFile = "LoggerNocRequest.xml";
        }
        // GET: NocRequest
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var UserId = objUserContextViewModel.UserId;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            CountryDB objCountryDB = new CountryDB();
            NocRequestModel nocRequestModel = new NocRequestModel();
            NocRequestDB nocRequestDB = new NocRequestDB();
            List<SelectListItem> lstNOCRequestReason = new SelectList(nocRequestDB.GetAllNocRequestReason().Where(x => x.id > 0).ToList(), "id", "text").ToList();
            ViewBag.lstNOCRequestReason = lstNOCRequestReason;
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries().Where(x => x.CountryId > 0).ToList(), "CountryId", "CountryName");
            List<SelectListItem> NOCLanguage = new List<SelectListItem>
                                                      {
                                                        new SelectListItem { Text = "English", Value = "English"},
                                                        new SelectListItem { Text = "Arabic", Value = "Arabic"}
                                                      };
            ViewBag.NOCLanguage = NOCLanguage;       
            nocRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            nocRequestModel.CreatedOn = DateTime.Now.Date;
            CertificateRequestFormDB formCertificateRequestDB = new CertificateRequestFormDB();
            nocRequestModel.certificateRequest = formCertificateRequestDB.GetFileDetails(UserId);
            nocRequestModel.IsAddMode = true;
            return View(nocRequestModel);
        }

        public ActionResult SaveNocRequest(FormCollection data)
        {
            NocRequestModel nocRequestModel = new NocRequestModel();
            nocRequestModel.certificateRequest = new CerficateRequestFormModel();
            HttpPostedFileBase filePF, fileVF, fileBT;
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            nocRequestModel.EmployeeID = objUserContextViewModel.UserId;
            string formProcessIDs = "";
            OperationDetails operationDetails = new OperationDetails();

            nocRequestModel.NOCReasonID = Convert.ToInt32(data["NOCReasonID"]);
            nocRequestModel.OtherReason = data["OtherReason"].ToString();
            nocRequestModel.CertificateAddressee = data["CertificateAddressee"].ToString();
            nocRequestModel.CountryID = Convert.ToInt32(data["CountryID"]);
            nocRequestModel.NOCLanguage = data["NOCLanguage"].ToString();
            nocRequestModel.FreeZoneVisa = Convert.ToBoolean(data["FreeZoneVisa"]);
            nocRequestModel.RequiredDate = data["RequiredDate"].ToString();
            nocRequestModel.DateOfTravel = data["DateOfTravel"].ToString();
            nocRequestModel.Comments = data["Comments"].ToString();
            nocRequestModel.FreeZoneVisa = Convert.ToBoolean(data["FreeZoneVisa"]);
            nocRequestModel.PurposeOfVisit = Convert.ToInt16(string.IsNullOrEmpty(data["PurposeOfVisit"]) ? "0" : data["PurposeOfVisit"]);
            nocRequestModel.TravelExpenseBorneBy = Convert.ToInt16(string.IsNullOrEmpty(data["TravelExpenseBorneBy"]) ? "0" : data["TravelExpenseBorneBy"]);
            nocRequestModel.certificateRequest.PassportFileID = Convert.ToInt32(string.IsNullOrEmpty(data["pfileid"]) ? "0" : data["pfileid"]);
            nocRequestModel.certificateRequest.VisaFileID = Convert.ToInt32(string.IsNullOrEmpty(data["vfileid"]) ? "0" : data["vfileid"]);
            nocRequestModel.ApprovedBusinessTravelFormId = Convert.ToInt32(string.IsNullOrEmpty(data["bfileid"]) ? "0" : data["bfileid"]);
            nocRequestModel.SalaryDetail = data["SalaryDetail"].ToString();

            EmployeeDocumentModel documentModel;
            documentModel = new EmployeeDocumentModel();
            documentModel = new EmployeeDocumentModel();
            DocumentDB documentDb = new DocumentDB();
            AllFormsFilesModel allfiles = new AllFormsFilesModel();
            List<AllFormsFilesModel> uploadList = new List<AllFormsFilesModel>();
            AllFormsFilesModel ObjFile;
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();

            if (Request.Files.Count > 0)
            {
                if (Request.Files["PassportFile"] != null)
                {
                    ObjFile = new AllFormsFilesModel();
                    filePF = Request.Files["PassportFile"];
                    ObjFile.FileName = filePF.FileName;
                    ObjFile.FileContentType = filePF.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(filePF);
                    ObjFile.FormFileIDName = "PassportFileID";
                    uploadFileList.Add(ObjFile);
                    nocRequestModel.certificateRequest.PassportFileID = 0;
                }
                if (Request.Files["VisaFile"] != null)
                {
                    ObjFile = new AllFormsFilesModel();
                    fileVF = Request.Files["VisaFile"];
                    ObjFile.FileName = fileVF.FileName;
                    ObjFile.FileContentType = fileVF.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileVF);
                    ObjFile.FormFileIDName = "VisaFileID";
                    uploadFileList.Add(ObjFile);
                    nocRequestModel.certificateRequest.VisaFileID = 0;
                }
                if (Request.Files["ApprovedBusinessTravelFormFile"] != null)
                {
                    ObjFile = new AllFormsFilesModel();
                    fileBT = Request.Files["ApprovedBusinessTravelFormFile"];
                    ObjFile.FileName = fileBT.FileName;
                    ObjFile.FileContentType = fileBT.ContentType;
                    ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileBT);
                    ObjFile.FormFileIDName = "ApprovedBusinessTravelFormId";
                    uploadFileList.Add(ObjFile);
                    nocRequestModel.ApprovedBusinessTravelFormId = 0;
                }
                if (nocRequestModel.certificateRequest.PassportFileID > 0)
                {
                    ObjFile = new AllFormsFilesModel();
                    ObjFile = new AllFormsFilesDB().GetAllFormsFiles(nocRequestModel.certificateRequest.PassportFileID);
                    if (ObjFile.FileID > 0)
                    {
                        nocRequestModel.certificateRequest.PassportFileID = nocRequestModel.certificateRequest.PassportFileID;
                    }
                    else
                    {
                        documentModel = new EmployeeDocumentModel();
                        documentModel = documentDb.GetEmployeeDocumentById(nocRequestModel.certificateRequest.PassportFileID);
                        ObjFile.DocumentFile = documentModel.DocumentFile;
                        ObjFile.FileContentType = documentModel.FileContentType;
                        ObjFile.FileName = documentModel.ImageName;
                        ObjFile.FormFileIDName = "PassportFileID";
                        uploadFileList.Add(ObjFile);
                        nocRequestModel.certificateRequest.PassportFileID = 0;
                    }
                }
                if (nocRequestModel.certificateRequest.VisaFileID > 0)
                {
                    ObjFile = new AllFormsFilesModel();
                    ObjFile = new AllFormsFilesDB().GetAllFormsFiles(nocRequestModel.certificateRequest.VisaFileID);
                    if (ObjFile.FileID > 0)
                    {
                        nocRequestModel.certificateRequest.VisaFileID = nocRequestModel.certificateRequest.VisaFileID;
                    }
                    else
                    {
                        documentModel = new EmployeeDocumentModel();
                        documentModel = documentDb.GetEmployeeDocumentById(nocRequestModel.certificateRequest.VisaFileID);
                        ObjFile.DocumentFile = documentModel.DocumentFile;
                        ObjFile.FileContentType = documentModel.FileContentType;
                        ObjFile.FileName = documentModel.ImageName;
                        ObjFile.FormFileIDName = "VisaFileID";
                        uploadFileList.Add(ObjFile);
                        nocRequestModel.certificateRequest.VisaFileID = 0;
                    }
                }
                if (nocRequestModel.ApprovedBusinessTravelFormId > 0)
                {
                    ObjFile = new AllFormsFilesModel();
                    ObjFile = new AllFormsFilesDB().GetAllFormsFiles(nocRequestModel.ApprovedBusinessTravelFormId);
                    if (ObjFile.FileID > 0)
                    {
                        nocRequestModel.ApprovedBusinessTravelFormId = nocRequestModel.ApprovedBusinessTravelFormId;
                    }
                    else
                    {
                        documentModel = new EmployeeDocumentModel();
                        documentModel = documentDb.GetEmployeeDocumentById(nocRequestModel.ApprovedBusinessTravelFormId);
                        ObjFile.DocumentFile = documentModel.DocumentFile;
                        ObjFile.FileContentType = documentModel.FileContentType;
                        ObjFile.FileName = documentModel.ImageName;
                        ObjFile.FormFileIDName = "ApprovedBusinessTravelFormId";
                        uploadFileList.Add(ObjFile);
                        nocRequestModel.ApprovedBusinessTravelFormId = 0;
                    }
                }
            }
            nocRequestModel.certificateRequest.AllFormsFilesModelList = uploadFileList;

            requestFormsProcessModelList = new NocRequestDB().SaveNocRequest(nocRequestModel);
            if (requestFormsProcessModelList != null)
            {
                formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                requestFormsApproverEmailModelList = new NocRequestDB().GetApproverEmailList(formProcessIDs, objUserContextViewModel.UserId);
                SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Details";
                operationDetails.CssClass = "error";

            }

            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateNocRequest(FormCollection data)
        {
            HttpPostedFileBase filePF, fileVF, fileBT;
            NocRequestModel nocRequestModel = new NocRequestModel();
            nocRequestModel.certificateRequest = new CerficateRequestFormModel();
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            NocRequestDB objNocRequestDB = new NocRequestDB();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            int FormProcessID = GetFormProcessSessionID();         
            nocRequestModel.ID = Convert.ToInt32(data["ID"]);
            nocRequestModel.FreeZoneVisa = Convert.ToBoolean(data["FreeZoneVisa"]);
            nocRequestModel.CertificateAddressee = data["CertificateAddressee"].ToString();
            nocRequestModel.RequiredDate = data["RequiredDate"].ToString();
            nocRequestModel.NOCReasonID = Convert.ToInt32(data["NOCReasonID"]);
            nocRequestModel.OtherReason = data["OtherReason"].ToString();
            nocRequestModel.CertificateAddressee = data["CertificateAddressee"].ToString();
            nocRequestModel.NOCLanguage = data["NOCLanguage"].ToString();
            nocRequestModel.DateOfTravel = data["DateOfTravel"].ToString();
            nocRequestModel.Comments = data["txtComments"].ToString();
            nocRequestModel.ReqStatusID = Convert.ToInt32(data["ReqStatusID"]);
            nocRequestModel.CompanyID = Convert.ToInt32(data["CompanyID"]);
            nocRequestModel.CountryID = Convert.ToInt32(data["CountryID"]);
            nocRequestModel.RequesterEmployeeID = Convert.ToInt32(data["RequesterEmployeeID"]);
            nocRequestModel.PurposeOfVisit = Convert.ToInt16(string.IsNullOrEmpty(data["PurposeOfVisit"]) ? "0" : data["PurposeOfVisit"]);
            nocRequestModel.TravelExpenseBorneBy = Convert.ToInt16(string.IsNullOrEmpty(data["TravelExpenseBorneBy"]) ? "0" : data["TravelExpenseBorneBy"]);
            nocRequestModel.certificateRequest.PassportFileID = Convert.ToInt32(string.IsNullOrEmpty(data["pfileid"]) ? "0" : data["pfileid"]);
            nocRequestModel.certificateRequest.VisaFileID = Convert.ToInt32(string.IsNullOrEmpty(data["vfileid"]) ? "0" : data["vfileid"]);
            nocRequestModel.ApprovedBusinessTravelFormId = Convert.ToInt32(string.IsNullOrEmpty(data["bfileid"]) ? "0" : data["bfileid"]);
            nocRequestModel.SalaryDetail = data["SalaryDetail"].ToString();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            nocRequestModel.EmployeeID = objUserContextViewModel.UserId;

            RequestFormsApproveModel requestFormsApproveModel = objNocRequestDB.GetPendingFormsApproval(FormProcessID);         
            if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                || (nocRequestModel.ReqStatusID == (int)RequestStatus.Rejected && nocRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId) 
                || isEditRequestFromAllRequests)

            {
                EmployeeDocumentModel documentModel;
                documentModel = new EmployeeDocumentModel();
                documentModel = new EmployeeDocumentModel();
                DocumentDB documentDb = new DocumentDB();
                AllFormsFilesModel allfiles = new AllFormsFilesModel();
                List<AllFormsFilesModel> uploadList = new List<AllFormsFilesModel>();
                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();

                if (Request.Files.Count > 0)
                {
                    if (Request.Files["PassportFile"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        filePF = Request.Files["PassportFile"];
                        ObjFile.FileName = filePF.FileName;
                        ObjFile.FileContentType = filePF.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(filePF);
                        ObjFile.FormFileIDName = "PassportFileID";
                        uploadFileList.Add(ObjFile);
                        nocRequestModel.certificateRequest.PassportFileID = 0;
                    }
                    if (Request.Files["VisaFile"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileVF = Request.Files["VisaFile"];
                        ObjFile.FileName = fileVF.FileName;
                        ObjFile.FileContentType = fileVF.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileVF);
                        ObjFile.FormFileIDName = "VisaFileID";
                        uploadFileList.Add(ObjFile);
                        nocRequestModel.certificateRequest.VisaFileID = 0;
                    }
                    if (Request.Files["ApprovedBusinessTravelFormFile"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileBT = Request.Files["ApprovedBusinessTravelFormFile"];
                        ObjFile.FileName = fileBT.FileName;
                        ObjFile.FileContentType = fileBT.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileBT);
                        ObjFile.FormFileIDName = "ApprovedBusinessTravelFormId";
                        uploadFileList.Add(ObjFile);
                        nocRequestModel.ApprovedBusinessTravelFormId = 0;
                    }
                    if (nocRequestModel.certificateRequest.PassportFileID > 0)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile = new AllFormsFilesDB().GetAllFormsFiles(nocRequestModel.certificateRequest.PassportFileID);
                        if (ObjFile.FileID > 0)
                        {
                            nocRequestModel.certificateRequest.PassportFileID = nocRequestModel.certificateRequest.PassportFileID;
                        }
                        else
                        {
                            documentModel = new EmployeeDocumentModel();
                            documentModel = documentDb.GetEmployeeDocumentById(nocRequestModel.certificateRequest.PassportFileID);
                            ObjFile.DocumentFile = documentModel.DocumentFile;
                            ObjFile.FileContentType = documentModel.FileContentType;
                            ObjFile.FileName = documentModel.ImageName;
                            ObjFile.FormFileIDName = "PassportFileID";
                            uploadFileList.Add(ObjFile);
                            nocRequestModel.certificateRequest.PassportFileID = 0;
                        }
                    }
                    if (nocRequestModel.certificateRequest.VisaFileID > 0)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile = new AllFormsFilesDB().GetAllFormsFiles(nocRequestModel.certificateRequest.VisaFileID);
                        if (ObjFile.FileID > 0)
                        {
                            nocRequestModel.certificateRequest.VisaFileID = nocRequestModel.certificateRequest.VisaFileID;
                        }
                        else
                        {
                            documentModel = new EmployeeDocumentModel();
                            documentModel = documentDb.GetEmployeeDocumentById(nocRequestModel.certificateRequest.VisaFileID);
                            ObjFile.DocumentFile = documentModel.DocumentFile;
                            ObjFile.FileContentType = documentModel.FileContentType;
                            ObjFile.FileName = documentModel.ImageName;
                            ObjFile.FormFileIDName = "VisaFileID";
                            uploadFileList.Add(ObjFile);
                            nocRequestModel.certificateRequest.VisaFileID = 0;
                        }
                    }
                    if (nocRequestModel.ApprovedBusinessTravelFormId > 0)
                    {
                        ObjFile = new AllFormsFilesModel();
                        ObjFile = new AllFormsFilesDB().GetAllFormsFiles(nocRequestModel.ApprovedBusinessTravelFormId);
                        if (ObjFile.FileID > 0)
                        {
                            nocRequestModel.ApprovedBusinessTravelFormId = nocRequestModel.ApprovedBusinessTravelFormId;
                        }
                        else
                        {
                            documentModel = new EmployeeDocumentModel();
                            documentModel = documentDb.GetEmployeeDocumentById(nocRequestModel.ApprovedBusinessTravelFormId);
                            ObjFile.DocumentFile = documentModel.DocumentFile;
                            ObjFile.FileContentType = documentModel.FileContentType;
                            ObjFile.FileName = documentModel.ImageName;
                            ObjFile.FormFileIDName = "ApprovedBusinessTravelFormId";
                            uploadFileList.Add(ObjFile);
                            nocRequestModel.ApprovedBusinessTravelFormId = 0;
                        }
                    }
                }
                nocRequestModel.certificateRequest.AllFormsFilesModelList = uploadFileList;

                operationDetails = objNocRequestDB.UpdateNocRequest(nocRequestModel, FormProcessID);

                if (operationDetails.Success)
                {
                    if (nocRequestModel.ReqStatusID == (int)RequestStatus.Rejected && nocRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                    {
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = objNocRequestDB.GetApproverEmailList(FormProcessID.ToString(), nocRequestModel.RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                    }
                    operationDetails.InsertedRowId = 1;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.InsertedRowId = -1;
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Details";
                    operationDetails.CssClass = "error";
                }
            }
            else
            {
                operationDetails.InsertedRowId = 0;
                operationDetails.Success = true;
                operationDetails.Message = "No permissions to update.";
                operationDetails.CssClass = "error";
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit()
        {
            int formProcessID = GetFormProcessSessionID();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            NocRequestModel nocRequestModel = new NocRequestModel();
            NocRequestDB nocRequestDB = new NocRequestDB();
            //get current/pending approver group and employee
            RequestFormsApproveModel requestFormsApproveModel = nocRequestDB.GetPendingFormsApproval(formProcessID);
            RequestFormsProcessModel objRequestFormsProcessModel = nocRequestDB.GetRequestFormsProcess(formProcessID);
            nocRequestModel = nocRequestDB.GetNocRequestDetail(formProcessID);
            ViewBag.CompanyId = nocRequestModel.CompanyID;
            //if login user is the present approver then can see form in edit mode 
            if (objUserContextViewModel.UserId == 0
                || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                || (nocRequestModel.ReqStatusID == (int)RequestStatus.Rejected && nocRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId))
            {
                if (nocRequestModel.ReqStatusID == (int)RequestStatus.Pending
                    || (nocRequestModel.ReqStatusID == (int)RequestStatus.Rejected && nocRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                    || nocRequestModel.ReqStatusID == (int)RequestStatus.Completed)
                {
                    CountryDB objCountryDB = new CountryDB();                   
                    List<SelectListItem> lstNOCRequestReason = new SelectList(nocRequestDB.GetAllNocRequestReason().Where(x => x.id > 0).ToList(), "id", "text").ToList();
                    ViewBag.lstNOCRequestReason = lstNOCRequestReason;
                    ViewBag.Country = new SelectList(objCountryDB.GetAllContries().Where(x => x.CountryId > 0).ToList(), "CountryId", "CountryName");
                    List<SelectListItem> NOCLanguage = new List<SelectListItem>
                                                          {
                                                            new SelectListItem { Text = "English", Value = "English"},
                                                            new SelectListItem { Text = "Arabic", Value = "Arabic"}
                                                          };
                    ViewBag.NOCLanguage = NOCLanguage;                  
                    ViewBag.formProcessID = formProcessID;
                    return View(nocRequestModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("ViewDetails");
        }
        public ActionResult UpdateDetails()
        {
            NocRequestDB nocRequestDB = new NocRequestDB();
            NocRequestModel nocRequestModel = new NocRequestModel();
            int formProcessID = GetFormProcessSessionID();
            RequestFormsProcessModel objRequestFormsProcessModel = nocRequestDB.GetRequestFormsProcess(formProcessID);
            nocRequestModel = nocRequestDB.GetNocRequestDetail(formProcessID);
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 11;

            if (formProcessID > 0)
            {
                if (nocRequestModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    CountryDB objCountryDB = new CountryDB();
                    List<SelectListItem> lstNOCRequestReason = new SelectList(nocRequestDB.GetAllNocRequestReason().Where(x => x.id > 0).ToList(), "id", "text").ToList();
                    ViewBag.lstNOCRequestReason = lstNOCRequestReason;
                    ViewBag.Country = new SelectList(objCountryDB.GetAllContries().Where(x => x.CountryId > 0).ToList(), "CountryId", "CountryName");
                    List<SelectListItem> NOCLanguage = new List<SelectListItem>
                                                          {
                                                            new SelectListItem { Text = "English", Value = "English"},
                                                            new SelectListItem { Text = "Arabic", Value = "Arabic"}
                                                          };
                    ViewBag.NOCLanguage = NOCLanguage;                  
                    ViewBag.FormProcessID = formProcessID;
                    return View("UpdateDetails", nocRequestModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }

            }
            else
                return RedirectToAction("Index");          
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            CountryDB objCountryDB = new CountryDB();
            NocRequestModel nocRequestModel = new NocRequestModel();
            NocRequestDB nocRequestDB = new NocRequestDB();
            nocRequestModel = nocRequestDB.GetNocRequestDetail(formProcessID);
            ViewBag.lstNOCRequestReason = nocRequestDB.GetAllNocRequestReason().Where(x => x.id == nocRequestModel.NOCReasonID).ToList().FirstOrDefault().text;
            ViewBag.PurposeOfVisit = nocRequestDB.GetAllNocRequestReason().Where(x => x.id == nocRequestModel.PurposeOfVisit).ToList().FirstOrDefault().text;
            ViewBag.TravelExpenseBorneBy = nocRequestDB.GetAllNocRequestReason().Where(x => x.id == nocRequestModel.TravelExpenseBorneBy).ToList().FirstOrDefault().text;
            ViewBag.Country = objCountryDB.GetAllContries().Where(x => x.CountryId == nocRequestModel.CountryID).ToList().FirstOrDefault().CountryName;                     
            ViewBag.formProcessID = formProcessID;
            return View(nocRequestModel);
        }
    }
}