﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using Newtonsoft.Json;
using System.Data;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class ShiftController : BaseController
    {
        public ActionResult Index()
        {
            ShiftDB objShiftDb = new ShiftDB();

            List<Employee> Employeelist = new List<Employee>();
            VacationDB vacationdb = new VacationDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in vacationdb.GetShiftList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text, Value = m.id.ToString() });

            }
            ViewBag.shiftddl = ObjSelectedList;
            return View();
        }
        //public ActionResult GetHealthList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        //{
        //    DBHelper objDBHelper = new DBHelper();
        //    //-------------------Grid Paging Info----------------------------------
        //    int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
        //    int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
        //    int numberOfRecords = Convert.ToInt16(iDisplayLength);
        //    string sortColumn = mDataProp_1;
        //    string sortOrder = sSortDir_0.ToUpper();
        //    int totalCount = 0;
        //    //----------------------------------------------------------------------

        //    //-------------Data Objects--------------------
        //    List<HealthModel> objHealthModelList = new List<HealthModel>();
        //    HealthDB objHealthDB = new HealthDB();
        //    objHealthModelList = objHealthDB.GetHealthListWithPaging(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount);
        //    //---------------------------------------------

        //    var vList = new object();

        //    vList = new
        //    {
        //        aaData = (from item in objHealthModelList
        //                  select new
        //                  {
        //                      Action = "<a class='btn btn-primary' onclick='EditHealth(" + item.DocHealthId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> Edit</a><a class='btn btn-primary' onclick='DeleteHealth(" + item.DocHealthId.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a><a class='btn btn-primary' style='display:none' onclick='ViewHealth(" + item.DocHealthId.ToString() + ")' title='View Details' ><i class='fa fa-pencil'></i>View Details</a>",
        //                      DocHealthId = item.DocHealthId,
        //                      DocumentNo = item.DocumentNo,
        //                      IssueCountry = item.IssueCountry,
        //                      IssuePlace =item.IssuePlace,
        //                      IssueDate = item.IssueDate.ToString("dd/MM/yyyy"),
        //                      ExpiryDate = item.ExpiryDate.ToString("dd/MM/yyyy"),
        //                      Note = item.Note,
        //                      IsPrimary = item.IsPrimary,
        //                      EmployeeId = item.EmployeeId

        //                  }).ToArray(),
        //        recordsTotal = totalCount,
        //        recordsFiltered = totalCount
        //    };
        //    return Json(vList, JsonRequestBehavior.AllowGet);
        //}



        //public ActionResult CopyShift(int shiftId)
        //{
        //    int A = 0;

        //    ShiftDB objShiftDB = new ShiftDB();
        //    ShiftModel objshiftmodel = new ShiftModel();
        //  //  objshiftmodel = objShiftDB.GetShiftById(shiftId);
        //    List<ShiftModel> objShiftModelList = new List<ShiftModel>();

        //    objShiftModelList = objShiftDB.GetShiftByName(objshiftmodel.ShiftName);
        //    string s = " ";
        //    for (int i = 0; i < objShiftModelList.Count; i++)
        //    {
        //        s = s + objShiftModelList[i].ShiftDay + ",";
        //        A = 1;
        //    }
        //    //For Weekdays
        //    for (int j = 1; j <= 5; j++)
        //    {
        //        if (s.Contains(j.ToString()) == false)
        //        {
        //            objshiftmodel.ShiftDay = j;
        //            objShiftDB.InsertShift(objshiftmodel);
        //        }
        //        A = 1;

        //    }
        //    //for weekends
        //    for (int j = 6; j <= 7; j++)
        //    {
        //        if (s.Contains(j.ToString()) == false)
        //        {
        //            objshiftmodel.ShiftDay = j;
        //            objshiftmodel.ShiftStart = "";
        //            objshiftmodel.ShiftEnd = "";
        //            objShiftDB.InsertShift(objshiftmodel);
        //            A = 1;
        //        }
        //    }
        //    OperationDetails objOperationDetails = new OperationDetails();
        //    objOperationDetails.Success = true;
        //    objOperationDetails.Message = "Copied Successfully.";
        //    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);


        //}

        //public ActionResult AddShift()
        //{
        //    List<System.Web.Mvc.SelectListItem> DaysList = new List<System.Web.Mvc.SelectListItem>()
        //      { 
        //        new System.Web.Mvc.SelectListItem()  { Text = "Select Shift Day", Value = "0" },
        //        new System.Web.Mvc.SelectListItem()  { Text = "Monday", Value = "1" },
        //        new System.Web.Mvc.SelectListItem() { Text = "Tuesday", Value = "2" },
        //        new System.Web.Mvc.SelectListItem() { Text = "Wednesday", Value = "3" }, 
        //        new System.Web.Mvc.SelectListItem()  { Text = "Thursday", Value = "4" },
        //        new System.Web.Mvc.SelectListItem() { Text = "Friday", Value = "5" }, 
        //        new System.Web.Mvc.SelectListItem()  { Text = "Saturday", Value = "6" },
        //        new System.Web.Mvc.SelectListItem()  { Text = "Sunday", Value = "7" }
        //        };
        //    ViewBag.DaysList = DaysList;

        //    DateTime start = DateTime.Now.AddDays(0);
        //    DateTime End = DateTime.Now.AddDays(1);

        //    List<ShiftModel> ShiftNameList = new List<ShiftModel>();
        //    ShiftDB objshiftdb = new ShiftDB();

        //    //ViewBag.ShiftNameList = new SelectList(objshiftdb.GetAllShiftNames(), "ShiftID", "ShiftName");


        //    //  ViewBag.EmployeeListInShift = objshiftdb.GetAllEmployeesInShift(int );


        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    ViewBag.EmployeeID = objUserContextViewModel.UserId;

        //    ShiftDB ObjShiftDB = new ShiftDB();



        //    return View();
        //}


        //public ActionResult AddNewShift()
        //{

        //    List<System.Web.Mvc.SelectListItem> DaysList = new List<System.Web.Mvc.SelectListItem>()
        //      { 
        //        new System.Web.Mvc.SelectListItem()  { Text = "Select Shift Day", Value = "0" },
        //        new System.Web.Mvc.SelectListItem()  { Text = "Monday", Value = "1" },
        //        new System.Web.Mvc.SelectListItem() { Text = "Tuesday", Value = "2" },
        //        new System.Web.Mvc.SelectListItem() { Text = "Wednesday", Value = "3" }, 
        //        new System.Web.Mvc.SelectListItem()  { Text = "Thursday", Value = "4" },
        //        new System.Web.Mvc.SelectListItem() { Text = "Friday", Value = "5" }, 
        //        new System.Web.Mvc.SelectListItem()  { Text = "Saturday", Value = "6" },
        //        new System.Web.Mvc.SelectListItem()  { Text = "Sunday", Value = "7" }
        //        };
        //    ViewBag.DaysList = DaysList;

        //    DateTime start = DateTime.Now.AddDays(0);
        //    DateTime End = DateTime.Now.AddDays(1);

        //    List<ShiftModel> ShiftNameList = new List<ShiftModel>();
        //    ShiftDB objshiftdb = new ShiftDB();
        //    //ViewBag.ShiftNameList = new SelectList(objshiftdb.GetAllShiftNames(), "ShiftID", "ShiftName");


        //    //  ViewBag.EmployeeListInShift = objshiftdb.GetAllEmployeesInShift(int );


        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    ViewBag.EmployeeID = objUserContextViewModel.UserId;

        //    ShiftDB ObjShiftDB = new ShiftDB();



        //    return View();
        //}

        [HttpPost]
        public ActionResult AddShift(List<ShiftModel> objShiftModelList, string hdnShiftChange, string hdnSelectedShiftsToUpdate)
        {
            if (objShiftModelList[0].ShiftName != null)
            {
                string shiftName = objShiftModelList[0].ShiftName;
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                OperationDetails objOperationDetails = new OperationDetails();
                OperationDetails objOperationShift = new OperationDetails();
                ShiftDB objShiftDB = new ShiftDB();
                int ShiftID = 0;
                if (objShiftModelList != null)
                {
                    if (objShiftModelList[0].ShiftID > -1)
                    {
                        objOperationShift = objShiftDB.InsertShift(objShiftModelList[0], 2);
                    }
                    else
                    {

                        objShiftModelList[0].CompanyId = objUserContextViewModel.CompanyId;
                        objShiftModelList[0].ShiftName = objShiftModelList[0].ShiftName;
                        // objShiftModelList[i - 1].ShiftDay = i;
                        objOperationShift = objShiftDB.InsertShift(objShiftModelList[0], 1);
                    }
                    ShiftID = objOperationShift.InsertedRowId;
                    if (objOperationShift.Success == true || objShiftModelList[0].ShiftID > -1)
                    {
                        int shiftId = 0;
                        int mode = 0;
                        if (objShiftModelList[0].ShiftID > -1)
                        {
                            shiftId = objShiftModelList[0].ShiftID;
                            mode = 2;
                        }
                        else
                        {
                            shiftId = objOperationShift.InsertedRowId;
                            mode = 1;
                        }

                        for (int i = 1; i <= 7; i++)
                        {
                            objShiftModelList[i - 1].ShiftID = shiftId;
                            //objShiftModelList[i - 1].ShiftDay = i;
                            objOperationDetails = objShiftDB.InsertShiftDetails(objShiftModelList[i - 1], mode);
                        }

                        List<ShiftModel> UpdateShiftList = JsonConvert.DeserializeObject<List<ShiftModel>>(hdnSelectedShiftsToUpdate);
                        if (UpdateShiftList!=null)
                        {
                            foreach (var item in UpdateShiftList)
                            {
                                objOperationDetails = objShiftDB.UpdateShiftDetails(objShiftModelList[0].ShiftID, item.ShiftID);
                            }
                        }

                        List<shiftChangeModel> shiftChangelist = JsonConvert.DeserializeObject<List<shiftChangeModel>>(hdnShiftChange);
                        List<ShiftModel> ShiftDetailsList = new List<ShiftModel>();
                        ShiftDB objShift = new ShiftDB();
                        ShiftDetailsList = objShift.GetShiftDetails(shiftId);
                        //objShiftModelList[i - 1].ShiftDay = i;
                        shiftChangelist = shiftChangelist == null ? new List<shiftChangeModel>() : shiftChangelist;
                        if (shiftChangelist.Count() > 0)
                        {
                            foreach (var item in objShiftModelList)
                            {
                                foreach (var cItem in shiftChangelist)
                                {
                                    if (item.ShiftDay == cItem.shiftDay && shiftId == cItem.ShiftID)
                                    {
                                        var opResult = objShift.UpdateAtt_ShiftTableByUser(shiftId, 0, item.ShiftDay, 2, item.ShiftStart, item.ShiftEnd, item.ShiftGraceIn, item.ShiftGraceOut, item.CutOffTime, item.ShiftBreakHours, item.IsWeekend, shiftName);
                                    }
                                }
                            }
                        }
                    }
                }
                objOperationDetails.InsertedRowId = ShiftID;
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.EmployeeID = objUserContextViewModel.UserId;

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill the Shift Name.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddNewShift(List<ShiftModel> objShiftModelList)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            if (ModelState.IsValid)
            {
                ShiftDB objShiftDB = new ShiftDB();
                OperationDetails objOperationDetails = new OperationDetails();

                for (int i = 1; i <= 7; i++)
                {
                    objShiftModelList[i - 1].CompanyId = objUserContextViewModel.CompanyId;
                    objOperationDetails = objShiftDB.InsertShift(objShiftModelList[i - 1], 1);
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {

                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                ViewBag.EmployeeID = objUserContextViewModel.UserId;

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }      

        public ActionResult GetAllShiftNames(bool isDeleteEnable)
        {
            ShiftDB objShift = new ShiftDB();
            List<ShiftModel> objShiftList = new List<ShiftModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            objShiftList = objShift.GetAllShiftNames(CompanyId);
            ViewBag.IsDeleteEnable = isDeleteEnable;
            return PartialView("_getAllSHift", objShiftList);
        }

        public ActionResult GetAllShiftNamesList()
        {
            ShiftDB objShift = new ShiftDB();
            List<ShiftModel> objShiftList = new List<ShiftModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            objShiftList = objShift.GetAllShiftNames(CompanyId);
            return Json(objShiftList, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetShiftDetailsByName(String shiftname)
        {
            List<ShiftModel> SiftDetailsList = new List<ShiftModel>();
            ShiftDB objShift = new ShiftDB();
            SiftDetailsList = objShift.GetShiftByName(shiftname);
            return Json(new { Result = String.Format("Success") });
        }

        public ActionResult GetShiftDetails(int ShiftID)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            List<ShiftModel> ShiftDetailsList = new List<ShiftModel>();
            ShiftDB objShift = new ShiftDB();
            ShiftDetailsList = objShift.GetShiftDetails(ShiftID);
            List<ShiftModel> lstshifts = new List<ShiftModel>();
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 7).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 1).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 2).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 3).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 4).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 5).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 6).FirstOrDefault());
            //ShiftDetailsList.FirstOrDefault().ShiftDay
            CommonDB common = new CommonDB();
            //ViewBag.ShiftDetailsList = ShiftDetailsList;
            return Json(lstshifts, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteShift(int id)
        {
            List<ShiftModel> SiftDetailsList = new List<ShiftModel>();
            ShiftDB objShift = new ShiftDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objShift.DeleteShiftById(id);
            return Json(new { Result = String.Format("Success") });
        }

        public int GetAllEmployeesShiftCount(int Id)
        {
            ShiftDB objShift = new ShiftDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<Employee> objEmpList = new List<Employee>();
            objEmpList = objShift.GetAllEmployeesInShift(Id, UserId);
            return objEmpList.Count();
        }

        [HttpGet]
        public JsonResult CheckFuturRecordInAttShiftTable(int ShiftId, string EmpIds)
        {
            ShiftDB objShift = new ShiftDB();
            int[] EmpID = JsonConvert.DeserializeObject<int[]>(EmpIds);
            EmpIds = string.Join(",", EmpID);
            if (objShift.GetFuturRecordAttShiftTableData(ShiftId, EmpIds, true).Count > 0)
                return Json(new { IsRecordExist = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { IsRecordExist = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UserShift()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true,UserId), "EmployeeId", "FirstName").ToList();
            emplist.Insert(0, (new SelectListItem { Text = "Select Employee", Value = "0" }));
            ViewBag.TomorrowDate = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
            ViewBag.EmployeeList = emplist;
            return View();
        }

        public ActionResult GetAttShiftData(int EmpId, string FromDate, string ToDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB shiftDb = new ShiftDB();
            var vList = new object();
            vList = new
            {
                aaData = (from item in shiftDb.GetAttShiftTableData(EmpId, FromDate, ToDate,UserId)
                          select new
                          {
                              Actions = item.Posted ? "<a disabled class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAttShiftData(" + item.UserShiftId.ToString() + ",this)' title='Edit' ><i class='fa fa-pencil'></i> </a>": "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAttShiftData(" + item.UserShiftId.ToString() + ",this)' title='Edit' ><i class='fa fa-pencil'></i> </a>",
                              UserShiftId = item.UserShiftId,
                              ShiftId=item.ShiftId,
                              EmployeeId = item.EmployeeId,
                              EmployeeName = item.EmployeeName,
                              AttDate = item.AttDate,
                              Day = item.Day,
                              ShiftName = item.ShiftName,
                              InTime = item.InTime,
                              OutTime = item.OutTime,
                              GreaceIn = item.GreaceIn,
                              GreaceOut = item.GreaceOut,
                              IsUserShiftUpdated = item.IsUserShiftUpdated.ToString(),
                              OffDays = item.OffDays ? "<input disabled type=\"checkbox\" id=\"chkOffday_" + item.UserShiftId + "\" checked />" : "<input disabled type=\"checkbox\" id=\"chkOffday_" + item.UserShiftId + "\" />",
                              Posted = item.Posted ? "<input disabled type=\"checkbox\" id=\"chkPosted_" + item.UserShiftId + "\" checked />" : "<input disabled type=\"checkbox\" id=\"chkPosted_" + item.UserShiftId + "\" />",
                              IsUpdated = false
                          }).ToArray(),
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };

            return result;
        }

        public ActionResult UpdateAttShiftData(AttShiftTableModel attShiftTableModel)
        {
            ShiftDB shiftDb = new ShiftDB();
            return Json(shiftDb.UpadateAttShiftTableData(attShiftTableModel), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSpecialShift()
        {
            ShiftDB objShiftDb = new ShiftDB();
            List<Employee> Employeelist = new List<Employee>();
            ShiftModel objShiftModel = new ShiftModel();
            // objShiftModel.ObjShiftModelList = new List<ShiftModel>();
            return View("SpecialShift", objShiftModel);
        }

        public ActionResult AddSpecialShiftDetails(string hdnShiftChange, string hdnSelectedShift, List<ShiftModel> objShiftModelList = null)
        {
            List<ShiftModel> specialShiftList = JsonConvert.DeserializeObject<List<ShiftModel>>(hdnSelectedShift);

            string shiftName = objShiftModelList[0].ShiftName;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails objOperationDetails = new OperationDetails();
            OperationDetails objOperationShift = new OperationDetails();
            ShiftDB objShiftDB = new ShiftDB();
            if (objShiftModelList != null)
            {
                int shiftId = 0;
                objShiftModelList[0].isSpecialShift = true;
                DataTable multipleSpecificShiftDetail = new DataTable();
                multipleSpecificShiftDetail.Columns.Add("ShiftId", typeof(int));
                multipleSpecificShiftDetail.Columns.Add("ShiftDay", typeof(int));
                multipleSpecificShiftDetail.Columns.Add("ShiftStart", typeof(string));
                multipleSpecificShiftDetail.Columns.Add("ShiftEnd", typeof(string));
                multipleSpecificShiftDetail.Columns.Add("ShiftGraceIn", typeof(string));
                multipleSpecificShiftDetail.Columns.Add("ShiftGraceOut", typeof(string));
                multipleSpecificShiftDetail.Columns.Add("ShiftBreakHours", typeof(string));
                multipleSpecificShiftDetail.Columns.Add("AutoSync", typeof(bool));
                multipleSpecificShiftDetail.Columns.Add("CutOffTime", typeof(string));
                multipleSpecificShiftDetail.Columns.Add("IsWeekend", typeof(bool));
                // multipleSpecificShiftDetail.Columns.Add("shiftDetailsId", typeof(int));
                if (specialShiftList.Count == 0)
                {
                    for (int i = 1; i <= 7; i++)
                    {
                        multipleSpecificShiftDetail.Rows.Add(
                        objShiftModelList[0].ShiftID,
                        objShiftModelList[i - 1].ShiftDay,
                        objShiftModelList[i - 1].ShiftStart,
                        objShiftModelList[i - 1].ShiftEnd,
                        objShiftModelList[i - 1].ShiftGraceIn,
                        objShiftModelList[i - 1].ShiftGraceOut,
                        objShiftModelList[i - 1].ShiftBreakHours,
                        objShiftModelList[i - 1].AutoSync,
                        objShiftModelList[i - 1].CutOffTime,
                        objShiftModelList[i - 1].IsWeekend
                        );
                    }
                    objOperationDetails = objShiftDB.InsertSpecialShiftDetails(objShiftModelList[0], objShiftModelList[0].ShiftID, multipleSpecificShiftDetail);
                }

                foreach (var item in specialShiftList)
                {
                    multipleSpecificShiftDetail.Clear();
                    for (int i = 1; i <= 7; i++)
                    {
                        multipleSpecificShiftDetail.Rows.Add(
                        item.ShiftID,
                        objShiftModelList[i - 1].ShiftDay,
                        objShiftModelList[i - 1].ShiftStart,
                        objShiftModelList[i - 1].ShiftEnd,
                        objShiftModelList[i - 1].ShiftGraceIn,
                        objShiftModelList[i - 1].ShiftGraceOut,
                        objShiftModelList[i - 1].ShiftBreakHours,
                        objShiftModelList[i - 1].AutoSync,
                        objShiftModelList[i - 1].CutOffTime,
                        objShiftModelList[i - 1].IsWeekend
                        );
                    }
                    objOperationDetails = objShiftDB.InsertSpecialShiftDetails(objShiftModelList[0], item.ShiftID, multipleSpecificShiftDetail);
                }

                List<shiftChangeModel> shiftChangelist = JsonConvert.DeserializeObject<List<shiftChangeModel>>(hdnShiftChange);
                List<ShiftModel> ShiftDetailsList = new List<ShiftModel>();
                ShiftDB objShift = new ShiftDB();
                ShiftDetailsList = objShift.GetShiftDetails(shiftId);
                //objShiftModelList[i - 1].ShiftDay = i;
                shiftChangelist = shiftChangelist == null ? new List<shiftChangeModel>() : shiftChangelist;
                if (shiftChangelist.Count() > 0)
                {
                    foreach (var item in objShiftModelList)
                    {
                        foreach (var cItem in shiftChangelist)
                        {
                            if (item.ShiftDay == cItem.shiftDay && shiftId == cItem.ShiftID)
                            {
                                var opResult = objShift.UpdateAtt_ShiftTableByUser(shiftId, 0, item.ShiftDay, 2, item.ShiftStart, item.ShiftEnd, item.ShiftGraceIn, item.ShiftGraceOut, item.CutOffTime, item.ShiftBreakHours, item.IsWeekend, shiftName);
                            }
                        }
                    }
                }
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetSpecialShiftDetails(int ShiftID)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            List<ShiftModel> ShiftDetailsList = new List<ShiftModel>();
            ShiftDB objShift = new ShiftDB();
            ShiftDetailsList = objShift.GetSpecialShiftDetails(ShiftID);
            List<ShiftModel> lstshifts = new List<ShiftModel>();
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 7).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 1).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 2).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 3).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 4).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 5).FirstOrDefault());
            lstshifts.Add(ShiftDetailsList.Where(x => x.ShiftDay == 6).FirstOrDefault());
            //ShiftDetailsList.FirstOrDefault().ShiftDay
            CommonDB common = new CommonDB();
            //ViewBag.ShiftDetailsList = ShiftDetailsList;
            return Json(lstshifts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMultipleUserShift(List<AttShiftTableModel> attShiftList)
        {
            ShiftDB shiftDb = new ShiftDB();
            return Json(shiftDb.UpdatMultipleUserShift(attShiftList,1), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CopyAndSaveMultipleUserShift(List<AttShiftTableModel> attShiftList)
        {
            ShiftDB shiftDb = new ShiftDB();
            OperationDetails op = new OperationDetails();
            op = shiftDb.UpdatMultipleUserShift(attShiftList, 2);
            if (op.Success==true)
            {
                op.Message = "User shift copied & updated successfully";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllEmployeesShift(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB objShift = new ShiftDB();
            List<Employee> objEmpList = new List<Employee>();
            objEmpList = objShift.GetAllEmployeesInShift(Id,UserId);
            return PartialView("_getAllEmployeesInShift", objEmpList);
        }

        public ActionResult GetAllEmployeesNotInShift(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB objShift = new ShiftDB();
            List<Employee> objEmpList = new List<Employee>();
            objEmpList = objShift.GetAllEmployeesNotInShift(Id, UserId);
            return PartialView("_getAllEmployeesNotInShift", objEmpList.OrderBy(x => x.FirstName).ToList());
        }

        public ActionResult AddEmployeeToShift(string EmployeeIds, int shiftID)
        {
            OperationDetails op = new OperationDetails();
            ShiftDB objShift = new ShiftDB();
            int[] EmpID = JsonConvert.DeserializeObject<int[]>(EmployeeIds);
            string EmpIds = string.Join(",", EmpID);
            var shiftData = objShift.GetShiftById(shiftID).FirstOrDefault();
            op = objShift.InsertEmployeeShift(shiftID, EmpIds);
            foreach (var item in EmpID)
            {
                var opResult = objShift.UpdateAtt_ShiftTableByUser(shiftID, item, 0, 1, "", "", "", "", "", "", false, "");
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveEmployeeShift(string EmployeeIds, int shiftId)
        {
            OperationDetails op = new OperationDetails();
            ShiftDB objShift = new ShiftDB();
            EmployeeIds = EmployeeIds.Replace("[", string.Empty).Replace("]", string.Empty);
            op = objShift.RemoveEmployeeShift(shiftId, EmployeeIds);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllSectionInShift(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB objShift = new ShiftDB();
            List<EmployeeSectionModel> objSectionShiftList = new List<EmployeeSectionModel>();
            objSectionShiftList = objShift.GetAllSectionInShift(Id, UserId);
            return PartialView("_getAllSectionInShift", objSectionShiftList);
        }

        public ActionResult GetAllSectionNotInShift(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB objShift = new ShiftDB();
            List<EmployeeSectionModel> objSectionShiftList = new List<EmployeeSectionModel>();
            objSectionShiftList = objShift.GetAllSectionNotInShift(Id, UserId);
            return PartialView("_getAllSectionNotInShift", objSectionShiftList);
        }

        public ActionResult AddSectionsToShift(string SectionIds, int shiftID)
        {
            OperationDetails op = new OperationDetails();
            ShiftDB objShift = new ShiftDB();
            int[] SectioinID = JsonConvert.DeserializeObject<int[]>(SectionIds);
            string sections = string.Join(",", SectioinID);
            op = objShift.InsertSectionsToShift(shiftID, sections);
            List<Employee> lstEmployee = objShift.GetEmployeeBySectionsIds(sections);
            foreach (var item in lstEmployee)
            {
                var opResult = objShift.UpdateAtt_ShiftTableByUser(shiftID, item.EmployeeId, 0, 1, "", "", "", "", "", "", false, "");
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveSectionsFromShift(string SectionIDs, int shiftId)
        {
            OperationDetails op = new OperationDetails();
            ShiftDB objShift = new ShiftDB();
            SectionIDs = SectionIDs.Replace("[", string.Empty).Replace("]", string.Empty);
            op = objShift.RemoveSectionsFromShift(shiftId, SectionIDs);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeAsPerSections(string SectionIds)
        {
            ShiftDB objShift = new ShiftDB();
            SectionIds = SectionIds.Replace("[", string.Empty).Replace("]", string.Empty);
            List<Employee> lstEmployee = objShift.GetEmployeeBySectionsIds(SectionIds);
            return PartialView(lstEmployee);
        }

        public ActionResult GetAllDepartmentInShift(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB objShift = new ShiftDB();
            List<EmployeeSectionModel> objSectionShiftList = new List<EmployeeSectionModel>();
            objSectionShiftList = objShift.GetAllDepartmentInShift(Id, UserId);
            return PartialView("_getAllDepartmentInShift", objSectionShiftList);
        }

        public ActionResult GetAllDepartmentNotInShift(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ShiftDB objShift = new ShiftDB();
            List<EmployeeSectionModel> objSectionShiftList = new List<EmployeeSectionModel>();
            objSectionShiftList = objShift.GetAllDepartmentNotInShift(Id, UserId);
            return PartialView("_GetAllDepartmentNotInShift", objSectionShiftList);
        }

        public ActionResult AddDepartmentsToShift(string DeptIds, int shiftID)
        {
            OperationDetails op = new OperationDetails();
            ShiftDB objShift = new ShiftDB();
            int[] DeptID = JsonConvert.DeserializeObject<int[]>(DeptIds);
            string sections = string.Join(",", DeptID);
            op = objShift.InsertDepartmentssToShift(shiftID, sections);
            List<Employee> lstEmployee = objShift.GetEmployeeByDeptIds(sections);
            foreach (var item in lstEmployee)
            {
                var opResult = objShift.UpdateAtt_ShiftTableByUser(shiftID, item.EmployeeId, 0, 1, "", "", "", "", "", "", false, "");
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveDepartmentFromShift(string DeptIds, int shiftId)
        {
            OperationDetails op = new OperationDetails();
            ShiftDB objShift = new ShiftDB();
            DeptIds = DeptIds.Replace("[", string.Empty).Replace("]", string.Empty);
            op = objShift.RemoveDepartmentsFromShift(shiftId, DeptIds);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeAsPerDepts(string DeptIds)
        {
            ShiftDB objShift = new ShiftDB();
            DeptIds = DeptIds.Replace("[", string.Empty).Replace("]", string.Empty);
            List<Employee> lstEmployee = objShift.GetEmployeeByDeptIds(DeptIds);
            return PartialView(lstEmployee);
        }

    }
}