﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using System.Drawing.Imaging;
using HRMS.DataAccess.FormsDB;

namespace HRMS.Web.Controllers
{
    public class CompanyController : BaseController
    {
        //
        // GET: /Company/
        // GET: /Company/
        public ActionResult Index()
        {
            HRMS.DataAccess.DBHelper objDBHelper = new HRMS.DataAccess.DBHelper();
            List<HRMS.Entities.General.MenuNavigationModel> objMenuNavigationModelList = new List<HRMS.Entities.General.MenuNavigationModel>();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationList().Where(x => x.URL.ToLower() == "/Company".ToLower()).ToList();
            HRMS.Entities.General.MenuNavigationModel menuNavigationModel = new HRMS.Entities.General.MenuNavigationModel();
            foreach (var item in objMenuNavigationModelList)
            {
                menuNavigationModel.IsAddCheckbox = item.IsAddCheckbox;
                menuNavigationModel.IsDeleteCheckbox = item.IsDeleteCheckbox;
                menuNavigationModel.IsUpdateCheckbox = item.IsUpdateCheckbox;
                menuNavigationModel.IsViewCheckBox = item.IsViewCheckBox;
            }
            ViewBag.MenuNavigationModelList = menuNavigationModel;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            UserRoleDB userRoleDB = new UserRoleDB();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;

            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/Company");
            Session["CompanyInfo"] = permissionModel;
            //Dont know what this line does
          //  new CommonHelper.CommonHelper().GetLoggerDetailsList();
            CompanyDB objCompanyDB = new CompanyDB();
            int companyCount = objCompanyDB.GetAllCompanyList().Count();
            ViewBag.CompanyCount = companyCount;
            return View();
        }

        public ActionResult GetCompanyList()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //-------------Data Objects--------------------
            List<HRMS.Entities.CompanyModel> objCompanyList = new List<HRMS.Entities.CompanyModel>();
            CompanyDB objCompanyDB = new CompanyDB();
            objCompanyList = objCompanyDB.GetAllCompany();
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/Company");
            HRMS.DataAccess.DBHelper objDBHelper = new HRMS.DataAccess.DBHelper();           
            List<HRMS.Entities.General.MenuNavigationModel> objMenuNavigationModelList = new List<HRMS.Entities.General.MenuNavigationModel>();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationList().Where(x => x.URL.ToLower()== "/Company".ToLower()).ToList();
            HRMS.Entities.General.MenuNavigationModel menuNavigationModel = new HRMS.Entities.General.MenuNavigationModel();
            foreach (var item in objMenuNavigationModelList)
            {
                menuNavigationModel.IsAddCheckbox = item.IsAddCheckbox;
                menuNavigationModel.IsDeleteCheckbox = item.IsDeleteCheckbox;
                menuNavigationModel.IsUpdateCheckbox = item.IsUpdateCheckbox;
                menuNavigationModel.IsViewCheckBox = item.IsViewCheckBox;
            }
            string Actions = "";         
            if (!permissionModel.IsUpdateOnlyPermission && permissionModel.IsDeleteOnlyPermission)
            {
                Actions = "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(CompanyId)' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewCategoryChannel(CompanyId)' title='View Details' ><i class='fa fa-eye'></i></a>";                                              
            }
            else if (!permissionModel.IsDeleteOnlyPermission && permissionModel.IsUpdateOnlyPermission && menuNavigationModel.IsDeleteCheckbox)
            {
                Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(CompanyId)' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewCategoryChannel(CompanyId)' title='View Details' ><i class='fa fa-eye'></i></a>";
            }
            else if (permissionModel.IsUpdateOnlyPermission && permissionModel.IsDeleteOnlyPermission)
            {
                if (menuNavigationModel.IsDeleteCheckbox)
                {
                    Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(CompanyId)' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(CompanyId)' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewCategoryChannel(CompanyId)' title='View Details' ><i class='fa fa-eye'></i></a>";
                }
                else
                {
                    Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(CompanyId)' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewCategoryChannel(CompanyId)' title='View Details' ><i class='fa fa-eye'></i></a>";
                }               
            }
            else 
            {
                Actions = "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewCategoryChannel(CompanyId)' title='View Details' ><i class='fa fa-eye'></i></a>";
            }
            var vList = new object();
            vList = new
            {
                aaData = (from item in objCompanyList
                          select new
                          {
                              Actions = string.IsNullOrEmpty(Actions)?"": Actions.Replace("CompanyId", item.CompanyId.ToString()),
                              CompanyId = item.CompanyId,
                              name = item.name,
                              OrganizationCode=item.OrganizationCode,
                              EmailId = item.EmailId,
                              ContactPersonName = item.ContactPersonName,
                              Address1 = item.Address1,
                              Address2 = item.Address2,
                              City = item.CityName,
                              Country = item.CountryName,
                              Pincode = item.POBOX.ToString(),
                              Phone = item.Phone,

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            CountryDB objCountryDB = new CountryDB();
            List<SelectListItem> ObjSelectedList = null;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "0" });
            foreach (var m in objCountryDB.GetAllContries())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });
            }
            ViewBag.CountryList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });
            ViewBag.CityList = ObjSelectedList;
            ViewBag.Employee = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList(), "EmployeeId", "FullName");

            List<SelectListItem> objCompanyTypeList = new List<SelectListItem>();
            objCompanyTypeList.Add(new SelectListItem { Text = "Select Company Type", Value = "" });
            CompanyTypeDB db = new CompanyTypeDB();
            foreach (var m in db.GetAllCompanyType())
            {
                objCompanyTypeList.Add(new SelectListItem { Text = m.CompanyTypeName, Value = m.CompanyTypeID.ToString() });
            }
            ViewBag.CompanyTypeList = objCompanyTypeList;

            HRMS.Entities.CompanyModel objCompany = new HRMS.Entities.CompanyModel();
            CompanyDB objCompanyDB = new CompanyDB();           
            if (id != 0)
            {
                objCompany = objCompanyDB.GetCompany(id);
                if (objCompany.Country > 0)
                {
                    CityDB objCity = new CityDB();
                    foreach (var m in objCity.GetCityByCountryId(Convert.ToString(objCompany.Country)))
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });
                    }
                    ViewBag.CityList = ObjSelectedList;
                }

                return View(objCompany);
            }
            else
            {
                return View();
            }

        }

        public ActionResult ViewCategoryDetails(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();                     
            ViewBag.Employee = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList(), "EmployeeId", "FullName");
            HRMS.Entities.CompanyModel objCompany = new HRMS.Entities.CompanyModel();
            CompanyDB objCompanyDB = new CompanyDB();
            objCompany = objCompanyDB.GetCompany(id);
            CityDB objCityDB = new CityDB();
            CountryDB objCountryDB = new CountryDB();                           
            var objCountry = objCountryDB.GetAllContries().Where(x=>x.CountryId== objCompany.Country);
            var objCity = objCityDB.GetCityByCountryId(Convert.ToString(objCompany.Country)).Where(x=>x.CityId== objCompany.City);
            foreach (var item in objCountry)
            {
                objCompany.CountryName = item.CountryName;
            }
            foreach (var item in objCity)
            {
                objCompany.CityName = item.CityName;
            }
            EmployeeDetails employeeDetail = new EmployeeDetails();
            if (objCompany.CompanyHead==0)
            {
                ViewBag.CompanyHead = "N/A";
            }
            else
            {
                employeeDetail=objEmployeeDB.GetEmployeedetailsById(objCompany.CompanyHead);
                ViewBag.CompanyHead = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            }           
            if (objCompany.Assistant1 == 0)
            {
                ViewBag.Assistant1 = "N/A";
            }
            else
            {
                employeeDetail = objEmployeeDB.GetEmployeedetailsById(objCompany.Assistant1);
                ViewBag.Assistant1 = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            }                                
            if (objCompany.Assistant2 == 0)
            {
                ViewBag.Assistant2 = "N/A";
            }
            else
            {
                employeeDetail = objEmployeeDB.GetEmployeedetailsById(objCompany.Assistant2);
                ViewBag.Assistant2 = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            }                     
            if (objCompany.Assistant3 == 0)
            {
                ViewBag.Assistant3 = "N/A";
            }
            else
            {
                employeeDetail = objEmployeeDB.GetEmployeedetailsById(objCompany.Assistant3);
                ViewBag.Assistant3 = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            }           
            return View(objCompany);
        }
        public JsonResult Save(CompanyModel objCompany)
        {
            string result = "";
            string resultMessage = "";
            try
            {                             
                CompanyDB objCompanyDB = new CompanyDB();

                if (objCompany.CompanyId == 0)
                {
                    objCompany.CreatedBy = 1;
                    objCompanyDB.AddCompany(objCompany);

                }
                else
                {
                    objCompany.ModifiedBy = 1;
                    objCompanyDB.UpdateCompany(objCompany);
                }

                //return Redirect("Index");
                if (objCompany.CompanyId == 0)
                {
                    result = "success";
                    resultMessage = "Organization Added Successfully.";
                }
                else if (objCompany.CompanyId != 0)
                {
                    result = "success";
                    resultMessage = "Organization Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while adding Organization.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            string result = "";
            string resultMessage = "";
            CompanyDB objCompanyDB = new CompanyDB();
            HRMS.Entities.CompanyModel objCompany = new HRMS.Entities.CompanyModel();
            objCompany.CompanyId = int.Parse(id);
            int success = objCompanyDB.DeleteCompany(objCompany);
            result = success > 0 ? "success" : "warning";
            resultMessage = success > 0 ? "Record Deleted Successfully" : "This Organization has been assigned to Employee.Unable to delete the record";
            //return Redirect("Index");
            //return Json(0, JsonRequestBehavior.AllowGet);
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcel()
        {
            byte[] content;
            string fileName = "Organizations" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CompanyDB objCompanyDB = new CompanyDB();
            System.Data.DataSet ds = objCompanyDB.GetAllCompanyDataset();
            ds.Tables[0].Columns["Company Head"].ColumnName = "Principal/Manager";
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            string fileName = "Organizations" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            CompanyDB objCompanyDB = new CompanyDB();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //CompanyDB objCompanyDB = new CompanyDB();
            System.Data.DataSet ds = objCompanyDB.GetAllCompanyDataset();
            ds.Tables[0].Columns["Company Head"].ColumnName = "Principal/ Manager";
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }

        public ActionResult checkSchoolOrgnization(int checkSchoolOrgnizationId)
        {
            CompanyDB objCompanyDB = new CompanyDB();
            bool isSchoolOrgnization = objCompanyDB.checkSchoolOrgnization(checkSchoolOrgnizationId);

            if (isSchoolOrgnization)
            {
                return Json(new { result = "success", resultMessage = "School organization is already exists, If you save record then school organization will remove from school organization." });
            }
            else
            {
                return Json(new { result = "error", resultMessage = "" });
            }

        }

    }
}