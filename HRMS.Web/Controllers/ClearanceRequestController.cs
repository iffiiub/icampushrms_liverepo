﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class ClearanceRequestController : FormsController
    {
        ClearanceRequestDB clearanceRequestDB;
        public ClearanceRequestController()
        {
            XMLLogFile = "LoggerClearanceRequest.xml";
            clearanceRequestDB = new ClearanceRequestDB();
        }
        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }
        public ActionResult Create()
        {
            int ClearanceEmployeeID = 0;
            if (Session["ClearanceEmployeeID"] != null)
                ClearanceEmployeeID = (int)Session["ClearanceEmployeeID"];

            if (ClearanceEmployeeID == 0)
                return RedirectToRoute(new { controller = "SeparatedEmployeeList", action = "Index" });

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ClearanceRequestModel clearanceRequestModel = new ClearanceRequestModel();
            clearanceRequestModel = clearanceRequestDB.GetClearanceRequestDetails(ClearanceEmployeeID);
            clearanceRequestModel.ClearanceRequestOptionsModel = clearanceRequestDB.GetClearanceRequestOptionDetails(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId, clearanceRequestModel.EmployeeID);
            clearanceRequestModel.IsUserExistsInGroup = clearanceRequestModel.ClearanceRequestOptionsModel.Where(x => x.IsExistInGroup == true).ToList().Count() > 0 ? true : false;
            clearanceRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            clearanceRequestModel.CreatedOn = DateTime.Now.Date;
            ViewBag.UserCompanyID = objUserContextViewModel.CompanyId;

            return View(clearanceRequestModel);
        }
        public ActionResult Edit()
        {
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0 && Session["ClearanceFormProcessID"] != null)
                formProcessID = (int)Session["ClearanceFormProcessID"];

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ClearanceRequestModel clearanceRequestModel = new ClearanceRequestModel();
            ViewBag.UserCompanyID = objUserContextViewModel.CompanyId;

            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            ViewBag.FormProcessID = formProcessID.ToString();

            if (formProcessID > 0)
            {
                clearanceRequestModel = clearanceRequestDB.GetForm(formProcessID);
                ViewBag.RequestID = clearanceRequestModel.RequestID;
                RequestFormsApproveModel requestFormsApproveModel = clearanceRequestDB.GetPendingFormsApproval(formProcessID);
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (clearanceRequestModel.ReqStatusID == (int)RequestStatus.Rejected && clearanceRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    if (clearanceRequestModel.ReqStatusID == (int)RequestStatus.Pending 
                        || (clearanceRequestModel.ReqStatusID == (int)RequestStatus.Rejected && clearanceRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                    {
                        clearanceRequestModel.ClearanceRequestOptionsModel = clearanceRequestDB.GetClearanceAssignedOption(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId, clearanceRequestModel.EmployeeID, clearanceRequestModel.ID, 1);
                        clearanceRequestModel.IsUserExistsInGroup = clearanceRequestModel.ClearanceRequestOptionsModel.Where(x => x.IsExistInGroup == true).ToList().Count() > 0 ? true : false;
                        return View(clearanceRequestModel);
                    }
                    else
                        return RedirectToAction("ViewDetails");
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }

        public ActionResult UpdateDetails()
        {
            ClearanceRequestModel clearanceRequestModel = new ClearanceRequestModel();
            int formProcessID = GetFormProcessSessionID();
            if (formProcessID == 0 && Session["ClearanceFormProcessID"] != null)
                formProcessID = (int)Session["ClearanceFormProcessID"];
            RequestFormsApproveModel requestFormsApproveModel = clearanceRequestDB.GetPendingFormsApproval(formProcessID);
            clearanceRequestModel = clearanceRequestDB.GetForm(formProcessID);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 14;

            if (formProcessID > 0)
            {
                if (clearanceRequestModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.RequestID = clearanceRequestModel.RequestID;
                    ViewBag.FormProcessID = formProcessID;
                    clearanceRequestModel.ClearanceRequestOptionsModel = clearanceRequestDB.GetClearanceAssignedOption(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId, clearanceRequestModel.EmployeeID, clearanceRequestModel.ID, 1);
                    clearanceRequestModel.IsUserExistsInGroup = clearanceRequestModel.ClearanceRequestOptionsModel.Where(x => x.IsExistInGroup == true).ToList().Count() > 0 ? true : false;
                    clearanceRequestModel.FormProcessID = formProcessID;
                    return View("UpdateDetails", clearanceRequestModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");          
        }

        [HttpPost]
        public ActionResult SaveForm(string stringClearanceRequestModel)
        {
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ClearanceRequestModel objClearanceRequestModel = new ClearanceRequestModel();
            if (!string.IsNullOrEmpty(stringClearanceRequestModel))
            {
                objClearanceRequestModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ClearanceRequestModel>(stringClearanceRequestModel);
            }
            objClearanceRequestModel.UserID = objUserContextViewModel.UserId;
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            string formProcessIDs = "";
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();

            try
            {
                requestFormsProcessModelList = clearanceRequestDB.SaveForm(objClearanceRequestModel);
                if (requestFormsProcessModelList != null)
                {
                    formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = clearanceRequestDB.GetApproverEmailList(Convert.ToString(formProcessIDs), objUserContextViewModel.UserId);                   
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList,false);
                    operationDetails.InsertedRowId = 1;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request generated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while Saving Details";
                    operationDetails.CssClass = "error";

                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
                operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateForm(string listclearanceRequestOptionsModel, int formprocessid, int ID, string Comments,int RequesterEmployeeID,int ReqStatusID)
        {
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            List<ClearanceRequestOptionsModel> objclearanceRequestOptionsModel = new List<ClearanceRequestOptionsModel>();
            if (!string.IsNullOrEmpty(listclearanceRequestOptionsModel))
            {
                objclearanceRequestOptionsModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClearanceRequestOptionsModel>>(listclearanceRequestOptionsModel);
            }
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails operationDetails = new OperationDetails();
            int result = 0;
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            try
            {
                RequestFormsApproveModel requestFormsApproveModel = clearanceRequestDB.GetPendingFormsApproval(formprocessid);
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (ReqStatusID == (int)RequestStatus.Rejected && RequesterEmployeeID == objUserContextViewModel.UserId)
                    || isEditRequestFromAllRequests)
                {
                    result = clearanceRequestDB.UpdateForm(objclearanceRequestOptionsModel, formprocessid, objUserContextViewModel.UserId, ID, Comments);
                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "No permissions to update.";
                    operationDetails.CssClass = "error";
                    return Json(operationDetails, JsonRequestBehavior.AllowGet);
                }
                if (result > 0)
                {
                    if (ReqStatusID == (int)RequestStatus.Rejected && RequesterEmployeeID == objUserContextViewModel.UserId)
                    {
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = clearanceRequestDB.GetApproverEmailList(Convert.ToString(formprocessid), RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                    }
                       
                    operationDetails.InsertedRowId = result;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating Details";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating details";
                operationDetails.CssClass = "error";
                operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (formProcessID == 0 && Session["ClearanceFormProcessID"] != null)
                formProcessID = (int)Session["ClearanceFormProcessID"];

            if (formProcessID > 0)
            {
                ClearanceRequestModel clearanceRequestModel = new ClearanceRequestModel();
                clearanceRequestModel = clearanceRequestDB.GetForm(formProcessID);
                clearanceRequestModel.ClearanceRequestOptionsModel = clearanceRequestDB.GetClearanceAssignedOption(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId, clearanceRequestModel.EmployeeID, clearanceRequestModel.ID, 1);                             
                clearanceRequestModel.FormProcessID = formProcessID;
                return View("ViewDetail", clearanceRequestModel);
            }
            else
            {
                return RedirectToRoute(new { controller = "FormsTaskList", action = "Index" });
            }
        }

        public ActionResult IsExistsClearanceRequest(int EmployeeID)
        {
            OperationDetails operationDetails = new OperationDetails();
            if (clearanceRequestDB.IsExistsClearanceRequest(EmployeeID))
            {
                operationDetails.Success = false;
                operationDetails.Message = "Already request generated for this employee.";
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = true;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSaveForm(string listclearanceRequestOptionsModel, int formprocessid, int ID, string Comments, int RequesterEmployeeID, int ReqStatusID)
        {
            List<ClearanceRequestOptionsModel> objclearanceRequestOptionsModel = new List<ClearanceRequestOptionsModel>();
            if (!string.IsNullOrEmpty(listclearanceRequestOptionsModel))
            {
                objclearanceRequestOptionsModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClearanceRequestOptionsModel>>(listclearanceRequestOptionsModel);
            }
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails operationDetails = new OperationDetails();
            int result = 0;
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();

            try
            {
                RequestFormsApproveModel requestFormsApproveModel = clearanceRequestDB.GetPendingFormsApproval(formprocessid);
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                         || (ReqStatusID == (int)RequestStatus.Rejected && RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    result = clearanceRequestDB.UpdateSaveForm(objclearanceRequestOptionsModel, formprocessid, objUserContextViewModel.UserId, ID, Comments);
                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "No permissions to update.";
                    operationDetails.CssClass = "error";
                    return Json(operationDetails, JsonRequestBehavior.AllowGet);
                }
                if (result > 0)
                {                   
                    operationDetails.InsertedRowId = result;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating Details";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating details";
                operationDetails.CssClass = "error";
                operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

    }
}