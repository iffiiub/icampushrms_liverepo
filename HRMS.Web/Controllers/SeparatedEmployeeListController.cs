﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class SeparatedEmployeeListController : BaseController
    {
        // GET: SeparatedEmployeeList
        public ActionResult Index()
        {

            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            List<SeparationRequestFormModel> objSeparationRequesList = new List<SeparationRequestFormModel>();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.SeparationType = new SelectList(separationRequestFormDB.GetAllSeparationRequestTypes(true), "SeparationTypeID", "SeparationTypeName_1");
            ViewBag.Employee = new SelectList(separationRequestFormDB.GetAllSeparationRequestedEmployee(objUserContextViewModel.UserId), "EmployeeId", "FullName");
            objSeparationRequesList = separationRequestFormDB.GetEmployeeSeparationList(Convert.ToInt32(RequestStatus.Completed), null, null, objUserContextViewModel.UserId);
            return View(objSeparationRequesList);
        }
        public ActionResult GetAllSeparationRequests(int? employeeId, int? separationTypeID)
        {
            SeparationRequestFormDB separationRequestFormDB = new SeparationRequestFormDB();
            List<SeparationRequestFormModel> objSeparationRequesList = new List<SeparationRequestFormModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objSeparationRequesList = separationRequestFormDB.GetEmployeeSeparationList(Convert.ToInt32(RequestStatus.Completed), employeeId, separationTypeID, objUserContextViewModel.UserId);
            return PartialView("_EmployeeSeparationList", objSeparationRequesList);
        }    
        public ActionResult SetExitInterviewFormProcessIDSesssion(int id)
        {
            Session["ExitInterviewFormProcessID"] = id;
            return Json(new { id = id }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetClearanceEmployeeIDSesssion(int id)
        {
            Session["ClearanceEmployeeID"] = id;
            return Json(new { id = id }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetClearanceInstanceIDSesssion(int id)
        {
            Session["ClearanceFormProcessID"] = id;
            return Json(new { id = id }, JsonRequestBehavior.AllowGet);
        }
       
    }
}