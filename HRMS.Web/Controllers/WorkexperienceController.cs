﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class WorkexperienceController: BaseController
    {
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);


            if (Session["EmployeeListID"] != null)
            {

                ViewBag.empid = Session["EmployeeListID"].ToString();
            }



            return View(employeeDetailsModelList);
        }

        public ActionResult GetWorkexperienceList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, int empid = 0)
        {

            if (empid != 0)
            {
            
            Session["EmployeeListID"] = empid;
            }
            
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<WorkexperienceModel> objWorkexperienceModelList = new List<WorkexperienceModel>();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
            CountryDB objCountryDB = new CountryDB();
            CityDB objCityDB = new CityDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objWorkexperienceModelList = objWorkexperienceDB.GetWorkexperienceList(empid);
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objWorkexperienceModelList
                          select new
                          {
                              Action = "<button class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditWorkexperience(" + item.WorkexperienceID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </button><button class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteWorkexperience(" + item.WorkexperienceID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </button>",
                              WorkexperienceID = item.WorkexperienceID,
                              EmployeeID = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName,
                              EmployerName_1 = item.EmployerName_1,
                              WorkStartDate = item.WorkStartDate,
                              WorkEndDate = item.WorkStartDate,
                              HRExperiencePositionID = objHRExperiencePositionDB.GetAllHRExperiencePosition().Where(x => x.HRExperiencePositionID == item.HRExperiencePositionID).Select(x => x.HRExperiencePositionName_1).SingleOrDefault(),

                              WorkAddress = item.WorkAddress,
                              ExperienceMonths = item.ExperienceMonths,
                              CountryID = objCountryDB.GetAllContries().Where(x => x.CountryId == item.CountryID).Select(x => x.CountryName).SingleOrDefault(),
                              CityID = objCityDB.GetCityByCountryId(item.CountryID.ToString()).Where(x => x.CityId == item.CityID).Select(x => x.CityName).SingleOrDefault()
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddWorkexperience()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
            CountryDB objCountryDB = new CountryDB();
            CityDB objCityDB = new CityDB();

            ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");
            // City List
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            ViewBag.CityList = ObjSelectedList;
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");
            //ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
            return View();
        }

        [HttpPost]
        public ActionResult AddWorkexperience(WorkexperienceModel objWorkexperienceModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //objWorkexperienceModel.EmployeeID = objUserContextViewModel.UserId;
                WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objWorkexperienceDB.InsertWorkexperience(objWorkexperienceModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();
                HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
                CountryDB objCountryDB = new CountryDB();
                CityDB objCityDB = new CityDB();

                ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

                ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
                List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
                ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
                ViewBag.CityList = ObjSelectedList;
                //ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
                ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditWorkexperience(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
            CountryDB objCountryDB = new CountryDB();
            CityDB objCityDB = new CityDB();

            ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");

            //ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;

            WorkexperienceDB objWorkexperienceModelDB = new WorkexperienceDB();
            WorkexperienceModel objWorkexperienceModel = new WorkexperienceModel();
            objWorkexperienceModel = objWorkexperienceModelDB.WorkexperienceById(id);
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FullName");
            ViewBag.City = new SelectList(objCityDB.GetCityByCountryId(objWorkexperienceModel.CountryID.ToString()), "CityId", "CityName");
            return View(objWorkexperienceModel);
        }

        [HttpPost]
        public JsonResult EditWorkexperience(WorkexperienceModel objWorkexperienceModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //objWorkexperienceModel.EmployeeID = objUserContextViewModel.UserId;
                WorkexperienceDB objWorkexperienceModelDB = new WorkexperienceDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objWorkexperienceModelDB.UpdateWorkexperience(objWorkexperienceModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();
                HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
                CountryDB objCountryDB = new CountryDB();
                CityDB objCityDB = new CityDB();

                ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

                ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
                List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
                ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
                ViewBag.CityList = ObjSelectedList;
                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewWorkexperience(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRExperiencePositionDB objHRExperiencePositionDB = new HRExperiencePositionDB();
            CountryDB objCountryDB = new CountryDB();
            CityDB objCityDB = new CityDB();

            ViewBag.ExperiencePosition = new SelectList(objHRExperiencePositionDB.GetAllHRExperiencePosition(), "HRExperiencePositionID", "HRExperiencePositionName_1");

            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            ViewBag.CityList = ObjSelectedList;
            ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
            WorkexperienceDB objWorkexperienceModelDB = new WorkexperienceDB();
            WorkexperienceModel objWorkexperienceModel = new WorkexperienceModel();
            objWorkexperienceModel = objWorkexperienceModelDB.WorkexperienceById(id);
            return View(objWorkexperienceModel);
        }

        public ActionResult DeleteWorkexperience(int id)
        {
            WorkexperienceDB objWorkexperienceDB = new WorkexperienceDB();
            objWorkexperienceDB.DeleteWorkexperienceById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmployeeSearch(string search)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyIdandEmpName(objUserContextViewModel.CompanyId, search);

            return Json(employeeDetailsModelList, JsonRequestBehavior.AllowGet);

        }

    }
}