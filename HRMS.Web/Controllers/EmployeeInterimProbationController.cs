﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static HRMS.Entities.Forms.InterimProbationRequestModel;

namespace HRMS.Web.Controllers
{
    public class EmployeeInterimProbationController : FormsController
    {
        EmployeeInterimProbationRequestDB objEmployeeInterimProbationRequestDB;
        // GET: EmployeeInterimProbation
        public EmployeeInterimProbationController()
        {
            objEmployeeInterimProbationRequestDB = new EmployeeInterimProbationRequestDB();
            XMLLogFile = "LoggerEmployeeInterimProbationRequest.xml";
        }

        public ActionResult Edit()
        {
            int formProcessID = GetFormProcessSessionID();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (formProcessID > 0)
            {
                InterimProbationRequestModel interimProbationRequestModel = objEmployeeInterimProbationRequestDB.GetInterimProbationDetailsById(formProcessID, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = objEmployeeInterimProbationRequestDB.GetPendingFormsApproval(formProcessID);
                if (objUserContextViewModel.UserId == 0
                    || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (interimProbationRequestModel.ReqStatusID == (int)RequestStatus.Rejected && interimProbationRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    if (interimProbationRequestModel.ReqStatusID == (int)RequestStatus.Pending
                        || (interimProbationRequestModel.ReqStatusID == (int)RequestStatus.Rejected && interimProbationRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || interimProbationRequestModel.ReqStatusID == (int)RequestStatus.Completed)
                    {
                        ViewBag.CompanyID = interimProbationRequestModel.CompanyID;

                        List<SelectListItem> lstEvaluationType = new SelectList(objEmployeeInterimProbationRequestDB.GetAllInterimProbationEvaluationType().Where(x => x.id > 0).ToList(), "id", "text").ToList();
                        ViewBag.lstEvaluationType = lstEvaluationType;

                        List<InterimProbationRatingScaleModel> lstRatingScaleModel = objEmployeeInterimProbationRequestDB.GetAllInterimProbationRatingScale().Where(x => x.RatingScaleID > 0).ToList();
                        ViewBag.lstRatingScaleModel = lstRatingScaleModel;

                        ViewBag.lstRatingScaleForDdl = new SelectList(lstRatingScaleModel, "RatingScaleID", "RatingScaleCode").ToList();

                        List<InterimProbationJobEvaluationModel> lstJobEvaluation = objEmployeeInterimProbationRequestDB.GetAllInterimProbationJobEvaluation(formProcessID, interimProbationRequestModel.RequestInitialize);
                        ViewBag.lstJobEvaluation = lstJobEvaluation;

                        List<InterimProbationCodeOfConductModel> lstCodeOfConduct = objEmployeeInterimProbationRequestDB.GetAllInterimProbationCodeOfConduct(formProcessID, interimProbationRequestModel.RequestInitialize);
                        ViewBag.lstCodeOfConduct = lstCodeOfConduct;

                        List<InterimProbationOthersModel> lstOthersOption = objEmployeeInterimProbationRequestDB.GetAllInterimProbationOthersOption(formProcessID, interimProbationRequestModel.RequestInitialize);
                        ViewBag.lstOthersOption = lstOthersOption;
                        ViewBag.formProcessID = formProcessID;
                        return View(interimProbationRequestModel);
                    }
                    else
                        return RedirectToAction("ViewDetails");
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }

            }
            else
                return RedirectToAction("ViewDetails");
        }

        public ActionResult UpdateDetails()
        {
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (formProcessID > 0)
            {
                InterimProbationRequestModel interimProbationRequestModel = objEmployeeInterimProbationRequestDB.GetInterimProbationDetailsById(formProcessID, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = objEmployeeInterimProbationRequestDB.GetPendingFormsApproval(formProcessID);
                if (interimProbationRequestModel.ReqStatusID != (int)RequestStatus.Rejected || isEditRequestFromAllRequests)
                {
                    ViewBag.CompanyID = interimProbationRequestModel.CompanyID;                 

                    List<SelectListItem> lstEvaluationType = new SelectList(objEmployeeInterimProbationRequestDB.GetAllInterimProbationEvaluationType().Where(x => x.id > 0).ToList(), "id", "text").ToList();
                    ViewBag.lstEvaluationType = lstEvaluationType;

                    List<InterimProbationRatingScaleModel> lstRatingScaleModel = objEmployeeInterimProbationRequestDB.GetAllInterimProbationRatingScale().Where(x => x.RatingScaleID > 0).ToList();
                    ViewBag.lstRatingScaleModel = lstRatingScaleModel;

                    ViewBag.lstRatingScaleForDdl = new SelectList(lstRatingScaleModel, "RatingScaleID", "RatingScaleCode").ToList();

                    List<InterimProbationJobEvaluationModel> lstJobEvaluation = objEmployeeInterimProbationRequestDB.GetAllInterimProbationJobEvaluation(formProcessID, interimProbationRequestModel.RequestInitialize);
                    ViewBag.lstJobEvaluation = lstJobEvaluation;

                    List<InterimProbationCodeOfConductModel> lstCodeOfConduct = objEmployeeInterimProbationRequestDB.GetAllInterimProbationCodeOfConduct(formProcessID, interimProbationRequestModel.RequestInitialize);
                    ViewBag.lstCodeOfConduct = lstCodeOfConduct;

                    List<InterimProbationOthersModel> lstOthersOption = objEmployeeInterimProbationRequestDB.GetAllInterimProbationOthersOption(formProcessID, interimProbationRequestModel.RequestInitialize);
                    ViewBag.lstOthersOption = lstOthersOption;
               
                    ViewBag.formProcessID = formProcessID;
                    return View(interimProbationRequestModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("ViewDetails");
        }

        public ActionResult UpdateEmployeeInterimProbationRequest(string interimProbationRequestModelDetails)
        {
            OperationDetails operationDetails = new OperationDetails();
            if (!string.IsNullOrEmpty(interimProbationRequestModelDetails))
            {
                InterimProbationRequestModel interimProbationRequestModelData = Newtonsoft.Json.JsonConvert.DeserializeObject<InterimProbationRequestModel>(interimProbationRequestModelDetails);
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                int result = 0;

                result = objEmployeeInterimProbationRequestDB.UpdateEmployeeInterimProbationRequest(interimProbationRequestModelData, objUserContextViewModel.UserId);
                operationDetails.InsertedRowId = result;
                if (result > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully";
                    operationDetails.CssClass = "success";
                    //if false, request is in intialized state ,next approver must be informed  
                    if (!interimProbationRequestModelData.RequestInitialize && interimProbationRequestModelData.FormProcessID > 0)
                    {
                        string formProcessIDs = interimProbationRequestModelData.FormProcessID.ToString();
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = objEmployeeInterimProbationRequestDB.GetApproverEmailList(Convert.ToString(formProcessIDs), objUserContextViewModel.UserId);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        operationDetails.Message += @" & Email send to next approver";
                    }

                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating Details";
                    operationDetails.CssClass = "error";
                }
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReInitializeEmployeeInterimProbationRequest(string interimProbationRequestModelDetails)
        {
            OperationDetails operationDetails = new OperationDetails();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            if (!string.IsNullOrEmpty(interimProbationRequestModelDetails))
            {
                try
                {
                    InterimProbationRequestModel interimProbationRequestModelData = Newtonsoft.Json.JsonConvert.DeserializeObject<InterimProbationRequestModel>(interimProbationRequestModelDetails);
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    int result = 0;

                    RequestFormsApproveModel requestFormsApproveModel = objEmployeeInterimProbationRequestDB.GetPendingFormsApproval(interimProbationRequestModelData.FormProcessID);
                    if ((interimProbationRequestModelData.ReqStatusID == (int)RequestStatus.Rejected && interimProbationRequestModelData.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || isEditRequestFromAllRequests)
                    {
                        result = objEmployeeInterimProbationRequestDB.ReInitializeEmployeeInterimProbationRequest(interimProbationRequestModelData, objUserContextViewModel.UserId);
                    }
                    else
                    {
                        operationDetails.Success = true;
                        operationDetails.Message = "No permissions to update.";
                        operationDetails.CssClass = "error";
                        return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    }
                    if (result > 0)
                    {
                        if (interimProbationRequestModelData.ReqStatusID == (int)RequestStatus.Rejected && interimProbationRequestModelData.RequesterEmployeeID == objUserContextViewModel.UserId)
                        {
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = objEmployeeInterimProbationRequestDB.GetApproverEmailList(Convert.ToString(interimProbationRequestModelData.FormProcessID), interimProbationRequestModelData.RequesterEmployeeID);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        }
                        operationDetails.InsertedRowId = result;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request updated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while updating Details";
                        operationDetails.CssClass = "error";
                    }
                }
                catch (Exception ex)
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating details";
                    operationDetails.CssClass = "error";
                    operationDetails.Exception = ex;
                }
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating Details";
                operationDetails.CssClass = "error";
            }

            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }                        
                        //Add any logic to be done once approval process is completed
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDetails()
        {
            int formProcessID = GetFormProcessSessionID();
            // UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            InterimProbationRequestModel interimProbationRequestModel = objEmployeeInterimProbationRequestDB.GetInterimProbationDetailsById(formProcessID, objUserContextViewModel.UserId);

            List<PickList> lstEvaluationType = new List<PickList>();
            List<InterimProbationRatingScaleModel> lstRatingScaleModel = new List<InterimProbationRatingScaleModel>();
            List<InterimProbationRatingScaleModel> FinalRatingCode = new List<InterimProbationRatingScaleModel>();

            lstEvaluationType = objEmployeeInterimProbationRequestDB.GetAllInterimProbationEvaluationType().ToList();
            string EvaluationType = "";
            string textFinalRatingCode = "";

            if (lstEvaluationType.Where(x => x.id > 0 && x.id == interimProbationRequestModel.TypeEvaluationID).ToList().Count > 0)
            {
                EvaluationType = lstEvaluationType.Where(x => x.id > 0 && x.id == interimProbationRequestModel.TypeEvaluationID).FirstOrDefault().text;
            }
            ViewBag.lstEvaluationType = EvaluationType;

            lstRatingScaleModel = objEmployeeInterimProbationRequestDB.GetAllInterimProbationRatingScale().Where(x => x.RatingScaleID > 0).ToList();
            ViewBag.lstRatingScaleModel = lstRatingScaleModel;

            if (lstRatingScaleModel.Where(x => x.RatingScaleID == interimProbationRequestModel.FinalRatingScaleID).ToList().Count > 0)
            {
                textFinalRatingCode = lstRatingScaleModel.Where(x => x.RatingScaleID == interimProbationRequestModel.FinalRatingScaleID).FirstOrDefault().RatingScaleCode;
            }
            ViewBag.FinalRatingCode = textFinalRatingCode;

            List<InterimProbationJobEvaluationModel> lstJobEvaluation = objEmployeeInterimProbationRequestDB.GetAllInterimProbationJobEvaluation(formProcessID, interimProbationRequestModel.RequestInitialize);
            ViewBag.lstJobEvaluation = lstJobEvaluation;

            List<InterimProbationCodeOfConductModel> lstCodeOfConduct = objEmployeeInterimProbationRequestDB.GetAllInterimProbationCodeOfConduct(formProcessID, interimProbationRequestModel.RequestInitialize);
            ViewBag.lstCodeOfConduct = lstCodeOfConduct;

            List<InterimProbationOthersModel> lstOthersOption = objEmployeeInterimProbationRequestDB.GetAllInterimProbationOthersOption(formProcessID, interimProbationRequestModel.RequestInitialize);
            ViewBag.lstOthersOption = lstOthersOption;

            ViewBag.formProcessID = formProcessID;
            interimProbationRequestModel.FormProcessID = formProcessID;

            return View(interimProbationRequestModel);
        }
    }
}