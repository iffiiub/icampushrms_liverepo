﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class PassportWithdrawalRequestController : FormsController
    {
        // GET: PassportWithdrawalRequest
        PassportWithdrawalRequestDB objPassportWithdrawalRequestDB;
        public PassportWithdrawalRequestController()
        {
            XMLLogFile = "LoggerPassportWithdrawalRequest.xml";
            objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
        }
        public ActionResult Index()
        {
            PassportWithdrawalRequestModel passportWithdrawalRequestModel = new PassportWithdrawalRequestModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.PassportReasonList = new SelectList(objPassportWithdrawalRequestDB.GetPassportRequestsReasonList(), "id", "text");
            ViewBag.FormId = 12;
            ViewBag.CompanyId = objUserContextViewModel.CompanyId.ToString();
            passportWithdrawalRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            passportWithdrawalRequestModel.CreatedOn = DateTime.Now.Date;
            passportWithdrawalRequestModel.IsAddMode = true;
            return View(passportWithdrawalRequestModel);
        }
        public ActionResult Edit()
        {
            PassportWithdrawalRequestModel objPassportWithdrawalRequestModel = new PassportWithdrawalRequestModel();
            objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
            int formProcessID = GetFormProcessSessionID();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (formProcessID > 0)
            {
                RequestFormsProcessModel objRequestFormsProcessModel = objPassportWithdrawalRequestDB.GetRequestFormsProcess(formProcessID);
                objPassportWithdrawalRequestModel = objPassportWithdrawalRequestDB.GetPassportWithdrawalRequest(objRequestFormsProcessModel.FormInstanceID, formProcessID);
                ViewBag.RequestID = objRequestFormsProcessModel.RequestID;               
                RequestFormsApproveModel requestFormsApproveModel = objPassportWithdrawalRequestDB.GetPendingFormsApproval(formProcessID);
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (objPassportWithdrawalRequestModel.ReqStatusID == (int)RequestStatus.Rejected && objPassportWithdrawalRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                   )
                {
                    if (objPassportWithdrawalRequestModel.ReqStatusID == (int)RequestStatus.Pending || (objPassportWithdrawalRequestModel.ReqStatusID == (int)RequestStatus.Rejected && objPassportWithdrawalRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                    {
                        ViewBag.PassportReasonList = new SelectList(objPassportWithdrawalRequestDB.GetPassportRequestsReasonList(), "id", "text");
                        objPassportWithdrawalRequestModel.FormProcessID = formProcessID;
                        return View(objPassportWithdrawalRequestModel);
                    }
                    else
                        return RedirectToAction("ViewDetails");
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Index");
        }
        public ActionResult UpdateDetails()
        {
            PassportWithdrawalRequestModel objPassportWithdrawalRequestModel = new PassportWithdrawalRequestModel();
            objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
            int formProcessID = GetFormProcessSessionID();
            RequestFormsProcessModel objRequestFormsProcessModel = objPassportWithdrawalRequestDB.GetRequestFormsProcess(formProcessID);
            objPassportWithdrawalRequestModel = objPassportWithdrawalRequestDB.GetPassportWithdrawalRequest(objRequestFormsProcessModel.FormInstanceID, formProcessID);
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 12;

            if (formProcessID > 0)
            {
                if (objPassportWithdrawalRequestModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
                    ViewBag.FormProcessID = formProcessID;
                    ViewBag.PassportReasonList = new SelectList(objPassportWithdrawalRequestDB.GetPassportRequestsReasonList(), "id", "text");
                    objPassportWithdrawalRequestModel.FormProcessID = formProcessID;
                    ViewBag.IsEditRequestFromAllRequests = isEditRequestFromAllRequests;
                    return View("UpdateDetails", objPassportWithdrawalRequestModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Index");         
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            PassportWithdrawalRequestModel objPassportWithdrawalRequestModel = new PassportWithdrawalRequestModel();
            objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
            RequestFormsProcessModel objRequestFormsProcessModel = objPassportWithdrawalRequestDB.GetRequestFormsProcess(formProcessID);
            objPassportWithdrawalRequestModel = objPassportWithdrawalRequestDB.GetPassportWithdrawalRequest(objRequestFormsProcessModel.FormInstanceID, formProcessID);
            ViewBag.RequestID = objRequestFormsProcessModel.RequestID;
            ViewBag.PassportReasonList = new SelectList(objPassportWithdrawalRequestDB.GetPassportRequestsReasonList(), "id", "text");                     
            objPassportWithdrawalRequestModel.FormProcessID = formProcessID;
            return View(objPassportWithdrawalRequestModel);
        }

        [HttpPost]
        public ActionResult SavePassportWithdrawalRequest(PassportWithdrawalRequestModel objPassportWithdrawalRequestModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objPassportWithdrawalRequestModel.CompanyID = objUserContextViewModel.CompanyId;
            objPassportWithdrawalRequestModel.EmployeeID = objUserContextViewModel.UserId;
            objPassportWithdrawalRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            RequestFormsProcessModel objRequestFormsProcessModel = objPassportWithdrawalRequestDB.SavePassportWithdrawalRequest(objPassportWithdrawalRequestModel, objUserContextViewModel.UserId);
            if (objRequestFormsProcessModel != null)
            {
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(objRequestFormsProcessModel.FormProcessID), objUserContextViewModel.UserId);
                SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdatePassportWithdrawalRequest(PassportWithdrawalRequestModel objPassportWithdrawalRequestModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            objPassportWithdrawalRequestDB = new PassportWithdrawalRequestDB();
            int result = 0;
            int formProcessID = GetFormProcessSessionID();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objPassportWithdrawalRequestModel.CompanyID = objUserContextViewModel.CompanyId;
            objPassportWithdrawalRequestModel.EmployeeID = objUserContextViewModel.UserId;
            objPassportWithdrawalRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            try
            {
                RequestFormsApproveModel requestFormsApproveModel = objPassportWithdrawalRequestDB.GetPendingFormsApproval(formProcessID);
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                         || (objPassportWithdrawalRequestModel.ReqStatusID == (int)RequestStatus.Rejected && objPassportWithdrawalRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                         || isEditRequestFromAllRequests)
                {                  
                    objPassportWithdrawalRequestModel.FormProcessID = formProcessID;
                    result = objPassportWithdrawalRequestDB.UpdatePassportWithdrawalRequest(objPassportWithdrawalRequestModel);
                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "No permissions to update.";
                    operationDetails.CssClass = "error";
                    return Json(operationDetails, JsonRequestBehavior.AllowGet);
                }
                if (result > 0)
                {
                    if (objPassportWithdrawalRequestModel.ReqStatusID == (int)RequestStatus.Rejected && objPassportWithdrawalRequestModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                    {
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = objPassportWithdrawalRequestDB.GetApproverEmailList(Convert.ToString(formProcessID), objPassportWithdrawalRequestModel.RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                    }
                    operationDetails.InsertedRowId = result;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating Details";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while updating details";
                operationDetails.CssClass = "error";
                operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}