﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using iTextSharp.text;
using System.Web.UI;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using HRMS.Entities.ViewModel;
using System.Data;
using HRMS.Entities;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class ViewEmployeeProfileController : BaseController
    {
        public ActionResult Index()
        {
            if (Session["EmployeeListID"] != null)
            {
                int id = (int)Session["EmployeeListID"];
                EmployeeDB objEmpDB = new EmployeeDB();
                HRMS.Entities.Employee employeeModel = objEmpDB.ViewEmployeeProfile(id);
                PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/BankInformation/Index");
                Session["BankPermission"] = permissionModel;
                PermissionModel permissionPersonalIdentityModel = CommonHelper.CommonHelper.CheckPermissionForUser("/document");
                Session["PersonalIdentityPermission"] = permissionPersonalIdentityModel.IsMainNavigationPermission;

                HRMS.Entities.PermissionModel objEmpPermissionModel = new HRMS.Entities.PermissionModel();
                objEmpPermissionModel = HRMS.Web.CommonHelper.CommonHelper.CheckPermissionForUser("/Employee");
                Session["EmployeePermission"] = objEmpPermissionModel;
                objEmpPermissionModel = HRMS.Web.CommonHelper.CommonHelper.CheckPermissionForUser("Employee Profile");
                Session["ViewEmployeeProfilePermission"] = objEmpPermissionModel;

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                List<int> avilablePermisssionForUser = null;
                UserRoleDB userRoleDB = new UserRoleDB();
                if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                    avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                else
                    avilablePermisssionForUser = new List<int>();

                var PermissionList = userRoleDB.GetOtherPermission();
                PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
                Session["otherPermissions"] = PermissionList;
                ViewBag.UserId = objUserContextViewModel.UserId;
                try
                {
                    string profileImgPath = Server.MapPath(employeeModel.employeeDetailsModel.ProfileImage);
                    if (profileImgPath != "" || !string.IsNullOrEmpty(profileImgPath))
                    {
                        if (System.IO.File.Exists(profileImgPath) == false)
                        {
                            employeeModel.employeeDetailsModel.ProfileImage = "";
                        }
                    }

                }
                catch (Exception ex)
                {
                }

                return View(employeeModel);
            }
            else
            {
                return RedirectToAction("ListEmployee", "Employee");
            }
        }

        public void ExportEmployeeProfileToPdf(int id)
        {
            if (id != null && id != 0)
            {
                Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);
                string fileName = "EmployeeProfile" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
                EmployeeDB objEmpDB = new EmployeeDB();
                DataSet ds = objEmpDB.GetEmployeeProfiletoExport(id);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName );
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Document dc = CommonHelper.CommonHelper.GenrateEmployeeProfilePDF(Response, ds, id);
                Response.Write(dc);
                Response.End();

            }
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        //public ActionResult LoadChildrenDetails(int EmployeeId)
        //{
        //    List<HRMS.Entities.ChildrenDetails> chilrenList = new List<HRMS.Entities.ChildrenDetails>();
        //    EmployeeDB empDB = new EmployeeDB();
        //    chilrenList = empDB.GetAllChildrenDetails(EmployeeId);
        //    return PartialView("_ViewChildrenDetails", chilrenList);
        //}

        public ActionResult ChangeFamily(int Id)
        {
            EmployeeDB objDB = new EmployeeDB();
            List<HRMS.Entities.FamilyDetails> lstFamilyDetails = new List<HRMS.Entities.FamilyDetails>();
            lstFamilyDetails = objDB.GetAllFamilyDetails(Id);
            return View(lstFamilyDetails);
        }

        public JsonResult UpdateFamilyId(int FamilyId, int EmpId)
        {
            string result = "";
            string resultMessage = "Error";
            HRMS.Entities.OperationDetails op = new Entities.OperationDetails();
            EmployeeDB empDB = new EmployeeDB();
            op = empDB.UpdateEmployeeFamilyId(EmpId, FamilyId);
            if (op.Success)
            {
                result = "success";
                resultMessage = "Family updated successfully.";
            }
            else
            {
                if (op.Message == "warning")
                {
                    result = "warning";
                    resultMessage = "Some error occurred. Unable to update family!";
                }
                else
                {
                    result = "error";
                    resultMessage = op.Message;
                }
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddChildtoDependant(int StudentId, int EmpId, bool isAddedtoDependant)
        {
            string result = "";
            string resultMessage = "Error";

            HRMS.Entities.OperationDetails op = new Entities.OperationDetails();
            if (isAddedtoDependant)
            {
                EmployeeDB empDB = new EmployeeDB();
                HRMS.Entities.ChildrenDetails child = new Entities.ChildrenDetails();
                child = empDB.GetChildDetails(StudentId);
                HRMS.Entities.EmployeeDependantModel objEmployeeDependantModel = new HRMS.Entities.EmployeeDependantModel();
                objEmployeeDependantModel.EmployeeId = EmpId;
                objEmployeeDependantModel.StudentId = StudentId;
                objEmployeeDependantModel.Country = 0;
                if (child.GenderName_1 == "Male")
                {
                    objEmployeeDependantModel.Relationship = 1;
                }
                else
                {
                    objEmployeeDependantModel.Relationship = 3;
                }
                objEmployeeDependantModel.FirstName = child.NameAsInPassport_1;
                //objEmployeeDependantModel.MiddleName = child.MiddleName_1;
                objEmployeeDependantModel.LastName = string.Empty;
                EmployeeDependantDB objEmployeeDependantDB = new EmployeeDependantDB();
                op = objEmployeeDependantDB.InsertEmployeeDependant(objEmployeeDependantModel);
            }
            else
            {
                EmployeeDependantDB objEmployeeDependantDB = new EmployeeDependantDB();
                op = objEmployeeDependantDB.DeleteEmployeeDependant(StudentId);
            }

            if (op.Success)
            {
                result = "success";
                resultMessage = op.Message;
            }
            else
            {
                if (op.Message == "warning")
                {
                    result = "warning";
                    resultMessage = "Some error occurred. Unable to update family!";
                }
                else
                {
                    result = "error";
                    resultMessage = op.Message;
                }
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcelInternalEmployee(int employeeId, bool addCurrentPosition)
        {
            byte[] content;
            string fileName = "Internal Employee" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xlsx";
            InternalEmploymentDB internalEmployeementDB = new InternalEmploymentDB();
            System.Data.DataSet ds = internalEmployeementDB.GetInternalEmployeeDataset(employeeId, addCurrentPosition);
            ds.Tables[0].Columns["EmployeeName"].ColumnName = "Employee Name";
            ds.Tables[0].Columns["StartDate"].ColumnName = "Start Date";
            ds.Tables[0].Columns["EndDate"].ColumnName = "End Date";
            ds.Tables[0].Columns["DepartmentName_1"].ColumnName = "Department Name";
            ds.Tables[0].Columns["PositionTitle"].ColumnName = "Position Name";
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        public void ExportToPdfInternalEmployee(int employeeId, bool addCurrentPosition)
        {
            string fileName = "Internal Employee" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            InternalEmploymentDB internalEmployeementDB = new InternalEmploymentDB();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //CompanyDB objCompanyDB = new CompanyDB();
            System.Data.DataSet ds = internalEmployeementDB.GetInternalEmployeeDataset(employeeId, addCurrentPosition);
            ds.Tables[0].Columns["EmployeeName"].ColumnName = "Employee Name";
            ds.Tables[0].Columns["StartDate"].ColumnName = "Start Date";
            ds.Tables[0].Columns["EndDate"].ColumnName = "End Date";
            ds.Tables[0].Columns["DepartmentName_1"].ColumnName = "Department Name";
            ds.Tables[0].Columns["PositionTitle"].ColumnName = "Position Name";
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }

        public ActionResult GetInternalEmploymentList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, int EmployeeID = 0)
        {
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<InternalEmploymentModel> objInternalEmploymentList = new List<InternalEmploymentModel>();
            InternalEmploymentDB objInternalEmploymentDB = new InternalEmploymentDB();
            objInternalEmploymentList = objInternalEmploymentDB.GetInternalEmploymentList(EmployeeID);
            //---------------------------------------------
            HRMS.Entities.PermissionModel objEmpPermissionModel = new HRMS.Entities.PermissionModel();
            objEmpPermissionModel = HRMS.Web.CommonHelper.CommonHelper.CheckPermissionForUser("/Employee");
            var vList = new object();
            vList = new
            {
                aaData = (from item in objInternalEmploymentList
                          select new
                          {
                              Actions = "<a " + (objEmpPermissionModel.IsMainNavigationPermission == true ? "" : "disabled") + " class='btn btn-success btn-rounded btn-condensed btn-sm'  onclick='AddInternalEmployment(" + item.InternalEmploymentID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a " + (objEmpPermissionModel.IsMainNavigationPermission == true ? "" : "disabled") + " class='btn btn-danger btn-rounded btn-condensed btn-sm'  onclick='DeleteInternalEmploymentChannel(" + item.InternalEmploymentID.ToString() + "," + item.EmployeeID + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              InternalEmploymentID = item.InternalEmploymentID,
                              FirstName = item.FirstName_1,
                              LastName = item.SurName_1,
                              StartDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.StartDate),
                              EndDate = DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(item.EndDate),
                              Department = item.DepartmentName,
                              Position = item.PositionName,
                              Location = item.LocationName
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult GetUserImage(int EmployeeId)
        {
            EmployeeDB employeeDB = new EmployeeDB();
            var dataContain = employeeDB.GetUserImage(EmployeeId);
            bool IsValidImage = false;
            if (dataContain.Count() > 0)
            {
                byte[] ProfilepIc = employeeDB.GetUserImage(EmployeeId);
                IsValidImage = CommonHelper.CommonHelper.IsValidImage(ProfilepIc);
                if (IsValidImage)
                {
                    return File(employeeDB.GetUserImage(EmployeeId), "image/png");
                }
                else
                {
                    return File(CommonHelper.CommonHelper.GetBlankImage(), "image/png");
                }

            }
            else
            {
                return File(CommonHelper.CommonHelper.GetBlankImage(), "image/png");
            }
        }


        public ActionResult ChangeEmployeePassword(int Id)
        {
            ChangePassword changePassword = new ChangePassword();
            changePassword.EmployeeId = Id;
            //TempData["ChangePasswordModel"] = changePassword;
            //return View("ChangePassword", changePassword);
            //ViewBag.EmployeeId = changePassword.EmployeeId;
            //return Json(true, JsonRequestBehavior.AllowGet);
            return View("ChangePassword", changePassword);
        }

        public ActionResult ChangePassword()
        {
            ChangePassword changePassword = (ChangePassword)TempData["ChangePasswordModel"];

            return View();
        }

        public ActionResult UpdatePassword(ChangePassword changePassword)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeCredentialModel objEmployeeCredentialModel = new EmployeeCredentialModel();
            EmployeeCredentialDB objEmployeeCredentialDB = new EmployeeCredentialDB();
            OperationDetails objOperationDetails = new OperationDetails();
            EncryptDecrypt encryptpass = new EncryptDecrypt();
            objEmployeeCredentialModel.EmployeeId = changePassword.EmployeeId;
            objEmployeeCredentialModel.Password = changePassword.newPassword;    //encryptpass.EncryptPassword(changePassword.newPassword);
            objEmployeeCredentialModel.ModifiedBy = objUserContextViewModel.UserId;
            string message = objEmployeeCredentialDB.UpdateEmployeePassword(objEmployeeCredentialModel);
            if (message == "Success")
            {
                objOperationDetails.Success = true;
                objOperationDetails.Message = "Password updated successfully";
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Error : while updating password.";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckOldPass(string OldPass, string EmployeeId)
        {
            EmployeeCredentialDB objEmployeeCredentialDB = new EmployeeCredentialDB();
            int Passcount = objEmployeeCredentialDB.CheckOldPass(OldPass, EmployeeId);
            return Json(new { resultMessage = "Please enter correct old password", Passcount = Passcount }, JsonRequestBehavior.AllowGet);
        }

        #region Upload Profile Image

        public ActionResult EditEmployeeProfile(int Id)
        {
            EmployeeDetailsModel employeeDetailsModel = new EmployeeDetailsModel();
            employeeDetailsModel.EmployeeID = Id;
            return PartialView("_UploadProfileImage", employeeDetailsModel);
        }

        [HttpPost]
        public ActionResult UploadProfileImage(string base64String, int EmployeeID)
        {
            string success = "";
            string resultMessage = "something went wrong!!";
            try
            {
                base64String = base64String.Replace("data:image/jpeg;base64,", "");
                byte[] bytes = Convert.FromBase64String(base64String);
                string fileExtention = Convert.ToString(Session["ProfileFileExtension"]);
                new EmployeeDB().UploadPhoto(bytes, EmployeeID, fileExtention);
                Session["ProfileFileExtension"] = null;
                Session["ProfileLogo"] = null;
                resultMessage = "Photo is saved successfully.";
                success = "true";
            }
            catch (Exception ex)
            {
                success = "false";
            }
            return Json(new { message = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveProfileImage(int Id)
        {
            string result = "";
            string resultMessage = "";
            OperationDetails operationDetails = new OperationDetails();
            EmployeeDetailsModel employeeDetailsModel = new EmployeeDetailsModel();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModel.EmployeeID = Id;
            operationDetails = employeeDB.RemoveProfileImage(employeeDetailsModel);

            if (operationDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Profile image removed successfully.";

            }
            else
            {
                result = "error";
                resultMessage = "Error occured while removing profile image.";
            }

            return Json(new { EmployeeId = Id, result = result, resultMessage = resultMessage, returnpath = "/Content/images/Person-icon-grey.jpg" }, JsonRequestBehavior.AllowGet);
        }
        #endregion



    }
}