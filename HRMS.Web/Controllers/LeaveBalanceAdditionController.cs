﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class LeaveBalanceAdditionController : BaseController
    {
        // GET: LeaveBalanceAddition
        public ActionResult Index()
        {
            VacationDB vacationdb = new VacationDB();
            LeaveTypeDB objleavetypedb = new LeaveTypeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            var vacationTypeList = vacationdb.GetVactionTypeList().Where(x => x.id > 0).ToList();
            var vacationTypeListWithCategory = new LeaveBalanceAdditionDB().GetAllVacationTypeListData().ToList();
            var filtererdVacationTypeList = vacationTypeListWithCategory.Where(e => vacationTypeList.Any(z => z.id == e.VacationTypeId)).ToList();
            filtererdVacationTypeList.RemoveAll(s => s.VTCategoryID == 1);
            ViewBag.VacationType = new SelectList(filtererdVacationTypeList.Where(x => x.VacationTypeId > 0).ToList(), "VacationTypeId", "Name");
            return View();
        }

        public ActionResult GetLeaveBalanceAdditionList(int? employeeId, int? vacationTypeId)
        {
            List<HRMS.Entities.LeaveBalanceAddition> objLeaveBalanceAdditionList = new List<HRMS.Entities.LeaveBalanceAddition>();
            LeaveBalanceAdditionDB leaveBalanceAddDb = new LeaveBalanceAdditionDB();
            objLeaveBalanceAdditionList = leaveBalanceAddDb.GetLeaveBalanceAdditionList(employeeId, vacationTypeId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in objLeaveBalanceAdditionList
                          select new
                          {
                              Actions = "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='deleteLeaveBalance(" + item.LeaveAdditionID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              LeaveAdditionID = item.LeaveAdditionID,
                              EmployeeID = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              VacationTypeName = item.VacationTypeName,
                              VacationTypeID = item.VacationTypeID,
                              FullPaidDays = item.FullPaidDays,
                              HalfPaidDays = item.HalfPaidDays,
                              UnPaidDays= item.UnPaidDays,
                              LeaveEffectiveDate = item.LeaveEffectiveDate,
                              FormattedLeaveEffectiveDate = item.LeaveEffectiveDate.ToString(HRMSDateFormat),
                              Comments = item.Comments,
                              ACYearID = item.ACYearID,
                              CreatedBy = item.CreatedBy,
                              CreatedOn = item.CreatedOn,
                              ModifiedBy = item.ModifiedBy,
                              ModifiedOn = item.ModifiedOn,
                            
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
            //<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='editLaveBalance(" + item.LeaveAdditionID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>
        }

        public ActionResult AddEditLeaveBalanceForm(int? leaveBalanceId)
        {
            VacationDB vacationdb = new VacationDB();
            LeaveBalanceAdditionDB leaveBalanceAddDb = new LeaveBalanceAdditionDB();
            LeaveBalanceAddition leaveBalanceEdit=null;
            if (leaveBalanceId.HasValue)
            {
                leaveBalanceEdit = leaveBalanceAddDb.GetLeaveBalanceAdditionDetails(leaveBalanceId);
                ViewBag.VTCategoryID =
               leaveBalanceEdit.IsAddMode = false;
            }
            else
            {
                leaveBalanceEdit = new LeaveBalanceAddition();
                leaveBalanceEdit.IsAddMode = true;
            }

            LeaveTypeDB objleavetypedb = new LeaveTypeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            var vacationList = new VacationDB().GetVactionTypeListAsPerEmployeeId(leaveBalanceEdit.EmployeeID);
            var vacationTypeListWithCategory = new LeaveBalanceAdditionDB().GetAllVacationTypeListData().ToList();
            var filtererdVacationTypeList = vacationTypeListWithCategory.Where(e => vacationList.Any(z => z.id == e.VacationTypeId)).ToList();
            filtererdVacationTypeList.RemoveAll(s => s.VTCategoryID == 1);
            ViewBag.VacationType = new SelectList(filtererdVacationTypeList.Where(x => x.VacationTypeId > 0).ToList(), "VacationTypeId", "Name").ToList();
           
            return View(leaveBalanceEdit);
        }

        public ActionResult UpdateLeaveBalanceAdditionData(LeaveBalanceAddition leaveBalance)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            leaveBalance.CreatedBy = objUserContextViewModel.UserId;
            leaveBalance.ModifiedBy = objUserContextViewModel.UserId;
            LeaveBalanceAdditionDB leaveBalanceAddDb = new LeaveBalanceAdditionDB();
            OperationDetails operationDetailsObj = leaveBalanceAddDb.UpdateLeaveBalanceAdditionData(leaveBalance);
            string resultMessage = operationDetailsObj.Message;
            string result = (operationDetailsObj.Success) ? "success" : "error";
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteLeaveBalance(int leaveAdditionId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveBalanceAdditionDB leaveBalanceAddDb = new LeaveBalanceAdditionDB();
            OperationDetails operationDetailsObj = leaveBalanceAddDb.DeleteLeaveBalance(leaveAdditionId, objUserContextViewModel.UserId);
            string resultMessage = operationDetailsObj.Message;
            string result = (operationDetailsObj.Success) ? "success" : "error";
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadVacationTypesBasedOnEmployee(int employeeId)
        {
            var vacationList = new VacationDB().GetVactionTypeListAsPerEmployeeId(employeeId);
            var vacationTypeListWithCategory = new LeaveBalanceAdditionDB().GetAllVacationTypeListData().ToList();
            var filtererdVacationTypeList = vacationTypeListWithCategory.Where(e => vacationList.Any(z => z.id == e.VacationTypeId)).ToList();
            filtererdVacationTypeList.RemoveAll(s => s.VTCategoryID == 1);
            return Json(filtererdVacationTypeList.ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetVacationType(int leaveTypeID)
        {
            VacationTypeModel vt = new VacationDB().GetVacationTypeById(leaveTypeID); 
            return Json(vt, JsonRequestBehavior.AllowGet);
        }

    }
}