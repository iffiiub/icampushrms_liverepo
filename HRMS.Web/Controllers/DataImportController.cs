﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using System.Configuration;
using System.Globalization;
using HRMS.DataAccess.FormsDB;

namespace HRMS.Web.Controllers
{
    public class DataImportController : BaseController
    {
        // GET: DataImport
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UploadFile(HttpPostedFileBase file, int DataImportTypeId)
        {
            string fileName = "";
            DataImportDB objDataImportDB = new DataImportDB();
            DataTable DtImportedExcel = new DataTable();
            List<string> ColumnsList = new List<string>();
            ColumnsList = objDataImportDB.GetColumnList(DataImportTypeId);
            DateTime CreatedOn = DateTime.Now;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            EmployeeDocumentModel employeeDocumentModel = new EmployeeDocumentModel();
            if (file != null && file.ContentLength > 0)
            {
                fileName = Path.GetFileNameWithoutExtension(file.FileName);
                string fileExtension = Path.GetExtension(file.FileName);
                fileName = fileName + CreatedOn.ToString("ddMMyyyyhhmm") + fileExtension;
                string DirectoryName = Server.MapPath(ConfigurationManager.AppSettings["DataImportPath"].ToString());
                if (!Directory.Exists(DirectoryName))
                {
                    Directory.CreateDirectory(DirectoryName);
                }
                string FilePath = Path.Combine(DirectoryName, fileName);
                file.SaveAs(FilePath);
                string ErrorMessage = string.Empty;
                DtImportedExcel = objDataImportDB.ReadExcel(FilePath, fileExtension, out ErrorMessage);
                if (ErrorMessage != "")
                {
                    return Json(new { result = false, Message = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                bool bColumnMatch = true;
                Dictionary<string, int> ColumnListIndex = new Dictionary<string, int>();
                foreach (string ColumnName in ColumnsList)
                {
                    bColumnMatch = DtImportedExcel.Columns.Cast<DataColumn>().Any(x => x.ColumnName.Equals(ColumnName));
                    if (!bColumnMatch)
                    {
                        break;
                    }
                    int Index = DtImportedExcel.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Equals(ColumnName)).Select(s => s.Ordinal).FirstOrDefault();
                    ColumnListIndex.Add(ColumnName, Index);
                }

                if (bColumnMatch)
                {
                    if (DataImportTypeId == (int)DataImportModel.DataImportType.PayAddition)
                    {
                        List<DataImportPayAddition> Data = new List<DataImportPayAddition>();
                        DataImportPayAddition objDataImportPayAddition = new DataImportPayAddition();
                        for (int Row = 0; Row < DtImportedExcel.Rows.Count; Row++)
                        {
                            objDataImportPayAddition = new DataImportPayAddition();
                            objDataImportPayAddition.PaySalaryAllowanceName = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Pay Salary Allowance Name (EN)"])].ToString();
                            objDataImportPayAddition.EmployeeID = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Employee ID"])].ToString();
                            var addittionDate = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Addition Date"])].ToString();
                            objDataImportPayAddition.AdditionDate = DateTime.ParseExact(addittionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            objDataImportPayAddition.FormattedAdditionDate = objDataImportPayAddition.AdditionDate.ToString("dd/MM/yyyy");
                            objDataImportPayAddition.Amount = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Amount"])].ToString();
                            objDataImportPayAddition.Description = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Description"])].ToString();
                            Data.Add(objDataImportPayAddition);
                        }

                        OperationDetails operationDetails = objDataImportDB.SavePreviewDataImport(DataImportTypeId, fileName, FilePath, objUserContextViewModel.UserId, CreatedOn);
                        if (!operationDetails.Success)
                        {
                            return Json(new { result = false, Message = operationDetails.Message }, JsonRequestBehavior.AllowGet);
                        }
                        ViewBag.DataImportId = operationDetails.InsertedRowId;
                        return PartialView("_PayAdditionDataImport", Data);
                    }
                    else if (DataImportTypeId == (int)DataImportModel.DataImportType.Insurance)
                    {
                        List<DataImportInsurance> Data = new List<DataImportInsurance>();
                        DataImportInsurance dataImportInsurance = new DataImportInsurance();
                        for (int Row = 0; Row < DtImportedExcel.Rows.Count; Row++)
                        {
                            dataImportInsurance = new DataImportInsurance();
                            dataImportInsurance.InsuranceCategory = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Insurance Category"])].ToString();
                            dataImportInsurance.InsuranceEligibility = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Insurance Eligibility"])].ToString();
                            dataImportInsurance.CardNumber = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Card Number"])].ToString();
                            dataImportInsurance.PolicyNumber = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Policy Number"])].ToString();
                            dataImportInsurance.InsuranceAmount = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Insurance Amount"])].ToString();
                            dataImportInsurance.AmountPaidBySchool = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Amount Paid By School"])].ToString();
                            dataImportInsurance.EmployeeID = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Employee ID"])].ToString();

                            var expiryDate = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Expiry Date"])].ToString();
                            dataImportInsurance.ExpiryDate = GetDateFromString(expiryDate);
                            dataImportInsurance.FormattedExpiryDate = dataImportInsurance.ExpiryDate.ToString("dd/MM/yyyy");
                            var issueDate = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Issue Date"])].ToString();
                            dataImportInsurance.IssueDate = GetDateFromString(issueDate);
                            dataImportInsurance.FormattedIssueDate = dataImportInsurance.IssueDate.ToString("dd/MM/yyyy");
                            var cancellationdate = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Cancellation Date"])].ToString();
                            if (!string.IsNullOrEmpty(cancellationdate))
                            {
                                dataImportInsurance.CancellationDate = GetDateFromString(cancellationdate);
                                dataImportInsurance.FormattedCancellationDate = dataImportInsurance.CancellationDate.Value.ToString("dd/MM/yyyy");
                            }

                            dataImportInsurance.Note = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Note"])].ToString();
                            Data.Add(dataImportInsurance);
                        }

                        OperationDetails operationDetails = objDataImportDB.SavePreviewDataImport(DataImportTypeId, fileName, FilePath, objUserContextViewModel.UserId, CreatedOn);
                        if (!operationDetails.Success)
                        {
                            return Json(new { result = false, Message = operationDetails.Message }, JsonRequestBehavior.AllowGet);
                        }
                        var EmployeeAlternativeIdList = Data.Select(s => s.EmployeeID).Distinct().ToList();
                        string EmployeeAlternativeIds = string.Join(",", EmployeeAlternativeIdList);
                        ViewBag.EmployeeDetailsList = new DataImportDB().GetEmployeeIdsRelatedAlternativeIds(EmployeeAlternativeIds);
                        ViewBag.InsuranceCategories = new InsuranceDB().GetInsuranceTypeList();
                        ViewBag.InsuranceEligibility = new InsuranceDB().GetInsuranceEligibilityList();
                        ViewBag.DataImportId = operationDetails.InsertedRowId;
                        return PartialView("_InsuranceDataImport", Data);
                    }
                    else if (DataImportTypeId == (int)DataImportModel.DataImportType.Accommodation)
                    {
                        List<DataImportAccommodation> Data = new List<DataImportAccommodation>();
                        DataImportAccommodation dataImportAccommodation = new DataImportAccommodation();
                        for (int Row = 0; Row < DtImportedExcel.Rows.Count; Row++)
                        {
                            dataImportAccommodation = new DataImportAccommodation();
                            dataImportAccommodation.Type = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Type"])].ToString();
                            dataImportAccommodation.ApartmentNo = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Apartment No"])].ToString();
                            dataImportAccommodation.Building = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Building"])].ToString();
                            dataImportAccommodation.Floor = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Floor"])].ToString();
                            dataImportAccommodation.Street = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Street"])].ToString();
                            dataImportAccommodation.Country = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Country"])].ToString();
                            dataImportAccommodation.City = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["City"])].ToString();
                            dataImportAccommodation.EmployeeID = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Employee ID"])].ToString();

                            Data.Add(dataImportAccommodation);
                        }

                        OperationDetails operationDetails = objDataImportDB.SavePreviewDataImport(DataImportTypeId, fileName, FilePath, objUserContextViewModel.UserId, CreatedOn);
                        if (!operationDetails.Success)
                        {
                            return Json(new { result = false, Message = operationDetails.Message }, JsonRequestBehavior.AllowGet);
                        }
                        var EmployeeAlternativeIdList = Data.Select(s => s.EmployeeID).Distinct().ToList();
                        string EmployeeAlternativeIds = string.Join(",", EmployeeAlternativeIdList);
                        ViewBag.EmployeeDetailsList = new DataImportDB().GetEmployeeIdsRelatedAlternativeIds(EmployeeAlternativeIds);
                        ViewBag.Country = new CountryDB().GetAllContries();
                        ViewBag.City = new CityDB().GetAllCity();
                        ViewBag.AccommodationTypeList = new AllowanceDB().GetAccommodationtype();
                        ViewBag.DataImportId = operationDetails.InsertedRowId;
                        return PartialView("_AccommodationDataImport", Data);
                    }
                    else if (DataImportTypeId == (int)DataImportModel.DataImportType.Airfare)
                    {
                        List<DataImportAirfare> Data = new List<DataImportAirfare>();
                        DataImportAirfare dataImportAirfare = new DataImportAirfare();
                        for (int Row = 0; Row < DtImportedExcel.Rows.Count; Row++)
                        {
                            dataImportAirfare = new DataImportAirfare();
                            dataImportAirfare.AirTicketCountry = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Air Ticket Country"])].ToString();
                            dataImportAirfare.AirTicketCity = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Air Ticket City"])].ToString();
                            dataImportAirfare.Airport = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Airport"])].ToString();
                            dataImportAirfare.AirTicketAmount = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Air Ticket Amount"])].ToString();
                            dataImportAirfare.AirfareFrequency = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Air Ticket Frequency"])].ToString();
                            dataImportAirfare.AirTicketClass = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Air Ticket Class"])].ToString();
                            dataImportAirfare.NoOfDependencies = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["No Of Tickets For Dependants"])].ToString();
                            dataImportAirfare.AirTicketAmountDependannt = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Air Ticket Amount For Dependants"])].ToString();
                            dataImportAirfare.TicketClassForDependant = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Ticket Class For Dependants"])].ToString();
                            dataImportAirfare.EmployeeID = DtImportedExcel.Rows[Row][Convert.ToInt32(ColumnListIndex["Employee ID"])].ToString();

                            Data.Add(dataImportAirfare);
                        }

                        OperationDetails operationDetails = objDataImportDB.SavePreviewDataImport(DataImportTypeId, fileName, FilePath, objUserContextViewModel.UserId, CreatedOn);
                        if (!operationDetails.Success)
                        {
                            return Json(new { result = false, Message = operationDetails.Message }, JsonRequestBehavior.AllowGet);
                        }
                        var EmployeeAlternativeIdList = Data.Select(s => s.EmployeeID).Distinct().ToList();
                        string EmployeeAlternativeIds = string.Join(",", EmployeeAlternativeIdList);
                        ViewBag.EmployeeDetailsList = new DataImportDB().GetEmployeeIdsRelatedAlternativeIds(EmployeeAlternativeIds);
                        ViewBag.Country = new CountryDB().GetAllContries();
                        ViewBag.TicketClasses = new EmployeeDB().GetAllAirFareClasses();
                        ViewBag.EmployeeAirfareFrequencyList = new EmployeeProfileCreationFormDB().GetEmployeeAirfareFrequencyList();
                        ViewBag.AirPortList = new EmployeeProfileCreationFormDB().GetAirPortByCountryId(0);
                        ViewBag.City = new CityDB().GetAllCity();
                        ViewBag.DataImportId = operationDetails.InsertedRowId;
                        return PartialView("_AirfareDataImport", Data);
                    }
                }
                else
                {
                    return Json(new { result = false, Message = "Column not match" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { result = false, Message = "Upload File again" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get SQL FormatDate
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetDateFromString(string date)
        {
            DateTime da = new DateTime();
            if (string.IsNullOrEmpty(date) == true || date.IndexOf("/") == -1 || date.Split("/".ToArray()).Length != 3)
            {
                return da;
            }
            else
            {

                int year = 0, month = 0, day = 0;
                date = date.Trim();
                date = date.IndexOf(" ") > 5 ? date.Substring(0, date.IndexOf(" ")) : date;
                if (int.TryParse(date.Split("/".ToArray())[2], out year) && int.TryParse(date.Split("/".ToArray())[0], out day) && int.TryParse(date.Split("/".ToArray())[1], out month))
                {
                    da = new DateTime(year, month, day);
                }
                return da;

            }

        }
        public ActionResult ImportExcelToPayAddition(string PayAdditions, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                List<PayAddition> lstPayAddion = JsonConvert.DeserializeObject<List<PayAddition>>(PayAdditions);
                DataImportDB objDataImportDB = new DataImportDB();
                operationDetails = objDataImportDB.SaveImportDataPayAddition(lstPayAddion, DataImportID);
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportExcelToInsurance(string Insurances, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                List<InsuranceModel> insuranceList = JsonConvert.DeserializeObject<List<InsuranceModel>>(Insurances);
                DataImportDB objDataImportDB = new DataImportDB();
                operationDetails = objDataImportDB.SaveImportDataInsurance(insuranceList, DataImportID);
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportExcelToAccommodation(string Allowance, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                List<AllowanceModel> accommodationList = JsonConvert.DeserializeObject<List<AllowanceModel>>(Allowance);
                DataImportDB objDataImportDB = new DataImportDB();
                operationDetails = objDataImportDB.SaveImportDataAccommodation(accommodationList, DataImportID);
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportExcelToAirfare(string Airfare, int DataImportID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                List<EmployeeAirFare> airfareList = JsonConvert.DeserializeObject<List<EmployeeAirFare>>(Airfare);
                DataImportDB objDataImportDB = new DataImportDB();
                operationDetails = objDataImportDB.SaveImportDataAirafare(airfareList, DataImportID);
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataImportFileTemplatePath(int DataImportTypeId)
        {
            DataImportDB objDataImportDB = new DataImportDB();
            string FilePath = objDataImportDB.GetDataImportFileTemplatePath(DataImportTypeId);
            string FileName = Path.GetFileName(FilePath);

            return Json(new { FileName = FileName }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadExcel(string FileName, int DataImportTypeId)
        {
            DataImportDB objDataImportDB = new DataImportDB();
            string FilePath = objDataImportDB.GetDataImportFileTemplatePath(DataImportTypeId);
            FilePath = Server.MapPath(FilePath);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(FilePath);
            Response.End();
            return new EmptyResult();
        }
    }
}