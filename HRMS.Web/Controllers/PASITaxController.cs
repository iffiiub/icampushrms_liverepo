﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class PASITaxController : BaseController
    {
        public ActionResult Index()
        {

            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            PASITAxDB PasiDb = new PASITAxDB();
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Month", Value = "0" });
            selectListItems.Add(new SelectListItem { Text = "January (1)", Value = "1" });
            selectListItems.Add(new SelectListItem { Text = "February (2)", Value = "2" });
            selectListItems.Add(new SelectListItem { Text = "March (3)", Value = "3" });
            selectListItems.Add(new SelectListItem { Text = "April (4)", Value = "4" });
            selectListItems.Add(new SelectListItem { Text = "May (5)", Value = "5" });
            selectListItems.Add(new SelectListItem { Text = "June (6)", Value = "6" });
            selectListItems.Add(new SelectListItem { Text = "July (7)", Value = "7" });
            selectListItems.Add(new SelectListItem { Text = "August (8)", Value = "8" });
            selectListItems.Add(new SelectListItem { Text = "September (9)", Value = "9" });
            selectListItems.Add(new SelectListItem { Text = "October (10)", Value = "10" });
            selectListItems.Add(new SelectListItem { Text = "November (11)", Value = "11" });
            selectListItems.Add(new SelectListItem { Text = "December (12)", Value = "12" });
            foreach (var item in selectListItems)
            {
                if (item.Value == DateTime.Now.Month.ToString())
                    item.Selected = true;
            }
            ViewBag.MonthList = selectListItems;//.Where(x=>x.Value == DateTime.Now.Month.ToString()).ToList().ForEach(i=>i.Selected = true);
            bool isPostDeductionrecordEnable = new AbsentDeductionDB().GetDeductionSetting().PostAbsentRecords;
            ViewBag.isPostDeductionrecordEnable = isPostDeductionrecordEnable;
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Year", Value = "0" });
            foreach (var items in objAcademicYearDB.GetAcademicYears())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.Year.ToString(),
                    Value = items.Year.ToString()
                };
                selectListItems.Add(selectListItem);
            }
            foreach (var item in selectListItems)
            {
                if (item.Value == DateTime.Now.Year.ToString())
                    item.Selected = true;
            }
            ViewBag.YearList = selectListItems;//.Where(x => x.Value == DateTime.Now.Year.ToString()).ToList().FirstOrDefault().Selected = true;

            PayCycleDB objPayCycleDB = new PayCycleDB();
            selectListItems = new List<SelectListItem>();

            foreach (var items in objPayCycleDB.GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                    Selected = items.active

                };
                selectListItems.Add(selectListItem);
            }
            ViewBag.CyclesList = selectListItems;
           // ViewBag.CyclesList = new SelectList(payrollDB.getPaycycles(payrollModel), "PayCycleID", "PayCycleName_1");
            TaxSetting pasiSetting = PasiDb.GetPasiTaxtSetting();
            return View(pasiSetting);
        }

        public ActionResult GetPASITaxList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "PayEmployeeAbsentID", int iSortCol_0 = 0, string PASIPercentage = "0")
        {
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int pageindex = pageNumber + 1;
            List<PASITaxModel> objPASITaxModelList = new List<PASITaxModel>();
            PASITAxDB objPASITAxDB = new PASITAxDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //TaxSetting pasiSetting = objPASITAxDB.GetPasiTaxtSetting();
           // bool displayTaxPercentage = pasiSetting.DisplayTaxPercentage;
            if (PASIPercentage== "JDTaxStatus")
            {
                objPASITaxModelList = objPASITAxDB.GetJordanPASITaxList();
            }
            else
            {
                objPASITaxModelList = objPASITAxDB.GetPASITaxList();
            }            
            int Count = objPASITaxModelList.Count;
            var vList = new object();
            vList = new
            {
                aaData = (from item in objPASITaxModelList
                          select new
                          {
                              CheckRow = "<input data-Id='" + item.EmployeeID + "' type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'Chkcheck" + item.EmployeeID + "' name = 'check" + item.EmployeeID + "'/>",
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeID = item.EmployeeID,
                              FullName = item.FullName,
                              Amount = item.Amount.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              PASI_TAX = item.PASI_TAX.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              Percentage = "<input onkeyup='calculateTax(this)' onchange='saveTaxSetting(this)' data-Max='100' data-Min='0' type='text' value='" + item.PasiPercentage + "' class='form-control inputPercentage'  />"
                          }).ToArray(),
                recordsTotal = Count,
                recordsFiltered = Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAbsent()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();

            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

            AbsentModel objAbsentModel = new AbsentModel();

            objAbsentModel.EmployeeID = empid;
            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            //HRContractTypeDB HRContractTypeDB = new HRContractTypeDB();
            //ViewBag.HRContractType = new SelectList(HRContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1"); 
            return View(objAbsentModel);
        }

        [HttpPost]
        public ActionResult AddAbsent(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objAbsentModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objAbsentDB.InsertAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditAbsent(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();


            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).LastName;

            return View(objAbsentModel);
        }

        [HttpPost]
        public JsonResult EditAbsent(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //     objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentModelDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                //if (objAbsentModel.PayEmployeeAbsentFromDate != "")
                //    objAbsentModel.PayEmployeeAbsentFromDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentFromDate).ToShortDateString();
                //if (objAbsentModel.PayEmployeeAbsentToDate != "")
                //    objAbsentModel.PayEmployeeAbsentToDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentToDate).ToShortDateString();
                objOperationDetails = objAbsentModelDB.UpdateAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();




                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;


                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpdateLeaveType(int LeavetypeID, int PayEmployeeAbsentID)
        {
            AbsentDB objAbsentModelDB = new AbsentDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAbsentModelDB.UpdateLeavetypeAbsent(LeavetypeID, PayEmployeeAbsentID);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewAbsent(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;

            return View(objAbsentModel);
        }

        public ActionResult DeleteAbsent(int id)
        {
            AbsentDB objAbsentDB = new AbsentDB();
            objAbsentDB.DeleteAbsentById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        // public ActionResult PushRecords(List<PASITaxModel> PasitaxLists, string effectivedate, int cycleid, string cyclename)
        public ActionResult PushRecords(List<PASITaxModel> PASITaxList)
        {
            //List<PASITaxModel> PASITaxList = PasitaxLists;
            //string effectivedate = "", cyclename = "";
            //int cycleid = 1;
            OperationDetails objOperationDetails = new OperationDetails();
            PASITAxDB objPASITAxDB = new PASITAxDB();
            bool success = false;
            //delete tax
            bool IsPasi = false;
            int DeductionTypeid = objPASITAxDB.GetPasiDeductionTypeID(ref IsPasi);
            if (DeductionTypeid > 0)
            {
                if (PASITaxList.Count > 0)
                {
                    string effectivedate = ""; int cycleid = 0; string cyclename = "";
                    effectivedate = PASITaxList[0].EffectiveDate.ToString();
                    cycleid = Convert.ToInt32(PASITaxList[0].Cycleid.ToString());
                    cyclename = PASITaxList[0].Cyclename.ToString();
                    for (int i = 0; i < PASITaxList.Count; i++)
                    {
                        objOperationDetails = objPASITAxDB.CheackPayDetailIsPresent(cycleid, DeductionTypeid, PASITaxList[i].EmployeeID);
                        if (objOperationDetails.Success)
                        {
                            PASITaxModel objPASITaxModel = new Entities.PASITaxModel();
                            objPASITaxModel.Amount = Convert.ToDecimal(PASITaxList[i].Amount.ToString());
                            objPASITaxModel.EmployeeID = Convert.ToInt32(PASITaxList[i].EmployeeID.ToString());
                            objPASITaxModel.EmployeeAlternativeID = PASITaxList[i].EmployeeAlternativeID.ToString();
                            objPASITaxModel.FullName = PASITaxList[i].FullName.ToString();
                            objPASITaxModel.PASI_TAX = Convert.ToDecimal(PASITaxList[i].PASI_TAX.ToString());
                            objOperationDetails = objPASITAxDB.InsertPASITAXintoPayDeduct(objPASITaxModel, cycleid, DeductionTypeid, effectivedate, cyclename, IsPasi);
                            success = true;
                        }
                    }
                }
            }
            if (success)
                return Json(new { success = true, msg = "Records have been posted succesfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, msg = "Record is exist" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePasiTax(List<PASITaxModel> PASITaxList)
        {
            string effectivedate = PASITaxList[0].EffectiveDate.ToString();
            int cycleid = Convert.ToInt32(PASITaxList[0].Cycleid.ToString());
            PASITAxDB objPASITAxDB = new PASITAxDB();
            var result = objPASITAxDB.UpdatePasiTax(PASITaxList, effectivedate, cycleid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCycleDataById(int cycleId, string date)
        {
            PayCycleDB paycycledb = new PayCycleDB();
            DateTime efficientDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(date));
            var cycleModel = paycycledb.GetPayCycleById(cycleId);
            DateTime startDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(cycleModel.DateFrom));
            DateTime endDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(cycleModel.DateTo));
            bool retuenVal = (startDate <= efficientDate && endDate >= efficientDate);
            return Json(retuenVal, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTax(bool taxForEmployee, bool taxForAll, decimal taxPercentage)
        {
            PASITAxDB pasiDb = new PASITAxDB();
            return Json(pasiDb.SavePasiTaxSetting(taxForEmployee, taxForAll, taxPercentage), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTaxPerEmployee(int employeeId, decimal taxPercentage, decimal taxAmount)
        {
            PASITAxDB pasiDb = new PASITAxDB();
            return Json(pasiDb.SavePasiTaxPerSetting(employeeId, taxPercentage,taxAmount));
        }

        //#region PASI DATA FUNCTIONS

        //public int CheckPASIExistOnCycleID(string cycleid)
        //{

        //    try
        //    {

        //        int Deductiontype = 0;
        //        Deductiontype = GetPasiDeductionTypeID();
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandText = " SELECT COUNT(*) FROM HR_PayDeduction WHERE  PaidCycle=" + cycleid + " AND DeductionTypeID= " + Deductiontype
        //                          ;
        //        cmd.Connection = helper.GetConnectionObject();
        //        int count = Convert.ToInt32(cmd.ExecuteScalar());
        //        return count;
        //    }
        //    catch (Exception)
        //    {

        //        return 0;
        //    }
        //}
        //public int DeletePASIOnCycleID(string cycleid)
        //{

        //    try
        //    {
        //        int Deductiontype = 0;
        //        Deductiontype = GetPasiDeductionTypeID();

        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandText = " DELETE FROM dbo.HR_PayDeductionDetail WHERE  PayDeductionID IN (SELECT PayDeductionID FROM HR_PayDeduction " +
        //                          " WHERE  cycleid=" + cycleid + " AND DeductionTypeID=" + Deductiontype + ") ; " +
        //                          " DELETE FROM HR_PayDeduction WHERE  cycleid=" + cycleid + " AND DeductionTypeID=" + Deductiontype + " ;"
        //                          ;
        //        cmd.Connection = helper.GetConnectionObject();
        //        int count = Convert.ToInt32(cmd.ExecuteScalar());
        //        return count;
        //    }
        //    catch (Exception)
        //    {

        //        return 0;
        //    }
        //}
        //public DataTable GetPASITaxAllTeachers(decimal taxPercent)
        //{
        //    try
        //    {

        //        DataTable dt = new DataTable();
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "[HR_USPGetPASITax]";
        //        cmd.Connection = helper.GetConnectionObject();
        //        // parameters
        //        cmd.Parameters.AddWithValue("@PASIPercentage", taxPercent);
        //        //============
        //        SqlDataAdapter adt = new SqlDataAdapter(cmd);
        //        adt.Fill(dt);
        //        return dt;
        //    }
        //    catch (Exception)
        //    {

        //        return null;
        //    }

        //}





        //int GetPasiDeductionTypeID()
        //{


        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandText = "  SELECT DeductionTypeID FROM dbo.GEN_PayDeductionType WHERE DeductionTypeName_1='pasi'";
        //    cmd.Connection = helper.GetConnectionObject();

        //    return Convert.ToInt32(cmd.ExecuteScalar() ?? "0");

        //}

        //public int InsertPASITAXintoPayDeduct(string cycleid, string cyclename, decimal passiamount, int teacherid, DateTime effectivedate)
        //{
        //    try
        //    {

        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "[HR_xspPayDeductionCUD]";
        //        cmd.Connection = helper.GetConnectionObject();
        //        int Deductiontype = 0;
        //        Deductiontype = GetPasiDeductionTypeID();
        //        // parameters ======================
        //        cmd.Parameters.AddWithValue("@aTntTranMode", 1);
        //        cmd.Parameters.AddWithValue("@aTntDeductionTypeID", Deductiontype);
        //        cmd.Parameters.AddWithValue("@aNumPaidCycle", 1);
        //        cmd.Parameters.AddWithValue("@CycleID", cycleid);
        //        cmd.Parameters.AddWithValue("@aBitIsInstallment", 0);
        //        cmd.Parameters.AddWithValue("@aNumAmount", passiamount);
        //        cmd.Parameters.AddWithValue("@aNumInstallment", 1);
        //        cmd.Parameters.AddWithValue("@aNvrComments", string.Format(" PASI for {0} .", cyclename));
        //        cmd.Parameters.AddWithValue("@aIntTeacherID", teacherid);
        //        cmd.Parameters.AddWithValue("@aNvrRefNumber", string.Format("PASI for {0} .", cyclename));
        //        cmd.Parameters.AddWithValue("@aDttEffectiveDate", effectivedate);
        //        cmd.Parameters.AddWithValue("@aDttTransactionDate", DateTime.Now);
        //        cmd.Parameters.AddWithValue("@aIntPvId", DBNull.Value);
        //        cmd.Parameters.AddWithValue("@output", 0);


        //        //==================================

        //        return cmd.ExecuteNonQuery();

        //    }
        //    catch (Exception)
        //    {

        //        return 0;
        //    }

        //}

        //#endregion
    }
}