﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;


namespace HRMS.Web.Controllers
{
    public class GovernmentCycleController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetGovernmentCycleList()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();            
            List<HRMS.Entities.GovernmentCycleModel> objGovernmentCycleList = new List<HRMS.Entities.GovernmentCycleModel>();
            GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();
            objGovernmentCycleList = objGovernmentCycleDB.GetAllGovernmentCycle();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objGovernmentCycleList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.GovernmentCycleID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.GovernmentCycleID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              GovernmentCycleID = item.GovernmentCycleID,
                              GovernmentCycleName_1 = item.GovernmentCycleName_1,
                              GovernmentCycleName_2 = item.GovernmentCycleName_2,
                              GovernmentCycleName_3 = item.GovernmentCycleName_3,
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            HRMS.Entities.GovernmentCycleModel objGovernmentCycle = new HRMS.Entities.GovernmentCycleModel();
            GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();

            if (id != -1)
            {
                objGovernmentCycle = objGovernmentCycleDB.GetGovernmentCycle(id);
            }
            else
            {
                objGovernmentCycle.GovernmentCycleID = id;
            }

            return View(objGovernmentCycle);
        }

        public JsonResult Save(GovernmentCycleModel objGovernmentCycle)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();

                if (objGovernmentCycle.GovernmentCycleID == -1)
                {
                    objGovernmentCycleDB.AddUpdateDeleteGovernmentCycle(objGovernmentCycle, 1);

                }
                else
                {
                    objGovernmentCycleDB.AddUpdateDeleteGovernmentCycle(objGovernmentCycle, 2);
                }

                //return Redirect("Index");
                if (objGovernmentCycle.GovernmentCycleID == 0)
                {
                    result = "success";
                    resultMessage = "Government Cycle Added Successfully.";
                }
                else if (objGovernmentCycle.GovernmentCycleID != 0)
                {
                    result = "success";
                    resultMessage = "Government Cycle Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while adding Government Cycle.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();
            HRMS.Entities.GovernmentCycleModel objGovernmentCycle = new HRMS.Entities.GovernmentCycleModel();
            objGovernmentCycle.GovernmentCycleID = int.Parse(id);
            string msg = objGovernmentCycleDB.AddUpdateDeleteGovernmentCycle(objGovernmentCycle, 3);
            //return Redirect("Index");
            //return Json(0, JsonRequestBehavior.AllowGet);
            if (msg == "")
                return Json(new { result = "warning", resultMessage = "This Government cycle has been assigned to Employee. Unable to delete the record" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { result = "success", resultMessage = "Government Cycle Deleted Successfully." }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ExportToExcel()
        //{
        //    byte[] content;
        //    string fileName = "GovernmentCycle" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    //ProfileDB profileDB = new ProfileDB();

        //    int pageNumber = 0;
        //    int numberOfRecords = 0;
        //    string sortColumn = "GovernmentCycleID";
        //    string sortOrder = "DESC";
        //    int GovernmentCycleID = objUserContextViewModel.CompanyId;
        //    int totalCount = 0;
        //    List<HRMS.Entities.GovernmentCycleModel> objGovernmentCycleList = new List<HRMS.Entities.GovernmentCycleModel>();
        //    GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();
        //    objGovernmentCycleList = objGovernmentCycleDB.GetAllGovernmentCycle(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount, "");
        //    var report = (from item in objGovernmentCycleList.AsEnumerable()
        //                  select new
        //                  {
        //                      //Id = item.Id,
        //                      GovernmentCycleID = item.GovernmentCycleID,
        //                      GovernmentCycleName_1 = item.GovernmentCycleName_1,
        //                      GovernmentCycleName_2 = item.GovernmentCycleName_2,
        //                      GovernmentCycleName_3 = item.GovernmentCycleName_3,
        //                  }).ToList();
        //    content = ExportListToExcel(report);
        //    return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        //}

        //public void ExportToPdf()
        //{
        //    Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

        //    string fileName = "GovernmentCycle" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    //ProfileDB profileDB = new ProfileDB();

        //    int pageNumber = 0;
        //    int numberOfRecords = 0;
        //    string sortColumn = "GovernmentCycleID";
        //    string sortOrder = "Desc";
        //    int GovernmentCycleID = objUserContextViewModel.CompanyId;
        //    int totalCount = 0;
        //    List<HRMS.Entities.GovernmentCycleModel> objGovernmentCycleList = new List<HRMS.Entities.GovernmentCycleModel>();
        //    GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();
        //    objGovernmentCycleList = objGovernmentCycleDB.GetAllGovernmentCycle(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount, "");
        //    var report = (from item in objGovernmentCycleList.AsEnumerable()
        //                  select new
        //                  {
        //                      //Id = item.Id,
        //                      GovernmentCycleID = item.GovernmentCycleID,
        //                      GovernmentCycleName_1 = item.GovernmentCycleName_1,
        //                      GovernmentCycleName_2 = item.GovernmentCycleName_2,
        //                      GovernmentCycleName_3 = item.GovernmentCycleName_3,
        //                  }).ToList();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    GridView gridvw = new GridView();
        //    gridvw.DataSource = report; //bind the data table to the grid view
        //    gridvw.DataBind();
        //    StringWriter swr = new StringWriter();
        //    HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        //    gridvw.RenderControl(tw);
        //    StringReader sr = new StringReader(swr.ToString());
        //    Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    pdfDoc.Open();
        //    htmlparser.Parse(sr);
        //    pdfDoc.Close();
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}
        public void ExportToPdf()
        {
            string fileName = "GovernmentCycle" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();
            System.Data.DataSet ds = objGovernmentCycleDB.GetGovernmentCycleDatasSet();

            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }

        public ActionResult ExportToExcel()
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "GovernmentCycle" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            GovernmentCycleDB objGovernmentCycleDB = new GovernmentCycleDB();
            System.Data.DataSet ds = objGovernmentCycleDB.GetGovernmentCycleDatasSet();

            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
    }
}