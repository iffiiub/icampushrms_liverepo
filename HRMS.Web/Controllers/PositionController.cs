﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class PositionController : BaseController
    {
        //
        // GET: /Position/
        public ActionResult Index()
        {
            ViewBag.Company =  GetCompanySelectList();
            return View();
        }

        public ActionResult GetPositionList(int? companyId)
        {
            DBHelper objDBHelper = new DBHelper();             
            List<PositionModel> objPositionModelList = new List<PositionModel>();
            PositionDB objPositionDB = new PositionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objPositionModelList = objPositionDB.GetPositionListWithPaging(companyId,objUserContextViewModel.UserId);
            //---------------------------------------------
            var employmentTypes = objDBHelper.GetEmploymentTypeList();
            var regTemps = objDBHelper.GetRegTempList();
            var vList = new object();

            vList = new
            {
                aaData = (from item in objPositionModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditPosition(" + item.PositionID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeletePosition(" + item.PositionID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewPosition(" + item.PositionID.ToString() + ")' title='View Details' ><i class='fa fa-eye'></i></a>",
                              PositionID = item.PositionID,
                              PositionTitle = item.PositionTitle,
                              PositionTitle_2 = item.PositionTitle_2,
                              PositionTitle_3 = item.PositionTitle_3,
                              //Naresh 2020-03-19 CompanyId added and DepartmentId removed
                              //DepartmentName = item.departmentName,
                              CompanyName = item.CompanyName,
                              IsActive = (item.IsActive == true ? "<input type=\"checkbox\" disabled=\"disabled\" checked />" : "<input type=\"checkbox\" disabled=\"disabled\"  />"),//objDBHelper.GetStatusList().Where(x => x.StatusID == item.StatusID).Select(x => x.StatusName).SingleOrDefault(),
                              EmploymentTypeID = employmentTypes.Where(x => x.EmploymentTypeID == item.EmploymentTypeID).Select(x => x.EmploymentTypeName).SingleOrDefault(),
                              Reg_TempID = regTemps.Where(x => x.Reg_TempID == item.Reg_TempID).Select(x => x.Reg_TempName).SingleOrDefault(),                            
                              isAcademic = (item.IsAcademic == true ? "<input type=\"checkbox\" disabled=\"disabled\" checked />" : "<input type=\"checkbox\" disabled=\"disabled\"  />")
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPosition()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DBHelper objDBHelper = new DBHelper();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            List<JobFamilyModel> objJobFamilyModel = new List<JobFamilyModel>();
            objJobFamilyModel = objDBHelper.GetJobFamilyList();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Job Family", Value = "0" });
            foreach (var item in objJobFamilyModel)
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.JobFamilyName + "/" + item.JobFamilyNameFrench, Value = item.JobFamilyID.ToString() });
            }
            //ViewBag.Department = new SelectList(objDepartmentDB.GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
            ViewBag.JobFamily = ObjSelectedList;
            ViewBag.Status = new SelectList(objDBHelper.GetStatusList(), "StatusID", "StatusName");
            ViewBag.RegTemp = new SelectList(objDBHelper.GetRegTempList(), "Reg_TempID", "Reg_TempName");
            ViewBag.Employment = new SelectList(objDBHelper.GetEmploymentTypeList(), "EmploymentTypeID", "EmploymentTypeName");
            //Naresh 2020-03-19 Added organization filter
            ViewBag.Company =  GetCompanySelectList();
            return View();
        }

        [HttpPost]
        public ActionResult AddPosition(PositionModel objPositionModel)
        {
            DBHelper objDBHelper = new DBHelper();
            if (ModelState.IsValid)
            {
                PositionDB objPositionModelDB = new PositionDB();
                JobFamilyModel objJobFamilyModel = new JobFamilyModel();
                objJobFamilyModel.JobFamilyName = Request.Form["newJobFamily"].ToString();
                objJobFamilyModel.JobFamilyNameFrench = Request.Form["newJobFamilyFrench"].ToString();
                if ((objJobFamilyModel.JobFamilyName != null && objJobFamilyModel.JobFamilyName != "") || (objJobFamilyModel.JobFamilyNameFrench != null && objJobFamilyModel.JobFamilyNameFrench != ""))
                {
                    objPositionModel.JobFamilyID = objDBHelper.InsertJobFamily(objJobFamilyModel).InsertedRowId;
                }
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objPositionModelDB.InsertPosition(objPositionModel);
                objOperationDetails.InitiatorIdentity = Request.Form["hidFormSubmissionInitiatorIdentity"].ToString();
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                //if (objOperationDetails.InitiatorIdentity == "btnSubmitAndNew")
                //{
                //    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    return RedirectToAction("Index");
                //}

            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the required fields correctly, including job information.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                // return View(objPositionModel);
            }
        }

        public ActionResult EditPosition(int id)
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DBHelper objDBHelper = new DBHelper();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            List<JobFamilyModel> objJobFamilyModel = new List<JobFamilyModel>();
            objJobFamilyModel = objDBHelper.GetJobFamilyList();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var item in objJobFamilyModel)
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.JobFamilyName + "/" + item.JobFamilyNameFrench, Value = item.JobFamilyID.ToString() });
            }
            //ViewBag.Department = new SelectList(objDepartmentDB.GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
            ViewBag.JobFamily = ObjSelectedList;
            ViewBag.Status = new SelectList(objDBHelper.GetStatusList(), "StatusID", "StatusName");
            ViewBag.RegTemp = new SelectList(objDBHelper.GetRegTempList(), "Reg_TempID", "Reg_TempName");
            ViewBag.Employment = new SelectList(objDBHelper.GetEmploymentTypeList(), "EmploymentTypeID", "EmploymentTypeName");
            ViewBag.Company =  GetCompanySelectList();
            PositionDB objPositionModelDB = new PositionDB();
            PositionModel objPositionModel = new PositionModel();
            objPositionModel = objPositionModelDB.GetPositionById(id);
            return View(objPositionModel);
        }

        [HttpPost]
        public JsonResult EditPosition(PositionModel objPositionModel)
        {
            DBHelper objDBHelper = new DBHelper();
            if (ModelState.IsValid)
            {
                PositionDB objPositionModelDB = new PositionDB();
                JobFamilyModel objJobFamilyModel = new JobFamilyModel();
                //objJobFamilyModel.JobFamilyName = Request.Form["newJobFamily"].ToString();
                //objJobFamilyModel.JobFamilyNameFrench = Request.Form["newJobFamilyFrench"].ToString();
                if (objJobFamilyModel.JobFamilyName != null && objJobFamilyModel.JobFamilyName != "")
                {
                    objPositionModel.JobFamilyID = objDBHelper.InsertJobFamily(objJobFamilyModel).InsertedRowId;
                }
                if (objJobFamilyModel.JobFamilyNameFrench != null && objJobFamilyModel.JobFamilyNameFrench != "")
                {
                    objPositionModel.JobFamilyID = objDBHelper.InsertJobFamily(objJobFamilyModel).InsertedRowId;
                }
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objPositionModelDB.UpdatePosition(objPositionModel);
                objOperationDetails.InitiatorIdentity = Request.Form["hidFormSubmissionInitiatorIdentity"].ToString();
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewPosition(int id)
        {
            PositionDB objPositionModelDB = new PositionDB();
            PositionModel objPositionModel = new PositionModel();
            objPositionModel = objPositionModelDB.GetPositionById(id);
            return View(objPositionModel);
        }

        public ActionResult DeletePosition(int id)
        {
            string result = "";
            string resultMessage = "";
            PositionDB objPositionModelDB = new PositionDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objPositionModelDB.DeletePositionById(id);


            result = objOperationDetails.InsertedRowId > 0 ? "success" : "warning";
            resultMessage = objOperationDetails.InsertedRowId > 0 ? "Record Deleted Successfully" : "This Position is been assigned to Employee.Unable to delete the record";
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

     

    
        public ActionResult ExportToExcel(int? companyId)
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "Position" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            PositionDB objPositionDB = new PositionDB();
            System.Data.DataSet ds = objPositionDB.GetPositionDatasSet(companyId);
            //ds.Tables[0].Columns["Reg_Temp Name"].ColumnName = "Regular/Temporary";          
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(int? companyId)
        {
            string fileName = "Position" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            PositionDB objPositionDB = new PositionDB();
            System.Data.DataSet ds = objPositionDB.GetPositionDatasSet(companyId);          
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();

        }

    }
}