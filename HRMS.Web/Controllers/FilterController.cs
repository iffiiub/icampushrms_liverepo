﻿using HRMS.DataAccess;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class FilterController : Controller
    {
        // GET: Filter
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDepartmentListByUserId()
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            int UserId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserId = objUserContextViewModel.UserId;
            ViewBag.DeptList = new SelectList(objDepartmentDB.GetDepartmentListByUserId(UserId), "DepartmentId", "DepartmentName_1");
            return PartialView();
        }

        public ActionResult GetSectionListByDepartmentId()
        {
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            int UserId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserId = objUserContextViewModel.UserId;
            ViewBag.SectionList = new SelectList(objEmployeeSectionDB.GetSectionListByUserId(UserId), "EmployeeSectionID", "EmployeeSectionName_1");
            return PartialView();
        }

        public ActionResult GetEmployeeListBySectionId(int? sectionID)
        {
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            int UserId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserId = objUserContextViewModel.UserId;
            EmployeeDB objEmployeeDB = new EmployeeDB();
            if (sectionID == null)
            {
                ViewBag.EmployeeList = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName");
            }
            else
            {
                ViewBag.EmployeeList = new SelectList(objEmployeeDB.GetALLEmployeeByUserIdAndSectionId(UserId, sectionID).Where(m => m.IsActive == 1), "EmployeeId", "FullName");
            }

            return PartialView();
        }
     
        public ActionResult LoadSectionByDeptID(int deptID)
        {
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            int UserId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserId = objUserContextViewModel.UserId;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();

            ObjSelectedList.Add(new SelectListItem { Text = "Select All Section", Value = "0" });
            var SectionList = objEmployeeSectionDB.GetEmployeeSectionByDepartmentID(deptID);
            foreach (var m in SectionList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.EmployeeSectionName_1, Value = m.EmployeeSectionID.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadEmployeeBySectionID(int sectionID)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            int UserId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserId = objUserContextViewModel.UserId;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select All Employee", Value = "0" });
            var EmpList = objEmployeeDB.GetALLEmployeeByUserIdAndSectionId(UserId, sectionID).Where(s=>s.IsActive==1).ToList();
            foreach (var m in EmpList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }

    }
}