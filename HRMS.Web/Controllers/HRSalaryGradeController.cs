﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using HRMS.Entities;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;


namespace HRMS.Web.Controllers
{
    public class HRSalaryGradeController: BaseController
    {
        //
        // GET: /HRSalaryGrade/
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GetHRSalaryGradeList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        {

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------
                   switch (iSortCol_0)
            {

                case 1:
                    sortColumn = "SalaryGradeCode";
                    break;
                case 2:
                    sortColumn = "SalaryGradeLevel";
                    break;
               
               

                case 4:
                    sortColumn = "MonthlyHours";
                    break;
                case 5:
                    sortColumn = "MinHourPrice";
                    break;
                case 6:
                    sortColumn = "MedHourPrice";
                    break;
                case 7:
                    sortColumn = "MaxHourPrice";
                    break;
                              case 8:
                    sortColumn = "IsActive";
                    break;


                           

                default:
                    sortColumn = "SalaryGradeCode";
                    break;
            }




            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //-------------Data Objects--------------------
            List<HRSalaryGrade> objHRSalaryGradeList = new List<HRSalaryGrade>();
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            objHRSalaryGradeList = objHRSalaryGradeDB.GetHRSalaryGrade(objUserContextViewModel.CompanyId, 0, pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount);
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objHRSalaryGradeList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm'  onclick='EditSalaryGrade(" + item.salaryGradeID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteSalaryGrade(" + item.salaryGradeID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewSalaryGrade(" + item.salaryGradeID.ToString() + ")' title='View Details' ><i class='fa fa-eye'></i></a>",
                              salaryGradeID = item.salaryGradeID,
                              salaryGradeCode = item.salaryGradeCode,
                              salaryGradeLevel = item.salaryGradeLevel,
                              description = item.description,
                              monthlyHours = item.monthlyHours,
                              minHourPrice = item.minHourPrice,
                              medHourPrice = item.medHourPrice,
                              maxHourPrice = item.maxHourPrice,
                        isActive = item.isActive  ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.isActive + "'/>": "<input type='checkbox' class='checkbox' disabled id='" + item.isActive + "'/>",
                              isDeleted = item.isDeleted,
                              companyId = item.companyId,
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRSalaryGrade> objHRSalaryGradeList = new List<HRSalaryGrade>();
            HRSalaryGrade objHRSalaryGrade = new HRSalaryGrade();
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            int totalCount = 0;
            if (id != 0)
            {
                objHRSalaryGradeList = objHRSalaryGradeDB.GetHRSalaryGrade(objUserContextViewModel.CompanyId, id, 0, 1, null, null, out  totalCount);
                objHRSalaryGrade = objHRSalaryGradeList[0];
            }

            objHRSalaryGrade.companyId = objUserContextViewModel.CompanyId;
            return View(objHRSalaryGrade);
        }

        public ActionResult ViewDetails(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRSalaryGrade> objHRSalaryGradeList = new List<HRSalaryGrade>();
            HRSalaryGrade objHRSalaryGrade = new HRSalaryGrade();
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            int totalCount = 0;
            if (id != 0)
            {
                objHRSalaryGradeList = objHRSalaryGradeDB.GetHRSalaryGrade(objUserContextViewModel.CompanyId, id, 0, 1, null, null, out  totalCount);
                objHRSalaryGrade = objHRSalaryGradeList[0];
            }

            objHRSalaryGrade.companyId = objUserContextViewModel.CompanyId;
            return View(objHRSalaryGrade);
        }

        public ActionResult Save(HRSalaryGrade objHRSalaryGrade)
        {
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objHRSalaryGrade.createdBy = objUserContextViewModel.UserId;
            objHRSalaryGrade.companyId = objUserContextViewModel.CompanyId;
            objHRSalaryGradeDB.InsertUpdateHRSalaryGrade(objHRSalaryGrade);
            return Redirect("Index");
        }

        public ActionResult Delete(string id)
        {
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            HRSalaryGrade objHRSalaryGrade = new HRSalaryGrade();
            objHRSalaryGrade.salaryGradeID = int.Parse(id);
            objHRSalaryGrade.isDeleted = true;
            objHRSalaryGradeDB.DeleteHRSalaryGrade(objHRSalaryGrade);
            return Json(0, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ExportToExcel()
        {
            byte[] content;
            string fileName = "HRSalaryGradeList" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "salaryGradeID";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;

            List<HRSalaryGrade> objHRSalaryGradeList = new List<HRSalaryGrade>();
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            objHRSalaryGradeList = objHRSalaryGradeDB.GetHRSalaryGrade(objUserContextViewModel.CompanyId, 0, pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount);

            var report = (from item in objHRSalaryGradeList
                          select new
                          {
                              //Id = item.Id,
                              salaryGradeID = item.salaryGradeID,
                              salaryGradeCode = item.salaryGradeCode,
                              salaryGradeLevel = item.salaryGradeLevel,
                              description = item.description,
                              monthlyHours = item.monthlyHours,
                              minHourPrice = item.minHourPrice,
                              medHourPrice = item.medHourPrice,
                              maxHourPrice = item.maxHourPrice,
                              isActive = item.isActive,
                              isDeleted = item.isDeleted,
                              companyId = item.companyId,

                          }).ToList();
            content = ExportListToExcel(report);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "HRSalaryGradeList" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "salaryGradeID";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRSalaryGrade> objHRSalaryGradeList = new List<HRSalaryGrade>();
            HRSalaryGradeDB objHRSalaryGradeDB = new HRSalaryGradeDB();
            objHRSalaryGradeList = objHRSalaryGradeDB.GetHRSalaryGrade(objUserContextViewModel.CompanyId, 0, pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount);

            var report = (from item in objHRSalaryGradeList
                          select new
                          {
                              //Id = item.Id,
                              salaryGradeID = item.salaryGradeID,
                              salaryGradeCode = item.salaryGradeCode,
                              salaryGradeLevel = item.salaryGradeLevel,
                              description = item.description,
                              monthlyHours = item.monthlyHours,
                              minHourPrice = item.minHourPrice,
                              medHourPrice = item.medHourPrice,
                              maxHourPrice = item.maxHourPrice,
                              isActive = item.isActive,
                              isDeleted = item.isDeleted,
                              companyId = item.companyId,

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
    }
}