﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace HRMS.Web.Controllers
{
    public class AbsentController : BaseController
    {
        public ActionResult Index()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Month", Value = "0" });
            selectListItems.Add(new SelectListItem { Text = "January (1)", Value = "1" });
            selectListItems.Add(new SelectListItem { Text = "February (2)", Value = "2" });
            selectListItems.Add(new SelectListItem { Text = "March (3)", Value = "3" });
            selectListItems.Add(new SelectListItem { Text = "April (4)", Value = "4" });
            selectListItems.Add(new SelectListItem { Text = "May (5)", Value = "5" });
            selectListItems.Add(new SelectListItem { Text = "June (6)", Value = "6" });
            selectListItems.Add(new SelectListItem { Text = "July (7)", Value = "7" });
            selectListItems.Add(new SelectListItem { Text = "August (8)", Value = "8" });
            selectListItems.Add(new SelectListItem { Text = "September (9)", Value = "9" });
            selectListItems.Add(new SelectListItem { Text = "October (10)", Value = "10" });
            selectListItems.Add(new SelectListItem { Text = "November (11)", Value = "11" });
            selectListItems.Add(new SelectListItem { Text = "December (12)", Value = "12" });
            foreach (var item in selectListItems)
            {
                if (item.Value == DateTime.Now.Month.ToString())
                    item.Selected = true;
            }
            ViewBag.MonthList = selectListItems;//.Where(x=>x.Value == DateTime.Now.Month.ToString()).ToList().ForEach(i=>i.Selected = true);
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Year", Value = "0" });
            foreach (var items in objAcademicYearDB.GetAcademicYears())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.Year.ToString(),
                    Value = items.Year.ToString()
                };
                selectListItems.Add(selectListItem);
            }
            foreach (var item in selectListItems)
            {
                if (item.Value == DateTime.Now.Year.ToString())
                    item.Selected = true;
            }
            ViewBag.YearList = selectListItems;//.Where(x => x.Val


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);

            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All Employees", Value = "0" });
            foreach (var m in objEmployeeList.Where(m => m.IsActive == 1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });

            }
            ViewBag.EmployeeList = ObjSelectedList;

            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            List<AbsentTypeModel> absentTypeList = objAbsentTypeDB.GetAbsentTypeList();
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Absent Type", Value = "0" });
            foreach (var obj in absentTypeList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = obj.PayAbsentTypeName_1, Value = obj.PayAbsentTypeID.ToString() });
            }
            ViewBag.AbsentTypeList = ObjSelectedList;
            int FilterCondition = 0;
            AbsentDeductionDB objAbsentDeductionDB = new AbsentDeductionDB();
            FilterCondition = objAbsentDeductionDB.GetFilterCondition();
            ViewBag.FilterCondition = FilterCondition;

            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }
            return View(employeeDetailsModelList);
        }

        public ActionResult GetAbsentList(int empid = 0,int deptID=0,int EMpSectionID=0,string startDate = "", string endDate = "")
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<AbsentModel> objAbsentModelList = new List<AbsentModel>();
            AbsentDB objAbsentDB = new AbsentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            AbsentTypeDB objAbsentType = new AbsentTypeDB();
            LeaveTypeDB objLeaveType = new LeaveTypeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objAbsentModelList = objAbsentDB.GetAbsentList(startDate, endDate, objUserContextViewModel.UserId, empid,deptID,EMpSectionID);
            //---------------------------------------------
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            List<AbsentTypeModel> absentTypeList = objAbsentTypeDB.GetAbsentTypeList();
            string absentTypes = "";
            foreach (var obj in absentTypeList)
            {
                absentTypes += "<option value='" + obj.PayAbsentTypeID + "'>" + obj.PayAbsentTypeName_1 + "</option>";
            }
            string checkBoxItem = "<input type='checkbox' onclick='addToAbsentIdCollection(PayEmployeeAbsentID)' class='ChkRowCheck' id = 'Chk_PayEmployeeAbsentID' name = 'checkPayEmployeeAbsentID'/>";
            string disabledCheckbox = "<input disabled type='checkbox' onclick='addToAbsentIdCollection(PayEmployeeAbsentID)' class='' id = 'Chk_PayEmployeeAbsentID' name = 'checkPayEmployeeAbsentID'/>";
            var vList = new object();

            vList = new
            {
                aaData = (from item in objAbsentModelList
                          select new
                          {
                              Action =item.IsConfirmed==true? "<a disabled class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAbsent(" + item.PayEmployeeAbsentID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> <a disabled class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewAbsent(" + item.PayEmployeeAbsentID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a> <a disabled class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteAbsent(" + item.PayEmployeeAbsentID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>" : "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAbsent(" + item.PayEmployeeAbsentID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewAbsent(" + item.PayEmployeeAbsentID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteAbsent(" + item.PayEmployeeAbsentID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              CheckRow = item.IsConfirmed==true? disabledCheckbox.Replace("PayEmployeeAbsentID", item.PayEmployeeAbsentID.ToString()) : checkBoxItem.Replace("PayEmployeeAbsentID", item.PayEmployeeAbsentID.ToString()),
                              PayEmployeeAbsentID = item.PayEmployeeAbsentID,
                              PayEmployeeAbsentHeaderID = item.PayEmployeeAbsentHeaderID,
                              EmployeeID = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName,
                              EmployeeAltID=item.EmployeeAltID,
                              EmployeeName = item.EmployeeName,
                              PayEmployeeAbsentFromDate = item.PayEmployeeAbsentFromDate,
                              PayEmployeeAbsentToDate = item.PayEmployeeAbsentToDate,
                              LeaveTypeID = item.AbsentPaidTypeID,
                              LeaveTypeName = item.AbsentPaidTypeName,
                              AbsentTypeID = item.PayAbsentTypeID,
                              AbsentStatus = item.AbsenceReason,
                              AbsentTypeName = item.IsConfirmed == true ? "<select disabled onchange='UpdateAbsentType(this," + item.PayEmployeeAbsentID + ")' class='form-control selectpickerddl ddlAbsentTypeName' id='ddlAbsentType_" + item.PayAbsentTypeID + "' > " + absentTypes + " </select>" : "<select onchange='UpdateAbsentType(this," + item.PayEmployeeAbsentID + ")' class='form-control selectpickerddl ddlAbsentTypeName' id='ddlAbsentType_" + item.PayAbsentTypeID + "' > " + absentTypes + " </select>",
                              IsMedicalCertificateProvided = item.IsConfirmed == true?(item.IsMedicalCertificateProvided ? "<input disabled type='checkbox' class='checkbox' checked='true' onclick='UpdateMedCertId(this," + item.PayEmployeeAbsentID.ToString() + ")' id='" + item.PayEmployeeAbsentID + "'/>" : "<input disabled type='checkbox' class='checkbox' onclick='UpdateMedCertId(this," + item.PayEmployeeAbsentID.ToString() + ")' id='" + item.PayEmployeeAbsentID + "'/>") : item.IsMedicalCertificateProvided ? "<input type='checkbox' class='checkbox' checked='true' onclick='UpdateMedCertId(this," + item.PayEmployeeAbsentID.ToString() + ")' id='" + item.PayEmployeeAbsentID + "'/>" : "<input type='checkbox' class='checkbox' onclick='UpdateMedCertId(this," + item.PayEmployeeAbsentID.ToString() + ")' id='" + item.PayEmployeeAbsentID + "'/>",
                              IsConfirmed=item.IsConfirmed
                          }).ToArray()
            };

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }


        public ActionResult AddAbsent()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();

            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            EmployeeDB objEmployeeDB = new EmployeeDB();
            //ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            AbsentModel objAbsentModel = new AbsentModel();

            objAbsentModel.EmployeeID = empid;
            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            //HRContractTypeDB HRContractTypeDB = new HRContractTypeDB();
            //ViewBag.HRContractType = new SelectList(HRContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1"); 
            return View(objAbsentModel);
        }

        [HttpPost]
        public ActionResult AddAbsent(AbsentModel objAbsentModel)
        {
            AbsentDB objAbsentDB = new AbsentDB();
            List<OperationDetails> opList = new List<OperationDetails>();
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(objAbsentModel.SelectedIds))
                {
                    List<AbsentModel> lstAbsentModel = new List<AbsentModel>();
                    int daysDifferance = DataAccess.GeneralDB.CommonDB.CalculateDayDifference(Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(objAbsentModel.PayEmployeeAbsentFromDate)), Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(objAbsentModel.PayEmployeeAbsentToDate))) + 1;
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    OperationDetails objOperationDetails = new OperationDetails();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    EmployeeDB employeeDb = new EmployeeDB();
                    string[] EmpIds = objAbsentModel.SelectedIds.Split(',');
                    List<Employee> EmpList = new List<Employee>();
                    EmpList = new EmployeeDB().GetALLEmployeeListWithPermission(objUserContextViewModel.UserId);
                    foreach (var item in EmpIds)
                    {
                        objAbsentModel.EmployeeID = Convert.ToInt32(item);
                        EmploymentInformation employeeInfo = employeeDb.GetEmployeeHireDateInformation().FirstOrDefault(x => x.EmployeeID == objAbsentModel.EmployeeID);

                        DateTime date = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(objAbsentModel.PayEmployeeAbsentFromDate));
                        string EmpName = EmpList.Where(x => x.EmployeeId == objAbsentModel.EmployeeID).First().FirstName + " " + EmpList.Where(x => x.EmployeeId == objAbsentModel.EmployeeID).First().LastName;
                        if (employeeInfo.HireDate != "")
                        {
                            objOperationDetails = new OperationDetails();
                            DateTime hireDate = Convert.ToDateTime(employeeInfo.HireDate);

                            for (int i = 0; i < daysDifferance; i++)
                            {
                                if (!employeeDb.checkHireDateIsGreater(date.AddDays(i), employeeInfo.HireDate))
                                {
                                    AbsentModel objAbsentModelNew = new AbsentModel();
                                    objAbsentModelNew.EmployeeID = Convert.ToInt32(item);
                                    objAbsentModelNew.PayEmployeeAbsentFromDate = date.AddDays(i).ToString("dd/MM/yyyy");
                                    objAbsentModelNew.PayEmployeeAbsentToDate = date.AddDays(i).ToString("dd/MM/yyyy");
                                    if (objAbsentModel.InTime == null || objAbsentModel.InTime == "")
                                        objAbsentModelNew.InTime = DateTime.Now.ToString("MM/dd/yyy");
                                    if (objAbsentModel.OutTime == null || objAbsentModel.OutTime == "")
                                        objAbsentModelNew.OutTime = DateTime.Now.ToString("MM/dd/yyy");
                                    objAbsentModelNew.PayAbsentTypeID = objAbsentModel.PayAbsentTypeID;
                                    objAbsentModelNew.StatusID = 2;
                                    objAbsentModelNew.AbsentPaidTypeID = objAbsentModel.AbsentPaidTypeID;
                                    objAbsentModelNew.IsMedicalCertificateProvided = objAbsentModel.IsMedicalCertificateProvided;
                                    objAbsentModelNew.IsDeducted = objAbsentModel.IsDeducted;
                                    objAbsentModelNew.Comments = objAbsentModel.Comments;
                                    lstAbsentModel.Add(objAbsentModelNew);
                                }
                                else
                                {
                                    DateTime postedDate = Convert.ToDateTime(CommonDB.SetCulturedDate(objAbsentModel.PayEmployeeAbsentFromDate));
                                    objOperationDetails.Message = "Joining date for employee " + EmpName + " is " + hireDate.ToString("dd MMM, yyyy") + " so you cannot enter attendance before joining date.";
                                    objOperationDetails.CssClass = "error";
                                    opList.Add(objOperationDetails);
                                }
                            }


                        }
                        else
                        {
                            objOperationDetails = new OperationDetails();
                            objOperationDetails.Success = false;
                            objOperationDetails.Message = "Please first enter hire date of employee " + EmpName + " from employee profile.";
                            objOperationDetails.CssClass = "error";
                            opList.Add(objOperationDetails);
                        }

                    }
                    foreach (var absModel in lstAbsentModel)
                    {
                        objOperationDetails = new OperationDetails();
                        string EmpName = EmpList.Where(x => x.EmployeeId == absModel.EmployeeID).First().FirstName + " " + EmpList.Where(x => x.EmployeeId == absModel.EmployeeID).First().LastName;
                        bool errorCheck = false;
                        PayEmployeeAbsentHeaderDB objPayEmployeeAbsentHeaderDB = new PayEmployeeAbsentHeaderDB();
                        PayEmployeeAbsentHeaderModel objPayEmployeeAbsentHeaderModel = new PayEmployeeAbsentHeaderModel();
                        objPayEmployeeAbsentHeaderModel.EmployeeID = absModel.EmployeeID;
                        objPayEmployeeAbsentHeaderModel.PayAbsentTypeID = absModel.PayAbsentTypeID;
                        objPayEmployeeAbsentHeaderModel.EntryDate = DateTime.Now.ToString();
                        objPayEmployeeAbsentHeaderModel.Active = true;
                        absModel.PayEmployeeAbsentHeaderID = objPayEmployeeAbsentHeaderDB.InsertPayEmployeeAbsentHeader(objPayEmployeeAbsentHeaderModel).InsertedRowId;
                        if (objAbsentDB.GetAbsentListWithinDates(absModel.EmployeeID, DateTime.ParseExact(absModel.PayEmployeeAbsentFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).Count() == 0)
                        {
                            objOperationDetails = objAbsentDB.InsertAbsent(absModel);
                            if (objOperationDetails.Exception == null)
                            {
                                errorCheck = false;
                                objOperationDetails.Message = "Absent saved successfully for Employee " + EmpName + " of date " + absModel.PayEmployeeAbsentFromDate + ".";
                                objOperationDetails.CssClass = "success";
                            }
                            else
                            {
                                errorCheck = true;
                                objOperationDetails.Message = "Error : while inserting Absent for Employee " + EmpName + " of date " + absModel.PayEmployeeAbsentFromDate + ".";
                                objOperationDetails.CssClass = "error";
                            }

                        }
                        else
                        {
                            errorCheck = true;
                            objOperationDetails.Success = false;
                            objOperationDetails.Message = "A record exists for selected date for employee " + EmpName + " of date " + absModel.PayEmployeeAbsentFromDate + ".";
                            objOperationDetails.CssClass = "error";
                        }
                        opList.Add(objOperationDetails);
                    }
                }
                else
                {
                    OperationDetails objOperationDetails = new OperationDetails();
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please select employees.";
                    objOperationDetails.CssClass = "error";
                    opList.Add(objOperationDetails);
                }
            }
            else
            {
                int empid = 0;
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }
                ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithPermission(objUserContextViewModel.UserId).Where(x => x.EmployeeId > 0).Where(x => x.IsActive == 1).OrderBy(x => x.FirstName).ToList(), "EmployeeId", "FirstName");
                
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");
                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                objOperationDetails.CssClass = "error";
                opList.Add(objOperationDetails);

            }
            return Json(opList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddMedCertificateStatus(int MedCertId, bool IsMedCert)
        {
            OperationDetails op = new OperationDetails();
            AbsentDB objAbsentDB = new AbsentDB();
            op=objAbsentDB.AddMedCertificateStatus(MedCertId, IsMedCert);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditAbsent(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);
            ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1", objAbsentModel.PayAbsentTypeID);
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).LastName;
            return View(objAbsentModel);
        }

        [HttpPost]
        public JsonResult EditAbsent(AbsentModel objAbsentModel, string hdnFromDate)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                AbsentDB objAbsentModelDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                EmployeeDB employeeDb = new EmployeeDB();
                EmploymentInformation employeeInfo = employeeDb.GetEmployeeHireDateInformation().FirstOrDefault(x => x.EmployeeID == objAbsentModel.EmployeeID);
                objAbsentModel.PayEmployeeAbsentToDate = objAbsentModel.PayEmployeeAbsentFromDate;
                DateTime date = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(objAbsentModel.PayEmployeeAbsentFromDate));
                if (employeeInfo.HireDate != "")
                {
                    DateTime hireDate = Convert.ToDateTime(employeeInfo.HireDate);
                    if (!employeeDb.checkHireDateIsGreater(date, employeeInfo.HireDate))
                    {
                        if ((objAbsentModelDB.GetAbsentList(objAbsentModel.EmployeeID).Where(x => x.PayEmployeeAbsentFromDate == objAbsentModel.PayEmployeeAbsentFromDate).Select(x => x).ToList().Count() == 0) || hdnFromDate == objAbsentModel.PayEmployeeAbsentFromDate)
                        {
                            objOperationDetails = objAbsentModelDB.UpdateAbsent(objAbsentModel);
                        }
                        else
                        {
                            objOperationDetails.Success = false;
                            objOperationDetails.Message = "A record exists for selected date";
                        }
                    }
                    else
                    {
                        objOperationDetails.Success = false;
                        objOperationDetails.Message = "Joining date for selected employee is " + hireDate.ToString("dd MMM, yyyy") + " so you cannot enter absent before joining date."; ;
                    }
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please first enter hire date of this employee from employee profile.";
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");
                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateLeaveType(int LeavetypeID, int PayEmployeeAbsentID)
        {
            AbsentDB objAbsentModelDB = new AbsentDB();
            OperationDetails objOperationDetails = objAbsentModelDB.UpdateLeavetypeAbsent(LeavetypeID, PayEmployeeAbsentID);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateAbsentType(int AbsentTypeID, int PayEmployeeAbsentID)
        {
            AbsentDB objAbsentModelDB = new AbsentDB();
            OperationDetails objOperationDetails = objAbsentModelDB.UpdateAbsentType(AbsentTypeID, PayEmployeeAbsentID);
            AbsentTypeModel AbsentType = new AbsentTypeDB().GetAbsentType(AbsentTypeID);
            return Json(new { objOperationDetails = objOperationDetails, LeaveTypeName = AbsentType.PaidTypeName }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewAbsent(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();          
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);
            AbsentTypeModel objAbsentType = objAbsentTypeDB.GetAbsentTypeList().Where(x => x.PayAbsentTypeID == objAbsentModel.PayAbsentTypeID).FirstOrDefault();
            ViewBag.AbsentType = objAbsentType.PayAbsentTypeName_1;
            ViewBag.AbsentPaidTypeId = objAbsentModel.AbsentPaidTypeID;
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;

            return View(objAbsentModel);
        }

        public ActionResult DeleteAbsent(int id)
        {
            AbsentDB objAbsentDB = new AbsentDB();
            objAbsentDB.DeleteAbsentById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteMultipleAbsentRecords(string PayEmployeeAbsentIDs)
        {
            AbsentDB objAbsentDB = new AbsentDB();            
            int[] EmployeeAbsentIds = JsonConvert.DeserializeObject<int[]>(PayEmployeeAbsentIDs);
            string EmployeeAbsentId = string.Join(",", EmployeeAbsentIds);
            OperationDetails op= objAbsentDB.DeleteMultipleAbsentIds(EmployeeAbsentId);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckEmployeeInLate(string EmpIds, string FromDate, string ToDate)
        {
            AbsentLateInEarlyOutDB absentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
            var data = absentLateInEarlyOutDB.GetAbsentLateInEarlyWithEmployeeIds(EmpIds, FromDate, ToDate);
            if (data.Count > 0)
                return Json(new { isPresent = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { isPresent = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateMultipleAbsentType(string PayEmployeeAbsentIDs, int AbsentTypeID)
        {
            OperationDetails op = new OperationDetails();
            int[] EmployeeAbsentIds = JsonConvert.DeserializeObject<int[]>(PayEmployeeAbsentIDs);
            string EmployeeAbsentId = string.Join(",", EmployeeAbsentIds);
            AbsentDB objAbsentDB = new AbsentDB();
            op = objAbsentDB.UpdateMultipleAbsentType(EmployeeAbsentId, AbsentTypeID);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        //****************************************************************** Export to excel functionality *****************************************************************
        public ActionResult ExportToExcel(int empId = 0, string startDate = "", string endDate = "")
        {
            byte[] content;
            string fileName = "Absent" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AbsentDB absentDB = new AbsentDB();
            System.Data.DataSet ds = absentDB.GetEmployeeDataSet(objUserContextViewModel.UserId, empId, startDate, endDate);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        public void ExportToPdf(int empId = 0, string startDate = "", string endDate = "")
        {
            string fileName = "Absent" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            AbsentDB absentDB = new AbsentDB();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);           
            System.Data.DataSet ds = absentDB.GetEmployeeDataSet(objUserContextViewModel.UserId, empId, startDate, endDate);          
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }
    }
}