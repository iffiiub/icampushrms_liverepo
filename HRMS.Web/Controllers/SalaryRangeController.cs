﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.ViewModel;


namespace HRMS.Web.Controllers
{
     public class SalaryRangeController :BaseController
     {
         // GET: SalaryRange
        public SalaryRangeController()
        {
            XMLLogFile = "LoggerSalaryRange.xml";
        }
        public ActionResult Index()
        {
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.Position = new SelectList(new List<PositionModel>(), "PositionID", "PositionTitle");
            CompanyDB objcompanyDB = new CompanyDB();
            //List<CompanyModel> companyList = objcompanyDB.GetAllCompanyList();
            var companyList = GetCompanySelectList();
            string companies = "";
            companies += "<option value=''>Select Company</option>";
            foreach (var obj in companyList)
            {
                companies += "<option value='" + obj.Value + "'>" + obj.Text+ "</option>";
            }
            ViewBag.Companies = companies;
            PositionDB objpositionDB = new PositionDB();
            List<PositionModel> positionList = new List<PositionModel>();
            //positionList=objpositionDB.GetPositionList();
            string positions = "";
            companies += "<option value=''>Select Position</option>";
            foreach (var obj in positionList)
            {
                positions += "<option value='" + obj.PositionID + "'>" + obj.PositionTitle + "</option>";
            }
            ViewBag.Positions = positions;
            return View();
        }
            
        public ActionResult AddMultipleSalaryRange()
        {
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.Position = new SelectList(new List<PositionModel>(), "PositionID", "PositionTitle");
            return View("AddMultipleSalary");
        }
        public ActionResult GetSalaryRanges(int? CompanyID, Int16? PositionID)
        {
            List<SalaryRangesModel> objSalaryRangeList = new List<SalaryRangesModel>();
            SalaryRangesDB objSalaryRange= new SalaryRangesDB();
           
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //CompanyBasedHierarchyModel companyBasedHierarchyModel = new CompanyBasedHierarchyModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objSalaryRangeList = objSalaryRange.GetAllSalaryRanges(CompanyID, PositionID,objUserContextViewModel.UserId);
            var UserId = objUserContextViewModel.UserId;
            CompanyDB objcompanyDB = new CompanyDB();
            //List<CompanyModel> companyList = objcompanyDB.GetAllCompanyList();
            var companyList = GetCompanySelectList();
            string companies = "";
            companies += "<option value=''>Select Company</option>";
            foreach (var obj in companyList)
            {
                companies += "<option value='" + obj.Value+ "'>" + obj.Text+ "</option>";
            }
            PositionDB objpositionDB = new PositionDB();
            List<PositionModel> positionList = objpositionDB.GetPositionList();
            string positions = "";
            companies += "<option value=''>Select Position</option>";
            foreach (var obj in positionList)
            {
                positions += "<option value='" + obj.PositionID + "'>" + obj.PositionTitle + "</option>";
            }
            var vList = new object();
            vList = new
            {
                aaData = (from item in objSalaryRangeList
                          select new
                          {
                              SalaryRangesID=item.SalaryRangesID,
                              CompanyId = item.CompanyID,
                              CompanyName = item.CompanyName,
                              PositionID = item.PositionID,
                              Position = item.PostionName,
                              MinSalary= item.MinSalary,
                              MaxSalary = item.MaxSalary,
                              OldMinSal= item.MinSalary,
                              OldMaxSal= item.MaxSalary,
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditSalaryRange(this," + item.CompanyID + "," + item.PositionID + "," + item.autoID + "," + item.SalaryRangesID + " )' title='Edit' ><i class='fa fa-pencil'></i> </a> " +
                                        "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"changeStatus(this," + item.CompanyID + "," + item.PositionID + "," + item.autoID + "," + item.SalaryRangesID + ")\" title='Delete'><i class='fa fa-times'></i> </a>",
                             
                              AutoId = item.autoID

                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        //*** Naresh 2020-03-30 Do not populate the departments, it will be populated based on company selection
        [HttpGet]
        public ActionResult GetPositions(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PositionDB objpositionDB = new PositionDB();
            List<PositionModel> positionList = new List<PositionModel>();
            if (companyId > 0)
                positionList = objpositionDB.GetPositionList(companyId);

            var result = positionList.Select(d => new { id = d.PositionID, name = d.PositionTitle }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateSaalryRange(SalaryRangesModel salaryRangesModel)
        {
            OperationDetails op = new OperationDetails();
            SalaryRangesDB salaryDb = new SalaryRangesDB();
           // salary heirarchyDb = new CompanyBasedHeirarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            salaryRangesModel.ModifiedBy = objUserContextViewModel.UserId;
            salaryRangesModel.ModifiedOn = DateTime.Now;
            salaryRangesModel.IsDeleted = false;
            op = salaryDb.UpdateSalaryRanges(salaryRangesModel);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }          
        public ActionResult DeleteSalaryRange(int SalaryRangesID)
        {
            OperationDetails op = new OperationDetails();
            SalaryRangesDB salaryDb = new SalaryRangesDB();
            op = salaryDb.DeleteSalaryrange(SalaryRangesID);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveMultipleSalaryRange(List<SalaryRangesModel> salaryList, SalaryRangesModel salaryRangeModel)
        {

            SalaryRangesDB salaryDb = new SalaryRangesDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            salaryRangeModel.ModifiedBy = objUserContextViewModel.UserId; ;
            salaryRangeModel.ModifiedOn = DateTime.Now;
            return Json(salaryDb.UpdatMultipleSalaryRange(salaryList, salaryRangeModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult InsertMultipleSalayRange(List<SalaryRangesModel> salaryListinsert, SalaryRangesModel salaryRangeModel)
        {
            SalaryRangesDB salaryDb = new SalaryRangesDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            salaryRangeModel.CreatedBy = objUserContextViewModel.UserId;
            salaryRangeModel.CreatedOn = DateTime.Now;
            salaryRangeModel.ModifiedBy = objUserContextViewModel.UserId; ;
            salaryRangeModel.ModifiedOn = DateTime.Now;
            return Json(salaryDb.InsertMultipleSalaryRange(salaryListinsert, salaryRangeModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckSalaryRangesRecordExists(int CompanyID, int PositionID, int OriginalMinSalary, int OriginalMaxSalary)
        {
            bool IsExistsSalary;
            SalaryRangesDB salaryDb = new SalaryRangesDB();
            IsExistsSalary = salaryDb.CheckSalaryRangesRecordExists(CompanyID, PositionID, OriginalMinSalary, OriginalMaxSalary);
            return Json(new { IsExistsSalary =IsExistsSalary }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalaryRangeByID(int salaryRangeID)
        {

            SalaryRangesModel salaryRangesModel = new SalaryRangesModel();
            SalaryRangesDB objSalaryRange = new SalaryRangesDB();
            salaryRangesModel = objSalaryRange.GetSalaryRangeById(salaryRangeID);
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(salaryRangesModel),
                ContentType = "application/json"
            };
            return result;
        }
    }
}