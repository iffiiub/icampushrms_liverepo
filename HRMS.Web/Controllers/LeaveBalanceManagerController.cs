﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class LeaveBalanceManagerController : BaseController
    {
        // GET: LeaveBalanceManager
        public ActionResult Index()
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
           CompanyDB objCompanyDB = new CompanyDB();
            LeaveBalanceManagerViewModel objLeaveBalanceManagerViewModel = new LeaveBalanceManagerViewModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            // objLeaveBalanceManagerViewModel = GetLeaveBalance(null,null);
            //List<Employee> empList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList<Employee>();
            List<Employee> empList = new List<Employee>();
            string employees = "";
            employees += "<option value=''>Select Employee Name</option>";
            foreach (var obj in empList)
            {
                employees += "<option value='" + obj.EmployeeId + "'>" + obj.FullName + "</option>";
            }
            ViewBag.EmployeeList = employees;
            ViewBag.Employee = new SelectList(empList, "EmployeeId", "FullName");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            //objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");

            ViewBag.Company = GetCompanySelectList();

            return View(objLeaveBalanceManagerViewModel);            
        }
        public ActionResult LoadLeaveBalanceGrid(int? employeeId, int? departmentId,int? companyId)
        {
            return PartialView("_LeaveBalanceGrid", GetLeaveBalance(employeeId, departmentId, companyId));
        }
       
        private LeaveBalanceManagerViewModel GetLeaveBalance(int? employeeId, int? departmentId, int? companyId)
        {
            LeaveBalanceManagerViewModel objLeaveBalanceManagerViewModel = new LeaveBalanceManagerViewModel();
            EmployeeBasedHierarchyDB objEmployeeBasedHierarchyDB = new EmployeeBasedHierarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objLeaveBalanceManagerViewModel.LeaveBalanceManagerModelList = new LeaveBalanceManagerDB().GetLeaveBalanceManagerList(objUserContextViewModel.UserId, employeeId, departmentId, companyId);
            objLeaveBalanceManagerViewModel.VacationTypeModellList = new VacationDB().GetVacationTypeListWithPaging(0);
            ViewBag.CountVacationType = objLeaveBalanceManagerViewModel.VacationTypeModellList.Count;          
            return objLeaveBalanceManagerViewModel;
        }

        public ActionResult UpdateLeaveBalance(List<AccumulativeAllVacations> accumulativeAllVacationList)
        {
            LeaveBalanceManagerDB objLeaveBalanceManagerDB = new LeaveBalanceManagerDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(objLeaveBalanceManagerDB.UpdateAccumulativeAllVacation(accumulativeAllVacationList, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }
    }
}