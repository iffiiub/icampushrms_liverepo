﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class AllowanceController : BaseController
    {
        public ActionResult Index(int Id = 0)
        {
            AllowanceModel objAllowanceModel = new AllowanceModel();
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            int EmployID = 0;
            if (Id != 0)
            {
                Session["EmployeeListID"] = Id.ToString();
                EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);
                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                ViewBag.EmployeeIDNo = Id.ToString();
                ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();
            }
            else
            {
                ViewBag.EmployeeIDNo = 0;
                if (Session["EmployeeListID"] != null)
                {
                    EmployID = Convert.ToInt32(Session["EmployeeListID"]);
                    objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);
                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = EmployID.ToString();
                    ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();
                }
                else
                {
                    Id = objEmployeeDB.GetFirstActiveEmployee();
                    Session["EmployeeListID"] = Id.ToString();
                    EmployID = Id;
                    objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);
                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = Id.ToString();
                    ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();
                }
            }
            CountryDB objCountryDB = new CountryDB();
            ViewBag.CountryList = new SelectList(objCountryDB.GetAllContries(), "CountryID", "CountryName");
            CityDB objCityDB = new CityDB();

            AllowanceDB AllowanceDB = new DataAccess.AllowanceDB();
            InsuranceDB InsuranceDB = new DataAccess.InsuranceDB();
            ViewBag.AccommodationTypeList = new SelectList(AllowanceDB.GetAccommodationtype(), "id", "text");
            if (EmployID > 0)
            {
                objAllowanceModel = AllowanceDB.AccommodationInfoByEmployeeId(EmployID);
                objAllowanceModel.transportationModel = AllowanceDB.TransportationByEmployeeID(EmployID).transportationModel; ;
                objAllowanceModel.EmpAirFare = AllowanceDB.GetEmployeeAirFarebyEmployeeId(EmployID).EmpAirFare;
                objAllowanceModel.InsuranceModel = InsuranceDB.GetInsuranceList(EmployID); ;
            }
            objAllowanceModel.EmpAirFare.AnnualAirTicket = objEmployee.employmentInformation.AnnualAirTicket;
            objAllowanceModel.InsuranceModel.Insurance = objEmployee.employmentInformation.HealthInsurance;
            objAllowanceModel.Allowance = objEmployee.employmentInformation.Accommodation;
            ViewBag.AnnualAirTicket = objEmployee.employmentInformation.AnnualAirTicket;
            ViewBag.CarTypeList = new SelectList(AllowanceDB.GetCarTypeList(), "CarTypeID", "CarTypeName");
            ViewBag.CityIDValue = objAllowanceModel.CityID;
            objAllowanceModel.EmployeeID = EmployID;

            List<CityModel> listCity = new List<CityModel>();
            if (objAllowanceModel.CountryID > 0)
            {
                listCity = objCityDB.GetCityByCountryId(objAllowanceModel.CountryID.ToString());
            }
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "" });
            if (listCity.Count() > 0)
                foreach (var iCity in listCity)
                    ObjSelectedList.Add(new SelectListItem { Text = iCity.CityName, Value = iCity.CityId.ToString() });
            ViewBag.CityList1 = ObjSelectedList;


            listCity = new List<CityModel>();
            if (objAllowanceModel.EmpAirFare.AirTicketCountryID > 0)
            {
                listCity = objCityDB.GetCityByCountryId(objAllowanceModel.EmpAirFare.AirTicketCountryID.ToString());
            }
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "" });
            if (listCity.Count() > 0)
                foreach (var iCity in listCity)
                    ObjSelectedList.Add(new SelectListItem { Text = iCity.CityName, Value = iCity.CityId.ToString() });
            ViewBag.AirTicketCityList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Airfare Class", Value = "" });
            foreach (var item in objEmployeeDB.GetAllAirFareClasses())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.AirFareClassName, Value = item.AirFareClassId.ToString() });
            }
            ViewBag.AirfareClassList = ObjSelectedList;
            EmployeeProfileCreationFormDB objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            ViewBag.EmployeeAirfareFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareFrequencyList(), "id", "text");
            List<PickList> picklist = objEmployeeProfileCreationFormDB.GetAirPortByCountryId(objAllowanceModel.EmpAirFare.AirTicketCountryID);
            if (picklist.Count > 0)
            {
                ViewBag.AirPortList = new SelectList(picklist, "id", "text");
            }
            else
            {
                ViewBag.AirPortList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            ViewBag.InsuranceTypeList = new SelectList(InsuranceDB.GetInsuranceTypeList(), "id", "text");
            ViewBag.InsuranceEligibilityList = new SelectList(InsuranceDB.GetInsuranceEligibilityList(), "id", "text");
            ViewBag.InsuranceCategoryAmounts = new SelectList(InsuranceDB.GetAmountByInsuranceCategoryId(0), "CatAmountID", "Amount");
            return View(objAllowanceModel);
        }

        public ActionResult AddMaintainenceInfo()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();

            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

            AbsentModel objAbsentModel = new AbsentModel();

            objAbsentModel.EmployeeID = empid;
            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            //HRContractTypeDB HRContractTypeDB = new HRContractTypeDB();
            //ViewBag.HRContractType = new SelectList(HRContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1"); 
            return View(objAbsentModel);
        }

        public ActionResult AddAccommodationInfo(AllowanceModel AllowanceModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (Session["EmployeeListID"] != null)
            {
                AllowanceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            AllowanceDB objAllowanceDB = new AllowanceDB();
            OperationDetails objOperationDetails = new OperationDetails();
            if (AllowanceModel.PayAccommodationID > 0)
            {
                objOperationDetails = objAllowanceDB.UpdateAccommodationInfo(AllowanceModel);
            }
            else
            {
                objOperationDetails = objAllowanceDB.InsertAccommodationInfo(AllowanceModel);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAirfareDetails(AllowanceModel AllowanceModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {
                AllowanceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            OperationDetails objOperationDetails = new OperationDetails();
            if (AllowanceModel.EmployeeID > 0)
            {
                AllowanceDB objAllowanceDB = new AllowanceDB();

                objOperationDetails = objAllowanceDB.AddAirfareDetails(AllowanceModel);
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Please select employee.";
            }

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddAccommodationHistory()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();

            //ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            //EmployeeDB objEmployeeDB = new EmployeeDB();
            //ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

            AllowanceModel objAllowanceModel = new AllowanceModel();

            AllowanceDB AllowanceDB = new DataAccess.AllowanceDB();

            ViewBag.AcademicYearList = new SelectList(AllowanceDB.GetAcademicYearList(), "PayEmployeeAccommodationHistoryYear", "PayEmployeeAccommodationHistoryYear");

            objAllowanceModel.EmployeeID = empid;
            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            //HRContractTypeDB HRContractTypeDB = new HRContractTypeDB();
            //ViewBag.HRContractType = new SelectList(HRContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1"); 
            return View(objAllowanceModel);
        }

        public ActionResult AddAccommodationHistoryData(AllowanceModel AllowanceModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (Session["EmployeeListID"] != null)
            {
                AllowanceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            AllowanceDB objAllowanceDB = new AllowanceDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAllowanceDB.InsertAccommodationHistory(AllowanceModel);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAccommodationHistoryList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<AllowanceModel> objAllowanceModelList = new List<AllowanceModel>();
            AllowanceDB objAllowanceDB = new AllowanceDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objAllowanceModelList = objAllowanceDB.AccommodationHistoryByEmployeeId(empid);
            String Count = objAllowanceModelList.Count().ToString();
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objAllowanceModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAccommodationHistory(" + item.PayEmployeeAccommodationHistoryID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteAccommodationHistory(" + item.PayEmployeeAccommodationHistoryID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PayEmployeeAccommodationHistoryID = item.PayEmployeeAccommodationHistoryID,
                              EmployeeID = item.EmployeeID,
                              PayEmployeeAccommodationHistoryYear = item.PayEmployeeAccommodationHistoryYear,
                              AccommodationHistoryAmount = item.AccommodationHistoryAmount.ToString(AmountFormat),
                              Furnished = item.Furnished ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.Furnished + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.Furnished + "'/>",
                              FurnishedAmount = item.FurnishedAmount.ToString(AmountFormat),
                              Electricity = item.Electricity ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.Electricity + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.Electricity + "'/>",
                              ElectricityAmount = item.ElectricityAmount.ToString(AmountFormat),
                              Water = item.Water ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.Water + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.Water + "'/>",
                              WaterAmount = item.WaterAmount.ToString(AmountFormat),
                          }).ToArray(),
                recordsTotal = Count,
                recordsFiltered = Count
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult EditAccommodationHistory(int id)
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {
                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            AllowanceDB ObjAllowanceDB = new AllowanceDB();
            //ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");
            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            //EmployeeDB objEmployeeDB = new EmployeeDB();
            //ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

            AllowanceModel objAllowanceModel = new AllowanceModel();

            objAllowanceModel = ObjAllowanceDB.AccommodationHistoryByHistoryID(id);



            ViewBag.AcademicYearList = new SelectList(ObjAllowanceDB.GetAcademicYearList(), "PayEmployeeAccommodationHistoryYear", "PayEmployeeAccommodationHistoryYear");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            //HRContractTypeDB HRContractTypeDB = new HRContractTypeDB();
            //ViewBag.HRContractType = new SelectList(HRContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1"); 
            return View("AddAccommodationHistory", objAllowanceModel);
        }

        public ActionResult DeleteAccommodationHistory(int id)
        {
            AllowanceDB objAllowanceDB = new AllowanceDB();
            objAllowanceDB.DeleteAccommodationHistory(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        //Maintenance
        public ActionResult AddMaintenanceInfo()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            AllowanceModel objAllowanceModel = new AllowanceModel();

            AllowanceDB AllowanceDB = new DataAccess.AllowanceDB();

            //ViewBag.AcademicYearList = new SelectList(AllowanceDB.GetAcademicYearList(), "PayEmployeeAccommodationHistoryYearID", "PayEmployeeAccommodationHistoryYear");

            objAllowanceModel.EmployeeID = empid;
            return View(objAllowanceModel);
        }

        //transportation
        public ActionResult AddTransportation(AllowanceModel AllowanceModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {
                AllowanceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            AllowanceDB objAllowanceDB = new AllowanceDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAllowanceDB.AddTransportation(AllowanceModel);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        //Gas Card
        public ActionResult AddGasCard()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            AllowanceModel objAllowanceModel = new AllowanceModel();

            AllowanceDB AllowanceDB = new DataAccess.AllowanceDB();

            //ViewBag.AcademicYearList = new SelectList(AllowanceDB.GetAcademicYearList(), "PayEmployeeAccommodationHistoryYearID", "PayEmployeeAccommodationHistoryYear");

            objAllowanceModel.EmployeeID = empid;
            return View(objAllowanceModel);


        }

        public ActionResult AddGasCardData(AllowanceModel AllowanceModel)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (Session["EmployeeListID"] != null)
            {

                AllowanceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
            AllowanceDB objAllowanceDB = new AllowanceDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAllowanceDB.InsertGasCardInfo(AllowanceModel);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);



        }

        public ActionResult GetGasCardList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<AllowanceModel.GasCardModel> objGasCardModelList = new List<AllowanceModel.GasCardModel>();
            AllowanceDB objAllowanceDB = new AllowanceDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objGasCardModelList = objAllowanceDB.GasCardInfoEmployeeId(empid);
            String Count = objGasCardModelList.Count().ToString();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objGasCardModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditGasCard(" + item.PayGasCardID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGasCard(" + item.PayGasCardID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PayGasCardID = item.PayGasCardID,
                              PayGasCardValue = item.PayGasCardValue,
                              Company = item.Company,
                              ReceivedDate = item.GasCardReceivedDate,
                              PayGasCardNumber = item.PayGasCardNumber,
                              PayGasCardPeriod = item.PayGasCardPeriodName
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteGasCard(int id)
        {
            AllowanceDB objAllowanceDB = new AllowanceDB();
            objAllowanceDB.DeleteGasCard(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditGasCard(int id)
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            AllowanceDB ObjAllowanceDB = new AllowanceDB();



            AllowanceModel objAllowanceModel = new AllowanceModel();

            objAllowanceModel = ObjAllowanceDB.GasCardInfoByID(id);

            objAllowanceModel.EmployeeID = empid;
            return View("AddGasCard", objAllowanceModel);
        }

        public ActionResult AddAccommodationMaintainence()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }


            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();

            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

            AbsentModel objAbsentModel = new AbsentModel();

            objAbsentModel.EmployeeID = empid;
            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            //HRContractTypeDB HRContractTypeDB = new HRContractTypeDB();
            //ViewBag.HRContractType = new SelectList(HRContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1"); 
            return View(objAbsentModel);
        }

        [HttpPost]
        public ActionResult AddAbsent(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objAbsentModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objAbsentDB.InsertAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.Employee = objEmployeeDB.GetEmployee(empid).FirstName + " " + objEmployeeDB.GetEmployee(empid).LastName;

                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditAbsent(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();


            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).LastName;

            return View(objAbsentModel);
        }

        [HttpPost]
        public JsonResult EditAbsent(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //     objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentModelDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                //if (objAbsentModel.PayEmployeeAbsentFromDate != "")
                //    objAbsentModel.PayEmployeeAbsentFromDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentFromDate).ToShortDateString();
                //if (objAbsentModel.PayEmployeeAbsentToDate != "")
                //    objAbsentModel.PayEmployeeAbsentToDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentToDate).ToShortDateString();
                objOperationDetails = objAbsentModelDB.UpdateAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();




                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;


                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateLeaveType(int LeavetypeID, int PayEmployeeAbsentID)
        {



            AbsentDB objAbsentModelDB = new AbsentDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAbsentModelDB.UpdateLeavetypeAbsent(LeavetypeID, PayEmployeeAbsentID);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);


        }

        public ActionResult ViewAbsent(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;

            return View(objAbsentModel);
        }

        public ActionResult DeleteAbsent(int id)
        {
            AbsentDB objAbsentDB = new AbsentDB();
            objAbsentDB.DeleteAbsentById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        #region Insurance
        public ActionResult GetInsdependenceList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<InsdependenceModel> objInsdependenceList = new List<InsdependenceModel>();
            InsuranceDB objInsuranceDB = new InsuranceDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objInsdependenceList = objInsuranceDB.GetInsdependenceList(empid);
            var vList = new object();
            vList = new
            {
                aaData = (from item in objInsdependenceList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditInsuranceDependence(" + item.InsuranceDependenceID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteInsuranceDependence(" + item.InsuranceDependenceID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              InsuranceDependence = item.InsuranceDependenceID,
                              Name = item.Name,
                              Relation = item.Relation,
                              RelationName = item.RelationName,
                              CardNumber = item.CardNumber,
                              Note = item.Note
                          }).ToArray(),
                recordsTotal = objInsdependenceList.Count,
                recordsFiltered = objInsdependenceList.Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInsdependence()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            InsdependenceModel objInsdependenceModel = new InsdependenceModel();
            InsuranceDB objInsuranceDB = new InsuranceDB();
            objInsdependenceModel.EmployeeID = empid;

            ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");
            return View(objInsdependenceModel);
        }

        [HttpPost]
        public ActionResult AddInsdependence(InsdependenceModel objInsdependenceModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objInsdependenceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                InsuranceDB objInsuranceDB = new InsuranceDB();
                OperationDetails objOperationDetails = new OperationDetails();


                if (objInsdependenceModel.InsuranceDependenceID > 0)
                {
                    objInsdependenceModel.TranMode = 2;
                }
                else
                {
                    objInsdependenceModel.TranMode = 1;
                }


                objOperationDetails = objInsuranceDB.InsertInsdependence(objInsdependenceModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                InsuranceDB objInsuranceDB = new InsuranceDB();
                objInsdependenceModel.EmployeeID = empid;

                ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");



                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult AddInsurance(AllowanceModel AllowanceModel)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (Session["EmployeeListID"] != null)
            {

                AllowanceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            AllowanceModel.InsuranceModel.EmployeeID = AllowanceModel.EmployeeID;
            //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
            InsuranceDB objInsuranceDB = new InsuranceDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objInsuranceDB.InsertInsurance(AllowanceModel.InsuranceModel);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult EditInsdependence(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            InsuranceDB objInsuranceDB = new InsuranceDB();
            InsdependenceModel objInsdependenceModel = new InsdependenceModel();
            objInsdependenceModel = objInsuranceDB.GetInsdependenceByID(id);

            ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");


            return View("AddInsdependence", objInsdependenceModel);
        }

        [HttpPost]
        public JsonResult EditInsdependence(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //     objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentModelDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                //if (objAbsentModel.PayEmployeeAbsentFromDate != "")
                //    objAbsentModel.PayEmployeeAbsentFromDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentFromDate).ToShortDateString();
                //if (objAbsentModel.PayEmployeeAbsentToDate != "")
                //    objAbsentModel.PayEmployeeAbsentToDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentToDate).ToShortDateString();
                objOperationDetails = objAbsentModelDB.UpdateAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();




                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;


                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewInsurance(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;

            return View(objAbsentModel);
        }

        public ActionResult DeleteInsdependence(int id)
        {
            InsuranceDB objInsuranceDB = new InsuranceDB();
            objInsuranceDB.DeleteInsdependenceById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAmountByInsuranceCategoryId(int insuranceCategoryId)
        {
            var categoryAmounts = new InsuranceDB().GetAmountByInsuranceCategoryId(insuranceCategoryId);
            return Json(categoryAmounts.ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}