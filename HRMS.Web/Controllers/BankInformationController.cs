﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess.GeneralDB;
using HRMS.DataAccess;
using HRMS.Entities.ViewModel;
using System.Threading;

namespace HRMS.Web.Controllers
{
    public class BankInformationController : BaseController
    {
        //
        // GET: /BankInformation/
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/BankInformation/Index");
            Session["BankPermission"] = permissionModel;
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }
            return View();
        }

        public ActionResult Edit(int id, string EmployeeID)
        {
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            BankInformationDB bankInformationDB = new BankInformationDB();
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();
            EmployeeDB objEmpDB = new EmployeeDB();
            HRMS.Entities.Employee employeeModel = objEmpDB.GetAllDetailsOfEmployee(Convert.ToInt32(EmployeeID));
            ViewBag.IBanLength = bankInformationDB.GetIBanLength();
            payDirectDepositModel.EmployeeID = Convert.ToInt32(EmployeeID);
            if (id != 0)
            {
                payDirectDepositModel = payDirectDepositDB.GetPayDirectDepositByID(id);
                payDirectDepositModel.BankList = new SelectList(bankInformationDB.GetBanksList(), "BankID", "BankName_1", payDirectDepositModel.BankID);
                payDirectDepositModel.BranchList = new SelectList(bankInformationDB.GetBankBranchesList(), "BranchID", "BranchName_1", payDirectDepositModel.BranchID);
                payDirectDepositModel.CategoriesList = new SelectList(bankInformationDB.GetPayCategoriesList(), "CategoryID", "CategoryName_1", payDirectDepositModel.CategoryID);
                payDirectDepositModel.AccountTypeList = new SelectList(bankInformationDB.GetPayAccountTypesList(), "AccountTypeID", "AccountTypeName_1", payDirectDepositModel.AccountTypeID);

                payDirectDepositModel.AccountDate = payDirectDepositModel.AccountDate;

                payDirectDepositModel.CancellationDate = payDirectDepositModel.CancellationDate;
            }
            else
            {
                //payDirectDepositModel.payDirectDepositList = payDirectDepositDB.GetPayDirectDepositFullInformation(id);
                //var dateAndTime = DateTime.Now;
                //var date = dateAndTime.Date.ToString("MM/dd/yyyy");
                //payDirectDepositModel.AccountDate = date;
                payDirectDepositModel.BankList = new SelectList(bankInformationDB.GetBanksList(), "BankID", "BankName_1");
                payDirectDepositModel.BranchList = new SelectList(bankInformationDB.GetBankBranchesList(), "BranchID", "BranchName_1");
                payDirectDepositModel.CategoriesList = new SelectList(bankInformationDB.GetPayCategoriesList(), "CategoryID", "CategoryName_1");
                payDirectDepositModel.AccountTypeList = new SelectList(bankInformationDB.GetPayAccountTypesList(), "AccountTypeID", "AccountTypeName_1");
            }
            if (string.IsNullOrEmpty(payDirectDepositModel.EmpName))
            {
                if (string.IsNullOrEmpty(employeeModel.employeeDetailsModel.MiddleName_1.Trim()))
                {
                    payDirectDepositModel.EmpName = employeeModel.employeeDetailsModel.FirstName_1 + " " + employeeModel.employeeDetailsModel.SurName_1;
                }
                else
                {
                    payDirectDepositModel.EmpName = employeeModel.employeeDetailsModel.FirstName_1 + " " + employeeModel.employeeDetailsModel.MiddleName_1 + " " + employeeModel.employeeDetailsModel.SurName_1;
                }

            }
            return PartialView("_Add", payDirectDepositModel);
        }

        public ActionResult ViewDirectDepositDetails(int id)
        {
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            BankInformationDB bankInformationDB = new BankInformationDB();
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();
            if (id != 0)
            {
                Session["EmployeeListID"] = id;
                payDirectDepositModel.payDirectDepositList = payDirectDepositDB.GetPayDirectDepositFullInformation(id);
            }

            return PartialView("_View", payDirectDepositModel.payDirectDepositList);
        }

        public JsonResult EmployeeSearch(string search)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyIdandEmpName(objUserContextViewModel.CompanyId, search);

            return Json(employeeDetailsModelList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Save(PayDirectDepositModel payDirectDepositModel)
        {
            string result = "";
            string resultMessage = "";
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();
            OperationDetails operationDetails = new OperationDetails();
            if (payDirectDepositModel.DirectDepositID != 0)
            {
                operationDetails = payDirectDepositDB.InsertPayDirectDeposit(payDirectDepositModel, 2);
            }
            else
            {
                operationDetails = payDirectDepositDB.InsertPayDirectDeposit(payDirectDepositModel, 1);
            }

            if (operationDetails.Message.Contains("Error"))
            {
                result = "error";
            }
            else
            {
                result = "success";
            }

            return Json(new { result = result, resultMessage = operationDetails.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            string result = "";
            string resultMessage = "";
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();
            OperationDetails operationDetails = new OperationDetails();
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            payDirectDepositModel.DirectDepositID = id;
            payDirectDepositModel.AccountDate = payDirectDepositModel.CancellationDate = DateTime.Now.ToShortDateString();
            operationDetails = payDirectDepositDB.InsertPayDirectDeposit(payDirectDepositModel, 3);


            if (operationDetails.Message.Contains("Error"))
            {
                result = "error";
            }
            else
            {
                result = "success";
            }

            return Json(new { result = result, resultMessage = operationDetails.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckBankDetailIsAssigned(int id)
        {
            int BankDetailCount = 0;           
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();           
            BankDetailCount=payDirectDepositDB.GetBankDetailIsCount(id);
            return Json(new { BankDetailCount = BankDetailCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CheckDefaultAccount(int EmployeeeId)
        {
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            payDirectDepositModel.payDirectDepositList = payDirectDepositDB.GetPayDirectDepositFullInformation(EmployeeeId);
            if (payDirectDepositModel.payDirectDepositList.Where(x => x.Active == true).Count() > 0)
            {
                return Json(new { isPresent = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isPresent = false }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult CheckUni_BanAccountNo(int DirectDepositID, int EmployeeeId, string UniAccountNumber)
        {
            PayDirectDepositDB payDirectDepositDB = new PayDirectDepositDB();

            if (payDirectDepositDB.CheckUni_BanAccountNumber(DirectDepositID, EmployeeeId, UniAccountNumber))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        /*
        public ActionResult GetEmployeeDetails(int Id)
        {
            Session["EmployeeListID"] = Id;

            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            if (Id != 0)
            {
                int EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                objEmployee.employeeDetailsModel.FirstName_1 = string.IsNullOrEmpty(objEmployee.employeeDetailsModel.FirstName_1) ? "" : objEmployee.employeeDetailsModel.FirstName_1;

                objEmployee.employeeDetailsModel.SurName_1 = string.IsNullOrEmpty(objEmployee.employeeDetailsModel.SurName_1) ? "" : objEmployee.employeeDetailsModel.SurName_1;

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;

                ViewBag.Positon = string.IsNullOrEmpty(objEmployee.employmentInformation.PositionName) ? "" : objEmployee.employmentInformation.PositionName;

                ViewBag.Department = string.IsNullOrEmpty(objEmployee.employmentInformation.DepartmentName) ? "" : objEmployee.employmentInformation.DepartmentName;

                ViewBag.EmployeeIDNo = Id.ToString();
            }
            return Json(objEmployee, JsonRequestBehavior.AllowGet);
        }
        */
    }
}