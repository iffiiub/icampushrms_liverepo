﻿using HRMS.DataAccess;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Text;
using NLog;
using System.Globalization;
using System.IO.Packaging;
using System.Xml;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using DocumentFormat.OpenXml.Drawing.Wordprocessing;
using System.Web.UI;
using HRMS.Web.FileUpload;

namespace HRMS.Web.Controllers
{
    public class PayrollController : BaseController
    {
        //
        // GET: /Payroll/
        Logger logger = LogManager.GetCurrentClassLogger();
        public ActionResult Index()
        {
            PayrollModel payrollModel = new PayrollModel();
            PayrollSettings payrollSettings = new PayrollSettings();
            DepartmentDB departmentDB = new DepartmentDB();
            UserRoleDB userRoleDB = new UserRoleDB();
            BankInformationDB bankInformationDB = new BankInformationDB();
            PayrollDB payrollDB = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            payrollModel.active = 1;
            payrollSettings.cyclesList = new SelectList(payrollDB.getPaycycles(payrollModel), "PayCycleID", "PayCycleName_1");
            payrollSettings.bankList = new SelectList(bankInformationDB.GetBanksList(), "BankID", "BankName_1");
            payrollSettings.departmenrList = new SelectList(departmentDB.GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
            ViewBag.PayrollTemplateSetting = payrollDB.GetPayrollTemplateSetting();
            List<JvsTemplateSetting> lstJvsTemplateSetting = new List<JvsTemplateSetting>();
            lstJvsTemplateSetting = payrollDB.GetJvsTemplateSetting();
            ViewBag.JvsTemplateSetting = lstJvsTemplateSetting;
            payrollSettings.CategoriesList = new SelectList(bankInformationDB.GetPayCategoriesList(), "CategoryID", "CategoryName_1");
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ViewBag.AccountCodeGroupBy = lstJvsTemplateSetting.Where(m => m.JvsTemplateSettingsID == 3).FirstOrDefault().AccountCode;
            ViewBag.AccountCodeOneJv = lstJvsTemplateSetting.Where(m => m.JvsTemplateSettingsID == 4).FirstOrDefault().AccountCode;

            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            return View(payrollSettings);
        }

        [HttpPost]
        public ActionResult SetPayrollData(PayrollModel payrollModel)
        {
            TempData["PayrollModel"] = payrollModel;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPayrollData(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Id", int iSortCol_0 = 0)
        {
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            //On load isPayActive must be checked
            payrollModel.isActive = true;
            payrollModel.isPayActive = true;
            if (TempData["PayrollModel"] != null)
            {
                payrollModel = (PayrollModel)TempData["PayrollModel"];
            }
            List<PayrollModel> payrollList = new List<PayrollModel>();
            payrollList = payrollDB.GetAllPayRollList(payrollModel);

            var vList = new
            {
                aaData = (from item in payrollList
                          select new
                          {
                              Id = item.EmployeeID,
                              EmployeeName = item.EmployeeName,
                              CategoryName = item.CategoryName_1,
                              Checked = "<input type=checkbox class='checkbox' " + (item.isPayActive == true ? "checked=\"checked\"" : "") + " data-val=\"" + item.EmployeeID + "\" onclick=\"return OnClickTr(this);\" />",
                              StaffID = item.EmployeeAlternativeID
                          }).ToArray(),
                recordsTotal = payrollList.Count,
                recordsFiltered = payrollList.Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search()
        {
            PayrollModel payrollModel = new PayrollModel();
            BankInformationDB bankInformationDB = new BankInformationDB();
            //on load isPayActive must be checked
            payrollModel.isActive = true;
            payrollModel.isPayActive = true;
            payrollModel.CategoriesList = new SelectList(bankInformationDB.GetPayCategoriesList(), "CategoryID", "CategoryName_1");
            return PartialView("_Search", payrollModel);
        }
        public JsonResult GetActivePayActiveEmployees(bool isActive,bool isPayActive)
        {
            PayrollDB payrollDB = new PayrollDB();
            var listEmployee = payrollDB.GetActivePayActiveEmployees(isActive, isPayActive);                   
            return Json(listEmployee, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateIsPayctive(int EmployeeID, bool isPayActive)
        {
            PayrollDB payrollDB = new PayrollDB();
            PayrollModel payrollModel = new PayrollModel
            {
                EmployeeID = EmployeeID,
                isPayActive = isPayActive
            };
            OperationDetails operationDetails = new OperationDetails();
            operationDetails = payrollDB.UpdateEmployeeDetailPayActive(payrollModel);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAllIsPayctive(List<string> values, bool isPayActive)
        {
            PayrollDB payrollDb = new PayrollDB();
            foreach (var employeeid in values)
            {
                PayrollModel payrollModel = new PayrollModel
                {
                    EmployeeID = Convert.ToInt32(employeeid),
                    isPayActive = isPayActive
                };

                OperationDetails operationDetails = new OperationDetails();
                operationDetails = payrollDb.UpdateEmployeeDetailPayActive(payrollModel);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult runCycle(PayrollSettings payrollSettings)
        {
            string result = "";
            string resultMessage = "";
            string showType = "default";
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            //if (payrollDB.GetPayrollGeneralSetting().IsconfirmRunPayroll && !payrollSettings.RunFinal)
            //{
            //    PayrollConfirmationRequest payrollConfirmationRequest = new PayrollConfirmationRequest();
            //    payrollConfirmationRequest.CycleId = payrollSettings.PayCycleID;
            //    payrollConfirmationRequest.Date = DateTime.Now.ToString();
            //    payrollConfirmationRequest.IsCancel = false;
            //    payrollConfirmationRequest.IsConfirm = false;
            //    payrollConfirmationRequest.RequestUserName = userContext.Name;
            //    operationDetails = payrollDB.PayrollConfirmationRequestSaveUpdate(payrollConfirmationRequest);
            //    if (operationDetails.Success)
            //    {
            //        operationDetails = payrollDB.RunPayrollForConfirmation(payrollSettings, userContext.UserId);
            //        if (operationDetails.Message == "success")
            //        {
            //            result = "success";
            //            resultMessage = "Request for run payroll send successfull.";
            //        }
            //        else
            //        {
            //            result = "error";
            //            resultMessage = "Technical error has occurred.";
            //        }

            //    }
            //    else
            //    {
            //        result = "error";
            //        resultMessage = operationDetails.Message;
            //    }
            //    return Json(new { result = result, resultMessage = resultMessage, showType = showType }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            if (payrollSettings.DepartmentID != 0 && payrollSettings.DepartmentID != null)
            {
                if (payrollSettings.BankID != 0 && payrollSettings.BankID != null)
                {
                    operationDetails = payrollDB.RunPayrollByDepartmentAndBank(payrollSettings, userContext.UserId);
                }
                else
                {
                    operationDetails = payrollDB.RunPayrollByDepartment(payrollSettings, userContext.UserId);
                }
            }
            else
            {
                if (payrollSettings.BankID != 0 && payrollSettings.BankID != null)
                {
                    operationDetails = payrollDB.RunPayrollByBank(payrollSettings, userContext.UserId);
                }
                else
                {
                    operationDetails = payrollDB.RunPayroll(payrollSettings, userContext.UserId);
                }
            }

            if (operationDetails.Message == "success")
            {
                result = "success";
                if (payrollSettings.RunFinal)
                    resultMessage = "Final cycle run successfully and Jvs are created.";
                else
                    resultMessage = "Cycle generated successfully.";
            }
            else if (operationDetails.Message == "successJv")
            {
                result = "success";
                resultMessage = "Final cycle run successfully and Jvs are created.";
            }
            else if (operationDetails.Message == "failed")
            {
                result = "error";
                resultMessage = "Pay category account settings are incorrect, Plesae make sure accounts setup correctly before final run the cycle to create correct JVs. <a href='/Reporting/PayrollVarificationreportViewer' target='_blank' style='font-size:11px;color:white' >Click here for payroll varification report</a>";
                showType = "custom";
            }
            else if (operationDetails.Message == "100")
            {
                result = "error";
                resultMessage = "Please make sure you have updated Bank Group for Banks.";
                showType = "custom";
            }
            else if (operationDetails.Message == "101")
            {
                result = "error";
                resultMessage = "Cost center configuration is mising for pay category or assign proper pay category to all employees.";
                showType = "custom";
            }
            else
            {
                result = "error";
                resultMessage = "Error Occured";
            }
            return Json(new { result = result, resultMessage = resultMessage, showType = showType }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult report(int bankid, int cycleid, int categoryId)
        {
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            OperationDetails operationDetails = new OperationDetails();

            return null;
        }

        public ActionResult GetCyclesByID(int cycleid)
        {
            PayCycle payCycle = new PayCycle();
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            List<PayCycle> paycycleList = payrollDB.getPaycycles(payrollModel);
            var payCycleModel = paycycleList.Where(x => x.PayCycleID == cycleid).FirstOrDefault();
            payCycleModel.NoOfCycleDays = (Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(payCycleModel.toDate)) - Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(payCycleModel.fromaDate))).TotalDays + 1;
            return Json(payCycleModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getBankListReport(string cycleId)
        {
            byte[] content;
            string fileName = "BankList.xls";
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            DataTable dt = new DataTable();
            dt = payrollDB.getBankListReport(cycleId);
            System.Data.DataSet ds = new DataSet();
            ds.Tables.Add(dt); ;
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        /*
        public ActionResult getBankListReport1(string cycleId)
        {
            byte[] content;
            string fileName = "BankList.xls";
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            OperationDetails operationDetails = new OperationDetails();
            List<BankListReport> bankListReport = new List<BankListReport>();
            bankListReport = payrollDB.getBankListReport(cycleId);

            var report = (from item in bankListReport.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmployeeID = item.EmployeeID,
                              EmpName = item.EmpName,
                              AccountNumber = item.AccountNumber,
                              Amount = item.Amount
                              //WorkEmail = item.WorkEmail,
                              //PersonalEmail = item.PersonalEmail,
                              //BankName = item.BankName,
                              //Category = item.Category,
                              //PayCycleName = item.PayCycleName
                          }).ToList();

            fileName = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/SundriesBankListReports/BankList-" + cycleId + ".xlsx");

            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/SampleSalaryFile-template.xlsx");

            string destinationFile = fileName;

            // Create a copy of the template file and open the copy 
            System.IO.File.Copy(sourceFile, destinationFile, true);

            Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);

            DataTable table = PayrollController.ToDataTable(report);

            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;

                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = ws.GetFirstChild<SheetData>();

                if (sheet == null)
                    throw new Exception("No sheed found in the template file. Please add the sheet");

                // Replace shared strings
                SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                IEnumerable<Text> sharedStringTextElements = sharedStringsPart.SharedStringTable.Descendants<Text>();

                foreach (var text in sharedStringTextElements)
                {
                    if (text.Text.Contains("companyname"))
                    {
                        SchoolInformation model = new SchoolInformationDB().GetSchoolInformation();
                        text.Text = text.Text.Replace("companyname", model.SchoolName_1);
                    }

                    if (text.Text.Contains("paydate"))
                        text.Text = text.Text.Replace("paydate", DateTime.Now.ToString("dd.MM.yyyy"));

                    if (text.Text.Contains("remarks"))
                        text.Text = text.Text.Replace("remarks", "Pay Sundies of " + DateTime.Now.ToString("MMM") + ", " + DateTime.Now.ToString("yyyy"));
                }

                int rowIndex = 18;
                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                var rows = worksheetPart.Worksheet.Descendants<Row>();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);
                }

                Row row;
                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    row = new Row();
                    foreach (string col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        row.AppendChild<Cell>(cell);
                    }
                    row.RowIndex = (uint)rowIndex;
                    sheetData.InsertAt(row, rowIndex);
                    rowIndex++;
                }

                //Insert a blank row
                row = new Row();
                row.RowIndex = (uint)rowIndex;
                sheetData.InsertAt(row, rowIndex);
            }

            return File(destinationFile, "application/vnd.ms-excel", fileName);
        }
        */
        public ActionResult OldCycles()
        {
            PayrollModel payrollModel = new PayrollModel();
            PayrollSettings payrollSettings = new PayrollSettings();
            DepartmentDB departmentDB = new DepartmentDB();
            BankInformationDB bankInformationDB = new BankInformationDB();
            PayrollDB payrollDB = new PayrollDB();
            payrollSettings.paycyclesModelList = payrollDB.getPaycycles(payrollModel).Where(m => m.active == false).ToList();
            payrollSettings.cyclesList = new SelectList(payrollSettings.paycyclesModelList, "PayCycleID", "PayCycleName_1");
            return PartialView("_OldCycles", payrollSettings);
        }

        public ActionResult CheckIfCycleGenerated(int CycleId)
        {
            bool isGenerated = false;
            PayrollDB objPayrollDB = new PayrollDB();
            isGenerated = objPayrollDB.CheckPayCycleGenerated(CycleId);
            return Json(new { isGenerated = isGenerated }, JsonRequestBehavior.AllowGet);
        }

        public void GetReport(string cycleid, int? bankid, int? categoryId, int? departmentId)
        {
            byte[] content;
            string fileName = "Payroll_" + DateTime.Now.ToString("ddMMMyyyy") + "_" + DateTime.Now.ToString("hhmmss") + ".xls";
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            OperationDetails operationDetails = new OperationDetails();
            DataTable dt = new DataTable();
            bool headerRepeated = false;
            string HeaderColumnNames = "";
            dt = payrollDB.GetReport(cycleid, bankid, categoryId, departmentId, out headerRepeated, out HeaderColumnNames);
            content = ExportPayrollReportListToExcel(dt, HeaderColumnNames);
            MemoryStream s = new MemoryStream(content);
            StreamReader sr = new StreamReader(s, Encoding.Unicode);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            Response.Write(sr.ReadToEnd());
            Response.End();
            // return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(string cycleid, int? bankid, int? categoryId, int? departmentId)
        {
            string fileName = "Payroll_" + DateTime.Now.ToString("ddMMMyyyy") + "_" + DateTime.Now.ToString("hhmmss") + ".pdf";
            PayrollModel payrollModel = new PayrollModel();
            payrollModel.active = 1;
            PayrollDB payrollDB = new PayrollDB();
            OperationDetails operationDetails = new OperationDetails();
            DataTable dt = new DataTable();
            bool headerRepeated = false;
            string HeaderColumnNames = "";

            dt = payrollDB.GetReport(cycleid, bankid, categoryId, departmentId, out headerRepeated, out HeaderColumnNames);
            var cycleList = payrollDB.getPaycycles(payrollModel).FirstOrDefault(x => x.PayCycleID == Convert.ToInt32(cycleid));
            DataTable dtNew = dt;
            if (!string.IsNullOrEmpty(HeaderColumnNames))
            {
                DataRow newRow = dt.NewRow();
                try
                {
                    string[] mainColumns = HeaderColumnNames.Split(',');
                    int additionsCount = Convert.ToInt32(mainColumns[2]);
                    int DeductionsCount = Convert.ToInt32(mainColumns[3]);
                    int k = dtNew.Columns["Gross"].Ordinal;
                    newRow[5 + (additionsCount / 2)] = mainColumns[0];
                    int j = dtNew.Columns["Net Payble"].Ordinal;
                    int l = (j - k) / 2;
                    newRow[k + l] = mainColumns[1];
                    newRow["Active"] = "14";
                }
                catch (Exception ex)
                {
                }
                dtNew.Rows.InsertAt(newRow, 1);
                // dtNew.Rows.RemoveAt(0);
            }
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            System.Data.DataSet ds = new DataSet();
            ds.Tables.Add(dtNew);
            iTextSharp.text.Document dc;
            string payrollString = "Run Payroll: " + DateTime.ParseExact(cycleList.fromaDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MMM yyyy");
            CommonHelper.CommonHelper.tempMessage = payrollString;
            if (headerRepeated)
            {
                dc = CommonHelper.CommonHelper.GenratePayrollPDFWithRepeatedHeader(Response, ds, true);
            }
            else
            {
                dc = CommonHelper.CommonHelper.GenratePayrollPDF(Response, ds, true);
            }
            Response.Write(dc);
            Response.End();
        }

        public ActionResult PaySlip(string cycleIds)
        {
            PaySlipSummary paySlipSummary = new PaySlipSummary();
            PayrollDB payRollDB = new PayrollDB();
            JavaScriptSerializer js = new JavaScriptSerializer();

            ViewBag.templateID = new PayrollDB().GetPayslipTemplateID().ToString();
            string[] cycles = js.Deserialize<string[]>(cycleIds);
            var result = string.Join(",", cycles);
            paySlipSummary.EmployeeList = new SelectList(payRollDB.GetTeacherEmployeeList(result), "EmployeeID", "TeacherName");
            paySlipSummary.CategoryList = new SelectList(payRollDB.GetTeacherCategoriesList(result), "CategoryID", "CategoryName_1");
            return PartialView("_PaySlip", paySlipSummary);
        }

        public JsonResult CheckAllPaySlip(string CycleID, int PayslipTypeId, string WhereID)
        {
            DataSet _teacherList;
            PayrollDB _Payrollbusiness = new PayrollDB();
            int TransMode = PayslipTypeId;
            int count = 0;

            string[] cycleidArray = CycleID.Split(',');
            foreach (var item in cycleidArray)
            {
                _teacherList = _Payrollbusiness.GetTeachers_DataSet(item.ToString(), TransMode, WhereID.ToString());
                if (_teacherList.Tables.Count > 0)
                {
                    count = count + _teacherList.Tables[0].Rows.Count;
                }
            }
            if (count > 0)
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToDocPaySlip(string CycleIDArray, int PayslipTypeId, string WhereID, bool isSundriesInclude)
        {
            string[] arryCycleId = CycleIDArray.Split(',');
            List<string> listfileName = new List<string>();
            string folderName = "";
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/PaySlip/");
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/PaySlipZip/");
            CommonHelper.CommonHelper.DeleteFile("/Downloads/PaySlip/");
            CommonHelper.CommonHelper.DeleteFile("/Downloads/PaySlipZip/");
            string fEmployeeID = "";
            string fEmployeeName = "";
            string fCycleName = "";
            int templateID = 1;
            templateID = new PayrollDB().GetPayslipTemplateID();
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            foreach (var CycleID in arryCycleId)
            {
                Employee employee;
                EmployeeDB employeedb = new EmployeeDB();
                PositionDB positiondb = new PositionDB();
                SendMailDB sendMailDb = new SendMailDB();
                bool result = false;
                DataSet _PayslipData, _teacherList, _Salary, _Addition, _Deduction, _Summary, _PayslipSummary;
                int TransMode = PayslipTypeId;
                try
                {
                    string destinationFile = "";
                    PayrollDB _Payrollbusiness = new PayrollDB();
                    HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                    List<DailyAttendanceViewModel> attendanceList = new List<DailyAttendanceViewModel>();
                    List<HR_PaySundriesModel> paySundriesList = new List<HR_PaySundriesModel>();

                    int[] attendance = new int[12];
                    DailyAttendanceViewDB _dailyAttendance = new DailyAttendanceViewDB();
                    PayCycleDB objPayCycleDB = new PayCycleDB();
                    PayCycleModel PayCycle = objPayCycleDB.GetPayCycleById(Convert.ToInt32(CycleID));
                    DateTime DateFrom = DateTime.ParseExact(PayCycle.DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime DateTo = DateTime.ParseExact(PayCycle.DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "", sundriesLebel = "";
                    string fileName = "";
                    if (templateID == 1)
                    {
                        attendanceList = _dailyAttendance.GetDailyAttendanceViewList(DateFrom.ToString("yyyy-MM-dd"), DateTo.ToString("yyyy-MM-dd"), null);
                    }
                    if (templateID == 3)
                    {
                        if (isSundriesInclude)
                        {
                            paySundriesList = objHR_PaySundriesDB.GetPaySundriesByDate(DateFrom, DateTo);
                            sundriesLebel = "Sundries Information";
                        }
                    }
                    _PayslipData = _Payrollbusiness.GetPaySlipData_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _teacherList = _Payrollbusiness.GetTeachers_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _PayslipSummary = _Payrollbusiness.GetPaySlipSummary_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());

                    SchoolInformationDB objSchoolInformationDB = new SchoolInformationDB();
                    SchoolInformation objSchoolInfo = objSchoolInformationDB.GetBasicSchoolInformation();
                    List<EmployeeInfo> lstEmployeeInfo = _Payrollbusiness.GetPaySlipEmployeeInfo();

                    #region GenratePaySlip
                    for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
                    {
                        EmployeeInfo EmpInfo = lstEmployeeInfo.Where(x => x.EmployeeID == Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"])).First();
                        SendMailLogModel logModel = new SendMailLogModel();
                        logModel.employeeID = Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"]);
                        logModel.cycleId = Convert.ToInt32(CycleID);
                        logModel.employeeEmailId = EmpInfo.EmpWorkEmail;
                        logModel.paySendEmailLogID = 0;
                        logModel.payslipLink = "";
                        logModel.sendFlag = false;
                        //sendMailDb.SaveAndUpdateModel(logModel, 1);
                        bool SalaryCount = false;
                        bool AdditionCount = false;
                        bool DeductionCount = false;
                        _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";
                        _Salary = new DataSet();
                        _Addition = new DataSet();
                        _Deduction = new DataSet();
                        _Summary = new DataSet();
                        _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
                        _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
                        _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
                        _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));
                        if (_Salary.Tables.Count > 0)
                        {
                            if (_Salary.Tables.Count > 0)
                            {
                                _employeeid = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                fEmployeeID = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                _name = _Salary.Tables[0].Rows[0]["name"].ToString();
                                fEmployeeName = _Salary.Tables[0].Rows[0]["name"].ToString();
                                _department = _Salary.Tables[0].Rows[0]["department"].ToString();
                                _accountnumber = _Salary.Tables[0].Rows[0]["accountnumber"].ToString();
                                _bank = _Salary.Tables[0].Rows[0]["bank"].ToString();
                            }
                            else
                            {
                                _employeeid = "";
                                _name = "";
                                _department = "";
                                _accountnumber = "";
                                _bank = "";
                            }
                            if (_Summary.Tables.Count > 0)
                            {
                                _cyclename = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                fCycleName = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                _paydate = _Summary.Tables[0].Rows[0]["paydate"].ToString();
                                _datefrom = _Summary.Tables[0].Rows[0]["datefrom"].ToString();
                                _dateto = _Summary.Tables[0].Rows[0]["dateto"].ToString();
                            }
                            else
                            {
                                _cyclename = "";
                                _paydate = "";
                                _datefrom = "";
                                _dateto = "";
                            }
                            string dateFromString = _datefrom.Split(new Char[0])[0];
                            string dateToString = _dateto.Split(new Char[0])[0];
                            DateTime dateFrom = Convert.ToDateTime(dateFromString);
                            DateTime dateTo = Convert.ToDateTime(dateToString);

                            int? employeeId = null;

                            if (!string.IsNullOrEmpty(_employeeid))
                                employeeId = Convert.ToInt32(_employeeid);

                            List<DailyAttendanceViewModel> employeeAttendanceList = new List<DailyAttendanceViewModel>();
                            employeeAttendanceList = attendanceList.Where(x => x.HRID == employeeId).ToList();
                            for (int j = 0; j <= 11; j++)
                            {
                                attendance[j] = employeeAttendanceList.Where(x => x.StatusID == j).Count();
                            }
                            string tempFileName = ("PaySlip-" + _employeeid + "-" + _name + "-" + _cyclename + DateTime.Now.ToString("ddMMyyyyhhmmsstt") + ".docx").Replace(" ", "").Replace("  ", "");
                            fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PaySlip/" + tempFileName);

                            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-1.docx");

                            if (templateID == 1)
                            {
                                sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-1.docx");
                            }
                            else if (templateID == 2)
                            {
                                sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-2.docx");
                            }
                            else if (templateID == 3)
                            {
                                sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-3.docx");
                            }
                            //destinationFile = _Filepath; //System.Web.HttpContext.Current.Server.MapPath("~/Uploads/testdoc.docx");
                            destinationFile = fileName;
                            //Create a copy of the template file and open the copy 
                            System.IO.File.Copy(sourceFile, destinationFile);
                            listfileName.Add(destinationFile);
                            //School Information

                            string schoolName = objSchoolInfo.SchoolName_1;
                            if (objSchoolInfo.CampusName_1 != "")
                                schoolName += " - " + objSchoolInfo.CampusName_1;

                            //create key value pair, key represents words to be replace and 
                            //values represent values in document in place of keys.
                            Dictionary<string, string> keyValues = new Dictionary<string, string>();
                            keyValues.Add("schoolname", schoolName);
                            keyValues.Add("address1", objSchoolInfo.Address);
                            keyValues.Add("city", objSchoolInfo.City);
                            keyValues.Add("country", objSchoolInfo.Country);

                            keyValues.Add("pobox", objSchoolInfo.POBox);
                            keyValues.Add("phoneno", objSchoolInfo.Phone);
                            keyValues.Add("faxno", objSchoolInfo.Fax);
                            keyValues.Add("emailid", objSchoolInfo.EmailId);
                            keyValues.Add("website", objSchoolInfo.Website);

                            keyValues.Add("paymentdate", _paydate != "" ? DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(_paydate) : "");
                            keyValues.Add("payfrom", dateFromString != "" ? DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dateFromString.ToString()) : "");
                            keyValues.Add("payto", dateToString != "" ? DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(dateToString.ToString()) : "");
                            keyValues.Add("employeeid", EmpInfo.EmployeeAlternativeID);
                            keyValues.Add("department", _department);
                            keyValues.Add("bankname", _bank);
                            keyValues.Add("inservicesince", (EmpInfo.HireDate != "") ? DateTime.ParseExact(EmpInfo.HireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd MMMM, yyyy") : "");
                            keyValues.Add("employeename", _name);
                            keyValues.Add("empposition", EmpInfo.PositionTitle);
                            keyValues.Add("accountnumber", _accountnumber);
                            keyValues.Add("remainingoffdays", "");
                            //keyValues.Add("monthname", _cyclename.Split(new char[0])[0] ?? ""); _datefrom
                            keyValues.Add("monthname", Convert.ToDateTime(_dateto).ToString("MMMM"));
                            keyValues.Add("YearName", Convert.ToDateTime(_dateto).ToString("yyyy"));
                            //Commented For Not To Print 
                            keyValues.Add("present", attendance[1].ToString());
                            keyValues.Add("absent", attendance[2].ToString());
                            keyValues.Add("lateinandearlyout", attendance[8].ToString());
                            keyValues.Add("offdays", attendance[7].ToString());
                            keyValues.Add("txt", attendance[9].ToString());
                            keyValues.Add("holidays", attendance[5].ToString());

                            //Remuneration
                            try
                            {
                                if (_Salary.Tables.Count > 0)
                                {
                                    if (_Salary.Tables[0].Rows.Count > 0)
                                    {
                                        for (int ii = 1; ii <= _Salary.Tables[0].Rows.Count; ii++)
                                        {

                                            keyValues.Add("Rem" + ii + "name", _Salary.Tables[0].Rows[ii - 1]["itemDescr"].ToString());
                                            keyValues.Add("Rem" + ii + "no", Convert.ToDouble(_Salary.Tables[0].Rows[ii - 1]["amount"]).ToString(AmountFormat));
                                        }

                                        for (int ii = _Salary.Tables[0].Rows.Count + 1; ii <= 5; ii++)
                                        {
                                            keyValues.Add("Rem" + ii + "name", "");
                                            keyValues.Add("Rem" + ii + "no", "");
                                        }
                                    }
                                    else
                                    {
                                        SalaryCount = true;
                                    }
                                }
                                else
                                    SalaryCount = true;
                            }
                            catch (Exception ex)
                            {

                                logger.Error(ex.Message + ": " + ex.StackTrace);

                            }

                            //Addition
                            try
                            {
                                if (_Addition.Tables.Count > 0)
                                {
                                    if (_Addition.Tables[0].Rows.Count > 0)
                                    {
                                        for (int iii = 1; iii <= _Addition.Tables[0].Rows.Count; iii++)
                                        {
                                            keyValues.Add("Add" + iii + "name", _Addition.Tables[0].Rows[iii - 1]["itemDescr"].ToString());
                                            keyValues.Add("Add" + iii + "no", Convert.ToDouble(_Addition.Tables[0].Rows[iii - 1]["amount"]).ToString(AmountFormat));
                                        }

                                        for (int ii = _Addition.Tables[0].Rows.Count + 1; ii <= 5; ii++)
                                        {
                                            keyValues.Add("Add" + ii + "name", "");
                                            keyValues.Add("Add" + ii + "no", "");
                                        }
                                    }
                                    else
                                    {
                                        AdditionCount = true;
                                    }
                                }
                                else
                                    AdditionCount = true;
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            //Deduction
                            try
                            {
                                if (_Deduction.Tables.Count > 0)
                                {
                                    if (_Deduction.Tables[0].Rows.Count > 0)
                                    {
                                        for (int iiii = 1; iiii <= _Deduction.Tables[0].Rows.Count; iiii++)
                                        {
                                            keyValues.Add("Ded" + iiii + "name", _Deduction.Tables[0].Rows[iiii - 1]["itemDescr"].ToString());
                                            keyValues.Add("Ded" + iiii + "no", Convert.ToDouble(_Deduction.Tables[0].Rows[iiii - 1]["amount"]).ToString(AmountFormat));
                                        }

                                        for (int ii = _Deduction.Tables[0].Rows.Count + 1; ii <= 5; ii++)
                                        {
                                            keyValues.Add("Ded" + ii + "name", "");
                                            keyValues.Add("Ded" + ii + "no", "");
                                        }
                                    }
                                    else
                                    {
                                        DeductionCount = true;
                                    }
                                }
                                else
                                    DeductionCount = true;
                            }
                            catch (Exception ex)
                            {

                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            //Totals
                            try
                            {
                                if (_Summary.Tables.Count > 0)
                                {
                                    if (_Summary.Tables[0].Rows.Count > 0)
                                    {
                                        keyValues.Add("remtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["salaryTotal"]).ToString(AmountFormat));
                                        keyValues.Add("addtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["additionTotal"]).ToString(AmountFormat));
                                        keyValues.Add("dedtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["deductionTotal"]).ToString(AmountFormat));
                                        keyValues.Add("netsalary", (string.Format("{0:n}", Convert.ToDouble(Convert.ToDouble(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat)))) + " " + DataAccess.GeneralDB.CommonDB.DecimalToWord(Convert.ToDecimal(Convert.ToDecimal(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat))));

                                    }
                                    else
                                    {
                                        keyValues.Add("remtotal", "");
                                        keyValues.Add("addtotal", "");
                                        keyValues.Add("dedtotal", "");
                                        keyValues.Add("netsalary", "");
                                    }

                                }
                            }


                            catch (Exception ex)
                            {
                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            if (DeductionCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Ded" + ii + "name", "");
                                    keyValues.Add("Ded" + ii + "no", "");
                                }
                            }

                            if (SalaryCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Rem" + ii + "name", "");
                                    keyValues.Add("Rem" + ii + "no", "");
                                }
                            }

                            if (AdditionCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Add" + ii + "name", "");
                                    keyValues.Add("Add" + ii + "no", "");
                                }
                            }

                            keyValues.Add("Sundry Information", sundriesLebel);

                            SearchAndReplace(destinationFile, keyValues, objSchoolInfo.Logo);
                            if (templateID == 3 && isSundriesInclude == true)
                                AddSundriesInformation(destinationFile, paySundriesList.Where(x => x.EmployeeID == logModel.employeeID).ToList(), AmountFormat);

                            _Salary.Dispose();
                            _Addition.Dispose();
                            _Deduction.Dispose();
                            _Summary.Dispose();
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ": " + ex.StackTrace);
                    result = false;
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            #region DownloadPaySlip
            if (listfileName.Count() > 0)
            {
                if (listfileName.Count() > 1)
                {
                    string destinationZip = "";
                    string destinationzipDownload = "";
                    GenrateZip("~/Downloads/PaySlip/", "~/Downloads/PaySlipZip/", listfileName, arryCycleId, fEmployeeID, fEmployeeName, fCycleName, out destinationzipDownload, out destinationZip);
                    return File(destinationzipDownload, "application/zip", destinationZip);
                }
                else if (listfileName.Count() == 1)
                {
                    string destinationZip = "";
                    string destinationzipDownload = "";
                    if (arryCycleId.Count() != 1)
                    {
                        GenrateZip("~/Downloads/PaySlip/", "~/Downloads/PaySlipZip/", listfileName, arryCycleId, fEmployeeID, fEmployeeName, fCycleName, out destinationzipDownload, out destinationZip);
                        return File(destinationzipDownload, "application/zip", destinationZip);
                    }
                    else
                    {
                        return File(listfileName.FirstOrDefault(), System.Net.Mime.MediaTypeNames.Application.Octet, "PaySlip-" + fEmployeeID + "-" + fEmployeeName + "-" + fCycleName + ".docx");//
                    }
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            #endregion
        }

        public void GenrateZip(string fileFolderName, string destinationFolderName, List<string> fileList, string[] cycleid, string employeeId, string employeeName, string cycleName, out string destinationzipDownload, out string destinationZip)
        {
            if (cycleid.Count() != 1)
                destinationZip = "PaySlip-" + employeeId + "-" + employeeName + ".zip";
            else
                destinationZip = "Payslip-" + cycleName + ".zip";

            string endfolderName = System.Web.HttpContext.Current.Server.MapPath(destinationFolderName);
            string zipPath = endfolderName;
            string folderName = System.Web.HttpContext.Current.Server.MapPath(fileFolderName);
            string sourcezip = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/Download.zip");
            destinationzipDownload = System.Web.HttpContext.Current.Server.MapPath(destinationFolderName + destinationZip);
            System.IO.File.Copy(sourcezip, destinationzipDownload);
            CreateZip(destinationzipDownload, fileList);
        }

        public void CreateZip(string destinationFile, List<string> listFile)
        {
            try
            {
                using (FileStream zipToOpen = new FileStream(destinationFile, FileMode.Open))
                {
                    string[] separators = { "\\" };
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                    {
                        foreach (var tepfileName in listFile)
                        {
                            archive.CreateEntryFromFile(tepfileName, tepfileName.Split(separators, StringSplitOptions.RemoveEmptyEntries).LastOrDefault());
                        }
                    }
                }
            }
            catch { }
        }


        /*
          These Action is for generating payslip for employee in doc file and save payslip link in sendmail table
          These Action Committed Becuse we don't need these at time.
          Do not delete it might be in future we need these.
       
        public ActionResult SendMailAndExportToDocPaySlip(string CycleIDArray, int PayslipTypeId, string WhereID)
        {
            string[] arryCycleId = CycleIDArray.Split(',');
            List<string> listfileName = new List<string>();
            string fEmployeeID = "";
            string fEmployeeName = "";
            string fCycleName = "";
            bool cheack = true;
            int templateID = 1;
            templateID = new PayrollDB().GetPayslipTemplateID();

            foreach (var CycleID in arryCycleId)
            {
                Employee employee;
                EmployeeDB employeedb = new EmployeeDB();
                PositionDB positiondb = new PositionDB();
                SendMailDB sendMailDb = new SendMailDB();
                bool result = false;
                DataSet _PayslipData, _teacherList, _Salary, _Addition, _Deduction, _Summary, _PayslipSummary;
                int TransMode = PayslipTypeId;
                try
                {
                    string destinationFile = "";
                    PayrollDB _Payrollbusiness = new PayrollDB();
                    List<DailyAttendanceViewModel> attendanceList = new List<DailyAttendanceViewModel>();
                    int[] attendance = new int[12];
                    DailyAttendanceViewDB _dailyAttendance = new DailyAttendanceViewDB();
                    _PayslipData = _Payrollbusiness.GetPaySlipData_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _teacherList = _Payrollbusiness.GetTeachers_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _PayslipSummary = _Payrollbusiness.GetPaySlipSummary_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";
                    string fileName = "";
                    SchoolInformationDB objSchoolInformationDB = new SchoolInformationDB();
                    SchoolInformation objSchoolInfo = objSchoolInformationDB.GetBasicSchoolInformation();
                    var position = positiondb.GetPositionList();

                    #region GenratePaySlip
                    for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
                    {
                        employee = employeedb.GetAllDetailsOfEmployee(Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"]));
                        SendMailLogModel logModel = new SendMailLogModel();
                        logModel.employeeID = Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"]);
                        logModel.cycleId = Convert.ToInt32(CycleID);
                        logModel.employeeEmailId = employee.employeeContactModel.WorkEmail;
                        logModel.paySendEmailLogID = 0;
                        logModel.payslipLink = "";
                        logModel.sendFlag = false;


                        bool SalaryCount = false;
                        bool AdditionCount = false;
                        bool DeductionCount = false;
                        _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";
                        _Salary = new DataSet();
                        _Addition = new DataSet();
                        _Deduction = new DataSet();
                        _Summary = new DataSet();
                        _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
                        _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
                        _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
                        _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));
                        if (_Salary.Tables.Count > 0)
                        {
                            if (_Salary.Tables.Count > 0)
                            {
                                _employeeid = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                fEmployeeID = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                _name = _Salary.Tables[0].Rows[0]["name"].ToString();
                                fEmployeeName = _Salary.Tables[0].Rows[0]["name"].ToString();
                                _department = _Salary.Tables[0].Rows[0]["department"].ToString();
                                _accountnumber = _Salary.Tables[0].Rows[0]["accountnumber"].ToString();
                                _bank = _Salary.Tables[0].Rows[0]["bank"].ToString();
                            }
                            else
                            {
                                _employeeid = "";
                                _name = "";
                                _department = "";
                                _accountnumber = "";
                                _bank = "";
                            }

                            if (_Summary.Tables.Count > 0)
                            {
                                _cyclename = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                fCycleName = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                _paydate = _Summary.Tables[0].Rows[0]["paydate"].ToString();
                                _datefrom = _Summary.Tables[0].Rows[0]["datefrom"].ToString();
                                _dateto = _Summary.Tables[0].Rows[0]["dateto"].ToString();
                            }
                            else
                            {
                                _cyclename = "";
                                _paydate = "";
                                _datefrom = "";
                                _dateto = "";
                            }
                            string dateFromString = _datefrom.Split(new char[0])[0];
                            string dateToString = _dateto.Split(new char[0])[0];
                            DateTime dateFrom = Convert.ToDateTime(dateFromString);
                            DateTime dateTo = Convert.ToDateTime(dateToString);

                            int? employeeId = null;

                            if (!string.IsNullOrEmpty(_employeeid))
                                employeeId = Convert.ToInt32(_employeeid);
                            var opLogData = sendMailDb.SaveAndUpdateModel(logModel, 1);
                            attendanceList = _dailyAttendance.GetDailyAttendanceViewList(dateFrom.ToString("yyyy-MM-dd"), dateTo.ToString("yyyy-MM-dd"), employeeId);
                            string destinationFileFinal = ("PaySlip-" + _employeeid + "-" + _name + "-" + _cyclename + "-" + opLogData.InsertedRowId + "-" + DateTime.Now.ToString("ddMMyyyyhhmmsstt") + ".docx").Replace(" ", "").Replace("  ", "");
                            logModel.payslipLink = destinationFileFinal;
                            logModel.paySendEmailLogID = opLogData.InsertedRowId;
                            for (int j = 0; j <= 11; j++)
                            {
                                attendance[j] = attendanceList.Where(x => x.StatusID == j).Count();
                            }

                            fileName = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/PaySlipSendMail/" + destinationFileFinal);
                            logModel.payslipLink = fileName;
                            sendMailDb.SaveAndUpdateModel(logModel, 2);
                            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-1.docx");

                            if (templateID == 1)
                            {
                                sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-1.docx");
                            }
                            else if (templateID == 2)
                            {
                                sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlipTemplates/Pay-slip-template-2.docx");
                            }


                            //destinationFile = _Filepath; //System.Web.HttpContext.Current.Server.MapPath("~/Uploads/testdoc.docx");
                            destinationFile = fileName;
                            //Create a copy of the template file and open the copy 
                            System.IO.File.Copy(sourceFile, destinationFile);
                            listfileName.Add(destinationFile);
                            //School Information

                            string schoolName = objSchoolInfo.SchoolName_1;
                            if (objSchoolInfo.CampusName_1 != "")
                                schoolName += " - " + objSchoolInfo.CampusName_1;

                            //create key value pair, key represents words to be replace and 
                            //values represent values in document in place of keys.
                            Dictionary<string, string> keyValues = new Dictionary<string, string>();
                            keyValues.Add("schoolname", schoolName);
                            keyValues.Add("address1", objSchoolInfo.Address);
                            keyValues.Add("city", objSchoolInfo.City);
                            keyValues.Add("country", objSchoolInfo.Country);

                            keyValues.Add("pobox", objSchoolInfo.POBox);
                            keyValues.Add("phoneno", objSchoolInfo.Phone);
                            keyValues.Add("faxno", objSchoolInfo.Fax);
                            keyValues.Add("emailid", objSchoolInfo.EmailId);
                            keyValues.Add("website", objSchoolInfo.Website);

                            keyValues.Add("paymentdate", _paydate.Split(new char[0])[0] ?? "");
                            keyValues.Add("payfrom", dateFromString ?? "");
                            keyValues.Add("payto", dateToString ?? "");
                            keyValues.Add("employeeid", _employeeid);
                            keyValues.Add("department", _department);
                            keyValues.Add("bankname", _bank);
                            keyValues.Add("inservicesince", (employee.employmentInformation.HireDate != "") ? Convert.ToDateTime(CommonDB.SetCulturedDate(employee.employmentInformation.HireDate)).ToString("dd MMMM, yyyy") : "");
                            keyValues.Add("employeename", _name);

                            try
                            {
                                keyValues.Add("empposition", position.Where(x => x.PositionID == employee.employmentInformation.PositionID).FirstOrDefault().PositionTitle);
                            }
                            catch
                            {
                                keyValues.Add("empposition", "");
                            }
                            keyValues.Add("accountnumber", _accountnumber);
                            keyValues.Add("remainingoffdays", "");
                            //keyValues.Add("monthname", _cyclename.Split(new char[0])[0] ?? ""); _datefrom
                            keyValues.Add("monthname", Convert.ToDateTime(_datefrom).ToString("MMMM"));
                            keyValues.Add("YearName", Convert.ToDateTime(_datefrom).ToString("yyyy"));
                            keyValues.Add("present", attendance[1].ToString());
                            keyValues.Add("absent", attendance[2].ToString());
                            keyValues.Add("lateinandearlyout", attendance[8].ToString());
                            keyValues.Add("offdays", attendance[7].ToString());
                            keyValues.Add("txtextradays", attendance[9].ToString());
                            keyValues.Add("holidays", attendance[5].ToString());

                            //Remuneration
                            try
                            {
                                if (_Salary.Tables.Count > 0)
                                {
                                    if (_Salary.Tables[0].Rows.Count > 0)
                                    {
                                        for (int ii = 1; ii <= _Salary.Tables[0].Rows.Count; ii++)
                                        {

                                            keyValues.Add("Rem" + ii + "name", _Salary.Tables[0].Rows[ii - 1]["itemDescr"].ToString());
                                            keyValues.Add("Rem" + ii + "no", Convert.ToDouble(_Salary.Tables[0].Rows[ii - 1]["amount"]).ToString());
                                        }

                                        for (int ii = _Salary.Tables[0].Rows.Count + 1; ii <= 5; ii++)
                                        {
                                            keyValues.Add("Rem" + ii + "name", "");
                                            keyValues.Add("Rem" + ii + "no", "");
                                        }
                                    }
                                    else
                                    {
                                        SalaryCount = true;
                                    }
                                }
                                else
                                    SalaryCount = true;
                            }
                            catch (Exception ex)
                            {

                                logger.Error(ex.Message + ": " + ex.StackTrace);

                            }

                            //Addition
                            try
                            {
                                if (_Addition.Tables.Count > 0)
                                {
                                    if (_Addition.Tables[0].Rows.Count > 0)
                                    {
                                        for (int iii = 1; iii <= _Addition.Tables[0].Rows.Count; iii++)
                                        {
                                            keyValues.Add("Add" + iii + "name", _Addition.Tables[0].Rows[iii - 1]["itemDescr"].ToString());
                                            keyValues.Add("Add" + iii + "no", Convert.ToDouble(_Addition.Tables[0].Rows[iii - 1]["amount"]).ToString());
                                        }

                                        for (int ii = _Addition.Tables[0].Rows.Count + 1; ii <= 5; ii++)
                                        {
                                            keyValues.Add("Add" + ii + "name", "");
                                            keyValues.Add("Add" + ii + "no", "");
                                        }
                                    }
                                    else
                                    {
                                        AdditionCount = true;
                                    }
                                }
                                else
                                    AdditionCount = true;
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            //Deduction
                            try
                            {
                                if (_Deduction.Tables.Count > 0)
                                {
                                    if (_Deduction.Tables[0].Rows.Count > 0)
                                    {
                                        for (int iiii = 1; iiii <= _Deduction.Tables[0].Rows.Count; iiii++)
                                        {
                                            keyValues.Add("Ded" + iiii + "name", _Deduction.Tables[0].Rows[iiii - 1]["itemDescr"].ToString());
                                            keyValues.Add("Ded" + iiii + "no", Convert.ToDouble(_Deduction.Tables[0].Rows[iiii - 1]["amount"]).ToString());
                                        }

                                        for (int ii = _Deduction.Tables[0].Rows.Count + 1; ii <= 5; ii++)
                                        {
                                            keyValues.Add("Ded" + ii + "name", "");
                                            keyValues.Add("Ded" + ii + "no", "");
                                        }
                                    }
                                    else
                                    {
                                        DeductionCount = true;
                                    }
                                }
                                else
                                    DeductionCount = true;
                            }
                            catch (Exception ex)
                            {

                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            //Totals
                            try
                            {
                                if (_Summary.Tables.Count > 0)
                                {
                                    if (_Summary.Tables[0].Rows.Count > 0)
                                    {
                                        keyValues.Add("remtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["salaryTotal"]).ToString());
                                        keyValues.Add("addtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["additionTotal"]).ToString());
                                        keyValues.Add("dedtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["deductionTotal"]).ToString());
                                        keyValues.Add("netsalary", Convert.ToDouble(_Summary.Tables[0].Rows[0]["net"]).ToString());

                                    }
                                    else
                                    {
                                        keyValues.Add("remtotal", "");
                                        keyValues.Add("addtotal", "");
                                        keyValues.Add("dedtotal", "");
                                        keyValues.Add("netsalary", "");
                                    }

                                }
                            }


                            catch (Exception ex)
                            {
                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            if (DeductionCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Ded" + ii + "name", "");
                                    keyValues.Add("Ded" + ii + "no", "");
                                }
                            }

                            if (SalaryCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Rem" + ii + "name", "");
                                    keyValues.Add("Rem" + ii + "no", "");
                                }
                            }

                            if (AdditionCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Add" + ii + "name", "");
                                    keyValues.Add("Add" + ii + "no", "");
                                }
                            }

                            SearchAndReplace(destinationFile, keyValues, objSchoolInfo.Logo);

                            _Salary.Dispose();
                            _Addition.Dispose();
                            _Deduction.Dispose();
                            _Summary.Dispose();
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ": " + ex.StackTrace);
                    result = false;
                    cheack = false;
                }
            }
            return Json(cheack, JsonRequestBehavior.AllowGet);


        }
 */

        public ActionResult ExportToExcelPaySlip(string CycleIds, int PayslipTypeId, string whereId)
        {
            byte[] content;
            string fileName = "PaySlip.xls";
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            PaySlipSummary paySlipSummary = new PaySlipSummary();
            List<PaySlipSummary> paySlipSummaryList = new List<PaySlipSummary>();
            List<PaySlipReport> paySlipReport = new List<PaySlipReport>();
            paySlipReport = payrollDB.GetPaySlipReport(CycleIds, PayslipTypeId, whereId);
            paySlipSummaryList = payrollDB.GetPaySlipSummary(CycleIds, PayslipTypeId, whereId);
            var report = (from item in paySlipSummaryList.AsEnumerable()
                          select new
                          {
                              EmployeeID = item.EmployeeID,
                              SalaryToatl = item.salaryTotal,
                              AdditionTotal = item.additionTotal,
                              DeductionTotal = item.deductionTotal,
                              Cycle = item.cycle,
                              Net = item.net,
                              CycleName = item.cycleName,
                              DateFrom = item.DateFrom,
                              DateTo = item.DateTo,
                              PayDate = item.paydate,
                              SchoolName = item.schoolname
                          }).ToList();


            content = ExportListToExcel(report);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };

        }

        public ActionResult GetEmailListReport(string cycleIds)
        {
            byte[] content;
            string fileName = "EmailList.xls";
            PayrollModel payrollModel = new PayrollModel();
            PayrollDB payrollDB = new PayrollDB();
            OperationDetails operationDetails = new OperationDetails();
            List<PaySlipSummary> emailListReport = new List<PaySlipSummary>();
            emailListReport = payrollDB.GetTeacherMailList(cycleIds);
            var report = (from item in emailListReport.AsEnumerable()
                          select new
                          {
                              EmployeeID = item.EmployeeID,
                              EmployeeAlterNativeID = item.EmployeeAlternativeID,
                              TeacherName = item.TeacherName

                          }).ToList();


            content = ExportListToExcel(report);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };

        }

        // To search and replace content in a document part.
        public void SearchAndReplace(string document, Dictionary<string, string> dict, string imagePath)
        {
            try
            {
                int count = 0;
                CommonHelper.CommonHelper commonHelper = new CommonHelper.CommonHelper();
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
                {
                    MainDocumentPart mainPart = wordDoc.MainDocumentPart;

                    string docText = null;

                    using (StreamReader sr = new StreamReader(mainPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    foreach (KeyValuePair<string, string> item in dict)
                    {
                        Regex regexText = new Regex(item.Key);
                        docText = regexText.Replace(docText, item.Value);
                    }

                    using (StreamWriter sw = new StreamWriter(
                              mainPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                    Inline selectedImage = mainPart.Document.Descendants<Inline>().First();
                    // get the ID from the inline element
                    string imageId = "default value";
                    DocumentFormat.OpenXml.Drawing.Blip blipElement = selectedImage.Descendants<DocumentFormat.OpenXml.Drawing.Blip>().First();
                    if (blipElement != null)
                    {
                        imageId = blipElement.Embed.Value;
                    }


                    ImagePart imagePart = (ImagePart)mainPart.GetPartById(imageId);
                    byte[] imageBytes = System.IO.File.ReadAllBytes(Server.MapPath(imagePath));
                    BinaryWriter writer = new BinaryWriter(imagePart.GetStream());
                    writer.Write(imageBytes);
                    writer.Close();

                    mainPart.GetStream(FileMode.Open);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        public void AddSundriesInformation(string document, List<HR_PaySundriesModel> sundriesModelList, string Amountformate)
        {
            int count = 0;
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                var doc = wordDoc.MainDocumentPart.Document;
                decimal amountTotal = 0;
                Table table = new Table();
                TableProperties props = CommonHelper.CommonHelper.GetTableProperties(6, 6, 6, 6, 6, 6, "9400");
                int[] widtharray = new int[] { 35, 15, 15, 20, 15 };
                string[,] sundriesColumn = new string[1, 5]
                {
                        {"Pay Sundries Date","Sundry","Batch","Academic Year","Amount"  }
                };

                table.AppendChild<TableProperties>(props);
                for (var i = 0; i <= sundriesColumn.GetUpperBound(0); i++)
                {
                    var tr = new TableRow();
                    for (var j = 0; j <= sundriesColumn.GetUpperBound(1); j++)
                    {

                        var tc = new TableCell();
                        RunProperties properties = new RunProperties();
                        Bold bold = new Bold();
                        FontSize fontSize = new FontSize { Val = "18" };
                        RunFonts runFonts = new RunFonts() { Ascii = "Arial" };
                        properties.AppendChild(bold);
                        properties.AppendChild(fontSize);
                        properties.AppendChild(runFonts);
                        TableCellProperties tableCellProperties = CommonHelper.CommonHelper.GetTableCellProperties(6, 6, 6, 6, 6, 6, widtharray[count++].ToString());
                        Shading shading =
                                new DocumentFormat.OpenXml.Wordprocessing.Shading()
                                {
                                    Color = "auto",
                                    Fill = "BFBFBF",
                                    Val = ShadingPatternValues.Clear
                                };

                        tableCellProperties.Append(shading);
                        tc.AppendChild<TableCellProperties>(tableCellProperties);
                        TableCellRightMargin cellRightMargin = new TableCellRightMargin() { Type = TableWidthValues.Dxa, Width = 200 };
                        TableCellLeftMargin cellLeftMargin = new TableCellLeftMargin() { Type = TableWidthValues.Dxa, Width = 200 };
                        Paragraph paraInner = new Paragraph();
                        Run innerRun3 = new Run();
                        innerRun3.AppendChild(cellRightMargin);
                        innerRun3.AppendChild(cellLeftMargin);
                        Text innerText3 = new Text(sundriesColumn[i, j]);
                        innerRun3.AppendChild(properties);
                        paraInner.AppendChild(CommonHelper.CommonHelper.GetParagraphProp("left"));
                        innerRun3.AppendChild(innerText3);
                        paraInner.AppendChild(innerRun3);
                        tc.AppendChild(paraInner);
                        tr.Append(tc);
                    }
                    table.Append(tr);
                }
                count = 0;
                foreach (var item in sundriesModelList)
                {
                    count = 0;
                    var tr = new TableRow();
                    for (int i = 0; i <= 4; i++)
                    {

                        var tc = new TableCell();
                        RunProperties properties = new RunProperties();
                        FontSize fontSize = new FontSize { Val = "18" };
                        RunFonts runFonts = new RunFonts() { Ascii = "Arial" };
                        properties.AppendChild(fontSize);
                        properties.AppendChild(runFonts);
                        TableCellProperties tableCellProperties = CommonHelper.CommonHelper.GetTableCellProperties(6, 6, 6, 6, 6, 6, widtharray[count++].ToString());

                        tc.AppendChild<TableCellProperties>(tableCellProperties);
                        TableCellRightMargin cellRightMargin = new TableCellRightMargin() { Type = TableWidthValues.Dxa, Width = 200 };
                        TableCellLeftMargin cellLeftMargin = new TableCellLeftMargin() { Type = TableWidthValues.Dxa, Width = 200 };
                        Paragraph paraInner = new Paragraph();
                        Run innerRun3 = new Run();
                        innerRun3.AppendChild(cellRightMargin);
                        innerRun3.AppendChild(cellLeftMargin);
                        Text innerText3 = new Text();
                        switch (i)
                        {
                            case 0:
                                innerText3.Text = item.PaySundriesDate.ToString();
                                break;
                            case 1:
                                innerText3.Text = item.SundryName.ToString();
                                break;
                            case 2:
                                innerText3.Text = item.PayBatchName.ToString();
                                break;
                            case 3:
                                innerText3.Text = item.AcademicYear.ToString();
                                break;
                            case 4:
                                innerText3.Text = item.Amount.ToString(Amountformate);
                                amountTotal = amountTotal + item.Amount;
                                break;
                        }
                        innerRun3.AppendChild(properties);
                        paraInner.AppendChild(CommonHelper.CommonHelper.GetParagraphProp("left"));
                        innerRun3.AppendChild(innerText3);
                        paraInner.AppendChild(innerRun3);
                        tc.AppendChild(paraInner);
                        tr.Append(tc);
                    }
                    table.Append(tr);
                }
                count = 0;

                for (var i = 0; i <= sundriesColumn.GetUpperBound(0); i++)
                {
                    var tr = new TableRow();
                    for (var j = 0; j <= sundriesColumn.GetUpperBound(1); j++)
                    {

                        var tc = new TableCell();
                        RunProperties properties = new RunProperties();
                        Bold bold = new Bold();
                        FontSize fontSize = new FontSize { Val = "18" };
                        RunFonts runFonts = new RunFonts() { Ascii = "Arial" };
                        properties.AppendChild(bold);
                        properties.AppendChild(fontSize);
                        properties.AppendChild(runFonts);
                        TableCellProperties tableCellProperties = CommonHelper.CommonHelper.GetTableCellProperties(0, 0, 0, 0, 0, 0, widtharray[count++].ToString());
                        HorizontalMerge horizantalMearge = new HorizontalMerge();
                        if (j == 0)
                        {
                            horizantalMearge.Val = MergedCellValues.Restart;
                        }
                        else
                        {
                            horizantalMearge.Val = MergedCellValues.Continue;
                        }
                        tableCellProperties.Append(horizantalMearge);

                        tc.AppendChild<TableCellProperties>(tableCellProperties);
                        TableCellRightMargin cellRightMargin = new TableCellRightMargin() { Type = TableWidthValues.Dxa, Width = 200 };
                        TableCellLeftMargin cellLeftMargin = new TableCellLeftMargin() { Type = TableWidthValues.Dxa, Width = 200 };
                        Paragraph paraInner = new Paragraph();
                        Run innerRun3 = new Run();
                        innerRun3.AppendChild(cellRightMargin);
                        innerRun3.AppendChild(cellLeftMargin);
                        Text innerText3 = new Text();
                        switch (j)
                        {
                            case 0:
                                innerText3.Text = "Net Sundries:" + amountTotal.ToString(Amountformate);
                                break;
                            case 1:
                                innerText3.Text = "";
                                break;
                            case 2:
                                innerText3.Text = "";
                                break;
                            case 3:
                                innerText3.Text = "";
                                break;
                            case 4:
                                innerText3.Text = "";
                                break;
                        }
                        innerRun3.AppendChild(properties);
                        paraInner.AppendChild(CommonHelper.CommonHelper.GetParagraphProp("left"));
                        innerRun3.AppendChild(innerText3);
                        paraInner.AppendChild(innerRun3);
                        tc.AppendChild(paraInner);
                        tr.Append(tc);
                    }
                    table.Append(tr);
                }
                doc.Body.Append(table);
                doc.Save();
            }
        }

        public ActionResult GetEmployeeNotInPayRoll()
        {
            PayrollDB payrollDB = new PayrollDB();
            List<EmployeeDetailsModel> employeeList = payrollDB.GetEmployeeNotinPayroll();
            OperationDetails oDetails = new OperationDetails();

            if (employeeList.Count > 0)
            {
                oDetails.Success = true;
                oDetails.Message = " <strong>" + employeeList.Count() + "</strong> employee(s) with missing bank or salary information";
                oDetails.CssClass = "warning";
            }
            return Json(oDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportPayslipToPdf(string CycleIDArray, int PayslipTypeId, string WhereID, bool isSundriesInclude)
        {
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            string[] arryCycleId = CycleIDArray.Split(',');
            List<string> listfileName = new List<string>();
            string folderName = "";
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/PaySlip/");
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/PaySlipZip/");
            CommonHelper.CommonHelper.DeleteFile("/Downloads/PaySlip/");
            CommonHelper.CommonHelper.DeleteFile("/Downloads/PaySlipZip/");
            string fEmployeeID = "";
            string fEmployeeName = "";
            string fCycleName = "";

            string fileName = "";
            int templateID = 1;
            templateID = new PayrollDB().GetPayslipTemplateID();

            #region Create Data Dictionary
            foreach (var CycleID in arryCycleId)
            {
                Employee employee;
                EmployeeDB employeedb = new EmployeeDB();
                PositionDB positiondb = new PositionDB();
                SendMailDB sendMailDb = new SendMailDB();
                HR_PaySundriesDB sundryDb = new HR_PaySundriesDB();
                bool result = false;
                DataSet _PayslipData, _teacherList, _Salary, _Addition, _Deduction, _Summary, _PayslipSummary;
                int TransMode = PayslipTypeId;
                try
                {

                    PayrollDB _Payrollbusiness = new PayrollDB();
                    List<DailyAttendanceViewModel> attendanceList = new List<DailyAttendanceViewModel>();
                    List<HR_PaySundriesModel> SundryModelList = new List<HR_PaySundriesModel>();
                    int[] attendance = new int[12];
                    DailyAttendanceViewDB _dailyAttendance = new DailyAttendanceViewDB();
                    PayCycleDB objPayCycleDB = new PayCycleDB();
                    PayCycleModel PayCycle = objPayCycleDB.GetPayCycleById(Convert.ToInt32(CycleID));
                    DateTime DateFrom = DateTime.ParseExact(PayCycle.DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime DateTo = DateTime.ParseExact(PayCycle.DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (templateID == 1)
                    {
                        attendanceList = _dailyAttendance.GetDailyAttendanceViewList(DateFrom.ToString("yyyy-MM-dd"), DateTo.ToString("yyyy-MM-dd"), null);
                    }
                    if (isSundriesInclude)
                    {
                        SundryModelList = sundryDb.GetPaySundriesByDate(DateFrom, DateTo);
                    }
                    _PayslipData = _Payrollbusiness.GetPaySlipData_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _teacherList = _Payrollbusiness.GetTeachers_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _PayslipSummary = _Payrollbusiness.GetPaySlipSummary_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";

                    SchoolInformationDB objSchoolInformationDB = new SchoolInformationDB();
                    SchoolInformation objSchoolInfo = objSchoolInformationDB.GetBasicSchoolInformation();
                    List<EmployeeInfo> lstEmployeeInfo = _Payrollbusiness.GetPaySlipEmployeeInfo();

                    #region GenratePaySlip
                    for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            EmployeeInfo EmpInfo = lstEmployeeInfo.Where(x => x.EmployeeID == Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"])).First();
                            SendMailLogModel logModel = new SendMailLogModel();
                            logModel.employeeID = Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"]);
                            logModel.cycleId = Convert.ToInt32(CycleID);
                            logModel.employeeEmailId = EmpInfo.EmpWorkEmail;
                            logModel.paySendEmailLogID = 0;
                            logModel.payslipLink = "";
                            logModel.sendFlag = false;
                            //sendMailDb.SaveAndUpdateModel(logModel, 1);
                            bool SalaryCount = false;
                            bool AdditionCount = false;
                            bool DeductionCount = false;
                            _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";
                            _Salary = new DataSet();
                            _Addition = new DataSet();
                            _Deduction = new DataSet();
                            _Summary = new DataSet();
                            _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
                            _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
                            _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
                            _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));
                            if (_Salary.Tables.Count > 0)
                            {
                                if (_Salary.Tables.Count > 0)
                                {
                                    _employeeid = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                    fEmployeeID = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                    _name = _Salary.Tables[0].Rows[0]["name"].ToString();
                                    fEmployeeName = _Salary.Tables[0].Rows[0]["name"].ToString();
                                    _department = _Salary.Tables[0].Rows[0]["department"].ToString();
                                    _accountnumber = _Salary.Tables[0].Rows[0]["accountnumber"].ToString();
                                    _bank = _Salary.Tables[0].Rows[0]["bank"].ToString();
                                }
                                else
                                {
                                    _employeeid = "";
                                    _name = "";
                                    _department = "";
                                    _accountnumber = "";
                                    _bank = "";
                                }

                                if (_Summary.Tables.Count > 0)
                                {
                                    _cyclename = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                    fCycleName = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                    _paydate = _Summary.Tables[0].Rows[0]["paydate"].ToString();
                                    _datefrom = _Summary.Tables[0].Rows[0]["datefrom"].ToString();
                                    _dateto = _Summary.Tables[0].Rows[0]["dateto"].ToString();
                                }
                                else
                                {
                                    _cyclename = "";
                                    _paydate = "";
                                    _datefrom = "";
                                    _dateto = "";
                                }
                                string dateFromString = _datefrom.Split(new char[0])[0];
                                string dateToString = _dateto.Split(new char[0])[0];
                                DateTime dateFrom = Convert.ToDateTime(dateFromString);
                                DateTime dateTo = Convert.ToDateTime(dateToString);

                                int? employeeId = null;

                                if (!string.IsNullOrEmpty(_employeeid))
                                    employeeId = Convert.ToInt32(_employeeid);

                                List<DailyAttendanceViewModel> employeeAttendanceList = new List<DailyAttendanceViewModel>();
                                employeeAttendanceList = attendanceList.Where(x => x.HRID == employeeId).ToList();
                                for (int j = 0; j <= 11; j++)
                                {
                                    attendance[j] = employeeAttendanceList.Where(x => x.StatusID == j).Count();
                                }
                                string tempFileName = ("PaySlip-" + _employeeid + "-" + _name + "-" + _cyclename + DateTime.Now.ToString("ddMMyyyyhhmmsstt") + ".pdf").Replace(" ", "").Replace("  ", "");
                                fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PaySlip/" + tempFileName);


                                listfileName.Add(fileName);
                                //School Information

                                string schoolName = objSchoolInfo.SchoolName_1;
                                if (objSchoolInfo.CampusName_1 != "")
                                    schoolName += " - " + objSchoolInfo.CampusName_1;

                                //create key value pair, key represents words to be replace and 
                                //values represent values in document in place of keys.
                                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                                keyValues.Add("schoolname", schoolName);
                                keyValues.Add("address1", objSchoolInfo.Address);
                                keyValues.Add("city", objSchoolInfo.City);
                                keyValues.Add("country", objSchoolInfo.Country);

                                keyValues.Add("pobox", objSchoolInfo.POBox);
                                keyValues.Add("phoneno", objSchoolInfo.Phone);
                                keyValues.Add("faxno", objSchoolInfo.Fax);
                                keyValues.Add("emailid", objSchoolInfo.EmailId);
                                keyValues.Add("website", objSchoolInfo.Website);

                                keyValues.Add("paymentdate", _paydate.Split(new char[0])[0] ?? "");
                                keyValues.Add("payfrom", dateFromString ?? "");
                                keyValues.Add("payto", dateToString ?? "");
                                keyValues.Add("employeeid", EmpInfo.EmployeeAlternativeID);
                                keyValues.Add("department", _department);
                                keyValues.Add("bankname", _bank);
                                keyValues.Add("inservicesince", (EmpInfo.HireDate != "") ? DateTime.ParseExact(EmpInfo.HireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd MMMM, yyyy") : "");
                                keyValues.Add("employeename", _name);

                                try
                                {
                                    keyValues.Add("empposition", EmpInfo.PositionTitle);
                                }
                                catch
                                {
                                    keyValues.Add("empposition", "");
                                }
                                keyValues.Add("accountnumber", _accountnumber);
                                keyValues.Add("remainingoffdays", "");
                                //keyValues.Add("monthname", _cyclename.Split(new char[0])[0] ?? ""); _datefrom
                                keyValues.Add("monthname", Convert.ToDateTime(_dateto).ToString("MMMM"));
                                keyValues.Add("YearName", Convert.ToDateTime(_dateto).ToString("yyyy"));
                                //Commented For Not To Print 
                                keyValues.Add("present", attendance[1].ToString());
                                keyValues.Add("absent", attendance[2].ToString());
                                keyValues.Add("lateinandearlyout", attendance[8].ToString());
                                keyValues.Add("offdays", attendance[7].ToString());
                                keyValues.Add("extradays", attendance[9].ToString());
                                keyValues.Add("holidays", attendance[5].ToString());


                                //Totals
                                try
                                {
                                    if (_Summary.Tables.Count > 0)
                                    {
                                        if (_Summary.Tables[0].Rows.Count > 0)
                                        {
                                            keyValues.Add("remtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["salaryTotal"]).ToString(AmountFormat));
                                            keyValues.Add("addtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["additionTotal"]).ToString(AmountFormat));
                                            keyValues.Add("dedtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["deductionTotal"]).ToString(AmountFormat));
                                            keyValues.Add("netsalary", (string.Format("{0:n}", Convert.ToDouble(Convert.ToDouble(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat)))) + " " + DataAccess.GeneralDB.CommonDB.DecimalToWord(Convert.ToDecimal(Convert.ToDecimal(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat))));
                                            //keyValues.Add("netsalary", Convert.ToDouble(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat));
                                        }
                                        else
                                        {
                                            keyValues.Add("remtotal", AmountFormat);
                                            keyValues.Add("addtotal", AmountFormat);
                                            keyValues.Add("dedtotal", AmountFormat);
                                            keyValues.Add("netsalary", AmountFormat);
                                        }

                                    }
                                }


                                catch (Exception ex)
                                {
                                    logger.Error(ex.Message + ": " + ex.StackTrace);
                                }

                                if (DeductionCount == true)
                                {
                                    for (int ii = 1; ii <= 5; ii++)
                                    {
                                        keyValues.Add("Ded" + ii + "name", "");
                                        keyValues.Add("Ded" + ii + "no", "");
                                    }
                                }

                                if (SalaryCount == true)
                                {
                                    for (int ii = 1; ii <= 5; ii++)
                                    {
                                        keyValues.Add("Rem" + ii + "name", "");
                                        keyValues.Add("Rem" + ii + "no", "");
                                    }
                                }

                                if (AdditionCount == true)
                                {
                                    for (int ii = 1; ii <= 5; ii++)
                                    {
                                        keyValues.Add("Add" + ii + "name", "");
                                        keyValues.Add("Add" + ii + "no", "");
                                    }
                                }


                                FileStream output = new FileStream(fileName, FileMode.Create);
                                if (templateID == 3)
                                {
                                    iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenratepaySlipPDFTemplateThree(output, keyValues, _Salary, _Addition, _Deduction, templateID, isSundriesInclude, SundryModelList.Where(x => x.EmployeeID == logModel.employeeID).ToList(), AmountFormat);
                                }
                                else
                                {
                                    iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenratepaySlipPDF(output, keyValues, _Salary, _Addition, _Deduction, templateID);
                                }


                                _Salary.Dispose();
                                _Addition.Dispose();
                                _Deduction.Dispose();
                                _Summary.Dispose();
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message + ": " + ex.StackTrace);
                            result = false;
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ": " + ex.StackTrace);
                    result = false;
                    //return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            #endregion
            #region DownloadPaySlip
            if (listfileName.Count() > 0)
            {
                if (listfileName.Count() > 1)
                {
                    string destinationZip = "";
                    string destinationzipDownload = "";
                    GenrateZip("~/Downloads/PaySlip/", "~/Downloads/PaySlipZip/", listfileName, arryCycleId, fEmployeeID, fEmployeeName, fCycleName, out destinationzipDownload, out destinationZip);
                    return File(destinationzipDownload, "application/zip", destinationZip);
                }
                else if (listfileName.Count() == 1)
                {
                    string destinationZip = "";
                    string destinationzipDownload = "";
                    if (arryCycleId.Count() != 1)
                    {
                        GenrateZip("~/Downloads/PaySlip/", "~/Downloads/PaySlipZip/", listfileName, arryCycleId, fEmployeeID, fEmployeeName, fCycleName, out destinationzipDownload, out destinationZip);
                        return File(destinationzipDownload, "application/zip", destinationZip);
                    }
                    else
                    {
                        return File(listfileName.FirstOrDefault(), System.Net.Mime.MediaTypeNames.Application.Octet, "PaySlip-" + fEmployeeID + "-" + fEmployeeName + "-" + fCycleName + ".pdf");//
                    }
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            #endregion

        }

        public ActionResult SendMailExportPayslipToPdf(string CycleIDArray, int PayslipTypeId, string WhereID, bool isSundriesInclude)
        {
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/PaySlipSendMail/");
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            string[] arryCycleId = CycleIDArray.Split(',');
            List<string> listfileName = new List<string>();
            string fEmployeeID = "";
            string fEmployeeName = "";
            string fCycleName = "";

            string fileName = "";
            int templateID = 1;
            templateID = new PayrollDB().GetPayslipTemplateID();
            #region Create Data Dictionary
            foreach (var CycleID in arryCycleId)
            {
                Employee employee;
                EmployeeDB employeedb = new EmployeeDB();
                PositionDB positiondb = new PositionDB();
                SendMailDB sendMailDb = new SendMailDB();
                HR_PaySundriesDB sundryDb = new HR_PaySundriesDB();
                DataSet _PayslipData, _teacherList, _Salary, _Addition, _Deduction, _Summary, _PayslipSummary;
                int TransMode = PayslipTypeId;
                try
                {
                    PayrollDB _Payrollbusiness = new PayrollDB();
                    List<DailyAttendanceViewModel> attendanceList = new List<DailyAttendanceViewModel>();
                    List<HR_PaySundriesModel> SundryModelList = new List<HR_PaySundriesModel>();
                    PayCycleDB objPayCycleDB = new PayCycleDB();
                    int[] attendance = new int[12];
                    DailyAttendanceViewDB _dailyAttendance = new DailyAttendanceViewDB();
                    PayCycleModel PayCycle = objPayCycleDB.GetPayCycleById(Convert.ToInt32(CycleID));
                    DateTime DateFrom = DateTime.ParseExact(PayCycle.DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime DateTo = DateTime.ParseExact(PayCycle.DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (templateID == 1)
                    {
                        attendanceList = _dailyAttendance.GetDailyAttendanceViewList(DateFrom.ToString("yyyy-MM-dd"), DateTo.ToString("yyyy-MM-dd"), null);
                    }
                    if (isSundriesInclude)
                    {
                        SundryModelList = sundryDb.GetPaySundriesByDate(DateFrom, DateTo);
                    }
                    _PayslipData = _Payrollbusiness.GetPaySlipData_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _teacherList = _Payrollbusiness.GetTeachers_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    _PayslipSummary = _Payrollbusiness.GetPaySlipSummary_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
                    string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";

                    SchoolInformationDB objSchoolInformationDB = new SchoolInformationDB();
                    SchoolInformation objSchoolInfo = objSchoolInformationDB.GetBasicSchoolInformation();
                    List<EmployeeInfo> lstEmployeeInfo = _Payrollbusiness.GetPaySlipEmployeeInfo();
                    DeleteFileByCycleId(CycleID);
                    #region GenratePaySlip
                    for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
                    {
                        EmployeeInfo EmpInfo = lstEmployeeInfo.Where(x => x.EmployeeID == Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"])).First();
                        SendMailLogModel logModel = new SendMailLogModel();
                        logModel.employeeID = Convert.ToInt32(_teacherList.Tables[0].Rows[i]["employeeid"]);
                        logModel.cycleId = Convert.ToInt32(CycleID);
                        logModel.employeeEmailId = EmpInfo.EmpWorkEmail;
                        logModel.paySendEmailLogID = 0;
                        logModel.payslipLink = "";
                        logModel.sendFlag = false;
                        var opLogData = sendMailDb.SaveAndUpdateModel(logModel, 1);

                        bool SalaryCount = false;
                        bool AdditionCount = false;
                        bool DeductionCount = false;
                        _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";
                        _Salary = new DataSet();
                        _Addition = new DataSet();
                        _Deduction = new DataSet();
                        _Summary = new DataSet();
                        _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
                        _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
                        _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
                        _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));
                        if (_Salary.Tables.Count > 0)
                        {
                            if (_Salary.Tables.Count > 0)
                            {
                                _employeeid = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                fEmployeeID = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                                _name = _Salary.Tables[0].Rows[0]["name"].ToString();
                                fEmployeeName = _Salary.Tables[0].Rows[0]["name"].ToString();
                                _department = _Salary.Tables[0].Rows[0]["department"].ToString();
                                _accountnumber = _Salary.Tables[0].Rows[0]["accountnumber"].ToString();
                                _bank = _Salary.Tables[0].Rows[0]["bank"].ToString();
                            }
                            else
                            {
                                _employeeid = "";
                                _name = "";
                                _department = "";
                                _accountnumber = "";
                                _bank = "";
                            }

                            if (_Summary.Tables.Count > 0)
                            {
                                _cyclename = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                fCycleName = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                                _paydate = _Summary.Tables[0].Rows[0]["paydate"].ToString();
                                _datefrom = _Summary.Tables[0].Rows[0]["datefrom"].ToString();
                                _dateto = _Summary.Tables[0].Rows[0]["dateto"].ToString();
                            }
                            else
                            {
                                _cyclename = "";
                                _paydate = "";
                                _datefrom = "";
                                _dateto = "";
                            }
                            string dateFromString = _datefrom.Split(new char[0])[0];
                            string dateToString = _dateto.Split(new char[0])[0];
                            DateTime dateFrom = Convert.ToDateTime(dateFromString);
                            DateTime dateTo = Convert.ToDateTime(dateToString);

                            int? employeeId = null;

                            if (!string.IsNullOrEmpty(_employeeid))
                                employeeId = Convert.ToInt32(_employeeid);

                            List<DailyAttendanceViewModel> employeeAttendanceList = new List<DailyAttendanceViewModel>();
                            employeeAttendanceList = attendanceList.Where(x => x.HRID == employeeId).ToList();
                            for (int j = 0; j <= 11; j++)
                            {
                                attendance[j] = employeeAttendanceList.Where(x => x.StatusID == j).Count();
                            }
                            _name = DataAccess.GeneralDB.CommonDB.RemoveSpecialCharacter(_name, "/,',\\");
                            string destinationFileFinal = ("PaySlip-" + _employeeid + "-" + _name + "-" + _cyclename + "-" + opLogData.InsertedRowId + "-" + DateTime.Now.ToString("ddMMyyyyhhmmsstt") + ".pdf").Replace(" ", "").Replace("  ", "");
                            fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PaySlipSendMail/" + destinationFileFinal);
                            logModel.paySendEmailLogID = opLogData.InsertedRowId;
                            logModel.payslipLink = fileName;
                            sendMailDb.SaveAndUpdateModel(logModel, 2);
                            listfileName.Add(fileName);
                            //School Information
                            string schoolName = objSchoolInfo.SchoolName_1;
                            if (objSchoolInfo.CampusName_1 != "")
                                schoolName += " - " + objSchoolInfo.CampusName_1;
                            //create key value pair, key represents words to be replace and 
                            //values represent values in document in place of keys.
                            Dictionary<string, string> keyValues = new Dictionary<string, string>();
                            keyValues.Add("schoolname", schoolName);
                            keyValues.Add("address1", objSchoolInfo.Address);
                            keyValues.Add("city", objSchoolInfo.City);
                            keyValues.Add("country", objSchoolInfo.Country);

                            keyValues.Add("pobox", objSchoolInfo.POBox);
                            keyValues.Add("phoneno", objSchoolInfo.Phone);
                            keyValues.Add("faxno", objSchoolInfo.Fax);
                            keyValues.Add("emailid", objSchoolInfo.EmailId);
                            keyValues.Add("website", objSchoolInfo.Website);

                            keyValues.Add("paymentdate", _paydate.Split(new char[0])[0] ?? "");
                            keyValues.Add("payfrom", dateFromString ?? "");
                            keyValues.Add("payto", dateToString ?? "");
                            keyValues.Add("employeeid", EmpInfo.EmployeeAlternativeID);
                            keyValues.Add("department", _department);
                            keyValues.Add("bankname", _bank);
                            keyValues.Add("inservicesince", (EmpInfo.HireDate != "") ? DateTime.ParseExact(EmpInfo.HireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd MMMM, yyyy") : "");
                            keyValues.Add("employeename", _name);
                            keyValues.Add("empposition", EmpInfo.PositionTitle);
                            keyValues.Add("accountnumber", _accountnumber);
                            keyValues.Add("remainingoffdays", "");
                            //keyValues.Add("monthname", _cyclename.Split(new char[0])[0] ?? ""); _datefrom
                            keyValues.Add("monthname", Convert.ToDateTime(_dateto).ToString("MMMM"));
                            keyValues.Add("YearName", Convert.ToDateTime(_dateto).ToString("yyyy"));
                            //Commented For Not To Print 
                            keyValues.Add("present", attendance[1].ToString());
                            keyValues.Add("absent", attendance[2].ToString());
                            keyValues.Add("lateinandearlyout", attendance[8].ToString());
                            keyValues.Add("offdays", attendance[7].ToString());
                            keyValues.Add("extradays", attendance[9].ToString());
                            keyValues.Add("holidays", attendance[5].ToString());


                            //Totals
                            try
                            {
                                if (_Summary.Tables.Count > 0)
                                {
                                    if (_Summary.Tables[0].Rows.Count > 0)
                                    {
                                        keyValues.Add("remtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["salaryTotal"]).ToString(AmountFormat));
                                        keyValues.Add("addtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["additionTotal"]).ToString(AmountFormat));
                                        keyValues.Add("dedtotal", Convert.ToDouble(_Summary.Tables[0].Rows[0]["deductionTotal"]).ToString(AmountFormat));
                                        keyValues.Add("netsalary", (string.Format("{0:n}", Convert.ToDouble(Convert.ToDouble(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat)))) + " " + DataAccess.GeneralDB.CommonDB.DecimalToWord(Convert.ToDecimal(Convert.ToDecimal(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat))));
                                        //keyValues.Add("netsalary", Convert.ToDouble(_Summary.Tables[0].Rows[0]["net"]).ToString(AmountFormat));
                                    }
                                    else
                                    {
                                        keyValues.Add("remtotal", AmountFormat);
                                        keyValues.Add("addtotal", AmountFormat);
                                        keyValues.Add("dedtotal", AmountFormat);
                                        keyValues.Add("netsalary", AmountFormat);
                                    }

                                }
                            }


                            catch (Exception ex)
                            {
                                logger.Error(ex.Message + ": " + ex.StackTrace);
                            }

                            if (DeductionCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Ded" + ii + "name", "");
                                    keyValues.Add("Ded" + ii + "no", "");
                                }
                            }

                            if (SalaryCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Rem" + ii + "name", "");
                                    keyValues.Add("Rem" + ii + "no", "");
                                }
                            }

                            if (AdditionCount == true)
                            {
                                for (int ii = 1; ii <= 5; ii++)
                                {
                                    keyValues.Add("Add" + ii + "name", "");
                                    keyValues.Add("Add" + ii + "no", "");
                                }
                            }


                            FileStream output = new FileStream(fileName, FileMode.Create);
                            if (templateID == 3)
                            {
                                iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenratepaySlipPDFTemplateThree(output, keyValues, _Salary, _Addition, _Deduction, templateID, isSundriesInclude, SundryModelList.Where(x => x.EmployeeID == logModel.employeeID).ToList(), AmountFormat);
                            }
                            else
                            {
                                iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenratepaySlipPDF(output, keyValues, _Salary, _Addition, _Deduction, templateID);
                            }


                            _Salary.Dispose();
                            _Addition.Dispose();
                            _Deduction.Dispose();
                            _Summary.Dispose();
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ": " + ex.StackTrace);
                }
            }
            #endregion
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePayrollTemplateSetting(int payrollTemplateId, int jvsTemplateId, string AccountCode)
        {
            PayrollDB payrollDB = new PayrollDB();
            bool parollSetting = payrollDB.UpdatePayrollTemplateSetting(payrollTemplateId);
            bool jvsSetting = payrollDB.UpdateJvsTemplateSetting(jvsTemplateId, AccountCode);
            return Json(new { parollSetting = parollSetting, jvsSetting = jvsSetting }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ValidateUser()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int userId = objUserContextViewModel.UserId;
            ViewBag.UserId = userId;
            LoginViewModel objLoginView = new LoginViewModel();
            return PartialView(objLoginView);
        }

        public ActionResult RetrieveFinalRun(string CycleId)
        {
            int[] k = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(CycleId);
            OperationDetails op = new OperationDetails();
            PayrollDB objPayrollDB = new PayrollDB();
            op = objPayrollDB.RetrieveFinalRun(Convert.ToInt32(k[0]));
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult confirmationOnSendMail(string CycleIds)
        {
            SendMailDB sendMailDB = new SendMailDB();
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            int MailCount = sendMailDB.GetCountOfMailByCycleId(CycleIds);
            int PaySalaryCount = paySalaryDB.GetCountOfPaySalaryByCycleId(CycleIds);
            if (MailCount == PaySalaryCount)
            {
                return Json(new { isContinue = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isContinue = true }, JsonRequestBehavior.AllowGet);
            }

        }

        public static void DeleteFileByCycleId(string CycleId)
        {
            int cycleId = Convert.ToInt32(CycleId);
            try
            {
                SendMailDB sendMailDB = new SendMailDB();
                DataSet _sentEmailList = sendMailDB.GetAllSendEmailListByCycleId(cycleId);

                if (_sentEmailList.Tables.Count > 0 && _sentEmailList.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < _sentEmailList.Tables[0].Rows.Count; i++)
                    {
                        string fileName = Convert.ToString(_sentEmailList.Tables[0].Rows[i]["PaySlipLink"]);
                        FileUploadHelper.DeleteFile(fileName);
                    }
                }
            }
            catch { }
        }
    }
}