﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class LeaveAccrualPostingController : FormsController
    {
        // GET: LeaveAccrualPosting
        public ActionResult Index()
        {
            return View();
        }

      
        public ActionResult Save(string accrualDate)
        {
            return Json(new LeaveBalanceManagerDB().SaveLeaveAccrual(accrualDate), JsonRequestBehavior.AllowGet);           
        }
    }
}