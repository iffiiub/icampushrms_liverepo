﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;

namespace HRMS.Web.Controllers
{
    public class LeaveTypeController : BaseController
    {

        public ActionResult LeaveReason()
        {


            return View();
        }
        public ActionResult GetLeaveReasonList()
        {
            DBHelper objDBHelper = new DBHelper();
            List<LeaveReason> objLeaveTypeModelList = new List<LeaveReason>();
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            objLeaveTypeModelList = objLeaveTypeDB.GetLeaveReasonListWithPaging();
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objLeaveTypeModelList
                          select new
                          {

                              ID = item.ID,
                              LeaveReason_1 = item.LeaveReason_1,
                              LeaveReason_2 = item.LeaveReason_2,
                              LeaveReason_3 = item.LeaveReason_3,
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.ID.ToString() + ")'><span class='fa fa-pencil'></span></a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onClick='DeleteChannel(" + item.ID.ToString() + ")'><span class='fa fa-times'></span></a>"

                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEditLeaveReason(int ID)
        {
            LeaveReason ObjLeaveReson = new LeaveReason();
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            if (ID != -1)
            {
                ObjLeaveReson = objLeaveTypeDB.GetLeaveReasonList(ID, 1).FirstOrDefault();
            }
            else {
                ObjLeaveReson.ID = ID;
            }
            return View(ObjLeaveReson);

        }

        public JsonResult SaveLeaveReason(LeaveReason ObjLeaveReason)
        {
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            objLeaveTypeDB.InsertUpdateLeaveReason(ObjLeaveReason);
            return Json(0, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteLeaveReason(int ID)
        {
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            objLeaveTypeDB.DeleteLeaveReason(ID);
            return Json(0, JsonRequestBehavior.AllowGet);
        }
   
        public JsonResult EditLeaveType(LeaveTypeModel objLeaveTypeModel)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                DBHelper objDBHelper = new DBHelper();
                LeaveTypeDB objLeaveTypeModelDB = new LeaveTypeDB();



                // Add class
                result = "success";
                resultMessage = "Leave Type Details Updated Successfully.";


            }
            catch (Exception)
            {
                result = "error";
                resultMessage = "Error occured while adding Leave Type Details.";
            }

            //return Redirect("Index");
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GEtLeaveTypeByID(int id)
        {
            LeaveTypeDB objLeaveTypeModelDB = new LeaveTypeDB();
            LeaveTypeModel objLeaveTypeModel = new LeaveTypeModel();
            objLeaveTypeModel = objLeaveTypeModelDB.GetLeaveTypeById(id);

            return Json(new { objLeaveTypeModel = objLeaveTypeModel }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewLeaveType(int id)
        {
            LeaveTypeDB objLeaveTypeModelDB = new LeaveTypeDB();
            LeaveTypeModel objLeaveTypeModel = new LeaveTypeModel();
            objLeaveTypeModel = objLeaveTypeModelDB.GetLeaveTypeById(id);
            return View(objLeaveTypeModel);
        }
       
        public void GetDropDowns()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];


            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> ObjSelectedList = null;


            GenderDB ObjGenderDB = new GenderDB();
            ReligionDB ObjReligionDB = new ReligionDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            MaritialStatusDB objMaritialStatusDB = new MaritialStatusDB();

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "2" });
            foreach (var m in ObjGenderDB.GetAllGenders())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.GenderName_1, Value = m.GenderID.ToString() });
            }
            ViewBag.GenderList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjReligionDB.GetAllReligion())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.ReligionName_1, Value = m.ReligionID.ToString() });

            }
            ViewBag.ReligionList = ObjSelectedList;


            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjNationalityDB.getAllNationalities())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.NationalityName_1, Value = m.NationalityID.ToString() });

            }
            ViewBag.NationalityList = ObjSelectedList;

            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in objMaritialStatusDB.getAllMaritialStatus())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MaritalStatusName_1, Value = m.MaritalStatusID.ToString() });

            }
            ViewBag.MaritalStatusList = ObjSelectedList;




        }

        #region commentted  Code
        //public ActionResult Index()
        //{

        //    GetDropDowns();
        //    return View();
        //}

        //public ActionResult Edit(int id)
        //{
        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];

        //    HRMS.Entities.LeaveTypeModel objLeaveType = new HRMS.Entities.LeaveTypeModel();
        //    int CompanyId = objUserContextViewModel.CompanyId;


        //    GenderDB ObjGenderDB = new GenderDB();
        //    ReligionDB ObjReligionDB = new ReligionDB();
        //    NationalityDB ObjNationalityDB = new NationalityDB();
        //    MaritialStatusDB objMaritalStatusDb = new MaritialStatusDB();
        //    LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();

        //    List<GenderModel> lstGender = ObjGenderDB.GetAllGenders();
        //    GenderModel naGender = new GenderModel();
        //    naGender.GenderID = 2;
        //    naGender.GenderName_1 = "N/A";
        //    lstGender.Insert(0, naGender);
        //    SelectList GenderList = new SelectList(lstGender, "GenderID", "GenderName_1");

        //    ViewBag.GenderList = GenderList;
        //    ViewBag.ReligionList = new SelectList(ObjReligionDB.GetAllReligion(), "ReligionID", "ReligionName_1");
        //    ViewBag.NationalityList = new SelectList(ObjNationalityDB.getAllNationalities(), "NationalityID", "NationalityName_1");
        //    ViewBag.MaritialStatusList = new SelectList(objMaritalStatusDb.getAllMaritialStatus(), "MaritalStatusId", "MaritalStatusName_1");


        //    if (id != 0)
        //    {
        //        objLeaveType = objLeaveTypeDB.GetLeaveTypeById(id);
        //        ViewBag.Edit = 1;
        //        ViewBag.SelectedGender = objLeaveType.GenderID;
        //        ViewBag.SelectedNationality = objLeaveType.NationalityID;
        //        ViewBag.SelectedReligionId = objLeaveType.ReligionID;
        //        ViewBag.SelectedMaritalId = objLeaveType.MaritalStatusId;

        //        ViewBag.SelectedIsCompensate = objLeaveType.isCompensate;
        //        ViewBag.SelectedAnnualLeave = objLeaveType.AnnualLeave;
        //        ViewBag.SelectedLifetimeLeave = objLeaveType.LifetimeLeave;
        //        ViewBag.SelectedMonthlySplit = objLeaveType.MonthlySplit;

        //        ViewBag.ApplicableAfterType = objLeaveType.ApplicableAfterTypeID;


        //    }

        //    return View(objLeaveType);
        //}

        //[HttpPost]
        //public JsonResult AddLeaveType(LeaveTypeModel objLeaveTypeModel)
        //{
        //    string result = "";
        //    string resultMessage = "";
        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    objLeaveTypeModel.CompanyId = objUserContextViewModel.CompanyId;
        //    try
        //    {

        //        LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();

        //        if (objLeaveTypeModel.LeaveTypeId == 0)
        //        {
        //            objLeaveTypeDB.InsertLeaveType(objLeaveTypeModel);
        //            result = "success";
        //            resultMessage = "Leave Type Added Successfully.";
        //        }
        //        else if (objLeaveTypeModel.LeaveTypeId != 0)
        //        {
        //            objLeaveTypeDB.UpdateLeaveType(objLeaveTypeModel);
        //            result = "success";
        //            resultMessage = "Leave Type Updated Successfully.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = "error";
        //        resultMessage = "Error occured while adding Leave Type.";
        //    }

        //    return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        //}

        #endregion
    }
}