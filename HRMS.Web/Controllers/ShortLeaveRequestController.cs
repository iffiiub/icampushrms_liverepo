﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class ShortLeaveRequestController : BaseController
    {
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<Employee> objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            if (IsPersonalUser(objUserContextViewModel.UserId))
                ViewBag.IsPersonalOnly = true;
            else
            {
                ViewBag.IsPersonalOnly = false;
                ObjSelectedList.Add(new SelectListItem { Text = "All Employees", Value = "0" });
            }
            foreach (var m in objEmployeeList.Where(m => m.IsActive == 1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });

            }
            ViewBag.EmployeeList = ObjSelectedList;
            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }
            return View();
        }
        public ActionResult GetShortLeaveRequestList(int empid = 0, string FromDate = "", string ToDate = "")
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            //-------------Data Objects--------------------
            List<ShortLeaveRequestModel> objShortLeaveRequestModelList = new List<ShortLeaveRequestModel>();
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objShortLeaveRequestModelList = objShortLeaveRequestDB.GetShortLeaveRequestList(empid, objUserContextViewModel.UserId, FromDate, ToDate);

            bool isPersonalUser = IsPersonalUser(objUserContextViewModel.UserId);
            int userId = isPersonalUser ? objUserContextViewModel.UserId : empid;
            bool isHrUser = !isPersonalUser ? IsHRUser(objUserContextViewModel.UserId) : false;
            bool isAdminUser = objUserContextViewModel.UserId == 0;

            var vList = new object();
            vList = new
            {
                aaData = (from item in objShortLeaveRequestModelList
                          select new
                          {
                              Action = "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewShortLeaveRequest(" + item.ShortLeaveID.ToString() + "," + item.EmployeeID.ToString() + ")' title='Details' ><i class='fa fa-eye'></i> </a>" +
                               (isAdminUser || (item.ApproverId == objUserContextViewModel.UserId && string.IsNullOrEmpty(item.ActualLeaveTime) && string.IsNullOrEmpty(item.ActualReturnTime)) ? " <a class='btn btn-warning btn-rounded btn-condensed btn-sm' onclick='ReviewShortLeaveRequest(" + item.ShortLeaveID.ToString() + ")' title='Review' ><i class='fa fa-check'></i> </a>" : "") +
                               (isAdminUser || (isHrUser && item.ApproverId != userId && item.ApprovalStatus == ApprovalStatus.Approved.ToString()) ? " <a class='btn btn-primary btn-rounded btn-condensed btn-sm' onclick='UpdateActualTimes(" + item.ShortLeaveID.ToString() + ")' title='Add/Edit Actual Times' ><i class='fa fa-clock-o'></i> </a>" : "") +
                               (isAdminUser || ((isHrUser || item.RequesterEmployeeID == objUserContextViewModel.UserId) && (item.ApprovalStatus == ApprovalStatus.Pending.ToString() || item.ApprovalStatus == ApprovalStatus.Rejected.ToString())) ? " <a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditShortLeaveRequest(" + item.ShortLeaveID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>" : "") +
                               (isAdminUser || ((isHrUser || item.RequesterEmployeeID == objUserContextViewModel.UserId) && (item.ApprovalStatus == ApprovalStatus.Pending.ToString() || item.ApprovalStatus == ApprovalStatus.Rejected.ToString())) ? " <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteShortLeaveRequest(" + item.ShortLeaveID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>" : ""),
                              OrganizationName = item.OrganizationName,
                              EmployeeName = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(item.EmployeeID).LastName,
                              EmpID = item.EmployeeAlternativeID,
                              LeaveDate = item.LeaveDate,
                              LeaveTime = item.LeaveTime,
                              ReturnTime = item.ReturnTime,
                              ActualLeaveTime = item.ActualLeaveTime,
                              ActualReturnTime = item.ActualReturnTime,
                              LateMinutes = item.LateMinutes,
                              EarlyMinutes = item.EarlyMinutes,
                              Comments = item.Comments,
                              ApprovalStatus = item.ApprovalStatus,
                              Status = string.IsNullOrEmpty(item.Status) ? "N/A" : item.Status
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public ActionResult AddShortLeaveRequest()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ShortLeaveRequestModel model = new ShortLeaveRequestModel();
            if (IsPersonalUser(objUserContextViewModel.UserId))
            {
                ViewBag.IsPersonalOnly = true;
                model.EmployeeID = model.RequesterEmployeeID = objUserContextViewModel.UserId;
            }
            else
            {
                ViewBag.IsPersonalOnly = false;
                ObjSelectedList.Add(new SelectListItem { Text = "All Employees", Value = "0" });
            }
            foreach (var m in objEmployeeList.Where(m => m.IsActive == 1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });

            }
            ViewBag.EmployeeList = ObjSelectedList;
            return View(model);
        }
        [HttpPost]
        public JsonResult AddShortLeaveRequest(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            objShortLeaveRequestModel.UserId = ((UserContextViewModel)Session["userContext"]).UserId;
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ShortLeaveSettingsModel objShortLeaveSettingsModel = objShortLeaveRequestDB.GetShortLeaveSettings();
            if (objShortLeaveSettingsModel.ApproverMasterId != 0)
            {
                int approverId = objShortLeaveRequestDB.GetShortLeaveApproverEmployeeId(objUserContextViewModel.UserId);
                if (approverId != 0)
                {
                    op = objShortLeaveRequestDB.AddShortLeave(objShortLeaveRequestModel);

                    //Send Email
                    CommonHelper.CommonHelper objCommonHelper = new CommonHelper.CommonHelper();

                    string emailID = "";
                    if (approverId != 0)
                    {
                        emailID = new EmployeeDB().GetEmployee(approverId).EmailId;
                    }
                    EmailSettings objEmailSettings = objShortLeaveRequestDB.GetShortLeaveRequestEmailBody("NewRequest", op.InsertedRowId, approverId);
                    objCommonHelper.SendMail(emailID, objEmailSettings.EmailSubject, objEmailSettings.EmailBody);
                }
                else
                {
                    op.Message = "Approver as employee is not defined.";
                }
            }
            else
            {
                op.Message = "Approver is not selected.";
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditShortLeaveRequest(int id)
        {
            int empid = 0;
            if (Session["EmployeeListID"] != null)
            {
                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(empid);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            ShortLeaveRequestModel objShortLeaveRequestModel = new ShortLeaveRequestModel();
            objShortLeaveRequestModel = objShortLeaveRequestDB.GetShortLeaveListById(id);
            return View(objShortLeaveRequestModel);
        }
        [HttpPost]
        public JsonResult EditShortLeaveRequest(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            if (objShortLeaveRequestModel.ApprovalStatus == ApprovalStatus.Pending.ToString() || objShortLeaveRequestModel.ApprovalStatus == ApprovalStatus.Rejected.ToString())
            {
                ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
                objShortLeaveRequestModel.UserId = ((UserContextViewModel)Session["userContext"]).UserId;
                op = objShortLeaveRequestDB.EditShortLeave(objShortLeaveRequestModel);
            }
            else
            {
                op.CssClass = "error";
                op.Message = "Error : You are not allowed to edit short leave request record.";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteShortLeaveRequest(int id)
        {
            OperationDetails op = new OperationDetails();
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            op = objShortLeaveRequestDB.DeleteShortLeaveRequest(id, ((UserContextViewModel)Session["userContext"]).UserId);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewShortLeaveRequest(int id, int EmpId)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(EmpId);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            ShortLeaveRequestModel objShortLeaveRequestModel = new ShortLeaveRequestModel();
            objShortLeaveRequestModel = objShortLeaveRequestDB.GetShortLeaveListById(id);
            return View(objShortLeaveRequestModel);
        }

        public ActionResult UpdateActualTimes(int id)
        {
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            ShortLeaveRequestModel objShortLeaveRequestModel = new ShortLeaveRequestModel();
            objShortLeaveRequestModel = objShortLeaveRequestDB.GetShortLeaveListById(id);
            ViewBag.EmployeePunches = objShortLeaveRequestDB.GetEmployeePunches(objShortLeaveRequestModel.EmployeeID, objShortLeaveRequestModel.LeaveDate);
            return View(objShortLeaveRequestModel);
        }
        [HttpPost]
        public JsonResult UpdateActualTimes(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            if (objShortLeaveRequestModel.ApprovalStatus == ApprovalStatus.Approved.ToString())
            {
                ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
                op = objShortLeaveRequestDB.UpdateShortLeaveActualTimes(objShortLeaveRequestModel);
            }
            else
            {
                op.CssClass = "error";
                op.Message = "Error : You are not allowed to update actual time of short leave request record.";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReviewShortLeaveRequest(int id)
        {
            int empid = 0;
            if (Session["EmployeeListID"] != null)
            {
                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(empid);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            ShortLeaveRequestModel objShortLeaveRequestModel = new ShortLeaveRequestModel();
            objShortLeaveRequestModel = objShortLeaveRequestDB.GetShortLeaveListById(id);
            ViewBag.ApprovalStatusList = GetApprovalStatusList();
            return View(objShortLeaveRequestModel);
        }
        [HttpPost]
        public JsonResult ReviewShortLeaveRequest(ShortLeaveRequestModel objShortLeaveRequestModel)
        {
            OperationDetails op = new OperationDetails();
            if (string.IsNullOrEmpty(objShortLeaveRequestModel.ActualLeaveTime) && string.IsNullOrEmpty(objShortLeaveRequestModel.ActualReturnTime))
            {
                ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
                op = objShortLeaveRequestDB.ReviewShortLeaveRequest(objShortLeaveRequestModel);

                //Send Email
                if (op.Success)
                {
                    CommonHelper.CommonHelper objCommonHelper = new CommonHelper.CommonHelper();
                    UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    int requesterId = objShortLeaveRequestModel.EmployeeID;
                    int approverId = objShortLeaveRequestDB.GetShortLeaveApproverEmployeeId(objUserContextViewModel.UserId);
                    string emailID = new EmployeeDB().GetEmployee(requesterId).EmailId;
                    if (objShortLeaveRequestModel.ApprovalStatus == "Approved")
                    {
                        ShortLeaveSettingsModel objShortLeaveSettingsModel = objShortLeaveRequestDB.GetShortLeaveSettings();
                        string CC = "";
                        List<Employee> objEmployeeList = objShortLeaveRequestDB.GetShortLeaveEmailList(objShortLeaveSettingsModel.EmployeeIdsToNotifyUponApproval);
                        foreach (var item in objEmployeeList)
                        {
                            CC = CC + "," + item.EmailId;
                        }
                        CC = CC.Substring(1);
                        EmailSettings objEmailSettings = objShortLeaveRequestDB.GetShortLeaveRequestEmailBody("RequestApproved", objShortLeaveRequestModel.ShortLeaveID, approverId);
                        objCommonHelper.SendMail(emailID, objEmailSettings.EmailSubject, objEmailSettings.EmailBody, CC);
                    }
                    else
                    {
                        EmailSettings objEmailSettings = objShortLeaveRequestDB.GetShortLeaveRequestEmailBody("RequestRejected", objShortLeaveRequestModel.ShortLeaveID, approverId);
                        objCommonHelper.SendMail(emailID, objEmailSettings.EmailSubject, objEmailSettings.EmailBody);
                    }
                }
            }
            else
            {
                op.CssClass = "error";
                op.Message = "Error : You are not allowed to approve or reject short leave request record.";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }


        private bool IsPersonalUser(int userId)
        {
            bool isPersonalOnly = false;
            List<UserGroupModel> listUserGroupModel = new List<UserGroupModel>();
            listUserGroupModel = new UserGroupDB().GetAllUserGroupsByUserId(userId);
            int IsPersonalCount = listUserGroupModel.Where(x => x.UserListAccessID == 1).ToList().Count();
            int IsNotPersonalCount = listUserGroupModel.Where(x => x.UserListAccessID > 1).ToList().Count();
            if (IsPersonalCount > 0 && IsNotPersonalCount == 0)
            {
                isPersonalOnly = true;
            }
            return isPersonalOnly;
        }
        private bool IsHRUser(int userId)
        {
            bool isHRUser = false;
            ShortLeaveSettingsModel listUserGroupModel = new ShortLeaveSettingsModel();
            listUserGroupModel = new ShortLeaveRequestDB().GetShortLeaveSettings();
            if (!string.IsNullOrEmpty(listUserGroupModel.EmployeeIdsToNotifyUponApproval))
                isHRUser = listUserGroupModel.EmployeeIdsToNotifyUponApproval.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Any(item => Convert.ToInt32(item) == userId);
            return isHRUser;
        }

        private List<SelectListItem> GetApprovalStatusList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Approve", Value = ApprovalStatus.Approved.ToString() });
            list.Add(new SelectListItem { Text = "Reject", Value = ApprovalStatus.Rejected.ToString() });
            return list;
        }

        #region Export

        public ActionResult ExportToExcel(int empId = 0, string startDate = "", string endDate = "")
        {
            byte[] content;
            string fileName = "ShortLeave" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            System.Data.DataSet ds = objShortLeaveRequestDB.ShortLeaveExport(objUserContextViewModel.UserId, empId, startDate, endDate);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        public void ExportToPdf(int empId = 0, string startDate = "", string endDate = "")
        {
            string fileName = "ShortLeave" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ShortLeaveRequestDB objShortLeaveRequestDB = new ShortLeaveRequestDB();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            System.Data.DataSet ds = objShortLeaveRequestDB.ShortLeaveExport(objUserContextViewModel.UserId, empId, startDate, endDate);
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }

        #endregion
    }
}