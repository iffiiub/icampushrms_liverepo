﻿using HRMS.DataAccess;
using HRMS.DataAccess.PDRP;
using HRMS.Entities;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.PDRP;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Globalization;
using HRMS.Web.CommonHelper;
using HRMS.Entities.Common;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Newtonsoft.Json;
using HRMS.Entities.General;

namespace HRMS.Web.Controllers
{
    public class DevelopmentPlanController : FormsController
    {
        // GET: DevelopmentPlan
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            DevelopmentGoalModel model = new DevelopmentGoalModel();

            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB objDepartmentDB = new DepartmentDB();
            ViewBag.Company = GetCompanySelectList(); //new SelectList(new CompanyDB().GetAllCompany(), "CompanyId", "Name");
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            //objDepartmentList = objDepartmentDB.GetAllDepartment();
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");

            return View();
        }

        public ActionResult MyDevelopmentPlans()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return View();
        }
        /// <summary>
        /// Get All Employees Development Plans 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public ActionResult GetEmployeeDevelopmentPlans(int? CompanyId, int? DepartmentId)
        {
            return GetEmployeesDevelopmentPlans(CompanyId, DepartmentId, false);
        }

        /// <summary>
        /// Get All Employees Development Plans 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public ActionResult GetMyDevelopmentPlans()
        {
            return GetEmployeesDevelopmentPlans(null, null, true);
        }
        private ActionResult GetEmployeesDevelopmentPlans(int? CompanyId, int? DepartmentId, bool IsMyPlanScreen)
        {
            List<EmployeeDevelopmentPlanListModel> objEmployeeDevelopmentPlanList = new List<EmployeeDevelopmentPlanListModel>();
            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            objEmployeeDevelopmentPlanList = DevelopmentModelDB.GetEmployeeDevelopmentPlan(CompanyId, DepartmentId, objUserContextViewModel.UserId, IsMyPlanScreen);

            string PrintActivePlan = "<a href='#'>Print</a>";

            var vList = new object();
            vList = new
            {
                aaData = (from item in objEmployeeDevelopmentPlanList
                          select new
                          {
                              CompanyID = item.CompanyID,
                              DepartmentID = item.DepartmentID,
                              TotalDevelopmentPlans = item.EmployeeTotalDevelopmentPlans,
                              OracleNumber = item.OracleNumber,
                              CompanyName = item.CompanyName,
                              DepartmentName = item.DepartmentName,
                              EmployeeName = item.EmployeeName,
                              CreateDevelopmentPlan = "<a class='btn' onclick=\"return CreateDevelopmentGoal(0," + item.OracleNumber + ",'" + item.EmployeeAlternativeID + "'," + item.CompanyID + "," + item.DepartmentID + "," + item.PositionID + ");\">Create</a>",
                              ActiveDevelopmentPlan = item.EmployeeTotalActiveDevelopmentPlans > 0 ?
                                                          "<a  class='btn' onclick=\"ViewAllDevelopmentPlans(" + item.CompanyID + ",'" + item.EmployeeAlternativeID + "'," + item.OracleNumber + ",'" + item.EmployeeName + "','" + DevelopmentPlanTypes.Active + "')\" > View </a>"
                                                          : "<a  href='#' class='btn disabled' >View</a>",
                              DevelopmentPlanHistory = item.EmployeeTotalCompletedDevelopmentPlans > 0 ? "<a class='btn' onclick =\"ViewAllDevelopmentPlans(" + item.CompanyID + "," + item.OracleNumber + ",'" + item.EmployeeAlternativeID + "','" + item.EmployeeName + "','" + DevelopmentPlanTypes.Completed + "')\" >Count (" + item.EmployeeTotalCompletedDevelopmentPlans + ")</a>"
                                                               : "<a  href='#' class='btn disabled' >Count (0)</a>",
                              PrintActivePlan = PrintActivePlan,
                              EmployeeAlternativeID = item.EmployeeAlternativeID
                          }).ToArray().ToList()
            };

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };

            return result;
        }

        public ActionResult DevelopmentGoalPDRP()
        {
            var developmenGoalSessionModel = SessionManager.DevelopmentGoalModel;
            if (developmenGoalSessionModel.EmployeeID == null)
            {
                return RedirectToAction("Index");
            }
            DevelopmentGoalModel model = new DevelopmentGoalModel();
            model.EmployeeID = developmenGoalSessionModel.EmployeeID ?? 0; ;
            model.CompanyId = developmenGoalSessionModel.CompanyID ?? 0;
            return View(model);
        }

        public ActionResult DevelopmentGoalForm()
        {
            DevelopmentGoalModel model = new DevelopmentGoalModel();
            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();
            var developmenGoalSessionModel = SessionManager.DevelopmentGoalModel;

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool IsMyPlanScreen = false;

            if (developmenGoalSessionModel.EmployeeID == null)
            {
                return RedirectToAction("Index");
            }
            if (developmenGoalSessionModel.DevelopmentGoalID > 0)
            {
                model = DevelopmentModelDB.SelectDevelopmentPlan(developmenGoalSessionModel.DevelopmentGoalID.Value);
                IsMyPlanScreen = (model.EmployeeID == objUserContextViewModel.UserId);
            }
            else
            {
                model.RequesterEmployeeID = objUserContextViewModel.UserId;
                model.EmployeeID = developmenGoalSessionModel.EmployeeID ?? 0;
                model.CompanyId = developmenGoalSessionModel.CompanyID ?? 0;
                model.DPCategoryID = developmenGoalSessionModel.CategoryID ?? 0;
                model.SectionRecordID = developmenGoalSessionModel.SectionID ?? 0;
                model.SectionName = developmenGoalSessionModel.SectionName ?? string.Empty;
                model.KPICompetency = developmenGoalSessionModel.KPICompetency ?? string.Empty;
                model.AnnualAppraisalID = developmenGoalSessionModel.AnnualAppraisalID ?? 0;
                model.JobDesignationCompetencyID = developmenGoalSessionModel.JobDesignationCompetencyID ?? 0;
            }
            DevelopmentGoalSearchModel developmentGoalSearchModel = new DevelopmentGoalSearchModel();


            developmentGoalSearchModel.EmployeeID = developmenGoalSessionModel.EmployeeID ?? 0;
            developmentGoalSearchModel.CompanyId = developmenGoalSessionModel.CompanyID ?? 0;
            developmentGoalSearchModel.StatusIds = GetDevelopmentPlanIds(DevelopmentPlanTypes.Active.ToString());


            List<HRMS.Entities.TrainingCompetenciesModel> objTrainingCompetenciesModelList = new List<HRMS.Entities.TrainingCompetenciesModel>();
            objTrainingCompetenciesModelList = DevelopmentModelDB.GetAllTrainingCompetencies(model.SectionRecordID);

            ViewBag.TrainingCompetencies = new SelectList(objTrainingCompetenciesModelList, "TrainingID", "TrainingName_1");
            List<HRMS.Entities.CategoriesModel> objCategoriesList = new List<HRMS.Entities.CategoriesModel>();
            objCategoriesList = DevelopmentModelDB.GetAllCategories();
            ViewBag.Category = new SelectList(objCategoriesList, "DPCategoryId", "CategoryName_1");
            List<HRMS.Entities.FormStatusModel> objFormStatusList = new List<HRMS.Entities.FormStatusModel>();
            objFormStatusList = DevelopmentModelDB.GetAllFormStatus();
            ViewBag.StatusofDevelopmentGoal = new SelectList(objFormStatusList, "ID", "StatusName_1");
            MasterDataModel objFormMasterDataList = new MasterDataModel();
            objFormMasterDataList = DevelopmentModelDB.GetAllFormMasterData();
            ViewBag.LearningCategory = new SelectList(objFormMasterDataList.LearningCategories, "HR_DPFormMasterDataID", "OptionName_1");
            ViewBag.TrainingActivityFormat = new SelectList(objFormMasterDataList.ActivityFormat, "HR_DPFormMasterDataID", "OptionName_1");
            ViewBag.DevelopmentGoals = DevelopmentModelDB.GetAllDevelopmentPlans(developmentGoalSearchModel);
            ViewBag.IsMyPlanScreen = IsMyPlanScreen;
            var html = MasterHelper.TempView(HtmlTempHelper);
            // DevelopmentFormHTML(html.ToString());
            return View(model);
        }


        public void DevelopmentFormHTML(string HTML)
        {
            StringReader sr = new StringReader(HTML.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=HTML.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();
            }

        }
        private string GetDevelopmentPlanIds(string type)
        {
            List<int> developmentPlans = new List<int>();
            if (type == DevelopmentPlanTypes.Active.ToString() || type == string.Empty)
            {
                developmentPlans.Add((int)DevelopmentGoalStatuses.NotStarted);
                developmentPlans.Add((int)DevelopmentGoalStatuses.InProgress);
                developmentPlans.Add((int)DevelopmentGoalStatuses.Behind);
            }
            if (type == DevelopmentPlanTypes.Completed.ToString() || type == string.Empty)
            {
                developmentPlans.Add((int)DevelopmentGoalStatuses.SuccessfullyCompleted);
                developmentPlans.Add((int)DevelopmentGoalStatuses.Dropped);
            }
            return !developmentPlans.Any() ? string.Empty : string.Join(",", developmentPlans.ToList());

        }
        public ActionResult GetAllDevelopmentPlans(int CompanyId, int EmployeeId, string DevelopmentPlanType)
        {
            return GetAllDevelopmentPlans(CompanyId, EmployeeId, DevelopmentPlanType, false);
        }

        public ActionResult GetMyAllDevelopmentPlans(int CompanyId, int EmployeeId, string DevelopmentPlanType)
        {
            return GetAllDevelopmentPlans(CompanyId, EmployeeId, DevelopmentPlanType, true);
        }
        private ActionResult GetAllDevelopmentPlans(int CompanyId, int EmployeeId, string DevelopmentPlanType, bool IsMyDevelopmentPlanScreen)
        {
            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();
            DevelopmentGoalSearchModel developmentGoalSearchModel = new DevelopmentGoalSearchModel();
            developmentGoalSearchModel.EmployeeID = EmployeeId;
            developmentGoalSearchModel.CompanyId = CompanyId;
            developmentGoalSearchModel.StatusIds = GetDevelopmentPlanIds(DevelopmentPlanType);
            var developmentGoals = DevelopmentModelDB.GetAllDevelopmentPlans(developmentGoalSearchModel);
            var vList = new object();
            vList = new
            {
                aaData = (from item in developmentGoals
                          select new
                          {
                              ID = item.ID,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              GoalName = item.GoalName,
                              Category = item.CategoryName_1,
                              LearningCategory = item.LearningCategory,
                              EmployeeName = item.EmployeeName,
                              EmployeeId = item.OracleNumber,
                              RequestNumber = item.RequestNumber,
                              Status = item.StatusName_1,
                              Action = "<a target= “_blank” class='btn btn-info btn-rounded btn-condensed btn-sm custom-margin' onclick=\""
                                            + string.Format("{0}", IsMyDevelopmentPlanScreen ? "ViewDevelopmentPlanDetail" : "") + "("
                                        + item.ID + "," + item.CompanyID + "," + item.OracleNumber + ",'" + item.EmployeeAlternativeID + "')\" title='View' >View Detail</a>",
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetSelectedDevelopmentPlanDetailInSession(int? DevelopmentGoalID, int? EmployeeID, int? CompanyID, int? DepartmentID, int? PositionID, int? CategoryID, int? SectionID, string SectionName, string SelectedKPIORCompetency, int? AnnualAppraisalID, int? JobDesignationCompetencyID, bool? HaveLoadedFromCategoryPage)
        {
            if (DevelopmentGoalID > 0 || EmployeeID > 0)
            {
                SessionManager.DevelopmentGoalModel = new DevelopmentGoalSessionModel();
            }
            SessionManager.DevelopmentGoalModel = new DevelopmentGoalSessionModel(
                                            SessionManager.DevelopmentGoalModel,
                                            DevelopmentGoalID,
                                            EmployeeID,
                                            CompanyID,
                                            DepartmentID,
                                            PositionID,
                                            CategoryID,
                                            SectionID,
                                            SectionName, SelectedKPIORCompetency, AnnualAppraisalID, JobDesignationCompetencyID, HaveLoadedFromCategoryPage);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobCompetency(int CategoryID)
        {
            var developmenGoalSessionModel = SessionManager.DevelopmentGoalModel;
            int PositionID = developmenGoalSessionModel.PositionID ?? 0;
            List<JDDesignationCompetenciesModel> objJDDesignationCompetenciesList = new List<JDDesignationCompetenciesModel>();
            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            objJDDesignationCompetenciesList = DevelopmentModelDB.GetAllJobCompetencies(developmenGoalSessionModel.PositionID ?? 0);
            int countr = 1;
            var vList = new object();
            vList = new
            {
                aaData = (from item in objJDDesignationCompetenciesList
                          select new
                          {
                              SrNo = countr++,
                              PositionID = item.PositionID,
                              JobDescriptionID = item.JobDescriptionID,
                              CompetencyID = item.CompetencyID,
                              Competency = "<b>" + item.Competency + " </b> <span>" + item.JobDescription + "</span>",
                              Add = "<a class='btn' onclick='return GetDevelopmentGoalFormData(" + CategoryID + ',' + item.CompetencyID + ',' + '"' + item.Competency + "\",\"" + item.Competency + "\",0," + item.JDDesignationCompetencyID + ",true);'>Add</a>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadTeachingGrid(int CategoryID)
        {
            var developmenGoalSessionModel = SessionManager.DevelopmentGoalModel;
            AppraisalTeachingModelDB AppraisalTeachingModelDB = new AppraisalTeachingModelDB();
            var culture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLowerInvariant();
            AppraisalTeachingModel AppraisalTeachingModel = new AppraisalTeachingModel();
            //AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID = Convert.ToInt32(Session["EmployeeID"] == null ? 0 : Session["EmployeeID"]);
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.TeacherID = 10310;
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.FormID = 49;
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYearID = 5;
            AppraisalTeachingModel = AppraisalTeachingModelDB.GetAppraisalTeachingEmployeeInformation(AppraisalTeachingModel, culture);
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.RequestDate = DateTime.Now;
            ViewBag.comapanyId = developmenGoalSessionModel.CompanyID ?? 0;
            //AppraisalTeachingModel.AppraisalKPIsListModel = new AppraisalKPIsListModel();
            //AppraisalTeachingModel.AppraisalKPIsListModel.AppraisalKPIsModel = new List<AppraisalKPIsModel>();
            AppraisalTeachingModel = LoadKPIs(AppraisalTeachingModel);
            var res = new
            {
                value1 = MasterHelper.PartialTeachingView(HtmlTempHelper, AppraisalTeachingModel).ToString(),
            };
            return Json(res);
        }
        public ActionResult LoadPDRPGrid(int CategoryID)
        {
            var developmenGoalSessionModel = SessionManager.DevelopmentGoalModel;
            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();
            var culture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLowerInvariant();
            DevelopmentPlanPDRPModel developmentPlanPDRPModel = new DevelopmentPlanPDRPModel();

            developmentPlanPDRPModel = DevelopmentModelDB.GetDevelopmentPlanPDRPInformation(developmenGoalSessionModel.EmployeeID ?? 0, developmenGoalSessionModel.CompanyID ?? 0, culture);
            ViewBag.comapanyId = developmenGoalSessionModel.CompanyID ?? 0;
            //developmentPlanPDRPModel.AppraisalTeachingModel.AppraisalKPIYears = new SelectList(developmentPlanPDRPModel.AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages.Where(m => m.RatingScaleNumber != 0).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            var res = new
            {
                value1 = developmentPlanPDRPModel.UserTypeId == 1 ? MasterHelper.PartialPDRPTeachingView(HtmlTempHelper, developmentPlanPDRPModel).ToString()
                : MasterHelper.PartialPDRPNonTeachingView(HtmlTempHelper, developmentPlanPDRPModel).ToString(),
            };
            return Json(res);
        }
        public AppraisalTeachingModel LoadKPIs(AppraisalTeachingModel AppraisalTeachingModel)
        {

            AppraisalTeachingModelDB AppraisalTeachingModelDB = new AppraisalTeachingModelDB();

            string KPIyear = AppraisalTeachingModelDB.GetAppraisalBeginYear(AppraisalTeachingModel.AppraisalTeachingEmployeeModel.AppraisalTeachingYearID);
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID = 9;
            AppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID = 5;
            AppraisalTeachingModel.AppraisalKPIsListModel = new AppraisalKPIsListModel();
            AppraisalTeachingModel.AppraisalKPIsListModel = AppraisalTeachingModelDB.GetKPIsById(KPIyear, AppraisalTeachingModel.AppraisalTeachingEmployeeModel.ID, true, AppraisalTeachingModel.AppraisalKPIsListModel, AppraisalTeachingModel.AppraisalTeachingEmployeeModel.CompanyID);

            //////*** F5
            ////// ViewBag.ObservationsRatingScaleWeightages = new SelectList(DropInsRatingScaleWeightagesNAList.OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");

            ////ViewBag.AppraisalKPIYears = new SelectList(LoadRatings(AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");
            AppraisalTeachingModel.AppraisalKPIYears = new SelectList(AppraisalTeachingModel.AppraisalTeachingRatingScaleWeightages.Where(m => m.RatingScaleNumber != 0).OrderByDescending(s => s.RatingScaleNumber), "RatingScaleNumber", "RatingScaleNumber");

            return AppraisalTeachingModel;
        }


        /// <summary>
        /// DevelopmentGoalFromSave
        /// </summary>
        /// <param name="objDevelopmentGoalModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveForm(DevelopmentGoalModel objDevelopmentGoalModel, string NoteDetails, string actionSteps)
        {
            List<DevelopmentGoalAttachmentModel> developmentGoalAttachmentModel = new List<DevelopmentGoalAttachmentModel>();
            objDevelopmentGoalModel.GoalNoteDetails = !string.IsNullOrEmpty(NoteDetails) ? JsonConvert.DeserializeObject<List<DevelopmentGoalAttachmentModel>>(NoteDetails) : new List<DevelopmentGoalAttachmentModel>();
            objDevelopmentGoalModel.DevelopmentGoalActionSteps = !string.IsNullOrEmpty(actionSteps) ? JsonConvert.DeserializeObject<List<DevelopmentGoalActionSteps>>(actionSteps) : new List<DevelopmentGoalActionSteps>();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails objOperationDetails = new OperationDetails();

            int developmentGoalId = objDevelopmentGoalModel.ID;

            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();
            HttpPostedFileBase fileOC;
            Entities.Forms.AllFormsFilesModel ObjFile;
            int fileId = 0;
            if (Request.Files.Count > 0 && Request.Files.AllKeys.Any(k => k.IndexOf("FilesArray") != -1))
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files.GetKey(i).IndexOf("FilesArray") != -1)
                    {
                        fileOC = Request.Files.Get(i);
                        ObjFile = new Entities.Forms.AllFormsFilesModel();
                        if (fileOC.FileName.IndexOf("@@@") > 0)
                        {
                            int.TryParse(fileOC.FileName.Substring(fileOC.FileName.IndexOf("@@@") + 3, fileOC.FileName.Length - fileOC.FileName.IndexOf("@@@") - 3).ToString(), out fileId);
                            ObjFile.FileName = fileOC.FileName.Split("@@@".ToArray())[0].ToString();
                        }
                        else
                        {
                            ObjFile.FileName = string.Empty;
                        }

                        ObjFile.FileID = fileId;
                        ObjFile.FileContentType = fileOC.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileOC);
                        ObjFile.FormFileIDName = Request.Files.GetKey(i).Replace("FilesArray_", "");
                        objDevelopmentGoalModel.Attachments.Add(ObjFile);
                    }

                }
            }

            if (ModelState.IsValid)
            {
                objOperationDetails = DevelopmentModelDB.InsertDevelopmentGoal((DevelopmentGoalModel)objDevelopmentGoalModel);
                if (developmentGoalId == 0 && objOperationDetails.InsertedRowId > 0)
                {
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = DevelopmentModelDB.GetDevelopmentGoalRequesterEmailList(objOperationDetails.InsertedRowId);
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                }
                SessionManager.DevelopmentGoalModel.DevelopmentGoalID = objOperationDetails.InsertedRowId;
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            //return View();
        }

    }

}
