﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using System.Threading.Tasks;
using HRMS.DataAccess;
using System.Data;
namespace HRMS.Web.Controllers
{
    public class LoginController: Controller
    {
        //
        // GET: /Login/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(HRMS.Entities.EmployeeCredentialModel objEmployee)
        {
            if (ModelState.IsValid)
            {
                //var user = await UserManager.FindAsync(model.Username, model.Password);
                EmployeeCredentialModel objEmployeeModel = new EmployeeCredentialModel();
                EmployeeCredentialDB objEmployeeCredentialDB = new EmployeeCredentialDB();
                DataTable dtEmployeeCredentials = new DataTable();

                dtEmployeeCredentials = objEmployeeCredentialDB.GetEmployeeCredentialsByUserName(objEmployee.Username);
                if (dtEmployeeCredentials != null && dtEmployeeCredentials.Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dtEmployeeCredentials.Rows[0]["IsActive"].ToString()))
                    {
                        if (Convert.ToBoolean(dtEmployeeCredentials.Rows[0]["IsEnable"].ToString()))
                        {
                            if (objEmployee.Password == (new EncryptDecrypt().decryptPassword(dtEmployeeCredentials.Rows[0]["Password"].ToString()))) // matchPassword
                            {
                                if (!objEmployeeCredentialDB.CheckEmployeeOtherLogin(objEmployee.Username)) // Check If user is not logged on other Machine
                                {
                                    string defaultPassword = dtEmployeeCredentials.Rows[0]["FirstName"].ToString() + dtEmployeeCredentials.Rows[0]["LastName"].ToString().Substring(0, 1);
                                    if (objEmployee.Password == defaultPassword) // Check If password is default Password
                                    {
                                        Session["defaultPassword"] = defaultPassword;
                                        Session["EmployeeId"] = dtEmployeeCredentials.Rows[0]["EmployeeId"].ToString();
                                        return RedirectToAction("Index", "ChangePassword");
                                    }
                                    else
                                    {
                                        // Not Default password
                                        Session["EmployeeId"] = dtEmployeeCredentials.Rows[0]["EmployeeId"].ToString();
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else
                                {
                                    // user is logged on other Machine
                                    ViewData["ErrorMessage"] = "You are already logged on some other machine. Please log-out from there or contact admin";
                                }
                            }
                            else
                            {
                                // Login Credentials Mis Match
                                ViewData["ErrorMessage"] = "Some Information mis matched.";
                            }
                        }
                        else
                        {
                            // User is Not Enabled
                            ViewData["ErrorMessage"] = "Your Account has been not Enabled yet. Please Contact Admin.";
                        }
                    }
                    else
                    {
                        // User Is InActivate
                        ViewData["ErrorMessage"] = "Your Account has been Inactivated. Please Contact Admin.";
                    }
                }
                else
                {
                    // User not found
                    ViewData["ErrorMessage"] = "User not found";
                }
            }
            return View(objEmployee);
        }

        public ActionResult LogOut()
        {
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("UserLogin", "Account");
        }
    }
}