﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class AbsentLateInEarlyOutController : BaseController
    {
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);

            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }
            return View(employeeDetailsModelList);
        }

        public ActionResult GetAbsentLateInEarlyOutList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            //-------------Data Objects--------------------
            List<AbsentLateInEarlyOutModel> objAbsentLateInEarlyOutModelList = new List<AbsentLateInEarlyOutModel>();
            AbsentLateInEarlyOutDB objAbsentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            PayLateTypeDB objPayLateTypeDB = new PayLateTypeDB();
            LeaveTypeDB objLeaveType = new LeaveTypeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objAbsentLateInEarlyOutModelList = objAbsentLateInEarlyOutDB.GetAbsentLateInEarlyOutList(empid);

            //objAbsentLateInEarlyOutModelList = objAbsentLateInEarlyOutModelList.OrderBy(p => DateTime.Parse(p.PayEmployeeLateDate)).ToList();

            var vList = new object();
            vList = new
            {
                aaData = (from item in objAbsentLateInEarlyOutModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditAbsentLateInEarlyOut(" + item.PayEmployeeLateID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteAbsentLateInEarlyOut(" + item.PayEmployeeLateID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PayEmployeeLateID = item.PayEmployeeLateID,
                              EmployeeID = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(item.EmployeeID).LastName,
                              PayEmployeeLateDate = item.PayEmployeeLateDate,
                              PayLateTypeID =item.Status,
                              InTime = item.InTime,
                              OutTime = item.OutTime,
                              LateMinutes = item.LateMinutes,
                              EarlyMinutes = item.EarlyMinutes,
                              IsExcused = item.IsExcused ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.IsExcused + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.IsExcused + "'/>",
                              IsDeducted = item.IsDeducted ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.IsDeducted + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.IsDeducted + "'/>",
                              LateReason=item.LateReason
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult AddAbsentLateInEarlyOut()
        {

            int empid = 0;
            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(empid);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            AbsentLateInEarlyOutModel objAbsentLateInEarlyOut = new AbsentLateInEarlyOutModel();
            objAbsentLateInEarlyOut.EmployeeID = empid;
            return View(objAbsentLateInEarlyOut);
        }

        [HttpPost]
        public ActionResult AddAbsentLateInEarlyOut(AbsentLateInEarlyOutModel objAbsentLateInEarlyOutModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                AbsentLateInEarlyOutDB objAbsentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
                OperationDetails objOperationDetails = new OperationDetails();
                EmployeeDB employeeDb = new EmployeeDB();
                EmploymentInformation employeeInfo = employeeDb.GetEmployeeHireDateInformation().FirstOrDefault(x => x.EmployeeID == objAbsentLateInEarlyOutModel.EmployeeID);
                DateTime date = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(objAbsentLateInEarlyOutModel.PayEmployeeLateDate));
                DateTime inTimeDate = Convert.ToDateTime(objAbsentLateInEarlyOutModel.InTime);
                DateTime outTimeDate = Convert.ToDateTime(objAbsentLateInEarlyOutModel.OutTime);
                objAbsentLateInEarlyOutModel.InTime = (new DateTime(date.Year, date.Month, date.Day, inTimeDate.Hour, inTimeDate.Minute, inTimeDate.Second)).ToString();
                objAbsentLateInEarlyOutModel.OutTime = (new DateTime(date.Year, date.Month, date.Day, outTimeDate.Hour, outTimeDate.Minute, outTimeDate.Second)).ToString();
                objAbsentLateInEarlyOutModel.IsManual = true;
                if (employeeInfo.HireDate != "")
                {
                    DateTime hireDate = Convert.ToDateTime(employeeInfo.HireDate);
                    if (!employeeDb.checkHireDateIsGreater(date, employeeInfo.HireDate))
                    {
                        objOperationDetails = objAbsentLateInEarlyOutDB.InsertAbsentLateInEarlyOut(objAbsentLateInEarlyOutModel);
                    }
                    else
                    {
                        objOperationDetails.Success = false;
                        objOperationDetails.Message = "Joining date for selected employee is " + hireDate.ToString("dd MMM, yyyy") + " so you cannot enter late and early out before joining date."; ;
                    }
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please first enter hire date of this employee from employee profile.";
                }

                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                PayLateTypeDB objPayLateTypeDB = new PayLateTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                ViewBag.PayLateType = new SelectList(objPayLateTypeDB.GetPayLateTypeList(), "PayLateTypeID", "PayLateTypeName_1"); EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;
                //Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditAbsentLateInEarlyOut(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            AbsentLateInEarlyOutDB objAbsentLateInEarlyOutModelDB = new AbsentLateInEarlyOutDB();
            AbsentLateInEarlyOutModel objAbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
            objAbsentLateInEarlyOutModel = objAbsentLateInEarlyOutModelDB.AbsentLateInEarlyOutById(id);
            objAbsentLateInEarlyOutModel.PayEmployeeLateDate = objAbsentLateInEarlyOutModel.PayEmployeeLateDate;
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(objAbsentLateInEarlyOutModel.EmployeeID);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            return View(objAbsentLateInEarlyOutModel);
        }

        [HttpPost]
        public JsonResult EditAbsentLateInEarlyOut(AbsentLateInEarlyOutModel objAbsentLateInEarlyOutModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //objAbsentLateInEarlyOutModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentLateInEarlyOutDB objAbsentLateInEarlyOutModelDB = new AbsentLateInEarlyOutDB();
                OperationDetails objOperationDetails = new OperationDetails();
                EmployeeDB employeeDb = new EmployeeDB();
                EmploymentInformation employeeInfo = employeeDb.GetEmployeeHireDateInformation().FirstOrDefault(x => x.EmployeeID == objAbsentLateInEarlyOutModel.EmployeeID);
                DateTime date = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(objAbsentLateInEarlyOutModel.PayEmployeeLateDate));
                if (employeeInfo.HireDate != "")
                {
                    DateTime hireDate = Convert.ToDateTime(employeeInfo.HireDate);
                    if (!employeeDb.checkHireDateIsGreater(date, employeeInfo.HireDate))
                    {
                        objOperationDetails = objAbsentLateInEarlyOutModelDB.UpdateAbsentLateInEarlyOut(objAbsentLateInEarlyOutModel);
                    }
                    else
                    {
                        objOperationDetails.Success = false;
                        objOperationDetails.Message = "Joining date for selected employee is " + hireDate.ToString("dd MMM, yyyy") + " so you cannot enter late and early out before joining date."; ;
                    }
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please first enter hire date of this employee from employee profile.";
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                PayLateTypeDB objPayLateTypeDB = new PayLateTypeDB();

                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                ViewBag.PayLateType = new SelectList(objPayLateTypeDB.GetPayLateTypeList(), "PayLateTypeID", "PayLateTypeName_1"); EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;

                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewAbsentLateInEarlyOut(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            PayLateTypeDB objPayLateTypeDB = new PayLateTypeDB();

            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            ViewBag.PayLateType = new SelectList(objPayLateTypeDB.GetPayLateTypeList(), "PayLateTypeID", "PayLateTypeName_1"); EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName;

            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1");
            AbsentLateInEarlyOutDB objAbsentLateInEarlyOutModelDB = new AbsentLateInEarlyOutDB();
            AbsentLateInEarlyOutModel objAbsentLateInEarlyOutModel = new AbsentLateInEarlyOutModel();
            objAbsentLateInEarlyOutModel = objAbsentLateInEarlyOutModelDB.AbsentLateInEarlyOutById(id);
            return View(objAbsentLateInEarlyOutModel);
        }

        public ActionResult DeleteAbsentLateInEarlyOut(int id)
        {
            AbsentLateInEarlyOutDB objAbsentLateInEarlyOutDB = new AbsentLateInEarlyOutDB();
            objAbsentLateInEarlyOutDB.DeleteAbsentLateInEarlyOutById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckEmployeeInAbsent(int EmpID, string FromDate, string ToDate)
        {
            AbsentDB absentDB = new AbsentDB();
            var data = absentDB.GetAbsentList(FromDate, ToDate, EmpID);
            if (data.Count > 0)
                return Json(new { isPresent = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { isPresent = false }, JsonRequestBehavior.AllowGet);
        }
    }
}