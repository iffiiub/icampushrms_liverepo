﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.General;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class PublicHolidayLeaveManagerController : FormsController
    {
        // GET: PublicHolidayLeaveManager
        public ActionResult Index()
        {
          
            DepartmentDB objDepartmentDB = new DepartmentDB();
            CompanyDB objCompanyDB = new CompanyDB();
            HolidayDB objHolidayDB = new HolidayDB();
           
            PublicHolidayLeaveManagerModel objPublicHolidayLeaveManagerModel = new PublicHolidayLeaveManagerModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
           
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(null,userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");

            ViewBag.Company = GetCompanySelectList();

            List<HolidayModel> objHolidayList = new List<HolidayModel>();
            //objHolidayList = objHolidayDB.GetHolidayList();
            ViewBag.Holiday = new SelectList(objHolidayList, "HolidayID", "HolidayName");

            return View(objPublicHolidayLeaveManagerModel);
        }

        public ActionResult LoadPublicHolidayLeaveGrid(int holidayID, int? departmentID, int? companyID)
        {
            return PartialView("_PublicHolidayLeaveGrid", GetPublicHolidayLeaveList(holidayID, departmentID, companyID));
        }

        private PublicHolidayLeaveManagerViewModel GetPublicHolidayLeaveList(int holidayID, int? departmentID, int? companyID)
        {
            PublicHolidayLeaveManagerViewModel objPublicHolidayLeaveManagerViewModel = new PublicHolidayLeaveManagerViewModel();
           
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objPublicHolidayLeaveManagerViewModel.PublicHolidayLeaveManagerModelList = new PublicHolidayLeaveManagerDB().GetPublicHolidayLeaveList(objUserContextViewModel.UserId, holidayID, departmentID, companyID);
            objPublicHolidayLeaveManagerViewModel.HolidayDateList = new HolidayDB().GetHolidayDates(holidayID);
           // ViewBag.CountVacationType = objLeaveBalanceManagerViewModel.VacationTypeModellList.Count;
            return objPublicHolidayLeaveManagerViewModel;
        }
        public ActionResult GetHolidayListByCompany(int? companyID,int?holidayID)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HolidayModel> objHolidayList = new List<HolidayModel>();
            HolidayDB objHolidayDB = new HolidayDB();
            objHolidayList = objHolidayDB.GetHolidayList(companyID, holidayID,objUserContextViewModel.UserId);
            ViewBag.Holiday = new SelectList(objHolidayList, "HolidayID", "HolidayName");
            return Json(objHolidayList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Save(PublicHolidayLeaveManagerSaveModel model)
        {            
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(new PublicHolidayLeaveManagerDB().Save(model.HolidayID, model.PublicHolidayLeaveManagerModelList, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }

    }
}