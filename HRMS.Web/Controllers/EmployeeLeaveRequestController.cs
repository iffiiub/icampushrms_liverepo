﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.ViewModel;
using System.IO;
using System.Globalization;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class EmployeeLeaveRequestController : FormsController
    {
        FormLeaveRequestDB formLeaveRequestDB;
        public EmployeeLeaveRequestController()
        {
            XMLLogFile = "LoggerEmployeeLeaveRequest.xml";
            formLeaveRequestDB = new FormLeaveRequestDB();
        }
        // GET: EmployeeLeaveRequest
        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }
        public ActionResult Create()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var UserId = objUserContextViewModel.UserId;
            List<Entities.General.OtherPermissions> permissionList = GetPermissionList();         
            ViewBag.IsEditRequestFromAllRequests = false;
            ViewBag.PermissionList = permissionList;
            LeaveRequestModel leaveRequest = new LeaveRequestModel();
            leaveRequest.RequesterEmployeeID = objUserContextViewModel.UserId;
            leaveRequest.CreatedOn = DateTime.Now.Date;
            leaveRequest.CompanyID = objUserContextViewModel.CompanyId;
            leaveRequest.ReqStatusID = 0;

            //List<EmployeeTaskHandoverModel> lstEmployeeTaskHandoverModel = new List<EmployeeTaskHandoverModel>();
            //lstEmployeeTaskHandoverModel = formLeaveRequestDB.GetAllEmployeeByDepartment(UserId);
            var employeeDB = new EmployeeDB();
            var lstEmployeeTaskHandoverModel = employeeDB.GetALLEmployeeByUserId(UserId).Where(e=>e.IsActive==1).ToList();
            ViewBag.LeaveType = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(UserId), "id", "text");
            ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName");
            ViewBag.Employee1 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
            ViewBag.Employee2 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
            ViewBag.Employee3 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
            ViewBag.Employee4 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
            ViewBag.Employee5 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
            leaveRequest.IsAddMode = true;

            return View(leaveRequest);
        }
        public ActionResult Edit()
        {
            LeaveRequestModel leaveRequestModel = new LeaveRequestModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int formProcessID = GetFormProcessSessionID();
            var UserId = objUserContextViewModel.UserId;
            var CompanyId = objUserContextViewModel.CompanyId;
            decimal lapsedays = 0, extendeddays = 0;
            if (formProcessID > 0)
            {
                leaveRequestModel = formLeaveRequestDB.GetForm(formProcessID, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = formLeaveRequestDB.GetPendingFormsApproval(formProcessID);
                // if ((objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID) || (objUserContextViewModel.UserId == leaveRequestModel.RequesterEmployeeID && leaveRequestModel.ReqStatusID == (int)RequestStatus.Rejected))
                if ((objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID))
                {
                    #region "Reject request related - commented"

                    //if rejected, then at for re submitting need to show updated balances
                    //if (leaveRequestModel.ReqStatusID == (int)RequestStatus.Rejected)
                    //{
                    //    LeaveBalanceInfoModel objLeaveBalanceInfoModel = GetLeaveBalanceInfo(leaveRequestModel.VacationTypeID, leaveRequestModel.RequesterEmployeeID);
                    //    leaveRequestModel.AvailableLeaveDays = objLeaveBalanceInfoModel.RemainingLeave;
                    //    lapsedays = objLeaveBalanceInfoModel.LapsingDays == null ? 0 : (decimal)objLeaveBalanceInfoModel.LapsingDays;
                    //    extendeddays = objLeaveBalanceInfoModel.ExtendedLapsingDays == null ? 0 : (decimal)objLeaveBalanceInfoModel.ExtendedLapsingDays;
                    //    leaveRequestModel.LapsingDays = objLeaveBalanceInfoModel.LapsingDays;
                    //    leaveRequestModel.ExtendedLapsingDays = objLeaveBalanceInfoModel.ExtendedLapsingDays;
                    //    leaveRequestModel.RemainingDays = (objLeaveBalanceInfoModel.RemainingLeave + lapsedays + extendeddays) - leaveRequestModel.RequestedDays;
                    //    //   leaveRequestModel.RequestedDays = Convert.ToDecimal(reader["RequestedDays"] == DBNull.Value ? "0" : reader["RequestedDays"].ToString());
                    //    leaveRequestModel.LapsingDate = objLeaveBalanceInfoModel.LapseDate;
                    //    leaveRequestModel.ExtendedLapsingDate = objLeaveBalanceInfoModel.ExtendedLapseDate;
                    #endregion
                    var TaskId = leaveRequestModel.ID;
                    List<EmployeeTaskHandoverModel> objTaskHandOverList = new List<EmployeeTaskHandoverModel>();
                    objTaskHandOverList = formLeaveRequestDB.GetTaskDetial(TaskId);
                    leaveRequestModel.EmployeeTaskHandoverList = objTaskHandOverList;
                    ViewBag.RecruitR1BudgetedID = leaveRequestModel.ID;
                    ViewBag.FormProcessID = formProcessID.ToString();
                    ViewBag.RequestID = leaveRequestModel.RequestID;
                    ViewBag.LeaveType = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(UserId), "id", "text");
                    ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName");
                    var employeeDB = new EmployeeDB();
                    var lstEmployeeTaskHandoverModel = employeeDB.GetALLEmployeeByUserId(leaveRequestModel.RequesterEmployeeID).Where(e => e.IsActive == 1).ToList();
                    foreach (EmployeeTaskHandoverModel empTask in objTaskHandOverList)
                    {
                        if (!lstEmployeeTaskHandoverModel.Any(item => item.EmployeeId == empTask.EmployeeId))
                        {
                            Employee emp = new Employee();
                            emp.EmployeeId = empTask.EmployeeId;
                            emp.EmployeeAlternativeID = empTask.EmployeeAlternativeID;
                            emp.FullName = empTask.FullName;
                            lstEmployeeTaskHandoverModel.Add(emp);
                        }
                    }
                    if (objTaskHandOverList.Count > 0)
                    {
                        ViewBag.Employee1 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[0].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee1 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }
                    if (objTaskHandOverList.Count > 1)
                    {
                        ViewBag.Employee2 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[1].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee2 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }

                    if (objTaskHandOverList.Count > 2)
                    {
                        ViewBag.Employee3 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[2].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee3 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }

                    if (objTaskHandOverList.Count > 3)
                    {
                        ViewBag.Employee4 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[3].TaskAssignEmpID : 0);

                    }
                    else
                    {
                        ViewBag.Employee4 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }
                    if (objTaskHandOverList.Count > 4)
                    {

                        ViewBag.Employee5 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[4].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee5 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }
                    return View("Edit", leaveRequestModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }
        public ActionResult UpdateDetails()
        {
            LeaveRequestModel leaveRequestModel = new LeaveRequestModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int formProcessID = GetFormProcessSessionID();
            var UserId = objUserContextViewModel.UserId;
            var CompanyId = objUserContextViewModel.CompanyId;
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            if (formProcessID > 0)
            {
                leaveRequestModel = formLeaveRequestDB.GetForm(formProcessID, objUserContextViewModel.UserId);
                RequestFormsApproveModel requestFormsApproveModel = formLeaveRequestDB.GetPendingFormsApproval(formProcessID);
                //Pending Or Completed and is coming from all requests page
                if (leaveRequestModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    var TaskId = leaveRequestModel.ID;
                    List<EmployeeTaskHandoverModel> objTaskHandOverList = new List<EmployeeTaskHandoverModel>();
                    objTaskHandOverList = formLeaveRequestDB.GetTaskDetial(TaskId);
                    leaveRequestModel.EmployeeTaskHandoverList = objTaskHandOverList;
                    ViewBag.RecruitR1BudgetedID = leaveRequestModel.ID;
                    leaveRequestModel.FormProcessID = formProcessID;
                    ViewBag.RequestID = leaveRequestModel.RequestID;
                    ViewBag.LeaveType = new SelectList(new VacationDB().GetVactionTypeListAsPerEmployeeId(UserId), "id", "text");
                    ViewBag.Country = new SelectList(new CountryDB().GetAllContries(), "CountryId", "CountryName");                
                    var employeeDB = new EmployeeDB();
                    var lstEmployeeTaskHandoverModel  = employeeDB.GetALLEmployeeByUserId(UserId).Where(e => e.IsActive == 1).ToList();
                    foreach(EmployeeTaskHandoverModel empTask in objTaskHandOverList)
                    {
                        if (!lstEmployeeTaskHandoverModel.Any(item => item.EmployeeId == empTask.EmployeeId))
                        {
                            Employee emp = new Employee();
                            emp.EmployeeId = empTask.EmployeeId;
                            emp.EmployeeAlternativeID = empTask.EmployeeAlternativeID;
                            emp.FullName = empTask.FullName;
                            lstEmployeeTaskHandoverModel.Add(emp);
                        }
                    }
                    if (objTaskHandOverList.Count > 0)
                    {
                        ViewBag.Employee1 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[0].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee1 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }
                    if (objTaskHandOverList.Count > 1)
                    {
                        ViewBag.Employee2 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[1].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee2 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }

                    if (objTaskHandOverList.Count > 2)
                    {
                        ViewBag.Employee3 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[2].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee3 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }

                    if (objTaskHandOverList.Count > 3)
                    {
                        ViewBag.Employee4 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[3].TaskAssignEmpID : 0);

                    }
                    else
                    {
                        ViewBag.Employee4 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }
                    if (objTaskHandOverList.Count > 4)
                    {

                        ViewBag.Employee5 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName", objTaskHandOverList.Count > 0 ? objTaskHandOverList[4].TaskAssignEmpID : 0);
                    }
                    else
                    {
                        ViewBag.Employee5 = new SelectList(lstEmployeeTaskHandoverModel, "EmployeeID", "FullName");
                    }
                    ViewBag.IsEditRequestFromAllRequests = isEditRequestFromAllRequests;
                    return View(leaveRequestModel);

                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }
        public ActionResult UpdateLeaveRequest()
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                int result = 0;
                var data = Request.Form;
                LeaveRequestModel leaveRequest = new LeaveRequestModel();
                leaveRequest.FromDate = Convert.ToString(data["FromDate"]);
                leaveRequest.ToDate = Convert.ToString(data["ToDate"]);
                leaveRequest.ID = Convert.ToInt32(data["ID"]);

                List<EmployeeTaskHandoverModel> TaskHandoverList = new List<EmployeeTaskHandoverModel>();
                var Emp = data["updatedData"];
                var jsonObj = new JavaScriptSerializer().Deserialize<List<EmployeeTaskHandoverModel>>(Emp);
                EmployeeTaskHandoverModel objTask;

                foreach (var obj in jsonObj)
                {
                    objTask = new EmployeeTaskHandoverModel();
                    objTask.TaskHandOverID = obj.TaskHandOverID;
                    objTask.TaskAssignEmpID = obj.TaskAssignEmpID;
                    objTask.TaskDetail = obj.TaskDetail;
                    TaskHandoverList.Add(objTask);
                }
                leaveRequest.EmployeeTaskHandoverList = TaskHandoverList;

                result = new FormLeaveRequestDB().UpdateLeaveRequest(leaveRequest);
                if (result > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while updating Details";
                    operationDetails.CssClass = "error";
                }
            }
            catch (Exception e)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveForm()
        {
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            HttpPostedFileBase fileMC;
            string formProcessIDs = "";
            int result = 0;
            int formprocessid = 0;
            LeaveRequestModel leaveRequest = new LeaveRequestModel();
            List<Entities.General.OtherPermissions> permissionList = GetPermissionList();
            bool isEditCompletedFormRequest = permissionList.Where(x => x.OtherPermissionId == 42).Count() > 0 ? true : false;


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            try
            {
                var data = Request.Form;
                leaveRequest.ID = Convert.ToInt32(string.IsNullOrEmpty(data["ID"]) ? "0" : data["ID"]);
                leaveRequest.VacationTypeID = Int16.Parse(data["VacationTypeID"]);
                leaveRequest.HalfDayLeave = bool.Parse(data["HalfDay"]);
                leaveRequest.FromDate = Convert.ToString(data["FromDate"]);
                leaveRequest.InCountry = bool.Parse(data["In"]);
                leaveRequest.OutCountry = bool.Parse(data["Out"]);
                leaveRequest.ContactInfo1 = Convert.ToString(data["ContactInfo1"]);
                leaveRequest.ContactInfo3 = Convert.ToString(data["ContactInfo3"]);
                leaveRequest.ToDate = Convert.ToString(data["ToDate"]);
                leaveRequest.CountryID = Int16.Parse(data["Country"]);
                leaveRequest.ContactInfo2 = Convert.ToString(data["ContactInfo2"]);
                leaveRequest.ContactInfo2 = Convert.ToString(data["ContactInfo2"]);
                // leaveRequest.ReqStatusID = 1;
                leaveRequest.ReqStatusID = Convert.ToInt16(data["hdnReqStatusID"]);
                leaveRequest.AvailableLeaveDays = Convert.ToDecimal(data["AvailableLeaveDays"]);
                leaveRequest.LapsingDays = Convert.ToDecimal(data["LapsingDays"]);
                leaveRequest.ExtendedLapsingDays = Convert.ToDecimal(data["ExtendedLapsingDays"]);
                leaveRequest.RemainingDays = Convert.ToDecimal(data["RemainingDays"]);
                leaveRequest.RequestedDays = Convert.ToDecimal(data["RequestedDays"]);
                leaveRequest.LapsingDate = string.IsNullOrEmpty(Convert.ToString(data["LapsingDate"])) ? (DateTime?)null : DateTime.ParseExact(data["LapsingDate"].ToString(), HRMSDateFormat, CultureInfo.InvariantCulture);
                // DateTime.ParseExact(leaveRequestModel.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                leaveRequest.ExtendedLapsingDate = string.IsNullOrEmpty(Convert.ToString(data["ExtendedLapsingDate"])) ? (DateTime?)null : DateTime.ParseExact(data["ExtendedLapsingDate"].ToString(), HRMSDateFormat, CultureInfo.InvariantCulture);

                List<EmployeeTaskHandoverModel> TaskHandoverList = new List<EmployeeTaskHandoverModel>();
                var Emp = data["updatedData"];
                var jsonObj = new JavaScriptSerializer().Deserialize<List<EmployeeTaskHandoverModel>>(Emp);
                EmployeeTaskHandoverModel objTask;

                foreach (var obj in jsonObj)
                {
                    objTask = new EmployeeTaskHandoverModel();
                    objTask.TaskAssignEmpID = obj.TaskAssignEmpID;
                    objTask.TaskDetail = obj.TaskDetail;
                    TaskHandoverList.Add(objTask);
                }
                leaveRequest.EmployeeTaskHandoverList = TaskHandoverList;
                leaveRequest.Comments = Convert.ToString(data["Comments"]);


                if (leaveRequest.ID > 0)
                {
                    leaveRequest.MadicalCerFileID = Convert.ToInt32(string.IsNullOrEmpty(data["MadicalCerFileID"]) ? "0" : data["MadicalCerFileID"]);
                }
                else
                {
                    leaveRequest.MadicalCerFileID = 0;

                }
                leaveRequest.CreatedBy = objUserContextViewModel.UserId;
                leaveRequest.CreatedOn = DateTime.Now;
                leaveRequest.ModifiedBy = objUserContextViewModel.UserId;
                leaveRequest.ModifiedOn = DateTime.Now;
                leaveRequest.EmployeeID = objUserContextViewModel.UserId;
                leaveRequest.RequesterEmployeeID = objUserContextViewModel.UserId;
                leaveRequest.CompanyID = objUserContextViewModel.CompanyId;

                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
                if (Request.Files.Count > 0)
                {
                    if (Request.Files["MedicalCertificate"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileMC = Request.Files["MedicalCertificate"];
                        ObjFile.FileName = fileMC.FileName;
                        ObjFile.FileContentType = fileMC.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileMC);
                        ObjFile.FormFileIDName = "MedicalCertificateID";
                        uploadFileList.Add(ObjFile);
                        leaveRequest.MadicalCerFileID = 0;
                    }
                    leaveRequest.AllFormsFilesModelList = uploadFileList;
                }
                if (leaveRequest.ID > 0)

                {
                    formprocessid = Convert.ToInt32(string.IsNullOrEmpty(data["FormProcessID"]) ? "0" : data["FormProcessID"]);
                    RequestFormsApproveModel requestFormsApproveModel = formLeaveRequestDB.GetPendingFormsApproval(formprocessid);

                    //   if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    //|| (ObjRecruitR1BudgetedModel.ReqStatusID == (int)RequestStatus.Rejected && ObjRecruitR1BudgetedModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                    //   {
                    //       ObjRecruitR1BudgetedModel.FormProcessID = formprocessid;
                    //       result = new RecruitR1BudgetedDB().UpdateForm(ObjRecruitR1BudgetedModel);
                    //       formProcessIDs = ObjRecruitR1BudgetedModel.FormProcessID.ToString();
                    //   }
                    //   else
                    //   {
                    //       operationDetails.Success = true;
                    //       operationDetails.Message = "No permissions to update.";
                    //       operationDetails.CssClass = "error";
                    //       return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    //   }

                    if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID ||
                        (leaveRequest.ReqStatusID == (int)RequestStatus.Rejected && leaveRequest.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || isEditCompletedFormRequest
                        )
                    {
                        result = new FormLeaveRequestDB().UpdateForm(leaveRequest);
                        formProcessIDs = formprocessid.ToString();
                    }
                    else
                    {
                        operationDetails.Success = true;
                        operationDetails.Message = "No permissions to update.";
                        operationDetails.CssClass = "error";
                        return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    }

                    if (result > 0)
                    {
                        //For updating, if re intilaizing , then only need to send Email.
                        if (leaveRequest.ReqStatusID == (int)RequestStatus.Rejected)
                        {
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = new FormLeaveRequestDB().GetApproverEmailList(formProcessIDs, leaveRequest.RequesterEmployeeID);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        }

                        operationDetails.Success = true;
                        operationDetails.Message = "Request updated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while updating Details";
                        operationDetails.CssClass = "error";
                    }
                }
                else
                {
                    formprocessid = 0;
                    requestFormsProcessModelList = new FormLeaveRequestDB().SaveForm(leaveRequest);
                    if (requestFormsProcessModelList != null)
                    {
                        formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = new FormLeaveRequestDB().GetApproverEmailList(formProcessIDs, leaveRequest.RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                        operationDetails.Success = true;
                        operationDetails.Message = "Request generated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while adding Details";
                        operationDetails.CssClass = "error";

                    }
                }


            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteAllFormsFile(int fileID, int formProcessID, string fileIDFieldName)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int res = new AllFormsFilesDB().DeleteAllFormsFile(fileID, formProcessID, fileIDFieldName, objUserContextViewModel.UserId);
            return Json(new { result = res }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            //Pooja's Changes
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            FormLeaverequestviewModel leaverequestviewModel = new FormLeaverequestviewModel();
            leaverequestviewModel = formLeaveRequestDB.GetFormDetails(formProcessID);
            leaverequestviewModel.FormProcessID = formProcessID;
            List<EmployeeTaskHandoverModel> objTaskHandOverList = new List<EmployeeTaskHandoverModel>();
            var TaskId = leaverequestviewModel.ID;
            objTaskHandOverList = formLeaveRequestDB.GetTaskDetial(TaskId);
            leaverequestviewModel.EmployeeTaskHandoverList = objTaskHandOverList;
            List<RequestFormsApproveModel> requestFormsApproveModelList = formLeaveRequestDB.GetAllRequestFormsApprovals(formProcessID);
            leaverequestviewModel.leaverequestmodel.RequestFormsApproveModelList = requestFormsApproveModelList;
            //LeaveBalanceInfoModel objLeaveBalanceInfoModel = GetLeaveBalanceInfo();
            ////List<LeaveBalanceInfoModel> lstLeaveBalanceInfoModel = new VacationDB().GetLeaveBalanceInfoById(objUserContextViewModel.UserId);
            ////if (lstLeaveBalanceInfoModel.Count > 0)
            ////{
            ////    double RequestedDays = 0;
            ////    if (leaverequestviewModel.HalfDayLeave)
            ////    {
            ////        RequestedDays = 0.5;
            ////    }
            ////    else if (!(string.IsNullOrEmpty(leaverequestviewModel.FromDate) && string.IsNullOrEmpty(leaverequestviewModel.FromDate)))
            ////    {
            ////        RequestedDays = (Convert.ToDateTime(leaverequestviewModel.ToDate) - Convert.ToDateTime(leaverequestviewModel.FromDate)).TotalDays;
            ////        RequestedDays = RequestedDays + 1;
            ////    }
            ////    leaverequestviewModel.RequestedDays = Convert.ToString(RequestedDays);
            ////    decimal RemainingLeave = 0;
            ////    if (lstLeaveBalanceInfoModel.Where(m => m.LeaveTypeID == leaverequestviewModel.VacationTypeID).Count() > 0)
            ////    {
            ////        RemainingLeave = lstLeaveBalanceInfoModel.Where(m => m.LeaveTypeID == leaverequestviewModel.VacationTypeID).FirstOrDefault().RemainingLeave;
            ////    }
            ////    leaverequestviewModel.RemainingDays = Convert.ToString(RemainingLeave - (decimal)RequestedDays);
            ////    leaverequestviewModel.TotalAvailableLeaves = Convert.ToString(RemainingLeave);
            ////}
            //Pooja's Changes
            UserRoleDB userRoleDB = new UserRoleDB();
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;
            return View(leaverequestviewModel);
        }
        public JsonResult IsEmployeeUnderProbation()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool IsEmployeeUnderProbation = new VacationDB().IsEmployeeUnderProbation(objUserContextViewModel.UserId);
            OperationDetails operationDetails = new OperationDetails();
            if (IsEmployeeUnderProbation)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Not Eligible! You are under probation.";
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = false;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeeProbationDetails()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ValidateProbationModel probation  = new FormLeaveRequestDB().GetEmployeeProbationDetails(objUserContextViewModel.UserId);              
            return Json(probation, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsPendingLeaveRequestExists(int leaveRequestID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool IsExists = new FormLeaveRequestDB().IsPendingLeaveExists(objUserContextViewModel.UserId, leaveRequestID);
            OperationDetails operationDetails = new OperationDetails();
            if (IsExists)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Pending Leave Request Exists.";
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = false;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeavesTakenInCurrentAnniversary(int LeaveTypeID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CurrentAnniversaryModel objLeaves = new VacationDB().GetLeavesTakenInCurrentAnniversary(objUserContextViewModel.UserId, LeaveTypeID);
            return Json(objLeaves, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsOffDayBeforeOrAfterAppliedLeave(string StartDate, string EndDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool IsOffDay = new VacationDB().IsOffDayBeforeOrAfterAppliedLeave(objUserContextViewModel.UserId, StartDate, EndDate);
            OperationDetails operationDetails = new OperationDetails();
            if (IsOffDay)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Please upload medical certificate.";
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = false;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveBalanceInfoById(int LeaveTypeID, int RequesterEmployeeID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveBalanceInfoModel objLeaveBalanceInfoModel = new LeaveBalanceInfoModel();
            List<LeaveBalanceInfoModel> lstLeaveBalanceInfoModel = new VacationDB().GetLeaveBalanceInfoById(RequesterEmployeeID);
            if (lstLeaveBalanceInfoModel.Count > 0)
            {
                objLeaveBalanceInfoModel = lstLeaveBalanceInfoModel.Where(m => m.LeaveTypeID == LeaveTypeID).FirstOrDefault();
            }

            return Json(objLeaveBalanceInfoModel, JsonRequestBehavior.AllowGet);
        }
        public LeaveBalanceInfoModel GetLeaveBalanceInfo(int LeaveTypeID, int RequesterEmployeeID)
        {
            LeaveBalanceInfoModel objLeaveBalanceInfoModel = new LeaveBalanceInfoModel();
            List<LeaveBalanceInfoModel> lstLeaveBalanceInfoModel = new VacationDB().GetLeaveBalanceInfoById(RequesterEmployeeID);
            if (lstLeaveBalanceInfoModel.Count > 0)
            {
                objLeaveBalanceInfoModel = lstLeaveBalanceInfoModel.Where(m => m.LeaveTypeID == LeaveTypeID).FirstOrDefault();
            }
            return objLeaveBalanceInfoModel;


        }
        public JsonResult GetVacationTypeDetail(int LeaveTypeID)
        {
            VacationTypeModel objVacationTypeModel = new VacationDB().GetVacationTypeById(LeaveTypeID);
            return Json(objVacationTypeModel, JsonRequestBehavior.AllowGet);
        }
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Add any logic to be done once approval process is completed
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }
                        LeaveRequestModel leaveRequestModel = new LeaveRequestModel();
                        leaveRequestModel = formLeaveRequestDB.GetForm(formProcessID, objUserContextViewModel.UserId);
                        // decimal TotalAppliedLeave = leaveRequestModel.HalfDayLeave ? (Decimal)0.5 : (Decimal)(DateTime.ParseExact(leaveRequestModel.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact(leaveRequestModel.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).TotalDays;
                        formLeaveRequestDB.SaveFinalLeaveApproval(formProcessID);
                    }

                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveBalanceInfoByCategoryId(int LeaveCategoryId, int RequesterEmployeeID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveBalanceInfoModel objLeaveBalanceInfoModel = new LeaveBalanceInfoModel();
            List<LeaveBalanceInfoModel> lstLeaveBalanceInfoModel = new VacationDB().GetLeaveBalanceInfoById(RequesterEmployeeID);
            if (lstLeaveBalanceInfoModel.Count > 0)
            {
                objLeaveBalanceInfoModel = lstLeaveBalanceInfoModel.Where(m => m.VTCategoryID == LeaveCategoryId).FirstOrDefault();
            }

            return Json(objLeaveBalanceInfoModel, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsLeaveDatesAlreadyExists(int employeeID, string fromDate, string toDate)
        {
            bool IsExists = new VacationDB().IsLeaveDatesAlreadyExists(employeeID, fromDate, toDate);
            OperationDetails operationDetails = new OperationDetails();
            if (IsExists)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Leave requested/availed already for the selected dates.";
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = false;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsLeaveDatesAlreadyExistsForCurrentReq(int employeeID, string fromDate, string toDate, int? LeaveRequestID)
        {
            bool IsExists = new VacationDB().IsLeaveDatesAlreadyExistsForCurrentReq(employeeID, fromDate, toDate, LeaveRequestID);
            OperationDetails operationDetails = new OperationDetails();
            if (IsExists)
            {
                operationDetails.Success = true;
                operationDetails.Message = "Leave requested/availed already for the selected dates.";
                operationDetails.CssClass = "error";
            }
            else
            {
                operationDetails.Success = false;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetValidLeaveRequestDates(int employeeID, int vacationTypeID, string fromDate, string toDate)
        {
            VacationModel vacationModel = new VacationDB().GetValidLeaveRequestDates(employeeID, vacationTypeID, fromDate, toDate);
            return Json(vacationModel, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetNoOfWorkingDays(int employeeID, string fromDate, string toDate)
        {
            int noOfDays = new VacationDB().GetNoOfWorkingDays(employeeID, fromDate, toDate);
            return Json(noOfDays, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CancelLeaveRequest(int? formProcessId, string cancelComments)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int userId = objUserContextViewModel.UserId;
            operationDetails = formLeaveRequestDB.CancelLeaveRequest(formProcessId, cancelComments, userId);
            string resultMessage = operationDetails.Message;
            string result = (operationDetails.Success) ? "success" : "error";
            int insertedRowId = operationDetails.InsertedRowId;
            return Json(new { result = result, resultMessage = resultMessage, insertedRowId = insertedRowId }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public override ActionResult RejectForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormLeaveRequestDB formsDB = new FormLeaveRequestDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.RejectForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                    if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                    {
                        SendRejectionEmail(requestFormsApproverEmailModelList);
                        operationDetails.Message += @" & Final Email Notification send";
                    }

                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";
                    operationDetails.Message = "Error while rejecting forms request";
                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }
    }
}