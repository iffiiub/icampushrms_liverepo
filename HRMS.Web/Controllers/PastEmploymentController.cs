﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using System.IO;
using HRMS.Web.FileUpload;
using System.Configuration;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class PastEmploymentController : BaseController
    {
        //
        // GET: /PastEmployment/
        public ActionResult Index(int Id = 0)
        {
            ViewBag.EmployeeID = Id;
            return View();

        }

        int LoginUser = 1;
        public ActionResult GetPastEmploymentList(int EmployeeID = 0)
        {

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
           
            List<PastEmploymentModel> objPastEmploymentList = new List<PastEmploymentModel>();
            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();
            objPastEmploymentList = objPastEmploymentDB.GetPastEmploymentList(EmployeeID);
            //---------------------------------------------
            var vList = new object();
            vList = new
            {
                aaData = (from item in objPastEmploymentList
                          select new
                          {
                               Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddPastEmployment(" + item.PastEmploymentID.ToString() + ',' + item.EmployeeID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannelPastEmployment(" + item.PastEmploymentID.ToString() + ',' + item.EmployeeID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PastEmploymentID = item.PastEmploymentID,
                              FirstName = item.FirstName_1,
                              LastName = item.SurName_1,
                              CompanyName = item.CompanyName,
                              StartDate = (item.StartDate),
                              EndDate = (item.EndDate),
                              Department = item.Department,
                              Position = item.Position,
                              Role = item.Role,
                              refrence = item.EmpReferences,
                          }).ToArray() 
            };           
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int Id = 0, int EmployeeID = 0)
        {
            ViewBag.EmployeeID = EmployeeID;
            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();
            PastEmploymentModel objPastEmploymentModel = new PastEmploymentModel();
            if (Id != 0)
            {

                objPastEmploymentModel = objPastEmploymentDB.GetPastEmploymentByID(Id);
                objPastEmploymentModel.StartDate = objPastEmploymentModel.StartDate == "" ? "" : (objPastEmploymentModel.StartDate);
                objPastEmploymentModel.EndDate = objPastEmploymentModel.EndDate == "" ? "" : (objPastEmploymentModel.EndDate);

            }
            else
            {
                objPastEmploymentModel.EmployeeID = EmployeeID;
            }

            return View("Edit", objPastEmploymentModel);
        }


        public ActionResult UpdatePastEmplymentDetails(PastEmploymentModel PastEmploymentModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            int PastEmploymentId = 0;
            PastEmploymentModel.Department = (PastEmploymentModel.Department == null ? "" : PastEmploymentModel.Department);
            PastEmploymentModel.Role = (PastEmploymentModel.Role == null ? "" : PastEmploymentModel.Role);
            PastEmploymentModel.EmpReferences = (PastEmploymentModel.EmpReferences == null ? "" : PastEmploymentModel.EmpReferences);
            PastEmploymentModel.Position = (PastEmploymentModel.Position == null ? "" : PastEmploymentModel.Position);
            if (Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(PastEmploymentModel.StartDate)) > Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(PastEmploymentModel.EndDate)))
            {
                result = "error";
                resultMessage = "Start date must be before end date";
                return Json(new { PastEmploymentId = PastEmploymentId, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
            }


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();

            if (PastEmploymentModel.PastEmploymentID == 0)
            {
                PastEmploymentModel.CreatedBy = objUserContextViewModel.UserId;
                operationDetails = objPastEmploymentDB.AddPastEmployment(PastEmploymentModel);
                result = operationDetails.Message;
                PastEmploymentId = operationDetails.InsertedRowId;
            }
            else
            {
                PastEmploymentId = PastEmploymentModel.PastEmploymentID;
                PastEmploymentModel.ModifiedBy = objUserContextViewModel.UserId;
                operationDetails = objPastEmploymentDB.UpdatePastEmployment(PastEmploymentModel);
                result = operationDetails.Message;
            }

            if (result == "Success")
            {
                if (PastEmploymentModel.PastEmploymentID == 0)
                {
                    result = "success";
                    resultMessage = "Past employment details save successfully";
                }
                else if (PastEmploymentModel.PastEmploymentID != 0)
                {
                    result = "success";
                    resultMessage = "Past employment details save successfully";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Technical error has occurred";
            }

            return Json(new { PastEmploymentID = PastEmploymentId, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Delete(int Id)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();
            operationDetails = objPastEmploymentDB.DeletePastEmployment(Id);

            if (operationDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Record deleted successfully";
            }
            else
            {
                result = "error";
                resultMessage = "Technical error has occurred";
            }
            return Json(new { PastEmploymentID = Id, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetPastEmploymentDocsList(int Id = 0)
        {

            //-------------Data Objects--------------------
            PastEmploymentModel objPastEmployment = new PastEmploymentModel();

            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();
            objPastEmployment.objPastEmploymentList = objPastEmploymentDB.GetPastEmploymentDocs(Id);
            objPastEmployment.PastEmploymentID = Id;
            //---------------------------------------------

            //var vList = new object();

            //vList = new
            //{
            //    aaData = (from item in objPastEmploymentList
            //              select new
            //              {

            //                  PastEmploymentID = item.PastEmploymentID,
            //                  FileName = "<a href='" + item.FilePath + "' target='_blank'  title='" + item.FileName + "' ><i class='fa fa-pencil'></i> " + item.FileName + "</a>",
            //                  Description = item.Description,
            //                  Size = item.FileSize,
            //                  Type = item.FileType,                                                          
            //                  Actions = "<a class='btn btn-primary' onclick='DeleteChannel(" + item.AttatchmentId.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
            //              }).ToArray(),               
            //};


            return PartialView("PastEmploymentDocs", objPastEmployment);
        }




        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, int PastEmploymentID, string Description, string FileType)
        {
            OperationDetails operationDetails = new OperationDetails();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();
            PastEmploymentModel objPastEmploymentModel = new PastEmploymentModel();
            try
            {

                //for (int i = 0; i < Request.Files.Count; i++)
                //{

                //}
                string Fullpath = "";
                string filename = "";
                string type = "";
                if (file != null && file.ContentLength > 0)
                {
                    var Directory = Server.MapPath(ConfigurationManager.AppSettings["PastEmploymentDocsPath"].ToString());
                    //file.SaveAs(path);
                    FileUploadHelper.UploadFile(Directory, file, PastEmploymentID, out Fullpath, out filename);
                    objPastEmploymentModel.PastEmploymentID = PastEmploymentID;
                    objPastEmploymentModel.FileName = filename;
                    objPastEmploymentModel.FilePath = ConfigurationManager.AppSettings["PastEmploymentDocsPath"].ToString() + "/" + filename;
                    objPastEmploymentModel.FileType = FileType;
                    objPastEmploymentModel.Description = Description;
                    objPastEmploymentModel.CreatedBy = objUserContextViewModel.UserId;
                    if (file.ContentLength >= 1048576)
                    {
                        objPastEmploymentModel.FileSize = Convert.ToString(file.ContentLength / 1048576) + " MB";
                    }
                    else if (file.ContentLength >= 1024)
                    {
                        objPastEmploymentModel.FileSize = Convert.ToString(file.ContentLength / 1024) + " KB";
                    }
                    else if (file.ContentLength < 1024)
                    {
                        objPastEmploymentModel.FileSize = Convert.ToString(file.ContentLength) + " bytes";
                    }
                    operationDetails = objPastEmploymentDB.InsertPastEmploymentDocs(objPastEmploymentModel);
                    objPastEmploymentModel.objPastEmploymentList = objPastEmploymentDB.GetPastEmploymentDocs(PastEmploymentID);

                }

            }
            catch (Exception ex)
            {
                throw ex;
                //TempData["message"] = ex.ToString();
            }
            //return Json(new PastEmploymentModel { PastEmploymentID = objPastEmploymentModel.PastEmploymentID }, JsonRequestBehavior.AllowGet);
            return new HttpStatusCodeResult(200, "Inserted");
        }


        public ActionResult Remove(int Id)
        {
            string result = "";
            string resultMessage = "";

            PastEmploymentDB objPastEmploymentDB = new PastEmploymentDB();
            objPastEmploymentDB.DeletePastEmploymentDocs(Id);


            if (result == "Success")
            {
                result = "success";
                resultMessage = "Past Employment Attatchment Removed Successfully.";

            }
            else
            {
                result = "error";
                resultMessage = "Error occured while removing Past Employment Attatchment.";
            }
            return Json(new { AttatchmentId = Id, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DownloadPastEmploymentDocs(string filename)
        {
            string PastEmploymentDocsPath = Server.MapPath(ConfigurationManager.AppSettings["PastEmploymentDocsPath"].ToString());
            //StandardFramework objStandardFramework = new StandardFramework(); 
            string[] extension = filename.Split('.');
            var path = Path.Combine(PastEmploymentDocsPath, filename);
            return File(path, "application/all", extension[0] + "." + extension[1]);

        }




    }
}