﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class DepartmentEmployeeController : BaseController
    {
        //int CompanyID =0;
        //
        // GET: /DepartmentEmployee/


        public ActionResult Index(int Id,int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int DepartmentID = Id;
            //int CompanyID = objUserContextViewModel.CompanyId;
            DepartmentEmployeeViewModel viewModel = new DepartmentEmployeeViewModel();
            //DepartmentModel objDepartmentModel = new DepartmentModel();            
            //if (Id == 0)
            //{
            //    viewModel.departmentList = new SelectList(getAllDepartment(CompanyID), "DepartmentID", "DepartmentName_1");
            //}
            //else
            //{
            //    viewModel.departmentList = new SelectList(getAllDepartment(CompanyID), "DepartmentID", "DepartmentName_1", Id);
            //}

            DepartmentEmployeeDB objDepartmentDB = new DepartmentEmployeeDB();

            //DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            //EmployeeDetailsModel objEmployeeDetailModel = new EmployeeDetailsModel();
            //objEmployeeDetailModel.DepartmentEmployeeList = new SelectList(objDepartmentEmployeeDB.GetDepartmentByCompanyId(CompanyID), "EmployeeID", "FirstName_1");
            if (Id != -1)
            {
                viewModel.DepartmentEmployeeList = ((from items in objDepartmentDB.GetEmployeeByDepartmentId(Id, companyId)
                                                     select new EmployeeDetails
                                                     {
                                                         EmployeeID = items.EmployeeID,
                                                         FirstName_1 = items.FirstName_1,
                                                         SurName_1 = items.SurName_1,
                                                         FullName = items.FullName,
                                                         isLeader = items.isLeader,
                                                         isActive = items.isActive
                                                     }).OrderBy(m => m.FullName).ToList()).Where(m => m.isActive == true).ToList();
            }
            else
            {
                viewModel.DepartmentEmployeeList = new List<EmployeeDetails>();
            }
            //EmployeeDetailsModel objEmployeeDetailNotInDepartmentModel = new EmployeeDetailsModel();
            //objEmployeeDetailNotInDepartmentModel.DepartmentEmployeeNotInDepartmentList = new SelectList(objDepartmentEmployeeDB.GetEmployeeNotInDepartmentId(DepartmentID, CompanyID), "EmployeeID", "FirstName_1 ");
            //DepartmentEmployeeDB objDepartmentEmployeeNotInDepartmentDB = new DepartmentEmployeeDB();

            viewModel.DepartmentEmployeeNotInDepartmentList = (from items in objDepartmentDB.GetEmployeeNotInDepartmentId(Id, companyId, objUserContextViewModel.UserId)
                                                               select new EmployeeDetails
                                                               {
                                                                   EmployeeID = items.EmployeeID,
                                                                   FirstName_1 = items.FirstName_1,
                                                                   SurName_1 = items.SurName_1,
                                                                   FullName = items.FullName,
                                                                   isActive = items.isActive
                                                               }).Where(m => m.isActive == true).OrderBy(m => m.FirstName_1).ToList();      // To fill Department Leader Dropdownlist
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "Select Department Leader", Value = "0" });
            foreach (var items in viewModel.DepartmentEmployeeList)
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.FullName,
                    Value = items.EmployeeID.ToString(),
                    Selected = items.isLeader == 1 ? true : false
                };
                selectListItems.Add(selectListItem);
            }
            ViewBag.DeptList = selectListItems;

            //return View(new HRMS.Entities.ViewModel.DepartmentEmployeeViewModel { departmentList = objDepartmentModel.departmentList, DepartmentEmployeeList = objEmployeeDetailModel.DepartmentEmployeeList, DepartmentEmployeeNotInDepartmentList = objEmployeeDetailNotInDepartmentModel.DepartmentEmployeeNotInDepartmentList });            
            return View(viewModel);
        }





        public ActionResult GetDepartmentEmployeeList(int Id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int DepartmentID = Id;
            int CompanyID = objUserContextViewModel.CompanyId;
            DepartmentEmployeeViewModel viewModel = new DepartmentEmployeeViewModel();
            //DepartmentModel objDepartmentModel = new DepartmentModel();            
            //if (Id == 0)
            //{
            //    viewModel.departmentList = new SelectList(getAllDepartment(CompanyID), "DepartmentID", "DepartmentName_1");
            //}
            //else
            //{
            //    viewModel.departmentList = new SelectList(getAllDepartment(CompanyID), "DepartmentID", "DepartmentName_1", Id);
            //}

            DepartmentEmployeeDB objDepartmentDB = new DepartmentEmployeeDB();

            //DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            //EmployeeDetailsModel objEmployeeDetailModel = new EmployeeDetailsModel();
            //objEmployeeDetailModel.DepartmentEmployeeList = new SelectList(objDepartmentEmployeeDB.GetDepartmentByCompanyId(CompanyID), "EmployeeID", "FirstName_1");

            viewModel.DepartmentEmployeeList = (from items in objDepartmentDB.GetEmployeeByDepartmentId(Id, CompanyID)
                                                select new EmployeeDetails
                                                {
                                                    EmployeeID = items.EmployeeID,
                                                    FirstName_1 = items.FirstName_1,
                                                    SurName_1 = items.SurName_1,
                                                    FullName = items.FullName,
                                                    isLeader = items.isLeader,
                                                    isActive = items.isActive
                                                }).Where(m => m.isActive == true).OrderBy(m => m.FullName).ToList();

            //EmployeeDetailsModel objEmployeeDetailNotInDepartmentModel = new EmployeeDetailsModel();
            //objEmployeeDetailNotInDepartmentModel.DepartmentEmployeeNotInDepartmentList = new SelectList(objDepartmentEmployeeDB.GetEmployeeNotInDepartmentId(DepartmentID, CompanyID), "EmployeeID", "FirstName_1 ");
            //DepartmentEmployeeDB objDepartmentEmployeeNotInDepartmentDB = new DepartmentEmployeeDB();

            //viewModel.DepartmentEmployeeNotInDepartmentList = (from items in objDepartmentDB.GetEmployeeNotInDepartmentId(Id, CompanyID)
            //                                                   select new EmployeeDetails
            //                                                   {
            //                                                       EmployeeID = items.EmployeeID,
            //                                                       FirstName_1 = items.FirstName_1,
            //                                                       SurName_1 = items.SurName_1
            //                                                   }).OrderBy(m => m.FirstName_1).ToList();      // To fill Department Leader Dropdownlist
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "---Select Leader---", Value = "0" });
            foreach (var items in viewModel.DepartmentEmployeeList)
            {

                if (items.isLeader == 1)
                {
                    ViewBag.DeptLeader = items.FullName;
                }
            }


            //return View(new HRMS.Entities.ViewModel.DepartmentEmployeeViewModel { departmentList = objDepartmentModel.departmentList, DepartmentEmployeeList = objEmployeeDetailModel.DepartmentEmployeeList, DepartmentEmployeeNotInDepartmentList = objEmployeeDetailNotInDepartmentModel.DepartmentEmployeeNotInDepartmentList });            
            return PartialView("DepartmentEmployeeList", viewModel);
        }


        private List<DepartmentModel> getAllDepartment(int CompanyID)
        {
            DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            List<DepartmentModel> genderList = objDepartmentEmployeeDB.GetDepartmentByCompanyId(CompanyID);
            return genderList;
        }


        public ActionResult GetEmployeeByDepartmentId(int DepartmentID, int CompanyID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CompanyID = objUserContextViewModel.CompanyId;
            DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            List<EmployeeDetails> employeelist = objDepartmentEmployeeDB.GetEmployeeByDepartmentId(DepartmentID, CompanyID).Where(m => m.isActive == true).OrderBy(m => m.FullName).ToList();
            return View("_getAllDepartmentEmployees", employeelist);
        }

        public ActionResult getAllEmployeesNotInDepartment(int DepartmentID, int CompanyID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CompanyID = objUserContextViewModel.CompanyId;
            DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            List<EmployeeDetails> employeelist = objDepartmentEmployeeDB.GetEmployeeNotInDepartmentId(DepartmentID, CompanyID, objUserContextViewModel.UserId).Where(m => m.isActive == true).OrderBy(m => m.FirstName_1).ToList();
            return View("_getAllEmployeesNotInDepartment", employeelist);
        }

        [HttpPost]
        public ActionResult DeleteDepartmentEmployeeByEmployeeId(string EmployeeID)
        {
            DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            objDepartmentEmployeeDB.DeleteDepartmentEmployeeByEmployeeId(EmployeeID);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDepartmentEmployeeByEmployeeId(int departmentid, string EmployeeID)
        {

            DepartmentEmployeeDB objDepartmentEmployeeDB = new DepartmentEmployeeDB();
            objDepartmentEmployeeDB.UpdateDepartmentEmployeeByEmployeeId(departmentid, EmployeeID);
            return Json(0, JsonRequestBehavior.AllowGet);

        }



        //public ActionResult GetAllDepartment(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        //{
        //    //-------------------Grid Paging Info----------------------------------
        //    int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
        //    int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
        //    int numberOfRecords = Convert.ToInt16(iDisplayLength);
        //    string sortColumn = mDataProp_1;
        //    string sortOrder = sSortDir_0.ToUpper();
        //    int totalCount = 0;
        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    int companyid = objUserContextViewModel.CompanyId;
        //    //----------------------------------------------------------------------

        //    //-------------Data Objects--------------------
        //    List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
        //    DepartmentDB objDepartmentDB = new DepartmentDB();
        //    objDepartmentList = objDepartmentDB.GetAllDepartment(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount, companyid);


        //    //---------------------------------------------



        //    var vList = new object();

        //    vList = new
        //    {
        //        aaData = (from item in objDepartmentList
        //                  select new
        //                  {
        //                      Actions = "<a class='btn btn-primary' onclick='EditChannel(" + item.DepartmentId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> Edit</a><a class='btn btn-primary' onclick='DeleteChannel(" + item.DepartmentId.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
        //                      DepartmentID = item.DepartmentId,
        //                      CompanyId = item.CompanyId,
        //                      DepartmentName_1 = item.DepartmentName_1,
        //                      DepartmentName_2 = item.DepartmentName_2,
        //                      DepartmentName_3 = item.DepartmentName_3,
        //                      Description = item.Description,
        //                      StartDate = item.StartDate,
        //                      CreatedBy = item.CreatedBy,


        //                  }).ToArray(),
        //        recordsTotal = totalCount,
        //        recordsFiltered = totalCount
        //    };


        //    return Json(vList, JsonRequestBehavior.AllowGet);

        //}

        public JsonResult InsertEmployeeDepartment(int departmentid, int EmployeeID)
        {
            DepartmentEmployeeDB objDept = new DepartmentEmployeeDB();
            string result = objDept.InsertEmployeeDepartment(departmentid, EmployeeID);
            return Json(new { Result = String.Format("Success") });
        }

        public JsonResult RemoveEmployeeDepartment(int EmployeeID, int DepartmentID)
        {
            DepartmentEmployeeDB objDept = new DepartmentEmployeeDB();
            string result = objDept.RemoveEmployeeDepartment(DepartmentID, EmployeeID);
            return Json(new { Result = String.Format("Success") });
        }

        public JsonResult UpdateDepartmentEmployeeLeader(int EmployeeID, int DepartmentID)
        {
            DepartmentEmployeeDB objDept = new DepartmentEmployeeDB();
            string result = objDept.UpdateDepartmentEmployeeLeader(DepartmentID, EmployeeID);
            return Json(new { Result = String.Format("Success") });
        }

    }
}