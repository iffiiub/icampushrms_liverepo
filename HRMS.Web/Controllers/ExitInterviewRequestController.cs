﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class ExitInterviewRequestController : FormsController
    {
        // GET: ExitInterviewRequest
        ExitInterviewRequestDB objExitInterviewRequestDB;
        public ExitInterviewRequestController()
        {
            XMLLogFile = "LoggerExitInterviewRequest.xml";
            objExitInterviewRequestDB = new ExitInterviewRequestDB();
        }
        public ActionResult Index()
        {
            int FormProcessID = Session["ExitInterviewFormProcessID"] == null ? 0 : Convert.ToInt32(Session["ExitInterviewFormProcessID"]);
            if (FormProcessID == 0)
                return RedirectToAction("Index", "Home");

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ExitInterviewRequestModel objExitInterviewRequestModel = new ExitInterviewRequestModel();
            objExitInterviewRequestDB = new ExitInterviewRequestDB();

            if (FormProcessID > 0)
            {
                objExitInterviewRequestModel = objExitInterviewRequestDB.GetExitInterviewEmployeeDetails(FormProcessID);

                if (objExitInterviewRequestModel.ReqStatusID == (int)RequestStatus.Pending)
                {
                    objExitInterviewRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
                    objExitInterviewRequestModel.CreatedOn = DateTime.Today;
                    objExitInterviewRequestModel.ExitInterviewReasonOptionList = objExitInterviewRequestDB.GetExitInterviewReasonOptionList();
                    objExitInterviewRequestModel.ExitInterviewSupervisorOptionList = objExitInterviewRequestDB.GetExitInterviewSupervisorOptionList();
                    objExitInterviewRequestModel.ExitInterviewRatingOptionList = objExitInterviewRequestDB.GetExitInterviewRatingOptionList();
                    objExitInterviewRequestModel.ExitInterviewJobOptionList = objExitInterviewRequestDB.GetExitInterviewJobOptionsList();
                    objExitInterviewRequestModel.ExitInterviewServiceOptionList = objExitInterviewRequestDB.GetExitInterviewServiceOptionsList();
                    objExitInterviewRequestModel.FormProcessID = FormProcessID;
                    return View("Index", objExitInterviewRequestModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("Index", "Home");

        }

        public ActionResult Edit()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objExitInterviewRequestDB = new ExitInterviewRequestDB();
            int formProcessID = GetFormProcessSessionID();

            ExitInterviewRequestModel objExitInterviewRequestModel = new ExitInterviewRequestModel();

            if (formProcessID > 0)
            {
                objExitInterviewRequestModel = objExitInterviewRequestDB.GetExitInterviewEmployeeDetails(formProcessID);

                if (objExitInterviewRequestModel.ReqStatusID == (int)RequestStatus.Pending)
                {
                    objExitInterviewRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
                    objExitInterviewRequestModel.CreatedOn = DateTime.Today;
                    objExitInterviewRequestModel.ExitInterviewReasonOptionList = objExitInterviewRequestDB.GetExitInterviewReasonOptionList();
                    objExitInterviewRequestModel.ExitInterviewSupervisorOptionList = objExitInterviewRequestDB.GetExitInterviewSupervisorOptionList();
                    objExitInterviewRequestModel.ExitInterviewRatingOptionList = objExitInterviewRequestDB.GetExitInterviewRatingOptionList();
                    objExitInterviewRequestModel.ExitInterviewJobOptionList = objExitInterviewRequestDB.GetExitInterviewJobOptionsList();
                    objExitInterviewRequestModel.ExitInterviewServiceOptionList = objExitInterviewRequestDB.GetExitInterviewServiceOptionsList();
                    objExitInterviewRequestModel.FormProcessID = formProcessID;
                    return View("Index", objExitInterviewRequestModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult UpdateDetails()
        {
            ExitInterviewRequestModel objExitInterviewRequestModel = new ExitInterviewRequestModel();
            objExitInterviewRequestDB = new ExitInterviewRequestDB();
            int FormProcessID = Session["ExitInterviewFormProcessID"] == null ? 0 : Convert.ToInt32(Session["ExitInterviewFormProcessID"]);
            if (FormProcessID == 0)
            {
                FormProcessID = GetFormProcessSessionID();
            }
            objExitInterviewRequestModel = objExitInterviewRequestDB.GetExitInterviewEmployeeDetails(FormProcessID);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 30;

            if (FormProcessID > 0)
            {
                if (objExitInterviewRequestModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    objExitInterviewRequestModel = objExitInterviewRequestDB.GetExitInterviewRequest(FormProcessID);
                    objExitInterviewRequestModel.RequesterEmployeeID = objUserContextViewModel.UserId;
                    objExitInterviewRequestModel.CreatedOn = DateTime.Today;
                    objExitInterviewRequestModel.ExitInterviewReasonOptionList = objExitInterviewRequestDB.GetExitInterviewReasonOptionList();
                    objExitInterviewRequestModel.ExitInterviewSupervisorOptionList = objExitInterviewRequestDB.GetExitInterviewSupervisorOptionList();
                    objExitInterviewRequestModel.ExitInterviewRatingOptionList = objExitInterviewRequestDB.GetExitInterviewRatingOptionList();
                    objExitInterviewRequestModel.ExitInterviewJobOptionList = objExitInterviewRequestDB.GetExitInterviewJobOptionsList();
                    objExitInterviewRequestModel.ExitInterviewServiceOptionList = objExitInterviewRequestDB.GetExitInterviewServiceOptionsList();

                    objExitInterviewRequestModel.ExitInterviewReasonDetailsList = objExitInterviewRequestDB.GetExitInterviewReasonDetailList(objExitInterviewRequestModel.ID);
                    objExitInterviewRequestModel.ExitInterviewSupervisorDetailsList = objExitInterviewRequestDB.GetExitInterviewSupervisorDetailList(objExitInterviewRequestModel.ID);
                    objExitInterviewRequestModel.ExitInterviewJobDetailsList = objExitInterviewRequestDB.GetExitInterviewJobDetailList(objExitInterviewRequestModel.ID);
                    objExitInterviewRequestModel.ExitInterviewServiceDetailsList = objExitInterviewRequestDB.GetExitInterviewServiceDetailList(objExitInterviewRequestModel.ID);
                    objExitInterviewRequestModel.FormProcessID = FormProcessID;
                    return View("UpdateDetails", objExitInterviewRequestModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SaveExitInterviewRequest(string exitInterviewRequest)
        {
            ExitInterviewRequestModel objExitInterviewRequestModel = JsonConvert.DeserializeObject<ExitInterviewRequestModel>(exitInterviewRequest);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objExitInterviewRequestDB = new ExitInterviewRequestDB();
            OperationDetails operationDetails = new OperationDetails();
            operationDetails = objExitInterviewRequestDB.SaveExitInterviewRequest(objExitInterviewRequestModel, objUserContextViewModel.UserId);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? Session["ExitInterviewFormProcessID"] == null ? 0 : Convert.ToInt32(Session["ExitInterviewFormProcessID"]) : (int)id;
            if (formProcessID == 0)
            {
                formProcessID = GetFormProcessSessionID();
            }

            if (formProcessID == 0)
                return RedirectToAction("Index", "Home");

            ExitInterviewRequestModel objExitInterviewRequestModel = new ExitInterviewRequestModel();
            objExitInterviewRequestDB = new ExitInterviewRequestDB();
            objExitInterviewRequestModel = objExitInterviewRequestDB.GetExitInterviewRequest(formProcessID);
            objExitInterviewRequestModel.ExitInterviewReasonOptionList = objExitInterviewRequestDB.GetExitInterviewReasonOptionList();
            objExitInterviewRequestModel.ExitInterviewSupervisorOptionList = objExitInterviewRequestDB.GetExitInterviewSupervisorOptionList();
            objExitInterviewRequestModel.ExitInterviewRatingOptionList = objExitInterviewRequestDB.GetExitInterviewRatingOptionList();
            objExitInterviewRequestModel.ExitInterviewJobOptionList = objExitInterviewRequestDB.GetExitInterviewJobOptionsList();
            objExitInterviewRequestModel.ExitInterviewServiceOptionList = objExitInterviewRequestDB.GetExitInterviewServiceOptionsList();

            objExitInterviewRequestModel.ExitInterviewReasonDetailsList = objExitInterviewRequestDB.GetExitInterviewReasonDetailList(objExitInterviewRequestModel.ID);
            objExitInterviewRequestModel.ExitInterviewSupervisorDetailsList = objExitInterviewRequestDB.GetExitInterviewSupervisorDetailList(objExitInterviewRequestModel.ID);
            objExitInterviewRequestModel.ExitInterviewJobDetailsList = objExitInterviewRequestDB.GetExitInterviewJobDetailList(objExitInterviewRequestModel.ID);
            objExitInterviewRequestModel.ExitInterviewServiceDetailsList = objExitInterviewRequestDB.GetExitInterviewServiceDetailList(objExitInterviewRequestModel.ID);
            return View(objExitInterviewRequestModel);
        }

    }
}