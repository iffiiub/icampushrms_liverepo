﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using HRMS.DataAccess.GeneralDB;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO.Packaging;
using System.IO;
using HRMS.Entities.ViewModel;
using System.Text;
using Newtonsoft.Json;
using DocumentFormat.OpenXml;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text.RegularExpressions;
using System.Globalization;

namespace HRMS.Web.Controllers
{
    public class WPSController : BaseController
    {
        // GET: WPS
        public ActionResult Index()
        {
            WPSModel wpsModel = new WPSModel();
            wpsModel.CycleList = new PayCycleDB().GetAllPayCycle();
            wpsModel.BankList = new BankInformationDB().GetBanksList();
            wpsModel.TemplatesList = new WPSDB().GetAllWPSTemplates();
            wpsModel.CategoriesList = new SelectList(new BankInformationDB().GetPayCategoriesList(), "CategoryID", "CategoryName_1");
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.IsHideExtraFilter = new WPSDB().GetIsHideExtraFilter();
            return View(wpsModel);
        }

        public ActionResult PreviewWPS(int? categoryId, string companyId = null,int? GroupById=null,bool IsPreview = false, string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0,
            string bankIds = "", int cycleId = 0, int templateId = 0, string establishNumber = "", string bankCodeEmployer = "")
        {
            JsonResult json = new JsonResult();
            object list;
            DataTable table = GetTemplateData(categoryId, bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer, true, out list);

            /*if (templateId == 2 || templateId == 3)
            {
                //Generate html
                StringBuilder sb = new StringBuilder();

                List<string> columns = new List<string>();
                sb.Append("<thead><tr>");
                foreach (DataColumn column in table.Columns)
                {
                    sb.Append("<th>" + column.ColumnName + "</th>");
                    columns.Add(column.ColumnName);
                }
                sb.Append("</tr></thead>");

                foreach (DataRow row in table.Rows)
                {
                    sb.Append("<tbody><tr>");
                    foreach (string col in columns)
                    {
                        sb.Append("<td>" + row[col].ToString() + "</td>");
                    }
                    sb.Append("</tr></tbody");
                }
                
                json = Json(sb.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {*/
            var vList = new object();
            vList = new
            {
                aaData = list,
                recordsTotal = table.Rows.Count,
                recordsFiltered = table.Rows.Count
            };

            json = Json(vList, JsonRequestBehavior.AllowGet);
            //}

            return json;
        }

        public ActionResult GetWPSData(int? categoryId, string bankIds = "", string companyId = null, int? GroupById = null, bool IsPreview = false, int cycleId = 0, int templateId = 0, string establishNumber = "", string bankCodeEmployer = "")
        {
            companyId = string.IsNullOrEmpty(companyId) ? null : companyId;
            JsonResult json = new JsonResult();
            object list;
            DataTable table = GetTemplateData(categoryId, bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer, true, out list);

            //var vList = new object();
            //vList = new
            //{
            //    aaData = list,
            //    recordsTotal = table.Rows.Count,
            //    recordsFiltered = table.Rows.Count
            //};

            json = Json(list, JsonRequestBehavior.AllowGet);

            return json;
        }

        public DataTable GetTemplateData(int? categoryId, string bankIds, string companyId,int? GroupById,bool? IsPreview, int cycleId, int templateId, string establishNumber, string bankCodeEmployer, bool getList, out object list)
        {
            //Get cycle 
            PayCycleModel model = new PayCycleDB().GetPayCycleById(cycleId);
            DateTime fromDate = Convert.ToDateTime(CommonDB.SetCulturedDate(model.DateFrom));
            DateTime toDate = Convert.ToDateTime(CommonDB.SetCulturedDate(model.DateTo));
            JsonResult json = new JsonResult();
            DataTable table = new DataTable();

            //Get userid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            if (Session["userContext"] != null)
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            string userId = objUserContextViewModel.UserId.ToString();

            WPSDB objWPSDB = new WPSDB();
            list = new object();

            switch (templateId)
            {
                case 1://WPS
                    List<Template_WPS> lstWPS = objWPSDB.GetTemplateData_WPS(categoryId, companyId, GroupById, IsPreview, templateId, cycleId.ToString(), fromDate.Month.ToString().PadLeft(2, '0'),
                        fromDate.Year.ToString(), fromDate.ToString("dd/MMM/yyyy"), toDate.ToString("dd/MMM/yyyy"), bankIds, userId);
                    table = WPSController.ToDataTable(lstWPS);
                    if (!((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue))
                    {
                       if( table.Columns.Contains("GroupByColumn"))
                        {
                            table.Columns.Remove("GroupByColumn");
                        }
                    }
                        list = lstWPS.ToArray();
                    break;
                
                case 4://Non WPS
                    string currentDate = DateTime.Now.ToString("ddMMyyyyHHMMSS");
                    string MonthDays = DateTime.DaysInMonth(fromDate.Year, fromDate.Month).ToString();
                    string SCRFromDate = currentDate.Substring(0, currentDate.Length < 4 ? currentDate.Length : 4);
                    string SCRToDate = fromDate.Month.ToString() + fromDate.Year.ToString();

                    List<Template_NonWPS> lstNonWPS = objWPSDB.GetTemplateData_NonWPS(categoryId, companyId, GroupById, IsPreview, templateId, cycleId.ToString(), MonthDays, fromDate.Month.ToString().PadLeft(2, '0'),
                        fromDate.Year.ToString(), bankIds, establishNumber, bankCodeEmployer, SCRFromDate, SCRToDate, "", "", userId);
                    table = WPSController.ToDataTable(lstNonWPS);
                    if (!((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue))
                    {
                        if (table.Columns.Contains("GroupByColumn"))
                        {
                            table.Columns.Remove("GroupByColumn");
                        }
                    }
                    list = lstNonWPS.ToArray();
                    break;
                case 5://Non WPS CBD
                    List<Template_NonWPSCBD> lstNonWPSCBD = objWPSDB.GetTemplateData_NonWPSCBD(categoryId, companyId, GroupById, IsPreview, templateId, cycleId.ToString(), fromDate.Month.ToString().PadLeft(2, '0'),
                         fromDate.Year.ToString(), fromDate.ToString("dd/MMM/yyyy"), toDate.ToString("dd/MMM/yyyy"), bankIds, userId);
                    table = WPSController.ToDataTable(lstNonWPSCBD);
                    if (!((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue))
                    {
                        if (table.Columns.Contains("GroupByColumn"))
                        {
                            table.Columns.Remove("GroupByColumn");
                        }
                    }
                    list = lstNonWPSCBD.ToArray();
                    break;
                case 6:
                    table = objWPSDB.GetTemplateData_ExternalFileData(categoryId, companyId, GroupById, IsPreview, templateId, cycleId.ToString(), fromDate.Month.ToString().PadLeft(2, '0'),
                         fromDate.Year.ToString(), fromDate.ToString("dd/MMM/yyyy"), toDate.ToString("dd/MMM/yyyy"), bankIds, userId);
                   
                    if (!((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue))
                    {
                        if (table.Columns.Contains("GroupByColumn"))
                        {
                            table.Columns.Remove("GroupByColumn");
                        }
                    }
                    DataTable dt = new DataTable();
                    dt = table.Copy();
                    dt.Columns.RemoveAt(dt.Columns.Count - 1);
                    List<DataRow> lstDr = new List<DataRow>();
                    lstDr = dt.AsEnumerable().ToList();
                    int columnsCount = dt.Columns.Count;
                    List<object> lst = new List<object>();

                    foreach (DataRow dr in lstDr)
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        for (int i = 0; i < columnsCount; i++)
                        {
                            dict.Add(dt.Columns[i].ColumnName.Replace("_Amt", ""), dr[i].ToString());
                        }
                        lst.Add(dict);
                    }
                    list = lst;
                    break;
                default://Dynamic
                    table = objWPSDB.GetDynamicWPSData(categoryId, companyId, GroupById, IsPreview, templateId, cycleId.ToString(), fromDate.Month.ToString().PadLeft(2, '0'),
                        fromDate.Year.ToString(), fromDate.ToString("dd/MMM/yyyy"), toDate.ToString("dd/MMM/yyyy"), bankIds, userId);
                    if (!((IsPreview.HasValue && IsPreview.Value) && GroupById.HasValue))
                    {
                        if (table.Columns.Contains("GroupByColumn"))
                        {
                            table.Columns.Remove("GroupByColumn");
                        }
                    }
                    if (getList)
                        list = GetData(table);
                    break;
            }
            return table;
        }

        public ActionResult ExportToExcel(int? categoryId, string bankIds, string companyId,int? GroupById,bool? IsPreview, int cycleId, int templateId, string establishNumber, string bankCodeEmployer)
        {
            CommonHelper.CommonHelper.DeleteFile("/Downloads/WPS/");
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/WPS/");
            companyId = string.IsNullOrEmpty(companyId) ? null : companyId;
            object list;
            DataTable table = GetTemplateData(categoryId, bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer, false, out list);
            string fileName = "";
            string templateName = "Report" + DateTime.Now.ToString("ddMMyyyyhhmm");

            //Get filename
            WPSTemplateSettingsModel WPSTemplateSettingsModel = new WPSDB().GetWPSTemplatesById(templateId);
            fileName = WPSTemplateSettingsModel.FileName;
            templateName = WPSTemplateSettingsModel.TemplateName + DateTime.Now.ToString("ddMMyyyyhhmm");

            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/WPS/" + fileName + ".xlsx");
            string destinationFile = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/WPS/" + templateName + ".xlsx");

            List<String> columns = new List<string>();
            foreach (System.Data.DataColumn column in table.Columns)
            {
                columns.Add(column.ColumnName);
            }
            if(!WPSTemplateSettingsModel.IsFixedTemplate)
            {
                GetDynamicExcel(sourceFile, destinationFile, fileName, table, columns, templateId, cycleId, WPSTemplateSettingsModel.ExcelTableStartIndex, WPSTemplateSettingsModel.MergeHeadersExcelCellsCount, 0, WPSTemplateSettingsModel.ExcelColumnNameRowNo);
            }
            else if (templateId == 6)
            {
                CommonController commonController = new CommonController();
                destinationFile = commonController.GetExcelForExternalTemplate(sourceFile, destinationFile, fileName, table, columns, templateId, WPSTemplateSettingsModel.ExcelTableStartIndex, WPSTemplateSettingsModel.ExcelColumnNameRowNo);
            }
            else
            {
                if (templateId == 1)
                {
                    destinationFile = GetFormatedExcel(sourceFile, destinationFile, fileName, table, columns, templateId, WPSTemplateSettingsModel.ExcelTableStartIndex);
                }
                else
                {
                    destinationFile = GetExcel(sourceFile, destinationFile, fileName, table, columns, templateId, WPSTemplateSettingsModel.ExcelTableStartIndex, WPSTemplateSettingsModel.ExcelColumnNameRowNo);
                }
            }
            return File(destinationFile, "application/vnd.ms-excel", templateName + ".xlsx");
        }


        public ActionResult ExportToPDF(int? categoryId, string bankIds, string companyId,int? GroupById,bool? IsPreview, int cycleId, int templateId, string establishNumber, string bankCodeEmployer)
        {
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/WPS/");
            CommonHelper.CommonHelper.DeleteFile("/Downloads/WPS/");
            string fileName = "";
            string templateName = "Report" + DateTime.Now.ToString("ddMMyyyyhhmm");
            string type = "Report";
            companyId = string.IsNullOrEmpty(companyId) ? null : companyId;
            object list;
            DataTable table = GetTemplateData(categoryId, bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer, false, out list);
            WPSTemplateSettingsModel WPSTemplateSettingsModel = new WPSDB().GetWPSTemplatesById(templateId);
            fileName = WPSTemplateSettingsModel.FileName;
            templateName = WPSTemplateSettingsModel.TemplateName + DateTime.Now.ToString("ddMMyyyyhhmm");
            type = WPSTemplateSettingsModel.Type;

            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/WPS/" + fileName + ".pdf");
            string destinationFile = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/WPS/" + templateName + ".pdf");
            if (templateId == 5)
            {
                GenrateWpsCBDPDF(cycleId, templateId, table, sourceFile, destinationFile);
            }
            else
            {
                GenrateDynamicPDF(cycleId, templateId, table, destinationFile, type);
            }

            return File(destinationFile, "application/pdf", templateName + ".pdf");

        }


        public ActionResult ExportToSIF(int? categoryId, string bankIds, string companyId,int? GroupById,bool? IsPreview, int cycleId, int templateId, string establishNumber, string bankCodeEmployer)
        {
            CommonHelper.CommonHelper.DeleteFile("/Downloads/WPS/");
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/WPS/");
            companyId = string.IsNullOrEmpty(companyId) ? null : companyId;
            object list;
            DataTable table = GetTemplateData(categoryId, bankIds, companyId, GroupById, IsPreview, cycleId, templateId, establishNumber, bankCodeEmployer, false, out list);
            string templateName = "Report" + DateTime.Now.ToString("ddMMyyyyhhmm");

            //Get filename
            WPSTemplateSettingsModel WPSTemplateSettingsModel = new WPSDB().GetWPSTemplatesById(templateId);
            templateName = WPSTemplateSettingsModel.TemplateName + DateTime.Now.ToString("ddMMyyyyhhmm");
            if (WPSTemplateSettingsModel.IsSpecificSIFFileFormat)
            {
                templateName = string.Empty;
                string OrganizationCode = string.Empty;
                if (!string.IsNullOrEmpty(companyId))
                {
                    OrganizationCode = new CompanyDB().GetCompany(Convert.ToInt32(companyId)).OrganizationCode;
                }
                templateName = "0000000" + OrganizationCode + DateTime.Now.ToString("yyMMddhhmmss");
            }
            
            string destinationFile = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/WPS/" + templateName + ".sif");

            GetSIF(destinationFile, table);
            return File(destinationFile, "application/sif", templateName + ".sif");
        }


        public string GetExcel(string sourceFile, string destinationFile, string fileName, DataTable table, List<string> columns, int templateId, int ExcelTableStartIndex, int ExcelTableIndex)
        {
            // Create a copy of the template file and open the copy 
            System.IO.File.Copy(sourceFile, destinationFile, true);
            int numberofDecimal = new PayrollDB().GetDigitAfterDecimal();
            Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);

            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = ws.GetFirstChild<SheetData>();
                DefinedName definedNames = workbookPart.Workbook.Descendants<DefinedName>().FirstOrDefault();
                WorkbookStylesPart sp = workbookPart.WorkbookStylesPart;

                if (sheet == null)
                    throw new Exception("No sheet found in the template file. Please add the sheet");

                int rowIndex = 2;//default

                if (templateId == 5)
                {
                    rowIndex = ExcelTableStartIndex;
                    // Replace shared strings
                    SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                    IEnumerable<Text> sharedStringTextElements = sharedStringsPart.SharedStringTable.Descendants<Text>();

                    foreach (var text in sharedStringTextElements)
                    {
                        if (text.Text.Contains("companyname"))
                        {
                            SchoolInformation model = new SchoolInformationDB().GetSchoolInformation();
                            text.Text = text.Text.Replace("companyname", model.SchoolName_1);
                        }

                        if (text.Text.Contains("paydate"))
                            text.Text = text.Text.Replace("paydate", DateTime.Now.ToString("dd.MM.yyyy"));

                        if (text.Text.Contains("remarks"))
                            text.Text = text.Text.Replace("remarks", "" + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.ToString("yyyy"));
                    }
                }

                Row row;
                var lastRow = sheetData.Elements<Row>().LastOrDefault().RowIndex;
                rowIndex = Convert.ToInt32((UInt32)lastRow) + 1;
                //Insert a blank row
                //rowIndex = 18;
                row = new Row();
                row.RowIndex = (uint)rowIndex;
                sheetData.AppendChild(row);

                if (templateId == 4)
                {
                    if (ExcelTableStartIndex > 2)
                    {
                        ExcelTableStartIndex = ExcelTableStartIndex - rowIndex;
                        for (int i = 0; i <= ExcelTableStartIndex; i++)
                        {
                            Row row1 = new Row();
                            row1.AppendChild<Cell>(GetCell("", 5));
                            sheetData.InsertAt(row1, rowIndex);
                            rowIndex++;
                        }
                    }
                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        row = new Row();
                        int colCounter = 1;
                        foreach (string col in columns)
                        {
                            Cell cell = new Cell();
                            if (colCounter == 5)
                            {
                                if (numberofDecimal == 0)
                                {
                                    cell.StyleIndex = 7;
                                }
                                else if (numberofDecimal == 1)
                                {
                                    cell.StyleIndex = 8;
                                }
                                else if (numberofDecimal == 2)
                                {
                                    cell.StyleIndex = 9;
                                }
                                else if (numberofDecimal == 3)
                                {
                                    cell.StyleIndex = 10;
                                }
                                else if (numberofDecimal == 4)
                                {
                                    cell.StyleIndex = 11;
                                }
                                else if (numberofDecimal == 5)
                                {
                                    cell.StyleIndex = 12;
                                }
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                cell.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[col]))));
                            }
                            else
                            {
                                cell.StyleIndex = 6;
                                cell.DataType = CellValues.String;
                                cell.CellValue = new CellValue(dsrow[col].ToString());
                            }
                            row.AppendChild<Cell>(cell);
                            colCounter++;
                        }
                        row.RowIndex = (uint)rowIndex;
                        sheetData.InsertAt(row, rowIndex);
                        rowIndex++;
                    }
                }
                else
                {
                    int rowCount = ExcelTableStartIndex;
                    //int rowCount = 18;
                    int styleindex = 1;

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        //Row rowData = sheetData.Elements<Row>().Where(x => x.RowIndex == rowCount).FirstOrDefault();
                        Row rowData = sheetData.Elements<Row>().ElementAtOrDefault(rowCount);
                        var cellData = rowData.Elements<Cell>();
                        int colCounter = 1;

                        foreach (var iCellData in cellData)
                        {

                            switch (colCounter)
                            {
                                case 1:
                                    iCellData.DataType = new EnumValue<CellValues>(CellValues.Number);
                                    iCellData.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[0]))));
                                    iCellData.StyleIndex = 0;
                                    //iCellData.StyleIndex = (uint)styleindex;
                                    break;
                                case 2:
                                    iCellData.DataType = CellValues.String;
                                    iCellData.CellValue = new CellValue(dsrow[1].ToString());
                                    iCellData.StyleIndex = 39;
                                    break;
                                case 3:
                                    iCellData.DataType = CellValues.Number;
                                    iCellData.CellValue = new CellValue(dsrow[2].ToString());
                                    iCellData.StyleIndex = 39;
                                    break;
                                case 4:
                                    iCellData.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[3]))));
                                    if (numberofDecimal == 0)
                                    {
                                        iCellData.StyleIndex = 60;
                                    }
                                    else if (numberofDecimal == 1)
                                    {
                                        iCellData.StyleIndex = 59;
                                    }
                                    else if (numberofDecimal == 2)
                                    {
                                        iCellData.StyleIndex = 58;
                                    }
                                    else if (numberofDecimal == 3)
                                    {
                                        iCellData.StyleIndex = 57;
                                    }

                                    break;
                            }
                            colCounter++;
                        }
                        rowCount++;
                        styleindex++;
                    }
                    definedNames.Text = "'Payment Sheet'!$A$1:$D$" + (rowCount - 1).ToString();
                }
                document.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                document.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
                workbookPart.Workbook.Save();
                document.Close();
            }
            return destinationFile;
        }


        public string GetFormatedExcel(string sourceFile, string destinationFile, string fileName, DataTable table, List<string> columns, int templateId, int ExcelTableStartIndex)
        {
            // Create a copy of the template file and open the copy 
            System.IO.File.Copy(sourceFile, destinationFile, true);

            //Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);
            int numberofDecimal = new PayrollDB().GetDigitAfterDecimal();
            int ExcelMergeCellsIndex = 0;
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(destinationFile, true))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;

                SheetData sheetData = ws.GetFirstChild<SheetData>();
                if (sheet == null)
                    throw new Exception("No sheed found in the template file. Please add the sheet");
                //--------------------Insert record after ExcelTableStartIndex                                                       
                var lastRow = sheetData.Elements<Row>().LastOrDefault().RowIndex;
                int rowIndex;
                rowIndex = Convert.ToInt32((UInt32)lastRow);
                if (ExcelTableStartIndex > 1)
                {
                    ExcelTableStartIndex = ExcelTableStartIndex - rowIndex;
                    for (int i = 0; i < ExcelTableStartIndex; i++)
                    {
                        Row row1 = new Row();
                        row1.AppendChild<Cell>(GetCell(""));
                        row1.AppendChild<Cell>(GetCell(""));
                        row1.AppendChild<Cell>(GetCell(""));
                        row1.AppendChild<Cell>(GetCell(""));
                        row1.AppendChild<Cell>(GetCell(""));
                        sheetData.InsertAt(row1, rowIndex);
                        rowIndex++;
                    }
                }
                //---------------------------             

                int dataTypeCounter = 1;
                int rowCounter = 2;
                ExcelMergeCellsIndex = rowIndex;
                UInt32 newRowindex = (UInt32)rowIndex; //sheetData.Elements<Row>().LastOrDefault().RowIndex;
                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    Row row = new Row();
                    foreach (string col in columns)
                    {
                        Cell cell = new Cell();
                        cell.StyleIndex = 11;
                        switch (dataTypeCounter)
                        {
                            case 1:
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                CellValue cellValue1 = new CellValue(DoubleValue.FromDouble(Convert.ToDouble(dsrow[col])));
                                cell.AppendChild(cellValue1);
                                break;
                            case 2:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue2 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue2);
                                break;
                            case 3:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue3 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue3);
                                break;
                            case 4:
                                if (numberofDecimal == 0)
                                {
                                    cell.StyleIndex = 13;
                                }
                                else if (numberofDecimal == 1)
                                {
                                    cell.StyleIndex = 14;
                                }
                                else if (numberofDecimal == 2)
                                {
                                    cell.StyleIndex = 15;
                                }
                                else if (numberofDecimal == 3)
                                {
                                    cell.StyleIndex = 16;
                                }
                                else if (numberofDecimal == 4)
                                {
                                    cell.StyleIndex = 17;
                                }
                                else if (numberofDecimal == 5)
                                {
                                    cell.StyleIndex = 18;
                                }
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                CellValue cellValue4 = new CellValue(DoubleValue.FromDouble(Convert.ToDouble(dsrow[col])));
                                cell.AppendChild(cellValue4);
                                break;
                            case 5:
                                if (numberofDecimal == 0)
                                {
                                    cell.StyleIndex = 13;
                                }
                                else if (numberofDecimal == 1)
                                {
                                    cell.StyleIndex = 14;
                                }
                                else if (numberofDecimal == 2)
                                {
                                    cell.StyleIndex = 15;
                                }
                                else if (numberofDecimal == 3)
                                {
                                    cell.StyleIndex = 16;
                                }
                                else if (numberofDecimal == 4)
                                {
                                    cell.StyleIndex = 17;
                                }
                                else if (numberofDecimal == 5)
                                {
                                    cell.StyleIndex = 18;
                                }
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                CellValue cellValue5 = new CellValue(DoubleValue.FromDouble(Convert.ToDouble(dsrow[col])));
                                cell.AppendChild(cellValue5);
                                break;
                            case 6:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue6 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue6);
                                break;
                            case 7:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue7 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue7);
                                break;
                            case 8:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue8 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue8);
                                break;
                            case 9:

                                CellValue cellValue9 = new CellValue();
                                if (col == "Sal_Month")
                                {
                                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                    cellValue9 = new CellValue() { Text = dsrow[col].ToString() };
                                }
                                else
                                {
                                    cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                    cellValue9 = new CellValue(DoubleValue.FromDouble(Convert.ToDouble(dsrow[col])));
                                }
                                cell.AppendChild(cellValue9);

                                break;
                            case 10:
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                CellValue cellValue10 = new CellValue(DoubleValue.FromDouble(Convert.ToDouble(dsrow[col])));
                                cell.AppendChild(cellValue10);
                                break;
                            case 11:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue11 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue11);
                                break;
                            case 12:
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                CellValue cellValue12 = new CellValue() { Text = dsrow[col].ToString() };
                                cell.AppendChild(cellValue12);
                                break;
                            case 13:
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                CellValue cellValue13 = new CellValue(DoubleValue.FromDouble(Convert.ToDouble(dsrow[col])));
                                cell.AppendChild(cellValue13);
                                break;
                            default:
                                cell = new Cell() { };
                                break;
                        }
                        row.AppendChild<Cell>(cell);
                        dataTypeCounter++;

                    }
                    //row.RowIndex = new UInt32Value(rowInd); 
                    sheetData.InsertAt(row, Convert.ToInt32(newRowindex++));
                    rowIndex++;
                    //rowInd++;
                    dataTypeCounter = 1;
                    rowCounter++;
                }
                //ws.Save();
                workbookPart.Workbook.Save();
                document.Close();
            }
            return destinationFile;
        }
     

        public string GetDynamicExcel(string sourceFile, string destinationFile, string fileName, DataTable table, List<string> columns, int templateId, int cycleId, int? ExcelTableStartIndex, int MergeHeadersExcelCells, int ExcelTableMergeCellsIndex, int? ExcelColumnNameRowNo)
        {
            if (!ExcelColumnNameRowNo.HasValue)
                ExcelColumnNameRowNo = 0;

            bool headerAvaibility = false;
            PayCycleDB paycycledb = new PayCycleDB();
            int numberofDecimal = new PayrollDB().GetDigitAfterDecimal();
            var activeCycle = paycycledb.GetPayCycleById(cycleId);
            DateTime toDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(activeCycle.DateTo));
            columns = new List<string>();
            foreach (System.Data.DataColumn column in table.Columns)
            {
                if (column.ColumnName.ToLower() == "topheader")
                    headerAvaibility = true;
                else
                    columns.Add(column.ColumnName);
            }
            System.IO.File.Copy(sourceFile, destinationFile, true);
            int ExcelMergeCellsIndex = 0;
            Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);
            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = ws.GetFirstChild<SheetData>();
                if (sheet == null)
                    throw new Exception("No sheed found in the template file. Please add the sheet");

                // Replace shared strings
                SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                var rows = worksheetPart.Worksheet.Descendants<Row>();             
              
                int rowIndex = 1;
                if (headerAvaibility)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        Row headerRow = new Row();
                        if (dr["TopHeader"].ToString() == "1")
                        {
                            foreach (string col in columns)
                            {
                                if (dr[col].ToString().Trim().Length > 2)
                                {
                                    Cell cell = new Cell();
                                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                    cell.CellValue = new CellValue() { Text = dr[col].ToString() };
                                    cell.StyleIndex = 1;
                                    headerRow.AppendChild<Cell>(cell);
                                }
                            }
                            headerRow.RowIndex = (uint)rowIndex;
                            rowIndex++;
                            sheetData.AppendChild(headerRow);
                        }
                    }
                }
                ExcelMergeCellsIndex = 0;

                // --------------------- insert column row at ExcelColumnNameRowNo
                if (ExcelColumnNameRowNo > 0)
                {
                    if (ExcelTableStartIndex < ExcelColumnNameRowNo)
                        ExcelTableStartIndex = ExcelColumnNameRowNo + 1;

                    int? excelColumnNameRowNoCount = ExcelColumnNameRowNo - rowIndex;
                    for (int i = 0; i <= excelColumnNameRowNoCount; i++)
                    {
                        Row row3 = new Row();
                        Cell cell = new Cell();
                        string cellValue = "";
                        cell.StyleIndex = 1;
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue() { Text = cellValue };
                        row3.AppendChild<Cell>(cell);
                        sheetData.InsertAt(row3, rowIndex);
                        rowIndex++;
                    }
                    Row row1 = new Row();
                    foreach (string col in columns)
                    {
                        Cell cell = new Cell();
                        cell.DataType = new EnumValue<CellValues>(CellValues.String);                    
                        cell.CellValue = new CellValue() { Text = col.ToString() };
                        cell.StyleIndex = 8;                       
                        row1.AppendChild<Cell>(cell);
                    }
                    row1.RowIndex = (uint)rowIndex;
                    sheetData.AppendChild(row1);
                    rowIndex++;
                }
                // --------------------------

                //--------------------Insert record at ExcelTableStartIndex                                                       
                //var lastRow = sheetData.Elements<Row>().LastOrDefault().RowIndex;  

                if (ExcelTableStartIndex > 0)
                {
                    ExcelTableStartIndex = ExcelTableStartIndex - rowIndex;
                    for (int i = 0; i <= ExcelTableStartIndex; i++)
                    {
                        Row row3 = new Row();
                        Cell cell = new Cell();
                        string cellValue = "";
                        cell.StyleIndex = 1;
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue() { Text = cellValue };
                        row3.AppendChild<Cell>(cell);
                        sheetData.InsertAt(row3, rowIndex);
                        rowIndex++;
                    }
                }
                else
                {
                    ExcelMergeCellsIndex = 0;
                }
                //---------------------------             

                if (ExcelColumnNameRowNo == 0)
                {
                    Row row4 = new Row();
                    foreach (string col in columns)
                    {
                        Cell cell = new Cell();
                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                        cell.CellValue = new CellValue() { Text = col.ToString() };
                        cell.StyleIndex = 1;
                        row4.AppendChild<Cell>(cell);
                    }
                    row4.RowIndex = (uint)rowIndex;
                    sheetData.AppendChild(row4);
                    rowIndex++;
                }
                
                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    bool insertRow = false;
                    if (headerAvaibility)
                    {
                        if (dsrow["TopHeader"].ToString() == "0")
                        {
                            insertRow = true;
                        }
                        else
                            insertRow = false;
                    }
                    else
                    {
                        insertRow = true;
                    }
                    if (insertRow)
                    {
                        Row row2 = new Row();
                        foreach (string col in columns)
                        {
                            var dataTypeOfCell = dsrow[col].GetType();
                            Cell cell = new Cell();
                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                            if (dataTypeOfCell.FullName.ToString().ToLower() == "system.decimal")
                            {
                                if (numberofDecimal == 0)
                                {
                                    cell.StyleIndex = 2;
                                }
                                else if (numberofDecimal == 1)
                                {
                                    cell.StyleIndex = 3;
                                }
                                else if (numberofDecimal == 2)
                                {
                                    cell.StyleIndex = 4;
                                }
                                else if (numberofDecimal == 3)
                                {
                                    cell.StyleIndex = 5;
                                }
                                else if (numberofDecimal == 4)
                                {
                                    cell.StyleIndex = 6;
                                }
                                else if (numberofDecimal == 5)
                                {
                                    cell.StyleIndex = 7;
                                }
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                cell.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[col]))));
                            }
                            else
                            {
                                cell.StyleIndex = 1;
                                cell.DataType = CellValues.String;
                                cell.CellValue = new CellValue(dsrow[col].ToString());
                            }
                            row2.AppendChild<Cell>(cell);
                        }
                        row2.RowIndex = (uint)rowIndex;
                        sheetData.InsertAt(row2, rowIndex);
                        rowIndex++;
                    }
                }
                

                workbookPart.Workbook.Save();
                document.Close();
            }
            
            DataColumnCollection colmns = table.Columns;
            if (colmns.Contains("TopHeader"))
            {
                if (table.AsEnumerable().Where(r => r["TopHeader"].ToString() == "1").Count() > 0)
                {
                    DataTable mergeDt = table.AsEnumerable().Where(r => r["TopHeader"].ToString() == "1").CopyToDataTable();
                    CommonController commonController = new CommonController();
                    commonController.MergeExcelCells(destinationFile, mergeDt, "Sheet1", MergeHeadersExcelCells, ExcelMergeCellsIndex);
                }
            }

            return destinationFile;
        }

        public string GetSIF(string destinationFile, DataTable table)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(destinationFile);
            int HeaderColIndex = -1;
            bool IsHeaderExist = table.Columns.Contains("TopHeader");
            if (IsHeaderExist)
            {
                foreach (DataRow row in table.Select("TopHeader=0").ToList())
                {
                    List<object> lstItems = row.ItemArray.ToList();
                    if (row.Table.Columns["TopHeader"] != null)
                    {
                        HeaderColIndex = row.Table.Columns["TopHeader"].Ordinal;
                        if (HeaderColIndex >= 0)
                        {
                            lstItems.RemoveAt(HeaderColIndex);
                        }
                    }
                    file.WriteLine(string.Join(",", lstItems));
                }
            }
            else
            {
                foreach (DataRow row in table.Rows)
                {
                    file.WriteLine(string.Join(",", row.ItemArray));
                }
            }


            file.Close();

            return destinationFile;
        }


        public Cell GetCell(string val, UInt32 styleIndex = 0)
        {
            Cell cell = new Cell();
            if (styleIndex != 0)
                cell.StyleIndex = styleIndex;
            cell.DataType = cell.DataType = new EnumValue<CellValues>(CellValues.String);
            CellValue CellValue = new CellValue() { Text = val.ToString() };
            cell.AppendChild(CellValue);
            return cell;
        }

        private bool IsColumnExists(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public JsonResult GetColumnData(int? categoryId, string bankIds, string companyId,int? GroupById, bool? IsPreview, int cycleId, int templateId)
        {
            //Get cycle 
            PayCycleModel model = new PayCycleDB().GetPayCycleById(cycleId);
            DateTime fromDate = Convert.ToDateTime(CommonDB.SetCulturedDate(model.DateFrom));
            DateTime toDate = Convert.ToDateTime(CommonDB.SetCulturedDate(model.DateTo));
            JsonResult json = new JsonResult();
            DataTable table = new DataTable();
            companyId = string.IsNullOrEmpty(companyId) ? null : companyId;
            //Get userid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            if (Session["userContext"] != null)
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            string userId = objUserContextViewModel.UserId.ToString();

            WPSDB objWPSDB = new WPSDB();

            table = objWPSDB.GetDynamicWPSData(categoryId, companyId, GroupById, IsPreview, templateId, cycleId.ToString(), fromDate.Month.ToString().PadLeft(2, '0'),
                fromDate.Year.ToString(), fromDate.ToString("dd/MMM/yyyy"), toDate.ToString("dd/MMM/yyyy"), bankIds, userId);

            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (table.Columns[i].ColumnName == "TopHeader")
                {
                    table.Columns.RemoveAt(table.Columns.Count - 1);
                }
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            List<string> columns = new List<string>();
            foreach (DataColumn column in table.Columns)
            {
                string mData = column.ColumnName.Replace("_Amt", "");
                if (column.ColumnName == "GroupByColumn")
                {
                    sb.Append("{\"mData\":\"" + mData.Replace(" ", "") + "\", \"sTitle\":\"" + column.ColumnName + "\", \"visible\": false},");
                }
                else
                {
                    sb.Append("{\"mData\":\"" + mData.Replace(" ", "") + "\", \"sTitle\":\"" + column.ColumnName.Replace("_Amt", "") + "\"},");
                }
            }
            if (table.Columns.Count > 0)
            { sb.Remove(sb.Length - 1, 1); }
            sb.Append("]");
            // sb.Replace(" ", "");
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        public object GetData(DataTable table)
        {
            List<string> columns = new List<string>();
            foreach (DataColumn column in table.Columns)
            {
                columns.Add(column.ColumnName);
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            if (table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    sb.Append("{");
                    foreach (string col in columns)
                    {
                        if (!(row[col].ToString() == "0"))
                        {
                            sb.Append("\"" + col.Replace(" ", "") + "\":\"" + row[col].ToString() + "\",");
                        }
                        else
                        {
                            sb.Append("\"" + col.Replace(" ", "") + "\":\"\",");
                        }
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append("},");
                }
                sb.Remove(sb.Length - 1, 1);
            }
            sb.Append("]");
            //sb.Replace(" ", "");
            List<IDictionary<string, object>> list = JsonConvert.DeserializeObject<List<IDictionary<string, object>>>(sb.ToString());

            return list;
        }

        public string GetNonLinearExcel(string sourceFile, string destinationFile, string fileName, DataTable table, List<string> columns, int rowIndex)
        {
            // Create a copy of the template file and open the copy 
            System.IO.File.Copy(sourceFile, destinationFile, true);
            rowIndex = 5;
            int count = 1;
            decimal iTotal = 0;
            //Borders borders = new Borders() { Count = (UInt32Value)1U };
            // Stylesheet newstyle = new Stylesheet();
            //newstyle.Borders = borders;
            Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);

            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = ws.GetFirstChild<SheetData>();

                if (sheet == null)
                    throw new Exception("No sheed found in the template file. Please add the sheet");

                // Replace shared strings
                SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                IEnumerable<Text> sharedStringTextElements = sharedStringsPart.SharedStringTable.Descendants<Text>();

                foreach (var text in sharedStringTextElements)
                {
                    if (text.Text.Contains("companyname"))
                    {
                        SchoolInformation model = new SchoolInformationDB().GetSchoolInformation();
                        text.Text = text.Text.Replace("companyname", model.SchoolName_1);
                    }

                    if (text.Text.Contains("paydate"))
                        text.Text = text.Text.Replace("paydate", DateTime.Now.ToString("dd.MM.yyyy"));

                    if (text.Text.Contains("remarks"))
                        text.Text = text.Text.Replace("remarks", "Private " + DateTime.Now.ToString("MMM") + ", " + DateTime.Now.ToString("yyyy"));
                }

                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                var rows = worksheetPart.Worksheet.Descendants<Row>();

                //Insert a blank row
                Row row = new Row();
                row.RowIndex = (uint)rowIndex;
                sheetData.InsertAt(row, rowIndex);
                rowIndex++;

                row = new Row();
                Cell cell = new Cell();
                row.AppendChild<Cell>(getCell(""));
                row.AppendChild<Cell>(getCell("#"));
                foreach (string col in columns)
                {
                    cell = new Cell();
                    cell.DataType = CellValues.String;
                    cell.CellValue = new CellValue(col);
                    row.AppendChild<Cell>(cell);
                }
                row.RowIndex = (uint)rowIndex;
                sheetData.InsertAt(row, rowIndex);
                rowIndex++;

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    row = new Row();
                    row.AppendChild<Cell>(getCell(""));
                    row.AppendChild<Cell>(getCell(count.ToString()));
                    foreach (string col in columns)
                    {
                        cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(dsrow[col].ToString());
                        row.AppendChild<Cell>(cell);
                    }
                    iTotal = iTotal + Convert.ToDecimal(dsrow["Salary"].ToString() != "" ? dsrow["Salary"].ToString() : "0");
                    row.RowIndex = (uint)rowIndex;
                    sheetData.InsertAt(row, rowIndex);
                    rowIndex++;
                    count++;
                }
                //Insert a blank row
                row = new Row();
                row.AppendChild<Cell>(getCell(""));
                row.AppendChild<Cell>(getCell(""));

                row.AppendChild<Cell>(getCell("TOTAL"));
                row.AppendChild<Cell>(getCell(iTotal.ToString()));
                row.RowIndex = (uint)rowIndex;
                sheetData.InsertAt(row, rowIndex);
            }

            return destinationFile;
        }

        public Cell getCell(string val)
        {
            Cell cell = new Cell();
            cell.DataType = CellValues.String;
            cell.CellValue = new CellValue(val);
            return cell;
        }
        /*
        public ActionResult PreviewWPS(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0,
            string bankIds = "", int cycleId = 0, int templateId = 0, string establishNumber = "", string bankCodeEmployer = "")
        {
        //Get cycle 
        PayCycleModel model = new PayCycleDB().GetPayCycleById(cycleId);
        DateTime fromDate = Convert.ToDateTime(CommonDB.SetCulturedDate(model.DateFrom));
        DateTime toDate = Convert.ToDateTime(CommonDB.SetCulturedDate(model.DateTo));

        //Get userid
        UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        if (Session["userContext"] != null)
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        string userId = objUserContextViewModel.UserId.ToString();

        //Get data
        WPSDB objWPSDB = new WPSDB();
        DataTable dt = objWPSDB.GetWPSData(templateId, cycleId.ToString(), fromDate.Month.ToString().PadLeft(2, '0'),
            fromDate.Year.ToString(), fromDate.ToString("dd/MMM/yyyy"), toDate.ToString("dd/MMM/yyyy"), bankIds, userId);


        //Generate html
        StringBuilder sb = new StringBuilder();

        List<string> columns = new List<string>();
        sb.Append("<tr>");
        foreach (DataColumn column in dt.Columns)
        {
            sb.Append("<th>" + column.ColumnName + "</th>");
            columns.Add(column.ColumnName);
        }
        sb.Append("</tr>");

        foreach (DataRow row in dt.Rows)
        {
            sb.Append("<tr>");
            foreach (string col in columns)
            {
                sb.Append("<td>" + row[col].ToString() + "</td>");
            }
            sb.Append("</tr>");
        }
        //return string
        return Json(sb.ToString(), JsonRequestBehavior.AllowGet);

            DataTable table = new DataTable();
            JsonResult json = GetListByTemplate(bankIds, cycleId, templateId, out table);
            return json;
        }
        */

        public void GenrateDynamicPDF(int cycleId, int templateID, DataTable dtTable, string destFilePath, string fileName)
        {
            PayCycleDB paycycledb = new PayCycleDB();
            var activeCycle = paycycledb.GetPayCycleById(cycleId);
            DataColumnCollection columns = dtTable.Columns;
            if (columns.Contains("TopHeader"))
            {
                if (dtTable.AsEnumerable().Where(r => r["TopHeader"].ToString() == "1").Count() > 0)
                {
                    var query = dtTable.AsEnumerable().Where(r => r["TopHeader"].ToString() == "1");
                    foreach (var row in query.ToList())
                        dtTable.Rows.Remove(row);
                    dtTable.Columns.Remove("TopHeader");
                }
            }
            DateTime toDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(activeCycle.DateTo));
            Document pdfDocument;
            pdfDocument = new Document(PageSize.A4.Rotate(), 0f, 0f, 180f, 20f);   //Postrait
            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, new FileStream(destFilePath, FileMode.OpenOrCreate)))
            {
                wri.PageEvent = new itextEvents();
                pdfDocument.Open();
                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);

                iTextSharp.text.Font ReportDataFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));


                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font OtherHeadingContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font PaySlipName = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font PaySlipInfo = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font OtherHeadingContentBold = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, Black));

                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);


                float cellHeight = pdfDocument.TopMargin;
                // jpg.ScaleAbsolute(100, 120);

                // PDF document size      
                Rectangle page = pdfDocument.PageSize;
                PDFHeader(ref pdfDocument, wri, fileName);

                #region PageContent

                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                iTextSharp.text.Font tableHeader = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0));
                pdfDocument.SetMargins(0f, 0f, 20f, 20f);
                PdfPTable content = new PdfPTable(dtTable.Columns.Count);
                content.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                content.TotalWidth = page.Width;
                content.WidthPercentage = 95;

                foreach (DataColumn dc in dtTable.Columns)
                {
                    Paragraph p = new Paragraph("", PaySlipInfo);
                    p.Alignment = Element.ALIGN_CENTER;

                    PdfPCell tableColumnCell = new PdfPCell();
                    tableColumnCell.Border = PdfPCell.NO_BORDER;
                    tableColumnCell.FixedHeight = 20f;
                    tableColumnCell.AddElement(p);
                    tableColumnCell.PaddingBottom = 10f;
                    content.AddCell(tableColumnCell);
                }

                foreach (DataColumn dc in dtTable.Columns)
                {
                    if (dc.ToString().ToLower() != "topheader")
                    {
                        Paragraph p = new Paragraph(dc.ToString(), tableHeader);
                        p.Alignment = Element.ALIGN_CENTER;
                        PdfPCell tableColumnCell = new PdfPCell();
                        tableColumnCell.SetLeading(0.0f, 1f);
                        tableColumnCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        tableColumnCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tableColumnCell.PaddingBottom = 10f;
                        tableColumnCell.PaddingTop = 4f;
                        tableColumnCell.AddElement(p);
                        content.AddCell(tableColumnCell);
                    }
                    else
                    {

                    }
                }

                foreach (DataRow DR in dtTable.Rows)
                {

                    for (int i = 0; i < dtTable.Columns.Count; i++)
                    {
                        PdfPCell tableCell;
                        if (dtTable.Columns[i].ToString().ToLower().Contains("(ar)"))
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), arabicFont));
                        else
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                        tableCell.SetLeading(0.0f, 1f);
                        tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tableCell.PaddingBottom = 8f;
                        tableCell.PaddingTop = 4f;
                        content.AddCell(tableCell);
                    }
                }
                pdfDocument.Add(content);
                #endregion


                pdfDocument.Close();

            }
            //  return pdfDocument;

        }

        public void GenrateWpsCBDPDF(int cycleId, int templateID, DataTable dtTable, string sourceFile, string destinationFile)
        {
            ///                 
            using (var existingFileStream = new FileStream(sourceFile, FileMode.Open))
            using (var newFileStream = new FileStream(destinationFile, FileMode.Create))
            {
                // Open existing PDF
                var pdfReader = new PdfReader(existingFileStream);

                // PdfStamper, which will create
                var stamper = new PdfStamper(pdfReader, newFileStream);

                var form = stamper.AcroFields;
                List<string> fieldKeys = form.Fields.Keys.ToList();

                SchoolInformation schoolInfo = new SchoolInformationDB().GetSchoolInformation();
                Dictionary<string, string> dictHeaderDetails = new Dictionary<string, string>();
                dictHeaderDetails.Add("companyname", schoolInfo.SchoolName_1);
                dictHeaderDetails.Add("paydate", DateTime.Now.ToString("dd.MM.yyyy"));
                dictHeaderDetails.Add("remarks", "Pay Sundries of " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.ToString("yyyy"));

                foreach (string fieldKey in fieldKeys)
                {
                    string replacedtext = "";
                    dictHeaderDetails.TryGetValue(fieldKey, out replacedtext);
                    form.SetField(fieldKey, replacedtext);
                }

                PdfContentByte cb = stamper.GetOverContent(1);
                Rectangle rect = pdfReader.GetPageSizeWithRotation(1);

                //pdfDocument.Open();
                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);

                iTextSharp.text.Font ReportDataFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));


                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font OtherHeadingContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font PaySlipName = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font PaySlipInfo = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font OtherHeadingContentBold = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, Black));
                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));


                PdfPTable content = new PdfPTable(dtTable.Columns.Count);
                content.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                content.TotalWidth = rect.Width - 30;
                //content.WidthPercentage = 95;

                foreach (DataColumn dc in dtTable.Columns)
                {
                    Paragraph p = new Paragraph("", PaySlipInfo);
                    p.Alignment = Element.ALIGN_CENTER;
                    PdfPCell tableColumnCell = new PdfPCell();
                    tableColumnCell.Border = PdfPCell.NO_BORDER;
                    tableColumnCell.FixedHeight = 20f;
                    tableColumnCell.AddElement(p);
                    tableColumnCell.PaddingBottom = 10f;
                    content.AddCell(tableColumnCell);
                }

                foreach (DataColumn dc in dtTable.Columns)
                {
                    Paragraph p = new Paragraph(dc.ToString(), PaySlipInfo);
                    p.Alignment = Element.ALIGN_CENTER;
                    PdfPCell tableColumnCell = new PdfPCell();
                    tableColumnCell.SetLeading(0.0f, 1f);
                    tableColumnCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    tableColumnCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    tableColumnCell.PaddingBottom = 10f;
                    tableColumnCell.PaddingTop = 4f;
                    tableColumnCell.AddElement(p);
                    content.AddCell(tableColumnCell);
                }

                foreach (DataRow DR in dtTable.Rows)
                {

                    for (int i = 0; i < dtTable.Columns.Count; i++)
                    {
                        PdfPCell tableCell;
                        if (dtTable.Columns[i].ToString().ToLower().Contains("(ar)"))
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), arabicFont));
                        else
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                        tableCell.SetLeading(0.0f, 1f);
                        tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tableCell.PaddingLeft = 4f;
                        tableCell.PaddingRight = 4f;
                        tableCell.PaddingBottom = 8f;
                        tableCell.PaddingTop = 4f;
                        content.AddCell(tableCell);
                    }
                }

                ColumnText column = new ColumnText(stamper.GetOverContent(1));
                Rectangle rectPage1 = new Rectangle(-50, 20, 840, 380);
                column.SetSimpleColumn(rectPage1);
                column.AddElement(content);
                int pagecount = 1;
                Rectangle rectPage2 = new Rectangle(-50, 20, 840, 600);
                int status = column.Go();
                while (ColumnText.HasMoreText(status))
                {
                    status = triggerNewPage(stamper, pdfReader.GetPageSizeWithRotation(1), column, rectPage2, ++pagecount);
                }
                stamper.FormFlattening = true;
                stamper.Close();
                pdfReader.Close();
            }
        }

        public int triggerNewPage(PdfStamper stamper, Rectangle pagesize, ColumnText column, Rectangle rect, int pagecount)
        {
            stamper.InsertPage(pagecount, pagesize);
            PdfContentByte canvas = stamper.GetOverContent(pagecount);
            column.Canvas = canvas;
            column.SetSimpleColumn(rect);
            return column.Go();
        }

        public void PDFHeader(ref Document pdfDocument, PdfWriter wri, string fileName)
        {
            iTextSharp.text.Font ReportDataFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
            BaseColor Black = new BaseColor(0, 0, 0);

            BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
            iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, Black));
            iTextSharp.text.Font OtherHeadingContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, Black));
            iTextSharp.text.Font PaySlipName = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, Black));
            iTextSharp.text.Font PaySlipInfo = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, Black));
            iTextSharp.text.Font OtherHeadingContentBold = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, Black));


            //School Info
            //School Info
            SchoolInformation sc = new SchoolInformation();
            sc = new SchoolInformationDB().GetBasicSchoolInformation();
            string Schoolname = sc.SchoolName_1;
            if (!string.IsNullOrEmpty(sc.CampusName_1))
            {
                Schoolname += " " + "-" + " " + sc.CampusName_1;
            }
            string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);


            float cellHeight = pdfDocument.TopMargin;
            // jpg.ScaleAbsolute(100, 120);

            // PDF document size      
            Rectangle page = pdfDocument.PageSize;


            // create two column table
            PdfPTable head = new PdfPTable(2);
            head.TotalWidth = page.Width;
            head.LockedWidth = true;

            float[] widths = new float[] { 120f, 455f };
            head.SetWidths(widths);


            // add image; PdfPCell() overload sizes image to fit cell
            PdfPCell c = new PdfPCell(jpg, true);
            c.FixedHeight = cellHeight;

            c.Border = PdfPCell.NO_BORDER;
            c.PaddingLeft = 20f;
            c.PaddingTop = 30f;
            c.PaddingBottom = 20f;
            head.AddCell(c);
            // add the header text

            PdfPTable Title = new PdfPTable(1);
            float[] Titlewidths = new float[] { 500f };
            Title.SetWidths(Titlewidths);

            PdfPCell SchoolName = new PdfPCell(new Phrase(Schoolname, Heading));
            SchoolName.Border = PdfPCell.NO_BORDER;
            SchoolName.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell Address = new PdfPCell(new Phrase(sc.Address + " " + sc.City + " " + sc.Country + " P.O.Box: " + sc.POBox, OtherHeadingContent));
            Address.Border = PdfPCell.NO_BORDER;
            Address.HorizontalAlignment = Element.ALIGN_LEFT;
            Address.PaddingRight = 70F;
            PdfPCell Phone = new PdfPCell(new Phrase("Phone: " + sc.Phone, OtherHeadingContent));
            Phone.Border = PdfPCell.NO_BORDER;
            Phone.HorizontalAlignment = Element.ALIGN_LEFT;
            Phone.PaddingRight = 70F;
            PdfPCell Fax = new PdfPCell(new Phrase("Fax: " + sc.Fax, OtherHeadingContent));
            Fax.Border = PdfPCell.NO_BORDER;
            Fax.HorizontalAlignment = Element.ALIGN_LEFT;
            Fax.PaddingRight = 70F;
            PdfPCell Email = new PdfPCell(new Phrase("EmailId: " + sc.EmailId, OtherHeadingContent));
            Email.Border = PdfPCell.NO_BORDER;
            Email.HorizontalAlignment = Element.ALIGN_LEFT;
            Email.PaddingRight = 70F;
            PdfPCell WebSite = new PdfPCell(new Phrase("Website: " + sc.Website, OtherHeadingContent));
            WebSite.Border = PdfPCell.NO_BORDER;
            WebSite.HorizontalAlignment = Element.ALIGN_LEFT;
            WebSite.PaddingRight = 70F;

            Title.AddCell(SchoolName);
            Title.AddCell(Address);
            Title.AddCell(Phone);
            Title.AddCell(Fax);
            Title.AddCell(Email);
            Title.AddCell(WebSite);
            Title.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell c1 = new PdfPCell();
            c1.AddElement(Title);
            c1.Border = PdfPCell.NO_BORDER;
            c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape
            c1.PaddingTop = 40F;
            c1.PaddingLeft = 0f;
            c1.PaddingRight = 200f;
            c1.FixedHeight = cellHeight;
            head.AddCell(c1);

            head.WriteSelectedRows(
              0, -1,  // first/last row; -1 flags all write all rows
              0,      // left offset
                      // ** bottom** yPos of the table
              page.Height - cellHeight + head.TotalHeight,
              wri.DirectContent
            );
            PdfContentByte cb = wri.DirectContent;
            cb.SetLineWidth(3f);
            cb.SetColorStroke(new CMYKColor(0.611f, 0.416f, 0f, 0.416f));
            cb.MoveTo(0, 425);
            cb.LineTo(510, 425);
            cb.Stroke();



            cb.SetLineWidth(3f);
            cb.MoveTo(750, 425);
            cb.LineTo(842, 425);
            cb.Stroke();

            PdfPCell WPSReportName = new PdfPCell(new Phrase(fileName, Heading));
            WPSReportName.Border = PdfPCell.NO_BORDER;
            WPSReportName.HorizontalAlignment = Element.ALIGN_CENTER;
            WPSReportName.VerticalAlignment = Element.ALIGN_MIDDLE;
            PdfPTable table = new PdfPTable(1);
            table.TotalWidth = 220f;
            table.AddCell(WPSReportName);
            table.WriteSelectedRows(0, -1, 520, 440, cb);


        }


        public ActionResult isPDFEnabledForWPSReport(int templateId)
        {
            //bool isPDFEnabled = false;
            var btnEnableData = new WPSDB().isPDFEnabled(templateId);
            JsonResult json = new JsonResult();
            json = Json(btnEnableData, JsonRequestBehavior.AllowGet);

            return json;
        }

        public static DataTable ReadExcelHeaderAsDataTable(string fileName, int headerIndex)
        {
            DataTable dataTable = new DataTable();
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                int rowsCount = rows.Count();

                for (int k = 0; k < headerIndex; k++)
                {
                    foreach (Cell cell in rows.ElementAt(k))
                    {
                        if (cell.InnerXml != "")
                        {
                            dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                        }
                    }
                }

                for (int k = 0; k < headerIndex; k++)
                {
                    DataRow dataRow = dataTable.NewRow();
                    foreach (Cell cell in rows.ElementAt(k))
                    {
                        dataRow[k] = cell.CellReference.Value;

                    }
                    dataTable.Rows.Add(dataRow);
                }



            }


            return dataTable;
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public static void InsertText(string docName, string text)
        {
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
            {
                // Get the SharedStringTablePart. If it does not exist, create a new one.
                SharedStringTablePart shareStringPart;
                if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
                {
                    shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                }
                else
                {
                    shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                }

                // Insert the text into the SharedStringTablePart.
                int index = InsertSharedStringItem(text, shareStringPart);

                // Insert a new worksheet.
                WorksheetPart worksheetPart = InsertWorksheet(spreadSheet.WorkbookPart);

                // Insert cell A1 into the new worksheet.
                Cell cell = InsertCellInWorksheet("A", 1, worksheetPart);

                // Set the value of cell A1.
                cell.CellValue = new CellValue(index.ToString());
                cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);

                // Save the new worksheet.
                worksheetPart.Worksheet.Save();
            }
        }
        private static int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        private static WorksheetPart InsertWorksheet(WorkbookPart workbookPart)
        {
            // Add a new worksheet part to the workbook.
            WorksheetPart newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            newWorksheetPart.Worksheet.Save();

            Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = workbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new sheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            string sheetName = "Sheet" + sheetId;

            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);
            workbookPart.Workbook.Save();

            return newWorksheetPart;
        }

        // Given a column name, a row index, and a WorksheetPart, inserts a cell into the worksheet. 
        // If the cell already exists, returns it. 
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (cell.CellReference.Value.Length == cellReference.Length)
                    {
                        if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                        {
                            refCell = cell;
                            break;
                        }
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }


    }

    public class itextEvents : IPdfPageEvent
    {
        PdfContentByte pdfContent;
        public static int pageCount = 0;
        public itextEvents()
        {
            pageCount = 0;
        }
        public void OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title)
        {
            throw new NotImplementedException();
        }

        public void OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnCloseDocument(PdfWriter writer, Document document)
        {
            //  throw new NotImplementedException();  


        }

        public void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            Rectangle page = document.PageSize;
            iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
            iTextSharp.text.Font Heading1 = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
            iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
            var userContext = (HRMS.Entities.ViewModel.UserContextViewModel)HttpContext.Current.Session["userContext"];
            PdfPTable footer = new PdfPTable(3);
            footer.HeaderRows = 1;
            footer.TotalWidth = page.Width;
            footer.WidthPercentage = 95;
            float[] widths = new float[] { 180f, 460f, 180f };  //landscape
            footer.SetWidths(widths);

            PdfPCell footerCell1 = new PdfPCell(new Phrase(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"), DateFont));
            footerCell1.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            footerCell1.FixedHeight = 20f;
            footerCell1.Border = PdfPCell.NO_BORDER;
            footerCell1.PaddingLeft = 0f;
            footerCell1.PaddingTop = 0f;
            footerCell1.PaddingBottom = 0f;
            footer.AddCell(footerCell1);

            PdfPCell footerCell2 = new PdfPCell(new Phrase("Printed By: " + userContext.UserRolls.FirstOrDefault(), DateFont));
            footerCell2.HorizontalAlignment = PdfPCell.ALIGN_MIDDLE;
            footerCell2.PaddingLeft = 100f;

            footerCell2.FixedHeight = 20f;
            footerCell2.Border = PdfPCell.NO_BORDER;
            footerCell2.PaddingTop = 0f;
            footerCell2.PaddingBottom = 0f;
            footer.AddCell(footerCell2);

            PdfPCell footerCell3 = new PdfPCell(new Phrase("Page No. " + (++pageCount).ToString(), DateFont));
            var id = footerCell3.ID;
            footerCell3.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
            footerCell3.FixedHeight = 20f;
            footerCell3.Border = PdfPCell.NO_BORDER;
            footerCell3.PaddingLeft = 0f;
            footerCell3.PaddingTop = 0f;
            footerCell3.PaddingBottom = 0f;
            footerCell3.PaddingRight = 40f;
            footer.AddCell(footerCell3);
            footer.WriteSelectedRows(0, -1, 20, document.Bottom, writer.DirectContent);

            //set pdfContent value
            pdfContent = writer.DirectContent;
            //Move the pointer and draw line to separate header section from rest of page
            pdfContent.MoveTo(30, document.PageSize.Height - 35);
            //  pdfContent.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 35);
            pdfContent.Stroke();
        }

        public void OnGenericTag(PdfWriter writer, Document document, Rectangle rect, string text)
        {
            throw new NotImplementedException();
        }

        public void OnOpenDocument(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

        public void OnParagraph(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title)
        {
            throw new NotImplementedException();
        }

        public void OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnStartPage(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

    }
}
