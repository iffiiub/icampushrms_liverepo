﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using HRMS.Entities.ViewModel;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class LateDeductionController : BaseController
    {
        //
        // GET: /LateDeduction/
        public ActionResult Index()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            AttendanceSetupModel attendanceSetupModel = new AttendanceSetupModel();
            AttendenceSetupDB attendenceSetupDB = new AttendenceSetupDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            PayLateTypeDB payLateTypeDB = new PayLateTypeDB();
            bool isDeductionRulesEnable = new DeductionDB().GetDeductionRuleSetting().AppliedDeductionRules;
            ViewBag.isDeductionRulesEnable = isDeductionRulesEnable;
            List<SelectListItem> lstDeductionType = new List<SelectListItem>();
            lstDeductionType.Add(new SelectListItem() { Text = "Late In", Value = "1" });
            lstDeductionType.Add(new SelectListItem() { Text = "Early Out", Value = "2" });

            ViewBag.DeductionType = new SelectList(lstDeductionType, "Value", "Text");
            lateDeductionModel.payLateTypeList = payLateTypeDB.GetPayLateTypeList();
            if (Session["GetAllList"] != null)
                lateDeductionModel.lateDeductionList = lateDeductionDB.GetLateDeductionList(lateDeductionModel).Where(x => x.IsConfirmed == false).ToList();

            lateDeductionModel.attendanceSetupModel = attendenceSetupDB.GetLateDeductionMinutesAndPecentage();
            PayCycleDB objPayCycleDB = new PayCycleDB();
            selectListItems = new List<SelectListItem>();

            foreach (var items in objPayCycleDB.GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                };
                selectListItems.Add(selectListItem);
            }
            bool isDeductibleSalaryAllowancePresent = new AdditionPaidCycleDB().CheckDeductibleSalaryAllowancePresent();
            ViewBag.CycleList = selectListItems;
            ViewBag.IsDeductibleAllowancePresent = isDeductibleSalaryAllowancePresent;
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();
            AcademicYearModel objYearModel = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault();

            bool isAcYearWarningEnable = false;
            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 3)
            {
                isAcYearWarningEnable = true;
            }
            ViewBag.ShowAcYearWarning = isAcYearWarningEnable;

            bool IsShowCycleWarning = false;
            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 1)
            {
                IsShowCycleWarning = true;
            }
            ViewBag.ShowCycleWarning = IsShowCycleWarning;
            UserRoleDB userRoleDB = new UserRoleDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();
            ViewBag.PermissionList = PermissionList;
            return View(lateDeductionModel);
        }

        public ActionResult GetAll()
        {
            Session["GetAllList"] = "All";
            return RedirectToAction("index");
        }

        public ActionResult FilteredLateDeductionList(string FromDate, string ToDate)
        {
            LateDeductionDB objLateDeductionDB = new LateDeductionDB();
            PayLateTypeDB payLateTypeDB = new PayLateTypeDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.payLateTypeList = payLateTypeDB.GetPayLateTypeList();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.lateDeductionList = objLateDeductionDB.GetLateDeductionList(lateDeductionModel).Where(x => x.IsConfirmed == false).ToList();
            return View(lateDeductionModel);
        }


        public ActionResult FilteredLateDeductionListWithMinSetup(string FromDate, string ToDate)
        {
            AttendenceSetupDB objAttendenceSetupDB = new AttendenceSetupDB();
            AttendanceSetupModel objAttendanceSetupModel = new AttendanceSetupModel();
            objAttendanceSetupModel = objAttendenceSetupDB.GetLateDeductionMinutesAndPecentage();
            LateDeductionDB objLateDeductionDB = new LateDeductionDB();
            PayLateTypeDB payLateTypeDB = new PayLateTypeDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.payLateTypeList = payLateTypeDB.GetPayLateTypeList();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.lateDeductionList = objLateDeductionDB.GetLateDeductionList(lateDeductionModel).Where(x => x.LateMinutes > objAttendanceSetupModel.LateMinutes).ToList();
            return View(lateDeductionModel);
        }

        //public ActionResult GetLateDeduction() {
        //    AttendanceSetupModel attendanceSetupModel = new AttendanceSetupModel();
        //    AttendenceSetupDB attendenceSetupDB = new AttendenceSetupDB();
        //    LateDeductionModel lateDeductionModel = new LateDeductionModel();
        //    LateDeductionDB lateDeductionDB = new LateDeductionDB();
        //    PayLateTypeDB payLateTypeDB = new PayLateTypeDB();
        //    lateDeductionModel.payLateTypeList = payLateTypeDB.GetPayLateTypeList();
        //    lateDeductionModel.lateDeductionList = lateDeductionDB.GetLateDeductionList(lateDeductionModel);
        //    lateDeductionModel.attendanceSetupModel = attendenceSetupDB.GetLateDeductionMinutesAndPecentage();
        //    return View(lateDeductionModel);


        //}


        public ActionResult GetLateDeductionList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Id", int iSortCol_0 = 0)
        {
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0, companyId = 0;
            PayLateTypeDB payLateTypeDB = new PayLateTypeDB();
            List<PayLateTypeModel> payLateTypeList = payLateTypeDB.GetPayLateTypeList();
            List<LateDeductionModel> lateDeductionList = new List<LateDeductionModel>();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionList = lateDeductionDB.GetLateDeductionList(lateDeductionModel);
            var vList = new
            {
                aaData = (from item in lateDeductionList
                          select new
                          {
                              Checkboxes = "<input data-rowID type=checkbox class='checkbox' id='" + item.EmployeeID + "'/>",
                              EmployeeName = item.EmployeeName,
                              LateMinutes = item.LateMinutes,
                              Amount = item.Amount,
                              EmployeeID = item.EmployeeID

                          }).ToArray(),
                recordsTotal = payLateTypeList.Count,
                recordsFiltered = payLateTypeList.Count
            };

            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDeductableEmployeeList(bool IsExcused, int DeductionType, string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Id", int iSortCol_0 = 0, string FromDate = "", string ToDate = "")
        {
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormateForLateAndEarly();
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0, companyId = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            int? UserId = null;
            UserId = objUserContextViewModel.UserId;
            PayLateTypeDB payLateTypeDB = new PayLateTypeDB();
            List<PayLateTypeModel> payLateTypeList = payLateTypeDB.GetPayLateTypeList();
            List<LateDeductionModel> lateDeductionList = new List<LateDeductionModel>();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.DeductionType = DeductionType;
            if (IsExcused == false)
            {
                lateDeductionModel.excused = null;
            }
            else
            {
                lateDeductionModel.excused = IsExcused;
            }
            lateDeductionList = lateDeductionDB.GetDeductableEmployeeList(lateDeductionModel, UserId);
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();
            int NullCyclesCount = 0;
            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 1)
            {
                NullCyclesCount = lateDeductionList.Where(m => m.CycleID == 0).Count();
            }

            UserRoleDB userRoleDB = new UserRoleDB();
            List<int> avilablePermisssionForUser = null;
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();
            var PermissionList = userRoleDB.GetOtherPermission();
            PermissionList = PermissionList.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();

            bool MarkingExcused = PermissionList.Where(x => x.OtherPermissionName.Trim() == "Marking Execused").Count() > 0 ? true : false;
            bool MarkingDeduct = PermissionList.Where(x => x.OtherPermissionName.Trim() == "Marking Deduct").Count() > 0 ? true : false;

            var vList = new
            {
                aaData = (from item in lateDeductionList
                          select new
                          {
                              EmployeeName = item.EmployeeName,
                              AttendanceDate = item.AttendanceDate,
                              Time = item.Time,
                              DeductableMinutes = item.DeductableMinutes,
                              excused = item.excused == true ? (MarkingExcused == true ? "<input type='checkbox' class='checkbox chkExcusedCheck' checked id='chkExcused" + item.PayEmployeeLateID + "' onclick='updateExcusedStatus(" + item.PayEmployeeLateID + ",this)' />" : "<input type='checkbox' disabled class='checkbox chkExcusedCheck' checked id='chkExcused" + item.PayEmployeeLateID + "' onclick='updateExcusedStatus(" + item.PayEmployeeLateID + ",this)' />") : (MarkingExcused == true ? "<input type='checkbox' class='checkbox chkExcusedCheck'  id='chkExcused" + item.PayEmployeeLateID + "'  onclick='updateExcusedStatus(" + item.PayEmployeeLateID + ",this)'/>" : "<input type='checkbox' disabled class='checkbox chkExcusedCheck'  id='chkExcused" + item.PayEmployeeLateID + "'  onclick='updateExcusedStatus(" + item.PayEmployeeLateID + ",this)'/>"),
                              deduct = item.deduct == true ? (MarkingDeduct == true ? "<input type='checkbox' class='checkbox chkDeductCheck' checked  id='chkDeduct" + item.PayEmployeeLateID + "' onclick='updateDeductionStatus(" + item.PayEmployeeLateID + ",this)' />" : "<input type='checkbox' disabled class='checkbox chkDeductCheck' checked  id='chkDeduct" + item.PayEmployeeLateID + "' onclick='updateDeductionStatus(" + item.PayEmployeeLateID + ",this)' />") : (MarkingDeduct == true ? "<input type = 'checkbox' class='checkbox chkDeductCheck'  id='chkDeduct" + item.PayEmployeeLateID + "'  onclick='updateDeductionStatus(" + item.PayEmployeeLateID + ",this)'/>" : "<input type = 'checkbox' disabled class='checkbox chkDeductCheck'  id='chkDeduct" + item.PayEmployeeLateID + "'  onclick='updateDeductionStatus(" + item.PayEmployeeLateID + ",this)'/>"),
                              Amount = item.Amount.ToString(AmountFormat),
                              EmployeeID = item.EmployeeID,
                              EmployeeAlternativeId = item.EmployeeAlterNativeId,
                              DelayToOtherWorkers = item.DelayToOtherWorkers == true ? "<input type='checkbox' class='checkbox' checked  id='chkDelayToOtherWorkers" + item.PayEmployeeLateID + "' onclick='updateAmountForDelayCause(" + item.PayEmployeeLateID + "," + item.EmployeeID + "," + item.DeductableMinutes + "," + item.CycleID + "," + item.CountLateInCycle + ",this)' />" : "<input type='checkbox' class='checkbox'  id='chkDelayToOtherWorkers" + item.PayEmployeeLateID + "'  onclick='updateAmountForDelayCause(" + item.PayEmployeeLateID + "," + item.EmployeeID + "," + item.DeductableMinutes + "," + item.CycleID + "," + item.CountLateInCycle + ",this)'/>",
                              NoOfNullCycleCount = NullCyclesCount,
                              LateReason = item.LateReason,
                              Actions = "<a class='btn btn-success  btn-rounded btn-condensed btn-sm btn-space' onclick='UpdateLateReason(" + item.PayEmployeeLateID.ToString() + ")' title=" + (DeductionType == 1 ? "'Enter Late Reason'" : "'Enter Early Reason'") + " )><i class='fa fa-file-text-o'></i> </a>"
                          }).ToArray(),
                recordsTotal = payLateTypeList.Count,
                recordsFiltered = payLateTypeList.Count
            };

            var serializer = new JavaScriptSerializer();


            serializer.MaxJsonLength = Int32.MaxValue;

            var resultData = new { Value = "foo", Text = "var" };
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };

            return result;
        }



        public ActionResult UpdateAttendanceSetup(LateDeductionModel objlateDeductionModel)
        {
            string result = "";
            string resultMessage = "";
            AttendanceSetupModel attendanceSetupModel = new AttendanceSetupModel();
            AttendenceSetupDB attendenceSetupDB = new AttendenceSetupDB();
            OperationDetails operationDetails = new OperationDetails();
            operationDetails = attendenceSetupDB.UpdateAttendanceSetup(objlateDeductionModel.attendanceSetupModel);
            if (operationDetails.Message == "success")
            {
                result = "success";
                resultMessage = "Attendence setup updated successfully.";
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while updating attendence setup.";
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult SaveLateDeduction(List<String> values, string FromDate, string ToDate,string GenerateDate)
        //{
        //    LateDeductionModel lateDeductionModel = new LateDeductionModel();
        //    LateDeductionDB lateDeductionDB = new LateDeductionDB();
        //    foreach(string EmployeeId in values)
        //    {
        //        lateDeductionModel.EmployeeID=Convert.ToInt32(EmployeeId);
        //        lateDeductionModel.FromDate=FromDate;
        //        lateDeductionModel.ToDate=ToDate;
        //        lateDeductionModel.EffectiveDate=GenerateDate;
        //        lateDeductionDB.ConfirmEmployeeForLateDeductedStatus(lateDeductionModel);
        //        lateDeductionDB.SetEmployeeLateDeductedStatus(lateDeductionModel);
        //    }


        //    //LocationDB locationDB = new LocationDB();
        //    //foreach (string EmployeeID in values)
        //    //{
        //    //    locationDB.InsertEmployeeLocation(LocationID, Convert.ToInt32(EmployeeID));
        //    //}


        //    return Json(new { result = "success", resultMessage="Attendence setup updated successfully." },JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult SaveLateDeduction(List<LateDeductionModel> lateDeductionList)
        //{
        //    OperationDetails operationDetails = new OperationDetails();
        //    //LateDeductionModel lateDeductionModel = new LateDeductionModel();
        //    LateDeductionDB lateDeductionDB = new LateDeductionDB();

        //    foreach (LateDeductionModel lateDeductionModel in lateDeductionList)
        //    {
        //        lateDeductionModel.EmployeeID = lateDeductionModel.EmployeeID;
        //        lateDeductionModel.FromDate = lateDeductionModel.FromDate;
        //        lateDeductionModel.ToDate = lateDeductionModel.ToDate;
        //        lateDeductionModel.EffectiveDate = lateDeductionModel.GenerateDate;
        //        var returnResult = lateDeductionDB.ConfirmEmployeeForLateDeductedStatus(lateDeductionModel);
        //        //lateDeductionDB.SetEmployeeLateDeductedStatus(lateDeductionModel);
        //        if (returnResult.InsertedRowId > 0)
        //        {
        //            PayDeductionModel payDeductionModel = new PayDeductionModel();
        //            payDeductionModel.PaidCycle = 1;
        //            payDeductionModel.IsInstallment = false;
        //            payDeductionModel.Amount = lateDeductionModel.Amount;
        //            payDeductionModel.Installment = 0;
        //            payDeductionModel.Comments = lateDeductionModel.Comments;
        //            payDeductionModel.RefNumber = "";
        //            payDeductionModel.PvId = 0;
        //            payDeductionModel.EmployeeID = lateDeductionModel.EmployeeID;
        //            payDeductionModel.EffectiveDate = lateDeductionModel.GenerateDate;
        //            payDeductionModel.DeductionTypeID = 9;
        //            operationDetails = lateDeductionDB.PayDeductionCrud(payDeductionModel, 1);
        //            PayDeductionDetailModel payDeductionDetailModel = new PayDeductionDetailModel();
        //            payDeductionDetailModel.PayDeductionID = operationDetails.InsertedRowId;
        //            payDeductionDetailModel.DeductionDate = payDeductionModel.EffectiveDate;
        //            payDeductionDetailModel.Amount = lateDeductionModel.Amount;
        //            payDeductionDetailModel.Comments = payDeductionModel.RefNumber;
        //            payDeductionDetailModel.IsActive = true;
        //            lateDeductionDB.PayDeductionDetailsCrud(payDeductionDetailModel, 1);
        //        }
        //    }

        //    return Json(new { result = "success", resultMessage = "Attendence setup updated successfully." }, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult SaveLateDeduction(int DeductionType, int CycleID, string GenerateDate, string FromDate = "", string ToDate = "")
        {
            OperationDetails op = new OperationDetails();
            //LateDeductionModel lateDeductionModel = new LateDeductionModel();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            List<LateDeductionModel> lateDeductionList = new List<LateDeductionModel>();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.DeductionType = DeductionType;

            int aCycleID = lateDeductionDB.GetCycleFromDate(GenerateDate);
            int DeductionTypeId = 0;
            if (DeductionType == 1)
            {
                DeductionTypeId = lateDeductionDB.GetDeductionTypeIdForLate();
            }
            else {
                DeductionTypeId = lateDeductionDB.GetDeductionTypeIdForEarly();
            }
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            int? UserId = null;
            UserId = objUserContextViewModel.UserId;

            if (aCycleID == CycleID)
            {
                lateDeductionList = lateDeductionDB.GetDeductableEmployeeList(lateDeductionModel, UserId).Where(x => x.deduct == true).ToList();
                string EmployeeLateIds = string.Join(",", lateDeductionList.Select(s => s.PayEmployeeLateID));
                op = lateDeductionDB.ConfirmDeductionStatus(EmployeeLateIds, GenerateDate, DeductionType);
                List<multipleDeduction> lstmultipleDeduction = new List<multipleDeduction>();
                PayDeductionModel payDeductionModel = new PayDeductionModel(); ;
                foreach (LateDeductionModel item in lateDeductionList)
                {
                    multipleDeduction objmultiplededuction = new multipleDeduction();
                    objmultiplededuction.amount = Convert.ToDecimal(item.Amount);
                    objmultiplededuction.cycle = 1;
                    objmultiplededuction.employeeId = item.EmployeeID;
                    if (DeductionType == 1)
                    {
                        objmultiplededuction.comments = "Late deduction for generate date: " + GenerateDate + " / attendance date " + item.AttendanceDate + "";
                    }
                    else
                    {
                        objmultiplededuction.comments = "Early out deduction for generatedate: " + GenerateDate + " / attendance date " + item.AttendanceDate + "";
                    }                   
                    objmultiplededuction.PayEmployeeLateId = item.PayEmployeeLateID;
                    lstmultipleDeduction.Add(objmultiplededuction);
                }

                op = lateDeductionDB.AddMultipleDeduction(DeductionTypeId, GenerateDate, "", CycleID, lstmultipleDeduction);
            }
            else
            {
                op.CssClass = "error";
                op.Message = "Cycle is not generated yet for provided date";
            }

            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateisDeductedStatus(int payEmployeeLateID, bool isDeducted, int deductionType, bool isExcused)
        {
            OperationDetails op = new OperationDetails();
            LateDeductionDB objLateDeductionDB = new LateDeductionDB();
            op = objLateDeductionDB.UpdateDeuctedStatus(payEmployeeLateID, isDeducted, deductionType, isExcused);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateisExcusedStatus(int payEmployeeLateID, bool isExcused, int deductionType, bool isDeducted)
        {
            OperationDetails op = new OperationDetails();
            LateDeductionDB objLateDeductionDB = new LateDeductionDB();
            op = objLateDeductionDB.UpdateExcusedStatus(payEmployeeLateID, isExcused, deductionType, isDeducted);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateisExcusedStatusWithAllEmployee(int DeductionType, bool isExcusedAll, bool isDeductAll, string FromDate = "", string ToDate = "")
        {
            OperationDetails op = new OperationDetails();
            List<LateDeductionModel> lateDeductionList = new List<LateDeductionModel>();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.DeductionType = DeductionType;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            int? UserId = null;
            UserId = objUserContextViewModel.UserId;

            lateDeductionList = lateDeductionDB.GetDeductableEmployeeList(lateDeductionModel, UserId);
            string EmployeeLateIds = string.Join(",", lateDeductionList.Select(m => m.PayEmployeeLateID).ToList());
            op = lateDeductionDB.UpdateExcusedStatusForAllEmployee(EmployeeLateIds, isExcusedAll, DeductionType, isDeductAll);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckAllExcused(int DeductionType, string FromDate = "", string ToDate = "")
        {
            OperationDetails op = new OperationDetails();
            List<LateDeductionModel> lateDeductionList = new List<LateDeductionModel>();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.DeductionType = DeductionType;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            int? UserId = null;
            UserId = objUserContextViewModel.UserId;
            lateDeductionList = lateDeductionDB.GetDeductableEmployeeList(lateDeductionModel, UserId);
            int excusedCount = 0;
            string EmployeeLateIds = string.Join(",", lateDeductionList.Select(m => m.PayEmployeeLateID).ToList());
            if (lateDeductionList.Count > 0)
            {
                excusedCount = lateDeductionDB.GetStatusCount(EmployeeLateIds, DeductionType);
            }
            bool CheckAllExcused = false;
            if (excusedCount == lateDeductionList.Count)
            {
                CheckAllExcused = true;
            }
            else
            {
                CheckAllExcused = false;
            }
            return Json(new { CheckAll = CheckAllExcused }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ConfirmLateDeductionStatus(int DeductionType, bool isExcusedAll, bool isDeductAll, string FromDate = "", string ToDate = "")
        {
            OperationDetails op = new OperationDetails();
            List<LateDeductionModel> lateDeductionList = new List<LateDeductionModel>();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            LateDeductionModel lateDeductionModel = new LateDeductionModel();
            lateDeductionModel.FromDate = FromDate;
            lateDeductionModel.ToDate = ToDate;
            lateDeductionModel.DeductionType = DeductionType;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            int? UserId = null;
            UserId = objUserContextViewModel.UserId;
            lateDeductionList = lateDeductionDB.GetDeductableEmployeeList(lateDeductionModel, UserId);
            string EmployeeLateIds = string.Join(",", (lateDeductionList.Where(m => m.isDeducted == true).ToList()).Select(m => m.PayEmployeeLateID));
            op = lateDeductionDB.UpdateExcusedStatusForAllEmployee(EmployeeLateIds, isExcusedAll, DeductionType, isDeductAll);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateCauseDelayStatus(int PayEmployeeLateId, int EmployeeID, int LateMinutes, int CycleID, bool isDelayCaused, int CountLateInCycle)
        {
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormateForLateAndEarly();
            LateDeductionDB lateDeductionDB = new LateDeductionDB();
            double amount = 0;
            amount = lateDeductionDB.GetDeductableEmployeeWithCauseDelay(PayEmployeeLateId, EmployeeID, LateMinutes, CycleID, isDelayCaused, CountLateInCycle);
            string Amount = amount.ToString(AmountFormat);
            return Json(new { Amount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNullcycleCount(string Startdate, string EndDate)
        {
            int count = 0;
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();
            if (objRuleSetting.AppliedDeductionRules == true)
            {
                LateDeductionDB objDB = new LateDeductionDB();
                count = objDB.GetNullcycleCountBetweenDates(Startdate, EndDate);
            }
            return Json(new { count }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetNullAcademicYears(string Startdate, string EndDate, int deductiontype)
        {
            string AcRecords = string.Empty;
            string Message = string.Empty;
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();

            if (objRuleSetting.AppliedDeductionRules && objRuleSetting.AbsentCountForCalculation == 3)
            {
                LateDeductionDB objDB = new LateDeductionDB();
                AcRecords = objDB.GetNullAcYearsBetweenDates(Startdate, EndDate, deductiontype);
                Message = " Ending date of Academic year "+AcRecords+" is not define, we cannot show record and calculate deduction amount.";
            }
            else
            {
                AcRecords = string.Empty;
            }

            return Json(new { AcRecords, Message }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpdateLateReason(int payEmployeeLateID)
        {
            ViewBag.PayEmployeeLateID = payEmployeeLateID;
            LateDeductionDB objLateDeductionDB = new LateDeductionDB();
            LateDeductionModel objLateDeductionModel = new LateDeductionModel();
            objLateDeductionModel = objLateDeductionDB.GetLateDeductionByPayLateID(payEmployeeLateID);
            ViewBag.LateReason = objLateDeductionModel.LateReason;
            return View(objLateDeductionModel);
        }

        public JsonResult AddLateReason(int hdnpayEmployeeLateID, string LateReason)
        {
            OperationDetails op = new OperationDetails();
            LateDeductionDB objLateDeductionDB = new LateDeductionDB();
            op = objLateDeductionDB.UpdateLateReason(hdnpayEmployeeLateID, LateReason);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

    }
}