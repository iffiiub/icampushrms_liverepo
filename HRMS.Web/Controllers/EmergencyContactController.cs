﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class EmergencyContactController : BaseController
    {
        public ActionResult Index(int Id)
        {
            ViewBag.EmployeeId = Id;
            return View();
        }

        public ActionResult GetEmergencyContactList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, int EmployeeID = 0)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = 10; //Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<EmergencyContactModel> emergencyContactModelList = new List<EmergencyContactModel>();
            EmergencyContactDB emergencyContactDB = new EmergencyContactDB();
            emergencyContactModelList = emergencyContactDB.GetAllEmergencyContactList(pageNumber, numberOfRecords, sortColumn, sortOrder, EmployeeID);
            //---------------------------------------------

            var vList = new object();
            vList = new
            {
                aaData = (from item in emergencyContactModelList
                          select new
                          {
                              //Actions = "<a class='btn btn-success'  title='Edit' ><i class='fa fa-pencil'></i></a><a class='btn btn-primary' style='display:none'  title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm'  onclick='AddEditEmergencyContact(" + item.EmergencyContactId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm'  onclick='DeleteContact(" + item.EmergencyContactId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmergencyContactId = item.EmergencyContactId,
                              EmployeeID = item.EmployeeID,
                              ContactName = item.ContactName,
                              CallInEmergancy = item.CallInEmergancy,
                              ResPhone = item.ResPhone,
                              Email = item.Email,
                              RelationId = item.RelationId,
                              WorkPhone = item.WorkPhone,
                              Mobile = item.Mobile,
                              RelationName = item.RelationName
                          }).ToArray(),
                recordsTotal = emergencyContactModelList.Count,
                recordsFiltered = emergencyContactModelList.Count
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Contact(int EmployeeId, int RefId)
        {
            ViewBag.EmployeeID = EmployeeId;
            EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
            EmergencyContactModel objEmergencyContactModel = new EmergencyContactModel();
            objEmergencyContactModel.EmployeeID = EmployeeId;
            ViewBag.CallEmergency = new SelectList(DataAccess.GeneralDB.CommonDB.GetBoolDDL(), "dataFeild1", "text");
            // Get Person Relation Dropdown
            PersonRelationDB objPersonRelation = new PersonRelationDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var m in objPersonRelation.GetAllPersonRelationList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.RelationName, Value = m.RelationId.ToString() });

            }
            ViewBag.PersonRelationList = ObjSelectedList;

            if (RefId != 0)
            {
                objEmergencyContactModel = objEmergencyContactDB.GetEmergencyContact(RefId);
            }

            return View(objEmergencyContactModel);
        }

        public JsonResult UpdateEmergencyContact(EmergencyContactModel ObjEmergencyContactModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
            operationDetails = objEmergencyContactDB.UpdateEmergencyContact(ObjEmergencyContactModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjEmergencyContactModel.EmergencyContactId == 0)
                {
                    result = "success";
                    resultMessage = "Emergency contact save successfully";
                }
                else if (ObjEmergencyContactModel.EmergencyContactId != 0)
                {
                    result = "success";
                    resultMessage = "Emergency contact save successfully";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Emergency Contact.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ExportToExcel(int EmployeeId)
        //{
        //    byte[] content;
        //    string fileName = "EmergencyContact" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
        //    //ProfileDB profileDB = new ProfileDB();

        //    int pageNumber = 0;
        //    int numberOfRecords = 0;
        //    string sortColumn = "EmergencyContactId";
        //    string sortOrder = "DESC";
        //    int PaySalaryAllowanceID = objUserContextViewModel.CompanyId;

        //    List<HRMS.Entities.EmergencyContactModel> objEmergencyContactList = new List<HRMS.Entities.EmergencyContactModel>();
        //    EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
        //    objEmergencyContactList = objEmergencyContactDB.GetAllEmergencyContactList(pageNumber, numberOfRecords, sortColumn, sortOrder, EmployeeId);
        //    var report = (from item in objEmergencyContactList.AsEnumerable()
        //                  select new
        //                  {
        //                      EmergencyContactId = item.EmergencyContactId,
        //                      EmployeeID = item.EmployeeID,
        //                      ContactName = item.ContactName,
        //                      CallInEmergancy = item.CallInEmergancy,
        //                      ResPhone = item.ResPhone,
        //                      Email = item.Email,
        //                      RelationId = item.RelationId,
        //                      WorkPhone = item.WorkPhone,
        //                      Mobile = item.Mobile,
        //                      RelationName = item.RelationName
        //                  }).ToList();
        //    content = ExportListToExcel(report);
        //    return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        //}

        //public void ExportToPdf(int EmployeeId)
        //{
        //    Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

        //    string fileName = "EmergencyContact" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

        //    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
        //    objUserContextViewModel = (UserContextViewModel)Session["userContext"];            

        //    int pageNumber = 0;
        //    int numberOfRecords = 0;
        //    string sortColumn = "EmergencyContactId";
        //    string sortOrder = "Desc";
        //    int PaySalaryAllowanceID = objUserContextViewModel.CompanyId;

        //    List<HRMS.Entities.EmergencyContactModel> objEmergencyContactList = new List<HRMS.Entities.EmergencyContactModel>();
        //    EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
        //    objEmergencyContactList = objEmergencyContactDB.GetAllEmergencyContactList(pageNumber, numberOfRecords, sortColumn, sortOrder, EmployeeId);
        //    var report = (from item in objEmergencyContactList.AsEnumerable()
        //                  select new
        //                  {
        //                      EmergencyContactId = item.EmergencyContactId,
        //                      EmployeeID = item.EmployeeID,
        //                      ContactName = item.ContactName,
        //                      CallInEmergancy = item.CallInEmergancy,
        //                      ResPhone = item.ResPhone,
        //                      Email = item.Email,
        //                      RelationId = item.RelationId,
        //                      WorkPhone = item.WorkPhone,
        //                      Mobile = item.Mobile,
        //                      RelationName = item.RelationName
        //                  }).ToList();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    GridView gridvw = new GridView();
        //    gridvw.DataSource = report; //bind the data table to the grid view
        //    gridvw.DataBind();
        //    StringWriter swr = new StringWriter();
        //    HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
        //    gridvw.RenderControl(tw);
        //    StringReader sr = new StringReader(swr.ToString());
        //    Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    pdfDoc.Open();
        //    htmlparser.Parse(sr);
        //    pdfDoc.Close();
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}


        public ActionResult ExportToExcel(int EmployeeId)
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "EmergencyContact" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "EmergencyContactId";
            string sortOrder = "Desc";
            EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
            System.Data.DataSet ds = objEmergencyContactDB.GetEmergencyContactDatasSet(pageNumber, numberOfRecords, sortColumn, sortOrder, EmployeeId);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(4);
            ds.Tables[0].Columns["CallInEmergancy"].ColumnName = "Call In Emergancy";
            ds.Tables[0].Columns["ResPhone"].ColumnName = "Residential Phone";
            ds.Tables[0].AcceptChanges();
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(int EmployeeId)
        {
            string fileName = "EmergencyContact" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "EmergencyContactId";
            string sortOrder = "Desc";
            EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
            System.Data.DataSet ds = objEmergencyContactDB.GetEmergencyContactDatasSet(pageNumber, numberOfRecords, sortColumn, sortOrder, EmployeeId);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(0);
            ds.Tables[0].Columns.RemoveAt(4);
            ds.Tables[0].Columns["CallInEmergancy"].ColumnName = "Call In Emergancy";
            ds.Tables[0].Columns["ResPhone"].ColumnName = "Residential Phone";
            ds.Tables[0].AcceptChanges();
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }

        public ActionResult DeleteContact(int id)
        {
            EmergencyContactDB objEmergencyContactDB = new EmergencyContactDB();
            var operationDetails = objEmergencyContactDB.DeleteEmergencyContactById(id);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}