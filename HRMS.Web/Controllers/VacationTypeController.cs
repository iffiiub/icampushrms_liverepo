﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using System.Text.RegularExpressions;

namespace HRMS.Web.Controllers
{
    public class VacationTypeController : BaseController
    {
        //
        // GET: /Position/
        public ActionResult Index()
        {            
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/VacationType/Index");
            Session["VacationType"] = permissionModel;
            GetDropDowns();
            return View();
        }

        public ActionResult GetVacationTypeList()
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            //-------------Data Objects--------------------
            List<VacationTypeModel> objVacationTypeModelList = new List<VacationTypeModel>();
            VacationDB objLeaveTypeDB = new VacationDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //objVacationTypeModelList = objLeaveTypeDB.GetVacationTypeListWithPaging(objUserContextViewModel.CompanyId);
            objVacationTypeModelList = objLeaveTypeDB.GetVacationTypeListWithPaging(0);
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/VacationType/Index");
            string Action = "";
          
            if (permissionModel.IsUpdateOnlyPermission)
            {
                Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditLeaveType(item)' title='Edit' ><i class='fa fa-pencil'></i></a>";
            }
            if (permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteVacationType(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
            }
            if (permissionModel.IsViewOnlyPermission)
            {
                Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewLeaveType(item)' title='View Details' ><i class='fa fa-eye'></i></a>";
            }

            //---------------------------------------------           
            var vList = new object();
            vList = new
            {
                aaData = (from item in objVacationTypeModelList
                          select new
                          {
                              Action = Action.Replace("item", item.VacationTypeId.ToString()),
                              Id = item.VacationTypeId,
                              VacationTypeName = item.Name,
                              //NoOfDays = item.NoOfDays,
                              //27-05-2019 
                              FullPaidDays = item.FullPaidDays,
                              HalfPaidDays = item.HalfPaidDays,
                              UnPaidDays = item.UnPaidDays,
                              //27-05-2019 
                             // PaidType = (item.FullPaidDays > 0 || item.HalfPaidDays > 0) ? "No" : "Yes",
                              Term = item.PerServiceVacation == 0 ? "Per Service" : "Per Annum",
                              //RepeatedFor = item.RepeatedFor,
                              ApplicableAfterDigits = item.ApplicableAfterDigits,
                              ApplicableAfterTypeID = ((ApplicableAfterTypeEnum)item.ApplicableAfterTypeID).ToString(),
                              ReligionName_1 = item.ReligionName_1,
                             // isCompensate = item.isCompensate,
                              GenderName_1 = item.GenderName_1,
                              NationalityName_1 = item.NationalityName_1,
                              AnnualLeave = item.AnnualLeave == 1 ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.VacationTypeId + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.VacationTypeId + "'/>",
                              LifetimeVacation = item.PerServiceVacation == 1 ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.VacationTypeId + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.VacationTypeId + "'/>",
                              MonthlySplit = item.MonthlySplit == 1 ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.VacationTypeId + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.VacationTypeId + "'/>"
                              //AnnualVacation = ((TrueFalseEnum)item.AnnualLeave).ToString(),
                              //LifetimeVacation = ((TrueFalseEnum)item.LifetimeLeave).ToString(),
                              //MonthlySplit = ((TrueFalseEnum)item.MonthlySplit).ToString(),
                              //StatusID = objDBHelper.GetStatusList().Where(x => x.StatusID == item.StatusID).Select(x => x.StatusName).SingleOrDefault(),
                              //EmploymentTypeID = objDBHelper.GetEmploymentTypeList().Where(x => x.EmploymentTypeID == item.EmploymentTypeID).Select(x => x.EmploymentTypeName).SingleOrDefault(),
                              //Reg_TempID = objDBHelper.GetRegTempList().Where(x => x.Reg_TempID == item.Reg_TempID).Select(x => x.Reg_TempName).SingleOrDefault(),                              
                              //IncumbentsNumber = item.IncumbentsNumber
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HRMS.Entities.VacationTypeModel objLeaveType = new HRMS.Entities.VacationTypeModel();
            objLeaveType.positionList = new List<VactionPositionModel>();
            objLeaveType.departmentList = new List<VacationDepartmentModel>();
            objLeaveType.jobTitleList = new List<VacationjobTitleModel>();
            objLeaveType.jobCategoryList = new List<VacationJobCategoryModel>();
            int CompanyId = objUserContextViewModel.CompanyId;

            GenderDB ObjGenderDB = new GenderDB();
            ReligionDB ObjReligionDB = new ReligionDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            MaritialStatusDB objMaritalStatusDb = new MaritialStatusDB();
            VacationDB objVacationTypeDB = new VacationDB();
            PositionDB positiondb = new PositionDB();

            List<GenderModel> lstGender = ObjGenderDB.GetAllGenders();
            GenderModel naGender = new GenderModel();
            naGender.GenderID = -1;
            naGender.GenderName_1 = "All Gender";
            lstGender.Insert(0, naGender);
            SelectList GenderList = new SelectList(lstGender, "GenderID", "GenderName_1");
            ViewBag.GenderList = GenderList;

            List<ReligionModel> religionList = ObjReligionDB.GetAllReligion().Where(m => m.ReligionID > 0).ToList();
            ReligionModel religionModel = new ReligionModel();
            religionModel.ReligionID = 0;
            religionModel.ReligionName_1 = "All Religions";
            religionList.Insert(0, religionModel);
            ViewBag.ReligionList = new SelectList(religionList, "ReligionID", "ReligionName_1");

            List<NationalityModel> lstNationality = ObjNationalityDB.getAllNationalities().Where(m => m.NationalityID > 0).ToList();
            NationalityModel nationalityModel = new NationalityModel();
            nationalityModel.NationalityID = 0;
            nationalityModel.NationalityName_1 = "All Nationalities";
            lstNationality.Insert(0, nationalityModel);
            ViewBag.NationalityList = new SelectList(lstNationality, "NationalityID", "NationalityName_1");

            //List<MaritialStausModel> lstMaritialStaus = objMaritalStatusDb.getAllMaritialStatus().Where(m => m.MaritalStatusID > 0).ToList();
            List < MaritialStausModel > lstMaritialStaus = objMaritalStatusDb.getAllMaritialStatus().Where(m => m.MaritalStatusID > 0).ToList();
            MaritialStausModel maritialStausModel = new MaritialStausModel();
            maritialStausModel.MaritalStatusID = 0;
            maritialStausModel.MaritalStatusName_1 = "All Marital Status";
            lstMaritialStaus.Insert(0, maritialStausModel);
            ViewBag.MaritialStatusList = new SelectList(lstMaritialStaus, "MaritalStatusId", "MaritalStatusName_1");

            ViewBag.ApplicableType = new SelectList(DataAccess.GeneralDB.CommonDB.GetApplicableType(), "id", "text");
            ViewBag.Timespan = new SelectList(DataAccess.GeneralDB.CommonDB.GetTimeSpans(), "id", "text");
            ViewBag.RoundingTypes = new SelectList(DataAccess.GeneralDB.CommonDB.GetAccumulateRoundingType(), "id", "text");
            ViewBag.DisplayLeaveBalanceType = new SelectList(DataAccess.GeneralDB.CommonDB.GetDisplayLeaveBalanceType(), "id", "text");
            ViewBag.boolOption = DataAccess.GeneralDB.CommonDB.GetBoolDDL();
            ViewBag.boolTypeDDl = new SelectList(DataAccess.GeneralDB.CommonDB.GetBoolDDL(), "id", "text");

            ViewBag.departmentList = new DepartmentDB().GetDepartmentList(CompanyId);
            ViewBag.jobTitleList = new JobDB().GetJobTitleList();
            ViewBag.jobCategoryList = new JobCategoryDB().GetJobCategory();
            objLeaveType.AnnualLeave = 1;


            List<VacationTypeCategoryModel> lstVTCategory = new VacationDB().GetAllVacationTypeCategory(null,true);
            VacationTypeCategoryModel VTCategoryModel = new VacationTypeCategoryModel();
            VTCategoryModel.VTCategoryID = 0;
            VTCategoryModel.VTCategoryName_1 = "Select";
            lstVTCategory.Insert(0, VTCategoryModel);
            ViewBag.vtCategoryList = new SelectList(lstVTCategory, "VTCategoryID", "VTCategoryName_1");
           // ViewBag.LeaveEntitlementTypeList = new SelectList(new LeaveTypeDB().GetLeaveEntitlementTypeList(), "id", "text");

            LeaveSetting objLeaveSetting = new LeaveTypeDB().GetLeaveSetting();
            ViewBag.BeamHideFields = objLeaveSetting.BeamHideFields;
            ViewBag.ShowVTCategoryField = objLeaveSetting.ShowVTCategoryField;
            //added to pass through modal validation (modal state valid) 
            //Will set as zero in save/update       
            if (objLeaveSetting.BeamHideFields)
            {
                objLeaveType.MaxNoOfDaystocarryForword = 1;
                objLeaveType.AccMaxNumberOfDays = 1;
            }

            if (id != 0)
            {

                objLeaveType = objVacationTypeDB.GetVacationTypeById(id);
                objLeaveType.positionList = objVacationTypeDB.getPositionByVactionId(objLeaveType.VacationTypeId);
                objLeaveType.departmentList = objVacationTypeDB.getDepartmentByVactionId(objLeaveType.VacationTypeId);
                objLeaveType.jobTitleList = objVacationTypeDB.getjobTitleByVactionId(objLeaveType.VacationTypeId);
                objLeaveType.jobCategoryList = objVacationTypeDB.getJobCategoryByVactionId(objLeaveType.VacationTypeId);
                // objLeaveType.jobTitleList=objVacationTypeDB.
                ViewBag.Edit = 1;
                ViewBag.SelectedGender = objLeaveType.GenderID;
                ViewBag.SelectedNationality = objLeaveType.NationalityID;
                ViewBag.SelectedReligionId = objLeaveType.ReligionID;
                ViewBag.SelectedMaritalId = objLeaveType.MaritalStatusId;
                //ViewBag.SelectedIsCompensate = objLeaveType.isCompensate;
                ViewBag.SelectedAnnualLeave = objLeaveType.AnnualLeave;
                ViewBag.SelectedLifetimeLeave = objLeaveType.PerServiceVacation;
                ViewBag.SelectedMonthlySplit = objLeaveType.MonthlySplit;
                ViewBag.ApplicableAfterType = objLeaveType.ApplicableAfterTypeID;
                ViewBag.VTCategoryID = objLeaveType.VTCategoryID;
               // ViewBag.LeaveEntitleTypeID = objLeaveType.LeaveEntitleTypeID;
                //added to pass through modal validation (modal state valid) 
                //Will set as zero in save/update         
                if (objLeaveSetting.BeamHideFields)
                {
                    objLeaveType.MaxNoOfDaystocarryForword = 1;
                    objLeaveType.AccMaxNumberOfDays = 1;
                }
            }
            List<PositionModel> lstPosition = new List<PositionModel>();
            if (id == 0)
            {
                lstPosition = positiondb.GetPositionList();
            }
            else
            {
                if (objLeaveType.isAccumulatedLeave)
                {
                    lstPosition = positiondb.GetPositionListByVacationType(id);
                }
                else
                {
                    lstPosition = positiondb.GetPositionList();
                }
            }
            ViewBag.positionList = lstPosition;


            return View(objLeaveType);
        }

        [HttpPost]
        public JsonResult AddVacationType(VacationTypeModel objVacationTypeModel, List<string> selectedPosition, List<string> selectedJobTitle, List<string> selectedDepartment, List<string> selectedJobCategory)
        {
            string result = "";
            string resultMessage = "";          
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveSetting objLeaveSetting = new LeaveTypeDB().GetLeaveSetting();
            objVacationTypeModel.AccMaxNumberOfDays = 0;
            if (objLeaveSetting.ShowVTCategoryField && (objVacationTypeModel.VTCategoryID==null || objVacationTypeModel.VTCategoryID ==0))
            {
                result = "error";
                resultMessage = "Please select Vacation Type Category";
                return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
            }       
           //// var errors = ModelState.Values.SelectMany(v => v.Errors);
           // string messages = string.Join("<br/> ", ModelState.Values
           //                                           .SelectMany(x => x.Errors)
           //                                           .Select(x => x.ErrorMessage));
            if (ModelState.IsValid)
            {
                if (objLeaveSetting.BeamHideFields)
                {
                    objVacationTypeModel.MaxNoOfDaystocarryForword = 0;
                    objVacationTypeModel.AccMaxNumberOfDays = 0;
                }              
                objVacationTypeModel.CompanyId = objUserContextViewModel.CompanyId;
                string position = (selectedPosition == null ? "" : String.Join(",", selectedPosition.ToArray()));
                string jobTitles = (selectedJobTitle == null ? "" : String.Join(",", selectedJobTitle.ToArray()));
                string departments = (selectedDepartment == null ? "" : String.Join(",", selectedDepartment.ToArray()));
                string jobCategories = (selectedJobCategory == null ? "" : String.Join(",", selectedJobCategory.ToArray()));
                //Formula = vacation type number of days *(1 - number input field) / days(365), months(12), years(1)
                decimal divideBy = 1;
                LeaveTypeDB leaveTypeDB = new LeaveTypeDB();
                if (objVacationTypeModel.AccumulateTimeSpanId == 0)
                {
                    divideBy = 365;
                }
                else if (objVacationTypeModel.AccumulateTimeSpanId == 1)
                {
                    divideBy = leaveTypeDB.GetLeaveSetting().AccumulateLeaveMonth;
                }
                else
                {
                    divideBy = 1;
                }
                //27-05-2019 
                //objVacationTypeModel.AccumulateNumber = (decimal)(objVacationTypeModel.NoOfDays * objVacationTypeModel.AccumulatePeriodNumber) / divideBy;
                objVacationTypeModel.AccumulateNumber = (decimal)(objVacationTypeModel.FullPaidDays * objVacationTypeModel.AccumulatePeriodNumber) / divideBy;
                try
                {
                    VacationDB objVacationTypeDB = new VacationDB();
                    var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
                    objVacationTypeModel.Name = objVacationTypeModel.Name.Trim();
                    if (regexItem.IsMatch(objVacationTypeModel.Name))
                    {
                        if (objVacationTypeModel.VacationTypeId == 0)
                        {
                            if (!objVacationTypeDB.isAvailableVacationType(objVacationTypeModel, 1))
                            {
                                objVacationTypeDB.InsertVacationType(objVacationTypeModel, position, departments, jobTitles, jobCategories);
                                result = "success";
                                resultMessage = "Leave Type Added Successfully.";
                            }
                            else
                            {
                                result = "error";
                                resultMessage = "Leave Type with this name already available. Please type different name for leave type.";
                            }
                        }
                        else if (objVacationTypeModel.VacationTypeId != 0)
                        {
                            if (!objVacationTypeDB.isAvailableVacationType(objVacationTypeModel, 2))
                            {
                                objVacationTypeDB.UpdateLeaveType(objVacationTypeModel, position, departments, jobTitles, jobCategories);
                                result = "success";
                                resultMessage = "Leave Type Updated Successfully.";
                            }
                            else
                            {
                                result = "error";
                                resultMessage = "Leave Type with this name already available. Please type different name for leave type.";
                            }
                        }
                    }
                    else
                    {
                        result = "error";
                        resultMessage = "Leave Type sholuld not contain any symbol.";
                    }


                }
                catch (Exception ex)
                {
                    result = "error";
                    resultMessage = "Error occured while adding Leave Type.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Please enter value greater than 0 for following fields: <br/>1 - Maximum number of days to accrued.<br/>2 - Accrues every.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewVacationType(int id)
        {
            VacationDB objVacationTypeModelDB = new VacationDB();
            VacationTypeModel objVacationTypeModel = new VacationTypeModel();
            MaritialStatusDB objMaritalStatusDb = new MaritialStatusDB();
            objVacationTypeModel = objVacationTypeModelDB.GetVacationTypeById(id);
            objVacationTypeModel.positionList = objVacationTypeModelDB.getPositionByVactionId(objVacationTypeModel.VacationTypeId);
            objVacationTypeModel.departmentList = objVacationTypeModelDB.getDepartmentByVactionId(objVacationTypeModel.VacationTypeId);
            objVacationTypeModel.jobTitleList = objVacationTypeModelDB.getjobTitleByVactionId(objVacationTypeModel.VacationTypeId);
            objVacationTypeModel.jobCategoryList = objVacationTypeModelDB.getJobCategoryByVactionId(objVacationTypeModel.VacationTypeId);
           // ViewBag.LeaveEntitlementTypeList = new LeaveTypeDB().GetLeaveEntitlementTypeList();         
            ViewBag.ApplicableType = DataAccess.GeneralDB.CommonDB.GetApplicableType();
            ViewBag.BoolType = DataAccess.GeneralDB.CommonDB.GetBoolDDL();
            ViewBag.Timespan = DataAccess.GeneralDB.CommonDB.GetTimeSpans();
            ViewBag.RoundingTypes = DataAccess.GeneralDB.CommonDB.GetAccumulateRoundingType();
            ViewBag.MaritialStatus = objMaritalStatusDb.getAllMaritialStatus();
            ViewBag.DisplayLeaveBalanceType = DataAccess.GeneralDB.CommonDB.GetDisplayLeaveBalanceType();
            LeaveSetting objLeaveSetting = new LeaveTypeDB().GetLeaveSetting();
            ViewBag.BeamHideFields = objLeaveSetting.BeamHideFields;
            ViewBag.ShowVTCategoryField = objLeaveSetting.ShowVTCategoryField;
            return View(objVacationTypeModel);
        }

        public ActionResult DeleteVacationType(int id)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            VacationDB objVacationTypeModelDB = new VacationDB();
            operationDetails = objVacationTypeModelDB.DeleteVacationTypeById(id);
            if (operationDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Leave Type Deleted Successfully.";
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while deleting Leave Type.";
            }
            return Json(new { LeaveTypeId = id, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CalculateAccumulatesBy(int noOfDays, int accumulatesEveryDays, int period)
        {
            //Formula = vacation type number of days *(1 - number input field) / days(365), months(12), years(1)
            LeaveTypeDB leaveTypeDB = new LeaveTypeDB();
            decimal divideBy = 1;
            if (period == 0)
            {
                divideBy = 365;
            }
            else if (period == 1)
            {
                divideBy = leaveTypeDB.GetLeaveSetting().AccumulateLeaveMonth;
            }
            else
            {
                divideBy = 1;
            }

            decimal byDays = (noOfDays * accumulatesEveryDays) / divideBy;
            var result = byDays.ToString("0.000");
            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
        }
        public void GetDropDowns()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];


            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> ObjSelectedList = null;


            GenderDB ObjGenderDB = new GenderDB();
            ReligionDB ObjReligionDB = new ReligionDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            MaritialStatusDB objMaritialStatusDB = new MaritialStatusDB();

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "2" });
            foreach (var m in ObjGenderDB.GetAllGenders())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.GenderName_1, Value = m.GenderID.ToString() });
            }
            ViewBag.GenderList = ObjSelectedList;


            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjReligionDB.GetAllReligion())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.ReligionName_1, Value = m.ReligionID.ToString() });

            }
            ViewBag.ReligionList = ObjSelectedList;


            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in ObjNationalityDB.getAllNationalities())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.NationalityName_1, Value = m.NationalityID.ToString() });

            }
            ViewBag.NationalityList = ObjSelectedList;

            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });

            foreach (var m in objMaritialStatusDB.getAllMaritialStatus())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MaritalStatusName_1, Value = m.MaritalStatusID.ToString() });

            }
            ViewBag.MaritalStatusList = ObjSelectedList;
        }
        public ActionResult CheckISAnyVacationTypeGratuityAnual(int VacationTypeId)
        {
            bool isGratuityAnnual = new VacationDB().CheckIsAnyLeaveGratuityAnnual(VacationTypeId);
            return Json(new { result = isGratuityAnnual }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExistingPositionInAccrual(string PositionIds, int VacationTypeID)
        {
            PositionDB objPositionDB = new PositionDB();
            List<PositionModel> lstPositions = new List<PositionModel>();
            string PositionTypes = "";
            lstPositions = objPositionDB.GetExistingPositionInAccrual(PositionIds, VacationTypeID);
            PositionTypes = string.Join(",", lstPositions.Select(m => m.PositionTitle).ToList());            
            return Json(PositionTypes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckPositionHavingAccrualLeave(int PositionId)
        {
            VacationTypeModel objLeaveType = new VacationTypeModel();
            VacationDB objVacationTypeDB = new VacationDB();
            objLeaveType = objVacationTypeDB.GetAccrualVacationLiinkedWithPosition(PositionId);
            bool isLinkedAcrual = false;
            if (string.IsNullOrEmpty(objLeaveType.Name))
            {
                isLinkedAcrual = false;
            }
            else {
                isLinkedAcrual = true;
            }
            return Json(new { result = isLinkedAcrual }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAccrualLeaveTypeForPosition(int PositionId,bool isAccrualPosition)
        {
            PositionModel objPosition = new PositionModel();
            PositionDB objPositionDB = new PositionDB();
            objPosition = objPositionDB.GetPositionById(PositionId);
            VacationDB vacationdb = new VacationDB();
            ViewBag.vactionddl = new SelectList(vacationdb.GetAccrualVactionTypeList(), "id", "text");
            ViewBag.isAccrual = isAccrualPosition;
            return View(objPosition);
        }

        public JsonResult CheckAccrualAvailable(int EmployeeId)
        {
            VacationDB objVacDB = new VacationDB();
            decimal AccLeavesCountT = 0;
            bool IsAvailable = objVacDB.checkAccrualLeavesAvailibility(EmployeeId,out AccLeavesCountT);
            return Json(new { result = IsAvailable }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateEmployeeLeaveType(int EmployeeId, int VacTypeId, int PositionId,bool isPositionAccrual)
        {
            OperationDetails op = new OperationDetails();
            VacationDB objVacDB = new VacationDB();
            op = objVacDB.CarryForwardEmployeeAccrualLeaveDetails(VacTypeId, PositionId, isPositionAccrual, EmployeeId);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetNonExistingDepartmentInAccrual(int VacationTypeID)
        //{
        //    DepartmentDB objPositionDB = new DepartmentDB();
        //    List<De> lstPositions = new List<PositionModel>();
        //    string PositionTypes = "";
        //    lstPositions = objPositionDB.GetExistingPositionInAccrual(PositionIds, VacationTypeID);
        //    PositionTypes = string.Join(",", lstPositions.Select(m => m.PositionTitle).ToList());
        //    return Json(PositionTypes, JsonRequestBehavior.AllowGet);
        //}
    }
}