﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.ViewModel;
using System.IO;

namespace HRMS.Web.Controllers
{
    public class HiringRequisitionR1BudgetedFormController : FormsController
    {
        RecruitR1BudgetedDB recruitR1BudgetedDB;
        public HiringRequisitionR1BudgetedFormController()
        {
            XMLLogFile = "LoggerHiringRequisitionR1BudgetedForm.xml";
            recruitR1BudgetedDB = new RecruitR1BudgetedDB();
        }
        // GET: HiringRequisitionR1BudgetedForm
        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }
        public ActionResult Create()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            List<Entities.General.OtherPermissions> permissionList = GetPermissionList();
            ViewBag.PermissionList = permissionList;

            RecruitR1BudgetedModel recruitR1BudgetedModel = new RecruitR1BudgetedModel();
            //just to avoid checking null in view 
            recruitR1BudgetedModel.AllFormsFilesModelList = new List<AllFormsFilesModel>();
            recruitR1BudgetedModel.CreatedOn = DateTime.Today.Date;
            recruitR1BudgetedModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            CompanyModel company = new CompanyDB().GetCompany(objUserContextViewModel.CompanyId);
            //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
            ViewBag.CompanyTypeID = company.CompanyTypeID;
            //ViewBag.CompanyID = company.CompanyId.ToString();

            ViewBag.Year = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true), "AcademicYearId", "Duration");
            ViewBag.EmploymentType = new SelectList(new DBHelper().GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");

            if (company.CompanyTypeID > 1)
            {
                //string companyIds = string.Join(",", companyList.Select(x => x.CompanyId));

                //List<EmployeeDetailsModel> AllEmployeeList = new EmployeeDB().GetAllEmployeeForAdminByCompanyID(companyId);
                //ViewBag.HiringMangr = new SelectList(AllEmployeeList, "EmployeeId", "FullName");
                //ViewBag.ReportingEmployee = new SelectList(AllEmployeeList, "EmployeeId", "FullName");
                ViewBag.Company = GetCompanySelectList();
                //ViewBag.SalaryRanges = GetSalaryRanges(companyIds, 0);
                //ViewBag.SalaryRanges = new SelectList(new SalaryRangesDB().GetSalaryRanges("'" + company.CompanyId + "'", 0), "SalaryRangesID", "SalaryRanges");
            }
            else
            {
                //string companyIds = string.Join(",", companyList.Select(x => x.CompanyId));
                //List<EmployeeDetailsModel> AllEmployeeList = new EmployeeDB().GetAllEmployeeByCompanyAndUser(company.CompanyId, objUserContextViewModel.UserId);
                ViewBag.Company = GetCompanySelectList(company.CompanyId);// new SelectList(companyList.Where(x => x.CompanyId == company.CompanyId), "CompanyId", "Name");
                //ViewBag.HiringMangr = new SelectList(AllEmployeeList, "EmployeeId", "FullName");
                //ViewBag.ReportingEmployee = new SelectList(AllEmployeeList, "EmployeeId", "FullName");
                //ViewBag.SalaryRanges = GetSalaryRanges(company.CompanyId.ToString(), 0);
            }
            ViewBag.ReportingEmployee = new SelectList(Enumerable.Empty<SelectListItem>());
            ViewBag.HiringMangr = new SelectList(Enumerable.Empty<SelectListItem>());
            ViewBag.SalaryRanges = new SelectList(GetStaticSalaryRanges(), "Text", "Value");//new SelectList(Enumerable.Empty<SelectListItem>());
            ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
            ViewBag.Department = new SelectList(new DepartmentDB().GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
            ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
            ViewBag.Category = new SelectList(new EmployeeProfileCreationFormDB().GetRecruitCategoryList(), "id", "text");
            ViewBag.EmployeeAirfareFrequencyList = new SelectList(recruitR1BudgetedDB.GetEmployeeAirfareFrequencyList(), "id", "text");
            ViewBag.EmployeeAirfareClassesList = new SelectList(recruitR1BudgetedDB.GetEmployeeAirfareClassesList(), "id", "text");
            ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
            HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
            ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
            recruitR1BudgetedModel.IsAddMode = true;
            return View(recruitR1BudgetedModel);
        }
        public ActionResult Edit()
        {
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            RecruitR1BudgetedModel recruitR1BudgetedModel = new RecruitR1BudgetedModel();
            CompanyModel company;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int id = GetFormProcessSessionID();           

            if (id > 0)
            {
                //get current/pending approver group and employee
                RequestFormsApproveModel requestFormsApproveModel = recruitR1BudgetedDB.GetPendingFormsApproval(id);
                //List<Employee> AllEmployeeList = new EmployeeDB().GetEmployeeByActive(true,0);
                recruitR1BudgetedModel = recruitR1BudgetedDB.GetForm(id, objUserContextViewModel.UserId);
                //if login user is the present approver then can see form in edit mode or is admin user
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (recruitR1BudgetedModel.ReqStatusID == (int)RequestStatus.Rejected && recruitR1BudgetedModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {

                    recruitR1BudgetedModel.ActualBudget = decimal.Parse(recruitR1BudgetedModel.ActualBudget.ToString(amountFormat));
                    recruitR1BudgetedModel.SalikAmount = decimal.Parse(recruitR1BudgetedModel.SalikAmount.ToString(amountFormat));
                    recruitR1BudgetedModel.PetrolCardAmount = decimal.Parse(recruitR1BudgetedModel.PetrolCardAmount.ToString(amountFormat));
                    //If form is pending then only show for edit
                    if (recruitR1BudgetedModel.ReqStatusID == (int)RequestStatus.Pending
                        || (recruitR1BudgetedModel.ReqStatusID == (int)RequestStatus.Rejected && recruitR1BudgetedModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                    {
                        ViewBag.RecruitR1BudgetedID = recruitR1BudgetedModel.ID;
                        ViewBag.FormProcessID = id.ToString();
                        ViewBag.RequestID = recruitR1BudgetedModel.RequestID;
                        company = new CompanyDB().GetCompany(recruitR1BudgetedModel.CompanyID);

                        //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();

                        ViewBag.CompanyTypeID = company.CompanyTypeID;
                        ViewBag.CompanyID = company.CompanyId.ToString();
                        ViewBag.Position = recruitR1BudgetedModel.HMDesignation;
                        ViewBag.Year = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true), "AcademicYearId", "Duration");
                        ViewBag.EmploymentType = new SelectList(new DBHelper().GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");

                        if (company.CompanyTypeID > 1)
                        {
                            if (id > 0)
                                ViewBag.Company = GetCompanySelectList(recruitR1BudgetedModel.CompanyID);// new SelectList(companyList.Where(x => x.CompanyId == recruitR1BudgetedModel.CompanyID), "CompanyId", "Name");
                            else
                                ViewBag.Company = GetCompanySelectList();
                        }
                        else
                        {
                            ViewBag.Company = GetCompanySelectList(company.CompanyId);// new SelectList(companyList.Where(x => x.CompanyId == company.CompanyId), "CompanyId", "Name");
                        }
                        List<EmployeeDetailsModel> AllEmployeeList = new EmployeeDB().GetAllEmployeeForAdminByCompanyID(recruitR1BudgetedModel.CompanyID);
                        ViewBag.HiringMangr = new SelectList(AllEmployeeList, "EmployeeId", "FullName", recruitR1BudgetedModel.HMEmployeeID);
                        ViewBag.ReportingEmployee = new SelectList(AllEmployeeList, "EmployeeId", "FullName", recruitR1BudgetedModel.ReportingEmployeeID);

                        ViewBag.SalaryRanges = new SelectList(GetStaticSalaryRanges(), "Text", "Value");//new SelectList(new SalaryRangesDB().GetSalaryRanges(recruitR1BudgetedModel.CompanyID, recruitR1BudgetedModel.PositionID), "SalaryRangesID", "SalaryRanges");

                        ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
                        ViewBag.Department = new SelectList(new DepartmentDB().GetAllDepartment(userId: objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1", recruitR1BudgetedModel.DepartmentID);
                        ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
                        ViewBag.Category = new SelectList(new EmployeeProfileCreationFormDB().GetRecruitCategoryList(), "id", "text");
                        ViewBag.EmployeeAirfareFrequencyList = new SelectList(recruitR1BudgetedDB.GetEmployeeAirfareFrequencyList(), "id", "text");
                        ViewBag.EmployeeAirfareClassesList = new SelectList(recruitR1BudgetedDB.GetEmployeeAirfareClassesList(), "id", "text");
                        ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
                        HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
                        ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
                        recruitR1BudgetedModel.FormProcessID = id;                       
                        return View("Create", recruitR1BudgetedModel);
                    }
                    else
                        return RedirectToAction("ViewDetails");

                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");

        }
        public ActionResult UpdateDetails()
        {
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            RecruitR1BudgetedModel recruitR1BudgetedModel = new RecruitR1BudgetedModel();
            CompanyModel company;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int FormProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            if (FormProcessID > 0)
            {
                RequestFormsApproveModel requestFormsApproveModel = recruitR1BudgetedDB.GetPendingFormsApproval(FormProcessID);
                recruitR1BudgetedModel = recruitR1BudgetedDB.GetForm(FormProcessID, objUserContextViewModel.UserId);
                recruitR1BudgetedModel.ActualBudget = decimal.Parse(recruitR1BudgetedModel.ActualBudget.ToString(amountFormat));
                recruitR1BudgetedModel.SalikAmount = decimal.Parse(recruitR1BudgetedModel.SalikAmount.ToString(amountFormat));
                recruitR1BudgetedModel.PetrolCardAmount = decimal.Parse(recruitR1BudgetedModel.PetrolCardAmount.ToString(amountFormat));
                if (recruitR1BudgetedModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    company = new CompanyDB().GetCompany(recruitR1BudgetedModel.CompanyID);
                    //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
                    ViewBag.RecruitR1BudgetedID = recruitR1BudgetedModel.ID;
                    ViewBag.FormProcessID = FormProcessID.ToString();
                    ViewBag.RequestID = recruitR1BudgetedModel.RequestID;
                    ViewBag.CompanyTypeID = company.CompanyTypeID;
                    ViewBag.CompanyID = company.CompanyId.ToString();
                    ViewBag.Position = recruitR1BudgetedModel.HMDesignation;
                    ViewBag.Year = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true), "AcademicYearId", "Duration");
                    ViewBag.EmploymentType = new SelectList(new DBHelper().GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");

                    if (company.CompanyTypeID > 1)
                    {
                        if (FormProcessID > 0)
                            ViewBag.Company = GetCompanySelectList(recruitR1BudgetedModel.CompanyID);// new SelectList(companyList.Where(x => x.CompanyId == recruitR1BudgetedModel.CompanyID), "CompanyId", "Name");
                        else
                            ViewBag.Company = GetCompanySelectList();
                    }
                    else
                    {
                        ViewBag.Company = GetCompanySelectList(company.CompanyId);// new SelectList(companyList.Where(x => x.CompanyId == company.CompanyId), "CompanyId", "Name");
                    }
                    List<EmployeeDetailsModel> AllEmployeeList = new EmployeeDB().GetAllEmployeeForAdminByCompanyID(recruitR1BudgetedModel.CompanyID);
                    ViewBag.HiringMangr = new SelectList(AllEmployeeList, "EmployeeId", "FullName", recruitR1BudgetedModel.HMEmployeeID);
                    ViewBag.ReportingEmployee = new SelectList(AllEmployeeList, "EmployeeId", "FullName", recruitR1BudgetedModel.ReportingEmployeeID);
                    ViewBag.SalaryRanges = new SelectList(GetStaticSalaryRanges(), "Text", "Value");
                    ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
                    ViewBag.Department = new SelectList(new DepartmentDB().GetAllDepartment(userId: objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1", recruitR1BudgetedModel.DepartmentID);
                    ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
                    ViewBag.Category = new SelectList(new EmployeeProfileCreationFormDB().GetRecruitCategoryList(), "id", "text");
                    ViewBag.EmployeeAirfareFrequencyList = new SelectList(recruitR1BudgetedDB.GetEmployeeAirfareFrequencyList(), "id", "text");
                    ViewBag.EmployeeAirfareClassesList = new SelectList(recruitR1BudgetedDB.GetEmployeeAirfareClassesList(), "id", "text");
                    ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
                    HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
                    ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
                    recruitR1BudgetedModel.FormProcessID = FormProcessID;
                    return View(recruitR1BudgetedModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");

        }
        [HttpPost]
        public ActionResult SaveForm()
        {
            RequesterInfoViewModel requesterInfoData = new RequesterInfoViewModel();
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            HttpPostedFileBase fileJD, fileOC, fileMP;
            string formProcessIDs = "";
            int result = 0;
            int formprocessid = 0;

            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            RecruitR1BudgetedModel ObjRecruitR1BudgetedModel = new RecruitR1BudgetedModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            try
            {
                var data = Request.Form;
                ObjRecruitR1BudgetedModel.ID = Convert.ToInt32(string.IsNullOrEmpty(data["RecruitR1BudgetedID"]) ? "0" : data["RecruitR1BudgetedID"]);

                if (!string.IsNullOrEmpty(data["CompanyID"]))
                    ObjRecruitR1BudgetedModel.CompanyID = Int16.Parse(data["CompanyID"]);

                ObjRecruitR1BudgetedModel.AcademicYearID = Int16.Parse(data["AcademicYearID"]);
                ObjRecruitR1BudgetedModel.EmploymentModeID = int.Parse(data["EmploymentModeID"]);
                ObjRecruitR1BudgetedModel.UserTypeID = int.Parse(data["UserTypeID"]);
                ObjRecruitR1BudgetedModel.RecCategoryID = short.Parse(data["RecCategoryID"]);
                ObjRecruitR1BudgetedModel.EmployeeJobCategoryID = int.Parse(data["EmployeeJobCategoryID"]);
                ObjRecruitR1BudgetedModel.DepartmentID = int.Parse(data["DepartmentID"]);
                ObjRecruitR1BudgetedModel.ProjectData = Convert.ToString(data["ProjectData"]);
                ObjRecruitR1BudgetedModel.SalaryRanges = Convert.ToString(data["SalaryRanges"]);
                ObjRecruitR1BudgetedModel.HMEmployeeID = !string.IsNullOrEmpty(data["HMEmployeeID"]) ? int.Parse(data["HMEmployeeID"]) : (int?)null;
                ObjRecruitR1BudgetedModel.PositionLocation = Convert.ToString(data["PositionLocation"]);
                //ObjRecruitR1BudgetedModel.JobGradeID = int.Parse(data["JobGradeID"]);
                ObjRecruitR1BudgetedModel.JobGradeID = string.IsNullOrEmpty(data["JobGradeID"]) ? (int?)null : Convert.ToInt32(data["JobGradeID"]);
                ObjRecruitR1BudgetedModel.ReportingEmployeeID = !string.IsNullOrEmpty(data["ReportingEmployeeID"]) ? int.Parse(data["ReportingEmployeeID"]) : (int?)null;
                ObjRecruitR1BudgetedModel.MinExpRequired = Convert.ToString(data["MinExpRequired"]);
                ObjRecruitR1BudgetedModel.SourceID = int.Parse(data["SourceID"]);
                ObjRecruitR1BudgetedModel.RecruitFromID = Int16.Parse(data["RecruitFromID"]);
                ObjRecruitR1BudgetedModel.VehicleToolTrade = bool.Parse(data["VehicleToolTrade"]);
                ObjRecruitR1BudgetedModel.ContractStatus = Convert.ToString(data["ContractStatus"]);
                ObjRecruitR1BudgetedModel.FamilySpouse = Convert.ToString(data["FamilySpouse"]);
                ObjRecruitR1BudgetedModel.AnnualAirTicket = bool.Parse(data["AnnualAirTicket"]);
                ObjRecruitR1BudgetedModel.AirfareFrequencyID = string.IsNullOrEmpty(data["AirfareFrequencyID"]) ? Convert.ToInt16("0") : Int16.Parse(data["AirfareFrequencyID"]);
                ObjRecruitR1BudgetedModel.AirfareClassID = string.IsNullOrEmpty(data["AirfareFrequencyID"]) ? 0 : int.Parse(data["AirfareClassID"]);
                ObjRecruitR1BudgetedModel.HealthInsurance = bool.Parse(data["HealthInsurance"]);
                ObjRecruitR1BudgetedModel.LifeInsurance = bool.Parse(data["LifeInsurance"]);
                ObjRecruitR1BudgetedModel.SalikTag = bool.Parse(data["SalikTag"]);
                ObjRecruitR1BudgetedModel.SalikAmount = decimal.Parse(decimal.Parse(string.IsNullOrEmpty(data["SalikAmount"]) ? "0" : data["SalikAmount"]).ToString(amountFormat));
                ObjRecruitR1BudgetedModel.PetrolCard = bool.Parse(data["PetrolCard"]);
                ObjRecruitR1BudgetedModel.PetrolCardAmount = decimal.Parse(decimal.Parse(string.IsNullOrEmpty(data["PetrolCardAmount"]) ? "0" : data["PetrolCardAmount"]).ToString(amountFormat));
                ObjRecruitR1BudgetedModel.ParkingCard = bool.Parse(data["ParkingCard"]);
                ObjRecruitR1BudgetedModel.ParkingAreas = Convert.ToString(data["ParkingAreas"]);
                ObjRecruitR1BudgetedModel.Comments = Convert.ToString(data["Comments"]);
                ObjRecruitR1BudgetedModel.SalaryRanges = Convert.ToString(data["SalaryRanges"]);
                ObjRecruitR1BudgetedModel.HeadCount = 1;
                if (ObjRecruitR1BudgetedModel.ID > 0)
                {
                    ObjRecruitR1BudgetedModel.JobDescriptionFileID = Convert.ToInt32(string.IsNullOrEmpty(data["JobDescriptionFileID"]) ? "0" : data["JobDescriptionFileID"]);
                    ObjRecruitR1BudgetedModel.OrgChartFileID = Convert.ToInt32(string.IsNullOrEmpty(data["OrgChartFileID"]) ? "0" : data["OrgChartFileID"]);
                    ObjRecruitR1BudgetedModel.ManPowerFileID = Convert.ToInt32(string.IsNullOrEmpty(data["ManPowerFileID"]) ? "0" : data["ManPowerFileID"]);
                }
                else
                {
                    ObjRecruitR1BudgetedModel.JobDescriptionFileID = 0;
                    ObjRecruitR1BudgetedModel.OrgChartFileID = 0;
                    ObjRecruitR1BudgetedModel.ManPowerFileID = 0;
                }

                ObjRecruitR1BudgetedModel.DivisionID = string.IsNullOrEmpty(data["DivisionID"]) ? (int?)null : Convert.ToInt32(data["DivisionID"]);
                //int.Parse(data["DivisionID"]);
                ObjRecruitR1BudgetedModel.CreatedBy = objUserContextViewModel.UserId;
                ObjRecruitR1BudgetedModel.ModifiedBy = objUserContextViewModel.UserId;
                ObjRecruitR1BudgetedModel.RequesterEmployeeID = objUserContextViewModel.UserId;

                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
                if (Request.Files.Count > 0)
                {
                    if (Request.Files["JobDescription"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileJD = Request.Files["JobDescription"];
                        //Setting the file Id for updating
                        ObjFile.FileID = ObjRecruitR1BudgetedModel.JobDescriptionFileID;
                        ObjFile.FileName = fileJD.FileName;
                        ObjFile.FileContentType = fileJD.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileJD);
                        ObjFile.FormFileIDName = "JobDescriptionFileID";
                        uploadFileList.Add(ObjFile);
                        //
                        ObjRecruitR1BudgetedModel.JobDescriptionFileID = 0;
                    }
                    if (Request.Files["OrganizationChart"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileOC = Request.Files["OrganizationChart"];
                        ObjFile.FileID = ObjRecruitR1BudgetedModel.OrgChartFileID;
                        ObjFile.FileName = fileOC.FileName;
                        ObjFile.FileContentType = fileOC.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileOC);
                        ObjFile.FormFileIDName = "OrgChartFileID";
                        uploadFileList.Add(ObjFile);
                        ObjRecruitR1BudgetedModel.OrgChartFileID = 0;
                    }

                    if (Request.Files["ManpowerPlan"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileMP = Request.Files["ManpowerPlan"];
                        ObjFile.FileID = ObjRecruitR1BudgetedModel.ManPowerFileID;
                        ObjFile.FileName = fileMP.FileName;
                        ObjFile.FileContentType = fileMP.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileMP);
                        ObjFile.FormFileIDName = "ManPowerFileID";
                        uploadFileList.Add(ObjFile);
                        ObjRecruitR1BudgetedModel.ManPowerFileID = 0;


                    }
                }
                ObjRecruitR1BudgetedModel.AllFormsFilesModelList = uploadFileList;

                if (ObjRecruitR1BudgetedModel.ID > 0)
                {
                    //ObjRecruitR1BudgetedModel.HeadCount = 1;
                    ObjRecruitR1BudgetedModel.ReqStatusID = Convert.ToInt16(data["hdnReqStatusID"]);
                    formprocessid = Convert.ToInt32(string.IsNullOrEmpty(data["FormProcessID"]) ? "0" : data["FormProcessID"]);
                    RequestFormsApproveModel requestFormsApproveModel = recruitR1BudgetedDB.GetPendingFormsApproval(formprocessid);
                    if ((ObjRecruitR1BudgetedModel.ReqStatusID == (int)RequestStatus.Rejected && ObjRecruitR1BudgetedModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || isEditRequestFromAllRequests)
                    {
                        ObjRecruitR1BudgetedModel.FormProcessID = formprocessid;
                        result = new RecruitR1BudgetedDB().UpdateForm(ObjRecruitR1BudgetedModel);
                        formProcessIDs = ObjRecruitR1BudgetedModel.FormProcessID.ToString();
                    }
                    else
                    {
                        operationDetails.Success = true;
                        operationDetails.Message = "No permissions to update.";
                        operationDetails.CssClass = "error";
                        return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    }
                    if (result > 0)
                    {
                        if (ObjRecruitR1BudgetedModel.ReqStatusID == (int)RequestStatus.Rejected && ObjRecruitR1BudgetedModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        {
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = recruitR1BudgetedDB.GetApproverEmailList(formProcessIDs, ObjRecruitR1BudgetedModel.RequesterEmployeeID);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        }

                        operationDetails.InsertedRowId = result;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request updated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while updating Details";
                        operationDetails.CssClass = "error";
                    }
                }
                else
                {
                    //ObjRecruitR1BudgetedModel.HeadCount = int.Parse(data["HeadCount"]);
                    ObjRecruitR1BudgetedModel.PositionID = int.Parse(data["PositionID"]);
                    //ObjRecruitR1BudgetedModel.SalaryRangesID = int.Parse(data["SalaryRangesID"]);
                    ObjRecruitR1BudgetedModel.ActualBudget = decimal.Parse(decimal.Parse(string.IsNullOrEmpty(data["ActualBudget"]) ? "0" : data["ActualBudget"]).ToString(amountFormat));
                    formprocessid = 0;
                    requestFormsProcessModelList = new RecruitR1BudgetedDB().SaveForm(ObjRecruitR1BudgetedModel);
                    if (requestFormsProcessModelList != null)
                    {
                        formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = recruitR1BudgetedDB.GetApproverEmailList(formProcessIDs, ObjRecruitR1BudgetedModel.RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);

                        operationDetails.InsertedRowId = 1;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request generated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while Saving Details";
                        operationDetails.CssClass = "error";

                    }

                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
                operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewDetails(int? id)
        {
            int detailId = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            RecruitR1BudgetedViewModel recruitR1BudgetedViewModel = new RecruitR1BudgetedViewModel();
            recruitR1BudgetedViewModel = recruitR1BudgetedDB.GetFormDetails(detailId);
            List<RequestFormsApproveModel> requestFormsApproveModelList = recruitR1BudgetedDB.GetAllRequestFormsApprovals(detailId);
            recruitR1BudgetedViewModel.RecruitR1BudgetedModel.RequestFormsApproveModelList = requestFormsApproveModelList;
            recruitR1BudgetedViewModel.RecruitR1BudgetedModel.FormProcessID = detailId;
            return View("ViewDetail", recruitR1BudgetedViewModel);
        }
        public List<SelectListItem> GetStaticSalaryRanges()
        {
            List<SelectListItem> listItem = new List<SelectListItem>();

            listItem.Add(new SelectListItem { Text = "0-1000", Value = "0-1000" });
            listItem.Add(new SelectListItem { Text = "1001-3000", Value = "1001-3000" });
            listItem.Add(new SelectListItem { Text = "3001-4000", Value = "3001-4000" });
            listItem.Add(new SelectListItem { Text = "4001-6000", Value = "4001-6000" });
            listItem.Add(new SelectListItem { Text = "6001-9000", Value = "6001-9000" });
            listItem.Add(new SelectListItem { Text = "9001-12000", Value = "9001-12000" });
            listItem.Add(new SelectListItem { Text = "12001-15000", Value = "12001-15000" });
            listItem.Add(new SelectListItem { Text = "15001-20000", Value = "15001-20000" });
            listItem.Add(new SelectListItem { Text = "20001-25000", Value = "20001-25000" });
            listItem.Add(new SelectListItem { Text = "25001-30000", Value = "25001-30000" });
            listItem.Add(new SelectListItem { Text = "30001-35000", Value = "30001-35000" });
            listItem.Add(new SelectListItem { Text = "35001-40000", Value = "35001-40000" });
            listItem.Add(new SelectListItem { Text = "40001-45000", Value = "40001-45000" });
            listItem.Add(new SelectListItem { Text = "45001-50000", Value = "45001-50000" });
            listItem.Add(new SelectListItem { Text = "50001-60000", Value = "50001-60000" });
            listItem.Add(new SelectListItem { Text = "60001-70000", Value = "60001-70000" });
            listItem.Add(new SelectListItem { Text = "70001-80000", Value = "70001-80000" });
            return listItem;
        }
        
    }
}