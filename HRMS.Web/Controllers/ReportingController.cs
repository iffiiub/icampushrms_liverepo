﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using HRMS.DataAccess.GeneralDB;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using System.Data;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using Stimulsoft.Report.MvcMobile;
using Stimulsoft.Report.Components;
using System.IO;
//using Novacode;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml;
using NotesFor.HtmlToOpenXml;
using System.Text.RegularExpressions;
using HRMS.Entities.Report;
using Stimulsoft.Report.Components.Table;
using System.Text;
using HRMS.Entities.General;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using System.Web;
using Svg;
using System.Drawing.Imaging;
using Stimulsoft.Report.Web;
using HRMS.Web.CommonHelper;
using System.Web.Caching;
using System.Data.SqlClient;
using System.IO.Compression;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel;
using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO.Packaging;

namespace HRMS.Web.Controllers
{
    public class ReportingController : BaseController
    {
        ReportingDB objReportingDB = new ReportingDB();
        EmployeeDB objEmployeeDB = new EmployeeDB();
        DataHelper helper;
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult EmployeeLabourContractReportingViewer()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All Departments", Value = "" }));
            ViewBag.DepartmentList = deplist;

            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> sectionlist;
            sectionlist = new SelectList(new EmployeeSectionDB().GetEmployeeSection(UserId), "EmployeeSectionID", "EmployeeSectionName_1").ToList();
            sectionlist.Insert(0, (new SelectListItem { Text = "All Sections", Value = "" }));
            ViewBag.SectionList = sectionlist;
            DocumentDB documentdb = new DocumentDB();
            List<SelectListItem> documentTypeList = new SelectList(documentdb.GetDocumentTypeList().Where(x => x.IsActive == true).ToList(), "docTypeId", "docTypeName").ToList();
            documentTypeList.Insert(0, new SelectListItem { Text = "All Document Types", Value = "0" });
            ViewBag.documentType = documentTypeList;
            return View();
        }

        public ActionResult GetExpireDocumentReport(string EmpIds, int? DeptId, int? SectionId, int? DocTypeId, bool isPrimary, bool isExpired, bool groupByEmp, bool groupByDept, bool groupBySect, bool groupByDocType)
        {
            ViewBag.ActionUrl = "GetReportSnapshotEmployeeLabourContractReporting";
            return View("ReportViewer");
        }

        public ActionResult GetReportSnapshotEmployeeLabourContractReporting(string EmpIds, int? DeptId, int? SectionId, int? DocTypeId, bool isPrimary, bool isExpired, bool groupByEmp, bool groupByDept, bool groupBySect, bool groupByDocType)
        {
            DocumentDB documentdb = new DocumentDB();
            //List<string> documentSettingday = documentdb.GetDocumentsettingDays();
            List<DocumentsModel> listDocumentModel = objReportingDB.GetDocumentList(EmpIds, DeptId, SectionId, DocTypeId, isPrimary, isExpired);
            StiReport report = new StiReport();
            if (listDocumentModel.Count > 0)
            {

                report.Load(Server.MapPath("~/Content/Reports/EmployeeLabourContractReporting.mrt"));
                SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
                schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
                schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegData("DocumentsModelData", listDocumentModel);
                try
                {
                    StiComponent scGroupByEmp = report.GetComponentByName("GroupHeaderEmpID");
                    StiComponent scGroupByDep = report.GetComponentByName("GroupHeaderEmpDepID");
                    StiComponent scGroupBySec = report.GetComponentByName("GroupHeaderSecID");
                    StiComponent scGroupByDocType = report.GetComponentByName("GroupHeaderDocTypeID");
                    if (!groupByEmp)
                    {
                        scGroupByEmp.Enabled = false;
                    }
                    if (!groupByDept)
                    {
                        scGroupByDep.Enabled = false;
                    }
                    if (!groupByDocType)
                    {
                        scGroupByDocType.Enabled = false;
                    }
                    if (!groupBySect)
                    {
                        scGroupBySec.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    //if component not found in report.
                }
            }
            else
            {
                report = NoDataReport();
            }

            return StiMvcViewer.GetReportSnapshotResult(report);
        }



        public ActionResult EmployeeLabourCardStatusReportingViewer()
        {
            return View();
        }

        public ActionResult GetReportSnapshotEmployeeLabourCardStatusReporting()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<DocumentsModel> lstGeneralModel = objReportingDB.GetEmployeeLabourCardStatusReporting(UserId);
            StiReport report = new StiReport();
            if (lstGeneralModel.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/EmployeeLabourCardStatusReporting.mrt"));
                SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
                schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
                schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegData("DocumentsModelData", lstGeneralModel);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult DetailStaffReportingViewer()
        {
            return View();
        }

        public ActionResult GetDetailStaffReporting()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<LabourContract> lstGeneralModel = objReportingDB.GetDetailStaffReporting(UserId);
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            StiReport report = new StiReport();
            if (lstGeneralModel.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/DetailStaffReporting.mrt"));
                schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
                schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegData("DocumentsModelData", lstGeneralModel);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        //--------------------------------------------------------CompanyStaffDetail-------------------------------------------------------------------------------
        public ActionResult CompanyStaffDetailReport()
        {
            HRDegreeDB objHRDegreeDB = new HRDegreeDB();
            ViewBag.Degree = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1");
            List<SelectListItem> qualificationlist;
            qualificationlist = new SelectList(objHRDegreeDB.GetAllHRDegree(), "HRDegreeID", "HRDegreeName_1").ToList();
            ViewBag.qualificationlist = qualificationlist;
            return View();
        }
        public ActionResult CompanyStaffDetailReportViewer()
        {
            ViewBag.ActionUrl = "GetCompanyStaffDetailReportViewer";
            ViewBag.ActionExportUrl = "ExportCompanyStaffDetailReport";
            ViewBag.ActionPrintUrl = "PrintCompanyStaffDetailReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetCompanyStaffDetailReportViewer(string QualificationIds, string txtYearAtCompany)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetCompanyStaffDetailReport(QualificationIds, txtYearAtCompany));
        }

        public ActionResult PrintCompanyStaffDetailReport(string QualificationIds, string txtYearAtCompany)
        {
            StiReport report = GetCompanyStaffDetailReport(QualificationIds, txtYearAtCompany);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportCompanyStaffDetailReport(string QualificationIds, string txtYearAtCompany)
        {
            StiReport report = GetCompanyStaffDetailReport(QualificationIds, txtYearAtCompany);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        public StiReport GetCompanyStaffDetailReport(string QualificationIds, string txtYearAtCompany)
        {
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            CompanyEmployeeDetailReportingModel objCompanyEmployeeDetailReportingModel = new CompanyEmployeeDetailReportingModel();
            objCompanyEmployeeDetailReportingModel = objReportingDB.GetCompanyStaffDetailHearderReporting().FirstOrDefault();
            List<StaffDetailsReportModel> StaffDetailsReportModelList = objReportingDB.GetCompanyStaffDetailContentReporting(QualificationIds, txtYearAtCompany);
            //objCompanyEmployeeDetailReportingModel.GeneralModel = lstGeneralModel;
            StiReport report = new StiReport();
            if (StaffDetailsReportModelList.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/CompanyStaffDetailReport.mrt"));
                report.RegBusinessObject("StaffDetailsReportModel", StaffDetailsReportModelList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report["Number of employee employed for 1 year or less"] = "Value";
                report["Variable2"] = "Value";
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------
        public ActionResult StaffPasswordDetail()
        {
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            EmployeeDB employeeDB = new EmployeeDB();
            if ((userContext.UserRolls.FindAll(x => x.ToLower().Contains("admin")).Count() > 0) && (userContext.UserId == 0))
            {
                ViewBag.EmployeeList = new SelectList(employeeDB.GetAllEmployeeActiveByCredential(userContext.UserId).ToList(), "EmployeeId", "Name");
            }
            else
            {
                ViewBag.EmployeeList = new SelectList(employeeDB.GetAllEmployeeActiveByCredential(userContext.UserId).Where(x => x.EmployeeId > 0).ToList(), "EmployeeId", "Name");
            }
            return View();
        }

        public ActionResult StaffPasswordDetailReportingViewer(string EmployeeId)
        {
            ViewBag.EmployeeId = EmployeeId;
            TempData["EmployeeId"] = EmployeeId;
            return View();
        }

        public ActionResult GetStaffPasswordDetailReporting(string EmployeeId)
        {
            StiReport report = new StiReport();
            List<GeneralModel> objGeneralModelList = new List<GeneralModel>();
            objGeneralModelList = objReportingDB.GetCompanyDetailReporting(EmployeeId);
            GeneralModel fg = new GeneralModel();
            if (objGeneralModelList.Count > 0)
            {

                for (int i = 0; i < objGeneralModelList.Count(); i++)
                {
                    try
                    {
                        //objGeneralModelList[i].Value3 = HRMS.Web.CommonHelper.CommonHelper.decryptPassword(objGeneralModelList[i].Value3);
                        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                        int url = Request.Url.Port;
                        //string[] splitedurl = url.Split('/');
                        xmlDoc.Load(Server.MapPath("~/Content/Reports/StaffPasswordReport.mrt"));
                        objGeneralModelList[i].Value6 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + objGeneralModelList[i].Value6.TrimStart('~'); //"http://localhost:3234/images/logo.jpg";
                    }
                    catch { }
                }

                SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
                schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
                schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.Load(Server.MapPath("~/Content/Reports/StaffPasswordReport.mrt"));
                report.RegBusinessObject("GeneralModel", objGeneralModelList);

            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult ActionITypedList()
        {
            return View("ViewITypedList");
        }

        //public ActionResult GetReportSnapshotITypedList()
        //{

        //    StiReport report = new StiReport();
        //    // report.LoadDocument(Server.MapPath("~/Content/SimpleList.mdc"));
        //    report.Load(Server.MapPath("~/Content/EmployeeDetails.mrt"));
        //    //report.RegData("EmployeeITypedList", CreateBusinessObjectsITypedList.GetEmployees());
        //    report.RegBusinessObject("EmployeeDetailsWithContectInfo", objEmployeeDB.GetALLEmployeeListForVisaInformationReporting());
        //    // report.RegData("EmployeeITypedList", objEmployeeDB.GetALLEmployeeList());

        //    return StiMvcViewer.GetReportSnapshotResult(report);
        //}

        //public ActionResult GetReportTemplate()
        //{
        //    StiReport report = new StiReport();
        //    // report.Load(Server.MapPath("~/Content/Reports/TwoSimpleLists.mrt"));
        //    report.Load(Server.MapPath("~/Content/EmployeeDetails.mrt"));
        //    DataSet data = new DataSet();
        //    data.ReadXml(Server.MapPath("~/Content/Data/Demo.xml"));
        //    //report.RegData(data);
        //    report.RegBusinessObject("EmployeeDetailsWithContectInfo", objEmployeeDB.GetALLEmployeeListForVisaInformationReporting());

        //    return StiMvcMobileDesigner.GetReportTemplateResult(HttpContext, report);
        //}

        public ActionResult GetReportSnapshot()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            StiReport report = StiMvcMobileDesigner.GetReportObject(HttpContext);
            report.RegBusinessObject("EmployeeDetailsWithContectInfo", objEmployeeDB.GetALLEmployeeListWithPermission(objUserContextViewModel.UserId));
            return StiMvcMobileDesigner.GetReportSnapshotResult(HttpContext, report);
        }

        public ActionResult OpenReportTemplate()
        {
            DataSet data = new DataSet();
            data.ReadXml(Server.MapPath("~/Content/Data/Demo.xml"));
            // return StiMvcMobileDesigner.OpenReportTemplateResult((HttpContext, objEmployeeDB.GetALLEmployeeList());

            return StiMvcMobileDesigner.OpenReportTemplateResult(HttpContext, data);
        }

        public ActionResult SaveReportTemplate()
        {
            StiReport report = StiMvcMobileDesigner.GetReportObject(HttpContext);
            report.Save(Server.MapPath("~/Content/" + report.ReportName + ".mrt"));

            return StiMvcMobileDesigner.SaveReportTemplateResult(HttpContext);
        }

        public ActionResult GetNewReportData()
        {
            string commandName = StiMvcMobileDesigner.GetActionCommandName(HttpContext);

            DataSet data = new DataSet();
            data.ReadXmlSchema(Server.MapPath("~/Content/Data/Demo.xsd"));
            data.ReadXml(Server.MapPath("~/Content/Data/Demo.xml"));

            return StiMvcMobileDesigner.GetNewReportDataResult(HttpContext, data);
        }

        public ActionResult DesignerEvent()
        {
            return StiMvcMobileDesigner.DesignerEventResult(HttpContext);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        private StiReport NoDataReport()
        {
            StiReport report = new StiReport();
            report.Load(Server.MapPath("~/Content/NoDataReport.mrt"));
            return report;
        }

        public ActionResult StaffInformationReportingViewer()
        {
            return View();
        }

        public ActionResult GetStaffInformationReporting()
        {
            StiReport report = new StiReport();
            GeneralModel model = new ReportingDB().GetStaffInformationReport();
            if (model != null)
            {
                report.Load(Server.MapPath("~/Content/StaffInformationReport.mrt"));
                report.RegBusinessObject("GeneralModel", model);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult StaffResponsibilities()
        {
            //List<EnumData> lst = EnumExtensions.GetDescriptions(typeof(enumStaffType));
            //ViewBag.StaffTypeList = new SelectList(lst, "Id", "Name");
            return View();
        }

        public ActionResult StaffResponsibiltiesReportingViewer(int staffTypeId)
        {
            TempData["StaffTypeId"] = ViewBag.StaffTypeId = staffTypeId;
            return View();
        }

        public ActionResult GetStaffResponsibilitiesReport(int staffTypeId)
        {
            StiReport report = new StiReport();

            List<StaffResponsibilities> lst = new ReportingDB().GetStaffResponsibilitiesReport(staffTypeId);
            if (lst.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/StaffResponsibilitiesReport.mrt"));
                report.RegBusinessObject("Staff", lst);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        #region AttendanceReports

        public JsonResult FilterSection(int? DeptId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> sectionlist;
            if (Convert.ToInt32(DeptId) != 0)
                sectionlist = new SelectList(new EmployeeSectionDB().GetEmployeeSection(UserId)
                                                                .Where(x => x.DepartmentID == DeptId), "EmployeeSectionID", "EmployeeSectionName_1").ToList();
            else
                sectionlist = new SelectList(new EmployeeSectionDB().GetEmployeeSection(UserId), "EmployeeSectionID", "EmployeeSectionName_1").ToList();
            sectionlist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            return Json(sectionlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FilterEmployee(int superVisiorId, bool? isActive)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            if (superVisiorId == 0)
                emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(isActive, UserId), "EmployeeId", "FirstName").ToList();
            else
                emplist = new SelectList(objEmployeeDB.GetEmployeeBySupervisiorId(superVisiorId, isActive), "EmployeeId", "FirstName").ToList();
            emplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            return Json(emplist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AttendanceReportViewer(string id)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.PageId = id;

            //Departments
            //List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartment(companyId: null, userId: objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.DepartmentList = deplist;

            //Employees           
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            emplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.EmployeeList = emplist;

            //Supevisor
            List<SelectListItem> supervisiorlist = new SelectList(objEmployeeDB.GetSupervisiorList(), "EmployeeId", "FullName").ToList();
            supervisiorlist.Insert(0, (new SelectListItem { Text = "Select Supervisior", Value = "" }));
            ViewBag.supervisiorlist = supervisiorlist;

            //Sections
            List<SelectListItem> sectionlist = new SelectList(new EmployeeSectionDB().GetEmployeeSection(UserId), "EmployeeSectionID", "EmployeeSectionName_1").ToList();
            sectionlist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.SectionList = sectionlist;

            //Shifts
            List<SelectListItem> shiftlist = new SelectList(new ShiftDB().GetAllShiftNames(objUserContextViewModel.CompanyId), "ShiftID", "ShiftName").ToList();
            shiftlist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.ShiftList = shiftlist;

            //Months
            var values = from enumMonths e in Enum.GetValues(typeof(enumMonths))
                         select new { Id = (int)e, Name = e.ToString() };
            ViewBag.MonthsList = new SelectList(values, "Id", "Name");
            //Years
            ViewBag.YearsList = new SelectList(new AcademicYearDB().GetAcademicYears(), "Year", "Year", DateTime.Now.Year);
            return View();
        }

        public ActionResult FilteredAttendanceReportViewer(string pageId, int? depId, int? empId, int? sectionId, int? shiftId,
              bool inactiveOnly, bool? postedOnly, bool? detailReports, string fromDate, string toDate, string statusID, string OrderBy, string OrderType, int? month, int? year, int? superVisorId, bool? isListReport, bool? isEmployeePageWise, bool? isPercentageBasedOndays)
        {
            pageId = pageId.ToLower();
            bool? activeStatus = null;
            if (inactiveOnly)
                activeStatus = false;
            else
            {
                activeStatus = true;
            }
            ReportingDB reprtDB = new ReportingDB();
            if (Convert.ToInt32(month) > 0)
            {
                DateTime now = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                var startDate = new DateTime(now.Year, now.Month, 1);
                fromDate = (startDate).ToString(HRMSDateFormat);
                toDate = (startDate.AddMonths(1).AddDays(-1)).ToString(HRMSDateFormat);
            }
            //Get attendance list
            //Get general Info
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation();
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            generalModel.Value2 = schoolInfo.SchoolName_1;
            generalModel.Value3 = fromDate;
            generalModel.Value4 = toDate;
            generalModel.Value6 = schoolInfo.CampusName_1;
            generalModel.Value5 = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            List<AttendanceReportModel> list = new List<AttendanceReportModel>();
            reprtDB.nonexcusedString = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Content/images/crossIcon.ico";
            reprtDB.excusedString = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Content/images/tick1.ico";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.Employee> sortedEmployeeList = new List<HRMS.Entities.Employee>();
            sortedEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);
            List<AttendanceReportModel> listAttendanceReport = new List<AttendanceReportModel>();



            switch (pageId)
            {
                case "workingdays":
                    listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 1, detailReports, superVisorId, empId, null, postedOnly, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null
                    listAttendanceReport = (from Emplist in listAttendanceReport
                                            join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                            select Emplist).ToList();
                    Session["TempReportData"] = listAttendanceReport;
                    if (detailReports == true)
                        Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/WorkingDaysDetailReport.mrt"));
                    else
                        Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/WorkingDaysReport.mrt"));
                    break;
                case "workinghours":
                    listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 1, detailReports, superVisorId, empId, null, postedOnly, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null
                    listAttendanceReport = (from Emplist in listAttendanceReport
                                            join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                            select Emplist).ToList();
                    Session["TempReportData"] = listAttendanceReport;
                    Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/WorkingHourReport.mrt"));

                    break;
                case "lateandearlyminutes":
                    listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 1, detailReports, superVisorId, empId, null, postedOnly, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null
                    listAttendanceReport = (from Emplist in listAttendanceReport
                                            join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                            select Emplist).ToList();
                    Session["TempReportData"] = listAttendanceReport;
                    Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/LateAndEarlyReport.mrt"));
                    break;
                case "attendancestatistics":
                    if (OrderType == "asc")
                    {
                        listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 0, detailReports, superVisorId, empId, null, null, activeStatus, depId, sectionId, shiftId, null, isPercentageBasedOndays).OrderBy(x => x.AttendancePercent).ToList();// "2014-12-01", "2014-12-30", null
                        listAttendanceReport = (from Emplist in listAttendanceReport
                                                join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                                select Emplist).ToList();
                        Session["TempReportData"] = listAttendanceReport;
                    }
                    else
                    {
                        listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 0, detailReports, superVisorId, empId, null, null, activeStatus, depId, sectionId, shiftId, null, isPercentageBasedOndays).OrderByDescending(x => x.AttendancePercent).ToList();
                        listAttendanceReport = (from Emplist in listAttendanceReport
                                                join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                                select Emplist).ToList();
                        Session["TempReportData"] = listAttendanceReport;
                    }
                    Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/AttendanceStatistics.mrt"));
                    break;
                case "dailyattendancebystatus":
                    string sId = "";
                    try
                    {
                        if (statusID == "8D")
                            sId = "8";
                        else
                            sId = statusID;

                        string[] arr = statusID.Split(',');
                        if (arr.Length > 1)
                        {
                            statusID = "All";
                        }

                        listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 0, detailReports, superVisorId, empId, sId, postedOnly, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null
                        listAttendanceReport = (from Emplist in listAttendanceReport
                                                join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                                select Emplist).ToList();

                        Session["TempReportData"] = listAttendanceReport;
                    }
                    catch
                    {
                        Session["TempReportData"] = listAttendanceReport;
                    }
                    switch (statusID)
                    {
                        case "1":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByPresent.mrt"));
                                break;
                            }
                        case "2":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByAbsent.mrt"));
                                break;
                            }
                        case "3":
                        case "11":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByLateIn.mrt"));
                                break;
                            }
                        case "4":
                        case "12":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByEarlyOut.mrt"));
                                break;
                            }
                        case "8":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByLateInEarlyOut.mrt"));
                                break;
                            }
                        case "8D":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByLateInEarlyOutDetails.mrt"));
                                break;
                            }
                        case "9":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByDontCheckOut.mrt"));
                                break;
                            }
                        case "10":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByExtraDay.mrt"));
                                break;
                            }
                        case "13":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByDidntSignIn.mrt"));
                                break;
                            }
                        case "All":
                            {
                                Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/Status/DailyAttendanceByAllStatus.mrt"));
                                break;
                            }
                    }
                    break;
                case "employeedailyattendance":
                    listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 0, detailReports, superVisorId, empId, null, postedOnly, activeStatus, depId, sectionId, shiftId, null).OrderBy(x => x.EmpName).ToList();
                    listAttendanceReport = (from Emplist in listAttendanceReport
                                            join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                            select Emplist).ToList();
                    Session["TempReportData"] = listAttendanceReport;// "2014-12-01", "2014-12-30", null
                    if (detailReports == true)
                    {
                        if (isEmployeePageWise == true && isListReport == true)
                        {
                            Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendanceDetailsPageWise.mrt"));
                        }
                        else if (isListReport == true)
                            Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendanceDetailsListView.mrt"));
                        else
                            Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendanceDetails.mrt"));

                    }
                    else
                    {
                        if (isEmployeePageWise == true && isListReport == true)
                            Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendancePageWise.mrt"));
                        else if (isListReport == true)
                        {
                            Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendanceListView.mrt"));
                        }
                        else
                            Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendance.mrt"));
                    }
                    break;
                case "monthlyattendancesummary":
                    listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 2, detailReports, superVisorId, empId, null, null, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null
                    listAttendanceReport = (from Emplist in listAttendanceReport
                                            join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                            select Emplist).ToList();
                    Session["TempReportData"] = listAttendanceReport;
                    Session["ReportName"] = (Server.MapPath("~/Content/Reports/AttendanceReport/MonthlyAttendanceSummaryReport.mrt"));

                    break;
            }
            ViewBag.ActionUrl = "GetAttendanceReport";
            return View("ReportViewer");
        }

        public ActionResult GetAttendanceReport(string pageId, int? depId, int? empId, int? sectionId, int? shiftId,
             bool inactiveOnly, bool? postedOnly, bool? detailReports, string fromDate, string toDate, string statusID, string OrderBy, string OrderType, int? month, int? year, int? superVisorId, bool? isListReport, bool? isEmployeePageWise)
        {
            pageId = pageId.ToLower();
            StiReport report = new StiReport();
            List<AttendanceReportModel> list = new List<AttendanceReportModel>();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            generalModel.Value2 = schoolInfo.SchoolName_1;
            generalModel.Value3 = CommonDB.GetFormattedDate_DDMMYYYY(fromDate);
            generalModel.Value4 = CommonDB.GetFormattedDate_DDMMYYYY(toDate);
            generalModel.Value6 = schoolInfo.CampusName_1;
            generalModel.Value5 = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            list = (List<AttendanceReportModel>)Session["TempReportData"];
            report.Load(Session["ReportName"].ToString());
            report.RegBusinessObject("GeneralModel", generalModel);
            report.RegData("AttendanceData", list);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            if (isListReport == true)
            {
                if (isEmployeePageWise == false || pageId == "workingdays")
                    try
                    {
                        StiComponent sc = report.GetComponentByName("GroupHeaderBand1");
                        StiComponent sc1 = report.GetComponentByName("GroupHeaderBand2");
                        StiComponent sc2 = report.GetComponentByName("GroupHeaderBand3");
                        StiComponent sc3 = report.GetComponentByName("HeaderBand4");

                        switch (pageId)
                        {
                            case "workingdays":
                                (sc as StiGroupHeaderBand).Condition.Value = "{WorkingDaysData.EmpName}";
                                break;
                        }
                        if (sc != null)
                            sc.Enabled = false;
                        if (sc1 != null)
                            sc1.Enabled = false;
                        if (sc2 != null)
                            sc2.Enabled = false;
                        if (sc3 != null)
                            sc3.Enabled = false;
                    }
                    catch (Exception ex)
                    {
                        //if component not found in report.
                    }
            }

            if (list.Count() == 0)
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }
        #endregion

        #region Dedcution Reports

        public ActionResult DeductionReportviewer(string reportType, string startDate, string toDate, bool? bConfirmed, bool? bGenrated, bool? bShowAllRecord, int? month, int? year, int? deductionType, string SortBy = "")
        {
            ViewBag.ActionUrl = "DeductionReportAction";
            return View("ReportViewer");
        }

        public ActionResult DeductionReportAction(string reportType, string startDate, string toDate, bool? bConfirmed, bool? bGenrated, bool? bShowAllRecord, int? month, int? year, int? deductionType, string SortBy = "")
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            StiReport report = new StiReport();
            IEnumerable<DeductionReportModel> objGeneralModelList = new List<DeductionReportModel>();

            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            GeneralModel gModel = new GeneralModel();
            gModel.Value5 = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Dictionary = new Stimulsoft.Report.Dictionary.StiDictionary();
            if (reportType == "AbsentDeductable")
            {
                if (SortBy.Equals("1"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.EmployeeID);
                }
                else if (SortBy.Equals("2"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.Name);
                }
                else if (SortBy.Equals("3"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.AttendanceDateTime);
                }
                report.Load(Server.MapPath("~/Content/Reports/DeductionReport/AbsentdeductableReport.mrt"));
                gModel.Value2 = startDate + " to " + toDate;
            }
            else if (reportType == "AbsentDeductableSalary")
            {
                if (SortBy.Equals("1"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.EmployeeID);
                }
                else if (SortBy.Equals("2"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.Name);
                }
                else if (SortBy.Equals("3"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.AttendanceDateTime);
                }
                else if (SortBy.Equals("4"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.AmountInDecimal);
                }
                else if (SortBy.Equals("5"))
                {
                    objGeneralModelList = new ReportingDB().GetAbsentdeductableReport(objUserContextViewModel.UserId, startDate, toDate, bConfirmed, bGenrated, null).OrderBy(x => x.TotalSalaryDecimal);
                }
                report.Load(Server.MapPath("~/Content/Reports/DeductionReport/AbsentdeductablesalaryReport.mrt"));
                gModel.Value2 = startDate + " to " + toDate;
            }
            else if (reportType == "AbsentUndeductableReportViewer")
            {
                objGeneralModelList = new ReportingDB().GetAbsentUndeductableReport(startDate, toDate, objUserContextViewModel.UserId);
                report.Load(Server.MapPath("~/Content/Reports/DeductionReport/AbsentundeductableReport.mrt"));
                gModel.Value2 = startDate + " To " + toDate;
            }
            else if (reportType == "LateDeductibleReportViewer")
            {
                if (SortBy.Equals("1"))
                {
                    objGeneralModelList = new ReportingDB().GetLateDeductibleReport(1, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId).OrderBy(x => x.EmployeeID);
                }
                else if (SortBy.Equals("2"))
                {
                    objGeneralModelList = new ReportingDB().GetLateDeductibleReport(1, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId).OrderBy(x => x.Name);
                }
                else if (SortBy.Equals("3"))
                {
                    objGeneralModelList = new ReportingDB().GetLateDeductibleReport(1, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId).OrderBy(x => x.AttendanceDate);
                }
                else if (SortBy.Equals("4"))
                {
                    objGeneralModelList = new ReportingDB().GetLateDeductibleReport(1, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId).OrderBy(x => x.LateInMinutes);
                }
                else if (SortBy.Equals("5"))
                {
                    objGeneralModelList = new ReportingDB().GetLateDeductibleReport(1, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId).OrderBy(x => x.LateDedcutionAmount);
                }
                else if (SortBy.Equals("6"))
                {
                    objGeneralModelList = new ReportingDB().GetLateDeductibleReport(1, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId).OrderBy(x => x.TotalSalaryDecimal);
                }
                report.Load(Server.MapPath("~/Content/Reports/DeductionReport/Latedeductible.mrt"));
                if (startDate != null)
                    gModel.Value2 = startDate + " to " + toDate;
            }
            else if (reportType == "EarlyDeductibleReportViewer")
            {
                objGeneralModelList = new ReportingDB().GetEarlyDeductibleReport(2, startDate, toDate, bConfirmed, bGenrated, objUserContextViewModel.UserId);
                report.Load(Server.MapPath("~/Content/Reports/DeductionReport/Earlydeductible.mrt"));
                if (startDate != null)
                    gModel.Value2 = startDate + " to " + toDate;
            }
            if (reportType == "PayDeductionReport")
            {
                objGeneralModelList = new ReportingDB().GetDeductionReport(Convert.ToInt32(deductionType), startDate, toDate, objUserContextViewModel.UserId);
                report.Load(Server.MapPath("~/Content/Reports/DeductionReport/PayDeductionReport.mrt"));
                gModel.Value2 = startDate + " to " + toDate;
            }
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            report.RegBusinessObject("DeductionReport", objGeneralModelList);
            report.RegBusinessObject("GeneralModel", gModel);
            if (!(objGeneralModelList.Count() > 0))
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult AbsentDeductableReportViewer()
        {
            ViewBag.DedctionReport = "";
            return View();
        }

        public ActionResult AbsentDeductableSalaryReportViewer()
        {
            ViewBag.DedctionReport = " Salary";
            return View("AbsentDeductableReportViewer");
        }

        public ActionResult AbsentUndeductableReportViewer()
        {
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();

            ViewBag.ddlYear = new SelectList(objAcademicYearDB.GetAcademicYears(), "Year", "Year", DateTime.Now.Year.ToString());
            return View();
        }

        public ActionResult LateDeductableReportViewer()
        {
            return View();
        }

        public ActionResult EarlyDeductableReport()
        {
            return View();
        }

        public ActionResult EmployeeGratuityListReportViewer()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //Departments
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.DepartmentList = deplist;
            ViewBag.SortingList = new SelectList(DataAccess.GeneralDB.CommonDB.GetSortingList(), "Key", "Value").ToList();
            //Employees          
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            // emplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.EmployeeList = emplist;
            List<string> selectedValue = new List<string>();
            if (Session["EmployeeIds"] != null)
            {
                selectedValue = Session["EmployeeIds"].ToString().Split(',').Select(x => x).ToList();
                Session["EmployeeIds"] = null;
            }
            return View(selectedValue);
        }

        public ActionResult EmployeeGratuityExport(string EmployeeId, int order)
        {
            byte[] content;
            string fileName = "EmployeeGratuity" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB departmentDB = new DepartmentDB();
            System.Data.DataSet ds = new DataSet();
            ds.Tables.Add(new ReportingDB().GetGratuityListData(EmployeeId, null));
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public ActionResult EmployeeGratuityReportviewer(string EmployeeId, int order)
        {
            ViewBag.ActionUrl = "EmployeeGratuityListReportAction";
            return View("ReportViewer");
        }

        public ActionResult EmployeeGratuityListReportAction(string EmployeeId, int order)
        {
            bool isConvert = false;
            StiReport report = new StiReport();
            List<GratuityReportModel> objGratuityModelList = new List<GratuityReportModel>();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            GeneralModel gModel = new GeneralModel();
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            report.Dictionary = new Stimulsoft.Report.Dictionary.StiDictionary();
            gModel.Value6 = DateTime.Now.ToString("d-MMM-yyyy HH:mm:tt");
            GeneralAccSetting AccSetting = new ReportingDB().GetGeneralAccountSetting();
            gModel.Value5 = AccSetting.CurrencyCode;
            objGratuityModelList = new ReportingDB().GetGratuityListReport(EmployeeId, null);
            try
            {
                foreach (var item in objGratuityModelList)
                {
                    item.IntEmployeeAlternativeId = Convert.ToInt32(item.EmployeeAlternativeId);
                }
                isConvert = true;
            }
            catch
            {
                isConvert = false;
            }
            report.Load(Server.MapPath("~/Content/Reports/GratuityReport/EmployeeGratuityList.mrt"));
            report.RegData("EmployeeGratuityData", objGratuityModelList);
            report.RegBusinessObject("GeneralModel", gModel);
            StiComponent groupHeader = report.GetComponentByName("GroupHeaderBand3");
            StiComponent dataBand = report.GetComponentByName("DataBand2");
            StiComponent textId = report.GetComponentByName("Text16");
            //(groupHeader as StiGroupHeaderBand).Condition.Value = "{EmployeeGratuityData.DeptID}";
            switch (order)
            {
                case 1:
                    {
                        (dataBand as StiDataBand).Sort = new string[] { "ASC", "EmpName" };
                        break;
                    }
                case 2:
                    {
                        (dataBand as StiDataBand).Sort = new string[] { "DESC", "EmpName" };
                        break;
                    }
                case 3:
                    {
                        if (isConvert)
                            (dataBand as StiDataBand).Sort = new string[] { "ASC", "IntEmployeeAlternativeId" };
                        break;
                    }
                case 4:
                    {
                        if (isConvert)
                            (dataBand as StiDataBand).Sort = new string[] { "DESC", "IntEmployeeAlternativeId" };
                        break;
                    }
            }
            if (isConvert)
                (textId as StiText).Text = "{EmployeeGratuityData.IntEmployeeAlternativeId}";
            else
                (textId as StiText).Text = "{EmployeeGratuityData.EmployeeAlternativeId}";
            if (!(objGratuityModelList.Count() > 0))
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        /*
         * This link temporarily delete from database    
         * Link Name "Employee Gratuity Report"     
         */
        public ActionResult EmployeeGratuityFinalSettlementReportViewer()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //Departments
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.DepartmentList = deplist;
            ViewBag.SortingList = new SelectList(DataAccess.GeneralDB.CommonDB.GetSortingList(), "Key", "Value").ToList();
            //Employees           
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            //emplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.EmployeeList = emplist;
            return View();

        }



        public ActionResult EmployeeGratuitySettlementReportviewer(int? departmentId, string EmployeeId, int order)
        {
            ViewBag.ActionUrl = "EmployeeGratuityFinalSettlementReportAction";
            return View("ReportViewer");
        }

        public ActionResult EmployeeGratuityFinalSettlementReportAction(int? departmentId, string EmployeeId, int order)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            StiReport report = new StiReport();
            List<GratuityReportModel> objGratuityModelList = new List<GratuityReportModel>();
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();

            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);

            GeneralModel gModel = new GeneralModel();
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            Employee emp = new Employee();
            emp = new EmployeeDB().GetEmployee(objUserContextViewModel.UserId);
            gModel.Value4 = emp.FirstName + emp.LastName + " on " + DateTime.Now.ToString("d MMM yyyy HH:mm");
            gModel.Value7 = DateTime.Now.ToString("d-MMM-yyyy HH:mm:tt");
            report.Dictionary = new Stimulsoft.Report.Dictionary.StiDictionary();

            objGratuityModelList = new ReportingDB().GetGratuityListReport(EmployeeId, departmentId);

            GeneralAccSetting AccSetting = new ReportingDB().GetGeneralAccountSetting();

            gModel.Value5 = AccSetting.CurrencyName;
            gModel.Value6 = AccSetting.FilsName;

            PaySalaryDB paySalaryDB = new PaySalaryDB();
            PayDirectDepositDB objPayDirectDepositDB = new PayDirectDepositDB();

            foreach (var item in objGratuityModelList)
            {
                decimal TotalSalary = paySalaryDB.GetEmployeeTotalSalary(Convert.ToInt32(item.EmpID));
                decimal BasicSalary = 0;
                PayDirectDepositModel objPayDirectDepositModel = new PayDirectDepositModel();
                if (TotalSalary == 0)
                {
                    IEnumerable<PaySalaryModel> paySalaryList = new List<PaySalaryModel>();
                    paySalaryList = paySalaryDB.GetEmployeeSalaryById(Convert.ToInt32(item.EmpID));
                    TotalSalary = (decimal)(paySalaryList.Where(x => x.IsActive == true)).Sum(x => x.Amount);
                    BasicSalary = (decimal)(paySalaryList.Where(x => x.PaySalaryAllowanceID == 1)).Sum(x => x.Amount);
                    objPayDirectDepositModel = objPayDirectDepositDB.GetPayDirectDepositFullInformationforEmployee(Convert.ToInt32(item.EmpID));
                }
                item.BasicSalary = BasicSalary.ToString(AmountFormat);
                item.TotalSalary = TotalSalary.ToString(AmountFormat); ;
                item.GratuityDays = "0";
                item.GratuityBeforeJuly1980 = AmountFormat;
                item.GratuityAlreadyPaid = AmountFormat;
                item.LWPDuringService = AmountFormat;
                item.AnnualLeaveBalance = AmountFormat;
                item.CompensatoryLeaveBalance = AmountFormat;
                item.ESAnnualLeaveEncashment = AmountFormat;
                item.ESOtherPayments = AmountFormat;
                item.ESOtherDeductions = AmountFormat;
                item.TotalDeductions = AmountFormat;
                item.TotalEarnings = TotalSalary + Convert.ToDecimal(item.Amount) + Convert.ToDecimal(item.ESAirfare);
                item.SettlementAmount = item.TotalEarnings - Convert.ToDecimal(item.TotalDeductions);
                item.BankName = objPayDirectDepositModel.payBankModel.BankName_1;
                item.BranchName = objPayDirectDepositModel.payBankBranchModel.BranchName_1;
                item.UniAccountNumber = objPayDirectDepositModel.UniAccountNumber;
                item.Currency = AccSetting.CurrencyCode;
                string noOfFils = string.Empty;
                decimal valueAfterPoint = (item.SettlementAmount - (long)item.SettlementAmount) * 100;
                if (valueAfterPoint != 0)
                {
                    noOfFils = " and " + CommonDB.NumberToWords(Convert.ToInt32(valueAfterPoint)) + " " + AccSetting.FilsName;
                }

                item.PaybleAmount = CommonDB.NumberToWords(Convert.ToInt32(item.SettlementAmount)) + " " + AccSetting.CurrencyName + " " + noOfFils;
            }

            report.Load(Server.MapPath("~/Content/Reports/GratuityReport/EmployeeGratuityFinalSettlement.mrt"));
            report.RegData("EmployeeGratuityData", objGratuityModelList);
            report.RegBusinessObject("GeneralModel", gModel);
            StiComponent dataBand = report.GetComponentByName("DataBand1");
            switch (order)
            {
                case 1:
                    {
                        (dataBand as StiDataBand).Sort = new string[] { "ASC", "EmpName" };
                        break;
                    }
                case 2:
                    {
                        (dataBand as StiDataBand).Sort = new string[] { "DESC", "EmpName" };
                        break;
                    }

            }
            if (!(objGratuityModelList.Count() > 0))
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult FingerPrintMachineData()
        {
            return View();
        }

        public ActionResult GetFingerPrintMachineDataReporting()
        {
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            GeneralModel generalModel = new GeneralModel();
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            generalModel.Value5 = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            List<FingerPrintMachineDataReportModel> lstFingerPrintMachineData = new List<FingerPrintMachineDataReportModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            lstFingerPrintMachineData = new ReportingDB().GetFingerPrintMachineData(UserId);
            if (lstFingerPrintMachineData.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/EmployeeDataForFingerPrintMachines.mrt"));
                report.RegBusinessObject("FingerPrintMachineDataModel", lstFingerPrintMachineData);
                report.RegBusinessObject("GeneralModel", generalModel);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult GetEmployeeHireDate(int employeeId)
        {
            helper = new DataHelper();
            string query = "Select * from HR_EmploymentInformation where EmployeeID = " + employeeId;
            DataTable dt = helper.ExcuteCommandText(query);
            DataRow dr = dt.Rows[0];
            string hireDate = dr["HireDate"].ToString() != "" ? Convert.ToDateTime(dr["HireDate"].ToString()).ToString(HRMSDateFormat) : "";
            return Json(new { hireData = hireDate, todayDate = DateTime.Now.ToString(HRMSDateFormat) }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PayDeductionReport()
        {
            DeductionDB objDeductionDB = new DeductionDB();
            List<SelectListItem> DeductionList = new SelectList(objDeductionDB.GetAllPayDeductionType(), "DeductionTypeID", "DeductionTypeName_1").ToList();
            DeductionList.Insert(0, new SelectListItem { Text = "All Deduction Type", Value = "0" });
            ViewBag.DeductionList = DeductionList;
            return View();
        }

        public ActionResult PayAdditionReport()
        {
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            List<SelectListItem> AllowanceList = new SelectList(objAdditionPaidCycleDB.GetAllPaySalaryAllowanceList(true, true), "PaySalaryAllowanceID", "PaySalaryAllowanceName_1").ToList();
            AllowanceList.Insert(0, new SelectListItem { Text = "All Addition Type", Value = "0" });
            ViewBag.AllowanceList = AllowanceList;
            return View();
        }

        public ActionResult AdditionReportviewer(string reportType, int addtionType, string startDate, string toDate)
        {
            ViewBag.ActionUrl = "AdditionReportAction";
            return View("ReportViewer");
        }

        public ActionResult AdditionReportAction(string reportType, int addtionType, string startDate, string toDate)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            StiReport report = new StiReport();
            IEnumerable<AdditionReportModel> objGeneralModelList = new List<AdditionReportModel>();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            if (reportType == "AdditionReport")
            {
                objGeneralModelList = new ReportingDB().GetAdditionReport(addtionType, startDate, toDate, UserId);
                report.Load(Server.MapPath("~/Content/Reports/AdditionReports/PayAdditionReport.mrt"));
            }
            report.RegBusinessObject("AdditionReportModel", objGeneralModelList);
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            if (!(objGeneralModelList.Count() > 0))
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        #endregion

        #region Letter Report
        public ActionResult LetterReport(int id = 0)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var otherPermissions = userRoleDB.GetOtherPermission();
            otherPermissions = otherPermissions.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();

            bool ShowOnlyDownloadLetterPanel = false;

            if (otherPermissions.Where(x => x.OtherPermissionId == 40).Count() > 0)
                ShowOnlyDownloadLetterPanel = true;

            ViewBag.ShowOnlyDownloadLetterPanel = ShowOnlyDownloadLetterPanel;

            LetterReportBuilder objLetterReportBuilder = new LetterReportBuilder();
            LetterTemplates letterTemplate = new LetterTemplates();
            List<ReportBuilderFieldsModel> Fields = new ReportBuilderDB().GetReportFieldsData("");
            objLetterReportBuilder.lstLetterTemplates = objReportingDB.GetLettersTemplate();
            if (id == 0)
            {
                ViewBag.LetterTemplate = new LetterTemplates();
            }
            else
            {
                ViewBag.LetterTemplate = letterTemplate = objReportingDB.GetLetterTemplateById(id);
            }
            if (letterTemplate.LetterReportFields != "")
            {
                int[] letterTemplateFields = letterTemplate.LetterReportFields.Split(',').Select(x => Convert.ToInt32(x)).ToArray();
                objLetterReportBuilder.lstReportFields = Fields.Where(x => letterTemplateFields.Contains(x.PayReportBuilderFieldsID)).ToList();
            }
            else
            {
                objLetterReportBuilder.lstReportFields = new List<ReportBuilderFieldsModel>();
            }
            objLetterReportBuilder.lstAllReportFields = Fields;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1).ToList(), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View(objLetterReportBuilder);
        }

        [HttpPost]
        public JsonResult GetIsActiveEmployee(bool IsActive)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<Employee> employeeList = new List<Employee>();
            if (IsActive)
            {
                employeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 0).ToList();
            }
            else
            {
                employeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1).ToList();
            }
            var EmployeeList = (from p in employeeList
                                select new
                                {
                                    EmployeeId = p.EmployeeId,
                                    FullName = p.FullName,
                                });
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Employee", Value = "0" });
            foreach (var m in EmployeeList)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });
            }
            return Json(ObjSelectedList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkForAvailableReport(int LetterId, string FileName)
        {
            OperationDetails op = new OperationDetails();
            bool isAvailable = objReportingDB.CheckTemplateAvailablity(LetterId, FileName);
            if (isAvailable)
            {
                op.Success = true;

            }
            else
            {
                op.Success = false;
            }
            return Json(op, JsonRequestBehavior.AllowGet);

        }

        [ValidateInput(false)]
        public ActionResult GetDocFile(int LetterId, string FileName, string LetterField, string Content, string oldFileName)
        {
            Content = Content.Replace("&quot;", "");
            MemoryStream stream = new MemoryStream();
            OperationDetails op = new OperationDetails();
            if (!Directory.Exists(Server.MapPath("~/Uploads/LetterTemplate")))
            {
                Directory.CreateDirectory(Server.MapPath("~/Uploads/LetterTemplate"));
            }
            string filename = Server.MapPath("~/Uploads/LetterTemplate/" + FileName + ".docx");
            if (oldFileName == FileName)
                CommonHelper.CommonHelper.DeleteSingleFile(filename);
            else
            {
                oldFileName = Server.MapPath("~/Uploads/LetterTemplate/" + oldFileName + ".docx");
                CommonHelper.CommonHelper.DeleteSingleFile(oldFileName);
            }
            string html = Content;
            try
            {
                using (WordprocessingDocument package = WordprocessingDocument.Create(stream, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }
                    HtmlConverter converter = new HtmlConverter(mainPart);
                    Body body = mainPart.Document.Body;
                    var paragraphs = converter.Parse(html);
                    foreach (var iPar in paragraphs)
                    {
                        body.Append(iPar);
                    }
                    mainPart.Document.Save();
                }
                System.IO.File.WriteAllBytes(filename, stream.ToArray());
                op = objReportingDB.UpdateLetterTemplate(LetterId, FileName, LetterField, LetterId == 0 ? 1 : 2, Content);
                if (op.Success)
                {
                    op.Message = "Letter template saved successfully";
                }
                else
                {
                    op.Message = "Error while saving letter template! ";
                }
            }
            catch (Exception ex)
            {
                op.Success = false;
                op.Message = "Error while saving letter template! ";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteLetterTemplate(int templateId, string FileName)
        {

            MemoryStream stream = new MemoryStream();
            OperationDetails op = new OperationDetails();
            string filename = Server.MapPath("~/Uploads/LetterTemplate/" + FileName + ".docx");

            try
            {
                bool isDeleted = false;
                isDeleted = objReportingDB.DeleteLetterTemplate(templateId);
                if (isDeleted)
                {
                    FileInfo fi = new FileInfo(filename);
                    fi.Delete();
                    op.Success = true;
                }
                else
                {
                    op.Success = false;
                }

                if (op.Success)
                {
                    op.Message = "Letter template deleted successfully";
                }
                else
                {
                    op.Message = "Error while deleting letter template! ";
                }

            }
            catch (Exception ex)
            {
                op.Success = false;
                op.Message = "Error while deleting letter template! ";
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadEmployeeLetter(string EmpId, string TemplateName)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            OperationDetails op = new OperationDetails();
            ReportBuilderDB objReportBuilderDB = new ReportBuilderDB();
            string whereSqlCondition = "";
            string groupBySqlID = "", orderBySqlID = "";
            try
            {
                if (!Directory.Exists(Server.MapPath("~/Downloads/LettersReport/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Downloads/LettersReport/"));
                }
                ClearFolder(Server.MapPath("~/Downloads/LettersReport"));
                string[] EmpIds = { };
                if (EmpId.Length > 0)
                {
                    EmpIds = EmpId.Split(',');
                }
                string EmployeeId = "";
                string TemplateFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/LetterTemplate/" + TemplateName + ".docx");
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(TemplateFilePath, true))
                {
                    MainDocumentPart mainPart = wordDoc.MainDocumentPart;

                    string docText = null;
                    using (StreamReader sr = new StreamReader(mainPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    string[] strSplits = docText.Split('{');
                    string finalToken = string.Empty;
                    foreach (var item in strSplits)
                    {
                        int index = item.IndexOf('}');
                        if (index > 0)
                        {
                            string substring = item.Substring(0, index);
                            finalToken = finalToken + substring.Split(':')[0] + ",";
                        }

                    }
                    if (finalToken.LastIndexOf(',') > 0)
                    {
                        finalToken = finalToken.Remove(finalToken.LastIndexOf(','));
                    }
                    DataTable dt = objReportBuilderDB.GetReportData(0, finalToken, 0, 0, 0, whereSqlCondition, 1, 1, "", UserId, out groupBySqlID, out orderBySqlID);
                }

                foreach (string EId in EmpIds)
                {
                    EmployeeId = EId;
                    string Destfilename = Server.MapPath("~/Downloads/LettersReport/" + TemplateName.Trim() + EmployeeId + ".docx");

                    try
                    {
                        System.IO.File.Copy(TemplateFilePath, Destfilename);
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(Destfilename, true))
                        {
                            MainDocumentPart mainPart = wordDoc.MainDocumentPart;

                            string docText = null;
                            using (StreamReader sr = new StreamReader(mainPart.GetStream()))
                            {
                                docText = sr.ReadToEnd();
                            }

                            List<string> Tokens = new List<string>();
                            List<string> TableElements = new List<string>();
                            string[] strSplits = docText.Split('{');
                            string finalToken = string.Empty;
                            foreach (var item in strSplits)
                            {
                                int index = item.IndexOf('}');
                                if (index > 0)
                                {
                                    string substring = item.Substring(0, index);
                                    TableElements.Add(substring);
                                    Tokens.Add("{" + substring + "}");
                                    finalToken = finalToken + substring.Split(':')[0] + ",";
                                }

                            }
                            if (finalToken.LastIndexOf(',') > 0)
                            {
                                finalToken = finalToken.Remove(finalToken.LastIndexOf(','));
                            }
                            DataTable dt = objReportBuilderDB.GetReportData(0, finalToken, 0, 0, 0, whereSqlCondition, 2, 1, EmployeeId, UserId, out groupBySqlID, out orderBySqlID);
                            Dictionary<string, string> dict = new Dictionary<string, string>();
                            int i = 0;
                            foreach (var item in Tokens)
                            {
                                try
                                {
                                    string ColumnName = TableElements[i].Split(':')[1];
                                    if (ColumnName.Contains('<'))
                                    {
                                        ColumnName = ColumnName.Remove(ColumnName.IndexOf('<'));
                                    }
                                    ColumnName = ColumnName.Trim();
                                    string textField = dt.Rows[0][i].ToString().Replace("&", "&amp;");
                                    dict.Add(item, textField);
                                    i++;
                                }
                                catch (Exception e)
                                {
                                    i++;
                                }

                            }
                            foreach (KeyValuePair<string, string> item in dict)
                            {
                                Regex regexText = new Regex(item.Key);
                                docText = regexText.Replace(docText, item.Value);
                            }

                            using (StreamWriter sw = new StreamWriter(
                                      mainPart.GetStream(FileMode.Create)))
                            {
                                sw.Write(docText);
                            }
                            try
                            {
                                int empid = Convert.ToInt32(EmployeeId);
                                OperationDetails operationDetails = objReportBuilderDB.DeletePayReportBuilderWhere(empid);
                            }
                            catch (Exception ex)
                            {

                            }


                            mainPart.GetStream(FileMode.Open);
                            op.Success = true;
                            op.Message = "Letter generated successfully";
                            op.CssClass = "success";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        // return null;
                        op.Success = false;
                        op.Message = "Error while generating Letter!";
                        op.CssClass = "error";
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                DataTable dt = objReportBuilderDB.GetReportData(0, "", 0, 0, 0, whereSqlCondition, 3, 1, "", UserId, out groupBySqlID, out orderBySqlID);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadEmpletter(string EmployeeId, string TemplateName)
        {
            string DestPath = Server.MapPath("~/Downloads/LettersReport");
            int fileCount = Directory.GetFiles(DestPath).Length;
            if (fileCount > 1)
            {
                string DestFileName = Server.MapPath("~/Downloads/LettersReportZip/" + TemplateName + ".zip");
                if (!Directory.Exists(Server.MapPath("~/Downloads/LettersReportZip")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Downloads/LettersReportZip/"));
                }
                ClearFolder(Server.MapPath("~/Downloads/LettersReportZip"));
                ZipFile.CreateFromDirectory(DestPath, DestFileName, System.IO.Compression.CompressionLevel.Fastest, false);
                ClearFolder(Server.MapPath("~/Downloads/LettersReport"));
                return File(DestFileName, "application/zip", TemplateName + ".zip");
            }
            else
            {
                string Destfilename = Server.MapPath("~/Downloads/LettersReport/" + TemplateName + EmployeeId + ".docx");
                if (System.IO.File.Exists(Destfilename))
                {
                    byte[] fileBytes = System.IO.File.ReadAllBytes(Destfilename);
                    string fileName = TemplateName + EmployeeId + ".docx";
                    try
                    {
                        FileInfo fi = new FileInfo(Destfilename);
                        fi.Delete();
                    }
                    catch (Exception Ex)
                    {

                    }
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return null;
                }
            }
        }

        private void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.IsReadOnly = false;
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                ClearFolder(di.FullName);
                di.Delete();
            }
        }

        #endregion

        public ActionResult AccommodationReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            AcademicYearDB academicYear = new AcademicYearDB();
            ViewBag.AcademicYear = new SelectList(academicYear.GetAllAcademicYear(), "Duration", "Duration");
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            emplist.Insert(0, (new SelectListItem { Text = "All Employees", Value = "" }));
            ViewBag.EmployeeList = emplist;
            return View();
        }

        public ActionResult AccommodationreportViewer(string academicYearId, int? employeeId)
        {
            ViewBag.ActionUrl = "AccommodationreportAction";
            return View("ReportViewer");
        }
        public ActionResult AccommodationreportAction(string academicYearId, int? employeeId)
        {
            ReportingDB reportingDb = new ReportingDB();
            StiReport report = new StiReport();
            GeneralModel gModel = new GeneralModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            IEnumerable<AccommodationModel> accommodationList = new List<AccommodationModel>();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            accommodationList = reportingDb.GetAccommodationList(employeeId, academicYearId, UserId);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            gModel.Value5 = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/Accommodation/AccommodationReport.mrt"));
            report.RegBusinessObject("GeneralModel", gModel);
            report.RegData("AccommodationModel", accommodationList);
            if (!(accommodationList.Count() > 0))
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult PayrollVarificationreportViewer()
        {
            return View();
        }

        public ActionResult GetPayrollVarificationReport()
        {
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            List<PayrollVerificationReport> lstMissingSalaryInfoEmployeeList = new List<PayrollVerificationReport>();
            List<PayrollVerificationReport> lstMissingBankInfoEmployeeList = new List<PayrollVerificationReport>();
            lstMissingBankInfoEmployeeList = new ReportingDB().GetMissingBankInfoEmployeeList();
            lstMissingSalaryInfoEmployeeList = new ReportingDB().GetMissingSalaryInfoEmployeeList();
            List<GeneralAccounts> generalAccountslist = new List<GeneralAccounts>();
            GeneralAccountsDB generalAccountsDB = new GeneralAccountsDB();
            generalAccountslist = generalAccountsDB.GetGeneralAccountList(null, null, null, true);
            if (lstMissingSalaryInfoEmployeeList.Count > 0 || lstMissingBankInfoEmployeeList.Count > 0 || generalAccountslist.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/PayrollReports/PayrollVarificationReport.mrt"));
                report.RegBusinessObject("MissingSalaryModel", lstMissingSalaryInfoEmployeeList);
                report.RegBusinessObject("MissingBankInfoModel", lstMissingBankInfoEmployeeList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("MissingPayCategoryAccounts", generalAccountslist);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult StatisticsPerNationalityReport()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //Departments
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();

            ViewBag.DepartmentList = deplist;

            NationalityDB objNationalityDB = new NationalityDB();
            List<SelectListItem> nationalityList = new SelectList(new NationalityDB().getAllNationalities(), "NationalityID", "NationalityName_1").ToList();

            // emplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.NationalityList = nationalityList;
            List<string> selectedValue = new List<string>();
            if (Session["EmployeeIds"] != null)
            {
                selectedValue = Session["EmployeeIds"].ToString().Split(',').Select(x => x).ToList();
                Session["EmployeeIds"] = null;
            }
            return View(selectedValue);
        }

        public ActionResult StatisticsPerNationalityReportviewer(string departmentIds, string NationalityIds)
        {
            ViewBag.ActionUrl = "GetStatisticsPerNationalityInfo";
            return View("ReportViewer");
        }


        public ActionResult GetStatisticsPerNationalityInfo(string departmentIds, string NationalityIds)
        {
            //StatisticsPerNationalityReport
            StiReport report = new StiReport();

            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            List<NationalitWiseData> employeeCountList = new List<NationalitWiseData>();

            employeeCountList = new ReportingDB().GetNationalitWiseGenderCountDetails(departmentIds, NationalityIds);
            GeneralAccountsDB generalAccountsDB = new GeneralAccountsDB();
            if (employeeCountList.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/MinistryReports/StatisticsPerNationalityReport.mrt"));
                report.RegData("NationalityWiseData", employeeCountList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult TotalStaffReport()
        {
            return View();
        }

        public ActionResult TotalStaffReportViewer(string mode)
        {
            if (mode == "Department")
                ViewBag.ActionUrl = "GetTotalStaffInfoByDepartment";
            else
                ViewBag.ActionUrl = "GetTotalStaffInfoByQualification";
            return View("ReportViewer");
        }

        public ActionResult GetTotalStaffInfoByDepartment()
        {
            //StatisticsPerNationalityReport
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            List<NationalitWiseData> employeeCountList = new List<NationalitWiseData>();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            employeeCountList = new ReportingDB().GetdepartmentWiseGenderCountDetails(UserId);

            List<GeneralAccounts> generalAccountslist = new List<GeneralAccounts>();
            GeneralAccountsDB generalAccountsDB = new GeneralAccountsDB();
            generalAccountslist = generalAccountsDB.GetGeneralAccountList(null, null, null, true);

            if (employeeCountList.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/MinistryReports/TotalStaffReport.mrt"));
                report.RegData("NationalityWiseData", employeeCountList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult GetTotalStaffInfoByQualification()
        {
            double pageWidth = 0;
            double columnWidth = 0;
            DataHelper dataHelper = new DataHelper();
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/ReportTemplates/ReportPotrait.mrt"));
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            report.ReportName = "Total Staff By Qualification";
            StiTable table;
            StiPage page = report.Pages[0];
            txtReportName.Text = "Total Staff By Qualification";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@UserId", UserId));

            DataTable tblDR = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_GetEmployeeCountByQualification");

            string tableName = "Table";
            string tempColumnName = "tempColumn";
            int tableCount = (tblDR.Columns.Count / 6) + ((tblDR.Columns.Count % 6) > 0 ? 1 : 0);
            int totalColumn = tblDR.Columns.Count;
            int columnNumber = 6;
            int columnCount = 0;
            int remainingColumn = tblDR.Columns.Count;
            int iterateColumn = 0;
            bool leftBorder = true;
            List<object> paramsObj = new List<object>();
            if (tblDR.Columns.Count >= columnNumber)
            {
                columnWidth = page.Width / columnNumber;
            }
            else
            {
                columnWidth = page.Width / tblDR.Columns.Count;
                iterateColumn = remainingColumn;
            }
            for (int i = 1; i <= tableCount; i++)
            {
                DataTable tempTable = new DataTable();
                paramsObj = new List<object>();
                leftBorder = true;
                tempTable.TableName = tableName + i;
                if (iterateColumn < remainingColumn)
                {
                    iterateColumn = columnNumber;
                    remainingColumn = remainingColumn - columnNumber;
                }
                else
                {
                    iterateColumn = remainingColumn;
                }
                for (int j = 1; j <= iterateColumn; j++)
                {
                    tempTable.Columns.Add(tblDR.Columns[columnCount].ToString(), typeof(string));
                    paramsObj.Add(tblDR.Rows[0][columnCount].ToString());
                    columnCount++;
                }
                if (iterateColumn != 6 && iterateColumn < 6 && tableCount > 1)
                {
                    for (int tempColumnAdd = 1; tempColumnAdd <= (6 - iterateColumn); tempColumnAdd++)
                    {
                        tempTable.Columns.Add(tempColumnName + tempColumnAdd, typeof(string));
                        paramsObj.Add("");
                    }
                }
                tempTable.Rows.Add(paramsObj.ToArray());
                report.RegData(tempTable);
                report.Dictionary.Synchronize();
                table = new StiTable();
                table.Name = "StimulesTable" + i;
                pageWidth = page.Width;
                table.ColumnCount = tempTable.Columns.Count;
                table.RowCount = 3;
                table.HeaderRowsCount = 1;
                table.FooterRowsCount = 1;
                table.Width = page.Width;
                table.GrowToHeight = false;
                table.CanShrink = false;
                table.CanGrow = true;
                table.HeaderCanGrow = true;
                table.HeaderCanShrink = false;
                table.DataSourceName = String.Format("{0}", tempTable.TableName);
                StiHeaderBand empty = new StiHeaderBand();
                empty.CanShrink = false;
                empty.CanGrow = true;
                empty.GrowToHeight = true;
                empty.MinHeight = 0.5;
                empty.Height = 0.5;
                page.Components.Add(table);
                page.Components.Add(empty);
                table.CreateCell();
                int indexHeaderCell = 0;
                int indexDataCell = tempTable.Columns.Count;

                foreach (DataColumn column in tempTable.Columns)
                {
                    //Set text on header 
                    StiTableCell headerCell = table.Components[indexHeaderCell] as StiTableCell;
                    headerCell.Text.Value = column.Caption;
                    headerCell.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                    headerCell.HorAlignment = StiTextHorAlignment.Center;
                    headerCell.VertAlignment = StiVertAlignment.Center;
                    headerCell.Border = new Stimulsoft.Base.Drawing.StiBorder((((Stimulsoft.Base.Drawing.StiBorderSides.None | Stimulsoft.Base.Drawing.StiBorderSides.Left)
                            | Stimulsoft.Base.Drawing.StiBorderSides.Right)
                            | Stimulsoft.Base.Drawing.StiBorderSides.Top), System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid);

                    headerCell.Border.Style = StiPenStyle.Solid;
                    headerCell.Border.Color = System.Drawing.Color.Black;
                    //headerCell.Margins = new StiMargins(3, 3, 3, 3);

                    headerCell.Height = 1;
                    headerCell.WordWrap = true;
                    headerCell.CanGrow = true;
                    headerCell.CanShrink = false;
                    headerCell.GrowToHeight = false;

                    StiTableCell dataCell = table.Components[indexDataCell] as StiTableCell;
                    string sDataCellExpresion = String.Format("{{{0}.{1}}}", tempTable.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols(column.ColumnName));
                    dataCell.Text.Value = sDataCellExpresion;
                    dataCell.HorAlignment = StiTextHorAlignment.Center;
                    dataCell.VertAlignment = StiVertAlignment.Center;
                    dataCell.Border.Side = StiBorderSides.All;
                    dataCell.Font = new System.Drawing.Font("Arial", 8);
                    //dataCell.Margins = new StiMargins(3, 3, 3, 3);
                    dataCell.WordWrap = true;
                    dataCell.CanGrow = true;
                    dataCell.CanShrink = false;
                    dataCell.GrowToHeight = false;
                    dataCell.Border.Style = StiPenStyle.Solid;
                    dataCell.Border.Color = System.Drawing.Color.Black;
                    dataCell.Height = 1;
                    if (iterateColumn != 6 && iterateColumn < 6)
                    {
                        if ((indexHeaderCell + 1) > (6 - (6 - iterateColumn)))
                        {
                            headerCell.Text.Value = "";
                            headerCell.Border.Side = StiBorderSides.None;
                            dataCell.Border.Side = StiBorderSides.None;
                            if (leftBorder)
                            {
                                headerCell.Border.Side = StiBorderSides.Left;
                                dataCell.Border.Side = StiBorderSides.Left;
                            }
                            leftBorder = false;
                        }
                    }
                    indexHeaderCell++;
                    indexDataCell++;
                }

            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult MinistryReportVersion1()
        {
            return View();
        }

        public ActionResult GetMinistryReportVersion1()
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            IEnumerable<MinistryModel> listMinistryModel;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            listMinistryModel = new ReportingDB().GetMinistryReportVersion1(UserId);
            if (listMinistryModel.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/MinistryReports/MinistryReportVersion1.mrt"));
                report.RegBusinessObject("MinistryModel", listMinistryModel);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult LeaveBalanceReport()
        {
            ViewBag.SalaryReport = false;
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //Departments
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId).Where(x => x.DepartmentId > 0).ToList(), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.DepartmentList = deplist;

            VacationDB vacationdb = new VacationDB();
            List<SelectListItem> VacationTypeList = new SelectList(vacationdb.GetVactionTypeList().Where(x => x.id > 0).ToList(), "id", "text").ToList();
            ViewBag.vactionddl = VacationTypeList;

            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.EmployeeList = emplist;


            List<string> selectedValue = new List<string>();
            if (Session["EmployeeIds"] != null)
            {
                selectedValue = Session["EmployeeIds"].ToString().Split(',').Select(x => x).ToList();
                Session["EmployeeIds"] = null;
            }
            return View(selectedValue);

        }

        public ActionResult LeaveBalanceSalaryReport()
        {
            ViewBag.SalaryReport = true;
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //Departments
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId).Where(x => x.DepartmentId > 0).ToList(), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.DepartmentList = deplist;

            VacationDB vacationdb = new VacationDB();
            List<SelectListItem> VacationTypeList = new SelectList(vacationdb.GetVactionTypeList().Where(x => x.id > 0).ToList(), "id", "text").ToList();
            ViewBag.vactionddl = VacationTypeList;

            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.EmployeeList = emplist;


            List<string> selectedValue = new List<string>();
            if (Session["EmployeeIds"] != null)
            {
                selectedValue = Session["EmployeeIds"].ToString().Split(',').Select(x => x).ToList();
                Session["EmployeeIds"] = null;
            }
            return View("LeaveBalanceReport", selectedValue);
        }

        public ActionResult LeaveBalanceReportExport(string EmployeeIds, string departmentid, string LeaveTypes, bool UsedLeaveEmployees, string ToDate, string salaryReport)
        {
            byte[] content;
            string fileName = salaryReport.Replace(" ", "") + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB departmentDB = new DepartmentDB();
            DateTime date = DateTime.ParseExact(ToDate, HRMSDateFormat, CultureInfo.InvariantCulture);
            DataTable dt = new ReportingDB().GetLeaveBalanceData(EmployeeIds, departmentid, LeaveTypes, UsedLeaveEmployees, date);
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            if (salaryReport != "Leave Balance Salary Report")
            {
                if (columnNames.Any(x => x == "Leave Salary"))
                    dt.Columns.Remove("Leave Salary");
            }
            foreach (string columnName in columnNames)
            {
                string newName = "";
                if (columnName.Contains("Rem_"))
                {
                    newName = columnName.Trim().Replace("Rem_", "Remaining ");
                    dt.Columns[columnName].ColumnName = newName;
                }
            }

            System.Data.DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public ActionResult LeaveBalanceReportViewer(string EmployeeIds, string departmentid, string LeaveTyes, bool UsedLeaveEmployees, string ToDate, string salaryReport)
        {
            ViewBag.ActionUrl = "GetLeaveBalanceReport";
            return View("ReportViewer");
        }

        public ActionResult GetLeaveBalanceReport(string EmployeeIds, string departmentid, string LeaveTypes, bool UsedLeaveEmployees, string ToDate, string salaryReport)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/ReportTemplates/ReportLandscape.mrt"));
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            txtReportName.Text = salaryReport;
            int columnDivider = 4;

            if (UsedLeaveEmployees)
            {
                EmployeeIds = "";
                departmentid = "";
            }
            //if (!string.IsNullOrEmpty(departmentid))
            //{
            //    EmployeeIds = "";
            //}
            DateTime dt = DateTime.ParseExact(ToDate, HRMSDateFormat, CultureInfo.InvariantCulture);
            DataTable tblDR = new ReportingDB().GetLeaveBalanceData(EmployeeIds, departmentid, LeaveTypes, UsedLeaveEmployees, dt);
            if (salaryReport == "Leave Balance Report")
            {
                tblDR.Columns.Remove("Leave Salary");
                columnDivider = 3;
            }

            tblDR.TableName = "tblDR";
            report.RegData(tblDR);
            report.RegData(tblDR);
            report.Dictionary.Synchronize();
            StiPage page = report.Pages[0];

            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Height = 0.5;
            headerBand.Name = "HeaderBand";
            page.Components.Add(headerBand);
            double pos = 0;
            //page.Width = 38;
            //double columnWidth = StiAlignValue.AlignToMinGrid(page.Width / tblDR.Columns.Count, 0.1, true);
            StiTable table = new StiTable();
            table.Name = "Table1";
            table.ColumnCount = tblDR.Columns.Count;
            table.RowCount = 3;
            table.HeaderRowsCount = 1;
            table.FooterRowsCount = 1;
            table.Width = page.Width;
            table.Height = page.GridSize * 12;
            table.DataSourceName = String.Format("{0}", tblDR.TableName);
            page.Components.Add(table);
            table.CreateCell();
            int indexHeaderCell = 0;
            int count = 0;
            double FirstHeaderWidth = 0.00;
            double SecondHeaderWidth = 0.00;
            foreach (DataColumn column in tblDR.Columns)
            {
                ////Set text on header
                StiTableCell headerCell = table.Components[indexHeaderCell] as StiTableCell;
                headerCell.Text.Value = column.Caption.Replace("Rem_", "");
                headerCell.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                headerCell.HorAlignment = StiTextHorAlignment.Center;
                headerCell.VertAlignment = StiVertAlignment.Center;
                headerCell.Border.Side = StiBorderSides.All;
                headerCell.WordWrap = true;
                headerCell.Margins = new StiMargins(3, 3, 3, 3);
                headerCell.CanGrow = true;
                headerCell.CanShrink = false;
                headerCell.GrowToHeight = true;
                headerCell.Height = 1f;
                if (!column.Caption.Contains("Rem_"))
                {
                    headerCell.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                }
                else if (column.Caption.Contains("Rem_"))
                {
                    headerCell.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(144, 238, 144));
                }

                if (indexHeaderCell < columnDivider)
                {
                    headerCell.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(205, 197, 191));
                    FirstHeaderWidth += headerCell.Width;
                }
                else if (!column.Caption.Contains("Rem_"))
                {
                    SecondHeaderWidth += headerCell.Width;
                }
                indexHeaderCell++;
            }

            int condCounter = 0;
            foreach (DataColumn column in tblDR.Columns)
            {
                StiTableCell dataCell = table.Components[indexHeaderCell] as StiTableCell;
                dataCell.HorAlignment = StiTextHorAlignment.Center;
                dataCell.VertAlignment = StiVertAlignment.Center;
                dataCell.Border.Side = StiBorderSides.All;
                string sDataCellExpresion = String.Format("{{{0}.{1}}}", tblDR.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols(column.ColumnName));


                if (condCounter > columnDivider)
                {
                    StiCondition sctextColorCond = new StiCondition();
                    sctextColorCond.DataType = StiFilterDataType.String;
                    sctextColorCond.TextColor = System.Drawing.Color.Red;
                    //sctextColorCond.Column = String.Format("{{{0}.{1}}}", tblDR.TableName,Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols(column.ColumnName));
                    sctextColorCond.Column = "tblDR." + StiNameValidator.CorrectName(column.ColumnName);
                    sctextColorCond.Condition = StiFilterCondition.Containing;
                    sctextColorCond.Value1 = "(";
                    sctextColorCond.Font = new System.Drawing.Font("Calibri", 10F,
                    System.Drawing.FontStyle.Bold);
                    sctextColorCond.BackColor = System.Drawing.Color.Beige;
                    sctextColorCond.Enabled = true;
                    dataCell.Conditions.Add(sctextColorCond);
                }
                dataCell.Text.Value = sDataCellExpresion;
                dataCell.Font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
                dataCell.WordWrap = true;
                dataCell.Margins = new StiMargins(3, 3, 3, 3);
                dataCell.FixedWidth = true;
                dataCell.CanGrow = true;
                dataCell.CanShrink = false;
                dataCell.GrowToHeight = true;
                dataCell.Height = 1f;
                indexHeaderCell++;
                condCounter++;
            }

            int nameIndex = 1;
            StiText headerText = new StiText(new RectangleD(pos, 0, FirstHeaderWidth, 1f));
            headerText.Text.Value = "";
            headerText.Name = "HeaderText" + nameIndex.ToString();
            headerText.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
            headerText.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(205, 197, 191));
            headerText.HorAlignment = StiTextHorAlignment.Center;
            headerText.VertAlignment = StiVertAlignment.Center;
            headerText.Border.Side = StiBorderSides.All;
            headerText.CanGrow = true;
            headerText.CanShrink = false;
            headerText.GrowToHeight = true;
            headerBand.Components.Add(headerText);
            pos += FirstHeaderWidth;
            nameIndex++;

            int leaveTypeColumnsCount = (tblDR.Columns.Count - 3) / 2;
            StiText headerText1 = new StiText(new RectangleD(pos, 0, SecondHeaderWidth, 1f));
            headerText1.Text.Value = "Used Leaves";
            headerText1.Name = "HeaderText" + nameIndex.ToString();
            headerText1.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
            headerText1.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
            headerText1.HorAlignment = StiTextHorAlignment.Center;
            headerText1.VertAlignment = StiVertAlignment.Center;
            headerText1.Border.Side = StiBorderSides.All;
            headerText1.CanGrow = true;
            headerText1.CanShrink = false;
            headerText1.GrowToHeight = true;
            headerBand.Components.Add(headerText1);
            pos += SecondHeaderWidth;
            nameIndex++;

            StiText headerText2 = new StiText(new RectangleD(pos, 0, page.Width - (FirstHeaderWidth + SecondHeaderWidth), 1f));
            headerText2.Text.Value = "Remaining Leaves";
            headerText2.Name = "HeaderText" + nameIndex.ToString();
            headerText2.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
            headerText2.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(144, 238, 144));
            headerText2.HorAlignment = StiTextHorAlignment.Center;
            headerText2.VertAlignment = StiVertAlignment.Center;
            headerText2.Border.Side = StiBorderSides.All;
            headerText2.CanGrow = true;
            headerText2.CanShrink = false;
            headerText2.GrowToHeight = true;
            headerBand.Components.Add(headerText2);
            report.ReportAlias = "LeaveBalanceReport";
            report.ReportName = "LeaveBalanceReport";
            return StiMvcViewer.GetReportSnapshotResult(report);
        }


        public ActionResult ProfessionalDevelopmentReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            SelectList emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName");
            ViewBag.EmployeeList = emplist;
            EmployeeTrainingDB objEmployeeTrainingDB = new EmployeeTrainingDB();
            CountryDB objCountryDB = new CountryDB();
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");
            ViewBag.Training = new SelectList(objEmployeeTrainingDB.GetAllCourse(), "CourseTitle", "CourseTitleName");
            return View();
        }

        public ActionResult EmployeeProfessionalDevelopmentReportViewer(int trainingId, int employeeId = 0, int countryId = 0, string fromDate = "", string toDate = "", bool TotalHour = false)
        {
            ViewBag.ActionUrl = "GetEmployeeProfessionalDevelopmentReportViewer";
            return View("ReportViewer");
        }
        public ActionResult GetEmployeeProfessionalDevelopmentByTraining(int trainingId, int employeeId = 0, int countryId = 0, string fromDate = "", string toDate = "", bool TotalHour = false, bool reportByTraining = false)
        {
            string[] formats = new string[] { "dd-MM-yyyy", "d/MM/yyyy" };
            string reportFileName = Server.MapPath("~/Content/Reports/BenefitReports/ProfessionalDevelopmentByTrainingCr.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            DataHelper dataHelper = new DataHelper();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeId", employeeId));
            parameterList.Add(new SqlParameter("@UserId", UserId));
            parameterList.Add(new SqlParameter("@TrainingId", trainingId));
            parameterList.Add(new SqlParameter("@CountryId", countryId));
            parameterList.Add(new SqlParameter("@FromDate", DateTime.ParseExact(fromDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)));
            parameterList.Add(new SqlParameter("@ToDate", DateTime.ParseExact(toDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_GetEmployeeTrainingDataForReport");
            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetEmployeeProfessionalDevelopmentReportViewer(int trainingId, int employeeId = 0, int countryId = 0, string fromDate = "", string toDate = "", bool TotalHour = false, bool reportByTraining = false)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            IEnumerable<EmployeeProfessionalDevelopmentReportModel> listEmployeeProfessionalModel;
            listEmployeeProfessionalModel = new ReportingDB().GetProfessionalDevelopmentReportDetails(trainingId, employeeId, countryId, fromDate, toDate, UserId);
            if (listEmployeeProfessionalModel.Count() > 0)
            {
                if (!TotalHour)
                    report.Load(Server.MapPath("~/Content/Reports/BenefitReports/ProfessionalDevelopmentReport.mrt"));
                else
                    report.Load(Server.MapPath("~/Content/Reports/BenefitReports/ProfessionalDevelopmentHourReport.mrt"));
                report.RegData("EmployeeProfessionalDevelopmentReportModel", listEmployeeProfessionalModel);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);

        }

        public ActionResult StaffQualificationReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            SelectList emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName");
            ViewBag.EmployeeList = emplist;
            JobCategoryDB jobCategoryDB = new JobCategoryDB();
            NationalityDB nationalityDB = new NationalityDB();
            ViewBag.Nationality = new SelectList(nationalityDB.getAllNationalities(), "NationalityID", "NationalityName_1");
            ViewBag.JobCategory = new SelectList(jobCategoryDB.GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
            return View();
        }

        public ActionResult StaffQualificationReportViewer(int jobCategory, int employeeId = 0, int nationalityId = 0)
        {
            ViewBag.ActionUrl = "GetStaffQualificationReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetStaffQualificationReportViewer(int jobCategory, int employeeId = 0, int nationalityId = 0)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            IEnumerable<StaffQualificationReportModel> staffQualificationList;
            staffQualificationList = new ReportingDB().GetQualificationReportDetails(employeeId, nationalityId, jobCategory, UserId);
            if (staffQualificationList.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/AcademicReports/StaffQualificationReport.mrt"));
                report.RegBusinessObject("AcademicReportModel", staffQualificationList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult EmployeeFinalSettlementReport()
        {
            List<SelectListItem> emplist = new SelectList(objEmployeeDB.GetEmployeeFromGratuity(), "EmployeeId", "FirstName").ToList();
            // emplist.Insert(0, (new SelectListItem { Text = "All", Value = "" }));
            ViewBag.EmployeeList = emplist;
            return View();
        }

        public ActionResult EmployeeFinalSettlementReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeeFinalSettlementReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetEmployeeFinalSettlementReportViewer(string employeeIds, bool isDirectcheque)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.ReportLogo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");

            GeneralModel gModel = new GeneralModel();
            Employee emp = new Employee();
            emp = new EmployeeDB().GetEmployee(objUserContextViewModel.UserId);
            gModel.Value1 = emp.FirstName + emp.LastName + " on " + DateTime.Now.ToString("d MMM yyyy HH:mm");
            EmployeeFinalSettlementReport objEmployeeFinalSettlement = reportingDB.GetFinalSettlementReport(employeeIds);
            GeneralAccSetting AccSetting = new ReportingDB().GetGeneralAccountSetting();
            gModel.Value2 = AccSetting.CurrencyCode;
            if (objEmployeeFinalSettlement.EmployeeDetailsList.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/GratuityReport/FinalSettlementReport.mrt"));
                if (isDirectcheque)
                {
                    StiComponent sc = report.GetComponentByName("DataBand10");
                    sc.Enabled = true;
                    StiComponent sc1 = report.GetComponentByName("DataBand9");
                    sc1.Enabled = false;
                }
                report.RegData("EmpDetails", objEmployeeFinalSettlement.EmployeeDetailsList);
                report.RegData("SalaryDetails", objEmployeeFinalSettlement.EmployeeSalaryList);
                report.RegData("GratuityDetails", objEmployeeFinalSettlement.EmployeeGratList);
                report.RegData("LeaveDetails", objEmployeeFinalSettlement.EmployeeLeaveList);
                report.RegData("OtherBenefits", objEmployeeFinalSettlement.EmployeeOtherBenefitsList);
                report.RegData("DeductionDetails", objEmployeeFinalSettlement.EmployeeOtherDeductionList);
                report.RegData("FinalSettlement", objEmployeeFinalSettlement.EmployeeFinalSettlementDue);
                report.RegData("BakDetails", objEmployeeFinalSettlement.EmpBankDetails);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", gModel);
                report.Pages[1].Enabled = false;
                report.CacheAllData = true;
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult SalaryIncrementReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            DepartmentDB departmentDB = new DepartmentDB();
            GeneralAccountsDB generalAccountsDB = new GeneralAccountsDB();
            ViewBag.payCategoryList = new SelectList(generalAccountsDB.GetPayCategories(), "id", "text");
            ViewBag.departmentlist = new SelectList(departmentDB.GetDepartmentListWithPermission(UserId), "DepartmentId", "DepartmentName_1");
            ViewBag.emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName");
            return View();
        }

        public ActionResult SalaryIncrementReportViewer(string employeeIds, int? departmentId, int? payCategory)
        {
            ViewBag.ActionUrl = "GetSalaryIncrementReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetSalaryIncrementReportViewer(string employeeIds, int? departmentId, int? payCategory)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            IEnumerable<SalaryReport> listSalaryReport;
            listSalaryReport = new ReportingDB().GetSalaryReportData(employeeIds, departmentId, payCategory);
            if (listSalaryReport.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/SalaryReport/SalaryIncrementReport.mrt"));
                report.RegBusinessObject("SalaryReport", listSalaryReport);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult EmployeeLeaveRegister()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName");
            return View();
        }

        public ActionResult EmployeeLeaveRegisterReportViewer(string employeesId, string fromDate, string toDate)
        {
            ViewBag.ActionUrl = "GetEmployeeLeaveRegisterReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetEmployeeLeaveRegisterReportViewer(string employeesId, string fromDate, string toDate)
        {
            List<EmployeeLeaveRegister> employeeLeaveRegisterList = objReportingDB.GetEmployeeLeaveRegister(fromDate, toDate);
            List<int> employeeIds;
            if (employeesId != "")
                employeeIds = employeesId.Split(',').Select(int.Parse).ToList();
            else
                employeeIds = new List<int>();
            employeeLeaveRegisterList = employeeLeaveRegisterList.Where(x => employeeIds.Contains(x.EmployeeId)).ToList();
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");

            if (employeeLeaveRegisterList.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LeaveReport/EmployeeLeaveRegister.mrt"));
                report.RegBusinessObject("EmployeeLeaveRegister", employeeLeaveRegisterList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult EmployeeStatisticsByNationalityReport()
        {
            NationalityDB nationalityDB = new NationalityDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Nationality = new SelectList(nationalityDB.getAllNationalities().Where(x => x.NationalityID > 0).OrderBy(x => x.NationalityName_1).ToList(), "NationalityID", "NationalityName_1");
            ViewBag.NationalityCount = nationalityDB.GetEmployeeNationalityCount(objUserContextViewModel.CompanyId);
            return View();
        }

        public ActionResult EmployeeStatisticsByNationalityReportViewer(string nationalityIds, int statisticCount)
        {
            ViewBag.ActionUrl = "GetEmployeeStatisticsByNationalityReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetEmployeeStatisticsByNationalityReportViewer(string nationalityIds, int statisticCount)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/ReportTemplates/ReportLandscape.mrt"));
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            if (statisticCount == 0)
                txtReportName.Text = "إحصاءات الموظفين حسب الجنسية";
            else
                txtReportName.Text = "إحصائيات الموظفين حسب الجنسية";
            DataTable tblDR = reportingDB.GetEmployeeStatisticsByNationalityData(nationalityIds, statisticCount, UserId);
            int omaniCount = 0;
            int nonOmaniCount = 0;
            if (statisticCount == 1)
            {
                omaniCount = Convert.ToInt32(tblDR.Rows[0]["Grand Total"].ToString());
                nonOmaniCount = Convert.ToInt32(tblDR.Rows[1]["Grand Total"].ToString());
                tblDR.Columns["Total"].ColumnName = "مجموع";
                tblDR.Columns.Remove("Grand Total");
            }
            tblDR.Columns["Nationality"].ColumnName = "جنسية";
            tblDR.TableName = "tblDR";
            report.RegData(tblDR);
            report.RegData(tblDR);
            report.Dictionary.Synchronize();
            StiPage page = report.Pages[0];

            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Height = 0.5;
            headerBand.Name = "HeaderBand";
            page.Components.Add(headerBand);
            double pos = 0;
            //page.Width = 38;
            //double columnWidth = StiAlignValue.AlignToMinGrid(page.Width / tblDR.Columns.Count, 0.1, true);
            StiTable table = new StiTable();
            table.Name = "Table1";
            table.ColumnCount = tblDR.Columns.Count;
            table.RowCount = 4;
            table.HeaderRowsCount = 1;
            table.FooterRowsCount = 2;
            table.Width = page.Width;
            table.Height = page.GridSize * 12;
            table.DataSourceName = String.Format("{0}", tblDR.TableName);
            page.Components.Add(table);
            table.CreateCell();
            int indexHeaderCell = 0;
            int count = 0;
            double FirstHeaderWidth = 0.00;
            double SecondHeaderWidth = 0.00;
            foreach (DataColumn column in tblDR.Columns)
            {
                ////Set text on header
                StiTableCell headerCell = table.Components[indexHeaderCell] as StiTableCell;
                headerCell.Text.Value = column.ColumnName;
                headerCell.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                headerCell.HorAlignment = StiTextHorAlignment.Center;
                headerCell.VertAlignment = StiVertAlignment.Center;
                headerCell.Border.Side = StiBorderSides.All;
                headerCell.WordWrap = true;
                headerCell.Margins = new StiMargins(3, 3, 3, 3);
                headerCell.CanGrow = true;
                headerCell.CanShrink = false;
                headerCell.GrowToHeight = true;
                headerCell.Height = 1.2f;
                indexHeaderCell++;
            }

            int condCounter = 0;
            foreach (DataColumn column in tblDR.Columns)
            {
                StiTableCell dataCell = table.Components[indexHeaderCell] as StiTableCell;
                dataCell.HorAlignment = StiTextHorAlignment.Center;
                dataCell.VertAlignment = StiVertAlignment.Center;
                dataCell.Border.Side = StiBorderSides.All;
                string sDataCellExpresion = String.Format("{{{0}.{1}}}", tblDR.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols(column.ColumnName));

                dataCell.Text.Value = sDataCellExpresion;
                dataCell.Font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
                dataCell.WordWrap = true;
                dataCell.Margins = new StiMargins(3, 3, 3, 3);
                dataCell.FixedWidth = true;
                dataCell.CanGrow = true;
                dataCell.CanShrink = false;
                dataCell.GrowToHeight = true;
                dataCell.Height = 1f;
                indexHeaderCell++;
                condCounter++;
            }
            if (statisticCount == 1)
            {
                foreach (DataColumn column in tblDR.Columns)
                {
                    StiTableCell dataCell = table.Components[indexHeaderCell] as StiTableCell;
                    dataCell.HorAlignment = StiTextHorAlignment.Center;
                    dataCell.VertAlignment = StiVertAlignment.Center;
                    if (count < tblDR.Columns.Count - 2 && count != 0)
                        dataCell.Border = new StiBorder(((StiBorderSides.None | StiBorderSides.Top)
                                    | StiBorderSides.Bottom), System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black), false);
                    else
                    {
                        dataCell.Border = new StiBorder(((StiBorderSides.None | StiBorderSides.Top)
                                    | StiBorderSides.Bottom | StiBorderSides.Right), System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black), false);
                        if (count == 0)
                        {
                            dataCell.Border = new StiBorder(((StiBorderSides.None | StiBorderSides.Top)
                                                            | StiBorderSides.Bottom | StiBorderSides.Left), System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black), false);
                        }
                    }
                    if (count == tblDR.Columns.Count - 2)
                    {
                        dataCell.Text.Value = omaniCount.ToString(); ;
                    }
                    else if (count == tblDR.Columns.Count - 1)
                    {
                        dataCell.Text.Value = "مجموع العمانيين";
                    }
                    else
                    {
                        dataCell.Text.Value = "";
                    }
                    // مجموع غير العوماني

                    dataCell.Font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
                    dataCell.WordWrap = true;
                    dataCell.Margins = new StiMargins(3, 3, 3, 3);
                    dataCell.FixedWidth = true;
                    dataCell.CanGrow = true;
                    dataCell.CanShrink = false;
                    dataCell.GrowToHeight = true;
                    dataCell.Height = 1f;
                    indexHeaderCell++;
                    condCounter++;
                    count++;
                }
                count = 0;
                foreach (DataColumn column in tblDR.Columns)
                {
                    StiTableCell dataCell = table.Components[indexHeaderCell] as StiTableCell;
                    dataCell.HorAlignment = StiTextHorAlignment.Center;
                    dataCell.VertAlignment = StiVertAlignment.Center;
                    if (count < tblDR.Columns.Count - 2 && count != 0)
                        dataCell.Border = new StiBorder(((StiBorderSides.None | StiBorderSides.Top)
                                    | StiBorderSides.Bottom), System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black), false);
                    else
                    {
                        dataCell.Border = new StiBorder(((StiBorderSides.None | StiBorderSides.Top)
                                    | StiBorderSides.Bottom | StiBorderSides.Right), System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black), false);
                        if (count == 0)
                        {
                            dataCell.Border = new StiBorder(((StiBorderSides.None | StiBorderSides.Top)
                                                            | StiBorderSides.Bottom | StiBorderSides.Left), System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black), false);
                        }
                    }
                    if (count == tblDR.Columns.Count - 2)
                    {
                        dataCell.Text.Value = nonOmaniCount.ToString(); ;
                    }
                    else if (count == tblDR.Columns.Count - 1)
                    {
                        dataCell.Text.Value = "مجموع غير العمانيين";
                    }
                    else
                    {
                        dataCell.Text.Value = "";
                    }

                    dataCell.Font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
                    dataCell.WordWrap = true;
                    dataCell.Margins = new StiMargins(3, 3, 3, 3);
                    dataCell.FixedWidth = true;
                    dataCell.CanGrow = true;
                    dataCell.CanShrink = false;
                    dataCell.GrowToHeight = true;
                    dataCell.Height = 1f;
                    indexHeaderCell++;
                    condCounter++;
                    count++;
                }
            }
            int nameIndex = 1;
            StiText headerText = new StiText(new RectangleD(pos, 0, FirstHeaderWidth, 1f));
            headerText.Text.Value = "";
            headerText.Name = "HeaderText" + nameIndex.ToString();
            headerText.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
            headerText.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(205, 197, 191));
            headerText.HorAlignment = StiTextHorAlignment.Center;
            headerText.VertAlignment = StiVertAlignment.Center;
            headerText.Border.Side = StiBorderSides.All;
            headerText.CanGrow = true;
            headerText.CanShrink = false;
            headerText.GrowToHeight = true;
            headerBand.Components.Add(headerText);
            pos += FirstHeaderWidth;
            nameIndex++;

            int leaveTypeColumnsCount = (tblDR.Columns.Count - 3) / 2;

            report.ReportAlias = "EmployeeStatisticsByNationalityReport";
            report.ReportName = "EmployeeStatisticsByNationalityReport";
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult EmployeeMinistryReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> DepartmentList = new SelectList(new DepartmentDB().GetDepartmentList(CompanyId), "DepartmentId", "DepartmentName_1").ToList();
            ViewBag.Dept = DepartmentList;
            List<SelectListItem> jobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1").ToList();
            ViewBag.JobList = jobCategoryList;
            return View();
        }

        public ActionResult EmployeeministryReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeeMinistryReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetEmployeeMinistryReportViewer(string DepartmentIds, string Jobcategories, bool isArabicReport)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<EmployeeMinistryDetail> lstEmployeeMinistryDetail = objReportingDB.GetEmployeeMinistryReport(DepartmentIds, Jobcategories, UserId);

            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            var CurrentAcademicYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault();

            if (lstEmployeeMinistryDetail.Count() > 0)
            {
                if (isArabicReport)
                {
                    report.Load(Server.MapPath("~/Content/Reports/MinistryReports/EmployeeRecordForMinistryReportAr.mrt"));
                }
                else
                {
                    report.Load(Server.MapPath("~/Content/Reports/MinistryReports/EmployeeRecordForMinistryReport.mrt"));
                }

                if (string.IsNullOrEmpty(DepartmentIds))
                {
                    StiComponent sc = report.GetComponentByName("GroupHeaderDepartment");
                    sc.Enabled = false;
                }
                if (string.IsNullOrEmpty(Jobcategories))
                {
                    StiComponent sc = report.GetComponentByName("GroupHeaderSection");
                    sc.Enabled = false;
                }
                report.RegData("EmployeeMinistryData", lstEmployeeMinistryDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);

                if (CurrentAcademicYear != null)
                    report.RegBusinessObject("GeneralModel", new GeneralModel() { Value1 = CurrentAcademicYear.Duration });

            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult EmployeeDistributionSectionReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            EmployeeSectionDB sectionDb = new EmployeeSectionDB();
            ViewBag.Section = new SelectList(sectionDb.GetEmployeeSection(UserId), "EmployeeSectionID", "EmployeeSectionName_1");
            return View();
        }

        public ActionResult EmployeeDistributionSectionViewer(string SectionIds)
        {
            ViewBag.ActionUrl = "GetEmployeeDistributionSectionViewer";
            return View("ReportViewer");
        }

        public ActionResult GetEmployeeDistributionSectionViewer(string SectionIds)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            List<EmployeeReport> EmployeeReportList = (from Sec in reportingDB.GetEmployeeSectionReport(SectionIds)
                                                       group Sec by Sec.SectionId into SecGroup
                                                       select new EmployeeReport
                                                       {
                                                           SectionName = SecGroup.FirstOrDefault().SectionName,
                                                           EmployeeCount = SecGroup.Count()
                                                       }).ToList();


            if (EmployeeReportList.Count() > 0)
            {

                report.Load(Server.MapPath("~/Content/Reports/EmployeeReports/EmployeeDistributionbySection.mrt"));

                report.RegData("EmployeeReports", EmployeeReportList);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult EmployeeTestimonalReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeeTestimonalReportViewer";
            ViewBag.ActionExportUrl = "ExportEmployeeTestimonalLetterReport";
            ViewBag.ActionPrintUrl = "PrintEmployeeTestimonalLetterReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeTestimonalReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeTestimonalReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeTestimonalLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeTestimonalReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text4,Text31,Text33,Text35,Text38";
            return StiMvcViewer.PrintReportResult(GetFormatedReportForTestimonal(report, Feilds));
        }

        public ActionResult ExportEmployeeTestimonalLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeTestimonalReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text4,Text31,Text33,Text35,Text38";
            return StiMvcViewer.ExportReportResult(GetFormatedReportForTestimonal(report, Feilds));
        }

        private StiReport GetEmployeeTestimonalReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeTestimoanl> lstEmployeeMinistryDetail = objReportingDB.GetEmployeeTestimonalReport(EmployeeIds, SignedByEn, SignedByAr);

            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            GeneralModel generalModel = new GeneralModel();
            string ReportSubTitleEN = "~/Content/images/Reports/LetterReports/Testimonial_title.png";
            string ReportSubTitleAR = "~/Content/images/Reports/LetterReports/Testimonial_title_ar.png";
            generalModel.Value1 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + ReportSubTitleEN.TrimStart('~');
            generalModel.Value2 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + ReportSubTitleAR.TrimStart('~');
            if (lstEmployeeMinistryDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeTestmonalReport.mrt"));
                if (!IsDisplayHeader)
                {
                    StiComponent PageHeaderBand = report.GetComponentByName("PageHeaderBand1");
                    StiComponent PageHeaderBand2 = report.GetComponentByName("PageHeaderBand2");
                    PageHeaderBand2.Height = 4;
                    PageHeaderBand.Enabled = false;

                }
                if (!IsDisplayFooter)
                {
                    StiComponent PageFooterBand2 = report.GetComponentByName("PageFooterBand2");
                    PageFooterBand2.Enabled = false;
                }
                report.RegData("EmployeeTestimpnalData", lstEmployeeMinistryDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", generalModel);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        public ActionResult StaffRenewalLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }

        public ActionResult StaffRenewalLetterReportViewer()
        {
            ViewBag.ActionUrl = "GetStaffRenewalLetterReportViewer";
            ViewBag.ActionExportUrl = "ExportStaffRenewalLetterReport";
            ViewBag.ActionPrintUrl = "PrintStaffRenewalLetterReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetStaffRenewalLetterReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter, string noteEn, string noteAr)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetStaffRenewalReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter, noteEn, noteAr));
        }

        public ActionResult PrintStaffRenewalLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter, string noteEn, string noteAr)
        {
            StiReport report = GetStaffRenewalReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter, noteEn, noteAr);
            string Feilds = "Text17,Text19,Text48,Text20,Text30,Text31,Text32,txtArAllowance,Text49";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, Feilds));
        }

        public ActionResult ExportStaffRenewalLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter, string noteEn, string noteAr)
        {
            StiReport report = GetStaffRenewalReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter, noteEn, noteAr);
            string Feilds = "Text17,Text19,Text48,Text20,Text30,Text31,Text32,txtArAllowance,Text49";
            return StiMvcViewer.ExportReportResult(GetFormatedReport(report, Feilds));
        }

        private StiReport GetStaffRenewalReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter, string noteEn, string noteAr)
        {
            List<StaffRenewalData> lstEmployeeMinistryDetail = objReportingDB.GetStaffRenewalReport(EmployeeIds, SignedByEn, SignedByAr, noteEn, noteAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeMinistryDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/StaffRenewalLetterReport.mrt"));
                if (!IsDisplayHeader)
                {
                    StiComponent PageHeaderBand = report.GetComponentByName("PageHeaderBand1");
                    StiComponent PageHeaderBand2 = report.GetComponentByName("PageHeaderBand2");
                    PageHeaderBand2.Height = 4;
                    PageHeaderBand.Enabled = false;
                }
                if (!IsDisplayFooter)
                {
                    StiComponent PageFooterBand2 = report.GetComponentByName("PageFooterBand2");
                    PageFooterBand2.Enabled = false;

                }
                report.RegData("StaffRenewalData", lstEmployeeMinistryDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        public ActionResult TransferOfSponsorShip()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }

        public ActionResult EmployeeNOCLetterReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeeNOCLetterReportViewer";
            ViewBag.ActionExportUrl = "ExportEmployeeNOCLetterReport";
            ViewBag.ActionPrintUrl = "PrintEmployeeNOCLetterReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeNOCLetterReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeNOCLetterReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeNOCLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeNOCLetterReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text17,Text19,Text20,Text29";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, Feilds));
        }

        public ActionResult ExportEmployeeNOCLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeNOCLetterReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text17,Text19,Text20,Text29";
            return StiMvcViewer.ExportReportResult(GetFormatedReport(report, Feilds));
        }

        private StiReport GetEmployeeNOCLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<StaffRenewalData> lstEmployeeMinistryDetail = objReportingDB.GetStaffRenewalReport(EmployeeIds, SignedByEn, SignedByAr, "", "", 1);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeMinistryDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeNocLetterReport.mrt"));
                if (!IsDisplayHeader)
                {
                    StiComponent PageHeaderBand = report.GetComponentByName("PageHeaderBand1");
                    StiComponent PageHeaderBand2 = report.GetComponentByName("PageHeaderBand2");
                    PageHeaderBand2.Height = 2.4;
                    PageHeaderBand.Enabled = false;
                }
                if (!IsDisplayFooter)
                {
                    StiComponent PageFooterBand2 = report.GetComponentByName("PageFooterBand2");
                    PageFooterBand2.Enabled = false;
                }
                report.RegData("StaffRenewalData", lstEmployeeMinistryDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        public ActionResult EmployeeExprienceLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult EmployeeExprienceLetterReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeeExprienceLetterReportViewer";
            ViewBag.ActionExportUrl = "ExportEmployeeExprienceLetterReport";
            ViewBag.ActionPrintUrl = "PrintEmployeeExprienceLetterReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeExprienceLetterReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeExprienceLetterReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeExprienceLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeExprienceLetterReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, Feilds));
        }

        public ActionResult ExportEmployeeExprienceLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeExprienceLetterReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(GetFormatedReport(report, Feilds));
        }

        private StiReport GetEmployeeExprienceLetterReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<StaffRenewalData> lstEmployeeExperienceDetail = objReportingDB.GetStaffRenewalReport(EmployeeIds, SignedByEn, SignedByAr, "", "");
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeExperienceDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeExperienceLetterReport.mrt"));
                if (!IsDisplayHeader)
                {
                    StiComponent PageHeaderBand = report.GetComponentByName("PageHeaderBand1");
                    StiComponent PageHeaderBand2 = report.GetComponentByName("PageHeaderBand2");
                    PageHeaderBand2.Height = 4;
                    PageHeaderBand.Enabled = false;
                }
                if (!IsDisplayFooter)
                {
                    StiComponent PageFooterBand2 = report.GetComponentByName("PageFooterBand2");
                    PageFooterBand2.Enabled = false;
                }
                report.RegData("StaffRenewalData", lstEmployeeExperienceDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        public ActionResult EmployeeAgreementLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }

        public ActionResult EmployeeAgreementLetterReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeeAgreementLetterReportViewer";
            return View();
        }

        public ActionResult GetEmployeeAgreementLetterReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, string AgrDate, string YearsofContract, string CStartDate, string CEndDate, string PaidLeaves, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployAgrementReport(EmployeeIds, SignedByEn, SignedByAr, AgrDate, YearsofContract, CStartDate, CEndDate, PaidLeaves, IsDisplayHeader, IsDisplayFooter));
        }

        public StiReport GetEmployAgrementReport(string EmployeeIds, string SignedByEn, string SignedByAr, string AgrDate, string YearsofContract, string CStartDate, string CEndDate, string PaidLeaves, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeAgreementData> lstEmpAgreementData = objReportingDB.GetEmpAgreementReport(EmployeeIds, SignedByEn, SignedByAr, AgrDate, YearsofContract, CStartDate, CEndDate, PaidLeaves);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmpAgreementData.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeAgreementReport.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                    StiComponent PageHeaderBand1 = report.GetComponentByName("PageHeaderBand1");
                    PageHeaderBand1.Height = 3.4;
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("EmployeeAgreementData", lstEmpAgreementData);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        public ActionResult PrintEmployAgreementReport(string EmployeeIds, string SignedByEn, string SignedByAr, string AgrDate, string YearsofContract, string CStartDate, string CEndDate, string PaidLeaves, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployAgrementReport(EmployeeIds, SignedByEn, SignedByAr, AgrDate, YearsofContract, CStartDate, CEndDate, PaidLeaves, IsDisplayHeader, IsDisplayFooter);
            string feilds = "Text6,Text8,Text10,Text14,Text18,Text24,Text28,Text72,Text76,Text80,Text84,Text52,Text54,Text58,Text62,Text66";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, feilds));
        }

        public ActionResult ExportEmployAgreementReport(string EmployeeIds, string SignedByEn, string SignedByAr, string AgrDate, string YearsofContract, string CStartDate, string CEndDate, string PaidLeaves, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployAgrementReport(EmployeeIds, SignedByEn, SignedByAr, AgrDate, YearsofContract, CStartDate, CEndDate, PaidLeaves, IsDisplayHeader, IsDisplayFooter);
            string feilds = "Text6,Text8,Text10,Text14,Text18,Text24,Text28,Text72,Text76,Text80,Text84,Text52,Text54,Text58,Text62,Text66";
            return StiMvcViewer.ExportReportResult(GetFormatedReport(report, feilds));
        }

        public ViewResult PayrollTransactionReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            return View();
        }
        public ViewResult PayrollTransactionReportViewer(string EmployeeId, string PayTypes, string FromDate, string ToDate)
        {
            ViewBag.ActionUrl = "GetPayrollTransactionReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetPayrollTransactionReportViewer(string EmployeeId, string PayTypes, string FromDate, string ToDate)
        {
            ReportingDB reportingDB = new ReportingDB();
            IEnumerable<PayrollReportDetails> lstPayrollReportDetails = reportingDB.GetPayrollReportDetails(EmployeeId, PayTypes, FromDate, ToDate);
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstPayrollReportDetails.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/PayrollReports/PayrollTransactionReport.mrt"));
                report.RegData("SalaryData", lstPayrollReportDetails);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }
        #region WPS - Monthly Salary Report
        public ViewResult WPSMonthlySalaryReport()
        {
            ViewBag.Company = GetCompanySelectList();
            ViewBag.PayCycleList = new SelectList(new PayCycleDB().GetAllPayCycle().Where(x => x.active == false), "PayCycleID", "PayCycleName_1");
            return View();
        }

        public ActionResult LoadWPSMonthlySalaryReport(string complanyIds, int? payCycleId, string payCycleTitle)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = Server.MapPath("~/Content/Reports/PayrollReports/WPSMonthlySalaryReport.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@CompanyIds", complanyIds));
            parameterList.Add(new SqlParameter("@PayCycleId", payCycleId));
            if (!string.IsNullOrEmpty(payCycleTitle))
            {
                if (payCycleTitle.ToLower().Contains("salaries"))
                    payCycleTitle = payCycleTitle.Replace("Salaries", "");
                else if (payCycleTitle.ToLower().Contains("salary"))
                    payCycleTitle = payCycleTitle.Replace("Salary", "");
            }
            parameterFieldList.Add(new CustomParameterField("PayCycleTitle", payCycleTitle));
            parameterFieldList.Add(new CustomParameterField("NoOfDecimalPlaces", noOfDecimals));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_WPSMonthlySalaryReportData");

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }

        public ActionResult WPSMonthlySalaryReportExport(string complanyIds, int? payCycleId, string payCycleTitle, string organizationtitle)
        {
            MemoryStream mem = new MemoryStream();
            CommonController commonController = new CommonController();
            string fileName = "WPS-Monthly Salary Report-" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DepartmentDB departmentDB = new DepartmentDB();
            DataTable dt = new ReportingDB().GetWPSMonthlySalaryReportData(complanyIds, payCycleId);
            DataTable NewTable = dt.DefaultView.ToTable(false, "OrganizationCode", "FormattedTotalAmount", "FileName", "EmployeeCount");
            NewTable.Columns.Add("ExtraColumn");
            string[] columnNames = NewTable.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            foreach (string columnName in columnNames)
            {
                string newName = "";
                if (columnName.Contains("OrganizationCode"))
                {
                    newName = columnName.Trim().Replace("OrganizationCode", "Employer ID");
                    NewTable.Columns[columnName].ColumnName = newName;
                }
                if (columnName.Contains("FormattedTotalAmount"))
                {
                    newName = columnName.Trim().Replace("FormattedTotalAmount", "Total Amount (AED)");
                    NewTable.Columns[columnName].ColumnName = newName;
                }
                if (columnName.Contains("FileName"))
                {
                    newName = columnName.Trim().Replace("FileName", "File Name");
                    NewTable.Columns[columnName].ColumnName = newName;
                }
                if (columnName.Contains("EmployeeCount"))
                {
                    newName = columnName.Trim().Replace("EmployeeCount", "Employee Count");
                    NewTable.Columns[columnName].ColumnName = newName;
                }
            }
            SetColumnsOrder(NewTable, new string[] { "File Name", "ExtraColumn", "Employer ID", "Employee Count", "Total Amount (AED)" });
            System.Data.DataSet ds = new DataSet();
            ds.Tables.Add(NewTable);
            string filePath = CommonHelper.CommonHelper.CreateAndWriteIntoExcelSheet(ds, fileName);
            if (!string.IsNullOrEmpty(payCycleTitle))
            {
                if (payCycleTitle.ToLower().Contains("salaries"))
                    payCycleTitle = payCycleTitle.Replace("Salaries", "");
                else if (payCycleTitle.ToLower().Contains("salary"))
                    payCycleTitle = payCycleTitle.Replace("Salary", "");
            }
            CustomizeWPSMonthlySalaryExcelReport(filePath, payCycleTitle, organizationtitle, dt);
            return File(filePath, "application/vnd.ms-excel", fileName);
        }

        private void SetColumnsOrder(DataTable table, params String[] columnNames)
        {
            int columnIndex = 0;
            foreach (var columnName in columnNames)
            {
                table.Columns[columnName].SetOrdinal(columnIndex);
                columnIndex++;
            }
        }

        private void CustomizeWPSMonthlySalaryExcelReport(string filePath, string payCycleTitle, string organizationtitle, DataTable dt)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            Worksheet worksheet = null;
            Package spreadsheetPackage = Package.Open(filePath, FileMode.Open, FileAccess.ReadWrite);
            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                worksheet = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                DocumentFormat.OpenXml.Spreadsheet.Columns columns = new DocumentFormat.OpenXml.Spreadsheet.Columns();
                columns.Append(new DocumentFormat.OpenXml.Spreadsheet.Column() { Min = 1, Max = 20, Width = 20, CustomWidth = true });
                worksheet.Append(columns);

                int totalEmployeeCount = 0;
                decimal totalAmount = 0;
                foreach (DataRow row in dt.Rows)
                {
                    totalEmployeeCount = totalEmployeeCount + Convert.ToInt32(row["EmployeeCount"]);
                    totalAmount = totalAmount + Convert.ToDecimal(row["TotalAmount"]);
                }
                totalAmount = Math.Round(totalAmount, noOfDecimals);

                DocumentFormat.OpenXml.Spreadsheet.Row totalDataRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                DocumentFormat.OpenXml.Spreadsheet.Cell totalCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                totalCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                totalCell.StyleIndex = 9;
                totalCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Totals");
                totalDataRow.AppendChild(totalCell);

                totalDataRow.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Cell() { StyleIndex = 9 });
                totalDataRow.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Cell() { StyleIndex = 9 });
                DocumentFormat.OpenXml.Spreadsheet.Cell totalEmployeeCountCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                totalEmployeeCountCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                totalEmployeeCountCell.StyleIndex = 9;
                totalEmployeeCountCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(totalEmployeeCount.ToString());
                totalDataRow.AppendChild(totalEmployeeCountCell);

                DocumentFormat.OpenXml.Spreadsheet.Cell totalAmountCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                totalAmountCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                totalAmountCell.StyleIndex = 9;
                totalAmountCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(totalAmount.ToString());
                totalDataRow.AppendChild(totalAmountCell);

                sheetData.AppendChild(totalDataRow);

                DocumentFormat.OpenXml.Spreadsheet.Row tableHeadingRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                DocumentFormat.OpenXml.Spreadsheet.Cell tableHeadingCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                tableHeadingCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                tableHeadingCell.StyleIndex = 2;
                tableHeadingCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Monthly Salary Payment transfer through DIB internet banking facility:");

                tableHeadingRow.AppendChild(tableHeadingCell);
                sheetData.PrependChild(tableHeadingRow);

                sheetData.PrependChild(new DocumentFormat.OpenXml.Spreadsheet.Row());

                DocumentFormat.OpenXml.Spreadsheet.Row reportTitleRow = new DocumentFormat.OpenXml.Spreadsheet.Row()
                { RowIndex = 7, CustomHeight = true, Height = 25 };
                DocumentFormat.OpenXml.Spreadsheet.Cell reportTitleCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                reportTitleCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                reportTitleCell.StyleIndex = 3;
                reportTitleCell.CellReference = "A7";
                reportTitleCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Monthly Salary Payment - " + payCycleTitle);
                reportTitleRow.AppendChild(reportTitleCell);
                sheetData.PrependChild(reportTitleRow);

                sheetData.PrependChild(new DocumentFormat.OpenXml.Spreadsheet.Row());
                sheetData.PrependChild(new DocumentFormat.OpenXml.Spreadsheet.Row());

                //// CurrentDate
                DocumentFormat.OpenXml.Spreadsheet.Row TodaysDate = new DocumentFormat.OpenXml.Spreadsheet.Row();
                DocumentFormat.OpenXml.Spreadsheet.Cell TodaysDateCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                TodaysDateCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                TodaysDateCell.StyleIndex = 0;
                TodaysDateCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Date:");
                TodaysDate.AppendChild(TodaysDateCell);
                DocumentFormat.OpenXml.Spreadsheet.Cell TodaysDateCell1 = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                TodaysDateCell1.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                TodaysDateCell1.StyleIndex = 0;
                TodaysDateCell1.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(DateTime.Now.ToString(HRMSDateFormat));
                TodaysDate.AppendChild(TodaysDateCell1);
                sheetData.PrependChild(TodaysDate);

                // Ref and date
                DocumentFormat.OpenXml.Spreadsheet.Row Ref = new DocumentFormat.OpenXml.Spreadsheet.Row();
                DocumentFormat.OpenXml.Spreadsheet.Cell RefCell1 = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                RefCell1.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                RefCell1.StyleIndex = 0;
                RefCell1.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Ref:");
                Ref.AppendChild(RefCell1);
                DocumentFormat.OpenXml.Spreadsheet.Cell RefCell2 = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                RefCell2.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                RefCell2.StyleIndex = 0;
                RefCell2.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("SAL/" + payCycleTitle);
                Ref.AppendChild(RefCell2);
                sheetData.PrependChild(Ref);

                sheetData.PrependChild(new DocumentFormat.OpenXml.Spreadsheet.Row());

                // create a MergeCells class to hold each MergeCell
                MergeCells mergeCells = new MergeCells();

                if (!string.IsNullOrEmpty(organizationtitle))
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row organizationTitleRow = new DocumentFormat.OpenXml.Spreadsheet.Row()
                    {
                        RowIndex = 1,
                        CustomHeight = true,
                        Height = 25
                    };
                    DocumentFormat.OpenXml.Spreadsheet.Cell organizationTitleCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                    organizationTitleCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    organizationTitleCell.StyleIndex = 3;
                    organizationTitleCell.CellReference = "A1";
                    organizationTitleCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(organizationtitle);
                    organizationTitleRow.AppendChild(organizationTitleCell);
                    sheetData.PrependChild(organizationTitleRow);
                    mergeCells.Append(new MergeCell() { Reference = new StringValue("A1:E1") });
                }
                else
                {
                    sheetData.PrependChild(new DocumentFormat.OpenXml.Spreadsheet.Row());
                }

                //append a MergeCell to the mergeCells for each set of merged cells              
                mergeCells.Append(new MergeCell() { Reference = new StringValue("A7:E7") });
                for (int i = 0; i < 6; i++)
                {
                    sheetData.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Row());
                }

                DocumentFormat.OpenXml.Spreadsheet.Row footerCellRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                List<string> footercellCols = new List<string>();
                footercellCols.Add("Prepared By");
                footercellCols.Add("Reviewed By");
                footercellCols.Add("Approved by");
                footercellCols.Add("Approved by");
                footercellCols.Add("Approved by");
                foreach (string s in footercellCols)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(s);
                    cell.StyleIndex = 8;
                    footerCellRow.AppendChild(cell);
                }
                sheetData.AppendChild(footerCellRow);

                int startindex = 11;
                for (int i = 0; i <= dt.Rows.Count; i++)
                {
                    mergeCells.Append(new MergeCell() { Reference = new StringValue("A" + startindex + ":B" + startindex) });
                    startindex++;
                }
                mergeCells.Append(new MergeCell() { Reference = new StringValue("A" + startindex + ":B" + startindex) });

                worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetData>().First());
                worksheet.Save();

                workbookPart.Workbook.Save();
                document.Close();
            }
        }
        #endregion
        #region Salary Payment Summery
        public ActionResult SalaryPaymentSummery()
        {
            ViewBag.Company = GetCompanySelectList();
            ViewBag.PayCycleList = new SelectList(new PayCycleDB().GetAllPayCycle().Where(x => x.active == false), "PayCycleID", "PayCycleName_1");
            ViewBag.CategoriesList = new SelectList(new BankInformationDB().GetPayCategoriesList(), "CategoryID", "CategoryName_1").ToList();
            return View();
        }

        public ActionResult LoadSalaryPaymentSummeryReport(string complanyIds, int? payCycleId, string payCycleTitle, string categoryIds)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = Server.MapPath("~/Content/Reports/PayrollReports/SalaryPaymentSummaryReport.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@CompanyIds", complanyIds));
            parameterList.Add(new SqlParameter("@PayCycleId", payCycleId));
            parameterList.Add(new SqlParameter("@CategoryIds", categoryIds));
            if (!string.IsNullOrEmpty(payCycleTitle))
            {
                if (payCycleTitle.ToLower().Contains("salaries"))
                    payCycleTitle = payCycleTitle.Replace("Salaries", "");
                else if (payCycleTitle.ToLower().Contains("salary"))
                    payCycleTitle = payCycleTitle.Replace("Salary", "");
            }
            parameterFieldList.Add(new CustomParameterField("PayCycleTitle", payCycleTitle));
            parameterFieldList.Add(new CustomParameterField("NoOfDecimalPlaces", noOfDecimals));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_SalaryPaymentSummeryReportData");

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }
        #endregion
        private StiReport GetFormatedReport(StiReport stiReport, string Feilds)
        {
            string[] arrayOfFilds = Feilds.Split(',').ToArray();
            foreach (var item in arrayOfFilds)
            {
                StiText stiText = stiReport.GetComponentByName(item) as StiText;
                if (stiText != null)
                    stiText.HorAlignment = StiTextHorAlignment.Width;
            }
            return stiReport;
        }

        private StiReport GetFormatedReportForTestimonal(StiReport stiReport, string Feilds)
        {
            string[] arrayOfFilds = Feilds.Split(',').ToArray();
            foreach (var item in arrayOfFilds)
            {
                StiText stiText = stiReport.GetComponentByName(item) as StiText;
                if (stiText != null)
                    stiText.HorAlignment = StiTextHorAlignment.Center;
            }
            return stiReport;
        }

        public ActionResult SalaryTransferCertificate()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            return View();
        }
        public ActionResult SalaryTransferCertificateReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader)
        {
            ViewBag.ActionUrl = "GetSalaryTransferCertificateReportViewer";
            ViewBag.ActionExportUrl = "ExportSalaryTransferCertificateReport";
            ViewBag.ActionPrintUrl = "PrintSalaryTransferCertificateReport";
            return View("LetterReportViewer");
        }

        private StiReport GetSalaryTransferCertificateReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeSalaryTransferCertificate> lstEmployeeSalaryTransferCertificate = objReportingDB.GetSalaryTransferCertificateReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeSalaryTransferCertificate.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/SalaryLetters/EmployeeSalaryCertificate.mrt"));
                if (!IsDisplayHeader)
                {
                    StiComponent PageHeaderBand = report.GetComponentByName("PageHeaderBand1");
                    StiComponent PageHeaderBand2 = report.GetComponentByName("PageHeaderBand2");
                    PageHeaderBand2.Height = 4;
                    PageHeaderBand.Enabled = false;
                }
                if (!IsDisplayFooter)
                {
                    StiComponent PageFooterBand2 = report.GetComponentByName("PageFooterBand2");
                    PageFooterBand2.Enabled = false;
                }

                report.RegData("EmployeeSalaryTransferCertificate", lstEmployeeSalaryTransferCertificate);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        public ActionResult GetSalaryTransferCertificateReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetSalaryTransferCertificateReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintSalaryTransferCertificateReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetSalaryTransferCertificateReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text41,Text17,Text48,Text19,Text20,Text9,Text13,Text10,Text9,Text16,Text30,Text31,Text32";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, Feilds));
        }

        public ActionResult ExportSalaryTransferCertificateReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetSalaryTransferCertificateReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text41,Text17,Text48,Text19,Text20,Text9,Text13,Text10,Text9,Text16,Text30,Text31,Text32";
            return StiMvcViewer.ExportReportResult(GetFormatedReport(report, Feilds));
        }


        public ActionResult EmployeeNoOfExcusesReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            return View();
        }

        public ActionResult EmployeeNoOfExcusesReportViewer(string FromDate, string ToDate, string EmployeeIds, string Sortby, string SelectCase, bool DetailsReport)
        {
            ViewBag.ActionUrl = "GetEmployeeNoOfExcusesReportViewer";
            return View("ReportViewer");
        }

        public ActionResult GetEmployeeNoOfExcusesReportViewer(string FromDate, string ToDate, string EmployeeIds, string Sortby, string SelectCase, bool DetailsReport)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<EmployeeExcuseDetail> listEmployeeExcuseDetail = null;
            if (Sortby.Equals("EmployeeName"))
                listEmployeeExcuseDetail = reportingDB.GetEmployeeExcuseDetailReport(FromDate, ToDate, EmployeeIds, SelectCase, DetailsReport, UserId).OrderBy(x => x.EmployeeName).ToList();
            else
                listEmployeeExcuseDetail = reportingDB.GetEmployeeExcuseDetailReport(FromDate, ToDate, EmployeeIds, SelectCase, DetailsReport, UserId).OrderBy(x => x.EmpId).ToList();

            if (listEmployeeExcuseDetail.Count() > 0)
            {
                if (DetailsReport)
                {
                    report.Load(Server.MapPath("~/Content/Reports/ExcuseReport/EmployeeNoOfExcuseDetailsReport.mrt"));
                }
                else
                {
                    report.Load(Server.MapPath("~/Content/Reports/ExcuseReport/EmployeeNoOfExcuseReport.mrt"));
                }

                report.RegData("EmployeeExcuseDetail", listEmployeeExcuseDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult SalaryReportBasedOnCycle()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            var PayCycleList = new PayCycleDB().GetAllPayCycle().Where(x => x.active == false).ToList();
            if (PayCycleList.Count > 0)
            {
                var selectedPayCycle = PayCycleList.FirstOrDefault();
                ViewBag.Paycyle = new SelectList(PayCycleList, "PayCycleID", "PayCycleName_1", selectedPayCycle.PayCycleID);
            }
            else
            {
                ViewBag.Paycyle = new SelectList(new List<PayCycleModel>(), "PayCycleID", "PayCycleName_1");
            }
            return View();
        }

        public PartialViewResult SalaryReportAsPerCycle(int? CycleId)
        {
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListAsPerCycleId(CycleId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            return PartialView("_LoadEmployeeAsPerCycle");
        }

        public ActionResult SalaryReportBasedOnCycleViewer(string EmployeeIds, int CycleId, bool IsReportArabic)
        {
            ViewBag.ActionUrl = "GetSalaryReportBasedOnCycleViewer";
            return View("ReportViewer");
        }
        public ActionResult GetSalaryReportBasedOnCycleViewer(string EmployeeIds, int CycleId, bool IsReportArabic)
        {
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            CommonDB commonDB = new CommonDB();
            PayCycleDB payCycleDB = new PayCycleDB();
            string ReportName = string.Empty;
            PayCycleModel cycledata = payCycleDB.GetPayCycleById(CycleId);
            DateTime cycleDate = DateTime.ParseExact(cycledata.DateFrom, HRMSDateFormat, CultureInfo.InvariantCulture);
            if (IsReportArabic)
            {
                ReportName = "سجل أجور شهر " + commonDB.GetArabicMonth()[cycleDate.Month].ToString() + " " + cycleDate.ToString("yyyy");
            }
            else
                ReportName = "Employees Salary Record of " + cycleDate.ToString("MMMM yyyy");
            return StiMvcViewer.GetReportSnapshotResult(this.HttpContext, GetReport(reportingDB.GetEmployeeSalaryDataForCycle(EmployeeIds, CycleId, IsReportArabic), ReportName));
        }

        public ActionResult EmployeeCountByNationalityGraph()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All Departments", Value = "" }));
            ViewBag.DepartmentList = deplist;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All MOL Titles", Value = "" });
            MOLTitleDB objMolTitleDB = new MOLTitleDB();
            foreach (var m in objMolTitleDB.GetAllMOLTitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MOLTitleName_1, Value = m.MOLTitleID.ToString() });

            }
            ViewBag.MOLTitleList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All MOE Titles", Value = "" });
            MOETitleDB objMOETitleDB = new MOETitleDB();
            foreach (var m in objMOETitleDB.GetAllMOETitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MOETitleName_1, Value = m.MOETitleID.ToString() });

            }
            ViewBag.MOETitleList = ObjSelectedList;

            return View();
        }

        [NonAction]
        private StiReport GetReport(DataTable dt, string ReportName)
        {
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');
            dt.TableName = "ReportBuilder";
            report.Load(Server.MapPath("~/Content/Reports/ReportTemplates/ReportLandscape.mrt"));
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            report.ReportName = ReportName;
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            txtReportName.Text = ReportName;
            report.RegData(dt);
            report.Dictionary.Synchronize();
            StiPage page = report.Pages[0];
            //page.Width = 26;

            //Create HeaderBand
            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Height = 1.2;

            headerBand.Name = "HeaderBand";
            headerBand.MinHeight = 1.2;
            headerBand.MaxHeight = 2;
            headerBand.CanShrink = true;
            headerBand.CanGrow = true;
            page.Components.Add(headerBand);

            StiGroupHeaderBand groupHeaderBand = new StiGroupHeaderBand();
            //Create Databand
            StiDataBand dataBand = new StiDataBand();
            dataBand.DataSourceName = "ReportBuilder";
            dataBand.Height = 1;
            dataBand.MaxHeight = 2;
            dataBand.MinHeight = 1;
            dataBand.Name = "DataBand";
            headerBand.CanShrink = true;
            headerBand.CanGrow = true;
            page.Components.Add(dataBand);

            double pos = 0;
            //double columnWidth = StiAlignValue.AlignToMinGrid(page.Width / dt.Columns.Count, 0.1, true);
            double columnWidth = Math.Round(page.Width / dt.Columns.Count, 2);
            page.Width = columnWidth * dt.Columns.Count;
            int nameIndex = 1;

            //Create texts
            foreach (DataColumn column in dt.Columns)
            {
                //Create text on header
                StiText headerText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                headerText.Text.Value = column.ColumnName;
                headerText.Name = "HeaderText" + nameIndex.ToString();
                CommonParameters(true, ref headerText);
                headerBand.Components.Add(headerText);
                //Create text on Data Band
                StiText dataText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                dataText.Text.Value = "{ReportBuilder." + column.ColumnName.Trim().Replace(" ", "_").Replace(".", "_") + "}";
                dataText.Name = "DataText" + nameIndex.ToString();
                CommonParameters(false, ref dataText);
                dataBand.Components.Add(dataText);
                pos += columnWidth;
                nameIndex++;
            }

            StringBuilder sbText = new StringBuilder();
            sbText.Append(schoolInfo.SchoolName_1);
            return report;
        }

        [NonAction]
        private void CommonParameters(bool isHeader, ref StiText text)
        {
            text.HorAlignment = StiTextHorAlignment.Center;
            text.VertAlignment = StiVertAlignment.Center;
            text.Border.Side = StiBorderSides.All;
            string fontName = "Arial";
            float fontEmSize = 10f;
            text.GrowToHeight = true;
            text.CanShrink = true;
            text.CanGrow = true;
            text.Margins = new StiMargins(3, 3, 3, 3);
            text.WordWrap = true;
            if (isHeader)
            {
                text.Font = new System.Drawing.Font(fontName, 10, FontStyle.Bold);
                text.Height = 1.2;
            }
            else
            {
                text.Font = new System.Drawing.Font(fontName, 8, FontStyle.Regular);
                text.Height = 1;
            }
        }

        //********************************************************** EmployeeClarificationLetterReport for English *****************************************************************
        public ActionResult EmployeeClarificationLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult EmployeeClarificationLetterReportViewerEN()
        {
            ViewBag.ActionUrl = "GetEmployeeClarificationLetterReportViewerEN";
            ViewBag.ActionExportUrl = "ExportEmployeeClarificationLetterReportEN";
            ViewBag.ActionPrintUrl = "PrintEmployeeClarificationLetterReportEN";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeClarificationLetterReportViewerEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeClarificationLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeClarificationLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeClarificationLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeClarificationLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeClarificationLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeClarificationLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeClarificationData> lstEmployeeClarificationDetail = objReportingDB.GetEmployeeClarificationReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeClarificationDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeClarificationLetterReportEN.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("EmployeeDetails", lstEmployeeClarificationDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }


        //***************************************************** EmployeeClarificationLetterReport for Arabic ***********************************************************************
        public ActionResult EmployeeClarificationLetterReportViewerAR()
        {
            ViewBag.ActionUrl = "GetEmployeeClarificationLetterReportViewerAR";
            ViewBag.ActionExportUrl = "ExportEmployeeClarificationLetterReportAR";
            ViewBag.ActionPrintUrl = "PrintEmployeeClarificationLetterReportAR";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeClarificationLetterReportViewerAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeClarificationLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeClarificationLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeClarificationLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text3,Text6,Text1,Text8,Text4,Text7,Text5,Text9,Text11,Text12,Text14,Text13,Text16";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeClarificationLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeClarificationLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text3,Text6,Text1,Text8,Text4,Text7,Text5,Text9,Text11,Text12,Text14,Text13,Text16";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeClarificationLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeClarificationData> lstEmployeeClarificationDetail = objReportingDB.GetEmployeeClarificationReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeClarificationDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeClarificationLetterReportAR.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("EmployeeDetails", lstEmployeeClarificationDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Remove header and footer *********************************************************************************************
        public void RemoveHeader(StiReport report)
        {
            StiComponent PageHeaderBand1 = report.GetComponentByName("PageHeaderBand1");
            StiComponent PanelSchoolInfoEn = report.GetComponentByName("PanelSchoolInfoEn");
            StiComponent ImgSchoolLogo = report.GetComponentByName("ImgSchoolLogo");
            StiComponent PanelSchoolInfoAr = report.GetComponentByName("PanelSchoolInfoAr");
            StiComponent txtReportTitle = report.GetComponentByName("txtReportTitle");
            PageHeaderBand1.Height = 4.7;
            PanelSchoolInfoEn.Enabled = false;
            ImgSchoolLogo.Enabled = false;
            PanelSchoolInfoAr.Enabled = false;
            txtReportTitle.Enabled = false;
        }

        public void RemoveFooter(StiReport report)
        {
            StiComponent PageFooterBand2 = report.GetComponentByName("PageFooterBand2");
            StiComponent PageNumberFooter = report.GetComponentByName("PageNumberFooter");
            StiComponent DateTimeFooter = report.GetComponentByName("DateTimeFooter");
            PageFooterBand2.Height = 1.1;
            PageNumberFooter.Enabled = false;
            DateTimeFooter.Enabled = false;
        }

        //*************************************************** Employee Service Certificate English *********************************************************************************
        public ActionResult EmployeeServiceCertificateLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult EmployeeServiceCertificateLetterReportViewerEN()
        {
            ViewBag.ActionUrl = "GetEmployeeServiceCertificateLetterReportViewerEN";
            ViewBag.ActionExportUrl = "ExportEmployeeServiceCertificateLetterReportEN";
            ViewBag.ActionPrintUrl = "PrintEmployeeServiceCertificateLetterReportEN";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeServiceCertificateLetterReportViewerEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeServiceCertificateLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeServiceCertificateLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeServiceCertificateLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeServiceCertificateLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeServiceCertificateLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeServiceCertificateLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeServiceCertificateData> lstEmployeeClarificationDetail = objReportingDB.GetEmployeeServiceCertificateReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeClarificationDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeServiceCertificateLetterReportEN.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("ServiceCertificateDetails", lstEmployeeClarificationDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Employee Service Certificate Arabic *********************************************************************************
        public ActionResult EmployeeServiceCertificateLetterReportViewerAR()
        {
            ViewBag.ActionUrl = "GetEmployeeServiceCertificateLetterReportViewerAR";
            ViewBag.ActionExportUrl = "ExportEmployeeServiceCertificateLetterReportAR";
            ViewBag.ActionPrintUrl = "PrintEmployeeServiceCertificateLetterReportAR";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeServiceCertificateLetterReportViewerAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeServiceCertificateLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeServiceCertificateLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeServiceCertificateLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeServiceCertificateLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeServiceCertificateLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeServiceCertificateLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeServiceCertificateData> lstEmployeeClarificationDetail = objReportingDB.GetEmployeeServiceCertificateReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeClarificationDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeServiceCertificateLetterReportAR.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("ServiceCertificateDetails", lstEmployeeClarificationDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Employee Permission Letter English *********************************************************************************
        public ActionResult EmployeePermissionLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult EmployeePermissionLetterReportViewerEN()
        {
            ViewBag.ActionUrl = "GetEmployeePermissionLetterReportViewerEN";
            ViewBag.ActionExportUrl = "ExportEmployeePermissionLetterReportEN";
            ViewBag.ActionPrintUrl = "PrintEmployeePermissionLetterReportEN";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeePermissionLetterReportViewerEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeePermissionLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeePermissionLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeePermissionLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeePermissionLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeePermissionLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeePermissionLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeePermissionData> lstEmployeeClarificationDetail = objReportingDB.GetEmployeePermissionReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = System.DateTime.Now.DayOfWeek.ToString();
            generalModel.Value2 = System.DateTime.Now.Date.ToString(HRMSDateFormat);
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeClarificationDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeePermissionLetterReportEN.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("PermissionLetterDetails", lstEmployeeClarificationDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", generalModel);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Employee Permission Letter Arabic *********************************************************************************
        public ActionResult EmployeePermissionLetterReportViewerAR()
        {
            ViewBag.ActionUrl = "GetEmployeePermissionLetterReportViewerAR";
            ViewBag.ActionExportUrl = "ExportEmployeePermissionLetterReportAR";
            ViewBag.ActionPrintUrl = "PrintEmployeePermissionLetterReportAR";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeePermissionLetterReportViewerAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeePermissionLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeePermissionLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeePermissionLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text45";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, Feilds));
        }

        public ActionResult ExportEmployeePermissionLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeePermissionLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeePermissionLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeePermissionData> lstEmployeeClarificationDetail = objReportingDB.GetEmployeePermissionReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = System.DateTime.Now.DayOfWeek.ToString();
            generalModel.Value2 = System.DateTime.Now.Date.ToString(HRMSDateFormat);
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeClarificationDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeePermissionLetterReportAR.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("PermissionLetterDetails", lstEmployeeClarificationDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", generalModel);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }
        //*************************************************** Employee Joining Letter English *********************************************************************************
        public ActionResult EmployeeSchoolJoiningDateLetterReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult EmployeeSchoolJoiningDateLetterReportViewerEN()
        {
            ViewBag.ActionUrl = "GetEmployeeSchoolJoiningDateLetterReportViewerEN";
            ViewBag.ActionExportUrl = "ExportEmployeeSchoolJoiningDateLetterReportEN";
            ViewBag.ActionPrintUrl = "PrintEmployeeSchoolJoiningDateLetterReportEN";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeSchoolJoiningDateLetterReportViewerEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeSchoolJoiningDateLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeSchoolJoiningDateLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeSchoolJoiningDateLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeSchoolJoiningDateLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeSchoolJoiningDateLetterReportEN(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeSchoolJoiningDateLetterReportEN(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeSchoolJoiningDateData> lstEmployeeSchoolJoiningDateDetail = objReportingDB.GetEmployeeSchoolJoiningDateReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeSchoolJoiningDateDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeSchoolJoiningDateLetterReportEN.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("SchoolJoiningDateDetails", lstEmployeeSchoolJoiningDateDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Employee Joining Letter English *********************************************************************************
        public ActionResult EmployeeSchoolJoiningDateLetterReportViewerAR()
        {
            ViewBag.ActionUrl = "GetEmployeeSchoolJoiningDateLetterReportViewerAR";
            ViewBag.ActionExportUrl = "ExportEmployeeSchoolJoiningDateLetterReportAR";
            ViewBag.ActionPrintUrl = "PrintEmployeeSchoolJoiningDateLetterReportAR";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeSchoolJoiningDateLetterReportViewerAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeSchoolJoiningDateLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeSchoolJoiningDateLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeSchoolJoiningDateLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeSchoolJoiningDateLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeSchoolJoiningDateLetterReportAR(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeSchoolJoiningDateLetterReportAR(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeSchoolJoiningDateData> lstEmployeeSchoolJoiningDateDetail = objReportingDB.GetEmployeeSchoolJoiningDateReport(EmployeeIds, SignedByEn, SignedByAr);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeSchoolJoiningDateDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/EmployeeLetters/EmployeeSchoolJoiningDateLetterReportAR.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("SchoolJoiningDateDetails", lstEmployeeSchoolJoiningDateDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Employee Absent And Late Records English *********************************************************************************
        public ActionResult EmployeeAbsentAndLateRecords()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult EmployeeAbsentAndLateLetterReportViewerEN()
        {
            ViewBag.ActionUrl = "GetEmployeeAbsentAndLateLetterReportViewerEN";
            ViewBag.ActionExportUrl = "ExportEmployeeAbsentAndLateLetterReportEN";
            ViewBag.ActionPrintUrl = "PrintEmployeeAbsentAndLateLetterReportEN";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeeAbsentAndLateLetterReportViewerEN(string EmployeeIds, string txtFromDate, string txtToDate, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeeAbsentAndLateLetterReportEN(EmployeeIds, txtFromDate, txtToDate, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeeAbsentAndLateLetterReportEN(string EmployeeIds, string txtFromDate, string txtToDate, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeAbsentAndLateLetterReportEN(EmployeeIds, txtFromDate, txtToDate, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeeAbsentAndLateLetterReportEN(string EmployeeIds, string txtFromDate, string txtToDate, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeeAbsentAndLateLetterReportEN(EmployeeIds, txtFromDate, txtToDate, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeeAbsentAndLateLetterReportEN(string EmployeeIds, string txtFromDate, string txtToDate, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            List<EmployeeAbsentAndLateData> lstEmployeeAbsentAndLateData = objReportingDB.GetEmployeeLateAbsentData(EmployeeIds, txtFromDate, txtToDate);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = System.DateTime.Now.DayOfWeek.ToString();
            generalModel.Value2 = System.DateTime.Now.Date.ToString(HRMSDateFormat);
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstEmployeeAbsentAndLateData.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/EmployeeReports/EmployeeAbsentAndLateRecords.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }
                report.RegData("EmployeeAbsentAndLateData", lstEmployeeAbsentAndLateData);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", generalModel);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }


        //**********************************************************************Salary Allowances Certificate*****************************************************************************************************

        public ActionResult SalaryAllowancesCertificateReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader)
        {
            ViewBag.ActionUrl = "GetSalaryAllowancesCertificateReportViewer";
            ViewBag.ActionExportUrl = "ExportSalaryAllowancesCertificateReport";
            ViewBag.ActionPrintUrl = "PrintSalaryAllowancesCertificateReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetSalaryAllowancesCertificateReportViewer(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetSalaryAllowancesCertificateReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintSalaryAllowancesCertificateReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetSalaryAllowancesCertificateReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text41,Text17,Text48,Text19,Text20,Text9,Text10,Text9,Text13,Text16,Text30,Text31,Text32";
            return StiMvcViewer.PrintReportResult(GetFormatedReport(report, Feilds));
        }

        public ActionResult ExportSalaryAllowancesCertificateReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetSalaryAllowancesCertificateReport(EmployeeIds, SignedByEn, SignedByAr, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text41,Text17,Text48,Text19,Text20,Text9,Text10,Text9,Text13,Text16,Text30,Text31,Text32";
            return StiMvcViewer.ExportReportResult(GetFormatedReport(report, Feilds));
        }

        private StiReport GetSalaryAllowancesCertificateReport(string EmployeeIds, string SignedByEn, string SignedByAr, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            DataSet dstEmployeeDeductionData = objReportingDB.GetAllEmployeeDataForReport(EmployeeIds);

            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = DateTime.Today.ToString(HRMSDateFormat);//"yyyy/MM/dd"
            generalModel.Value2 = SignedByAr;
            if (dstEmployeeDeductionData.Tables[0].Rows.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/LetterReports/SalaryLetters/EmployeeSalaryAllowancesReport.mrt"));
                if (!IsDisplayHeader)
                {
                    RemoveHeader(report);
                }
                if (!IsDisplayFooter)
                {
                    RemoveFooter(report);
                }

                report.RegData("EmployeeDetailsData", dstEmployeeDeductionData.Tables[0]);
                report.RegData("EmployeeSalaryDetailsData", dstEmployeeDeductionData.Tables[1]);
                report.RegData("EmployeeAdditionData", dstEmployeeDeductionData.Tables[2]);
                report.RegData("EmployeeDeductionData", dstEmployeeDeductionData.Tables[3]);
                report.RegData("TotalSalary", dstEmployeeDeductionData.Tables[4]);
                report.RegData("TotalAddition", dstEmployeeDeductionData.Tables[5]);
                report.RegData("TotalDeduction", dstEmployeeDeductionData.Tables[6]);
                report.RegData("TotalNetSalaryAmount", dstEmployeeDeductionData.Tables[7]);
                report.RegData("FooterDetails", dstEmployeeDeductionData.Tables[8]);
                report.RegBusinessObject("GeneralModel", generalModel);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Employee Pay Slip Statement Report *********************************************************************************
        public ActionResult EmployeePaySlipStatementReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            var PayCycleList = new PayCycleDB().GetAllPayCycle().Where(x => x.active == false).ToList();
            if (PayCycleList.Count > 0)
            {
                var selectedPayCycle = PayCycleList.FirstOrDefault();
                ViewBag.Paycyle = new SelectList(PayCycleList, "PayCycleID", "PayCycleName_1", selectedPayCycle.PayCycleID).ToList();
            }
            else
            {
                ViewBag.Paycyle = new SelectList(new List<PayCycleModel>(), "PayCycleID", "PayCycleName_1").ToList();
            }
            return View();
        }
        public ActionResult EmployeePaySlipStatementReportViewer()
        {
            ViewBag.ActionUrl = "GetEmployeePaySlipStatementReportViewer";
            ViewBag.ActionExportUrl = "ExportEmployeePaySlipStatementReport";
            ViewBag.ActionPrintUrl = "PrintEmployeePaySlipStatementReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetEmployeePaySlipStatementReportViewer(string EmployeeIds, string CycleIds, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetEmployeePaySlipStatementReport(EmployeeIds, CycleIds, IsDisplayHeader, IsDisplayFooter));
        }

        public ActionResult PrintEmployeePaySlipStatementReport(string EmployeeIds, string CycleIds, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeePaySlipStatementReport(EmployeeIds, CycleIds, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportEmployeePaySlipStatementReport(string EmployeeIds, string CycleIds, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            StiReport report = GetEmployeePaySlipStatementReport(EmployeeIds, CycleIds, IsDisplayHeader, IsDisplayFooter);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetEmployeePaySlipStatementReport(string EmployeeIds, string CycleIds, bool IsDisplayHeader, bool IsDisplayFooter)
        {
            string[] arr = EmployeeIds.Split(',');
            StiReport report = new StiReport();
            report.Load(Server.MapPath("/Content/Reports/SalaryReport/PaySlipStatementReport.mrt"));
            DataSet dstPaySlipStatementReport = objReportingDB.GetPaySlipStatementReport(EmployeeIds, CycleIds, 1);
            string orientation = "landscape";
            string reportName = "Pay Slip Statement";
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            report.ReportName = reportName;
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            txtReportName.Text = reportName;
            report.RegData("EmployeeDetails", dstPaySlipStatementReport.Tables[0]);
            StiDataSource dataSource = report.Dictionary.DataSources[0];
            int i = 0;
            for (int j = 0; j < arr.Length; j++)
            {

                DataSet dstPaySlipStatementReport1 = objReportingDB.GetPaySlipStatementReport(arr[j], CycleIds, 2);
                if (dstPaySlipStatementReport1.Tables.Count > 0)
                {
                    dstPaySlipStatementReport1.Tables[0].TableName = "Table_" + i.ToString();
                    dstPaySlipStatementReport.Tables.Add(dstPaySlipStatementReport1.Tables[0].Copy());
                    string objReportBuilder = "ReportBuilder" + i.ToString();
                    DataTable dt = dstPaySlipStatementReport.Tables[i + 1];
                    dt.TableName = objReportBuilder;

                    //Add data to datastore                        
                    report.RegData(dt);

                    //Fill dictionary
                    report.Dictionary.Synchronize();
                    StiDataSource dataSource1 = report.Dictionary.DataSources[i + 1];
                    string objMyRelation = "MyRelation" + i.ToString();
                    StiDataRelation dataRelation = new StiDataRelation(objMyRelation, dataSource, dataSource1, new string[] { "EmployeeID" }, new string[] { "EmployeeId" });
                    report.Dictionary.RegRelation(dataRelation);
                    report.Dictionary.Relations.Add(dataRelation);

                    StiPage page = report.Pages[0];
                    page.Orientation = StiPageOrientation.Portrait;
                    //page.Width = 26;

                    //Create HeaderBand
                    StiHeaderBand headerBand = new StiHeaderBand();
                    headerBand.Name = "headerBand_" + i.ToString();
                    headerBand.Height = 1.2;
                    headerBand.MinHeight = 1.2;
                    headerBand.MaxHeight = 2;
                    headerBand.CanShrink = true;
                    headerBand.CanGrow = true;
                    page.Components.Add(headerBand);

                    //Create Databand
                    StiDataBand dataBand = new StiDataBand();
                    dataBand.DataSourceName = objReportBuilder;
                    dataBand.Height = 1;
                    dataBand.MaxHeight = 2;
                    dataBand.MinHeight = 1;
                    dataBand.Name = "DataBand_" + i.ToString();
                    headerBand.CanShrink = true;
                    headerBand.CanGrow = true;
                    dataBand.MasterComponent = report.GetComponentByName("DataBand1");

                    page.Components.Add(dataBand);
                    dataBand.DataRelationName = objMyRelation;
                    StiDataRelation srt = dataBand.DataRelation;

                    double pos = 0;
                    int cmncount = dt.Columns.Count - 2;
                    double columnWidth = Math.Round(page.Width / dt.Columns.Count, 2);
                    double maincolumnWidth = Math.Round(page.Width / cmncount, 2);
                    page.Width = columnWidth * dt.Columns.Count;
                    int nameIndex = 1;
                    columnWidth = maincolumnWidth;
                    //Create texts
                    foreach (DataColumn column in dt.Columns)
                    {
                        string strcolumn = column.ColumnName;
                        if (!((strcolumn.Equals("TempEmpID")) || (strcolumn.Equals("EmployeeId"))))
                        {
                            //Create text on header
                            StiText headerText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                            headerText.Text.Value = column.ColumnName;
                            headerText.Name = "HeaderText" + nameIndex.ToString() + i.ToString();
                            CommonParameters(true, ref headerText);
                            headerBand.Components.Add(headerText);

                            //Create text on Data Band
                            StiText dataText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                            dataText.Text.Value = "{" + objReportBuilder + "." + column.ColumnName.Trim().Replace(" ", "_").Replace(".", "_") + "}";
                            dataText.Name = "DataText" + nameIndex.ToString() + i.ToString();
                            CommonParameters(false, ref dataText);
                            dataBand.Components.Add(dataText);
                            pos += columnWidth;
                            nameIndex++;
                        }
                    }
                    StringBuilder sbText = new StringBuilder();
                    sbText.Append(schoolInfo.SchoolName_1);
                    i++;
                }
            }
            if (dstPaySlipStatementReport.Tables[0].Rows.Count == 0)
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Pay Cycle Summary Report *********************************************************************************
        public ActionResult PayCycleSummaryReport()
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in new GeneralAccountsDB().GetPayCategories().Where(m => m.id > 0))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.id.ToString() });
            }
            ViewBag.Categories = ObjSelectedList;

            var PayCycleList = new PayCycleDB().GetAllPayCycle().Where(x => x.active == false).ToList();
            if (PayCycleList.Count > 0)
            {
                var selectedPayCycle = PayCycleList.FirstOrDefault();
                ViewBag.Paycyle = new SelectList(PayCycleList, "PayCycleID", "PayCycleName_1", selectedPayCycle.PayCycleID).ToList();
            }
            else
            {
                ViewBag.Paycyle = new SelectList(new List<PayCycleModel>(), "PayCycleID", "PayCycleName_1").ToList();
            }
            return View();
        }
        public ActionResult PayCycleSummaryReportViewer()
        {
            ViewBag.ActionUrl = "GetPayCycleSummaryReportViewer";
            ViewBag.ActionExportUrl = "ExportPayCycleSummaryReport";
            ViewBag.ActionPrintUrl = "PrintPayCycleSummaryReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetPayCycleSummaryReportViewer(string PayCycleId, string CategoryIds)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetPayCycleSummaryReport(PayCycleId, CategoryIds));
        }

        public ActionResult PrintPayCycleSummaryReport(string PayCycleId, string CategoryIds)
        {
            StiReport report = GetPayCycleSummaryReport(PayCycleId, CategoryIds);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportPayCycleSummaryReport(string PayCycleId, string CategoryIds)
        {
            StiReport report = GetPayCycleSummaryReport(PayCycleId, CategoryIds);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetPayCycleSummaryReport(string PayCycleId, string CategoryIds)
        {
            DataSet dstPayCycleSummaryData = objReportingDB.GetPayCycleSummaryReport(PayCycleId, CategoryIds);

            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = DateTime.Today.ToString(HRMSDateFormat);//"yyyy/MM/dd"
            if (dstPayCycleSummaryData.Tables[0].Rows.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/SalaryReport/PayCycleSummaryReport.mrt"));
                report.RegData("PayCycleSummaryData", dstPayCycleSummaryData.Tables[0]);
                report.RegData("SummaryGrandTotal", dstPayCycleSummaryData.Tables[1]);
                report.RegBusinessObject("GeneralModel", generalModel);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }

        //*************************************************** Salaries By Section Report *********************************************************************************
        public ActionResult SalariesBySectionReport()
        {
            CompanyDB objCompanyDB = new CompanyDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in new EmployeeDB().GetAllSectionList().Where(m => m.id > 0))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.id.ToString() });
            }
            ViewBag.SectionList = ObjSelectedList;

            //List<SelectListItem> ObjOrganizationList = new List<SelectListItem>();
            //foreach (var m in objCompanyDB.GetAllCompanyList().Where(m => m.CompanyId > 0))
            //{
            //    ObjOrganizationList.Add(new SelectListItem { Text = m.name.ToString(), Value = m.CompanyId.ToString() });
            //}
            ViewBag.OrganizationList = GetCompanySelectList();// ObjOrganizationList;
            return View();
        }
        public ActionResult SalariesBySectionReportViewer()
        {
            ViewBag.ActionUrl = "GetSalariesBySectionReportViewer";
            ViewBag.ActionExportUrl = "ExportSalariesBySectionReport";
            ViewBag.ActionPrintUrl = "PrintSalariesBySectionReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetSalariesBySectionReportViewer(string SectionId, string OrganizationIds)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetSalariesBySectionReport(SectionId, OrganizationIds));
        }

        public ActionResult PrintSalariesBySectionReport(string SectionId, string OrganizationIds)
        {
            StiReport report = GetSalariesBySectionReport(SectionId, OrganizationIds);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportSalariesBySectionReport(string SectionId, string OrganizationIds)
        {
            StiReport report = GetSalariesBySectionReport(SectionId, OrganizationIds);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetSalariesBySectionReport(string SectionId, string OrganizationIds)
        {
            DataSet dstPayCycleSummaryData = objReportingDB.GetSalariesBySectionReport(SectionId, OrganizationIds);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/SalaryReport/SalariesBySectionReport.mrt"));
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            txtReportName.Text = "Salaries By Section Report";

            DataTable tblDR = dstPayCycleSummaryData.Tables[0];

            tblDR.TableName = "tblDR";
            report.RegData(tblDR);
            report.RegData(tblDR);
            report.Dictionary.Synchronize();
            StiPage page = report.Pages[0];

            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Height = 0.5;
            headerBand.Name = "HeaderBand";
            page.Components.Add(headerBand);

            StiDataBand stiDataBand = new StiDataBand();

            StiTable table = new StiTable();
            table.Name = "Table1";
            table.ColumnCount = (tblDR.Columns.Count) - 1;
            table.RowCount = 3;
            table.HeaderRowsCount = 1;
            table.FooterRowsCount = 1;
            table.Width = page.Width;
            table.Height = page.GridSize * 12;
            table.DataSourceName = String.Format("{0}", tblDR.TableName);
            page.Components.Add(table);
            table.CreateCell();

            double pos = 0;
            int cmncount = tblDR.Columns.Count - 1;
            double columnWidth = Math.Round(table.Width / tblDR.Columns.Count, 2);
            double maincolumnWidth = Math.Round(page.Width / cmncount, 2);
            // page.Width = columnWidth * tblDR.Columns.Count;
            int nameIndex = 1;
            columnWidth = maincolumnWidth;

            int indexHeaderCell = 0;
            int count = 0;
            double FirstHeaderWidth = 0.00;
            double SecondHeaderWidth = 0.00;
            int txtId = 0;
            int TypeColumnsCount = (tblDR.Columns.Count - 3) / 2;
            foreach (DataColumn column in tblDR.Columns)
            {
                string strcolumn = column.ColumnName;
                if (!strcolumn.Equals("Name"))
                {
                    if (strcolumn.Equals("Number") || strcolumn.Equals("Total"))
                    {
                        if (strcolumn.Equals("Number"))
                        {
                            StiText headerText1 = new StiText(new RectangleD(pos, 0, (columnWidth + columnWidth), 1f));
                            string sDataCellExpresion = String.Format("{{{0}.{1}}}", tblDR.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols("Name"));
                            headerText1.Text.Value = sDataCellExpresion;// "Company Name";
                            headerText1.Name = "HeaderText" + txtId.ToString();
                            headerText1.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                            headerText1.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                            headerText1.HorAlignment = StiTextHorAlignment.Center;
                            headerText1.VertAlignment = StiVertAlignment.Center;
                            headerText1.Border.Side = StiBorderSides.All;
                            headerText1.CanGrow = true;
                            headerText1.CanShrink = false;
                            headerText1.GrowToHeight = true;
                            headerBand.Components.Add(headerText1);
                        }
                    }
                    else
                    {
                        StiText headerText1 = new StiText(new RectangleD(pos, 0, columnWidth, 1f));
                        headerText1.Text.Value = "";
                        headerText1.Name = "HeaderText" + txtId.ToString();
                        headerText1.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                        headerText1.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                        headerText1.HorAlignment = StiTextHorAlignment.Center;
                        headerText1.VertAlignment = StiVertAlignment.Center;
                        headerText1.Border.Side = StiBorderSides.All;
                        headerText1.CanGrow = true;
                        headerText1.CanShrink = false;
                        headerText1.GrowToHeight = true;
                        headerBand.Components.Add(headerText1);
                    }
                    pos += columnWidth;
                    txtId++;
                }
            }

            foreach (DataColumn column in tblDR.Columns)
            {
                ////Set text on header
                string strcolumn = column.ColumnName;
                if (!strcolumn.Equals("Name"))
                {
                    StiTableCell headerCell = table.Components[indexHeaderCell] as StiTableCell;
                    headerCell.Text.Value = column.Caption.Replace("Rem_", "");
                    headerCell.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                    headerCell.HorAlignment = StiTextHorAlignment.Center;
                    headerCell.VertAlignment = StiVertAlignment.Center;
                    headerCell.Border.Side = StiBorderSides.All;
                    headerCell.WordWrap = true;
                    headerCell.Margins = new StiMargins(3, 3, 3, 3);
                    headerCell.CanGrow = true;
                    headerCell.CanShrink = false;
                    headerCell.GrowToHeight = true;
                    headerCell.Height = 1f;
                    headerCell.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                    SecondHeaderWidth += headerCell.Width;
                    indexHeaderCell++;
                }
            }

            int condCounter = 0;
            foreach (DataColumn column in tblDR.Columns)
            {
                string strcolumn = column.ColumnName;
                if (!strcolumn.Equals("Name"))
                {
                    StiTableCell dataCell = table.Components[indexHeaderCell] as StiTableCell;
                    dataCell.HorAlignment = StiTextHorAlignment.Center;
                    dataCell.VertAlignment = StiVertAlignment.Center;
                    dataCell.Border.Side = StiBorderSides.All;
                    string sDataCellExpresion = String.Format("{{{0}.{1}}}", tblDR.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols(column.ColumnName));

                    dataCell.Text.Value = sDataCellExpresion;
                    if (strcolumn.Equals("Section"))
                    {
                        dataCell.ProcessingDuplicates = StiProcessingDuplicatesType.Merge;
                    }
                    dataCell.Font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
                    dataCell.WordWrap = true;
                    dataCell.Margins = new StiMargins(3, 3, 3, 3);
                    dataCell.FixedWidth = true;
                    dataCell.CanGrow = true;
                    dataCell.CanShrink = false;
                    dataCell.GrowToHeight = true;
                    dataCell.Height = 1f;
                    indexHeaderCell++;
                    condCounter++;
                }
            }

            report.ReportAlias = "SalariesBySectionReport";
            report.ReportName = "SalariesBySectionReport";
            return report;
        }

        private StiReport GetSalariesBySectionReport_1(string SectionId, string OrganizationIds)
        {
            SectionId = "0," + SectionId;
            DataSet dstPayCycleSummaryData = objReportingDB.GetSalariesBySectionReport(SectionId, OrganizationIds);
            //DataSet dstPaySlipStatementReport = objReportingDB.GetPaySlipStatementReport(EmployeeIds, CycleIds);
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/SalaryReport/SalariesBySectionReport.mrt"));
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            // report.RegData("EmployeeDetails", dstPayCycleSummaryData.Tables[0]);
            StiComponent sc = report.GetComponentByName("txtReportName");
            StiText txtReportName = (StiText)report.GetComponentByName("txtReportName");
            txtReportName.Text = "Salaries By Section Report";

            DataTable tblDR = dstPayCycleSummaryData.Tables[0];

            tblDR.TableName = "tblDR";
            report.RegData(tblDR);
            report.RegData(tblDR);
            report.Dictionary.Synchronize();
            StiPage page = report.Pages[0];

            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Height = 0.5;
            headerBand.Name = "HeaderBand";
            page.Components.Add(headerBand);

            StiDataBand stiDataBand = new StiDataBand();

            StiTable table = new StiTable();
            table.Name = "Table1";
            table.ColumnCount = (tblDR.Columns.Count) - 1;
            table.RowCount = 3;
            table.HeaderRowsCount = 1;
            table.FooterRowsCount = 1;
            table.Width = page.Width;
            table.Height = page.GridSize * 12;
            table.DataSourceName = String.Format("{0}", tblDR.TableName);
            page.Components.Add(table);
            table.CreateCell();
            GridView gv = new GridView();
            gv.DataSource = dstPayCycleSummaryData.Tables[0];
            gv.DataBind();
            double pos = 0;
            int cmncount = tblDR.Columns.Count - 1;
            double columnWidth = Math.Round(table.Width / tblDR.Columns.Count, 2);
            double maincolumnWidth = Math.Round(page.Width / cmncount, 2);
            // page.Width = columnWidth * tblDR.Columns.Count;
            int nameIndex = 1;
            columnWidth = maincolumnWidth;

            int indexHeaderCell = 0;
            int count = 0;
            int SecCount = 7;
            double FirstHeaderWidth = 0.00;
            double SecondHeaderWidth = 0.00;
            int txtId = 0;
            int TypeColumnsCount = (tblDR.Columns.Count - 1) / 2;
            foreach (DataColumn column in tblDR.Columns)
            {
                string strcolumn = column.ColumnName;
                if (SecCount > 0)
                {
                    if (!strcolumn.Equals("Name"))
                    {
                        if (strcolumn.Equals("Number") || strcolumn.Equals("Total"))
                        {
                            if (strcolumn.Equals("Number"))
                            {
                                StiText headerText1 = new StiText(new RectangleD(pos, 0, (columnWidth + columnWidth), 1f));
                                string sDataCellExpresion = String.Format("{{{0}.{1}}}", tblDR.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols("Name"));
                                headerText1.Text.Value = sDataCellExpresion;// "Company Name";
                                headerText1.Name = "HeaderText" + txtId.ToString();
                                headerText1.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                                headerText1.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                                headerText1.HorAlignment = StiTextHorAlignment.Center;
                                headerText1.VertAlignment = StiVertAlignment.Center;
                                headerText1.Border.Side = StiBorderSides.All;
                                headerText1.CanGrow = true;
                                headerText1.CanShrink = false;
                                headerText1.GrowToHeight = true;
                                headerBand.Components.Add(headerText1);
                            }
                        }
                        else
                        {
                            StiText headerText1 = new StiText(new RectangleD(pos, 0, columnWidth, 1f));
                            headerText1.Text.Value = "";
                            headerText1.Name = "HeaderText" + txtId.ToString();
                            headerText1.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                            headerText1.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                            headerText1.HorAlignment = StiTextHorAlignment.Center;
                            headerText1.VertAlignment = StiVertAlignment.Center;
                            headerText1.Border.Side = StiBorderSides.All;
                            headerText1.CanGrow = true;
                            headerText1.CanShrink = false;
                            headerText1.GrowToHeight = true;
                            headerBand.Components.Add(headerText1);
                        }
                        pos += columnWidth;
                        txtId++;
                    }
                }
                SecCount--;
            }
            SecCount = 7;
            foreach (DataColumn column in tblDR.Columns)
            {
                ////Set text on header
                string strcolumn = column.ColumnName;
                if (SecCount > 0)
                {
                    if (!strcolumn.Equals("Name"))
                    {
                        StiTableCell headerCell = table.Components[indexHeaderCell] as StiTableCell;
                        headerCell.Text.Value = column.Caption.Replace("Rem_", "");
                        headerCell.Font = new System.Drawing.Font("Arial", 10f, FontStyle.Bold);
                        headerCell.HorAlignment = StiTextHorAlignment.Center;
                        headerCell.VertAlignment = StiVertAlignment.Center;
                        headerCell.Border.Side = StiBorderSides.All;
                        headerCell.WordWrap = true;
                        headerCell.Margins = new StiMargins(3, 3, 3, 3);
                        headerCell.CanGrow = true;
                        headerCell.CanShrink = false;
                        headerCell.GrowToHeight = true;
                        headerCell.Height = 1f;
                        headerCell.Brush = new StiSolidBrush(System.Drawing.Color.FromArgb(244, 238, 224));
                        SecondHeaderWidth += headerCell.Width;
                        indexHeaderCell++;
                    }
                }
                SecCount--;
            }

            int condCounter = 0;
            SecCount = 7;
            foreach (DataColumn column in tblDR.Columns)
            {
                string strcolumn = column.ColumnName;
                if (SecCount > 0)
                {
                    if (!strcolumn.Equals("Name"))
                    {
                        StiTableCell dataCell = table.Components[indexHeaderCell] as StiTableCell;
                        dataCell.HorAlignment = StiTextHorAlignment.Center;
                        dataCell.VertAlignment = StiVertAlignment.Center;
                        dataCell.Border.Side = StiBorderSides.All;
                        string sDataCellExpresion = String.Format("{{{0}.{1}}}", tblDR.TableName, Stimulsoft.Report.CodeDom.StiCodeDomSerializator.ReplaceSymbols(column.ColumnName));

                        dataCell.Text.Value = sDataCellExpresion;
                        if (strcolumn.Equals("Section"))
                        {
                            dataCell.ProcessingDuplicates = StiProcessingDuplicatesType.Merge;
                        }
                        dataCell.Font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
                        dataCell.WordWrap = true;
                        dataCell.Margins = new StiMargins(3, 3, 3, 3);
                        dataCell.FixedWidth = true;
                        dataCell.CanGrow = true;
                        dataCell.CanShrink = false;
                        dataCell.GrowToHeight = true;
                        dataCell.Height = 1f;
                        indexHeaderCell++;
                        condCounter++;
                    }
                }
                SecCount--;
            }
            report.ReportAlias = "PaySlipStatementReport";
            report.ReportName = "PaySlipStatementReport";
            return report;
        }

        //*************************************************** Jordan Income Tax Report *********************************************************************************
        public ActionResult JordanIncomeTaxReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult JordanIncomeTaxReportViewer()
        {
            ViewBag.ActionUrl = "GetJordanIncomeTaxReportViewer";
            ViewBag.ActionExportUrl = "ExportJordanIncomeTaxReport";
            ViewBag.ActionPrintUrl = "PrintJordanIncomeTaxReport";
            return View("LetterReportViewer");
        }

        public ActionResult GetJordanIncomeTaxReportViewer(string EmployeeIds, string sortBy)
        {
            return StiMvcViewer.GetReportSnapshotResult(GetJordanIncomeTaxReport(EmployeeIds, sortBy));
        }

        public ActionResult PrintJordanIncomeTaxReport(string EmployeeIds, string sortBy)
        {
            StiReport report = GetJordanIncomeTaxReport(EmployeeIds, sortBy);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.PrintReportResult(report);
        }

        public ActionResult ExportJordanIncomeTaxReport(string EmployeeIds, string sortBy)
        {
            StiReport report = GetJordanIncomeTaxReport(EmployeeIds, sortBy);
            string Feilds = "Text16,Text34,Text35,Text39,Text4,Text17";
            return StiMvcViewer.ExportReportResult(report);
        }

        private StiReport GetJordanIncomeTaxReport(string EmployeeIds, string sortBy)
        {
            List<JordanIncomeTaxData> lstJordanIncomeTaxDetail = null;
            if (sortBy.Equals("ASC"))
                lstJordanIncomeTaxDetail = objReportingDB.GetJordanIncomeTaxReport(EmployeeIds).OrderBy(x => x.EmpNameAr).ToList();
            if (sortBy.Equals("DESC"))
                lstJordanIncomeTaxDetail = objReportingDB.GetJordanIncomeTaxReport(EmployeeIds).OrderByDescending(x => x.EmpNameAr).ToList();
            StiReport report = new StiReport();
            ReportingDB reportingDB = new ReportingDB();
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = System.DateTime.Now.DayOfWeek.ToString();
            generalModel.Value2 = System.DateTime.Now.Date.ToString(HRMSDateFormat);
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            schoolInfo.AcNextYear = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault().Duration;
            if (lstJordanIncomeTaxDetail.Count() > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/JordanIncomeTaxReport.mrt"));
                report.RegData("JordanIncomeTaxDetails", lstJordanIncomeTaxDetail);
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", generalModel);
            }
            else
            {
                report = NoDataReport();
            }
            return report;
        }


        //***************************************************************************************************************************************************************************
        public ActionResult GetHighChartsView()
        {
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            return View("HighChartViewer", schoolInfo);
        }

        public ActionResult GetEmployeenationalityPoints(int? departmentId, int? MOLTitleID, int? MOETitleID)
        {
            DashBoardDB dashBoardDB = new DashBoardDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var vList = new object();
            List<NationalityCountModel> lstEmployeenationality = new List<NationalityCountModel>();
            NationalityDB objNationalityDB = new NationalityDB();
            lstEmployeenationality = objNationalityDB.GetEmployeeNationalityCountWithFilters(departmentId, MOLTitleID, MOETitleID, objUserContextViewModel.UserId);
            int TotalCount = lstEmployeenationality.Sum(m => m.NationalityCount);
            vList = new
            {
                aaData = (from item in lstEmployeenationality
                          select new
                          {
                              name = item.NationalityName,
                              y = Math.Round((Convert.ToDecimal(item.NationalityCount) / TotalCount), 2),
                          }).ToArray()

            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getImage(string base64image)
        {
            string subPath = "~/Uploads/GraphImage/SVG";
            bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));
            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
            subPath = "~/Uploads/GraphImage/PNG";
            exists = System.IO.Directory.Exists(Server.MapPath(subPath));
            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

            string convert = base64image.Replace("data:image/svg+xml;base64,", String.Empty);
            var randomFileName = Guid.NewGuid().ToString().Substring(0, 4);
            var bytes = Convert.FromBase64String(convert);
            using (var img = new FileStream(Server.MapPath("~/Uploads/GraphImage/SVG/" + randomFileName + ".svg"), FileMode.Create))
            {
                img.Write(bytes, 0, bytes.Length);
                img.Flush();
            }
            var svgDocument = SvgDocument.Open(Path.Combine(Server.MapPath("~/Uploads/GraphImage/SVG/"), randomFileName + ".svg"));
            var bitmap = svgDocument.Draw();
            //save converted svg to file system
            bitmap.Save(Server.MapPath("~/Uploads/GraphImage/PNG/" + randomFileName + ".png"), ImageFormat.Png);
            return Json(randomFileName, JsonRequestBehavior.AllowGet);
        }
        public void GetNationalityReportPdf(string imgname)
        {
            string FileNameForDownlaod = "NationalityReport" + DateTime.Now.ToString("ddmmyyyyhm");
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + FileNameForDownlaod + ".pdf'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenrateGraphicalReportPDF(Response, Server.MapPath("~/Uploads/GraphImage/png/" + imgname + ".png"));
            if (System.IO.File.Exists(Server.MapPath("~/Uploads/GraphImage/PNG/" + imgname + ".png")))
            {
                System.IO.File.Delete(Server.MapPath("~/Uploads/GraphImage/PNG/" + imgname + ".png"));
            }
            if (System.IO.File.Exists(Server.MapPath("~/Uploads/GraphImage/SVG/" + imgname + ".svg")))
            {
                System.IO.File.Delete(Server.MapPath("~/Uploads/GraphImage/SVG/" + imgname + ".svg"));
            }
            Response.Write(dc);
            Response.End();
        }

        public void downloadGraphReport(string imgname)
        {
            StiReport report = new StiReport();
            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            schoolInfo.TodaysDateTime = DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt");
            report.Load(Server.MapPath("~/Content/Reports/GraphicalReports/EmployeeReports/EmployeeCountByNationality1.mrt"));
            GeneralModel objGeneralModel = new GeneralModel();
            objGeneralModel.Value6 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Uploads/GraphImage/SVG/" + imgname + ".svg";
            report.RegBusinessObject("SchoolInfoModel", schoolInfo);
            report.RegBusinessObject("GeneralModel", objGeneralModel);
            StiOptions.Export.Html.UseImageResolution = true;
            //report.
            StiReportResponse.ResponseAsPdf(report);
        }

        /****Crystal Reports  *******/
        public void AddCommonParameters(List<CustomParameterField> parameterList)
        {

            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            string AddLineEn1 = "";
            if (!string.IsNullOrEmpty(schoolInfo.POBox))
            {
                AddLineEn1 += schoolInfo.POBox + " ";
            }
            if (!string.IsNullOrEmpty(schoolInfo.City))
            {
                AddLineEn1 += schoolInfo.City + ", ";
            }
            if (!string.IsNullOrEmpty(schoolInfo.Country))
            {
                AddLineEn1 += schoolInfo.Country;
            }

            string AddLineAr1 = "";
            if (!string.IsNullOrEmpty(schoolInfo.POBox_2))
            {
                AddLineAr1 += schoolInfo.POBox_2 + " ";
            }
            if (!string.IsNullOrEmpty(schoolInfo.City_2))
            {
                AddLineAr1 += schoolInfo.City_2 + ", ";
            }
            if (!string.IsNullOrEmpty(schoolInfo.Country_2))
            {
                AddLineAr1 += schoolInfo.Country_2;
            }
            parameterList.Add(new CustomParameterField("SchoolNameEn", schoolInfo.SchoolName_1));
            parameterList.Add(new CustomParameterField("SchoolNameAr", schoolInfo.SchoolName_2));
            parameterList.Add(new CustomParameterField("CampusNameEn", schoolInfo.CampusName_1));
            parameterList.Add(new CustomParameterField("CampusNameAr", schoolInfo.CampusName_2));
            parameterList.Add(new CustomParameterField("AddressLineEn1", AddLineEn1));
            parameterList.Add(new CustomParameterField("AddressLineAr1", AddLineAr1));
            parameterList.Add(new CustomParameterField("AddressLineEn2", schoolInfo.Phone + schoolInfo.Fax));
            parameterList.Add(new CustomParameterField("AddressLineAr2", schoolInfo.Phone_2 + schoolInfo.Fax_2));
            parameterList.Add(new CustomParameterField("SchoolPOBoxAr", schoolInfo.POBox_2));
            parameterList.Add(new CustomParameterField("SchoolCityAr", schoolInfo.City_2));
            parameterList.Add(new CustomParameterField("SchoolCountryAr", schoolInfo.Country_2));
            string logoPath = System.Web.HttpContext.Current.Server.MapPath(schoolInfo.Logo);
            parameterList.Add(new CustomParameterField("SchoolLogo", logoPath));
            HRMS.Entities.Employee emp = (HRMS.Entities.Employee)Session["EmpInfo"];
            parameterList.Add(new CustomParameterField("PrintedBy", emp.FullName));
            parameterList.Add(new CustomParameterField("PrintedOn", DateTime.Now.ToString(HRMSDateFormat + " hh:mm tt")));
        }
        /****Salary Statement Report***/
        public ActionResult SalaryStatementDetailReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId).Where(x => x.EmployeeId > 0).OrderBy(x => x.FirstName), "EmployeeId", "FirstName").ToList();
            emplist.Insert(0, (new SelectListItem { Text = "Select Employee", Value = "" }));
            ViewBag.ActiveEmployee = emplist;
            return View();
        }

        public ActionResult LoadCrystalReport(string fromDate, string toDate, int empId, bool ShowAirfare)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = Server.MapPath("~/Content/Reports/SalaryReport/SalaryStatementDetailReport.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            parameterFieldList.Add(new CustomParameterField("ShowAirfareInformation", ShowAirfare));
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("EmployeeId", empId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementEmployeeInfo");

            parameterFieldList.Add(new CustomParameterField("@EmployeeID", empId));

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);
            DataTable dt1 = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementSalaryInfo");
            reportDocPersister.SubReports.Add("SalaryInfoReport", dt1);

            DataTable dtCurrentAccomodationInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementCurrentAccomodationInfo");
            reportDocPersister.SubReports.Add("CurrentAccomodationInfoReport", dtCurrentAccomodationInfo);

            DataTable dtAirfareInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementEmpAirfareInfo");
            reportDocPersister.SubReports.Add("AirfareInfoReport", dtAirfareInfo);

            int Accommodationsundry = 0;
            int Furnituresundry = 0;
            string Account = "";
            // For Get Account
            DataTable dtACcAccount = dataHelper.ExcuteCommandText("SELECT TOP 1 AccountCode FROM GEN_HRSundry WHERE IsAccommodation = 1");
            foreach (DataRow row in dtACcAccount.Rows)
            {
                Account = row["AccountCode"].ToString();
            }
            DataTable dtAccSundry = dataHelper.ExcuteCommandText("SELECT TOP 1 HRSundryID FROM GEN_HRSundry WHERE IsAccommodation = 1");
            foreach (DataRow row in dtAccSundry.Rows)
            {
                Accommodationsundry = Convert.ToInt32(row["HRSundryID"].ToString());
            }
            DataTable dtFurniture = dataHelper.ExcuteCommandText("select top 1 HRSundryID FROM GEN_HRSundry Where IsFurniture=1");
            foreach (DataRow row in dtFurniture.Rows)
            {
                Furnituresundry = Convert.ToInt32(row["HRSundryID"].ToString());
            }
            parameterList.Add(new SqlParameter("@datefrom", fromDate));
            parameterList.Add(new SqlParameter("@dateto", toDate));
            parameterList.Add(new SqlParameter("@accommodationSundryId", Accommodationsundry));
            DataTable dtAccomodationInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementAccomodationInfo");
            reportDocPersister.SubReports.Add("AccomodationReport", dtAccomodationInfo);

            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeId", empId));
            DataTable dtInsuranceInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_SalaryStatementInsuranceInfo");
            reportDocPersister.SubReports.Add("InsuranceInfoReport", dtInsuranceInfo);

            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeId", empId));
            DataTable dtFamilyOutstanding = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementFamilyOutstandingDetails");
            reportDocPersister.SubReports.Add("FamilyOutstandingReport", dtFamilyOutstanding);

            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeId", empId));
            parameterList.Add(new SqlParameter("@dateto", toDate));
            parameterList.Add(new SqlParameter("@datefrom", fromDate));
            DataTable dtPayrollInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementPayrollInfo");
            reportDocPersister.SubReports.Add("Payrolldetails", dtPayrollInfo);

            DataTable dtPayments = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_SalaryStatementPaymentLabel");
            reportDocPersister.SubReports.Add("Payments", dtPayments);

            DataTable dtCRDRInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementCRDR");
            reportDocPersister.SubReports.Add("DRCR", dtCRDRInfo);

            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeId", empId));
            parameterList.Add(new SqlParameter("@dateto", toDate));
            parameterList.Add(new SqlParameter("@datefrom", fromDate));
            parameterList.Add(new SqlParameter("@accommodationSundryId", Accommodationsundry));
            parameterList.Add(new SqlParameter("@AccommodationAccount", Account));
            DataTable dtTransferInfo = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_SalaryStatementPayTransferInfo");
            reportDocPersister.SubReports.Add("Transfer", dtTransferInfo);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }

        private DataTable GetReportData(string Query)
        {
            DataHelper dataHelper = new DataHelper();

            return dataHelper.ExcuteCommandText(Query);
        }

        private DataSet GetReportDataSet()
        {
            DataHelper dataHelper = new DataHelper();
            DataSet ds = new DataSet();
            string query = "Hr_Stp_SalaryStatementEmployeeInfo";
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeId", 104));
            ds.Tables.Add(dataHelper.ExcuteStoredProcedureWithParameter(parameterList, query));
            query = "select * from GEN_Holiday";
            ds.Tables.Add(dataHelper.ExcuteStoredProcedureWithParameter(parameterList, ""));

            return ds;

        }

        public string GetReportViewerPageUrl(ReportDocumentPersister reportDocPersister)
        {
            string key = Guid.NewGuid().ToString();
            Cache cacheObj = System.Web.HttpContext.Current.Cache;

            try
            {
                cacheObj.Remove(key);
            }
            catch
            {
            }

            cacheObj.Insert(key, reportDocPersister, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(30));
            return "/aspx/ReportViewer.aspx?r=" + key;
        }

        /****Salary Revision report***/
        public ActionResult SalaryRevisionCrReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId).Where(x => x.EmployeeId > 0).OrderBy(x => x.FirstName), "EmployeeId", "FirstName").ToList();

            ViewBag.ActiveEmployee = emplist;

            List<string> selectedValue = new List<string>();
            if (Session["EmployeeIds"] != null)
            {
                selectedValue = Session["EmployeeIds"].ToString().Split(',').Select(x => x).ToList();
                Session["EmployeeIds"] = null;
            }
            return View(selectedValue);
        }

        public ActionResult LoadSalaryRevisionCrReport(string EmpIDs)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = Server.MapPath("~/Content/Reports/SalaryReport/SalaryRevisionReport.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmpIds", EmpIDs));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_GetSalaryRevisionData");
            parameterFieldList.Add(new CustomParameterField("@EmpIds", EmpIDs));
            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }


        /*********************/

        //-------------------------------------------------------------------------------------------------
        public ActionResult SalaryListReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            var PayCycleList = new PayCycleDB().GetAllPayCycle().ToList();
            if (PayCycleList.Count > 0)
            {
                var selectedPayCycle = PayCycleList.FirstOrDefault();
                ViewBag.Paycyle = new SelectList(PayCycleList, "PayCycleID", "PayCycleName_1", selectedPayCycle.PayCycleID).ToList();
            }
            else
            {
                ViewBag.Paycyle = new SelectList(new List<PayCycleModel>(), "PayCycleID", "PayCycleName_1").ToList();
            }
            DepartmentDB departmentDB = new DepartmentDB();
            ViewBag.departmentlist = new SelectList(departmentDB.GetDepartmentListWithPermission(UserId), "DepartmentId", "DepartmentName_1").ToList();
            return View();
        }

        public ActionResult LoadSalaryListReport(string DepartmentIds, int PaycyleId)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = Server.MapPath("~/Content/Reports/SalaryReport/SalaryListReport.rpt");

            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));

            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@PayCycleId", PaycyleId));
            parameterList.Add(new SqlParameter("@DepartmentId", DepartmentIds));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_GetSalaryListByCycle");

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadSalaryListReportTemplate()
        {
            string reportFileName = Server.MapPath("~/Content/Reports/SalaryReport/SalaryListReportTemplate.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_EmployeeSalaryListReportData");

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }
        //-------------------------------------------------------------------------------------------------      
        public ActionResult EmployeeGeneralReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult LoadEmployeeGeneralReport(string EmpIds, string fromDate, string toDate, bool IsDisplayHeader, bool IsDisplayFooter, bool chkReportAr)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = "";
            if (chkReportAr == true)
            {
                reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/GeneralReportArabic.rpt");
            }
            else
            {
                reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/GeneralReportEnglish.rpt");
            }

            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            parameterFieldList.Add(new CustomParameterField("fromDate", fromDate));
            parameterFieldList.Add(new CustomParameterField("toDate", toDate));
            parameterFieldList.Add(new CustomParameterField("showHeader", IsDisplayHeader));
            parameterFieldList.Add(new CustomParameterField("showFooter", IsDisplayFooter));

            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIds", EmpIds));
            if (!string.IsNullOrEmpty(fromDate))
            {
                parameterList.Add(new SqlParameter("@StartDate", DateTime.ParseExact(fromDate, CommonHelper.SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture)));
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                parameterList.Add(new SqlParameter("@EndDate", DateTime.ParseExact(toDate, CommonHelper.SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture)));
            }
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_GetGeneralReportData");

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);

        }

        //-------------------------------------------------------------------------------------------------      
        public ActionResult StaffFollowUpReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult LoadStaffFollowUpReport(string EmpIds, string fromDate, string toDate, bool IsDisplayHeader, bool IsDisplayFooter, bool chkReportAr)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = "";
            if (chkReportAr == true)
            {
                reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/StaffFollowUpReportAr.rpt");
            }
            else
            {
                reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/StaffFollowUpReportEn.rpt");
            }

            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            parameterFieldList.Add(new CustomParameterField("fromDate", fromDate));
            parameterFieldList.Add(new CustomParameterField("toDate", toDate));
            parameterFieldList.Add(new CustomParameterField("showHeader", IsDisplayHeader));
            parameterFieldList.Add(new CustomParameterField("showFooter", IsDisplayFooter));
            SqlParameter StartDate = new SqlParameter();
            SqlParameter EndDate = new SqlParameter();
            if (!string.IsNullOrEmpty(fromDate))
            {
                StartDate = new SqlParameter("@StartDate", DateTime.ParseExact(fromDate, CommonHelper.SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture));
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                EndDate = new SqlParameter("@EndDate", DateTime.ParseExact(toDate, CommonHelper.SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture));
            }

            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIds", EmpIds));
            parameterList.Add(new SqlParameter("@mode", 1));
            parameterList.Add(StartDate);
            parameterList.Add(EndDate);

            DataTable EmpDetails = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_GetStaffFollowUpReportData");
            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIds", EmpIds));
            parameterList.Add(new SqlParameter("@mode", 2));
            parameterList.Add(StartDate);
            parameterList.Add(EndDate);

            DataTable LateDetails = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_GetStaffFollowUpReportData");
            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIds", EmpIds));
            parameterList.Add(new SqlParameter("@mode", 3));
            parameterList.Add(StartDate);
            parameterList.Add(EndDate);

            DataTable ShortLeaveDetails = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_GetStaffFollowUpReportData");
            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIds", EmpIds));
            parameterList.Add(new SqlParameter("@mode", 4));
            parameterList.Add(StartDate);
            parameterList.Add(EndDate);

            DataTable AbsentDetails = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_GetStaffFollowUpReportData");

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, EmpDetails, false);
            reportDocPersister.SubReports.Add("LateDetailsSubReport", LateDetails);
            reportDocPersister.SubReports.Add("ParmissionDetailsSubReport", ShortLeaveDetails);
            reportDocPersister.SubReports.Add("AbsentDetailsSubReport", AbsentDetails);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);

        }

        //------------------------------------------------------------------------------------------------- 
        public ActionResult EmployeeDetailedReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult LoadEmployeeDetailedReport(string EmpIds, string fromDate, string toDate, bool IsCertificate, bool IsDisplayHeader, bool IsDisplayFooter, bool chkReportAr)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = "";
            if (chkReportAr == true)
            {
                reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/EmployeeDetailedReportAr.rpt");
            }
            else
            {
                reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/EmployeeDetailedReportEn.rpt");
            }

            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            parameterFieldList.Add(new CustomParameterField("fromDate", fromDate));
            parameterFieldList.Add(new CustomParameterField("toDate", toDate));
            parameterFieldList.Add(new CustomParameterField("showHeader", IsDisplayHeader));
            parameterFieldList.Add(new CustomParameterField("showFooter", IsDisplayFooter));

            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIds", EmpIds));
            parameterList.Add(new SqlParameter("@IsCertificate", IsCertificate));
            if (!string.IsNullOrEmpty(fromDate))
            {
                parameterList.Add(new SqlParameter("@StartDate", DateTime.ParseExact(fromDate, SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture)));
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                parameterList.Add(new SqlParameter("@EndDate", DateTime.ParseExact(toDate, SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture)));
            }
            DataTable EmpDetails = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_STP_GetEmployeeDetailedReportData");
            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, EmpDetails, false);

            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);

        }

        //------------------------------------------------------------------------------------------------- 
        /****Salary Revision report***/
        public ActionResult EmployeePhotoListReport()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId).Where(x => x.EmployeeId > 0).OrderBy(x => x.FirstName), "EmployeeId", "FirstName").ToList();
            ViewBag.ActiveEmployee = emplist;
            List<string> selectedValue = new List<string>();
            if (Session["EmployeeIds"] != null)
            {
                selectedValue = Session["EmployeeIds"].ToString().Split(',').Select(x => x).ToList();
                Session["EmployeeIds"] = null;
            }
            List<SelectListItem> deplist = new SelectList(new DepartmentDB().GetAllDepartmentByCompanyId(objUserContextViewModel.CompanyId, objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1").ToList();
            deplist.Insert(0, (new SelectListItem { Text = "All Departments", Value = "" }));
            ViewBag.DepartmentList = deplist;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            PositionDB ObjPositionDB = new PositionDB();
            ObjSelectedList.Add(new SelectListItem { Text = "All Position", Value = "" });
            foreach (var m in ObjPositionDB.GetPositionList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PositionTitle, Value = m.PositionID.ToString() });
            }
            ViewBag.PositionList = ObjSelectedList;
            return View(selectedValue);
        }

        public ActionResult LoadEmployeePhotoCrReport(string EmpIDs, int? DeptID, int? PosID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            string reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/PhotoListReport.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmpIds", EmpIDs));
            parameterList.Add(new SqlParameter("@DeptID", DeptID));
            parameterList.Add(new SqlParameter("@PositionId", PosID));
            parameterList.Add(new SqlParameter("@UserId", UserId));
            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "Hr_Stp_GetEmployeeProfilePicData");
            byte[] btDefaulImage = CommonHelper.CommonHelper.GetBlankImage();
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["NoPhoto"]) == 0)
                {
                    bool isValid = CommonHelper.CommonHelper.IsValidImage((byte[])dr["Photo"]);
                    if (!isValid)
                    {
                        dr["Photo"] = btDefaulImage;
                    }
                }
            }

            DataRow[] selected = dt.Select("NoPhoto = 1");

            foreach (var item in selected)
            {
                item["Photo"] = btDefaulImage;
            }

            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }

        /*********************/
        //-------------------------------------------------------------------------------------------------      
        public ActionResult EmployeePermissionReport()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetEmployeeByActive(true, UserId), "EmployeeId", "FirstName").ToList();
            ViewBag.emplist = emplist;
            return View();
        }
        public ActionResult LoadEmployeePermissionReport(string EmpIds, bool onlyPermittedModule)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = Server.MapPath("~/Content/Reports/EmployeeReports/EmployeePermissionReport.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            DataHelper dataHelper = new DataHelper();
            List<SqlParameter> parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIDs", EmpIds));
            parameterList.Add(new SqlParameter("@Mode", 1));
            parameterList.Add(new SqlParameter("@OnlyShowPermitted", onlyPermittedModule));

            DataTable dt = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_GetPermissionDetailsByEmployeeIds");
            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIDs", EmpIds));
            parameterList.Add(new SqlParameter("@Mode", 2));
            parameterList.Add(new SqlParameter("@OnlyShowPermitted", onlyPermittedModule));
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            DataTable dt1 = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_GetPermissionDetailsByEmployeeIds");
            ds.Tables.Add(dt1);
            parameterList = new List<SqlParameter>();
            parameterList.Add(new SqlParameter("@EmployeeIDs", EmpIds));
            parameterList.Add(new SqlParameter("@Mode", 3));
            parameterList.Add(new SqlParameter("@OnlyShowPermitted", onlyPermittedModule));
            DataTable dt2 = dataHelper.ExcuteStoredProcedureWithParameter(parameterList, "HR_Stp_GetPermissionDetailsByEmployeeIds");
            ds.Tables.Add(dt2);
            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, ds, true);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------------------------------------------------------------------
        public ActionResult LoadAttendanceReportByWorkingDays(string pageId, int? depId, int? empId, int? sectionId, int? shiftId,
             bool inactiveOnly, bool? postedOnly, bool? detailReports, string fromDate, string toDate, string statusID, string OrderBy, string OrderType, int? month, int? year, int? superVisorId, bool? isListReport, bool? isEmployeePageWise, bool? isPercentageBasedOndays)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = "";
            reportFileName = Server.MapPath("~/Content/Reports/AttendanceReport/WorkingDaysReportCr.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            parameterFieldList.Add(new CustomParameterField("fromDate", fromDate));
            parameterFieldList.Add(new CustomParameterField("toDate", toDate));
            parameterFieldList.Add(new CustomParameterField("showHeader", false));
            parameterFieldList.Add(new CustomParameterField("showFooter", false));
            //2019-02-07 Included parameter to show report as a list report
            parameterFieldList.Add(new CustomParameterField("IsListView", isListReport));

            ReportingDB reprtDB = new ReportingDB();
            bool? activeStatus = null;
            if (inactiveOnly)
                activeStatus = false;
            else
            {
                activeStatus = true;
            }
            reprtDB.nonexcusedString = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Content/images/crossIcon.ico";
            reprtDB.excusedString = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Content/images/tick1.ico";
            List<AttendanceReportModel> listAttendanceReport = new List<AttendanceReportModel>();
            DataTable dt;
            listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 1, detailReports, superVisorId, empId, null, postedOnly, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null          

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.Employee> sortedEmployeeList = new List<HRMS.Entities.Employee>();
            sortedEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);

            listAttendanceReport = (from Emplist in listAttendanceReport
                                    join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                    select Emplist).ToList();

            dt = HRMS.Web.CommonHelper.ConvertListToDataTable.ToDataTable(listAttendanceReport);
            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);

        }
        //-------------------------------------------------------------------------------------------------
        public ActionResult LoadAttendanceReportByDateByEmployee(string pageId, int? depId, int? empId, int? sectionId, int? shiftId,
          bool inactiveOnly, bool? postedOnly, bool? detailReports, string fromDate, string toDate, string statusID, string OrderBy, string OrderType, int? month, int? year, int? superVisorId, bool? isListReport, bool? isEmployeePageWise, bool? isPercentageBasedOndays)
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            string reportFileName = "";
            reportFileName = Server.MapPath("~/Content/Reports/AttendanceReport/EmployeeDailyAttendanceCr.rpt");
            List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
            AddCommonParameters(parameterFieldList);
            parameterFieldList.Add(new CustomParameterField("NoOfDecimals", noOfDecimals));
            parameterFieldList.Add(new CustomParameterField("fromDate", fromDate));
            parameterFieldList.Add(new CustomParameterField("toDate", toDate));
            parameterFieldList.Add(new CustomParameterField("showHeader", false));
            parameterFieldList.Add(new CustomParameterField("showFooter", false));
            parameterFieldList.Add(new CustomParameterField("isEmployeePageWise", isEmployeePageWise));
            ReportingDB reprtDB = new ReportingDB();
            bool? activeStatus = null;
            if (inactiveOnly)
                activeStatus = false;
            else
            {
                activeStatus = true;
            }
            reprtDB.nonexcusedString = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Content/images/crossIcon.ico";
            reprtDB.excusedString = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + "/Content/images/tick1.ico";
            List<AttendanceReportModel> listAttendanceReport = new List<AttendanceReportModel>();
            DataTable dt;
            listAttendanceReport = reprtDB.GetAttendanceViewList(fromDate, toDate, pageId, 0, detailReports, superVisorId, empId, null, postedOnly, activeStatus, depId, sectionId, shiftId, null);// "2014-12-01", "2014-12-30", null          

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.Employee> sortedEmployeeList = new List<HRMS.Entities.Employee>();
            sortedEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);

            listAttendanceReport = (from Emplist in listAttendanceReport
                                    join UGA in sortedEmployeeList on Emplist.EmployeeID equals UGA.EmployeeId
                                    select Emplist).ToList();

            dt = HRMS.Web.CommonHelper.ConvertListToDataTable.ToDataTable(listAttendanceReport);
            ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(reportFileName, parameterFieldList, dt, false);
            return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);

        }
        //-------------------------------------------------------------------------------------------------
    }
}
