﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using System.Web.Script.Serialization;

namespace HRMS.Web.Controllers
{
    public class ShortLeaveController : BaseController
    {
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All Employees", Value = "0" });
            foreach (var m in objEmployeeList.Where(m => m.IsActive == 1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });

            }
            ViewBag.EmployeeList = ObjSelectedList;
            if (Session["EmployeeListID"] != null)
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }
            return View(employeeDetailsModelList);
        }
        public ActionResult GetShortLeaveList(int empid = 0, string FromDate = "", string ToDate = "")
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            //-------------Data Objects--------------------
            List<ShortLeaveModel> objShortLeaveModelList = new List<ShortLeaveModel>();
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objShortLeaveModelList = objShortLeaveDB.GetShortLeaveList(empid, objUserContextViewModel.UserId, FromDate, ToDate);

            var vList = new object();
            vList = new
            {
                aaData = (from item in objShortLeaveModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditShortLeave(" + item.ShortLeaveID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewShortLeave(" + item.ShortLeaveID.ToString() +","+ item.EmployeeID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteShortLeave(" + item.ShortLeaveID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeName = objEmployeeDB.GetEmployee(item.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(item.EmployeeID).LastName,
                              EmpID = item.EmployeeAlternativeID,
                              LeaveDate = item.LeaveDate,
                              LeaveTime = item.LeaveTime,
                              ReturnTime = item.ReturnTime,
                              ActualLeaveTime = item.ActualLeaveTime,
                              ActualReturnTime = item.ActualReturnTime,
                              LateMinutes = item.LateMinutes,
                              EarlyMinutes = item.EarlyMinutes,
                              Comments = item.Comments
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        public ActionResult AddShortLeave()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.Employee> objEmployeeList = new List<HRMS.Entities.Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objEmployeeList = objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Employee", Value = "" });
            foreach (var m in objEmployeeList.Where(m => m.IsActive == 1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.FullName, Value = m.EmployeeId.ToString() });

            }
            ViewBag.EmployeeList = ObjSelectedList;
            return View();
        }
        [HttpPost]
        public JsonResult AddShortLeave(ShortLeaveModel objShortLeaveModel)
        {
            OperationDetails op = new OperationDetails();
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            op = objShortLeaveDB.AddShortLeave(objShortLeaveModel);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditShortLeave(int id)
        {
            int empid = 0;
            if (Session["EmployeeListID"] != null)
            {
                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(empid);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            ShortLeaveModel objShortLeaveModel = new ShortLeaveModel();
            objShortLeaveModel = objShortLeaveDB.GetShortLeaveListById(id);
            return View(objShortLeaveModel);
        }
        [HttpPost]
        public JsonResult EditShortLeave(ShortLeaveModel objShortLeaveModel)
        {
            OperationDetails op = new OperationDetails();
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            op = objShortLeaveDB.EditShortLeave(objShortLeaveModel);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteShortLeave(int id)
        {
            OperationDetails op = new OperationDetails();
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            op = objShortLeaveDB.DeleteShortLeave(id);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewShortLeave(int id,int EmpId)
        {           
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            var employeeDetail = objEmployeeDB.GetEmployeedetailsById(EmpId);
            ViewBag.Employee = employeeDetail.FirstName_1 + " " + employeeDetail.SurName_1;
            ShortLeaveModel objShortLeaveModel = new ShortLeaveModel();
            objShortLeaveModel = objShortLeaveDB.GetShortLeaveListById(id);
            return View(objShortLeaveModel);
        }
        //***********************************************Export functionality****************************************************************************************
        public ActionResult ExportToExcel(int empId = 0, string startDate = "", string endDate = "")
        {
            byte[] content;
            string fileName = "ShortLeave" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            System.Data.DataSet ds = objShortLeaveDB.ShortLeaveExport(objUserContextViewModel.UserId, empId, startDate, endDate);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        public void ExportToPdf(int empId = 0, string startDate = "", string endDate = "")
        {
            string fileName = "ShortLeave" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ShortLeaveDB objShortLeaveDB = new ShortLeaveDB();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            System.Data.DataSet ds = objShortLeaveDB.ShortLeaveExport(objUserContextViewModel.UserId, empId, startDate, endDate);
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }
       
    }
}