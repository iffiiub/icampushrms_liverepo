﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.FormsDB;

namespace HRMS.Web.Controllers
{
    public class EmployeeBasedHierarchyController : BaseController
    {
        // GET: EmployeeBasedHierarchy
        public ActionResult Index()
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            EmployeeBasedHierarchyModel objEmployeeBasedHierarchyModel = new EmployeeBasedHierarchyModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeBasedHierarchyModel = getEmployeeHierarchy(null, null, null);

            List<Employee> empList =  objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList<Employee>();
            string employees = "";
            employees += "<option value=''>Select Employee Name</option>";
            foreach (var obj in empList)
            {
                employees += "<option value='" + obj.EmployeeId + "'>" + obj.FullName + "</option>";
            }
            ViewBag.EmployeeList = employees;
            ViewBag.Employee = new SelectList(empList, "EmployeeId", "FullName");
            //*** Naresh 2020-03-22 Company filtered added
            ViewBag.Company = GetCompanySelectList();
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            //*** Naresh 2020-03-30 Do not populate the departments, it will be populated based on company selection
            //objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            return View(objEmployeeBasedHierarchyModel);
        }

        public ActionResult LoadEmployeeHierchyGrid(int? employeeId, int? departmentId, int? companyId)
        {
            return PartialView("_EmployeeHierarchyGrid", getEmployeeHierarchy(employeeId, departmentId, companyId));
        }
        private EmployeeBasedHierarchyModel getEmployeeHierarchy(int? employeeId, int? departmentId, int? companyId)
        {
            EmployeeBasedHierarchyModel objEmployeeBasedHierarchyModel = new EmployeeBasedHierarchyModel();
            EmployeeBasedHierarchyDB objEmployeeBasedHierarchyDB = new EmployeeBasedHierarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeBasedHierarchyModel.WorkFlowGroupModelList = objEmployeeBasedHierarchyDB.GetWorkFlowGroupModelList(false);
            ViewBag.CountWorkFlowGroup = objEmployeeBasedHierarchyModel.WorkFlowGroupModelList.Count;
            objEmployeeBasedHierarchyModel.EmployeeHierarchyModelList = objEmployeeBasedHierarchyDB.GetEmployeeHierarchyModelList(objUserContextViewModel.UserId, employeeId, departmentId, companyId);
            return objEmployeeBasedHierarchyModel;
        }
        //*** Naresh 2020-03-24 Added export functionality
        public ActionResult ExportToExcel(int? companyId, int? employeeId, int? departmentId)
        {
            string fileName = "Emplpyee Based Hierarchy- " + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            var ds = GetEmployeeHierarchyData(companyId, employeeId, departmentId);
            var content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        public void ExportToPdf(int? companyId, int? employeeId, int? departmentId)
        {
            string fileName = "Employee Based Hierarchy- " + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            var ds = GetEmployeeHierarchyData(companyId, employeeId, departmentId);

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }
        private System.Data.DataSet GetEmployeeHierarchyData(int? companyId, int? employeeId, int? departmentId)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeBasedHierarchyDB objEmployeeBasedHierarchyDB = new EmployeeBasedHierarchyDB();
            if (employeeId.HasValue && employeeId.Value == 0) employeeId = null;
            if (departmentId.HasValue && departmentId.Value == 0) departmentId = null;
            if (companyId.HasValue && companyId.Value == 0) companyId = null;
            var ds = new System.Data.DataSet();
            var workFlowGroupModelList = objEmployeeBasedHierarchyDB.GetWorkFlowGroupModelList(false);

            var dtRecords = new System.Data.DataTable();
            dtRecords.Columns.Add(new System.Data.DataColumn("Organization", typeof(string)));
            dtRecords.Columns.Add(new System.Data.DataColumn("Emp ID", typeof(int)));
            dtRecords.Columns.Add(new System.Data.DataColumn("Employee Name", typeof(string)));
            dtRecords.Columns.Add(new System.Data.DataColumn("Department", typeof(string)));
            foreach (var workFlowItem in workFlowGroupModelList)
            {
                dtRecords.Columns.Add(new System.Data.DataColumn(workFlowItem.GroupName, typeof(string)));
            }
            var employeeHierarchyModelList = objEmployeeBasedHierarchyDB.GetEmployeeHierarchyModelList(objUserContextViewModel.UserId, employeeId, departmentId, companyId);
            if (employeeHierarchyModelList.Any())
            {
                var empList = employeeHierarchyModelList.Select(x => new { x.EmployeeID, x.EmployeeAlternativeID, x.EmployeeName, x.DepartmentName, x.CompanyName }).Distinct().ToList();
                foreach (var emp in empList)
                {
                    var row = dtRecords.NewRow();
                    foreach (var workFlowItem in workFlowGroupModelList)
                    {
                        var manager = employeeHierarchyModelList
                            .Where(x => x.EmployeeID == emp.EmployeeID && x.GroupID == workFlowItem.GroupID)
                            .Select(r => new { r.ManagerName, r.ManagerID })
                            .FirstOrDefault();
                        if (manager != null)
                            row[workFlowItem.GroupName] = manager.ManagerName;
                    }
                    row["Organization"] = emp.CompanyName;
                    row["Emp ID"] = emp.EmployeeID;
                    row["Employee Name"] = emp.EmployeeName;
                    row["Department"] = emp.DepartmentName;
                    dtRecords.Rows.Add(row);
                }
            }
            ds.Tables.Add(dtRecords);
            return ds;
        }
        
        public ActionResult SaveMultipleEmployees(List<EmployeeHierarchyModel> employeeHeirarchyList)
        {
            EmployeeBasedHierarchyDB objEmployeeBasedHierarchyDB = new EmployeeBasedHierarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(objEmployeeBasedHierarchyDB.UpdateEmployeeHierarchy(employeeHeirarchyList, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteEmployeeHierarchy(int employeeId)
        {
            EmployeeBasedHierarchyDB objEmployeeBasedHierarchyDB = new EmployeeBasedHierarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(objEmployeeBasedHierarchyDB.DeleteEmployeeHierarchy(employeeId, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult IsPendingApprovalExists(int employeeID, int groupID, int managerID)
        {
            return Json(new EmployeeBasedHierarchyDB().IsPendingApprovalExists(employeeID, groupID, managerID), JsonRequestBehavior.AllowGet);
        }

    }
}