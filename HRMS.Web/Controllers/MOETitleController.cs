﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using System.Data;

namespace HRMS.Web.Controllers
{
    public class MOETitleController: BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetMOETitleList()
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
           
            //-------------Data Objects--------------------
            List<HRMS.Entities.MOETitleModel> objMOETitleList = new List<HRMS.Entities.MOETitleModel>();
            MOETitleDB objMOETitleDB = new MOETitleDB();
            objMOETitleList = objMOETitleDB.GetAllMOETitle();
            
            var vList = new object();
            vList = new
            {
                aaData = (from item in objMOETitleList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.MOETitleID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.MOETitleID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              MOETitleID = item.MOETitleID,
                              MOETitleName_1 = item.MOETitleName_1,
                              MOETitleName_2 = item.MOETitleName_2,
                              MOETitleName_3 = item.MOETitleName_3,
                              MOETitleShortName_1 = item.MOETitleShortName_1,
                              MOETitleShortName_2 = item.MOETitleShortName_2,
                              MOETitleShortName_3 = item.MOETitleShortName_3,
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            HRMS.Entities.MOETitleModel objMOETitle = new HRMS.Entities.MOETitleModel();
            MOETitleDB objMOETitleDB = new MOETitleDB();            

            if (id != 0)
            {
                objMOETitle = objMOETitleDB.GetMOETitle(id);
            }

            return View(objMOETitle);
        }

        public JsonResult Save(MOETitleModel objMOETitle)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                MOETitleDB objMOETitleDB = new MOETitleDB();

                if (objMOETitle.MOETitleID == 0)
                {
                    objMOETitleDB.AddUpdateDeleteMOETitle(objMOETitle, 1);

                }
                else
                {
                    objMOETitleDB.AddUpdateDeleteMOETitle(objMOETitle, 2);
                }

                //return Redirect("Index");
                if (objMOETitle.MOETitleID == 0)
                {
                    result = "success";
                    resultMessage = "MOE Title Added Successfully.";
                }
                else if (objMOETitle.MOETitleID != 0)
                {
                    result = "success";
                    resultMessage = "MOE Title Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while adding MOE Title.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            MOETitleDB objMOETitleDB = new MOETitleDB();
            HRMS.Entities.MOETitleModel objMOETitle = new HRMS.Entities.MOETitleModel();
            objMOETitle.MOETitleID = int.Parse(id);
            objMOETitleDB.AddUpdateDeleteMOETitle(objMOETitle, 3);
            //return Redirect("Index");
            //return Json(0, JsonRequestBehavior.AllowGet);
            return Json(new { result = "success", resultMessage = "MOE Title Deleted Successfully." }, JsonRequestBehavior.AllowGet);
        }

       
        

        public ActionResult ExportToExcel()
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "MOETitle" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            MOETitleDB objMOETitleDB = new MOETitleDB();
            DataSet  ds = objMOETitleDB.GetAllMOETitleDatasSet();

            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            string fileName = "MOETitle" + DateTime.Now.ToString("ddMMyyyy") + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            MOETitleDB objMOETitleDB = new MOETitleDB();
            DataSet ds = objMOETitleDB.GetAllMOETitleDatasSet();

            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();

        }
    }
}