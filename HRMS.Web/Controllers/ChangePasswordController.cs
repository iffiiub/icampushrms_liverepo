﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HRMS.DataAccess;
using HRMS.Entities;

namespace HRMS.Web.Controllers
{
    public class ChangePasswordController: Controller
    {
        //
        // GET: /ChangePassword/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(HRMS.Entities.ChangePassword objChangePassword)
        {
            if (ModelState.IsValid)
            {
                if ((string)Session["defaultPassword"] == objChangePassword.oldPassword)
                {
                    if (objChangePassword.oldPassword == objChangePassword.newPassword)
                    {
                        ViewData["ErrorMessage"] = "Your Old and new password can't be same";
                    }
                    else
                    {
                        EmployeeCredentialModel objEmployeeCredentialModel = new EmployeeCredentialModel();
                        EmployeeCredentialDB objEmployeeCredentialDB = new EmployeeCredentialDB();

                        objEmployeeCredentialModel.EmployeeId = Convert.ToInt16(Session["EmployeeId"].ToString());                        
                        objEmployeeCredentialModel.Password = objChangePassword.newPassword;// new EncryptDecrypt().EncryptPassword(objChangePassword.newPassword);
                        objEmployeeCredentialModel.ModifiedBy = Convert.ToInt16(Session["EmployeeId"].ToString());
                        if (objEmployeeCredentialDB.UpdateEmployeePassword(objEmployeeCredentialModel).StartsWith("Success"))
                        {
                            Session.Remove("defaultPassword");
                            return RedirectToAction("Index", "Employee");
                        }
                        else
                        {
                            ViewData["ErrorMessage"] = "Your password change was unsuccessful.";
                        }
                    }
                }
                else
                {
                    ViewData["ErrorMessage"] = "Your Old password is incorrect";
                }
            }
            return View(objChangePassword);
        }
    }
}