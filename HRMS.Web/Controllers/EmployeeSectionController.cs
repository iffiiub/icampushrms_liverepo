﻿using HRMS.DataAccess;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class EmployeeSectionController: BaseController
    {
        // GET: /EmployeeSection/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployeeSectionList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "EmployeeSectionID", int iSortCol_0 = 0)
        {

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<EmployeeSectionModel> objEmployeeSectionList = new List<EmployeeSectionModel>();
            EmployeeSectionDB objEmployeeDB = new EmployeeSectionDB();
            objEmployeeSectionList = objEmployeeDB.GetEmployeeSectionList(pageNumber, numberOfRecords, sortColumn, sortOrder, out totalCount);


            //---------------------------------------------



            var vList = new object();

            vList = new
            {
                aaData = (from item in objEmployeeSectionList
                          select new
                          {

                              Actions = "<a class='btn btn-primary' onclick='EditEmployeeSection(" + item.EmployeeSectionID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i>Edit</a> <a class='btn btn-primary' onclick='DeleteEmployeeSection(" + item.EmployeeSectionID.ToString() + ")' title='Delete' ><i class='fa fa-pencil'></i> Delete</a>",
                              EmployeeSectionID = item.EmployeeSectionID,
                              EmployeeSectionName_1 = item.EmployeeSectionName_1,
                              EmployeeSectionName_2 = item.EmployeeSectionName_2,
                              EmployeeSectionName_3 = item.EmployeeSectionName_3,
                              Head = item.Head,
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEmployeeSection(EmployeeSectionModel employeeSection)
        {
            List<EmployeeSectionModel> employeeSectionModelList = new List<EmployeeSectionModel>();
            EmployeeSectionDB employeeSectionDB = new EmployeeSectionDB();
            OperationDetails operationDetails = new OperationDetails();
            string mode = "";
            if (employeeSection.EmployeeSectionID == 0)
            {
                operationDetails = employeeSectionDB.EmployeeSectionCrud(employeeSection, 1);
                mode = "Insert";
            }
            else
            {
                operationDetails = employeeSectionDB.EmployeeSectionCrud(employeeSection, 2);
                mode = "Update";
            }

            if (operationDetails.Message == "success")
            {
                if (mode == "Insert")
                    return Json(new { Id = operationDetails.InsertedRowId, Value = employeeSection.EmployeeSectionName_1, mode = mode, result = "success", resultMessage = "Employee Section Added Successfully." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Id = operationDetails.InsertedRowId, Value = employeeSection.EmployeeSectionName_1, mode = mode, result = "success", resultMessage = "Employee Section Updated Successfully." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Error while adding Employee Section details" }, JsonRequestBehavior.AllowGet);
            }


        }


        public ActionResult EditEmployeeSection(int id)
        {
            EmployeeSectionDB employeeSectionDB = new EmployeeSectionDB();
            EmployeeSectionModel employeeSectionModel = new EmployeeSectionModel();
            if (id != 0)
            {
                employeeSectionModel = employeeSectionDB.GetEmployeeSectionById(id);
            }

            return PartialView("_Edit", employeeSectionModel);
        }

        public ActionResult DeleteEmployeeSection(int Id)
        {
            //string result = "";
            //string resultMessage = "";
            EmployeeSectionModel employeeSectionModel = new EmployeeSectionModel();
            OperationDetails operationDetails = new OperationDetails();
            EmployeeSectionDB employeeSectionDB = new EmployeeSectionDB();
            employeeSectionModel.EmployeeSectionID = Id;
            operationDetails = employeeSectionDB.EmployeeSectionCrud(employeeSectionModel, 3);

            if (operationDetails.Message == "success")
            {
                return Json(new { result = "success", resultMessage = "Employee Section Deleted Successfully." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Error while deleting Employee Section details" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}