﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using System.Data;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using iTextSharp.text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Globalization;
using iTextSharp.text.pdf;
using System.IO;

namespace HRMS.Web.Controllers
{
    public class DeductionController : BaseController
    {
        //
        // GET: /Deduction/
        public ActionResult Index(int Id = 0)
        {
            Id = CommonHelper.CommonHelper.GetEmployeeId(Id);
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            if (Id != 0)
            {
                Session["EmployeeListID"] = Id.ToString();
                int EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                ViewBag.EmployeeIDNo = Id.ToString();
                ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();

            }
            else
            {
                ViewBag.EmployeeIDNo = 0;
                if (Session["EmployeeListID"] != null)
                {
                    int EmployID = Convert.ToInt32(Session["EmployeeListID"]);
                    objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = EmployID.ToString();
                    ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();
                }

            }
            return View(objEmployee);
        }

        public ActionResult GetPayDeductionList(int EmployeeID = 0)
        {
            if (Session["EmployeeListID"] != null)
            {
                EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());

            }
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<PayDeductionModel> payDeductionModelList = new List<PayDeductionModel>();
            DeductionDB deductionDB = new DeductionDB();
            payDeductionModelList = deductionDB.GetAllPayDeductionList(EmployeeID);
            List<PayDeductionModel> objpayDeductionModelList = new List<PayDeductionModel>();
            string Actions = "<a  class='btn btn-success btn-rounded btn-condensed btn-sm custom-margin' onclick='AddPayDeduction1(PayDeductionID)' title='Edit' ><i class='fa fa-pencil'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='EditDeductionDetails(PayDeductionID, AmountLeft)' title='Edit Installment Plan' ><i class='fa fa-list'></i></a><a  class='btn btn-danger btn-rounded btn-condensed btn-sm custom-margin' onclick='DeleteDeduction(PayDeductionID)' title='Delete' ><i class='fa fa-times'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDeductionDetails(PayDeductionID)' title='View' ><i class='fa fa-eye'></i> </a><a  class='btn btn-primary btn-rounded btn-condensed btn-sm custom-margin' onclick='ExportDeductionDetailToPdf(PayDeductionID)' title='Download Pay Deduction Detail' ><i class='fa fa-download'></i> </a>";
            string IsFamilyBalanceActions = "<a  class='btn btn-success btn-rounded btn-condensed btn-sm custom-margin' onclick='AddPayDeduction1(PayDeductionID)' title='Edit' ><i class='fa fa-pencil'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='EditDeductionDetails(PayDeductionID, AmountLeft)' title='Edit Installment Plan' ><i class='fa fa-list'></i></a><a  class='btn btn-danger btn-rounded btn-condensed btn-sm custom-margin' onclick='DeleteDeduction(PayDeductionID)' title='Delete' ><i class='fa fa-times'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDeductionDetails(PayDeductionID)' title='View' ><i class='fa fa-eye'></i> </a><a  class='btn btn-primary btn-rounded btn-condensed btn-sm custom-margin' onclick='ExportDeductionDetailToPdf(PayDeductionID)' title='Download Pay Deduction Detail' ><i class='fa fa-download'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='EditFOSTBalanceDetails(PayDeductionID)' title='Edit Family Outstanding Balance' ><i class='fa fa-list'></i></a>";
            string NonEditedActins = "<a disabled class='btn btn-success btn-rounded btn-condensed btn-sm custom-margin' onclick='AddPayDeduction1(PayDeductionID)' title='Edit' ><i class='fa fa-pencil'></i> </a><a disabled class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='EditDeductionDetails(PayDeductionID, AmountLeft)' title='Edit Installment Plan' ><i class='fa fa-list'></i></a><a disabled class='btn btn-danger btn-rounded btn-condensed btn-sm custom-margin' onclick='DeleteDeduction(PayDeductionID)' title='Delete' ><i class='fa fa-times'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDeductionDetails(PayDeductionID)' title='View' ><i class='fa fa-eye'></i> </a><a  class='btn btn-primary btn-rounded btn-condensed btn-sm custom-margin' onclick='ExportDeductionDetailToPdf(PayDeductionID)' title='Download Pay Deduction Detail' ><i class='fa fa-download'></i></a>";
            string NonEditedActinsFamilyBal = "<a disabled class='btn btn-success btn-rounded btn-condensed btn-sm custom-margin' onclick='AddPayDeduction1(PayDeductionID)' title='Edit' ><i class='fa fa-pencil'></i> </a><a disabled class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='EditDeductionDetails(PayDeductionID, AmountLeft)' title='Edit Installment Plan' ><i class='fa fa-list'></i></a><a disabled class='btn btn-danger btn-rounded btn-condensed btn-sm custom-margin' onclick='DeleteDeduction(PayDeductionID)' title='Delete' ><i class='fa fa-times'></i> </a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='ViewDeductionDetails(PayDeductionID)' title='View' ><i class='fa fa-eye'></i> </a><a  class='btn btn-primary btn-rounded btn-condensed btn-sm custom-margin' onclick='ExportDeductionDetailToPdf(PayDeductionID)' title='Download Pay Deduction Detail' ><i class='fa fa-download'></i></a><a  class='btn btn-default btn-rounded btn-condensed btn-sm custom-margin' onclick='EditFOSTBalanceDetails(PayDeductionID)' title='Edit Family Outstanding Balance' ><i class='fa fa-list'></i></a>";
            var vList = new object();
            vList = new
            {
                aaData = (from item in payDeductionModelList
                          select new
                          {
                              Actions = item.DeductionTypeID == 0 ? "" : (item.IsProcessed == true ? (item.IsFamilyBalanceDeduction == true ? NonEditedActinsFamilyBal.Replace("PayDeductionID", item.PayDeductionID.ToString()).Replace("AmountLeft", item.AmountLeft.ToString())
                                        : NonEditedActins.Replace("PayDeductionID", item.PayDeductionID.ToString()).Replace("AmountLeft", item.AmountLeft.ToString()))
                                        :(item.IsFamilyBalanceDeduction == true ? IsFamilyBalanceActions.Replace("PayDeductionID", item.PayDeductionID.ToString()).Replace("AmountLeft", item.AmountLeft.ToString()) :
                                        Actions.Replace("PayDeductionID", item.PayDeductionID.ToString()).Replace("AmountLeft", item.AmountLeft.ToString()))),
                              PayDeductionID = item.PayDeductionID,
                              DeductionTypeID = item.DeductionTypeID,
                              EmployeeID = item.EmployeeID,
                              RefNumber = "<div style='width:100px; word-wrap:break-word;'>" + item.RefNumber + "</div>",
                              DeductionType = item.DeductionType,
                              EffectiveDate = item.EffectiveDate,
                              GenerateDate = item.GenerateDate,
                              PaidCycle = item.PaidCycle,
                              IsInstallment = item.IsInstallment == true ? "<input type='checkbox' class='checkbox' checked='true' id='" + item.PayDeductionID + "' onclick='updateInstallmentStatus(" + item.PayDeductionID + ")' />" : "<input type='checkbox' class='checkbox'  id='" + item.PayDeductionID + "'  onclick='updateInstallmentStatus(" + item.PayDeductionID + ")'/>",
                              Amount = item.Amount.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              Installment = item.Installment.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              TransactionDate = item.TransactionDate,
                              PvId = item.PvId
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult EditPayDeduction(int id, int EmployeeId)
        {
            PayDeductionModel payDeductionModel = new PayDeductionModel();
            DeductionDB deductionDB = new DeductionDB();
            payDeductionModel.EmployeeID = EmployeeId;
            List<PayDeductionTypeModel> lstPayDedType = new List<PayDeductionTypeModel>();
            if (id == 0)
            {
                payDeductionModel.PaidCycle = 1;
            }
            else
            {
                payDeductionModel = deductionDB.GetPayDeductionByID(id);
            }
            int countPayDedType = 0;
            countPayDedType = deductionDB.GetAllPayDeductionType().Where(x => x.DeductionTypeID == payDeductionModel.DeductionTypeID).Where(x => x.IsFamilyBalanceDeduction == true).ToList().Count();
            if (countPayDedType > 0)
            {
                lstPayDedType = deductionDB.GetAllPayDeductionType().Where(x => x.DeductionTypeID > 0).Where(x => x.IsFamilyBalanceDeduction == true).ToList();
            }
            else
            {
                lstPayDedType = deductionDB.GetAllPayDeductionType().Where(x => x.DeductionTypeID > 0).Where(x => x.IsFamilyBalanceDeduction == false).ToList();
            }
            ViewBag.DeductionType = new SelectList(lstPayDedType, "DeductionTypeID", "DeductionTypeName_1");
            EmployeeDB objEmployeeDB = new EmployeeDB();
            Employee objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployeeId);
            ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + " " + objEmployee.employeeDetailsModel.SurName_1;
            ViewBag.Positon = objEmployee.employmentInformation.PositionName;
            ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
            ViewBag.EmployeeIDNo = EmployeeId;

            return View("AddPayDeduction", payDeductionModel);
        }

        public ActionResult AddMultipleDeduction()
        {
            DeductionDB deductionDB = new DeductionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.DeductionTypeDdl = new SelectList(deductionDB.GetAllPayDeductionType().Where(x => x.DeductionTypeID > 0 && x.IsFamilyBalanceDeduction == false), "DeductionTypeID", "DeductionTypeName_1");
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.employeeDdl = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1 && x.CompanyId == objUserContextViewModel.CompanyId).ToList(), "EmployeeID", "FullName");
            return View();
        }

        public ActionResult AddPayDeduction(PayDeductionModel payDeductionModel, double OldAmount, int OldPaidCycle)
        {
            DeductionDB deductionDB = new DeductionDB();
            PayrollDB payrollDb = new PayrollDB();
            bool IsDeductionSendForApproval = false;
            string resultMessage = string.Empty;
            int cycle = 0;
            if (payDeductionModel.EmployeeID == 0)
            {
                return Json(new OperationDetails { Success = false, Message = "Please select Employee first" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<PayDeductionModel> payDeductionModelList = new List<PayDeductionModel>();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                OperationDetails operationDetails = new OperationDetails();
                PayDeductionDetailModel payDeductionDetailModel;
                PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
                PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
                objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 3).FirstOrDefault();
                objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
                bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;

                if (IsConfirmRunPayroll && IsSendForConfirmation)
                {
                    IsDeductionSendForApproval = true;
                }
                else
                {
                    IsDeductionSendForApproval = false;
                }
                cycle = payDeductionModel.PaidCycle;
                CommonHelper.CommonHelper.GetAmountFormat();
                if (payDeductionModel.PaidCycle > 0)
                {
                    payDeductionModel.IsInstallment = true;
                }

                if (payDeductionModel.PayDeductionID == 0)//Add
                {
                    if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                    {
                        operationDetails = deductionDB.PayDeductionCrud(payDeductionModel, 1);
                        operationDetails.CssClass = "success";
                        //Add details
                        if (operationDetails.Success)
                        {
                            for (int i = 0; i < cycle; i++)
                            {
                                payDeductionDetailModel = new PayDeductionDetailModel();
                                payDeductionDetailModel.PayDeductionID = operationDetails.InsertedRowId;
                                if (i == 0)
                                {
                                    payDeductionDetailModel.DeductionDate = payDeductionModel.EffectiveDate;
                                }
                                else
                                {
                                    DateTime dt = ((DateTime)CommonDB.SetCulturedDate(payDeductionModel.EffectiveDate)).AddMonths(i);//changed AddMonths to AddDays
                                    payDeductionDetailModel.DeductionDate = CommonDB.GetFormattedDate_DDMMYYYY(dt.ToString());
                                }

                                payDeductionDetailModel.Amount = Math.Round(payDeductionModel.Amount / cycle, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
                                payDeductionDetailModel.Comments = payDeductionModel.RefNumber;
                                payDeductionDetailModel.IsActive = true;
                                deductionDB.PayDeductionDetailsCrud(payDeductionDetailModel, 1);
                            }
                        }
                    }
                    else
                    {
                        payDeductionDetailModel = new PayDeductionDetailModel()
                        {
                            DeductionDate = "",
                            Amount = Math.Round(payDeductionModel.Amount / cycle, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero),
                            PayDeductionDetailID = 0,
                            PayDeductionID = 0,
                            IsActive = false,
                            Comments = ""
                        };
                        DataTable dt = new DataTable();
                        operationDetails = deductionDB.SavePayrollConfirmationRequestForDeduction(payDeductionModel, payDeductionDetailModel, dt, objUserContextViewModel.UserId, 1);
                        operationDetails.CssClass = "Request";
                    }
                }
                else//Edit
                {
                    if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                    {
                        operationDetails = deductionDB.PayDeductionCrud(payDeductionModel, 2);
                        //Check if amount or pay cycle is changed, if yes give message to use to recalculate
                        operationDetails.CssClass = "success";
                        if (OldPaidCycle > 1)
                        {
                            if (operationDetails.Message == "Pay Addition Updated Successfully.")
                            {
                                if (payDeductionModel.Amount != OldAmount || cycle != OldPaidCycle)
                                {
                                    operationDetails.Message = "Please re-calculate the installments amount manually.";
                                    operationDetails.CssClass = "warning";
                                }
                            }

                        }
                    }
                    else
                    {

                        payDeductionDetailModel = new PayDeductionDetailModel()
                        {
                            DeductionDate = "",
                            Amount = 0,
                            PayDeductionDetailID = 0,
                            PayDeductionID = 0,
                            IsActive = false,
                            Comments = ""
                        };

                        DataTable multipleDeductionDetail = new DataTable();
                        multipleDeductionDetail.Columns.Add("PayDeductionDetailID", typeof(int));
                        multipleDeductionDetail.Columns.Add("PayDeductionId", typeof(int));
                        multipleDeductionDetail.Columns.Add("DeductionDate", typeof(DateTime));
                        multipleDeductionDetail.Columns.Add("Amount", typeof(decimal));
                        multipleDeductionDetail.Columns.Add("Comments", typeof(string));
                        multipleDeductionDetail.Columns.Add("Active", typeof(bool));
                        multipleDeductionDetail.Columns.Add("employeeId", typeof(int));
                        multipleDeductionDetail.Columns.Add("IsProcessed", typeof(bool));
                        multipleDeductionDetail.Columns.Add("IsWaived", typeof(int));
                        multipleDeductionDetail.Columns.Add("rowNum", typeof(int));

                        List<PayDeductionDetailModel> lstPayDeductionDetail = new List<PayDeductionDetailModel>();
                        lstPayDeductionDetail = deductionDB.GetAllPayDeductionDetailsList(payDeductionModel.PayDeductionID).Where(m => m.IsProcessed == true).ToList();
                        double TotalAmountPaid = lstPayDeductionDetail.Sum(m => m.Amount);
                        double RemainingAmount = payDeductionModel.Amount - TotalAmountPaid;
                        int RemainingCycle = cycle - lstPayDeductionDetail.Count;
                        double SplitAmount = Math.Round(RemainingAmount / RemainingCycle, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
                        PayDeductionDetailModel lastPaidDeduction = lstPayDeductionDetail.OrderByDescending(m => m.PayDeductionDetailID).FirstOrDefault();
                        DateTime lastDeductionDate = new DateTime();
                        if (lastPaidDeduction != null)
                        {
                            lastDeductionDate = DateTime.ParseExact(lastPaidDeduction.DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddMonths(1);
                        }
                        else
                        {
                            lastDeductionDate = DateTime.ParseExact(payDeductionModel.EffectiveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        int k = 0;
                        foreach (var item in lstPayDeductionDetail)
                        {
                            multipleDeductionDetail.Rows.Add(
                             0,
                             payDeductionModel.PayDeductionID,
                             DateTime.ParseExact(item.DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                             item.Amount,
                             payDeductionModel.Comments,
                             item.IsActive,
                             payDeductionModel.EmployeeID,
                             false,
                             item.IsWaived,
                             k++
                             );
                        }
                        for (int i = 0; i < RemainingCycle; i++)
                        {
                            DateTime dt = new DateTime();
                            if (i == 0)
                            {
                                dt = lastDeductionDate;
                            }
                            else
                            {
                                dt = lastDeductionDate.AddMonths(i);
                            }

                            multipleDeductionDetail.Rows.Add(
                                0,
                                payDeductionModel.PayDeductionID,
                                dt,
                                SplitAmount,
                                payDeductionModel.Comments,
                                true,
                                payDeductionModel.EmployeeID,
                                false,
                                null,
                                i
                                );
                        }
                        operationDetails = deductionDB.SavePayrollConfirmationRequestForDeduction(payDeductionModel, payDeductionDetailModel, multipleDeductionDetail, objUserContextViewModel.UserId, 2);
                        operationDetails.CssClass = "Request";
                    }
                }
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        private List<PayDeductionModel> CalculateDeductionWithoutInstallment(PayDeductionModel payDeductionModel)
        {
            List<PayDeductionModel> payDeductionModelList = new List<PayDeductionModel>();
            PayDeductionModel objPayDeductionModel;
            if (payDeductionModel.PaidCycle != 0)
            {
                for (int i = 0; i < payDeductionModel.PaidCycle; i++)
                {
                    objPayDeductionModel = new PayDeductionModel();
                    objPayDeductionModel.DeductionTypeID = payDeductionModel.DeductionTypeID;
                    objPayDeductionModel.EffectiveDate = payDeductionModel.EffectiveDate;
                    objPayDeductionModel.PaidCycle = payDeductionModel.PaidCycle;
                    objPayDeductionModel.IsInstallment = payDeductionModel.IsInstallment;
                    objPayDeductionModel.Amount = payDeductionModel.Amount;
                    objPayDeductionModel.Installment = payDeductionModel.Installment;
                    objPayDeductionModel.Comments = payDeductionModel.Comments;
                    objPayDeductionModel.EmployeeID = payDeductionModel.EmployeeID;
                    objPayDeductionModel.RefNumber = payDeductionModel.RefNumber;
                    objPayDeductionModel.RefNumber = payDeductionModel.TransactionDate;
                    objPayDeductionModel.PvId = payDeductionModel.PvId;
                }
            }

            return payDeductionModelList;
        }

        private List<PayDeductionModel> CalculateDeductionWithInstallment(PayDeductionModel payDeductionModel)
        {
            List<PayDeductionModel> payDeductionModelList = new List<PayDeductionModel>();

            return payDeductionModelList;
        }

        [HttpPost]
        public ActionResult CalculateDays(int Id, string days, string effectiveDate)
        {
            string result = "";
            string resultMessage = "";
            string StrAmount = "";
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            DateTime currentDate = new DateTime();
            int NoOfCycleDays = 30;
            if (days == "0" || string.IsNullOrEmpty(days))
            {
                result = "error";
                resultMessage = "Please enter number of days greate then zero(0)";
                StrAmount = 0.ToString(AmountFormat);
            }
            else
            {
                double Amount = CalculateAmountForDays(Id, days, effectiveDate, 1, out currentDate, out NoOfCycleDays);
                if (Amount != 0)
                {
                    CommonHelper.CommonHelper.GetAmountFormat();
                    result = "success";
                    resultMessage = "";
                    StrAmount = Amount.ToString(AmountFormat);
                }
                else
                {
                    result = "error";
                    resultMessage = "Sum of deductable Salary allowances are zero (0).Please update salary allowances.";
                    StrAmount = 0.ToString(AmountFormat);
                }
                if (NoOfCycleDays == 0)
                {
                    result = "error";
                    resultMessage = "Please create cycle for " + currentDate.ToString("MMM") + " Month";
                    StrAmount = 0.ToString(AmountFormat);
                }
            }
            return Json(new { result = result, resultMessage = resultMessage, Amount = StrAmount }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllEmployees()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId);

            return PartialView("_Employees", employeeDetailsModelList);
        }

        public ActionResult EditDeductionType(int id)
        {
            DeductionDB dedectionDB = new DeductionDB();
            PayDeductionTypeModel payDeductionTypeModel = new PayDeductionTypeModel();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "All", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            if (id > -1)
            {
                payDeductionTypeModel = dedectionDB.GetDeductionById(id);
            }
            else
            {
                payDeductionTypeModel.DeductionTypeID = id;
            }

            return PartialView("_DeductionType", payDeductionTypeModel);
        }

        public ActionResult AddDeductiontype(PayDeductionTypeModel payDeductionTypeModel)
        {
            List<PayDeductionTypeModel> payDeductionTypeModelList = new List<PayDeductionTypeModel>();
            DeductionDB deductionDB = new DeductionDB();
            OperationDetails operationDetails = new OperationDetails();
            string mode = "";
            payDeductionTypeModel.IsActive = true;
            if (payDeductionTypeModel.DeductionTypeID == -1)
            {
                operationDetails = deductionDB.PayDeductionTypeCrud(payDeductionTypeModel, 1);
                mode = "Insert";
            }
            else
            {
                operationDetails = deductionDB.PayDeductionTypeCrud(payDeductionTypeModel, 2);
                mode = "Update";
            }

            if (operationDetails.Message == "success")
            {
                if (mode == "Insert")
                    return Json(new { Id = operationDetails.InsertedRowId, Value = payDeductionTypeModel.DeductionTypeName_1, mode = mode, result = "success", resultMessage = "Deduction Type Added Successfully." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Id = operationDetails.InsertedRowId, Value = payDeductionTypeModel.DeductionTypeName_1, mode = mode, result = "success", resultMessage = "Deduction Type Updated Successfully." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Error while adding deduction type details" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllDeductionTypes()
        {
            DeductionDB deductionDB = new DeductionDB();
            List<PayDeductionTypeModel> payDeductionTypeModelList = new List<PayDeductionTypeModel>();
            payDeductionTypeModelList = deductionDB.GetAllPayDeductionType();
            return Json(payDeductionTypeModelList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult EmployeeSearch(string search)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<EmployeeDetailsModel> employeeDetailsModelList = new List<EmployeeDetailsModel>();
            EmployeeDB employeeDB = new EmployeeDB();
            employeeDetailsModelList = employeeDB.GetAllEmployeeByCompanyIdandEmpName(objUserContextViewModel.CompanyId, search);

            return Json(employeeDetailsModelList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DeletePayDeduction(int Id)
        {
            PayDeductionModel payDeductionModel;
            OperationDetails operationDetails;
            DeductionDB deductionDB = new DeductionDB();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 3).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                payDeductionModel = new PayDeductionModel();
                payDeductionModel.PayDeductionID = Id;
                operationDetails = deductionDB.PayDeductionCrud(payDeductionModel, 3);
                operationDetails.Success = true;
                operationDetails.CssClass = "DirectDelete";
                operationDetails.Message = "Pay Deduction Deleted Successfully";
            }
            else
            {
                PayDeductionDetailModel payDeductionDetailModel = new PayDeductionDetailModel()
                {
                    DeductionDate = "",
                    Amount = 0,
                    PayDeductionDetailID = 0,
                    PayDeductionID = 0,
                    IsActive = false,
                    Comments = ""
                };
                payDeductionModel = deductionDB.GetPayDeductionByID(Id);
                DataTable dt = new DataTable();
                operationDetails = deductionDB.SavePayrollConfirmationRequestForDeduction(payDeductionModel, payDeductionDetailModel, dt, objUserContextViewModel.UserId, 3);
                operationDetails.CssClass = "Request";
                if (operationDetails.Success)
                    operationDetails.Message = "Pay deduction modification request send for confirmation.";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DeductionTypes()
        {
            return View("DeductionTypeList");
        }

        public ActionResult GetDeductionTypesList()
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<PayDeductionTypeModel> payDeductionTypeModelList = new List<PayDeductionTypeModel>();
            DeductionDB deductionDB = new DeductionDB();
            SocialSecurityDB objSocialSecurityDB = new SocialSecurityDB();
            //payDeductionTypeModelList = deductionDB.GetAllPayDeductionType();
            //totalCount = payDeductionTypeModelList.Count;
            DeductionSetting deductionSetting = new DeductionSetting();
            deductionSetting = deductionDB.GetDeductionSetting();
            List<int> ExcludedTypes = new List<int>();
            ExcludedTypes.Add(deductionSetting.AbsentDeductionTypeId);
            ExcludedTypes.Add(deductionSetting.LateDeductionTypeId);
            ExcludedTypes.Add(deductionSetting.EarlyDeductionTypeId);
            ExcludedTypes.Add(deductionSetting.GeneralDeductionTypeId);
            ExcludedTypes.Add(deductionSetting.TaxPayDeductionTypeId);
            ExcludedTypes.Add(deductionSetting.FamilyBalanceDeductionTypeID);
            ExcludedTypes.Add(objSocialSecurityDB.GetDeduTypeIdForSS());
            payDeductionTypeModelList = deductionDB.GetPayDeductionTypeListBySearch();
            //---------------------------------------------
            var vList = new object();

            vList = new
            {
                aaData = (from item in payDeductionTypeModelList
                          select new
                          {
                              Actions = item.DeductionTypeID == 0 ? "" : (ExcludedTypes.Contains(item.DeductionTypeID) ? "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditDeductionType(" + item.DeductionTypeID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i></a> <a disabled class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteDeductionType(" + item.DeductionTypeID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>" : "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditDeductionType(" + item.DeductionTypeID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteDeductionType(" + item.DeductionTypeID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>"),
                              DeductionTypeID = item.DeductionTypeID,
                              DeductionTypeName_1 = item.DeductionTypeName_1,
                              DeductionTypeName_2 = item.DeductionTypeName_2,
                              DeductionTypeName_3 = item.DeductionTypeName_3,
                              DeductionTypeShortName_1 = item.DeductionTypeShortName_1,
                              //DeductionTypeShortName_2 = item.DeductionTypeShortName_2,
                              //DeductionTypeShortName_3 = item.DeductionTypeShortName_3,                            
                              TransactionDate = item.TransactionDate,
                              //IsActive = item.IsActive,
                              Sequence = item.Sequence,
                              acc_code = item.acc_code

                          }).ToArray()
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePayDeductionType(int Id)
        {
            //string result = "";
            //string resultMessage = "";
            PayDeductionTypeModel payDeductionTypeModel = new PayDeductionTypeModel();
            OperationDetails operationDetails = new OperationDetails();
            DeductionDB deductionDB = new DeductionDB();
            payDeductionTypeModel.DeductionTypeID = Id;
            operationDetails = deductionDB.PayDeductionTypeCrud(payDeductionTypeModel, 3);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updateInstallmentStatus(int Id, string status)
        {
            string result = "";
            string resultMessage = "";
            PayDeductionModel payDeductionModel = new PayDeductionModel();
            OperationDetails opertaionDetails = new OperationDetails();
            DeductionDB deductionDB = new DeductionDB();

            payDeductionModel.PayDeductionID = Id;
            payDeductionModel.IsInstallment = Convert.ToBoolean(status) == true ? true : false;

            opertaionDetails = deductionDB.updateInstallmentStatus(payDeductionModel);

            if (opertaionDetails.Message == "Success")
            {
                result = "success";
                if (status == "true")
                {
                    resultMessage = "Installment status changed successfully";
                }
                else
                {

                    resultMessage = "Installment status changed successfully";
                }

            }
            return Json(new { result = result, resultMessage = resultMessage, EmployeeId = Id }, JsonRequestBehavior.AllowGet);
        }

        #region Deduction Details

        public ActionResult GetViewDeductionDetailsList(string iDisplayStart = "0", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, int PayDeductionId = 0)
        {
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = 0; //(Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = 0; //Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;

            List<PayDeductionDetailModel> payDeductionDetailModelList = new List<PayDeductionDetailModel>();
            DeductionDB deductionDB = new DeductionDB();
            payDeductionDetailModelList = deductionDB.GetAllPayDeductionDetailsList(PayDeductionId);
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            var vList = new object();

            vList = new
            {
                aaData = (from item in payDeductionDetailModelList
                          select new
                          {
                              PayDeductionDetailID = item.PayDeductionDetailID,
                              PayDeductionID = item.PayDeductionID,
                              DeductionDate = item.DeductionDate != "" ? item.DeductionDate.Split(' ')[0] : "",
                              Amount = item.Amount.ToString(AmountFormat),
                              Comments = item.Comments,
                              IsActive = item.IsActive == true ? "<input type='checkbox' class='checkbox' checked='true' id='" + item.PayDeductionDetailID + "' disabled='disabled' />" : "<input type='checkbox' class='checkbox'  id='" + item.PayDeductionDetailID + "' disabled='disabled' />",
                              IsWaived = item.IsWaived == true ? "<input type='checkbox' class='checkbox' checked='true' id='" + item.PayDeductionDetailID + "' disabled='disabled' />" : "<input type='checkbox' class='checkbox'  id='" + item.PayDeductionDetailID + "' disabled='disabled' />",
                          }).ToArray(),
                recordsTotal = payDeductionDetailModelList.Count,
                recordsFiltered = payDeductionDetailModelList.Count
            };

            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Deduction Details Export
        DataSet Installmentds;
        public ActionResult GetInstallmentDataForExport(int? InstallmentPayDeductionId)
        {
            OperationDetails op = new OperationDetails();
            int EmployeeID = 0;
            if (Session["EmployeeListID"] != null)
            {
                EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());

            }
            DeductionDB objDeductionDB = new DeductionDB();
            Installmentds = objDeductionDB.GetInstallmentDeductionDetail(EmployeeID, InstallmentPayDeductionId);
            TempData["Installmentds"] = Installmentds;
            if (Installmentds.Tables.Count > 0)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public void ExportDeductionDetailToPdf()
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);
            string fileName = "EmployeeDeductionDetails" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            DataSet ds = (DataSet)TempData["Installmentds"];
            Document dc = CommonHelper.CommonHelper.GenrateDeductionDetailPDF(Response, ds);
            Response.Write(dc);
            Response.End();

        }

        public ActionResult ExportToExcel(int EmployeeId)
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "PayDeduction" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            DeductionDB objDeductionDB = new DeductionDB();

            System.Data.DataSet ds = objDeductionDB.GetAllPayDeductionDataset(EmployeeId);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
        #endregion
        public ActionResult SaveMultipleDeduction(string hdnPayAdditionList, string deductionDdl, string effectiveDatetxt, string refNotxt, string descriptiontxt)
        {
            List<multipleDeduction> multipleDeductionDetails = JsonConvert.DeserializeObject<List<multipleDeduction>>(hdnPayAdditionList);
            DeductionDB deductionDB = new DeductionDB();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails operationDetails;

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 3).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                operationDetails = deductionDB.AddMultipleDeduction(Convert.ToInt32(deductionDdl), effectiveDatetxt, refNotxt, descriptiontxt, multipleDeductionDetails, objUserContextViewModel.UserId, true);
                operationDetails.CssClass = "DirectMultipleSave";
                if (operationDetails.Success)
                    operationDetails.Message = "Pay deduction record save successfull.";
                else
                    operationDetails.Message = "Technical error has occurred";
            }
            else
            {
                operationDetails = deductionDB.AddMultipleDeduction(Convert.ToInt32(deductionDdl), effectiveDatetxt, refNotxt, descriptiontxt, multipleDeductionDetails, objUserContextViewModel.UserId, false);
                operationDetails.CssClass = "Request";
                if (operationDetails.Success)
                    operationDetails.Message = "Pay deduction modification request send for confirmation.";
                else
                    operationDetails.Message = "Technical error has occurred";
            }

            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public double CalculateAmountForDays(int Id, string days, string effectiveDate, int mode, out DateTime Date1, out int NoOfCycleDays)
        {
            DeductionDB deductionDB = new DeductionDB();
            double value = -1, Amount = 0;
            DataTable dsSalary = deductionDB.GetEmployeeFullSalary(Id, mode);
            CommonHelper.CommonHelper.GetAmountFormat();
            double salary = Convert.ToDouble(dsSalary.Rows[0][0].ToString());
            try
            {
                if (effectiveDate.Contains("-"))
                    effectiveDate = effectiveDate.Replace('-', '/');
                Date1 = DateTime.ParseExact(effectiveDate, "dd/MM/yyyy", null);

            }
            catch (Exception ex)
            {
                Date1 = new DateTime();
            }
            PayrollDB objPayrollDB = new PayrollDB();
            NoOfCycleDays = objPayrollDB.GetNoOfDaysInCycle();

            if (NoOfCycleDays == 0)
            {
                NoOfCycleDays = objPayrollDB.GetNoOfDaysInCycleForDate(Date1);
            }
            if (days != null)
            {
                value = Convert.ToDouble(days);
            }

            if (NoOfCycleDays > 0)
            {
                if (value != -1 && salary != 0)
                {
                    Amount = Math.Round((salary / NoOfCycleDays), CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero) * value;
                }
                Amount = Math.Round(Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
            }
            return Amount;
        }

        public ActionResult CheckDeductionNameExist(string deductionName)
        {
            DeductionDB deductionDB = new DeductionDB();
            var deductionList = deductionDB.GetPayDeductionDddlList();
            if (deductionList.Where(x => x.text.ToLower() == deductionName.ToLower()).Count() > 0)
                return Json("true", JsonRequestBehavior.AllowGet);
            else
                return Json("false", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteDeductionDetail(int PayDeductionID, int deductionDetailId)
        {
            OperationDetails op = new OperationDetails();
            DeductionDB objDeductionDB = new DeductionDB();
            PayrollDB objPayrollDB = new PayrollDB();

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = objPayrollDB.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 3).FirstOrDefault();
            objPayrollSetting = objPayrollDB.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                op = objDeductionDB.DeletePayDeductionDetail(deductionDetailId);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                PayDeductionModel objPayDeductionModel = new PayDeductionModel();
                objPayDeductionModel = objDeductionDB.GetPayDeductionByID(PayDeductionID);
                PayDeductionDetailModel objPayDeductionDetail = new PayDeductionDetailModel();
                objPayDeductionDetail = objDeductionDB.GetDeductionDetailById(deductionDetailId);

                List<PayDeductionDetailModel> lstpayDeductiondetailModel = new List<PayDeductionDetailModel>();
                lstpayDeductiondetailModel = objDeductionDB.GetAllPayDeductionDetailsList(PayDeductionID);

                DataTable dt = new DataTable();
                foreach (var item in lstpayDeductiondetailModel)
                {
                    if (item.PayDeductionDetailID != objPayDeductionDetail.PayDeductionDetailID)
                    {
                        item.Amount = item.Amount + (item.Amount / (lstpayDeductiondetailModel.Count - 1));
                    }
                }

                lstpayDeductiondetailModel.Remove(objPayDeductionDetail);
                DataTable multipleDeductionDetail = new DataTable();
                multipleDeductionDetail.Columns.Add("PayDeductionDetailID", typeof(int));
                multipleDeductionDetail.Columns.Add("PayDeductionId", typeof(int));
                multipleDeductionDetail.Columns.Add("DeductionDate", typeof(DateTime));
                multipleDeductionDetail.Columns.Add("Amount", typeof(decimal));
                multipleDeductionDetail.Columns.Add("Comments", typeof(string));
                multipleDeductionDetail.Columns.Add("Active", typeof(bool));
                multipleDeductionDetail.Columns.Add("employeeId", typeof(int));
                multipleDeductionDetail.Columns.Add("IsProcessed", typeof(bool));
                multipleDeductionDetail.Columns.Add("IsWaived", typeof(int));
                multipleDeductionDetail.Columns.Add("rowNum", typeof(int));

                for (int i = 0; i < lstpayDeductiondetailModel.Count; i++)
                {
                    if (lstpayDeductiondetailModel[i].PayDeductionDetailID != objPayDeductionDetail.PayDeductionDetailID)
                    {
                        multipleDeductionDetail.Rows.Add(
                        lstpayDeductiondetailModel[i].PayDeductionDetailID,
                        lstpayDeductiondetailModel[i].PayDeductionID,
                        DateTime.ParseExact(lstpayDeductiondetailModel[i].DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                        lstpayDeductiondetailModel[i].Amount,
                        lstpayDeductiondetailModel[i].Comments,
                        lstpayDeductiondetailModel[i].IsActive,
                        objPayDeductionModel.EmployeeID,
                        false,
                        lstpayDeductiondetailModel[i].IsWaived,
                        i
                        );
                    }
                }

                op = objDeductionDB.SavePayrollConfirmationRequestForDeduction(objPayDeductionModel, objPayDeductionDetail, multipleDeductionDetail, objUserContextViewModel.UserId, 3);
                op.CssClass = "Request";
                if (op.Success)
                    op.Message = "Pay deduction modification request send for confirmation.";
            }


            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDeductionDetails(int DeductionID)
        {
            PayDeductionModel objPayDeductionModel = new PayDeductionModel();
            DeductionDB objDeductionDB = new DeductionDB();
            objPayDeductionModel = objDeductionDB.GetPayDeductionByID(DeductionID);
            return View(objPayDeductionModel);
        }

        //Deduction Installments
        public ActionResult GetDeductionInstallments(int PayDeductionId)
        {
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            List<PayDeductionDetailModel> payDeductionDetailModelList = new List<PayDeductionDetailModel>();
            DeductionDB deductionDB = new DeductionDB();
            payDeductionDetailModelList = deductionDB.GetAllPayDeductionDetailsList(PayDeductionId);
            PayDeductionModel payDeductionModel = deductionDB.GetPayDeductionByID(PayDeductionId);
            ViewBag.Amount = payDeductionModel.Amount.ToString(CommonHelper.CommonHelper.GetAmountFormat());
            ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
            return View(payDeductionDetailModelList);
        }
        public ActionResult GetDeductionInstallmentDetails(int PayDeductionId)
        {
            List<PayDeductionDetailModel> payDeductionDetailModelList = new List<PayDeductionDetailModel>();
            DeductionDB deductionDB = new DeductionDB();
            payDeductionDetailModelList = deductionDB.GetAllPayDeductionDetailsList(PayDeductionId);
            PayDeductionModel payDeductionModel = deductionDB.GetPayDeductionByID(PayDeductionId);
            return Json(new { list = payDeductionDetailModelList, TotalAmount = payDeductionModel.Amount.ToString(CommonHelper.CommonHelper.GetAmountFormat()) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckWaivedInstallments(string DeductDetailIds, int PayDeductionId)
        {
            int CountOfWaived = 0;
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            DeductionDB deductionDB = new DeductionDB();
            CountOfWaived = deductionDB.CheckWaivedInstallments(DeductDetailIds, PayDeductionId);
            return Json(CountOfWaived, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveMultipleInstallments(string InstallmentDetails, int PayDeductionID, bool IsWaivedRecord)
        {
            PayDeductionDetailModel payDeductionDetailModel = new PayDeductionDetailModel();
            OperationDetails operationDetails = new OperationDetails();
            DeductionDB deductionDB = new DeductionDB();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            OperationDetails op = new OperationDetails();
            List<PayDeductionDetailModel> multipleDeductionDetails = JsonConvert.DeserializeObject<List<PayDeductionDetailModel>>(InstallmentDetails);
            DataTable multipleDeductionDetail = new DataTable();
            multipleDeductionDetail.Columns.Add("PayDeductionDetailId", typeof(int));
            multipleDeductionDetail.Columns.Add("PayDeductionId", typeof(int));
            multipleDeductionDetail.Columns.Add("DeductionDate", typeof(DateTime));
            multipleDeductionDetail.Columns.Add("Amount", typeof(decimal));
            multipleDeductionDetail.Columns.Add("Comments", typeof(string));
            multipleDeductionDetail.Columns.Add("Active", typeof(bool));
            multipleDeductionDetail.Columns.Add("employeeId", typeof(int));
            multipleDeductionDetail.Columns.Add("IsProcessed", typeof(int));
            multipleDeductionDetail.Columns.Add("IsWaived", typeof(int));
            multipleDeductionDetail.Columns.Add("rowNum", typeof(int));
            int rowCount = 1;
            foreach (var item in multipleDeductionDetails.Where(m => m.IsDeleted == false))
            {
                DateTime dt = DateTime.ParseExact(item.DeductionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                multipleDeductionDetail.Rows.Add(
                    0,
                    item.PayDeductionID,
                    dt,
                    item.Amount,
                    item.Comments,
                    item.IsActive,
                    0,
                    item.IsProcessed,
                    item.IsWaived,
                    rowCount
                    );
                rowCount++;
            }
            if (IsWaivedRecord)
            {
                op = deductionDB.UpdateDeductionDetails(multipleDeductionDetail, PayDeductionID, 1);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!payrollDb.GetPayrollGeneralSetting().IsconfirmRunPayroll || CommonHelper.CommonHelper.checkUrlPermission("Payroll/PayrollConfirmation", "Url"))
                {
                    op = deductionDB.UpdateDeductionDetails(multipleDeductionDetail, PayDeductionID, 0);
                    return Json(op, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    PayDeductionModel payDeductionModel = deductionDB.GetPayDeductionByID(PayDeductionID);
                    payDeductionDetailModel = multipleDeductionDetails.FirstOrDefault();
                    operationDetails = deductionDB.SavePayrollConfirmationRequestForDeduction(payDeductionModel, payDeductionDetailModel, multipleDeductionDetail, objUserContextViewModel.UserId, 2);
                    operationDetails.CssClass = "Request";
                    if (operationDetails.Success)
                        operationDetails.Message = "Pay deduction modification request send for confirmation.";
                }
            }

            if (operationDetails.Success)
            {
                return Json(new { CssClass = operationDetails.CssClass, result = "success", Message = operationDetails.Message, amountLeft = operationDetails.InitiatorIdentity }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { CssClass = operationDetails.CssClass, result = "error", Message = "Technical error has occurred" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CheckPaidCycleValidation(int payDeductionId, int PaidCycles, double Amount)
        {
            DeductionDB objDeductionDB = new DeductionDB();
            bool IsPaidcycleValid = false;
            bool IsPaidAmountValid = false;
            IsPaidcycleValid = objDeductionDB.CheckPaidCycleValidation(payDeductionId, PaidCycles, Amount, out IsPaidAmountValid);
            return Json(new { IsPaidcycleValid = IsPaidcycleValid, IsPaidAmountValid = IsPaidAmountValid }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckDedTypeExistForOST()
        {
            DeductionDB objDeductionDB = new DeductionDB();
            bool isExist = objDeductionDB.CheckIfDeductionTypeExistForOst();

            if (isExist)
            {
                return Json(new { result = "success", resultMessage = "Primary document is already exists, If you save record then other document will remove from primary." });
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Please select an employee." });
            }
        }

        public ActionResult FOSTBalanceDetails(int PayDeductionId)
        {
            ViewBag.PayDeductionId = PayDeductionId;
            return View();
        }
        public ActionResult GetFOSTBalanceDetails(int PayDeductionId)
        {
            List<StudentFOSTBDetails> studentFOSTBDetails = new List<StudentFOSTBDetails>();
            DeductionDB deductionDB = new DeductionDB();
            studentFOSTBDetails = deductionDB.GetFOSTBDetailsListByDeductionId(PayDeductionId, "", 1);
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            string Actions = Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditFOSTBPerStudent(PayDeductionID,DeductionDate,AmtPerStud)' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewFOSTBPerStudent(PayDeductionID,DeductionDate,AmtPerStud)' title='View Details' ><i class='fa fa-eye'></i></a>";
            string NonEditedActins = "<a disabled class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditFOSTBPerStudent(PayDeductionID,DeductionDate,AmtPerStud)' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewFOSTBPerStudent(PayDeductionID,DeductionDate,AmtPerStud)' title='View Details' ><i class='fa fa-eye'></i></a>";
            var vList = new
            {
                aaData = (from item in studentFOSTBDetails
                          select new
                          {
                              Amount = item.Amount.ToString(AmountFormat),
                              DeductionDate = item.DeductionDate,
                              Action = item.IsProcessed == true ? NonEditedActins.Replace("PayDeductionID", item.PayDeductionID.ToString()).Replace("DeductionDate", '"' + item.DeductionDate.ToString() + '"').Replace("AmtPerStud", '"' + item.Amount.ToString() + '"') : Actions.Replace("PayDeductionID", item.PayDeductionID.ToString()).Replace("DeductionDate", '"' + item.DeductionDate.ToString() + '"').Replace("AmtPerStud", '"' + item.Amount.ToString() + '"')
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditFOSTBalanceDetails(int PayDeductionId, string DeductionDate, string AmtPerStud)
        {
            List<StudentFOSTBDetails> studentFOSTBDetails = new List<StudentFOSTBDetails>();
            DeductionDB deductionDB = new DeductionDB();
            studentFOSTBDetails = deductionDB.GetFOSTBDetailsListByDeductionId(PayDeductionId, DeductionDate, 2);
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            ViewBag.AmtPerStud = AmtPerStud;
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
            ViewBag.IsExistUnregStud = studentFOSTBDetails.Where(m => m.IsRegistered == false).ToList().Count();
            return View(studentFOSTBDetails);
        }
        [HttpPost]
        public ActionResult EditFOSTBalanceDetails(string studentDetails)
        {
            List<StudentFOSTBDetails> studentFOSTBDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StudentFOSTBDetails>>(studentDetails);
            OperationDetails operationDetails = new OperationDetails();
            DeductionDB deductionDB = new DeductionDB();
            StudentFOSTBDetails studentFOSTB;
            foreach (var item in studentFOSTBDetails)
            {
                studentFOSTB = new StudentFOSTBDetails();
                studentFOSTB.DeductionStdID = item.DeductionStdID;
                studentFOSTB.Amount = item.Amount;
                deductionDB.StudentFOSTBDetailsCrud(studentFOSTB, 2);
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewFOSTBalanceDetails(int PayDeductionId, string DeductionDate, string AmtPerStud)
        {
            List<StudentFOSTBDetails> studentFOSTBDetails = new List<StudentFOSTBDetails>();
            DeductionDB deductionDB = new DeductionDB();
            studentFOSTBDetails = deductionDB.GetFOSTBDetailsListByDeductionId(PayDeductionId, DeductionDate, 2);
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            ViewBag.AmtPerStud = AmtPerStud;
            int NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
            ViewBag.NoOfDecimalPlaces = NoOfDecimalPlaces;
            ViewBag.IsExistUnregStud = studentFOSTBDetails.Where(m => m.IsRegistered == false).ToList().Count();
            return View(studentFOSTBDetails);
        }
    }
}