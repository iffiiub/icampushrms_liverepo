﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;

namespace HRMS.Web.Controllers
{
    public class HolidayController : BaseController
    {
        public ActionResult Index()
        {
            CompanyDB objCompanyDB = new CompanyDB();
            HolidayDB objHolidayDB = new HolidayDB();
            List<HRMS.Entities.CompanyModel> objCompanyList = new List<HRMS.Entities.CompanyModel>();
            //objCompanyList = objCompanyDB.GetAllCompanyList();
            ViewBag.Company = GetCompanySelectList();// new SelectList(objCompanyList, "CompanyId", "name");

            List<HolidayModel> objHolidayList = new List<HolidayModel>();
           // objHolidayList = objHolidayDB.GetHolidayList(null);
            ViewBag.Holiday = new SelectList(objHolidayList, "HolidayID", "HolidayName");

            return View();
        }

        public ActionResult GetHolidayList(int? companyID,int?holidayID)
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            

            //-------------Data Objects--------------------
            List<HolidayModel> objHolidayModelList = new List<HolidayModel>();
            HolidayDB objHolidayDB = new HolidayDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objHolidayModelList = objHolidayDB.GetHolidayList(companyID, holidayID,objUserContextViewModel.UserId);
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objHolidayModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHoliday(" + item.HolidayID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteHoliday(" + item.HolidayID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              HolidayID = item.HolidayID,
                              HolidayName = item.HolidayName,
                              CompanyName = item.CompanyName,
                              From = item.From,
                              to = item.to
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddHoliday()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HolidayModel holidayModel = new HolidayModel();
            //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
            ViewBag.CompanyList = GetCompanySelectList();// new SelectList(companyList, "CompanyId", "Name");
            holidayModel.From = DateTime.Now.ToString(HRMSDateFormat);
            return View(holidayModel);
        }

        [HttpPost]
        public ActionResult AddHoliday(HolidayModel objHolidayModel)
        {
            objHolidayModel.CompanyIDs = Request["CompanyList"];
            if (string.IsNullOrEmpty(objHolidayModel.CompanyIDs))
                ModelState.AddModelError("Company", "Please select atleast one organization");
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HolidayDB objHolidayDB = new HolidayDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objHolidayModel.CreatedBy = objUserContextViewModel.UserId;
                objOperationDetails = objHolidayDB.InsertHoliday(objHolidayModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                string messages = string.Join("<br/> ", ModelState.Values
                                                     .SelectMany(x => x.Errors)
                                                     .Select(x => x.ErrorMessage));
                objOperationDetails.Message = messages;
               // objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditHoliday(int id)
        {         
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];           
            //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
            ViewBag.CompanyList =GetCompanySelectList();
            HolidayDB objHolidayModelDB = new HolidayDB();
            HolidayModel objHolidayModel = new HolidayModel();
            objHolidayModel = objHolidayModelDB.HolidayById(id);           
            return View(objHolidayModel);          

        }

        [HttpPost]
        public JsonResult EditHoliday(HolidayModel objHolidayModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HolidayDB objHolidayModelDB = new HolidayDB();
                objHolidayModel.ModifiedBy = objUserContextViewModel.UserId;
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objHolidayModelDB.UpdateHoliday(objHolidayModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewHoliday(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HolidayDB objHolidayModelDB = new HolidayDB();
            HolidayModel objHolidayModel = new HolidayModel();
            objHolidayModel = objHolidayModelDB.HolidayById(id);
            return View(objHolidayModel);
        }

        public ActionResult DeleteHoliday(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HolidayDB objHolidayDB = new HolidayDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objHolidayDB.DeleteHolidayById(id, objUserContextViewModel.UserId);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetHolidayListByCompany(int? companyID,int?holidayID)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HolidayModel> objHolidayList = new List<HolidayModel>();
            HolidayDB objHolidayDB = new HolidayDB();
            objHolidayList = objHolidayDB.GetHolidayList(companyID, holidayID,objUserContextViewModel.UserId);
            ViewBag.Holiday = new SelectList(objHolidayList, "HolidayID", "HolidayName");
            return Json(objHolidayList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetHoliday(int holidayID)
        {         
            HolidayDB objHolidayModelDB = new HolidayDB();
            HolidayModel objHolidayModel = new HolidayModel();
            objHolidayModel = objHolidayModelDB.HolidayById(holidayID);
            return Json(objHolidayModel, JsonRequestBehavior.AllowGet);
        }
    }
}