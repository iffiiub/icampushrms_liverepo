﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class GratuitySetupController : BaseController
    {
        // GET: GratuitySetup
        public ActionResult Index()
        {
            GratuityModel Gratuity = new GratuityModel();
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            List<PaySalaryAllowance> lstPaySalaryModel = new List<PaySalaryAllowance>();
            lstPaySalaryModel = paySalaryDB.GetAllPaySalaryAllowanceList(0);
            foreach (var item in lstPaySalaryModel)
            {
                Gratuity.Items.Add(new CheckListBoxItem { Text = item.PaySalaryAllowanceName_1, Value = item.PaySalaryAllowanceID.ToString() });
            }
            return View(Gratuity);
        }

        public ActionResult LoadAllowances()
        {
            GratuityModel Gratuity = new GratuityModel();
            PaySalaryDB paySalaryDB = new PaySalaryDB();
            List<PaySalaryAllowance> lstPaySalaryModel = new List<PaySalaryAllowance>();
            lstPaySalaryModel = paySalaryDB.GetAllPaySalaryAllowanceList(0);
            foreach (var item in lstPaySalaryModel)
            {
                Gratuity.Items.Add(new CheckListBoxItem { Text = item.PaySalaryAllowanceName_1, Value = item.PaySalaryAllowanceID.ToString() });
            }
            return PartialView("LoadAllowances", Gratuity);
        }

        public JsonResult AddGratuitySetting(string selectedAllowances, bool isStandard)
        {
            string result = "";
            string resultMessage = "Error";
            GratuitySetupDB objGratuitySetupDB = new GratuitySetupDB();
            List<SalaryPackageAllowances> salaryPacksList = JsonConvert.DeserializeObject<List<SalaryPackageAllowances>>(selectedAllowances);

            try
            {
                decimal percent = 60;
                int standardbit = 0;
                if (isStandard)
                {
                    percent = 0;
                    standardbit = 1;
                }
                string SalaryAllwoances = "";
                foreach (var item in salaryPacksList)
                {
                    SalaryAllwoances = SalaryAllwoances + item.id.ToString() + ",";
                }

                SalaryAllwoances = SalaryAllwoances.Remove(SalaryAllwoances.LastIndexOf(','));
                resultMessage = objGratuitySetupDB.AddGratuitySetting(SalaryAllwoances, standardbit, percent);
                if (resultMessage == "success")
                {
                    resultMessage = "Gratiuty setting inserted successfully";
                    result = "success";

                }
                else
                {
                    result = "error";
                    resultMessage = "Error occured while adding gratuity setting.";
                }
            }
            catch (Exception)
            {
                result = "error";
                resultMessage = "Error occured while adding gratuity setting.";
            }

            //return Redirect("Index");
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGratuitySetupList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "DESC", string mDataProp_1 = "InsuranceDependence", int iSortCol_0 = 0, int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            switch (iSortCol_0)
            {
                default:
                    sortColumn = "NationalityName";
                    break;
            }


            //-------------Data Objects--------------------
            List<SalaryPackageAllowances> objSalPackAllowances = new List<SalaryPackageAllowances>();
            GratuitySetupDB objGratuitySetupDB = new GratuitySetupDB();

            objSalPackAllowances = objGratuitySetupDB.GetGratuitySetting();
            //---------------------------------------------


            var vList = new object();

            vList = new
            {
                aaData = (from item in objSalPackAllowances
                          select new
                          {
                              AllowanceName = item.PaySalaryAllowanceName,
                              Action = "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteAllowance(" + item.id.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              Percentage = item.AmountInPercent + "%",
                              IsStandered = item.isSelected == true ? "<input type=\"checkbox\" checked disabled=\"disabled\">" : "<input type=\"checkbox\" disabled=\"disabled\">"
                          }).ToArray(),
                recordsTotal = objSalPackAllowances.Count,
                recordsFiltered = objSalPackAllowances.Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);


        }


        public JsonResult DeleteAllowanceFromSetting(string AllowanceId)
        {
            string result = "";
            string resultMessage = "Error";
            GratuitySetupDB objGratuitySetupDB = new GratuitySetupDB();
            try
            {
                resultMessage = objGratuitySetupDB.DeleteAllowanceFromSetting(AllowanceId);
                if (resultMessage == "success")
                {
                    resultMessage = "Gratiuty setting updated successfully.";
                    result = "success";

                }
                else
                {
                    result = "error";
                    resultMessage = "Error occured while updating gratuity setting.";
                }
            }
            catch (Exception)
            {
                result = "error";
                resultMessage = "Error occured while updating gratuity setting.";
            }

            //return Redirect("Index");
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveEmployeeId(string employeeid)
        {
            Session["EmployeeIds"] = employeeid;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddFinalSettlementofEmployee(int GratuityFSID)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            EmployeeGratuityModel objEmpGratuityModel = new EmployeeGratuityModel();
            List<Employee> lstEmployee = new List<Employee>();
            ReportingDB objReportingDB = new ReportingDB();
            EmployeeDB objEmployeeSectionDB = new EmployeeDB();
            lstEmployee = objEmployeeSectionDB.GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1).ToList();
            List<SelectListItem> EOSList = new List<SelectListItem>();
            EOSList.Add(new SelectListItem { Text = "Resigned", Value = "0" });
            EOSList.Add(new SelectListItem { Text = "Termination", Value = "1" });
            ViewBag.EOSList = EOSList;
            if (GratuityFSID > 0)
            {
                objEmpGratuityModel = new GratuitySetupDB().GetFinalSettlmentDetailsbyEmployee(GratuityFSID);
            }
            foreach (var item in lstEmployee)
            {
                objEmpGratuityModel.Items.Add(new CheckListBoxItem { Text = item.FullName, Value = item.EmployeeId.ToString() });
            }
            return PartialView(objEmpGratuityModel);
        }
        public ActionResult FinalSettlement()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            EmployeeGratuityModel objEmpGratuityModel = new EmployeeGratuityModel();
            List<Employee> lstEmployee = new List<Employee>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            lstEmployee = objEmployeeDB.GetEmployeeByActive(true,UserId);
            List<SelectListItem> EOSList = new List<SelectListItem>();
            EOSList.Add(new SelectListItem { Text = "Resigned", Value = "0" });
            EOSList.Add(new SelectListItem { Text = "Termination", Value = "1" });
            ViewBag.EOSList = EOSList;
            foreach (var item in lstEmployee)
            {
                objEmpGratuityModel.Items.Add(new CheckListBoxItem { Text = item.FirstName, Value = item.EmployeeId.ToString() });
            }
            return View(objEmpGratuityModel);
        }

        public ActionResult SaveFinalSettlement(string EmpGrat, string EmployeeIds)
        {
            OperationDetails op = new OperationDetails();
            EmployeeGratuityModel empGrat = Newtonsoft.Json.JsonConvert.DeserializeObject<EmployeeGratuityModel>(EmpGrat);
            GratuitySetupDB objGratuitySetupDB = new GratuitySetupDB();
            List<Int32> empIdArray = new List<int>();
            if (string.IsNullOrEmpty(EmployeeIds))
            {
                EmployeeIds = empGrat.EmployeeId.ToString();
            }
            try
            {
                empIdArray = EmployeeIds.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                string EOS = "";
                if (empGrat.EOSType == 0)
                {
                    EOS = "Resigned";
                }
                else
                {
                    EOS = "Termination";
                }
                foreach (var empId in empIdArray)
                {
                    op = objGratuitySetupDB.CalcGratuity(empId.ToString(), empGrat.ToDate, EOS);
                }
                // empGrat.LastWorkingDays = (DateTime.ParseExact(empGrat.ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) - new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).Days + 1;
                //empGrat.LastWorkingDays = DateTime.ParseExact(empGrat.ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Day;
                int lastWorkingDays = DateTime.ParseExact(empGrat.ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Day;
                int LastMonth = DateTime.ParseExact(empGrat.ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Month;
                int LastYear = DateTime.ParseExact(empGrat.ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Year;
                int daysInMonth = System.DateTime.DaysInMonth(LastYear, LastMonth);
                if (lastWorkingDays == 31 && daysInMonth==31)
                {
                    empGrat.LastWorkingDays = 30;
                }
                else if (lastWorkingDays == 29 && daysInMonth == 29)
                {
                    empGrat.LastWorkingDays = 28;
                }
                else if (lastWorkingDays == 28 && daysInMonth == 28)
                {
                    empGrat.LastWorkingDays = 28;
                }
                else
                {
                    empGrat.LastWorkingDays = lastWorkingDays;
                }

                op = objGratuitySetupDB.SaveFinalSettlement(empGrat, EmployeeIds);
                if (op.Success)
                {
                    op.Message = "Gratiuty Calculated Successfully";
                    op.CssClass = "success";

                }
                else
                {
                    op.Message = "Error While Calculating Gratuity";
                    op.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                op.Success = false;
                op.Message = "Error While Calculating Gratuity";
                op.CssClass = "error";
            }
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckGratuitySetup()
        {
            OperationDetails op = new OperationDetails();
            bool isGratSetupAvl = new GratuitySetupDB().CheckGratuitySetup();
            if (!isGratSetupAvl)
            {
                op.CssClass = "error";
                op.Message = "You cannot run the gratuity and final settlement because gratuity setup is empty.";
            }
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeesNotInSalary(string EmpIds)
        {
            string EmployeeNames = "";
            OperationDetails op = new OperationDetails();
            EmployeeNames = new GratuitySetupDB().GetEmployeenamesNotHavingSalary(EmpIds);
            op.CssClass = "warning";
            if (!string.IsNullOrEmpty(EmployeeNames))
            {
                op.Message = "Gratuity will not generated because salary not exists for following employees: " + EmployeeNames;
            }
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFinalSettlmentDetails()
        {
            GratuitySetupDB objGratuitySetupDB = new GratuitySetupDB();
            List<EmployeeGratuityModel> finalSettlmentList;
            finalSettlmentList = objGratuitySetupDB.GetFinalSettlmentDetails();
            var vList = new object();
            vList = new
            {
                aaData = (from item in finalSettlmentList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.GratuityFSID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGratuityWithSettlement(" + item.EmployeeId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeId = item.EmpAlternativeID,
                              employeename = item.EmployeeName,
                              GratuityBeforeNEAmount = item.GratuityBeforeNEAmount,
                              ESOthersAmount = item.ESOthersAmount,
                              CompensatoryLeaveAmount = item.CompensatoryLeaveAmount,
                              GratuityAlreadyPaidAmount = item.GratuityAlreadyPaidAmount,
                              ESOthersDeductionAmount = item.ESOthersDeductionAmount
                          }).ToArray(),

            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteGratuityAndSettlement(int EmployeeId)
        {
            OperationDetails op = new OperationDetails();
            try
            {
                op = new GratuitySetupDB().DeleteGratuityWithFinalSettlement(EmployeeId);
            }
            catch (Exception ex)
            {
                op.Message = "Error while deleting Gratuity and Final Settlement Details.";
                op.CssClass = "error";
            }
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }
    }
}