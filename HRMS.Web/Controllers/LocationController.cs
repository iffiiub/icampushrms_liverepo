﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace HRMS.Web.Controllers
{
    public class LocationController:BaseController
    {
         
        // GET: /Location Controller/
        public ActionResult Index()
        {
            
            return View();
        }


        public void GetDropDowns(int id, int CountryID,int StateID,int CityID)
        {
            List<SelectListItem> ObjSelectedList = null;            
            CountryDB objCountryDB = new CountryDB();
            StateDB objState = new StateDB();
            CityDB objCity = new CityDB();

            //Get Country List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "0" });
            foreach (var m in objCountryDB.GetAllContries())
            {
                if (id == 0)
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });
                }
                else
                {
                    if(m.CountryId==CountryID)
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString(),Selected=true });
                    }
                    else
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });
                    }
                    
                }

            }
            ViewBag.CountryList = ObjSelectedList;

            // State List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select State", Value = "0" });
            if (id != 0)
            {
                foreach (var state in objState.GetAllStatesByCountryId(Convert.ToString(CountryID)))
                {                    
                        if (state.StateId == StateID)
                        {
                            ObjSelectedList.Add(new SelectListItem { Text = state.StateName, Value = state.StateId.ToString(), Selected = true });
                        }
                        else
                        {
                            ObjSelectedList.Add(new SelectListItem { Text = state.StateName, Value = state.StateId.ToString() });
                        }                    
                }
            }
            ViewBag.StateList = ObjSelectedList;

            // City List
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });

            if (id != 0)
            {
                foreach (var city in objCity.GetAllCityByStateId(StateID.ToString()))
                {
                    if (city.CityId == CityID)
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = city.CityName, Value = city.CityId.ToString(), Selected = true });
                    }
                    else
                    {
                        ObjSelectedList.Add(new SelectListItem { Text = city.CityName, Value = city.CityId.ToString() });
                    }
                }
            }

            ViewBag.CityList = ObjSelectedList;

        }
        


        public ActionResult GetLocationList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<LocationModel> objLocationList = new List<LocationModel>();
            LocationDB objLocationDB = new LocationDB();
            objLocationList = objLocationDB.GetLocationList(pageNumber, numberOfRecords, sortColumn, sortOrder, objUserContextViewModel.CompanyId, out totalCount);


            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objLocationList
                          select new
                          {
                              Actions = " <a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.LocationID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.LocationID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='viewLocation(" + item.LocationID.ToString() + "," + item.CompanyId + ")' title='LocationSummary' ><i class='fa fa-eye'></i></a>",
                              LocationID = item.LocationID,
                              CountryName = item.CountryName,
                              CityName = item.CityName,
                              StateName = item.StateName
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LocationModel locationModel = new LocationModel();
            LocationDB locationDB = new LocationDB();

            
            if (id != 0)
            {
                //locationDB = locationDB.GetEmployee(id);
                locationModel = locationDB.GetLocationByID(id);
                GetDropDowns(id, locationModel.CountryID,locationModel.StateID,locationModel.CityID);
            }
            else
            {
                locationModel.LocationCompanyID = objUserContextViewModel.CompanyId;
                GetDropDowns(id,0,0,0);
            }
            

            return View("Edit",locationModel);
        }


        public ActionResult LocationDetails()
        {
            GetDropDowns(0, 0, 0, 0);
            return PartialView("LocationDetails");
        }


        [HttpGet]
        public ActionResult GetStatesByCountryId(string Id)
        {
            StateDB objState = new StateDB();
            if (String.IsNullOrEmpty(Id))
            {
                throw new ArgumentNullException("Id");
            }
            int id = 0;
            bool isValid = Int32.TryParse(Id, out id);
            var states = objState.GetAllStatesByCountryId(Id);
            var result = (from s in states
                          select new
                          {
                              id = s.StateId,
                              name = s.StateName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCityByStateId(string Id)
        {
            CityDB objCity = new CityDB();
            if (String.IsNullOrEmpty(Id))
            {
                throw new ArgumentNullException("Id");
            }
            int id = 0;
            bool isValid = Int32.TryParse(Id, out id);
            var states = objCity.GetAllCityByStateId(Id);
            var result = (from s in states
                          select new
                          {
                              id = s.CityId,
                              name = s.CityName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult UpdateLocationDetails(LocationModel locationModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result="";
            string resultMessage="";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LocationDB objLocationDB = new LocationDB();
            int LocationId=0;
             if (locationModel.LocationID == 0)
             {
                 locationModel.CreatedBy = objUserContextViewModel.UserId;
                 operationDetails=objLocationDB.InsertLocation(locationModel);
                 LocationId = operationDetails.InsertedRowId;
             }
             else
             {
                 LocationId = locationModel.LocationID;
                 locationModel.ModifiedBy = objUserContextViewModel.UserId;
                 operationDetails = objLocationDB.UpdateLocation(locationModel);
             }

             if (operationDetails.Message== "Success")
            {
                if(locationModel.LocationID==0)
                {
                    result = "success";
                    resultMessage = "Location Added Successfully.";
                }
                else if (locationModel.LocationID != 0)
                {
                    result = "success";
                    resultMessage = "Location Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Location.";
            }

             return Json(new { LocationId = LocationId, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult DeleteLocation(int Id)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            LocationDB objLocationDB = new LocationDB();
            operationDetails=objLocationDB.DeleteLocation(Id);
            if (operationDetails.Message == "Success")
            {
                result = "success";
                resultMessage = "Location Deleted Successfully.";
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while deleting Location.";
            }
            return Json(new { EmployeeID = Id, result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }
        


        public ActionResult GetEmployeeByLocationId(int LocationId, int CompanyID)
        {
            EmployeeLocationModel employeeLocationModel = new EmployeeLocationModel();
            LocationDB locationDB = new LocationDB();
            employeeLocationModel.employeeInLocation = locationDB.GetEmployeeByLocationId(LocationId, CompanyID);
            employeeLocationModel.employeeNotInLocation = locationDB.GetEmployeeNotInLocationId(LocationId, CompanyID);
            return View("EmployeeLocation", employeeLocationModel);
        }


        public JsonResult GetNotInEmployeesSearch(int LocationID,int CompanyID,string search)
        {

            List<EmployeeDetailsModel> employeeDetailsModel = new List<EmployeeDetailsModel>();
            LocationDB locationDB = new LocationDB();
            employeeDetailsModel = locationDB.GetEmployeeNotInLocationId(LocationID, CompanyID, search);
            return Json(employeeDetailsModel, JsonRequestBehavior.AllowGet);
            
        }


        public JsonResult SaveList(List<String> values, int LocationID)
        {
            LocationDB locationDB = new LocationDB();
            foreach(string EmployeeID in values)
            {
                locationDB.InsertEmployeeLocation(LocationID, Convert.ToInt32(EmployeeID));                
            }
            return Json(new { Result = String.Format("Success") });
        }

        public JsonResult RemoveEmployeeLocation(int EmployeeID, int LocationID)
        {
            LocationDB locationDB = new LocationDB();           
            locationDB.RemoveEmployeeLocation(LocationID, Convert.ToInt32(EmployeeID));            
            return Json(new { Result = String.Format("Success") });
        }



        public ActionResult ViewLocationSummarry(int LocationId, int CompanyID)
        {
            LocationModel locationModel = new LocationModel();
            LocationDB locationDB = new LocationDB();
            if (LocationId != 0)
            {
                //locationDB = locationDB.GetEmployee(id);
                locationModel = locationDB.GetLocationByID(LocationId);
                locationModel.employeeInLocation = locationDB.GetEmployeeByLocationId(LocationId, CompanyID); 
            }
            return View("LocationSummarry",locationModel);
        }

        public ActionResult ExportToExcel()
        {
            byte[] content;
            string fileName = "Location" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "LocationID";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<LocationModel> objLocationList = new List<LocationModel>();
            LocationDB objLocationDB = new LocationDB();
            objLocationList = objLocationDB.GetLocationList(pageNumber, numberOfRecords, sortColumn, sortOrder, objUserContextViewModel.CompanyId, out totalCount);
            var report = (from item in objLocationList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              LocationID = item.LocationID,
                              CountryName = item.CountryName,
                              CityName = item.CityName,
                              StateName = item.StateName

                          }).ToList();
            content = ExportListToExcel(report);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "Location" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "LocationID";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<LocationModel> objLocationList = new List<LocationModel>();
            LocationDB objLocationDB = new LocationDB();
            objLocationList = objLocationDB.GetLocationList(pageNumber, numberOfRecords, sortColumn, sortOrder, objUserContextViewModel.CompanyId, out totalCount);
            var report = (from item in objLocationList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              LocationID = item.LocationID,
                              CountryName = item.CountryName,
                              CityName = item.CityName,
                              StateName = item.StateName

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        

    }
}