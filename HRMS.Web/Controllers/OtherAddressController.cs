﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class OtherAddressController : BaseController
    {
        public ActionResult Index(int Id)
        {
            ViewBag.EmployeeId = Id;
            return View();
        }

        public ActionResult GetOtherAddressList(int EmployeeID = 0)
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<OtherAddressModel> otherAddressModelList = new List<OtherAddressModel>();
            OtherAddressDB otherAddressDB = new OtherAddressDB();
            otherAddressModelList = otherAddressDB.GetAllOtherAddressList(EmployeeID);
            //---------------------------------------------

            var vList = new object();
            vList = new
            {
                aaData = (from item in otherAddressModelList
                          select new
                          {
                              //Actions = "<a class='btn btn-primary' onclick='EditOtherAddress(" + item.OtherAddressId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> Edit</a>",
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm'  onclick='AddEditOtherAddress(" + item.AddressInfoId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm'  onclick='DeleteContact(" + item.AddressInfoId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              OtherAddressId = item.AddressInfoId,
                              EmployeeID = item.EmployeeID,
                              ApartmentNo = item.ApartmentNo,
                              Address = item.Address,
                              AddressTypeName = item.AddressTypeName,
                              Street = item.Street,
                              PoBox = item.PoBox,
                              CountryID = item.CountryID,
                              StateID = item.StateID,
                              CityID = item.CityID,
                              AreaId = item.AreaId,
                              CountryName = item.CountryName,
                              StateName = item.StateName,
                              CityName = item.CityName,
                              AreaName = item.AreaName
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OtherAddress(int EmployeeId, int RefId)
        {
            ViewBag.EmployeeID = EmployeeId;
            OtherAddressDB objOtherAddressDB = new OtherAddressDB();
            OtherAddressModel objOtherAddressModel = new OtherAddressModel();
            GenderDB ObjGenderDB = new GenderDB();
            objOtherAddressModel.EmployeeID = EmployeeId;

            if (RefId != 0)
            {
                objOtherAddressModel = objOtherAddressDB.GetOtherAddress(RefId);
            }

            //Get Country List
            CountryDB objCountryDB = new CountryDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Country", Value = "0" });
            foreach (var m in objCountryDB.GetAllContries())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.CountryName, Value = m.CountryId.ToString() });

            }
            ViewBag.CountryList = ObjSelectedList;

            // State List
            ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select State", Value = "0" });
            if (objOtherAddressModel.CountryID > 0)
            {
                StateDB statdb = new StateDB();
                foreach (var m in statdb.GetAllStatesByCountryId(objOtherAddressModel.CountryID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.StateName, Value = m.StateId.ToString() });
                }
            }
            ViewBag.StateList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();

            foreach (var m in ObjGenderDB.GetTypes("stp_Gen_GetIMType"))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.Text, Value = m.ID.ToString() });

            }
            ViewBag.IMAddressTypeList = ObjSelectedList;

            // City List
            ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select City", Value = "0" });
            if (objOtherAddressModel.StateID > 0)
            {
                CityDB citydb = new CityDB();
                foreach (var m in citydb.GetAllCityByStateId(objOtherAddressModel.StateID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.CityName, Value = m.CityId.ToString() });
                }
            }
            ViewBag.CityList = ObjSelectedList;

            // Area List
            ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Area", Value = "0" });
            if (objOtherAddressModel.CityID > 0)
            {
                AreaDB areadb = new AreaDB();
                foreach (var m in areadb.GetAllAreaByCityId(objOtherAddressModel.CityID.ToString()))
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.AreaName, Value = m.AreaId.ToString() });
                }
            }
            ViewBag.AreaList = ObjSelectedList;

            return View(objOtherAddressModel);
        }

        public JsonResult UpdateOtherAddress(OtherAddressModel ObjOtherAddressModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";

            OtherAddressDB objOtherAddressDB = new OtherAddressDB();
            operationDetails = objOtherAddressDB.UpdateOtherAddress(ObjOtherAddressModel);
            result = operationDetails.Message;
            if (result == "Success")
            {
                if (ObjOtherAddressModel.AddressInfoId == 0)
                {
                    result = "success";
                    resultMessage = "Other address save successfully";
                }
                else if (ObjOtherAddressModel.AddressInfoId != 0)
                {
                    result = "success";
                    resultMessage = "Other address save successfully";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Technical error has occurred";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAddress(int id)
        {
            OtherAddressDB otherAddressDB = new OtherAddressDB();
            var operationDetails = otherAddressDB.DeleteOtherAddressById(id);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }


        public void ExportToPdf(int EmployeeId)
        {
            string fileName = "OtherAddress" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            OtherAddressDB objOtherAddressDB = new OtherAddressDB();
            System.Data.DataSet ds = objOtherAddressDB.GetAllOtherAddressDataSet(EmployeeId);
            ds.Tables[0].Columns.Remove("CountryID");
            ds.Tables[0].Columns.Remove("StateID");
            ds.Tables[0].Columns.Remove("CityID");
            ds.Tables[0].Columns.Remove("AreaID");
            ds.Tables[0].Columns["Apt No."].ColumnName = "Apartment Name";
            ds.Tables[0].Columns["PoBox"].ColumnName = "Po Box";
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }

        public ActionResult ExportToExcel(int EmployeeId)
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "OtherAddress" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            OtherAddressDB objOtherAddressDB = new OtherAddressDB();
            System.Data.DataSet ds = objOtherAddressDB.GetAllOtherAddressDataSet(EmployeeId);
            ds.Tables[0].Columns.Remove("CountryID");
            ds.Tables[0].Columns.Remove("StateID");
            ds.Tables[0].Columns.Remove("CityID");
            ds.Tables[0].Columns.Remove("AreaID");
            ds.Tables[0].Columns["Apt No."].ColumnName = "Apartment Name";
            ds.Tables[0].Columns["PoBox"].ColumnName = "Po Box";
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }


    }
}