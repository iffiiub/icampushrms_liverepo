﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using HRMS.DataAccess;
using System.Collections;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using HRMS.Entities;
using HRMS.Entities.General;
using System.Data;
using System.Reflection;
using System.Web.Script.Serialization;
using HRMS.DataAccess.GeneralDB;
using System.Configuration;
using System.Threading;
using System.Globalization;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{

    public class BaseController : Controller
    {
        /// <summary>
        /// To show success/error messagesH
        /// </summary>
        IDictionary<string, int> rowIndex = new Dictionary<string, int>();
        bool applyFormating = false;
        public bool HasLogfile { get; set; }
        public string XMLLogFile { get; set; }
        public static string HRMSDateFormat
        {
            get { return ConfigurationManager.AppSettings["HRMSDateFormat"]; }
        }
        //public static string HRMSDateFormatJquery
        //{
        //    get { return ConfigurationManager.AppSettings["HRMSDateFormatJquery"]; }
        //}
        public void ShowStatusMessage(string message)
        {
            ViewBag.message = message;
            TempData["message"] = message;
            HasLogfile = false;
            XMLLogFile = "LoggerDetail.xml";

        }

        protected HtmlHelper HtmlTempHelper
        {
            get
            {
                return new HtmlHelper(new ViewContext(ControllerContext, new FakeView(), ViewData, TempData, TextWriter.Null), new ViewPage());
            }
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((!HttpContext.Request.Url.AbsolutePath.ToLower().Contains("/account/userlogin")))
            {
                if (Session["userContext"] == null)
                {
                    if (HttpContext.Request.IsAjaxRequest())
                        filterContext.Result = new RedirectResult("/Account/AjaxSessionExpire");
                    else
                    {
                        filterContext.Result = new RedirectResult("/Account/UserLogin");
                        TempData["ReturnUrl"] = HttpContext.Request.Url.AbsolutePath.ToLower();   //To redirect the user
                    }
                    return;
                }
                HRMS.Entities.ViewModel.UserContextViewModel userContext = new HRMS.Entities.ViewModel.UserContextViewModel();
                userContext = (HRMS.Entities.ViewModel.UserContextViewModel)Session["userContext"];
                string userName = userContext.Name;
                int userId = userContext.UserId;
                string browserName = DataAccess.GeneralDB.CommonDB.FindBroserName(filterContext.HttpContext.Request.Browser.Type);
                string browserversion = filterContext.HttpContext.Request.Browser.Version;
                DBHelper objDBHelper = new DBHelper();

                List<HRMS.Entities.General.MenuNavigationModel> objMenuNavigationModelList = new List<HRMS.Entities.General.MenuNavigationModel>();
                objMenuNavigationModelList = objDBHelper.GetMenuNavigationList().Where(x => userContext.Permisssion.Contains(x.MainNavigationSubCategoryId) == true).Select(x => x).ToList();
                Session["AccessibleMenuList"] = objMenuNavigationModelList;

                bool flag = false;

                List<string> lstInnerPages = new List<string>();
                lstInnerPages.Add("/pastemployment");
                //lstInnerPages.Add("/InternalEmploymentList");
                //lstInnerPages.Add("/EditInternalEmployment");
                lstInnerPages.Add("/EmergencyContact");
                lstInnerPages.Add("/OtherAddress");
                lstInnerPages.Add("/GetAllEmployee");
                lstInnerPages.Add("/GetAllEmpSearch");
                lstInnerPages.Add("/LeaveType");
                lstInnerPages.Add("/GetAttendanceCountNew");
                lstInnerPages.Add("/GetDailyAttendanceViewList");
                string actionType = HttpContext.Request.HttpMethod;
                dynamic routParameters = filterContext.ActionParameters.FirstOrDefault().Value;
                string controllerName = filterContext.RouteData.Values["controller"].ToString();
                string actionName = filterContext.RouteData.Values["action"].ToString();
                //  bool hasLogFile = false;
                string xmlLogfileName = "";
                var xmlLogfileProp = filterContext.Controller.GetType().GetProperty("XMLLogFile");
                if (xmlLogfileProp != null)
                    xmlLogfileName = (string)xmlLogfileProp.GetValue(this, null);
                else
                    xmlLogfileName = "";

                foreach (var item in objMenuNavigationModelList)
                {
                    if (HttpContext.Request.Url.AbsolutePath.ToLower().Contains(item.URL.ToLower()) || lstInnerPages.Contains(HttpContext.Request.Url.AbsolutePath.ToLower()))
                    {
                        flag = true;
                        if (actionType == "POST")
                            SaveUserLogger(routParameters, item.URL, userName, userId, actionName, browserName, browserversion, item.MainNavigationSubCategoryName, xmlLogfileName);
                    }
                }

                //Check inner pages
                foreach (var item in lstInnerPages)
                {
                    if (HttpContext.Request.Url.AbsolutePath.ToLower().Contains(item.ToLower()))
                    {
                        flag = true;
                        if (actionType == "POST")
                            SaveUserLogger(routParameters, item, userName, userId, actionName, browserName, browserversion, "", xmlLogfileName);
                    }
                }
                if (!flag)
                {
                    List<HRMS.Entities.General.MenuNavigationModel> objListOfByPassPermissionExeceptCreate = new List<HRMS.Entities.General.MenuNavigationModel>();
                    objListOfByPassPermissionExeceptCreate = objDBHelper.GetListOfByPassPermissionExeceptCreate();
                    if (objListOfByPassPermissionExeceptCreate.Where(m =>HttpContext.Request.Url.AbsolutePath.ToLower().Contains(m.URL.ToLower())).ToList().Count > 0&&!(HttpContext.Request.Url.AbsolutePath.ToLower().Contains("create")))
                    {
                        flag = true;
                    }
                }

                if (!flag)
                {
                    filterContext.Result = new RedirectResult("/Home/Index");
                    return;
                }
                else
                {
                    if (!HttpContext.Request.IsAjaxRequest())
                    {
                        RebuildUserContextModel(userContext.UserId);
                    }
                }
            }
        }


        protected bool SaveUserLogger(object obj, string moduleUrl, string username, int userId, string actionName, string browserName, string browserVersion, string modelRefrence, string xmlLogfile)
        {
            bool objReturn = false;
            string nameOfProperty = "";
            string moduleName = "";
            int primaryId = 0;
            UserLogModel userLog = new UserLogModel();
            UserLoggerDB UserLoggerDB = new UserLoggerDB();
            userLog.userId = userId;
            userLog.userName = username;
            userLog.browserName = browserName;
            userLog.browserVersion = browserVersion;
            bool isDelete = false;
            bool directSave = false;
            bool directUpdate = false;
            string DbFileName = "";
            string DbFunctionname = "";
            string FilterParmeter = "";
            int NoOfParameter = 0;
            string DefaultParmeter = "";
            if (obj != null)
            {
                string typename = obj.GetType().FullName;
                if ((typename == "System.String" || typename == "System.Int32") && !(typename.Contains("HRMS")))
                {
                    if (Int32.TryParse(obj.ToString(), out primaryId))
                        isDelete = true;
                }
            }
            //if (obj != null)
           // {
                try
                {
                    foreach (var item in new CommonHelper.CommonHelper().GetLoggerDetailsList(xmlLogfile))
                    {
                        if (moduleUrl.ToLower() == item.ModuleUrl.ToLower())
                        {
                            nameOfProperty = item.NameOfProperty;
                            moduleName = item.ModuleName;
                            DbFileName = item.DBFileName;
                            DbFunctionname = item.DBFunctionName;
                            NoOfParameter = item.NoOfParams;
                            FilterParmeter = item.FilterParameter;
                            DefaultParmeter = item.DefaultParameter;
                            if (item.ActionUrlList != null)
                            {
                                foreach (var iItem in item.ActionUrlList)
                                {
                                    if (actionName.ToLower() == iItem.ActionName.ToLower())
                                    {
                                        if (string.IsNullOrEmpty(item.ModuleRefrence))
                                        {
                                            if (iItem.IsDelete)
                                                isDelete = true;

                                            if (iItem.DirectUpdate)
                                                directUpdate = true;

                                            if (iItem.DirectSave)
                                                directSave = true;

                                            if (!string.IsNullOrEmpty(iItem.NameOfProperty))
                                                nameOfProperty = iItem.NameOfProperty;

                                            if (!string.IsNullOrEmpty(iItem.ModuleName))
                                                moduleName = iItem.ModuleName;

                                            if (iItem.ValueOfProperty == 1)
                                                primaryId = 1;

                                            if (!string.IsNullOrEmpty(iItem.DBFileName))
                                                DbFileName = iItem.DBFileName;

                                            if (!string.IsNullOrEmpty(iItem.DBFunctionName))
                                                DbFunctionname = iItem.DBFunctionName;

                                            if (iItem.NoOfParams != 1)
                                                NoOfParameter = iItem.NoOfParams;

                                            if (!string.IsNullOrEmpty(iItem.FilterParameter))
                                                FilterParmeter = iItem.DBFunctionName;
                                        }
                                        else
                                        {
                                            if (modelRefrence.ToLower() == item.ModuleRefrence.ToLower())
                                            {
                                                if (iItem.IsDelete)
                                                    isDelete = true;

                                                if (iItem.DirectUpdate)
                                                    directUpdate = true;

                                                if (iItem.DirectSave)
                                                    directSave = true;

                                                if (!string.IsNullOrEmpty(iItem.NameOfProperty))
                                                    nameOfProperty = iItem.NameOfProperty;

                                                if (!string.IsNullOrEmpty(iItem.ModuleName))
                                                    moduleName = iItem.ModuleName;

                                                if (iItem.ValueOfProperty == 1)
                                                    primaryId = 1;

                                                if (!string.IsNullOrEmpty(iItem.DBFileName))
                                                    DbFileName = iItem.DBFileName;

                                                if (!string.IsNullOrEmpty(iItem.DBFunctionName))
                                                    DbFunctionname = iItem.DBFunctionName;

                                                if (iItem.NoOfParams != 1)
                                                    NoOfParameter = iItem.NoOfParams;

                                                if (!string.IsNullOrEmpty(iItem.FilterParameter))
                                                    FilterParmeter = iItem.DBFunctionName;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (directSave == true || directUpdate == true)
                        isDelete = false;


                    if (directSave == false && directUpdate == false)
                    {
                    if (obj != null)
                    {
                        if (primaryId == 0)
                        {
                            primaryId = Convert.ToInt32(DataAccess.GeneralDB.CommonDB.GetPropertyValue(obj, nameOfProperty));
                        }
                    }
                    }

                    if (isDelete)
                    {
                        userLog.moduleName = moduleName;
                        userLog.oprationType = "Delete";
                        userLog.Date = DateTime.Now.ToString();
                        userLog.description = "User " + username + " Delete " + moduleName + " on date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    }
                    else
                    {
                        if (directSave)
                        {
                            userLog.moduleName = moduleName;
                            userLog.oprationType = "Save";
                            userLog.Date = DateTime.Now.ToString();
                            userLog.description = "User " + username + " Save " + moduleName + " on date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

                        }
                        else
                        {
                            if (directUpdate == true)
                            {
                                userLog.moduleName = moduleName;
                                userLog.oprationType = "Update";
                                userLog.Date = DateTime.Now.ToString();
                                userLog.description = "User " + username + " Update " + moduleName + " on date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                            }
                            else
                            {
                                if (primaryId <= 0)
                                {
                                    userLog.moduleName = moduleName;
                                    userLog.oprationType = "Save";
                                    userLog.Date = DateTime.Now.ToString();
                                    userLog.description = "User " + username + " Save " + moduleName + " on date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                                }
                                else
                                {
                                    userLog.moduleName = moduleName;
                                    userLog.oprationType = "Update";
                                    userLog.Date = DateTime.Now.ToString();
                                    string description = CompareModule(obj, DbFileName, DbFunctionname, primaryId, NoOfParameter, userLog.moduleName, FilterParmeter, DefaultParmeter);
                                    userLog.description = "User " + username + " Update " + moduleName + description + " on date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                                }
                            }
                        }
                    }
                    if (userLog.moduleName != "")
                        UserLoggerDB.AddUserLog(userLog);
                }
                catch (Exception e)
                {

                }
            //}
            return objReturn;
        }

        public string CompareModule(object obj, string DbFileName, string DbFunctionName, int PrimaryId, int NoOfParameter, string ModuleName, string FilterParameter, string DefaultParameter)
        {
            try
            {
                Type UpdatedType = obj.GetType();
                Type DbType = Type.GetType(DbFileName);
                object myObj = Activator.CreateInstance(Type.GetType(DbFileName));
                int IFilterId = 0;
                MethodInfo DbMethods = DbType.GetMethod(DbFunctionName);
                object OldObject;
                if (NoOfParameter == 1)
                {
                    OldObject = DbMethods.Invoke(myObj, new object[] { PrimaryId });
                }
                else
                {
                    OldObject = DbMethods.Invoke(myObj, new object[] { PrimaryId, Convert.ToInt32(DefaultParameter) });
                }
                if (!string.IsNullOrEmpty(FilterParameter))
                {
                    IFilterId = Convert.ToInt32(DataAccess.GeneralDB.CommonDB.GetPropertyValue(obj, FilterParameter));
                }
                if (!string.IsNullOrEmpty(FilterParameter))
                {
                    if (ModuleName == "Salary Information")
                    {
                        OldObject = ((List<PaySalaryModel>)OldObject).Where(x => x.PaySalaryID == IFilterId).FirstOrDefault();
                    }
                }
                List<string> Description = new List<string>();
                StringBuilder PropertyName = new StringBuilder();
                IList<PropertyInfo> props = new List<PropertyInfo>(UpdatedType.GetProperties());

                foreach (PropertyInfo prop in props)
                {
                    if (prop.PropertyType.IsClass && (prop.PropertyType.FullName.Contains("HRMS.Entities")))
                    {
                        foreach (PropertyInfo propInner in prop.PropertyType.GetProperties())
                        {
                            string tempDescription = CompareProperties(propInner, OldObject, obj, prop.Name);
                            if (!string.IsNullOrEmpty(tempDescription))
                                Description.Add(tempDescription);
                        }
                    }
                    else
                    {
                        string tempDescription = CompareProperties(prop, OldObject, obj, "");
                        if (!string.IsNullOrEmpty(tempDescription))
                            Description.Add(tempDescription);
                    }
                }
                return string.Join(", ", Description.ToArray());
            }
            catch (Exception e)
            {
                return "";
            }

        }

        public IList<PropertyInfo> GetProperty(Type type)
        {
            IList<PropertyInfo> props = new List<PropertyInfo>(type.GetProperties());
            return props;
        }

        public string CompareProperties(PropertyInfo prop, object OldObject, object obj, string innerObjectName)
        {
            try
            {
                string propertyName = "";
                string description = "";
                if (!string.IsNullOrEmpty(innerObjectName))
                {
                    OldObject = OldObject.GetType().GetProperty(innerObjectName).GetValue(OldObject, null);
                    obj = obj.GetType().GetProperty(innerObjectName).GetValue(obj, null);
                }
                foreach (CustomAttributeData customAttr in prop.CustomAttributes)
                {
                    object compareObject = prop.GetValue(obj, null);
                    object compareToObject = prop.GetValue(OldObject, null);
                    if (customAttr.ConstructorArguments.Count == 2)
                    {
                        string MemberName = customAttr.ConstructorArguments[0].Value.ToString();
                        if (customAttr.ConstructorArguments[1].ArgumentType.Name == "Boolean" && Convert.ToBoolean(customAttr.ConstructorArguments[1].Value))
                        {
                            description = MemberName + " updated from " + compareToObject.ToString() + " to " + compareObject.ToString();
                        }
                    }
                    foreach (CustomAttributeTypedArgument argument in customAttr.ConstructorArguments)
                    {
                        if (argument.ArgumentType.Name == "Boolean" && Convert.ToBoolean(argument.Value))
                        {
                            // propertyName = argument.TypedValue.Value.ToString();

                            if (compareObject != null && compareToObject != null)
                            {
                                if (compareObject.ToString() != compareToObject.ToString())
                                {
                                    description = propertyName + " updated from " + compareToObject.ToString() + " to " + compareObject.ToString();
                                }
                            }
                        }
                    }

                    foreach (CustomAttributeNamedArgument argument in customAttr.NamedArguments)
                    {
                        if (argument.MemberName == "Name")
                            propertyName = argument.TypedValue.Value.ToString();
                        if (argument.MemberName == "GroupName")
                        {
                            // object compareObject = prop.GetValue(obj, null);
                            // object compareToObject = prop.GetValue(OldObject, null);
                            if (compareObject != null && compareToObject != null)
                            {
                                if (compareObject.ToString() != compareToObject.ToString())
                                {
                                    description = propertyName + " updated from " + compareToObject.ToString() + " to " + compareObject.ToString();
                                }
                            }
                        }
                    }
                }
                return description;
            }
            catch (Exception e)
            {
                return "";
            }
        }


        /// <summary>
        /// Export content to List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listToExport"></param>
        /// <returns></returns>
        /// 
        public byte[] ExportListToExcel<T>(List<T> listToExport)
        {
            try
            {
                GridView gridvw = new GridView();
                gridvw.DataSource = listToExport; //bind the data table to the grid view
                gridvw.DataBind();
                StringWriter swr = new StringWriter();
                HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
                gridvw.RenderControl(tw);
                byte[] content = Encoding.ASCII.GetBytes(swr.ToString());
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Export content to List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listToExport"></param>
        /// <returns></returns>
        /// 
        public byte[] ExportListToExcel(System.Data.DataTable dtMatrix)
        {
            try
            {
                GridView gridvw = new GridView();
                gridvw.DataSource = dtMatrix; //bind the data table to the grid view
                gridvw.DataBind();
                StringWriter swr = new StringWriter();
                HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
                gridvw.RenderControl(tw);
                byte[] content = Encoding.ASCII.GetBytes(swr.ToString());
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] ExportListToPdf<T>(List<T> listToExport)
        {
            try
            {
                GridView gridvw = new GridView();
                gridvw.DataSource = listToExport; //bind the data table to the grid view
                gridvw.DataBind();
                StringWriter swr = new StringWriter();
                HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
                gridvw.RenderControl(tw);
                byte[] content = Encoding.ASCII.GetBytes(swr.ToString());
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] ExportListToPdf(System.Data.DataTable dtMatrix)
        {
            try
            {
                GridView gridvw = new GridView();
                gridvw.DataSource = dtMatrix; //bind the data table to the grid view
                gridvw.DataBind();
                StringWriter swr = new StringWriter();
                HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
                gridvw.RenderControl(tw);
                byte[] content = Encoding.ASCII.GetBytes(swr.ToString());
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Export content to List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listToExport"></param>
        /// <returns></returns>
        /// 
        string HeaderCols = "";
        int ColumnsCount = 0;
        public byte[] ExportPayrollReportListToExcel(System.Data.DataTable dtMatrix, string HeaderColumnNames)
        {
            try
            {
                string AmountFormat = " ";
                CommonHelper.CommonHelper.GetAmountFormat();
                int decimalcouunt = CommonHelper.CommonHelper.NoOfDecimalPlaces;
                if (decimalcouunt == 0)
                {
                    AmountFormat = " ";
                }
                else if (decimalcouunt == 1)
                {
                    AmountFormat = ".0";
                }
                else if (decimalcouunt == 2)
                {
                    AmountFormat = ".00";
                }
                else if (decimalcouunt == 3)
                {
                    AmountFormat = ".000";
                }
                dtMatrix.Columns.Remove("TopHeader");

                string strHeaderRow = "";
                DataTable dtNewTable = AddHeaderRows(dtMatrix, out strHeaderRow);
                GridView gridvw = new GridView();
                if (!string.IsNullOrEmpty(HeaderColumnNames))
                {
                    HeaderCols = HeaderColumnNames;
                    DataRow newRow = dtNewTable.NewRow();
                    try
                    {
                        string[] mainColumns = HeaderColumnNames.Split(',');
                        int additionsCount = Convert.ToInt32(mainColumns[2]);
                        int DeductionsCount = Convert.ToInt32(mainColumns[3]);
                        int k = dtMatrix.Columns["Gross"].Ordinal;
                        newRow[5] = mainColumns[0];
                        int j = dtMatrix.Columns["Net Payble"].Ordinal;
                        int l = (j - DeductionsCount);
                        newRow[l] = mainColumns[1];
                        newRow["Active"] = "14-" + k + "-" + j + "-" + (additionsCount + 1) + "-" + (DeductionsCount + 1);
                    }
                    catch (Exception ex)
                    {

                    }
                    dtNewTable.Rows.InsertAt(newRow, 1);
                    ColumnsCount = dtNewTable.Columns.Count;
                    gridvw.RowDataBound += new GridViewRowEventHandler(gr_RowDataBoundNew);
                    dtNewTable.Rows.RemoveAt(0);
                    gridvw.DataSource = dtNewTable; //bind the data table to the grid view
                    gridvw.DataBind();
                }
                else
                {
                    gridvw.RowDataBound += new GridViewRowEventHandler(gr_RowDataBound);
                    gridvw.DataSource = dtNewTable; //bind the data table to the grid view
                    gridvw.DataBind();
                }
                for (int i = 0; i < gridvw.Columns.Count; i++)
                {
                    gridvw.HeaderRow.Cells.RemoveAt(i);
                }
                gridvw.ShowHeader = false;
                gridvw.HeaderRow.Visible = false;
                StringWriter swr = new StringWriter();
                swr.Write("<style> .cost{mso-number-format:\"\\#,\\#0\\" + AmountFormat + "\";} </style> ");
                HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
                gridvw.RenderControl(tw);
                string strContent = swr.ToString();
                byte[] content = Encoding.Unicode.GetBytes(strContent);
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable AddHeaderRows(DataTable dtOldData, out string strHeaderRow)
        {
            DataTable dtNewTable = dtOldData.Clone();
            strHeaderRow = "<tr>\r\n\t\t\t";
            for (int i = 0; i < dtOldData.Columns.Count; i++)
            {
                dtNewTable.Columns[i].DataType = typeof(string);
                dtNewTable.Columns[i].MaxLength = 100;
                if (dtNewTable.Columns[i].ColumnName == "header")
                {
                    strHeaderRow += "<th scope=\"col\"></th>";
                }
                else
                {
                    strHeaderRow += "<th scope=\"col\">" + dtOldData.Columns[i].ColumnName + "</th>";
                }

            }
            strHeaderRow += "\r\n\t\t</tr>";

            foreach (DataRow row in dtOldData.Rows)
            {
                dtNewTable.ImportRow(row);
            }
            try
            {
                int rowCount = dtOldData.Rows.Count;
                for (int r = 0; r < rowCount; r++)
                {
                    if (dtNewTable.Rows[r]["active"].ToString() == "2" && dtNewTable.Rows[r - 1]["active"].ToString() != "2")
                    {
                        DataRow headerRow = dtNewTable.NewRow();
                        for (int c = 0; c < dtOldData.Columns.Count; c++)
                        {
                            headerRow[c] = dtOldData.Columns[c].ColumnName;
                        }
                        dtNewTable.Rows.InsertAt(headerRow, r);
                        r++;
                        rowCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            return dtNewTable;
        }

        //protected void gr_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    bool PayActiveAll = false;
        //    //PagePermission _pagePermission = null;
        //    int Active = 0;
        //    int Net = 0;
        //    int AddTotal = 0;
        //    int DedTotal = 0;
        //    int SalTotal = 0;
        //    int offset = 0;

        //    GridViewRow drHeader = null;
        //    CheckBox chkPayActiveAll = null;

        //    GridViewRow dr = e.Row as GridViewRow;
        //    ArrayList HeaderRows = new ArrayList();

        //    //   e.Row.BackColor = System.Drawing.Color.YellowGreen;

        //    if (dr.RowIndex == -1)   //header Row
        //    {

        //        //  dr.RowIndex[-1].
        //        offset = 0;
        //        Active = dr.Cells.Count - 1;
        //        Net = dr.Cells.Count - 2;
        //        AddTotal = -1;
        //        DedTotal = -1;
        //        SalTotal = -1;
        //        for (int i = 0; i < dr.Cells.Count; i++)
        //        {
        //            //  dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");//Header   System.Drawing.Color.Red;

        //            if (dr.Cells[i].Text == "Add Total")
        //            {
        //                AddTotal = i;
        //                break;
        //            }
        //        }
        //        for (int i = 0; i < dr.Cells.Count; i++)
        //        {

        //            if (dr.Cells[i].Text == "Ded Total")
        //            {
        //                DedTotal = i;
        //                break;
        //            }
        //        }
        //        for (int i = 0; i < dr.Cells.Count; i++)
        //        {

        //            if (dr.Cells[i].Text == "Sal Total")
        //            {
        //                SalTotal = i;
        //                break;
        //            }
        //        }

        //        for (int j = 0; j < dr.Cells.Count - 1; j++)
        //        {
        //            //drHeader.Cells[j].Text = dr.Cells[j].Text;
        //            HeaderRows.Add(dr.Cells[j].Text);
        //        }

        //        for (int i = 0; i < dr.Cells.Count; i++)
        //        {
        //            dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");//Header   System.Drawing.Color.Red;

        //        }

        //    }
        //    if (dr.RowIndex != -1)
        //    {
        //        //if (dr.Cells[Active].Text != "8" && dr.Cells[Active].Text != "9" && dr.Cells[Active].Text != "10")
        //        //{
        //        //    if (SalTotal != -1)
        //        //        e.Row.Cells[SalTotal].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99"); // System.Drawing.Color.YellowGreen;
        //        //    if (DedTotal != -1)
        //        //        e.Row.Cells[DedTotal].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99"); // System.Drawing.Color.YellowGreen;
        //        //    if (AddTotal != -1)
        //        //        e.Row.Cells[AddTotal].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");  // System.Drawing.Color.YellowGreen;
        //        //    if (Net != -1)
        //        //        e.Row.Cells[Net].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");  // System.Drawing.Color.GreenYellow;
        //        //}
        //        //if (dr.Cells[Active].Text == "8")
        //        //{

        //        //    for (int i = 0; i < dr.Cells.Count; i++)
        //        //    {
        //        //        dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");//Header   System.Drawing.Color.Red;

        //        //    }

        //        //}
        //        //       dr.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");  // System.Drawing.Color.Brown;
        //        //if (dr.Cells[Active].Text == "9")
        //        //    dr.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");  // System.Drawing.Color.LimeGreen;
        //        //if (dr.Cells[Active].Text == "10")
        //        //    dr.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");  // System.Drawing.Color.Cyan;


        //        //if (dr.Cells[Active].Text == "8" || dr.Cells[Active].Text == "9" || dr.Cells[Active].Text == "10")
        //        //{
        //        //    for (int i = 0; i < dr.Cells.Count; i++)
        //        //    {
        //        //        e.Row.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
        //        //    }
        //        //}
        //        //if (dr.Cells[Active].Text == "7")
        //        //{
        //        //    for (int i = 0; i < dr.Cells.Count - 1; i++)
        //        //    {
        //        //        dr.Cells[i].BorderWidth = 0;
        //        //     //   dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");
        //        //        dr.Cells[i].Font.Bold = true;
        //        //        dr.Cells[i].Font.Italic = true;
        //        //    }
        //        //}
        //        //dr.Cells[Active].Width = 0;
        //        //dr.Cells[Active].Text = "";


        //        for (int i = 0; i < dr.Cells.Count; i++)
        //        {

        //            if (dr.Cells[19].Text == "8" || dr.Cells[19].Text == "9" || dr.Cells[19].Text == "10" )
        //            {
        //                dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");//Header   System.Drawing.Color.Red;

        //            }

        //            if (dr.Cells[19].Text == "7" || dr.Cells[19].Text == "2")
        //            {


        //                dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");//Header   System.Drawing.Color.Red;


        //            }

        //        }
        //    }
        //}


        //Rohit's

        int EmpIdColumnPos = 0;
        int EarnedDayColumnPos = 0;
        int UniqueIdColumnPos = 0;
        int HireDatePos = 0;
        int AgentIDColumnPos = 0;

        protected void gr_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int Active = 19;
                int Net = 0;
                int AddTotal = 0;
                int DedTotal = 0;
                int SalTotal = 0;


                GridViewRow dr = e.Row as GridViewRow;

                if (dr.RowIndex != -1)
                {
                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        if (dr.Cells[i].Text == "Employee ID" || dr.Cells[i].Text == "EmpID")
                        {
                            EmpIdColumnPos = i;
                            break;
                        }
                    }
                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        if (dr.Cells[i].Text == "Hire Date" || dr.Cells[i].Text == "Joining Date")
                        {
                            HireDatePos = i;
                            break;
                        }
                    }

                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        if (dr.Cells[i].Text == "Earned Days")
                        {
                            EarnedDayColumnPos = i;
                            break;
                        }

                    }

                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        if (dr.Cells[i].Text == "Employee Unique ID")
                        {
                            UniqueIdColumnPos = i;
                            break;
                        }

                    }

                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        if (dr.Cells[i].Text == "Agent ID")
                        {
                            AgentIDColumnPos = i;
                            break;
                        }

                    }

                    if (dr.Cells[0].Text == "header") //(dr.RowIndex == -1)   //header Row
                    {
                        Active = dr.Cells.Count - 1;
                        Net = dr.Cells.Count - 2;
                        AddTotal = -1;
                        DedTotal = -1;
                        SalTotal = -1;

                        for (int i = 0; i < dr.Cells.Count; i++)
                        {

                            if (dr.Cells[i].Text == "Add Total")
                            {
                                AddTotal = i;
                                if (!applyFormating)
                                    rowIndex.Add("Add Total", i);
                                break;
                            }
                        }
                        for (int i = 0; i < dr.Cells.Count; i++)
                        {

                            if (dr.Cells[i].Text == "Ded Total")
                            {
                                DedTotal = i;
                                if (!applyFormating)
                                    rowIndex.Add("Ded Total", i);
                                break;
                            }

                        }
                        for (int i = 0; i < dr.Cells.Count; i++)
                        {

                            if (dr.Cells[i].Text == "Net")
                            {
                                if (!applyFormating)
                                    rowIndex.Add("Net", i);
                                break;
                            }

                        }
                        for (int i = 0; i < dr.Cells.Count; i++)
                        {
                            if (dr.Cells[i].Text == "Sal Total")
                            {
                                SalTotal = i;
                                if (!applyFormating)
                                    rowIndex.Add("Sal Total", i);
                                applyFormating = true;
                                break;
                            }
                        }

                        for (int i = 0; i < dr.Cells.Count; i++)
                        {
                            dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");
                            dr.Cells[i].Font.Bold = true;
                        }
                    }

                    Active = dr.Cells.Count - 1;

                    if (dr.Cells[Active].Text != "8" && dr.Cells[Active].Text != "9" && dr.Cells[Active].Text != "10" && dr.Cells[0].Text != "header")
                    {
                        if (SalTotal != -1)
                            e.Row.Cells[SalTotal].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                        if (DedTotal != -1)
                            e.Row.Cells[DedTotal].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                        if (AddTotal != -1)
                            e.Row.Cells[AddTotal].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                        if (Net != -1)
                            e.Row.Cells[Net].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                    }

                    if (dr.Cells[Active].Text == "8")
                    {
                        for (int i = 0; i < dr.Cells.Count; i++)
                        {
                            dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                        }
                    }

                    if (dr.Cells[Active].Text == "9")
                    {
                        if (dr.Cells[Active].Text == "10")
                        {
                            if (dr.Cells[Active].Text == "8" || dr.Cells[Active].Text == "9" || dr.Cells[Active].Text == "10")
                            {
                                for (int i = 0; i < dr.Cells.Count; i++)
                                {
                                    e.Row.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                                }
                            }
                        }
                    }

                    if (dr.Cells[Active].Text == "7")
                    {
                        string tempName = string.Empty;
                        for (int i = 0; i < dr.Cells.Count - 1; i++)
                        {
                            dr.Cells[i].BorderWidth = 0;
                            dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");
                            dr.Cells[i].Font.Bold = true;
                            dr.Cells[i].Font.Italic = true;
                            if (dr.Cells[i].Text.Length > 3 && dr.Cells[i].Text.Trim() != "&nbsp;")
                            {

                                tempName = dr.Cells[i].Text;
                                dr.Cells[i].Text = "";
                                dr.Cells[0].Text = tempName;
                            }
                        }
                        dr.Cells[0].ColumnSpan = 3;
                        dr.Cells[1].Visible = false;
                        dr.Cells[2].Visible = false;
                    }

                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        if (dr.Cells[Active].Text == "8" || dr.Cells[Active].Text == "9" || dr.Cells[Active].Text == "10")
                        {
                            dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                        }
                        if (dr.Cells[Active].Text == "7" || dr.Cells[Active].Text == "2")
                        {
                            dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                            if (dr.Cells[Active].Text == "2")
                            {
                                foreach (var item in rowIndex)
                                {
                                    dr.Cells[item.Value].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                                }
                            }
                        }
                        dr.Cells[dr.Cells.Count - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                    }

                    dr.Cells[Active].Width = 0;
                    dr.Cells[Active].Text = "";
                    if (dr.Cells[0].Text == "Sl No")
                    {
                        dr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    }

                    if (dr.Cells[0].Text == "header")
                    {
                        dr.Cells[0].Text = "";
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        for (int i = 1; i < e.Row.Cells.Count; i++)
                        {
                            if (i != EmpIdColumnPos && i != EarnedDayColumnPos && i != HireDatePos && i != UniqueIdColumnPos)
                            {
                                e.Row.Cells[i].Attributes.Add("class", "cost");
                            }

                            if (i == UniqueIdColumnPos || i == AgentIDColumnPos)
                            {
                                if (e.Row.Cells[i].Text != "Employee Unique ID" && e.Row.Cells[i].Text != "Agent ID")
                                    e.Row.Cells[i].Text = "=" + "\"" + e.Row.Cells[i].Text + "\"";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        int ActiveColPos = 0;
        protected void gr_RowDataBoundNew(object sender, GridViewRowEventArgs e)
        {
            int Net = 0;

            GridViewRow dr = e.Row as GridViewRow;

            // if (dr.RowIndex != -1)
            {
                for (int i = 0; i < dr.Cells.Count; i++)
                {
                    if (dr.Cells[i].Text == "Employee ID" || dr.Cells[i].Text == "EmpID" || dr.Cells[i].Text == "Emp ID")
                    {
                        EmpIdColumnPos = i;
                        break;
                    }
                }

                for (int i = 0; i < dr.Cells.Count; i++)
                {
                    if (dr.Cells[i].Text.ToLower() == "active")
                    {
                        ActiveColPos = i;
                        break;
                    }
                }

                for (int i = 0; i < dr.Cells.Count; i++)
                {
                    if (dr.Cells[i].Text == "Earned Days")
                    {
                        EarnedDayColumnPos = i;
                        break;
                    }
                }
                if (dr.Cells[0].Text == "Sl No") //(dr.RowIndex == -1)   //header Row
                {
                    ActiveColPos = dr.Cells.Count - 1;
                    Net = dr.Cells.Count - 2;

                    for (int i = 0; i < dr.Cells.Count; i++)
                    {
                        dr.Cells[i].Font.Bold = true;
                    }
                }

                if (dr.Cells[ActiveColPos].Text == "7")
                {
                    for (int i = 0; i < dr.Cells.Count - 1; i++)
                    {
                        if (dr.Cells[i].Text != "&nbsp;" && dr.Cells[i].Text != " ")
                        {
                            dr.Cells[i].RowSpan = 2;
                            dr.Cells[i].ColumnSpan = ColumnsCount - 1;
                            dr.Cells[i].BorderWidth = 0;
                            dr.Cells[i].HorizontalAlign = HorizontalAlign.Left;
                        }
                        else
                        {
                            dr.Cells[i].Visible = false;
                            dr.Cells[i].BorderWidth = 0;
                        }

                        // dr.Cells[i].BorderWidth = 0;
                        dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");
                        dr.Cells[i].Font.Bold = true;
                        dr.Cells[i].Font.Italic = true;
                        // dr.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        dr.Cells[i].VerticalAlign = VerticalAlign.Top;
                    }
                }

                if (dr.Cells[dr.Cells.Count - 1].Text.Contains("14-"))
                {
                    string[] sizeArr = dr.Cells[dr.Cells.Count - 1].Text.Split('-');
                    if (sizeArr.Length == 5)
                    {
                        int j = Convert.ToInt32(sizeArr[1]);
                        int k = Convert.ToInt32(sizeArr[2]);

                        for (int i = 2; i < k; i++)
                        {
                            dr.Cells[i].Font.Bold = true;
                            dr.Cells[i].BorderColor = System.Drawing.ColorTranslator.FromHtml("#000000");

                            if (dr.Cells[i].Text != "&nbsp;" && dr.Cells[i].Text == HeaderCols.Split(',')[0])
                            {
                                for (int kk = i + 1; kk <= (i + Convert.ToInt32(sizeArr[3])); kk++)
                                {
                                    dr.Cells[kk].Visible = false;
                                }
                                dr.Cells[i].ColumnSpan = Convert.ToInt32(sizeArr[3]);
                                dr.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                            }

                            if (dr.Cells[i].Text != "&nbsp;" && dr.Cells[i].Text == HeaderCols.Split(',')[1])
                            {
                                for (int kk = j + 3; kk <= ((j + Convert.ToInt32(sizeArr[4]))); kk++)
                                {
                                    dr.Cells[kk].Visible = false;
                                }

                                dr.Cells[i].ColumnSpan = Convert.ToInt32(sizeArr[4]);
                                dr.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                            }
                        }
                    }

                }


                for (int i = 0; i < dr.Cells.Count; i++)
                {
                    if (dr.Cells[ActiveColPos].Text == "8" || dr.Cells[ActiveColPos].Text == "9" || dr.Cells[ActiveColPos].Text == "10")
                    {
                        dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                    }
                    if (dr.Cells[ActiveColPos].Text == "7" || dr.Cells[ActiveColPos].Text == "2")
                    {
                        dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                        if (dr.Cells[ActiveColPos].Text == "2")
                        {
                            foreach (var item in rowIndex)
                            {
                                dr.Cells[item.Value].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                            }
                        }
                    }
                    dr.Cells[dr.Cells.Count - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCC99");
                }

                for (int i = 0; i < dr.Cells.Count; i++)
                {
                    dr.Cells[i].BorderColor = System.Drawing.ColorTranslator.FromHtml("#488AC7");
                    if (i == dr.Cells.Count - 1)
                    {
                        //dr.Cells[i].Text = "";
                        //dr.Cells[i].BorderColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        //dr.Cells[i].BorderWidth = 0;
                        dr.Cells[i].Visible = false;
                    }
                    if (i == dr.Cells.Count - 2)
                    {
                        dr.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCBA4");
                        dr.Cells[i].BorderColor = System.Drawing.ColorTranslator.FromHtml("#488AC7");
                    }
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    for (int i = 1; i < e.Row.Cells.Count; i++)
                    {
                        if (i != EmpIdColumnPos && i != EarnedDayColumnPos)
                        {
                            e.Row.Cells[i].Attributes.Add("class", "cost");
                        }

                    }

                }
            }
        }

        public void ExportBankListToExcel1(List<BankListReport> lstBankList)
        {
            try
            {
                var SourcetemplateFilePath = Server.MapPath("/CommonHelper/SalaryTemplate.xlsx");
                var DestinationFilePath = Server.MapPath("/Uploads/BankListReports/SalaryTemplate.xlsx");
                System.IO.File.Copy(SourcetemplateFilePath, DestinationFilePath, true);

                DataTable dt = ToDataTable(lstBankList);

                // Create cell reference array 
                string[] CellReferenceArray = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" };
                //Open your saved excel file that you have created using template file.
                using (SpreadsheetDocument myDoc = SpreadsheetDocument.Open(DestinationFilePath, true))
                {
                    // Create reference of main Workbook part, which contains all reference.
                    WorkbookPart objworkbook = myDoc.WorkbookPart;

                    // Create style sheet object that will be used for applying styling.
                    Stylesheet objstyleSheet = objworkbook.WorkbookStylesPart.Stylesheet;

                    // pick up first worksheet
                    WorksheetPart objworksheet = objworkbook.WorksheetParts.First();

                    // will be used in end while creating sheet data
                    string objorigninalSheetId = objworkbook.GetIdOfPart(objworksheet);
                    WorksheetPart objreplacementPart = objworkbook.AddNewPart<WorksheetPart>();
                    string objreplacementPartId = objworkbook.GetIdOfPart(objreplacementPart);

                    // Create object reader to read from excel file.
                    OpenXmlReader objreader = OpenXmlReader.Create(objworksheet);

                    // create writer object to write in excel sheet.
                    OpenXmlWriter objOpenXmwriter = OpenXmlWriter.Create(objreplacementPart);

                    int i = 1;
                    Row r = new Row();
                    Cell c = new Cell();
                    Columns col1 = new Columns();
                    UInt32 index = 1;

                    while (objreader.Read())
                    {
                        if (objreader.ElementType == typeof(SheetData))
                        {
                            if (objreader.IsEndElement)
                                continue;

                            objOpenXmwriter.WriteStartElement(new SheetData());
                            objOpenXmwriter.WriteStartElement(r);

                            // Loop to insert header
                            foreach (DataColumn colHead in dt.Columns)
                            {
                                c = new Cell
                                {
                                    DataType = CellValues.String,
                                    CellReference = CellReferenceArray[i] + Convert.ToString(index)
                                };
                                CellValue v1 = new CellValue(colHead.ColumnName.ToString());
                                c.Append(v1);
                                objOpenXmwriter.WriteElement(c);
                                i += 1;
                            }
                            objOpenXmwriter.WriteEndElement();
                            index += 1;

                            //Loop to insert datatable row in excel	
                            foreach (DataRow dr in dt.Rows)
                            {
                                objOpenXmwriter.WriteStartElement(r);
                                i = 1;
                                foreach (DataColumn col in dt.Columns)
                                {
                                    c = new Cell
                                    {
                                        DataType = CellValues.String,
                                        CellReference = CellReferenceArray[i] + Convert.ToString(index)
                                    };
                                    CellValue v1 = new CellValue(dr[col].ToString());
                                    c.AppendChild(v1);
                                    objOpenXmwriter.WriteElement(c);
                                    i += 1;
                                }
                                objOpenXmwriter.WriteEndElement();
                                index += 1;
                            }
                            objOpenXmwriter.WriteEndElement();
                        }
                    }

                    //close all objects
                    objreader.Close();
                    objOpenXmwriter.Close();

                    Sheet sheet = objworkbook.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(objorigninalSheetId)).First();
                    sheet.Id.Value = objreplacementPartId;
                    objworkbook.DeletePart(objworksheet);

                }
                //download created excel sheet.
                Response.AddHeader("Content-Disposition", "inline;filename=BankList.xls");
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.WriteFile(DestinationFilePath);
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, prop.PropertyType);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public void ExportBankListToExcel(List<BankListReport> lstBankList)
        {
            var templatePath = Server.MapPath("/CommonHelper/SalTemplate.xlsx");
            var resultPath = Server.MapPath("/Uploads/BankListReports/SalTemplate.xlsx");

            //var templatePath = @"C:\Temp\template.xlsx";
            //var resultPath = @"C:\Temp\result.xlsx";

            string replacementText = "newtest";

            using (Stream xlsxStream = new MemoryStream())
            {
                // Read template from disk
                using (var fileStream = System.IO.File.OpenRead(templatePath))
                    fileStream.CopyTo(xlsxStream);

                // Do replacements
                ProcessTemplate(xlsxStream, lstBankList);

                // Reset stream to beginning
                xlsxStream.Seek(0L, SeekOrigin.Begin);

                // Write results back to disk
                using (var resultFile = System.IO.File.Create(resultPath))
                    xlsxStream.CopyTo(resultFile);
            }

            //download created excel sheet.
            Response.AddHeader("Content-Disposition", "inline;filename=BankList.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.WriteFile(resultPath);
            Response.End();
        }

        private static void ProcessTemplate(Stream template, List<BankListReport> lstBankList)
        {
            using (var workbook = SpreadsheetDocument.Open(template, true, new OpenSettings { AutoSave = true }))
            {
                // Replace shared strings
                SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                IEnumerable<Text> sharedStringTextElements = sharedStringsPart.SharedStringTable.Descendants<Text>();
                DoReplace(sharedStringTextElements, lstBankList);

                // Replace inline strings
                IEnumerable<WorksheetPart> worksheetParts = workbook.GetPartsOfType<WorksheetPart>();
                foreach (var worksheet in worksheetParts)
                {
                    var allTextElements = worksheet.Worksheet.Descendants<Text>();
                    DoReplace(allTextElements, lstBankList);
                }

            } // AutoSave enabled
        }

        private static void DoReplace(IEnumerable<Text> textElements, List<BankListReport> lstBankList)
        {
            foreach (var text in textElements)
            {
                if (text.Text.Contains("companyname"))
                    text.Text = text.Text.Replace("companyname", "test");

                if (text.Text.Contains("remarks"))
                    text.Text = text.Text.Replace("remarks", lstBankList[0].PayCycleName);

                for (int i = 0; i < lstBankList.Count; i++)
                {
                    if (text.Text.Contains("empid" + (i + 1)))
                        text.Text = text.Text.Replace("empid" + (i + 1), lstBankList[i].EmployeeID.ToString());
                    else
                        text.Text = text.Text.Replace("empid" + (i + 1), "");

                    if (text.Text.Contains("empname" + (i + 1)))
                        text.Text = text.Text.Replace("empname" + (i + 1), lstBankList[i].EmpName);
                    else
                        text.Text = text.Text.Replace("empname" + (i + 1), "");

                    if (text.Text.Contains("bankacct" + (i + 1)))
                        text.Text = text.Text.Replace("bankacct" + (i + 1), lstBankList[i].AccountNumber);
                    else
                        text.Text = text.Text.Replace("bankacct" + (i + 1), "");

                    if (text.Text.Contains("amount" + (i + 1)))
                        text.Text = text.Text.Replace("amount" + (i + 1), lstBankList[i].AccountNumber);
                    else
                        text.Text = text.Text.Replace("amount" + (i + 1), "");
                }
            }
        }

        public static string DataTableToJSONWithJavaScriptSerializer(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        public void RebuildUserContextModel(int EmployeeId)
        {
            var userModel = new UserDB().GetUserPermissionById(EmployeeId);
            HRMS.Entities.ViewModel.UserContextViewModel userContext = new HRMS.Entities.ViewModel.UserContextViewModel();
            userContext.Name = userModel.Username;
            userContext.Role = ((HRMS.Web.CommonHelper.UserRole)userModel.UserRoleId).ToString();
            userContext.UserRolls = userModel.UserRoles;
            userContext.UserRoleId = userModel.UserRoleId;
            userContext.UserId = userModel.UserId;
            userContext.Password = userModel.Password;
            userContext.CompanyId = userModel.CompanyId;
            userContext.ViewPagesSubCategoryIDs = userModel.ViewPagesSubCategoryIDs;
            userContext.AddPagesSubCategoryIDs = userModel.AddPagesSubCategoryIDs;
            userContext.UpdatePagesSubCategoryIDs = userModel.UpdatePagesSubCategoryIDs;
            userContext.DeletePagesSubCategoryIDs = userModel.DeletePagesSubCategoryIDs;
            userContext.OtherPermissionIDs = userModel.OtherPermissionIDs;
            char[] seperator = new char[] { ',' };
            string[] PermissionArray = userModel.Permission.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            List<int> PermissionList = new List<int>();
            foreach (var item in PermissionArray)
            {
                PermissionList.Add(Convert.ToInt32(item));
            }
            userContext.Permisssion = PermissionList;
            string[] ReadonlyPagesArray = userModel.ViewPagesSubCategoryIDs.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            List<int> ReadonlyPagesList = new List<int>();
            foreach (var item in ReadonlyPagesArray)
            {
                ReadonlyPagesList.Add(Convert.ToInt32(item));
            }
            userContext.ReadonlyPages = ReadonlyPagesList;
            Session["userContext"] = userContext;
        }
        //*** F8
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;
            
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = null;
            if (Request.Url.Segments.Length>1 &&  Request.Url.Segments[1].ToUpper()=="PDRP/")
            {
                if (Request.Url.Segments.Count() > 2 && (Request.Url.Segments[2].ToLower() == "dropins/" || Request.Url.Segments[2].ToLower() == "kpis/" || Request.Url.Segments[2].ToLower() == "observations/" || Request.Url.Segments[2].ToLower() == "appraisalteaching/"))
                {
                    //*** Danny 14/04/2020 Remove Arabic from Dropin Manage Form
                    if (Request.Url.Segments.Count() > 3 && (Request.Url.Segments[3].ToLower() != "dropinmanage" && Request.Url.Segments[3].ToLower() != "getallteachingemployee" && Request.Url.Segments[3].ToLower() != "observationprocess" && Request.Url.Segments[3].ToLower() != "index" && Request.Url.Segments[3].ToLower() != "admindropins" && Request.Url.Segments[3].ToLower() != "mydropins" && Request.Url.Segments[3].ToLower() != "setdropinsactiveemployeeidsesson" && Request.Url.Segments[3].ToLower() != "setobservationvisitdatemulty"))
                    {
                        cultureCookie = Request.Cookies["_HRMSculture"];
                    }
                }
                //if (Request.Url.Segments[2].ToLower() == "dropins/" || Request.Url.Segments[2].ToLower() == "kpis/" || Request.Url.Segments[2].ToLower() == "observations/" || Request.Url.Segments[2].ToLower() == "appraisalteaching/")
                //{
                //    //*** Danny 14/04/2020 Remove Arabic from Dropin Manage Form
                //    if (Request.Url.Segments.Count() > 2 && (Request.Url.Segments[3].ToLower() != "dropinmanage" && Request.Url.Segments[3].ToLower() != "getallteachingemployee"))
                //    {
                //        cultureCookie = Request.Cookies["_HRMSculture"];
                //    }
                //}
            }
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                //cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                //        Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                //        null;
                cultureName = "en=GB";

            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures 
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            //Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
            return base.BeginExecuteCore(callback, state);
        }
        public ActionResult SetCulture(int languageid)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                CommonDB CommonDB = new CommonDB();
                List<XMLLanguages> XMLLanguagesList = CommonDB.GetLanguageList(languageid);

                if (XMLLanguagesList != null)
                {
                    Session["UserLanguageSelection"] = XMLLanguagesList[0];
                    //new LanguageMang().SetLanguage("ar");
                    //return RedirectToAction("Index", "Home");
                }
                else
                {
                    var DefaultLanguage = ConfigurationManager.AppSettings["DefaultLanguage"];


                }

                // Validate input
                string culture = CultureHelper.GetImplementedCulture(XMLLanguagesList[0].LanguageCulture);
                // Save culture in a cookie
                HttpCookie cookie = Request.Cookies["_HRMSculture"];
                if (cookie != null)
                    cookie.Value = culture;   // update cookie value
                else
                {
                    cookie = new HttpCookie("_HRMSculture");
                    cookie.Value = culture;
                    cookie.Expires = DateTime.Now.AddYears(1);
                }
                Response.Cookies.Add(cookie);

                operationDetails.CssClass = "success";
                operationDetails.Success = true;
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        //*** Naresh 2020-03-17 Check view level access
        protected bool CheckViewLevelAccess()
        {
            string actionName = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            var userContextViewModel = (UserContextViewModel)Session["userContext"];
            var dBHelper = new HRMS.DataAccess.DBHelper();
            var menuNavigationModels = dBHelper.GetMenuNavigationList();
            var menuNavigationItem = menuNavigationModels.FirstOrDefault(m => HttpContext.Request.Url.AbsolutePath.ToLower().Contains(m.URL.ToLower()) && m.ViewAccess.Equals(actionName));
            if (menuNavigationItem != null && userContextViewModel.Permisssion.Contains(menuNavigationItem.MainNavigationSubCategoryId))
                return true;
            return false;
        }

        protected ActionResult RedirectToHome()
        {
            return RedirectToAction("Index", "Home", new { area = "" });
        }
        protected SelectList GetCompanySelectList()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return new SelectList(new CompanyDB().GetCompaniesForDropdown(objUserContextViewModel.UserId), "CompanyID", "CompanyName");
        }
        protected SelectList GetCompanySelectListWithCurrentCompany()
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return new SelectList(new CompanyDB().GetCompaniesForDropdown(objUserContextViewModel.UserId), "CompanyID", "CompanyName", objUserContextViewModel.CompanyId);
        }
        protected SelectList GetCompanySelectList(int companyId)
        {
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return new SelectList(new CompanyDB().GetCompaniesForDropdown(objUserContextViewModel.UserId).Where(c => c.CompanyID == companyId),"CompanyID","CompanyName");
        }
        //*** Naresh 2020-03-30 Do not populate the departments, it will be populated based on company selection
        [HttpGet]
        public ActionResult GetDepartments(int companyId)
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            if (companyId > 0)
                objDepartmentList = objDepartmentDB.GetAllDepartment(companyId: companyId, userId: objUserContextViewModel.UserId);
            var result = objDepartmentList.Select(d => new { id = d.DepartmentId, name = d.DepartmentName_1 }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetEmployees(int companyId)
        {
            DepartmentDB objDepartmentDB = new DepartmentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmployeeDB = new EmployeeDB();
            var employees = new List<EmployeeDetailsModel>();

            List<HRMS.Entities.DepartmentModel> objDepartmentList = new List<HRMS.Entities.DepartmentModel>();
            if (companyId > 0)
                employees = objEmployeeDB.GetAllEmployeeByCompanyAndUser(CompanyID: companyId, UserID: objUserContextViewModel.UserId);
            var result = employees.Select(d => new { id = d.EmployeeID, name = d.FullName }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        internal class FakeView : IView
        {
            public void Render(ViewContext viewContext, TextWriter writer)
            {
                throw new InvalidOperationException();
            }
        }
    }
}
