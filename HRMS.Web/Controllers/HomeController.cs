﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using System.Drawing;
using System.Threading.Tasks;
using System.Data;
using HRMS.Web.CommonHelper;

namespace HRMS.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {            
            DashBoardModel dashBoardModel = new DashBoardModel();
            DashBoardDB dashBoardDB = new DashBoardDB();
            AdditionPaidCycleDB objAdditionPadiCycleDB = new AdditionPaidCycleDB();
            DeductionDB objDeductionDB = new DeductionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int mode = (objUserContextViewModel.UserRolls.Any(x => x.ToLower().Equals("admin")) ? 1 : 2);
            ViewBag.DashboarFlashforAdditions = objAdditionPadiCycleDB.GetDashboardFlashForAdditions();
            ViewBag.DashboarFlashforDeductions = objDeductionDB.GetDashboardFlashForDeductions();
            ViewBag.EmployeeByDepartment = dashBoardDB.GetEmployeeByDepartmentList();
            List<DashBoardModel> dashBoardModelList = new List<DashBoardModel>();
            dashBoardModelList = dashBoardDB.GetEmployeeStatusList();
            ViewBag.EmployeeStatusList = dashBoardModelList;
            ViewBag.ActiveEmployeesCount = dashBoardModelList.Where(m => m.isActive == "Active").Select(m => m.Strength).FirstOrDefault();
            ViewBag.EmployeeGender = dashBoardDB.GetEmployeeWithGender();
            ViewBag.EmployeeWithNoLabourCardList = dashBoardDB.GetEmployeeWithNoLabourCardList();
            ViewBag.SectionEmployeeList = dashBoardDB.GetSectionAndEmployeeList(objUserContextViewModel.CompanyId);
            ViewBag.CountryLocation = dashBoardDB.GetEmployeeCountry(objUserContextViewModel.CompanyId);
            DocumentDB documentdb = new DocumentDB();
            //List<string> documentSettingday = documentdb.GetDocumentsettingDays();
            int UserId = objUserContextViewModel.UserId;
            ViewBag.ExpiredDocumentsList = documentdb.GetDocumentList(0, "2", 0, true, UserId);
            ViewBag.ExpiringDocumentsList = documentdb.GetDocumentList(0, "1", 0, true, UserId);
            ViewBag.PayrollRequestSetting = new PayrollDB().GetPayrollGeneralSetting().IsconfirmRunPayroll.ToString();
            //List<string> DocumentDaysSettings = new DocumentDB().GetDocumentsettingDays();
            //ViewBag.DocumentAbouttoExpireDays = DocumentDaysSettings[0];
            //ViewBag.DocumentsExpiredDays = DocumentDaysSettings[1];    
            ViewBag.ShortLeaveRequestPendingCountForApprover = new ShortLeaveRequestDB().GetShortLeaveRequestsCountForApprover(UserId);
            ViewBag.ShortLeaveRequestPendingCountForHR = IsHRUser(UserId) ? new ShortLeaveRequestDB().GetPendingShortLeaveRequestCountForHR() : 0;
            return View();
        }

        [HttpPost]
        public ActionResult GetPiePoints()
        {
            DashBoardDB dashBoardDB = new DashBoardDB();
            //Piechart
            var a = dashBoardDB.GetEmployeeWithGender();
            var vList = new object();
            List<DotNet.Highcharts.Options.Point> pointList = new List<DotNet.Highcharts.Options.Point>();
            DotNet.Highcharts.Options.Point firstPoint = new DotNet.Highcharts.Options.Point();

            Decimal TotalStrength = 0;
            for (int j = 0; j < a.Count; j++)
            {

                TotalStrength = TotalStrength + Convert.ToDecimal(a[j].Strength);

            }


            List<DashBoardModel> objDashBoardModelList = new List<DashBoardModel>();
            objDashBoardModelList = dashBoardDB.GetEmployeeByDepartmentList();

            //---------------------------------------------



            vList = new
            {
                aaData = (from item in objDashBoardModelList
                          select new
                          {
                              //  var pointPercentage = Math.Round(Convert.ToDecimal(Convert.ToDecimal(a[i].Strength.ToString()) / Convert.ToDecimal(TotalStrength)), 2);

                              name = item.DepartmentName,
                              y = Math.Round((Convert.ToDecimal(item.Strength) / TotalStrength), 2),
                          }).ToArray()

            };





            return Json(vList, JsonRequestBehavior.AllowGet);




        }

        [HttpPost]
        public ActionResult GetCountryLocation()
        {

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            DashBoardDB dashBoardDB = new DashBoardDB();
            var vList = new object();
            List<DashBoardModel> objDashBoardModelList = new List<DashBoardModel>();
            objDashBoardModelList = dashBoardDB.GetEmployeeCountry(objUserContextViewModel.CompanyId);


            vList = new
            {
                aaData = (from item in objDashBoardModelList
                          select new
                          {
                              latLng = item.LAtLongList,
                              name = item.CountryName + ",Total Employee = " + item.TotalEmployee,
                          }).ToArray()

            };





            return Json(vList, JsonRequestBehavior.AllowGet);




        }

        [HttpPost]
        public ActionResult GetAttendanceCount()
        {

            DailyAttendanceController objDailAttendanceController = new DailyAttendanceController();
            int[] s = new int[12];
            s = objDailAttendanceController.GetDailyAttendanceEmployeeCount(DateTime.Now.ToString("MM/dd/yyyy"), DateTime.Now.ToString("MM/dd/yyyy"));

            //s = objDailAttendanceController.GetDailyAttendanceEmployeeCount("2014-11-12", "2014-12-12");
            return Json(s, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult GetChartPoints()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DashBoardDB dashBoardDB = new DashBoardDB();
            int mode = (objUserContextViewModel.UserRolls.Any(x => x.ToLower().Equals("admin")) ? 1 : 2);
            var a = dashBoardDB.GetEmployeeWithGender();
            var aList = new object();
            List<DotNet.Highcharts.Options.Point> pointList = new List<DotNet.Highcharts.Options.Point>();
            DotNet.Highcharts.Options.Point firstPoint = new DotNet.Highcharts.Options.Point();
            List<DocumentsModel> documentdModelList = new List<DocumentsModel>();
            documentdModelList = dashBoardDB.GetExpiredDocumentList(objUserContextViewModel.UserId, mode);
            aList = new
            {
                aaData = (from item in documentdModelList.GroupBy(x => x.DocType)
                          select new
                          {
                              name = item.FirstOrDefault().DocType,
                              y = item.Count(),
                          }).ToArray()

            };
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PieWithLegend()
        {
            Highcharts chart = new Highcharts("chart")
                .InitChart(new Chart { PlotShadow = false, PlotBackgroundColor = null, PlotBorderWidth = null })
                .SetTitle(new Title { Text = "Browser market shares at a specific website, 2010" })
                .SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }" })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels { Enabled = false },
                        ShowInLegend = true
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Browser share",
                    Data = new Data(new object[]
                                               {
                                                   new object[] { "Firefox", 45.0 },
                                                   new object[] { "IE", 26.8 },
                                                   //new Point
                                                   //{
                                                   //    Name = "Chrome",
                                                   //    Y = 12.8,
                                                   //    Sliced = true,
                                                   //    Selected = true
                                                   //},
                                                   new object[] { "Safari", 8.5 },
                                                   new object[] { "Opera", 6.2 },
                                                   new object[] { "Others", 0.7 }
                                               })
                });

            return View(chart);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult GetEmployeeDetails(int Id)
        {
            Session["EmployeeListID"] = Id;

            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            if (Id != 0)
            {
                int EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                objEmployee.employeeDetailsModel.FirstName_1 = string.IsNullOrEmpty(objEmployee.employeeDetailsModel.FirstName_1) ? "" : objEmployee.employeeDetailsModel.FirstName_1;

                objEmployee.employeeDetailsModel.SurName_1 = string.IsNullOrEmpty(objEmployee.employeeDetailsModel.SurName_1) ? "" : objEmployee.employeeDetailsModel.SurName_1;

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;

                ViewBag.Positon = string.IsNullOrEmpty(objEmployee.employmentInformation.PositionName) ? "" : objEmployee.employmentInformation.PositionName;

                ViewBag.Department = string.IsNullOrEmpty(objEmployee.employmentInformation.DepartmentName) ? "" : objEmployee.employmentInformation.DepartmentName;

                ViewBag.EmployeeIDNo = Id.ToString();
            }
            return Json(objEmployee, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckUrlPermission(string url, string mode)
        {
            return Json(CommonHelper.CommonHelper.checkUrlPermission(url, mode), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult PortalLeaveRequestConfirmation()
        {
            VacationDB objVacDB = new VacationDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            List<VacationModel> RequstList = null;
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.IsPermisssion = CommonHelper.CommonHelper.checkUrlPermission("/PortalLeaveRequest", "Url");
            if (ViewBag.IsPermisssion)
            {
                RequstList = objVacDB.GetPortalVacationRequests().Where(m => m.StatusId == 0).ToList();
            }
            else
            {
                RequstList = new List<VacationModel>();
            }

            return PartialView("~/Views/PortalLeaveRequest/PortalLeaveRequestConfirmation.cshtml", RequstList.Count);
        }

        public PartialViewResult PayRequestConfirmationAlert()
        {
            PayrollDB payrollDB = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            List<PayrollConfirmationRequest> RequstList = null;
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.IsPermisssion = CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url");
            if (ViewBag.IsPermisssion)
            {
                RequstList = payrollDB.GetPayrollConfirmationRequestDate("All", false, false, 0);
            }
            else
            {
                RequstList = payrollDB.GetPayrollConfirmationRequestDate("All", false, false, objUserContextViewModel.UserId);
            }

            return PartialView("~/Views/PayRequestConfirmation/PayRequestConfirmationAlert.cshtml", RequstList.Count.ToString());
        }

        public PartialViewResult GetEmpUnpaidLeaveGrid(string FromDate, string ToDate,int IsPaid)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PermissionModel VacPermissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/Vacation/Index");
            PermissionModel VacTypePermissionModel = CommonHelper.CommonHelper.CheckPermissionForUser("/VacationType/Index");
            bool isShowUnPaidLeaveDetails = false;
            if (VacPermissionModel.IsViewOnlyPermission || VacTypePermissionModel.IsViewOnlyPermission)
            {
                isShowUnPaidLeaveDetails = true;
            }
            ViewBag.ShowUnPaidLeaveDetails = isShowUnPaidLeaveDetails;
            List<VacationModel> vacationList = new VacationDB().GetEmpUnpaidLeaveData(FromDate, ToDate, IsPaid, objUserContextViewModel.UserId);
            return PartialView("_EmpUnpaidLeave", vacationList);
        }

        public ActionResult GetApplicationLogo(int mode)
        {
            string imagepath = "";
            GeneralControlDB generalControlDB = new GeneralControlDB();
            imagepath = generalControlDB.GetApplicationLogo(mode);
            return Json(new { imagepath = imagepath }, JsonRequestBehavior.AllowGet);
        }

        private bool IsHRUser(int userId)
        {
            bool isHRUser = false;
            ShortLeaveSettingsModel listUserGroupModel = new ShortLeaveSettingsModel();
            listUserGroupModel = new ShortLeaveRequestDB().GetShortLeaveSettings();
            if (!string.IsNullOrEmpty(listUserGroupModel.EmployeeIdsToNotifyUponApproval))
                isHRUser = listUserGroupModel.EmployeeIdsToNotifyUponApproval.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Any(item => Convert.ToInt32(item) == userId);
            return isHRUser;
        }

        public ActionResult ExportToExcelTeamAttendance()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<DailyAttendanceExportModel> objDailyAttendanceViewModelList = new List<DailyAttendanceExportModel>();
            //(DateTime.Now.Date.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"));
            objDailyAttendanceViewModelList = new DailyAttendanceViewDB().GetDailyAttendanceExportList(DateTime.Now.Date.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"),null,null,null,null,null,null,null,objUserContextViewModel.UserId, null);
            DataTable dt = ConvertListToDataTable.ToDataTable(objDailyAttendanceViewModelList);
            byte[] content;
            string fileName = "TeamAttendance" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            dt.Columns.Remove("EmployeeID");
            dt.Columns.Remove("StatusID");
            dt.Columns.Remove("HolidayName");
            dt.Columns["AttDate"].ColumnName = "Date";
            dt.Columns["AlternateEmployeeID"].ColumnName = "Employee ID";
            dt.Columns["EmployeeName"].ColumnName = "Employee Name";
            dt.Columns["DepartmentName"].ColumnName = "Department Name";
            dt.Columns["InTime"].ColumnName = "In time";
            dt.Columns["OutTime"].ColumnName = "Out time";
           // dt.Columns["HolidayName"].ColumnName = "Holiday Name";
            dt.Columns["Status"].ColumnName = "Status";
            dt.Columns["LateMinutes"].ColumnName = "Late Minutes";
            dt.Columns["EarlyMinutes"].ColumnName = "Early Minutes";
            dt.Columns["TotalHrs"].ColumnName = "Total Hrs";
            dt.Columns["ShiftHrs"].ColumnName = "Shift Hrs";            
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds, true);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }
    }
}