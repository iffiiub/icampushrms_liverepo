﻿using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.Entities;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class CompOffPreApprovalController : FormsController
    {
        CompOffPreApprovalFormDB compOffPreApprovalFormDB;
        public CompOffPreApprovalController()
        {
            XMLLogFile = "LoggerCompOffPreApproval.xml";
            compOffPreApprovalFormDB = new CompOffPreApprovalFormDB();
        }
        // GET: CompOffPreApproval
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {          
            int formProcessId = GetFormProcessSessionID();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.comapanyId = objUserContextViewModel.CompanyId;
            var UserId = objUserContextViewModel.UserId;
            var CompanyId = objUserContextViewModel.CompanyId;
            CompOffPreApprovalModel model = new CompOffPreApprovalModel();
            model.RequesterEmployeeID = objUserContextViewModel.UserId;
            model.CreatedOn = DateTime.Now.Date;
            model.WorkingDate = DateTime.Now.Date.ToString(HRMSDateFormat);
            model.IsAddMode = true;
            model.CompanyID = CompanyId;
            return View(model);
        }
        public ActionResult Edit()
        {
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];         
            int formProcessId = GetFormProcessSessionID();
            if (formProcessId > 0)
            {
                CompOffPreApprovalModel compOffPreApprovalModel = compOffPreApprovalFormDB.GetFormDetails(formProcessId);
                RequestFormsApproveModel requestFormsApproveModel = compOffPreApprovalFormDB.GetPendingFormsApproval(formProcessId);
                //checking is hr team is logged in.
                //if login user is the present approver then can see form in edit mode or is admin user
                if (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                    || (compOffPreApprovalModel.RequestStatusID == (int)RequestStatus.Rejected && compOffPreApprovalModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    //If form is pending then only show for edit
                    if (compOffPreApprovalModel.RequestStatusID == (int)RequestStatus.Pending || (compOffPreApprovalModel.RequestStatusID == (int)RequestStatus.Rejected && compOffPreApprovalModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                    {
                        if (compOffPreApprovalModel.RequestStatusID == (int)RequestStatus.Rejected && compOffPreApprovalModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        {
                            ViewBag.isReinitialized = true;
                            return View("Create", compOffPreApprovalModel);
                        }
                        if (requestFormsApproveModel.FormsWorkflowGroupModel != null)
                        {
                            ViewBag.isHrTeam = requestFormsApproveModel.FormsWorkflowGroupModel.CompOffHRGroup;
                        }                       
                        return View("Create", compOffPreApprovalModel);
                    }
                    else
                    {
                        return RedirectToAction("ViewDetails");
                    }

                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            return RedirectToAction("Create");
        }
        public ActionResult UpdateDetails()
        {
            int formProcessId = GetFormProcessSessionID();
            CompOffPreApprovalModel compOffPreApprovalModel = compOffPreApprovalFormDB.GetFormDetails(formProcessId);
            RequestFormsApproveModel requestFormsApproveModel = compOffPreApprovalFormDB.GetPendingFormsApproval(formProcessId);
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            ViewBag.CompanyId = objUserContextViewModel.CompanyId;
            ViewBag.FormId = 8;

            if (formProcessId > 0)
            {
                if (compOffPreApprovalModel.RequestStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.isReinitialized = false;
                    if (requestFormsApproveModel.FormsWorkflowGroupModel != null)
                    {
                        ViewBag.isHrTeam = requestFormsApproveModel.FormsWorkflowGroupModel.CompOffHRGroup;
                    }
                    compOffPreApprovalModel.FormProcessID = formProcessId;
                    return View("UpdateDetails", compOffPreApprovalModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }
        [HttpPost]
        public ActionResult SaveForm()
        {           
            string formProcessIDs = "";
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            OperationDetails operationDetails = new OperationDetails();
            CompOffPreApprovalModel compOffPreApprovalModel = new CompOffPreApprovalModel();            
            try
            {
                var data = Request.Form;
                decimal workingHours = 0;               
                compOffPreApprovalModel.AdditionalWorkingHours = workingHours;
               // DateTime workingDate = DateTime.MinValue;
               // DateTime.TryParse(data["WorkingDate1"], out workingDate);
                compOffPreApprovalModel.WorkingDate = Convert.ToString(data["WorkingDate1"]);
                compOffPreApprovalModel.Comments = Convert.ToString(string.IsNullOrEmpty(data["RequesterComments"]) ? "" : data["RequesterComments"]);
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                compOffPreApprovalModel.EmployeeID = objUserContextViewModel.UserId;
                compOffPreApprovalModel.CreatedBy = objUserContextViewModel.UserId;
                compOffPreApprovalModel.CreatedOn = DateTime.Now;
                compOffPreApprovalModel.ModifiedBy = objUserContextViewModel.UserId;
                compOffPreApprovalModel.ModifiedOn = DateTime.Now;
                compOffPreApprovalModel.RequesterEmployeeID = objUserContextViewModel.UserId;
                compOffPreApprovalModel.CompanyID = objUserContextViewModel.CompanyId;
                compOffPreApprovalModel.RequestStatusID = 1;//pending

                EmployeeDocumentModel documentModel;
                documentModel = new EmployeeDocumentModel();              
                DocumentDB documentDb = new DocumentDB();
                               
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();             
                compOffPreApprovalModel.AllFormsFilesModelList = uploadFileList;
                requestFormsProcessModelList = compOffPreApprovalFormDB.SaveForm(compOffPreApprovalModel);

                if (requestFormsProcessModelList != null)
                {
                    formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = compOffPreApprovalFormDB.GetApproverEmailList(Convert.ToString(formProcessIDs), objUserContextViewModel.UserId);
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);
                    operationDetails.InsertedRowId = 1;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request generated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.InsertedRowId = 0;
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Details";
                    operationDetails.CssClass = "error";
                }

            }
            catch (Exception ex)
            {
                operationDetails.InsertedRowId = 0;
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Details : "+ex.Message;
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateForm()
        {
            HttpPostedFileBase fileTF;
            int updateResult = 0;
            OperationDetails operationDetails = new OperationDetails();
            CompOffPreApprovalModel compOffPreApprovalModel = new CompOffPreApprovalModel();
            try
            {
                var data = Request.Form;
                decimal workingHours = 0;
                int formInstanceId = 0;
                int formProcessId = 0;
                bool isHrTeam = false;              
                int.TryParse(data["formInstanceId"].ToString(), out formInstanceId);
                if (data["AdditionalWorkingHours1"] != null)
                    decimal.TryParse(data["AdditionalWorkingHours1"].ToString(), out workingHours);
                int.TryParse(data["FormProcessID1"].ToString(), out formProcessId);
                bool.TryParse(data["isHrGroup1"].ToString(), out isHrTeam);               

                compOffPreApprovalModel.WorkingDate = Convert.ToString(data["WorkingDate1"]);
                compOffPreApprovalModel.ID = formInstanceId;
                compOffPreApprovalModel.AdditionalWorkingHours = workingHours;               
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                compOffPreApprovalModel.ModifiedBy = objUserContextViewModel.UserId;
                compOffPreApprovalModel.ModifiedOn = DateTime.Now;
                compOffPreApprovalModel.Comments = data["Comments1"].ToString();
                compOffPreApprovalModel.FormProcessID = formProcessId;              
                DocumentDB documentDb = new DocumentDB();
                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
                if (Request.Files.Count > 0)
                {
                    if (Request.Files["TimeSheetFile"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileTF = Request.Files["TimeSheetFile"];
                        ObjFile.FileName = fileTF.FileName;
                        ObjFile.FileContentType = fileTF.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileTF);
                        ObjFile.FormFileIDName = "TimeSheetFileID";
                        uploadFileList.Add(ObjFile);
                        compOffPreApprovalModel.TimeSheetFileID = 0;
                    }
                }

                compOffPreApprovalModel.AllFormsFilesModelList = uploadFileList;
                updateResult = compOffPreApprovalFormDB.UpdateForm(compOffPreApprovalModel, isHrTeam);

                if (updateResult != 0)
                {
                    operationDetails.InsertedRowId = 1;
                    operationDetails.Success = true;
                    operationDetails.Message = "Request updated successfully.";
                    operationDetails.CssClass = "success";
                }
                else
                {
                    operationDetails.InsertedRowId = 0;
                    operationDetails.Success = false;
                    operationDetails.Message = "Error occured while adding Details";
                    operationDetails.CssClass = "error";
                }

            }
            catch (Exception ex)
            {
                operationDetails.InsertedRowId = 0;
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while adding Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessId = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CompOffPreApprovalModel compOffPreApprovalModel = compOffPreApprovalFormDB.GetFormDetails(formProcessId);
            ViewBag.isHrTeam = compOffPreApprovalFormDB.GetAllFormsApproval(formProcessId).Where(x => x.ApproverEmployeeID == objUserContextViewModel.UserId).Any(x => x.FormsWorkflowGroupModel.CompOffHRGroup);
            return View(compOffPreApprovalModel);
        }
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            OperationDetails operationDetails = new OperationDetails();
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                    //If ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendFinalNotificationEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }

                        //Add any logic to be done once approval process is completed
                        compOffPreApprovalFormDB.SaveEligibleCompOffHours(formProcessID);
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckCompOffLeaveRequestLoadValidation(int? employeeId, int? id)
        {
            OperationDetails operationDetails = new OperationDetails();
            CompOffPreApprovalModel compOffPreApprovalModel = compOffPreApprovalFormDB.CheckCompOffLeaveRequestLoadValidation(employeeId, id);
            operationDetails.CssClass = compOffPreApprovalModel.IsValid ? "success" : "error";
            operationDetails.Success = compOffPreApprovalModel.IsValid ? true : false;
            operationDetails.Message = compOffPreApprovalModel.ValidationMessage;
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}