﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using Newtonsoft.Json;

namespace HRMS.Web.Controllers
{
    public class SocialSecurityController : BaseController
    {
        // GET: SocialSecurity
        public ActionResult Index()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            PayCycleDB objPayCycleDB = new PayCycleDB();
            selectListItems = new List<SelectListItem>();

            foreach (var items in objPayCycleDB.GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                    Selected = items.active

                };
                selectListItems.Add(selectListItem);
            }
            ViewBag.CyclesList = selectListItems;
            TaxSetting pasiSetting = new PASITAxDB().GetPasiTaxtSetting();
            return View(pasiSetting);
        }

        public ActionResult GetSocialSecurityList()
        {
            List<SocialSecurirtyModel> objSocialSecurirtyList = new List<SocialSecurirtyModel>();
            SocialSecurityDB objSocialSecurityDB = new SocialSecurityDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objSocialSecurirtyList = objSocialSecurityDB.GetSocialSecurityList();
            int Count = objSocialSecurirtyList.Count;
            var vList = new object();
            vList = new
            {
                aaData = (from item in objSocialSecurirtyList
                          select new
                          {
                              CheckRow = "<input data-Id='" + item.EmployeeID + "' data-ssid='" + item.SSID.ToString() + "' type='checkbox' onclick='ChkRowCheckClick(this)' class='ChkRowCheck' id = 'Chkcheck" + item.EmployeeID + "' name = 'check" + item.EmployeeID + "'/>",
                              EmployeeAlternativeID = item.EmployeeAltId,
                              EmployeeID = item.EmployeeID,
                              FullName = item.EmployeeName,
                              Amount = item.Amount.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              Salary = item.Salary.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              Percentage = "<input onkeyup='calculateTax(this)' onchange='saveSecurityTax(this)' data-Max='100' data-Min='0' type='text' value='" + item.Percentage + "' class='form-control inputPercentage' />"
                          }).ToArray(),
                recordsTotal = Count,
                recordsFiltered = Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        // public ActionResult PushRecords(List<PASITaxModel> PasitaxLists, string effectivedate, int cycleid, string cyclename)
        public ActionResult PushRecords(string SocialSecurityDetails, string EffectiveDate, string CycleName, int CycleID, bool IsValidate)
        {
            List<SocialSecurirtyModel> SocialSecurityList = JsonConvert.DeserializeObject<List<SocialSecurirtyModel>>(SocialSecurityDetails);
            List<SocialSecurirtyModel> SocialSecurityListToUpdate = new List<SocialSecurirtyModel>();
            string empIds = string.Join(",", SocialSecurityList.Select(n => n.EmployeeID.ToString()).ToArray());
            OperationDetails objOperationDetails = new OperationDetails();
            SocialSecurityDB objSocialSecurityDB = new SocialSecurityDB();
            PASITAxDB objPASITAxDB = new PASITAxDB();
            int SocialSecDedTypeId = objSocialSecurityDB.GetDeduTypeIdForSS();
            if (IsValidate)
            {
                objOperationDetails = objSocialSecurityDB.CheackPayDetailIsPresent(CycleID, SocialSecDedTypeId, empIds);
            }
            else
            {
                objOperationDetails.Success = true;
            }

            if (objOperationDetails.Success)
            {
                if (SocialSecDedTypeId > 0)
                {
                    if (SocialSecurityList.Count > 0)
                    {
                        for (int i = 0; i < SocialSecurityList.Count; i++)
                        {
                            objOperationDetails = objPASITAxDB.CheackPayDetailIsPresent(CycleID, SocialSecDedTypeId, SocialSecurityList[i].EmployeeID);

                            if (objOperationDetails.Success)
                            {
                                if (SocialSecurityList[i].Amount > 0)
                                {
                                    objOperationDetails = objSocialSecurityDB.InsertSocialSecurityintoPayDeduct(SocialSecurityList[i], CycleID, SocialSecDedTypeId, EffectiveDate, CycleName);
                                }
                            }
                            else
                            {
                                SocialSecurityListToUpdate.Add(SocialSecurityList[i]);
                            }

                        }
                        if (SocialSecurityListToUpdate.Count > 0)
                        {
                            objSocialSecurityDB.UpdateSocialSecurityDeduction(SocialSecurityListToUpdate, EffectiveDate, CycleID);
                        }
                        objOperationDetails.CssClass = "success";
                        objOperationDetails.Message = "Deduction generated seuccessfully for selected employee";
                    }
                }
                else
                {
                    objOperationDetails.CssClass = "error";
                    objOperationDetails.Message = "Deduction type for social security not available";
                }
            }
            else
            {
                objOperationDetails.CssClass = "warning";
                objOperationDetails.Message = "Deduction is already available for employees in selected list";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSocialTaxExcludingPresent(string SocialSecurityDetails, string EffectiveDate, string CycleName, int CycleID, bool IsValidate)
        {
            List<SocialSecurirtyModel> SocialSecurityList = JsonConvert.DeserializeObject<List<SocialSecurirtyModel>>(SocialSecurityDetails);
            string empIds = string.Join(",", SocialSecurityList.Select(n => n.EmployeeID.ToString()).ToArray());
            OperationDetails objOperationDetails = new OperationDetails();
            SocialSecurityDB objSocialSecurityDB = new SocialSecurityDB();
            PASITAxDB objPASITAxDB = new PASITAxDB();
            int SocialSecDedTypeId = objSocialSecurityDB.GetDeduTypeIdForSS();
            // objOperationDetails = objSocialSecurityDB.CheackPayDetailIsPresent(CycleID, SocialSecDedTypeId, empIds);

            if (SocialSecDedTypeId > 0)
            {
                if (SocialSecurityList.Count > 0)
                {
                    for (int i = 0; i < SocialSecurityList.Count; i++)
                    {
                        objOperationDetails = objPASITAxDB.CheackPayDetailIsPresent(CycleID, SocialSecDedTypeId, SocialSecurityList[i].EmployeeID);

                        if (objOperationDetails.Success)
                        {
                            objOperationDetails = objSocialSecurityDB.InsertSocialSecurityintoPayDeduct(SocialSecurityList[i], CycleID, SocialSecDedTypeId, EffectiveDate, CycleName);
                            
                        }
                    }
                    objOperationDetails.CssClass = "success";
                    objOperationDetails.Message = "Deduction generated seuccessfully for selected employee";
                }
            }
            else
            {
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Deduction type for social security not available";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCycleDataById(int cycleId, string date)
        {
            PayCycleDB paycycledb = new PayCycleDB();
            DateTime efficientDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(date));
            var cycleModel = paycycledb.GetPayCycleById(cycleId);
            DateTime startDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(cycleModel.DateFrom));
            DateTime endDate = Convert.ToDateTime(DataAccess.GeneralDB.CommonDB.SetCulturedDate(cycleModel.DateTo));
            bool retuenVal = (startDate <= efficientDate && endDate >= efficientDate);
            return Json(retuenVal, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTax(bool taxForEmployee, bool taxForAll, decimal taxPercentage)
        {
            PASITAxDB pasiDb = new PASITAxDB();
            return Json(pasiDb.SavePasiTaxSetting(taxForEmployee, taxForAll, taxPercentage), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTaxPerEmployee(int employeeId, decimal taxPercentage, int? SSID,decimal taxAmount)
        {
            SocialSecurityDB objSocialSecurityDB = new SocialSecurityDB();
            OperationDetails op = new OperationDetails();
            op = objSocialSecurityDB.SavePasiTaxPerSetting(employeeId, taxPercentage, SSID, taxAmount);
            if (op.Success)
            {
                op.Message = "Social Security updated successfully";
                op.CssClass = "success";
            }
            else
            {
                op.Message = "Error while updating social security";
                op.CssClass = "error";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
    }
}