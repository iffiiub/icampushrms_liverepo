﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using HRMS.Entities.ViewModel;
using System.Data;

namespace HRMS.Web.Controllers
{
    public class EmployeeCredentialController : BaseController
    {
        // GET: /EmployeeCredentials/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployeeCredentialList()
        {
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            //-------------------Grid Paging Info----------------------------------
             
            //-------------Data Objects--------------------
            List<HRMS.Entities.EmployeeCredentialModel> objEmployeeCredentialList = new List<HRMS.Entities.EmployeeCredentialModel>();
            EmployeeCredentialDB objEmployeeCredentialDB = new EmployeeCredentialDB();
            objEmployeeCredentialList = objEmployeeCredentialDB.GetAllEmployeeCredentials().Where(m => m.EmployeeId != 0).ToList();
            if (userContext.UserId != 0)
            {
                objEmployeeCredentialList = objEmployeeCredentialDB.GetAllEmployeeCredentials().Where(m => m.EmployeeId != 0).ToList(); ;
            }
            UserRoleDB objUserRoleDB = new UserRoleDB();
            List<UserRoleModel> objUserRoleModel = new List<UserRoleModel>();
            objUserRoleModel = objUserRoleDB.GetUserRoleList();
            //---------------------------------------------
            
            var vList = new object();
            vList = new
            {
                aaData = (from item in objEmployeeCredentialList
                          select new
                          {
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.UserId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>",
                              //Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.EmployeeCredentialId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(" + item.EmployeeCredentialId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              UserId = item.UserId,
                              EmployeeId = item.EmployeeId,
                              EmployeeAlternativeId = item.EmployeeAlternativeID,
                              EmployeeName = item.employeeName,
                              Username = item.Username,
                              Password = "*******",
                              IsEnable = item.IsEnable ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.IsEnable + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.IsEnable + "'/>",

                          }).ToArray()
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            HRMS.Entities.EmployeeCredentialModel objEmployeeCredentialModel = new HRMS.Entities.EmployeeCredentialModel();
            EmployeeCredentialDB objEmployeeCredentialDb = new EmployeeCredentialDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserRoleDB objUserRoleDB = new UserRoleDB();


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int CompanyID = objUserContextViewModel.CompanyId;

            if (id != -1)
            {
                objEmployeeCredentialModel = objEmployeeCredentialDb.GetEmployeeCredential(id);
                string dycryptedPassword = "";
                //if (objEmployeeCredentialModel.Password != null && objEmployeeCredentialModel.Password.Length > 0)
                //    dycryptedPassword = decryptPassword(objEmployeeCredentialModel.Password);

                //objEmployeeCredentialModel.Password = dycryptedPassword;
                ViewData["Employees"] = new SelectList(objEmployeeDB.GetNonCredentialsEmployee(objEmployeeCredentialModel.EmployeeId), "EmployeeId", "FirstName", objEmployeeCredentialModel.EmployeeId);
                //ViewData["UserType"] = new SelectList(objUserRoleDB.GetUserRoleList(), "UserRoleId", "UserRoleName", null);
            }
            else
            {                
                ViewData["Employees"] = new SelectList(objEmployeeDB.GetNonCredentialsEmployee(0).Where(m => m.EmployeeId > 0), "EmployeeId", "FirstName");
                if (objUserContextViewModel.UserId != 0)
                {
                    ViewData["Employees"] = new SelectList(objEmployeeDB.GetNonCredentialsEmployee(0).Where(m=>m.EmployeeId>0), "EmployeeId", "FirstName");
                }
                //ViewData["UserType"] = new SelectList(objUserRoleDB.GetUserRoleList(), "UserRoleId", "UserRoleName", null);
            }

            return View(objEmployeeCredentialModel);
        }

        public ActionResult Save(HRMS.Entities.EmployeeCredentialModel objEmployeeCredentialModel)
        {
            try
            {
                EmployeeCredentialDB objEmployeeCredentialDb = new EmployeeCredentialDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (objEmployeeCredentialModel.UserId == 0)
                {
                    objEmployeeCredentialModel.Createdby = objUserContextViewModel.UserId;
                   // objEmployeeCredentialModel.NormalPassword = objEmployeeCredentialModel.Password;
                    //objEmployeeCredentialModel.Password = EncryptPassword(objEmployeeCredentialModel.Password);
                    objEmployeeCredentialDb.AddEmployeeCredential(objEmployeeCredentialModel);

                }
                else
                {
                    objEmployeeCredentialModel.ModifiedBy = objUserContextViewModel.UserId;
                   // objEmployeeCredentialModel.NormalPassword = objEmployeeCredentialModel.Password;
                 //   objEmployeeCredentialModel.Password = EncryptPassword(objEmployeeCredentialModel.Password);
                    objEmployeeCredentialDb.UpdateEmployeeCredential(objEmployeeCredentialModel);
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        private string decryptPassword(string password)
        {
            byte[] HashKey = SecureIt.GetHashKey(Properties.Settings.Default["salt1"].ToString());
            string Decrypted = SecureIt.Decrypt(HashKey, password);
            return Decrypted;
        }
        private string EncryptPassword(string password)
        {
            byte[] HashKey = SecureIt.GetHashKey(Properties.Settings.Default["salt1"].ToString());
            string encrypted = SecureIt.Encrypt(HashKey, password);
            return encrypted;
        }

        public ActionResult Delete(string id)
        {
            EmployeeCredentialDB objEmployeeCredentialDb = new EmployeeCredentialDB();
            HRMS.Entities.EmployeeCredentialModel objEmployeeCredential = new HRMS.Entities.EmployeeCredentialModel();
            objEmployeeCredential.UserId = int.Parse(id);
            objEmployeeCredentialDb.DeleteEmployeeCredential(objEmployeeCredential);
            //return Redirect("Index");
            return Json(0, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ExportToExcel()
        {
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            byte[] content;
            string fileName = "UserAccounts" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";
            EmployeeCredentialDB objEmpCredentialDB = new EmployeeCredentialDB();
            System.Data.DataSet ds;
            if ((userContext.UserRolls.FindAll(x => x.ToLower().Contains("admin")).Count() > 0) && (userContext.UserId == 0))
                ds = objEmpCredentialDB.GetAllEmployeeCredentialsDatasSet(-1);
            else
                ds = objEmpCredentialDB.GetAllEmployeeCredentialsDatasSet(0);
            ds.Tables[0].Columns["Username"].ColumnName = "User name";
            //foreach (DataRow DR in ds.Tables[0].Rows)
            //{
            //    if (DR["Password"].ToString().Trim().Length > 0)
            //    {
            //        DR["Password"] = HRMS.Web.CommonHelper.CommonHelper.decryptPassword(DR["Password"].ToString());
            //    }
            //}
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            UserContextViewModel userContext = (UserContextViewModel)Session["userContext"];
            string fileName = "UserAccounts" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //CompanyDB objCompanyDB = new CompanyDB();
            EmployeeCredentialDB objEmpCredentialDB = new EmployeeCredentialDB();
            System.Data.DataSet ds;
            if ((userContext.UserRolls.FindAll(x => x.ToLower().Contains("admin")).Count() > 0) && (userContext.UserId == 0))
                ds = objEmpCredentialDB.GetAllEmployeeCredentialsDatasSet(-1);
            else
                ds = objEmpCredentialDB.GetAllEmployeeCredentialsDatasSet(0);

            ds.Tables[0].Columns["Username"].ColumnName = "User name";
            //foreach (DataRow DR in ds.Tables[0].Rows)
            //{
            //    if (DR["Password"].ToString().Trim().Length > 0)
            //    {
            //        DR["Password"] = HRMS.Web.CommonHelper.CommonHelper.decryptPassword(DR["Password"].ToString());
            //    }
            //}
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, false);
            Response.Write(dc);
            Response.End();

        }

        public string CheckUserName(string username)
        {
            string result = "0";
            EmployeeCredentialDB objEmployeeCredentialDB = new EmployeeCredentialDB();
            System.Data.DataTable dtEmployeeCredentials = objEmployeeCredentialDB.GetEmployeeCredentialsByUserName(username);
            if (dtEmployeeCredentials != null && dtEmployeeCredentials.Rows.Count > 0)
            {
                result = "1";
            }
            return result;
        }
    }
}