﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class LabourController : BaseController
    {
        public ActionResult Index()
        {
            if (Session["EmployeeListID"] == null)
            {
                ViewBag.empid = "";
            }
            else
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }
            return View();
        }

        public ActionResult GetLabourList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        {
            string empid = "";
            if (Session["EmployeeListID"] == null)
            {
                return null;
            }
            else
            {
                empid = Session["EmployeeListID"].ToString();
            }

            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<DocumentsModel> objDocumentsModelList = new List<DocumentsModel>();
            DocumentDB objDocumentDB = new DocumentDB();

            objDocumentsModelList = objDocumentDB.GetDocumentList(Convert.ToInt32(empid), 3);

            //--------------------------------------Update Delete permission
            PermissionModel permissionModel = HRMS.Web.CommonHelper.CommonHelper.CheckPermissionForUser("/Labour");
            string Action = "";

            if (permissionModel.IsUpdateOnlyPermission && permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditLabour(item)' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewLabour(item)' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteLabour(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
            }
            else if (!permissionModel.IsUpdateOnlyPermission && !permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewLabour(item)' title='View' ><i class='fa fa-eye'></i> </a>";
            }
            else
            {
                if (permissionModel.IsUpdateOnlyPermission)
                {
                    Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditLabour(item)' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewLabour(item)' title='View' ><i class='fa fa-eye'></i> </a>";
                }
                if (permissionModel.IsDeleteOnlyPermission)
                {
                    Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewLabour(item)' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteLabour(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
                }
            }

            //---------------------------------------------

            var vList = new object();
            vList = new
            {
                aaData = (from item in objDocumentsModelList
                          select new
                          {
                              Action = Action.Replace("item", item.DocId.ToString()),
                              DocLabour = item.DocId,
                              DocumentNo = item.ActualDocumentFile != null ? "<a href='#' onclick='ViewDocumentById(" + item.DocId.ToString() + ")'>" + item.DocNo + "</a>" : "<span title='No file available'>" + item.DocNo + "<span>",
                              IssueCountry = item.IssueCountry,
                              IssuePlace = item.IssuePlace,
                              IssueDate = CommonDB.GetFormattedDate_DDMMYYYY(item.IssueDate),
                              ExpiryDate = CommonDB.GetFormattedDate_DDMMYYYY(item.ExpiryDate),
                              Note = item.Note,
                              IsPrimary = item.IsPrimary ? "Yes" : "No",
                              EmployeeId = item.EmployeeId
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddLabour(int EmployeeID)
        {
            CountryDB objCountryDB = new CountryDB();
            CityDB obJCityDB = new CityDB();
            DocumentDB objDocumentDB = new DocumentDB();
            CompanyDB companyDB = new CompanyDB();
            CompanyModel companyModal = new CompanyModel();
            int SchoolCompanyID = objDocumentDB.GetSchoolCompanyId();
            companyModal = companyDB.GetCompany(SchoolCompanyID);
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName", companyModal.Country);
            ViewBag.cityList = new SelectList(obJCityDB.GetCityByCountryId(Convert.ToString(companyModal.Country)), "CityId", "CityName", companyModal.City);
            ViewBag.sponsorList = new SelectList(objDocumentDB.GetSponsorList(true), "SponsorID", "SponsorName_1");
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            int MolTitleID = new EmployeeDB().GetEmpoymentInfoByEmployeeID(EmployeeID).MOLTitle;
            ObjSelectedList.Add(new SelectListItem { Text = "Select MOL.", Value = "0" });
            foreach (var m in new MOLTitleDB().GetAllMOLTitleList())
            {
                if (m.MOLTitleID == MolTitleID)
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.MOLTitleName_1, Value = m.MOLTitleID.ToString(), Selected = true });
                }
                else
                {
                    ObjSelectedList.Add(new SelectListItem { Text = m.MOLTitleName_1, Value = m.MOLTitleID.ToString() });
                }

            }
            ViewBag.MOLTitleList = ObjSelectedList;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return View();
        }

        [HttpPost]
        public ActionResult AddLabour(DocumentsModel objDocumentsModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            string sessionVariable = Request.Form["sessionVariable"];
            string empid = Session["EmployeeListID"].ToString();
            if (empid != "")
            {
                if (Session[sessionVariable] != null)
                {
                    EmployeeDocumentModel employeeDocumentModel = (EmployeeDocumentModel)Session[sessionVariable];
                    if (employeeDocumentModel.DocumentFile != null)
                    {
                        objDocumentsModel.DocumentFileName = employeeDocumentModel.ImageName;
                        objDocumentsModel.ActualDocumentFile = employeeDocumentModel.DocumentFile;
                        objDocumentsModel.FileContentType = employeeDocumentModel.FileContentType;
                        Session.Remove(sessionVariable);
                    }
                }
                objDocumentsModel.EmpId = Convert.ToInt32(empid);
                if (ModelState.IsValid)
                {
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    DocumentDB objDocumentDB = new DocumentDB();
                    objDocumentsModel.DocTypeId = 3;
                    objOperationDetails = objDocumentDB.AddEditDocument(objDocumentsModel, 1);
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please fill all the fields carefully.";
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please select an employee.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EditLabour(int id)
        {
            DocumentDB objDocumentDB = new DocumentDB();
            CountryDB objCountryDB = new CountryDB();
            CityDB obJCityDB = new CityDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.sponsorList = new SelectList(objDocumentDB.GetSponsorList(true), "SponsorID", "SponsorName_1");
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select MOL.", Value = "0" });
            foreach (var m in new MOLTitleDB().GetAllMOLTitleList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MOLTitleName_1, Value = m.MOLTitleID.ToString() });
            }
            ViewBag.MOLTitleList = ObjSelectedList;
            DocumentsModel objDocumentsModel = new DocumentsModel();
            objDocumentsModel = objDocumentDB.GetDocumentByDocId(id);
            if (objDocumentsModel.ActualDocumentFile != null)
            {
                ViewBag.Status = "true";
            }
            else
            {
                ViewBag.Status = "false";
            }
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");
            ViewBag.City = new SelectList(obJCityDB.GetCityByCountryId(objDocumentsModel.IssueCountryID.ToString()), "CityId", "CityName", objDocumentsModel.IssueCityID);
            return View(objDocumentsModel);
        }

        [HttpPost]
        public JsonResult EditLabour(DocumentsModel objDocumentsModel)
        {
            DocumentDB objDocumentDB = new DocumentDB();
            string oldFile = objDocumentsModel.DocumentFile;
            string sessionVariable = Request.Form["sessionVariable"];
            string empid = Session["EmployeeListID"].ToString();
            if (Session[sessionVariable] != null)
            {
                EmployeeDocumentModel employeeDocumentModel = (EmployeeDocumentModel)Session[sessionVariable];
                if (employeeDocumentModel.DocumentFile != null)
                {
                    objDocumentsModel.DocumentFileName = employeeDocumentModel.ImageName;
                    objDocumentsModel.ActualDocumentFile = employeeDocumentModel.DocumentFile;
                    objDocumentsModel.FileContentType = employeeDocumentModel.FileContentType;
                    Session.Remove(sessionVariable);
                }
            }
            objDocumentsModel.EmpId = Convert.ToInt32(empid);
            if (ModelState.IsValid)
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objDocumentDB.AddEditDocument(objDocumentsModel, 2);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewLabour(int id)
        {
            DocumentDB objDocumentDB = new DocumentDB();
            DocumentsModel objDocumentsModel = new DocumentsModel();
            objDocumentsModel = objDocumentDB.GetDocumentByDocId(id);
            if (objDocumentsModel.ActualDocumentFile != null)
            {
                ViewBag.Status = "true";
            }
            else
            {
                ViewBag.Status = "false";
            }
            return View(objDocumentsModel);
        }

        public ActionResult DeleteLabour(int id)
        {
            DocumentDB objDocumentDB = new DocumentDB();
            OperationDetails op = new OperationDetails();
            DocumentsModel objDocumentsModel = new DocumentsModel();
            objDocumentsModel.DocId = id;
            op = objDocumentDB.AddEditDocument(objDocumentsModel, 3);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
    }
}