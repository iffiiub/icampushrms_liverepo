﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class GeneralDeductionsController : BaseController
    {
        // GET: GeneralDeductions
        public ActionResult Index()
        {
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            string FromDate = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");
            ViewBag.FromDate = FromDate;
            string DeductionTypes = "<option value='0'> Select Deduction Type </option>";
            foreach (var item in new DeductionDB().GetGeneralDeductionRules())
            {
                DeductionTypes += "<option value='" + item.DeductionRuleID + "'>" + item.DeductionRuleDesc.ToString() + "</option>";
                ObjSelectedList.Add(new SelectListItem { Text = item.DeductionRuleDesc, Value = item.DeductionRuleID.ToString() });
            }
            ViewBag.DeductionTypeList = ObjSelectedList;
            ViewBag.DeductionTypes = DeductionTypes;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Employee", Value = "" });
            foreach (var item in new EmployeeDB().GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1))
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.FullName, Value = item.EmployeeId.ToString() });
            }
            ViewBag.EmployeeList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();

            foreach (var items in new PayCycleDB().GetAllPayCycle())
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = items.PayCycleName_1,
                    Value = items.PayCycleID.ToString(),
                };
                ObjSelectedList.Add(selectListItem);
            }
            RuleDeductionSetting objRuleSetting = new DeductionDB().GetDeductionRuleSetting();
            AcademicYearModel objYearModel = new AcademicYearDB().GetAllAcademicYearList().FirstOrDefault();

            ViewBag.CycleList = ObjSelectedList;
            return View();
        }

        public ActionResult AddGeneralDeductions()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Employee", Value = "" });
            foreach (var item in new EmployeeDB().GetALLEmployeeListWithAlternatioveId(UserId).Where(x => x.IsActive == 1).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.FullName, Value = item.EmployeeId.ToString() });
            }
            ViewBag.EmployeeList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Deduction Type", Value = "0" });

            foreach (var item in new DeductionDB().GetGeneralDeductionRules())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.DeductionRuleDesc, Value = item.DeductionRuleID.ToString() });
            }
            ViewBag.DeductionTypeList = ObjSelectedList;
            return View();
        }

        public ActionResult SaveGeneralDeductions(string GeneralDeductins, string DeductionDate)
        {
            UserContextViewModel model = (UserContextViewModel)Session["userContext"];
            List<EmployeeGeneralDeduction> lstEmployeeDeductions = new List<EmployeeGeneralDeduction>();
            lstEmployeeDeductions = JsonConvert.DeserializeObject<List<EmployeeGeneralDeduction>>(GeneralDeductins);
            DeductionDB objDeductionDB = new DeductionDB();
            OperationDetails op = new OperationDetails();
            op = objDeductionDB.AddMultipleGeneralDeductions(lstEmployeeDeductions, DeductionDate, model.UserId);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CalculateAmount(int EmployeeId, int DeductionRuleId, string DeductionDate)
        {
            DeductionDB objDeductionDB = new DeductionDB();
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            Decimal DeductionAmount = objDeductionDB.GetAmountForDeductionRule(EmployeeId, DeductionRuleId, DeductionDate);
            string Amount = DeductionAmount.ToString(AmountFormat);
            return Json(new { Amount }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDeductableEmployeeList(bool IsConfirmed, int? EmployeeId, int? DeductionTypeId, string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Id", int iSortCol_0 = 0, string FromDate = "", string ToDate = "")
        {
            string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            List<EmployeeGeneralDeduction> GeneralDeductionList = new List<EmployeeGeneralDeduction>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
           
            int? UserId = null;

            UserId = objUserContextViewModel.UserId;

            DeductionDB objDeductionDB = new DeductionDB();
            GeneralDeductionList = objDeductionDB.GetEmployeeGeneralDeductions(EmployeeId, DeductionTypeId, UserId, FromDate, ToDate, IsConfirmed);
            var vList = new
            {
                aaData = (from item in GeneralDeductionList
                          select new
                          {
                              EmployeeAlternativeId = item.EmployeeAltID,
                              EmployeeName = item.EmployeeName,
                              DeductionDate = item.DeductionDate,
                              DeductionRuleDesc = item.DeductionRuleDesc,
                              DeductionAmount = item.DeductionAmount.ToString(AmountFormat),
                              isConfirmed = item.isConfirmed == true ? "<input type='checkbox' class='checkbox' checked id='chkConfirmed" + item.EmployeeGeneralDeductionID + "' disabled/>" : "<input type='checkbox' class='checkbox chkConfirmed'  id='chkConfirmed" + item.EmployeeGeneralDeductionID + "'  onclick='updateConfirmStatus(" + item.EmployeeGeneralDeductionID + ",this)'/>",
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='editDeduction(" + item.EmployeeGeneralDeductionID + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='deleteDeduction(" + item.EmployeeGeneralDeductionID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>"
                          }).ToArray(),
                recordsTotal = GeneralDeductionList.Count,
                recordsFiltered = GeneralDeductionList.Count
            };

            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditGeneralDeduction(int EmployeeGeneralDeductionID)
        {
            EmployeeGeneralDeduction objEmployeeGeneralDeduction = new EmployeeGeneralDeduction();
            DeductionDB objDeductionDB = new DeductionDB();
            objEmployeeGeneralDeduction = objDeductionDB.GetEmployeeGeneralDeductionDetails(EmployeeGeneralDeductionID);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Deduction Type", Value = "0" });

            foreach (var item in objDeductionDB.GetGeneralDeductionRules())
            {
                ObjSelectedList.Add(new SelectListItem { Text = item.DeductionRuleDesc, Value = item.DeductionRuleID.ToString() });
            }
            ViewBag.DeductionTypeList = ObjSelectedList;
            return View(objEmployeeGeneralDeduction);
        }

        [HttpPost]
        public JsonResult UpdateGeneralDeduction(EmployeeGeneralDeduction objGeneralDeduction)
        {
            OperationDetails op = new OperationDetails();
            if (ModelState.IsValid)
            {
                UserContextViewModel model = (UserContextViewModel)Session["userContext"];
                DeductionDB objDeductionDB = new DeductionDB();
                op = objDeductionDB.UpdateGeneralDeductions(objGeneralDeduction, model.UserId);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteGeneralDeduction(int EmployeeGeneralDeductionID)
        {
            OperationDetails op = new OperationDetails();
            UserContextViewModel model = (UserContextViewModel)Session["userContext"];
            DeductionDB objDeductionDB = new DeductionDB();
            op = objDeductionDB.DeleteGeneralDeductions(EmployeeGeneralDeductionID);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveGeneralDeduction(string SelectedIds, int CycleID, string GenerateDate)
        {
            OperationDetails op = new OperationDetails();
            List<EmployeeGeneralDeduction> lstEmployeeDeductions = new List<EmployeeGeneralDeduction>();
            lstEmployeeDeductions = JsonConvert.DeserializeObject<List<EmployeeGeneralDeduction>>(SelectedIds);
            DeductionDB objDeductionDB = new DeductionDB();
            string SelectedDeductions = "";

            int aCycleID = new LateDeductionDB().GetCycleFromDate(GenerateDate);
            if (aCycleID == CycleID)
            {
                if (lstEmployeeDeductions.Count > 0)
                {
                    SelectedDeductions = string.Join(",", lstEmployeeDeductions.Select(x => x.EmployeeGeneralDeductionID));
                    lstEmployeeDeductions = new List<EmployeeGeneralDeduction>();
                    lstEmployeeDeductions = objDeductionDB.GetSelectedEmployeeGeneralDeductions(SelectedDeductions);
                    op = objDeductionDB.ConfirmedMultipleGeneralDeductions(lstEmployeeDeductions, GenerateDate, CycleID);
                }
                else
                {
                    op.CssClass = "error";
                    op.Message = "no records to confirm.";
                }
            }
            else
            {
                op.CssClass = "error";
                op.Message = "Cycle is not generated yet for provided date";
            }


            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllSelectedEmployeeList(int? EmployeeId, int? DeductionTypeId, string FromDate = "", string ToDate = "")
        {
            List<EmployeeGeneralDeduction> GeneralDeductionList = new List<EmployeeGeneralDeduction>();
            DeductionDB objDeductionDB = new DeductionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserGroupDB objUserGroupDB = new UserGroupDB();
            
            int? UserId = null;

            UserId = objUserContextViewModel.UserId;

            GeneralDeductionList = objDeductionDB.GetEmployeeGeneralDeductions(EmployeeId, DeductionTypeId, UserId, FromDate, ToDate, false);
            return Json(GeneralDeductionList, JsonRequestBehavior.AllowGet);
        }



    }
}