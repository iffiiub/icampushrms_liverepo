﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HRMS.Entities.ViewModel;

namespace HRMS.Web.Controllers
{
    public class CompanyBasedHierarchyController : BaseController
    {
        // GET: CompanyBasedHierarchy
        public CompanyBasedHierarchyController()
        {
            XMLLogFile = "LoggerCompanyBasedHeirarchy.xml";
        }
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company =  GetCompanySelectList();
            ViewBag.Group = new SelectList(new CompanyBasedHeirarchyDB().GetAllGroups(true), "GroupID", "GroupName");
            EmployeeDB objempDB = new EmployeeDB();
            List<Employee> empList = objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId);
            string employees = "";
            employees += "<option value=''>Select Employee Name</option>";
            foreach (var obj in empList)
            {
                employees += "<option value='" + obj.EmployeeId + "'>" + obj.FirstName + "</option>";
            }
            ViewBag.Employee = employees;
            return View();
        }
        public ActionResult GetCompanyBasedHeirarchy(int? CompanyID, Int16? GroupID)
        {
            List<CompanyBasedHierarchyModel> objCompanyModelList = new List<CompanyBasedHierarchyModel>();
            CompanyBasedHeirarchyDB objCompany = new CompanyBasedHeirarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            //CompanyBasedHierarchyModel companyBasedHierarchyModel = new CompanyBasedHierarchyModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objCompanyModelList = objCompany.GetAllCompanyGroups(CompanyID, GroupID,objUserContextViewModel.UserId);
            var UserId = objUserContextViewModel.UserId;
            EmployeeDB objempDB = new EmployeeDB();
            List<Employee> empList = objempDB.GetALLEmployeeByUserId(UserId);
            string employees = "";
            employees += "<option value=''>Select Employee Name</option>";
            foreach (var obj in empList)
            {
                employees += "<option value='" + obj.EmployeeId + "'>" + obj.FullName + "</option>";
            }
            var vList = new object();
            vList = new
            {
                aaData = (from item in objCompanyModelList
                          select new
                          {
                              CompanyId = item.CompanyID,
                              CompanyName = item.Name,
                              GroupID = item.GroupID,
                              ApproverGroup = item.GroupName,
                              EmployeeId = item.EmployeeID,
                              Approver = item.EmployeeName,
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditHeirarchy(this," + item.CompanyID + "," + item.GroupID + "," + item.autoID + "," + item.CBHierarchyID + " )' title='Edit' ><i class='fa fa-pencil'></i> </a> " +
                                        "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"changeStatus(this," + item.CompanyID + "," + item.GroupID + "," + item.autoID + "," + item.CBHierarchyID + ")\" title='Delete'><i class='fa fa-times'></i> </a>",
                              CBHierarchyID = item.CBHierarchyID,
                              AutoId = item.autoID,
                              EmpId = item.EmployeeID

                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        [HttpPost]
        public ActionResult AddUpdateCompanyHeirarchy(CompanyBasedHierarchyModel companyBasedHierarchyModel)
        {
            OperationDetails op = new OperationDetails();
            CompanyBasedHeirarchyDB heirarchyDb = new CompanyBasedHeirarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            companyBasedHierarchyModel.ModifiedBy = objUserContextViewModel.UserId;
            companyBasedHierarchyModel.ModifiedOn = DateTime.Now;
            op = heirarchyDb.UpdateCompanybasedHeirachy(companyBasedHierarchyModel);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteCompanyDetails(int CBHierarchyID)
        {
            OperationDetails op = new OperationDetails();
            CompanyBasedHeirarchyDB heirarchyDb = new CompanyBasedHeirarchyDB();
            op = heirarchyDb.DeleteCompanyDetails(CBHierarchyID);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveMultipleEmployees(List<CompanyBasedHierarchyModel> heirarchyList, CompanyBasedHierarchyModel heirarchyModel)
        {

            CompanyBasedHeirarchyDB heirachyDb = new CompanyBasedHeirarchyDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            heirarchyModel.ModifiedBy = objUserContextViewModel.UserId; ;
            heirarchyModel.ModifiedOn = DateTime.Now;
            return Json(heirachyDb.UpdatMultipleEmployees(heirarchyList, heirarchyModel), JsonRequestBehavior.AllowGet);
        }
        //*** Naresh 2020-03-24 Added export functionality
        public ActionResult ExportToExcel(int? companyId, Int16? groupId)
        {
            byte[] content;
            string fileName = "Organization Based Hierarchy- " + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CompanyBasedHeirarchyDB objCompany = new CompanyBasedHeirarchyDB();
            System.Data.DataSet ds = objCompany.GetAllCompanyGroupsDataset(companyId, groupId);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(int? companyId, Int16? groupId)
        {
            string fileName = "Organization Based Hierarchy- " + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            CompanyBasedHeirarchyDB objCompany = new CompanyBasedHeirarchyDB();
            System.Data.DataSet ds = objCompany.GetAllCompanyGroupsDataset(companyId, groupId);
            iTextSharp.text.Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }
    }
}