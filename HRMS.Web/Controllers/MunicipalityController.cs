﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;



namespace HRMS.Web.Controllers
{
    public class MunicipalityController : BaseController
    {
        public ActionResult Index()
        {
            if (Session["EmployeeListID"] == null)
            {
                ViewBag.empid = "";
            }
            else
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }


            return View();
        }

        public ActionResult GetMunicipalityList()
        {
            string empid = "";
            if (Session["EmployeeListID"] == null)
            {
                return null;
            }
            else
            {
                empid = Session["EmployeeListID"].ToString();
            }
            PermissionModel permissionModel = HRMS.Web.CommonHelper.CommonHelper.CheckPermissionForUser("/Municipality");
            string Action = "";

            if (permissionModel.IsUpdateOnlyPermission && permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditMunicipality(item)' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewMunicipality(item)' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteMunicipality(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
            }
            else if (!permissionModel.IsUpdateOnlyPermission && !permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewMunicipality(item)' title='View' ><i class='fa fa-eye'></i> </a>";
            }
            else
            {
                if (permissionModel.IsUpdateOnlyPermission)
                {
                    Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditMunicipality(item)' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewMunicipality(item)' title='View' ><i class='fa fa-eye'></i> </a>";
                }
                if (permissionModel.IsDeleteOnlyPermission)
                {
                    Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewMunicipality(item)' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteMunicipality(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
                }
            }

            DBHelper objDBHelper = new DBHelper();
            CountryDB objCountryDB = new CountryDB();
            IssuePlaceDB objIssuePlaceDB = new IssuePlaceDB();
            var vList = new object();
            List<DocumentsModel> objDocumentsModelList = new List<DocumentsModel>();
            DocumentDB objDocumentDB = new DocumentDB();
            objDocumentsModelList = objDocumentDB.GetDocumentList(Convert.ToInt32(empid), 7);
            vList = new
            {
                aaData = (from item in objDocumentsModelList
                          select new
                          {
                              Action = Action.Replace("item", item.DocId.ToString()),
                              DocMunicipalityID = item.DocId,
                              DocumentNo = item.ActualDocumentFile != null ? "<a href='#' onclick='ViewDocumentById(" + item.DocId.ToString() + ")'>" + item.DocNo + "</a>" : "<span title='No file available'>" + item.DocNo + "<span>",
                              IssueCountry = item.IssueCountry,
                              IssueDate =  item.IssueDate == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(item.IssueDate),
                              Note = item.Note,
                              IsPrimary = item.IsPrimary ? "Yes" : "No",
                              EmployeeId = item.EmployeeId,
                              Vacc1Date = item.VAcc1 == "" ? "" : item.VAcc1,
                              IssuePlace = item.IssuePlace,
                              ExpiryDate = item.ExpiryDate == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(item.ExpiryDate)
                          }).ToArray(),
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMunicipality()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CountryDB objCountryDB = new CountryDB();
            CityDB citydb = new CityDB();
            DocumentDB documentDB = new DocumentDB();
            int SchoolCompanyID = documentDB.GetSchoolCompanyId();
            CompanyDB companyDB = new CompanyDB();
            CompanyModel companyModal = new CompanyModel();
            companyModal = companyDB.GetCompany(SchoolCompanyID);
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName", companyModal.Country);
            ViewBag.cityList = new SelectList(citydb.GetCityByCountryId(Convert.ToString(companyModal.Country)), "CityId", "CityName", companyModal.City);
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FirstName");
            return View();
        }

        [HttpPost]
        public ActionResult AddMunicipality(DocumentsModel objDocumentsModel)
        {
            string sessionVariable = Request.Form["sessionVariable"];
            string empid = Session["EmployeeListID"].ToString();
            OperationDetails objOperationDetails = new OperationDetails();
            if (empid != "")
            {
                if (Session[sessionVariable] != null)
                {
                    EmployeeDocumentModel employeeDocumentModel = (EmployeeDocumentModel)Session[sessionVariable];
                    if (employeeDocumentModel.DocumentFile != null)
                    {
                        objDocumentsModel.DocumentFileName = employeeDocumentModel.ImageName;
                        objDocumentsModel.ActualDocumentFile = employeeDocumentModel.DocumentFile;
                        objDocumentsModel.FileContentType = employeeDocumentModel.FileContentType;
                        Session.Remove(sessionVariable);
                    }
                }
                objDocumentsModel.EmpId = Convert.ToInt32(empid);
                if (ModelState.IsValid)
                {
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    DocumentDB objDocumentDB = new DocumentDB();
                    objDocumentsModel.DocTypeId = 7;
                    objOperationDetails = objDocumentDB.AddEditDocument(objDocumentsModel, 1);
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please fill all the fields carefully.";
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please select an employee.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMunicipality(int id)
        {
            CountryDB objCountryDB = new CountryDB();
            DocumentDB objDocumentDB = new DocumentDB();
            CityDB obJCityDB = new CityDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DocumentsModel objDocumentsModel = new DocumentsModel();
            objDocumentsModel = objDocumentDB.GetDocumentByDocId(id);
            if (objDocumentsModel.ActualDocumentFile != null)
            {
                ViewBag.Status = "true";
            }
            else
            {
                ViewBag.Status = "false";
            }
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName", objDocumentsModel.IssueCountryID);
            ViewBag.City = new SelectList(obJCityDB.GetCityByCountryId(objDocumentsModel.IssueCountryID.ToString()), "CityId", "CityName", objDocumentsModel.IssueCityID);
            return View(objDocumentsModel);
        }

        [HttpPost]
        public ActionResult EditMunicipality(DocumentsModel objDocumentsModel)
        {
            string oldFile = objDocumentsModel.DocumentFile;
            string sessionVariable = Request.Form["sessionVariable"];
            string empid = Session["EmployeeListID"].ToString();
            OperationDetails objOperationDetails = new OperationDetails();
            if (empid != "")
            {
                if (Session[sessionVariable] != null)
                {
                    EmployeeDocumentModel employeeDocumentModel = (EmployeeDocumentModel)Session[sessionVariable];
                    if (employeeDocumentModel.DocumentFile != null)
                    {
                        objDocumentsModel.DocumentFileName = employeeDocumentModel.ImageName;
                        objDocumentsModel.ActualDocumentFile = employeeDocumentModel.DocumentFile;
                        objDocumentsModel.FileContentType = employeeDocumentModel.FileContentType;
                        Session.Remove(sessionVariable);
                    }
                }
                objDocumentsModel.EmpId = Convert.ToInt32(empid);

                if (ModelState.IsValid)
                {
                    DocumentDB objDocumentDB = new DocumentDB();
                    objOperationDetails = objDocumentDB.AddEditDocument(objDocumentsModel, 2);
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please fill all the fields carefully.";
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please select an employee.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewMunicipality(int id)
        {
            CountryDB objCountryDB = new CountryDB();
            CityDB obJCityDB = new CityDB();
            DocumentDB objDocumentDB = new DocumentDB();
            DocumentsModel objMunicipalityModel = new DocumentsModel();
            objMunicipalityModel = objDocumentDB.GetDocumentByDocId(id);
            if (objMunicipalityModel.ActualDocumentFile != null)
            {
                ViewBag.Status = "true";
            }
            else
            {
                ViewBag.Status = "false";
            }
            return View(objMunicipalityModel);
        }

        public ActionResult DeleteMunicipality(int id)
        {
            DocumentDB objDocumentDB = new DocumentDB();
            OperationDetails op = new OperationDetails();
            DocumentsModel objDocumentsModel = new DocumentsModel();
            objDocumentsModel.DocId = id;
            op = objDocumentDB.AddEditDocument(objDocumentsModel, 3);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

    }
}