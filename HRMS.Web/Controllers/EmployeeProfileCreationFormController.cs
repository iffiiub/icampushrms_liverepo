﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.General;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.Forms;
using HRMS.DataAccess.FormsDB;
using System.IO;
using Newtonsoft.Json;

namespace HRMS.Web.Controllers
{
    public class EmployeeProfileCreationFormController : FormsController
    {
        // GET: EmployeeProfileCreationForm
        EmployeeProfileCreationFormDB objEmployeeProfileCreationFormDB;
        public EmployeeProfileCreationFormController()
        {
            XMLLogFile = "LoggerEmployeeProfileCreationForm.xml";
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
        }
        public ActionResult Index()
        {
            return View(LoadR1FormRequestDetails(null));
        }
        public ActionResult Edit()
        {
            int formProcessID = GetFormProcessSessionID();
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            DBHelper objDBHelper = new DBHelper();
            GenderDB ObjGenderDB = new GenderDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            CountryDB objCountryDB = new CountryDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            ReligionDB objReligionDB = new ReligionDB();
            LanguageDB ObjLanguageDB = new LanguageDB();
            MaritialStatusDB ObjMaritialStatusDB = new MaritialStatusDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRContractTypeDB hrContractTypeDB = new HRContractTypeDB();
            objEmployeeProfileCreationFormModel.FormProcessID = formProcessID;
            //get current/pending approver group and employee
            RequestFormsApproveModel requestFormsApproveModel = objEmployeeProfileCreationFormDB.GetPendingFormsApproval(formProcessID);
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            objEmployeeProfileCreationFormModel = objEmployeeProfileCreationFormDB.GetProfileCreationFormRequestDetails(Convert.ToInt32(RequestStatus.Completed), formProcessID);
            //if login user is the present approver then can see form in edit mode 
            if (objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID || objUserContextViewModel.UserId == 0
                || (objEmployeeProfileCreationFormModel.ReqStatusID == (int)RequestStatus.Rejected && objEmployeeProfileCreationFormModel.RequesterEmployeeID == objUserContextViewModel.UserId))
            {

                //If form is pending then only show for edit
                if (objEmployeeProfileCreationFormModel.ReqStatusID == (int)RequestStatus.Pending
                    || (objEmployeeProfileCreationFormModel.ReqStatusID == (int)RequestStatus.Rejected && objEmployeeProfileCreationFormModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    //Comments field will always show the pending approver's comments while showing for edit
                    objEmployeeProfileCreationFormModel.Comments = requestFormsApproveModel.Comments;
                    ViewBag.SupervisorList = new SelectList(objEmployeeDB.GetAllEmployeeForAdminByCompanyID(objEmployeeProfileCreationFormModel.CompanyID), "EmployeeId", "FullName");
                    ViewBag.EmploymentTypeList = new SelectList(objDBHelper.GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");
                    ViewBag.CategoryList = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
                    ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
                    List<DepartmentModel> objDepartmentList = new List<DepartmentModel>();
                    objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
                    ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
                    ViewBag.JobGrade = new SelectList(objEmployeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
                    ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");
                    ViewBag.GenderList = new SelectList(ObjGenderDB.GetAllGenders(), "GenderID", "GenderName_1");
                    ViewBag.ReligionList = new SelectList(objReligionDB.GetAllReligion(), "ReligionID", "ReligionName_1");
                    ViewBag.LanguageList = new SelectList(ObjLanguageDB.GetAllLanguages(), "LanguageID", "LanguageName_1");
                    ViewBag.MaritalStatusList = new SelectList(ObjMaritialStatusDB.getAllMaritialStatus(), "MaritalStatusID", "MaritalStatusName_1");
                    ViewBag.DivisionList = new SelectList(objEmployeeProfileCreationFormDB.GetDivisionList(), "id", "text");
                    ViewBag.SalaryBasisFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetSalaryBasisFrequencyList(), "id", "text");
                    ViewBag.LeaveEntitlementList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementDaysList(), "id", "text");
                    ViewBag.LeaveEntitlementTypeList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementTypeList(), "id", "text");

                    if (objEmployeeProfileCreationFormModel.CountryID > 0)
                    {
                        CityDB objCity = new CityDB();
                        ViewBag.CityList = new SelectList(objCity.GetCityByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.CountryID)), "CityId", "CityName");
                        ViewBag.AirPortList = new SelectList(objEmployeeProfileCreationFormDB.GetAirPortByCountryId(objEmployeeProfileCreationFormModel.CountryID.Value), "id", "text");
                    }
                    else
                    {
                        ViewBag.CityList = new SelectList(Enumerable.Empty<SelectListItem>());
                        ViewBag.AirPortList = new SelectList(Enumerable.Empty<SelectListItem>());
                    }
                    if (objEmployeeProfileCreationFormModel.BirthCountryID > 0)
                    {
                        CityDB objCity = new CityDB();
                        ViewBag.BirthCityList = new SelectList(objCity.GetBirthPlaceByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.BirthCountryID)), "BirthPlaceID", "BirthPlaceName_1");
                    }
                    else
                    {
                        ViewBag.BirthCityList = new SelectList(Enumerable.Empty<SelectListItem>());
                    }
                    if (objEmployeeProfileCreationFormModel.NationalityID != null && objEmployeeProfileCreationFormModel.NationalityID.Length > 0)
                    {
                        List<string> selectedList = objEmployeeProfileCreationFormModel.NationalityID.Split(',').ToList();
                        List<SelectListItem> ddlNationalityList = ObjNationalityDB.getAllNationalities()
                        .Select(c => new SelectListItem
                        {
                            Text = c.NationalityName_1,
                            Value = c.NationalityID.ToString(),
                            Selected = selectedList.Contains(c.NationalityID.ToString()) ? true : false
                        }).ToList();
                        ViewBag.NationalityList = ddlNationalityList;
                        ViewBag.DefaultNationalityList = new SelectList(ObjNationalityDB.getAllNationalities().Where(s => objEmployeeProfileCreationFormModel.NationalityID.Split(',').Contains(Convert.ToString(s.NationalityID))), "NationalityID", "NationalityName_1");
                    }
                    else
                    {
                        ViewBag.NationalityList = ObjNationalityDB.getAllNationalities().Select(s => new SelectListItem { Text = s.NationalityName_1, Value = s.NationalityID.ToString() }).ToList();
                        ViewBag.DefaultNationalityList = new SelectList(Enumerable.Empty<SelectListItem>());
                    }
                    ViewBag.contractList = new SelectList(hrContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1");
                    ViewBag.IsITGroup = requestFormsApproveModel.FormsWorkflowGroupModel == null ? false : requestFormsApproveModel.FormsWorkflowGroupModel.IsITGroup;
                    ViewBag.AssetTypesList = new SelectList(objEmployeeProfileCreationFormDB.GetAssetTypesList(), "id", "text");
                    ViewBag.EmployeeAirfareFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareFrequencyList(), "id", "text");
                    ViewBag.EmployeeAirfareClassesList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareClassesList(), "id", "text");
                    ViewBag.GroupId = requestFormsApproveModel.GroupID;
                    ViewBag.IsHrGroup = requestFormsApproveModel.FormsWorkflowGroupModel == null ? false : requestFormsApproveModel.FormsWorkflowGroupModel.IsHrGroup;
                    ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
                    ViewBag.JobStatusList = new SelectList(objEmployeeProfileCreationFormDB.GetJobStatusList(), "id", "text");
                    ViewBag.RecCategoryList = new SelectList(objEmployeeProfileCreationFormDB.GetRecruitCategoryList(), "id", "text");
                    ViewBag.PDRPFormsList = new SelectList(objEmployeeProfileCreationFormDB.GetPDRPFormsList(), "id", "text");
                    ViewBag.InsuranceCategory = new SelectList(new InsuranceDB().GetInsuranceTypeList(), "id", "text", objEmployeeProfileCreationFormModel.InsuranceCategory);
                    ViewBag.InsuranceEligibility = new SelectList(new InsuranceDB().GetInsuranceEligibilityList(), "id", "text", objEmployeeProfileCreationFormModel.InsuranceEligibility);
                    ViewBag.AccommodationTypeList = new SelectList(new AllowanceDB().GetAccommodationtype(), "id", "text", objEmployeeProfileCreationFormModel.AccommodationType);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
            {
                return RedirectToAction("ViewDetails");
            }
            return View("Edit", objEmployeeProfileCreationFormModel);
        }
        public ActionResult UpdateDetails()
        {
            int formProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            DBHelper objDBHelper = new DBHelper();
            GenderDB ObjGenderDB = new GenderDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            CountryDB objCountryDB = new CountryDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            ReligionDB objReligionDB = new ReligionDB();
            LanguageDB ObjLanguageDB = new LanguageDB();
            MaritialStatusDB ObjMaritialStatusDB = new MaritialStatusDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRContractTypeDB hrContractTypeDB = new HRContractTypeDB();
            if (formProcessID > 0)
            {
                RequestFormsApproveModel requestFormsApproveModel = objEmployeeProfileCreationFormDB.GetPendingFormsApproval(formProcessID);
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                objEmployeeProfileCreationFormModel = objEmployeeProfileCreationFormDB.GetProfileCreationFormRequestDetails(Convert.ToInt32(RequestStatus.Completed), formProcessID);
                objEmployeeProfileCreationFormModel.FormProcessID = formProcessID;
                if (objEmployeeProfileCreationFormModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    ViewBag.SupervisorList = new SelectList(objEmployeeDB.GetAllEmployeeForAdminByCompanyID(objEmployeeProfileCreationFormModel.CompanyID), "EmployeeId", "FullName");
                    ViewBag.EmploymentTypeList = new SelectList(objDBHelper.GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");
                    ViewBag.CategoryList = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");                    
                    ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
                    List<DepartmentModel> objDepartmentList = new List<DepartmentModel>();
                    objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
                    ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
                    ViewBag.JobGrade = new SelectList(objEmployeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
                    ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");
                    ViewBag.GenderList = new SelectList(ObjGenderDB.GetAllGenders(), "GenderID", "GenderName_1");
                    ViewBag.ReligionList = new SelectList(objReligionDB.GetAllReligion(), "ReligionID", "ReligionName_1");
                    ViewBag.LanguageList = new SelectList(ObjLanguageDB.GetAllLanguages(), "LanguageID", "LanguageName_1");
                    ViewBag.MaritalStatusList = new SelectList(ObjMaritialStatusDB.getAllMaritialStatus(), "MaritalStatusID", "MaritalStatusName_1");
                    ViewBag.DivisionList = new SelectList(objEmployeeProfileCreationFormDB.GetDivisionList(), "id", "text");
                    ViewBag.SalaryBasisFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetSalaryBasisFrequencyList(), "id", "text");
                    ViewBag.LeaveEntitlementList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementDaysList(), "id", "text");
                    ViewBag.LeaveEntitlementTypeList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementTypeList(), "id", "text");
                    ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
                    if (objEmployeeProfileCreationFormModel.CountryID > 0)
                    {
                        CityDB objCity = new CityDB();
                        ViewBag.CityList = new SelectList(objCity.GetCityByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.CountryID)), "CityId", "CityName");
                        ViewBag.AirPortList = new SelectList(objEmployeeProfileCreationFormDB.GetAirPortByCountryId(objEmployeeProfileCreationFormModel.CountryID.Value), "id", "text");
                    }
                    else
                    {
                        ViewBag.CityList = new SelectList(Enumerable.Empty<SelectListItem>());
                        ViewBag.AirPortList = new SelectList(Enumerable.Empty<SelectListItem>());
                    }
                    if (objEmployeeProfileCreationFormModel.BirthCountryID > 0)
                    {
                        CityDB objCity = new CityDB();
                        ViewBag.BirthCityList = new SelectList(objCity.GetBirthPlaceByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.BirthCountryID)), "BirthPlaceID", "BirthPlaceName_1");
                    }
                    else
                    {
                        ViewBag.BirthCityList = new SelectList(Enumerable.Empty<SelectListItem>());
                    }
                    if (objEmployeeProfileCreationFormModel.NationalityID != null && objEmployeeProfileCreationFormModel.NationalityID.Length > 0)
                    {
                        List<string> selectedList = objEmployeeProfileCreationFormModel.NationalityID.Split(',').ToList();
                        List<SelectListItem> ddlNationalityList = ObjNationalityDB.getAllNationalities()
                        .Select(c => new SelectListItem
                        {
                            Text = c.NationalityName_1,
                            Value = c.NationalityID.ToString(),
                            Selected = selectedList.Contains(c.NationalityID.ToString()) ? true : false
                        }).ToList();
                        ViewBag.NationalityList = ddlNationalityList;
                        ViewBag.DefaultNationalityList = new SelectList(ObjNationalityDB.getAllNationalities().Where(s => objEmployeeProfileCreationFormModel.NationalityID.Split(',').Contains(Convert.ToString(s.NationalityID))), "NationalityID", "NationalityName_1");
                    }
                    else
                    {
                        ViewBag.NationalityList = ObjNationalityDB.getAllNationalities().Select(s => new SelectListItem { Text = s.NationalityName_1, Value = s.NationalityID.ToString() }).ToList();
                        ViewBag.DefaultNationalityList = new SelectList(Enumerable.Empty<SelectListItem>());
                    }
                    ViewBag.contractList = new SelectList(hrContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1");                    
                    ViewBag.AssetTypesList = new SelectList(objEmployeeProfileCreationFormDB.GetAssetTypesList(), "id", "text");
                    ViewBag.EmployeeAirfareFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareFrequencyList(), "id", "text");
                    ViewBag.EmployeeAirfareClassesList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareClassesList(), "id", "text");
                    ViewBag.GroupId = requestFormsApproveModel.GroupID;
                    ViewBag.IsITGroup = true;
                    ViewBag.IsHrGroup = true;
                    ViewBag.PDRPFormsList = new SelectList(objEmployeeProfileCreationFormDB.GetPDRPFormsList(), "id", "text", objEmployeeProfileCreationFormModel.PDRPFormID); 
                    ViewBag.InsuranceCategory = new SelectList(new InsuranceDB().GetInsuranceTypeList(), "id", "text", objEmployeeProfileCreationFormModel.InsuranceCategory);
                    ViewBag.InsuranceEligibility = new SelectList(new InsuranceDB().GetInsuranceEligibilityList(), "id", "text", objEmployeeProfileCreationFormModel.InsuranceEligibility);
                    ViewBag.AccommodationTypeList = new SelectList(new AllowanceDB().GetAccommodationtype(), "id", "text", objEmployeeProfileCreationFormModel.AccommodationType);
                    return View(objEmployeeProfileCreationFormModel);
                }
                else
                    return RedirectToAction("ViewDetails");
            }
            else
                return RedirectToAction("ViewDetails");
        }
        public ActionResult Create(int? requestId)
        {
            return PartialView("_Create", LoadR1FormRequestDetails(requestId));
        }
        public EmployeeProfileCreationFormModel LoadR1FormRequestDetails(int? requestId)
        {
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            DBHelper objDBHelper = new DBHelper();
            GenderDB ObjGenderDB = new GenderDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            CountryDB objCountryDB = new CountryDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            ReligionDB objReligionDB = new ReligionDB();
            LanguageDB ObjLanguageDB = new LanguageDB();
            MaritialStatusDB ObjMaritialStatusDB = new MaritialStatusDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRContractTypeDB hrContractTypeDB = new HRContractTypeDB();

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            objEmployeeProfileCreationFormModel.AllFormsFilesModelList = new List<AllFormsFilesModel>();
            objEmployeeProfileCreationFormModel.CreatedOn = DateTime.Today.Date;
            objEmployeeProfileCreationFormModel.RequesterEmployeeID = objUserContextViewModel.UserId;

            if (requestId.HasValue)
            {
                objEmployeeProfileCreationFormModel = objEmployeeProfileCreationFormDB.GetRequestDetailList(Convert.ToInt32(RequestStatus.Completed), requestId);
                ViewBag.SupervisorList = new SelectList(objEmployeeDB.GetAllEmployeeForAdminByCompanyID(objEmployeeProfileCreationFormModel.CompanyID), "EmployeeId", "FullName");
            }
            else
            {
                ViewBag.SupervisorList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            ViewBag.RequestList = new SelectList(objEmployeeProfileCreationFormDB.GetRequestList(Convert.ToInt32(RequestStatus.Completed)), "id", "text");
            ViewBag.EmploymentTypeList = new SelectList(objDBHelper.GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");
            ViewBag.UserTypeList = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
            //ViewBag.SubCategoryList = new SelectList(objEmployeeProfileCreationFormDB.GetSubCategory(), "id", "text");
            ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
            List<DepartmentModel> objDepartmentList = new List<DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            ViewBag.JobGrade = new SelectList(objEmployeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");

            ViewBag.GenderList = new SelectList(ObjGenderDB.GetAllGenders(), "GenderID", "GenderName_1");
            ViewBag.ReligionList = new SelectList(objReligionDB.GetAllReligion(), "ReligionID", "ReligionName_1");
            ViewBag.LanguageList = new SelectList(ObjLanguageDB.GetAllLanguages(), "LanguageID", "LanguageName_1");
            ViewBag.MaritalStatusList = new SelectList(ObjMaritialStatusDB.getAllMaritialStatus(), "MaritalStatusID", "MaritalStatusName_1");
            ViewBag.DivisionList = new SelectList(objEmployeeProfileCreationFormDB.GetDivisionList(), "id", "text");
            ViewBag.SalaryBasisFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetSalaryBasisFrequencyList(), "id", "text");
            ViewBag.LeaveEntitlementList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementDaysList(), "id", "text");
            ViewBag.LeaveEntitlementTypeList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementTypeList(), "id", "text");

            if (objEmployeeProfileCreationFormModel.CountryID > 0)
            {
                CityDB objCity = new CityDB();
                ViewBag.CityList = new SelectList(objCity.GetCityByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.CountryID)), "CityId", "CityName");
                ViewBag.AirPortList = new SelectList(objEmployeeProfileCreationFormDB.GetAirPortByCountryId(objEmployeeProfileCreationFormModel.CountryID.Value), "id", "text");
            }
            else
            {
                ViewBag.CityList = new SelectList(Enumerable.Empty<SelectListItem>());
                ViewBag.AirPortList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            if (objEmployeeProfileCreationFormModel.BirthCountryID > 0)
            {
                CityDB objCity = new CityDB();
                ViewBag.BirthCityList = new SelectList(objCity.GetBirthPlaceByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.BirthCountryID)), "BirthPlaceID", "BirthPlaceName_1");
            }
            else
            {
                ViewBag.BirthCityList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            if (objEmployeeProfileCreationFormModel.NationalityID != null && objEmployeeProfileCreationFormModel.NationalityID.Length > 0)
            {
                List<string> selectedList = objEmployeeProfileCreationFormModel.NationalityID.Split(',').ToList();
                List<SelectListItem> ddlNationalityList = ObjNationalityDB.getAllNationalities()
                .Select(c => new SelectListItem
                {
                    Text = c.NationalityName_1,
                    Value = c.NationalityID.ToString(),
                    Selected = selectedList.Contains(c.NationalityID.ToString()) ? true : false
                }).ToList();
                ViewBag.NationalityList = ddlNationalityList;
                ViewBag.DefaultNationalityList = new SelectList(ObjNationalityDB.getAllNationalities().Where(s => objEmployeeProfileCreationFormModel.NationalityID.Split(',').Contains(Convert.ToString(s.NationalityID))), "NationalityID", "NationalityName_1");
            }
            else
            {
                ViewBag.NationalityList = ObjNationalityDB.getAllNationalities().Select(s => new SelectListItem { Text = s.NationalityName_1, Value = s.NationalityID.ToString() }).ToList();
                ViewBag.DefaultNationalityList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            ViewBag.IsITGroup = false;
            ViewBag.AssetTypesList = new SelectList(Enumerable.Empty<SelectListItem>());
            ViewBag.contractList = new SelectList(hrContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1");
            ViewBag.EmployeeAirfareFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareFrequencyList(), "id", "text");
            ViewBag.EmployeeAirfareClassesList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareClassesList(), "id", "text");
            ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
            ViewBag.InsuranceCategory = new SelectList(new InsuranceDB().GetInsuranceTypeList(), "id", "text");
            ViewBag.InsuranceEligibility = new SelectList(new InsuranceDB().GetInsuranceEligibilityList(), "id", "text");
            ViewBag.AccommodationTypeList = new SelectList(new AllowanceDB().GetAccommodationtype(), "id", "text");
            objEmployeeProfileCreationFormModel.IsAddMode = true;
            //objEmployeeProfileCreationFormModel.CompanyID = objUserContextViewModel.CompanyId;

            ViewBag.RecCategoryList = new SelectList(objEmployeeProfileCreationFormDB.GetRecruitCategoryList(), "id", "text");
            ViewBag.JobStatusList = new SelectList(objEmployeeProfileCreationFormDB.GetJobStatusList(), "id", " text");
            ViewBag.PDRPFormsList = new SelectList(objEmployeeProfileCreationFormDB.GetPDRPFormsList(), "id", "text");

            return objEmployeeProfileCreationFormModel;
        }

        public JsonResult GetEmployeeList(int companyId)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            IEnumerable<Employee> empList = objEmployeeDB.GetALLEmployeeByUserId(0).Where(emp => emp.CompanyId == companyId);
            return Json(empList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCityList(int countryId)
        {
            CityDB objCity = new CityDB();
            return Json(objCity.GetCityByCountryId(Convert.ToString(countryId)), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBirthPlaceList(int countryId)
        {
            CityDB objCity = new CityDB();
            return Json(objCity.GetBirthPlaceByCountryId(Convert.ToString(countryId)), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAirPortListByCountryId(int countryId)
        {
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            return Json(objEmployeeProfileCreationFormDB.GetAirPortByCountryId(countryId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRequestList()
        {
            EmployeeProfileCreationFormDB objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            return Json(objEmployeeProfileCreationFormDB.GetRequestList(Convert.ToInt32(RequestStatus.Completed)), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAssetTypes()
        {
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            return Json(objEmployeeProfileCreationFormDB.GetAssetTypesList(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveEmployeeProfileCreationForm(EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel, HttpPostedFileBase InterviewFile, HttpPostedFileBase OfferLetter)
        {
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            OperationDetails operationDetails = new OperationDetails();
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
            AllFormsFilesModel ObjFile;
            if (InterviewFile != null)
            {
                ObjFile = new AllFormsFilesModel();
                ObjFile.FileName = InterviewFile.FileName;
                ObjFile.FileContentType = InterviewFile.ContentType;
                ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(InterviewFile);
                ObjFile.FormFileIDName = "InterviewFileID";
                uploadFileList.Add(ObjFile);
            }
            if (OfferLetter != null)
            {
                ObjFile = new AllFormsFilesModel();
                ObjFile.FileName = OfferLetter.FileName;
                ObjFile.FileContentType = OfferLetter.ContentType;
                ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(OfferLetter);
                ObjFile.FormFileIDName = "OfferLetterFileID";
                uploadFileList.Add(ObjFile);
            }

            objEmployeeProfileCreationFormModel.AllFormsFilesModelList = uploadFileList;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
           // objEmployeeProfileCreationFormModel.CompanyID = objUserContextViewModel.CompanyId;
            RequestFormsProcessModel objRequestFormsProcessModel = objEmployeeProfileCreationFormDB.SaveEmployeeProfileCreationForm(objEmployeeProfileCreationFormModel, objUserContextViewModel.UserId);
            if (objRequestFormsProcessModel != null)
            {
                List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                requestFormsApproverEmailModelList = objEmployeeProfileCreationFormDB.GetApproverEmailList(objRequestFormsProcessModel.FormProcessID.ToString(), objUserContextViewModel.UserId);
                SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);

                operationDetails.InsertedRowId = 1;
                operationDetails.Success = true;
                operationDetails.Message = "Request generated successfully.";
                operationDetails.CssClass = "success";
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateEmployeeProfileCreationForm(EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel, string AssetsAssignData, HttpPostedFileBase InterviewFile, HttpPostedFileBase OfferLetter)
        {
            List<ProfileAssignAssetsModel> objProfileAssignAssetsModel = JsonConvert.DeserializeObject<List<ProfileAssignAssetsModel>>(AssetsAssignData);
            OperationDetails operationDetails = new OperationDetails();
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
            List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
           // objEmployeeProfileCreationFormModel.CompanyID = objUserContextViewModel.CompanyId;
            AllFormsFilesModel ObjFile;
            if (InterviewFile != null)
            {
                ObjFile = new AllFormsFilesModel();
                ObjFile.FileName = InterviewFile.FileName;
                ObjFile.FileContentType = InterviewFile.ContentType;
                ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(InterviewFile);
                ObjFile.FormFileIDName = "InterviewFileID";
                uploadFileList.Add(ObjFile);
                if (objEmployeeProfileCreationFormModel.InterviewFileID > 0)
                {
                    new AllFormsFilesDB().DeleteAllFormsFile(objEmployeeProfileCreationFormModel.InterviewFileID, objEmployeeProfileCreationFormModel.FormProcessID, "InterviewFileID", objUserContextViewModel.UserId);
                }
                objEmployeeProfileCreationFormModel.InterviewFileID = 0;
            }
            if (OfferLetter != null)
            {
                ObjFile = new AllFormsFilesModel();
                ObjFile.FileName = OfferLetter.FileName;
                ObjFile.FileContentType = OfferLetter.ContentType;
                ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(OfferLetter);
                ObjFile.FormFileIDName = "OfferLetterFileID";
                uploadFileList.Add(ObjFile);
                if (objEmployeeProfileCreationFormModel.OfferLetterFileID > 0)
                {
                    new AllFormsFilesDB().DeleteAllFormsFile(objEmployeeProfileCreationFormModel.OfferLetterFileID, objEmployeeProfileCreationFormModel.FormProcessID, "OfferLetterFileID", objUserContextViewModel.UserId);
                }
                objEmployeeProfileCreationFormModel.OfferLetterFileID = 0;
            }

            objEmployeeProfileCreationFormModel.AllFormsFilesModelList = uploadFileList;

            RequestFormsApproveModel requestFormsApproveModel = objEmployeeProfileCreationFormDB.GetPendingFormsApproval(objEmployeeProfileCreationFormModel.FormProcessID);
            if (objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID
                 || (objEmployeeProfileCreationFormModel.ReqStatusID == (int)RequestStatus.Rejected && objEmployeeProfileCreationFormModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                 || isEditRequestFromAllRequests)
            {
                //If form is resubmitting , have to update all details
                if (objEmployeeProfileCreationFormModel.ReqStatusID == (int)RequestStatus.Rejected)
                    objEmployeeProfileCreationFormModel.IsUpdateDetailMode = 1;
                operationDetails = objEmployeeProfileCreationFormDB.UpdateEmployeeProfileCreationForm(objEmployeeProfileCreationFormModel, objUserContextViewModel.UserId);
                if (objProfileAssignAssetsModel != null)
                {
                    objEmployeeProfileCreationFormDB.UpdateProfileAssignAssets(objProfileAssignAssetsModel, objEmployeeProfileCreationFormModel.ID);
                    operationDetails.Message += @" & Assets Details Updated successfully";
                }
                if (objEmployeeProfileCreationFormModel.ReqStatusID == (int)RequestStatus.Rejected && operationDetails.Success)
                {
                    List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                    requestFormsApproverEmailModelList = objEmployeeProfileCreationFormDB.GetApproverEmailList(objEmployeeProfileCreationFormModel.FormProcessID.ToString(), objEmployeeProfileCreationFormModel.RequesterEmployeeID);
                    SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                }
            }
            else
            {
                operationDetails.Success = true;
                operationDetails.Message = "No permissions to update.";
                operationDetails.CssClass = "error";
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public override ActionResult ApproveForm(int formProcessID, string comments)
        {
            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                FormsDB formsDB = new FormsDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                RequestFormsApproveModel requestFormsApproveModel = new RequestFormsApproveModel();
                requestFormsApproveModel.FormProcessID = formProcessID;
                requestFormsApproveModel.Comments = comments;
                requestFormsApproveModel.ApproverEmployeeID = objUserContextViewModel.UserId;
                requestFormsApproveModel.ReqStatusID = (int)RequestStatus.Approved;
                operationDetails = formsDB.ApproveRequestForm(requestFormsApproveModel);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";

                    //if ReqStatus is approved, then only need to send next approval related emails
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Approved)
                    {
                        requestFormsApproverEmailModelList = formsDB.GetApproverEmailList(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Email send to next approver";
                        }
                    }
                    //ReqStatusID is 4/Completed Final Approval is done
                    if (operationDetails.InsertedRowId == (int)RequestStatus.Completed)
                    {
                        //Getting the final email notification group's employee & email details
                        requestFormsApproverEmailModelList = formsDB.GetFinalNotificationEmail(formProcessID);
                        if (requestFormsApproverEmailModelList != null && requestFormsApproverEmailModelList.Count > 0)
                        {
                            SendApproverEmail(requestFormsApproverEmailModelList);
                            operationDetails.Message += @" & Final Email Notification send";
                        }

                        //Add any logic to be do once approval process is completed
                        EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
                        objEmployeeProfileCreationFormModel = objEmployeeProfileCreationFormDB.GetProfileCreationFormRequestDetails(Convert.ToInt32(RequestStatus.Completed), formProcessID);

                        EmployeeDetailsModel objEmployeeDetailsModel = new EmployeeDetailsModel();
                        objEmployeeDetailsModel.CompanyID = objEmployeeProfileCreationFormModel.CompanyID;
                        objEmployeeDetailsModel.FirstName_1 = objEmployeeProfileCreationFormModel.FirstName;
                        objEmployeeDetailsModel.MiddleName_1 = objEmployeeProfileCreationFormModel.MiddleName;
                        objEmployeeDetailsModel.SurName_1 = objEmployeeProfileCreationFormModel.SurName;
                        objEmployeeDetailsModel.GenderID = objEmployeeProfileCreationFormModel.GenderID;
                        objEmployeeDetailsModel.MaritalStatusID = objEmployeeProfileCreationFormModel.MaritalStatusID ?? 0;
                        objEmployeeDetailsModel.NationalityID = objEmployeeProfileCreationFormModel.NationalityID;
                        objEmployeeDetailsModel.ReligionID = objEmployeeProfileCreationFormModel.ReligionID;
                        objEmployeeDetailsModel.HomeLanguageID = objEmployeeProfileCreationFormModel.LanguageID;
                        objEmployeeDetailsModel.BirthDate = objEmployeeProfileCreationFormModel.DOB;
                        objEmployeeDetailsModel.BirthPlaceID = objEmployeeProfileCreationFormModel.BirthCityID;
                        objEmployeeDetailsModel.BirthCountryID = objEmployeeProfileCreationFormModel.BirthCountryID ?? 0;
                        objEmployeeDetailsModel.isActive = true;
                        objEmployeeDetailsModel.usertypeid = objEmployeeProfileCreationFormModel.UserTypeID;
                        objEmployeeDetailsModel.DefaultNationalityID = objEmployeeProfileCreationFormModel.DefaultNationalityID;
                        objEmployeeDetailsModel.CommunicationLanguageID = objEmployeeProfileCreationFormModel.LanguageID;
                        objEmployeeDetailsModel.MothersName_1 = objEmployeeProfileCreationFormModel.MotherName;
                        objEmployeeDetailsModel.ModifiedBy = objUserContextViewModel.UserId;
                        objEmployeeDetailsModel.EmployeeAlternativeID = objEmployeeProfileCreationFormModel.EmployeeAlternativeID;

                        EmployeeContactModel objEmployeeContactModel = new EmployeeContactModel();
                        objEmployeeContactModel.WorkEmail = objEmployeeProfileCreationFormModel.WorkEmailID;
                        objEmployeeContactModel.MobileNumberHome = objEmployeeProfileCreationFormModel.MobileNumber;
                        objEmployeeContactModel.MobileNumberWork = objEmployeeProfileCreationFormModel.LandlinePhone;
                        objEmployeeContactModel.ModifiedBy = objUserContextViewModel.UserId;

                        EmploymentInformation objEmploymentInformation = new EmploymentInformation();
                        objEmploymentInformation.DepartmentID = objEmployeeProfileCreationFormModel.DepartmentID;
                        objEmploymentInformation.PositionID = objEmployeeProfileCreationFormModel.PositionID;
                        objEmploymentInformation.SuperviserID = objEmployeeProfileCreationFormModel.SupervisorID;
                        objEmploymentInformation.EmployeeContractTermId = objEmployeeProfileCreationFormModel.HRContractTypeID ?? 0;
                        objEmploymentInformation.EmployementMode = Convert.ToString(objEmployeeProfileCreationFormModel.EmploymentModeId);
                        objEmploymentInformation.CompanyId = objEmployeeProfileCreationFormModel.CompanyID;
                        objEmploymentInformation.NoticePeriod = objEmployeeProfileCreationFormModel.NoticedPeriod.ToString();
                        objEmploymentInformation.HireDate = objEmployeeProfileCreationFormModel.JoiningDate;
                        objEmploymentInformation.ModifiedBy = objUserContextViewModel.UserId;

                        objEmploymentInformation.JobGradeID = Convert.ToInt16(objEmployeeProfileCreationFormModel.JobGradeID);
                        objEmploymentInformation.ContractStatus = objEmployeeProfileCreationFormModel.ContractStatus;
                        objEmploymentInformation.FamilySpouse = objEmployeeProfileCreationFormModel.FamilySpouse;
                        objEmploymentInformation.AnnualAirTicket = objEmployeeProfileCreationFormModel.AnnualAirTicket;
                        objEmploymentInformation.HealthInsurance = objEmployeeProfileCreationFormModel.HealthInsurance;
                        objEmploymentInformation.LifeInsurance = objEmployeeProfileCreationFormModel.LifeInsurance;
                        objEmploymentInformation.SalaryBasisID = Convert.ToInt16(objEmployeeProfileCreationFormModel.SalaryBasisID);
                        objEmploymentInformation.CostCenter = objEmployeeProfileCreationFormModel.CostCenter;
                        objEmploymentInformation.CostCenterCode = objEmployeeProfileCreationFormModel.CostCenterCode;
                        objEmploymentInformation.LocationCode = objEmployeeProfileCreationFormModel.LocationCode;
                        objEmploymentInformation.OfficeLocation = objEmployeeProfileCreationFormModel.OfficeLocation;
                        objEmploymentInformation.ProjectData = objEmployeeProfileCreationFormModel.ProjectData;
                        objEmploymentInformation.ProbationPeriod = objEmployeeProfileCreationFormModel.ProbationPeriod;
                        objEmploymentInformation.LeaveEntitleDaysID = Convert.ToInt16(objEmployeeProfileCreationFormModel.LeaveEntitleDaysID);
                        objEmploymentInformation.LeaveEntitleTypeID = Convert.ToInt16(objEmployeeProfileCreationFormModel.LeaveEntitleTypeID);

                        objEmploymentInformation.DivisionID = objEmployeeProfileCreationFormModel.DivisionID;
                        objEmploymentInformation.RecCategoryID = objEmployeeProfileCreationFormModel.RecCategoryID;
                        objEmploymentInformation.JobStatusID = objEmployeeProfileCreationFormModel.JobStatusID;
                        objEmploymentInformation.Extension = objEmployeeProfileCreationFormModel.Extension;
                        objEmploymentInformation.JoiningDate = objEmployeeProfileCreationFormModel.JoiningDate;
                        objEmploymentInformation.PDRPFormId = objEmployeeProfileCreationFormModel.PDRPFormID;
                        objEmploymentInformation.OfferLetterPositionID = objEmployeeProfileCreationFormModel.PositionID;
                        objEmploymentInformation.EmployeeJobCategoryID = objEmployeeProfileCreationFormModel.EmployeeJobCategoryID;

                        EmployeePersonalIdentityInfoModel objEmployeePersonalIdentityInfoModel = new EmployeePersonalIdentityInfoModel();
                        objEmployeePersonalIdentityInfoModel.PassportNumber = objEmployeeProfileCreationFormModel.PassportNo;
                        objEmployeePersonalIdentityInfoModel.PassportIssueDate = objEmployeeProfileCreationFormModel.PassportIssueDate;
                        objEmployeePersonalIdentityInfoModel.PassportExpiryDate = objEmployeeProfileCreationFormModel.PassportExpiryDate;

                        HRMS.Entities.Employee objEmployee = new HRMS.Entities.Employee();
                        objEmployee.employeeDetailsModel = objEmployeeDetailsModel;
                        objEmployee.employeeContactModel = objEmployeeContactModel;
                        objEmployee.employmentInformation = objEmploymentInformation;
                        objEmployee.employeePersonalIdentityInfoModel = objEmployeePersonalIdentityInfoModel;
                        string previousResultMessage = operationDetails.Message;
                        operationDetails = UpdateEmployeeDetails(objEmployee, null, null, null, null, null, null, null, null, null);
                        operationDetails.Message = previousResultMessage + @" & Employee details saved successfully";
                        objEmployeeProfileCreationFormModel.EmployeeID = operationDetails.InsertedRowId;
                        objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
                        objEmployeeProfileCreationFormDB.UpdateEmployeeIdToEmployeeProfileCreationForm(objEmployeeProfileCreationFormModel.ID, objEmployeeProfileCreationFormModel.EmployeeID, objUserContextViewModel.UserId);
                        objEmployeeProfileCreationFormDB.UpdateLineMangerOfEmployee(objEmployeeProfileCreationFormModel.EmployeeID, objEmployeeProfileCreationFormModel.SupervisorID, objUserContextViewModel.UserId);

                        AllowanceModel AllowanceModel = new AllowanceModel();
                        AllowanceModel.EmpAirFare.AirTicketCountryID = Convert.ToInt32(objEmployeeProfileCreationFormModel.CountryID);
                        AllowanceModel.EmpAirFare.AirTicketCityID = Convert.ToInt32(objEmployeeProfileCreationFormModel.CityID);
                        AllowanceModel.EmpAirFare.AirTicketClassID = Convert.ToInt32(objEmployeeProfileCreationFormModel.AirfareClassID);
                        AllowanceModel.EmpAirFare.AirfareFrequencyID = Convert.ToInt32(objEmployeeProfileCreationFormModel.AirfareFrequencyID);
                        AllowanceModel.EmpAirFare.AirportListID = Convert.ToInt32(objEmployeeProfileCreationFormModel.AirportListID);
                        AllowanceModel.EmployeeID = Convert.ToInt32(objEmployeeProfileCreationFormModel.EmployeeID);
                        AllowanceModel.EmpAirFare.AnnualAirTicket = objEmployeeProfileCreationFormModel.AnnualAirTicket;
                        OperationDetails objOperationDetails = new OperationDetails();
                        AllowanceDB objAllowanceDB = new AllowanceDB();
                        objOperationDetails = objAllowanceDB.AddAirfareDetails(AllowanceModel);
                    }

                }
                //0 means no more approval permission for this user /already approved or rejected
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;

                }
                //Exception is hit at db level while saving approval operation
                else
                {
                    operationDetails.CssClass = "error";
                    operationDetails.Success = false;
                    operationDetails.Message = "Error while approving form request";
                }

            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteAllFormsFile(int fileID, int formProcessID, string fileIDFieldName)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int res = new AllFormsFilesDB().DeleteAllFormsFile(fileID, formProcessID, fileIDFieldName, objUserContextViewModel.UserId);
            return Json(new { result = res }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ViewDetails(int? id)
        {
            int formProcessID = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            string allcomments = "";
            objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
            EmployeeProfileCreationFormModel objEmployeeProfileCreationFormModel = new EmployeeProfileCreationFormModel();
            DBHelper objDBHelper = new DBHelper();
            GenderDB ObjGenderDB = new GenderDB();
            DepartmentDB objDepartmentDB = new DepartmentDB();
            CountryDB objCountryDB = new CountryDB();
            NationalityDB ObjNationalityDB = new NationalityDB();
            ReligionDB objReligionDB = new ReligionDB();
            LanguageDB ObjLanguageDB = new LanguageDB();
            MaritialStatusDB ObjMaritialStatusDB = new MaritialStatusDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            HRContractTypeDB hrContractTypeDB = new HRContractTypeDB();

            //get current/pending approver group and employee
            RequestFormsApproveModel requestFormsApproveModel = objEmployeeProfileCreationFormDB.GetPendingFormsApproval(formProcessID);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeProfileCreationFormModel = objEmployeeProfileCreationFormDB.GetProfileCreationFormRequestDetails(Convert.ToInt32(RequestStatus.Completed), formProcessID);
            ViewBag.SupervisorList = new SelectList(objEmployeeDB.GetAllEmployeeForAdminByCompanyID(objEmployeeProfileCreationFormModel.CompanyID), "EmployeeId", "FullName");
            ViewBag.EmploymentTypeList = new SelectList(objDBHelper.GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");
            ViewBag.CategoryList = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
            //ViewBag.SubCategoryList = new SelectList(objEmployeeProfileCreationFormDB.GetSubCategory(), "id", "text");
            ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
            List<DepartmentModel> objDepartmentList = new List<DepartmentModel>();
            objDepartmentList = objDepartmentDB.GetAllDepartment(userId: objUserContextViewModel.UserId);
            ViewBag.Department = new SelectList(objDepartmentList, "DepartmentId", "DepartmentName_1");
            ViewBag.JobGrade = new SelectList(objEmployeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName");
            ViewBag.GenderList = new SelectList(ObjGenderDB.GetAllGenders(), "GenderID", "GenderName_1");
            ViewBag.ReligionList = new SelectList(objReligionDB.GetAllReligion(), "ReligionID", "ReligionName_1");
            ViewBag.LanguageList = new SelectList(ObjLanguageDB.GetAllLanguages(), "LanguageID", "LanguageName_1");
            ViewBag.MaritalStatusList = new SelectList(ObjMaritialStatusDB.getAllMaritialStatus(), "MaritalStatusID", "MaritalStatusName_1");
            ViewBag.DivisionList = new SelectList(objEmployeeProfileCreationFormDB.GetDivisionList(), "id", "text");
            ViewBag.SalaryBasisFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetSalaryBasisFrequencyList(), "id", "text");
            ViewBag.LeaveEntitlementList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementDaysList(), "id", "text");
            ViewBag.LeaveEntitlementTypeList = new SelectList(objEmployeeProfileCreationFormDB.GetLeaveEntitlementTypeList(), "id", "text");

            if (objEmployeeProfileCreationFormModel.CountryID > 0)
            {
                CityDB objCity = new CityDB();
                ViewBag.CityList = new SelectList(objCity.GetCityByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.CountryID)), "CityId", "CityName");
                ViewBag.AirPortList = new SelectList(objEmployeeProfileCreationFormDB.GetAirPortByCountryId(objEmployeeProfileCreationFormModel.CountryID.Value), "id", "text");
            }
            else
            {
                ViewBag.CityList = new SelectList(Enumerable.Empty<SelectListItem>());
                ViewBag.AirPortList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            if (objEmployeeProfileCreationFormModel.BirthCountryID > 0)
            {
                CityDB objCity = new CityDB();
                ViewBag.BirthCityList = new SelectList(objCity.GetBirthPlaceByCountryId(Convert.ToString(objEmployeeProfileCreationFormModel.BirthCountryID)), "BirthPlaceID", "BirthPlaceName_1");
            }
            else
            {
                ViewBag.BirthCityList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            if (objEmployeeProfileCreationFormModel.NationalityID != null && objEmployeeProfileCreationFormModel.NationalityID.Length > 0)
            {
                List<string> selectedList = objEmployeeProfileCreationFormModel.NationalityID.Split(',').ToList();
                List<SelectListItem> ddlNationalityList = ObjNationalityDB.getAllNationalities()
                .Select(c => new SelectListItem
                {
                    Text = c.NationalityName_1,
                    Value = c.NationalityID.ToString(),
                    Selected = selectedList.Contains(c.NationalityID.ToString()) ? true : false
                }).ToList();
                ViewBag.NationalityList = ddlNationalityList;
                ViewBag.DefaultNationalityList = new SelectList(ObjNationalityDB.getAllNationalities().Where(s => objEmployeeProfileCreationFormModel.NationalityID.Split(',').Contains(Convert.ToString(s.NationalityID))), "NationalityID", "NationalityName_1");
            }
            else
            {
                ViewBag.NationalityList = ObjNationalityDB.getAllNationalities().Select(s => new SelectListItem { Text = s.NationalityName_1, Value = s.NationalityID.ToString() }).ToList();
                ViewBag.DefaultNationalityList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            ViewBag.IsITGroup = false;
            ViewBag.IsShowOfferLetter = requestFormsApproveModel.FormsWorkflowGroupModel == null ? false : requestFormsApproveModel.FormsWorkflowGroupModel.IsITGroup;
            ViewBag.AssetTypesList = new SelectList(objEmployeeProfileCreationFormDB.GetAssetTypesList(), "id", "text");
            ViewBag.contractList = new SelectList(hrContractTypeDB.GetAllHRContractType(), "HRContractTypeID", "HRContractTypeName_1");
            ViewBag.EmployeeAirfareFrequencyList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareFrequencyList(), "id", "text");
            ViewBag.EmployeeAirfareClassesList = new SelectList(objEmployeeProfileCreationFormDB.GetEmployeeAirfareClassesList(), "id", "text");
            ViewBag.AllComments = allcomments;
            objEmployeeProfileCreationFormModel.FormProcessID = formProcessID;
            ViewBag.RecCategoryList = new SelectList(objEmployeeProfileCreationFormDB.GetRecruitCategoryList(), "id", "text");
            ViewBag.JobStatusList = new SelectList(objEmployeeProfileCreationFormDB.GetJobStatusList(), "id", " text");
            ViewBag.PDRPFormsList = new SelectList(objEmployeeProfileCreationFormDB.GetPDRPFormsList(), "id", "text");
            ViewBag.InsuranceCategory = new SelectList(new InsuranceDB().GetInsuranceTypeList(), "id", "text");
            ViewBag.InsuranceEligibility = new SelectList(new InsuranceDB().GetInsuranceEligibilityList(), "id", "text");
            ViewBag.AccommodationTypeList = new SelectList(new AllowanceDB().GetAccommodationtype(), "id", "text");
            return View("ViewDetail", objEmployeeProfileCreationFormModel);
        }
        public OperationDetails UpdateEmployeeDetails(HRMS.Entities.Employee objEmp, IList<string> txtDynamicEmails, IList<string> txtDynamicPersonalEmail, IList<string> ddDynamicEmailType, IList<string> txtDynamicIM, IList<string> ddDynamicIMType, IList<string> txtDynamicMobile, IList<string> txtDynamicPhone, IList<string> ddDynamicMobileType, string hidMultipleIMContact)
        {
            OperationDetails operationDetails = new OperationDetails();
            int EmployeeID = 0;
            try
            {
                objEmp.employeeDetailsModel.EmployeeID = objEmp.EmployeeId;
                objEmp.employeeDetailsModel.CompanyID = objEmp.employmentInformation.CompanyId;
                EmployeeID = UpdateEmployeePersonalDetails(objEmp.employeeDetailsModel);
                objEmp.employeeContactModel.EmployeeID = EmployeeID;
                UpdateEmployeeContactDetails(objEmp.employeeContactModel, objEmp.employeeAddressInfoModel, txtDynamicEmails, txtDynamicPersonalEmail, ddDynamicEmailType, txtDynamicIM, ddDynamicIMType, txtDynamicMobile, txtDynamicPhone, ddDynamicMobileType, hidMultipleIMContact);
                objEmp.employmentInformation.EmployeeID = EmployeeID;
                UpdateEmploymentInformation(objEmp.employmentInformation);

                objEmp.employeePersonalIdentityInfoModel.EmployeeID = EmployeeID;
                UpdateEmployeePersonalIdentity(objEmp.employeePersonalIdentityInfoModel);
                operationDetails.Success = true;
                operationDetails.CssClass = "success";
                operationDetails.Message = "Employee details saved successfully";
                operationDetails.InsertedRowId = EmployeeID;
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.CssClass = "error";
                operationDetails.Message = "Error while saving employee details";
            }
            return operationDetails;
        }
        public int UpdateEmployeePersonalDetails(EmployeeDetailsModel ObjEmployeeDetailsModel)
        {
            int EmployeeID = 0;
            OperationDetails operationDetails = new OperationDetails();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ObjEmployeeDetailsModel.NationalityIDCardNo = ObjEmployeeDetailsModel.NationalityIDCardNo ?? "";

            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            MaritialStatusDB objMaritialStatusDB = new MaritialStatusDB();
            MaritialStausModel objMaritialStausModel = new MaritialStausModel();

            ObjEmployeeDetailsModel.ModifiedBy = objUserContextViewModel.UserId;

            #region Generate random password for new employee

            if (ObjEmployeeDetailsModel.EmployeeID == 0)
            {
                ObjEmployeeDetailsModel.Password = RandomString(8);// HRMS.Web.CommonHelper.CommonHelper.EncryptPassword(ObjEmployeeDetailsModel.NormalPassword);
            }

            #endregion
            if (ObjEmployeeDetailsModel.SpokenLanguages != null)
            {
                if (ObjEmployeeDetailsModel.SpokenLanguages.Where(x => x.IsNative == true).Count() > 0)
                {
                    ObjEmployeeDetailsModel.HomeLanguageID = ObjEmployeeDetailsModel.SpokenLanguages.FirstOrDefault(x => x.IsNative == true).LanguageId;
                }
                else
                    ObjEmployeeDetailsModel.HomeLanguageID = null;
            }

            operationDetails = objEmployeeDB.UpdateEmployeePersonalInformation(ObjEmployeeDetailsModel);
            EmployeeID = operationDetails.InsertedRowId;
            return EmployeeID;
        }
        //Updating ContactDetail Info
        public OperationDetails UpdateEmployeeContactDetails(EmployeeContactModel ObjEmployeeDetailsModel, EmployeeAddressInfoModel AddressModel, IList<string> txtDynamicEmails, IList<string> txtDynamicPersonalEmail, IList<string> ddDynamicEmailType, IList<string> txtDynamicIM, IList<string> ddDynamicIMType, IList<string> txtDynamicMobile, IList<string> txtDynamicPhone, IList<string> ddDynamicMobileType, string hidMultipleIMContact)
        {
            int empId = ObjEmployeeDetailsModel.EmployeeID;
            OperationDetails operationDetails = new OperationDetails();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ObjEmployeeDetailsModel.ModifiedBy = objUserContextViewModel.UserId;
            operationDetails = objEmployeeDB.UpdateEmployeeContactDetails(ObjEmployeeDetailsModel, AddressModel, txtDynamicEmails, txtDynamicPersonalEmail, ddDynamicEmailType, txtDynamicIM, ddDynamicIMType, txtDynamicMobile, txtDynamicPhone, ddDynamicMobileType);

            if (hidMultipleIMContact != null && hidMultipleIMContact != "")
            {
                hidMultipleIMContact = hidMultipleIMContact.TrimEnd('#');
                string[] IMContactArray = hidMultipleIMContact.Split('#');
                EmployeeIMContactDB objEmployeeIMContactDB = new EmployeeIMContactDB();
                foreach (var item in IMContactArray)
                {
                    string[] IMContactDetailArray = item.Split(',');
                    EmployeeIMContactModel objEmployeeIMContactModel = new EmployeeIMContactModel();
                    objEmployeeIMContactModel.IMContact = IMContactDetailArray[0];
                    objEmployeeIMContactModel.IMContactType = IMContactDetailArray[1];
                    if (empId != 0)
                    {
                        objEmployeeIMContactModel.EmployeeId = ObjEmployeeDetailsModel.EmployeeID;
                    }
                    else
                    {
                        objEmployeeIMContactModel.EmployeeId = operationDetails.InsertedRowId;
                    }
                    objEmployeeIMContactDB.InsertEmployeeIMContact(objEmployeeIMContactModel);
                }
            }
            return operationDetails;// Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }
        //Updating Employee Info
        public OperationDetails UpdateEmploymentInformation(EmploymentInformation ObjEmploymentInformation)
        {
            OperationDetails operationDetails = new OperationDetails();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            // Set userid and companyid
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            ObjEmploymentInformation.ModifiedBy = objUserContextViewModel.UserId;
            EmploymentModeModel objEmploymentModeModel = new EmploymentModeModel();
            DBHelper objDBHelper = new DBHelper();
            ObjEmploymentInformation.HireDate = string.Empty;
            operationDetails = objEmployeeDB.UpdateEmploymentInformation(ObjEmploymentInformation);

            ShiftDB objShift = new ShiftDB();
            var opResult = objShift.UpdateAtt_ShiftTableByUser(ObjEmploymentInformation.ShiftID, ObjEmploymentInformation.EmployeeID, 0, 1, "", "", "", "", "", "", false, "");
            return operationDetails;
        }
        //Updating Employee Personal Identity Information
        public OperationDetails UpdateEmployeePersonalIdentity(EmployeePersonalIdentityInfoModel ObjEmployeePersonalIdentityInfoModel)
        {
            OperationDetails operationDetails = new OperationDetails();
            int mode = 1;

            EmployeeDB objEmployeeDB = new EmployeeDB();
            if (ObjEmployeePersonalIdentityInfoModel.DocPassportId > 0)
                mode = 2;

            return objEmployeeDB.UpdateEmployeePeronalIdentityInformation(ObjEmployeePersonalIdentityInfoModel);
        }
        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public ActionResult IsEmployeeAlternativeIDExists(string employeeAlternativeID, int profileRequestInstanceID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                objEmployeeProfileCreationFormDB = new EmployeeProfileCreationFormDB();
                operationDetails = objEmployeeProfileCreationFormDB.IsEmployeeAlternativeIDExists(employeeAlternativeID, profileRequestInstanceID);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";

                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }
        public ActionResult IsEmployeeWorkEmailExists(string workEmail, int? profileRequestInstanceID,int? employeeID)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];              
                operationDetails =  new EmployeeDB().IsEmployeeWorkEmailExists(workEmail, profileRequestInstanceID, employeeID);
                if (operationDetails.InsertedRowId > 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "success";
                }
                else if (operationDetails.InsertedRowId == 0)
                {
                    operationDetails.Success = true;
                    operationDetails.CssClass = "error";

                }
                else
                {
                    operationDetails.Success = false;
                    operationDetails.CssClass = "error";

                }
            }
            catch (Exception ex)
            {
                operationDetails.CssClass = "Error";
                operationDetails.Success = false;
                operationDetails.Message = "Error :" + ex.Message;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);

        }
    }
}