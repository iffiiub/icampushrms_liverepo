﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using System.Data;

namespace HRMS.Web.Controllers
{
    public class UserRoleController : BaseController
    {
        char[] seperator = new char[] { ',' };

        public ActionResult Index_old()
        {
            return View();
        }

        public ActionResult GetUserRoleList()
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<UserRoleModel> objUserRoleModelList = new List<UserRoleModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            UserRoleDB objUserRoleDB = new UserRoleDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationList();
            objUserRoleModelList = objUserRoleDB.GetAllRoles();
            int i = 0;
            foreach (var item in objUserRoleModelList)
            {
                string[] permissionArray = item.Permissions.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                objUserRoleModelList[i].Permissions = "";
                foreach (var inneritem in permissionArray)
                {
                    objUserRoleModelList[i].Permissions += objMenuNavigationModelList.Where(x => x.MainNavigationSubCategoryId == Convert.ToInt32(inneritem)).Select(x => x.MainNavigationSubCategoryName).SingleOrDefault() + ",";
                }
                i = i + 1;
            }
            var vList = new object();
            vList = new
            {
                aaData = (from item in objUserRoleModelList
                          select new
                          {
                              Action = item.UserRoleId == 1 ? "" : "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditUserRole(" + item.UserRoleId.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteUserRole(" + item.UserRoleId.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              UserRoleId = item.UserRoleId,
                              UserRoleName = item.UserRoleName,
                              Permissions = item.Permissions.Trim(seperator)
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUserRole_old()
        {
            DBHelper objDBHelper = new DBHelper();
            List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationList();
            ViewBag.MenuNavigation = objMenuNavigationModelList;

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserRoleDB objUserRoleDB = new UserRoleDB();

            ViewBag.GetOtherPermission = objUserRoleDB.GetOtherPermission();
            ViewBag.PermissionString = "";

            return View();
        }

        [HttpPost]
        public ActionResult AddUserRole_old(UserRoleModel objUserRoleModel)
        {
            if (ModelState.IsValid)
            {
                objUserRoleModel.Permissions = objUserRoleModel.Permissions != null && objUserRoleModel.Permissions != "" ? objUserRoleModel.Permissions.Trim(seperator) : "";
                objUserRoleModel.ViewPagesSubCategoryIDs = objUserRoleModel.ViewPagesSubCategoryIDs != null && objUserRoleModel.ViewPagesSubCategoryIDs != "" ? objUserRoleModel.ViewPagesSubCategoryIDs.Trim(seperator) : "";
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserRoleDB objUserRoleDB = new UserRoleDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objUserRoleDB.InsertUserRole(objUserRoleModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                DBHelper objDBHelper = new DBHelper();
                List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
                objMenuNavigationModelList = objDBHelper.GetMenuNavigationList();
                ViewBag.MenuNavigation = objMenuNavigationModelList;
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserRoleDB objUserRoleDB = new UserRoleDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditUserRole_old(int id)
        {
            DBHelper objDBHelper = new DBHelper();
            List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationList();
            ViewBag.MenuNavigation = objMenuNavigationModelList;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserRoleDB objUserRoleDB = new UserRoleDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserRoleModel objUserRoleModel = new UserRoleModel();
            objUserRoleModel = objUserRoleDB.UserRoleById(id);

            List<int> permissionList = new List<int>();
            if (objUserRoleModel.Permissions != null && objUserRoleModel.Permissions != "")
            {
                permissionList = objUserRoleModel.Permissions.Split(seperator, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList();
            }
            ViewBag.UserRole = permissionList;
            ViewBag.GetOtherPermission = objUserRoleDB.GetOtherPermission();
            return View(objUserRoleModel);
        }

        [HttpPost]
        public JsonResult EditUserRole_old(UserRoleModel objUserRoleModel)
        {
            if (ModelState.IsValid)
            {
                objUserRoleModel.Permissions = objUserRoleModel.Permissions != null && objUserRoleModel.Permissions != "" ? objUserRoleModel.Permissions.Trim(seperator) : "";
                objUserRoleModel.ViewPagesSubCategoryIDs = objUserRoleModel.ViewPagesSubCategoryIDs != null && objUserRoleModel.ViewPagesSubCategoryIDs != "" ? objUserRoleModel.ViewPagesSubCategoryIDs.Trim(seperator) : "";
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserRoleDB objUserRoleModelDB = new UserRoleDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objUserRoleModelDB.UpdateUserRole(objUserRoleModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                DBHelper objDBHelper = new DBHelper();
                List<MenuNavigationModel> objMenuNavigationModelList = new List<MenuNavigationModel>();
                objMenuNavigationModelList = objDBHelper.GetMenuNavigationList();
                ViewBag.MenuNavigation = objMenuNavigationModelList;
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                UserRoleDB objUserRoleDB = new UserRoleDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();
                objUserRoleModel = objUserRoleDB.UserRoleById(objUserRoleModel.UserRoleId);
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewUserRole(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            UserRoleDB objUserRoleDB = new UserRoleDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserRoleDB objUserRoleModelDB = new UserRoleDB();
            UserRoleModel objUserRoleModel = new UserRoleModel();
            objUserRoleModel = objUserRoleModelDB.UserRoleById(id);
            return View(objUserRoleModel);
        }

        public ActionResult DeleteUserRole_old(int id)
        {
            UserRoleDB objUserRoleDB = new UserRoleDB();
            objUserRoleDB.DeleteUserRoleById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        //**********************New Functionality**********************//

        public ActionResult Index()
        {
            UserRoleDB objUserRoleDB = new UserRoleDB();
            List<UserRoleModel> lstUserRoleModel = objUserRoleDB.GetAllRoles();
            ViewBag.lstUserRoleModel = lstUserRoleModel;
            HRMS.DataAccess.DBHelper objDBHelper = new HRMS.DataAccess.DBHelper();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in objDBHelper.GetMainMenuNavigationList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.MainNavigationName, Value = m.MainNavigationId.ToString() });

            }
            ViewBag.ParentNavigationDDL = ObjSelectedList;
            return View();
        }

        [HttpPost]
        public JsonResult GetModuleTree(int? ParentNavId, bool? ShowOnlyPermitted, int? selectedRole)
        {
            HRMS.DataAccess.DBHelper objDBHelper = new HRMS.DataAccess.DBHelper();
            List<TreeNavigationModel> objMenuNavigationModelList = new List<TreeNavigationModel>();
            if (ShowOnlyPermitted == true)
            {
                objMenuNavigationModelList = objDBHelper.GetMenuNavigationTree(ParentNavId, selectedRole);
            }
            else
            {
                objMenuNavigationModelList = objDBHelper.GetMenuNavigationTree(ParentNavId, null);
            }
            List<TreeItemView> treeList = new List<TreeItemView>();
            objMenuNavigationModelList.ForEach(l => treeList.Add(new TreeItemView() { id = (int?)l.ModuleID, parentId = l.ParentModuleID, name = l.ModuleName, url = l.ModuleURL }));

            return Json(treeList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetModulePermissions(int moduleID, string moduleName, int roleId, bool showOnlyAssigned, int? parentId)
        {
            HRMS.DataAccess.DBHelper objDBHelper = new HRMS.DataAccess.DBHelper();
            UserRoleDB objUserRoleDB = new UserRoleDB();
            List<UserRolePermissions> lstUserRolePermissions = new List<UserRolePermissions>();
            lstUserRolePermissions = objUserRoleDB.GetPermissionDetailsByNavigation(moduleID, roleId, showOnlyAssigned);
            bool IsDashboard = false;
            if (lstUserRolePermissions.Where(x => x.ModuleName.ToLower().Trim() == "dashboard").ToList().Count() > 0)
            {
                IsDashboard = true;
            }
            ViewBag.IsDashboard = IsDashboard;
            ViewBag.UserRoleId = roleId;
            return PartialView("_ModulePermissions", lstUserRolePermissions);
        }

        public ActionResult UpdatePermissionTypes(List<CustomPermissionEdit> mappingDetails, string operationType, int ActNavID, int moduleId,
            int userRoleId, bool IsFoolControl)
        {
            OperationDetails operationDetails = new OperationDetails();
            DataTable multipleOtherPermissions = new DataTable();
            List<TreeNavigationModel> objMenuNavigationModelList = new List<TreeNavigationModel>();
            HRMS.DataAccess.DBHelper objDBHelper = new HRMS.DataAccess.DBHelper();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationTree(null, null);
            TreeNavigationModel objTreeNavigationModel = objMenuNavigationModelList.Where(m => m.ModuleID == moduleId).FirstOrDefault();
            int? parentNavID = null;
            if (objTreeNavigationModel.IsThirdNavigation)
            {
                parentNavID = objMenuNavigationModelList.Where(m => m.ModuleID == objTreeNavigationModel.ParentModuleID).FirstOrDefault().ActNavigationID;
            }
            multipleOtherPermissions.Columns.Add("OtherPermissionID", typeof(int));
            multipleOtherPermissions.Columns.Add("IsAdd", typeof(bool));
            foreach (var item in mappingDetails)
            {
                if (item.PermissionType == "Other")
                {
                    multipleOtherPermissions.Rows.Add(
                        item.PermissionId,
                        item.GrantPermission
                        );
                }
            }
            bool? isView = null;
            bool? isAdd = null;
            bool? isUpdate = null;
            bool? isDelete = null;

            if (operationType == "selectAll" && mappingDetails.Where(m => m.PermissionType == "selectAll").FirstOrDefault().GrantPermission == true)
            {
                isView = true;
                isAdd = true;
                isDelete = true;
                isUpdate = true;
            }
            else if (operationType == "View" && mappingDetails.Where(m => m.PermissionType == "View").FirstOrDefault().GrantPermission == false)
            {
                isAdd = false;
                isDelete = false;
                isUpdate = false;
                isView = false;
            }
            else
            {
                foreach (var item in mappingDetails)
                {
                    if (item.PermissionType == "View")
                    {
                        isView = item.GrantPermission;
                        if (item.GrantPermission == true)
                        {
                            IsFoolControl = true;
                        }
                    }
                    else if (item.PermissionType == "Add")
                    {
                        isAdd = item.GrantPermission;
                        if (item.GrantPermission == true)
                        {
                            isView = true;
                            IsFoolControl = true;
                        }
                    }
                    else if (item.PermissionType == "Update")
                    {
                        isUpdate = item.GrantPermission;
                        if (item.GrantPermission == true)
                        {
                            isView = true;
                            IsFoolControl = true;
                        }
                    }
                    else if (item.PermissionType == "Delete")
                    {
                        isDelete = item.GrantPermission;
                        if (item.GrantPermission == true)
                        {
                            isView = true;
                            IsFoolControl = true;
                        }
                    }
                }
            }


            UserRoleDB objUserRoleDB = new UserRoleDB();
            operationDetails = objUserRoleDB.UpdateUserRolePermission(multipleOtherPermissions, userRoleId, ActNavID, IsFoolControl,
                isView, isAdd, isUpdate, isDelete, parentNavID);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUserRole()
        {
            ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
            UserRoleModel objUserRoleModel = new UserRoleModel();
            return View(objUserRoleModel);
        }
        [HttpPost]
        public ActionResult AddUserRole(UserRoleModel objUserRoleModel)
        {
            OperationDetails op = new OperationDetails();
            UserRoleDB objUserRoleDB = new UserRoleDB();
            op = objUserRoleDB.AddUserRole(objUserRoleModel);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowUserRoles()
        {
            UserRoleDB objUserRoleDB = new UserRoleDB();
            List<UserRoleModel> lstUserRoleModel = objUserRoleDB.GetAllRoles();
            return View("_RolePermissionIndex", lstUserRoleModel);
        }

        public ActionResult DeleteUserRole(int id)
        {
            UserRoleDB objUserRoleDB = new UserRoleDB();
            OperationDetails op = new OperationDetails();
            op = objUserRoleDB.DeleteUserRoleById(id);
            if (op.Success)
            {
                op.CssClass = "success";
            }
            else
            {
                op.CssClass = "error";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }


    }
}