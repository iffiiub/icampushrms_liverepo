﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using System.IO.Compression;
using HRMS.Web.FileUpload;

namespace HRMS.Web.Controllers
{
    public class DocumentController : BaseController
    {
        public ActionResult Index(int Id = 0)
        {
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<DocumentTypeModel> documentTypeList = new DocumentDB().GetDocumentTypeList();
            ViewBag.documentType = documentTypeList;
            List<DocumentTypeModel> documentTypeModelList = GetDocumentTypeListAfterAppliedPermission();
            ViewBag.canDownloadDocs = documentTypeModelList.Count() > 0 ? true : false;
            if (Id != 0)
            {
                Session["EmployeeListID"] = Id.ToString();
                int EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                ViewBag.EmployeeIDNo = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();
            }
            else
            {
                ViewBag.EmployeeIDNo = "";
                if (Session["EmployeeListID"] != null)
                {
                    int EmployID = Convert.ToInt32(Session["EmployeeListID"]);
                    objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);
                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();


                }
            }
            return View(objEmployee);
        }

        public ActionResult CheckEmployee()
        {
            string Id = Convert.ToString(Session["EmployeeListID"]);
            if (Id != "")
            {
                return Json(new { result = "success", resultMessage = "" });
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Please select an employee." });
            }

        }

        public ActionResult DocumentExpiry(int docTypeId = 0, string mode = "")
        {
            DocumentDB documentdb = new DocumentDB();
            List<SelectListItem> documentTypeList = new SelectList(documentdb.GetDocumentTypeList().Where(x => x.IsActive == true).ToList(), "docTypeId", "docTypeName", docTypeId.ToString()).ToList();
            documentTypeList.Insert(0, new SelectListItem { Text = "All Document Types", Value = "0" });
            ViewBag.documentType = documentTypeList;
            //ViewBag.documentSetting = documentdb.GetDocumentsettingDays();
            ViewBag.filterMode = mode;
            return View();
        }

        public ActionResult GetDocumentList(int docTyprid, string mode, int days, bool isPrimary)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            DocumentDB documentdb = new DocumentDB();
            List<DocumentsModel> documentModelList;
            documentModelList = documentdb.GetDocumentList(docTyprid, mode, days, isPrimary, UserId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in documentModelList
                          select new
                          {
                              employeeId = item.EmployeeId,
                              employeename = item.EmployeeName,
                              doctypeId = item.DocTypeId,
                              doctype = item.DocType,
                              documentNo = (item.FilePath != "" ? "<a href=\"" + item.FilePath + "\" target=\"_blank\">" + item.DocNo + "</a>" : "<a href=\"#\" style=\"text-decoration:none;\" >" + item.DocNo + "</a>"),
                              issueDate = item.IssueDate,
                              expiryDate = item.ExpiryDate,
                              issueCountry = item.IssueCountry,
                              issuePlace = item.IssuePlace,
                          }).ToArray(),

            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckPrimary(int DocTypeId, int DocId)
        {
            string empid = "";
            if (Session["EmployeeListID"] == null)
            {
                return null;
            }
            else
            {
                empid = Session["EmployeeListID"].ToString();
            }
            DocumentDB objDocumentDB = new DocumentDB();
            bool isPrimary = objDocumentDB.isPrimaryDocument(Convert.ToInt32(empid), DocTypeId, DocId);

            if (isPrimary)
            {
                return Json(new { result = "success", resultMessage = "Primary document is already exists, If you save record then other document will remove from primary." });
            }
            else
            {
                return Json(new { result = "error", resultMessage = "Please select an employee." });
            }

        }

        public ActionResult DownloadDocument()
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            int EmployID = Convert.ToInt32(Session["EmployeeListID"]);
            List<SelectListItem> emplist;
            emplist = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(m => m.IsActive == 1), "EmployeeId", "FullName").ToList();
            ViewBag.emplist = emplist;
            List<DocumentTypeModel> documentTypeModelList = GetDocumentTypeListAfterAppliedPermission();
            List<SelectListItem> documentTypeList = new SelectList(documentTypeModelList, "docTypeId", "docTypeName").ToList();
            ViewBag.documentType = documentTypeList;
            ViewBag.EmpIDD = EmployID;
            return View();
        }
        public List<DocumentTypeModel> GetDocumentTypeListAfterAppliedPermission()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DocumentDB documentdb = new DocumentDB();
            List<DocumentTypeModel> documentTypeModelList = documentdb.GetDocumentTypeListIsActive();

            //------------------------Checking permission for all docs

            List<int> avilablePermisssionForUser = null;
            UserRoleDB userRoleDB = new UserRoleDB();
            if (!string.IsNullOrEmpty(objUserContextViewModel.OtherPermissionIDs))
                avilablePermisssionForUser = objUserContextViewModel.OtherPermissionIDs.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            else
                avilablePermisssionForUser = new List<int>();

            var otherPermissions = userRoleDB.GetOtherPermission();
            otherPermissions = otherPermissions.Join(avilablePermisssionForUser, x => x.OtherPermissionId, m => m, (x, m) => new { x, m }).Select(n => n.x).ToList();

            List<int> docIds = new List<int>();
            if (otherPermissions.Where(x => x.OtherPermissionId == 34).Count() > 0)
                docIds.Add(1);
            if (otherPermissions.Where(x => x.OtherPermissionId == 30).Count() > 0)
                docIds.Add(2);
            if (otherPermissions.Where(x => x.OtherPermissionId == 31).Count() > 0)
                docIds.Add(3);
            if (otherPermissions.Where(x => x.OtherPermissionId == 29).Count() > 0)
                docIds.Add(4);
            if (otherPermissions.Where(x => x.OtherPermissionId == 33).Count() > 0)
                docIds.Add(5);
            if (otherPermissions.Where(x => x.OtherPermissionId == 32).Count() > 0)
                docIds.Add(6);
            if (otherPermissions.Where(x => x.OtherPermissionId == 28).Count() > 0)
                docIds.Add(7);
            if (otherPermissions.Where(x => x.OtherPermissionId == 38).Count() > 0)
                docIds.Add(8);
            if (otherPermissions.Where(x => x.OtherPermissionId == 37).Count() > 0)
                docIds.Add(9);
            if (otherPermissions.Where(x => x.OtherPermissionId == 35).Count() > 0)
                docIds.Add(10);
            if (otherPermissions.Where(x => x.OtherPermissionId == 36).Count() > 0)
                docIds.Add(11);
            if (otherPermissions.Where(x => x.OtherPermissionId == 39).Count() > 0)
                docIds.Add(12);

            documentTypeModelList = documentTypeModelList.Where(x => docIds.Contains(x.docTypeId)).ToList();
            return documentTypeModelList;
        }
        public JsonResult CheckForDownloadDocument(string EmpID, string PayDocumentID, bool isPrimary)
        {
            DocumentDB documentdb = new DocumentDB();
            List<DocumentsModel> documentsModelList = new List<DocumentsModel>();
            documentsModelList = documentdb.GetAllDocumentsForDownload(EmpID, PayDocumentID, isPrimary);
            int count = 0;

            foreach (var documentsModel in documentsModelList)
            {
                if (documentsModel.ActualDocumentFile != null)
                {
                    count++;
                }
            }
            if (count > 0)
                return Json(new { IsCheck = true, count = count }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { IsCheck = false, count = count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PostReportPartial(string EmpID, string PayDocumentID, bool isPrimary, int count)
        {
            DocumentDB documentdb = new DocumentDB();
            List<string> listfileName = new List<string>();
            string FileName = "";
            List<DocumentsModel> documentsModelList = new List<DocumentsModel>();
            documentsModelList = documentdb.GetAllDocumentsForDownload(EmpID, PayDocumentID, isPrimary);
            if (count == 1)
            {
                string[] separators = { "\\" };               
                string fileName = "";
                foreach (var documentsModel in documentsModelList)
                {
                    if (documentsModel.ActualDocumentFile != null)
                    {                       
                        TempData["FileByte"] = documentsModel.ActualDocumentFile;
                        TempData["FileName"] = documentsModel.DocumentFileName;
                        TempData["FileContentType"] = documentsModel.FileContentType;
                        FileName = fileName;
                    }
                }

            }
            else
            {
                CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/Documents/");
                CommonHelper.CommonHelper.DeleteFile("/Downloads/Documents/");
                CommonHelper.CommonHelper.CheckAndCreatDirectory("/Uploads/All Documents/");
                CommonHelper.CommonHelper.DeleteFile("/Uploads/All Documents/");

                string sourceFilePath = System.Web.HttpContext.Current.Server.MapPath("/Uploads/All Documents/");
                string destinationFilePath = "~/Downloads/Documents/";
                string nameOfzip = "";
                string destinationOfzip = "";
                foreach (var documentsModel in documentsModelList)
                {
                    if (documentsModel.ActualDocumentFile != null)
                    {
                        using (FileStream fs = System.IO.File.Create(sourceFilePath + documentsModel.DocumentFileName))
                        {
                            Byte[] info = documentsModel.ActualDocumentFile;                            
                            fs.Write(info, 0, info.Length);
                            listfileName.Add(sourceFilePath + documentsModel.DocumentFileName);
                        }
                    }
                }
                GenrateZip(destinationFilePath, listfileName, out destinationOfzip, out nameOfzip);
                TempData["Destination"] = destinationOfzip;
                TempData["NameOfZip"] = nameOfzip;
                FileName = nameOfzip;
            }

            return Json(new { FileName = FileName });

        }

        public ActionResult DownloadFiles(int count)
        {
            if (count == 1)
            {
                string[] separators = { "\\" };
                byte[] fileBytes = null;
                string fileName = "";
                string FileContentType = "application/octet-stream";
                if (TempData["FileContentType"]!=null)
                {
                    FileContentType = TempData["FileContentType"].ToString();
                }                
                fileBytes = TempData["FileByte"] as byte[];
                fileName = TempData["FileName"].ToString();
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            else
            {
                CommonHelper.CommonHelper.DeleteFile("/Uploads/All Documents/");
                string nameOfzip = "";
                string destinationOfzip = "";
                destinationOfzip = TempData["Destination"].ToString();
                nameOfzip = TempData["NameOfZip"].ToString();
                return File(destinationOfzip, "application/zip", nameOfzip);
            }
        }

        public void GenrateZip(string destinationFolderName, List<string> fileList, out string destinationOfzip, out string nameOfzip)
        {
            nameOfzip = "AllDocuments" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
            string sourcezip = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/Download.zip");
            destinationOfzip = System.Web.HttpContext.Current.Server.MapPath(destinationFolderName + nameOfzip);
            System.IO.File.Copy(sourcezip, destinationOfzip);
            CreateZip(destinationOfzip, fileList);
        }
        public void CreateZip(string destinationOfzip, List<string> listFile)
        {
            try
            {
                using (FileStream zipToOpen = new FileStream(destinationOfzip, FileMode.Open))
                {
                    string[] separators = { "\\" };
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                    {
                        foreach (var tepfileName in listFile)
                        {
                            archive.CreateEntryFromFile(tepfileName, tepfileName.Split(separators, StringSplitOptions.RemoveEmptyEntries).LastOrDefault());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public ActionResult DeleteDocumentPhysically(string filePath)
        {
            string sessionVariable = (string)Session["DocumentSessionVariable"];
            Session.Remove(sessionVariable);
            return Json(new { resultMessage = true }, JsonRequestBehavior.AllowGet);
        }

        public string UploadFile(HttpPostedFileBase file, string sessionVariable)
        {
            byte[] uploadDoc;
            string fileName = "";
            EmployeeDocumentModel employeeDocumentModel = new EmployeeDocumentModel();
            if (file != null && file.ContentLength > 0)
            {
                fileName = Path.GetFileName(file.FileName);
                using (var binaryReader = new BinaryReader(file.InputStream))
                    uploadDoc = binaryReader.ReadBytes(file.ContentLength);
                employeeDocumentModel.ImageName = fileName;
                employeeDocumentModel.FileContentType = file.ContentType;
                employeeDocumentModel.DocumentFile = uploadDoc;
                Session[sessionVariable] = employeeDocumentModel;
                Session["DocumentSessionVariable"] = sessionVariable;
                return fileName;
            }

            return null;
        }
        public ActionResult GetEmployeeDocument(int DocId)
        {
            DocumentDB documentDB = new DocumentDB();
            EmployeeDocumentModel employeeDocumentModel = documentDB.GetEmployeeDocumentById(DocId);
            if (employeeDocumentModel.DocumentFile != null)
                return File(employeeDocumentModel.DocumentFile, employeeDocumentModel.FileContentType, employeeDocumentModel.ImageName);
            else
            {
                return File(CommonHelper.CommonHelper.GetBlankImage(), "image/png");
            }
        }
    }
}