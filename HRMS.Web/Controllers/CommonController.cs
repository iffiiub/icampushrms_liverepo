﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Data;
using System.Text.RegularExpressions;
using OfficeOpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using DocumentFormat.OpenXml;
using HRMS.Entities.ViewModel;
using HRMS.Entities.Forms;
using HRMS.DataAccess.FormsDB;
using System.IO.Packaging;

namespace HRMS.Web.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        CityDB citydb;
        GeneralAccountsDB generalAccountDb;
        PaySalaryDB paysalryDb;
        DeductionDB deductionDB;
       
        public ActionResult FilterCityDropDown(int CountryID)
        {
            citydb = new CityDB();
            SelectList cityList = new SelectList(citydb.GetCityByCountryId(CountryID.ToString()), "CityId", "CityName");
            return Json(cityList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PayCategoryDropDown()
        {
            generalAccountDb = new GeneralAccountsDB();
            SelectList category = new SelectList(generalAccountDb.GetPayCategories(), "id", "text");
            return Json(category, JsonRequestBehavior.AllowGet);
        }
        public ActionResult VacationCodeDropDown()
        {
            generalAccountDb = new GeneralAccountsDB();
            SelectList codeList = new SelectList(generalAccountDb.GetAccountCode(), "selectedValue", "text");
            return Json(codeList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLateAndEarlyMinutesForEmployee(int employeeId, string date, string inTime, string outTime)
        {
            ShiftDB shiftdb = new ShiftDB();
            int lateTimeMinute = 0;
            int earlyTimeMinute = 0;
            List<ShiftModel> shiftDetails = shiftdb.GetShiftDetailsByEmployee(employeeId);
            DataAccess.GeneralDB.CommonDB common = new DataAccess.GeneralDB.CommonDB();
            DateTime shiftDate = DateTime.ParseExact(date, CommonHelper.SettingsHelper.HRMSDateFormat, CultureInfo.InvariantCulture);
            if (shiftDetails.Count() > 0)
            {
                int shiftDay = common.weekDays.FirstOrDefault(x => x.Key == shiftDate.DayOfWeek.ToString()).Value;
                int graceIn = Convert.ToInt32(shiftDetails.FirstOrDefault(x => x.ShiftDay == shiftDay).ShiftGraceIn);
                int graceOut = Convert.ToInt32(shiftDetails.FirstOrDefault(x => x.ShiftDay == shiftDay).ShiftGraceOut);
                DateTime shiftStart = Convert.ToDateTime(shiftDetails.FirstOrDefault(x => x.ShiftDay == shiftDay).ShiftStart);
                DateTime shiftEnd = Convert.ToDateTime(shiftDetails.FirstOrDefault(x => x.ShiftDay == shiftDay).ShiftEnd).AddMinutes(-graceOut);
                shiftStart = (new DateTime(shiftDate.Year, shiftDate.Month, shiftDate.Day, shiftStart.Hour, shiftStart.Minute, shiftStart.Second));
                shiftEnd = (new DateTime(shiftDate.Year, shiftDate.Month, shiftDate.Day, shiftEnd.Hour, shiftEnd.Minute, shiftEnd.Second));
                if (inTime != "")
                {
                    DateTime inTimeDate = Convert.ToDateTime(inTime);
                    DateTime employeeInTime = (new DateTime(shiftDate.Year, shiftDate.Month, shiftDate.Day, inTimeDate.Hour, inTimeDate.Minute, inTimeDate.Second));
                    if (employeeInTime > shiftStart.AddMinutes(graceIn))
                    {
                        TimeSpan timeSpan = employeeInTime.Subtract(shiftStart);
                        if (timeSpan.Minutes > 0)
                        {
                            lateTimeMinute = (timeSpan.Hours * 60) + timeSpan.Minutes;
                        }
                    }
                }
                if (outTime != "")
                {
                    DateTime outTimeDate = Convert.ToDateTime(outTime);
                    DateTime employeeOutTime = (new DateTime(shiftDate.Year, shiftDate.Month, shiftDate.Day, outTimeDate.Hour, outTimeDate.Minute, outTimeDate.Second));
                    TimeSpan timeSpan = shiftEnd.Subtract(employeeOutTime);
                    if (timeSpan.Minutes > 0)
                    {
                        earlyTimeMinute = (timeSpan.Hours * 60) + timeSpan.Minutes;
                    }
                }
            }

            return Json(new { lateMinute = lateTimeMinute, earlyMinute = earlyTimeMinute }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDecimalPointSetting()
        {
            CommonHelper.CommonHelper commonHelper = new CommonHelper.CommonHelper();
            return Json(CommonHelper.CommonHelper.GetAmountFormat(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeDdl(bool? active)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int UserId = objUserContextViewModel.UserId;
            EmployeeDB objEmployeeDB = new EmployeeDB();
            return Json(new SelectList(objEmployeeDB.GetEmployeeByActive(active, UserId), "EmployeeId", "FirstName"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIncrementedSalaryEmployee(bool? isIncludeEmpJoinInLastMonth)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<Employee> lstEmployee = objEmployeeDB.GetIncrementedSalaryEmployee();
            if (isIncludeEmpJoinInLastMonth==true)
            {
                List<Employee> lstEmpJoinInLastMonth = objEmployeeDB.GetEmployeeJoinInLastMonth();
                lstEmployee = lstEmployee.Union(lstEmpJoinInLastMonth).GroupBy(x => x.EmployeeId).Select(y => y.First()).ToList();
            }
          
            return Json(new SelectList(lstEmployee, "EmployeeId", "FirstName"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeInJoinInLastMonth(bool? isIncludeIncSalEmp)
        {
            EmployeeDB objEmployeeDB = new EmployeeDB();
            List<Employee> lstEmployee = objEmployeeDB.GetEmployeeJoinInLastMonth();
            if (isIncludeIncSalEmp == true)
            {
                List<Employee> lstIncSalEmp = objEmployeeDB.GetIncrementedSalaryEmployee();
                lstEmployee = lstEmployee.Union(lstIncSalEmp).GroupBy(x => x.EmployeeId).Select(y => y.First()).ToList();
            }
            return Json(new SelectList(lstEmployee, "EmployeeId", "FirstName"), JsonRequestBehavior.AllowGet);
        }


        public ActionResult CheckSession()
        {
            if (Session["userContext"] == null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadEmployeeRespectToHiredate(string Date)
        {
            DateTime dDate = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            EmployeeDB employeeDb = new EmployeeDB();
            DataAccess.GeneralDB.CommonDB commonDb = new DataAccess.GeneralDB.CommonDB();
            var employeeList = commonDb.GetEmpoyeeHireDateIsLessThanDate(employeeDb.GetEmployeeInformationWithHireDate(), dDate).ToList();
            var vList = new object();
            vList = new
            {
                data = (from item in employeeList
                        select new
                        {
                            label = item.EmployeeName,
                            value = item.EmployeeId.ToString()
                        }
                       )
            };

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult LoadActiveEmployee( )
        {            
            EmployeeDB employeeDb = new EmployeeDB();
            DataAccess.GeneralDB.CommonDB commonDb = new DataAccess.GeneralDB.CommonDB();
            var employeeList = employeeDb.GetEmployeeInformationWithHireDate();
            var vList = new object();
            vList = new
            {
                data = (from item in employeeList
                        select new
                        {
                            label = item.EmployeeName,
                            value = item.EmployeeId.ToString()
                        }
                       )
            };

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult LoadEmployeeRespectToHiredateAndTerminationDate(string Date)
        {
            DateTime dDate = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            EmployeeDB employeeDb = new EmployeeDB();
            DataAccess.GeneralDB.CommonDB commonDb = new DataAccess.GeneralDB.CommonDB();
            var employeeList = employeeDb.GetEmployeeInformationWithHireAndTerminationDate(dDate);
            var vList = new object();
            vList = new
            {
                data = (from item in employeeList
                        select new
                        {
                            label = item.EmployeeName,
                            value = item.EmployeeId.ToString()
                        }
                       )
            };

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }
        [HttpPost]
        public ActionResult SetActiveEmployeeIDSesson(int EMployeeID,bool IsEmployeePageRequest)
        {         
            Session["EmployeeListID"] = EMployeeID;
            Session["IsEmployeePageRequest"] = IsEmployeePageRequest;
            return Json(new {success= true } , JsonRequestBehavior.AllowGet);
        }

        public string GetExcelByReadingExcelColumnAsDataTable(string sourceFile, string destinationFile, string fileName, DataTable table, List<string> columns, int templateId, int ExcelTableStartIndex, int ExcelColumnNameRowNo)
        {
            char[] Alphabets = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            DataTable dt = ReadExcelColumnAsDataTable(sourceFile, ExcelTableStartIndex, ExcelColumnNameRowNo);
            int Count = table.Rows.Count;
            // Create a copy of the template file and open the copy 
            System.IO.File.Copy(sourceFile, destinationFile, true);
            FileInfo xlFile = new FileInfo(destinationFile);
            int numberofDecimal = new PayrollDB().GetDigitAfterDecimal();
            using (var excelPackage = new ExcelPackage(xlFile))
            {
                var workbook = excelPackage.Workbook;
                ExcelWorksheet worksheet = workbook.Worksheets[1];

                int l = 0;
                foreach (DataColumn column in dt.Columns)
                {
                    for (int i = 0; i < ExcelTableStartIndex; i++)
                    {
                        string TopHeader = "";

                        try
                        {
                            TopHeader = table.Rows[i]["TopHeader"].ToString();
                        }
                        catch (Exception ex)
                        {

                        }
                        if (TopHeader == "1")
                        {
                            string val = table.Rows[i][l].ToString();
                            if (!string.IsNullOrEmpty(val))
                            {
                                using (ExcelRange Rng = worksheet.Cells[Alphabets[l].ToString() + (i + 1).ToString()])
                                {
                                    Rng.Value = val;
                                }
                            }
                        }
                    }
                    l++;
                }

                if (table.AsEnumerable()
                        .Where(r => r["TopHeader"].ToString() == "0").Count() > 0)
                {
                    table = table.AsEnumerable()
                        .Where(r => r["TopHeader"].ToString() == "0")
                        .CopyToDataTable();
                }
                int k = 0;
                foreach (DataColumn column in dt.Columns)
                {
                    string RangeVal = dt.Rows[0][k].ToString();
                    int RangeColumnCount = Convert.ToInt32(Regex.Replace(RangeVal, @"[^0-9]+", String.Empty)) + 1;
                    string RangeColumn = Regex.Replace(RangeVal, @"[^A-Z]+", String.Empty);

                    foreach (DataColumn exportColumn in table.Columns)
                    {
                        string ColumnName = Regex.Replace(column.ColumnName, @"[^0-9a-zA-Z]+", "_").Replace(" ", "_");
                        if (exportColumn.ColumnName.Replace("_Amt", "") == ColumnName)
                        {
                            if (exportColumn.ColumnName.Contains("_Amt"))
                            {
                                int j = RangeColumnCount;
                                foreach (DataRow dr in table.Rows)
                                {
                                    using (ExcelRange Rng = worksheet.Cells[RangeColumn + j.ToString()])
                                    {
                                        if (numberofDecimal == 0)
                                        {
                                            Rng.Style.Numberformat.Format = "0";
                                        }
                                        else if (numberofDecimal == 1)
                                        {
                                            Rng.Style.Numberformat.Format = "0.0";
                                        }
                                        else if (numberofDecimal == 2)
                                        {
                                            Rng.Style.Numberformat.Format = "0.00";
                                        }
                                        else if (numberofDecimal == 3)
                                        {
                                            Rng.Style.Numberformat.Format = "0.000";
                                        }
                                        if (dr[exportColumn.ColumnName].ToString() != "")
                                        {
                                            Rng.Value = Convert.ToDouble(dr[exportColumn.ColumnName]);
                                        }
                                    }
                                    j++;
                                }

                            }
                            else
                            {
                                List<string> lstData = new List<string>();
                                foreach (DataRow dr in table.Rows)
                                {
                                    lstData.Add(dr[exportColumn.ColumnName].ToString());
                                }
                                using (ExcelRange Rng = worksheet.Cells[RangeColumn + RangeColumnCount.ToString() + ":" + RangeColumn + (RangeColumnCount + Count).ToString()])
                                {
                                    Rng.LoadFromCollection(lstData);
                                }
                            }
                        }
                    }
                    k++;
                }

                excelPackage.Save();
            }
            return destinationFile;
        }

        public string GetExcelForExternalTemplate(string sourceFile, string destinationFile, string fileName, DataTable table, List<string> columns, int templateId, int ExcelTableStartIndex, int ExcelColumnNameRowNo)
        {
            bool headerAvaibility = false;
            int numberofDecimal = new PayrollDB().GetDigitAfterDecimal();
            SetColumnsOrder(table, new string[] { "EmpId", "Tran_Type", "Beneficiary_Name", "Beneficiary_A_c_IBAN", "Amount_in_AED_Amt", "Payment_Details" });
            columns = new List<string>();
            foreach (System.Data.DataColumn column in table.Columns)
            {
                if (column.ColumnName.ToLower() == "topheader")
                    headerAvaibility = true;
                else
                    columns.Add(column.ColumnName);
            }
            System.IO.File.Copy(sourceFile, destinationFile, true);
            Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);
            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = ws.GetFirstChild<SheetData>();
                if (sheet == null)
                    throw new Exception("No sheed found in the template file. Please add the sheet");

                // Replace shared strings
                SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                var rows = worksheetPart.Worksheet.Descendants<Row>();
                DataTable dataTable = new DataTable();
                foreach (Cell cell in rows.ElementAt(ExcelColumnNameRowNo - 1))
                {
                    if (cell.InnerXml != "")
                    {
                        dataTable.Columns.Add(GetCellValue(document, cell));
                    }
                }

                //--------------------Insert record at ExcelTableStartIndex                                                       
                //var lastRow = sheetData.Elements<Row>().LastOrDefault().RowIndex;  

                int rowIndex = 3;
                if (ExcelTableStartIndex > 0)
                {
                    ExcelTableStartIndex = ExcelTableStartIndex - rowIndex;
                    for (int i = 0; i < ExcelTableStartIndex; i++)
                    {
                        Row row3 = new Row();
                        Cell cell = new Cell();
                        string cellValue = "";
                        cell.StyleIndex = 1;
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue() { Text = cellValue };
                        row3.AppendChild<Cell>(cell);
                        sheetData.InsertAt(row3, rowIndex);
                        rowIndex++;
                    }                  
                }
                else
                {                 
                    rowIndex = 1;
                }
                //---------------------------      
                                                                                                                 
                if (headerAvaibility)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        Row headerRow = new Row();
                        if (dr["TopHeader"].ToString() == "1")
                        {
                            foreach (string col in columns)
                            {
                                if (dr[col].ToString().Trim().Length > 2)
                                {
                                    Cell cell = new Cell();
                                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                    cell.CellValue = new CellValue() { Text = dr[col].ToString() };
                                    cell.StyleIndex = 1;
                                    headerRow.AppendChild<Cell>(cell);
                                }
                            }
                            headerRow.RowIndex = (uint)rowIndex;
                            rowIndex++;
                            sheetData.AppendChild(headerRow);
                        }
                    }
                }            

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    bool insertRow = false;
                    if (headerAvaibility)
                    {
                        if (dsrow["TopHeader"].ToString() == "0" || string.IsNullOrEmpty(dsrow["TopHeader"].ToString()))
                        {
                            insertRow = true;
                        }
                        else
                            insertRow = false;
                    }
                    else
                    {
                        insertRow = true;
                    }
                    if (insertRow)
                    {
                        Row row2 = new Row();
                        foreach (string col in columns)
                        {                         
                            var dataTypeOfCell = dsrow[col].GetType();
                            Cell cell = new Cell();
                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                            if (dataTypeOfCell.FullName.ToString().ToLower() == "system.decimal")
                            {
                                if (numberofDecimal == 0)
                                {
                                    cell.StyleIndex = 2;                                    
                                }
                                else if (numberofDecimal == 1)
                                {
                                    cell.StyleIndex = 3;
                                }
                                else if (numberofDecimal == 2)
                                {
                                    cell.StyleIndex = 4;
                                }
                                else if (numberofDecimal == 3)
                                {
                                    cell.StyleIndex = 5;
                                }
                                else if (numberofDecimal == 4)
                                {
                                    cell.StyleIndex = 6;
                                }
                                else if (numberofDecimal == 5)
                                {
                                    cell.StyleIndex = 7;
                                }
                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                cell.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[col]))));
                            }
                            else
                            {
                                cell.StyleIndex = 1;
                                cell.DataType = CellValues.String;
                                if(col == "EmpId")
                                    cell.CellValue = new CellValue("");
                                else
                                    cell.CellValue = new CellValue(dsrow[col].ToString());
                            }
                            row2.AppendChild<Cell>(cell);
                        }
                        row2.RowIndex = (uint)rowIndex;
                        sheetData.InsertAt(row2, rowIndex);
                        rowIndex++;
                    }

                }

                workbookPart.Workbook.Save();
                document.Close();
            }
            return destinationFile;
        }

        private void SetColumnsOrder(DataTable table, params String[] columnNames)
        {
            int columnIndex = 0;
            foreach (var columnName in columnNames)
            {
                table.Columns[columnName].SetOrdinal(columnIndex);
                columnIndex++;
            }
        }

        public static DataTable ReadExcelColumnAsDataTable(string fileName, int headerIndex, int ExcelColumnNameRowNo)
        {
            char[] Alphabets = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            DataTable dataTable = new DataTable();
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                int rowsCount = rows.Count();
                foreach (Cell cell in rows.ElementAt(ExcelColumnNameRowNo - 1))
                {
                    if (cell.InnerXml != "")
                    {
                        dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                    }
                }
                int i = 0;
                DataRow dataRow = dataTable.NewRow();
                foreach (Cell cell in rows.ElementAt(ExcelColumnNameRowNo - 1))
                {
                    if (cell.InnerXml != "")
                    {
                        //dataRow[i] = cell.CellReference.Value;
                        dataRow[i] = (Alphabets[i].ToString() + headerIndex.ToString()).Replace(" ", "");
                        i++;
                    }
                }
                dataTable.Rows.Add(dataRow);

            }


            return dataTable;
        }
        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public void MergeExcelCells(string docName, DataTable mergeDt, string sheetName, int MergeHeadersExcelCells, int ExcelTableMergeCellsIndex)
        {
            if (mergeDt.Rows.Count > 0)
            {
                if (MergeHeadersExcelCells > 1)
                {
                    char[] Alphabets = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                    int rowCount = 1;
                    if (ExcelTableMergeCellsIndex != 0)
                    {
                        rowCount = ExcelTableMergeCellsIndex + 1;
                    }
                    List<string> colList = new List<string>();
                    foreach (System.Data.DataRow dsrow in mergeDt.Rows)
                    {
                        string mergeCols = Alphabets[0].ToString() + rowCount + ":" + Alphabets[MergeHeadersExcelCells - 1].ToString() + rowCount;
                        mergeCols = mergeCols.Replace(" ", "");
                        colList.Add(mergeCols);
                        rowCount++;
                    }
                    // Open the document for editing.
                    using (SpreadsheetDocument document = SpreadsheetDocument.Open(docName, true))
                    {
                        Worksheet worksheet = GetWorksheet(document, sheetName);
                        if (worksheet == null || string.IsNullOrEmpty(colList.Count.ToString()))
                        {
                            return;
                        }

                        MergeCells mergeCells = new MergeCells();
                        if (worksheet.Elements<MergeCells>().Count() > 0)
                        {
                            mergeCells = worksheet.Elements<MergeCells>().First();
                        }
                        else
                        {
                            // Insert a MergeCells object into the specified position.
                            mergeCells = new MergeCells();                           
                            worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetData>().First());
                        }
                        // Create the merged cell and append it to the MergeCells collection.                       
                        MergeCell mergeCell;
                        foreach (string item in colList)
                        {
                            mergeCell = new MergeCell() { Reference = new StringValue(item) };
                            mergeCells.Append(mergeCell);
                        }
                        worksheet.Save();
                        document.Close();
                    }
                }
            }
        }

        private static Worksheet GetWorksheet(SpreadsheetDocument document, string worksheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name.Value.ToLower() == worksheetName.ToLower());
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheets.First().Id);
            if (sheets.Count() == 0)
                return null;
            else
                return worksheetPart.Worksheet;
        }

        public ActionResult GetEmployeeDesignation(int employeeID)
        {
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(employeeID);
            return Json(objEmployee.employmentInformation.PositionName, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalaryRange(int companyID, int positionID)
        {
            List<SalaryRangesModel> salaryRangesList = new SalaryRangesDB().GetSalaryRanges(companyID, positionID);
            //  objSalary = objHiring.GetSalaryRange(CompId, PositionId);
            return Json(salaryRangesList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsMainNavigationPermission(string Url)
        {
            PermissionModel permissionModel = CommonHelper.CommonHelper.CheckPermissionForUser(Url);
            OperationDetails operationDetails = new OperationDetails();
            if (permissionModel.IsMainNavigationPermission)
            {
                operationDetails.Success = true;
            }
            else
            {
                operationDetails.Success = false;
                operationDetails.Message = "<strong>Sorry! </strong>You don't have permission to navigate to form. Please contact with your administrator.";
                operationDetails.CssClass = "error";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}