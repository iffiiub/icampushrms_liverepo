﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using HRMS.DataAccess.GeneralDB;
using System.Configuration;

namespace HRMS.Web.Controllers
{
    public class HR_PaySundriesController: BaseController

    {
        public ActionResult Index()
        {            
            PayBatchDB objPayBatchDB = new PayBatchDB();
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList().OrderByDescending(m => m.AcademicYearId).ToList(), "AcademicYearId", "Duration");
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");
            
            List<SelectListItem>  ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var m in objPayBatchDB.GetAllPayBatch(0,"All").OrderByDescending(m => m.PayBatchID).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });

            }
            ViewBag.PayBatch = ObjSelectedList;
           // ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch().OrderByDescending(m=>m.PayBatchID).ToList(), "PayBatchID", "PayBatchName");
            return View();
        }

        public ActionResult AddHR_PaySundries()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();            
            PayBatchDB objPayBatchDB = new PayBatchDB();
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();

            ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");
            ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName");
            ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FullName");
            return View();
        }

        [HttpPost]
        public ActionResult AddHR_PaySundries(HR_PaySundriesModel objHR_PaySundriesModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objHR_PaySundriesDB.InsertHR_PaySundries(objHR_PaySundriesModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();                
                PayBatchDB objPayBatchDB = new PayBatchDB();
                AcademicYearDB objAcademicYearDB = new AcademicYearDB();

                ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
                HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
                List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
                ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");
                

                ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName");
                ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FullName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditHR_PaySundries(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();            
            PayBatchDB objPayBatchDB = new PayBatchDB();
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();

            ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");
            

            ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName");
            ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FullName");
            HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
            HR_PaySundriesModel objHR_PaySundriesModel = new HR_PaySundriesModel();
            objHR_PaySundriesModel = objHR_PaySundriesModelDB.HR_PaySundriesById(id);
            return View(objHR_PaySundriesModel);
        }

        [HttpPost]
        public JsonResult EditHR_PaySundries(HR_PaySundriesModel objHR_PaySundriesModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objHR_PaySundriesModelDB.UpdateHR_PaySundries(objHR_PaySundriesModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();                
                PayBatchDB objPayBatchDB = new PayBatchDB();
                AcademicYearDB objAcademicYearDB = new AcademicYearDB();

                ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
                HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
                List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
                ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");                

                ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName");
                ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FullName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewHR_PaySundries(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();            
            PayBatchDB objPayBatchDB = new PayBatchDB();
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();

            ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

            ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName");
            ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FullName");
            HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
            HR_PaySundriesModel objHR_PaySundriesModel = new HR_PaySundriesModel();
            objHR_PaySundriesModel = objHR_PaySundriesModelDB.HR_PaySundriesById(id);
            return View(objHR_PaySundriesModel);
        }

        public ActionResult DeleteHR_PaySundries(int id)
        {
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesDB.DeleteHR_PaySundriesById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }


        public void ExportToPdfReport(int BatchId, int SundriesId)
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "Sundries" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "SundryName";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForReport(BatchId, SundriesId);
            var report = (from item in objHR_PaySundriesModelList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmpName = item.EmpName,
                              PayBatchName = item.PayBatchName,
                              SundryName = item.SundryName,
                              AccountNumber = item.AccountNumber,
                              BankName = item.BankName,
                              Amount = item.Amount

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }


        public ActionResult ExportToExcelSundries(int BatchId, int SundriesId, int AcademicYearId)
        {
            byte[] content;
            string fileName = "Sundries" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "EmpName";
            string sortOrder = "DESC";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForWPS(BatchId, SundriesId, AcademicYearId);
            var report = (from item in objHR_PaySundriesModelList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmpName = item.EmpName,
                              AccountNumber = item.AccountNumber,
                              Amount = item.Amount

                          }).ToList();
            content = ExportListToExcel(report);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public ActionResult GenerateJV(int BatchId, int AcademicYear)
        {
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            int currentAcYearID = objAcademicYearDB.GetAllAcademicYearList().Where(x => x.IsActive == true).Select(x => x.AcademicYearId).SingleOrDefault();
            if (AcademicYear != currentAcYearID)
            {
                OperationDetails objOperationDetails = new OperationDetails(false, "You can't generate JV for previous academic years!", null, 0);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                return Json(objHR_PaySundriesDB.GetHR_PaySundriesListForGenerateJV(BatchId, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
            }

            
        }

        public void PrintLetterToPdf(int BatchId, int SundriesId)
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);
            string fileName = "Sundries" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();
            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "SundryName";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForReport(BatchId, SundriesId);
            var report = (from item in objHR_PaySundriesModelList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmpName = item.EmpName,
                              PayBatchName = item.PayBatchName,
                              SundryName = item.SundryName,
                              AccountNumber = item.AccountNumber,
                              BankName = item.BankName,
                              Amount = item.Amount

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            StringReader sr2 = new StringReader("<p>Hi,</p><p>Here is the listing of sundries report retrived based on batch and sundries.</p>");
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr2);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public ActionResult SendtLetter(int BatchId, int SundriesId)
        {            
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();
            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "SundryName";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForReport(BatchId, SundriesId);
            var report = (from item in objHR_PaySundriesModelList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmpName = item.EmpName,
                              PayBatchName = item.PayBatchName,
                              SundryName = item.SundryName,
                              AccountNumber = item.AccountNumber,
                              BankName = item.BankName,
                              Amount = item.Amount

                          }).ToList();
            
            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            string mailUserName = ConfigurationManager.AppSettings["FROM_ADDR"];
            StringReader sr2 = new StringReader("<p>Hi,</p><p>Here is the listing of sundries report retrived based on batch and sundries.</p>");
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = new Web.CommonHelper.CommonHelper().SendMail(objUserContextViewModel.Email, "Sundries Report", sr.ToString() + sr2.ToString());
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }


    }
}