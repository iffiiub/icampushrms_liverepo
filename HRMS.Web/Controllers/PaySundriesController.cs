﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.DataAccess.GeneralDB;
using System.Configuration;
using System.IO.Packaging;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Web.Script.Serialization;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using Newtonsoft.Json;
using DocumentFormat.OpenXml;
using HRMS.Entities.General;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace HRMS.Web.Controllers
{
    public class PaySundriesController : BaseController
    {
        public ActionResult Index()
        {
            //Sundry
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

            //BankList ddl
            BankInformationDB bankInformationDB = new BankInformationDB();
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            ViewBag.BankList = new SelectList(bankInformationDB.GetBanksList(), "BankID", "BankName_1"); ;

            //Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYearList();
            int currentAcademicYear = listAcYear.Where(x => x.IsActive).FirstOrDefault().AcademicYearId;
            ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYear(), "AcademicYearId", "Duration", currentAcademicYear.ToString());

            //Pay Batch
            PayBatchDB objPayBatchDB = new PayBatchDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Pay Batch", Value = "0" });
            foreach (var m in objPayBatchDB.GetAllPayBatch(0, "All").Where(x => x.active == true).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });
            }
            ViewBag.PayBatch = ObjSelectedList;

            return View();
        }

        public ActionResult GetHR_PaySundriesList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0, int BatchId = 0, int SundriesId = 0, int AcademicYearId = 0, string BankListIds = "")
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;
            sortOrder = "DESC";
            switch (iSortCol_0)
            {
                case 0:
                    sortColumn = "PaySundriesDate";
                    break;
                case 1:
                    sortColumn = "EmployeeName";
                    break;
                case 2:
                    sortColumn = "Sundry";
                    break;
                case 3:
                    sortColumn = "Batch";
                    break;
                case 4:
                    sortColumn = "AcademicYear";
                    break;
                case 5:
                    sortColumn = "Amount";
                    break;
                default:
                    sortColumn = "PaySundriesDate";
                    break;
            }

            List<PayBatchModel> objPayBatchModel = new List<PayBatchModel>();
            PayBatchDB objPayBatchDB = new PayBatchDB();
            objPayBatchModel = objPayBatchDB.GetAllPayBatch(0, "All");
            List<HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HR_PaySundriesModel>();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesList_Sort(pageNumber, numberOfRecords, sortColumn, sortOrder, BatchId, SundriesId, AcademicYearId, BankListIds, out totalCount);
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objHR_PaySundriesModelList
                          select new
                          {
                              Actions = objPayBatchModel.Where(m => m.PayBatchID == item.PayBatchID && m.active == true).ToList().Count() > 0 ? "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.PaySundriesID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>"
                                        + "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteHR_PaySundries(" + item.PaySundriesID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>" :
                                         "<a disabled class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.PaySundriesID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a>"
                                        + "<a disabled class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteHR_PaySundries(" + item.PaySundriesID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              EmployeeName = item.EmpName,
                              PaySundriesDate = item.PaySundriesDate,
                              Sundry = item.SundryName,
                              Batch = item.PayBatchName,
                              AcademicYear = item.AcademicYear,
                              Amount = Math.Round(item.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero).ToString(AmountFormat)
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            HR_PaySundriesModel objPaySundriesModel = new HR_PaySundriesModel();
            objPaySundriesModel = objHRSundryDB.HR_PaySundriesById(id);
            objPaySundriesModel.Amount = Math.Round(objPaySundriesModel.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
            //Sundry           
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

            //Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYear();
            var AcademicYear = objAcademicYearDB.GetAllAcademicYear();
            ViewBag.AcademicYear = new SelectList(AcademicYear, "AcademicYearId", "Duration", AcademicYear.Where(x => x.IsActive).FirstOrDefault().AcademicYearId.ToString());

            //Pay Batch
            PayBatchDB objPayBatchDB = new PayBatchDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Pay Batch", Value = "0" });
            List<PayBatchModel> objPayBatch = new List<PayBatchModel>();
            objPayBatch = objPayBatchDB.GetAllPayBatch(objPaySundriesModel.AcYear, "Seleted").Where(x => x.active == true).ToList();
            foreach (var m in objPayBatch)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });
            }
            ViewBag.PayBatch = ObjSelectedList;

            //Employee
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1 && x.CompanyId == objUserContextViewModel.CompanyId).ToList(), "EmployeeID", "FullName");

            return View(objPaySundriesModel);
        }
        [HttpPost]
        public JsonResult Edit(HR_PaySundriesModel objPaySundriesList)
        {
            List<HR_PaySundriesModel> objHR_PaySundriesList = new List<HR_PaySundriesModel>();
            objHR_PaySundriesList.Add(objPaySundriesList);
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
            PayrollDB payrollDb = new PayrollDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails.Success = true;

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 4).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                foreach (var objHR_PaySundriesModel in objHR_PaySundriesList)
                    if (objOperationDetails.Success == true)
                        objOperationDetails = objHR_PaySundriesModelDB.UpdateHR_PaySundries(objHR_PaySundriesModel);
                objOperationDetails.CssClass = "DirectEdit";
            }
            else
            {
                foreach (var objHR_PaySundriesModel in objHR_PaySundriesList)
                    if (objOperationDetails.Success == true)
                        objOperationDetails = objHR_PaySundriesModelDB.SavePayrollConfirmationRequestForSundry(objHR_PaySundriesModel, objUserContextViewModel.UserId, 2);
                objOperationDetails.CssClass = "Request";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddHR_PaySundries()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //Sundry
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

            //Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYear();
            var AcademicYear = objAcademicYearDB.GetAllAcademicYear();
            ViewBag.AcademicYear = new SelectList(AcademicYear, "AcademicYearId", "Duration", AcademicYear.Where(x => x.IsActive).FirstOrDefault().AcademicYearId.ToString());

            //Pay Batch
            PayBatchDB objPayBatchDB = new PayBatchDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            //ObjSelectedList.Add(new SelectListItem { Text = "Select Pay Batch", Value = "0" });
            foreach (var m in objPayBatchDB.GetAllPayBatch(0, "All").Where(x => x.active == true).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });
            }
            ViewBag.PayBatch = ObjSelectedList;

            //Employee
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1 && x.CompanyId == objUserContextViewModel.CompanyId).ToList(), "EmployeeID", "FullName");

            return View();
        }

        [HttpPost]
        public ActionResult AddHR_PaySundries(HR_PaySundriesModel objHR_PaySundriesModel, string hidPaySundriesList)
        {

            List<HR_PaySundriesModel> sundriesList = JsonConvert.DeserializeObject<List<HR_PaySundriesModel>>(hidPaySundriesList);
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                PayrollDB payrollDb = new PayrollDB();
                OperationDetails objOperationDetails = new OperationDetails();
                CommonHelper.CommonHelper.GetAmountFormat();
                objHR_PaySundriesModel.Amount = Math.Round(objHR_PaySundriesModel.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);

                PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
                PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
                objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 4).FirstOrDefault();
                objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
                bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
                bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
                bool IsDeductionSendForApproval = false;
                if (IsConfirmRunPayroll && IsSendForConfirmation)
                {
                    IsDeductionSendForApproval = true;
                }
                else
                {
                    IsDeductionSendForApproval = false;
                }

                if (sundriesList != null)
                {
                    if (sundriesList.Count() > 0)
                    {
                        if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                        {
                            foreach (var item in sundriesList)
                            {
                                objHR_PaySundriesModel.Amount = Math.Round(item.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
                                objHR_PaySundriesModel.EmployeeID = item.EmployeeID;
                                objHR_PaySundriesModel.PaySundriesDate = item.PaySundriesDate;
                                objOperationDetails = objHR_PaySundriesDB.InsertHR_PaySundries(objHR_PaySundriesModel);
                                objOperationDetails.CssClass = "DirectSave";
                            }
                        }
                        else
                        {
                            foreach (var item in sundriesList)
                            {
                                objHR_PaySundriesModel.Amount = Math.Round(item.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero);
                                objHR_PaySundriesModel.EmployeeID = item.EmployeeID;
                                objHR_PaySundriesModel.PaySundriesDate = item.PaySundriesDate;
                                objOperationDetails = objHR_PaySundriesDB.SavePayrollConfirmationRequestForSundry(objHR_PaySundriesModel, objUserContextViewModel.UserId, 1);
                                objOperationDetails.CssClass = "Request";
                            }
                        }
                    }
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
                EmployeeDB objEmployeeDB = new EmployeeDB();
                PayBatchDB objPayBatchDB = new PayBatchDB();
                PayrollDB payrollDb = new PayrollDB();
                AcademicYearDB objAcademicYearDB = new AcademicYearDB();

                ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
                HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
                List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
                ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

                ViewBag.PayBatch = new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName");
                ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeID", "FullName");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditHR_PaySundries(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //Sundry
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

            //Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYearList();
            ViewBag.AcademicYear = new SelectList(listAcYear, "AcademicYearId", "Duration");

            //Employee
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeID", "FullName");

            HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
            HR_PaySundriesModel objHR_PaySundriesModel = new HR_PaySundriesModel();
            objHR_PaySundriesModel = objHR_PaySundriesModelDB.HR_PaySundriesById(id);
            objHR_PaySundriesModel.Amount = Math.Round(objHR_PaySundriesModel.Amount, 2, MidpointRounding.AwayFromZero);

            //Pay Batch
            PayBatchDB objPayBatchDB = new PayBatchDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var m in objPayBatchDB.GetAllPayBatchByAcademicYear(objHR_PaySundriesModel.AcYear).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });
            }
            ViewBag.PayBatch = ObjSelectedList;

            return View(objHR_PaySundriesModel);
        }

        [HttpPost]
        public JsonResult EditHR_PaySundries(List<HR_PaySundriesModel> objHR_PaySundriesList)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
            PayrollDB payrollDb = new PayrollDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails.Success = true;

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 4).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                foreach (var objHR_PaySundriesModel in objHR_PaySundriesList)
                    if (objOperationDetails.Success == true)
                        objOperationDetails = objHR_PaySundriesModelDB.UpdateHR_PaySundries(objHR_PaySundriesModel);
                objOperationDetails.CssClass = "DirectEdit";
            }
            else
            {
                foreach (var objHR_PaySundriesModel in objHR_PaySundriesList)
                    if (objOperationDetails.Success == true)
                        objOperationDetails = objHR_PaySundriesModelDB.SavePayrollConfirmationRequestForSundry(objHR_PaySundriesModel, objUserContextViewModel.UserId, 2);
                objOperationDetails.CssClass = "Request";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewHR_PaySundries(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //Sundry
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            ViewBag.HRSundry = new SelectList(lst, "PaySundryID", "SundryName_1");

            //Academic Year
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            List<AcademicYearModel> listAcYear = objAcademicYearDB.GetAllAcademicYearList();
            ViewBag.AcademicYear = new SelectList(listAcYear, "AcademicYearId", "Duration");

            //Pay Batch
            PayBatchDB objPayBatchDB = new PayBatchDB();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var m in objPayBatchDB.GetAllPayBatchByAcademicYear(listAcYear.FirstOrDefault().AcademicYearId).ToList())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });
            }
            ViewBag.PayBatch = ObjSelectedList;

            //Employee
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeID", "FullName");

            HR_PaySundriesDB objHR_PaySundriesModelDB = new HR_PaySundriesDB();
            HR_PaySundriesModel objHR_PaySundriesModel = new HR_PaySundriesModel();
            objHR_PaySundriesModel = objHR_PaySundriesModelDB.HR_PaySundriesById(id);
            return View(objHR_PaySundriesModel);
        }

        public ActionResult DeleteHR_PaySundries(int id)
        {
            PayrollDB payrollDb = new PayrollDB();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesModel objHR_PaySundriesModel = objHR_PaySundriesDB.HR_PaySundriesById(id);

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 4).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                OperationDetails operationDetails = objHR_PaySundriesDB.DeleteHR_PaySundriesById(id);
                operationDetails.CssClass = "DirectDelete";
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OperationDetails operationDetails = objHR_PaySundriesDB.SavePayrollConfirmationRequestForSundry(objHR_PaySundriesModel, objUserContextViewModel.UserId, 3);
                if (operationDetails.Success)
                    operationDetails.Message = "Pay sundries modification request send for confirmation.";
                else
                    operationDetails.Message = "Technical error has occurred";
                operationDetails.CssClass = "Request";
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }

        }

        public void ExportToPdfReport(int BatchId, int SundriesId)
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "Sundries" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "SundryName";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForReport(BatchId, SundriesId);
            var report = (from item in objHR_PaySundriesModelList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmpName = item.EmpName,
                              PayBatchName = item.PayBatchName,
                              SundryName = item.SundryName,
                              AccountNumber = item.AccountNumber,
                              BankName = item.BankName,
                              Amount = item.Amount

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public ActionResult ExportToExcelSundries(int? BatchId, int? SundriesId, int? AcademicYearId, string BankListIds = "")
        {
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/SundriesBankListReports/");
            CommonHelper.CommonHelper.DeleteFile("/Downloads/SundriesBankListReports/");
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int numberofDecimal = new PayrollDB().GetDigitAfterDecimal();
            int companyid = objUserContextViewModel.CompanyId;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            int batchId1 = BatchId ?? 0;
            int SundriesId1 = SundriesId ?? 0;
            int AcademicYearId1 = AcademicYearId ?? 0;
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForWPS(batchId1, SundriesId1, AcademicYearId1, BankListIds);
            int counter = 1;
            var report = (from item in objHR_PaySundriesModelList.OrderBy(x => x.EmpName).AsEnumerable()
                          select new
                          {
                              EmployeeID = counter++,
                              EmpName = item.EmpName,
                              AccountNumber = item.AccountNumber,
                              Amount = Math.Round(item.Amount, 2, MidpointRounding.AwayFromZero)
                          }).ToList();

            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/SundriesTemplate/SampleSalaryFile-template.xlsx");

            string fileName = "Sundries" + DateTime.Now.ToString("ddMMyyyyhhmm");
            string destinationFile = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/SundriesBankListReports/" + fileName + ".xlsx");

            // Create a copy of the template file and open the copy 
            System.IO.File.Copy(sourceFile, destinationFile, true);

            Package spreadsheetPackage = Package.Open(destinationFile, FileMode.Open, FileAccess.ReadWrite);

            DataTable dtReport = new DataTable("ReportTable");
            dtReport.Columns.Add("EmployeeID", typeof(int));
            dtReport.Columns.Add("EmpName", typeof(string));
            dtReport.Columns.Add("AccountNumber", typeof(string));
            dtReport.Columns.Add("Amount", typeof(decimal));
            foreach (var item in report)
            {
                DataRow row = dtReport.NewRow();
                row["EmployeeID"] = item.EmployeeID;
                row["EmpName"] = item.EmpName;
                row["AccountNumber"] = item.AccountNumber;
                row["Amount"] = item.Amount;
                dtReport.Rows.Add(row);
            }

            //DataTable table = PaySundriesController.ToDataTable(report);

            using (var document = SpreadsheetDocument.Open(spreadsheetPackage))
            {
                var workbookPart = document.WorkbookPart;
                var workbook = workbookPart.Workbook;

                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                Worksheet ws = ((WorksheetPart)(workbookPart.GetPartById(sheet.Id))).Worksheet;
                SheetData sheetData = ws.GetFirstChild<SheetData>();

                if (sheet == null)
                    throw new Exception("No sheed found in the template file. Please add the sheet");

                // Replace shared strings
                SharedStringTablePart sharedStringsPart = workbook.WorkbookPart.SharedStringTablePart;
                IEnumerable<Text> sharedStringTextElements = sharedStringsPart.SharedStringTable.Descendants<Text>();

                foreach (var text in sharedStringTextElements)
                {
                    if (text.Text.Contains("companyname"))
                    {
                        SchoolInformation model = new SchoolInformationDB().GetSchoolInformation();
                        text.Text = text.Text.Replace("companyname", model.SchoolName_1);
                    }

                    if (text.Text.Contains("paydate"))
                        text.Text = text.Text.Replace("paydate", DateTime.Now.ToString("dd.MM.yyyy"));

                    if (text.Text.Contains("remarks"))
                        text.Text = text.Text.Replace("remarks", DateTime.Now.ToString("MMMM") + " " + DateTime.Now.ToString("yyyy"));
                }

                int rowIndex = 18;
                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                var rows = worksheetPart.Worksheet.Descendants<Row>();
                int styleIndexCounter = 1;
                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in dtReport.Columns)
                {
                    columns.Add(column.ColumnName);
                }

                Row row;
                int rowCount = 18;
                foreach (System.Data.DataRow dsrow in dtReport.Rows)
                {
                    //row = new Row();
                    //foreach (string col in columns)
                    //{
                    //    Cell cell = new Cell();
                    //    cell.DataType = CellValues.String;
                    //    cell.CellValue = new CellValue(dsrow[col].ToString());
                    //    row.AppendChild<Cell>(cell);
                    //}
                    //row.RowIndex = (uint)rowIndex;
                    //sheetData.InsertAt(row, rowIndex);
                    //rowIndex++;

                    Row rowData = sheetData.Elements<Row>().Where(x => x.RowIndex == rowCount).FirstOrDefault();
                    var cellData = rowData.Elements<Cell>();
                    int colCounter = 1;
                    foreach (var iCellData in cellData)
                    {
                        //iCellData.StyleIndex = 50;
                        switch (colCounter)
                        {
                            case 1:
                                iCellData.DataType = new EnumValue<CellValues>(CellValues.Number);
                                iCellData.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[0]))));
                                iCellData.StyleIndex = 0;
                                break;
                            case 2:
                                iCellData.DataType = CellValues.String;
                                iCellData.CellValue = new CellValue(dsrow[1].ToString());
                                iCellData.StyleIndex = 39;
                                break;
                            case 3:
                                iCellData.DataType = new EnumValue<CellValues>(CellValues.Number);
                                iCellData.CellValue = new CellValue(dsrow[2].ToString());
                                iCellData.StyleIndex = 60;
                                break;
                            case 4:
                                iCellData.DataType = new EnumValue<CellValues>(CellValues.Number);
                                iCellData.CellValue = new CellValue(DoubleValue.FromDouble(Convert.ToDouble((dsrow[3]))));
                                if (numberofDecimal == 0)
                                {
                                    iCellData.StyleIndex = 59;
                                }
                                else if (numberofDecimal == 1)
                                {
                                    iCellData.StyleIndex = 58;
                                }
                                else if (numberofDecimal == 2)
                                {
                                    iCellData.StyleIndex = 57;
                                }
                                else if (numberofDecimal == 3)
                                {
                                    iCellData.StyleIndex = 56;
                                }
                                break;
                        }
                        colCounter++;
                    }
                    rowCount++;
                    styleIndexCounter++;
                }
                document.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                document.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
                workbookPart.Workbook.Save();
                document.Close();
                //Insert a blank row
                //row = new Row();
                //row.RowIndex = (uint)rowIndex;
                //sheetData.InsertAt(row, rowIndex);
            }

            return File(destinationFile, "application/vnd.ms-excel", fileName + ".xlsx");
        }

        public ActionResult ExternalExcelExport(int? BatchId, int? SundriesId, int? AcademicYearId, string BankListIds = "")
        {
            //-----------
            CommonHelper.CommonHelper.DeleteFile("/Downloads/WPS/");
            CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/WPS/");
            object list;
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            DataTable table = objHR_PaySundriesDB.ExternalExcelExport(BatchId, SundriesId, AcademicYearId, BankListIds);
            string fileName = "";
            string templateName = "";

            //Get filename
            int templateId = 6;
            WPSTemplateSettingsModel WPSTemplateSettingsModel = new WPSDB().GetWPSTemplatesById(templateId);
            fileName = WPSTemplateSettingsModel.FileName;
            templateName = WPSTemplateSettingsModel.TemplateName + DateTime.Now.ToString("ddMMyyyyhhmm");

            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/WPS/" + fileName + ".xlsx");
            string destinationFile = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/WPS/" + templateName + ".xlsx");

            List<String> columns = new List<string>();
            foreach (System.Data.DataColumn column in table.Columns)
            {
                columns.Add(column.ColumnName);
            }

            //-----------       
            CommonController commonController = new CommonController();
            destinationFile = commonController.GetExcelForExternalTemplate(sourceFile, destinationFile, fileName, table, columns, templateId, WPSTemplateSettingsModel.ExcelTableStartIndex, WPSTemplateSettingsModel.ExcelColumnNameRowNo);

            return File(destinationFile, "application/vnd.ms-excel", templateName + ".xlsx");
        }

        public ActionResult NBDExternalExcelExport(int? BatchId, int? SundriesId, int? AcademicYearId, string BankListIds = "")
        {

            byte[] content;
            string fileName = "SundriesNBDBanklist" + DateTime.Now.ToString("ddMMyyyy") + ".xls";                      
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            DataTable table = objHR_PaySundriesDB.NBDExternalExcelExport(BatchId, SundriesId, AcademicYearId, BankListIds);
            DataSet ds = new DataSet();
            ds.Tables.Add(table);
            content = CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };           
        }
        public static byte[] CreateExcelsheet(DataSet ds)
        {
            using (MemoryStream mem = new MemoryStream())
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(mem, SpreadsheetDocumentType.Workbook))
                {
                    // create the workbook
                    spreadSheet.AddWorkbookPart();
                    spreadSheet.WorkbookPart.Workbook = new Workbook();     // create the worksheet
                    spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet = new Worksheet();

                    // create sheet data
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.AppendChild(new SheetData());

                    //// save worksheet
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.Save();

                    //// create the worksheet to workbook relation
                    spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());

                    foreach (System.Data.DataTable table in ds.Tables)
                    {

                        var sheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                        var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                        sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                        DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                        string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(sheetPart);

                        uint sheetId = 1;
                        if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                        {
                            sheetId = sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                        sheets.Append(sheet);
                       
                        ////Blank Row
                        //sheetData.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Row());

                        DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        /// Styling 
                        WorkbookStylesPart stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                        //stylesPart.Stylesheet = CreateStylesheet();
                        stylesPart.Stylesheet = HRMS.Web.CommonHelper.CommonHelper.GenerateStyleSheet();
                        stylesPart.Stylesheet.Save();

                        List<String> columns = new List<string>();
                        foreach (System.Data.DataColumn column in table.Columns)
                        {
                            columns.Add(column.ColumnName);
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                            cell.StyleIndex = 1;
                            headerRow.AppendChild(cell);
                        }
                        sheetData.AppendChild(headerRow);
                        foreach (System.Data.DataRow dsrow in table.Rows)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            foreach (String col in columns)
                            {
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                                newRow.AppendChild(cell);
                            }
                            sheetData.AppendChild(newRow);
                        }

                    }
                    spreadSheet.WorkbookPart.Workbook.Save();
                    spreadSheet.Close();
                    return mem.ToArray();
                }
            }
        }

        public ActionResult GenerateJV(int BatchId, int AcademicYear)
        {
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            int currentAcYearID = objAcademicYearDB.GetAllAcademicYearList().Where(x => x.IsActive == true).Select(x => x.AcademicYearId).SingleOrDefault();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            return Json(objHR_PaySundriesDB.GetHR_PaySundriesListForGenerateJV(BatchId, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }

        public void PrintLetterToPdf(int BatchId, int SundriesId)
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "Sundries" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "SundryName";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objHR_PaySundriesModelList = objHR_PaySundriesDB.GetHR_PaySundriesListForReport(BatchId, SundriesId);
            var report = (from item in objHR_PaySundriesModelList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              EmpName = item.EmpName,
                              PayBatchName = item.PayBatchName,
                              SundryName = item.SundryName,
                              AccountNumber = item.AccountNumber,
                              BankName = item.BankName,
                              Amount = item.Amount

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            StringReader sr2 = new StringReader("<p>Hi,</p><p>Here is the listing of sundries report retrived based on batch and sundries.</p>");
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr2);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        #region Pay Batch

        public ActionResult AddPayBatch()
        {
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYear(), "AcademicYearId", "Duration");
            return View();
        }

        [HttpPost]
        public ActionResult AddPayBatch(PayBatchModel model)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                PayBatchDB objPayBatchDB = new PayBatchDB();
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails = objPayBatchDB.InsertPayBatch(model);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                AcademicYearDB objAcademicYearDB = new AcademicYearDB();
                ViewBag.AcademicYear = new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration");
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetAllPayBatch(int academicYearId, string mode)
        {
            PayBatchDB objPayBatchDB = new PayBatchDB();
            List<PayBatchModel> lst;
            if (mode == "All")
                lst = objPayBatchDB.GetAllPayBatch(0, "All").Where(x => x.active).ToList();
            else
                lst = objPayBatchDB.GetAllPayBatch(academicYearId, "Selected").Where(x => x.active).ToList();

            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Pay Batch", Value = "" });

            foreach (var m in lst)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBatchName, Value = m.PayBatchID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string json = jss.Serialize(ObjSelectedList);

            return json;
        }

        #endregion

        #region Sundry

        public ActionResult AddHRSundry()
        {
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            PaySundryType paySundryType = new PaySundryType();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Account Code", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            paySundryType.PaySundryID = -1;
            return View(paySundryType);
        }

        [HttpPost]
        public ActionResult AddHRSundry(PaySundryType objPaySundryType)
        {
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            OperationDetails op = new OperationDetails();
            if (objPaySundryType.PaySundryID == -1)
            {
                op = objHR_PaySundriesDB.UpdatePaySundrytypeDetails(objPaySundryType, 1);
                if (op.Success)
                {
                    op.Message = "Pay sundry type inserted successfully";
                }
                else
                {
                    op.Message = "Error while inserting Pay sundry type";
                }
            }
            else
            {
                op = objHR_PaySundriesDB.UpdatePaySundrytypeDetails(objPaySundryType, 2);
                if (op.Success)
                {
                    op.Message = "Pay sundry type updated successfully";
                }
                else
                {
                    op.Message = "Error while updating pay sundry type ";
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public string GetAllHRSundries()
        {
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            foreach (var m in lst)
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.SundryName_1, Value = m.PaySundryID.ToString() });
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string json = jss.Serialize(ObjSelectedList);

            return json;
        }

        #endregion

        #region Report

        public ActionResult PaySundriesReportingViewer(int BatchId, int SundriesId, int AcademicYearId, string BankListIds = "")
        {
            return View();
        }

        public ActionResult GetPaySundriesReporting(int BatchId, int SundriesId, int AcademicYearId, string BankListIds = "")
        {
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            objHR_PaySundriesModelList = new HR_PaySundriesDB().GetHR_PaySundriesListForWPS(BatchId, SundriesId, AcademicYearId, BankListIds);
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            int counter = 1;
            var list = (from item in objHR_PaySundriesModelList.AsEnumerable()
                        select new
                        {
                            SrNo = counter++,
                            EmployeeName = item.EmpName,
                            AccountNumber = item.AccountNumber,
                            Amount = Math.Round(item.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero).ToString(AmountFormat),
                            BankID = item.BankID,
                            BankName = item.BankName,
                            BatchName = item.PayBatchName,
                            cumulativeTotal = Math.Round(objHR_PaySundriesModelList.Where(x => x.BankID == item.BankID).Sum(x => x.Amount), CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero),
                        }).ToList();

            SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
            generalModel.Value2 = schoolInfo.SchoolName_1;
            generalModel.Value3 = "Pay Sundries Report " + new AcademicYearDB().GetAcademicYearById(AcademicYearId).Duration;
            generalModel.Value4 = list[0].BatchName;//TODO: verify if correct

            StiReport report = new StiReport();
            if (list.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/PaySundries/PaySundriesReport.mrt"));
                schoolInfo.Logo = "http://" + Request.Url.Host.ToLower() + ":" + Request.Url.Port.ToString() + schoolInfo.Logo.TrimStart('~');// "http://localhost:56214/images/logo.jpg";
                schoolInfo.TodaysDateTime = DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt");
                report.RegBusinessObject("SchoolInfoModel", schoolInfo);
                report.RegBusinessObject("GeneralModel", generalModel);
                report.RegData("SundriesData", list.OrderBy(x => x.EmployeeName).AsEnumerable());
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        private StiReport NoDataReport()
        {
            StiReport report = new StiReport();
            report.Load(Server.MapPath("~/Content/NoDataReport.mrt"));
            return report;
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #endregion

        #region Print Letter Report

        public ActionResult PaySundriesLetterReportingViewer(int BatchId, int SundriesId, int AcademicYearId)
        {
            return View();
        }

        public ActionResult GetPaySundriesLetterReporting(int BatchId, int SundriesId, int AcademicYearId, string BankListIds = "")
        {
            List<HRMS.Entities.HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HRMS.Entities.HR_PaySundriesModel>();
            objHR_PaySundriesModelList = new HR_PaySundriesDB().GetHR_PaySundriesListForWPS(BatchId, SundriesId, AcademicYearId, BankListIds);
            int counter = 1;
            string AmountFormat = "";
            AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            var list = (from item in objHR_PaySundriesModelList.AsEnumerable()
                        select new
                        {
                            SrNo = counter++,
                            EmployeeID = item.EmployeeID,
                            EmployeeName = item.EmpName,
                            AccountNumber = item.AccountNumber,
                            Amount = Math.Round(item.Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero).ToString(AmountFormat),
                            BankID = item.BankID,
                            BankName = (item.BankName != "" ? "in " + item.BankName + ((item.PayBatchName != "" ? " as " + item.PayBatchName : "")) : ""),
                            BatchName = item.PayBatchName,
                            SundryName=item.SundryName
                        }).ToList();
            HR_PaySundriesModel gg = new HR_PaySundriesModel();

            GeneralModel generalModel = new GeneralModel();
            generalModel.Value1 = DateTime.Now.ToString("dd/MM/yyyy");

            StiReport report = new StiReport();
            if (list.Count > 0)
            {
                report.Load(Server.MapPath("~/Content/Reports/PaySundries/PaySundriesLetterReport.mrt"));
                report.RegBusinessObject("GeneralModel", generalModel);
                report.RegData("SundriesData", list);
            }
            else
            {
                report = NoDataReport();
            }
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        #endregion

        public ActionResult SendLetter(int BatchId, int SundriesId, int AcademicYearId, string BankListIds = "")
        {
            try
            {
                OperationDetails objOperationDetails = new OperationDetails();
                CommonHelper.CommonHelper objCommonHelper = new CommonHelper.CommonHelper();
                List<HR_PaySundriesModel> objHR_PaySundriesModelList = new List<HR_PaySundriesModel>();
                objHR_PaySundriesModelList = new HR_PaySundriesDB().GetHR_PaySundriesListForWPS(BatchId, SundriesId, AcademicYearId, BankListIds);
                SchoolInformation schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation(true);
                string msg = "";
                string mailFormat = "<div>" +
                                        "<div style = \"float:right;\">" +
                                        "Date : {0}" +
                                        "</div>" +
                                        "<div style=\"clear:both;\">" +
                                            "<p>Dear {1},</p>" +
                                             "<p> We are pleased to inform you that we have transferred the amount of AED.{2}/= to your account number {3} in {4} related to {5}.</p>" +
                                             "<p> We thank you and urge you on to continuous and better achievements.</p>" +
                                        "</div>" +
                                        "<div style = \"margin-top:30px;\">" +
                                            "Best Regards," +
                                        "</div>" +
                                        "<div>" + schoolInfo.SchoolName_1 + "</div>" +
                                    "</div>";

                string body = "", to = "";
                string Subject = "";
                string AmountFormat = "";
                AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
                var MailNotSentEmp = "Letter is sent successully to employee's work email except following employees, Please verify their work email.";
                int IsNotSentMail = 0;               
                foreach (var item in objHR_PaySundriesModelList)
                {
                    Subject = (item.SundryName == "N/A" ? item.PayBatchName : item.SundryName);
                    to = new EmployeeDB().GetEmployee(item.EmployeeID).EmailId;//TODO: verify WorkEmail to be used
                    body = string.Format(mailFormat, DateTime.Now.ToString("dd/MM/yyyy"), item.EmpName, item.Amount.ToString(AmountFormat), item.AccountNumber, item.BankName,item.SundryName);
                    if (to == "")
                    {
                        MailNotSentEmp = MailNotSentEmp + "(" + item.EmployeeID + ") " + item.EmpName + ",";
                        IsNotSentMail = 1;
                    }
                    else
                    {
                        objOperationDetails = objCommonHelper.SendMail(to, Subject, body);
                        if (objOperationDetails.Success == false)
                        {
                            IsNotSentMail = 2;
                            break;
                        }
                    }
                }
                if (IsNotSentMail == 1)
                {
                    MailNotSentEmp = MailNotSentEmp.TrimEnd(',');
                    objOperationDetails.Message = MailNotSentEmp;
                    objOperationDetails.CssClass = "success";
                }
                else if (IsNotSentMail == 0)
                {
                    objOperationDetails.CssClass = "success";
                    objOperationDetails.Message = "Letter is sent successully to employee's work email.";
                }
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetEmployeeDDL()
        {
            VacationDB vacationdb = new VacationDB();
            return Json(new SelectList(vacationdb.GetEmployeeList(), "id", "text"), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAcademicYearDDL()
        {
            AcademicYearDB objAcademicYearDB = new AcademicYearDB();
            return Json(new SelectList(objAcademicYearDB.GetAllAcademicYearList(), "AcademicYearId", "Duration"), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetBatchDDL()
        {
            PayBatchDB objPayBatchDB = new PayBatchDB();

            return Json(new SelectList(objPayBatchDB.GetAllPayBatch(0, "All"), "PayBatchID", "PayBatchName"), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSundriesDDL()
        {
            HR_PaySundriesDB objHRSundryDB = new HR_PaySundriesDB();
            List<PaySundryType> lst = objHRSundryDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            return Json(new SelectList(lst, "PaySundryID", "SundryName_1"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBankName()
        {
            return View();
        }

        public ActionResult AddBankNameById()
        {
            //BankList ddl
            BankInformationDB bankInformationDB = new BankInformationDB();
            PayDirectDepositModel payDirectDepositModel = new PayDirectDepositModel();
            ViewBag.BankList = new SelectList(bankInformationDB.GetBanksList(), "BankID", "BankName_1"); ;
            return PartialView("_AddBankName");
        }
    }
}