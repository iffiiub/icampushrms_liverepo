﻿using HRMS.DataAccess;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.General;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Drawing.Imaging;
using HRMS.Web.FileUpload;
using Newtonsoft.Json;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using HRMS.DataAccess.FormsDB;
namespace HRMS.Web.Controllers
{
    public class GeneralControlController : BaseController
    {
        // GET: General
        public ActionResult Index()
        {
            // Session["SelectedGeneral"] = 0;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Control", Value = "0" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Pay Banks", Value = "1" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Pay Bank Groups", Value = "12" });
            //*** Naresh 2020-03-19 Hide Employee Section
            //ObjSelectedList.Add(new SelectListItem { Text = "Employee Section", Value = "2" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Employee ID Setting", Value = "3" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Generate Work Email", Value = "4" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Document Setting", Value = "5" });
            // ObjSelectedList.Add(new SelectListItem { Text = "Absent Type", Value = "6" });
            // ObjSelectedList.Add(new SelectListItem { Text = "Validation Setting", Value = "7" });
            ObjSelectedList.Add(new SelectListItem { Text = "MOL Title", Value = "8" });
            ObjSelectedList.Add(new SelectListItem { Text = "SMTP/Email Configuration", Value = "9" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Accrual Leave Balance", Value = "10" });
            ObjSelectedList.Add(new SelectListItem { Text = "Employee Sponsor", Value = "11" });
            ObjSelectedList.Add(new SelectListItem { Text = "Academic Institutes", Value = "13" });
            ObjSelectedList.Add(new SelectListItem { Text = "Qualification Details", Value = "14" });
            ObjSelectedList.Add(new SelectListItem { Text = "Subject Details", Value = "15" });
            ObjSelectedList.Add(new SelectListItem { Text = "Application Logos", Value = "16" });
            // ObjSelectedList.Add(new SelectListItem { Text = "Pay Category List", Value = "17" });
            //ObjSelectedList.Add(new SelectListItem { Text = "Pay Sundry Type", Value = "18" });
            ObjSelectedList.Add(new SelectListItem { Text = "Reminders Setup", Value = "22" });
            ObjSelectedList.Add(new SelectListItem { Text = "Training Competencies", Value = "23" });
            ObjSelectedList.Add(new SelectListItem { Text = "Data Import", Value = "19" });
            // ObjSelectedList.Add(new SelectListItem { Text = "Short Leave Settings", Value = "20" });
            ObjSelectedList.Add(new SelectListItem { Text = "Insurance Category Amount", Value = "21" });
            ViewBag.ControlList = new SelectList(ObjSelectedList, "Value", "Text");
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.IsAdmin = objUserContextViewModel.UserId;
            DataImportDB objDataImportDB = new DataImportDB();
            ViewBag.ImportTypeList = new SelectList(objDataImportDB.GetDataImportTypeList(), "ImportTypeID", "ImportTypeName");
            List<PickList> insuranceTypeList = new InsuranceDB().GetInsuranceTypeList();
            string insuranceTypes = "";
            insuranceTypes += "<option value=''>Select Company</option>";
            foreach (var obj in insuranceTypeList)
            {
                insuranceTypes += "<option value='" + obj.id + "'>" + obj.text + "</option>";
            }
            ViewBag.InsuranceType = insuranceTypes;
            List<AcademicYearModel> academicYearList = new AcademicYearDB().GetAllAcademicYear();
            string academicYears = "";
            insuranceTypes += "<option value=''>Select Company</option>";
            foreach (var obj in academicYearList)
            {
                academicYears += "<option value='" + obj.AcademicYearId + "'>" + obj.Year + "</option>";
            }
            ViewBag.AcademicYearList = academicYears;
            return View();
        }

        public ActionResult GetBankList()
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            List<PayBanksModel> objBankList = new List<PayBanksModel>();
            BankInformationDB objBankInformationDB = new BankInformationDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objBankList = objBankInformationDB.GetBankListPaging();
            //---------------------------------------------
            var vList = new object();
            vList = new
            {
                aaData = (from item in objBankList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.BankID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.BankID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.BankID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                              BankName_1 = item.BankName_1,
                              BankName_2 = item.BankName_2,
                              BankName_3 = item.BankName_3,
                              BankGroup = item.BankGroupName,
                              acc_code = item.acc_code,
                              RtnCode = item.RtnCode,
                              SwiftCode = item.SwiftCode,
                              CashBank = item.CashBank ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.BankID + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.BankID + "'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditBankDetails(int id)
        {
            BankInformationDB objBankInfoDB = new BankInformationDB();
            PayBanksModel objPayBank = new PayBanksModel();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Account Code", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Bank Group", Value = "" });
            foreach (var m in objBankInfoDB.GetBankgroupList().Where(m => m.IsActive == true))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.BankGroupName_1, Value = m.BankGroupID.ToString() });
            }
            ViewBag.BankGroupList = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Pay Mode", Value = "" });
            foreach (var m in objBankInfoDB.GetPayBanksModeList().Where(m => m.IsActive == true))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayBanksModeName_1, Value = m.PayBanksModeID.ToString() });
            }
            ViewBag.PayBankModeList = ObjSelectedList;

            if (id != 0)
            {
                objPayBank = objBankInfoDB.GetBankdetailsById(id);
            }
            return View(objPayBank);
        }

        [HttpPost]
        public ActionResult AddEditBankDetails(PayBanksModel PayBank)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            BankInformationDB objBankInfoDB = new BankInformationDB();
            if (PayBank.BankID == 0)
            {
                objOperationDetails = objBankInfoDB.AddEditBank(PayBank, 1);
            }
            else
            {
                objOperationDetails = objBankInfoDB.AddEditBank(PayBank, 2);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewBankDetails(int id)
        {
            BankInformationDB objBankInfoDB = new BankInformationDB();
            PayBanksModel objPayBank = new PayBanksModel();
            if (id != 0)
            {
                objPayBank = objBankInfoDB.GetBankdetailsById(id);
            }
            return View(objPayBank);
        }

        public ActionResult DeleteBank(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            BankInformationDB objBankInfoDB = new BankInformationDB();
            PayBanksModel objPayBank = new PayBanksModel();
            objPayBank.BankID = id;
            objOperationDetails = objBankInfoDB.AddEditBank(objPayBank, 3);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeSectionList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }

            DBHelper objDBHelper = new DBHelper();
            List<EmployeeSectionModel> objEmployeeSectionList = new List<EmployeeSectionModel>();
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objEmployeeSectionList = objEmployeeSectionDB.GetEmployeeSectionListPaging();
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objEmployeeSectionList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.EmployeeSectionID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.EmployeeSectionID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.EmployeeSectionID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                              EmployeeSectionName = item.EmployeeSectionName_1,
                              Head = item.HeadName,
                              AssistantName_1 = item.AssistantName_1,
                              AssistantName_2 = item.AssistantName_2,
                              AssistantName_3 = item.AssistantName_3,
                              ShiftName = item.ShiftName,
                              Department = item.DepartmentName
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditEmployeeSection(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int CompanyId = objUserContextViewModel.CompanyId;
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            EmployeeSectionModel objEmployeeSection = new EmployeeSectionModel();
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList(), "EmployeeId", "FullName");
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Shift", Value = "0" });

            foreach (var m in new ShiftDB().GetAllShiftNames(CompanyId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.ShiftName, Value = m.ShiftID.ToString() });

            }
            ViewBag.ShiftList = ObjSelectedList;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Department", Value = "0" });
            foreach (var m in new DepartmentDB().GetDepartmentList(CompanyId))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.DepartmentName_1, Value = m.DepartmentId.ToString() });
            }
            ViewBag.DepartmentList = ObjSelectedList;
            ViewBag.vacationddl = new VacationDB().GetVactionTypeList();
            if (id != 0)
            {
                objEmployeeSection = objEmployeeSectionDB.GetEmployeeSectionById(id);
            }
            return View(objEmployeeSection);
        }

        [HttpPost]
        public ActionResult AddEditEmployeeSection(EmployeeSectionModel EmpSection)
        {
            OperationDetails op = new OperationDetails();
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            if (EmpSection.EmployeeSectionID == 0)
            {
                op = objEmployeeSectionDB.AddEditEmployeeSection(EmpSection, 1);
            }
            else
            {
                op = objEmployeeSectionDB.AddEditEmployeeSection(EmpSection, 2);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewEmployeeSectionDetails(int id)
        {
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            EmployeeSectionModel objEmployeeSection = new EmployeeSectionModel();
            if (id != 0)
            {
                objEmployeeSection = objEmployeeSectionDB.GetEmployeeSectionById(id);
            }
            return View(objEmployeeSection);
        }

        public ActionResult DeleteSection(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            EmployeeSectionDB objEmployeeSectionDB = new EmployeeSectionDB();
            EmployeeSectionModel objEmployeeSection = new EmployeeSectionModel();
            objEmployeeSection.EmployeeSectionID = id;
            objOperationDetails = objEmployeeSectionDB.AddEditEmployeeSection(objEmployeeSection, 3);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeIdSettingDetails()
        {
            EmployeeIdSetting objEmployeeIdSetting = new EmployeeIdSetting();
            objEmployeeIdSetting = new EmployeeDB().GetEmployeeIdSetting();
            return Json(objEmployeeIdSetting, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveEmployeeIdSetting(string prefix, int? StartFrom)
        {
            prefix = prefix.Trim();
            OperationDetails op = new OperationDetails();
            op = new EmployeeDB().AddEmpAlternativeIdSettings(prefix, StartFrom);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteEmployeeIdSettingDetails()
        {
            OperationDetails op = new OperationDetails();
            op = new EmployeeDB().DeleteEmpAlternativeIdSettings();
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoGenerateWorkEmailSetting()
        {
            EmployeeWorkEmailSetting objSetting = new EmployeeWorkEmailSetting();
            objSetting = new EmployeeDB().GetEmployeeWorkEmailSetting();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select first name", Value = "" });
            ObjSelectedList.Add(new SelectListItem { Text = "Employee first name", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "First character of employee first name", Value = "2" });
            ViewBag.FirstOptions = new SelectList(ObjSelectedList, "Value", "Text");

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select operator", Value = "" });
            ObjSelectedList.Add(new SelectListItem { Text = ".", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "_", Value = "2" });
            ViewBag.Secondoptions = new SelectList(ObjSelectedList, "Value", "Text");

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select third name.", Value = "" });
            ObjSelectedList.Add(new SelectListItem { Text = "Employee last name", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "First character of employee last name", Value = "2" });
            ObjSelectedList.Add(new SelectListItem { Text = "Employee last name with employee ID", Value = "3" });
            ViewBag.ThirdOptions = new SelectList(ObjSelectedList, "Value", "Text");
            return View(objSetting);
        }

        public ActionResult AddEditWorkEmailSetting(EmployeeWorkEmailSetting objSetting)
        {
            string Formula = objSetting.WorkEmailFirstOption + "-" + objSetting.WorkEmailSecondOption + "-" + objSetting.WorkEmailThirdOption;
            EmployeeDB objEmpDB = new EmployeeDB();
            OperationDetails op = new OperationDetails();
            op = objEmpDB.SaveEmployeeWorkEmailSetting(Formula, objSetting.EmailDomainName);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeWorkEmailList(int empid = 0)
        {
            if (empid != 0)
            {

                Session["EmployeeListID"] = empid;
            }

            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------

            List<EmployeeContactModel> objWorkEmailList = new List<EmployeeContactModel>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objWorkEmailList = objEmployeeDB.GetAllEmployeeWorkEmailDetails();
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objWorkEmailList
                          select new
                          {
                              EmployeeAltId = item.EmpAltId,
                              EmployeeName = item.EmployeeName,
                              WorkEmail = item.WorkEmail
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeWithMissingWorkEmailList(int empid = 0)
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            List<EmployeeContactModel> objWorkEmailList = new List<EmployeeContactModel>();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            objWorkEmailList = objEmployeeDB.GetAllEmployeeWorkEmailDetails().Where(x => x.WorkEmail == "").ToList();
            //---------------------------------------------

            var vList = new object();

            vList = new
            {
                aaData = (from item in objWorkEmailList
                          select new
                          {
                              EmployeeAltId = item.EmpAltId,
                              EmployeeName = item.EmployeeName,
                              WorkEmail = item.WorkEmail
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEmployeeWorkEmails(int Mode)
        {
            EmployeeWorkEmailSetting objMailsetting = new EmployeeWorkEmailSetting();
            EmployeeDB objEmpDB = new EmployeeDB();
            objMailsetting = objEmpDB.GetEmployeeWorkEmailSetting();
            DataAccess.GeneralDB.CommonDB commonDb = new CommonDB();
            string Formula = commonDb.WorkEmailFirstOption.Where(x => x.Key == objMailsetting.WorkEmailFirstOption).FirstOrDefault().Value + "+'" +
                           commonDb.WorkEmailSecondOption.Where(x => x.Key == objMailsetting.WorkEmailSecondOption).FirstOrDefault().Value + "'+" +
                           commonDb.WorkEmailThirdOption.Where(x => x.Key == objMailsetting.WorkEmailThirdOption).FirstOrDefault().Value;
            OperationDetails OP = new OperationDetails();
            OP = objEmpDB.CreateEmployeeWorkEmails(Mode, Formula, objMailsetting.EmailDomainName);
            return Json(OP, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLeaveTypes()
        {
            DBHelper objDBHelper = new DBHelper();
            List<AbsentTypeModel> AbsentTypeModelList = new List<AbsentTypeModel>();
            AbsentTypeModelList = new AbsentTypeDB().GetAbsentTypeList();
            var vList = new object();
            CommonDB objCommonDB = new CommonDB();
            vList = new
            {
                aaData = (from item in AbsentTypeModelList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.PayAbsentTypeID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.PayAbsentTypeID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.PayAbsentTypeID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                              PayAbsentTypeName1 = item.PayAbsentTypeName_1,
                              PayAbsentTypeName2 = item.PayAbsentTypeName_2,
                              PayAbsentTypeName3 = item.PayAbsentTypeName_3,
                              IsDeductible = item.ISDeductable ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.ISDeductable + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.ISDeductable + "'/>",
                              PaidType = item.PaidType == 0 ? "" : objCommonDB.PaidType.Where(x => x.Key == item.PaidType).FirstOrDefault().Value,
                              AnnualLeave = item.AnnualLeave ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.AnnualLeave + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.AnnualLeave + "'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddEditAbsentType(int AbsentTypeId)
        {
            AbsentTypeModel objAbsentType = new AbsentTypeModel();
            AbsentTypeDB objAbsentDB = new AbsentTypeDB();
            objAbsentType = objAbsentDB.GetAbsentType(AbsentTypeId);
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select paid type", Value = "0" });
            ObjSelectedList.Add(new SelectListItem { Text = "Not paid", Value = "1" });
            ObjSelectedList.Add(new SelectListItem { Text = "Paid", Value = "2" });
            ObjSelectedList.Add(new SelectListItem { Text = "Half Paid", Value = "3" });
            ViewBag.PaidTypes = new SelectList(ObjSelectedList, "Value", "Text");
            return View(objAbsentType);
        }

        [HttpPost]
        public ActionResult AddEditAbsentType(AbsentTypeModel objAbsentType)
        {

            AbsentTypeDB objAbsentDB = new AbsentTypeDB();
            OperationDetails op = new OperationDetails();
            if (objAbsentType.PayAbsentTypeID == 0)
            {
                op = objAbsentDB.AddEditAbsentType(objAbsentType, 1);
            }
            else
            {
                op = objAbsentDB.AddEditAbsentType(objAbsentType, 2);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewAbsentType(int id)
        {
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            AbsentTypeModel objAbsentModel = new AbsentTypeModel();
            objAbsentModel = objAbsentTypeDB.GetAbsentType(id);
            CommonDB objCommonDB = new CommonDB();
            ViewBag.PaidType = objAbsentModel.PaidType == 0 ? "" : objCommonDB.PaidType.Where(x => x.Key == objAbsentModel.PaidType).FirstOrDefault().Value;
            return View(objAbsentModel);
        }

        public ActionResult DeleteAbsentType(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            AbsentTypeModel AbsentType = new AbsentTypeModel();
            AbsentType.PayAbsentTypeID = id;
            objOperationDetails = objAbsentTypeDB.AddEditAbsentType(AbsentType, 3);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDocumentExprirySetting()
        {
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            List<DocumentDaySettingModel> objDocumentDaySettingList = new List<DocumentDaySettingModel>();
            DocumentDB objDocumentDB = new DocumentDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objDocumentDaySettingList = objDocumentDB.GetDocumentDaySetting();
            //---------------------------------------------
            var vList = new object();
            vList = new
            {
                aaData = (from item in objDocumentDaySettingList
                          select new
                          {
                              DocumentTypeId = item.DocumentTypeId,
                              DocumentTypeName = item.DocumentTypeName,
                              DocumentsAboutToExpireDays = "<input type='text' class='form-control number DocumentsAboutToExpireDays' value='" + item.DocumentsAboutToExpireDays + "' />",
                              DocumentExpiredDays = "<input type='text' class='form-control number DocumentExpiredDays' value='" + item.DocumentExpiredDays + "' />",
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditDocumentSetting(this," + item.DocumentTypeId + ")' title='Save' ><i class='fa fa-floppy-o'></i> </a>",
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UpdateDocumentDaysSetting(int DocumentTypeId, int DocumentsAboutToExpireDays, int DocumentExpiredDays)
        {
            DocumentDB documentDB = new DocumentDB();
            return Json(documentDB.UpdateDocumentDaysSetting(new DocumentDaySettingModel() { DocumentTypeId = DocumentTypeId, DocumentsAboutToExpireDays = DocumentsAboutToExpireDays, DocumentExpiredDays = DocumentExpiredDays }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetValidationSettingDetails()
        {
            CommonDB objCommonDB = new CommonDB();
            var vList = new object();
            List<ValidationSetting> lstValidationList = objCommonDB.GetValidationSetting();
            vList = new
            {
                aaData = (from item in lstValidationList
                          select new
                          {
                              ValidationSettingID = item.ValidationSettingID,
                              ValidationKey = item.ValidationKey,
                              IsMandatory = item.IsMandatory ? "<input type='checkbox' class='checkbox' checked='true' id='ValidationSetting_" + item.ValidationSettingID + "' onclick='updateValidationSetting(" + item.ValidationSettingID + ")'/>" : "<input type='checkbox' class='checkbox' id='ValidationSetting_" + item.ValidationSettingID + "' onclick='updateValidationSetting(" + item.ValidationSettingID + ")'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateValidationSetting(int ValidationSettingID, bool isChecked)
        {
            OperationDetails op = new OperationDetails();
            CommonDB objCommonDB = new CommonDB();
            op = objCommonDB.UpdateValidationSetting(ValidationSettingID, isChecked);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMolTitleList()
        {
            MOLTitleDB objMOLTitleDB = new MOLTitleDB();
            var vList = new object();
            List<MOLTitleModel> lstMOLTitleList = objMOLTitleDB.GetAllMOLTitleList();
            vList = new
            {
                aaData = (from item in lstMOLTitleList
                          select new
                          {
                              MolTitle_1 = item.MOLTitleName_1,
                              MolTitle_2 = item.MOLTitleName_2,
                              MolTitle_3 = item.MOLTitleName_3,
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.MOLTitleID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.MOLTitleID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditMolTitle(int id)
        {
            MOLTitleDB objMoltitleDB = new MOLTitleDB();
            MOLTitleModel objMOLTitle = new MOLTitleModel();
            if (id != 0)
            {
                objMOLTitle = objMoltitleDB.GetMOLTitle(id);
            }
            return View(objMOLTitle);
        }

        [HttpPost]
        public ActionResult AddEditMolTitle(MOLTitleModel objMOLTitle)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            MOLTitleDB objMoltitleDB = new MOLTitleDB();
            if (objMOLTitle.MOLTitleID == 0)
            {
                objOperationDetails = objMoltitleDB.AddUpdateDeleteMOLTitle(objMOLTitle, 1);
            }
            else
            {
                objOperationDetails = objMoltitleDB.AddUpdateDeleteMOLTitle(objMOLTitle, 2);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteMOLTitle(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            MOLTitleDB objMOLTitleDB = new MOLTitleDB();
            MOLTitleModel objMol = new MOLTitleModel();
            objMol.MOLTitleID = id;
            objOperationDetails = objMOLTitleDB.AddUpdateDeleteMOLTitle(objMol, 3);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditSMTPConfig()
        {
            CommonDB objCommonDB = new CommonDB();
            SMTPConfig objConfig = new SMTPConfig();
            objConfig = objCommonDB.GetSMTPConfig();
            return View(objConfig);
        }

        [HttpPost]
        public ActionResult AddEditSMTPConfig(SMTPConfig objSMTP)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            CommonDB objCommonDB = new CommonDB();
            objOperationDetails = objCommonDB.UpdateSMTPSetting(objSMTP, 1);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSMTPConfiguration()
        {
            CommonDB objCommonDB = new CommonDB();
            SMTPConfig objConfig = new SMTPConfig();
            objConfig = objCommonDB.GetSMTPConfig();
            objConfig.Password = "******";
            return Json(objConfig, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSMTPConfiguration()
        {
            OperationDetails op = new OperationDetails();
            CommonDB objCommonDB = new CommonDB();
            SMTPConfig objSMTP = new SMTPConfig();
            op = objCommonDB.UpdateSMTPSetting(objSMTP, 2);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewSMTPConfig()
        {
            CommonDB objCommonDB = new CommonDB();
            SMTPConfig objConfig = new SMTPConfig();
            objConfig = objCommonDB.GetSMTPConfig();
            return View(objConfig);
        }
        public ActionResult SendTestEmail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendTestEmail(string ReceiverEmailAddress)
        {
            OperationDetails op = new OperationDetails();
            CommonDB objCommonDB = new CommonDB();
            SMTPConfig objSMTP = new SMTPConfig();
            objSMTP = objCommonDB.GetSMTPConfig();
            CommonHelper.CommonHelper objCommon = new Web.CommonHelper.CommonHelper(); ;
            bool IsMailSent = true;
            op = objCommon.SendMail(ReceiverEmailAddress, "Test Email Configuration", "Hi,<br /><br />Sending test email feature is working successfully.<br /><br />Thank You");
            //creating the object of MailMessage
            if (op.Success)
            {
                op.Message = "Test email sent successfully";
                op.Success = true;
                op.CssClass = "success";
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeOpeningBalanceLeaves()
        {
            var vList = new object();
            VacationDB objVacDB = new VacationDB();
            List<VacationModel> lstOpeningBalanceList = objVacDB.GetEmployeeOpeningBalance();
            int i = 0;
            vList = new
            {
                aaData = (from item in lstOpeningBalanceList
                          select new
                          {
                              index = i = i + 1,
                              OBID = item.AccumulativeOBID,
                              EmpId = item.employeeId,
                              EmployeeID = item.employeeAlternativeId,
                              EmployeeName = item.employeeName,
                              VacationTypeID = item.vacationTypeId,
                              vacationName = item.vacationType,
                              OpeningBalance = item.OpeningBalance.ToString(),
                              BalanceDate = item.OpeningBalanceDate,
                              Action = "<a data-OpeningBalance='" + item.OpeningBalance + "' class='btn btn-success  btn-rounded btn-condensed btn-sm btn-space btnEditRow' href='javascript:void(0);' onclick='EditOpeningBalance(this," + i.ToString() + ")' title='Edit' id='btnEdit_" + i.ToString() + "'><i class='fa fa-pencil' ></i></a>"
                                   + "<a data-OpeningBalance='" + item.OpeningBalance + "' data-OpeningBalDate='" + item.OpeningBalanceDate + "' class='btn btn-info  btn-rounded btn-condensed btn-sm btn-space btnRowRefresh' href='javascript:void(0);' onclick='RefreshRow(this," + i.ToString() + ")' style='display:none' title='Refresh' id='btnRefresh_" + i.ToString() + "'><i class='fa fa-refresh' ></i></a>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PostEmployeeOpeningBalance(List<VacationModel> listOpeningBalance)
        {
            OperationDetails op = new OperationDetails();
            op = new VacationDB().AddMultipleAllowance(listOpeningBalance);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetEmployeeSponsor()
        {
            var vList = new object();
            DocumentDB documentDB = new DocumentDB();
            List<SponsorModel> lstSponsorModel = documentDB.GetSponsorList(null);
            int i = 0;
            vList = new
            {
                aaData = (from item in lstSponsorModel
                          select new
                          {

                              SponsorID = item.SponsorID,
                              SponsorName_1 = item.SponsorName_1,
                              SponsorName_2 = item.SponsorName_2,
                              SponsorName_3 = item.SponsorName_3,
                              IsActive = "<input type='checkbox'  " + (item.IsActive ? "checked" : "") + " disabled='disabled' >",
                              Action = "<a  class='btn btn-success  btn-rounded btn-condensed btn-sm btn-space' href='javascript:void(0);' onclick='AddEditSponsor(" + item.SponsorID + ")' title='Edit' id='btnEditSponsorid'><i class='fa fa-pencil' ></i></a>"
                                   + "<a  class='btn btn-danger  btn-rounded btn-condensed btn-sm btn-space' href='javascript:void(0);' onclick='DeleteSponsor(" + item.SponsorID + ")'   title='Delete' id='btnDeleteSponsorid' ><i class='fa fa-times' ></i></a>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUpdateSponsor(int id = 0)
        {
            DocumentDB documentDB = new DocumentDB();
            SponsorModel sponsorModel = new SponsorModel();
            if (id != 0)
                sponsorModel = documentDB.GetSponsorList(null).FirstOrDefault(x => x.SponsorID == id);
            return View(sponsorModel);
        }

        [HttpPost]
        public ActionResult AddUpdateSponsor(SponsorModel model)
        {
            DocumentDB documentDB = new DocumentDB();
            OperationDetails operationDetails = null;
            operationDetails = documentDB.CURDSponsor(model, model.SponsorID == 0 ? 1 : 2);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSponsor(int SponsorId)
        {
            DocumentDB documentDB = new DocumentDB();
            SponsorModel sponsorModel = new SponsorModel() { SponsorID = SponsorId, SponsorName_1 = "", SponsorName_2 = "", SponsorName_3 = "", SponsorPassportNumber = "", IsActive = false };
            OperationDetails operationDetails = operationDetails = documentDB.CURDSponsor(sponsorModel, 3);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBankGroupList()
        {
            List<PayBankGroup> objBankGroupList = new List<PayBankGroup>();
            BankInformationDB objBankInformationDB = new BankInformationDB();
            objBankGroupList = objBankInformationDB.GetBankgroupList();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objBankGroupList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.BankGroupID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.BankGroupID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.BankGroupID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                              BankGroupName_1 = item.BankGroupName_1,
                              BankGroupName_2 = item.BankGroupName_2,
                              BankGroupName_3 = item.BankGroupName_3,
                              SalaryPayableAccountCode = item.SalaryPayableAccountCode,
                              IsActive = item.IsActive ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.BankGroupID + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.BankGroupID + "'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditBankGroupDetails(int id)
        {
            BankInformationDB objBankInfoDB = new BankInformationDB();
            PayBankGroup objPayBankGroup = new PayBankGroup();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Account Code", Value = "" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;
            if (id != 0)
            {
                objPayBankGroup = objBankInfoDB.GetBankGroupDetailsById(id);
            }
            return View(objPayBankGroup);
        }

        [HttpPost]
        public ActionResult AddEditBankGroupDetails(PayBankGroup payBankGroup)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            BankInformationDB objBankInfoDB = new BankInformationDB();
            if (payBankGroup.BankGroupID == 0)
            {
                objOperationDetails = objBankInfoDB.AddEditBankGroup(payBankGroup, 1);
            }
            else
            {
                objOperationDetails = objBankInfoDB.AddEditBankGroup(payBankGroup, 2);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewBankGroupDetails(int id)
        {
            BankInformationDB objBankInfoDB = new BankInformationDB();
            PayBankGroup objPayBankGRoup = new PayBankGroup();
            if (id != 0)
            {
                objPayBankGRoup = objBankInfoDB.GetBankGroupDetailsById(id);
            }
            return View(objPayBankGRoup);
        }

        public ActionResult DeleteBankGroup(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            BankInformationDB objBankInfoDB = new BankInformationDB();
            PayBankGroup objPayBankGroup = new PayBankGroup();
            objPayBankGroup.BankGroupID = id;
            objOperationDetails = objBankInfoDB.AddEditBankGroup(objPayBankGroup, 3);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAcademicInstitutes()
        {
            List<HRInstituteModel> objAcademicInstitutesList = new List<HRInstituteModel>();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            objAcademicInstitutesList = objHRInstituteDB.GetAcademicInstitutes();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objAcademicInstitutesList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.HRInstituteID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.HRInstituteID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.HRInstituteID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              HRInstituteName_1 = item.HRInstituteName_1,
                              HRInstituteName_2 = item.HRInstituteName_2,
                              HRInstituteName_3 = item.HRInstituteName_3,
                              IsActive = item.IsActive ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.HRInstituteID + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.HRInstituteID + "'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditAcademicInstitutes(int id)
        {
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRInstituteModel academicInstitute = new HRInstituteModel();
            if (id != 0)
            {
                academicInstitute = objHRInstituteDB.GetAcademicInstitutesById(id);
            }
            return View(academicInstitute);
        }

        [HttpPost]
        public ActionResult AddEditAcademicInstitutes(HRInstituteModel academicInstituteModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            if (academicInstituteModel.HRInstituteID == 0)
            {
                objOperationDetails = objHRInstituteDB.AddEditAcademicInstitute(academicInstituteModel, 1);
            }
            else
            {
                objOperationDetails = objHRInstituteDB.AddEditAcademicInstitute(academicInstituteModel, 2);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewAcademicInstitutesDetails(int id)
        {
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            HRInstituteModel academicInstitute = new HRInstituteModel();
            if (id != 0)
            {
                academicInstitute = objHRInstituteDB.GetAcademicInstitutesById(id);
            }
            return View(academicInstitute);
        }

        public ActionResult DeleteAcademicInstitutes(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            HRInstituteModel academicInstituteModel = new HRInstituteModel();
            HRInstituteDB objHRInstituteDB = new HRInstituteDB();
            academicInstituteModel.HRInstituteID = id;
            objOperationDetails = objHRInstituteDB.AddEditAcademicInstitute(academicInstituteModel, 3);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetQualifications()
        {
            List<HRQualificationModel> objHRQualificationList = new List<HRQualificationModel>();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            objHRQualificationList = objHRQualificationDB.GetQualifications();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objHRQualificationList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.HRQualificationID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.HRQualificationID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.HRQualificationID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              HRQualificationName_1 = item.HRQualificationName_1,
                              HRQualificationName_2 = item.HRQualificationName_2,
                              HRQualificationName_3 = item.HRQualificationName_3,
                              IsActive = item.IsActive ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.HRQualificationID + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.HRQualificationID + "'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditQualifications(int id)
        {
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            HRQualificationModel qualificationModel = new HRQualificationModel();
            if (id != 0)
            {
                qualificationModel = objHRQualificationDB.GetQualificationsById(id);
            }
            return View(qualificationModel);
        }

        [HttpPost]
        public ActionResult AddEditQualifications(HRQualificationModel qualificationModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            if (qualificationModel.HRQualificationID == 0)
            {
                objOperationDetails = objHRQualificationDB.AddEditQualifications(qualificationModel, 1);
            }
            else
            {
                objOperationDetails = objHRQualificationDB.AddEditQualifications(qualificationModel, 2);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewQualificationsDetails(int id)
        {
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            HRQualificationModel qualificationModel = new HRQualificationModel();
            if (id != 0)
            {
                qualificationModel = objHRQualificationDB.GetQualificationsById(id);
            }
            return View(qualificationModel);
        }

        public ActionResult DeleteQualifications(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            HRQualificationModel qualificationModel = new HRQualificationModel();
            qualificationModel.HRQualificationID = id;
            objOperationDetails = objHRQualificationDB.AddEditQualifications(qualificationModel, 3);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetQualificationSubjects()
        {
            List<HRQualificationModel> objHRQualificationList = new List<HRQualificationModel>();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            objHRQualificationList = objHRQualificationDB.GetQualificationSubjects();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objHRQualificationList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.HRQualificationID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.HRQualificationID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.HRQualificationID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              HRQualificationName_1 = item.HRQualificationName_1,
                              HRQualificationName_2 = item.HRQualificationName_2,
                              HRQualificationName_3 = item.HRQualificationName_3,
                              IsActive = item.IsActive ? "<input type='checkbox' class='checkbox' checked='true' disabled id='" + item.HRQualificationID + "'/>" : "<input type='checkbox' class='checkbox' disabled id='" + item.HRQualificationID + "'/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditQualificationSubject(int id)
        {
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            HRQualificationModel qualificationModel = new HRQualificationModel();
            if (id != 0)
            {
                qualificationModel = objHRQualificationDB.GetQualificationSubjectById(id);
            }
            return View(qualificationModel);
        }

        [HttpPost]
        public ActionResult AddEditQualificationSubject(HRQualificationModel qualificationModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            if (qualificationModel.HRQualificationID == 0)
            {
                objOperationDetails = objHRQualificationDB.AddEditQualificationSubject(qualificationModel, 1);
            }
            else
            {
                objOperationDetails = objHRQualificationDB.AddEditQualificationSubject(qualificationModel, 2);
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewQualificationSubjectDetails(int id)
        {
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            HRQualificationModel qualificationModel = new HRQualificationModel();
            if (id != 0)
            {
                qualificationModel = objHRQualificationDB.GetQualificationSubjectById(id);
            }
            return View(qualificationModel);
        }

        public ActionResult DeleteQualificationSubject(int id)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            HRQualificationModel qualificationModel = new HRQualificationModel();
            qualificationModel.HRQualificationID = id;
            objOperationDetails = objHRQualificationDB.AddEditQualificationSubject(qualificationModel, 3);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ApplicationLogoModal(string modalId)
        {
            ViewBag.modalId = modalId;
            return View();
        }

        public ActionResult GetImageCropper(string id, string chkImageFor, string fileName, string fileExtention)
        {
            Session["fileName"] = fileName;
            Session["fileExtention"] = fileExtention;
            ViewBag.imagePath = id;
            ViewBag.chkImageFor = chkImageFor;
            ViewBag.fileName = fileName;
            return PartialView("~/Views/Shared/GetImageCropper.cshtml");
        }
        public ActionResult GetImageCropperTest(string id, string chkImageFor, string fileName, string fileExtention)
        {
            
            return PartialView("~/Views/Shared/GetImageCropper.cshtml");
        }
        public ActionResult UploadImage(string base64String, string chkImageForId)
        {
            base64String = base64String.Replace("data:image/jpeg;base64,", "");
            byte[] bytes = Convert.FromBase64String(base64String);
            string filename = (string)Session["fileName"] + DateTime.Now.ToString("ddMMyyyyhhmmss") + "." + (string)Session["fileExtention"];
            var filePathOriginal = "";
            string savedFileName = "";
            string dbFilePath = "";
            if (chkImageForId.Equals("modalL"))
            {
                filePathOriginal = Server.MapPath("~/Content/images/Logo");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/Logo/" + filename;
                Session["LargeLogoPath"] = dbFilePath;
            }
            else if (chkImageForId.Equals("modalS"))
            {
                filePathOriginal = Server.MapPath("~/Content/images/Logo");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/Logo/" + filename;
                Session["SmallLogoPath"] = dbFilePath;
            }
            else if (chkImageForId.Equals("modalR"))
            {
                filePathOriginal = Server.MapPath("~/Content/images");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/" + filename;
                Session["ReportLogoPath"] = dbFilePath;
            }
            else
            {
                filePathOriginal = Server.MapPath("~/Content/images");
                savedFileName = Path.Combine(filePathOriginal, filename);
                dbFilePath = "/Content/images/" + filename;
                Session["ProfileLogo"] = dbFilePath;
                Session["ProfileFileExtension"] = (string)Session["fileExtention"];
            }


            FileUploadHelper.UploadFile(bytes, savedFileName);
            Session["fileName"] = null;
            Session["fileExtention"] = null;

            return Json(new { chkImageForId = chkImageForId }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveApplicationLogo()
        {
            string resultMessage = "";
            string SavedImagePath = "";
            GeneralControlDB generalControlDB = new GeneralControlDB();
            if (Session["LargeLogoPath"] != null)
            {
                resultMessage = generalControlDB.SaveApplicationLogo(Convert.ToString(Session["LargeLogoPath"]), 1);
                Session["App_LargeLogo"] = Convert.ToString(Session["LargeLogoPath"]);
                SavedImagePath = Convert.ToString(Session["LargeLogoPath"]);
                Session["LargeLogoPath"] = null;
            }
            if (Session["SmallLogoPath"] != null)
            {
                resultMessage = generalControlDB.SaveApplicationLogo(Convert.ToString(Session["SmallLogoPath"]), 2);
                Session["App_SmallLogo"] = Convert.ToString(Session["SmallLogoPath"]);
                SavedImagePath = Convert.ToString(Session["SmallLogoPath"]);
                Session["SmallLogoPath"] = null;
            }
            if (Session["ReportLogoPath"] != null)
            {
                resultMessage = generalControlDB.SaveApplicationLogo(Convert.ToString(Session["ReportLogoPath"]), 3);
                Session["ReportLogoPath"] = null;
            }

            return Json(new { message = resultMessage, SavedImagePath = SavedImagePath }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteImage()
        {
            if (Session["LargeLogoPath"] != null)
            {
                string fullPath = (string)Session["LargeLogoPath"];
                string filePathOriginal = Server.MapPath(fullPath);
                FileUploadHelper.DeleteFile(filePathOriginal);
                Session["LargeLogoPath"] = null;
            }
            if (Session["SmallLogoPath"] != null)
            {
                string fullPath = (string)Session["SmallLogoPath"];
                string filePathOriginal = Server.MapPath(fullPath);
                FileUploadHelper.DeleteFile(filePathOriginal);
                Session["SmallLogoPath"] = null;
            }

            if (Session["ReportLogoPath"] != null)
            {
                string fullPath = (string)Session["ReportLogoPath"];
                string filePathOriginal = Server.MapPath(fullPath);
                FileUploadHelper.DeleteFile(filePathOriginal);
                Session["ReportLogoPath"] = null;
            }
            if (Session["ProfileLogo"] != null)
            {
                string fullPath = (string)Session["ProfileLogo"];
                string filePathOriginal = Server.MapPath(fullPath);
                FileUploadHelper.DeleteFile(filePathOriginal);
                Session["ProfileFileExtension"] = null;
                Session["ProfileLogo"] = null;
            }
            return Json(JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPayCategories()
        {
            List<PayCategoriesModel> objPaycategoryList = new List<PayCategoriesModel>();
            BankInformationDB objBankInformationDB = new BankInformationDB();
            objPaycategoryList = objBankInformationDB.GetPayCategoriesList().Where(m => m.isDeleted == false).ToList();
            var vList = new object();
            vList = new
            {
                aaData = (from item in objPaycategoryList
                          select new
                          {
                              Action = item.CategoryID == 0 ? "" : "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.CategoryID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.CategoryID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.CategoryID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PayCategoryName1 = item.CategoryName_1,
                              PayCategoryName2 = item.CategoryName_2,
                              PayCategoryName3 = item.CategoryName_3,
                              Seq = item.seq
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditPayCatagory(int id)
        {
            BankInformationDB objBankDB = new BankInformationDB();
            PayrollDB objPayrollDB = new PayrollDB();
            PayCategoriesModel PayCategory = new PayCategoriesModel();
            int ActiveCCCount = 0;
            bool IsCostCenterValidated = objPayrollDB.GetCostCenterValidation(out ActiveCCCount);
            if (id != -1)
            {
                PayCategory = objBankDB.GetPayCategoryDetailsByID(id);
            }
            else
            {
                PayCategory.CategoryID = -1;
            }
            PayCategory.lstCostCenters = objPayrollDB.GetPayCategoryCostCenterDetails(id);
            if (IsCostCenterValidated && ActiveCCCount > 1)
            {
                ViewBag.IsCostCenterValidated = false;
            }
            else
            {
                ViewBag.IsCostCenterValidated = IsCostCenterValidated;
            }
            return View(PayCategory);
        }

        [HttpPost]
        public ActionResult AddEditPayCatagory(PayCategoriesModel objPayCategory, string CostCenters)
        {
            List<CostCenter> lstCostCenters = JsonConvert.DeserializeObject<List<CostCenter>>(CostCenters);

            DataTable multipleCostCenters = new DataTable();
            multipleCostCenters.Columns.Add("PayCategoryId", typeof(int));
            multipleCostCenters.Columns.Add("DimensionValueID", typeof(int));
            multipleCostCenters.Columns.Add("Percentage", typeof(decimal));
            multipleCostCenters.Columns.Add("rowNum", typeof(int));

            int rowCount = 1;
            foreach (var item in lstCostCenters)
            {
                multipleCostCenters.Rows.Add(
                    item.PayCategoryID,
                    item.DimensionValueID,
                    item.Percentage,
                    rowCount
                    );
                rowCount++;
            }


            PayrollDB objPayrollDB = new PayrollDB();
            OperationDetails op = new OperationDetails();
            if (objPayCategory.CategoryID == -1)
            {
                op = objPayrollDB.UpdatePayCategoryDetails(objPayCategory, 1, multipleCostCenters);
                if (op.Success)
                {
                    op.Message = "Pay category inserted successfully";
                }
                else
                {
                    op.Message = "Error while inserting pay category";
                }
            }
            else
            {
                op = objPayrollDB.UpdatePayCategoryDetails(objPayCategory, 2, multipleCostCenters);
                if (op.Success)
                {
                    op.Message = "Pay category updated successfully";
                }
                else
                {
                    op.Message = "Error while updating pay category";
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCostCenterValidation()
        {
            int ActiveCCCount = 0;
            PayrollDB objPayrollDB = new PayrollDB();
            OperationDetails oDetails = new OperationDetails();
            bool IsCostCenterValidated = objPayrollDB.GetCostCenterValidation(out ActiveCCCount);
            oDetails.Success = true;
            if (ActiveCCCount > 1 && IsCostCenterValidated)
            {
                oDetails.Success = false;
                oDetails.Message = " System currently not support multiple cost centers. You have to proceed manually for assigning cost center under JVs.";
                oDetails.CssClass = "warning";
            }
            return Json(oDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePayCategory(int id)
        {
            OperationDetails oDetails = new OperationDetails();
            PayCategoriesModel objPayCategory = new PayCategoriesModel();
            PayrollDB objPayrollDB = new PayrollDB();
            objPayCategory.CategoryID = id;
            DataTable multipleCostCenters = new DataTable();
            multipleCostCenters.Columns.Add("PayCategoryId", typeof(int));
            multipleCostCenters.Columns.Add("DimensionValueID", typeof(int));
            multipleCostCenters.Columns.Add("Percentage", typeof(decimal));
            multipleCostCenters.Columns.Add("rowNum", typeof(int));
            oDetails = objPayrollDB.UpdatePayCategoryDetails(objPayCategory, 3, multipleCostCenters);
            if (oDetails.Success)
            {
                oDetails.Message = "Pay category deleted successfully";
            }
            else
            {
                oDetails.Message = "Error while deleting pay category";
            }
            return Json(oDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewPayCategory(int id)
        {
            BankInformationDB objBankDB = new BankInformationDB();
            PayrollDB objPayrollDB = new PayrollDB();
            PayCategoriesModel PayCategory = new PayCategoriesModel();
            int ActiveCCCount = 0;
            bool IsCostCenterValidated = objPayrollDB.GetCostCenterValidation(out ActiveCCCount);
            if (id != 0)
            {
                PayCategory = objBankDB.GetPayCategoryDetailsByID(id);
            }
            PayCategory.lstCostCenters = objPayrollDB.GetPayCategoryCostCenterDetails(id);
            if (IsCostCenterValidated && ActiveCCCount > 1)
            {
                ViewBag.IsCostCenterValidated = false;
            }
            else
            {
                ViewBag.IsCostCenterValidated = IsCostCenterValidated;
            }

            //ViewBag.CostCenter = PayCategory.DimensionName==""?"":PayCategory.DimensionName + " - " + PayCategory.DimensionValueName;
            return View(PayCategory);
        }

        public ActionResult GetCostCenterDetails(int PayCategoryId)
        {
            if (PayCategoryId == 0)
            {
                PayCategoryId = -1;
            }
            List<CostCenter> costCenterList = new List<CostCenter>();
            PayrollDB objPayrollDB = new PayrollDB();
            costCenterList = objPayrollDB.GetPayCategoryCostCenterDetails(PayCategoryId);
            return Json(new { list = costCenterList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPaySundries()
        {
            List<PaySundryType> paySundryList = new List<PaySundryType>();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            paySundryList = objHR_PaySundriesDB.GetPaySundriesType().Where(m => m.IsDeleted == false).ToList();
            var vList = new object();
            vList = new
            {
                aaData = (from item in paySundryList
                          select new
                          {
                              Action = item.PaySundryID == 0 ? "" : "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.PaySundryID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.PaySundryID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              PaySundryName1 = item.SundryName_1,
                              PaySundryName2 = item.SundryName_2,
                              PaySundryName3 = item.SundryName_3,
                              AccountCode = item.AccountCode,
                              IsAccommodation = item.IsAccommodation == true ? "<input type='checkbox' class='checkbox' checked='true' disabled />" : "<input type='checkbox' class='checkbox' disabled/>",
                              IsFurniture = item.IsFurniture == true ? "<input type='checkbox' class='checkbox' checked='true' disabled />" : "<input type='checkbox' class='checkbox' disabled/>"
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEditPaySundryType(int id)
        {
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            PaySundryType paySundryType = new PaySundryType();
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            // ObjSelectedList.Add(new SelectListItem { Text = "Select Account Code", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.selectedValue.ToString() });
            }
            ViewBag.lstAccountCode = ObjSelectedList;

            if (id != -1)
            {
                paySundryType = objHR_PaySundriesDB.GetPaySundryTypeID(id);
            }
            else
            {
                paySundryType.PaySundryID = -1;
            }

            return View(paySundryType);
        }

        [HttpPost]
        public ActionResult AddEditPaySundryType(PaySundryType objPaySundryType)
        {
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            OperationDetails op = new OperationDetails();
            if (objPaySundryType.PaySundryID == -1)
            {
                op = objHR_PaySundriesDB.UpdatePaySundrytypeDetails(objPaySundryType, 1);
                if (op.Success)
                {
                    op.Message = "Pay sundry type inserted successfully";
                }
                else
                {
                    op.Message = "Error while inserting Pay sundry type";
                }
            }
            else
            {
                op = objHR_PaySundriesDB.UpdatePaySundrytypeDetails(objPaySundryType, 2);
                if (op.Success)
                {
                    op.Message = "Pay sundry type updated successfully";
                }
                else
                {
                    op.Message = "Error while updating pay sundry type ";
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePaySundriesType(int id)
        {
            OperationDetails oDetails = new OperationDetails();
            PaySundryType objPaySundryType = new PaySundryType();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();
            objPaySundryType.PaySundryID = id;
            oDetails = objHR_PaySundriesDB.UpdatePaySundrytypeDetails(objPaySundryType, 3);
            if (oDetails.Success)
            {
                oDetails.Message = "Pay sundry type deleted successfully";
            }
            else
            {
                oDetails.Message = "Error while deleting pay sundry type";
            }
            return Json(oDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckIfAlreadySundrySelected(int type, int SundryId)
        {
            OperationDetails oDetails = new OperationDetails();
            HR_PaySundriesDB objHR_PaySundriesDB = new HR_PaySundriesDB();

            oDetails = objHR_PaySundriesDB.CheckIfAlreadySundrySelected(type, SundryId);

            return Json(oDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowShortLeaveSettings()
        {
            ShortLeaveRequestDB shortLeaveDB = new ShortLeaveRequestDB();
            ViewBag.ApproverTypeList = new SelectList(shortLeaveDB.GetShortLeaveApproverMasterDetails(), "ShortLeaveApproverMasterId", "ApproverType");
            ViewBag.EmployeeIdsToNotifyList = new SelectList(new UserGroupDB().GetAllEmployees(), "EmployeeID", "FullName");
            return PartialView("_ShortLeaveSettings", shortLeaveDB.GetShortLeaveSettings());
        }

        [HttpPost]
        public ActionResult AddEditShortLeaveSettings(ShortLeaveSettingsModel shortLeaveSettingsModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            try
            {
                ShortLeaveRequestDB shortLeaveDB = new ShortLeaveRequestDB();
                objOperationDetails = shortLeaveDB.AddEditShortLeaveSettings(shortLeaveSettingsModel);
            }
            catch (Exception ex)
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Error : while inserting short leave setting.";
                objOperationDetails.CssClass = "error";
            }
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsuranceCategoryAmounts()
        {

            DBHelper objDBHelper = new DBHelper();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var categoryAmounts = new InsuranceDB().GetInsuranceCategoryAmountPaging();
            var vList = new object();

            vList = new
            {
                aaData = (from item in categoryAmounts
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.CatAmountID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewGeneralControl(" + item.CatAmountID.ToString() + ")' title='View' ><i class='fa fa-eye'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteGeneralControl(" + item.CatAmountID.ToString() + ")' title='Edit' ><i class='fa fa-times'></i> </a>",
                              CatAmountID = item.CatAmountID,
                              HRInsuranceTypeID = item.HRInsuranceTypeID,
                              ACYearID = item.ACYearID,
                              InsuranceTypeName = item.InsuranceTypeName,
                              AcademicYear = item.AcademicYear,
                              Amount = item.Amount,
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMultipleInsuranceAmount(int id)
        {
            InsuranceDB insuranceDB = new InsuranceDB();
            ViewBag.InsuranceType = new SelectList(insuranceDB.GetInsuranceTypeList(), "id", "text");
            ViewBag.AcademicYear = new SelectList(new AcademicYearDB().GetAllAcademicYear(), "AcademicYearId", "Duration");
            InsuranceCategoryAmount insuranceCategoryAmount = null;
            if (id > 0)
            {
                insuranceCategoryAmount = insuranceDB.GetInsuranceCategoryAmountById(id);
            }
            return View(insuranceCategoryAmount);
        }

        public ActionResult ViewMultipleInsuranceAmount(int id)
        {
            InsuranceDB insuranceDB = new InsuranceDB();
            InsuranceCategoryAmount insuranceCategoryAmount = null;
            insuranceCategoryAmount = insuranceDB.GetInsuranceCategoryAmountById(id);
            return View(insuranceCategoryAmount);
        }

        public ActionResult DeleteInsuranceCategoryAmount(int id)
        {
            var result = new InsuranceDB().DeleteInsuranceCategoryAmount(id);

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CheckInsuranceCategoryAmountExists(int insuranceTypeId, int academicYearId, decimal amount)
        {
            bool IscategoryAmountExists;
            IscategoryAmountExists = new InsuranceDB().CheckInsuranceCategoryAmountExists(insuranceTypeId, academicYearId, amount);
            return Json(new { IscategoryAmountExists = IscategoryAmountExists }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertInsuranceCategoryAmount(List<InsuranceCategoryAmount> insuranceAmountList)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            return Json(new InsuranceDB().InsertInsuranceCategoryAmount(insuranceAmountList, objUserContextViewModel.UserId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditDocumentReminder()
        {
            DBHelper objDBHelper = new DBHelper();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var documentReminder = new GeneralControlDB().GetDocumentReminder();
            ViewBag.DailyReminderMsg = "Every day";
            ViewBag.WeeklyReminderMsg = string.Format("Every week on {0} ({1})", DateTime.Now.ToString(HRMSDateFormat), DateTime.Now.DayOfWeek.ToString());
            ViewBag.MonthlyReminderMsg = string.Format("Every month on {0} ({1})", DateTime.Now.ToString(HRMSDateFormat), DateTime.Now.DayOfWeek.ToString());
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            ViewBag.SelectedEmployeeList = !string.IsNullOrWhiteSpace(documentReminder.RecieverEmployeeIDs) ? documentReminder.RecieverEmployeeIDs.Split(',').Select(int.Parse).ToList() : new List<int>();
            return View(documentReminder);
        }

        [HttpPost]
        public ActionResult AddEditDocumentReminder(DocumentRemindersModel documentRemindersModel)
        {
            documentRemindersModel.RecieverEmployeeIDs = Request["EmployeeList"];
            OperationDetails objOperationDetails = new OperationDetails();
            if (!documentRemindersModel.AboutToExpire && !documentRemindersModel.AlreadyExpire)
            {
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Please select Reminder Type";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else if (string.IsNullOrWhiteSpace(documentRemindersModel.RecieverEmployeeIDs))
            {
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Please select alteast one employee";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            documentRemindersModel.MonthlyDay = 0;
            documentRemindersModel.WeeklyDayName = "";
            if (documentRemindersModel.FrequencyType == "Monthly")
                documentRemindersModel.MonthlyDay = DateTime.Now.Day;
            else if (documentRemindersModel.FrequencyType == "Weekly")
                documentRemindersModel.WeeklyDayName = DateTime.Now.DayOfWeek.ToString();

            objOperationDetails = new GeneralControlDB().UpdateDocumentReminder(documentRemindersModel, objUserContextViewModel.UserId);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditAttendanceReminder()
        {
            DBHelper objDBHelper = new DBHelper();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            var documentReminder = new GeneralControlDB().GetAttendanceReminder();
            ViewBag.EmployeeList = new SelectList(new EmployeeDB().GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1), "EmployeeId", "FullName");
            ViewBag.AbsentStatusList = new SelectList(new CommonDB().AbsentStatus.Where(a => a.Key == 2 || a.Key == 3 || a.Key == 4), "Key", "Value");
            ViewBag.SelectedAbsentStatusList = !string.IsNullOrWhiteSpace(documentReminder.AttendanceStatusIDs) ? documentReminder.AttendanceStatusIDs.Split(',').Select(int.Parse).ToList() : new List<int>();
            //AbsentStatus
            ViewBag.SelectedEmployeeList = !string.IsNullOrWhiteSpace(documentReminder.RecieverEmployeeIDs) ? documentReminder.RecieverEmployeeIDs.Split(',').Select(int.Parse).ToList() : new List<int>();
            return View(documentReminder);
        }

        [HttpPost]
        public ActionResult AddEditAttendanceReminder(AttendanceRemindersModel attendanceRemindersModel)
        {
            attendanceRemindersModel.RecieverEmployeeIDs = Request["AttEmployeeList"];
            attendanceRemindersModel.AttendanceStatusIDs = Request["AttendanceStatusList"];
            OperationDetails objOperationDetails = new OperationDetails();
            if (string.IsNullOrWhiteSpace(attendanceRemindersModel.AttendanceStatusIDs))
            {
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Please select alteast one Attendance Status";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else if (string.IsNullOrWhiteSpace(attendanceRemindersModel.RecieverEmployeeIDs))
            {
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Please select alteast one employee";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            objOperationDetails = new GeneralControlDB().UpdateAttendanceReminder(attendanceRemindersModel, objUserContextViewModel.UserId);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetTrainingCompetencies()
        {
            var trainingCompetencies = new List<TrainingCompetencyDetailModel>();
            HRQualificationDB objHRQualificationDB = new HRQualificationDB();
            trainingCompetencies = new GeneralControlDB().GetTrainingCompetencies().Where(tc => tc.TrainingID != 0).ToList();
            var vList = new object();
            vList = new
            {
                aaData = (from item in trainingCompetencies
                          select new
                          {
                              //Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='AddEditGeneralControl(" + item.TrainingID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteTrainingCompetencies(" + item.TrainingID.ToString() + "," + item.CompetencyID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              Action = "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteTrainingCompetencies(" + item.TrainingID.ToString() + "," + item.CompetencyID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              TrainingName = item.TrainingName,
                              CompetencyTitle = item.CompetencyTitle
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSelectedTrainingCompetencies(int trainingId)
        {
            var trainingCompetencies = new SelectList(new DevelopmentModelDB().GetAllActiveCompetencies(trainingId), "CompentencyID", "Competencytitle_1");
            return Json(trainingCompetencies, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditTrainingCompetency(int id)
        {
            TrainingCompetencyDetailModel trainingCompetencyDetailModel = new TrainingCompetencyDetailModel();
            if (id != 0)
            {
                trainingCompetencyDetailModel = new GeneralControlDB().GetTrainingCompetencyById(id);
            }
            Session["AddMode"] = id == 0 ? true : false;
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            DevelopmentModelDB DevelopmentModelDB = new DevelopmentModelDB();
            List<HRMS.Entities.TrainingCompetenciesModel> objTrainingCompetenciesModelList = DevelopmentModelDB.GetAllActiveTrainingCompetencies(objUserContextViewModel.CompanyId).Where(tc => tc.TrainingID != 0).ToList();
            ViewBag.TrainingCompetencies = new SelectList(objTrainingCompetenciesModelList, "TrainingID", "TrainingName_1");
            ViewBag.Competencies = new SelectList(DevelopmentModelDB.GetAllActiveCompetencies(null), "CompentencyID", "Competencytitle_1");
            ViewBag.SelectedCompetencyList = !string.IsNullOrWhiteSpace(trainingCompetencyDetailModel.CompetencyIDs) ? trainingCompetencyDetailModel.CompetencyIDs.Split(',').Select(int.Parse).ToList() : new List<int>();
            return View(trainingCompetencyDetailModel);
        }

        [HttpPost]
        public ActionResult AddEditTrainingCompetency(TrainingCompetencyDetailModel trainingCompetencyModel)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            trainingCompetencyModel.CompetencyIDs = Request["CompetenciesList"];
            if (string.IsNullOrWhiteSpace(trainingCompetencyModel.CompetencyIDs))
            {
                objOperationDetails.CssClass = "error";
                objOperationDetails.Message = "Please select alteast one competency";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            if (Session["AddMode"] != null && Convert.ToBoolean(Session["AddMode"].ToString()))
                objOperationDetails = new GeneralControlDB().AddEditTrainingCompetency(trainingCompetencyModel, 1);
            else
                objOperationDetails = new GeneralControlDB().AddEditTrainingCompetency(trainingCompetencyModel, 2);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteTrainingCompetency(int trainingId, int competencyId)
        {
            OperationDetails objOperationDetails = new OperationDetails();
            TrainingCompetencyDetailModel trainingCompetencyModel = new TrainingCompetencyDetailModel();
            trainingCompetencyModel.TrainingID = trainingId;
            trainingCompetencyModel.CompetencyIDs = competencyId.ToString();
            objOperationDetails = new GeneralControlDB().AddEditTrainingCompetency(trainingCompetencyModel, 3);

            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

        }
    }
}