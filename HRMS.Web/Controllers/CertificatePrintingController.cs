﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HRMS.DataAccess;
using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using static System.Collections.Specialized.BitVector32;
using FontSize = DocumentFormat.OpenXml.Wordprocessing.FontSize;
using Paragraph = DocumentFormat.OpenXml.Wordprocessing.Paragraph;
using ParagraphProperties = DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties;
using Run = DocumentFormat.OpenXml.Wordprocessing.Run;
using RunProperties = DocumentFormat.OpenXml.Wordprocessing.RunProperties;
using Style = DocumentFormat.OpenXml.Wordprocessing.Style;
using Table = DocumentFormat.OpenXml.Wordprocessing.Table;
using TableCell = DocumentFormat.OpenXml.Wordprocessing.TableCell;
using TableCellProperties = DocumentFormat.OpenXml.Wordprocessing.TableCellProperties;
using TableProperties = DocumentFormat.OpenXml.Wordprocessing.TableProperties;
using TableRow = DocumentFormat.OpenXml.Wordprocessing.TableRow;
using TableStyle = DocumentFormat.OpenXml.Wordprocessing.TableStyle;
using Text = DocumentFormat.OpenXml.Wordprocessing.Text;

namespace HRMS.Web.Controllers
{
    public class CertificatePrintingController : BaseController
    {
        // GET: CertificatePrinting
        public ActionResult Index()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ViewBag.Company = GetCompanySelectList();
            ViewBag.Employee = new SelectList(new EmployeeDB().GetEmployeeByActive(true, objUserContextViewModel.UserId).Where(x => x.employeeDetailsModel.usertypeid == 1), "EmployeeId", "FirstName");
            return View();
        }

        public ActionResult GetCertificatePrintingScreenData(int? organizationId,int? employeeId)
        {
            CertificatePrintingDB certificatePrintingDB = new CertificatePrintingDB();
            List<CertificatePrintingModel> certificatePrintingList = certificatePrintingDB.GetCertificatePrintingScreenData(organizationId, employeeId);
            var vList = new object();
            vList = new
            {
                aaData = (from item in certificatePrintingList
                          select new
                          {
                              ComapanyName = item.ComapanyName,
                              RequestID = item.RequestID,
                              RequestDate = item.RequestDate,
                              EmployeeAlternativeID = item.EmployeeAlternativeID,
                              EmployeeName = item.EmployeeName,
                              CertificateAddress = item.CertificateAddress,
                              DocumentType = item.DocumentType,
                              CertificateType = item.CertificateType,
                              FormProcessID = item.FormProcessID,
                              ReqStatusID = item.ReqStatusID,
                              Action = item.TemplateId != 0 ? "<a href='javascript:void(0)' onclick=printCertificateTemplate(" + item.FormId + "," + item.TemplateId + "," + item.ID + ") id='printCertificate' title='Print Certificate' style='cursor: pointer';>Print</a>" : ""
                          }).ToArray()
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintCertificateTemplate(int formId, int templateId, int id)
        {
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            bool success = true;
            string fileName = string.Empty;
            string sourceFile = string.Empty;
            string destinationFile = "";
            string downloadFileName = string.Empty;
            CertificatePrintingDB certificatePrintingDB = new CertificatePrintingDB();
            PrintCertificateEmployeeInfoModel employeeInfoModel = new PrintCertificateEmployeeInfoModel();
            try
            {
                CommonHelper.CommonHelper.CheckAndCreatDirectory("/Downloads/PrintCertificateTemplate/");
                string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();

                if (formId == 11)
                {
                    if (templateId == (int)NOCCertificateTemplate.NOCPersonalStandard)
                    {
                        sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PrintCertificateTemplate/NOC_Personal_Standard Template.docx");
                        fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PrintCertificateTemplate/NOC-Personal-" + DateTime.Now.ToString("ddMMyyyyhhmm"));
                        employeeInfoModel = certificatePrintingDB.GetNOCPersonalEmployeeInfoData(id);
                        downloadFileName = "NOC-Personal-" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".docx";
                        keyValues = GetKeyValuesOfNOCPersonalTemplate(employeeInfoModel);
                    }

                    else if (templateId == (int)NOCCertificateTemplate.NOCBusinessStandard)
                    {
                        sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PrintCertificateTemplate/NOC_Business_Standard Template.docx");
                        fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PrintCertificateTemplate/NOC-Business-" + DateTime.Now.ToString("ddMMyyyyhhmm"));
                        employeeInfoModel = certificatePrintingDB.GetNOCBusinessEmployeeInfoData(id);
                        downloadFileName = "NOC-Business-" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".docx";
                        keyValues = GetKeyValuesOfNOCBusinessTemplate(employeeInfoModel);
                    }
                }
                else if (formId == 19 || formId == 20 || formId == 21 || formId == 22)
                {
                    if (templateId == (int)SalaryCertificateTemplate.SalaryTransferLetter)
                    {
                        sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PrintCertificateTemplate/Salary Transfer Letter_Standard Template.docx");
                        fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PrintCertificateTemplate/SalaryTransferLetter-" + DateTime.Now.ToString("ddMMyyyyhhmm"));
                        employeeInfoModel = certificatePrintingDB.GetSalaryTransferLetterData(id);
                        downloadFileName = "SalaryTransferLetter-" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".docx";
                        keyValues = GetKeyValuesOfSalaryTransferLetter(employeeInfoModel);
                    }

                    else if (templateId == (int)SalaryCertificateTemplate.SalaryCertificateStandard)
                    {
                        sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PrintCertificateTemplate/Salary Certificate_Standard Template.docx");
                        fileName = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/PrintCertificateTemplate/SalaryCertificateStandard-" + DateTime.Now.ToString("ddMMyyyyhhmm"));
                        employeeInfoModel = certificatePrintingDB.GetSalaryCertificateStandardData(id);
                        downloadFileName = "SalaryCertificateStandard-" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".docx";
                        keyValues = GetKeyValuesOfSalaryCertificateStandard(employeeInfoModel);
                    }
                }

                destinationFile = fileName;
                // Create a copy of the template file and open the copy
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                System.IO.File.Copy(sourceFile, destinationFile);
                SearchAndReplace(destinationFile, keyValues);
                if (templateId == (int)SalaryCertificateTemplate.SalaryCertificateStandard)
                    AddSalaryInformation(destinationFile, employeeInfoModel, amountFormat);
            }
            catch (Exception ex)
            {
                success = false;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json(new { fileName = fileName, downloadFileName = downloadFileName, success = success }, JsonRequestBehavior.AllowGet);
        }

        #region NOC Personal Standard Template
        private Dictionary<string, string> GetKeyValuesOfNOCPersonalTemplate(PrintCertificateEmployeeInfoModel employeeInfoModel)
        {
            //create key value pair, key represents words to be replace and 
            //values represent values in document in place of keys.
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues.Add("OrgId", employeeInfoModel.OrganizationCode);
            keyValues.Add("TodayDate", employeeInfoModel.TodaysDate);
            keyValues.Add("EntityName", employeeInfoModel.EntityName);
            keyValues.Add("Title", employeeInfoModel.EmployeeTitle);
            keyValues.Add("FullName", employeeInfoModel.EmployeeName);
            keyValues.Add("NationalityName", employeeInfoModel.Nationality);
            keyValues.Add("PassportNo", employeeInfoModel.PassportNo);
            keyValues.Add("MOLDesignation", employeeInfoModel.MOLDesignation);
            keyValues.Add("HireDate", employeeInfoModel.HireDate);
            keyValues.Add("Element1", employeeInfoModel.Title2);
            keyValues.Add("UserLabel1", employeeInfoModel.Title1);
            keyValues.Add("TravelDate", employeeInfoModel.TravelDate);
            keyValues.Add("TotalSalary", employeeInfoModel.TotalSalary);
            keyValues.Add("Element2", employeeInfoModel.Title4);

            return keyValues;
        }
        #endregion

        #region NOC Business Standard Template
        private Dictionary<string, string> GetKeyValuesOfNOCBusinessTemplate(PrintCertificateEmployeeInfoModel employeeInfoModel)
        {
            //create key value pair, key represents words to be replace and 
            //values represent values in document in place of keys.
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues.Add("OrgId", employeeInfoModel.OrganizationCode);
            keyValues.Add("TodayDate", employeeInfoModel.TodaysDate);
            keyValues.Add("EntityName", employeeInfoModel.EntityName);
            keyValues.Add("Title", employeeInfoModel.EmployeeTitle);
            keyValues.Add("FullName", employeeInfoModel.EmployeeName);
            keyValues.Add("NationalityName", employeeInfoModel.Nationality);
            keyValues.Add("PassportNo", employeeInfoModel.PassportNo);
            keyValues.Add("MOLDesignation", employeeInfoModel.MOLDesignation);
            keyValues.Add("HireDate", employeeInfoModel.HireDate);
            keyValues.Add("Element1", employeeInfoModel.Title2);
            keyValues.Add("UserLabel1", employeeInfoModel.Title1);
            keyValues.Add("TravelDate", employeeInfoModel.TravelDate);
            keyValues.Add("TotalSalary", employeeInfoModel.TotalSalary);
            keyValues.Add("Element2", employeeInfoModel.Title4);

            return keyValues;
        }
        #endregion

        #region Salary Transfer LetterDailyAttendance
        private Dictionary<string, string> GetKeyValuesOfSalaryTransferLetter(PrintCertificateEmployeeInfoModel employeeInfoModel)
        {
            //create key value pair, key represents words to be replace and 
            //values represent values in document in place of keys.
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues.Add("OrgId", employeeInfoModel.OrganizationCode);
            keyValues.Add("TodayDate", employeeInfoModel.TodaysDate);
            keyValues.Add("BankName", employeeInfoModel.BankName);
            keyValues.Add("Title", employeeInfoModel.EmployeeTitle);
            keyValues.Add("FullName", employeeInfoModel.EmployeeName);
            keyValues.Add("LinkHRDesignation", employeeInfoModel.LinkHRDesignation);
            keyValues.Add("HireDate", employeeInfoModel.HireDate);
            keyValues.Add("EmploymentStatus", employeeInfoModel.EmploymentStatus);
            keyValues.Add("GrossSalary", employeeInfoModel.GrossSalary);
            keyValues.Add("SalaryInText", DataAccess.GeneralDB.CommonDB.DecimalToWord(Convert.ToDecimal(employeeInfoModel.GrossSalary)));
            keyValues.Add("Code", employeeInfoModel.IBAN);
            keyValues.Add("MOLBUName", employeeInfoModel.MOLBUName);
            keyValues.Add("Element1", employeeInfoModel.Title4);

            return keyValues;
        }
        #endregion

        #region Salary Certificate Standard Template
        private Dictionary<string, string> GetKeyValuesOfSalaryCertificateStandard(PrintCertificateEmployeeInfoModel employeeInfoModel)
        {
            //create key value pair, key represents words to be replace and 
            //values represent values in document in place of keys.
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues.Add("OrgId", employeeInfoModel.OrganizationCode);
            keyValues.Add("TodayDate", employeeInfoModel.TodaysDate);
            keyValues.Add("BankName", employeeInfoModel.BankName);
            keyValues.Add("Purpose", employeeInfoModel.Purpose);
            keyValues.Add("Title", employeeInfoModel.EmployeeTitle);
            keyValues.Add("FullName", employeeInfoModel.EmployeeName);
            keyValues.Add("NationalityName", employeeInfoModel.Nationality);
            keyValues.Add("PassportNo", employeeInfoModel.PassportNo);
            keyValues.Add("LinkHRDesignation", employeeInfoModel.LinkHRDesignation);
            keyValues.Add("HireDate", employeeInfoModel.HireDate);
            keyValues.Add("BUName", employeeInfoModel.MOLBUName);
            keyValues.Add("TotalSalary", employeeInfoModel.TotalSalary);

            return keyValues;
        }

        public void AddSalaryInformation(string document, PrintCertificateEmployeeInfoModel employeeInfoModel, string Amountformate)
        {
            int count = 0;
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                var doc = wordDoc.MainDocumentPart.Document;
                double amountTotal = 0;
                Table table = new Table();
                TableProperties props = CommonHelper.CommonHelper.GetTableProperties(0, 0, 0, 0, 0, 0, "7800", "center");
                table.AppendChild<TableProperties>(props);
                int[] widtharray = new int[] { 50, 25, 25 };
                foreach (var item in employeeInfoModel.SalryCertificateList)
                {
                    count = 0;
                    var tr = new TableRow();
                    for (int i = 0; i <= 2; i++)
                    {

                        var tc = new TableCell();
                        RunProperties properties = new RunProperties();
                        FontSize fontSize = new FontSize { Val = "23" };
                        RunFonts runFonts = new RunFonts() { Ascii = "Tahoma" };
                        properties.AppendChild(fontSize);
                        properties.AppendChild(runFonts);
                        TableCellProperties tableCellProperties = CommonHelper.CommonHelper.GetTableCellProperties(0, 0, 0, 0, 0, 0, widtharray[count++].ToString(), "FFFFFF");

                        tc.AppendChild<TableCellProperties>(tableCellProperties);
                        TableCellRightMargin cellRightMargin = new TableCellRightMargin() { Type = TableWidthValues.Dxa, Width = 300 };
                        TableCellLeftMargin cellLeftMargin = new TableCellLeftMargin() { Type = TableWidthValues.Dxa, Width = 300 };
                        Paragraph paraInner = new Paragraph();
                        Run innerRun3 = new Run();
                        innerRun3.AppendChild(cellRightMargin);
                        innerRun3.AppendChild(cellLeftMargin);
                        Text innerText3 = new Text();
                        switch (i)
                        {
                            case 0:
                                innerText3.Text = item.SalaryType.ToString();
                                break;
                            case 1:
                                innerText3.Text = item.AmountLabel.ToString();
                                break;
                            case 2:
                                innerText3.Text = item.Amount.ToString(Amountformate);
                                amountTotal = amountTotal + item.Amount;
                                break;
                        }
                        innerRun3.AppendChild(properties);
                        if (i == 0)
                        { paraInner.AppendChild(CommonHelper.CommonHelper.GetParagraphProp("left")); }
                        else
                        { paraInner.AppendChild(CommonHelper.CommonHelper.GetParagraphProp("right")); }
                        innerRun3.AppendChild(innerText3);
                        paraInner.AppendChild(innerRun3);
                        tc.AppendChild(paraInner);
                        tr.Append(tc);
                    }
                    table.Append(tr);
                }

                Paragraph p = new Paragraph();
                Paragraph paragraph4 = (Paragraph)doc.MainDocumentPart.Document.Body.Descendants<Paragraph>().ElementAt(17);
                paragraph4.Append(p);
                paragraph4.Append(table);

                doc.Save();
                mainPart.GetStream(FileMode.Open);
            }
        }
        #endregion

        // To search and replace content in a document part.
        public void SearchAndReplace(string document, Dictionary<string, string> dict)
        {
            try
            {
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
                {
                    MainDocumentPart mainPart = wordDoc.MainDocumentPart;

                    string docText = null;

                    using (StreamReader sr = new StreamReader(mainPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    foreach (KeyValuePair<string, string> item in dict)
                    {
                        Regex regexText = new Regex(item.Key);
                        docText = regexText.Replace(docText, item.Value);
                    }

                    using (StreamWriter sw = new StreamWriter(
                              mainPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }

                    mainPart.GetStream(FileMode.Open);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        public ActionResult DownloadPrintCertificate(string fileName, string downloadFileName)
        {
            string userType = string.Empty;
            Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + downloadFileName);
            Response.TransmitFile(fileName);
            Response.End();
            return new EmptyResult();
        }

        public JsonResult LoadEmployeesbasedOnOrganization(int companyId)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objempDB = new EmployeeDB();
            var employeeList = objempDB.GetEmployeeByActive(true, objUserContextViewModel.UserId).Where(x => x.employeeDetailsModel.usertypeid == 1 && x.employeeDetailsModel.CompanyID == companyId);
            return new JsonResult()
            {
                Data = employeeList.ToList(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

    }
}