﻿using HRMS.DataAccess.FormsDB;
using HRMS.Entities.Forms;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class LeaveRejoingListController : BaseController
    {
        // GET: LeaveRejoingList
        public ActionResult Index()
        {
            LeaveRejoingDB objLeaveRejoingDB = new LeaveRejoingDB();
            LeaveRejoiningModel objLeaveRejoiningModel = new LeaveRejoiningModel();
            UserContextViewModel objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objLeaveRejoiningModel.LeaveRequestDetailModelList = objLeaveRejoingDB.GetLeavesReJoiningList(objUserContextViewModel.UserId);
            return View(objLeaveRejoiningModel);
        }

        public ActionResult SetLeaveIDSesssion(int id)
        {
            Session["LeaveID"] = id;
            return Json(new { id = id }, JsonRequestBehavior.AllowGet);

        }
    }
}