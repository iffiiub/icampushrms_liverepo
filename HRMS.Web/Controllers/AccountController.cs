﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using HRMS.Web.Models;
using System.Web.Security;
using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Web.AuthenticationExt;
using System.Web.Script.Serialization;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities.General;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using HRMS.Web.CommonHelper;

namespace HRMS.Web.Controllers
{

    public class AccountController : Controller
    {


        #region User profile management
        // GET: /Account/Index
        public ActionResult Index()
        {
            return RedirectToAction("UserLogin");
        }

        /// <summary>
        /// Action for UserLogin: GET
        /// </summary>
        /// <param name="isLoggedOut"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult UserLogin(string returnUrl, bool isLoggedOut = false)
        {
            //To redirect the user           
            ViewBag.ReturnUrl = returnUrl != null ? returnUrl : Convert.ToString(TempData["ReturnUrl"]);
            if (isLoggedOut == true)
            {
                //string showMessage = HRMS.Web.CommonHelper.CommonHelper.LogoutMsg;
                //ShowStatusMessage(showMessage);
            }
            else
            {
                //GetBrowserInfo();
            }
            if (!CommonHelper.CommonHelper.GetHrSettings().IsMicrosoftAuthEnabled)
            {
                return View();
            }
            else
            {
                return Redirect(Url.Action("SignIn", "Account", null, Request.Url.Scheme));
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult AjaxSessionExpire()
        {
            //ViewBag.ReturnUrl =  Convert.ToString(TempData["ReturnUrl"]);
            return View();
        }

        /// <summary>
        /// Action for Admin Login: Post
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult UserLogin(HRMS.Entities.ViewModel.LoginViewModel loginViewModel, string returnUrl)
        {
            UserDB userDB = new UserDB();
            UserLoggerDB UserLoggerDB = new UserLoggerDB();
            //if Azure verified password will not be there
            if ((loginViewModel.Username != null && loginViewModel.Password != null) || (loginViewModel.Username != null && loginViewModel.IsMicrosoftAuthVerified))
            {
                //loginViewModel.Password = HRMS.Web.CommonHelper.CommonHelper.EncryptPassword(loginViewModel.Password);
                UserModel userModel = new UserModel();
                userModel = userDB.AuthenticateUser(loginViewModel);

                if (userModel != null)
                {
                    GeneralControlDB generalControlDB = new GeneralControlDB();
                    Session["App_LargeLogo"] = generalControlDB.GetApplicationLogo(1);//"../../Content/images/Logo/logo.png";
                    Session["App_SmallLogo"] = generalControlDB.GetApplicationLogo(2);//"http://localhost:56214/Content/images/Logo/logosmall.png";

                    if (userModel.IsActive && userModel.IsLoginSuccess)
                    {
                        SetAuthentication(userModel, false);
                        FormsAuthentication.SetAuthCookie(userModel.Username, true);
                        UserLogModel userLog = new UserLogModel()
                        {
                            userName = userModel.Username,
                            browserName = HttpContext.Request.Browser.Type,
                            browserVersion = DataAccess.GeneralDB.CommonDB.FindBroserName(HttpContext.Request.Browser.Version),
                            userId = ((HRMS.Entities.ViewModel.UserContextViewModel)Session["userContext"]).UserId,
                            Date = DateTime.Now.ToString(),
                            description = "User " + userModel.Username + " Log In On Date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"),
                            moduleName = "Account",
                            oprationType = "Log In",
                        };
                        EmployeeDB objEmployeeDB = new EmployeeDB();
                        Employee emp = objEmployeeDB.GetEmployeeInfoByUserID(userModel.UserId);
                        Session["EmpInfo"] = emp;
                        var dataContain = objEmployeeDB.GetUserImage(emp.EmployeeId);
                        if (dataContain.Count() > 0)
                            Session["ProfileImage"] = dataContain;
                        else
                        {
                            Session["ProfileImage"] = CommonHelper.CommonHelper.GetBlankImage();
                        }
                        UserLoggerDB.AddUserLog(userLog);
                        string[] permissoins = userModel.Permission.Split(',');
                        if (string.IsNullOrWhiteSpace(userModel.Permission) || userModel.Permission == ",")
                        {
                            ViewBag.PermissionFailed = true;
                            if (loginViewModel.IsMicrosoftAuthVerified)
                            {
                                ShowStatusMessage(CommonHelper.CommonHelper.NoPermissionsMsg);
                                return RedirectToAction("ExternalLoginFailure");
                            }
                            else
                            {
                                return View(loginViewModel);
                            }

                        }
                        else
                        {
                            return RedirectToLocal(returnUrl);
                        }

                    }
                    else
                    {
                        if (!userModel.IsLoginSuccess)
                        {

                            ShowStatusMessage(string.IsNullOrEmpty(userModel.LoginMessage) ? "Invalid Credentials!.Please contact Administrator" : userModel.LoginMessage);
                            if (loginViewModel.IsMicrosoftAuthVerified)
                            {
                                return RedirectToAction("ExternalLoginFailure");
                            }
                            else
                            {
                                return View(loginViewModel);
                            }
                        }
                    }
                }
                else
                {
                    ShowStatusMessage(HRMS.Web.CommonHelper.CommonHelper.InvalidCredentialMsg);
                    return View(loginViewModel);
                }
            }
            else
            {
                return View(loginViewModel);
            }
            return View();

            #region OldCode

            //if (loginViewModel.Username != null && loginViewModel.Password != null)
            //{
            //    //loginViewModel.Password = HRMS.Web.CommonHelper.CommonHelper.EncryptPassword(loginViewModel.Password);
            //    UserModel userModel = new UserModel();
            //    userModel = userDB.AuthenticateUser(loginViewModel);

            //    if (userModel != null)
            //    {
            //        GeneralControlDB generalControlDB = new GeneralControlDB();
            //        Session["App_LargeLogo"] = generalControlDB.GetApplicationLogo(1);//"../../Content/images/Logo/logo.png";
            //        Session["App_SmallLogo"] = generalControlDB.GetApplicationLogo(2);//"http://localhost:56214/Content/images/Logo/logosmall.png";

            //        if (userModel.IsActive)
            //        {
            //            SetAuthentication(userModel, false);
            //            FormsAuthentication.SetAuthCookie(userModel.Username, true);
            //            UserLogModel userLog = new UserLogModel()
            //            {
            //                userName = userModel.Username,
            //                browserName = HttpContext.Request.Browser.Type,
            //                browserVersion = DataAccess.GeneralDB.CommonDB.FindBroserName(HttpContext.Request.Browser.Version),
            //                userId = ((HRMS.Entities.ViewModel.UserContextViewModel)Session["userContext"]).UserId,
            //                Date = DateTime.Now.ToString(),
            //                description = "User " + userModel.Username + " Log In On Date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"),
            //                moduleName = "Account",
            //                oprationType = "Log In",
            //            };
            //            EmployeeDB objEmployeeDB = new EmployeeDB();
            //            Employee emp = objEmployeeDB.GetEmployeeInfoByUserID(userModel.UserId);
            //            Session["EmpInfo"] = emp;                      
            //            var dataContain = objEmployeeDB.GetUserImage(emp.EmployeeId);
            //            if (dataContain.Count() > 0)
            //                Session["ProfileImage"] = dataContain;
            //            else
            //            {
            //                Session["ProfileImage"] = CommonHelper.CommonHelper.GetBlankImage();
            //            }
            //            UserLoggerDB.AddUserLog(userLog);
            //            string[] permissoins = userModel.Permission.Split(',');
            //            if (userModel.Permission == ",")
            //            {
            //                ViewBag.PermissionFailed = true;
            //                return View(loginViewModel);
            //            }
            //            else
            //            {
            //                return RedirectToLocal(returnUrl);
            //            }

            //        }
            //    }
            //    else
            //    {
            //        string showMessage = HRMS.Web.CommonHelper.CommonHelper.InvalidCredentialMsg;
            //        ShowStatusMessage(showMessage);
            //        return View(loginViewModel);
            //    }
            //}
            //else
            //{
            //    return View(loginViewModel);
            //}
            //return View();
            #endregion
        }
        [HttpGet]
        //[AllowAnonymous]
        public async Task<ActionResult> SignIn()
        {
            AuthenticationContext authContext = new AuthenticationContext(SettingsHelper.AzureLoginUrl);

            // The url in HRMS that Azure should redirect to after successful signin
            string redirectUri = Url.Action("Authorize", "Account", null, Request.Url.Scheme);
            // Generate the parameterized URL for Azure signin
            Uri authUri = await authContext.GetAuthorizationRequestUrlAsync(SettingsHelper.O365UnifiedResource, SettingsHelper.ClientId,
                new Uri(redirectUri), UserIdentifier.AnyUser, null);
            // Redirect the browser to the Azure signin page
            return Redirect(authUri.ToString());
        }
        public async Task<ActionResult> Authorize()
        {
            // Get the 'code' parameter from the Azure redirect
            string authCode = Request.Params["code"];
            if (string.IsNullOrEmpty(authCode))
            {
                //ViewBag.PermissionFailed = true;
                ShowStatusMessage("Unable to login!.Please contact your administrator");
                return RedirectToAction("ExternalLoginFailure");
            }
            HRMS.Entities.ViewModel.LoginViewModel loginViewModel = new HRMS.Entities.ViewModel.LoginViewModel();
            AuthenticationContext authContext = new AuthenticationContext(SettingsHelper.AzureLoginUrl);

            // The same url we specified in the auth code request
            string redirectUri = Url.Action("Authorize", "Account", null, Request.Url.Scheme);

            // Use client ID and secret to establish app identity
            ClientCredential credential = new ClientCredential(SettingsHelper.ClientId, SettingsHelper.ClientSecret);

            try
            {
                // Get the token
                var authResult = await authContext.AcquireTokenByAuthorizationCodeAsync(
                    authCode, new Uri(redirectUri), credential, SettingsHelper.O365UnifiedResource);
                //By default DisplayableId  will be the email id            
                if (!string.IsNullOrEmpty(authResult.UserInfo.DisplayableId))
                {
                    //Verified in Azure
                    loginViewModel.IsMicrosoftAuthVerified = true;
                    loginViewModel.Username = authResult.UserInfo.DisplayableId;
                    //Just set # to keep some text in Password, If Azure login password is not checked
                    loginViewModel.Password = "#";
                }

                return UserLogin(loginViewModel, string.Empty);
            }
            catch (AdalException ex)
            {
                ShowStatusMessage(string.Format("ERROR accessing Microsoft Account: {0}", ex.Message));
                return View();

            }
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult AdminLogin(string returnUrl, bool isLoggedOut = false)
        {
            //To redirect the user           
            ViewBag.ReturnUrl = returnUrl != null ? returnUrl : Convert.ToString(TempData["ReturnUrl"]);
            return View("UserLogin");
        }


        /// <summary>
        /// Logout Action 
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            Entities.ViewModel.UserContextViewModel userModel = ((HRMS.Entities.ViewModel.UserContextViewModel)Session["userContext"]);
            //*** Naresh 2020-03-01 If session already expired then we can not get value from session, need to handle null
            if (userModel != null)
            {
                UserLoggerDB UserLoggerDB = new UserLoggerDB();
                UserLogModel userLog = new UserLogModel()
                {
                    userName = userModel.Name,
                    browserName = DataAccess.GeneralDB.CommonDB.FindBroserName(HttpContext.Request.Browser.Type),
                    browserVersion = HttpContext.Request.Browser.Version,
                    userId = userModel.UserId,
                    Date = DateTime.Now.ToString(),
                    description = "User " + userModel.Name + " Log Out On Date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"),
                    moduleName = "Account",
                    oprationType = "Log Out",
                };
                UserLoggerDB.AddUserLog(userLog);
            }
            FormsAuthentication.SignOut();
            Session.Abandon();
            CommonDB commonDB = new CommonDB();
            if (CommonHelper.CommonHelper.GetHrSettings().IsMicrosoftAuthEnabled)
            {
                Uri uri = new Uri(SettingsHelper.AzureLogoutUrl);
                return Redirect(uri.ToString());
            }
            return RedirectToAction("UserLogin", "Account", new { @area = "", isLoggedOut = true });
        }

        #endregion

        #region setting log in user authentication

        /// <summary>
        /// Method for setting Authentication for user
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="RememberMe"></param>
        private void SetAuthentication(UserModel userModel, bool RememberMe)
        {
            HRMS.Entities.ViewModel.UserContextViewModel userContext = new HRMS.Entities.ViewModel.UserContextViewModel();
            userContext.Name = userModel.Username;
            userContext.Role = ((HRMS.Web.CommonHelper.UserRole)userModel.UserRoleId).ToString();
            userContext.UserRolls = userModel.UserRoles;
            userContext.UserRoleId = userModel.UserRoleId;
            userContext.UserId = userModel.UserId;
            userContext.Password = userModel.Password;
            userContext.CompanyId = userModel.CompanyId;
            userContext.ViewPagesSubCategoryIDs = userModel.ViewPagesSubCategoryIDs;
            userContext.AddPagesSubCategoryIDs = userModel.AddPagesSubCategoryIDs;
            userContext.UpdatePagesSubCategoryIDs = userModel.UpdatePagesSubCategoryIDs;
            userContext.DeletePagesSubCategoryIDs = userModel.DeletePagesSubCategoryIDs;
            userContext.OtherPermissionIDs = userModel.OtherPermissionIDs;
            char[] seperator = new char[] { ',' };
            string[] PermissionArray = userModel.Permission.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            List<int> PermissionList = new List<int>();
            foreach (var item in PermissionArray)
            {
                PermissionList.Add(Convert.ToInt32(item));
            }
            userContext.Permisssion = PermissionList;
            string[] ReadonlyPagesArray = userModel.ViewPagesSubCategoryIDs.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            List<int> ReadonlyPagesList = new List<int>();
            foreach (var item in ReadonlyPagesArray)
            {
                ReadonlyPagesList.Add(Convert.ToInt32(item));
            }
            userContext.ReadonlyPages = ReadonlyPagesList;
            Session["userContext"] = userContext;
        }

        public void OnAuthorization()
        {
            HRMS.Entities.ViewModel.UserContextViewModel userContext = (HRMS.Entities.ViewModel.UserContextViewModel)Session["userContext"];
            if (userContext != null)
            {

            }

            //if (CurrentUser != null && CurrentUser.Identity != null && CurrentUser.Identity.IsAuthenticated)
            //{
            //    HttpCookie authCookie =
            //      filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            //    if (authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
            //    {

            //        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            //        JavaScriptSerializer serializer = new JavaScriptSerializer();
            //        AuthenticationSerialize serialiseAuth = serializer.Deserialize<AuthenticationSerialize>(authTicket.UserData);
            //        Authentication auth = new Authentication(authTicket.Name);
            //        if (authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
            //            auth.UserContext = serialiseAuth.UserContext;
            //        if (!string.IsNullOrEmpty(Permissions))
            //        {
            //            if (serialiseAuth != null)
            //            {
            //                if (!serialiseAuth.UserContext.Permisssion.Contains(Permissions))
            //                {
            //                    filterContext.Result = new RedirectResult("/Account/UserLogin");
            //                }
            //            }
            //        }
            //        HttpContext.Current.User = (IPrincipal)auth;
            //    }
            //}
            else
            {
                //filterContext.Controller.TempData["message"] = "You are not logged in.";
                // filterContext.Result = new RedirectResult("/Account/UserLogin");
            }
        }

        #endregion

        public AccountController()
                 : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
             : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
             : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
             : message == ManageMessageId.Error ? "An error has occurred."
             : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut();
        //    return RedirectToAction("Index", "Home");
        //}

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                //18-05-2020 Nithin Handle Session Logout redirect issue from PDRP Area 
                if (returnUrl.ToLower().Contains("formstasklist"))
                    return RedirectToAction("Index", "FormsTaskList");
                else
                    return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        public void ShowStatusMessage(string message)
        {
            ViewBag.message = message;
            TempData["message"] = message;
        }
        public string GetVersionDate()
        {
            return CommonHelper.AssemblyExtension.FindCurrentAssemblyVersion();
        }

        public ActionResult PayrollUserLogin(HRMS.Entities.ViewModel.LoginViewModel loginViewModel)
        {
            UserDB userDB = new UserDB();
            UserLoggerDB UserLoggerDB = new UserLoggerDB();
            OperationDetails op = new OperationDetails();
            if (loginViewModel.Username != null && loginViewModel.Password != null)
            {
                //loginViewModel.Password = HRMS.Web.CommonHelper.CommonHelper.EncryptPassword(loginViewModel.Password);
                UserModel userModel = new UserModel();
                userModel = userDB.AuthenticateUser(loginViewModel);

                if (userModel != null)
                {
                    if (userModel.IsActive)
                    {
                        SetAuthentication(userModel, false);
                        FormsAuthentication.SetAuthCookie(userModel.Username, true);
                        UserLogModel userLog = new UserLogModel()
                        {
                            userName = userModel.Username,
                            browserName = HttpContext.Request.Browser.Type,
                            browserVersion = DataAccess.GeneralDB.CommonDB.FindBroserName(HttpContext.Request.Browser.Version),
                            userId = ((HRMS.Entities.ViewModel.UserContextViewModel)Session["userContext"]).UserId,
                            Date = DateTime.Now.ToString(),
                            description = "User " + userModel.Username + " Log In On Date " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"),
                            moduleName = "Account",
                            oprationType = "Log In",
                        };
                        UserLoggerDB.AddUserLog(userLog);
                        string[] permissoins = userModel.Permission.Split(',');
                        if (userModel.Permission == ",")
                        {
                            op.Success = false;
                            op.InsertedRowId = -1;
                            return Json(op, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            op.Success = true;
                            return Json(op, JsonRequestBehavior.AllowGet);
                        }

                    }
                }
                else
                {
                    op.Message = HRMS.Web.CommonHelper.CommonHelper.InvalidCredentialMsg;
                    op.Success = false;
                    return Json(op, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                op.Success = false;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            op.Success = false;
            return Json(op, JsonRequestBehavior.AllowGet);
        }


    }
}