﻿using HRMS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.Entities.ViewModel;
using System.Data;
using HRMS.DataAccess.GeneralDB;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Globalization;

namespace HRMS.Web.Controllers
{
    public class PayAdditionController : BaseController
    {
        public ActionResult Index(int Id = 0)
        {
            Id = CommonHelper.CommonHelper.GetEmployeeId(Id);
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
           
            if (Id != 0)
            {
                Session["EmployeeListID"] = Id.ToString();
                int EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                ViewBag.EmployeeIDNo = Id;
                ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();

            }
            else
            {
                ViewBag.EmployeeIDNo = 0;
                if (Session["EmployeeListID"] != null)
                {
                    int EmployID = Convert.ToInt32(Session["EmployeeListID"]);
                    if (EmployID > 0)
                    {
                        objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);
                        ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                        ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                        ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                        ViewBag.EmployeeIDNo = EmployID.ToString();
                        ViewBag.AlternativeId = "";
                    }
                }

            }

            return View();
        }

        public JsonResult CalculateExtraDays(double extraDays, int EmployeeID, string AdditionDate)
        {
            string result = "";
            string resultMessage = "";
            try
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                PaySalaryDB objPaySalaryDB = new PaySalaryDB();
                double Amount = 0;                
                DateTime date = DateTime.ParseExact(AdditionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
                PayAdditionSettingsModel payAdditionSettingsModel = objAdditionPaidCycleDB.GetPayAdditionSettings();
                DeductionDB deductionDB = new DeductionDB();
                if (payAdditionSettingsModel.PerDayFixAmount == 0)
                {
                    EmployeeSalaryDetails empSalary = deductionDB.GetEmployeeSalaryDetails(EmployeeID, AdditionDate);
                    PayrollDB objPayrollDB = new PayrollDB();
                    int NoOfCycleDays = 30;
                    NoOfCycleDays = objPayrollDB.GetNoOfDaysInCycle();
                    if (NoOfCycleDays == 0)
                    {                     
                        NoOfCycleDays = objPayrollDB.GetNoOfDaysInCycleForDate(date);
                    }
                    if (NoOfCycleDays > 0)
                    {
                        if (empSalary.FullSalary != 0)
                        {
                            Amount = empSalary.PerDaySalary * extraDays;
                        }
                        else
                        {
                            result = "error";
                            resultMessage = "Employee Salary is 0. Calculator is unable to calculate it.";
                            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        result = "error";
                        resultMessage = "Please create cycle for " + date.ToString("MMM") + " Month";
                    }
                }
                else
                {
                    Amount = payAdditionSettingsModel.PerDayFixAmount * extraDays;
                }

                if (Amount != 0)
                {
                    result = "success";
                    string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
                    resultMessage = Math.Round(Amount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero).ToString(AmountFormat);
                }
                else
                {
                    result = "error";
                    resultMessage = "Amount is 0";
                }

            }
            catch (Exception ex)
            {
                result = "error";
                resultMessage = "Error occured while calculating.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateOverTime(double OverTime, int EmployeeID, string AdditionDate, string Type)
        {
            string result = "";
            string resultMessage = "";
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            PaySalaryDB objPaySalaryDB = new PaySalaryDB();
            double Amount = 0;       
            var payAdditionSettingsModel = objAdditionPaidCycleDB.GetPayAdditionSettings();
            DeductionDB deductionDB = new DeductionDB();       
            DateTime Date1 = DateTime.ParseExact(AdditionDate,"dd/MM/yyyy",CultureInfo.InvariantCulture);
            int NoOfCycleDays = 30;
            PayrollDB objPayrollDB = new PayrollDB();
            NoOfCycleDays = objPayrollDB.GetNoOfDaysInCycle();

            if (NoOfCycleDays == 0)
            {
                NoOfCycleDays = objPayrollDB.GetNoOfDaysInCycleForDate(Date1);
            }
            if (NoOfCycleDays > 0)
            {
                EmployeeSalaryDetails empSalary = deductionDB.GetEmployeeSalaryDetails(EmployeeID, AdditionDate);
                if (empSalary.FullSalary != 0)
                {
                    try
                    {
                        if (Type == "Normal")
                            Amount = ((empSalary.PerDaySalary / payAdditionSettingsModel.OverTimeWorkingHours)* payAdditionSettingsModel.OverTimeWorkingHoursExtra) * OverTime;
                        else
                            Amount = (((empSalary.PerDaySalary) / payAdditionSettingsModel.OverTimeWorkingHours) * payAdditionSettingsModel.OverTimeWorkingHoursExtra1) * OverTime;
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            if (Amount != 0)
            {
                string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
                result = "success";
                resultMessage = Math.Round(Convert.ToDecimal(Amount), CommonHelper.CommonHelper.NoOfDecimalPlaces).ToString(AmountFormat);
            }
            else
            {
                result = "error";
                resultMessage = "Something went wrong. Calculator is unable to calculate it.";
            }
            if (NoOfCycleDays == 0)
            {
                result = "error";
                resultMessage = "Please create cycle for " + Date1.ToString("MMM") + " Month";
            }
            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPayAdditionListEmployee()
        {
            int EmployeeId = 0;
            if (Session["EmployeeListID"] != null)
            {
                EmployeeId = Convert.ToInt32(Session["EmployeeListID"].ToString());

            }
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<HRMS.Entities.PayAddition> objPayAdditionList = new List<HRMS.Entities.PayAddition>();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            objPayAdditionList = objAdditionPaidCycleDB.GetAllPayAdditionForEmployee(EmployeeId).ToList();
            string Actions = "<a class='btn btn-success  btn-rounded btn-condensed btn-sm btn-space' onclick='EditChannel(PayAdditionID)' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(PayAdditionID)' title='Delete' ><i class='fa fa-times'></i> </a>";
            string NonEditedActions = "<a disabled class='btn btn-success  btn-rounded btn-condensed btn-sm btn-space' onclick='EditChannel(PayAdditionID)' title='Edit' ><i class='fa fa-pencil'></i> </a><a disabled class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteChannel(PayAdditionID)' title='Delete' ><i class='fa fa-times'></i> </a>";
            var vList = new object();
            vList = new
            {
                aaData = (from item in objPayAdditionList
                          select new
                          {
                              Actions = (item.IsProcessed == true ? NonEditedActions.Replace("PayAdditionID", item.PayAdditionID.ToString()) : Actions.Replace("PayAdditionID", item.PayAdditionID.ToString())),
                              PayAdditionID = item.PayAdditionID,
                              PaySalaryAllowanceID = item.PaySalaryAllowanceID,
                              PaySalaryAllowanceName = item.PaySalaryAllowanceName,
                              EmployeeID = item.EmployeeID,
                              AdditionDate = item.AdditionDate != "" ? item.AdditionDate : "",
                              Amount = item.Amount.ToString(CommonHelper.CommonHelper.GetAmountFormat()),
                              Description = item.Description,
                              IsActive = item.IsProcessed == true ? (item.IsActive == true ? "<input type='checkbox' disabled class='checkbox' checked='true' id='" + item.PayAdditionID + "' onclick='updatePayAdditionStatus(" + item.PayAdditionID + "," + item.PaySalaryAllowanceID + ")' />" : "<input type='checkbox' disabled class='checkbox'  id='" + item.PayAdditionID + "'  onclick='updatePayAdditionStatus(" + item.PayAdditionID + "," + item.PaySalaryAllowanceID + ")'/>")
                                         :(item.IsActive == true ? "<input type='checkbox' class='checkbox' checked='true' id='" + item.PayAdditionID + "' onclick='updatePayAdditionStatus(" + item.PayAdditionID + "," + item.PaySalaryAllowanceID + ")' />" : "<input type='checkbox' class='checkbox'  id='" + item.PayAdditionID + "'  onclick='updatePayAdditionStatus(" + item.PayAdditionID + "," + item.PaySalaryAllowanceID + ")'/>"),
                              TransactionDate = item.TransactionDate != null ? item.TransactionDate.ToString() : "",
                              AcademicYearID = item.AcademicYearID
                          }).ToArray()
            };
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult Edit(int id, int EmployeeId)
        {
            HRMS.Entities.PayAddition objPayAddition = new HRMS.Entities.PayAddition();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();

            // Dropdown for salary allowance selection
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Addition Type", Value = "" });

            foreach (var m in objAdditionPaidCycleDB.GetAllPaySalaryAllowanceList(true, true).Where(x=>x.PaySalaryAllowanceID>0))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PaySalaryAllowanceName_1, Value = m.PaySalaryAllowanceID.ToString() });
            }
            ViewBag.SalaryAllowanceList = ObjSelectedList;            
            PayAdditionSettingsModel payAdditionSettingsModel = objAdditionPaidCycleDB.GetPayAdditionSettings();
            if (payAdditionSettingsModel.PerDayFixAmount != 0)
            {
                string AmountFormat = CommonHelper.CommonHelper.GetAmountFormat();
                ViewBag.DayFixAmount = Math.Round(payAdditionSettingsModel.PerDayFixAmount, CommonHelper.CommonHelper.NoOfDecimalPlaces, MidpointRounding.AwayFromZero).ToString(AmountFormat);
            }
            else
            {
                ViewBag.DayFixAmount = "0";
            }

            if (id != 0)
            {
                objPayAddition = objAdditionPaidCycleDB.GetPayAddition(id);
                // ViewBag.PayAdditionDate = objPayAddition.AdditionDate.Value.ToShortDateString();
            }
            else
            {
                objPayAddition.IsActive = true;
                objPayAddition.AdditionDate = DateTime.Now.ToString("dd/MM/yyyy");
            }

            objPayAddition.EmployeeID = EmployeeId;

            EmployeeDB objEmployeeDB = new EmployeeDB();
            Employee objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployeeId);
            ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + " " + objEmployee.employeeDetailsModel.SurName_1;
            ViewBag.Positon = objEmployee.employmentInformation.PositionName;
            ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
            ViewBag.EmployeeIDNo = EmployeeId;
            objPayAddition.OverTimeExtra = objAdditionPaidCycleDB.GetPayAdditionSettings().OverTimeWorkingHoursExtra.ToString();
            objPayAddition.OverTimeExtra1 = objAdditionPaidCycleDB.GetPayAdditionSettings().OverTimeWorkingHoursExtra1.ToString();
            return View(objPayAddition);
        }

        public JsonResult Save(PayAddition objPayAddition)
        {
            OperationDetails opertaionDetails = new OperationDetails();
            try
            {
                if (objPayAddition.EmployeeID != 0)
                {
                    AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
                    PayrollDB payrollDb = new PayrollDB();
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objPayAddition.Description = objPayAddition.Description == null ? "" : objPayAddition.Description;
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                    PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
                    PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
                    objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 2).FirstOrDefault();
                    objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
                    bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
                    bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
                    bool IsDeductionSendForApproval = false;
                    if (IsConfirmRunPayroll && IsSendForConfirmation)
                    {
                        IsDeductionSendForApproval = true;
                    }
                    else
                    {
                        IsDeductionSendForApproval = false;
                    }

                    if (objPayAddition.PayAdditionID == 0)
                    {
                        if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                        {
                            objAdditionPaidCycleDB.AddUpdateDeletePayAddition(objPayAddition, 1);
                            opertaionDetails.Success = true;
                            opertaionDetails.CssClass = "DirectSave";
                            opertaionDetails.Message = "Pay addition added successfully.";
                        }
                        else
                        {
                            opertaionDetails = objAdditionPaidCycleDB.SavePayrollConfirmationRequestForAddition(objPayAddition, objUserContextViewModel.UserId, 1);
                            if (opertaionDetails.Success)
                            {
                                opertaionDetails.Message = "Pay addition modification request send for confirmation.";
                                opertaionDetails.CssClass = "Request";
                            }
                            else
                            {
                                opertaionDetails.Message = "Technical error has occurred.";
                                opertaionDetails.CssClass = "error";
                            }

                        }
                    }
                    else
                    {
                        if (objAdditionPaidCycleDB.GetAllPaySalaryAllowanceList(true, true).Where(x => x.PaySalaryAllowanceID == objPayAddition.PaySalaryAllowanceID).Count() > 0 || !objPayAddition.IsActive)
                        {
                            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                            {
                                objAdditionPaidCycleDB.AddUpdateDeletePayAddition(objPayAddition, 2);
                                opertaionDetails.Success = true;
                                opertaionDetails.CssClass = "DirectUpdate";
                                opertaionDetails.Message = "Pay addition updated successfully.";
                            }
                            else
                            {
                                opertaionDetails = objAdditionPaidCycleDB.SavePayrollConfirmationRequestForAddition(objPayAddition, objUserContextViewModel.UserId, 2);
                                if (opertaionDetails.Success)
                                {
                                    opertaionDetails.Message = "Pay addition modification request send for confirmation.";
                                    opertaionDetails.CssClass = "Request";
                                }
                                else
                                {
                                    opertaionDetails.Message = "Technical error has occurred.";
                                    opertaionDetails.CssClass = "error";
                                }
                            }
                        }
                        else
                        {
                            opertaionDetails.Success = false;
                            opertaionDetails.CssClass = "error";
                            opertaionDetails.Message = "This addition type no longer exist so you cannot update this record.";
                        }
                    }
                }
                else
                {
                    opertaionDetails.CssClass = "error";
                    opertaionDetails.Message = "Please select Employee first";
                }
            }
            catch (Exception ex)
            {
                opertaionDetails.CssClass = "error";
                opertaionDetails.Message = "Error occured while adding Pay Addition.";
            }

            return Json(opertaionDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            HRMS.Entities.PayAddition objPayAddition = new HRMS.Entities.PayAddition();
            PayrollDB payrollDb = new PayrollDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            OperationDetails opertaionDetails = new OperationDetails();
            objPayAddition = objAdditionPaidCycleDB.GetPayAddition(Convert.ToInt32(id));

            PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
            PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
            objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 2).FirstOrDefault();
            objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
            bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
            bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
            bool IsDeductionSendForApproval = false;
            if (IsConfirmRunPayroll && IsSendForConfirmation)
            {
                IsDeductionSendForApproval = true;
            }
            else
            {
                IsDeductionSendForApproval = false;
            }

            if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
            {
                objAdditionPaidCycleDB.AddUpdateDeletePayAddition(objPayAddition, 3);
                opertaionDetails.Message = "Pay addtion is deleted successfully";
                opertaionDetails.CssClass = "DirectDelete";
                opertaionDetails.Success = true;
            }
            else
            {
                opertaionDetails = objAdditionPaidCycleDB.SavePayrollConfirmationRequestForAddition(objPayAddition, objUserContextViewModel.UserId, 3);
                opertaionDetails.CssClass = "Request";
                if (opertaionDetails.Success)
                {
                    opertaionDetails.Message = "Pay addition modification request send for confirmation.";
                }
                else
                {
                    opertaionDetails.Message = "Technical error has occurred.";
                    opertaionDetails.CssClass = "error";
                }
            }
            return Json(opertaionDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcel(int EmployeeId)
        {
            DBHelper objDBHelper = new DBHelper();
            byte[] content;
            string fileName = "PayAddition" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            System.Data.DataSet ds = objAdditionPaidCycleDB.GetPayAdditionDataTable(EmployeeId);
            content = CommonHelper.CommonHelper.CreateExcelsheet(ds);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf(int EmployeeId)
        {
            string fileName = "PayAddition" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            System.Data.DataSet ds = objAdditionPaidCycleDB.GetPayAdditionDataTable(EmployeeId);
            Document dc = CommonHelper.CommonHelper.GenratePDF(Response, ds, true);
            Response.Write(dc);
            Response.End();
        }

        public ActionResult updatePayAdditionStatus(int Id, int additionId, string status)
        {
            OperationDetails opertaionDetails = new OperationDetails();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
            HRMS.Entities.PayAddition objPayAddition = new HRMS.Entities.PayAddition();
            objPayAddition.PayAdditionID = Id;
            objPayAddition.IsActive = Convert.ToBoolean(status) == true ? true : false;

            if (objAdditionPaidCycleDB.GetAllPaySalaryAllowanceList(true, true).Where(x => x.PaySalaryAllowanceID == additionId).Count() > 0 || !objPayAddition.IsActive)
            {
                opertaionDetails = objAdditionPaidCycleDB.updatePayAdditionStatus(objPayAddition);
                opertaionDetails.Success = true;
                opertaionDetails.CssClass = "success";
                opertaionDetails.Message = status == "true" ? "Pay Addition activated successfully" : "Pay Addition deactivated successfully";
            }
            else
            {
                opertaionDetails.Success = false;
                opertaionDetails.CssClass = "error";
                opertaionDetails.Message = "This Addition type no longer exist so you cannot update this record.";
            }

            return Json(opertaionDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMultipleAddition()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            HRMS.Entities.PayAddition objPayAddition = new HRMS.Entities.PayAddition();
            AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();

            // Dropdown for salary allowance selection
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "Select Addition Type", Value = "" });

            foreach (var m in objAdditionPaidCycleDB.GetAllPaySalaryAllowanceList(true, true).Where(x=>x.PaySalaryAllowanceID>0))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PaySalaryAllowanceName_1, Value = m.PaySalaryAllowanceID.ToString() });
            }
            ViewBag.SalaryAllowanceList = ObjSelectedList;
            objPayAddition.IsActive = true;
            objPayAddition.AdditionDate = DateTime.Now.ToString("dd/MM/yyyy");
            objPayAddition.AdditionCycles = "1";
            //Employee
            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = new SelectList(objEmployeeDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1 && x.CompanyId == objUserContextViewModel.CompanyId).ToList(), "EmployeeID", "FullName");
            return View(objPayAddition);
        }
        public JsonResult loadEmployeeList()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            EmployeeDB objEmpDB = new EmployeeDB();
            var EmpList = objEmpDB.GetALLEmployeeListWithAlternatioveId(objUserContextViewModel.UserId).Where(X => X.IsActive == 1).ToList();
            var EmployeeList = (from p in EmpList
                                select new
                                {
                                    empId = p.EmployeeId,
                                    EmpName = p.FullName
                                });
            return Json(EmployeeList.ToList(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult SaveMultipleAddition(int cycles, int AllowanceId, string AdditionDate, string description, string PayAdditions)
        {
            OperationDetails operationDetails = new OperationDetails();
            try
            {
                IEnumerable<PayAddition> lstPayAddion = JsonConvert.DeserializeObject<List<PayAddition>>(PayAdditions);
                DateTime StartDate = new DateTime();
                StartDate = Convert.ToDateTime(CommonDB.SetCulturedDate(AdditionDate));
                string subAdditionDate = "";
                AdditionPaidCycleDB objAdditionPaidCycleDB = new AdditionPaidCycleDB();
                PayrollDB payrollDb = new PayrollDB();
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                List<PayAddition> lstNewPayAddition = new List<PayAddition>();

                foreach (var item in lstPayAddion)
                {
                    item.PaySalaryAllowanceID = AllowanceId;
                    item.Description = description;
                    PayAddition objPayAddition;
                    for (int i = 0; i < cycles; i++)
                    {
                        objPayAddition = new PayAddition();
                        objPayAddition.IsActive = true;
                        subAdditionDate = StartDate.AddMonths(i).ToString();
                        objPayAddition.AdditionDate = subAdditionDate;
                        objPayAddition.PaySalaryAllowanceID = item.PaySalaryAllowanceID;
                        objPayAddition.Description = item.Description;
                        objPayAddition.EmployeeID = item.EmployeeID;
                        objPayAddition.Amount = item.Amount;
                        objPayAddition.TransactionDate = DateTime.Now;
                        objPayAddition.AcademicYearID = 0;
                        lstNewPayAddition.Add(objPayAddition);
                    }
                }
                string msg = string.Empty;

                PayrollVerificationSetting objVerificationSetting = new PayrollVerificationSetting();
                PayrollGeneralSetting objPayrollSetting = new PayrollGeneralSetting();
                objVerificationSetting = payrollDb.GetPayrollVerificationSetting().Where(m => m.PayrollVerificationID == 2).FirstOrDefault();
                objPayrollSetting = payrollDb.GetPayrollGeneralSetting();
                bool IsSendForConfirmation = objVerificationSetting.IsEnabled;
                bool IsConfirmRunPayroll = objPayrollSetting.IsconfirmRunPayroll;
                bool IsDeductionSendForApproval = false;
                if (IsConfirmRunPayroll && IsSendForConfirmation)
                {
                    IsDeductionSendForApproval = true;
                }
                else
                {
                    IsDeductionSendForApproval = false;
                }

                if (!IsDeductionSendForApproval || CommonHelper.CommonHelper.checkUrlPermission("/PayRequestConfirmation", "Url"))
                {
                    operationDetails = objAdditionPaidCycleDB.AddMultipleAllowance(lstNewPayAddition, objUserContextViewModel.UserId, true);
                    operationDetails.CssClass = "DirectMultipleSave";
                }
                else
                {
                    operationDetails = objAdditionPaidCycleDB.AddMultipleAllowance(lstNewPayAddition, objUserContextViewModel.UserId, false);
                    operationDetails.CssClass = "Request";
                    operationDetails.Message = "Pay addition modification request send for confirmation.";
                }

            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Technical error has occurred";
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}