﻿using HRMS.DataAccess;
using HRMS.DataAccess.GeneralDB;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMS.Web.Controllers
{
    public class JobApprovalController : Controller
    {
        // GET: JobApproval
        public ActionResult Index()
        {
            if (Session["EmployeeListID"] == null)
            {
                ViewBag.empid = "";
            }
            else
            {
                ViewBag.empid = Session["EmployeeListID"].ToString();
            }


            return View();
        }

        public ActionResult GetJobApprovalList()
        {
            string empid = "";
            if (Session["EmployeeListID"] == null)
            {
                return null;
            }
            else
            {
                empid = Session["EmployeeListID"].ToString();
            }

            DBHelper objDBHelper = new DBHelper();
            IssuePlaceDB objIssuePlaceDB = new IssuePlaceDB();
            var vList = new object();
            List<DocumentsModel> objDocumentsModelList = new List<DocumentsModel>();
            DocumentDB objDocumentDB = new DocumentDB();
            objDocumentsModelList = objDocumentDB.GetDocumentList(Convert.ToInt32(empid), 9);
            //--------------------------------------Update Delete permission
            PermissionModel permissionModel = HRMS.Web.CommonHelper.CommonHelper.CheckPermissionForUser("/JobApproval");
            string Action = "";

            if (permissionModel.IsUpdateOnlyPermission && permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditJobApproval(item)' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewJobApproval(item)' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteJobApproval(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
            }
            else if (!permissionModel.IsUpdateOnlyPermission && !permissionModel.IsDeleteOnlyPermission)
            {
                Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewJobApproval(item)' title='View' ><i class='fa fa-eye'></i> </a>";
            }
            else
            {
                if (permissionModel.IsUpdateOnlyPermission)
                {
                    Action += "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditJobApproval(item)' title='Edit' ><i class='fa fa-pencil'></i></a> <a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewJobApproval(item)' title='View' ><i class='fa fa-eye'></i> </a>";
                }
                if (permissionModel.IsDeleteOnlyPermission)
                {
                    Action += "<a class='btn btn-default btn-rounded btn-condensed btn-sm' onclick='ViewJobApproval(item)' title='View' ><i class='fa fa-eye'></i> </a> <a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteJobApproval(item)' title='Delete' ><i class='fa fa-times'></i> </a>";
                }
            }

            //---------------------------------------------
            vList = new
            {
                aaData = (from item in objDocumentsModelList
                          select new
                          {
                              Action = Action.Replace("item", item.DocId.ToString()),
                              DocMunicipalityID = item.DocId,
                              DocumentNo = item.ActualDocumentFile != null ? "<a href='#' onclick='ViewDocumentById(" + item.DocId.ToString() + ")'>" + item.DocNo + "</a>" : "<span title='No file available'>" + item.DocNo + "<span>",
                              IssueCountry = item.IssueCountry,
                              IssueDate = item.IssueDate == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(item.IssueDate),
                              Note = item.Note,
                              IsPrimary = item.IsPrimary ? "Yes" : "No",
                              EmployeeId = item.EmployeeId,
                              Vacc1Date = item.VAcc1 == "" ? "" : item.VAcc1,
                              IssuePlace = item.IssuePlace,
                              ExpiryDate = item.ExpiryDate == "" ? "" : CommonDB.GetFormattedDate_DDMMYYYY(item.ExpiryDate)
                          }).ToArray(),
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddJobApproval()
        {
            CountryDB objCountryDB = new CountryDB();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            CityDB citydb = new CityDB();
            DocumentDB documentDB = new DocumentDB();
            CompanyDB companyDB = new CompanyDB();
            CompanyModel companyModal = new CompanyModel();
            int SchoolCompanyID = documentDB.GetSchoolCompanyId();
            companyModal = companyDB.GetCompany(SchoolCompanyID);
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName", companyModal.Country);
            ViewBag.cityList = new SelectList(citydb.GetCityByCountryId(Convert.ToString(companyModal.Country)), "CityId", "CityName", companyModal.City);
            ViewBag.Employee = new SelectList(objEmployeeDB.GetEmployeeList(), "EmployeeId", "FirstName");
            return View();
        }

        [HttpPost]
        public ActionResult AddJobApproval(DocumentsModel objDocumentsModel)
        {
            string sessionVariable = Request.Form["sessionVariable"];
            string empid = Session["EmployeeListID"].ToString();
            OperationDetails objOperationDetails = new OperationDetails();
            if (empid != "")
            {
                if (Session[sessionVariable] != null)
                {
                    EmployeeDocumentModel employeeDocumentModel = (EmployeeDocumentModel)Session[sessionVariable];
                    if (employeeDocumentModel.DocumentFile != null)
                    {
                        objDocumentsModel.DocumentFileName = employeeDocumentModel.ImageName;
                        objDocumentsModel.ActualDocumentFile = employeeDocumentModel.DocumentFile;
                        objDocumentsModel.FileContentType = employeeDocumentModel.FileContentType;
                        Session.Remove(sessionVariable);
                    }
                }
                objDocumentsModel.EmpId = Convert.ToInt32(empid);

                if (ModelState.IsValid)
                {
                    UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                    objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                    DocumentDB objDocumentDB = new DocumentDB();
                    objDocumentsModel.DocTypeId = 9;
                    objOperationDetails = objDocumentDB.AddEditDocument(objDocumentsModel, 1);
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please fill all the fields carefully.";
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please select an employee.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditJobApproval(int id)
        {
            CountryDB objCountryDB = new CountryDB();
            DocumentDB objDocumentDB = new DocumentDB();
            CityDB obJCityDB = new CityDB();
            DocumentsModel objDocumentsModel = new DocumentsModel();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objDocumentsModel = objDocumentDB.GetDocumentByDocId(id);
            if (objDocumentsModel.ActualDocumentFile != null)
            {
                ViewBag.Status = "true";
            }
            else
            {
                ViewBag.Status = "false";
            }
            ViewBag.Country = new SelectList(objCountryDB.GetAllContries(), "CountryId", "CountryName", objDocumentsModel.IssueCountryID);
            ViewBag.City = new SelectList(obJCityDB.GetCityByCountryId(objDocumentsModel.IssueCountryID.ToString()), "CityId", "CityName", objDocumentsModel.IssueCityID);
            return View(objDocumentsModel);
        }

        [HttpPost]
        public ActionResult EditJobApproval(DocumentsModel objDocumentsModel)
        {
            string oldFile = objDocumentsModel.DocumentFile;
            string sessionVariable = Request.Form["sessionVariable"];
            string empid = Session["EmployeeListID"].ToString();
            OperationDetails objOperationDetails = new OperationDetails();
            if (empid != "")
            {
                if (Session[sessionVariable] != null)
                {
                    EmployeeDocumentModel employeeDocumentModel = (EmployeeDocumentModel)Session[sessionVariable];
                    if (employeeDocumentModel.DocumentFile != null)
                    {
                        objDocumentsModel.DocumentFileName = employeeDocumentModel.ImageName;
                        objDocumentsModel.ActualDocumentFile = employeeDocumentModel.DocumentFile;
                        objDocumentsModel.FileContentType = employeeDocumentModel.FileContentType;
                        Session.Remove(sessionVariable);
                    }
                }
                objDocumentsModel.EmpId = Convert.ToInt32(empid);

                if (ModelState.IsValid)
                {
                    DocumentDB objDocumentDB = new DocumentDB();
                    objOperationDetails = objDocumentDB.AddEditDocument(objDocumentsModel, 2);
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objOperationDetails.Success = false;
                    objOperationDetails.Message = "Please fill all the fields carefully.";
                    return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please select an employee.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewJobApproval(int id)
        {
            CountryDB objCountryDB = new CountryDB();
            CityDB obJCityDB = new CityDB();
            DocumentDB objDocumentDB = new DocumentDB();
            DocumentsModel objJobApprovalModel = new DocumentsModel();
            objJobApprovalModel = objDocumentDB.GetDocumentByDocId(id);
            if (objJobApprovalModel.ActualDocumentFile != null)
            {
                ViewBag.Status = "true";
            }
            else
            {
                ViewBag.Status = "false";
            }

            return View(objJobApprovalModel);
        }

        public ActionResult DeleteJobApproval(int id)
        {
            DocumentDB objDocumentDB = new DocumentDB();
            OperationDetails op = new OperationDetails();
            DocumentsModel objDocumentsModel = new DocumentsModel();
            objDocumentsModel.DocId = id;
            op = objDocumentDB.AddEditDocument(objDocumentsModel, 3);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
    }
}