﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using System.Data;
using HRMS.Entities.ViewModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace HRMS.Web.Controllers
{
    public class ProfileController: BaseController
    {
        //
        // GET: /Profile/
        public ActionResult Index()
        {
            return View("Index");
        }


        public ActionResult GetProfileList(string iDisplayStart = "0", string iDisplayLength = "2", string sSortDir_0 = "ASC", string mDataProp_1 = "Status", int iSortCol_0 = 0)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            int pageNumber = (Convert.ToInt32(iDisplayStart) / Convert.ToInt32(iDisplayLength));
            int numberOfRecords = Convert.ToInt16(iDisplayLength);
            string sortColumn = mDataProp_1;
            string sortOrder = sSortDir_0.ToUpper();
            int totalCount = 0;

            switch (iSortCol_0)
            {
                case 0:
                    sortColumn = "ProfileName";
                    break;
                case 1:
                    sortColumn = "SessionTimeout";
                    break;
            }

            //----------------------------------------------------------------------

            //-------------Data Objects--------------------
            List<ProfileModel> objProfileList = new List<ProfileModel>();
            ProfileDB objProfileDB = new ProfileDB();
            objProfileList = objProfileDB.GetAllProfiles(pageNumber, numberOfRecords, sortColumn, sortOrder, objUserContextViewModel.CompanyId, out totalCount);


            //---------------------------------------------



            var vList = new object();

            vList = new
            {
                aaData = (from item in objProfileList
                          select new
                          {
                              
                              ProfileId = item.ProfileId,
                              ProfileName = item.ProfileName,
                              SessionTimeout = item.SessionTimeout,
                              Actions = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditChannel(" + item.ProfileId.ToString() + ")'><span class='fa fa-pencil'></span></a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onClick='DeleteChannel(" + item.ProfileId.ToString() + ")'><span class='fa fa-times'></span></a>"
                          }).ToArray(),
                recordsTotal = totalCount,
                recordsFiltered = totalCount
            };


            return Json(vList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id)
        {
            ProfileModel profileModel = new ProfileModel();
            ProfileDB objProfileDB = new ProfileDB();

            if (id != 0)
            {
                profileModel = objProfileDB.Get_ProfileByProfileId(id);
                profileModel.checkids=GetCheckboxSelectedItems(id);
            }

            return View(profileModel);
        }

        private string GetCheckboxSelectedItems(int ProfileId)
        {
            ProfileModel profileModel = new ProfileModel();
            ProfileDB objProfileDB = new ProfileDB();
            string no_of_days = "";            
            List<ProfileDaysAccessModel> objProfileDaysList = objProfileDB.GetAllProfilesDaysAccessByProfileId(ProfileId);

            foreach(ProfileDaysAccessModel objProfileDays in objProfileDaysList)
            {
                no_of_days += objProfileDays.No_Of_Days + ";";
            }

            //if (no_of_days != "")
            //{
            //    int length = no_of_days.Length - 1;
            //    no_of_days = no_of_days.Remove(length);
            //}

            return no_of_days;
        }


        public ActionResult Save(ProfileModel objProfile)
        {
            
            OperationDetails operationDetails = new OperationDetails();
            string result = "";
            string resultMessage = "";
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            ProfileDB objobjProfileDB = new ProfileDB();

            if (objProfile.ProfileId == 0)
            {
                objProfile.CompanyId = objUserContextViewModel.CompanyId;
                objProfile.CreatedBy = objUserContextViewModel.UserId;
                operationDetails = objobjProfileDB.AddProfile(objProfile);
                AddProfileDayAcceess(objProfile.checkids, operationDetails.InsertedRowId);                
            }
            else
            {
                objProfile.CompanyId = objUserContextViewModel.CompanyId;
                objProfile.ModifiedBy = objUserContextViewModel.UserId;
                operationDetails=objobjProfileDB.UpdateProfile(objProfile);
                AddProfileDayAcceess(objProfile.checkids, objProfile.ProfileId);
            }

            if (operationDetails.Message == "Success")
            {
                if (objProfile.ProfileId == 0)
                {
                    result = "success";
                    resultMessage = "Profile Details Added Successfully.";
                }
                else if (objProfile.ProfileId != 0)
                {
                    result = "success";
                    resultMessage = "Profile Details Updated Successfully.";
                }
            }
            else
            {
                result = "error";
                resultMessage = "Error occured while adding Profile Details.";
            }

            return Json(new { result = result, resultMessage = resultMessage }, JsonRequestBehavior.AllowGet);

        }

        private void AddProfileDayAcceess(string daysaccess,int id)
        {
            ProfileDaysAccessModel objProfiledayaccess=new ProfileDaysAccessModel();
            ProfileDB objProfileDB=new ProfileDB();
            if (daysaccess != "" && daysaccess != null)
            {
                int length = daysaccess.Length-1;
                daysaccess = daysaccess.Remove(length);
                string[] days=daysaccess.Split(';');
                objProfiledayaccess.ProfileId = id;
                objProfileDB.DeleteProfileDaysAccess(objProfiledayaccess);
                foreach(string no_of_days in days)
                {
                    objProfiledayaccess.No_Of_Days=int.Parse(no_of_days);
                    objProfiledayaccess.ProfileId=id;
                    objProfiledayaccess.CreatedBy=1;
                    objProfileDB.AddProfileDaysAccess(objProfiledayaccess);
                }
                
            }

        }

        public JsonResult Delete(string id)
        {
            ProfileDB objProfileDB = new ProfileDB();
            ProfileModel objProfile = new ProfileModel();
            objProfile.ProfileId = int.Parse(id);
            objProfileDB.DeleteProfile(objProfile);

            return Json(new { result = "success", resultMessage = "Profile Deleted Successfully." }, JsonRequestBehavior.AllowGet);
            //return Redirect("Index");
        }

        public ActionResult ExportToExcel()
        {
            byte[] content;
            string fileName = "Profile" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xls";

            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();
            
            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "ProfileId";
            string sortOrder = "Desc";
            int companyid =objUserContextViewModel.CompanyId;
             int totalCount = 0;
            List<ProfileModel> objProfileList = new List<ProfileModel>();
            ProfileDB objProfileDB = new ProfileDB();
            objProfileList = objProfileDB.GetAllProfiles(pageNumber, numberOfRecords, sortColumn, sortOrder, companyid, out totalCount);
            var report = (from item in objProfileList.AsEnumerable()
                          select new
                          { 
                              //Id = item.Id,
                              ProfileId = item.ProfileId,
                              ProfileName = item.ProfileName,
                              SessionTimeout = item.SessionTimeout             

                          }).ToList();
            content = ExportListToExcel(report);
            return new FileContentResult(content, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        public void ExportToPdf()
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);

            string fileName = "Profile" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".pdf";


            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            //ProfileDB profileDB = new ProfileDB();

            int pageNumber = 0;
            int numberOfRecords = 0;
            string sortColumn = "ProfileId";
            string sortOrder = "Desc";
            int companyid = objUserContextViewModel.CompanyId;
            int totalCount = 0;
            List<ProfileModel> objProfileList = new List<ProfileModel>();
            ProfileDB objProfileDB = new ProfileDB();
            objProfileList = objProfileDB.GetAllProfiles(pageNumber, numberOfRecords, sortColumn, sortOrder, companyid, out totalCount);
            var report = (from item in objProfileList.AsEnumerable()
                          select new
                          {
                              //Id = item.Id,
                              ProfileId = item.ProfileId,
                              ProfileName = item.ProfileName,
                              SessionTimeout = item.SessionTimeout             

                          }).ToList();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename='" + fileName + "'");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            GridView gridvw = new GridView();
            gridvw.DataSource = report; //bind the data table to the grid view
            gridvw.DataBind();
            StringWriter swr = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(swr);   //use to create sequence of html element
            gridvw.RenderControl(tw);
            StringReader sr = new StringReader(swr.ToString());
            Document pdfDocs = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public JsonResult SaveAccessDays(string days)
        {
            TempData["AccessDays"] = days;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}