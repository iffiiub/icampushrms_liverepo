﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities;
using HRMS.DataAccess;
using System.Web.Script.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace HRMS.Web.Controllers
{
    public class GeneralAccountsController : BaseController
    {
        // GET: GeneralAccounts

        GeneralAccountsDB generalAccountsDB;       

        public ActionResult Index()
        {
            string AccountCode = "<option value='0'>Select Account Code</option>";
            foreach (var m in new GeneralAccountsDB().GetAccountCode())
            {
                AccountCode += "<option value='" + m.id + "'>" + m.text + "</option>";
            }
            ViewBag.AccountCode = AccountCode;
            List<SelectListItem> ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All", Value = "0" });
            foreach (var m in new GeneralAccountsDB().GetPayCategories())
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.text.ToString(), Value = m.id.ToString() });
            }
            ViewBag.Categories = ObjSelectedList;

            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All", Value = "0" });
            ObjSelectedList.Add(new SelectListItem { Text = "Salary Allowances", Value ="1" });
            ObjSelectedList.Add(new SelectListItem { Text = "Additions", Value = "2" });
            ObjSelectedList.Add(new SelectListItem { Text = "Deductions", Value = "3" });
            ViewBag.isRunWithJvs = new PayrollDB().GetJvsSettingFromPayroll();
            ViewBag.AllwanceType = ObjSelectedList;
            PayrollModel payrollModel = new PayrollModel();
            payrollModel.active = 1;
            ObjSelectedList = new List<SelectListItem>();
            ObjSelectedList.Add(new SelectListItem { Text = "All", Value = "0" });
            foreach (var m in new PayrollDB().getPaycycles(payrollModel))
            {
                ObjSelectedList.Add(new SelectListItem { Text = m.PayCycleName_1.ToString(), Value = m.PayCycleID.ToString() });
            }
            ViewBag.CycleList = ObjSelectedList;
            return View();
        }
   
        public ActionResult ViewGeneralAccounts(int? CategoryID,int? AllwanceType,int? CycleId, bool ShowMissingOnly)
        {
            List<GeneralAccounts> generalAccountslist = new List<GeneralAccounts>();

            generalAccountsDB = new GeneralAccountsDB();
            generalAccountslist = generalAccountsDB.GetGeneralAccountList(CategoryID, AllwanceType,CycleId, ShowMissingOnly);
            List<PaySalaryAllowance> objPaySalaryAllowanceList = new List<PaySalaryAllowance>();
            objPaySalaryAllowanceList = new PaySalaryDB().GetAllPaySalaryAllowanceList(0);
            
            var vList = new object();
            int i = 0;
            vList = new
            {
                aaData = (from item in generalAccountslist
                          select new
                          {
                              PayCategory = item.payCategories,
                              Type = item.allowanceName,            
                              AccountCode=item.accountCode,
                              VacationAccountCode=item.vacationAccountCode,                              
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm editButton' id=\"btnEdit\" data-id=" + item.autoID + " title='Edit' onclick=\"edit(this," + item.allowanceType + "," + item.id + "," + item.payCategoriesId + "," + item.allowanceId + ","+item.autoID+")\"><i class='fa fa-pencil'></i> </a>"+
                                       "<a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick=\"changeStatus(this," + item.id + "," + item.allowanceType + "," + item.autoID + ")\" title='Delete'><i class='fa fa-times'></i> </a>",
                              AccountCodeId = item.accountCodeId,
                              VacationCodeId = item.vacationAccountCodeId
                          }).ToArray(),

            };
            long size = 0;
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(vList),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult AddUpdateAccountCode(int AccountCodeIdval,int VacationAccountCodeId, int allowanceType, int id, int payCategoryId, int allowanceId)
        {
            OperationDetails op = new OperationDetails();
            GeneralAccountsDB objgeneralAccountsDB = new GeneralAccountsDB();
            GeneralAccounts generalAccountsModel = new GeneralAccounts();
            generalAccountsModel.payCategoriesId = payCategoryId;
            generalAccountsModel.id = id;
            generalAccountsModel.allowanceId = allowanceId;
            generalAccountsModel.accountCodeId = AccountCodeIdval;
            generalAccountsModel.vacationAccountCodeId = VacationAccountCodeId;
            int operationType = 1;
            if (id == 0)
            {
                operationType = 1;
            }
            else
            {
                operationType = 2;
            }
            op = objgeneralAccountsDB.AddUpdateAccountCode(generalAccountsModel, allowanceType, operationType);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult DeleteAccountDetails(int id, int allowanceType)
        {
            OperationDetails op = new OperationDetails();
            GeneralAccountsDB objgeneralAccountsDB = new GeneralAccountsDB();
            op = objgeneralAccountsDB.DeleteAccountDetails(id, allowanceType);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveJvsSetting(bool isRunWithJvs)
        {
            OperationDetails op = new OperationDetails();
            op = new PayrollDB().UpdateJvsSetting(isRunWithJvs);
            return Json(new { result = op.CssClass, resultMessage = op.Message }, JsonRequestBehavior.AllowGet);
        }
    }
}