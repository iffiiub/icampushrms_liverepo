﻿using HRMS.DataAccess;
using HRMS.Entities;
using HRMS.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using HRMS.DataAccess.GeneralDB;

namespace HRMS.Web.Controllers
{
    public class InsuranceController : BaseController
    {
        public ActionResult Index(int Id = 0)
        {
            Id = CommonHelper.CommonHelper.GetEmployeeId(Id);
            Employee objEmployee = new Entities.Employee();
            EmployeeDB objEmployeeDB = new EmployeeDB();
            int EmployID = 0;
            if (Id != 0)
            {

                Session["EmployeeListID"] = Id.ToString();
                EmployID = Id;
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);

                ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                ViewBag.Positon = objEmployee.employmentInformation.PositionName;

                ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                ViewBag.EmployeeIDNo = Id.ToString();
                ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();

            }
            else
            {

                if (Session["EmployeeListID"] != null)
                {
                    Id = objEmployeeDB.GetFirstActiveEmployee();
                    Session["EmployeeListID"] = Id.ToString();
                }

                EmployID = Convert.ToInt32(Session["EmployeeListID"]);
                objEmployee = objEmployeeDB.GEtBasicEmployeeDetails(EmployID);
                if (EmployID == 0)
                {
                    ViewBag.EmployeeIDNo = "";
                    ViewBag.EmployeeName = "";
                    ViewBag.Positon = "";
                    ViewBag.Department = "";
                    ViewBag.EmployeeIDNo = "";
                    ViewBag.AlternativeId = "";
                }
                else
                {
                    ViewBag.EmployeeIDNo = EmployID;
                    ViewBag.EmployeeName = objEmployee.employeeDetailsModel.FirstName_1 + "   " + objEmployee.employeeDetailsModel.SurName_1;
                    ViewBag.Positon = objEmployee.employmentInformation.PositionName;
                    ViewBag.Department = objEmployee.employmentInformation.DepartmentName;
                    ViewBag.EmployeeIDNo = EmployID.ToString();
                    ViewBag.AlternativeId = objEmployee.employeeDetailsModel.EmployeeAlternativeID.ToString();
                }
            }
            InsuranceDB objInsuranceDB = new InsuranceDB();
            ViewBag.InsuranceTypeList = new SelectList(objInsuranceDB.GetInsuranceTypeList(), "HRInsuranceTypeID", "InsuranceTypeName_1");
            InsuranceModel objInsuranceModel = new InsuranceModel();
            if (EmployID > 0)
                objInsuranceModel = objInsuranceDB.GetInsuranceList(EmployID);
            objInsuranceModel.EmployeeID = EmployID;
            return View(objInsuranceModel);
        }
        public ActionResult GetInsdependenceList(int empid = 0)
        {
            if (empid != 0)
            {
                Session["EmployeeListID"] = empid;
            }
            DBHelper objDBHelper = new DBHelper();
            //-------------------Grid Paging Info----------------------------------
            int pageMode = Convert.ToInt16(HttpContext.Items["PageMode"]);
            List<InsdependenceModel> objInsdependenceList = new List<InsdependenceModel>();
            InsuranceDB objInsuranceDB = new InsuranceDB();
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objInsdependenceList = objInsuranceDB.GetInsdependenceList(empid);
            var vList = new object();
            vList = new
            {
                aaData = (from item in objInsdependenceList
                          select new
                          {
                              Action = "<a class='btn btn-success btn-rounded btn-condensed btn-sm' onclick='EditInsuranceDependence(" + item.InsuranceDependenceID.ToString() + ")' title='Edit' ><i class='fa fa-pencil'></i> </a><a class='btn btn-danger btn-rounded btn-condensed btn-sm' onclick='DeleteInsuranceDependence(" + item.InsuranceDependenceID.ToString() + ")' title='Delete' ><i class='fa fa-times'></i> </a>",
                              InsuranceDependence = item.InsuranceDependenceID,
                              Name = item.Name,
                              Relation = item.Relation,
                              RelationName = item.RelationName,
                              CardNumber = item.CardNumber,
                              Note = item.Note
                          }).ToArray(),
                recordsTotal = objInsdependenceList.Count,
                recordsFiltered = objInsdependenceList.Count
            };
            return Json(vList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInsdependence()
        {
            int empid = 0;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            if (Session["EmployeeListID"] != null)
            {

                empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
            }

            InsdependenceModel objInsdependenceModel = new InsdependenceModel();
            InsuranceDB objInsuranceDB = new InsuranceDB();
            objInsdependenceModel.EmployeeID = empid;

            ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");

            return View(objInsdependenceModel);
        }


        [HttpPost]
        public ActionResult AddInsdependence(InsdependenceModel objInsdependenceModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objInsdependenceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                InsuranceDB objInsuranceDB = new InsuranceDB();
                OperationDetails objOperationDetails = new OperationDetails();


                if (objInsdependenceModel.InsuranceDependenceID > 0)
                {
                    objInsdependenceModel.TranMode = 2;
                }
                else
                {
                    objInsdependenceModel.TranMode = 1;
                }


                objOperationDetails = objInsuranceDB.InsertInsdependence(objInsdependenceModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                InsuranceDB objInsuranceDB = new InsuranceDB();
                objInsdependenceModel.EmployeeID = empid;

                ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");



                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }



        [HttpPost]
        public ActionResult AddInsurance(InsuranceModel objInsuranceModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                if (Session["EmployeeListID"] != null)
                {

                    objInsuranceModel.EmployeeID = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }


                //  objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                InsuranceDB objInsuranceDB = new InsuranceDB();
                OperationDetails objOperationDetails = new OperationDetails();





                objOperationDetails = objInsuranceDB.InsertInsurance(objInsuranceModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
            else
            {
                int empid = 0;

                if (Session["EmployeeListID"] != null)
                {

                    empid = Convert.ToInt32(Session["EmployeeListID"].ToString());
                }

                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];

                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult EditInsdependence(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            InsuranceDB objInsuranceDB = new InsuranceDB();
            InsdependenceModel objInsdependenceModel = new InsdependenceModel();
            objInsdependenceModel = objInsuranceDB.GetInsdependenceByID(id);

            ViewBag.RelationList = new SelectList(objInsuranceDB.GetRelationTypeList(), "RelationId", "RelationName");


            return View("AddInsdependence", objInsdependenceModel);
        }

        [HttpPost]
        public JsonResult EditInsdependence(AbsentModel objAbsentModel)
        {
            if (ModelState.IsValid)
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                //     objAbsentModel.EmployeeID = objUserContextViewModel.UserId;
                AbsentDB objAbsentModelDB = new AbsentDB();
                OperationDetails objOperationDetails = new OperationDetails();
                //if (objAbsentModel.PayEmployeeAbsentFromDate != "")
                //    objAbsentModel.PayEmployeeAbsentFromDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentFromDate).ToShortDateString();
                //if (objAbsentModel.PayEmployeeAbsentToDate != "")
                //    objAbsentModel.PayEmployeeAbsentToDate = Convert.ToDateTime(objAbsentModel.PayEmployeeAbsentToDate).ToShortDateString();
                objOperationDetails = objAbsentModelDB.UpdateAbsent(objAbsentModel);
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UserContextViewModel objUserContextViewModel = new UserContextViewModel();
                objUserContextViewModel = (UserContextViewModel)Session["userContext"];
                LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
                AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
                ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

                //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");
                EmployeeDB objEmployeeDB = new EmployeeDB();




                ViewBag.Employee = objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;


                //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
                OperationDetails objOperationDetails = new OperationDetails();
                objOperationDetails.Success = false;
                objOperationDetails.Message = "Please fill all the fields carefully.";
                return Json(objOperationDetails, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpdateLeaveType(int LeavetypeID, int PayEmployeeAbsentID)
        {



            AbsentDB objAbsentModelDB = new AbsentDB();
            OperationDetails objOperationDetails = new OperationDetails();
            objOperationDetails = objAbsentModelDB.UpdateLeavetypeAbsent(LeavetypeID, PayEmployeeAbsentID);
            return Json(objOperationDetails, JsonRequestBehavior.AllowGet);


        }

        public ActionResult ViewInsurance(int id)
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            LeaveTypeDB objLeaveTypeDB = new LeaveTypeDB();
            AbsentTypeDB objAbsentTypeDB = new AbsentTypeDB();
            ViewBag.LeaveType = new SelectList(objLeaveTypeDB.GetEmployeeLeaveTypeList(), "LeaveTypeId", "LeaveTypeName");

            //ViewBag.AbsentType = new SelectList(objAbsentTypeDB.GetAbsentTypeList(), "PayAbsentTypeID", "PayAbsentTypeName_1");


            //ViewBag.Employee = new SelectList(objEmployeeDB.GetAllEmployeeByCompanyId(objUserContextViewModel.CompanyId), "EmployeeId", "FirstName_1"); 
            AbsentDB objAbsentModelDB = new AbsentDB();
            AbsentModel objAbsentModel = new AbsentModel();
            objAbsentModel = objAbsentModelDB.AbsentById(id);

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.Employee = objEmployeeDB.GetEmployee(objAbsentModel.EmployeeID).FirstName + " " + objEmployeeDB.GetEmployee(objUserContextViewModel.UserId).LastName;

            return View(objAbsentModel);
        }

        public ActionResult DeleteInsdependence(int id)
        {
            InsuranceDB objInsuranceDB = new InsuranceDB();
            objInsuranceDB.DeleteInsdependenceById(id);
            return Json(0, JsonRequestBehavior.AllowGet);
        }
    }
}