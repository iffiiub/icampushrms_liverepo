﻿using HRMS.DataAccess.FormsDB;
using HRMS.DataAccess;
using HRMS.Entities.Forms;
using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMS.Entities.ViewModel;
using System.IO;

namespace HRMS.Web.Controllers
{
    public class HiringR1ReplacementController : FormsController
    {
        HiringR1ReplacementDB hiringR1ReplacementDB;
        public HiringR1ReplacementController()
        {
            XMLLogFile = "LoggerHiringR1Replacement.xml";
            hiringR1ReplacementDB = new HiringR1ReplacementDB();
        }

        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }

        public ActionResult Create()
        {
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            HiringR1ReplacementModel hiringR1ReplacementModel = new HiringR1ReplacementModel();

            hiringR1ReplacementModel.AllFormsFilesModelList = new List<AllFormsFilesModel>();
            hiringR1ReplacementModel.RequestInitialize = false;
            hiringR1ReplacementModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            hiringR1ReplacementModel.CreatedOn = DateTime.Now.Date;
            CompanyModel company = new CompanyDB().GetCompany(objUserContextViewModel.CompanyId);
            //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();
            ViewBag.CompanyTypeID = company.CompanyTypeID;

            ViewBag.Year = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true), "AcademicYearId", "Duration");
            ViewBag.EmploymentType = new SelectList(new DBHelper().GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");
            if (company.CompanyTypeID > 1)
            {
                ViewBag.Company = GetCompanySelectList();// new SelectList(companyList, "CompanyId", "Name");
            }
            else
            {
                ViewBag.Company = GetCompanySelectList(company.CompanyId);// new SelectList(companyList.Where(x => x.CompanyId == company.CompanyId), "CompanyId", "Name");
            }
            ViewBag.HiringMangr = new SelectList(Enumerable.Empty<SelectListItem>());
            ViewBag.SalaryRanges = new SelectList(GetStaticSalaryRanges(), "Text", "Value");
            ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
            ViewBag.Department = new SelectList(new DepartmentDB().GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
            ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
            ViewBag.Category = new SelectList( new EmployeeProfileCreationFormDB().GetRecruitCategoryList(), "id", "text");

            EmployeeDB objEmployeeDB = new EmployeeDB();
            ViewBag.employeeList = new SelectList(objEmployeeDB.GetALLEmployeeByUserId(objUserContextViewModel.UserId).Where(x => x.IsActive == 1).ToList(), "EmployeeId", "FullName");
            ViewBag.EmployeeAirfareFrequencyList = new SelectList(hiringR1ReplacementDB.GetEmployeeAirfareFrequencyList(), "id", "text");
            ViewBag.EmployeeAirfareClassesList = new SelectList(hiringR1ReplacementDB.GetEmployeeAirfareClassesList(), "id", "text");
            ViewBag.RequestInitialize = hiringR1ReplacementModel.RequestInitialize;
            hiringR1ReplacementModel.RequesterEmployeeID = objUserContextViewModel.UserId;
            ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
            HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
            ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
            hiringR1ReplacementModel.IsAddMode = true;
            return View(hiringR1ReplacementModel);
        }
        public ActionResult Edit()
        {
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            HiringR1ReplacementModel hiringR1ReplacementModel = new HiringR1ReplacementModel();
            CompanyModel company;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int id = GetFormProcessSessionID();
            hiringR1ReplacementModel.FormProcessID = id;

            if (id > 0)
            {
                RequestFormsApproveModel requestFormsApproveModel = hiringR1ReplacementDB.GetPendingFormsApproval(id);
                hiringR1ReplacementModel = hiringR1ReplacementDB.GetForm(id, objUserContextViewModel.UserId);
                hiringR1ReplacementModel.ActualBudget = decimal.Parse(hiringR1ReplacementModel.ActualBudget.ToString(amountFormat));
                hiringR1ReplacementModel.SalikAmount = decimal.Parse(hiringR1ReplacementModel.SalikAmount.ToString(amountFormat));
                hiringR1ReplacementModel.PetrolCardAmount = decimal.Parse(hiringR1ReplacementModel.PetrolCardAmount.ToString(amountFormat));

                ViewBag.RecruitR1BudgetedID = hiringR1ReplacementModel.ID;
                ViewBag.FormProcessID = id.ToString();
                ViewBag.RequestID = hiringR1ReplacementModel.RequestID;
                ViewBag.RequestInitialize = hiringR1ReplacementModel.RequestInitialize;
                ViewBag.SalaryRangesID = hiringR1ReplacementModel.SalaryRangesID == null ? 0 : hiringR1ReplacementModel.SalaryRangesID;
                company = new CompanyDB().GetCompany(hiringR1ReplacementModel.CompanyID);

                //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();

                ViewBag.CompanyTypeID = company.CompanyTypeID;
                ViewBag.CompanyID = company.CompanyId.ToString();
                ViewBag.Position = hiringR1ReplacementModel.HMDesignation;
                ViewBag.Year = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true), "AcademicYearId", "Duration");
                ViewBag.EmploymentType = new SelectList(new DBHelper().GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");

                if (company.CompanyTypeID > 1)
                {
                    if (id > 0)
                        ViewBag.Company = GetCompanySelectList(hiringR1ReplacementModel.CompanyID);//  new SelectList(companyList.Where(x => x.CompanyId == hiringR1ReplacementModel.CompanyID), "CompanyId", "Name");
                    else
                        ViewBag.Company = GetCompanySelectList();
                }
                else
                {
                    ViewBag.Company = GetCompanySelectList(company.CompanyId);// new SelectList(companyList.Where(x => x.CompanyId == company.CompanyId), "CompanyId", "Name");
                }
                ViewBag.HiringMangr = new SelectList(new EmployeeDB().GetAllEmployeeForAdminByCompanyID(company.CompanyId), "EmployeeId", "FullName");
                ViewBag.SalaryRanges = new SelectList(GetStaticSalaryRanges(), "Text", "Value");
                ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
                ViewBag.Department = new SelectList(new DepartmentDB().GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
                ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
                ViewBag.Category = new SelectList(new EmployeeProfileCreationFormDB().GetRecruitCategoryList(), "id", "text");

                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.employeeList = new SelectList(objEmployeeDB.GetEmployeeByActive(true, 0).Where(m => m.EmployeeId == hiringR1ReplacementModel.ReplacedEmployeeID), "EmployeeId", "FirstName");
                ViewBag.EmployeeAirfareFrequencyList = new SelectList(hiringR1ReplacementDB.GetEmployeeAirfareFrequencyList(), "id", "text");
                ViewBag.EmployeeAirfareClassesList = new SelectList(hiringR1ReplacementDB.GetEmployeeAirfareClassesList(), "id", "text");
                ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
                HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
                ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");
                if ((hiringR1ReplacementModel.ReqStatusID == (int)RequestStatus.Rejected && hiringR1ReplacementModel.RequesterEmployeeID == objUserContextViewModel.UserId))
                {
                    return View("Edit", hiringR1ReplacementModel);
                }
                else if ((hiringR1ReplacementModel.ReqStatusID == (int)RequestStatus.Pending) && (objUserContextViewModel.UserId == 0 || objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID))
                {
                    if (hiringR1ReplacementModel.RequestInitialize)
                        return View("Edit", hiringR1ReplacementModel);
                    else
                        return View("Create", hiringR1ReplacementModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }
        public ActionResult UpdateDetails()
        {
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            HiringR1ReplacementModel hiringR1ReplacementModel = new HiringR1ReplacementModel();
            CompanyModel company;
            UserContextViewModel objUserContextViewModel = new UserContextViewModel();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];

            int FormProcessID = GetFormProcessSessionID();
            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();
           
            if (FormProcessID > 0)
            {
                RequestFormsApproveModel requestFormsApproveModel = hiringR1ReplacementDB.GetPendingFormsApproval(FormProcessID);
                hiringR1ReplacementModel = hiringR1ReplacementDB.GetForm(FormProcessID, objUserContextViewModel.UserId);
                hiringR1ReplacementModel.FormProcessID = FormProcessID;
                hiringR1ReplacementModel.ActualBudget = decimal.Parse(hiringR1ReplacementModel.ActualBudget.ToString(amountFormat));
                hiringR1ReplacementModel.SalikAmount = decimal.Parse(hiringR1ReplacementModel.SalikAmount.ToString(amountFormat));
                hiringR1ReplacementModel.PetrolCardAmount = decimal.Parse(hiringR1ReplacementModel.PetrolCardAmount.ToString(amountFormat));

                ViewBag.RecruitR1BudgetedID = hiringR1ReplacementModel.ID;
                ViewBag.FormProcessID = FormProcessID.ToString();
                ViewBag.RequestID = hiringR1ReplacementModel.RequestID;
                ViewBag.RequestInitialize = hiringR1ReplacementModel.RequestInitialize;
                ViewBag.SalaryRangesID = hiringR1ReplacementModel.SalaryRangesID == null ? 0 : hiringR1ReplacementModel.SalaryRangesID;

                company = new CompanyDB().GetCompany(hiringR1ReplacementModel.CompanyID);
                //List<CompanyModel> companyList = new CompanyDB().GetAllCompany();

                ViewBag.CompanyTypeID = company.CompanyTypeID;
                ViewBag.CompanyID = company.CompanyId.ToString();
                ViewBag.Position = hiringR1ReplacementModel.HMDesignation;
                ViewBag.Year = new SelectList(new AcademicYearDB().GetAllAcademicYearList().Where(x => x.IsActive == true || x.IsNextYear == true), "AcademicYearId", "Duration");
                ViewBag.EmploymentType = new SelectList(new DBHelper().GetEmploymentModeList(), "EmploymentModeId", "EmploymentModeName1");

                if (company.CompanyTypeID > 1)
                {
                    if (FormProcessID > 0)
                        ViewBag.Company = GetCompanySelectList(hiringR1ReplacementModel.CompanyID);// new SelectList(companyList.Where(x => x.CompanyId == hiringR1ReplacementModel.CompanyID), "CompanyId", "Name");
                    else
                        ViewBag.Company = GetCompanySelectList();
                }
                else
                {
                    ViewBag.Company = GetCompanySelectList(company.CompanyId);// new SelectList(companyList.Where(x => x.CompanyId == company.CompanyId), "CompanyId", "Name");
                }
                ViewBag.HiringMangr = new SelectList(new EmployeeDB().GetAllEmployeeForAdminByCompanyID(company.CompanyId), "EmployeeId", "FullName");
                ViewBag.SalaryRanges = new SelectList(GetStaticSalaryRanges(), "Text", "Value");
                ViewBag.PositionTitle = new SelectList(new PositionDB().GetPositionList(), "PositionID", "PositionTitle");
                ViewBag.Department = new SelectList(new DepartmentDB().GetDepartmentListByUserId(objUserContextViewModel.UserId), "DepartmentID", "DepartmentName_1");
                ViewBag.UserType = new SelectList(new UserTypeDB().GetUserTypesForEmployee(), "UserTypeID", " UserTypeName");
                ViewBag.Category = new SelectList(new EmployeeProfileCreationFormDB().GetRecruitCategoryList(), "id", "text");

                EmployeeDB objEmployeeDB = new EmployeeDB();
                ViewBag.employeeList = new SelectList(objEmployeeDB.GetEmployeeByActive(true, 0).Where(m => m.EmployeeId == hiringR1ReplacementModel.ReplacedEmployeeID), "EmployeeId", "FirstName");
                ViewBag.EmployeeAirfareFrequencyList = new SelectList(hiringR1ReplacementDB.GetEmployeeAirfareFrequencyList(), "id", "text");
                ViewBag.EmployeeAirfareClassesList = new SelectList(hiringR1ReplacementDB.GetEmployeeAirfareClassesList(), "id", "text");
                ViewBag.JobCategoryList = new SelectList(new JobCategoryDB().GetJobCategory(), "EmployeeJobCategoryID", "EmployeeJobCategoryName_1");
                HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB employeeProfileCreationFormDB = new HRMS.DataAccess.FormsDB.EmployeeProfileCreationFormDB();
                ViewBag.JobGrade = new SelectList(employeeProfileCreationFormDB.GetJobGradeList(), "id", "text");

                if (hiringR1ReplacementModel.ReqStatusID != (int)RequestStatus.Rejected && isEditRequestFromAllRequests)
                {
                    return View(hiringR1ReplacementModel);
                }
                else
                {
                    return RedirectToAction("ViewDetails");
                }
            }
            else
                return RedirectToAction("Create");
        }
        [HttpPost]
        public ActionResult SaveForm(string hiringR1ReplacementModel)
        {
            HiringR1ReplacementModel objHiringR1ReplacementModel = Newtonsoft.Json.JsonConvert.DeserializeObject<HiringR1ReplacementModel>(hiringR1ReplacementModel);
            List<RequestFormsProcessModel> requestFormsProcessModelList = new List<RequestFormsProcessModel>();
            RequestFormsProcessModel requestFormsProcessModel = new RequestFormsProcessModel();
            OperationDetails operationDetails = new OperationDetails();
            HttpPostedFileBase fileJD, fileOC, fileMP;
            string formProcessIDs = "";
            int result = 0;
            int formprocessid = 0;
            string amountFormat = CommonHelper.CommonHelper.GetAmountFormat();
            objUserContextViewModel = (UserContextViewModel)Session["userContext"];
            objHiringR1ReplacementModel.HeadCount = 1;

            bool isEditRequestFromAllRequests = GetEditRequestsFormProccessSession();

            try
            {
                var data = Request.Form;
                AllFormsFilesModel ObjFile;
                List<AllFormsFilesModel> uploadFileList = new List<AllFormsFilesModel>();
                if (Request.Files.Count > 0)
                {
                    if (Request.Files["JobDescription"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileJD = Request.Files["JobDescription"];
                        //Setting the file Id for updating
                        ObjFile.FileID = objHiringR1ReplacementModel.JobDescriptionFileID;
                        ObjFile.FileName = fileJD.FileName;
                        ObjFile.FileContentType = fileJD.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileJD);
                        ObjFile.FormFileIDName = "JobDescriptionFileID";
                        uploadFileList.Add(ObjFile);
                        objHiringR1ReplacementModel.JobDescriptionFileID = 0;
                    }
                    if (Request.Files["OrganizationChart"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileOC = Request.Files["OrganizationChart"];
                        ObjFile.FileID = objHiringR1ReplacementModel.OrgChartFileID;
                        ObjFile.FileName = fileOC.FileName;
                        ObjFile.FileContentType = fileOC.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileOC);
                        ObjFile.FormFileIDName = "OrgChartFileID";
                        uploadFileList.Add(ObjFile);
                        objHiringR1ReplacementModel.OrgChartFileID = 0;
                    }

                    if (Request.Files["ManpowerPlan"] != null)
                    {
                        ObjFile = new AllFormsFilesModel();
                        fileMP = Request.Files["ManpowerPlan"];
                        ObjFile.FileID = objHiringR1ReplacementModel.ManPowerFileID;
                        ObjFile.FileName = fileMP.FileName;
                        ObjFile.FileContentType = fileMP.ContentType;
                        ObjFile.DocumentFile = CommonHelper.CommonHelper.ConvertToByte(fileMP);
                        ObjFile.FormFileIDName = "ManPowerFileID";
                        uploadFileList.Add(ObjFile);
                        objHiringR1ReplacementModel.ManPowerFileID = 0;


                    }
                }
                objHiringR1ReplacementModel.AllFormsFilesModelList = uploadFileList;

                if (objHiringR1ReplacementModel.ID > 0)
                {
                    objHiringR1ReplacementModel.ModifiedBy = objUserContextViewModel.UserId;

                    formprocessid = Convert.ToInt32(string.IsNullOrEmpty(data["FormProcessID"]) ? "0" : data["FormProcessID"]);
                    RequestFormsApproveModel requestFormsApproveModel = hiringR1ReplacementDB.GetPendingFormsApproval(formprocessid);
                    if (objUserContextViewModel.UserId == 0 || (objUserContextViewModel.UserId == requestFormsApproveModel.ApproverEmployeeID)
                        || (objHiringR1ReplacementModel.ReqStatusID == (int)RequestStatus.Rejected && objHiringR1ReplacementModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        || isEditRequestFromAllRequests)
                    {
                        objHiringR1ReplacementModel.FormProcessID = formprocessid;
                        if (objHiringR1ReplacementModel.RequestInitialize && !isEditRequestFromAllRequests)
                        {
                            requestFormsProcessModel = hiringR1ReplacementDB.UpdateFormOnSeparationFinalApproval(objHiringR1ReplacementModel);
                            if (requestFormsProcessModel != null && requestFormsProcessModel.FormProcessID > 0)
                            {
                                formProcessIDs = requestFormsProcessModel.FormProcessID.ToString();
                                result = 1;
                            }
                            else
                            {
                                operationDetails.Success = false;
                                operationDetails.Message = "Error occured while Saving Details";
                                operationDetails.CssClass = "error";

                            }
                        }
                        else
                        {
                            result = hiringR1ReplacementDB.UpdateForm(objHiringR1ReplacementModel);
                            formProcessIDs = objHiringR1ReplacementModel.FormProcessID.ToString();
                        }
                        //if success on update, now update is only for rejected re initialize 
                        if (result == 1 && objHiringR1ReplacementModel.ReqStatusID == (int)RequestStatus.Rejected && objHiringR1ReplacementModel.RequesterEmployeeID == objUserContextViewModel.UserId)
                        {
                            List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                            requestFormsApproverEmailModelList = hiringR1ReplacementDB.GetApproverEmailList(formProcessIDs, objHiringR1ReplacementModel.RequesterEmployeeID);
                            SendRequestInitializeEmail(requestFormsApproverEmailModelList, true);
                        }
                    }
                    else
                    {
                        operationDetails.Success = true;
                        operationDetails.Message = "No permissions to update.";
                        operationDetails.CssClass = "error";
                        return Json(operationDetails, JsonRequestBehavior.AllowGet);
                    }
                    if (result > 0)
                    {
                        operationDetails.InsertedRowId = result;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request updated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while updating Details";
                        operationDetails.CssClass = "error";
                    }
                }
                else
                {

                    objHiringR1ReplacementModel.CreatedBy = objUserContextViewModel.UserId;
                    objHiringR1ReplacementModel.ModifiedBy = objUserContextViewModel.UserId;
                    objHiringR1ReplacementModel.RequesterEmployeeID = objUserContextViewModel.UserId;

                    requestFormsProcessModelList = hiringR1ReplacementDB.SaveForm(objHiringR1ReplacementModel);
                    if (requestFormsProcessModelList != null)
                    {
                        formProcessIDs = string.Join(",", requestFormsProcessModelList.Select(x => x.FormProcessID));
                        List<RequestFormsApproverEmailModel> requestFormsApproverEmailModelList = new List<RequestFormsApproverEmailModel>();
                        requestFormsApproverEmailModelList = hiringR1ReplacementDB.GetApproverEmailList(formProcessIDs, objHiringR1ReplacementModel.RequesterEmployeeID);
                        SendRequestInitializeEmail(requestFormsApproverEmailModelList, false);

                        operationDetails.InsertedRowId = 1;
                        operationDetails.Success = true;
                        operationDetails.Message = "Request generated successfully.";
                        operationDetails.CssClass = "success";
                    }
                    else
                    {
                        operationDetails.Success = false;
                        operationDetails.Message = "Error occured while Saving Details";
                        operationDetails.CssClass = "error";

                    }
                }
            }
            catch (Exception ex)
            {
                operationDetails.Success = false;
                operationDetails.Message = "Error occured while Saving Details";
                operationDetails.CssClass = "error";
                operationDetails.Exception = ex;
            }
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewDetails(int? id)
        {
            int detailId = id == null || id == 0 ? GetFormProcessSessionID() : (int)id;
            HiringR1ReplacementViewModel hiringR1ReplacementViewModel = new HiringR1ReplacementViewModel();
            hiringR1ReplacementViewModel = hiringR1ReplacementDB.GetFormDetails(detailId);
            List<RequestFormsApproveModel> requestFormsApproveModelList = hiringR1ReplacementDB.GetAllRequestFormsApprovals(detailId);
            hiringR1ReplacementViewModel.HiringR1ReplacementModel.RequestFormsApproveModelList = requestFormsApproveModelList;
            hiringR1ReplacementViewModel.HiringR1ReplacementModel.FormProcessID = detailId;
            return View("ViewDetail", hiringR1ReplacementViewModel);
        }
        public ActionResult GetR1ReplacementDetails(int? EmployeeID)
        {
            HiringR1ReplacementModel hiringR1ReplacementModel = new HiringR1ReplacementModel();
            hiringR1ReplacementModel = hiringR1ReplacementDB.GetR1ReplacementDetails(EmployeeID);
            return Json(hiringR1ReplacementModel, JsonRequestBehavior.AllowGet);
        }
        public List<SelectListItem> GetStaticSalaryRanges()
        {
            List<SelectListItem> listItem = new List<SelectListItem>();

            listItem.Add(new SelectListItem { Text = "0-1000", Value = "0-1000" });
            listItem.Add(new SelectListItem { Text = "1001-3000", Value = "1001-3000" });
            listItem.Add(new SelectListItem { Text = "3001-4000", Value = "3001-4000" });
            listItem.Add(new SelectListItem { Text = "4001-6000", Value = "4001-6000" });
            listItem.Add(new SelectListItem { Text = "6001-9000", Value = "6001-9000" });
            listItem.Add(new SelectListItem { Text = "9001-12000", Value = "9001-12000" });
            listItem.Add(new SelectListItem { Text = "12001-15000", Value = "12001-15000" });
            listItem.Add(new SelectListItem { Text = "15001-20000", Value = "15001-20000" });
            listItem.Add(new SelectListItem { Text = "20001-25000", Value = "20001-25000" });
            listItem.Add(new SelectListItem { Text = "25001-30000", Value = "25001-30000" });
            listItem.Add(new SelectListItem { Text = "30001-35000", Value = "30001-35000" });
            listItem.Add(new SelectListItem { Text = "35001-40000", Value = "35001-40000" });
            listItem.Add(new SelectListItem { Text = "40001-45000", Value = "40001-45000" });
            listItem.Add(new SelectListItem { Text = "45001-50000", Value = "45001-50000" });
            listItem.Add(new SelectListItem { Text = "50001-60000", Value = "50001-60000" });
            listItem.Add(new SelectListItem { Text = "60001-70000", Value = "60001-70000" });
            listItem.Add(new SelectListItem { Text = "70001-80000", Value = "70001-80000" });
            return listItem;
        }
    }
}